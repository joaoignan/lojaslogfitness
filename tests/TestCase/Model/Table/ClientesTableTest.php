<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClientesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClientesTable Test Case
 */
class ClientesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ClientesTable
     */
    public $Clientes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.clientes',
        'app.cities',
        'app.states',
        'app.countries',
        'app.academias',
        'app.status',
        'app.banners',
        'app.assine_pages',
        'app.embalagens',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.marcas',
        'app.produto_base',
        'app.propriedades',
        'app.produto_objetivos',
        'app.objetivos',
        'app.produtos',
        'app.interesses',
        'app.pedido_itens',
        'app.produto_comentarios',
        'app.produto_stars',
        'app.produto_combinations',
        'app.pagamento_formas',
        'app.transportadoras',
        'app.pedidos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Clientes') ? [] : ['className' => 'App\Model\Table\ClientesTable'];
        $this->Clientes = TableRegistry::get('Clientes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Clientes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
