<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AcademiasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AcademiasTable Test Case
 */
class AcademiasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AcademiasTable
     */
    public $Academias;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.academias',
        'app.cities',
        'app.states',
        'app.countries',
        'app.status',
        'app.banners',
        'app.assine_pages',
        'app.clientes',
        'app.embalagens',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.marcas',
        'app.produto_base',
        'app.propriedades',
        'app.produto_objetivos',
        'app.objetivos',
        'app.produtos',
        'app.interesses',
        'app.pedido_itens',
        'app.produto_comentarios',
        'app.produto_stars',
        'app.produto_combinations',
        'app.pagamento_formas',
        'app.transportadoras'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Academias') ? [] : ['className' => 'App\Model\Table\AcademiasTable'];
        $this->Academias = TableRegistry::get('Academias', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Academias);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
