<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProdutoBaseTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProdutoBaseTable Test Case
 */
class ProdutoBaseTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProdutoBaseTable
     */
    public $ProdutoBase;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.produto_base',
        'app.marcas',
        'app.status',
        'app.academias',
        'app.banners',
        'app.assine_pages',
        'app.clientes',
        'app.embalagens',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.objetivos',
        'app.produto_objetivos',
        'app.pagamento_formas',
        'app.produto_comentarios',
        'app.produtos',
        'app.interesses',
        'app.pedido_itens',
        'app.produto_stars',
        'app.transportadoras',
        'app.propriedades'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProdutoBase') ? [] : ['className' => 'App\Model\Table\ProdutoBaseTable'];
        $this->ProdutoBase = TableRegistry::get('ProdutoBase', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProdutoBase);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
