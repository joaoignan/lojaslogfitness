<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProdutoObjetivosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProdutoObjetivosTable Test Case
 */
class ProdutoObjetivosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProdutoObjetivosTable
     */
    public $ProdutoObjetivos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.produto_objetivos',
        'app.produto_base',
        'app.embalagens',
        'app.status',
        'app.academias',
        'app.banners',
        'app.assine_pages',
        'app.clientes',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.marcas',
        'app.objetivos',
        'app.pagamento_formas',
        'app.produto_comentarios',
        'app.produtos',
        'app.interesses',
        'app.pedido_itens',
        'app.produto_stars',
        'app.transportadoras'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProdutoObjetivos') ? [] : ['className' => 'App\Model\Table\ProdutoObjetivosTable'];
        $this->ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProdutoObjetivos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
