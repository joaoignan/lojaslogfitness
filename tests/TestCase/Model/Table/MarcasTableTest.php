<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MarcasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MarcasTable Test Case
 */
class MarcasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MarcasTable
     */
    public $Marcas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.marcas',
        'app.status',
        'app.academias',
        'app.banners',
        'app.assine_pages',
        'app.clientes',
        'app.embalagens',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.objetivos',
        'app.produto_objetivos',
        'app.produto_base',
        'app.propriedades',
        'app.produtos',
        'app.interesses',
        'app.pedido_itens',
        'app.produto_comentarios',
        'app.produto_stars',
        'app.pagamento_formas',
        'app.transportadoras'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Marcas') ? [] : ['className' => 'App\Model\Table\MarcasTable'];
        $this->Marcas = TableRegistry::get('Marcas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Marcas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeMarshal method
     *
     * @return void
     */
    public function testBeforeMarshal()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test wiUpload method
     *
     * @return void
     */
    public function testWiUpload()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test sanitizeString method
     *
     * @return void
     */
    public function testSanitizeString()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
