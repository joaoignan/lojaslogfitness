<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BannerEsportesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BannerEsportesTable Test Case
 */
class BannerEsportesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BannerEsportesTable
     */
    public $BannerEsportes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.banner_esportes',
        'app.banners',
        'app.status',
        'app.academias',
        'app.cities',
        'app.states',
        'app.countries',
        'app.clientes',
        'app.interesses',
        'app.pedidos',
        'app.transportadoras',
        'app.pedido_status',
        'app.pedido_itens',
        'app.produtos',
        'app.produto_base',
        'app.marcas',
        'app.fornecedores',
        'app.propriedades',
        'app.produto_objetivos',
        'app.objetivos',
        'app.produto_combinations',
        'app.produto_comentarios',
        'app.produto_stars',
        'app.pedido_pagamentos',
        'app.embalagens',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.pagamento_formas',
        'app.assine_pages',
        'app.esportes',
        'app.cliente_esportes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BannerEsportes') ? [] : ['className' => 'App\Model\Table\BannerEsportesTable'];
        $this->BannerEsportes = TableRegistry::get('BannerEsportes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BannerEsportes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
