<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TransportadorasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TransportadorasTable Test Case
 */
class TransportadorasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TransportadorasTable
     */
    public $Transportadoras;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.transportadoras',
        'app.cities',
        'app.states',
        'app.countries',
        'app.status',
        'app.academias',
        'app.clientes',
        'app.banners',
        'app.assine_pages',
        'app.embalagens',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.marcas',
        'app.produto_base',
        'app.propriedades',
        'app.produto_objetivos',
        'app.objetivos',
        'app.produtos',
        'app.interesses',
        'app.pedido_itens',
        'app.produto_comentarios',
        'app.produto_stars',
        'app.produto_combinations',
        'app.pagamento_formas',
        'app.pedidos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Transportadoras') ? [] : ['className' => 'App\Model\Table\TransportadorasTable'];
        $this->Transportadoras = TableRegistry::get('Transportadoras', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Transportadoras);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
