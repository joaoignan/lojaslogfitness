<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProdutoCombinationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProdutoCombinationsTable Test Case
 */
class ProdutoCombinationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProdutoCombinationsTable
     */
    public $ProdutoCombinations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.produto_combinations',
        'app.produtos',
        'app.produto_base',
        'app.marcas',
        'app.status',
        'app.academias',
        'app.banners',
        'app.assine_pages',
        'app.clientes',
        'app.embalagens',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.objetivos',
        'app.produto_objetivos',
        'app.pagamento_formas',
        'app.produto_comentarios',
        'app.transportadoras',
        'app.propriedades',
        'app.interesses',
        'app.pedido_itens',
        'app.produto_stars'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProdutoCombinations') ? [] : ['className' => 'App\Model\Table\ProdutoCombinationsTable'];
        $this->ProdutoCombinations = TableRegistry::get('ProdutoCombinations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProdutoCombinations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
