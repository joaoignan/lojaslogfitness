<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CategoriasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CategoriasTable Test Case
 */
class CategoriasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CategoriasTable
     */
    public $Categorias;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.categorias',
        'app.status',
        'app.produto_categorias',
        'app.produto_base',
        'app.marcas',
        'app.status',
        'app.academias',
        'app.cities',
        'app.states',
        'app.countries',
        'app.clientes',
        'app.interesses',
        'app.pedidos',
        'app.transportadoras',
        'app.pedido_status',
        'app.pedido_itens',
        'app.produtos',
        'app.produto_comentarios',
        'app.produto_stars',
        'app.pedido_pagamentos',
        'app.banners',
        'app.assine_pages',
        'app.embalagens',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.objetivos',
        'app.produto_objetivos',
        'app.pagamento_formas',
        'app.fornecedores',
        'app.propriedades',
        'app.produto_combinations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Categorias') ? [] : ['className' => 'App\Model\Table\CategoriasTable'];
        $this->Categorias = TableRegistry::get('Categorias', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Categorias);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
