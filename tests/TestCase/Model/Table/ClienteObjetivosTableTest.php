<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClienteObjetivosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClienteObjetivosTable Test Case
 */
class ClienteObjetivosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ClienteObjetivosTable
     */
    public $ClienteObjetivos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cliente_objetivos',
        'app.clientes',
        'app.cities',
        'app.states',
        'app.countries',
        'app.academias',
        'app.status',
        'app.banners',
        'app.assine_pages',
        'app.embalagens',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.marcas',
        'app.produto_base',
        'app.propriedades',
        'app.produto_objetivos',
        'app.objetivos',
        'app.produto_combinations',
        'app.produtos',
        'app.interesses',
        'app.pedido_itens',
        'app.pedidos',
        'app.transportadoras',
        'app.pedido_status',
        'app.pedido_pagamentos',
        'app.produto_comentarios',
        'app.produto_stars',
        'app.pagamento_formas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ClienteObjetivos') ? [] : ['className' => 'App\Model\Table\ClienteObjetivosTable'];
        $this->ClienteObjetivos = TableRegistry::get('ClienteObjetivos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ClienteObjetivos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
