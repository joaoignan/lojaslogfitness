<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PropriedadesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PropriedadesTable Test Case
 */
class PropriedadesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PropriedadesTable
     */
    public $Propriedades;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.propriedades',
        'app.status',
        'app.academias',
        'app.banners',
        'app.assine_pages',
        'app.clientes',
        'app.embalagens',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.marcas',
        'app.objetivos',
        'app.produto_objetivos',
        'app.produto_base',
        'app.produtos',
        'app.interesses',
        'app.pedido_itens',
        'app.produto_comentarios',
        'app.produto_stars',
        'app.pagamento_formas',
        'app.transportadoras'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Propriedades') ? [] : ['className' => 'App\Model\Table\PropriedadesTable'];
        $this->Propriedades = TableRegistry::get('Propriedades', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Propriedades);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
