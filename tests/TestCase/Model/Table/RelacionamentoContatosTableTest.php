<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RelacionamentoContatosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RelacionamentoContatosTable Test Case
 */
class RelacionamentoContatosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RelacionamentoContatosTable
     */
    public $RelacionamentoContatos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.relacionamento_contatos',
        'app.status',
        'app.academias',
        'app.cities',
        'app.states',
        'app.countries',
        'app.clientes',
        'app.banners',
        'app.assine_pages',
        'app.embalagens',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.marcas',
        'app.produto_base',
        'app.propriedades',
        'app.produto_objetivos',
        'app.objetivos',
        'app.produtos',
        'app.interesses',
        'app.pedido_itens',
        'app.produto_comentarios',
        'app.produto_stars',
        'app.produto_combinations',
        'app.pagamento_formas',
        'app.transportadoras',
        'app.pedidos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RelacionamentoContatos') ? [] : ['className' => 'App\Model\Table\RelacionamentoContatosTable'];
        $this->RelacionamentoContatos = TableRegistry::get('RelacionamentoContatos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RelacionamentoContatos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
