<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ProfessoresController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ProfessoresController Test Case
 */
class ProfessoresControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.professores',
        'app.cities',
        'app.states',
        'app.countries',
        'app.status',
        'app.academias',
        'app.users',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.academia_banners',
        'app.academia_comissoes',
        'app.academia_esportes',
        'app.esportes',
        'app.cliente_esportes',
        'app.clientes',
        'app.interesses',
        'app.pedidos',
        'app.transportadoras',
        'app.pedido_status',
        'app.pedido_itens',
        'app.produtos',
        'app.produto_base',
        'app.marcas',
        'app.fornecedores',
        'app.propriedades',
        'app.produto_objetivos',
        'app.objetivos',
        'app.produto_categorias',
        'app.categorias',
        'app.produto_combinations',
        'app.produto_comentarios',
        'app.produto_stars',
        'app.pedido_pagamentos',
        'app.banners',
        'app.banner_objetivos',
        'app.banner_esportes',
        'app.embalagens',
        'app.pagamento_formas'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
