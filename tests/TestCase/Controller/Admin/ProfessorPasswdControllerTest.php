<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\ProfessorPasswdController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\ProfessorPasswdController Test Case
 */
class ProfessorPasswdControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'ProfessorPasswd' => 'app.professor_passwd',
        'Professores' => 'app.professores',
        'Cities' => 'app.cities',
        'States' => 'app.states',
        'Countries' => 'app.countries',
        'Status' => 'app.status',
        'Academias' => 'app.academias',
        'Users' => 'app.users',
        'Groups' => 'app.groups',
        'Cca' => 'app.cca',
        'CcaActions' => 'app.cca_actions',
        'AcademiaBanners' => 'app.academia_banners',
        'AcademiaComissoes' => 'app.academia_comissoes',
        'AcademiaEsportes' => 'app.academia_esportes',
        'Esportes' => 'app.esportes',
        'ClienteEsportes' => 'app.cliente_esportes',
        'Clientes' => 'app.clientes',
        'Professor' => 'app.professor',
        'Interesses' => 'app.interesses',
        'Pedidos' => 'app.pedidos',
        'Transportadoras' => 'app.transportadoras',
        'PedidoStatus' => 'app.pedido_status',
        'PedidoItens' => 'app.pedido_itens',
        'Produtos' => 'app.produtos',
        'ProdutoBase' => 'app.produto_base',
        'Marcas' => 'app.marcas',
        'Fornecedores' => 'app.fornecedores',
        'Propriedades' => 'app.propriedades',
        'ProdutoObjetivos' => 'app.produto_objetivos',
        'Objetivos' => 'app.objetivos',
        'ProdutoCategorias' => 'app.produto_categorias',
        'Categorias' => 'app.categorias',
        'ProdutoCombinations' => 'app.produto_combinations',
        'ProdutoComentarios' => 'app.produto_comentarios',
        'ProdutoStars' => 'app.produto_stars',
        'PedidoPagamentos' => 'app.pedido_pagamentos',
        'ProfessorAcademias' => 'app.professor_academias',
        'Banners' => 'app.banners',
        'BannerObjetivos' => 'app.banner_objetivos',
        'BannerEsportes' => 'app.banner_esportes',
        'Embalagens' => 'app.embalagens',
        'PagamentoFormas' => 'app.pagamento_formas'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
