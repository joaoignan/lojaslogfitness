<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\PropriedadesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\PropriedadesController Test Case
 */
class PropriedadesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.propriedades',
        'app.status',
        'app.academias',
        'app.banners',
        'app.assine_pages',
        'app.clientes',
        'app.embalagens',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.marcas',
        'app.objetivos',
        'app.produto_objetivos',
        'app.produto_base',
        'app.produtos',
        'app.interesses',
        'app.pedido_itens',
        'app.produto_comentarios',
        'app.produto_stars',
        'app.pagamento_formas',
        'app.transportadoras'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
