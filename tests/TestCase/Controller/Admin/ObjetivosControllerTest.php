<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\ObjetivosController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\ObjetivosController Test Case
 */
class ObjetivosControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'Objetivos' => 'app.objetivos',
        'Status' => 'app.status',
        'Academias' => 'app.academias',
        'Banners' => 'app.banners',
        'AssinePages' => 'app.assine_pages',
        'Clientes' => 'app.clientes',
        'Embalagens' => 'app.embalagens',
        'Groups' => 'app.groups',
        'Cca' => 'app.cca',
        'CcaActions' => 'app.cca_actions',
        'Users' => 'app.users',
        'Marcas' => 'app.marcas',
        'PagamentoFormas' => 'app.pagamento_formas',
        'ProdutoBase' => 'app.produto_base',
        'ProdutoComentarios' => 'app.produto_comentarios',
        'Produtos' => 'app.produtos',
        'Transportadoras' => 'app.transportadoras',
        'ProdutoObjetivos' => 'app.produto_objetivos'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
