<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\BannersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\BannersController Test Case
 */
class BannersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'Banners' => 'app.banners',
        'Status' => 'app.status',
        'Academias' => 'app.academias',
        'Clientes' => 'app.clientes',
        'Embalagens' => 'app.embalagens',
        'Groups' => 'app.groups',
        'Cca' => 'app.cca',
        'CcaActions' => 'app.cca_actions',
        'Users' => 'app.users',
        'Marcas' => 'app.marcas',
        'ProdutoBase' => 'app.produto_base',
        'Propriedades' => 'app.propriedades',
        'ProdutoObjetivos' => 'app.produto_objetivos',
        'Objetivos' => 'app.objetivos',
        'Produtos' => 'app.produtos',
        'Interesses' => 'app.interesses',
        'PedidoItens' => 'app.pedido_itens',
        'ProdutoComentarios' => 'app.produto_comentarios',
        'ProdutoStars' => 'app.produto_stars',
        'PagamentoFormas' => 'app.pagamento_formas',
        'Transportadoras' => 'app.transportadoras',
        'AssinePages' => 'app.assine_pages'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
