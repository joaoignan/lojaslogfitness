<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\ClientesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\ClientesController Test Case
 */
class ClientesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'Clientes' => 'app.clientes',
        'Cities' => 'app.cities',
        'States' => 'app.states',
        'Countries' => 'app.countries',
        'Academias' => 'app.academias',
        'Status' => 'app.status',
        'Banners' => 'app.banners',
        'AssinePages' => 'app.assine_pages',
        'Embalagens' => 'app.embalagens',
        'Groups' => 'app.groups',
        'Cca' => 'app.cca',
        'CcaActions' => 'app.cca_actions',
        'Users' => 'app.users',
        'Marcas' => 'app.marcas',
        'ProdutoBase' => 'app.produto_base',
        'Propriedades' => 'app.propriedades',
        'ProdutoObjetivos' => 'app.produto_objetivos',
        'Objetivos' => 'app.objetivos',
        'ProdutoCombinations' => 'app.produto_combinations',
        'Produtos' => 'app.produtos',
        'Interesses' => 'app.interesses',
        'PedidoItens' => 'app.pedido_itens',
        'Pedidos' => 'app.pedidos',
        'Transportadoras' => 'app.transportadoras',
        'PedidoStatus' => 'app.pedido_status',
        'PedidoPagamentos' => 'app.pedido_pagamentos',
        'ProdutoComentarios' => 'app.produto_comentarios',
        'ProdutoStars' => 'app.produto_stars',
        'PagamentoFormas' => 'app.pagamento_formas'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
