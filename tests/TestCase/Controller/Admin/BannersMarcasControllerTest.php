<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\BannersMarcasController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\BannersMarcasController Test Case
 */
class BannersMarcasControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.banners_marcas',
        'app.marcas',
        'app.status',
        'app.academias',
        'app.cities',
        'app.states',
        'app.countries',
        'app.clientes',
        'app.interesses',
        'app.pedidos',
        'app.transportadoras',
        'app.pedido_status',
        'app.pedido_itens',
        'app.produtos',
        'app.produto_base',
        'app.fornecedores',
        'app.propriedades',
        'app.produto_objetivos',
        'app.objetivos',
        'app.produto_categorias',
        'app.categorias',
        'app.produto_combinations',
        'app.produto_comentarios',
        'app.produto_stars',
        'app.pedido_pagamentos',
        'app.banners',
        'app.banner_objetivos',
        'app.banner_esportes',
        'app.esportes',
        'app.cliente_esportes',
        'app.embalagens',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.pagamento_formas'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
