<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\AcademiasController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\AcademiasController Test Case
 */
class AcademiasControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'Academias' => 'app.academias',
        'Cities' => 'app.cities',
        'States' => 'app.states',
        'Countries' => 'app.countries',
        'Status' => 'app.status',
        'Banners' => 'app.banners',
        'AssinePages' => 'app.assine_pages',
        'Clientes' => 'app.clientes',
        'Interesses' => 'app.interesses',
        'Pedidos' => 'app.pedidos',
        'Transportadoras' => 'app.transportadoras',
        'PedidoStatus' => 'app.pedido_status',
        'PedidoItens' => 'app.pedido_itens',
        'Produtos' => 'app.produtos',
        'ProdutoBase' => 'app.produto_base',
        'Marcas' => 'app.marcas',
        'Propriedades' => 'app.propriedades',
        'ProdutoObjetivos' => 'app.produto_objetivos',
        'Objetivos' => 'app.objetivos',
        'ProdutoCombinations' => 'app.produto_combinations',
        'ProdutoComentarios' => 'app.produto_comentarios',
        'ProdutoStars' => 'app.produto_stars',
        'PedidoPagamentos' => 'app.pedido_pagamentos',
        'Embalagens' => 'app.embalagens',
        'Groups' => 'app.groups',
        'Cca' => 'app.cca',
        'CcaActions' => 'app.cca_actions',
        'Users' => 'app.users',
        'PagamentoFormas' => 'app.pagamento_formas'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
