<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\ProdutosController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\ProdutosController Test Case
 */
class ProdutosControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.produtos',
        'app.produto_base',
        'app.embalagens',
        'app.status',
        'app.academias',
        'app.banners',
        'app.assine_pages',
        'app.clientes',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.marcas',
        'app.objetivos',
        'app.produto_objetivos',
        'app.pagamento_formas',
        'app.produto_comentarios',
        'app.transportadoras',
        'app.interesses',
        'app.pedido_itens',
        'app.produto_stars'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
