<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\AcademiaComissoesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\AcademiaComissoesController Test Case
 */
class AcademiaComissoesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.academia_comissoes',
        'app.academias',
        'app.cities',
        'app.states',
        'app.countries',
        'app.status',
        'app.banners',
        'app.banner_objetivos',
        'app.objetivos',
        'app.produto_objetivos',
        'app.produto_base',
        'app.marcas',
        'app.fornecedores',
        'app.propriedades',
        'app.produto_categorias',
        'app.categorias',
        'app.produto_combinations',
        'app.produtos',
        'app.interesses',
        'app.pedido_itens',
        'app.pedidos',
        'app.clientes',
        'app.produto_stars',
        'app.transportadoras',
        'app.pedido_status',
        'app.pedido_pagamentos',
        'app.produto_comentarios',
        'app.banner_esportes',
        'app.esportes',
        'app.cliente_esportes',
        'app.embalagens',
        'app.groups',
        'app.cca',
        'app.cca_actions',
        'app.users',
        'app.pagamento_formas'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
