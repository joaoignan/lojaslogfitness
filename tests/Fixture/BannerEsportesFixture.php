<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BannerEsportesFixture
 *
 */
class BannerEsportesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'banner_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'esporte_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'esporte_id' => ['type' => 'index', 'columns' => ['esporte_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'unique_banner_esporte' => ['type' => 'unique', 'columns' => ['banner_id', 'esporte_id'], 'length' => []],
            'banner_esportes_ibfk_2' => ['type' => 'foreign', 'columns' => ['esporte_id'], 'references' => ['esportes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'banner_esportes_ibfk_1' => ['type' => 'foreign', 'columns' => ['banner_id'], 'references' => ['banners', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'banner_id' => 1,
            'esporte_id' => 1,
            'created' => '2016-04-19 11:08:10'
        ],
    ];
}
