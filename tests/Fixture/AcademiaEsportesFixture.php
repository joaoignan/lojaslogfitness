<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AcademiaEsportesFixture
 *
 */
class AcademiaEsportesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'academia_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'esporte_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'cliente_id' => ['type' => 'index', 'columns' => ['academia_id'], 'length' => []],
            'esporte_id' => ['type' => 'index', 'columns' => ['esporte_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'academia_esportes_ibfk_1' => ['type' => 'foreign', 'columns' => ['academia_id'], 'references' => ['academias', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'academia_esportes_ibfk_2' => ['type' => 'foreign', 'columns' => ['esporte_id'], 'references' => ['esportes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'academia_id' => 1,
            'esporte_id' => 1,
            'created' => '2016-04-23 03:11:10'
        ],
    ];
}
