<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ProdutoObjetivosFixture
 *
 */
class ProdutoObjetivosFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'produto_base_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'objetivo_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'produto_base_id' => ['type' => 'index', 'columns' => ['produto_base_id'], 'length' => []],
            'objetivo_id' => ['type' => 'index', 'columns' => ['objetivo_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'produto_objetivos_ibfk_2' => ['type' => 'foreign', 'columns' => ['objetivo_id'], 'references' => ['objetivos', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'produto_objetivos_ibfk_1' => ['type' => 'foreign', 'columns' => ['produto_base_id'], 'references' => ['produto_base', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'produto_base_id' => 1,
            'objetivo_id' => 1,
            'created' => '2016-03-11 12:12:40'
        ],
    ];
}
