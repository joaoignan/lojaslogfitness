<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ProfessorAcademiasFixture
 *
 */
class ProfessorAcademiasFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'professor_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'academia_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'professor_id' => ['type' => 'index', 'columns' => ['professor_id'], 'length' => []],
            'academia_id' => ['type' => 'index', 'columns' => ['academia_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'professor_academias_ibfk_2' => ['type' => 'foreign', 'columns' => ['academia_id'], 'references' => ['academias', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'professor_academias_ibfk_1' => ['type' => 'foreign', 'columns' => ['professor_id'], 'references' => ['professores', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'professor_id' => 1,
            'academia_id' => 1,
            'created' => '2016-06-30 11:49:02'
        ],
    ];
}
