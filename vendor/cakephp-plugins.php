<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'MaxAdm' => $baseDir . '/plugins/MaxAdm/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'Mongodb' => $baseDir . '/plugins/Mongodb/',
        'PagSeguroLibrary' => $baseDir . '/plugins/PagSeguroLibrary/',
        'WideImage' => $baseDir . '/plugins/WideImage/'
    ]
];