<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return !in_array($schema->columnType($field), ['binary', 'text']);
    })
    ->take(7);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <CakePHPBakeOpenTag= __('Actions') CakePHPBakeCloseTag>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <CakePHPBakeOpenTag= $this->Html->link(__('New <?= $singularHumanName ?>'), ['action' => 'add'], ['class' => 'btn btn-default']) CakePHPBakeCloseTag>

        <?php
        $done = [];
        foreach ($associations as $type => $data):
        foreach ($data as $alias => $details):
        if ($details['controller'] != $this->name && !in_array($details['controller'], $done)):
        ?>
        <CakePHPBakeOpenTag= $this->Html->link(__('List <?= $this->_pluralHumanName($alias) ?>'),
            ['controller' => '<?= $details['controller'] ?>', 'action' => 'index'], ['class' => 'btn btn-default']) CakePHPBakeCloseTag>
        <CakePHPBakeOpenTag= $this->Html->link(__('New <?= $this->_singularHumanName($alias) ?>'),
        ['controller' => '<?= $details['controller'] ?>', 'action' => 'add'], ['class' => 'btn btn-default']) CakePHPBakeCloseTag>
        <?php
        $done[] = $details['controller'];
        endif;
        endforeach;
        endforeach;
        ?>
    </div>

</div>

<div class="<?= $pluralVar ?> index">
    <table class="table table-hover">
    <thead>
        <tr>
    <?php foreach ($fields as $field): ?>
        <th><CakePHPBakeOpenTag= $this->Paginator->sort('<?= $field ?>') CakePHPBakeCloseTag></th>
    <?php endforeach; ?>
        <th class="actions"></th>
        </tr>
    </thead>
    <tbody>
    <CakePHPBakeOpenTagphp foreach ($<?= $pluralVar ?> as $<?= $singularVar ?>): CakePHPBakeCloseTag>
        <tr>
<?php        foreach ($fields as $field) {
            $isKey = false;
            if (!empty($associations['BelongsTo'])) {
                foreach ($associations['BelongsTo'] as $alias => $details) {
                    if ($field === $details['foreignKey']) {
                        $isKey = true;
?>
            <td>
                <CakePHPBakeOpenTag= $<?= $singularVar ?>->has('<?= $details['property'] ?>') ? $this->Html->link($<?= $singularVar ?>-><?= $details['property'] ?>-><?= $details['displayField'] ?>, ['controller' => '<?= $details['controller'] ?>', 'action' => 'view', $<?= $singularVar ?>-><?= $details['property'] ?>-><?= $details['primaryKey'][0] ?>]) : '' CakePHPBakeCloseTag>
            </td>
<?php
                        break;
                    }
                }
            }
            if ($isKey !== true) {
                if (!in_array($schema->columnType($field), ['integer', 'biginteger', 'decimal', 'float'])) {
?>
            <td><CakePHPBakeOpenTag= h($<?= $singularVar ?>-><?= $field ?>) CakePHPBakeCloseTag></td>
<?php
                } else {
?>
            <td><CakePHPBakeOpenTag= $this->Number->format($<?= $singularVar ?>-><?= $field ?>) CakePHPBakeCloseTag></td>
<?php
                }
            }
        }

        $pk = '$' . $singularVar . '->' . $primaryKey[0];
?>
            <td class="actions">
                <div class="btn-group">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                        <i class="glyph-icon icon-navicon"></i>
                        <span class="sr-only"><CakePHPBakeOpenTag= __('Actions'); CakePHPBakeCloseTag></span>
                    </button>
                    <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                        <li>
                            <CakePHPBakeOpenTag= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                            ['action' => 'view', <?= $pk ?>],
                            ['escape' => false]) CakePHPBakeCloseTag>
                        </li>
                        <li>
                            <CakePHPBakeOpenTag= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                            ['action' => 'edit', <?= $pk ?>],
                            ['escape' => false]) CakePHPBakeCloseTag>
                        </li>
                        <li>
                            <CakePHPBakeOpenTag= $this->Form->postLink('<i class="glyph-icon icon-trash-o"></i>  '.__('Delete'),
                            ['action' => 'delete', <?= $pk ?>],
                            ['confirm' => __('Are you sure you want to delete # {0}?', <?= $pk ?>), 'escape' => false]) CakePHPBakeCloseTag>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>

    <CakePHPBakeOpenTagphp endforeach; CakePHPBakeCloseTag>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <CakePHPBakeOpenTag= $this->Paginator->prev('< ' . __('previous')) CakePHPBakeCloseTag>
            <CakePHPBakeOpenTag= $this->Paginator->numbers() CakePHPBakeCloseTag>
            <CakePHPBakeOpenTag= $this->Paginator->next(__('next') . ' >') CakePHPBakeCloseTag>
        </ul>
        <p><CakePHPBakeOpenTag= $this->Paginator->counter() CakePHPBakeCloseTag></p>
    </div>
</div>


