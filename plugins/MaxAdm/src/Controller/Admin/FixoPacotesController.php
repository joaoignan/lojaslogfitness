<?php
namespace MaxAdm\Controller\Admin;

use MaxAdm\Controller\AppController;

/**
 * FixoPacotes Controller
 *
 * @property \MaxAdm\Model\Table\FixoPacotesTable $FixoPacotes
 */
class FixoPacotesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Status']
        ];
        $this->set('fixoPacotes', $this->paginate($this->FixoPacotes));
        $this->set('_serialize', ['fixoPacotes']);
    }

    /**
     * View method
     *
     * @param string|null $id Fixo Pacote id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fixoPacote = $this->FixoPacotes->get($id, [
            'contain' => ['Status', 'Combinados']
        ]);
        $this->set('fixoPacote', $fixoPacote);
        $this->set('_serialize', ['fixoPacote']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fixoPacote = $this->FixoPacotes->newEntity();
        if ($this->request->is('post')) {
            $fixoPacote = $this->FixoPacotes->patchEntity($fixoPacote, $this->request->data);
            if ($this->FixoPacotes->save($fixoPacote)) {
                $this->Flash->success('The fixo pacote has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The fixo pacote could not be saved. Please, try again.');
            }
        }
        $status = $this->FixoPacotes->Status->find('list', ['limit' => 200]);
        $this->set(compact('fixoPacote', 'status'));
        $this->set('_serialize', ['fixoPacote']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fixo Pacote id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $fixoPacote = $this->FixoPacotes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fixoPacote = $this->FixoPacotes->patchEntity($fixoPacote, $this->request->data);
            if ($this->FixoPacotes->save($fixoPacote)) {
                $this->Flash->success('The fixo pacote has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The fixo pacote could not be saved. Please, try again.');
            }
        }
        $status = $this->FixoPacotes->Status->find('list', ['limit' => 200]);
        $this->set(compact('fixoPacote', 'status'));
        $this->set('_serialize', ['fixoPacote']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fixo Pacote id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fixoPacote = $this->FixoPacotes->get($id);
        if ($this->FixoPacotes->delete($fixoPacote)) {
            $this->Flash->success('The fixo pacote has been deleted.');
        } else {
            $this->Flash->error('The fixo pacote could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
