<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return !in_array($schema->columnType($field), ['binary', 'text']);
    })
    ->take(7);
%>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= $this->Html->link(__('New <%= $singularHumanName %>'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>

        <%
        $done = [];
        foreach ($associations as $type => $data):
        foreach ($data as $alias => $details):
        if ($details['controller'] != $this->name && !in_array($details['controller'], $done)):
        %>
        <?= $this->Html->link(__('List <%= $this->_pluralHumanName($alias) %>'),
            ['controller' => '<%= $details['controller'] %>', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New <%= $this->_singularHumanName($alias) %>'),
        ['controller' => '<%= $details['controller'] %>', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <%
        $done[] = $details['controller'];
        endif;
        endforeach;
        endforeach;
        %>
    </div>

</div>

<div class="<%= $pluralVar %> index">
    <table class="table table-hover">
    <thead>
        <tr>
    <% foreach ($fields as $field): %>
        <th><?= $this->Paginator->sort('<%= $field %>') ?></th>
    <% endforeach; %>
        <th class="actions"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($<%= $pluralVar %> as $<%= $singularVar %>): ?>
        <tr>
<%        foreach ($fields as $field) {
            $isKey = false;
            if (!empty($associations['BelongsTo'])) {
                foreach ($associations['BelongsTo'] as $alias => $details) {
                    if ($field === $details['foreignKey']) {
                        $isKey = true;
%>
            <td>
                <?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?>
            </td>
<%
                        break;
                    }
                }
            }
            if ($isKey !== true) {
                if (!in_array($schema->columnType($field), ['integer', 'biginteger', 'decimal', 'float'])) {
%>
            <td><?= h($<%= $singularVar %>-><%= $field %>) ?></td>
<%
                } else {
%>
            <td><?= $this->Number->format($<%= $singularVar %>-><%= $field %>) ?></td>
<%
                }
            }
        }

        $pk = '$' . $singularVar . '->' . $primaryKey[0];
%>
            <td class="actions">
                <div class="btn-group">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                        <i class="glyph-icon icon-navicon"></i>
                        <span class="sr-only"><?= __('Actions'); ?></span>
                    </button>
                    <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                        <li>
                            <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                            ['action' => 'view', <%= $pk %>],
                            ['escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                            ['action' => 'edit', <%= $pk %>],
                            ['escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Form->postLink('<i class="glyph-icon icon-trash-o"></i>  '.__('Delete'),
                            ['action' => 'delete', <%= $pk %>],
                            ['confirm' => __('Are you sure you want to delete # {0}?', <%= $pk %>), 'escape' => false]) ?>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


