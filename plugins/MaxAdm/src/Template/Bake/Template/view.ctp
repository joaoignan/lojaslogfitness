<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$associations += ['BelongsTo' => [], 'HasOne' => [], 'HasMany' => [], 'BelongsToMany' => []];
$immediateAssociations = $associations['BelongsTo'] + $associations['HasOne'];
$associationFields = collection($fields)
    ->map(function($field) use ($immediateAssociations) {
        foreach ($immediateAssociations as $alias => $details) {
            if ($field === $details['foreignKey']) {
                return [$field => $details];
            }
        }
    })
    ->filter()
    ->reduce(function($fields, $value) {
        return $fields + $value;
    }, []);

$groupedFields = collection($fields)
    ->filter(function($field) use ($schema) {
        return $schema->columnType($field) !== 'binary';
    })
    ->groupBy(function($field) use ($schema, $associationFields) {
        $type = $schema->columnType($field);
        if (isset($associationFields[$field])) {
            return 'string';
        }
        if (in_array($type, ['integer', 'float', 'decimal', 'biginteger'])) {
            return 'number';
        }
        if (in_array($type, ['date', 'time', 'datetime', 'timestamp'])) {
            return 'date';
        }
        return in_array($type, ['text', 'boolean']) ? $type : 'string';
    })
    ->toArray();

$groupedFields += ['number' => [], 'string' => [], 'boolean' => [], 'date' => [], 'text' => []];
$pk = "\$$singularVar->{$primaryKey[0]}";
%>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?= $this->Html->link(__('Edit <%= $singularHumanName %>'), ['action' => 'edit', <%= $pk %>], ['class' => 'btn btn-default']) ?>
        <?= $this->Form->postLink(__('Delete <%= $singularHumanName %>'), ['action' => 'delete', <%= $pk %>],
        ['confirm' => __('Are you sure you want to delete # {0}?', <%= $pk %>), 'class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('List <%= $pluralHumanName %>'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New <%= $singularHumanName %>'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
            <%
            $done = [];
            foreach ($associations as $type => $data) {
            foreach ($data as $alias => $details) {
            if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
            %>
        <?= $this->Html->link(__('List <%= $this->_pluralHumanName($alias) %>'),
        ['controller' => '<%= $details['controller'] %>', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New <%= Inflector::humanize(Inflector::singularize(Inflector::underscore($alias))) %>'),
        ['controller' => '<%= $details['controller'] %>', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <%
        $done[] = $details['controller'];
        }
        }
        }
        %>
    </div>

</div>

<div class="<%= $pluralVar %> view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($<%= $singularVar %>-><%= $displayField %>) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <% if ($groupedFields['string']) : %>
                <table class="table table-hover">
                    <% foreach ($groupedFields['string'] as $field) : %>
                        <% if (isset($associationFields[$field])) :
                                    $details = $associationFields[$field];
                        %>
                            <tr>
                            <td class="font-bold"><?= __('<%= Inflector::humanize($details['property']) %>') ?></td>
                            <td><?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?></td>
                            </tr>
                        <% else : %>
                            <tr>
                            <td class="font-bold"><?= __('<%= Inflector::humanize($field) %>') ?></td>
                            <td><?= h($<%= $singularVar %>-><%= $field %>) ?></td>
                            </tr>
                        <% endif; %>
                    <% endforeach; %>
                    </table>
            <% endif; %>
            </div>

            <% if ($groupedFields['number']) : %>
                <div class="col-md-3">
                    <table class="table table-hover">
                    <% foreach ($groupedFields['number'] as $field) : %>
                        <tr>
                            <td class="font-bold"><?= __('<%= Inflector::humanize($field) %>') ?></td>
                            <td><?= $this->Number->format($<%= $singularVar %>-><%= $field %>) ?></td>
                        </tr>
                    <% endforeach; %>
            <% endif; %>
            <% if ($groupedFields['date']) : %>
                <% foreach ($groupedFields['date'] as $field) : %>
                    <tr>
                        <td class="font-bold"><%= "<%= __('" . Inflector::humanize($field) . "') %>" %></td>
                        <td><?= h($<%= $singularVar %>-><%= $field %>) ?></td>
                    </tr>
                <% endforeach; %>
            <% endif; %>
            <% if ($groupedFields['boolean']) : %>
                <% foreach ($groupedFields['boolean'] as $field) : %>
                    <tr>
                    <td class="font-bold"><?= __('<%= Inflector::humanize($field) %>') ?></td>
                    <td><?= $<%= $singularVar %>-><%= $field %> ? __('Yes') : __('No'); ?></td>
                    </tr>
                <% endforeach; %>
            <% endif; %>
            <% if ($groupedFields['text']) : %>
            <% foreach ($groupedFields['text'] as $field) : %>

                        <tr>
                        <td class="font-bold"><?= __('<%= Inflector::humanize($field) %>') ?></td>
                        <?= $this->Text->autoParagraph(h($<%= $singularVar %>-><%= $field %>)); ?>
                        </tr>
            <% endforeach; %>
            <% endif; %>
             </table>
            </div>
        </div>
    </div>
<%
$relations = $associations['HasMany'] + $associations['BelongsToMany'];
foreach ($relations as $alias => $details):
    $otherSingularVar = Inflector::variable($alias);
    $otherPluralHumanName = Inflector::humanize($details['controller']);
    %>
<div class="row">
    <div class="column large-12">
    <h4 class="font-bold"><?= __('Related <%= $otherPluralHumanName %>') ?></h4>
    <?php if (!empty($<%= $singularVar %>-><%= $details['property'] %>)): ?>
    <table class="table table-hover">
        <tr>
<% foreach ($details['fields'] as $field): %>
            <th><?= __('<%= Inflector::humanize($field) %>') ?></th>
<% endforeach; %>
            <th class="actions"></th>
        </tr>
        <?php foreach ($<%= $singularVar %>-><%= $details['property'] %> as $<%= $otherSingularVar %>): ?>
        <tr>
            <%- foreach ($details['fields'] as $field): %>
            <td><?= h($<%= $otherSingularVar %>-><%= $field %>) ?></td>
            <%- endforeach; %>

            <%- $otherPk = "\${$otherSingularVar}->{$details['primaryKey'][0]}"; %>
            <td class="actions">
                <div class="btn-group">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                        <i class="glyph-icon icon-navicon"></i>
                        <span class="sr-only"><?= __('Actions'); ?></span>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu" style="left: -110px;">
                        <li>
                            <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                            ['<%= $details['controller'] %>', 'action' => 'view', <%= $otherPk %>],
                            ['escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                            ['controller' => '<%= $details['controller'] %>', 'action' => 'edit', <%= $otherPk %>],
                            ['escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Form->postLink('<i class="glyph-icon icon-trash-o"></i>  '.__('Delete'),
                            ['controller' => '<%= $details['controller'] %>', 'action' => 'delete', <%= $otherPk %>],
                            ['confirm' => __('Are you sure you want to delete # {0}?', <%= $otherPk %>), 'escape' => false]) ?>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<% endforeach; %>


