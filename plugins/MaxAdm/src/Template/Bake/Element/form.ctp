<%
/**
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @since         0.1.0
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/
use Cake\Utility\Inflector;

$fields = collection($fields)
->filter(function($field) use ($schema) {
return $schema->columnType($field) !== 'binary';
});
%>
<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <% if (strpos($action, 'add') === false): %>
        <?= $this->Form->postLink(
            __('Delete'),
            ['action' => 'delete', $<%= $singularVar %>-><%= $primaryKey[0] %>],
        ['confirm' => __('Are you sure you want to delete # {0}?', $<%= $singularVar %>-><%= $primaryKey[0] %>),
        'class' => 'btn btn-default']
        )
        ?>
            <% endif; %>
        <?= $this->Html->link(__('List <%= $pluralHumanName %>'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
            <%
            $done = [];
            foreach ($associations as $type => $data) {
            foreach ($data as $alias => $details) {
            if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
            %>
        <?= $this->Html->link(__('List <%= $this->_pluralHumanName($alias) %>'),
        ['controller' => '<%= $details['controller'] %>', 'action' => 'index'],
        ['class' => 'btn btn-default']) %>
        <?= $this->Html->link(__('New <%= $this->_singularHumanName($alias) %>'),
        ['controller' => '<%= $details['controller'] %>', 'action' => 'add'],
        ['class' => 'btn btn-default']) %>
        <%
        $done[] = $details['controller'];
        }
        }
        }
        %>
        </div>
        </div>

        <div class="<%= $pluralVar %> form">
        <?= $this->Form->create($<%= $singularVar %>, ['class' => 'form-horizontal bordered-row']); ?>
        <fieldset>
            <legend><?= __('<%= Inflector::humanize($action) %> <%= $singularHumanName %>') ?></legend>
            <%
            foreach ($fields as $field) {
            if (in_array($field, $primaryKey)) {
            continue;
            }
            if (isset($keyFields[$field])) {
            $fieldData = $schema->column($field);
            if (!empty($fieldData['null'])) {
            %>
            <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('<%= $field %>') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('<%= $field %>', [
                    'options' => $<%= $keyFields[$field] %>,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select'
                    ]) ?>
                </div>
            </div>

            <%
            } else {
            %>
            <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('<%= $field %>') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('<%= $field %>', [
                    'options' => $<%= $keyFields[$field] %>,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[]'
                    ]) ?>
                </div>
            </div>

            <%
            }
            continue;
            }
            if (!in_array($field, ['created', 'modified', 'updated'])) {
            $fieldData = $schema->column($field);
            if (($fieldData['type'] === 'date') && (!empty($fieldData['null']))) {
            %>
            <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('<%= $field %>') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('<%= $field %>', array(
                    'empty' => true,
                    'default' => '',
                    'label' => false,
                    'class' => 'form-control validate[]'
                    )) ?>
                </div>
            </div>
            <%
            } else {
            %>
            <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('<%= $field %>') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('<%= $field %>', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

            <%
            }
            }
            }
            if (!empty($associations['BelongsToMany'])) {
            foreach ($associations['BelongsToMany'] as $assocName => $assocData) {
            %>
            <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('<%= $assocData['property'] %>._ids') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('<%= $assocData['property'] %>._ids', [
                    'options' => $<%= $assocData['variable'] %>,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[]'
                    ]) ?>
                </div>
            </div>
            <%
            }
            }
            %>
            <div class="form-group text-center">
                <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                'escape' => false]); ?>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>


