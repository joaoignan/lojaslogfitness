<head>
    <?= $this->Html->css('owl.carousel') ?>
    <?= $this->Html->css('owl.theme') ?>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <?= $this->Html->script('owl-carousel/owl.carousel.min')?>
</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#page-top" style=" padding-top: 5px; padding-bottom: 5px;"><img src="//logfitness.r.worldssl.net/webroot/images/logfitness.png" width="170px" /></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li id="link-modelo" class="page-scroll">
                        <a href="#">Loja Modelo</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#cadastrar">Cadastrar</a>
                    </li>
                    <li class="popup-trigger page-scroll">
                        <a href="#popup">Tenho cadastro</a>
                    </li>
                </ul>
            </div>


            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    
    <!-- Header -->

<script>
    $('#link-modelo').click(function() {
        $('.popup-modelo').fadeIn();
        $('.popup-modelo').css('overflow', 'auto');
        $('body').css('overflow', 'hidden');
    });
</script>

<div class="popup">
    <div class="cortina">
                <?= $this->Form->create(null, ['id' => 'form-login'])?>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-xs-offset-11 col-xs-1">
                            <span id="popup-btn-close" class="popup-btn-close"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
                        </div>
                    </div>
                    <div class="row">
                        <div>
                            <?= $this->Html->link('<i class="fa fa-facebook-official"></i> Login com Facebook ',
                            '/fb-login/',
                            ['class' => 'btn btn-primary', 'escape' => false, 'style' => 'margin-top: 5px; width:100%;']) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div style="width:100%; text-align:center">
                            <span class="text-center" style="font-size: 16px; font-weight: bold; color: white;">ou</span>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-0">
                            <?= $this->Form->input('email', [
                                'div'           => false,
                                'label'         => false,
                                'class'         => 'form-control validate[required, custom[email]]',
                                'placeholder'   => 'E-MAIL',
                                'style'         => 'width: 90%; margin: 10px auto;',
                            ])?>
                            <h6 id="info-cadastro-2" class="col-md-12 white forgot-new font-bold"></h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-0">
                            <?= $this->Form->input('password', [
                                'id'            => 'login-password',
                                'div'           => false,
                                'label'         => false,
                                'class'         => 'form-control validate[required]',
                                'placeholder'   => 'SENHA',
                                'style'         => 'width: 90%; margin: 10px auto;',
                            ])?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $this->Form->button('Entrar', [
                                'type'      => 'button',
                                'id'        => 'submit-login',
                                'class'     => 'btn btn-default btn-entrar',
                                'escape'    => false,
                                'style'     => 'btn btn-primary; width: 100%; margin: 10px auto;'
                            ]) ?>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-xs-12">
                            <h6 class="col-xs-12 col-sm-12 col-md-12 col-lg-12 white forgot-new" style="font-size:12px">
                                <?= $this->Html->link('Esqueci a senha',
                                    $SSlug.'/login/esqueci-a-senha',[
                                    'class' => 'white',  
                                    'style'         => 'font-size: 10px;',
                                ])?>
                            </h6>
                        </div>
                    </div>

                </div>
                <?= $this->Form->end()?>
    </div>
</div>

<div class="popup-modelo">
    <div class="cortina-modelo">
        <div class="row">
            <div class="col-xs-offset-10 col-sm-offset-11 col-xs-1">
                <span id="close-modelo" class="popup-btn-close close-modelo"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <p class="texto-modelo">Aqui você terá a experiência de como é comprar na <strong>LOG</strong> da sua academia</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <p class="subtexto-modelo">Como funciona?</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-6 portfolio-item">
                <span><img id="img_funciona" src="//logfitness.r.worldssl.net/webroot/images/icone1.png" /></span>
                <p align="center" id="txt_funciona">CADASTRE-SE NA PLATAFORMA E ESCOLHA SUA ACADEMIA.</p>
            </div>
            <div class="col-md-3 col-xs-6 portfolio-item">
                <span><img id="img_funciona" src="//logfitness.r.worldssl.net/webroot/images/icone7.png" /></span>
                <p align="center" id="txt_funciona">ACESSE A LOJA VIRTUAL DA SUA ACADEMIA, FAÇA SEU PEDIDO E PAGUE PELA PLATAFORMA.</p>
            </div>
            <div class="col-md-3 col-xs-6 portfolio-item">
                <span><img id="img_funciona" src="//logfitness.r.worldssl.net/webroot/images/icone8.png" /></span>
                <p align="center" id="txt_funciona">NO DIA DO SEU TREINO, VOCÊ RETIRA SEU PEDIDO <STRONG>NA SUA ACADEMIA</STRONG>.<br/>FRETE FREE!</p>
            </div>
            <div class="col-md-3 col-xs-6 portfolio-item">
                <span><img id="img_funciona" src="//logfitness.r.worldssl.net/webroot/images/icone4.png" /></span>
                <p align="center" id="txt_funciona">NÃO ENCONTROU SUA ACADEMIA? NÃO ENTRE EM PÂNICO, VOCÊ PODE INDICÁ-LA!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12" style="margin: auto;">
                <?= $this->Html->link('IR PARA LOJA MODELO',
                    'https://www.logfitness.com.br/lojamodelo',
                    ['class' => 'btn btn-lg btn-outline botao-acad-prof botao-modelo', 'escape' => false]) ?>
            </div>
        </div>
    </div>
</div>
    
    <script>
        $('#close-modelo').click(function() {
            $('.popup-modelo').fadeOut();
            $('.popup-modelo').css('overflow', 'hidden');
            $('body').css('overflow', 'auto');
        });
    </script>

    <style>
        .close-modelo {
            color: #5087C7;
        }
        .botao-modelo {
            color: #5087C7;
            border-color: #5087C7;
        }
        .texto-modelo {
            font-family: nexa_boldregular;
            font-size: 2.6em;
            color: #5087C7;
            line-height: 1em;
        }
        .subtexto-modelo {
            font-family: nexa_boldregular;
            font-size: 2em;
            color: #5087C7;
            margin-top: 20px;
            margin-bottom: 30px;
        }
        #header-home {
            background-color: #5087C7; 
            z-index: 2; 
            text-align: center; 
            margin-top: 70px;
        }
        #txt_funciona {
            color: #5087C7;
        }
        .pra-vce-aluno {
            color:#5087c7; 
            font-family: nexa_boldregular; 
            font-size: 5.4em; 
            letter-spacing: -3px;
        }
        .contador {
            color:#5087c7; 
            font-family: nexa_boldregular; 
            font-size: 5.4em; 
            letter-spacing: -3px;
            text-align: right;
        }
        .pos-contador{
            color:#5087c7; 
            font-family: nexa_boldregular; 
            font-size: 3.4em; 
            letter-spacing: -2px;
            margin-top: 30px;
        }
        .header-texto {
            color:#FDD403; 
            font-family: nexa_boldregular; 
            font-size: 3.6em; 
            letter-spacing: -3px;
        }
        .produtos-home {
            margin-top: 120px;
        }
        .owl-pagination {
            display: none!important;
        }
        #owl-produtos {
            margin-top: 35px;
        }
        .bg-white-produto {
            box-shadow: 0px 1px 10px 1px rgba(0,0,0,0.75);
            min-height: 200px;
            margin: 10px;
            padding: 5px;
            max-height: 290px;
        }
        .produto-grade {
            max-height: 150px;
            max-width: 110px;
            margin-top: 25%;
        }
        .description {
            font-size: 16px;
        }
        .produtos-box:first-child {
            margin-left: -5px;
        }
        .popup-modelo {
            overflow: auto;
            display: none;
            position: fixed;
            top: 0!important;
            width: 100%;
            height: 100%;
            padding: 50px 30px;
            background: rgba(0, 0, 0, .9);
            color: #333;
            font-size: 19px;
            line-height: 30px;
            z-index: 9999;
        }
        .cortina-modelo {
            margin-left: 12%;
            margin-top: 50px;
            width: 75%;
            height: auto;
            background: #f4d637;
            padding: 25px;
            border-radius: 10px;
            text-align: center;
        }
        .produtos_pc{

        }
        .produtos_m{
            display: none;
        }
        @media all and (max-width: 768px){
            .produtos_pc{
                display: none;
            }
            .produtos_m{
                display: block;
                width: 100%;
                margin-top: 40%
            }
        }
        @media all and (max-width: 450px) {
            #header-home {
                margin-top: 50px;
            }
            .bg-white-produto {
                max-height: 200px;
            }
            .texto-modelo {
                font-size: 1.5em;
            }
            .cortina-modelo {
                width: 100%;
                margin-left: 0;
            }
            .contador{
                text-align: center;
            }
            .pos-contador{
                font-size: 2.1em;
                text-align: center;
            }
            .produtos_m{
                display: block;
                width: 100%;
                margin-top: 0;
            }
        }
        @media all and (max-width: 420px){
            .portfolio-item {
                width: 100%;
            }
            .btn-outline {
                font-size: 15px;
            }
        }
        @media all and (max-width: 1024px){
            .produtos_pc{
                width: 100%;
                padding-top: 25%;
            }
        }
        @media all and (min-width: 1200px){
            .produtos_pc{
                width: 100%;
            }
        }
    </style>

    <!-- Section inicial -->
    <section id="header-home">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 portfolio-item">
                    <p align="center" class="header-texto">Encontre <span style="color: white">suplementos</span> para atingir seus objetivos na <span style="color: white">primeira plataforma</span> da nova era fitness</p>
                </div>
                <div class="col-sm-6 portfolio-item">
                    <?= $this->Html->image('logprodutos.png', ['class' => 'produtos_pc'])?>
                    <?= $this->Html->image('logprodutos_semcaixa.png', ['class' => 'produtos_m'])?>

                    <!-- <div id="owl-produtos" class="owl-carousel">

                        <?php
                        $i = 1;
                        foreach($produtos as $produto):
                            $fotos = unserialize($produto->fotos);
                            ?>
                            <div class="col-md-12 col-xs-12 text-center produtos-box">
                                <div class="bg-white-produto col-xs-12">

                                    <?= $this->Html->link('<span class="helper"></span>'.
                                        $this->Html->image(
                                            'produtos/'.$fotos[0],
                                            ['class' => 'produto-grade', 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]),
                                        //$SSlug.'/produto/'.$produto->slug,
                                        $SSlug.'#',
                                        ['escape' => false]
                                    ); ?>
                                </div>
                            </div>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <!-- Fim Section inicial -->

    <!-- Como Funciona -->
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center" style="padding-bottom: 50px;">
                    <p class="pra-vce-aluno">pra você aluno</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 portfolio-item">
                    <span><?= $this->Html->image('icone1.png', ['id' => 'img_funciona'])?></span>
                    <p align="center" id="txt_funciona">CADASTRE-SE NA PLATAFORMA E ESCOLHA SUA ACADEMIA.</p>
                </div>
                <div class="col-sm-3 portfolio-item">
                    <span><img id="img_funciona" src="//logfitness.r.worldssl.net/webroot/images/icone7.png" /></span>
                    <p align="center" id="txt_funciona">ACESSE A LOJA VIRTUAL DA SUA ACADEMIA, FAÇA SEU PEDIDO E PAGUE PELA PLATAFORMA.</p>
                </div>
                <div class="col-sm-3 portfolio-item">
                    <span><img id="img_funciona" src="//logfitness.r.worldssl.net/webroot/images/icone8.png" /></span>
                    <p align="center" id="txt_funciona">NO DIA DO SEU TREINO, VOCÊ RETIRA SEU PEDIDO <STRONG>NA SUA ACADEMIA</STRONG>.<br/>FRETE FREE!</p>
                </div>
                <div class="col-sm-3 portfolio-item">
                    <span><img id="img_funciona" src="//logfitness.r.worldssl.net/webroot/images/icone4.png" /></span>
                    <p align="center" id="txt_funciona">NÃO ENCONTROU SUA ACADEMIA? NÃO ENTRE EM PÂNICO, VOCÊ PODE INDICÁ-LA!</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Fim Como Funciona -->

    <!--O que nos torna únicos -->
    <header>
        <div class="container">
            <div class="containerdesk">
                <div class="hidden-lg-down">
                    <div class="col-lg-12 text-center" style="padding-bottom: 50px;">
                        <p style="color:#FDD403; font-family: nexa_boldregular; font-size: 5.5em; letter-spacing: -3px;">o que nos torna únicos</p>
                    </div>
                    <img class="img-responsive" src="//logfitness.r.worldssl.net/webroot/images/header_home.png" alt="Comodidade, variedade e preço + atendimento, segurança e sem frete = Logfitness e sua academia">
                </div> 
            </div>
        </div>
    </header>

    <header>
        <div class="container">
            <div class="containermobile">
                <div class="hidden-lg-down">
                    <div class="col-lg-12 text-center" style="padding-bottom: 50px;">
                        <p style="color:#FDD403; font-family: nexa_boldregular; font-size: 3.5em; letter-spacing: -2px;">o que nos torna únicos</p>
                    </div>
                    <img class="img-responsive" src="//logfitness.r.worldssl.net/webroot/images/header_home_mobile.png" alt="Comodidade, variedade e preço + atendimento, segurança e sem frete = Logfitness e sua academia">
                </div> 
            </div>
        </div>
    </header>
    <!--Fim O que nos torna únicos -->


    <!-- Cadastrar Section -->
    <section id="cadastrar">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-5 col-lg-5">
                <p class="contador"><?=$academia_ativas ?></p>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7">
                <p class="pos-contador">academias com LOG!</p>
            </div>
        </div>
        <div class="container">
                <div class="col-sm-6 col-md-7 col-lg-8 col-xs-10" style="padding-top: 42px;">
                    <div id="map_canvas" style="height: 540px"></div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4 form-wrap" style="padding-left: 0px; padding-right: 0px">
                    <div class="tabs">
                        <h3 class="signup-tab" style="margin-top: 0" id="cadastro"><a class="active" href="#signup-tab-content">Cadastrar</a></h3>
                        <h3 class="login-tab" style="margin-top: 0"><a  href="#login-tab-content">Indicar</a></h3>
                    </div>
                    <div class="tabs-content">
                        <div id="signup-tab-content" class="active">
                            <p align="center">Cadastre-se, é mais rápido que uma série de flexão!</p>
                            <p id="info-cadastro" class="text-center"></p>
                                <?= $this->Form->create(null, ['id' => 'form-cadastro', 'role' => 'form', 'default' => false, 'class' => 'form-cadastro-home signup-form' ])?>
                            <div class="form-group row">
                                <?= $this->Form->input('name', [
                                            'div'           => false,
                                            'label'         => false,
                                            'class'         => 'input-cadastro form-control validate[required] input-cadastro-width',
                                            'placeholder'   => 'NOME COMPLETO',
                                        ])?>
                                    
                                        <?= $this->Form->input('valid_name', [
                                            'id'            => 'validname',
                                            'div'           => false,
                                        ])?>

                                        <?= $this->Form->input('email', [
                                            'div'           => false,
                                            'label'         => false,
                                            'class'         => 'input-cadastro form-control validate[required, custom[email]] input-cadastro-width ck-c-email',
                                            'placeholder'   => 'E-MAIL',
                                        ])?>
                                    
                                        <?= $this->Form->input('password', [
                                            'id'            => 'passhome',
                                            'div'           => false,
                                            'label'         => false,
                                            'class'         => 'input-cadastro form-control validate[required] input-cadastro-width',
                                            'placeholder'   => 'SENHA',
                                        ])?>   

                                        <?= $this->Form->input('password_confirm', [
                                            'id'            => 'passhomeconfirm',
                                            'div'           => false,
                                            'label'         => false,
                                            'class'         => 'input-cadastro form-control validate[required] input-cadastro-width',
                                            'placeholder'   => 'CONFIRMAR SENHA',
                                            'type'          => 'password'
                                        ])?> 

                                        <?= $this->Form->input('telephone', [
                                            'div'           => false,
                                            'label'         => false,
                                            'class'         => 'input-cadastro form-control validate[required] phone-mask input-cadastro-width',
                                            'placeholder'   => 'TELEFONE',
                                        ])?>
                                    
                                        <?= $this->Form->input('uf_id', [
                                            //'value'         => isset($cliente->city->state->id) ? $cliente->city->state->id : '',
                                            'value'         => SLUG ? $academia_slug->city->state_id : '',
                                            'id'            => 'states_academias',
                                            'div'           => false,
                                            'label'         => false,
                                            'options'       => $states,
                                            'empty'         => 'Estado...',
                                            'class'         => 'validate[required] form-control input-cadastro input-cadastro-width'
                                        ]) ?>
                                    
                                        <?= $this->Form->input('city_id', [
                                            //'value'         => isset($cliente->city->id) ? $cliente->city->id : '',
                                            'value'         => SLUG ? $academia_slug->city_id : '',
                                            'id'            => 'city',
                                            'div'           => false,
                                            'options'       => SLUG ? $academia_slug_cities : [],
                                            'label'         => false,
                                            'empty'         => 'Selecione uma cidade',
                                            'class'         => 'validate[required] form-control input-cadastro input-cadastro-width cidade-academia'
                                        ]) ?>
                                    
                                        <?= $this->Form->input('academia_id', [
                                            'id'            => 'select-academia',
                                            'options'       => [],
                                            //'value'         => SLUG ? $academia_slug->id : '',
                                            'div'           => false,
                                            'empty'         => 'Selecione uma academia...',
                                            'label'         => false,
                                            'class'         => 'form-control input-cadastro validate[optional] input-cadastro-width'
                                        ])?>
                                    <div class="text-center">
                                        <?= $this->Form->button('INSCREVER-SE', [
                                            'id'            => 'btn-submit-cadastro',
                                            'type'          => 'button',
                                            'class'         => 'button btn font-bold btn-inscrever'
                                        ])?>
                                    </div>
                                    <div class="col-lg-12 col-xs-12">
                                        <p class="text-center p-input-ou">ou</p>
                                    </div>
                                    <div class="text-center">
                                        <?= $this->Html->link('<i class="fa fa-facebook-square"></i> Cadastrar com o facebook',
                                        '/fb-login/',
                                        ['class' => 'btn btn-primary', 'escape' => false,
                                         'style' => ' width:100%;']) ?>
                                    </div>
                            </div>
                        </div> 
                                <?= $this->Form->end()?>

                        <div id="login-tab-content">
                            <p align="center">Indique sua academia e aproveite todos os benefícios da LOG.</p>
                            <?= $this->Form->create(null, [
                            'id'        => 'form-indicar-academia',
                            'role'      => 'form',
                            'default'   => false,
                            'type'      => 'file'
                            ]) ?>

                            <div class="form-group row">
                                <?= $this->Form->input('name', [
                                    'label'         => false,
                                    'div'           => false,
                                    'placeholder'   => 'Nome da Academia',
                                    'class'         => 'validate[required] form-control margin-bottom-10'
                                ]) ?>
                    
                    
                                <?= $this->Form->input('telephone', [
                                    'label'         => false,
                                    'div'           => false,
                                    'placeholder'   => 'Telefone',
                                    'class'         => 'validate[required] form-control phone-mask margin-bottom-10'
                                ]) ?>
                   
                                <?= $this->Form->input('responsavel', [
                                    'label'         => false,
                                    'div'           => false,
                                    'placeholder'   => 'Responsável pela Academia',
                                    'class'         => 'validate[required] form-control margin-bottom-10'
                                ]) ?>
                    
                                <?= $this->Form->input('uf_id', [
                                    'label'         => false,
                                    'id'            => 'states-indica',
                                    'div'           => false,
                                    'options'       => $states,
                                    'empty'         => 'Selecione um estado',
                                    'placeholder'   => 'Estado',
                                    'class'         => 'validate[required] form-control margin-bottom-10'
                                ]) ?>
                    
                                <?= $this->Form->input('city_id', [
                                    'label'         => false,
                                    'id'            => 'city-indica',
                                    'div'           => false,
                                    'empty'         => 'Selecione uma cidade',
                                    'placeholder'   => 'Cidade',
                                    'class'         => 'validate[required] form-control margin-bottom-10'
                                ]) ?>
                    
                                <?= $this->Form->button('Enviar Indicação de Academia', [
                                    'id'            => 'submit-indicar-academia',
                                    'type'          => 'submit',
                                    'div'           => false,
                                    'class'         => 'button btn float-right btn-width',
                                ]) ?>                   
                            </div>
                        </div>
            
                        <?= $this->Form->end() ?>
                        <p id="message-academia-indica"></p>
                    </div>
                </div>        
        </div>  
    </section>

    <!-- About Section -->
    <section class="success" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center aluno">
                    <img src="//logfitness.r.worldssl.net/webroot/images/naosoualuno.png" alt="Não sou aluno">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <p align="center">Se você possui um centro de treinamento ou é um professor e está buscando ser mais completo para o seu aluno acesse a página que criamos exclusivamente pra você !</p>
                </div>
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <?= $this->Html->link('academia',
                    '/academia/',
                    ['class' => 'btn btn-lg btn-outline botao-acad-prof', 'escape' => false]) ?>
                    <?= $this->Html->link('professor',
                    '/professor/',
                    ['class' => 'btn btn-lg btn-outline botao-acad-prof', 'escape' => false]) ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-12">
                        <h3>Midias Sociais</h3>
                        <ul class="list-inline">
                            <li>
                                <a href="https://open.spotify.com/user/logfitness/playlist/5osNGX93BsYhhJzI94dASW" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-spotify"></i></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/logfitness.com.br" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/logfitness.com.br/" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-instagram"></i></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/LOGFITNESS" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container info-central-box form-toggle" id="form-toggle-contato">

            <?= $this->Form->create(null, [
                'id'        => 'form-central-relacionamento',
                'role'      => 'form',
                'default'   => false,
                'type'      => 'file'
            ]) ?>

            <div class="form-group row">
                <div class="col-lg-8">
                    <?= $this->Form->input('name', [
                        'id'            => 'contato-name',
                        'div'           => false,
                        'label'         => 'Nome',
                        'placeholder'   => 'Nome Completo',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-3">
                    <?= $this->Form->input('phone', [
                        'id'            => 'contato-phone',
                        'div'           => false,
                        'label'         => 'Telefone',
                        'placeholder'   => 'Telefone',
                        'class'         => 'validate[required] form-control phone-mask',
                    ]) ?>
                </div>

                <div class="col-lg-5">
                    <?= $this->Form->input('email', [
                        'id'            => 'contato-email',
                        'div'           => false,
                        'label'         => 'E-mail',
                        'placeholder'   => 'E-mail',
                        'class'         => 'validate[required,custom[email]] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-8">
                    <?= $this->Form->input('description', [
                        'id'            => 'contato-description',
                        'div'           => false,
                        'label'         => 'Assunto',
                        'placeholder'   => 'Assunto / Descrição',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-8">
                    <?= $this->Form->input('message', [
                        'id'            => 'contato-name',
                        'type'          => 'textarea',
                        'div'           => false,
                        'label'         => 'Mensagem',
                        'placeholder'   => 'Mensagem',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-lg-2">
                    <?= $this->Form->button('Enviar Contato', [
                        'id'            => 'submit-cadastrar-central-relacionamento',
                        'type'          => 'button',
                        'div'           => false,
                        'class'         => 'btn btn-default float-right',
                    ]) ?>

                </div>

                <div class="col-lg-5" id="form-toggle-message"></div>
            </div>

            <?= $this->Form->end() ?>
        </div>

        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-md-10">
                        Copyright &copy; <span>www.logfitness.com.br</span>. TODOS OS DIREITOS RESERVADOS.
                Todo o conteúdo do site, todas as fotos,imagens, logotipos, marcas, dizeres, som, software, conjunto imagem,
                layout, trade dress, aqui veiculados são de propriedade exclusiva da LogFitness. É vedada qualquer reprodução,
                total ou parcial, de qualquer elemento de identidade, sem expressa autorização. A violação de qualquer direito mencionado
                implicará na responsabilização cível e criminal nos termos da Lei.

                CACIQUE DIGITAL REPRESENTACOES LTDA - ME - CNPJ: 22.921.802/0001-77 Rua Antonio Munia, 141, Sala 01 - Jardim Nazareth - CEP 15054-160 - São José do Rio Preto - SP
                    </div>
                    <div class="col-md-2">
                        <?= $this->Form->button('Contato',
                        ['class' => 'btn-conheca a-contato contato-home',
                            'escape' => false]
                    ) ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

    <script>
        jQuery(document).ready(function($) {
    tab = $('.tabs h3 a');

    tab.on('click', function(event) {
        event.preventDefault();
        tab.removeClass('active');
        $(this).addClass('active');

        tab_content = $(this).attr('href');
        $('div[id$="tab-content"]').removeClass('active');
        $(tab_content).addClass('active');
    });
});
    </script>

    <script>
    // Popup Window
    var scrollTop = '';
    var newHeight = '70';
     
    $(window).bind('scroll', function() {
      scrollTop = $( window ).scrollTop();
      newHeight = scrollTop + 70;
    });
            
    $('.popup-trigger').click(function(e) {
      e.stopPropagation();
      $('body').css('overflow', 'hidden'); 
      if(jQuery(window).width() < 767) {
        $('nav').after( $( ".popup" ) ); 
        $('.popup').show();
      } else {
        $('.popup').removeClass('popup-mobile').css('top', newHeight).toggle();
      };
    });
            
    $('html').click(function() {
      $('.popup').hide();
    });
     
    $('#popup-btn-close').click(function(e){
      $('.popup').hide();
      $('body').css('overflow', 'auto'); 
    });
     
    $('.popup').click(function(e){
      e.stopPropagation();
    });

    $("#owl-produtos").owlCarousel({
 
      autoPlay: 2500,
 
      items             : 3,
      itemsDesktop      : [1199,3],
      itemsDesktopSmall : [979,2],
      itemsMobile       : [400,1]
 
    });
    </script>

    <!-- jQuery 
    <script src="vendor/jquery/jquery.min.js"></script> -->
    <?= $this->Html->script('jquery')?>

    <!-- Bootstrap Core JavaScript 
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>-->
    <?= $this->Html->script('bootstrap.min')?>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript 
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>-->
    <?= $this->Html->script('jqBootstrapValidation')?>
    <?= $this->Html->script('contact_me')?>

    <!-- Theme JavaScript 
    <script src="js/freelancer.min.js"></script>-->
    <?= $this->Html->script('freelancer.min')?>

</body>