<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Exception;
//use Moip;

//use PagarMe;
//use PagarMe_Transaction;


/**
 * Transportadoras Controller
 *
 * @property \App\Model\Table\ProdutosTable $Produtos
 */
class ProdutosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index(){

        $this->paginate = [
            'contain' => ['ProdutoBase', 'Status']
        ];
        $produtos = $this->paginate($this->Produtos);

        $this->set(compact('produtos'));
        $this->set('_serialize', ['produtos']);
    }

    /**
     * View method
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($slug = null){

        if($slug == null){
            $this->redirect('/');
        }

        $produto = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                    'ProdutoComentarios',
                ]
            ])
            ->where(['Produtos.status_id' => 1])
            ->andWhere(['Produtos.preco > ' => 0.1])
            ->andWhere(['Produtos.slug' => $slug])
            ->first();


        if($produto == null){
            $this->redirect('/');
        }

        $produtos = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                    'ProdutoComentarios',
                ]
            ])
            ->where(['Produtos.status_id'           => 1])
            ->andWhere(['Produtos.preco > '         => 0.1])
            ->andWhere(['Produtos.produto_base_id'  => $produto->produto_base_id])
            ->andWhere(['Produtos.id <>'            => $produto->id])
            ->all();

        $produto_objetivos = TableRegistry::get('ProdutoObjetivos')
            ->find('list')
            ->where(['produto_base_id' => $produto->produto_base_id])
            ->all()
            ->toArray();

        /*$produto_combinations = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'ProdutoBase.ProdutoCombinations',
                    'ProdutoBase',
                ]
            ])
            ->innerJoin(
                ['ProdutoCombinations' => 'produto_combinations'],
                ['ProdutoCombinations.produto_base1_id = Produtos.produto_base_id'])
            ->where(['ProdutoCombinations.produto_base1_id'     => $produto->produto_base_id])
            ->orWhere(['ProdutoCombinations.produto_base2_id'   => $produto->produto_base_id])
            ->limit(4)
            ->all();*/

        if(count($produto_objetivos) >= 1){
            $produto_combinations = $this->Produtos
                ->find('all', [
                    'contain' => [
                        'ProdutoBase.Marcas',
                        'ProdutoBase.Propriedades',
                        'ProdutoBase',
                    ]
                ])
                ->innerJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                ->where(['Produtos.status_id' => 1])
                ->where(['ProdutoObjetivos.objetivo_id IN' => $produto_objetivos])
                ->distinct(['ProdutoBase.id'])
                ->limit(4)
                ->all();
        }else{
            $produto_combinations = [];
        }

        $session    = $this->request->session();

        $session_produto = $session->read('Carrinho');

        $session_cliente = $session->read('Cliente');

        $session_produto_ids = [];
        if(!empty($session_produto)) {
            foreach ($session_produto as $key => $item) {
                $session_produto_ids[] = $key;
            }
        }

        if(!empty($session_produto_ids)) {
            $s_produtos = $this->Produtos
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->where(['Produtos.id IN' => $session_produto_ids])
                ->all();
        }else{
            $s_produtos = [];
        }


        $this->set(compact('s_produtos', 'session_produto', 'session_cliente', 'produto', 'produtos', 'produto_combinations'));
        $this->set('_serialize', ['produto']);
    }

    /**
     * PROMOÇÃO
     * @param null $slug
     */
    public function view_promo($slug = null){

        if($slug == null){
            $this->redirect('/');
        }else{
            $produto = $this->Produtos
                ->find('all', ['contain' => [
                    'ProdutoBase'
                ]])
                ->where(['Produtos.status_id'   => 1])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->andWhere(['Produtos.slug'     => $slug])
                ->first();

            if($produto){

                $produto_combinations = $this->Produtos
                    ->find('all', [
                        'contain' => [
                            'ProdutoBase.Marcas',
                            'ProdutoBase.Propriedades',
                            'ProdutoBase.ProdutoCombinations',
                            'ProdutoBase',
                        ]
                    ])
                    ->innerJoin(
                        ['ProdutoCombinations' => 'produto_combinations'],
                        ['ProdutoCombinations.produto_base1_id = Produtos.produto_base_id'])
                    ->where(['ProdutoCombinations.produto_base1_id'     => $produto->produto_base_id])
                    ->orWhere(['ProdutoCombinations.produto_base2_id'   => $produto->produto_base_id])
                    ->limit(4)
                    ->all();

                $this->set(compact('produto', 'produto_combinations'));
            }else{
                $this->redirect('/');
            }

        }

    }

    /**
     * BOTAO COMPRAR - ADICIONA ITEM AO CARRINHO
     * @param null $slug
     * @param int $quantidade
     */
    public function comprar($slug = null, $quantidade = 1){

        $produto = $this->Produtos
            ->find('all')
            ->andWhere(['Produtos.preco > ' => 0.1])
            ->where(['Produtos.slug' => $slug])
            ->first();

        if(count($produto) == 0){
            $this->redirect('/');
        }else{

            $session    = $this->request->session();

            $session->start();

            $session_produto = $session->read('Carrinho.'.$produto->id);

            if($session_produto){
                $quantidade = $session_produto + 1;
                $session->write('Carrinho.'.$produto->id, $quantidade);
            }else{
                $session->write('Carrinho.'.$produto->id, $quantidade);
            }

            $session->delete('PedidoFinalizado');
            $session->delete('PedidoFinalizadoItem');
            $session->delete('ClienteFinalizado');

            $this->redirect('/produto/'.$produto->slug);

        }

    }

    /**
     * CARRINHO
     */
    public function carrinho(){

        $session    = $this->request->session();

        $session_produto = $session->read('Carrinho');

        $session_cliente = $session->read('Cliente');

        $session_produto_ids = [];
        if(!empty($session_produto)) {
            foreach ($session_produto as $key => $item) {
                $session_produto_ids[] = $key;
            }
        }

        if(!empty($session_produto_ids)) {
            $produtos = $this->Produtos
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->where(['Produtos.id IN' => $session_produto_ids])
                ->all();
        }else{
            $produtos = [];
        }

        $this->set(compact('produtos', 'session_produto', 'session_cliente'));
    }

    /**
     * INDICAÇÃO DE PRODUTOS - ADD CARRINHO
     * @param null $pre_order_id
     */
    public function carrinho_indicacao($pre_order_id = null){

        if($pre_order_id != null){
            $session    = $this->request->session();

            $session_produto    = $session->read('Carrinho');
            $cliente            = $session->read('Cliente');

            $PreOrders = TableRegistry::get('PreOrders');

            $pre_orders = $PreOrders
                ->find('all')
                ->contain([
                    'PreOrderItems',
                ])
                ->where(['PreOrders.cliente_id' => $cliente->id])
                ->andWhere(['PreOrders.id' => $pre_order_id])
                ->first();

            $session->delete('Carrinho');

            foreach ($pre_orders->pre_order_items as $item):
                if($item->quantidade > 0):
                    $session->write('Carrinho.'.$item->produto_id, $item->quantidade);
                endif;
            endforeach;

            $session->write('PreOrder', $pre_order_id);

            $this->redirect('/mochila');
        }else{
            $this->redirect('/minha-conta/indicacao-de-produtos');
        }

        $this->render('carrinho_edit');
    }

    /**
     * EDITAR ITENS DO CARRINHO
     * @param null $id
     * @param null $qtd
     */
    public function carrinho_edit($id = null, $qtd = null){

        if($id != null && $qtd != null){
            $session    = $this->request->session();

            $session_produto = $session->read('Carrinho');
            if(isset($session_produto[$id])){
                if($qtd <= 0){
                    $session->delete('Carrinho.'.$id);
                }else{
                    $session->write('Carrinho.'.$id, $qtd);

                    $session->delete('PedidoFinalizado');
                    $session->delete('PedidoFinalizadoItem');
                    $session->delete('ClienteFinalizado');
                }
            }
        }
    }

    /**
     * CARRINHO - PASSO 1 - VERIFICAR SE CLIENTE ESTÁ LOGADO
     * CARRINHO - PASSO 2 - VERIFICAR SE OS DADOS ESTÃO COMPLETOS
     * ROTA::/mochila/fechar-o-ziper
     */
    public function carrinho_identifica($retormar_pedido = false, $pedido_id = null){
        $session    = $this->request->session();

        $session_cliente = $session->read('Cliente');
        $PreOrders = TableRegistry::get('PreOrders');
        $session->read('PreOrder') ? $pre_order_id = (int)$session->read('PreOrder') : $pre_order_id = 0;

        $config = $session->read('Configurations');
        isset($config['transferir_taxa_parcelamento'])
            ? $taxa_parcelamento = (boolean)$config['transferir_taxa_parcelamento']
            : $taxa_parcelamento = true;

        /**
         * CLIENTE NÃO LOGADO
         */
        if(empty($session_cliente)){
            $this->redirect('/login/mochila__fechar-o-ziper');
        }
        /**
         * CLIENTE LOGADO NO SISTEMA
         */
        else{

            $Pedidos = TableRegistry::get('Pedidos');

            $cliente = TableRegistry::get('Clientes')
                ->find('all')
                ->where(['Clientes.id' => $session_cliente->id])
                ->contain(['Cities', 'Academias', 'Academias.Cities'])
                ->first();

            /**
             * RETORMAR PEDIDO JÁ REALIZADO
             */
            if($retormar_pedido == 'retormar-pedido' && $pedido_id >= 1){
                $this->set('completar_cadastro', false);

                $PedidoCliente  = $Pedidos
                    ->find('all', ['contain' => [
                        'PedidoItens',
                        'PedidoItens.Produtos',
                        'PedidoItens.Produtos.ProdutoBase',
                        'PedidoItens.Produtos.ProdutoBase.Marcas'
                    ]
                    ])
                    ->where(['Pedidos.id' => $pedido_id])
                    ->first();

                if(count($PedidoCliente) >= 1) {
                    $token = $PedidoCliente->tid;
                    $this->set(compact('PedidoCliente', 'cliente', 'token'));
                }else{
                    $this->redirect('/mochila');
                }
            }
            /**
             * CONTINUAR COM PEDIDO NOVO
             */
            else{
                /**
                 * CLIENTE COM CADASTRO INCOMPLETO
                 */
                if(
                    !isset($session_cliente->cpf) ||
                    !isset($session_cliente->academia_id) ||
                    !isset($session_cliente->city_id)
                ){
                    $this->set('completar_cadastro', true);

                    $states     = TableRegistry::get('States')->find('list');

                    if(isset($cliente->city->state_id)) {
                        $cities = TableRegistry::get('Cities')
                            ->find('list')
                            ->where(['state_id' => $cliente->city->state_id]);
                    }else{
                        $cities = [];
                    }
                    $academias  = TableRegistry::get('Academias')
                        ->find('list');

                    $this->set(compact('states', 'cliente', 'academias', 'cities'));
                }
                /**
                 * CLIENTE COM CADASTRO OK - PROSSEGUIR COM A FINALIZAÇÃO
                 */
                else{
                    $this->set('completar_cadastro', false);
                    $session_carrinho = $session->read('Carrinho');

                    $pedido = $Pedidos->newEntity();

                    $valor = 0;

                    foreach ($session_carrinho as $ks => $item):
                        $produto_carrinho = $this->Produtos->find('all')->where(['id' => $ks])->sumOf('preco');
                        $valor += $item * $produto_carrinho;
                    endforeach;
                    debug($valor);

                    $data = [
                        'cliente_id'        => $session_cliente->id,
                        'pedido_status_id'  => 1,
                        'frete'             => 0,
                        'academia_id'       => $session_cliente->academia_id,
                        'professor_id'      => $session_cliente->professor_id,
                        'valor'             => (float)$valor,
                    ];

                    $pedido = $Pedidos->patchEntity($pedido, $data);


                    /**
                     * SE O PEDIDO AINDA NÃO FOI REGISTRADO NO SISTEMA
                     */
                    if(!$session->read('PedidoFinalizado') &&
                        !$session->read('PedidoFinalizadoItem') &&
                        !$session->read('ClienteFinalizado')){

                        if ($PEDIDO_SAVE = $Pedidos->save($pedido)){

                            $PEDIDO_SAVE_ID = ['id' => (20600000 + $PEDIDO_SAVE->id)];

                            $PEDIDO_SAVE = $Pedidos->patchEntity($PEDIDO_SAVE, $PEDIDO_SAVE_ID);
                            if($PEDIDO = $Pedidos->save($PEDIDO_SAVE)){

                                $session->write('PedidoFinalizado', $PEDIDO);

                                $PedidoItens = TableRegistry::get('PedidoItens');

                                //$total = 0;

                                if($session_carrinho && count($session_carrinho) >= 1){

                                    foreach ($session_carrinho as $key => $item){

                                        $produto = $this->Produtos
                                            ->find('all')
                                            ->where(['Produtos.id' => $key])
                                            ->first();

                                        if ($produto) {
                                            $produto->preco_promo ?
                                                $preco = $produto->preco_promo :
                                                $preco = $produto->preco;

                                            $pedido_item = $PedidoItens->newEntity();
                                            $item_data = [
                                                'pedido_id' => $PEDIDO->id,
                                                'produto_id' => $key,
                                                'quantidade' => $item,
                                                'desconto' => 0,
                                                'preco' => $preco,
                                            ];

                                            $pedido_item = $PedidoItens->patchEntity($pedido_item, $item_data);

                                            if ($PEDIDO_ITEM_SAVE = $PedidoItens->save($pedido_item)) {
                                                $session->write('PedidoFinalizadoItem.'.$PEDIDO_ITEM_SAVE->id, $PEDIDO_ITEM_SAVE);
                                                //$total += ($item_data['quantidade'] * $item_data['preco']) - $item_data['desconto'];

                                                if($pre_order_id > 0):
                                                    $pre_order = $PreOrders
                                                        ->find('all')
                                                        ->where(['PreOrders.id' => $pre_order_id])
                                                        ->first();

                                                    $pre_order_data = ['pedido_id' => $PEDIDO->id];
                                                    $pre_order = $PreOrders->patchEntity($pre_order, $pre_order_data);

                                                    if($pre_order_save = $PreOrders->save($pre_order));
                                                endif;
                                            }
                                        }
                                    }

                                    //$total = number_format($total, 2, '.', '');
                                    $total = number_format($valor, 2, '.', '');

                                    $PedidoCliente = $Pedidos
                                        ->find('all', ['contain' => [
                                            'PedidoItens',
                                            'PedidoItens.Produtos',
                                            'PedidoItens.Produtos.ProdutoBase',
                                            'PedidoItens.Produtos.ProdutoBase.Marcas'
                                        ]
                                        ])
                                        ->where(['Pedidos.id' => $PEDIDO->id])
                                        ->first();

                                    $session->write('ClienteFinalizado', $cliente);

                                    $parcelas = $this->_parcelas($total);
                                    $moip = $this->_pagamento_moip($cliente, $PedidoCliente->id, $total, $parcelas, $taxa_parcelamento);

                                    debug($moip);

                                    //RESPOSTA
                                    $token = $moip->getAnswer()->token;
                                    $payment_url = $moip->getAnswer()->payment_url;

                                    $PedidoCliente->valor       = $total;
                                    $PedidoCliente->tid         = $token;
                                    $PedidoCliente->token       = $token;
                                    $PedidoCliente->payment_url = $payment_url;
                                    $Pedidos->save($PedidoCliente);

                                    $this->set(compact('PedidoCliente', 'cliente', 'token'));
                                }else{
                                    $this->redirect('/mochila');
                                }
                            }
                        }else{
                            $this->redirect('/mochila/');
                        }
                    }else{

                        if($session->read('PedidoFinalizado.id') > 1 && $session->read('ClienteFinalizado')) {

                            debug($session->read('PedidoFinalizado.id'));

                            $PedidoCliente = $Pedidos
                                ->find('all', ['contain' => [
                                    'PedidoItens',
                                    'PedidoItens.Produtos',
                                    'PedidoItens.Produtos.ProdutoBase',
                                    'PedidoItens.Produtos.ProdutoBase.Marcas'
                                ]
                                ])
                                ->where(['Pedidos.id' => $session->read('PedidoFinalizado.id')])
                                ->first();

                            //$cliente = $session->read('ClienteFinalizado');
                            $token = $PedidoCliente->tid;

                            $parcelas = $this->_parcelas($PedidoCliente->valor);
                            $moip = $this->_pagamento_moip($cliente, $PedidoCliente->id, $PedidoCliente->valor, $parcelas, $taxa_parcelamento);
                            debug($moip);

                            $this->set(compact('PedidoCliente', 'cliente', 'token'));

                        }elseif ($PEDIDO_SAVE = $Pedidos->save($pedido)){

                            $PEDIDO_SAVE_ID = ['id' => (20600000 + $PEDIDO_SAVE->id)];

                            $PEDIDO_SAVE = $Pedidos->patchEntity($PEDIDO_SAVE, $PEDIDO_SAVE_ID);
                            if($PEDIDO = $Pedidos->save($PEDIDO_SAVE)){



                                $session->write('PedidoFinalizado', $PEDIDO);

                                $PedidoItens = TableRegistry::get('PedidoItens');
                                $session_carrinho = $session->read('Carrinho');

                                //$total = 0;

                                if($session_carrinho) {


                                    foreach ($session_carrinho as $key => $item){
                                        //$produto = $this->Produtos->get($key);
                                        $produto = $this->Produtos
                                            ->find('all')
                                            ->where(['Produtos.id' => $key])
                                            ->first();

                                        if ($produto) {
                                            $produto->preco_promo ?
                                                $preco = $produto->preco_promo :
                                                $preco = $produto->preco;

                                            $pedido_item = $PedidoItens->newEntity();
                                            $item_data = [
                                                'pedido_id' => $PEDIDO->id,
                                                'produto_id' => $key,
                                                'quantidade' => $item,
                                                'desconto' => 0,
                                                'preco' => $preco,
                                            ];

                                            $pedido_item = $PedidoItens->patchEntity($pedido_item, $item_data);

                                            if ($PEDIDO_ITEM_SAVE = $PedidoItens->save($pedido_item)) {
                                                $session->write('PedidoFinalizadoItem.'.$PEDIDO_ITEM_SAVE->id, $PEDIDO_ITEM_SAVE);
                                                //$total += ($item_data['quantidade'] * $item_data['preco']) - $item_data['desconto'];

                                                if($pre_order_id > 0):
                                                    $pre_order = $PreOrders
                                                        ->find('all')
                                                        ->where(['PreOrders.id' => $pre_order_id])
                                                        ->first();

                                                    $pre_order_data = ['pedido_id' => $PEDIDO->id];
                                                    $pre_order = $PreOrders->patchEntity($pre_order, $pre_order_data);

                                                    if($pre_order_save = $PreOrders->save($pre_order));
                                                endif;
                                            }
                                        }
                                    }

                                    //$total = number_format($total, 2, '.', '');
                                    $total = number_format($valor, 2, '.', '');

                                    $PedidoCliente = $Pedidos
                                        ->find('all', ['contain' => [
                                            'PedidoItens',
                                            'PedidoItens.Produtos',
                                            'PedidoItens.Produtos.ProdutoBase',
                                            'PedidoItens.Produtos.ProdutoBase.Marcas'
                                        ]
                                        ])
                                        ->where(['Pedidos.id' => $PEDIDO->id])
                                        ->first();

                                    /*$cliente = TableRegistry::get('Clientes')
                                        ->find('all')
                                        ->where(['Clientes.id' => $session_cliente->id])
                                        ->contain(['Cities', 'Academias', 'Academias.Cities'])
                                        ->first();*/

                                    $session->write('ClienteFinalizado', $cliente);

                                    $parcelas = $this->_parcelas($total);
                                    $moip = $this->_pagamento_moip($cliente, $PEDIDO->id, $total, $parcelas, $taxa_parcelamento);
                                    debug($moip);

                                    //RESPOSTA
                                    $token = $moip->getAnswer()->token;
                                    $payment_url = $moip->getAnswer()->payment_url;

                                    $PedidoCliente->valor       = $total;
                                    $PedidoCliente->tid         = $token;
                                    $PedidoCliente->token       = $token;
                                    $PedidoCliente->payment_url = $payment_url;
                                    $Pedidos->save($PedidoCliente);

                                    $this->set(compact('PedidoCliente', 'cliente', 'token'));
                                }else{

                                    $Pedidos->delete($PEDIDO_SAVE);
                                    $Pedidos->delete($PEDIDO);

                                    $session->delete('PedidoFinalizado');
                                    $session->delete('PedidoFinalizadoItem');
                                    $session->delete('ClienteFinalizado');

                                    $this->redirect('/mochila');

                                }
                            }
                        }


                    }

                    /**
                     * ESVAZIAR O CARRINHO - CLIENTE COM CADASTRO COMPLETO
                     */
                    $session->delete('Carrinho');
                }

                debug($moip);

                $this->set('professores_list', TableRegistry::get('Professores')
                    ->find('list')
                    ->innerJoin(
                        ['ProfessorAcademias' => 'professor_academias'],
                        ['ProfessorAcademias.professor_id = Professores.id'])
                    ->where(['Professores.status_id' => 1])
                    ->andWhere(['ProfessorAcademias.academia_id' => $cliente->academia_id])
                    ->all()
                    ->toArray()
                );


            }

            $this->set('professores_list', TableRegistry::get('Professores')
                ->find('list')
                ->innerJoin(
                    ['ProfessorAcademias' => 'professor_academias'],
                    ['ProfessorAcademias.professor_id = Professores.id'])
                ->where(['Professores.status_id' => 1])
                ->andWhere(['ProfessorAcademias.academia_id' => $cliente->academia_id])
                ->all()
                ->toArray()
            );



            $this->set(compact('parcelas'));
        }
    }

    /**
     * DEFINIR PROFESSOR ESCOLHIDO NA TELA DE FINALIZAÇÃO DO PEDIDO
     * @param null $pedido_id
     * @param null $professor_id
     */
    public function pedido_set_professor($pedido_id = null, $professor_id = null){

        //if($pedido_id != null && $professor_id != null){
        if($pedido_id != null){

            $Pedidos = TableRegistry::get('Pedidos');

            //$pedido = $Pedidos->get($pedido_id);
            $pedido = $Pedidos
                ->find('all')
                ->where(['Pedidos.id' => $pedido_id])
                ->first();

            $pedido->professor_id = $professor_id;

            if($Pedidos->save($pedido)){
                echo true;
            }
        }
        echo false;

        $this->viewBuilder()->layout('ajax');
        $this->render('comprar');
    }

    /**
     * GERAR BOLETO VIA AJAX NA TELA DE FINALIZAÇÃO DO PEDIDO
     */
    public function carrinho_get_boleto(){

        if($this->request->is(['post', 'put', 'patch'])){

            // create a new cURL resource
            $ch = curl_init();

            // set URL and other appropriate options
            /*curl_setopt($ch, CURLOPT_URL, $url_transacao);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);*/

            $credentials = MOIP_TOKEN.':'.MOIP_KEY;

            $header[] = "Expect:";
            $header[] = "Authorization: Basic " . base64_encode($credentials);

            $ch = curl_init();
            $options = array(CURLOPT_URL => $this->request->data['url'],
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POST => false,
                //CURLOPT_POSTFIELDS => $xml,
                CURLOPT_RETURNTRANSFER => true,
                CURLINFO_HEADER_OUT => true
            );

            curl_setopt_array($ch, $options);

            // grab URL and pass it to the browser
            $detalhes = curl_exec($ch);
            $ret = curl_exec($ch);
            $err = curl_error($ch);
            $info = curl_getinfo($ch);

            // close cURL resource, and free up system resources
            curl_close($ch);
        }

        $this->viewBuilder()->layout('ajax');
    }

    /**
     * CARRINHO - CHECKOUT
     * ROTA::/mochila/checkout
     */
    public function carrinho_checkout($pedido_id = null, $cliente_id = null, $token = null){

        if($pedido_id == null || $cliente_id == null || $token == null) {
            $this->redirect('/mochila');
        }else{

            $Pedidos = TableRegistry::get('Pedidos');
            //$pedido = $Pedidos->get($pedido_id);
            $pedido = $Pedidos
                ->find('all')
                ->where(['Pedidos.id' => $pedido_id])
                ->first();

            $url_transacao = MOIP_AMBIENTE.'ws/alpha/ConsultarInstrucao/'.$token;

            $credentials = MOIP_TOKEN.':'.MOIP_KEY;

            $header[] = "Expect:";
            $header[] = "Authorization: Basic " . base64_encode($credentials);

            $ch = curl_init();
            $options = array(CURLOPT_URL => $url_transacao,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POST => false,
                //CURLOPT_POSTFIELDS => $xml,
                CURLOPT_RETURNTRANSFER => true,
                CURLINFO_HEADER_OUT => true
            );
            curl_setopt_array($ch, $options);

            // grab URL and pass it to the browser
            $detalhes = curl_exec($ch);
            $ret = curl_exec($ch);
            $err = curl_error($ch);
            $info = curl_getinfo($ch);

            // close cURL resource, and free up system resources
            curl_close($ch);

            @header('Content-Type: text/html; charset=utf-8');
            $xml = \simplexml_load_string($detalhes);
            $xml = json_encode($xml);
            $xml = json_decode($xml, true);

            if(isset($xml['RespostaConsultar']['Autorizacao']['Pagamento']['Status'])){
                $status_pedido = $xml['RespostaConsultar']['Autorizacao']['Pagamento']['Status'];
                if($status_pedido == 'Iniciado' || $status_pedido == 'BoletoImpresso'){
                    $status_pedido = 2;
                }elseif($status_pedido == 'EmAnalise'){
                    $status_pedido = 3;
                }elseif($status_pedido == 'Autorizado'){
                    $status_pedido = 4;
                }elseif($status_pedido == 'Cancelado'){
                    $status_pedido = 8;
                }else{
                    $status_pedido = 1;
                }
            }else{
                $status_pedido = 2;
            }

            $data = [
                'pedido_status_id'  => $status_pedido,
                'payment_method'    => $xml['RespostaConsultar']['Autorizacao']['Pagamento']['FormaPagamento'],
                'transaction_data'  => serialize($detalhes),
            ];

            $pedido = $Pedidos->patchEntity($pedido, $data);

            $session = $this->request->session();

            if($PedidoSave = $Pedidos->save($pedido)){

                $session->delete('PedidoFinalizado');
                $session->delete('PedidoFinalizadoItem');
                $session->delete('ClienteFinalizado');

                $this->redirect('/mochila/finalizado/'.$pedido_id);
            }else{
                $this->redirect('/mochila/fechar-o-ziper');
            }

            $this->set(compact('token'));
        }
    }

    /**
     * CARRINHO - FINALIZADO
     * @param null $pedido_id
     */
    public function carrinho_finalizado($pedido_id = null){
        $Pedidos = TableRegistry::get('Pedidos');

        //$pedido = $Pedidos->get($pedido_id);
        $pedido = $Pedidos
            ->find('all')
            ->where(['Pedidos.id' => $pedido_id])
            ->first();

        $this->set(compact('pedido'));

    }

    public function search(){

        if($this->request->is(['post', 'put'])){

            $query = $this->request->data['query'];

            $produtos = $this->Produtos
                ->find('all', ['contain' => [
                    'ProdutoBase'
                ]])
                //->andWhere(['Produtos.preco > ' => 0.1])
                //->where(['Produtos.status_id' => 1])
                /*->andWhere(['Produtos.visivel' => 1]/*,
                    [
                        'OR' =>
                            [
                                ['ProdutoBase.name LIKE' => '%'.$query.'%'],
                                ['ProdutoBase.embalagem_conteudo LIKE' => '%'.$query.'%'],
                                //['Produtos.cod like' => '%'.$query.'%'],
                               // ['Produtos.propriedade like' => '%'.$query.'%'],
                            ]
                    ])*/
                ->orWhere([
                    ['ProdutoBase.name LIKE' => '%'.$query.'%'],
                    //['ProdutoBase.embalagem_conteudo LIKE' => '%'.$query.'%'],
                    //['Produtos.cod like' => '%'.$query.'%'],
                    //['Produtos.propriedade like' => '%'.$query.'%'],
                ])
                ->order(['Produtos.estoque' => 'desc', 'Produtos.preco' => 'asc', 'Produtos.propriedade' => 'asc'])
                ->all();

        }else{
            $produtos = [];
        }
        $this->set(compact('produtos'));


        $this->viewBuilder()->layout('ajax');
    }

    public function filter_objetivo_categoria($objetivo_id = null, $categoria_id = null){

        if($objetivo_id == null || $categoria_id == null){
            $produtos = [];
        }else{

            $produtos = $this->Produtos
                ->find('all', ['contain' => [
                    'ProdutoBase'
                ]])
                ->innerJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                ->innerJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                ->where(['Produtos.status_id'       => 1])
                ->andWhere(['Produtos.preco > '     => 0.1])
                ->andWhere(['Produtos.visivel'      => 1])
                ->andWhere(['ProdutoCategorias.categoria_id' => $categoria_id])
                ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo_id])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('ProdutoBase.id')
                ->order(['Produtos.preco' => 'asc', 'Produtos.propriedade' => 'asc'])
                ->all();
        }

        $this->set(compact('produtos'));

        $this->viewBuilder()->layout('ajax');
    }

    public function filter_objetivo_categoria_ordenar($objetivo_id = null, $categoria_id = null, $ordenar_id = null, $valor_minimo = null, $valor_maximo = null){

        if($objetivo_id == null || $categoria_id == null){
            $produtos = [];
        }else{

            if($ordenar_id == 1) {
                $ordenar_id = 'desc';
            } else if($ordenar_id == 2) {
                $ordenar_id = 'asc';
            } else {
                $ordenar_id = 'rand()';
            }

            /*if($ordenar_id != 2) {
                $ordenar_id = 'desc';
            } else {
                $ordenar_id = 'asc';
            }*/

            if($ordenar_id == 'rand()') {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoCategorias.categoria_id' => $categoria_id])
                    ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo_id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('ProdutoBase.id')
                    ->order($ordenar_id)
                    ->all();
            } else {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoCategorias.categoria_id' => $categoria_id])
                    ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo_id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('ProdutoBase.id')
                    ->order(['Produtos.preco' => $ordenar_id, 'Produtos.propriedade' => 'asc'])
                    ->all();
            }
        }

        $this->set(compact('produtos'));

        $this->viewBuilder()->layout('ajax');
    }

    public function filter_valor($ordenar_id = null, $valor_minimo = null, $valor_maximo = null){

        echo 'blablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablabla
        blablablablablablablablablablablablablablablablablablablablablablablablablablablabla';

        if($ordenar_id == 1) {
            $ordenar_id = 'desc';
        } else if($ordenar_id == 2) {
            $ordenar_id = 'asc';
        } else {
            $ordenar_id = 'rand()';
        }

        if($valor_minimo == null || $valor_maximo == null){
            $produtos = [];
        }else{

            if($ordenar_id == 'rand()') {
                $produtos = $this->Produtos
                ->find('all', ['contain' => [
                    'ProdutoBase'
                ]])
                ->innerJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                ->innerJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                ->where(['Produtos.status_id'       => 1])
                ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                ->andWhere(['Produtos.visivel'      => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('ProdutoBase.id')
                ->order($ordenar_id)
                ->all();
            } else {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('ProdutoBase.id')
                    ->order(['Produtos.preco' => $ordenar_id, 'Produtos.propriedade' => 'asc'])
                    ->all();
            }
        }

        $this->set(compact('produtos'));

        $this->viewBuilder()->layout('ajax');
    }

    public function filter_objetivo($objetivo_id = null){

        if($objetivo_id == null){
            $produtos = [];
        }else{

            $produtos = $this->Produtos
                ->find('all', ['contain' => [
                    'ProdutoBase'
                ]])
                ->innerJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                ->innerJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                ->where(['Produtos.status_id'       => 1])
                ->andWhere(['Produtos.preco > '     => 0.1])
                ->andWhere(['Produtos.visivel'      => 1])
                ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo_id])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('ProdutoBase.id')
                ->order(['Produtos.preco' => 'asc', 'Produtos.propriedade' => 'asc'])
                ->all();
        }

        $this->set(compact('produtos'));

        $this->viewBuilder()->layout('ajax');
    }

    public function filter_ordenar($ordenar_id = null, $valor_minimo = null, $valor_maximo = null){

        if($ordenar_id == 1) {
            $ordenar_id = 'desc';
        } else if($ordenar_id == 2) {
            $ordenar_id = 'asc';
        } else {
            $ordenar_id = 'rand()';
        }

        /*if($ordenar_id != 2) {
            $ordenar_id = 'desc';
        } else {
            $ordenar_id = 'asc';
        }*/

        if($ordenar_id == null){
            $produtos = [];
        }else{

            if($ordenar_id == 'rand()') {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('ProdutoBase.id')
                    ->order($ordenar_id)
                    ->all();
            } else {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('ProdutoBase.id')
                    ->order(['Produtos.preco' => $ordenar_id, 'Produtos.propriedade' => 'asc'])
                    ->all();
            }
        }

        $this->set(compact('produtos'));

        $this->viewBuilder()->layout('ajax');
    }

    public function filter_marca($marca_id = null, $ordenar_id = null, $valor_minimo = null, $valor_maximo = null){

        if($ordenar_id != null) {
            if($ordenar_id == 1) {
                $ordenar_id = 'desc';
            } else if($ordenar_id == 2) {
                $ordenar_id = 'asc';
            } else {
                $ordenar_id = 'rand()';
            }
        }

        if($marca_id == null){
            $produtos = [];
        } if($marca_id != null && $ordenar_id == null && $valor_minimo == null && $valor_maximo == null) {
            $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco > '     => 0.1])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.marca_id'  => $marca_id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('ProdutoBase.id')
                    ->order('rand()')
                    ->all();
        } else{
            if($ordenar_id == 'rand()') {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.marca_id'  => $marca_id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('ProdutoBase.id')
                    ->order($ordenar_id)
                    ->all();
            } else {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.marca_id'  => $marca_id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('ProdutoBase.id')
                    ->order(['Produtos.preco' => $ordenar_id, 'Produtos.propriedade' => 'asc'])
                    ->all();
            }
        }

        $this->set(compact('produtos'));

        $this->viewBuilder()->layout('ajax');
    }

    public function search_categoria($categoria = null, $objetivo = null){

        $produtos = $this->Produtos
            ->find('all', ['contain' => [
                'ProdutoBase'
            ]])
            ->where(['Produtos.status_id' => 1])
            ->andWhere(['Produtos.visivel' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])

            ->order(['Produtos.estoque' => 'desc', 'Produtos.preco' => 'asc', 'Produtos.propriedade' => 'asc'])
            ->all();

        $this->set(compact('produtos'));

        $this->viewBuilder()->layout('ajax');
    }

    /**
     * MÉTODO DE PAGAMENTO - MOIP
     * @param null $pedido_id
     * @param float $total
     * @param $cliente
     * @return \Moip
     */
    private function _pagamento_moip($cliente, $pedido_id = null, $total = 0.00, $parcelas = 1, $transferir_taxa = true){

        //REQUISIÇÃO - ENVIO (REQUEST)
        include_once VENDOR . 'moiplabs' . DS . 'moip-php' . DS . 'lib' . DS . 'Moip.php';
        include_once VENDOR . 'moiplabs' . DS . 'moip-php' . DS . 'lib' . DS . 'MoipClient.php';
        include_once VENDOR . 'moiplabs' . DS . 'moip-php' . DS . 'lib' . DS . 'MoipStatus.php';

        $moip = new \Moip();
        $moip->setEnvironment();
        $moip->setCredential(array(
            'key' => MOIP_KEY,
            'token' => MOIP_TOKEN
        ));

        $moip->setUniqueID(date('Ymd').'_'.$pedido_id);
        $moip->setValue($total);
        $moip->setReason('LogFitness - Pedido #'.$pedido_id);

        $moip->setPayer(array(
            'name'              => $cliente->name,
            'email'             => $cliente->email,
            'payerId'           => $cliente->id,
            'billingAddress'    => array(
                'address'       => $cliente->address,
                'number'        => $cliente->number,
                'complement'    => $cliente->complement,
                'neighborhood'  => $cliente->area,
                'city'          => $cliente->city->name,
                'state'         => $cliente->city->uf,
                'country'       => 'BRA',
                'zipCode'       => str_replace('.', '', $cliente->cep),
                'phone'         => $cliente->telephone,
            ),
        ));

        if($parcelas > 1) {
            $moip->addParcel(1, $parcelas, null, $transferir_taxa);
        }

        $moip->validate('Identification');

        $moip->send();

        return $moip;

    }

    /**
     * FAIXA DE PARCELAMENTO - QUANTIDADE MÁXIMA DE PARCELAS
     * @param float $pedido_valor
     * @return int
     */
    private function _parcelas($pedido_valor = 0.00){
        /**
         * DEFINIR QUANTIDADE DE PARCELAS DE ACORDO COM O VALOR DA COMPRA
         */
        $session = $this->request->session();
        $config = $session->read('Configurations');

        $parcelas = 1;
        //if(isset($config['parcelas']) && isset($PedidoCliente)){
        if(isset($config['parcelas']) && $pedido_valor > 0){
            foreach ($config['parcelas'] as $p => $parcela):
                if(
                    $pedido_valor >= $config['parcelamento_inicial'][$p] &&
                    $pedido_valor <= $config['parcelamento_final'][$p]
                ):
                    $parcelas = $parcela;
                endif;
            endforeach;
        }

        return $parcelas;
        //$this->set(compact('parcelas'));
    }

    public function erp_uploadImages( $id = null){
        if ( $id != null ) {
            $produto = $this->Produtos->get($id, [
                'contain' => []
            ]);

            var_dump($produto);
            die();

            if ($this->request->is(['post', 'put'])) {

                $fotos = [];
                $this->request->data['status_id'] = 1;
                $produto_base = $this->Produtos->ProdutoBase->get($produto->produto_base_id);

                $dir = WWW_ROOT.'img' . DS . 'temp';
                $files = scandir($dir);
                foreach ($files as $value){
                    if (($value !== '.') && ($value !== '..')) {
                        $image = file_get_contents($dir . DS . $value);
                        $name = str_replace(' ', '-', strtolower(trim($produto_base->slug))) . '-' . $id;
                        $name = $this->sanitizeString($name);
                        $arquivo  = $this->wiUpload($image,         $name, 'produtos', null, 800, 800);
                        $arquivo1 = $this->wiUpload($image, 'md-' . $name, 'produtos', null, 400, 400);
                        $arquivo2 = $this->wiUpload($image, 'sm-' . $name, 'produtos', null, 170, 170);
                        $arquivo3 = $this->wiUpload($image, 'xs-' . $name, 'produtos', null, 60, 60);
                        $fotos[] = $arquivo;
                    }
                }
                $produto_base->fotos = serialize($fotos);
                $this->Produtos->ProdutoBase->save($produto_base);
            }
            $this->response->statusCode(200);
        }
    }




}
