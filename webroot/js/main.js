/**
 * BUSCA PRODUTOS
 * @type {*|jQuery|HTMLElement}
 */
var search_button = $('.search-button');
var search_form = $('.search-form');
var grade_produtos = $('#grade-produtos');
var loading = $('.loading');

$(document).ready(function() {

    var referer = document.referrer;
    var redirect_action = $('#redirect');

    function go_to(id, delay, timeout) {

        $("html, body").delay(300).animate({ scrollTop: $(id).offset().top }, 1000);
    }

    $('#btn-arrow').click(function() {
        go_to('#bloco-2');
    });

    /**
     * VERIFICAR SE ELEMENTO ESTA VISIVEL AO ROLAR A PAGINA
     * @param elem
     * @param pixels
     * @returns {boolean}
     */
    function isScrolledIntoView(elem, pixels) {
        var $elem = $(elem);
        var $window = $(window);

        var docViewTop = $window.scrollTop();
        var docViewBottom = docViewTop + $window.height();

        var elemTop = $elem.offset().top + pixels;
        var elemBottom = elemTop + $elem.height();

        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    /**
     * MENUS
     * MARCAS E OBJETIVOS
     * @type {*|jQuery|HTMLElement}
     */
    var nav_marcas = $('#nav-marcas');
    var nav_objetivos = $('.nav-objetivos');
    var div_marcas = $('#div-marcas');
    var div_objetivos = $('.div-objetivo');

    $('#valid-name').parent().parent().hide();
    $('#validname').parent().hide();

    function apenasNumeros(string) {
        var numsStr = string.replace(/[^0-9]/g, '');
        return numsStr;
    }

    function ValidaCPF(cpf) {
        var Soma;
        var Resto;
        var cpf = apenasNumeros(cpf);
        Soma = 0;
        if (cpf == "00000000000") return false;

        for (i = 1; i <= 9; i++) Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11)) Resto = 0;
        if (Resto != parseInt(cpf.substring(9, 10))) return false;

        Soma = 0;
        for (i = 1; i <= 10; i++) Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11)) Resto = 0;
        if (Resto != parseInt(cpf.substring(10, 11))) return false;
        return true;
    }

    $(document).on('blur', '.cpf-check-padrao', function() {
        var cpf = $(this).val();

        $('.alerta-cpf').fadeOut();

        loading.show();

        if (ValidaCPF(cpf)) {
            setTimeout(function() {
                $('.alerta-cpf.valido').fadeIn();
            }, 400);
            loading.hide();
        } else {
            setTimeout(function() {
                $('.alerta-cpf.invalido').fadeIn();
            }, 400);
            $(this).focus();
            loading.hide();
        }
    });

    /*
     * Carregar mais produtos tela inicial
     */
    $('#new-produtos-load-more').click(function() {
        loading.show(1);

        if ($('.marca_selected').length != 0) {
            marca_id = $('.marca_selected').attr('data-id');
        } else {
            marca_id = "0";
        }

        if ($('.categoria_selected').length != 0) {
            categoria_id = $('.categoria_selected').attr('data-id');
            objetivo_id = $('.categoria_selected').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar').val();

        $.ajax({
                type: "POST",
                url: WEBROOT_URL + 'produtos/atualizar-produtos/',
                data: {
                    query: $('.search-query').val(),
                    objetivo: objetivo_id,
                    categoria: categoria_id,
                    marca: marca_id,
                    ordenar: ordenar_id,
                    valor_min: valor_min_id,
                    valor_max: valor_max_id
                }
            })
            .done(function(data) {
                $('#grade-produtos').append(data);
                loading.hide(1);
            });
    });

    /*
     * Carregar mais produtos tela inicial
     */
    $('#new-produtos-load-more-express').click(function() {
        loading.show(1);

        if ($('.marca_selected').length != 0) {
            marca_id = $('.marca_selected').attr('data-id');
        } else {
            marca_id = "0";
        }

        if ($('.categoria_selected').length != 0) {
            categoria_id = $('.categoria_selected').attr('data-id');
            objetivo_id = $('.categoria_selected').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar').val();

        $.ajax({
                type: "POST",
                url: WEBROOT_URL + 'produtos/atualizar-produtos-express/',
                data: {
                    query: $('.search-query').val(),
                    objetivo: objetivo_id,
                    categoria: categoria_id,
                    marca: marca_id,
                    ordenar: ordenar_id,
                    valor_min: valor_min_id,
                    valor_max: valor_max_id
                }
            })
            .done(function(data) {
                $('#grade-produtos').append(data);
                loading.hide(1);
            });
    });

    /*
     * Carregar mais produtos tela inicial
     */
    $('#new-produtos-load-more-indicacao').click(function() {
        loading.show(1);

        if ($('.marca_selected').length != 0) {
            marca_id = $('.marca_selected').attr('data-id');
        } else {
            marca_id = "0";
        }

        if ($('.categoria_selected').length != 0) {
            categoria_id = $('.categoria_selected').attr('data-id');
            objetivo_id = $('.categoria_selected').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar-prof').val();

        $.ajax({
                type: "POST",
                url: WEBROOT_URL + 'produtos/atualizar-produtos-indicacao/',
                data: {
                    query: $('.search-query').val(),
                    objetivo: objetivo_id,
                    categoria: categoria_id,
                    marca: marca_id,
                    ordenar: ordenar_id,
                    valor_min: valor_min_id,
                    valor_max: valor_max_id
                }
            })
            .done(function(data) {
                $('#grade-produtos').append(data);
                loading.hide(1);
            });
    });

    var ctrl_ordenar = 0;
    var objetivo_id = 0;
    var categoria_id = 0;
    var ordenar_id = 0;
    var marca_id = 0;
    var valor_id = $('#value-slider').val();
    var valor_min_id = 0;
    var valor_max_id = 0;
    var cupom_aplicado = 0;

    $('#nav-marcas').click(function() {
        $('#div-marcas').slideToggle();
        $('.div-objetivos').slideUp();
    });

    $('.nav-objetivos').click(function() {
        var data_objetivo = '#div-objetivo-' + $(this).attr('data-objetivo');
        $('#div-marcas').slideUp();
        $('.div-objetivos').not(data_objetivo).slideUp();
        $(data_objetivo).slideToggle();
    });

    /**
     * FILTRO PROFESSOR INDICAÇÃO
     */

    /**
     * CLICK VALOR
     */
    $('.valor-box-prof').click(function() {
        loading.show(1);
        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar-prof').val();

        if (medida_tela <= 768) {
            $('.close-filter').click();
        }

        if ($('.marca_selected').length != 0) {
            marca_id = $('.marca_selected').attr('data-id');
        } else {
            marca_id = "0";
        }

        if ($('.categoria_selected').length != 0) {
            categoria_id = $('.categoria_selected').attr('data-id');
            objetivo_id = $('.categoria_selected').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        get_produtos_prof(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'valor');
    });

    /**
     * CLICK ORDENAR
     */

    $('#filtro-ordenar-prof').change(function() {
        loading.show(1);
        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar-prof').val();

        if (medida_tela <= 768) {
            $('.close-filter').click();
        }

        if ($('.marca_selected').length != 0) {
            marca_id = $('.marca_selected').attr('data-id');
        } else {
            marca_id = "0";
        }

        if ($('.categoria_selected').length != 0) {
            categoria_id = $('.categoria_selected').attr('data-id');
            objetivo_id = $('.categoria_selected').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        get_produtos_prof(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'ordenar');
    });

    /**
     * CLICK OBJETIVOS / CATEGORIA / VALOR
     */
    $('.link-produto-categoria-prof').click(function() {
        loading.show(1);

        objetivo_id = $(this).attr('data-oid');
        categoria_id = $(this).attr('data-cid');
        marca_id = 0;

        if (medida_tela <= 768) {
            $('.close-filter').click();
        }

        if (grade_produtos.attr('data-section') == 'home') {
            var valor_id = $('#value-slider').val();
            var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
            var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
            var ordenar_id = $('#filtro-ordenar-prof').val();

            get_produtos_prof(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'objcat');
        } else {
            location.href = WEBROOT_URL + 'professor-indicacao?oid=' + objetivo_id + '&cid=' + categoria_id;
        }
    });

    $('#search-button-prof').click(function() {
        $('.search-form-prof').submit();
    });

    $('.search-form-prof').submit(function() {
        loading.show(1);

        if (medida_tela <= 768) {
            $('.close-filter').click();
        }

        if ($('.marca_selected').length != 0) {
            marca_id = $('.marca_selected').attr('data-id');
        } else {
            marca_id = "0";
        }

        if ($('.categoria_selected').length != 0) {
            categoria_id = $('.categoria_selected').attr('data-id');
            objetivo_id = $('.categoria_selected').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        if (grade_produtos.attr('data-section') == 'home') {
            var valor_id = $('#value-slider').val();
            var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
            var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
            var ordenar_id = $('#filtro-ordenar-prof').val();

            get_produtos_prof(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'busca');
        } else {
            location.href = WEBROOT_URL + 'professor-indicacao?busca=' + $('.search-query').val();
        }
    });

    /**
     * CLICK MARCA MENU - FILTRO PRODUTOS
     */
    $('.link-marca-prof').click(function() {
        loading.show(1);
        marca_id = $(this).attr('data-id');
        objetivo_id = 0;
        categoria_id = 0;

        if (medida_tela <= 768) {
            $('.close-filter').click();
        }

        if (grade_produtos.attr('data-section') == 'home') {
            var valor_id = $('#value-slider').val();
            var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
            var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
            var ordenar_id = $('#filtro-ordenar-prof').val();

            get_produtos_prof(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'marca');
        } else {
            location.href = WEBROOT_URL + 'professor-indicacao?m=' + marca_id;
        }
    });

    function get_produtos_prof(objetivo, categoria, marca, ordenar, valor_min, valor_max, tipo) {
        if (tipo == 'valor') {
            $('.valor-hidden').val('valor_min=' + valor_min + '&valor_max=' + valor_max);
        } else if (tipo == 'ordenar') {
            $('.ordenar-hidden').val('&ord=' + ordenar);
        } else if (tipo == 'objcat') {
            $('.objcat-hidden').val('&oid=' + objetivo + '&cid=' + categoria);
            $('.marca-hidden').val('&m=');
        } else if (tipo == 'marca') {
            $('.marca-hidden').val('&m=' + marca);
            $('.objcat-hidden').val('&oid=&cid=');
        } else if (tipo == 'busca') {
            $('.busca-hidden').val('&busca=' + $('.search-query').val());
        }

        var string_gets = $('.valor-hidden').val() +
            $('.ordenar-hidden').val() +
            $('.objcat-hidden').val() +
            $('.marca-hidden').val() +
            $('.busca-hidden').val();

        $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/busca-professor/',
                data: { query: $('.search-query').val(), objetivo: objetivo, categoria: categoria, marca: marca, ordenar: ordenar, valor_min: valor_min, valor_max: valor_max }
            })
            .done(function(data) {
                $('html, body').animate({
                    scrollTop: grade_produtos.offset().top - 125
                }, 700);
                grade_produtos.html(data);
                loading.hide(1);

                window.history.pushState('', $('head').find('title').text(), '/professor-indicacao?' + string_gets);
            });
    }

    /**
     * FILTRO LOJA
     */

    /**
     * CLICK VALOR
     */
    $('.valor-box').click(function() {
        loading.show(1);
        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar').val();

        if (medida_tela <= 768) {
            $('.close-filter').click();
        }

        if ($('.marca_selected').length != 0) {
            marca_id = $('.marca_selected').attr('data-id');
        } else {
            marca_id = "0";
        }

        if ($('.categoria_selected').length != 0) {
            categoria_id = $('.categoria_selected').attr('data-id');
            objetivo_id = $('.categoria_selected').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        get_produtos_completo(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'valor');
    });

    /**
     * CLICK ORDENAR
     */

    $('#filtro-ordenar').change(function() {
        loading.show(1);
        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar').val();

        if (medida_tela <= 768) {
            $('.close-filter').click();
        }

        if ($('.marca_selected').length != 0) {
            marca_id = $('.marca_selected').attr('data-id');
        } else {
            marca_id = "0";
        }

        if ($('.categoria_selected').length != 0) {
            categoria_id = $('.categoria_selected').attr('data-id');
            objetivo_id = $('.categoria_selected').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        get_produtos_completo(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'ordenar');
    });

    /**
     * CLICK OBJETIVOS / CATEGORIA / VALOR
     */
    $('.link-produto-categoria').click(function() {

        var academiaslug = $('.academiaslug').val();

        loading.show(1);

        objetivo_id = $(this).attr('data-oid');
        categoria_id = $(this).attr('data-cid');
        marca_id = 0;

        if (medida_tela <= 768) {
            $('.close-filter').click();
        }

        if (grade_produtos.attr('data-section') == 'home') {
            var valor_id = $('#value-slider').val();
            var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
            var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
            var ordenar_id = $('#filtro-ordenar').val();

            get_produtos_completo(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'objcat');
        } else {
            location.href = WEBROOT_URL + academiaslug + '?&valor_min=&valor_max=&ord=&oid=' + objetivo_id + '&cid=' + categoria_id + '&m=&busca=';
        }
    });

    $('#limpar-filtro').click(function() {

        var academiaslug = $('.academiaslug').val();

        loading.show(1);
        var marca_id = 0;
        var categoria_id = 0;
        var objetivo_id = 0;
        $('#filtro-ordenar').val(0);
        $('.search-query').val('');
        $('.valor-hidden').val('valor_min=&valor_max=');
        $('.ordenar-hidden').val('&ord=');
        $('.marca-hidden').val('&m=');
        $('.objcat-hidden').val('&oid=&cid=');
        $('.busca-hidden').val('&busca=');

        var string_gets = $('.valor-hidden').val() +
            $('.ordenar-hidden').val() +
            $('.objcat-hidden').val() +
            $('.marca-hidden').val() +
            $('.busca-hidden').val();

        $.get(WEBROOT_URL + 'produtos/limpar-filtro/',
            function(data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);

                $('#grade-produtos').html(data);
                loading.hide(1);
                window.history.pushState('', $('head').find('title').text(), '/' + academiaslug + '?' + string_gets);
            });
    });

    $('.query_selected').click(function() {

        var academiaslug = $('.academiaslug').val();

        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar').val();
        $('.busca-hidden').val('&busca=');

        var string_gets = $('.valor-hidden').val() +
            $('.ordenar-hidden').val() +
            $('.objcat-hidden').val() +
            $('.marca-hidden').val() +
            $('.busca-hidden').val();

        loading.show(1);

        $('.search-query').val('');

        if ($('.marca_selected')) {
            marca_id = $('.marca_selected').attr('data-id');
        } else {
            marca_id = "0";
        }

        if ($('.categoria_selected')) {
            categoria_id = $('.categoria_selected').attr('data-id');
            objetivo_id = $('.categoria_selected').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/busca-completa/',
                data: { query: $('.search-query').val(), objetivo: objetivo_id, categoria: categoria_id, marca: marca_id, ordenar: ordenar_id, valor_min: valor_min_id, valor_max: valor_max_id }
            })
            .done(function(data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);
                $('#grade-produtos').html(data);
                loading.hide(1);
                window.history.pushState('', $('head').find('title').text(), '/' + academiaslug + '?' + string_gets);
            });
    });

    $('.marca_selected').click(function() {

        var academiaslug = $('.academiaslug').val();

        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar').val();
        $('.marca-hidden').val('&m=');

        var string_gets = $('.valor-hidden').val() +
            $('.ordenar-hidden').val() +
            $('.objcat-hidden').val() +
            $('.marca-hidden').val() +
            $('.busca-hidden').val();

        loading.show(1);

        marca_id = "0";

        if ($('.categoria_selected')) {
            categoria_id = $('.categoria_selected').attr('data-id');
            objetivo_id = $('.categoria_selected').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/busca-completa/',
                data: { query: $('.search-query').val(), objetivo: objetivo_id, categoria: categoria_id, marca: marca_id, ordenar: ordenar_id, valor_min: valor_min_id, valor_max: valor_max_id }
            })
            .done(function(data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);
                $('#grade-produtos').html(data);
                loading.hide(1);
                window.history.pushState('', $('head').find('title').text(), '/' + academiaslug + '?' + string_gets);
            });
    });

    $('.categoria_selected').click(function() {
        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar').val();
        $('.objcat-hidden').val('&oid=&cid=');
        loading.show(1);

        categoria_id = "0";
        objetivo_id = "0";

        if ($('.marca_selected')) {
            marca_id = $('.marca_selected').attr('data-id');
        } else {
            marca_id = "0";
        }

        $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/busca-completa/',
                data: { query: $('.search-query').val(), objetivo: objetivo_id, categoria: categoria_id, marca: marca_id, ordenar: ordenar_id, valor_min: valor_min_id, valor_max: valor_max_id }
            })
            .done(function(data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);
                $('#grade-produtos').html(data);
                loading.hide(1);
                $('#new-produtos-load-more').show();
            });
    });

    if ($('.filtragem a').length == 0) {
        $('.voltar-link').remove();
    }

    /**
     * CLICK MARCA MENU - FILTRO PRODUTOS
     */
    $('.link-marca').click(function() {

        var academiaslug = $('.academiaslug').val();

        loading.show(1);
        marca_id = $(this).attr('data-id');
        objetivo_id = 0;
        categoria_id = 0;

        if (medida_tela <= 768) {
            $('.close-filter').click();
        }

        if (grade_produtos.attr('data-section') == 'home') {
            var valor_id = $('#value-slider').val();
            var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
            var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
            var ordenar_id = $('#filtro-ordenar').val();

            get_produtos_completo(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'marca');
        } else {
            location.href = WEBROOT_URL + academiaslug + '?&valor_min=&valor_max=&ord=&oid=&cid=&m=' + marca_id + '&busca=';
        }
    });

    /**
     * BUSCAR - CLIQUE NO BOTAO
     */
    search_button.click(function() {
        search_form.submit();
    });

    /**
     * BUSCAR - SUBMIT FORM
     */
    search_form.submit(function() {
        loading.show(1);

        var academiaslug = $('.academiaslug').val();

        if (medida_tela <= 768) {
            $('#botao-search').click();
        }

        if ($('.marca_selected').length != 0) {
            marca_id = $('.marca_selected').attr('data-id');
        } else {
            marca_id = "0";
        }

        if ($('.categoria_selected').length != 0) {
            categoria_id = $('.categoria_selected').attr('data-id');
            objetivo_id = $('.categoria_selected').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        if (grade_produtos.attr('data-section') == 'home') {
            var valor_id = $('#value-slider').val();
            var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
            var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
            var ordenar_id = $('#filtro-ordenar').val();

            get_produtos_completo(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'busca');
        } else {
            location.href = WEBROOT_URL + academiaslug + '?&valor_min=&valor_max=&ord=&oid=&cid=&m=&busca=' + $('.search-query').val();
        }
    });


    function get_produtos_completo(objetivo, categoria, marca, ordenar, valor_min, valor_max, tipo) {

        var academiaslug = $('.academiaslug').val();

        if (tipo == 'valor') {
            $('.valor-hidden').val('valor_min=' + valor_min + '&valor_max=' + valor_max);
        } else if (tipo == 'ordenar') {
            $('.ordenar-hidden').val('&ord=' + ordenar);
        } else if (tipo == 'objcat') {
            $('.objcat-hidden').val('&oid=' + objetivo + '&cid=' + categoria);
            $('.marca-hidden').val('&m=');
        } else if (tipo == 'marca') {
            $('.marca-hidden').val('&m=' + marca);
            $('.objcat-hidden').val('&oid=&cid=');
        } else if (tipo == 'busca') {
            $('.busca-hidden').val('&busca=' + $('.search-query').val());
        }

        var string_gets = $('.valor-hidden').val() +
            $('.ordenar-hidden').val() +
            $('.objcat-hidden').val() +
            $('.marca-hidden').val() +
            $('.busca-hidden').val();

        $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/busca-completa/',
                data: { query: $('.search-query').val(), objetivo: objetivo, categoria: categoria, marca: marca, ordenar: ordenar, valor_min: valor_min, valor_max: valor_max }
            })
            .done(function(data) {
                $('html, body').animate({
                    scrollTop: grade_produtos.offset().top - 125
                }, 700);
                grade_produtos.html(data);
                loading.hide(1);

                window.history.pushState('', $('head').find('title').text(), '/' + academiaslug + '?' + string_gets);
            });
    }

    /**
     * FILTRO EXPRESS
     */

    /**
     * CLICK VALOR
     */
    $('.valor-box-express').click(function() {
        loading.show(1);
        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar-express').val();

        if (medida_tela <= 768) {
            $('.close-filter').click();
        }

        if ($('.marca_selected-express').length != 0) {
            marca_id = $('.marca_selected-express').attr('data-id');
        } else {
            marca_id = "0";
        }

        if ($('.categoria_selected-express').length != 0) {
            categoria_id = $('.categoria_selected-express').attr('data-id');
            objetivo_id = $('.categoria_selected-express').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        get_produtos_express(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'valor');
    });

    /**
     * CLICK ORDENAR
     */

    $('#filtro-ordenar-express').change(function() {
        loading.show(1);
        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar-express').val();

        if (medida_tela <= 768) {
            $('.close-filter').click();
        }

        if ($('.marca_selected-express').length != 0) {
            marca_id = $('.marca_selected-express').attr('data-id');
        } else {
            marca_id = "0";
        }

        if ($('.categoria_selected-express').length != 0) {
            categoria_id = $('.categoria_selected-express').attr('data-id');
            objetivo_id = $('.categoria_selected-express').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        get_produtos_express(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'ordenar');
    });

    /**
     * CLICK OBJETIVOS / CATEGORIA / VALOR
     */
    $('.link-produto-categoria-express').click(function() {
        loading.show(1);

        objetivo_id = $(this).attr('data-oid');
        categoria_id = $(this).attr('data-cid');
        marca_id = 0;

        if (medida_tela <= 768) {
            $('.close-filter').click();
        }

        if (grade_produtos.attr('data-section') == 'home') {
            var valor_id = $('#value-slider').val();
            var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
            var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
            var ordenar_id = $('#filtro-ordenar-express').val();

            get_produtos_express(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'objcat');
        } else {
            location.href = WEBROOT_URL + 'academia/admin/express?&valor_min=&valor_max=&ord=&oid=' + objetivo_id + '&cid=' + categoria_id + '&m=&busca=';
        }
    });

    $('#limpar-filtro-express').click(function() {
        loading.show(1);
        var marca_id = 0;
        var categoria_id = 0;
        var objetivo_id = 0;
        $('#filtro-ordenar').val(0);
        $('.search-query').val('');
        $('.valor-hidden').val('valor_min=&valor_max=');
        $('.ordenar-hidden').val('&ord=');
        $('.marca-hidden').val('&m=');
        $('.objcat-hidden').val('&oid=&cid=');
        $('.busca-hidden').val('&busca=');

        var string_gets = $('.valor-hidden').val() +
            $('.ordenar-hidden').val() +
            $('.objcat-hidden').val() +
            $('.marca-hidden').val() +
            $('.busca-hidden').val();

        $.get(WEBROOT_URL + 'produtos/limpar-filtro-express/',
            function(data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);

                $('#grade-produtos').html(data);
                loading.hide(1);

                window.history.pushState('', $('head').find('title').text(), '/academia/admin/express?' + string_gets);
            });
    });

    $('.query_selected-express').click(function() {
        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar-express').val();
        $('.busca-hidden').val('&busca=');

        var string_gets = $('.valor-hidden').val() +
            $('.ordenar-hidden').val() +
            $('.objcat-hidden').val() +
            $('.marca-hidden').val() +
            $('.busca-hidden').val();

        loading.show(1);

        $('.search-query').val('');

        if ($('.marca_selected-express')) {
            marca_id = $('.marca_selected-express').attr('data-id');
        } else {
            marca_id = "0";
        }

        if ($('.categoria_selected-express')) {
            categoria_id = $('.categoria_selected-express').attr('data-id');
            objetivo_id = $('.categoria_selected-express').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/busca-express/',
                data: { query: $('.search-query').val(), objetivo: objetivo_id, categoria: categoria_id, marca: marca_id, ordenar: ordenar_id, valor_min: valor_min_id, valor_max: valor_max_id }
            })
            .done(function(data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);
                $('#grade-produtos').html(data);
                loading.hide(1);

                window.history.pushState('', $('head').find('title').text(), '/academia/admin/express?' + string_gets);
            });
    });

    $('.marca_selected-express').click(function() {
        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar-express').val();
        $('.marca-hidden').val('&m=');

        var string_gets = $('.valor-hidden').val() +
            $('.ordenar-hidden').val() +
            $('.objcat-hidden').val() +
            $('.marca-hidden').val() +
            $('.busca-hidden').val();

        loading.show(1);

        marca_id = "0";

        if ($('.categoria_selected-express')) {
            categoria_id = $('.categoria_selected-express').attr('data-id');
            objetivo_id = $('.categoria_selected-express').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/busca-express/',
                data: { query: $('.search-query').val(), objetivo: objetivo_id, categoria: categoria_id, marca: marca_id, ordenar: ordenar_id, valor_min: valor_min_id, valor_max: valor_max_id }
            })
            .done(function(data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);
                $('#grade-produtos').html(data);
                loading.hide(1);

                window.history.pushState('', $('head').find('title').text(), '/academia/admin/express?' + string_gets);
            });
    });

    $('.categoria_selected-express').click(function() {
        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        var ordenar_id = $('#filtro-ordenar-express').val();
        $('.objcat-hidden').val('&oid=&cid=');

        var string_gets = $('.valor-hidden').val() +
            $('.ordenar-hidden').val() +
            $('.objcat-hidden').val() +
            $('.marca-hidden').val() +
            $('.busca-hidden').val();

        loading.show(1);

        categoria_id = "0";
        objetivo_id = "0";

        if ($('.marca_selected-express')) {
            marca_id = $('.marca_selected-express').attr('data-id');
        } else {
            marca_id = "0";
        }

        $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/busca-express/',
                data: { query: $('.search-query').val(), objetivo: objetivo_id, categoria: categoria_id, marca: marca_id, ordenar: ordenar_id, valor_min: valor_min_id, valor_max: valor_max_id }
            })
            .done(function(data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);
                $('#grade-produtos').html(data);
                loading.hide(1);

                window.history.pushState('', $('head').find('title').text(), '/academia/admin/express?' + string_gets);
            });
    });

    if ($('.filtragem a').length == 0) {
        $('.voltar-link').remove();
    }

    /**
     * CLICK MARCA MENU - FILTRO PRODUTOS
     */
    $('.link-marca-express').click(function() {
        loading.show(1);
        marca_id = $(this).attr('data-id');
        objetivo_id = 0;
        categoria_id = 0;

        if (medida_tela <= 768) {
            $('.close-filter').click();
        }

        if (grade_produtos.attr('data-section') == 'home') {
            var valor_id = $('#value-slider').val();
            var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
            var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
            var ordenar_id = $('#filtro-ordenar-express').val();

            get_produtos_express(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'marca');
        } else {
            location.href = WEBROOT_URL + 'academia/admin/express?&valor_min=&valor_max=&ord=&oid=&cid=&m=' + marca_id + '&busca=';
        }
    });

    /**
     * BUSCAR - CLIQUE NO BOTAO
     */
    $('#search-button-express').click(function() {
        $('.search-form-express').submit();
    });

    /**
     * BUSCAR - SUBMIT FORM
     */
    $('.search-form-express').submit(function() {
        loading.show(1);

        if (medida_tela <= 768) {
            $('.close-filter').click();
        }

        if ($('.marca_selected-express').length != 0) {
            marca_id = $('.marca_selected-express').attr('data-id');
        } else {
            marca_id = "0";
        }

        if ($('.categoria_selected-express').length != 0) {
            categoria_id = $('.categoria_selected-express').attr('data-id');
            objetivo_id = $('.categoria_selected-express').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        if (grade_produtos.attr('data-section') == 'home') {
            var valor_id = $('#value-slider').val();
            var valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
            var valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
            var ordenar_id = $('#filtro-ordenar-express').val();

            get_produtos_express(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id, 'busca');
        } else {
            location.href = WEBROOT_URL + 'academia/admin/express?&valor_min=&valor_max=&ord=&oid=&cid=&m=&busca=' + $('.search-query').val();
        }
    });


    function get_produtos_express(objetivo, categoria, marca, ordenar, valor_min, valor_max, tipo) {
        if (tipo == 'valor') {
            $('.valor-hidden').val('valor_min=' + valor_min + '&valor_max=' + valor_max);
        } else if (tipo == 'ordenar') {
            $('.ordenar-hidden').val('&ord=' + ordenar);
        } else if (tipo == 'objcat') {
            $('.objcat-hidden').val('&oid=' + objetivo + '&cid=' + categoria);
            $('.marca-hidden').val('&m=');
        } else if (tipo == 'marca') {
            $('.marca-hidden').val('&m=' + marca);
            $('.objcat-hidden').val('&oid=&cid=');
        } else if (tipo == 'busca') {
            $('.busca-hidden').val('&busca=' + $('.search-query').val());
        }

        var string_gets = $('.valor-hidden').val() +
            $('.ordenar-hidden').val() +
            $('.objcat-hidden').val() +
            $('.marca-hidden').val() +
            $('.busca-hidden').val();

        $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/busca-express/',
                data: { query: $('.search-query').val(), objetivo: objetivo, categoria: categoria, marca: marca, ordenar: ordenar, valor_min: valor_min, valor_max: valor_max }
            })
            .done(function(data) {
                $('html, body').animate({
                    scrollTop: grade_produtos.offset().top - 125
                }, 700);
                grade_produtos.html(data);
                loading.hide(1);

                window.history.pushState('', $('head').find('title').text(), '/academia/admin/express?' + string_gets);
            });
    }

    /*window.addEventListener("popstate", function(e) {

        function apenasNumeros(string) 
        {
            var numsStr = string.replace(/[^0-9]/g,'');
            return parseInt(numsStr);
        }
         
        var query = location.search.slice(1);
        var variaveis = query.split('&');
        var data = {};
        variaveis.forEach(function (parte) {
            var chaveValor = parte.split('=');
            var chave = chaveValor[0];
            var valor = chaveValor[1];
            data[chave] = valor;
        });

        data['busca']     ? busca     = data['busca']     : busca     = '';
        data['oid']       ? objetivo  = data['oid']       : objetivo  = 0;
        data['cid']       ? categoria = data['cid']       : categoria = 0;
        data['m']         ? marca     = data['m']         : marca     = 0;
        data['ord']       ? ordenar   = data['ord']       : ordenar   = 0;
        data['valor_min'] ? valor_min = data['valor_min'] : valor_min = 0;
        data['valor_max'] ? valor_max = data['valor_max'] : valor_max = 2000;

        var url_atual = window.location.href;
        var indicacao_url = url_atual.split("/");
        indicacao_url = indicacao_url[3].toString().split("?");
        if(indicacao_url[0] != 'professor-indicacao') {
            $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/busca-completa/',
                data: {query: busca, objetivo: objetivo, categoria: categoria, marca: marca, ordenar: ordenar, valor_min: valor_min, valor_max: valor_max}
            })
                .done(function (data) {
                    $('html, body').animate({
                        scrollTop: grade_produtos.offset().top - 125
                    }, 700);
                    grade_produtos.html(data);
                    loading.hide(1);
                });
        } else {
            $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/busca-professor/',
                data: {query: busca, objetivo: objetivo, categoria: categoria, marca: marca, ordenar: ordenar, valor_min: valor_min, valor_max: valor_max}
            })
                .done(function (data) {
                    $('html, body').animate({
                        scrollTop: grade_produtos.offset().top - 125
                    }, 700);
                    grade_produtos.html(data);
                    loading.hide(1);
                });
        }
    });*/

    //colocar produto na mochila
    $(document).on('click', '#comprar_id, .incluir-mochila', function(e) {
        e.preventDefault();
        loading.show(1);

        var url_comprar = $(this).attr('val');
        var SSlug = $(this).attr('slug-id');
        $.get(WEBROOT_URL + SSlug + '/produtos/comprar/' + url_comprar,
            function(data) {
                $('.mochila-nova').html(data);
                $('#abrir-mochila').click();
                loading.hide(1);
            });
    });

    $('#comprar_express_id, .incluir-mochila-express').click(function(e) {
        e.preventDefault();
        loading.show(1);

        var url_comprar = $(this).attr('val');
        $.get(WEBROOT_URL + 'produtos/comprar_express/' + url_comprar,
            function(data) {
                $('.laterais.mochila').html(data);
                if ($('.laterais.mochila').width() < 200) {
                    $('.close-mochila').click();
                }
                $('.produtos-mochila').animate({ 'width': '228px', 'opacity': 1 }, function() {
                    $('.info-direita-mochila').fadeIn(200);
                });
                loading.hide(1);
            });
    });

    $('#comprar_indicar_id, .indicar-mochila').click(function(e) {
        e.preventDefault();
        loading.show(1);

        var url_comprar = $(this).attr('val');
        $.get(WEBROOT_URL + 'produtos/comprar_indicar/' + url_comprar,
            function(data) {
                $('.laterais.mochila').html('');
                $('.laterais.mochila').html(data);
                $('.produtos-mochila').animate({ 'width': '228px', 'opacity': 1 }, function() {
                    $('.info-direita-mochila').fadeIn(200);
                });
                if ($('.laterais.mochila').width() < 200) {
                    $('.close-mochila').click();
                }
                loading.hide(1);
            });
    });

    /**
     * CENTRAL DE NOTIFICAÇÕES
     * @type {*|jQuery|HTMLElement}
     */
    var medida_tela = $(window).width();
    var medida_mochila_aberta = $(window).width() - 230;
    var medida_metade = medida_mochila_aberta / 2;
    var correcao = medida_metade - 247 + 50;
    var ctrl_abrir = 0;
    var ctrl_filter = 0;


    $('.img-produto-principal-drag, .img-combina-com, .produto-grade-drag').draggable({
        helper: 'clone'
    });


    $('.produtos-mochila').droppable({

        drop: function(evt, ui) {

            if (ui.draggable.parent()[0] == this) {
                return;
            }

            var t = $(this);
            var e = ui.draggable;
            var diff = { x: evt.pageX - ui.position.left, y: evt.pageY - ui.position.top };

            e.draggable('option', 'helper', '');

            $.get(WEBROOT_URL + 'produtos/comprar/' + e.attr('val'),
                function(data) {
                    $('.laterais.mochila').html('');
                    $('.laterais.mochila').html(data);
                    $('.produtos-mochila').animate({ 'width': '228px', 'opacity': 1 }, function() {
                        $('.info-direita-mochila').fadeIn(200);
                    });
                    loading.hide(1);
                });
        }
    });

    $('.close-mochila').click(function() {
        if ($('.mochila').width() > 1) {
            $('.info-direita-mochila').fadeOut(200);
            $('.produtos-mochila').animate({ 'width': '0', 'opacity': 0 }, 1000);
            $('.mochila').animate({ 'width': '0', 'opacity': 0 }, 1000);
            $('.fundo-carrinho').animate({ marginLeft: '250px' }, 1000);
            $('.box-close-mochila').animate({ width: '60px' });
            $('.cupom-desconto, .total-compra, .btn-fecha-mochila').animate({ 'right': '-250px' }, 1000);
        } else {
            $('.box-close-mochila').animate({ width: 0 }, 100);
            $('.mochila').animate({ 'width': '230px', 'opacity': 1 }, 1000);
            $('.produtos-mochila').animate({ 'width': '228px', 'opacity': 1 }, 1000, function() {
                $('.info-direita-mochila').fadeIn(200);
            });
            $('.fundo-carrinho').animate({ marginLeft: '0' }, 1000);
            $('.cupom-desconto, .total-compra, .btn-fecha-mochila').animate({ 'right': '0' }, 1000);
            if (medida_tela <= 768) {
                $('.close-mochila, .contador-itens').fadeOut();
            }
        }
    });

    if (medida_tela > 768) {

        $('#expandir-mochila').click(function() {
            if (ctrl_abrir == 0) {
                $('.produtos-mochila').animate({ 'width': medida_mochila_aberta - 2 }, 2000);
                $('.mochila').animate({ 'width': medida_mochila_aberta }, 2000, function() {
                    $('.info-prod, .valor-produto').fadeIn(400);
                });
                $('.mochila-titulo').animate({ 'paddingLeft': correcao }, 2000);
                $('#expandir-mochila').removeClass('seta-animate-fechar').addClass('seta-animate-abrir');
                $('.info-direita-mochila').animate({ paddingRight: '30px', width: '300px' }, 2000);
                $('.close-mochila').css('pointer-events', 'none');
                ctrl_abrir = 1;
            } else {
                $('.mochila').animate({ 'width': '230px' }, 2000);
                $('.produtos-mochila').animate({ 'width': '228px' }, 2000);
                $('.info-prod, .valor-produto').fadeOut(400);
                $('.mochila-titulo').animate({ 'paddingLeft': '30px' }, 2000);
                $('#expandir-mochila').removeClass('seta-animate-abrir').addClass('seta-animate-fechar');
                $('.info-direita-mochila').animate({ paddingRight: 0, width: '130px' }, 2000);
                $('.close-mochila').css('pointer-events', 'auto');
                ctrl_abrir = 0;
            }
        });

    }

    $('.close-filter').click(function() {
        if ($('.filtro').width() > 1) {
            $('.filtro').animate({ 'width': '0', 'opacity': 0 }, 1000);
            $('.box-filtro').animate({ 'left': '-250px' }, 1000);
            if (medida_tela > 768) {
                $('.close-filter').animate({ left: '-100px' }, 1000, function() {
                    $('.box-close-filter').animate({ width: '60px' });
                    $('.close-filter').animate({ left: '16px' });
                });
            } else {
                $('.close-filter').fadeIn();
                $('body').css('overflow', 'auto');
            }
            ctrl_filter = 0;
        } else {
            $('.box-close-filter').animate({ width: 0 });
            $('.filtro').animate({ 'width': '230px', 'opacity': 1 }, 1000);
            $('.box-filtro').animate({ 'left': '0' }, 1000);
            if (medida_tela <= 768) {
                $('.close-filter').fadeOut();
                $('body').css('overflow', 'hidden');
            }
            ctrl_filter = 1;
        }
    });

    var mobile_menu = $('#mobile-menu');
    $('#mobile-menu-button').click(function() {
        mobile_menu.slideToggle(350);
    });

    if (medida_tela > 768) {
        $('.detalhes-btn').click(function(e) {
            e.preventDefault();
            $('#expandir-mochila').click();
        });
    }

    /**
     * OBJETIVO MOBILE CLICK
     * @type {*|jQuery|HTMLElement}
     */
    var div_objetivo_mobile = $('.objetivo-mobile');
    div_objetivo_mobile.click(function() {
        var objetivo_id = $(this).attr('data-oid');
        loading.show(1);

        mobile_menu.is(':visible') ? mobile_menu.slideToggle(350) : '';

        $.get(WEBROOT_URL + 'produtos/filtro-objetivo/' + objetivo_id,
            function(data) {
                $('html, body').animate({
                    scrollTop: grade_produtos.offset().top - 125
                }, 700);

                grade_produtos.html(data);
                loading.hide(1);
            });
    });

    var div_marca_mobile = $('.marcas-mobile');

    /**
     * FUNÇÕES A SEREM EXECUTADAS AO SE CLICAR NO FUNDO
     * ESCONDER MENUS ATIVOS
     * @type {*|jQuery|HTMLElement}
     */
    /*var fundo_degrade = $('.fundo-degrade');
     fundo_degrade.on('click', function () {
     setTimeout(function () {
     if(notify.is(':visible')){
     notify.slideUp(300);
     }

     if(div_marcas.is(':visible')){
     nav_marcas.removeClass('navbar-active');
     div_marcas.hide(1);
     }

     if(div_objetivos.is(':visible')){
     nav_objetivos.removeClass('navbar-active').removeClass('massa-muscular');
     div_objetivos.hide(1);
     }
     }, 50);
     });*/

    /*
     * Botão Pagar Meus Pedidos - Ajax
     */
    $('.pagar-btn').click(function(e) {
        e.preventDefault();
        $.get(WEBROOT_URL + '/mochila/fechar-o-ziper/retormar-pedido/' + $(this).attr('data-id'),
            function(data) {

            });
    })


    $('.fundo-painel').on('mouseleave', function(event) {
        event.stopPropagation();
        var passadas = 0;

        if (notify.length > 0 && notify.is(':visible')) {
            notify.slideUp(300);
        }

        if (div_marcas.length > 0 && div_marcas.is(':visible')) {
            div_marcas.removeClass('navbar-active');
            div_marcas.hide(1);
        }

        if (div_objetivos.length > 0 && div_objetivos.is(':visible')) {
            nav_objetivos.removeClass('navbar-active').removeClass('massa-muscular');
            div_objetivos.hide(1);
        }
    });

    /**
     * BLOCO FORMULA - INTERNA PRODUTO
     * @type {*|jQuery|HTMLElement}
     */
    var formula_html = $('.formula');
    $('#formula').click(function() {
        if (!formula_html.is(':visible')) {
            $(this).addClass('atributos-produto-active').find('i').addClass('fa-rotate-180');
            $(formula_html).slideDown('slow');
        } else {
            $(this).removeClass('atributos-produto-active').find('i').removeClass('fa-rotate-180');
            $(formula_html).slideUp('slow');
        }
    });

    /**
     * FORM TOGGLE - ABRIR FORMULARIO OCULTO
     * @type {*|jQuery|HTMLElement}
     */
    var form_toggle = $('#form-toggle');
    $('#form-toggle-open').click(function() {
        if (!form_toggle.is(':visible')) {
            $(form_toggle).slideDown('slow');
        } else {
            $(form_toggle).slideUp('slow');
        }
    });

    /**
     * AJAX FORM
     * ENVIAR AVALIAÇÃO DO PRODUTO
     */
    $('#aval-submit').click(function() {
        var aval_form = $('#aval-form');
        var valid = aval_form.validationEngine("validate");
        if (valid == true) {
            var aval_form_data = aval_form.serialize();
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + "produto-comentarios-add",
                    data: aval_form_data
                        //dataType:  "json"
                })
                .done(function(data) {
                    $('#aval-submit').hide(1);
                    $('#produto-avaliacao').html(data);
                });
        } else {
            aval_form.validationEngine({
                updatePromptsPosition: true,
                //promptPosition: 'bottomLeft'
                promptPosition: 'inline',
                scroll: false
            });
        }
    });

    $('.miniatura').click(function() {
        $('#foto-produto').attr('src', WEBROOT_URL + 'img/loading.gif');
        $('#foto-produto').attr('src', WEBROOT_URL + 'img/produtos/' + $(this).find('.product-mini').attr('data-image'));
        $('.miniatura').removeClass('miniatura-active');
        $(this).addClass('miniatura-active');
    });

    /**
     * MASCARAS
     */
    if ($('.cep').length > 0) {
        $('.cep').mask('00.000-000', { reverse: false })
    }

    var SPMaskBehavior = function(val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

    if ($('.phone-mask').length > 0) {
        $('.phone-mask').mask(SPMaskBehavior, spOptions);
    }

    if ($('.cnpj').length > 0) {
        $('.cnpj').mask('00.000.000/0000-00', { reverse: false });
    }

    if ($('.cpf').length > 0) {
        $('.cpf').mask('000.000.000-00', { reverse: false });
    }

    if ($('.br_date').length > 0) {
        $('.br_date').mask('00/00/0000', { reverse: false });
    }

    if ($('.number').length > 0) {
        $('.number').mask('00000', { reverse: false });
    }

    /**
     * AJAX FORM
     * CENTRAL DE RELACIONAMENTO
     */
    $('#submit-cadastrar-central-relacionamento').on('click', function(e) {
        e.preventDefault();

        var form_contato = $('#form-central-relacionamento');
        var valid_contato = form_contato.validationEngine("validate");
        console.log(valid_contato);
        if (valid_contato == true) {

            loading.show(1);
            var form_data_contato = form_contato.serialize();
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + "relacionamento-contatos-add",
                    data: form_data_contato,
                    /*processData: false,
                     contentType: false*/
                })
                .done(function(data) {
                    $('#submit-cadastrar-central-relacionamento').hide(1);
                    $('#form-toggle-message').html(data);
                    loading.hide(1);
                });
        } else {
            console.log(form_contato);
            form_contato.validationEngine({
                validateNonVisibleFields: true,
                updatePromptsPosition: true,
                //promptPosition: 'bottomLeft'
                promptPosition: 'inline',
                scroll: false
            });
        }
    });

    /**
     * AJAX FORM
     * CADASTRAR ACADEMIA
     */
    $('#submit-cadastrar-academia').click(function() {
        var form = $('#form-cadastrar-academia');
        var valid = form.validationEngine("validate");
        if (valid == true) {
            var form_data = form.serialize();
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + "academias-add",
                    data: form_data,
                    /*processData: false,
                     contentType: false*/
                })
                .done(function(data) {
                    $('#submit-cadastrar-academia').hide(1);
                    $('#form-toggle-message-academia').html(data);
                    $('#submit-cadastrar-academia').show(5000);
                });
        } else {
            form.validationEngine({
                updatePromptsPosition: true,
                //promptPosition: 'bottomLeft'
                promptPosition: 'inline',
                scroll: false
            });
        }
    });

    $('#submit-indicar-academia').click(function() {
        var form = $('#form-indicar-academia');
        var valid = form.validationEngine("validate");
        if (valid == true) {
            var form_data = form.serialize();
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + "academias-indicar",
                    data: form_data,
                    /*processData: false,
                     contentType: false*/
                })
                .done(function(data) {
                    //$('#submit-cadastrar-academia').hide(1);
                    $('#submit-indicar-academia').hide(1);
                    $('#message-academia-indica').html('Indicação enviada com sucesso! <br />' +
                        'Em breve entraremos em contato com o responsável pela academia.');

                });
        } else {
            form.validationEngine({
                updatePromptsPosition: true,
                //promptPosition: 'bottomLeft'
                promptPosition: 'inline',
                scroll: false
            });
        }
    });

    /**
     * AJAX FORM
     * CADASTRAR TRANSPORTADORA
     */
    $('#submit-cadastrar-transportadora').click(function() {
        var form = $('#form-cadastrar-transportadora');
        var valid = form.validationEngine("validate");
        if (valid == true) {
            var form_data = form.serialize();
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + "transportadoras-add",
                    data: form_data,
                    /*processData: false,
                     contentType: false*/
                })
                .done(function(data) {
                    //$('#submit-cadastrar-transportadora').hide(1);
                    $('#form-toggle-message').html(data);
                });
        } else {
            form.validationEngine({
                updatePromptsPosition: true,
                //promptPosition: 'bottomLeft'
                promptPosition: 'inline',
                scroll: false
            });
        }
    });



    /**
     * AJAX FORM
     * CADASTRAR PARCEIRO
     */
    $('#submit-cadastrar-parceiro').click(function() {
        var form = $('#form-cadastrar-parceiro');
        var valid = form.validationEngine("validate");
        if (valid == true) {
            var form_data = form.serialize();
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + "fornecedores-add",
                    data: form_data,
                    /*processData: false,
                     contentType: false*/
                })
                .done(function(data) {
                    //$('#submit-cadastrar-parceiro').hide(1);
                    $('#form-toggle-message').html(data);
                });
        } else {
            form.validationEngine({
                updatePromptsPosition: true,
                //promptPosition: 'bottomLeft'
                promptPosition: 'inline',
                scroll: false
            });
        }
    });

    var form_cadastro = $('#form-cadastro');
    var form_login = $('#form-login');
    var info_cadastro = $('#info-cadastro');
    var submit_cadastro = $('#btn-submit-cadastro');
    // $('#btn-cadastro').click(function(){
    //     if(!form_cadastro.is(':visible')){
    //         $(this).html(' &laquo; FAZER LOGIN');
    //         info_cadastro.html('Informe os dados a seguir');
    //         form_login.slideUp(20);
    //         form_cadastro.slideDown(20);
    //         $('#escolher-academia-box').show(1);
    //     }else{
    //         $(this).html('CRIAR CADASTRO &raquo;');
    //         info_cadastro.html('Já tenho cadastro:');
    //         form_login.slideDown(20);
    //         form_cadastro.slideUp(20);
    //         $('#escolher-academia-box').hide(1);
    //     }
    // });


    /***
     * LOGIN / CADASTRO - EXIBIR OPÇÕES CONFORME OPÇÃO ESCOLHIDA: LOGAR OU CADASTRAR
     * @type {*|jQuery|HTMLElement}
     */
    var form_cadastro_main = $('#form-cadastro-main');
    var form_login_main = $('#form-login-main');
    var info_cadastro_main_com = $('#info-cadastro-main-com');
    var info_cadastro_main_sem = $('#info-cadastro-main-sem');
    var submit_cadastro_main = $('#btn-submit-cadastro-main');
    var fb_login = $('#fb-login');

    $('#btn-cadastro-login').click(function() {
        if (!form_login_main.is(':visible')) {
            //document.getElementById('#btn-cadastro-login-2').addClass('padding-0');
            document.getElementById("tela-login").style.paddingTop = null;
            document.getElementById("tela-login2").style.paddingTop = null;
            info_cadastro_main_com.slideDown(20);
            fb_login.slideDown(20);
            form_login_main.slideDown(20);
            $('#btn-cadastro-login-2').hide(1);
            $(this).html('&laquo; VOLTAR ');
            $('#escolher-academia-box').hide(1);
        } else {
            document.getElementById("tela-login").style.paddingTop = ('60px');
            document.getElementById("tela-login2").style.paddingTop = ('60px');
            info_cadastro_main_com.hide(1);
            fb_login.hide(1);
            form_login_main.hide(1);
            $('#btn-cadastro-login').slideDown(20);
            $('#btn-cadastro-login-2').slideDown(20);
            $(this).html(' FAZER LOGIN &raquo;');
            $('#escolher-academia-box').hide(1);
        }
    });

    $('#btn-cadastro-login-2').click(function() {
        if (!form_cadastro_main.is(':visible')) {
            $('#btn-cadastro-login').hide(1);
            document.getElementById("tela-login2").style.paddingTop = null;
            document.getElementById("tela-login").style.paddingTop = null;
            form_cadastro_main.slideDown(20);
            fb_login.slideDown(20);
            info_cadastro_main_sem.slideDown(20);
            $('#escolher-academia-box').slideDown(20);
            $(this).html('&laquo; VOLTAR');
        } else {
            $('#btn-cadastro-login').slideDown(20);
            document.getElementById("tela-login").style.paddingTop = ('60px');
            document.getElementById("tela-login2").style.paddingTop = ('60px');
            form_cadastro_main.hide(1);
            fb_login.hide(1);
            info_cadastro_main_sem.hide(1);
            $('#escolher-academia-box').hide(1);
            $(this).html('CRIAR CADASTRO &raquo;');

        }
    });



    /**
     * SUBMIT CADASTRO - TELA DE LOGIN (CADASTRO RAPIDO)
     */
    submit_cadastro_main.click(function() {
        var valid = form_cadastro_main.validationEngine("validate");
        if (valid == true) {
            var form_data = form_cadastro_main.serialize();
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + "clientes-add/" + $('#select-academia').val() + '/' + redirect_action.val(),
                    data: form_data
                })
                .done(function(data) {
                    info_cadastro_main_sem.html(data);
                });
        } else {
            form_cadastro_main.validationEngine({
                updatePromptsPosition: true,
                promptPosition: 'inline',
                scroll: false
            });
        }
    });

    submit_cadastro.on('click', function() {
        var valid = form_cadastro.validationEngine("validate");
        if ($('#validname').val() == '') {
            if ($('#passhome').val() != $('#passhomeconfirm').val()) {
                info_cadastro.css('color', 'red').html('Senhas não correspondem');
            } else {
                $('#passhomeconfirm').val() == 1;
                if (valid == true) {
                    var form_data = form_cadastro.serialize();
                    $.ajax({
                            type: "POST",
                            url: WEBROOT_URL + "clientes-add/" + $('#select-academia').val() + '/' + redirect_action.val(),
                            data: form_data,
                        })
                        .done(function(data) {
                            info_cadastro.css('color', 'green').html(data);
                        });
                } else {
                    form_cadastro.validationEngine({
                        updatePromptsPosition: true,
                        promptPosition: 'inline',
                        scroll: false
                    });
                }
            }
        }
    });

    form_cadastro_main.submit(function() {
        var valid = form_cadastro_main.validationEngine("validate");
        if ($('#senhalogin').val() != $('#conf_senhalogin').val()) {
            if (valid == true) {
                var form_data = form_cadastro_main.serialize();
                $.ajax({
                        type: "POST",
                        url: WEBROOT_URL + "clientes-add/" + $('#select-academia').val() + '/' + redirect_action.val(),
                        data: form_data,
                        /*processData: false,
                         contentType: false*/
                    })
                    .done(function(data) {
                        info_cadastro_main_sem.html(data);
                    });
            } else {
                form_cadastro_main.validationEngine({
                    updatePromptsPosition: true,
                    //promptPosition: 'bottomLeft'
                    promptPosition: 'inline',
                    scroll: false
                });
            }
        } else {
            info_cadastro_main_sem.html("As senha não combinam");
        }
    });

    form_cadastro.submit(function() {
        var valid = form_cadastro.validationEngine("validate");
        if ($('#validname').val() == '') {
            if ($('#passhome').val() != $('#passhomeconfirm').val()) {
                info_cadastro.css('color', 'red').html('Senhas não correspondem');
            } else {
                if (valid == true) {
                    var form_data = form_cadastro.serialize();
                    $.ajax({
                            type: "POST",
                            url: WEBROOT_URL + "clientes-add/" + $('#select-academia').val() + '/' + redirect_action.val(),
                            data: form_data,
                            /*processData: false,
                             contentType: false*/
                        })
                        .done(function(data) {
                            info_cadastro.css('color', 'green').html(data);
                        });
                } else {
                    form_cadastro.validationEngine({
                        updatePromptsPosition: true,
                        //promptPosition: 'bottomLeft'
                        promptPosition: 'inline',
                        scroll: false
                    });
                }
            }
        }
    });

    $('#submit-cadastrar-professor').click(function(e) {
        e.preventDefault();
        var valid = $('#form-cadastrar-professor').validationEngine("validate");
        if ($('#password').val() == $('#password-confirm').val()) {
            if (valid == true) {
                var form_data = $('#form-cadastrar-professor').serialize();
                $.ajax({
                        type: "POST",
                        url: WEBROOT_URL + "professor-add/",
                        data: form_data,
                        /*processData: false,
                         contentType: false*/
                    })
                    .done(function(data) {
                        $('#form-toggle-message').html(data);
                    });
            } else {
                $('#form-cadastrar-professor').validationEngine({
                    updatePromptsPosition: true,
                    //promptPosition: 'bottomLeft'
                    promptPosition: 'inline',
                    scroll: false
                });
            }
        } else {
            $('#form-toggle-message').html("As senha não combinam");
        }
    });

    /**
     * SUBMIT CADASTRO COMPLETO - CLIENTE
     */
    var form_cadastro_completo = $('#form-completar-cadastro-cliente');
    var info_cadastro_completo = $('#form-toggle-message');
    var submit_cadastro_completo = $('#submit-completar-cadastro-cliente');
    submit_cadastro_completo.click(function() {
        var valid = form_cadastro_completo.validationEngine("validate");
        if (valid == true) {
            var form_data = form_cadastro_completo.serialize();
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + "clientes-edit/" + xyz,
                    data: form_data
                })
                .done(function(data) {
                    info_cadastro_completo.html(data);
                });
        } else {
            form_cadastro_completo.validationEngine({
                updatePromptsPosition: true,
                promptPosition: 'inline',
                scroll: false
            });
        }
    });

    $('#cadastrar-academia-home').click(function() {
        $('#form-toggle').slideToggle(400);
        $('#form-toggle-contato').slideUp(400);
    });

    $('#cadastrar-academia-home-mobile').click(function() {
        $('#form-toggle').slideToggle(400).show(1);
        $('#form-toggle-contato').slideUp(400).hide(1);
    });

    $('.contato-home').click(function() {
        $('#form-toggle').hide(1);
        $('#form-toggle-contato').slideToggle(400);
        //go_to('#form-toggle-contato');
    });

    $('.conheca-home').click(function() {
        go_to('#bloco-conheca');
    });

    /**
     * AJAX FORM
     * LOGIN
     */
    //var form_login = $('#form-login');
    $('#submit-login').click(function() {
        ajax_login();
    });

    $('#login-password').click(function() {
        ajax_login();
    });

    form_login.submit(function() {
        ajax_login();
    });

    form_login_main.submit(function() {
        ajax_login_main();
    });

    $('#submit-login-main').click(function() {
        ajax_login_main();
    });

    $('#submit-login-express').click(function() {
        ajax_login_express();
    });


    /**
     * AJAX LOGIN FUNCTION     *
     */



    function ajax_login() {
        var form_login = $('#form-login');
        var valid = form_login.validationEngine("validate");
        if (valid == true) {
            var form_login_data = form_login.serialize();
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + "login/" + redirect,
                    data: form_login_data,
                    /*processData: false,
                     contentType: false*/
                })
                .done(function(data) {
                    $('#info-cadastro-2').html(data);
                });
        } else {
            form_login.validationEngine({
                updatePromptsPosition: true,
                //promptPosition: 'bottomLeft'
                promptPosition: 'inline',
                scroll: false
            });
        }
    }

    function ajax_login_main() {
        loading.show(1);
        var form_login = $('#form-login-main');
        var valid = form_login.validationEngine("validate");
        if (valid == true) {
            var form_login_data = form_login.serialize();
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + "login",
                    data: form_login_data,
                    /*processData: false,
                     contentType: false*/
                })
                .done(function(data) {
                    loading.hide(1);
                    $('#info-cadastro-main-com').html(data);
                });
        } else {
            form_login.validationEngine({
                updatePromptsPosition: true,
                //promptPosition: 'bottomLeft'
                promptPosition: 'inline',
                scroll: false
            });
        }
    }

    function ajax_login_express() {
        var form_login = $('#form-login-main');
        var valid = form_login.validationEngine("validate");
        if (valid == true) {
            var form_login_data = form_login.serialize();
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + "academia/admin/express/login",
                    data: form_login_data,
                    /*processData: false,
                     contentType: false*/
                })
                .done(function(data) {
                    $('#info-cadastro-main-com').html(data);
                });
        } else {
            form_login.validationEngine({
                updatePromptsPosition: true,
                //promptPosition: 'bottomLeft'
                promptPosition: 'inline',
                scroll: false
            });
        }
    }

    /**
     * CARREGAR CIDADES COM BASE NO ESTADO SELECIONADO
     */
    $('#states').on('change', function() {
        var state_id = $(this).val();
        var data;
        $.get(WEBROOT_URL + '/cidades/' + state_id,
            function(data) {
                $('#city').html(data);
            });

    });

    /**
     * CARREGAR CIDADES COM BASE NO ESTADO SELECIONADO
     */
    $('#states_acad').on('change', function() {
        var state_id = $(this).val();
        $.get(WEBROOT_URL + '/cidades/' + state_id,
            function(data) {
                $('#city_acad').html(data);
            });

        console.log('data');
    });

    $('#states-indica').on('change', function() {
        loading.show(1);
        var state_id = $(this).val();
        $.get(WEBROOT_URL + '/cidades/' + state_id,
            function(data) {
                $('#city-indica').html(data);
                loading.hide(1);
            });
    });

    /**
     * CARREGAR CIDADES QUE POSSUAM ACADEMIA COM BASE NO ESTADO SELECIONADO
     */
    $('#states_academias').on('change', function() {
        loading.show(1);
        var state_id = $(this).val();
        $.get(WEBROOT_URL + '/cidades/' + state_id + '/1',
            function(data) {
                $('#city').html(data);
                loading.hide(1);
            });
    });

    /**
     * CARREGAR CIDADES QUE POSSUAM ACADEMIA COM BASE NO ESTADO SELECIONADO - DADOS FATURAMENTO
     */
    $('#states_academia').on('change', function() {
        var state_id = $(this).val();
        $.get(WEBROOT_URL + '/cidades/' + state_id + '/1',
            function(data) {
                $('#city_academia').html(data);
            });
    });

    /**
     * IMAGE PREVIEW
     * @param input
     */
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#image_preview').attr('src', e.target.result);
                $('#image_action').val('new')
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image_upload").change(function() {
        readURL(this);
    });

    var valor_total;

    function atualizar_contador() {
        var contador = 0;

        $('.produto-qtd').each(function() {
            contador = contador + parseInt($(this).html());
        });

        $('#contador_mochila').html(contador);
    }

    $('.mochila, .lixo, .produto-mais, .produto-menos').click(function(e) {
        e.preventDefault();
        valor_total = $('#carrinho-total').attr('data-total');
        var url_atual = window.location.href;
        var express_url = url_atual.split("/");
        if (express_url[3] == 'academia' && express_url[4] == 'admin') {
            if (valor_total < 1000) {
                $('.btn-fecha-mochila').prop("disabled", true);
            } else {
                $('.btn-fecha-mochila').prop("disabled", false);
            }
        }
    });

    /**
     * CARRINHO - EXCLUI PRODUTO
     */
    $('.lixo').click(function(e) {
        e.preventDefault();
        var produto_id = $(this).attr('data-id');
        var produto = $('#produto-' + produto_id);
        if (produto.html() > 0) {
            produto.html(parseInt(produto.html()) - parseInt(produto.html()));
            var quantidade = (parseInt(produto.html()));

            calc_produto_carrinho(produto_id, 'subtracao');
            carrinho_edit(produto_id, quantidade);
        }
        $('#fundo-' + produto_id).remove();
        atualizar_contador();
    });

    /**
     * CARRINHO - DIMINUIR QUANTIDADE
     */
    $('.produto-menos').click(function() {
        var produto_id = $(this).attr('data-id');
        var produto = $('#produto-' + produto_id);
        $('.error-qtd').remove();
        if (produto.html() == 1) {
            $('#fundo-' + produto_id).remove();
        }
        if (produto.html() > 0) {
            produto.html((parseInt(produto.html()) - 1));
            var quantidade = (parseInt(produto.html()));

            calc_produto_carrinho(produto_id, 'subtracao');
            carrinho_edit(produto_id, quantidade);
        }
        atualizar_contador();
    });

    $(window).click(function() {
        $('.error-qtd').remove();
        $('.notify').fadeOut();
    });

    /**
     * CARRINHO - AUMENTAR QUANTIDADE
     */
    $('.produto-mais').click(function(event) {
        event.stopPropagation();
        var produto_id = $(this).attr('data-id');
        var estoque = $(this).attr('eid');
        var produto = $('#produto-' + produto_id);
        var quantidade = (parseInt(produto.html()) + 1);
        $('.error-qtd').remove();

        if (quantidade <= estoque) {
            produto.html(quantidade);

            calc_produto_carrinho(produto_id, 'soma');
            carrinho_edit(produto_id, quantidade);
        } else {
            $(this).parent().append('<i class="fa fa-exclamation-circle error-qtd" aria-hidden="true" title="esses são os últimos, em breve vamos colocar mais na prateleira!"></i>');

            $('.error-qtd').click(function(event) {
                event.stopPropagation();
            });

            if ($('#expandir-mochila').hasClass('seta-animate-abrir')) {
                $('.error-qtd').addClass('error-qtd-expanded');
            }
        }
        atualizar_contador();
    });

    $('.error-qtd').click(function(event) {
        event.stopPropagation();
    });

    /**
     * CARRINHO - CALCULO VALOR TOTAL PRODUTO
     * @param id
     * @param operacao
     */
    function calc_produto_carrinho(id, operacao) {

        var element = $('#produto-' + id);
        var preco_unitario = element.attr('data-preco');
        var quantidade = parseInt(element.html());

        var preco_total = parseFloat((preco_unitario * quantidade));

        $('#total-produto-' + id)
            .html('R$ ' + preco_total.toFixed(2).replace('.', ','))
            .attr('data-total', preco_total.toFixed(2));

        var subtotal = 0;
        $('.total-produto').each(function() {
            subtotal = parseFloat(subtotal + parseFloat($(this).attr('data-total')));
            console.log(subtotal);

        });

        var total = 0.0;
        var valor_cupom = $('.cupom-value').attr('data-id');
        var tipo_cupom = $('.cupom-value').attr('tipo-id');
        var desconto = 1.0 - (valor_cupom * .01);

        var url_atual = window.location.href;
        var express_url = url_atual.split("/");

        if (express_url[3] == 'academia' && express_url[4] == 'admin') {
            total = subtotal;
            console.log(total);
        } else {
            if (valor_cupom != 0) {
                if (tipo_cupom == 1) {
                    total = subtotal * desconto;
                    console.log(total);
                } else {
                    total = subtotal - valor_cupom;
                    console.log(total);
                }
            } else {
                total = subtotal;
                console.log(total);
            }
        }

        if (total.toFixed(2) >= 99.0) {
            console.log(total);
            $('.proximo-passo .botoes-mochila-sub').html('pagamento');
            $('.proximo-passo button').prop('disabled', false);
            $('.proximo-passo button').removeClass('inativo');
        } else {
            $('.proximo-passo .botoes-mochila-sub').html('*valor mínimo: R$99');
            $('.proximo-passo button').prop('disabled', true);
            $('.proximo-passo button').addClass('inativo');
        }
        console.log(total);

        $('#carrinho-total')
            .html('valor total: <span>R$' + total.toFixed(2).replace('.', ',') + '</span>')
            .attr('data-total', total.toFixed(2));
        console.log(total);

        if (express_url[3] == 'academia' && express_url[4] == 'admin') {
            if (total.toFixed(2) < 1000) {
                $('.btn-fecha-mochila').prop("disabled", true);
            } else {
                $('.btn-fecha-mochila').prop("disabled", false);
            }
        }
    }

    /**
     * CARRINHO - ALTERAR ITENS
     * @param id
     * @param qtd
     */
    function carrinho_edit(id, qtd) {
        $.get(WEBROOT_URL + '/mochila/alterar/' + id + '/' + qtd,
            function(data) {
                console.log('Itens alterados...');

            });

        var valor_total = $('#carrinho-total').attr('data-total');
        if (valor_total < 99.0) {
            return false;
        }
    }

    /**
     * COUNTDOWN PROMOCAO PAGE
     */
    if ($("#countdown").length > 0) {
        $("#countdown").countdown("2077/01/01", function(event) {
            $(this).text(
                event.strftime('%Hh %Mm %Ss')
            );
        });
    }


    /**
     * DADOS INCOMPLETOS - DESTACAR
     * @type {*|jQuery|HTMLElement}
     */
    var verificar_dados_form = $('.verificar-dados-incompletos');
    if (verificar_dados_form.length > 0) {
        var inputs = verificar_dados_form.find('input, select');

        inputs.each(function() {
            if ($(this).val() == '' || $(this).find(':selected').val() == '') {
                $(this).addClass('dado-incompleto');
            }
        })
    }

    /**
     * BUSCAR ACADEMIAS POR CIDADE
     */
    $('.cidade-academia').change(function() {
        loading.show(1);
        $.get(WEBROOT_URL + 'academias/busca-cidade/0/' + $(this).val(),
            function(data) {
                $('#select-academia').html(data);
                loading.hide(1);
            });
    });

    /**
     * BUSCAR PROFESSORES POR ACADEMIA
     */
    $('.academia-professor').change(function() {
        loading.show(1);
        $.get(WEBROOT_URL + 'academias/busca-professor/' + $(this).val(),
            function(data) {
                $('#select-professor').html(data);
                loading.hide(1);
            });
    });

    /**
     * BUSCAR ACADEMIAS POR ESTADO
     */
    $('.estado-academia').change(function() {
        loading.show(1);
        $.get(WEBROOT_URL + 'academias/busca-cidade/' + $(this).val(),
            function(data) {
                $('#select-academia').html(data);
                loading.hide(1);
            });
    });

    /**
     * BUSCAR ACADEMIAS POR CIDADE - DADOS FATURAMENTO
     */
    $('#city_academia').change(function() {
        loading.show(1);
        $.get(WEBROOT_URL + 'academias/busca-cidade/0/' + $(this).val(),
            function(data) {
                $('#academia-name-select').html(data);
                loading.hide(1);
            });
    });

    /**
     * RECOLOCAR PEDIDO
     */
    $('.recolocar-mochila').click(function(e) {
        e.preventDefault();
        loading.show(1);
        $('.erro-inativo').remove();
        $('.recolocar-mochila').removeClass('produto-verifica');
        $(this).addClass('produto-verifica');
        $.get(WEBROOT_URL + '/mochila/recolocar-pedido/' + $(this).attr('data-id'),
            function(data) {
                $('.laterais.mochila').html('');
                $('.laterais.mochila').html(data);
                $('.produtos-mochila').animate({ 'width': '228px', 'opacity': 1 }, function() {
                    $('.info-direita-mochila').fadeIn(200);
                });
                if ($('.laterais.mochila').width() < 200) {
                    $('.close-mochila').click();
                }
                loading.hide(1);
            });
    });

    $('.btn-recolocar').click(function(e) {
        e.preventDefault();
        loading.show(1);
        $.get(WEBROOT_URL + '/mochila/incluir-indicacao/' + $(this).attr('data-id'),
            function(data) {
                $('.laterais.mochila').html('');
                $('.laterais.mochila').html(data);
                $('.produtos-mochila').animate({ 'width': '228px', 'opacity': 1 }, function() {
                    $('.info-direita-mochila').fadeIn(200);
                });
                if ($('.laterais.mochila').width() < 200) {
                    $('.close-mochila').click();
                }
                loading.hide(1);
            });
    });

    /**
     * BUSCAR PRODUTOS
     */
    function search() {
        loading.show(1);
        valor_id = $('#value-slider').val();
        valor_min_id = valor_id.substring(0, valor_id.indexOf(';'));
        valor_max_id = valor_id.substring(valor_id.indexOf(';') + 1);
        ordenar_id = $('#filtro-ordenar').val();

        var search_query = $('.search-query');

        $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/busca/',
                data: { query: search_query.val() }
            })
            .done(function(data) {
                $('html, body').animate({
                    scrollTop: grade_produtos.offset().top - 125
                }, 700);
                grade_produtos.html(data);
                loading.hide(1);
            });

    }

    /**
     * CLICK CATEGORIAS::MENU OBJETIVOS - FILTRO PRODUTOS
     */
    /*$('.link-produto-categoria').click(function(){
     var objetivo    = $(this).attr('data-oid');
     var categoria   = $(this).attr('data-cid');

     loading.show(1);

     $.get(WEBROOT_URL + '/produtos/filtro-objetivo-categoria/' + objetivo + '/' + categoria,
     function (data) {
     $('html, body').animate({
     scrollTop: grade_produtos.offset().top - 125
     }, 700);
     grade_produtos.html(data);
     loading.hide(1);
     });
     });*/

    var menu_secundario = $('#menu-secundario');
    $(document).scroll(function() {
        if ($('#menu-principal').length > 0) {
            if (isScrolledIntoView('#menu-principal', 85)) {
                menu_secundario.removeClass('search-bar-fixed');
            } else {
                menu_secundario.addClass('search-bar-fixed');
            }
        }
    });

    if ($('.chosen-select').length > 0) {
        $('.chosen-select').multiselect({
            selectAllText: ' Selecionar tudo',
            filterPlaceholder: 'Busca',
            nonSelectedText: 'Nenhum selecionado',
            nSelectedText: 'selecionados',
            allSelectedText: 'Tudo selecionado',
        });
    }

    if ($('#login').length > 0) {
        $('#login-modal').fancybox({
            closeBtn: false,
            closeClick: false, // prevents closing when clicking INSIDE fancybox
            helpers: {
                overlay: { closeClick: false } // prevents closing when clicking OUTSIDE fancybox
            },
            keys: {
                close: null
            } // prevents close when clicking escape button
        }).trigger('click');
    }

    /*if($('.fancybox').length > 0) {
        $('.fancybox').fancybox();
    }*/

    $('.banners_home').carousel({
        interval: 1000 * 10
    });

    /*$('#login-modal').fancybox({
     padding : 0,
     margin : 0,
     'min-height' : 500,
     'max-height' : 500,
     'autoScale': false,
     'transitionIn': 'elastic',
     'transitionOut': 'elastic',
     'speedIn': 500,
     'speedOut': 300,
     'autoDimensions': false
     //'centerOnScroll': true
     }).trigger('click');*/


    /*$('.ffundo-painel').enscroll({
     showOnHover: false,
     verticalTrackClass: 'track3',
     verticalHandleClass: 'handle3'
     });*/

    if ($(".nano").length > 0) {
        $(".nano").nanoScroller({ alwaysVisible: false });
    }

    $('#academia-name-select').change(function() {
        var id = $(this).val();
        var professores = $('#professor');

        loading.show(1);

        $.get(WEBROOT_URL + '/academia-ajax/' + id,
            function(data) {
                data = JSON.parse(data);

                $('input[name="academia_cep"]').val(data['cep']);
                $('input[name="academia_address"]').val(data['address']);
                $('input[name="academia_number"]').val(data['number']);
                $('input[name="academia_complement"]').val(data['complement']);
                $('input[name="academia_area"]').val(data['area']);
                $('input[name="academia_uf"]').val(data['city']['state']['name']);
                $('input[name="academia_city"]').val(data['city']['name']);

                if (professores.length > 0) {
                    $.get(WEBROOT_URL + 'professores/listar-professores/' + id,
                        function(data_professor) {
                            professores.html(data_professor);
                        }
                    );
                }

                loading.hide(1);
            }
        );
    });

    /**
     * VALIDATION ENGINE
     */
    $('form').validationEngine({
        binded: true,
        updatePromptsPosition: true,
        //promptPosition: 'bottomLeft'
        promptPosition: 'inline',
        scroll: false
    });


    /**
     * *****************************************************************************************************************
     * PRODUTOS HOME PAGE LOAD MORE
     */
    $('#produtos-load-more-submit').click(function() {
        var last_produto = $("[id^=produto-]:visible:last").attr('id');
        last_produto = last_produto.replace('produto-', '');
        for (var i = last_produto; i <= (parseInt(last_produto) + parseInt(12)); i++) {
            $('#produto-' + i).removeClass('hide');
        }
        if ($("[id^=produto-]:last").is(':visible')) {
            $('#produtos-load-more-submit').hide();
        }
    });

    $('input').blur(function() {

        var field_id = $(this).attr('id');
        //var div_error   = $('.' + field_id + 'formError');

        var isValid = !$(this).validationEngine('validate');

        if (!isValid) {
            $(this).addClass('validation_error');
        } else {
            $(this).removeClass('validation_error');
        }

    });

    var check_email_input = $('.ck-c-email');
    check_email_input.blur(function() {
        if (!check_email_input.validationEngine('validate')) {
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + '/clientes/checkar-email/',
                    data: { email: $(this).val() }
                })
                .done(function(data) {

                    if (data == 1) {
                        check_email_input
                            .validationEngine('showPrompt', 'Email já cadastrado!', 'load')
                            .focus()
                            .select()
                            .addClass('validation_error');
                    } else {
                        check_email_input
                        //.validationEngine('showPrompt', 'Email já cadastrado!', 'load')
                            .removeClass('validation_error');
                    }
                });
        }
    });

    $('.ck-c-cpf').blur(function() {
        if (!$(this).validationEngine('validate')) {
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + '/mochila/fechar-o-ziper/checkar-cpf/',
                    data: { cpf: $(this).val() }
                })
                .done(function(data) {

                    if (data == 1) {
                        $('.ck-c-cpf')
                            .validationEngine('showPrompt', 'CPF já cadastrado!', 'load')
                            .focus()
                            .select()
                            .addClass('validation_error');
                    } else {
                        $('.ck-c-cpf').removeClass('validation_error');
                    }
                });
        }
    });

    $('#cpf').blur(function() {

        if (!$(this).validationEngine('validate')) {
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + '/mochila/fechar-o-ziper/checkar-cpf-professor/',
                    data: { cpf: $(this).val() }
                })
                .done(function(data) {

                    if (data == 1) {
                        $('#cpf')
                            .validationEngine('showPrompt', 'CPF já cadastrado!', 'load')
                            .addClass('validation_error');
                    } else {
                        $('#cpf').removeClass('validation_error');
                    }
                });
        }
    });

    $('.aplicar-cupom').click(function() {
        $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/check-cupom/',
                data: { codigo: $('.cupom-value').val() }
            })
            .done(function(data) {
                $('.area-cupom').html(data);

                var subtotal = 0;

                $('.total-produto').each(function() {
                    subtotal = parseFloat(subtotal + parseFloat($(this).attr('data-total')));
                });

                var valor_cupom = $('.cupom-value').attr('data-id');
                var desconto = 1.0 - (valor_cupom * .01);

                if (valor_cupom != 0) {
                    total = subtotal * desconto;
                } else {
                    total = subtotal;
                }

                $('#carrinho-total').html('valor total: <span>R$' + total.toFixed(2).replace('.', ',') + '</span>');

                if (total == 0) {
                    $('.btn-fecha-mochila').attr('disabled', true);
                } else {
                    $('.btn-fecha-mochila').attr('disabled', false);
                }
            });
    });

    $('#professor_carrinho').change(function() {

        var pedido_id = $(this).attr('data-pid');
        var professor_id = $(this).val();
        $.ajax({
            type: "GET",
            url: WEBROOT_URL + 'pedido-set-professor/' + pedido_id + '/' + professor_id,
            data: {}
        }).done(function(data) {

            if (data == 1) {

            } else {

            }
        });

    });

    /*setTimeout(function errorBorder(input){

     var field_id    = input.attr('id');
     var div_error   = $('.' + field_id + 'formError');
     if(div_error.length() > 0){

     $(this).addClass('validation_error');

     }else{
     $(this).removeClass('validation_error');

     }
     }, 500);*/

    /**
     * MOIP PAYMENTS
     */
    $('#pagarBoleto').click(function() {
        $(this).html('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> Gerando Boleto...');
        pagarBoleto();
    });

    $('#pagarCredito').click(function() {
        $(this).html('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> Processando Pagamento...');
        pagarCredito();
    });

    /**
     * CEP - VIACEP
     */
    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#address").val("");
        $("#area").val("");
        //$("#city").val("");
        //$("#states").val("");
        //$("#ibge").val("");
    }

    /**
     * CEP - VIACEP
     */
    function limpa_formulário_cep_acad() {
        // Limpa valores do formulário de cep.
        $("#address-acad").val("");
        $("#area-acad").val("");
        //$("#city").val("");
        //$("#states").val("");
        //$("#ibge").val("");
    }

    //Quando o campo cep perde o foco.
    $("#cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');
        console.log(cep);

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#address").val("...");
                $("#area").val("...");
                //$("#city").val("...");
                //$("#states").val("...");
                $("#ibge").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        console.log(dados);
                        //Atualiza os campos com os valores da consulta.
                        $("#address").val(dados.logradouro);
                        $("#area").val(dados.bairro);
                        $("#number_cli").val("");


                        var uf = '';

                        switch (dados.uf) {
                            case 'AC':
                                uf = 1;
                                break;
                            case 'AL':
                                uf = 2;
                                break;
                            case 'AM':
                                uf = 3;
                                break;
                            case 'AP':
                                uf = 4;
                                break;
                            case 'BA':
                                uf = 5;
                                break;
                            case 'CE':
                                uf = 6;
                                break;
                            case 'DF':
                                uf = 7;
                                break;
                            case 'ES':
                                uf = 8;
                                break;
                            case 'GO':
                                uf = 9;
                                break;
                            case 'MA':
                                uf = 10;
                                break;
                            case 'MG':
                                uf = 11;
                                break;
                            case 'MS':
                                uf = 12;
                                break;
                            case 'MT':
                                uf = 13;
                                break;
                            case 'PA':
                                uf = 14;
                                break;
                            case 'PB':
                                uf = 15;
                                break;
                            case 'PE':
                                uf = 16;
                                break;
                            case 'PI':
                                uf = 17;
                                break;
                            case 'PR':
                                uf = 18;
                                break;
                            case 'RJ':
                                uf = 19;
                                break;
                            case 'RN':
                                uf = 20;
                                break;
                            case 'RO':
                                uf = 21;
                                break;
                            case 'RR':
                                uf = 22;
                                break;
                            case 'RS':
                                uf = 23;
                                break;
                            case 'SC':
                                uf = 24;
                                break;
                            case 'SE':
                                uf = 25;
                                break;
                            case 'SP':
                                uf = 26;
                                break;
                            case 'TO':
                                uf = 27;
                                break;
                            default:
                                uf = '';
                        }

                        //$("#states").val(uf);

                        /*var state_id = $('#states').val();
                        $.get(WEBROOT_URL + '/cidades/' + state_id,
                            function(data_options){
                                $('#city').html(data_options);
                            });

                        setTimeout(function(){
                            var $dd = $('#city');
                            var $options = $('option', $dd);
                            $options.each(function() {
                                if ($(this).text() == dados.localidade) {
                                    $(this).attr('selected', true);
                                }
                            });
                        }, 700);*/

                        //$("#ibge").val(dados.ibge);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });

    //Quando o campo cep perde o foco.
    $("#cep-acad").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');
        console.log(cep);

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#address-acad").val("...");
                $("#area-acad").val("...");
                //$("#city").val("...");
                //$("#states").val("...");
                $("#ibge").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        console.log(dados);
                        //Atualiza os campos com os valores da consulta.
                        $("#address-acad").val(dados.logradouro);
                        $("#area-acad").val(dados.bairro);
                        $("#numbercli").val("");

                        var uf = '';

                        switch (dados.uf) {
                            case 'AC':
                                uf = 1;
                                break;
                            case 'AL':
                                uf = 2;
                                break;
                            case 'AM':
                                uf = 3;
                                break;
                            case 'AP':
                                uf = 4;
                                break;
                            case 'BA':
                                uf = 5;
                                break;
                            case 'CE':
                                uf = 6;
                                break;
                            case 'DF':
                                uf = 7;
                                break;
                            case 'ES':
                                uf = 8;
                                break;
                            case 'GO':
                                uf = 9;
                                break;
                            case 'MA':
                                uf = 10;
                                break;
                            case 'MG':
                                uf = 11;
                                break;
                            case 'MS':
                                uf = 12;
                                break;
                            case 'MT':
                                uf = 13;
                                break;
                            case 'PA':
                                uf = 14;
                                break;
                            case 'PB':
                                uf = 15;
                                break;
                            case 'PE':
                                uf = 16;
                                break;
                            case 'PI':
                                uf = 17;
                                break;
                            case 'PR':
                                uf = 18;
                                break;
                            case 'RJ':
                                uf = 19;
                                break;
                            case 'RN':
                                uf = 20;
                                break;
                            case 'RO':
                                uf = 21;
                                break;
                            case 'RR':
                                uf = 22;
                                break;
                            case 'RS':
                                uf = 23;
                                break;
                            case 'SC':
                                uf = 24;
                                break;
                            case 'SE':
                                uf = 25;
                                break;
                            case 'SP':
                                uf = 26;
                                break;
                            case 'TO':
                                uf = 27;
                                break;
                            default:
                                uf = '';
                        }

                        //$("#states").val(uf);

                        /*var state_id = $('#states').val();
                        $.get(WEBROOT_URL + '/cidades/' + state_id,
                            function(data_options){
                                $('#city').html(data_options);
                            });

                        setTimeout(function(){
                            var $dd = $('#city');
                            var $options = $('option', $dd);
                            $options.each(function() {
                                if ($(this).text() == dados.localidade) {
                                    $(this).attr('selected', true);
                                }
                            });
                        }, 700);*/

                        //$("#ibge").val(dados.ibge);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep_acad();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep_acad();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep_acad();
        }
    });




});

$('#Instituicao').change(function() {
    if ($(this).val() != 0 && $(this).val() != '') {
        parcelas();
    }
});

var boleto_url = $('#boleto_url');

/**
 * GERAR BOLETO E REDIRECIONAR
 */
boleto_url.click(function() {

    loading.show(1);

    $.ajax({
            type: "POST",
            url: WEBROOT_URL + '/carrinho/get_boleto/',
            data: { url: boleto_url.attr('data-href') },
        })
        .done(function(data) {
            loading.hide(1);

            window.open(boleto_url.attr('data-href'));

            setTimeout(function() {
                window.location.replace(WEBROOT_URL + 'mochila/checkout/' +
                    boleto_url.attr('data-pid') + '/' +
                    boleto_url.attr('data-cid') + '/' +
                    boleto_url.attr('data-k'));
            }, 1000);
        });

});

/**
 * MOIP - SUCESSO PAGAMENTO
 * @param data
 */
var pagamento_sucesso = function(data) {
    //alert('Sucesso: \n' + JSON.stringify(data));


    var btn_pagar_credito = $('#pagarCredito');
    console.log(data);
    $('#pagarBoleto').hide();
    btn_pagar_credito.hide();

    if (typeof data['Status'] != 'undefined') {
        loading.show();
        window.location.replace(WEBROOT_URL + 'mochila/checkout/' +
            btn_pagar_credito.attr('data-pid') + '/' +
            btn_pagar_credito.attr('data-cid') + '/' +
            btn_pagar_credito.attr('data-k'));
    } else {
        boleto_url.attr('data-href', data['url']).removeClass('hide').show(1);
    }
};

/**
 * MOIP - FALHA PAGAMENTO
 * @param data
 */
var pagamento_falha = function(data) {

    console.log(data);

    if (JSON.stringify(data[0]["Codigo"]) == 914) {
        $('#pagarBoleto').hide();
        $('#pagarCredito').hide();
        $('.fechar-overlay').hide();
        $('#cartao-pagamento').css('padding', '30px 45px 30px');
        $('#boleto-pagamento').html('A ponte entre a LOG e o cofre do banco está congestionada! Volte daqui a pouquinho =)');
        $('#cartao-pagamento').html('A ponte entre a LOG e o cofre do banco está congestionada! Volte daqui a pouquinho =)');

        location.href = WEBROOT_URL + 'home/fechar-o-ziper/';

    } else if (JSON.stringify(data[0]["Codigo"]) == 905 || JSON.stringify(data[0]["Codigo"]) == 912) {
        $('#pagarBoleto').hide();
        $('#pagarCredito').hide();
        $('.fechar-overlay').hide();
        $('#cartao-pagamento').css('padding', '30px 45px 30px');
        $('#boleto-pagamento').html(JSON.stringify(data[0]["Codigo"]));
        $('#cartao-pagamento').html(JSON.stringify(data[0]["Codigo"]));
    } else {
        $('#pagarBoleto').show();
        boleto_url.attr('href', '#').addClass('hide').hide(1);
    }
};

var bloco_boleto = $('#boleto-pagamento');
var bloco_cc = $('#cartao-pagamento');

var pagamento_forma = 'BoletoBancario';

/**
 * FORMA DE PAGAMENTO - BOLETO
 */
$('#pag_boleto').click(function() {
    bloco_cc.hide(1);
    bloco_boleto.slideDown(200);
});

/**
 * FORMA DE PAGAMENTO - CARTAO DE CREDITO
 */
$('#pag_cc').click(function() {
    bloco_boleto.hide(1);
    bloco_cc.slideDown(200);
});

/**
 * MOIP - PAGAR COM BOLETO
 */
pagarBoleto = function() {
    var settings = { "Forma": "BoletoBancario" };
    MoipWidget(settings);
};

/**
 * MOIP - PAGAR COM CARTAO DE CREDITO
 */
pagarCredito = function() {

    var form_cc = $('#cc');
    var valid = form_cc.validationEngine("validate");
    if (valid == true) {
        var settings = {
            "Forma": "CartaoCredito",
            "Instituicao": $('select[name="Instituicao"]').val(),
            "Parcelas": $('select[name="Parcelas"]').val(),
            "CartaoCredito": {
                "Numero": $('input[name="Numero"]').val(),
                "Expiracao": $('select[name="ExpiracaoMes"]').val() + '/' + $('select[name="ExpiracaoAno"]').val(),
                "CodigoSeguranca": $('input[name="CodigoSeguranca"]').val(),
                "Portador": {
                    "Nome": $('input[name="Portador_Nome"]').val(),
                    "DataNascimento": $('input[name="Portador_DataNascimento"]').val(),
                    "Telefone": $('input[name="Portador_Telefone"]').val(),
                    "Identidade": $('input[name="Portador_Identidade"]').val()
                }
            }
        };

        console.log(settings);

        MoipWidget(settings);
    } else {
        form_cc.validationEngine({
            updatePromptsPosition: true,
            //promptPosition: 'bottomLeft'
            promptPosition: 'inline',
            scroll: false
        });
    }
};

/**
 * PARCELAS
 */
parcelas = function() {

    var settings = {
        cofre: "",
        instituicao: $('select[name="Instituicao"]').val(),
        callback: "retornoCalculoParcelamento"
    };

    MoipUtil.calcularParcela(settings);
};


retornoCalculoParcelamento = function(parcelamento) {

    $("#Parcelas option").not('[value=1]').remove();

    $.each(parcelamento.parcelas, function(i, parcela) {
        console.log(parcela);
        if (parcela.quantidade > 1) {
            $('#Parcelas')
                .append(
                    '<option value="' + parcela.quantidade + '">' +
                    parcela.quantidade + 'x de R$ ' + parcela.valor.replace('.', ',') +
                    ' (Total: R$ ' + parcela.valor_total.replace('.', ',') + ')' +
                    '</option>');
        }
    })

};



/**
 * SELECT ACADEMIA
 * @param id
 */
function select_academia(estado, cidade, id) {
    var uf = '';

    switch (estado) {
        case 'AC':
            uf = 1;
            break;
        case 'AL':
            uf = 2;
            break;
        case 'AM':
            uf = 3;
            break;
        case 'AP':
            uf = 4;
            break;
        case 'BA':
            uf = 5;
            break;
        case 'CE':
            uf = 6;
            break;
        case 'DF':
            uf = 7;
            break;
        case 'ES':
            uf = 8;
            break;
        case 'GO':
            uf = 9;
            break;
        case 'MA':
            uf = 10;
            break;
        case 'MG':
            uf = 11;
            break;
        case 'MS':
            uf = 12;
            break;
        case 'MT':
            uf = 13;
            break;
        case 'PA':
            uf = 14;
            break;
        case 'PB':
            uf = 15;
            break;
        case 'PE':
            uf = 16;
            break;
        case 'PI':
            uf = 17;
            break;
        case 'PR':
            uf = 18;
            break;
        case 'RJ':
            uf = 19;
            break;
        case 'RN':
            uf = 20;
            break;
        case 'RO':
            uf = 21;
            break;
        case 'RR':
            uf = 22;
            break;
        case 'RS':
            uf = 23;
            break;
        case 'SC':
            uf = 24;
            break;
        case 'SE':
            uf = 25;
            break;
        case 'SP':
            uf = 26;
            break;
        case 'TO':
            uf = 27;
            break;
        default:
            uf = '';
    }

    $('#states_academias option[value=' + uf + ']').attr('selected', 'selected');

    setTimeout(function() {
        $.get(WEBROOT_URL + '/cidades/' + uf + '/1',
            function(data) {
                $('#city').html(data);

                $('#city').children().each(function() {
                    if ($(this).text() == cidade) {
                        cidade_val = $(this).val();

                        $('#city option[value=' + cidade_val + ']').attr('selected', 'selected');

                        $.get(WEBROOT_URL + 'academias/busca-cidade/' + uf + '/' + cidade_val,
                            function(data) {
                                $('#select-academia').html(data);
                            });

                        setTimeout(function() {
                            $('#map_canvas').effect('transfer', { to: "#select-academia", className: "ui-effects-transfer" }, 300, {});

                            $('#select-academia')
                                .val(id)
                                .attr('selected', true)
                                .effect('highlight', {}, 1200, {});
                        }, 200);
                    }
                });
            });
    }, 200);
}


// by http://codepen.io/RenanB/pen/GZeBNg
$('.parallax-img').each(function() {
    var img = $(this);
    var imgParent = $(this).parent();

    function parallaxImg() {
        var speed = img.data('speed');
        var imgY = imgParent.offset().top;
        var winY = $(this).scrollTop();
        var winH = $(this).height();
        var parentH = imgParent.innerHeight();

        var winBottom = winY + winH;

        if (winBottom > imgY && winY < imgY + parentH) {
            var imgBottom = ((winBottom - imgY) * speed);
            var imgTop = winH + parentH;
            var imgPercent = ((imgBottom / imgTop) * 100) + (50 - (speed * 50));
        }
        img.css({
            top: imgPercent + '%',
            transform: 'translate(-50%, -' + imgPercent + '%)'
        });
    }
    $(document).on({
        scroll: function() {
            parallaxImg();
        },
        ready: function() {
            parallaxImg();
        }
    });
});