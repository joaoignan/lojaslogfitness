function nicCount(editor, ctrId, maxChars)  {
    // Count characters remaining in the given nicEditor (editor), and update the given counter (ctrId)
    counterObj = document.getElementById(ctrId);
    if(counterObj) {
        var content = editor.getContent();
        if(maxChars - content.length <= 0) {
            // if there are no characters remaining, display the negative count in red
            counterObj.innerHTML = "<font color='red'>"+ (maxChars - content.length) + "</font>";
        }
        else { // characters remaining
            counterObj.innerHTML = maxChars - content.length;
        }

    }
}