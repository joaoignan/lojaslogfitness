$(document).ready(function() {

	$(document).on('click', '.buscar-btn', function() {
		var query = $('.buscar-query').val();

        if(query == '') {
            query = 0;
        }

        var url_atual = window.location.href;

        if((marca_url == null || marca_url == '0') && (filtro_url == null || filtro_url == '0')) {
            setCookie("image_bg", 'padrao-comparador.jpg', 365);
        }

        loading.show();
        var string_url = query + '/' + marca_url + '/' + tipo_filtro_url + '/' + filtro_url + '/' + ordenacao_url;

        $.get(WEBROOT_URL + '/produtos/filtrar/' + string_url,
            function (data) {
                setTimeout(function() {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 700);

                    $('.content-master').html(data);
                    
                    loading.hide();
                }, 600);
                window.history.pushState('', $('head').find('title').text(), WEBROOT_URL+'/comparador/'+string_url);
            });
	});

	$(document).on('click', '.marcas-btn', function() {
		var marca_name = $(this).attr('data-id');

        setCookie("image_bg", 'marcas/'+marca_name+'.jpg', 365);
		
        var url_atual = window.location.href;

        loading.show();
        var string_url = query_url + '/' + marca_name + '/' + tipo_filtro_url + '/' + filtro_url + '/' + ordenacao_url;

        $.get(WEBROOT_URL + '/produtos/filtrar/' + string_url,
            function (data) {
                setTimeout(function() {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 700);

                    $('.content-master').html(data);
                    
                    loading.hide();
                }, 600);
                window.history.pushState('', $('head').find('title').text(), WEBROOT_URL+'/comparador/'+string_url);
            });
	});

    $(document).on('click', '.objetivo-btn', function() {
        var objetivo_name = $(this).attr('data-id');

        setCookie("image_bg", 'objetivos/'+objetivo_name+'.jpg', 365);
        
        var url_atual = window.location.href;

        loading.show();
        var string_url = query_url + '/' + marca_url + '/objetivo/' + objetivo_name + '/' + ordenacao_url;

        $.get(WEBROOT_URL + '/produtos/filtrar/' + string_url,
            function (data) {
                setTimeout(function() {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 700);

                    $('.content-master').html(data);
                    
                    loading.hide();
                }, 600);
                window.history.pushState('', $('head').find('title').text(), WEBROOT_URL+'/comparador/'+string_url);
            });
    });

    $(document).on('click', '.categoria-btn', function() {
        var categoria_name = $(this).attr('data-id');

        setCookie("image_bg", 'categorias/'+categoria_name+'.jpg', 365);
        
        var url_atual = window.location.href;

        loading.show();
        var string_url = query_url + '/' + marca_url + '/categoria/' + categoria_name + '/' + ordenacao_url;

        $.get(WEBROOT_URL + '/produtos/filtrar/' + string_url,
            function (data) {
                setTimeout(function() {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 700);

                    $('.content-master').html(data);
                    
                    loading.hide();
                }, 600);
                window.history.pushState('', $('head').find('title').text(), WEBROOT_URL+'/comparador/'+string_url);
            });
    });

    $(document).on('click', '.subcategoria-btn', function() {
        var subcategoria_name = $(this).attr('data-id');

        setCookie("image_bg", 'subcategorias/'+subcategoria_name+'.jpg', 365);
        
        var url_atual = window.location.href;

        loading.show();
        var string_url = query_url + '/' + marca_url + '/subcategoria/' + subcategoria_name + '/' + ordenacao_url;

        $.get(WEBROOT_URL + '/produtos/filtrar/' + string_url,
            function (data) {
                setTimeout(function() {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 700);

                    $('.content-master').html(data);
                    
                    loading.hide();
                }, 600);
                window.history.pushState('', $('head').find('title').text(), WEBROOT_URL+'/comparador/'+string_url);
            });
    });

    $(document).on('keypress', '.buscar-query', function(e) {
      if (e.which == 13) {
        $('.buscar-btn').click();
        return false;
      }
    });

    $(document).on('click', '.btn-promocoes', function(e) {
        e.preventDefault();
        location.href = WEBROOT_URL + '/comparador/promocoes/';
    });

    $(document).on('click', '.btn-mais-vendidos', function(e) {
        e.preventDefault();
        location.href = WEBROOT_URL + '/comparador/mais-vendidos/';
    });

    $(document).on('change', '.ordenar-select', function() {
        var ordenacao = $(this).val();


        loading.show();
        var string_url = query_url + '/' + marca_url + '/' + tipo_filtro_url + '/' + filtro_url + '/' + ordenacao;

        $.get(WEBROOT_URL + '/produtos/filtrar/' + string_url,
            function (data) {
                $('html, body').animate({
                    scrollTop: 0
                }, 700);

                $('.content-master').html(data);
                loading.hide();
                $('.overlay-filtro').fadeOut();
                window.history.pushState('', $('head').find('title').text(), WEBROOT_URL+'/comparador/'+string_url);
            });
    });

    var atualizando = 0;

    $(window).scroll(function() {
        var posicao_pagina = $(document).scrollTop();
        var container = $('.container-grade');
        var quarto_produto = $('.box-completa-div:nth-of-type(4)');

        var url_atual = window.location.href;

        if(url_atual.indexOf("similares") != -1) {
            var posicao_container = (container.offset().top + 280);
        } else {
            var posicao_container = (container.offset().top + 370);
        }

        if(quarto_produto.val() != undefined && quarto_produto.val() != null) {
            var posicao_quarto_produto = (quarto_produto.offset().top - 260);
        }
        var id_produtos = [];

        //FILTRO

        if(posicao_pagina >= posicao_container) {
            $('.filtro-div').addClass('fixed_filtro');
            $('.produtos-div').addClass('col-md-offset-2');
        } else {
            $('.filtro-div').removeClass('fixed_filtro');
            $('.produtos-div').removeClass('col-md-offset-2');
        }

        //SUBFILTRO

        if($(document).width() > 992) {
            if(posicao_pagina >= (posicao_container + 200) && posicao_pagina < posicao_quarto_produto) {
                $('.subfiltro-completo').hide().addClass('fixed_subfiltro');
                $('.subfiltro-completo h4').removeClass('texto-sombra');
                $('.produtos-scroll').css('margin-top', '87px');
            } else if(posicao_pagina < (posicao_container + 200)) {
                $('.subfiltro-completo h4').addClass('texto-sombra');
                $('.subfiltro-completo').removeClass('fixed_subfiltro').show();
                $('.produtos-scroll').css('margin-top', '15px');
            }

            if(posicao_pagina >= posicao_quarto_produto) {
                $('.fixed_subfiltro').css('width', $('.produtos-scroll').width());
                $('.fixed_subfiltro').fadeIn(200);
            } else if(posicao_pagina < posicao_quarto_produto && posicao_pagina >= (posicao_container + 200)) {
                $('.fixed_subfiltro').css('width', '100%');
                $('.fixed_subfiltro').fadeOut(200);
            }
        }

        //CARREGAR MAIS PRODUTOS

        if(posicao_pagina + $(window).height() >= ($(document).height() - 800) && atualizando == 0) {
            atualizando = 1;

            $('.box-completa-produto').each(function() {
              id_produtos.push($( this ).attr('data-id'));
            });

            $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/atualizar-produtos/',
                data: {
                    query: query_url,
                    marca: marca_url, 
                    tipo_filtro: tipo_filtro_url, 
                    filtro: filtro_url, 
                    ordenacao: ordenacao_url,
                    produtos_ids: id_produtos
                }
            })
                .done(function (data) {
                    $('.produtos-scroll').append(data);
                    loading.hide(1);
                    setTimeout(function() {
                        atualizando = 0;
                    }, 1200);
                });
        }
    });

    $(document).on('click', '.query_selected', function() {
        loading.show();
        var string_url = '0/' + marca_url + '/' + tipo_filtro_url + '/' + filtro_url + '/' + ordenacao_url;

        $.get(WEBROOT_URL + '/produtos/filtrar/' + string_url,
            function (data) {
                $('html, body').animate({
                    scrollTop: 0
                }, 700);

                $('.content-master').html(data);
                loading.hide();
                $('.overlay-filtro').fadeOut();
                window.history.pushState('', $('head').find('title').text(), WEBROOT_URL+'/comparador/'+string_url);
            });
    });

    $(document).on('click', '.marca_selected', function() {
        if(filtro_url != null && filtro_url != '0') {
            setCookie("image_bg", tipo_filtro_url+'s/'+filtro_url+'.jpg', 365);
        } else {
            setCookie("image_bg", 'padrao-comparador.jpg', 365);
        }
        
        loading.show();
        var string_url = query_url + '/0/' + tipo_filtro_url + '/' + filtro_url + '/' + ordenacao_url;

        $.get(WEBROOT_URL + '/produtos/filtrar/' + string_url,
            function (data) {
                $('html, body').animate({
                    scrollTop: 0
                }, 700);

                $('.content-master').html(data);
                loading.hide();
                $('.overlay-filtro').fadeOut();
                window.history.pushState('', $('head').find('title').text(), WEBROOT_URL+'/comparador/'+string_url);
            });
    });

    $(document).on('click', '.filtro_selected', function() {
        if(marca_url != null && marca_url != '0') {
            setCookie("image_bg", 'marcas/'+marca_url+'.jpg', 365);
        } else {
            setCookie("image_bg", 'padrao-comparador.jpg', 365);
        }

        loading.show();
        var string_url = query_url + '/' + marca_url + '/0/0/' + ordenacao_url;

        $.get(WEBROOT_URL + '/produtos/filtrar/' + string_url,
            function (data) {
                $('html, body').animate({
                    scrollTop: 0
                }, 700);

                $('.content-master').html(data);
                loading.hide();
                $('.overlay-filtro').fadeOut();
                window.history.pushState('', $('head').find('title').text(), WEBROOT_URL+'/comparador/'+string_url);
            });
    });
});