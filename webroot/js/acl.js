$(document).ready(function(){

    //SELECT / UNSELECT ALL
    $('.grupo').on('click', function(){
        var classe = $(this).attr('id');
        if($(this).is(':checked')){
            $(this).attr('checked', true);
            $('.' + classe).attr('checked', true);
        }else{
            $(this).attr('checked', false);
            $('.' + classe).attr('checked', false);
        }
    })
});
