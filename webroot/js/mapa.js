var map;
	function initMap() {
		var academia = {lat: -20.788564, lng: -49.356108};
		map = new google.maps.Map(document.getElementById('map'), {
		  center: academia,
		  zoom: 15
		});
		var marker = new google.maps.Marker({
          position: academia,
          map: map,
          icon: 'img/map-marker.png'
        });
        var infowindow = new google.maps.InfoWindow(), marker;
 
		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				var imagem = "<?= $this->Html->image('icone3.png',['title' => 'logo-academia', 'alt' => 'Logo']) ?>";
		        infowindow.setContent("<div class='col-xs-12 academia-single'><div class='single-main'><div class='single-content'><div class='content-logo'>"+imagem+"</div><div class='content-name'><p><strong>Nome da academia</strong></p><p><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i></p></div><div class='content-desc col-xs-12'><p><strong>Endereço:</strong> Rua Netshoes, 2525 - Prédio Roxo</p><p><strong>Modalidades:</strong> Frete, Entrega atrasada, Ruim</p><p><strong>A partir de:</strong> R$ 59,90</p></div></div></div></div>");
		        infowindow.open(map, marker);
			}
		})(marker))
	}