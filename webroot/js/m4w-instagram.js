/**
 * M4W Instagram
 */

$(document).ready(function() {

    var owl = $("#owl-demo");

    owl.owlCarousel({
        items : 10, //10 items above 1000px browser width
        itemsDesktop : [1000,5], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,3], // betweem 900px and 601px
        itemsTablet: [600,2], //2 items between 600 and 0
        itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
    });

    // Custom Navigation Events
    $(".next").click(function(){
        owl.trigger('owl.next');
    });
    $(".prev").click(function(){
        owl.trigger('owl.prev');
    });
    $(".play").click(function(){
        owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
    });
    $(".stop").click(function(){
        owl.trigger('owl.stop');
    });

    $("#owl").owlCarousel({

        autoPlay: 3000, //autoPlay 3 segundos

        items : 7, // Quantidade de itens por "bloco"

        itemsDesktop : [1000,6], //5 itens entre 1000px e 901px
        itemsDesktopSmall : [900,3], // 3 itens entre 900px e 601px
        itemsTablet: [600,2], //2 itens entre 600 e 0;
        itemsMobile : false, // desabilitado, herda opções do itemsTablet

    });

});