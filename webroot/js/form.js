var loading = $('.loading');

$(document).ready(function() {
    var referrer = document.referrer;

    /**
     * CARREGAR CIDADES COM BASE NO ESTADO SELECIONADO
     */
    $('#states').change(function() {

        var state_id = $(this).val();
        $.get(WEBROOT_URL + '/cidades-ajax/' + state_id,
            function(data) {
                $('#city').html(data).trigger('chosen:updated');
            });
    });

    /**
     * MASCARAS
     */
    if ($('.cep').length > 0) {
        $('.cep').mask('00.000-000', { reverse: false })
    }

    var SPMaskBehavior = function(val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

    if ($('.phone').length > 0) {
        $('.phone').mask(SPMaskBehavior, spOptions);
    }

    if ($('.cnpj').length > 0) {
        $('.cnpj').mask('00.000.000/0000-00', { reverse: false });
    }

    if ($('.cpf').length > 0) {
        $('.cpf').mask('000.000.000-00', { reverse: false });
    }

    if ($('.br_date').length > 0) {
        $('.br_date').mask('00/00/0000', { reverse: false });
    }

    if ($('.price').length > 0) {
        $('.price').mask("#.##0,00", { reverse: true });
    }


    /**
     * SLUG VARS FOR FUNCTION
     * @type {*|jQuery|HTMLElement}
     */
    var propriedade = $('#propriedade');
    var name_field = $('#name');
    var slug_field = $('#slug');
    var tamanho_field = $('.tamanho-doses');
    var unmedida_field = $('#unidade-medida');
    var propriedade_field = $('#propriedade_text');
    var embalagem_field = $('.embalagem-text');

    /**
     * GENERATE SLUG
     * @param text
     */
    function generate_slug(text) {

        $.ajax({
            type: "POST",
            url: WEBROOT_URL + 'sanitize-string/',
            data: { str: text }
        }).done(function(data) {
            slug_field.val(data.toLowerCase());
        });

        /*var r = text.replace(/ /g, '-');
         r = r.toLowerCase();
         slug_field.val(r);*/
    }

    name_field.blur(function() {
        if (propriedade_field.length && propriedade_field.val() != '') {
            generate_slug($(this).val() + tamanho_field.val() + ' ' + unmedida_field.val() + '-' + embalagem_field.val() + '-' + propriedade_field.val());
        } else {
            if (tamanho_field.val() != null) {
                generate_slug($(this).val() + tamanho_field.val() + ' ' + unmedida_field.val() + '-' + embalagem_field.val());
            } else if (embalagem_field.val() != null) {
                generate_slug($(this).val() + '-' + embalagem_field.val());
            } else {
                generate_slug($(this).val());
            }
        }
    });

    propriedade_field.blur(function() {
        generate_slug(name_field.val() + tamanho_field.val() + ' ' + unmedida_field.val() + '-' + embalagem_field.val() + '-' + $(this).val());
    });

    if ($('form').length > 0) {
        jQuery('form').validationEngine({
            validateNonVisibleFields: true,
            updatePromptsPosition: true,
            //promptPosition: 'bottomLeft'
            promptPosition: 'inline'
        });
        /*if($('#form-contato').length > 0){
         jQuery('#form-contato').validationEngine('attach', {
         promptPosition: "topLeft",
         onValidationComplete: function (form, status) {
         if (status == true) {
         jQuery('#submit-contato').hide(1);
         jQuery('#loader-contato').show(1);
         jQuery("html, body").animate({scrollTop: jQuery('#loader-contato').offset().top}, 1000);
         form.validationEngine('detach');
         form.submit();
         } else {
         jQuery('#loader-contato').hide(1);
         jQuery('#submit-contato').show(1);
         }
         }
         });
         }else{
         $.validationEngine.defaults.validateAttribute = "class";
         jQuery('form').validationEngine({
         validateNonVisibleFields: true,
         updatePromptsPosition: true,
         promptPosition: 'bottomRight'
         });
         }*/
    }

    //LINKS DEFAULT LAYOUT
    var url = window.location.pathname;
    var host_url = window.location.host + '//' + window.location.host;
    url.replace(host_url, '');

    var nav_element = $('.sidebar-submenu a[href="' + url + '"]');

    if (nav_element.length > 0) {
        nav_element.addClass('nav-hover');
        nav_element.closest('.sidebar-submenu').addClass('show');
    } else {
        var param = url.substr(url.lastIndexOf('/') + 1);
        url = url.replace('/' + param, '');

        nav_element = $('.sidebar-submenu a[href="' + url + '"]');

        nav_element.addClass('nav-hover');
        nav_element.closest('.sidebar-submenu').addClass('show');
        //alert(url);
    }

    //alert(Controller);
    //alert(Action);

    /**
     * PREVIEW SELECTED IMAGE TO UPLOAD
     * @param input
     */
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#image_preview').attr('src', e.target.result);
                $('#image_action').val('new')
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image_upload").change(function() {
        readURL(this);
    });

    /**
     * IMAGE UPLOAD PREVIEW
     * @param input
     * @param preview
     */
    function image_upload_preview(input, preview) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(preview).attr('src', e.target.result);
                $(preview).closest('a').attr('href', e.target.result);
                //$('#image_action').val('new')
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".foto_upload").change(function() {
        var preview = $(this).attr('data-preview');
        image_upload_preview(this, preview);
    });

    $('.clear-foto').click(function() {
        var foto_id = $(this).attr('data-id');
        $('#' + foto_id).val('');
        $('#' + foto_id + '-preview').attr('src', produto_foto_default);
        console.log('upd');
    });

    /**
     * CHANGE PROPRIEDADE
     * @type {*|jQuery|HTMLElement}
     */
    $('#propriedade_select').change(function() {
        if ($(this).val() >= 1) {
            var propriedade_titulo = $(this).find('option:selected').text();
            propriedade.removeClass('hidden').show();
            propriedade.find('label').html(propriedade_titulo);
            if (propriedade_titulo == 'Sabor') {
                propriedade.find('input').attr('placeholder', 'Ex.: "Morango" ou "Chocolate"');
            } else if (propriedade_titulo == 'Cor') {
                propriedade.find('input').attr('placeholder', 'Ex.: "Vermelho" ou "Azul"');
            }
        } else {
            propriedade.addClass('hidden').hide();
            propriedade.find('label').html('Propriedade');
            propriedade.find('input').val('');
        }
    });

    /**
     * BUSCAR INFORMAÇÕES COM BASE NO PRODUTO BASE SELECIONADO
     */
    $('#produto_base_id').change(function() {
        $('#marcas_select').attr('selected', true).siblings().removeAttr('selected');
        /**
         * POPULAR OS CAMPOS
         */
        if ($(this).val() >= 1) {
            $.post(WEBROOT_URL + 'admin/produtos/ajax_view/' + $(this).val(), function(data) {

                data = JSON.parse(data);

                console.log(data);

                $('input[name="name"]').val(data['name']).attr('disabled', true);

                $('#marcas_select option[value=' + data['marca_id'] + ']')
                    .attr('selected', true).siblings().removeAttr('selected')
                    .trigger('chosen:updated');
                $('#marcas_select').prop('disabled', true).trigger('chosen:updated');


                $('#forma_consumo')
                    .val(data['forma_consumo']);

                $('#quando_tomar')
                    .val(data['quando_tomar']);

                $('.objetivo-checkbox').attr('disabled', false);
                $('.categoria-checkbox').attr('disabled', false);
                $('.subcategoria-checkbox').attr('disabled', false);

                $('.objetivo-checkbox:checked').click();

                $.each(data['produto_objetivos'], function(index, produto_objetivo) {
                    $('.objetivo-checkbox').each(function() {
                        if ($(this).val() == produto_objetivo.objetivo_id) {
                            $(this).click();
                        }
                    });
                });

                $.each(data['produto_categorias'], function(index, produto_categoria) {
                    $('.categoria-checkbox').each(function() {
                        if ($(this).val() == produto_categoria.categoria_id) {
                            $(this).click();
                        }
                    });
                });

                $.each(data['produto_subcategorias'], function(index, produto_subcategoria) {
                    $('.subcategoria-checkbox').each(function() {
                        if ($(this).val() == produto_subcategoria.subcategoria_id) {
                            $(this).click();
                        }
                    });
                });

                $('.objetivo-checkbox').attr('disabled', true);
                $('.categoria-checkbox').attr('disabled', true);
                $('.subcategoria-checkbox').attr('disabled', true);

                $('#atributos').html(data['atributos']);
                $('#dicas').html(data['dicas']);
                $('#beneficios').html(data['beneficios']);

                $('input[name="atributos"]').val(data['atributos']);
                $('#summernote').find('.note-editable').html(data['atributos']);

                $('input[name="dicas"]').val(data['dicas']);
                $('#summernote-dicas').find('.note-editable').html(data['dicas']);

                $('input[name="como_tomar"]').val(data['como_tomar']);
                $('#summernote-como_tomar').find('.note-editable').html(data['como_tomar']);

                $('input[name="ingredientes"]').val(data['ingredientes']);
                $('#summernote-ingredientes').find('.note-editable').html(data['ingredientes']);

                $('input[name="beneficios"]').val(data['beneficios']);
                $('#summernote-beneficios').find('.note-editable').html(data['beneficios']);

                $.each(data['produto_objetivos'], function(i, e) {
                    $('#objetivos-ids option[value="' + e.objetivo_id + '"]').attr('selected', true).trigger('chosen:updated');
                });

                $.each(data['produto_categorias'], function(i, e) {
                    $('#categorias-ids option[value="' + e.categoria_id + '"]').attr('selected', true).trigger('chosen:updated');
                });

                /**
                 * GERAR SLUG
                 */
                if (propriedade_field.length && propriedade_field.val() != '') {
                    generate_slug(name_field.val() + '-' + embalagem_field.val() + '-' + propriedade_field.val());
                } else {
                    generate_slug(name_field.val() + '-' + embalagem_field.val());
                }

                var ultima_substancia = $('.substancia').last();
                var value_ultima_substancia = ultima_substancia.attr('id').split('-');
                value_ultima_substancia = parseInt(value_ultima_substancia[1]);
                var new_value = value_ultima_substancia;

                //Substâncias
                if (data['substancias']) {
                    $("#substancia-0").val('');
                    $("#valor-substancia-0").val('');
                    new_value = 0;

                    while ($('#substancia-1').length >= 1) {
                        $('.remover-substancia').click();
                    }

                    $.each(data['substancias'], function(i, e) {

                        if (new_value != 0) {
                            $('.adicionar-substancia').click();
                        }

                        $("#substancia-" + new_value).val(e.substancia);
                        $("#valor-substancia-" + new_value).val(e.valor);

                        new_value++;
                    });
                } else {
                    $("#substancia-0").val('');
                    $("#valor-substancia-0").val('');

                    while ($('#substancia-1').length >= 1) {
                        $('.remover-substancia').click();
                    }
                }

                //Textos
                if (data['textos']) {
                    $('textarea[name="ingredientes"]').html(data['textos']['ingredientes']);
                    $('textarea[name="como_tomar"]').html(data['textos']['como_tomar']);
                } else {
                    $('textarea[name="ingredientes"]').html('');
                    $('textarea[name="como_tomar"]').html('');
                }
            });
        } else {
            /**
             * RESET DOS CAMPOS
             */
            $('input[name="name"]').val('').attr('disabled', false);
            $('input[name="atributos"]').val('');
            $('input[name="dicas"]').val('');
            $('input[name="embalagem_conteudo"]').val('').attr('disabled', false);
            $('#forma_consumo').val('');
            $('#quando_tomar').val('');
            $('input[name="peso"]').val('').attr('disabled', false);
            $('input[name="largura"]').val('').attr('disabled', false);
            $('input[name="altura"]').val('').attr('disabled', false);
            $('input[name="profundidade"]').val('').attr('disabled', false);
            $('input[name="doses"]').val('').attr('disabled', false);

            $('.objetivo-checkbox').attr('disabled', false);
            $('.categoria-checkbox').attr('disabled', false);
            $('.subcategoria-checkbox').attr('disabled', false);

            $('.objetivo-checkbox:checked').click();

            $('#propriedade_select')
                .val('')
                .prop('disabled', false)
                .trigger('chosen:updated');

            $('#marcas_select')
                .val('')
                .prop('disabled', false)
                .trigger('chosen:updated');

            $('#fornecedor_id')
                .val('')
                .prop('disabled', false)
                .trigger('chosen:updated');

            //FORMULA
            $('input[name="atributos"]').val('');
            $('#summernote').find('.note-editable').html('');

            //DICAS
            $('input[name="dicas"]').val('');
            $('#summernote-dicas').find('.note-editable').html('');

            //COMO TOMAR
            $('input[name="como_tomar"]').val('');
            $('#summernote-como_tomar').find('.note-editable').html('');

            //INGREDIENTES
            $('input[name="ingredientes"]').val('');
            $('#summernote-ingredientes').find('.note-editable').html('');

            //BENEFÍCIOS
            $('input[name="beneficios"]').val('');
            $('#summernote-beneficios').find('.note-editable').html('');

            //Substâncias
            $("#substancia-0").val('');
            $("#valor-substancia-0").val('');

            while ($('#substancia-1').length >= 1) {
                $('.remover-substancia').click();
            }
        }

        setTimeout(function() {
            if ($('#propriedade_select').find('option:selected').text()) {
                var propriedade_titulo = $('#propriedade_select').find('option:selected').text();
                propriedade.removeClass('hidden').show();
                propriedade.find('label').html(propriedade_titulo);
                if (propriedade_titulo == 'Sabor') {
                    propriedade.find('input').attr('placeholder', 'Ex.: "Morango" ou "Chocolate"');
                } else if (propriedade_titulo == 'Cor') {
                    propriedade.find('input').attr('placeholder', 'Ex.: "Vermelho" ou "Azul"');
                }
            } else {
                propriedade.addClass('hidden').hide();
                propriedade.find('label').html('Propriedade');
                propriedade.find('input').val('');
            }
        }, 2000)


    });

    /**
     * INICIALIZAR CAMPO PROPRIEDADE (SEGMENTAÇÃO)
     */
    setTimeout(function() {
        if ($('#propriedade_select').find('option:selected').val() >= 1) {
            var propriedade_titulo = $('#propriedade_select').find('option:selected').text();
            propriedade.removeClass('hidden').show();
            propriedade.find('label').html(propriedade_titulo);
            if (propriedade_titulo == 'Sabor') {
                propriedade.find('input').attr('placeholder', 'Ex.: "Morango" ou "Chocolate"');
            } else if (propriedade_titulo == 'Cor') {
                propriedade.find('input').attr('placeholder', 'Ex.: "Vermelho" ou "Azul"');
            }
        } else {
            propriedade.addClass('hidden').hide();
            propriedade.find('label').html('Propriedade');
            //propriedade.find('input').val('');
        }
    }, 500);

    /**
     * FANCYBOX
     */
    $('.fancybox').fancybox();

    $('.summernote').summernote({
        height: 300, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: true, // set focus to editable area after initializing summernote
        lang: 'pt-BR',
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            //['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['table', 'link', 'hr']],
            //['height', ['height']]
        ]
    });

    $('#summernote').on('keyup blur click keypress', function() {
        $('#summernote-input').val($('#summernote-text').summernote('code'));
    });

    $('#summernote-dicas').on('keyup blur click keypress', function() {
        $('#summernote-dicas-input').val($('#summernote-dicas-text').summernote('code'));
    });

    $('#summernote-como_tomar').on('keyup blur click keypress', function() {
        $('#summernote-como_tomar-input').val($('#summernote-como_tomar-text').summernote('code'));
    });

    $('#summernote-ingredientes').on('keyup blur click keypress', function() {
        $('#summernote-ingredientes-input').val($('#summernote-ingredientes-text').summernote('code'));
    });

    $('#summernote-beneficios').on('keyup blur click keypress', function() {
        $('#summernote-beneficios-input').val($('#summernote-beneficios-text').summernote('code'));
    });

    $('#summernote-observacao-acad').on('keyup blur click keypress', function() {
        $('#summernote-observacao-acad-input').val($('#summernote-observacao-acad-text').summernote('code'));
    });

    $('.porcentagem-custo').on('keyup change blur', function() {
        var pid = $(this).attr('data-id');

        var custo = $('#custo-' + pid).val().replace('.', '').replace(',', '.');
        var porcentagem = $('#porcentagem-' + pid).val().replace('.', '').replace(',', '.');
        //var preco       = $('#preco-' + pid).val().replace('.', '').replace(',', '.');

        if (porcentagem == '' || custo == '') {
            $('#preco-' + pid).val('0,00').effect('highlight', {}, 1200, {});
        } else {
            var preco = ((parseFloat(custo) * parseFloat(porcentagem)) / 100) + parseFloat(custo);
            $('#preco-' + pid).val(preco.toFixed(2).replace('.', ',')).effect('highlight', {}, 1200, {});
        }

    });

    $('.preco-porcentagem').on('keyup change blur', function() {
        var pid = $(this).attr('data-id');

        var custo = $('#custo-' + pid).val().replace('.', '').replace(',', '.');
        //var porcentagem = $('#porcentagem-' + pid).val().replace('.', '').replace(',', '.');
        var preco = $('#preco-' + pid).val().replace('.', '').replace(',', '.');

        if (preco == '' || custo == '') {
            $('#custo-' + pid).val('0,00').effect('highlight', {}, 1200, {});
        } else {
            var porcentagem = ((parseFloat(preco) / parseFloat(custo) - 1) * 100);
            $('#porcentagem-' + pid).val(porcentagem.toFixed(2).replace('.', ',')).effect('highlight', {}, 1200, {});
        }
    });

    $('.datetimepicker').datetimepicker({
        locale: 'pt-br',
        icons: {
            time: "glyph-icon icon-calendar",
            date: "glyph-icon icon-calendar",
            up: "glyph-icon icon-arrow-up",
            down: "glyph-icon icon-arrow-down"
        }
    });


    $('input').on('blur', function() {

        /* $(this).removeClass('validation_error');

         var field_id = $(this).attr('id');
         var div_error = $('.' + field_id + 'formError');

         console.log(div_error.length);

         if (div_error.length == 0) {
         $(this).addClass('validation_error');
         } else {
         $(this).removeClass('validation_error');
         }*/
    });

    var notification_esportes = $('#notification-esportes');
    var notification_objetivos = $('#notification-objetivos');

    $('#notification-global').change(function() {
        if ($(this).val() == 1) {
            notification_esportes.slideUp(100);
            notification_objetivos.slideUp(100);
        } else {
            notification_esportes.slideDown(100);
            notification_objetivos.slideDown(100);
        }
    });


    /**
     * CEP - VIACEP
     */
    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#address").val("");
        $("#area").val("");
        //$("#city").val("");
        //$("#states").val("");
        //$("#ibge").val("");
    }

    /**
     * CEP - VIACEP
     */
    function limpa_formulário_cep_acad() {
        // Limpa valores do formulário de cep.
        $("#address-acad").val("");
        $("#area-acad").val("");
        //$("#city").val("");
        //$("#states").val("");
        //$("#ibge").val("");
    }

    //Quando o campo cep perde o foco.
    $("#cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');
        console.log(cep);

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#address").val("...");
                $("#area").val("...");
                //$("#city").val("...");
                //$("#states").val("...");
                $("#ibge").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        console.log(dados);
                        //Atualiza os campos com os valores da consulta.
                        $("#address").val(dados.logradouro);
                        $("#area").val(dados.bairro);

                        var uf = '';

                        switch (dados.uf) {
                            case 'AC':
                                uf = 1;
                                break;
                            case 'AL':
                                uf = 2;
                                break;
                            case 'AM':
                                uf = 3;
                                break;
                            case 'AP':
                                uf = 4;
                                break;
                            case 'BA':
                                uf = 5;
                                break;
                            case 'CE':
                                uf = 6;
                                break;
                            case 'DF':
                                uf = 7;
                                break;
                            case 'ES':
                                uf = 8;
                                break;
                            case 'GO':
                                uf = 9;
                                break;
                            case 'MA':
                                uf = 10;
                                break;
                            case 'MG':
                                uf = 11;
                                break;
                            case 'MS':
                                uf = 12;
                                break;
                            case 'MT':
                                uf = 13;
                                break;
                            case 'PA':
                                uf = 14;
                                break;
                            case 'PB':
                                uf = 15;
                                break;
                            case 'PE':
                                uf = 16;
                                break;
                            case 'PI':
                                uf = 17;
                                break;
                            case 'PR':
                                uf = 18;
                                break;
                            case 'RJ':
                                uf = 19;
                                break;
                            case 'RN':
                                uf = 20;
                                break;
                            case 'RO':
                                uf = 21;
                                break;
                            case 'RR':
                                uf = 22;
                                break;
                            case 'RS':
                                uf = 23;
                                break;
                            case 'SC':
                                uf = 24;
                                break;
                            case 'SE':
                                uf = 25;
                                break;
                            case 'SP':
                                uf = 26;
                                break;
                            case 'TO':
                                uf = 27;
                                break;
                            default:
                                uf = '';
                        }

                        /*$("#states").val(uf).trigger('chosen:updated');

                        var state_id = $('#states').val();
                        $.get(WEBROOT_URL + '/cidades/' + state_id,
                            function(data_options){
                                $('#city').html(data_options);
                            });

                        setTimeout(function(){
                            var $dd = $('#city');
                            var $options = $('option', $dd);
                            $options.each(function() {
                                if ($(this).text() == dados.localidade) {
                                    $(this).attr('selected', true).trigger('chosen:updated');
                                }
                            });
                        }, 700);*/

                        //$("#ibge").val(dados.ibge);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });

    //Quando o campo cep perde o foco.
    $("#cep-acad").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');
        console.log(cep);

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#address-acad").val("...");
                $("#area-acad").val("...");
                //$("#city").val("...");
                //$("#states").val("...");
                $("#ibge").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        console.log(dados);
                        //Atualiza os campos com os valores da consulta.
                        $("#address-acad").val(dados.logradouro);
                        $("#area-acad").val(dados.bairro);

                        var uf = '';

                        switch (dados.uf) {
                            case 'AC':
                                uf = 1;
                                break;
                            case 'AL':
                                uf = 2;
                                break;
                            case 'AM':
                                uf = 3;
                                break;
                            case 'AP':
                                uf = 4;
                                break;
                            case 'BA':
                                uf = 5;
                                break;
                            case 'CE':
                                uf = 6;
                                break;
                            case 'DF':
                                uf = 7;
                                break;
                            case 'ES':
                                uf = 8;
                                break;
                            case 'GO':
                                uf = 9;
                                break;
                            case 'MA':
                                uf = 10;
                                break;
                            case 'MG':
                                uf = 11;
                                break;
                            case 'MS':
                                uf = 12;
                                break;
                            case 'MT':
                                uf = 13;
                                break;
                            case 'PA':
                                uf = 14;
                                break;
                            case 'PB':
                                uf = 15;
                                break;
                            case 'PE':
                                uf = 16;
                                break;
                            case 'PI':
                                uf = 17;
                                break;
                            case 'PR':
                                uf = 18;
                                break;
                            case 'RJ':
                                uf = 19;
                                break;
                            case 'RN':
                                uf = 20;
                                break;
                            case 'RO':
                                uf = 21;
                                break;
                            case 'RR':
                                uf = 22;
                                break;
                            case 'RS':
                                uf = 23;
                                break;
                            case 'SC':
                                uf = 24;
                                break;
                            case 'SE':
                                uf = 25;
                                break;
                            case 'SP':
                                uf = 26;
                                break;
                            case 'TO':
                                uf = 27;
                                break;
                            default:
                                uf = '';
                        }

                        /*$("#states").val(uf).trigger('chosen:updated');

                        var state_id = $('#states').val();
                        $.get(WEBROOT_URL + '/cidades/' + state_id,
                            function(data_options){
                                $('#city').html(data_options);
                            });

                        setTimeout(function(){
                            var $dd = $('#city');
                            var $options = $('option', $dd);
                            $options.each(function() {
                                if ($(this).text() == dados.localidade) {
                                    $(this).attr('selected', true).trigger('chosen:updated');
                                }
                            });
                        }, 700);*/

                        //$("#ibge").val(dados.ibge);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep_acad();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep_acad();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep_acad();
        }
    });


    /**
     * CARREGAR CIDADES COM BASE NO ESTADO SELECIONADO
     */
    $('#states').on('change', function() {
        var state_id = $(this).val();
        $.get(WEBROOT_URL + '/cidades/' + state_id,
            function(data) {
                $('#city').html(data).trigger('chosen:updated');
            });
    });

    /**
     * CARREGAR CIDADES COM BASE NO ESTADO SELECIONADO
     */
    $('#states_acad').on('change', function() {
        var state_id = $(this).val();
        $.get(WEBROOT_URL + '/cidades/' + state_id,
            function(data) {
                $('#city_acad').html(data).trigger('chosen:updated');
            });
    });

    $('#states-indica').on('change', function() {
        loading.show(1);
        var state_id = $(this).val();
        $.get(WEBROOT_URL + '/cidades/' + state_id,
            function(data) {
                $('#city-indica').html(data).trigger('chosen:updated');
                loading.hide(1);
            });
    });

    /**
     * CARREGAR CIDADES QUE POSSUAM ACADEMIA COM BASE NO ESTADO SELECIONADO
     */
    $('#states_academias').on('change', function() {
        loading.show(1);
        var state_id = $(this).val();
        $.get(WEBROOT_URL + '/cidades/' + state_id + '/1',
            function(data) {
                $('#city').html(data).trigger('chosen:updated');
                loading.hide(1);
            });
    });

    /**
     * BUSCAR ACADEMIAS POR CIDADE
     */
    $('.cidade-academia').change(function() {
        loading.show(1);
        $.get(WEBROOT_URL + 'academias/busca-cidade/0/' + $(this).val(),
            function(data) {
                $('#select-academia').html(data).trigger('chosen:updated');
                loading.hide(1);
            });
    });

    /**
     * VALIDAÇÃO FAIXA DE PARCELAMENTO
     */
    $('body')
        /**
         * PARCELAMENTO FINAL
         */
        .on('blur', 'input.parcelamento-final', function(f) {

            var valor_inicial = parseFloat($(this).closest('div.form-group').find('input.parcelamento-inicial').val());

            if (!$(this).validationEngine('validate')) {
                if (parseFloat($(this).val()) <= valor_inicial) {
                    $(this)
                        .validationEngine('showPrompt', 'Valor deve ser maior que ' + valor_inicial, 'load')
                        .focus()
                        .select()
                        .addClass('validation_error');
                } else {
                    $(this)
                        .validationEngine('hide')
                        .removeClass('validation_error');
                }
            }
        })
        /**
         * PARCELAMENTO INICIAL
         */
        .on('blur', 'input.parcelamento-inicial', function(f) {

            var faixa_anterior = $(this).closest('div.form-group').attr('id').replace('faixa-', '');

            var div_anterior = $('#faixa-' + (faixa_anterior - 1));

            if (div_anterior.length > 0) {

                var valor_final = parseFloat(div_anterior.find('.parcelamento-final').val());

                if (!$(this).validationEngine('validate')) {
                    if (valor_final >= parseFloat($(this).val())) {
                        $(this)
                            .validationEngine('showPrompt', 'Valor deve ser maior que ' + valor_final, 'load')
                            .focus()
                            .select()
                            .addClass('validation_error');
                    } else {
                        $(this)
                            .validationEngine('hide')
                            .removeClass('validation_error');
                    }
                }
            }
        });

    /**
     * ADICIONAR / REMOVER FAIXA DE PARCELAMENTO
     * @type {any}
     * ADD
     */
    var add_faixa_parcelamento = $('#add-faixa-parcelamento');
    var faixas = add_faixa_parcelamento.attr('data-faixa');

    var delete_faixa_parcelamento = $('#delete-faixa-parcelamento');
    var Faixas = delete_faixa_parcelamento.attr('data-faixa');

    add_faixa_parcelamento.click(function() {
        /*$('.parcelamento-inicial').trigger('blur');
         $('.parcelamento-final').trigger('blur');*/
        if ($('#form-config-parcelamento').validationEngine('validate')) {
            faixas++;
            var faixa_html = $('#faixa-parcelamento').clone().html();

            faixa_html = faixa_html.replace(/9999/g, faixas);
            $('#faixas-parcelamento').append(faixa_html);

            Faixas++;
            delete_faixa_parcelamento.show(1);
            delete_faixa_parcelamento.attr('data-faixa', Faixas);
        }
    });

    /**
     * REMOVER FAIXA DE PARCELAMENTO
     * @type {any}
     */
    delete_faixa_parcelamento.click(function() {

        $('#faixa-' + Faixas).remove();

        Faixas--;
        faixas--;

        $(this).attr('data-faixa', Faixas);
        add_faixa_parcelamento.attr('data-faixa', Faixas);

        if (Faixas <= 0) {
            $(this).hide(1);
        }
    });

    /**
     * REMOVER PRODUTO
     */
    $('body').on('click', 'button#delete-produto', function() {
        var id = $(this).attr('data-id');

        console.log(id);

        if (id > 1) {
            $('#produto-' + id).remove();
        }
    });

    /**
     * ADICIONAR PRODUTO
     */
    $('#add-produto').click(function() {
        var id = $(this).attr('data-id');
        id++;
        $(this).attr('data-id', id);

        var produto = $('#add-9999').html().replace(/9999/g, id);
        $('#produtos').append(produto);
        $('#produto_id_' + id + '_chosen').remove();
        $('#produto-id-' + id).chosen().trigger('chosen:updated');

    });


    $('#academias-professor').change(function() {
        loading.show();

        $.get(WEBROOT_URL + 'alunos/filtro-academia-professor/' + $(this).val() + '/' + $(this).attr('data-pid'),
            function(data) {
                $('#alunos-professor').html(data).trigger('chosen:updated');
                loading.hide(1);
            });
    });

});