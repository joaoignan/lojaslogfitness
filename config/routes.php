<?php

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

//Router::defaultRouteClass('DashedRoute');
Router::defaultRouteClass('InflectedRoute');

Router::scope('/:slug/', function (RouteBuilder $routes) {

    $routes->connect('/rastreio-rapido/*',              ['controller' => 'Pages', 'action' => 'rastreio_rapido']);

    //Nova loja
    $routes->connect('/pesquisa/*',                     ['controller' => 'Produtos', 'action' => 'busca']);
    $routes->connect('/similares/*',                    ['controller' => 'Produtos', 'action' => 'similares']);
    $routes->connect('/produtos/atualizar-produtos/*',  ['controller' => 'Produtos', 'action' => 'atualizar_produtos']);
    $routes->connect('/produtos/filtrar/*',             ['controller' => 'Produtos', 'action' => 'filtrar']);
    $routes->connect('/produtos/comprar/*',             ['controller' => 'Produtos', 'action' => 'comprar']);
    

    $routes->connect('/comparador/completo/*',          ['controller' => 'Pages', 'action' => 'comparacao_completa']);

    /*$routes->connect('/',                             ['controller' => 'Pages', 'action' => 'new_home']);
    $routes->connect('/saiba-mais/*',                   ['controller' => 'Pages', 'action' => 'new_home2']);
    $routes->connect('/home/*',                         ['controller' => 'Pages', 'action' => 'home']);*/

    $routes->connect('/',                               ['controller' => 'Pages', 'action' => 'home']);
    $routes->connect('/home/*',                         ['controller' => 'Pages', 'action' => 'home']);
    $routes->connect('/iuguwebhooks/*',                 ['controller' => 'Pages', 'action' => 'iuguwebhooks']);
    $routes->connect('/iugu_subconta_verifica/*',       ['controller' => 'Pages', 'action' => 'iugu_subconta_verifica']);
    $routes->connect('/calculadora/*',                  ['controller' => 'Pages', 'action' => 'calculadora_log']);
    $routes->connect('/calculadora-envio/*',            ['controller' => 'Pages', 'action' => 'calculadora_envio']);
    $routes->connect('/mochila/pagamento/*',            ['controller' => 'Produtos', 'action' => 'carrinho_pagamento']);
    //$routes->connect('/academia-*/',                  ['controller' => 'Pages', 'action' => 'home']);
    $routes->connect('/pages/*',                        ['controller' => 'Pages', 'action' => 'display']);
    
    $routes->connect('/promocoes/atualizar-produtos/*', ['controller' => 'Produtos', 'action' => 'promocoes_atualizar_produtos']);

    // CATALOGO DE Produtos
    $routes->connect('/catalogo/',                  ['controller' => 'Produtos', 'action' => 'catalogo']);
    $routes->connect('/catalogo/atualizar-produtos/*', ['controller' => 'Produtos', 'action' => 'catalogo_atualizar_produtos']);
    $routes->connect('/montar-catalogo/',           ['controller' => 'Produtos', 'action' => 'montar_catalogo']);
    $routes->connect('/catalogo/preview',           ['controller' => 'Produtos', 'action' => 'catalogo_preview']);
    $routes->connect('/catalogo/preview2',          ['controller' => 'Produtos', 'action' => 'catalogo_preview_2']);
    $routes->connect('/teste_preview',              ['controller' => 'Produtos', 'action' => 'teste_preview']);

    // COMBO DE Produtos
    $routes->connect('/montar-combo/*',             ['controller' => 'Produtos', 'action' => 'montar_combo']);
    $routes->connect('/atualizar-produtos-combo/*', ['controller' => 'Produtos', 'action' => 'montar_combo_atualizar_produtos']);
    $routes->connect('/combo/produto/*',            ['controller' => 'Produtos', 'action' => 'view_combo']);
    $routes->connect('/selecionar-combo/*',         ['controller' => 'Produtos', 'action' => 'selecionar_tipo_combo']);
    $routes->connect('/salvar-combo/*',             ['controller' => 'Produtos', 'action' => 'salvar_combo']);
    $routes->connect('/fechar-combo/*',             ['controller' => 'Produtos', 'action' => 'fechar_combo']);


    /**
     * ADMIN ACADEMIA
     */

    $routes->connect('/academia-ajax/*',            ['controller' => 'Academias', 'action' => 'view_ajax']);
    
    /**
     * ACADEMIA MENSALIDADE
     */
    $routes->connect('/mensalidade/*',              ['controller' => 'Academias', 'action' => 'mensalidade']);
    $routes->connect('/mensalidades/',              ['controller' => 'Academias', 'action' => 'mensalidade']);
    $routes->connect('/mensalidade/comprar/*',      ['controller' => 'Academias', 'action' => 'mensalidade_comprar']);
    $routes->connect('/mensalidade/finalizado/*',   ['controller' => 'Academias', 'action' => 'mensalidade_finalizado']);


    //Allan
    $routes->connect('/solucionar_problemas',       ['controller' => 'Pages', 'action' => 'solucionar_problemas']);
    $routes->connect('/teste_allan',                ['controller' => 'Pages', 'action' => 'teste_default']);

    $routes->connect('/promocoes/*',                ['controller' => 'Produtos', 'action' => 'promocoes']);
    $routes->connect('/mais-vendidos/',             ['controller' => 'Produtos', 'action' => 'mais_vendidos']);
    


    $routes->connect('/academias/busca-cidade/*',    ['controller' => 'Academias', 'action' => 'ajax_options']);
    $routes->connect('/academias/busca-professor/*', ['controller' => 'Academias', 'action' => 'busca_professor']);

    $routes->connect('/atletas/',                   ['controller' => 'Atletas', 'action' => 'index']);

    $routes->connect('/transportadoras',            ['controller' => 'Transportadoras', 'action' => 'index']);
    $routes->connect('/central-de-relacionamento',  ['controller' => 'RelacionamentoContatos', 'action' => 'index']);
    $routes->connect('/seja-um-parceiro',           ['controller' => 'Fornecedores', 'action' => 'index']);

    $routes->connect('/produtos/atualizar-produtos-express/*', ['controller' => 'Produtos', 'action' => 'atualizar_produtos_express']);
    $routes->connect('/produtos/atualizar-produtos-indicacao/*', ['controller' => 'Produtos', 'action' => 'atualizar_produtos_indicacao']);

    $routes->connect('/produtos/overlay-indicar-produtos/*', ['controller' => 'Produtos', 'action' => 'overlay_indicar_produtos']);
    $routes->connect('/produtos/overlay-indicar-produtos-home/*', ['controller' => 'Produtos', 'action' => 'overlay_indicar_produtos_home']);
    $routes->connect('/produtos/produto-acabou/*', ['controller' => 'Produtos', 'action' => 'produto_acabou']);
    $routes->connect('/produtos/busca/*',                       ['controller' => 'Produtos', 'action' => 'search']);
    $routes->connect('/produtos/filtro-objetivo-categoria/*',   ['controller' => 'Produtos', 'action' => 'filter_objetivo_categoria']);
    $routes->connect('/produtos/busca-completa/*',   ['controller' => 'Produtos', 'action' => 'search_completo']);
    $routes->connect('/produtos/busca-professor/*',   ['controller' => 'Produtos', 'action' => 'search_professor']);
    $routes->connect('/produtos/busca-express/*',   ['controller' => 'Produtos', 'action' => 'search_express']);
    $routes->connect('/produtos/filtro-marca/*',                ['controller' => 'Produtos', 'action' => 'filter_marca']);

    $routes->connect('/promocao/*',                 ['controller' => 'Produtos', 'action' => 'view_promo']);
    $routes->connect('/produtos/comprar/*',         ['controller' => 'Produtos', 'action' => 'comprar']);
    $routes->connect('/produtos/comprar_ind/*',     ['controller' => 'Produtos', 'action' => 'comprar_ind']);
    $routes->connect('/produtos/comprar_express/*',             ['controller' => 'Produtos', 'action' => 'comprar_express']);
    $routes->connect('/produtos/comprar_indicar/*', ['controller' => 'Produtos', 'action' => 'comprar_indicar']);
    $routes->connect('/produto/*',                  ['controller' => 'Produtos', 'action' => 'view']);
    $routes->connect('/academia-*/produto/*',                  ['controller' => 'Produtos', 'action' => 'view']);

    $routes->connect('/mochila/checar-estoque/*',   ['controller' => 'Produtos', 'action' => 'carrinho_estoque']);
    $routes->connect('/mochila/alterar/*',          ['controller' => 'Produtos', 'action' => 'carrinho_edit']);
    $routes->connect('/mochila/fechar-o-ziper/*',   ['controller' => 'Produtos', 'action' => 'carrinho_identifica']);
    $routes->connect('/mochila/usuario-preencher-dados/*', ['controller' => 'Produtos', 'action' => 'carrinho_preencher_dados']);
    $routes->connect('/mochila/pagamento/*',        ['controller' => 'Produtos', 'action' => 'carrinho_pagamento']);
    $routes->connect('/mochila/checkout/*',         ['controller' => 'Produtos', 'action' => 'carrinho_checkout']);
    $routes->connect('/mochila/finalizado/*',       ['controller' => 'Produtos', 'action' => 'carrinho_finalizado']);
    $routes->connect('/mochila/finalizado-express/*',['controller' => 'Produtos', 'action' => 'carrinho_finalizado_express']);

    $routes->connect('/log_express_pagamento/*',['controller' => 'Produtos', 'action' => 'log_express_pagamento']);

    $routes->connect('/login/esqueci-a-senha/*',        ['controller' => 'Clientes', 'action' => 'forgot_passwd']);
    $routes->connect('/login/recuperar-a-senha/*',      ['controller' => 'Clientes', 'action' => 'recover_passwd']);
    $routes->connect('/fb-login/next/*',                ['controller' => 'Clientes', 'action' => 'fb_login_callback']);
    $routes->connect('/fb-login/*',                     ['controller' => 'Clientes', 'action' => 'fb_login']);
    $routes->connect('/login/*',                        ['controller' => 'Clientes', 'action' => 'login']);
    $routes->connect('/logout/*',                       ['controller' => 'Clientes', 'action' => 'logout']);
    $routes->connect('/minha-conta/dados-faturamento',  ['controller' => 'Clientes', 'action' => 'view_dados']);
    $routes->connect('/minha-conta/meus-dados',         ['controller' => 'Clientes', 'action' => 'view_meus_dados']);
    $routes->connect('/minha-conta/alterar-senha/*',    ['controller' => 'Clientes', 'action' => 'view_passwd']);
    $routes->connect('/minha-conta/mensalidades/',      ['controller' => 'Clientes', 'action' => 'view_mensalidades']);
    $routes->connect('/minha-conta/',                   ['controller' => 'Clientes', 'action' => 'view']);
    $routes->connect('/minha-academia/',                ['controller' => 'Clientes', 'action' => 'view_academia']);

    $routes->fallbacks('DashedRoute');

});

Router::scope('/', function (RouteBuilder $routes) {

    //api
    $routes->connect('/api/fechar-comissoes',           ['controller' => 'Apis', 'action' => 'api_fechar_comissoes']);
    $routes->connect('/api/buscar-produto/*',           ['controller' => 'Apis', 'action' => 'api_buscar_produto']);


    $routes->connect('/',                               ['controller' => 'Pages', 'action' => 'redireciona']);
    $routes->connect('/contar-click/*',                 ['controller' => 'Pages', 'action' => 'contar_click']);

    $routes->connect('/buscar-produtos-list/*',         ['controller' => 'Pages', 'action' => 'buscar_produtos_list']);
    $routes->connect('/salvar-mais-vendidos/*',         ['controller' => 'Pages', 'action' => 'salvar_mais_vendidos']);

    $routes->connect('/pagar/iugu/boleto/*',            ['controller' => 'Produtos', 'action' => 'pagar_iugu_boleto']);
    $routes->connect('/pagar/iugu/credito/*',           ['controller' => 'Produtos', 'action' => 'pagar_iugu_credito']);
    $routes->connect('/pagar/cielo/credito/*',          ['controller' => 'Produtos', 'action' => 'pagar_cielo_credito']);
    $routes->connect('/pagar/cielo/debito/*',           ['controller' => 'Produtos', 'action' => 'pagar_cielo_debito']);
    $routes->connect('/pagar/pagseguro/boleto/*',       ['controller' => 'Produtos', 'action' => 'pagar_pagseguro_boleto']);
    $routes->connect('/pagar/pagseguro/credito/*',      ['controller' => 'Produtos', 'action' => 'pagar_pagseguro_credito']);
    $routes->connect('/pagar/dinheiro/*',               ['controller' => 'Produtos', 'action' => 'pagar_dinheiro']);

    $routes->connect('/mochila/finalizado/debito/*',    ['controller' => 'Produtos', 'action' => 'carrinho_finalizado_debito']);

    $routes->connect('/atualizar/*',                    ['controller' => 'Pages', 'action' => 'atualizar']);
    $routes->connect('/atualizar/prazo-entrega/*',      ['controller' => 'Pages', 'action' => 'atualizar_prazo_entrega']);
    
    $routes->connect('/chatbot/buscar-galeria/*',       ['controller' => 'Pages', 'action' => 'chatbot_galeria']);
    $routes->connect('/chatbot/buscar-academias/*',     ['controller' => 'Pages', 'action' => 'chatbot_academias']);
    $routes->connect('/chatbot/rastrear-pedido/*',      ['controller' => 'Pages', 'action' => 'chatbot_rastrear_pedido']);
    $routes->connect('/chatbot/promocoes/*',            ['controller' => 'Pages', 'action' => 'chatbot_promocoes']);
    $routes->connect('/chatbot/cupom/*',                ['controller' => 'Pages', 'action' => 'chatbot_cupom']);
    
    $routes->connect('/sms/vendas/meio-mes/acad/*',     ['controller' => 'Pages', 'action' => 'sms_vendas_meio_mes_acad']);
    $routes->connect('/sms/vendas/meio-mes/prof/*',     ['controller' => 'Pages', 'action' => 'sms_vendas_meio_mes_prof']);
    $routes->connect('/sms/vendas/fim-mes/acad/*',      ['controller' => 'Pages', 'action' => 'sms_vendas_fim_mes_acad']);
    $routes->connect('/sms/vendas/fim-mes/prof/*',      ['controller' => 'Pages', 'action' => 'sms_vendas_fim_mes_prof']);
    $routes->connect('/sms/remarketing/pendentes/*',    ['controller' => 'Pages', 'action' => 'sms_remarketing_pendentes']);
    $routes->connect('/sms/academia/dados-bancarios-invalidos/*',['controller' => 'Pages', 'action' => 'sms_academia_dados_bancarios_invalidos']);
    $routes->connect('/sms/professor/dados-bancarios-invalidos/*',['controller' => 'Pages', 'action' => 'sms_professor_dados_bancarios_invalidos']);

    //Comparador
    $routes->connect('/comparador/*',                   ['controller' => 'Produtos', 'action' => 'comparador']);
    $routes->connect('/comparador/similares/*',         ['controller' => 'Produtos', 'action' => 'similares_comparador']);
    $routes->connect('/comparador/promocoes/*',         ['controller' => 'Produtos', 'action' => 'promocoes_comparador']);
    $routes->connect('/comparador/promocoes/atualizar-produtos/*',  ['controller' => 'Produtos', 'action' => 'promocoes_comparador_atualizar_produtos']);
    $routes->connect('/produtos/atualizar-produtos/*',  ['controller' => 'Produtos', 'action' => 'atualizar_produtos_comparador']);
    $routes->connect('/produtos/filtrar/*',             ['controller' => 'Produtos', 'action' => 'filtrar_comparador']);
    $routes->connect('/comparador/produto/*',           ['controller' => 'Produtos', 'action' => 'view_comparador']);
    $routes->connect('/comparador/adicionar-produto/*', ['controller' => 'Produtos', 'action' => 'adicionar_comparador_comparador']);
    $routes->connect('/comparador/remover-produto/*',   ['controller' => 'Produtos', 'action' => 'remover_comparador_comparador']);

    $routes->connect('/adicionar-produto-comparador/*', ['controller' => 'Produtos', 'action' => 'adicionar_comparador']);
    $routes->connect('/remover-produto-comparador/*',   ['controller' => 'Produtos', 'action' => 'remover_comparador']);

    $routes->connect('/comparador/completo/*',          ['controller' => 'Pages', 'action' => 'comparacao_completa_comparador']);
    $routes->connect('comparador/mais-vendidos/',       ['controller' => 'Produtos', 'action' => 'mais_vendidos_comparador']);

    /**
     * ACADEMIA MENSALIDADE
     */
    $routes->connect('/mensalidade/*',                  ['controller' => 'Academias', 'action' => 'mensalidade']);
    $routes->connect('/mensalidade/comprar/*',          ['controller' => 'Academias', 'action' => 'mensalidade_comprar']);
    $routes->connect('/mensalidade/finalizado/*',       ['controller' => 'Academias', 'action' => 'mensalidade_finalizado']);
    $routes->connect('/academias/verifica-professor-cpf/*', ['controller' => 'Academias', 'action' => 'verifica_professor_cpf']);
    $routes->connect('/academias/verifica-professor-email/*', ['controller' => 'Academias', 'action' => 'verifica_professor_email']);

    
    $routes->connect('/home/*',                         ['controller' => 'Pages', 'action' => 'home']);
    $routes->connect('/iuguwebhooks/*',                 ['controller' => 'Pages', 'action' => 'iuguwebhooks']);
    $routes->connect('/iugu_parcela_fatura_liberada/*', ['controller' => 'Pages', 'action' => 'iugu_parcela_fatura_liberada']);
    $routes->connect('/iugu_renovacao_assinatura/*',    ['controller' => 'Pages', 'action' => 'iugu_renovacao_assinatura']);
    $routes->connect('/iugu_cobranca_assinatura/*',     ['controller' => 'Pages', 'action' => 'iugu_cobranca_assinatura']);
    $routes->connect('/iugu_subconta_verifica/*',       ['controller' => 'Pages', 'action' => 'iugu_subconta_verifica']);
    $routes->connect('/pagsegurowebhooks/*',            ['controller' => 'Pages', 'action' => 'pagsegurowebhooks']);
    $routes->connect('/calculadora/*',                  ['controller' => 'Pages', 'action' => 'calculadora_log']);
    $routes->connect('/calculadora-envio/*',            ['controller' => 'Pages', 'action' => 'calculadora_envio']);
    $routes->connect('/ebook/*',                        ['controller' => 'Pages', 'action' => 'ebook_download']);
    $routes->connect('/mochila/pagamento/*',            ['controller' => 'Produtos', 'action' => 'carrinho_pagamento']);
    //$routes->connect('/academia-*/', ['controller' => 'Pages', 'action' => 'home']);
    $routes->connect('/pages/*',                        ['controller' => 'Pages', 'action' => 'display']);
    $routes->connect('/politica-de-privacidade',        ['controller' => 'Pages', 'action' => 'politica_de_privacidade']);

    /**
     * ADMIN ACADEMIA
     */
    $routes->connect('/loja/admin',                     ['controller' => 'Academias', 'action' => 'login']);
    $routes->connect('/loja/acesso/*',                  ['controller' => 'Academias', 'action' => 'login']);
    $routes->connect('/loja/login/*',                   ['controller' => 'Academias', 'action' => 'login']);
    $routes->connect('/loja/logout',                    ['controller' => 'Academias', 'action' => 'logout']);
    $routes->connect('/loja/esqueci-a-senha/*',         ['controller' => 'Academias', 'action' => 'forgot_passwd']);
    $routes->connect('/loja/recover_password/*',        ['controller' => 'Academias', 'action' => 'recover_passwd']);
    $routes->connect('/loja/admin/dashboard/*',         ['controller' => 'Academias', 'action' => 'admin_index']);
    $routes->connect('/loja/admin/express/*',           ['controller' => 'Academias', 'action' => 'admin_express']);
    $routes->connect('/loja/admin/mudar-academia/*',    ['controller' => 'Academias', 'action' => 'admin_mudar_academia']);
    $routes->connect('/loja/admin/passwd/*',            ['controller' => 'Academias', 'action' => 'passwd']);
    $routes->connect('/loja/admin/meus-dados/*',        ['controller' => 'Academias', 'action' => 'admin_dados']);
    $routes->connect('/loja/admin/dados-bancarios/*',   ['controller' => 'Academias', 'action' => 'admin_dados_bancarios']);
    $routes->connect('/loja/admin/dados-bancarios/salvar-cnpj/*',['controller' => 'Academias', 'action' => 'salvar_dados_bancarios_cnpj']);
    $routes->connect('/loja/admin/dados-bancarios/salvar-cpf/*',['controller' => 'Academias', 'action' => 'salvar_dados_bancarios_cpf']);
    $routes->connect('/loja/admin/comissoes/*',         ['controller' => 'Academias', 'action' => 'admin_comissoes_index']);
    $routes->connect('/loja/admin/pedidos',             ['controller' => 'Academias', 'action' => 'admin_pedidos']);
    $routes->connect('/loja/admin/alunos',              ['controller' => 'Academias', 'action' => 'admin_alunos']);
    $routes->connect('/loja/admin/alunos/convidar',     ['controller' => 'Academias', 'action' => 'admin_alunos_convidar']);
    $routes->connect('/loja/admin/alunos/convidar_cpf', ['controller' => 'Academias', 'action' => 'admin_alunos_convidar_cpf']);
    $routes->connect('/loja/admin/alunos/view/*',       ['controller' => 'Academias', 'action' => 'admin_aluno_detalhes']);
    $routes->connect('/loja/admin/pedido/excluir_pedido/*', ['controller' => 'Academias', 'action' => 'excluir_pedido']);
    $routes->connect('/loja/admin/pedido/*',            ['controller' => 'Academias', 'action' => 'admin_pedido_view']);
    $routes->connect('/loja/admin/comissao/*',          ['controller' => 'Academias', 'action' => 'admin_comissao']);
    $routes->connect('/loja/admin/time/*',              ['controller' => 'Academias', 'action' => 'admin_professores']);
    $routes->connect('/loja/admin/time/convidar/*',     ['controller' => 'Academias', 'action' => 'admin_professores_convidar']);
    $routes->connect('/loja/admin/aluno/indicacoes-de-produtos/*',
                                                        ['controller' => 'Academias', 'action' => 'admin_indicacoes_produtos']);
    $routes->connect('/loja/admin/time/edit/*',         ['controller' => 'Academias', 'action' => 'admin_edit_professores']);
    $routes->connect('/loja/admin/time/add/*',          ['controller' => 'Academias', 'action' => 'admin_add_professores']);
    $routes->connect('/loja/admin/time/view/*',         ['controller' => 'Academias', 'action' => 'admin_professores_view']);
    $routes->connect('/loja/admin/relatorios/professor/*', ['controller' => 'Academias', 'action' => 'admin_relatorios_professor']);    
    $routes->connect('/loja/admin/express-fechar/*',    ['controller' => 'Academias', 'action' => 'admin_express_fechar']);
    $routes->connect('/loja/admin/express/login/*',     ['controller' => 'Academias', 'action' => 'admin_express_login']);

    /**
     * ADMIN PROFESSOR
     */
    $routes->connect('/time/admin',                     ['controller' => 'Professores', 'action' => 'login']);
    $routes->connect('/time/acesso',                    ['controller' => 'Professores', 'action' => 'login']);
    $routes->connect('/time/login',                     ['controller' => 'Professores', 'action' => 'login']);
    $routes->connect('/time/logout',                    ['controller' => 'Professores', 'action' => 'logout']);
    $routes->connect('/time/esqueci-a-senha/*',         ['controller' => 'Professores', 'action' => 'forgot_passwd']);
    $routes->connect('/time/recover_password/*',        ['controller' => 'Professores', 'action' => 'recover_passwd']);
    $routes->connect('/time/admin/dashboard/*',         ['controller' => 'Professores', 'action' => 'admin_index']);
    $routes->connect('/time/admin/passwd/*',            ['controller' => 'Professores', 'action' => 'passwd']);
    $routes->connect('/time/admin/meus-dados/*',        ['controller' => 'Professores', 'action' => 'admin_dados']);
    $routes->connect('/time/admin/dados-bancarios/*',   ['controller' => 'Professores', 'action' => 'admin_dados_bancarios']);
    $routes->connect('/time/admin/dados-bancarios/salvar-cnpj/*',['controller' => 'Professores', 'action' => 'salvar_dados_bancarios_cnpj']);
    $routes->connect('/time/admin/dados-bancarios/salvar-cpf/*',['controller' => 'Professores', 'action' => 'salvar_dados_bancarios_cpf']);
    $routes->connect('/time/admin/alunos',              ['controller' => 'Professores', 'action' => 'admin_alunos']);
    $routes->connect('/time/admin/alunos/convidar',     ['controller' => 'Professores', 'action' => 'admin_alunos_convidar']);
    $routes->connect('/time/admin/alunos/convidar_cpf', ['controller' => 'Professores', 'action' => 'admin_alunos_convidar_cpf']);
    $routes->connect('/time-indicacao/*',               ['controller' => 'Pages', 'action' => 'professor_indicacao']);
    $routes->connect('/time/admin/aluno/indicacoes-de-produtos/*',['controller' => 'Professores', 'action' => 'admin_indicacoes_produtos']);
    $routes->connect('/time/admin/aluno/indicar-produtos/*',['controller' => 'Professores', 'action' => 'admin_indicar_produtos']);
    $routes->connect('/time/listar-time/*',   ['controller' => 'Professores', 'action' => 'listar']);
    $routes->connect('/time/admin/pedidos',             ['controller' => 'Professores', 'action' => 'admin_pedidos']);
    $routes->connect('/time/admin/pedido/*',            ['controller' => 'Professores', 'action' => 'admin_pedido_view']);
    $routes->connect('/time/admin/comissoes/*',         ['controller' => 'Professores', 'action' => 'admin_comissoes_index']);
    $routes->connect('/time/admin/comissao/*',          ['controller' => 'Professores', 'action' => 'admin_comissao']);
    $routes->connect('/time/admin/indicar-academia/*',  ['controller' => 'Professores', 'action' => 'indicar_academia']);
    $routes->connect('/time/admin/minhas-academias/*',  ['controller' => 'Professores', 'action' => 'minhas_academias']);
    $routes->connect('/time/admin/listar-minhas-academias/*',['controller' => 'Professores', 'action' => 'listar_minhas_academias']);
    $routes->connect('/time/admin/academia/edit/*',     ['controller' => 'Professores', 'action' => 'admin_edit_professores']);
    $routes->connect('/time/admin/academia/view/*',     ['controller' => 'Professores', 'action' => 'admin_view_academias']);
    $routes->connect('/time/admin/clientes/view/*',     ['controller' => 'Professores', 'action' => 'admin_aluno_detalhes']);
    $routes->connect('/time/admin/relatorios/academias/*', ['controller' => 'Professores', 'action' => 'admin_relatorios_academia']);

//    $routes->connect('/professores/admin/busca/*',                       ['controller' => 'Produtos', 'action' => 'search']);


    $routes->connect('/academia-ajax/*',                ['controller' => 'Academias', 'action' => 'view_ajax']);
    $routes->connect('/academias/busca-cidade/*',       ['controller' => 'Academias', 'action' => 'ajax_options']);
    $routes->connect('/academias/busca-professor/*',    ['controller' => 'Academias', 'action' => 'busca_professor']);
    $routes->connect('/cidades/*',                      ['controller' => 'Cities', 'action' => 'index']);
    $routes->connect('/academia/buscar/',               ['controller' => 'Pages', 'action' => 'encontre_sua_academia_busca']);
    $routes->connect('/academia/buscar-nome/',          ['controller' => 'Pages', 'action' => 'encontre_sua_academia_busca_nome']);
    $routes->connect('/academia/mais/',                 ['controller' => 'Pages', 'action' => 'encontre_sua_academia_mais']);
    $routes->connect('/atletas/',                       ['controller' => 'Atletas', 'action' => 'index']);
    $routes->connect('/transportadoras',                ['controller' => 'Transportadoras', 'action' => 'index']);
    $routes->connect('/central-de-relacionamento',      ['controller' => 'RelacionamentoContatos', 'action' => 'index']);
    $routes->connect('/seja-um-parceiro',               ['controller' => 'Fornecedores', 'action' => 'index']);
    $routes->connect('/promocoes/atualizar-produtos/*', ['controller' => 'Produtos', 'action' => 'promocoes_atualizar_produtos']);
    $routes->connect('/produtos/atualizar-produtos/*',  ['controller' => 'Produtos', 'action' => 'atualizar_produtos']);
    $routes->connect('/produtos/atualizar-produtos-express/*', ['controller' => 'Produtos', 'action' => 'atualizar_produtos_express']);
    $routes->connect('/produtos/atualizar-produtos-indicacao/*', ['controller' => 'Produtos', 'action' => 'atualizar_produtos_indicacao']);

    $routes->connect('/produtos/limpar-filtro/*',       ['controller' => 'Produtos', 'action' => 'limpar_filtro']);
    $routes->connect('/produtos/limpar-filtro-express/*', ['controller' => 'Produtos', 'action' => 'limpar_filtro_express']);
    $routes->connect('/produtos/overlay-indicar-produtos/*', ['controller' => 'Produtos', 'action' => 'overlay_indicar_produtos']);
    $routes->connect('/produtos/overlay-indicar-produtos-home/*', ['controller' => 'Produtos', 'action' => 'overlay_indicar_produtos_home']);
    $routes->connect('/produtos/produto-acabou/*',      ['controller' => 'Produtos', 'action' => 'produto_acabou']);
    $routes->connect('/produtos/busca/*',               ['controller' => 'Produtos', 'action' => 'search']);
    $routes->connect('/produtos/busca-completa/*',      ['controller' => 'Produtos', 'action' => 'search_completo']);
    $routes->connect('/produtos/busca-professor/*',     ['controller' => 'Produtos', 'action' => 'search_professor']);
    $routes->connect('/produtos/busca-express/*',       ['controller' => 'Produtos', 'action' => 'search_express']);
    $routes->connect('/processar-imagens/*',            ['controller' => 'Produtos', 'action' => 'erp_uploadImages']);
    $routes->connect('/produtos/filtro-valor/*',        ['controller' => 'Produtos', 'action' => 'filter_valor']);
    $routes->connect('/produtos/filtro-ordenar/*',      ['controller' => 'Produtos', 'action' => 'filter_ordenar']);
    $routes->connect('/produtos/filtro-objetivo/*',     ['controller' => 'Produtos', 'action' => 'filter_objetivo']);
    $routes->connect('/produtos/filtro-objetivo-categoria/*',   ['controller' => 'Produtos', 'action' => 'filter_objetivo_categoria']);
    $routes->connect('/produtos/filtro-objetivo-categoria-ordenar/*',   ['controller' => 'Produtos', 'action' => 'filter_objetivo_categoria_ordenar']);
    $routes->connect('/produtos/filtro-ordenar-categoria-objetivo/*',   ['controller' => 'Produtos', 'action' => 'filter_ordenar_categoria_objetivo']);
    $routes->connect('/produtos/filtro-marca/*',        ['controller' => 'Produtos', 'action' => 'filter_marca']);

    $routes->connect('/promocao/*',                     ['controller' => 'Produtos', 'action' => 'view_promo']);
    $routes->connect('/produtos/comprar_ind/*',         ['controller' => 'Produtos', 'action' => 'comprar_ind']);
    $routes->connect('/produtos/comprar_express/*',     ['controller' => 'Produtos', 'action' => 'comprar_express']);
    $routes->connect('/produtos/comprar_indicar/*',     ['controller' => 'Produtos', 'action' => 'comprar_indicar']);
    $routes->connect('/produto/*',                      ['controller' => 'Produtos', 'action' => 'view']);
    $routes->connect('/produto_indicar/*',              ['controller' => 'Produtos', 'action' => 'view_indicacao']);
    $routes->connect('/produto_express/*',              ['controller' => 'Produtos', 'action' => 'view_express']);
    $routes->connect('/academia-*/produto/*',           ['controller' => 'Produtos', 'action' => 'view']);
    
    $routes->connect('/mochila/checar-estoque/*',       ['controller' => 'Produtos', 'action' => 'carrinho_estoque']);
    $routes->connect('/mochila/alterar/*',              ['controller' => 'Produtos', 'action' => 'carrinho_edit']);
    $routes->connect('/mochila/fechar-o-ziper/*',       ['controller' => 'Produtos', 'action' => 'carrinho_identifica']);
    $routes->connect('/mochila/pagamento/*',            ['controller' => 'Produtos', 'action' => 'carrinho_pagamento']);
    $routes->connect('/mochila/usuario-preencher-dados/*', ['controller' => 'Produtos', 'action' => 'carrinho_preencher_dados']);
    $routes->connect('/mochila/checkout/*',             ['controller' => 'Produtos', 'action' => 'carrinho_checkout']);
    $routes->connect('/mochila/finalizado/*',           ['controller' => 'Produtos', 'action' => 'carrinho_finalizado']);
    $routes->connect('/mochila/finalizado-express/*',   ['controller' => 'Produtos', 'action' => 'carrinho_finalizado_express']);
    $routes->connect('/log_express_pagamento/*',        ['controller' => 'Produtos', 'action' => 'log_express_pagamento']);
    $routes->connect('/mochila/recolocar-pedido/*',     ['controller' => 'Produtos', 'action' => 'recolocar_pedido']);
    $routes->connect('/mochila/incluir-indicacao/*',    ['controller' => 'Produtos', 'action' => 'carrinho_indicacao']);

    $routes->connect('/login/esqueci-a-senha/*',        ['controller' => 'Clientes', 'action' => 'forgot_passwd']);
    $routes->connect('/login/recuperar-a-senha/*',      ['controller' => 'Clientes', 'action' => 'recover_passwd']);
    $routes->connect('/fb-login/next/*',                ['controller' => 'Clientes', 'action' => 'fb_login_callback']);
    $routes->connect('/fb-login/*',                     ['controller' => 'Clientes', 'action' => 'fb_login']);
    $routes->connect('/login/*',                        ['controller' => 'Clientes', 'action' => 'login']);
    $routes->connect('/logout/*',                       ['controller' => 'Clientes', 'action' => 'logout']);
    $routes->connect('/minha-conta/indicacao-de-produtos',
                                                        ['controller' => 'Clientes', 'action' => 'view_indicacoes']);
    $routes->connect('/minha-conta/dados-faturamento',  ['controller' => 'Clientes', 'action' => 'view_dados']);
    $routes->connect('/minha-conta/meus-dados',         ['controller' => 'Clientes', 'action' => 'view_meus_dados']);
    $routes->connect('/minha-conta/alterar-senha/*',    ['controller' => 'Clientes', 'action' => 'view_passwd']);
    $routes->connect('/minha-conta/mensalidades/',      ['controller' => 'Clientes', 'action' => 'view_mensalidades']);
    $routes->connect('/minha-conta/',                   ['controller' => 'Clientes', 'action' => 'view']);
    $routes->connect('/minha-conta/minha-academia/',    ['controller' => 'Clientes', 'action' => 'view_academia']);
    $routes->connect('/avaliar-academia/',              ['controller' => 'Clientes', 'action' => 'avaliar_academia']);
    $routes->connect('/cancelar_plano/*',               ['controller' => 'Clientes', 'action' => 'cancelar_plano']);


    /**
     * AJAX
     */
    $routes->connect('/completar-cadastro/',            ['controller' => 'Clientes', 'action' => 'completar_cadastro']);
    $routes->connect('/enviar-erro',                    ['controller' => 'Pages', 'action' => 'enviaErro']);
    $routes->connect('/cidades-ajax/*',                 ['controller' => 'Cities', 'action' => 'index']);
    $routes->connect('/sanitize-string/*',              ['controller' => 'Functions', 'action' => 'sanitize_string']);
    $routes->connect('/carrinho/get_boleto/*',          ['controller' => 'Produtos', 'action' => 'carrinho_get_boleto']);
    $routes->connect('/pedido-set-professor/*',         ['controller' => 'Produtos', 'action' => 'pedido_set_professor']);
    $routes->connect('/clientes-add/*',                 ['controller' => 'Clientes', 'action' => 'add']);
    $routes->connect('/professor-add/*',                ['controller' => 'Professores', 'action' => 'add']);
    $routes->connect('/clientes-edit/*',                ['controller' => 'Clientes', 'action' => 'edit']);
    $routes->connect('/clientes/checkar-email/*',       ['controller' => 'Clientes', 'action' => 'check_email']);
    $routes->connect('/mochila/fechar-o-ziper/checkar-cpf/*',
                                                        ['controller' => 'Produtos', 'action' => 'check_cpf']);
    $routes->connect('/mochila/fechar-o-ziper/checkar-cpf-professor/*',
                                                        ['controller' => 'Produtos', 'action' => 'check_cpf_professor']);
    $routes->connect('/produtos/check-cupom/*',         ['controller' => 'Produtos', 'action' => 'check_cupom_desconto']);
    $routes->connect('/alunos/filtro-academia/*',       ['controller' => 'Clientes', 'action' => 'filtro_academia']);
    $routes->connect('/alunos/filtro-academia-professor/*',
                                                        ['controller' => 'Clientes', 'action' => 'filtro_academia_professor']);
    $routes->connect('/academias-add/*',                ['controller' => 'Academias', 'action' => 'add']);
    $routes->connect('/academia-cordinates-add/*',      ['controller' => 'Academias', 'action' => 'maps_cordinates_add']);
    $routes->connect('/academias-indicar/*',            ['controller' => 'AcademiasSugestoes', 'action' => 'add']);
    $routes->connect('/produto-comentarios-add/*',      ['controller' => 'ProdutoComentarios', 'action' => 'add']);
    $routes->connect('/transportadoras-add/*',          ['controller' => 'Transportadoras', 'action' => 'add']);
    $routes->connect('/relacionamento-contatos-add/*',  ['controller' => 'RelacionamentoContatos', 'action' => 'add']);
    $routes->connect('/fornecedores-add/*',             ['controller' => 'Fornecedores', 'action' => 'add']);
    $routes->connect('/buscar-aluno/*',                 ['controller' => 'Academias', 'action' => 'buscar_aluno']);
    $routes->connect('/buscar-aluno-email/*',           ['controller' => 'Academias', 'action' => 'buscar_aluno_email']);

    $routes->connect('perfil/cartao-autorizado/*',      ['controller' => 'Academias', 'action' => 'mensalidade_cartao_autorizado']);
    $routes->connect('perfil/cartao-recusado/*',        ['controller' => 'Academias', 'action' => 'mensalidade_cartao_recusado']);

    // IONIC
    $routes->connect('/listar-ionic/*',                 ['controller' => 'Academias', 'action' => 'listar_ionic']);

    // $routes->connect('/',                               ['controller' => 'Pages', 'action' => 'home_new']);
    $routes->connect('/overlay',                        ['controller' => 'Pages', 'action' => 'overlay']);
    $routes->connect('/entenda',                        ['controller' => 'Pages', 'action' => 'home_new']);
    $routes->connect('/arquivo',                        ['controller' => 'Pages', 'action' => 'arquivo']);
    $routes->connect('/academia/',                      ['controller' => 'Pages', 'action' => 'academia_entenda']);
    $routes->connect('/academias/',                     ['controller' => 'Pages', 'action' => 'academia_entenda']);
    $routes->connect('/professor/',                     ['controller' => 'Pages', 'action' => 'professor_entenda']);
    $routes->connect('/professores/',                   ['controller' => 'Pages', 'action' => 'professor_entenda']);
    $routes->connect('/professor/verifica-professor/*', ['controller' => 'Professores', 'action' => 'verifica_professor']);    
    $routes->connect('/saiba-mais/*',                   ['controller' => 'Pages', 'action' => 'academia_new']);
    $routes->connect('/professor/acesso',               ['controller' => 'Professores', 'action' => 'login']);
    $routes->connect('/minha-conta/meus-pedidos/*',     ['controller' => 'Clientes', 'action' => 'view_pedidos']);
    $routes->connect('/minha-conta/meus_pedidos/*',     ['controller' => 'Clientes', 'action' => 'view_pedidos']);
    $routes->connect('/fornecedor',                     ['controller' => 'Admin/Users', 'action' => 'login']);
    // $routes->connect('/franquia',                       ['controller' => 'Pages', 'action' => 'franquia_teste_ab']);
    $routes->connect('/franquia',                       ['controller' => 'Pages', 'action' => 'franquia3']);
    $routes->connect('/franquias',                      ['controller' => 'Pages', 'action' => 'franquias']);
    $routes->connect('/1franquia',                      ['controller' => 'Pages', 'action' => 'franquia']);
    $routes->connect('/2franquia',                      ['controller' => 'Pages', 'action' => 'franquia2']);
    $routes->connect('/3franquia',                      ['controller' => 'Pages', 'action' => 'franquia3']);
    $routes->connect('/franquia-envio',                 ['controller' => 'Pages', 'action' => 'franquia_envio']);
    $routes->connect('/franquia-envio2',                ['controller' => 'Pages', 'action' => 'franquia_envio2']);
    $routes->connect('/entenda_academia',               ['controller' => 'Pages', 'action' => 'entenda_academia']);
    $routes->connect('/indicar-academia',               ['controller' => 'Pages', 'action' => 'encontre_sua_academia_indicar']);

    //BLOG REDIRECT
    $routes->connect('/blog',                           ['controller' => 'Pages', 'action' => 'blog_redirect']);
    //PERFIL ACADEMIA
    $routes->connect('/perfil/*',                       ['controller' => 'Academias', 'action' => 'profile_acad']);
    $routes->connect('/perfil/comprar_pacote/*',        ['controller' => 'Academias', 'action' => 'comprar_pacote']);
    $routes->connect('/perfil/pagar_plano',             ['controller' => 'Academias', 'action' => 'pagar_plano']);
    $routes->connect('/perfil/pagar_plano_personalizado', ['controller' => 'Academias', 'action' => 'pagar_plano_personalizado']);
    $routes->connect('/perfil/pagar_assinatura',        ['controller' => 'Academias', 'action' => 'pagar_assinatura']);
    $routes->connect('/',                               ['controller' => 'Pages', 'action' => 'home_new']);
    $routes->connect('/buscar_academia',                ['controller' => 'Pages', 'action' => 'encontre_sua_academia']);

    //ERP
    $routes->connect('/processar-imagens/*',            ['controller' => 'Produtos', 'action' => 'erp_uploadImages']);

    Router::extensions('csv');

    //antigo 
    //$routes->connect('/academias',                  ['controller' => 'Academias', 'action' => 'index']);
    // $routes->connect('/mochila',                    ['controller' => 'Produtos', 'action' => 'carrinho']);



    //novoz
    


    $routes->fallbacks('DashedRoute');

});

Router::prefix('admin', function ($routes) {
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'index']);
    $routes->fallbacks('InflectedRoute');
});

Plugin::routes();