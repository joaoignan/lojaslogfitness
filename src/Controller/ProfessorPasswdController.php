<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProfessorPasswd Controller
 *
 * @property \App\Model\Table\ProfessorPasswdTable $ProfessorPasswd
 */
class ProfessorPasswdController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Professores']
        ];
        $professorPasswd = $this->paginate($this->ProfessorPasswd);

        $this->set(compact('professorPasswd'));
        $this->set('_serialize', ['professorPasswd']);
    }

    /**
     * View method
     *
     * @param string|null $id Professor Passwd id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $professorPasswd = $this->ProfessorPasswd->get($id, [
            'contain' => ['Professores']
        ]);

        $this->set('professorPasswd', $professorPasswd);
        $this->set('_serialize', ['professorPasswd']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $professorPasswd = $this->ProfessorPasswd->newEntity();
        if ($this->request->is('post')) {
            $professorPasswd = $this->ProfessorPasswd->patchEntity($professorPasswd, $this->request->data);
            if ($this->ProfessorPasswd->save($professorPasswd)) {
                $this->Flash->success(__('The professor passwd has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The professor passwd could not be saved. Please, try again.'));
            }
        }
        $professores = $this->ProfessorPasswd->Professores->find('list', ['limit' => 200]);
        $this->set(compact('professorPasswd', 'professores'));
        $this->set('_serialize', ['professorPasswd']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Professor Passwd id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $professorPasswd = $this->ProfessorPasswd->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $professorPasswd = $this->ProfessorPasswd->patchEntity($professorPasswd, $this->request->data);
            if ($this->ProfessorPasswd->save($professorPasswd)) {
                $this->Flash->success(__('The professor passwd has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The professor passwd could not be saved. Please, try again.'));
            }
        }
        $professores = $this->ProfessorPasswd->Professores->find('list', ['limit' => 200]);
        $this->set(compact('professorPasswd', 'professores'));
        $this->set('_serialize', ['professorPasswd']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Professor Passwd id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $professorPasswd = $this->ProfessorPasswd->get($id);
        if ($this->ProfessorPasswd->delete($professorPasswd)) {
            $this->Flash->success(__('The professor passwd has been deleted.'));
        } else {
            $this->Flash->error(__('The professor passwd could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
