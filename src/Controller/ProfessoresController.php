<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Symfony\Component\Console\Helper\Table;

/**
 * Professores Controller
 *
 * @property \App\Model\Table\ProfessoresTable $Professores
 */
class ProfessoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Cities', 'Status']
        ];
        $professores = $this->paginate($this->Professores);

        $this->set(compact('professores'));
        $this->set('_serialize', ['professores']);
    }

    /**
     * View method
     *
     * @param string|null $id Professore id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $professore = $this->Professores->get($id, [
            'contain' => ['Cities', 'Status']
        ]);

        $this->set('professore', $professore);
        $this->set('_serialize', ['professore']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    /*
    public function add()
    {

        $professor = $this->Professores->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['status_id'] = 1;
            $professor = $this->Professores->patchEntity($professor, $this->request->data);

            if ($this->Professores->save($professor)) {
                $this->Flash->success(__('Cadastro realizado com sucesso!'));
                return $this->redirect('/professores/cadastro-sucesso');
            } else {
                $this->Flash->error(__('Falha ao realizar o cadastro. Tente novamente...'));
            }
        }

        $states = TableRegistry::get('States')->find('list');

        $this->set(compact('professor', 'status', 'states'));
        $this->set('_serialize', ['professor']);

        $this->viewBuilder()->layout('default2');
    }
    */

    //home de professor
    public function professor_add()
    {
        $professor = $this->Professores->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['status_id'] = 1;
            if($this->request->data['password'] == $this->request->data['password_confirm']) {
                $this->request->data['password_confirm'] = 1;
                $confirmado = 1;
            } else {
                $confirmado = 0;
            }

            $this->request->data['mobile'] ? : $this->request->data['mobile'] = $this->request->data['phone'];

            $professor = $this->Professores->patchEntity($professor, $this->request->data);

            if($this->request->data['valid_name'] == null) {
                if($confirmado == 1) {
                    if ($this->Professores->save($professor)) {
                        echo 'Cadastro realizado com sucesso!';
                        //$this->Flash->success(__('Cadastro realizado com sucesso!'));
                        //$this->redirect('/professores/cadastro-sucesso');
                    } else {
                        //$this->Flash->error(__('Falha ao realizar o cadastro. Tente novamente...'));
                        echo 'Falha ao realizar o cadastro. Tente novamente...';
                    }
                } else {
                    echo 'Senhas não correspondem';
                }
            }
        }

        $states = TableRegistry::get('States')->find('list');

        $this->set(compact('professor', 'status', 'states'));
        $this->set('_serialize', ['professor']);

        $this->viewBuilder()->layout('default3');
    }

    public function add()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $Professor_Academias = TableRegistry::get('ProfessorAcademias');
            $Academias = TableRegistry::get('Academias');

            $professor = $this->Professores->newEntity();

            $cpf_verify = $this->Professores
                ->find('all')
                ->where(['Professores.cpf' => $this->request->data['cpf']])
                ->orWhere(['Professores.email' => $this->request->data['email']])
                ->first();

            $this->request->data['status_id'] = 1;
            $this->request->data['cpf'] = $this->request->data['cpf_professor'];

            $academia = $this->request->data['academia_id'];
            $this->request->data['academia_id'] = [];

            $this->request->data['mobile'] ? : $this->request->data['mobile'] = $this->request->data['phone'];

            $professor = $this->Professores->patchEntity($professor, $this->request->data);

            if($this->request->data['valid_name'] == null) {
                if($this->request->data['password'] == $this->request->data['password_confirm']) {
                    if(count($cpf_verify) < 1) {    
                        if ($professor_save = $this->Professores->save($professor)) {
                            
                            $professor_academias = $Professor_Academias->newEntity();

                            $professoracademias = $Professor_Academias
                                ->find('all')
                                ->where(['ProfessorAcademias.academia_id' => $academia])
                                ->andWhere(['ProfessorAcademias.professor_id' => $professor_save->id])
                                ->first();

                            if($professoracademias == null) {
                                $data = [
                                    'status_id'    => 1,
                                    'academia_id'  => $academia,
                                    'professor_id' => $professor_save->id
                                ];

                                $professor_academias = $Professor_Academias->patchEntity($professor_academias, $data);

                                if ($prof_save = $Professor_Academias->save($professor_academias))
                                {
                                    $academia_prof = $Academias
                                        ->find('all')
                                        ->where(['Academias.id' => $academia])
                                        ->first();

                                    Email::configTransport('professor', [
                                        'className' => 'Smtp',
                                        'host' => 'mail.logfitness.com.br',
                                        'port' => 587,
                                        'timeout' => 30,
                                        'username' => 'uhull@logfitness.com.br',
                                        'password' => 'Lvrt6174?',
                                        'client' => null,
                                        'tls' => null,
                                    ]);

                                    $email = new Email();

                                    $email
                                        ->transport('professor')
                                        ->template('novo_professor_com_academia')
                                        ->emailFormat('html')
                                        ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                                        ->to($professor_save->email)
                                        ->subject('LogFitness - Cadastro de Professor')
                                        ->set([
                                            'name'              => $professor_save->name,
                                            'academia'          => $academia_prof->shortname,
                                            'logo_academia'     => $academia_prof->image,
                                        ])
                                        ->send();

                                    $session = $this->request->session();
                                    $session->destroy('AdminProfessor');

                                    $this->Flash->success('Cadastro realizado com sucesso!');
                                    return $this->redirect('/professor/acesso');
                                } else {
                                    $this->Flash->error('Falha ao realizar cadastro! Tente novamente...');
                                    return $this->redirect('/professor');
                                }
                            }
                        } else {
                            $this->Flash->error('Falha ao realizar cadastro! Tente novamente...');
                            return $this->redirect('/professor');
                        }
                    } else {
                        $this->Flash->error('CPF ou Email já utilizado');
                        return $this->redirect('/professor');
                    }
                } else {
                    $this->Flash->error('Senhas não correspondem');
                    return $this->redirect('/professor');
                }
            }
        }

        return $this->redirect('/professor');

        $this->viewBuilder()->layout(false);
        $this->render(false);
    }

    /**
     * Edit method
     *
     * @param string|null $id Professore id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $professore = $this->Professores->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $professore = $this->Professores->patchEntity($professore, $this->request->data);
            if ($this->Professores->save($professore)) {
                $this->Flash->success(__('The professore has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The professore could not be saved. Please, try again.'));
            }
        }
        $cities = $this->Professores->Cities->find('list', ['limit' => 200]);
        $status = $this->Professores->Status->find('list', ['limit' => 200]);
        $this->set(compact('professore', 'cities', 'status'));
        $this->set('_serialize', ['professore']);
    }


    /*******************************************************************************************************************
     * ADMIN ACADEMIA
     ******************************************************************************************************************/

    public function beforeRender() {
        $session    = $this->request->session();
        $Professor   = $session->read('AdminProfessor');

        if($Professor) {
            $ProfessorAcademias = TableRegistry::get('ProfessorAcademias');
            $ProfessorClientes  = TableRegistry::get('Clientes');
            $ProfessorPedidos    = TableRegistry::get('Pedidos');

            $professores_navbar = $ProfessorAcademias
                ->find('all')
                ->contain(['Academias'])
                ->where(['ProfessorAcademias.professor_id' => $Professor->id])
                ->andWhere(['ProfessorAcademias.status_id' => 1])
                ->all();

            $clientes_navbar = $ProfessorClientes
                ->find('all')
                ->where(['Clientes.professor_id' => $Professor->id])
                ->andWhere(['Clientes.status_id' => 1])
                ->order(['Clientes.created' => 'desc'])
                ->all();

            $pedidos_navbar = $ProfessorPedidos
                ->find('all')
                ->contain(['Clientes'])
                ->where(['Pedidos.professor_id' => $Professor->id])
                ->andWhere(['Pedidos.pedido_status_id <>' => 8])
                ->order(['Pedidos.modified' => 'desc'])
                ->all();

            $this->set(compact(
                'professores_navbar',
                'clientes_navbar',
                'pedidos_navbar',
                'Professor'
            ));
        }
    }

    /**
     * PROFESSOR - LOGIN
     */
    public function login()
    {

        $session = $this->request->session();
        $Professor = $session->read('AdminProfessor');
        if ($Professor) {
            $this->redirect(['action' => 'admin_index']);
        }

        if ($this->request->is(['post', 'patch', 'put'])) {

            $this->request->data['email'] ? $email = $this->request->data['email'] : $email = '';

            if(strpos($email, '@')) {
                $professor = $this->Professores
                    ->find('all')
                    ->contain(['Cities'])
                    ->where(['Professores.email' => $email])
                    ->andWhere(['Professores.status_id' => 1])
                    ->first();
            } else if(strpos($email, '.')) {
                $professor = $this->Professores
                    ->find('all')
                    ->contain(['Cities'])
                    ->where(['Professores.cpf' => $email])
                    ->andWhere(['Professores.status_id' => 1])
                    ->first();
            } else {
                $parte_um     = substr($email, 0, 3);
                $parte_dois   = substr($email, 3, 3);
                $parte_tres   = substr($email, 6, 3);
                $parte_quatro = substr($email, 9, 2);

                $cpf = $parte_um.'.'.$parte_dois.'.'.$parte_tres.'-'.$parte_quatro;

                $professor = $this->Professores
                    ->find('all')
                    ->contain(['Cities'])
                    ->where(['Professores.cpf' => $cpf])
                    ->andWhere(['Professores.status_id' => 1])
                    ->first();
            }

            $hasher = new DefaultPasswordHasher();
            $verify = new DefaultPasswordHasher;
            if ($verify->check($this->request->data['password'], $professor->password)) {
                $session = $this->request->session();
                $session->write('AdminProfessor', $professor);

                $now = Time::now();

                $data = [
                    'last_access' => $now
                ];

                $professor = $this->Professores->patchEntity($professor, $data);

                $this->Professores->save($professor);

                $this->redirect(['action' => 'admin_index']);
            } else {
                $this->Flash->auth(__('Email/CNJP ou senha inválidos. Tente novamente...'));
            }
        }

    }

    /**
     * PROFESSOR - LOGOUT
     */
    public function logout()
    {
        $session = $this->request->session();
        $session->destroy('AdminProfessor');

        $this->redirect(['action' => 'login']);
    }

    /**
     * PROFESSOR - ALTERAR SENHA
     */
    public function passwd()
    {
        $session = $this->request->session();
        $Professor = $session->read('AdminProfessor');
        if (!$Professor) {
            $this->redirect('/professores/acesso/');
        }

        $professor = $this->Professores->get($Professor->id);

        if ($professor) {
            $this->set('professor', $professor);
            $this->set('_serialize', ['professor']);

            if ($this->request->is(['post', 'put', 'patch'])) {
                $hasher = new DefaultPasswordHasher();
                $verify = new DefaultPasswordHasher;
                if ($verify->check($this->request->data['old_password'], $professor->password)) {
                    if ($verify->check($this->request->data['confirm_new_password'],
                        $hasher->hash($this->request->data['password']))
                    ) {
                        $professor->password = $this->request->data['password'];
                        if ($this->Professores->save($professor)) {
                            $this->Flash->success('Senha alterada com sucesso!');
                            $this->redirect('/professores/admin/passwd');
                        } else {
                            $this->Flash->error('Falha ao alterar a senha :(');
                        }
                    }
                }
            }

        } else {
            $this->redirect('/professores/acesso');
        }

        $this->viewBuilder()->layout('admin_professor');
    }

    /**
     * PROFESSOR - DASHBOARD
     * @route :: /professores/admin/dashboard
     */
    public function admin_index()
    {

        $session   = $this->request->session();
        $Professor = $session->read('AdminProfessor');
        if (!$Professor) {
            $this->redirect('/professores/acesso/');
        }

        $ProfessorComissoes        = TableRegistry::get('professorComissoes');
        $ProfessorAcademias        = TableRegistry::get('ProfessorAcademias');
        $Academias                 = TableRegistry::get('Academias');
        $ProfessorClientes         = TableRegistry::get('Clientes');
        $ProfessorPedidos          = TableRegistry::get('Pedidos');
        $ProfessorComissoesPedidos = TableRegistry::get('ProfessorComissoesPedidos');
        $ProdutoObjetivos          = TableRegistry::get('ProdutoObjetivos');
        $Produtos                  = TableRegistry::get('Produtos');
        $max_date = new \DateTime(date('Y-m').'-01');

        $professor_comissao_pedidos = $ProfessorComissoesPedidos
            ->find('list', [
                'keyField'      => 'pedido_id',
                'valueField'    => 'pedido_id'
            ])
            ->toArray();

        if(empty($professor_comissao_pedidos)){
            $professor_comissao_pedidos = [0];
        }

        $professor_comissoes_pagas       = $ProfessorComissoes
            ->find('all')
            ->contain(['Professores'])
            ->where(['professorComissoes.paga' => 1])
            ->andWhere(['professorComissoes.professor_id' => $Professor->id])
            ->all();

        $professor_comissoes_pendentes   = $ProfessorPedidos
            ->find('all')
            ->where(['Pedidos.professor_id'              => $Professor->id])
            ->andWhere(['Pedidos.pedido_status_id >='   => 3])
            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
            ->andWhere(['Pedidos.id NOT IN'             => $professor_comissao_pedidos])
            ->all();

        $professor_comissoes_receber   = $ProfessorComissoes
            ->find('all')
            ->contain(['Professores'])
            ->where(['professorComissoes.paga' => 0])
            ->andWhere(['professorComissoes.professor_id' => $Professor->id])
            ->all();

        $professor_comissoes_button   = $ProfessorPedidos
            ->find('all')
            ->where(['Pedidos.professor_id'              => $Professor->id])
            ->andWhere(['Pedidos.created <'             => $max_date])
            ->andWhere(['Pedidos.pedido_status_id >='   => 3])
            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
            ->andWhere(['Pedidos.id NOT IN'             => $professor_comissao_pedidos])
            ->all();

        $professor_academias = $ProfessorAcademias
            ->find('all')
            ->contain(['Academias'])
            ->where(['ProfessorAcademias.professor_id' => $Professor->id])
            ->andWhere(['ProfessorAcademias.status_id' => 1])
            ->all();

        $professor_clientes = $ProfessorClientes
            ->find('all')
            ->where(['Clientes.professor_id' => $Professor->id])
            ->andWhere(['Clientes.status_id' => 1])
            ->order(['Clientes.created' => 'desc'])
            ->all();

        $professor_pedidos = $ProfessorPedidos
            ->find('all')
            ->contain(['Clientes'])
            ->where(['Pedidos.professor_id' => $Professor->id])
            ->andWhere(['Pedidos.pedido_status_id <>' => 8])
            ->order(['Pedidos.modified' => 'desc'])
            ->all();

        $professor = $this->Professores
            ->find('all')
            ->where(['Professores.id' => $Professor->id])
            ->limit(1)
            ->first();

        $produtos_novidades = $Produtos
            ->find('all', ['contain' => [
                'ProdutoBase'
            ]])
            ->where(['Produtos.status_id'       => 1])
            ->andWhere(['Produtos.visivel'      => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->distinct(['ProdutoBase.id'])
            ->order(['Produtos.created' => 'desc'])
            ->limit(5)
            ->all();

        foreach ($produtos_novidades as $pn) {
            $produtos_novidades_id[] = $pn->produto_base_id;
        }

        $produtos_novidades_objetivos = $ProdutoObjetivos
            ->find('all')
            ->contain(['Objetivos'])
            ->where(['ProdutoObjetivos.produto_base_id IN' => $produtos_novidades_id])
            ->all();

        $produtos_promocoes = $Produtos
            ->find('all', ['contain' => [
                'ProdutoBase'
            ]])
            ->where(['Produtos.status_id'          => 1])
            ->andWhere(['Produtos.visivel'         => 1])
            ->andWhere(['ProdutoBase.status_id'    => 1])
            ->andWhere(['Produtos.preco_promo >'   => 0])
            ->distinct(['ProdutoBase.id'])
            ->order(['rand()'])
            ->limit(5)
            ->all();

        if(!count($produtos_promocoes) == 0) {
            foreach ($produtos_promocoes as $pp) {
                $produtos_promocoes_id[] = $pp->produto_base_id;
            }

            $produtos_promocoes_objetivos = $ProdutoObjetivos
            ->find('all')
            ->contain(['Objetivos'])
            ->where(['ProdutoObjetivos.produto_base_id IN' => $produtos_promocoes_id])
            ->all();
        }

        $time = Time::now();

        $min_date = Time::now();
        $min_date->subYear(1);

        $pedidos_grafico = $ProfessorPedidos
            ->find('all')
            ->where(['Pedidos.professor_id' => $Professor->id])
            ->andWhere(['Pedidos.created >' => $min_date])
            ->andWhere(['Pedidos.pedido_status_id >=' => 3])
            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
            ->andWhere(['Pedidos.comissao_status' => 1])
            ->all();

        $data_grafico_vendas[0] = 0.0;
        $data_grafico_vendas[1] = 0.0;
        $data_grafico_vendas[2] = 0.0;
        $data_grafico_vendas[3] = 0.0;
        $data_grafico_vendas[4] = 0.0;
        $data_grafico_vendas[5] = 0.0;
        $data_grafico_vendas[6] = 0.0;
        $data_grafico_vendas[7] = 0.0;
        $data_grafico_vendas[8] = 0.0;
        $data_grafico_vendas[9] = 0.0;
        $data_grafico_vendas[10] = 0.0;
        $data_grafico_vendas[11] = 0.0;

        $ctrl = 0;
        for($i = 12; $i >= 1; $i--) {
            $data_grafico = Time::now();
            $data_grafico->subMonth($i);

            $data_grafico_mes[$ctrl] = $data_grafico->format('m-y');

            $ctrl++;
        }

        foreach ($pedidos_grafico as $key => $pedido_grafico) {
            for ($i=0; $i < 12; $i++) { 
                if($pedido_grafico->created->format('m-y') == $data_grafico_mes[$i]) {
                    $data_grafico_vendas[$i] += $pedido_grafico->valor;
                }
            }
        }

        if ($this->request->is(['post', 'put'])) {

            $session->write('AdminProfessor', $professor);

            $termos = $this->request->data['aceite_termos'];

            $professor = $this->Professores->patchEntity($professor, $this->request->data);

            if ($prof_salvo = $this->Professores->save($professor)) {
                if($termos == 1) {
                    $this->Flash->success('Dados atualizados com sucesso!');
                    $this->redirect('/professores/admin/dashboard');
                } else {
                    $status_id = ['status_id' => 3];
                    $prof_salvo = $this->Professores->patchEntity($prof_salvo, $status_id);

                    if($this->Professores->save($prof_salvo)) {
                        $session->destroy('AdminProfessor');
                        $this->Flash->error('Conta inativada! Para saber mais entre em contato..');
                        $this->redirect('/professor/acesso');
                    }
                }
            }
        }

        $this->set(compact(
            'professor_comissoes_pendentes',
            'professor_comissoes_receber',
            'professor_comissoes_pagas',
            'professor_comissoes_button',
            'professor_banners',
            'data_grafico_vendas',
            'professor_academias',
            'professor_clientes',
            'professor_pedidos',
            'produtos_novidades',
            'produtos_novidades_objetivos',
            'produtos_promocoes',
            'produtos_promocoes_objetivos',
            'academias_professor_id',
            'acad_relatorio_name', 
            'academia_prof_relatorio',
            'time'
        ));

        $this->viewBuilder()->layout('admin_professor');
    }

    /**
     * PROFESSOR - DADOS
     * @route :: /professores/admin/meus-dados
     */
    public function admin_dados()
    {
        $session = $this->request->session();
        $Professor = $session->read('AdminProfessor');
        if (!$Professor) {
            $this->redirect(['action' => 'login']);
        }

        $professor = $this->Professores
            ->find('all')
            ->contain(['Cities', 'Cities.States'])
            ->where(['Professores.id' => $Professor->id])
            ->limit(1)
            ->first();

        if ($this->request->is(['post', 'put'])) {

            if($this->request->data['imagem']['size'] == 0 && $this->request->data['imagem']['error'] == 1) {
                $this->Flash->error('Por favor, coloque uma imagem de até 2mb');
                $this->redirect(['action' => 'admin_dados']);
            } else {
                $professor = $this->Professores->patchEntity($professor, $this->request->data);

                if ($this->Professores->save($professor)) {
                    $session->write('AdminProfessor', $professor);

                    $this->Flash->success('Dados atualizados com sucesso!');
                    $this->redirect(['action' => 'admin_dados']);
                } else {
                    $this->Flash->error('Falha ao atualizar dados. Tente novamente...');
                }
            }
        }

        $cities = $this->Professores->Cities->find('list')->where(['Cities.state_id' => $professor->city->state_id]);
        $states = $this->Professores->Cities->States->find('list');

        $this->set(compact('professor', 'cities', 'states'));

        $this->viewBuilder()->layout('admin_professor');
    }

    /**
     * PROFESSOR - DADOS BANCÁRIOS
     * @route :: /professores/admin/dados-bancarios
     */
    public function admin_dados_bancarios($comissao = 0){
        $session    = $this->request->session();
        $Professor   = $session->read('AdminProfessor');
        if(!$Professor){
            $this->redirect(['action' => 'login']);
        }

        $professor = $this->Professores
            ->find('all')
            ->contain(['Cities', 'Cities.States'])
            ->where(['Professores.id' => $Professor->id])
            ->limit(1)
            ->first();

        if($comissao == 1) {
            $this->Flash->info('Preencha seus dados bancários para fechar suas comissões!');
        }

        if($this->request->is(['post', 'put'])){

            $professor = $this->Professores->patchEntity($professor, $this->request->data);

            if($this->Professores->save($professor)){
                $this->Flash->success('Dados bancários atualizados com sucesso!');
                $this->redirect(['action' => 'admin_dados_bancarios']);
            }else{
                $this->Flash->error('Falha ao atualizar dados bancários. Tente novamente...');
            }
        }

        $cities = $this->Professores->Cities->find('list')->where(['Cities.state_id' => $professor->city->state_id]);
        $states = $this->Professores->Cities->States->find('list');

        $this->set(compact('professor', 'states', 'cities', 'comissao'));

        $this->viewBuilder()->layout('admin_professor');
    }

    /**
     * ADMIN ACADEMIA - COMISSÕES
     * @route :: /professores/admin/comissoes
     */
    public function admin_comissoes_index(){

        $session    = $this->request->session();
        $Professor  = $session->read('AdminProfessor');
        if(!$Professor){
            $this->redirect(['action' => 'login']);
        }

        $professor = $this->Professores
            ->find('all')
            ->contain(['Cities', 'Cities.States'])
            ->where(['Professores.id' => $Professor->id])
            ->limit(1)
            ->first();

        $ProfessorComissoes = TableRegistry::get('ProfessorComissoes');

        $Pedidos           = TableRegistry::get('Pedidos');

        $AcademiaComissoesPedidos = TableRegistry::get('AcademiaComissoesPedidos');  

        $ProfessorComissoesPedidos = TableRegistry::get('ProfessorComissoesPedidos');  

        $academia_comissao_pedidos = $AcademiaComissoesPedidos
            ->find('list', [
                'keyField'      => 'pedido_id',
                'valueField'    => 'pedido_id'
            ])
            ->toArray();

        if(empty($academia_comissao_pedidos)){
            $academia_comissao_pedidos = [0];
        } 

        $professor_comissao_pedidos = $ProfessorComissoesPedidos
            ->find('list', [
                'keyField'      => 'pedido_id',
                'valueField'    => 'pedido_id'
            ])
            ->toArray();

        if(empty($professor_comissao_pedidos)){
            $professor_comissao_pedidos = [0];
        }       

        $max_date = new \DateTime(date('Y-m').'-01');

        $pedidos = $Pedidos
            ->find('all')
            ->contain([
                'Clientes',
                'Academias',
                'Academias.Cities',
                'PedidoStatus',
            ])
            //->where(['Pedidos.academia_id'              => $Academia->id])
            ->where(['Pedidos.professor_id'               => $Professor->id])
            ->andWhere(['Pedidos.created <'               => $max_date])
            ->andWhere(['Pedidos.comissao_status'         => 1])
            ->andWhere(['Pedidos.pedido_status_id >='     => 3])
            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
            //->andWhere(['Pedidos.id NOT IN'               => $professor_comissao_pedidos])
            ->order(['Pedidos.id' => 'desc'])
            ->all();

        $profComissoes = $ProfessorComissoes
            ->find('all')
            ->contain(['Professores'])
            ->where(['ProfessorComissoes.professor_id'         => $Professor->id])
            ->all();

        $this->paginate = [
            'contain' => ['Professores'],
            'conditions' => ['ProfessorComissoes.professor_id' => $Professor->id]
        ];
        $professorComissoes = $this->paginate($ProfessorComissoes);

        $this->set(compact('professor', 'profComissoes', 'pedidos', 'professorComissoes'));
        $this->set('_serialize', ['professorComissoes']);

        $this->viewBuilder()->layout('admin_professor');
    }

    /**
     * ADMIN ACADEMIA - ACEITE DE COMISSÕES
     * @param null $ano
     * @param null $mes
     * @route :: /professores/admin/comissao
     */
    public function admin_comissao($comissao_id){

        $session    = $this->request->session();
        $Professor   = $session->read('AdminProfessor');
        if(!$Professor){
            $this->redirect(['action' => 'login']);
        }

        $professor = $this->Professores
            ->find('all')
            ->contain(['Cities', 'Cities.States'])
            ->where(['Professores.id' => $Professor->id])
            ->limit(1)
            ->first();

        $Pedidos = TableRegistry::get('Pedidos');
        $ProfessorComissoes = TableRegistry::get('ProfessorComissoes');
        $ProfessorComissoesPedidos = TableRegistry::get('ProfessorComissoesPedidos');

        $comissao = $ProfessorComissoes
            ->find('all')
            ->where(['id' => $comissao_id])
            ->andWhere(['aceita' => 2])
            ->andWhere(['paga' => 0])
            ->first();

        if(!$comissao) {
            $this->redirect(['action' => 'admin_comissoes_index']);
        }

        $comissao_pedidos = $ProfessorComissoesPedidos
            ->find('all')
            ->where(['professor_comissao_id' => $comissao->id])
            ->all();

        foreach ($comissao_pedidos as $comissao_pedido) {
            $comissao_pedidos_id[] = $comissao_pedido->pedido_id;
        }

        $pedidos = $Pedidos
            ->find('all')
            ->contain([
                'Clientes',
                'Academias',
                'Professores',
                'Professores.Cities',
                'PedidoStatus',
            ])
            ->where(['Pedidos.professor_id' => $Professor->id])
            ->andWhere(['Pedidos.id IN' => $comissao_pedidos_id])
            ->order(['Pedidos.id' => 'desc'])
            ->all();

        if($this->request->is(['post', 'put'])){

            if($professor->is_cpf == 0) {
                $nome = $professor->razao_bancario;
                $cpf = $professor->cnpj_bancario;
            } else {
                $nome = $professor->favorecido;
                $cpf = $professor->cpf_favorecido;
            }

            $data = [
                'aceita' => $this->request->data['aceite'],
                'nome_titular' => $nome,
                'cpf_titular' => $cpf,
                'tipo_conta' => $professor->tipo_conta,
                'banco' => $professor->banco,
                'agencia' => $professor->agencia,
                'agencia_dv' => $professor->agencia_dv,
                'conta' => $professor->conta,
                'conta_dv' => $professor->conta_dv,
                'data_aceite' => Time::now()
            ];

            $comissao = $ProfessorComissoes->patchEntity($comissao, $data);

            if($professor_comissao = $ProfessorComissoes->save($comissao)){
                $this->Flash->success('Relatório enviado com sucesso!');
                $this->redirect(['action' => 'admin_comissoes_index']);
            }else{
                $this->Flash->error('Falha ao enviar relatório. Tente novamente...');
            }

        }

        $this->set(compact('professor', 'pedidos', 'ano', 'mes'));
        $this->set('_serialize', ['pedidos']);

        $this->viewBuilder()->layout('admin_professor');
    }


    /**
     * PROFESSOR - ALUNOS
     * @route :: /professores/admin/alunos
     */
    public function admin_alunos(){

        $session = $this->request->session();
        $Professor = $session->read('AdminProfessor');
        if (!$Professor) {
            $this->redirect(['action' => 'login']);
        }

        $Alunos = TableRegistry::get('Clientes');

        $produtos_sessao = $session->read('Carrinho');

        $this->paginate = [
            'contain' => ['Cities', 'Cities.States'],
            'order' => ['Clientes.name' => 'asc'],
            'conditions' => ['Clientes.status_id' => 1, 'Clientes.professor_id' => $Professor->id]
        ];

        $alunos = $this->paginate($Alunos);

        $this->set(compact('produtos_sessao', 'alunos'));

        $this->viewBuilder()->layout('admin_professor');
    }

    public function admin_indicacoes_produtos(){
        $session = $this->request->session();
        $Professor = $session->read('AdminProfessor');
        if (!$Professor) {
            $this->redirect('/professores/acesso/');
        }

        $PreOrders  = TableRegistry::get('PreOrders');

        $pre_orders = $PreOrders
            ->find('all')
            ->contain(['Clientes', 'Academias', 'PreOrderItems', 'PreOrderItems.Produtos', 'PreOrderItems.Produtos.ProdutoBase'])
            ->where(['PreOrders.professor_id' => $Professor->id])
            ->order(['PreOrders.pedido_id' => 'desc', 'PreOrders.cliente_id' => 'asc'])
        ;

        $pre_orders = $this->paginate($pre_orders);

        $this->set(compact('pre_orders'));

        $this->viewBuilder()->layout('admin_professor');
    }



    /**
     * ADMIN PROFESSOR - INDICAR PRODUTOS AO ALUNO
     * @param null $aluno_id
     */
    public function admin_indicar_produtos($aluno_id = null){
        $session = $this->request->session();
        $Professor = $session->read('AdminProfessor');
        if (!$Professor) {
            $this->redirect('/professores/acesso/');
        }
        if (isset($_GET['indicar'])) {
            $session->delete('Carrinho');
            $this->redirect('/professor-indicacao/');
        }
        $Alunos     = TableRegistry::get('Clientes');
        $Academias  = TableRegistry::get('Academias');
        $Produtos   = TableRegistry::get('Produtos');

        $produtos_sessao = $session->read('Carrinho');

        $produtos_sessao_id = [];
        if(!empty($produtos_sessao)) {
            foreach ($produtos_sessao as $key => $item) {
                $produtos_sessao_id[] = $key;
            }
        }

        if(!empty($produtos_sessao_id)) {
            $produtos_ind = $Produtos
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->where(['Produtos.id IN' => $produtos_sessao_id])
                ->all();
        }else{
            $produtos_ind = [];
        }

        /**
         * PREPARAR PARA SALVAR INDICAÇÃO
         */
        if($this->request->is(['post'])){

            if(isset($this->request->data['name'])) {

                $aluno = $Alunos
                    ->find('all')
                    ->where(['Clientes.email' => $this->request->data['email']])
                    ->first();

                if(count($aluno) == 0) {
                    $cliente = $Alunos->newEntity();

                    $academia = $Academias
                        ->find('all')
                        ->where(['Academias.id' => $this->request->data['academia_id']])
                        ->first();

                    function randString($size){
                        $basic = '0123456789';

                        $return= "";

                        for($count= 0; $size > $count; $count++){
                            $return.= $basic[rand(0, strlen($basic) - 1)];
                        }

                        return $return;
                    }

                    $senha = randString(8);

                    $this->request->data['status_id']   = 1;
                    $this->request->data['password']   = $senha;
                    $this->request->data['professor_id']   = $Professor->id;
                    $this->request->data['city_id']   = $academia->city_id;

                    $cliente = $Alunos->patchEntity($cliente, $this->request->data);

                    if ($CLIENTE = $Alunos->save($cliente)) {
                        $PreOrders = TableRegistry::get('PreOrders');

                        $pre_order_data = [
                            'professor_id'  => $Professor->id,
                            'academia_id'   => $this->request->data['academia_id'],
                            'cliente_id'    => $CLIENTE->id,
                        ];
                        $pre_order = $PreOrders->newEntity();
                        $pre_order = $PreOrders->patchEntity($pre_order, $pre_order_data);

                        if($pre_order_save = $PreOrders->save($pre_order)):

                            $aluno = $Alunos
                                ->find('all')
                                ->where(['id' => $CLIENTE->id])
                                ->first();

                            $PreOrderItems = TableRegistry::get('PreOrderItems');

                            foreach ($produtos_sessao as $key => $produto):
                                if(!empty($key) && !empty($produto)):
                                    $items[$key] = [
                                        'pre_order_id'  =>  $pre_order_save->id,
                                        'produto_id'    =>  $key,
                                        'quantidade'    =>  $produto,
                                    ];
                                endif;
                            endforeach;

                            if(!empty($items)):
                                $pre_order_items = $PreOrderItems->newEntities($items);
                                if($PreOrderItems->saveMany($pre_order_items)):

                                    $Notifications = TableRegistry::get('Notifications');

                                    $notification = $Notifications->newEntity();
                                    $notification = $Notifications->patchEntity($notification, [
                                        'cliente_id'    => $CLIENTE->id,
                                        'name'          => 'Indicação de Produto(s)',
                                        'description'   => 'O professor <strong>'.$Professor->name.'</strong> lhe enviou uma indicação 
                                                            de produto(s).<br>Clique aqui para visualizar',
                                        'link'          =>  WEBROOT_URL.'minha-conta/indicacao-de-produtos',
                                        'global'        =>  0,
                                        'status_id'     =>  1,
                                    ]);

                                    $notify_message = '';
                                    if($Notifications->save($notification)):
                                        $notify_message = '<br> O mesmo já foi notificado';
                                    else:
                                        $notify_message = '<br> Houve uma falha ao notificá-lo :/';
                                    endif;

                                    Email::configTransport('cliente', [
                                        'className' => 'Smtp',
                                        'host' => 'mail.logfitness.com.br',
                                        'port' => 587,
                                        'timeout' => 30,
                                        'username' => 'sos@logfitness.com.br',
                                        'password' => 'Lvrt6174?',
                                        'client' => null,
                                        'tls' => null,
                                    ]);

                                    $email = new Email();

                                    $email
                                        ->transport('cliente')
                                        ->template('aluno_pre_order_email')
                                        ->emailFormat('html')
                                        ->from(['sos@logfitness.com.br' => 'LogFitness'])
                                        ->to($CLIENTE->email)
                                        ->subject('LogFitness - Indicação de Produto')
                                        ->set([
                                            'name'              => $CLIENTE->name,
                                            'professor'         => $Professor->name,
                                            'academia'          => $academia->shortname,
                                            'logo_academia'     => $academia->image,
                                            'senha'             => $senha
                                        ])
                                        ->send('cliente_passwd');

                                    $this->Flash->success(
                                        'Indicação enviada com sucesso ao aluno <strong>'.$CLIENTE->name.'</strong>'
                                        .$notify_message
                                    );

                                    $this->redirect('/professores/admin/alunos');

                                else:
                                    $this->Flash->success('Falha ao enviar indicação para o aluno <strong>'.$CLIENTE->name.'</strong>');
                                endif;
                            endif;
                        endif;
                    }
                } else {
                    $PreOrders = TableRegistry::get('PreOrders');

                    $pre_order_data = [
                        'professor_id'  => $Professor->id,
                        'academia_id'   => $this->request->data['academia_id'],
                        'cliente_id'    => $aluno->id,
                    ];
                    $pre_order = $PreOrders->newEntity();
                    $pre_order = $PreOrders->patchEntity($pre_order, $pre_order_data);

                    /**
                     * SALVAR PRÉ-VENDA (INDICAÇÃO)
                     */
                    if($pre_order_save = $PreOrders->save($pre_order)):

                        $academia = $Academias
                            ->find('all')
                            ->where(['Academias.id' => $this->request->data['academia_id']])
                            ->first();

                        $PreOrderItems = TableRegistry::get('PreOrderItems');

                        /**
                         * PREPARAR ITENS DA PRÉ-VENDA
                         */
                        foreach ($produtos_sessao as $key => $produto):
                            if(!empty($key) && !empty($produto)):
                                $items[$key] = [
                                    'pre_order_id'  =>  $pre_order_save->id,
                                    'produto_id'    =>  $key,
                                    'quantidade'    =>  $produto,
                                ];
                            endif;
                        endforeach;

                        /**
                         * SALVAR ITENS DA PRÉ-VENDA
                         */
                        if(!empty($items)):
                            $pre_order_items = $PreOrderItems->newEntities($items);
                            if($PreOrderItems->saveMany($pre_order_items)):

                                $Notifications = TableRegistry::get('Notifications');

                                $notification = $Notifications->newEntity();
                                $notification = $Notifications->patchEntity($notification, [
                                    'cliente_id'    => $aluno->id,
                                    'name'          => 'Indicação de Produto(s)',
                                    'description'   => 'O professor <strong>'.$Professor->name.'</strong> lhe enviou uma indicação 
                                                        de produto(s).<br>Clique aqui para visualizar',
                                    'link'          =>  WEBROOT_URL.'minha-conta/indicacao-de-produtos',
                                    'global'        =>  0,
                                    'status_id'     =>  1,
                                ]);

                                $notify_message = '';
                                if($Notifications->save($notification)):
                                    $notify_message = '<br> O mesmo já foi notificado';
                                else:
                                    $notify_message = '<br> Houve uma falha ao notificá-lo :/';
                                endif;

                                Email::configTransport('cliente', [
                                    'className' => 'Smtp',
                                    //'host' => 'ssl://smtp.gmail.com',
                                    'host' => 'mail.logfitness.com.br',
                                    //'host' => 'localhost',
                                    'port' => 587,
                                    'timeout' => 30,
                                    //'username' => 'm4web.mkt@gmail.com',
                                    'username' => 'sos@logfitness.com.br',
                                    //'password' => '2VRKHUXBSj2Xyfjw',
                                    'password' => 'Lvrt6174?',
                                    //'password' => 'sos@2016',
                                    'client' => null,
                                    'tls' => null,
                                ]);

                                $email = new Email();

                                $email
                                    ->transport('cliente')
                                    ->template('aluno_pre_order')
                                    ->emailFormat('html')
                                    ->from(['sos@logfitness.com.br' => 'LogFitness'])
                                    ->to($aluno->email)
                                    ->subject('LogFitness - Indicação de Produto')
                                    ->set([
                                        'name'              => $aluno->name,
                                        'professor'         => $Professor->name,
                                        'academia'          => $academia->shortname,
                                        'logo_academia'     => $academia->image
                                    ])
                                    ->send('cliente_passwd');

                                $this->Flash->success(
                                    'Indicação enviada com sucesso ao aluno <strong>'.$aluno->name.'</strong>'
                                    .$notify_message
                                );

                                $this->redirect('/professores/admin/alunos');

                            else:
                                $this->Flash->success('Falha ao enviar indicação para o aluno <strong>'.$aluno->name.'</strong>');
                            endif;
                        endif;
                    endif;
                }
            } else {
                $PreOrders = TableRegistry::get('PreOrders');

                $pre_order_data = [
                    'professor_id'  => $Professor->id,
                    'academia_id'   => $this->request->data['academia_id'],
                    'cliente_id'    => $this->request->data['cliente_id'],
                ];
                $pre_order = $PreOrders->newEntity();
                $pre_order = $PreOrders->patchEntity($pre_order, $pre_order_data);

                /**
                 * SALVAR PRÉ-VENDA (INDICAÇÃO)
                 */
                if($pre_order_save = $PreOrders->save($pre_order)):

                    $aluno = $Alunos
                        ->find('all')
                        ->where(['id' => $pre_order_data['cliente_id']])
                        ->first();

                    $academia = $Academias
                        ->find('all')
                        ->where(['Academias.id' => $this->request->data['academia_id']])
                        ->first();

                    $PreOrderItems = TableRegistry::get('PreOrderItems');

                    /**
                     * PREPARAR ITENS DA PRÉ-VENDA
                     */
                    foreach ($produtos_sessao as $key => $produto):
                        if(!empty($key) && !empty($produto)):
                            $items[$key] = [
                                'pre_order_id'  =>  $pre_order_save->id,
                                'produto_id'    =>  $key,
                                'quantidade'    =>  $produto,
                            ];
                        endif;
                    endforeach;

                    /**
                     * SALVAR ITENS DA PRÉ-VENDA
                     */
                    if(!empty($items)):
                        $pre_order_items = $PreOrderItems->newEntities($items);
                        if($PreOrderItems->saveMany($pre_order_items)):

                            $Notifications = TableRegistry::get('Notifications');

                            $notification = $Notifications->newEntity();
                            $notification = $Notifications->patchEntity($notification, [
                                'cliente_id'    => $aluno->id,
                                'name'          => 'Indicação de Produto(s)',
                                'description'   => 'O professor <strong>'.$Professor->name.'</strong> lhe enviou uma indicação 
                                                    de produto(s).<br>Clique aqui para visualizar',
                                'link'          =>  WEBROOT_URL.'minha-conta/indicacao-de-produtos',
                                'global'        =>  0,
                                'status_id'     =>  1,
                            ]);

                            $notify_message = '';
                            if($Notifications->save($notification)):
                                $notify_message = '<br> O mesmo já foi notificado';
                            else:
                                $notify_message = '<br> Houve uma falha ao notificá-lo :/';
                            endif;

                            Email::configTransport('cliente', [
                                'className' => 'Smtp',
                                //'host' => 'ssl://smtp.gmail.com',
                                'host' => 'mail.logfitness.com.br',
                                //'host' => 'localhost',
                                'port' => 587,
                                'timeout' => 30,
                                //'username' => 'm4web.mkt@gmail.com',
                                'username' => 'sos@logfitness.com.br',
                                //'password' => '2VRKHUXBSj2Xyfjw',
                                'password' => 'Lvrt6174?',
                                //'password' => 'sos@2016',
                                'client' => null,
                                'tls' => null,
                            ]);

                            $email = new Email();

                            $email
                                ->transport('cliente')
                                ->template('aluno_pre_order')
                                ->emailFormat('html')
                                ->from(['sos@logfitness.com.br' => 'LogFitness'])
                                ->to($aluno->email)
                                ->subject('LogFitness - Indicação de Produto')
                                ->set([
                                    'name'              => $aluno->name,
                                    'professor'         => $Professor->name,
                                    'academia'          => $academia->shortname,
                                    'logo_academia'     => $academia->image
                                ])
                                ->send('cliente_passwd');

                            $this->Flash->success(
                                'Indicação enviada com sucesso ao aluno <strong>'.$aluno->name.'</strong>'
                                .$notify_message
                            );

                            $this->redirect('/professores/admin/alunos');

                        else:
                            $this->Flash->success('Falha ao enviar indicação para o aluno <strong>'.$aluno->name.'</strong>');
                        endif;
                    endif;
                endif;
            }
        }

        $alunos = $Alunos
            ->find('list')
            ->where(['Clientes.status_id' => 1, 'Clientes.professor_id' => $Professor->id]);

        $academias_list = $Academias
            ->find('list')
            ->contain(['ProfessorAcademias'])
            ->innerJoin(
                ['ProfessorAcademias'    => 'professor_academias'],
                ['ProfessorAcademias.academia_id = Academias.id']
            )
            ->where(['Academias.status_id' => 1])
            ->andWhere(['ProfessorAcademias.professor_id' => $Professor->id])
            ->andWhere(['ProfessorAcademias.status_id' => 1])
            ->toArray();

        $produtos = $Produtos
            ->find('all',                       ['contain' => ['ProdutoBase']])
            ->where(['Produtos.visivel'       => 1])
            ->andWhere(['Produtos.preco > '     => 0.1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->distinct('ProdutoBase.id')
            ->order('ProdutoBase.name')
            ->all();

        $produtos_list = [];

        foreach ($produtos as $produto):
            $produtos_list[$produto->id] = ['id' => '', 'value' => $produto->produto_base->name];
        endforeach;

        $this->set(compact('produtos_sessao', 'produtos_ind', 'academias_list', 'alunos', 'aluno_id', 'produtos_list', 'Professor'));

        $this->viewBuilder()->layout('default5');
    }

    /**
     * PROFESSOR - PEDIDOS DOS ALUNOS
     * @route :: /professores/admin/pedidos
     */
    public function admin_pedidos()
    {
        $session = $this->request->session();
        $Professor = $session->read('AdminProfessor');
        if (!$Professor) {
            $this->redirect('/professores/acesso/');
        }

        $Pedidos = TableRegistry::get('Pedidos');

        $this->paginate = [
            'contain' => [
                'Clientes',
                'Academias',
                'Academias.Cities',
                'PedidoStatus',
                'PedidoItens',
                'PedidoItens.Produtos',
                'PedidoItens.Produtos.ProdutoBase',
            ],
            'conditions' => ['Pedidos.professor_id' => $Professor->id, 'Pedidos.pedido_status_id !=' => 8],
            'order' => ['Pedidos.id' => 'desc']
        ];
        $pedidos = $this->paginate($Pedidos);

        $this->set(compact('pedidos'));
        $this->set('_serialize', ['pedidos']);

        $this->viewBuilder()->layout('admin_professor');
    }

    /**
     * PROFESSOR - PEDIDOS DOS ALUNOS - VISUALIZAR
     * @route ::
     * @param null $id
     */
    public function admin_pedido_view($id = null)
    {

        $session = $this->request->session();
        $Professor = $session->read('AdminProfessor');
        if (!$Professor) {
            $this->redirect('/professores/acesso/');
        }

        $Pedidos = TableRegistry::get('Pedidos');

        $pedido = $Pedidos
            ->find('all')
            ->contain([
                'Clientes', 'Academias', 'Transportadoras', 'PedidoStatus', 'PedidoItens',
                'PedidoItens.Produtos',
                'PedidoItens.Produtos.ProdutoBase', 'PedidoPagamentos'
            ])
            ->where(['Pedidos.id' => $id])
            ->andWhere(['Pedidos.professor_id' => $Professor->id])
            ->first();

        if ($pedido != null) {

            $this->set('pedido', $pedido);
            $this->set('_serialize', ['pedido']);

            $this->viewBuilder()->layout('admin_professor');
        } else {
            $this->Flash->warning('Pedido #' . $id . ' inexiste ou não relacionado a esta conta.');
            $this->redirect('/professores/admin/pedidos');
        }
    }

    /**
     * ESQUECI A SENHA
     * @param bool $solicitacao
     */
    public function forgot_passwd($solicitacao = false)
    {
        if($this->request->is(['post', 'put']))
        {
            $this->request->data['email'] ? $email = $this->request->data['email'] : $email = '';

            if(strpos($email, '@')) {
                $professores = $this->Professores
                    ->find('all')
                    ->contain(['Cities'])
                    ->where(['Professores.email' => $email])
                    ->andWhere(['Professores.status_id' => 1])
                    ->first();
            } else if(strpos($email, '.')) {
                $professores = $this->Professores
                    ->find('all')
                    ->contain(['Cities'])
                    ->where(['Professores.cpf' => $email])
                    ->andWhere(['Professores.status_id' => 1])
                    ->first();
            } else {
                $parte_um     = substr($email, 0, 3);
                $parte_dois   = substr($email, 3, 3);
                $parte_tres   = substr($email, 6, 3);
                $parte_quatro = substr($email, 9, 2);

                $cpf = $parte_um.'.'.$parte_dois.'.'.$parte_tres.'-'.$parte_quatro;

                $professores = $this->Professores
                    ->find('all')
                    ->contain(['Cities'])
                    ->where(['Professores.cpf' => $cpf])
                    ->andWhere(['Professores.status_id' => 1])
                    ->first();
            }

            if(count($professores) >= 1)
            {
                $ProfPasswd = TableRegistry::get('ProfessorPasswd');

                $data = [
                    'professor_id'    => $professores->id,
                    'recover_key'     => sha1($professores->id.date('YmdHis')),
                    'passwd'          => 0,
                ];

                $passwd = $ProfPasswd->newEntity();
                $passwd = $ProfPasswd->patchEntity($passwd, $data);

                if($ProfPasswd->save($passwd)){

                    Email::configTransport('professor', [
                        'className' => 'Smtp',
                        'host' => 'mail.logfitness.com.br',
                        'port' => 587,
                        'timeout' => 30,
                        'username' => EMAIL_USER,
                        'password' => EMAIL_SENHA,
                        'client' => null,
                        'tls' => null,
                    ]);

                    $email = new Email();
                    $email
                        ->transport('professor')
                        ->template('professor_passwd')
                        ->emailFormat('html')
                        ->from([EMAIL_USER => EMAIL_NAME])
                        ->to($professores->email)
                        ->subject(EMAIL_NAME.' - Recuperar Senha')
                        ->set([
                            'name'              => $professores->name,
                            'email'             => $professores->email,
                            'recover_key'      => $data['recover_key'],
                        ])
                        ->send('professor_passwd');

                    $this->Flash->success('Solicitação enviada com sucesso. Siga as instruções no seu email');
                    $this->redirect('/time/esqueci-a-senha/solicitacao-enviada');
                }else{
                    $this->Flash->error('Falha ao solicitar alteração. Tente novamente...');
                }
            }else{
                $this->Flash->error('Não foi encontrado professor com o email ou CPF informado...');
                $solicitacao = 'falha';
            }
        }
        $this->set(compact('solicitacao'));
    }

    public function recover_passwd($email = null, $recover_key = null, $sucesso = false){

        if($email == null || $recover_key == null){
            $this->redirect(['action' => 'login']);
        }else {

            if ($this->request->is(['post', 'put'])) {

                $professor = $this->Professores
                    ->find('all')
                    ->where(['email' => $email])
                    ->first();

                if (count($professor) >= 1) {

                    $ProfessorPasswd = TableRegistry::get('ProfessorPasswd');

                    $professor_passwd = $ProfessorPasswd
                        ->find('all')
                        ->where(['professor_id'      => $professor->id])
                        ->andWhere(['recover_key'   => $recover_key])
                        ->first();

                    if (count($professor_passwd) >= 1) {

                        $professor->password = $this->request->data['password'];

                        if ($this->Professores->save($professor)) {

                            $professor_passwd->passwd = 1;

                            if ($ProfessorPasswd->save($professor_passwd)) {
                                $this->Flash->success('Recuperação realizada com sucesso. Faça login para continuar');
                                $this->redirect(['action' => 'login']);
                            }
                        }
                    }
                }
            }
        }
        $this->set(compact('sucesso'));
    }

    /**
     * PROFESSOR :: INDICAR ACADEMIAS
     */
    public function indicar_academia()
    {
        $session = $this->request->session();
        $Professor = $session->read('AdminProfessor');
        if (!$Professor) {
            $this->redirect('/professores');
        }

        $Academias_Sugestoes = TableRegistry::get('AcademiasSugestoes');

        $academia_sugestoes = $Academias_Sugestoes->newEntity();

        if ($this->request->is(['patch', 'post', 'put'])) {

            $this->request->data['professor_id'] = $Professor->id;

            $academia_sugestoes = $Academias_Sugestoes->patchEntity($academia_sugestoes, $this->request->data);
            if ($Academias_Sugestoes->save($academia_sugestoes)) {
                $this->Flash->success('Sua indicação foi enviada com sucesso!<br>Em breve entraremos em contato com a academia');
                $this->redirect('/professores/admin/indicar-academia');

            } else {

                $this->Flash->success('Falha ao cadastrar academia... Tente novamente');
            }
        }
        $states         = TableRegistry::get('States')->find('list');
        $cities         = [];

        $this->set(compact('cadastro', 'academia','professor' ,'relacionamento_contato', 'states', 'cities'));
        $this->viewBuilder()->layout('admin_professor');
    }

    public function minhas_academias($search = null)
    {
        $session    = $this->request->session();
        $Professor   = $session->read('AdminProfessor');
        if(!$Professor){
            $this->redirect('/professores');
        }

        $states = TableRegistry::get('States')->find('list');

        $Professor_Academias = TableRegistry::get('ProfessorAcademias');
        $Academias_Sugestoes = TableRegistry::get('AcademiasSugestoes');
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            if($this->request->data['academia_id']) {
                $professor_academias = $Professor_Academias->newEntity();

                $this->request->data['professor_id'] = $Professor->id;
                $this->request->data['status_id'] = 1;

                $professoracademias = $Professor_Academias
                    ->find('all')
                    ->where(['ProfessorAcademias.academia_id' => $this->request->data['academia_id'] ])
                    ->andWhere(['ProfessorAcademias.professor_id' => $this->request->data['professor_id'] ])
                    ->first();

                if($professoracademias == null ) {
                    $professor_academias = $Professor_Academias->patchEntity($professor_academias, $this->request->data);

                    if ($Professor_Academias->save($professor_academias))
                    {
                        $this->Flash->success('Academia adicionada com sucesso!<br>');
                        $this->redirect('/professores/admin/minhas-academias');
                    } else {
                        $this->Flash->error('Falha ao se cadastrar nesta academia... Tente novamente.');
                    }
                }
                else if($professoracademias->status_id == 1){
                    $this->Flash->success('Você já está nessa academia!');
                }
                else{
                    if($professoracademias->status_id == 2) {
                        $professoracademias->status_id = 1;
                        if ($Professor_Academias->save($professoracademias)) {
                            $this->Flash->success('Academia adicionada com sucesso!<br>');
                            $this->redirect('/professores/admin/minhas-academias');
                        } else {
                            $this->Flash->error('Falha ao se cadastrar nesta academia... Tente novamente.');
                        }
                    }
                }
            } else {
                $academia_sugestoes = $Academias_Sugestoes->newEntity();

                $this->request->data['professor_id'] = $Professor->id;

                $academia_sugestoes = $Academias_Sugestoes->patchEntity($academia_sugestoes, $this->request->data);
                if ($Academias_Sugestoes->save($academia_sugestoes)) {
                    $this->Flash->success('Sua indicação foi enviada com sucesso!<br>Em breve entraremos em contato com a academia');
                    $this->redirect('/professores/admin/minhas-academias');

                } else {

                    $this->Flash->success('Falha ao cadastrar academia... Tente novamente');
                }
            }
        }

        $states         = TableRegistry::get('States')->find('list');
        $cities         = [];

        $this->set(compact('cadastro', 'academia','professor' ,'relacionamento_contato', 'states', 'cities', 'academias', 'professor_academias', 'professoracademias', 'city'));
        $this->viewBuilder()->layout('admin_professor');
    }

    public function listar_minhas_academias()
    {
        $session    = $this->request->session();
        $Professor   = $session->read('AdminProfessor');
        if(!$Professor){
            $this->redirect('/professores');
        }

        $Prof_Academia = TableRegistry::get('ProfessorAcademias');

        $Academias = TableRegistry::get('Academias');

        $academias = $Academias
            ->find('list')
            ->all();

        $prof_academia = $Prof_Academia
            ->find('all')
            ->contain(['Academias', 'Professores', 'Academias.Cities'])
            ->where(['ProfessorAcademias.professor_id' => $Professor->id])
            ->andWhere(['ProfessorAcademias.status_id IN' => [1,3]])
            ->order(['Academias.shortname'])
            ->all();

        /**
         * MENSAGEM DE ALERTA PARA PROFESSORES AGUARDANDO ACEITE
         */
        $prof_alert = $Prof_Academia
            ->find('all')
            ->contain(['Academias', 'Professores','Academias.Cities', 'Professores.Cities'])
            ->where(['ProfessorAcademias.professor_id' => $Professor->id])
            ->andWhere(['ProfessorAcademias.status_id' => 3])
            ->all();

        if(count($prof_alert) >= 1){
            $this->Flash->info_laranja('Existe academia com aceite pendente.');
        }

        $states = TableRegistry::get('States')->find('list');
        $cities = TableRegistry::get('Cities')->find('list');

        $this->set(compact('academias','prof_academia', 'states', 'cities'));
        $this->viewBuilder()->layout('admin_professor');
    }

    public function listar($academia_id = null){

        $professores = [];

        if($academia_id != null):

            $professores = $this->Professores
                ->find('all')
                ->contain(['ProfessorAcademias'])
                ->innerJoin(
                    ['ProfessorAcademias' => 'professor_academias'],
                    ['ProfessorAcademias.professor_id = Professores.id']
                )
                ->where(['ProfessorAcademias.academia_id' => $academia_id])
                ->all();

        endif;

        $this->set(compact('professores'));

        $this->viewBuilder()->layout('ajax');

    }

    /**
     * ADMIN PROFESSOR - ACT
     */
    public function admin_act(){

        $session    = $this->request->session();
        $Professor   = $session->read('AdminProfessor');
        if(!$Professor){
            $this->redirect('/professores');
        }

        $act     = TableRegistry::get('Act')
            ->find('all')
            ->where(['Act.is_visible IN' => [0,2]])
            ->andWhere(['Act.status_id' => 1])
            ->orderDesc('Act.modified')
            ->all();

        $this->paginate = [
            'contain' => ['Status']
        ];
        $this->set('_serialize', ['act']);

        $this->set(compact('act'));

        $this->viewBuilder()->layout('admin_professor');

    }

    /** DESLIGAR DE UMA ACADEMIA
     * @return \Cake\Network\Response|null
     */
    public function admin_edit_professores($id = null)
    {
        $session    = $this->request->session();
        $Professor   = $session->read('AdminProfessor');

        if($Professor){

            $ProfessorAcademia =  TableRegistry::get('ProfessorAcademias');

            $prof_academia = $ProfessorAcademia
                ->find('all')
                ->where(['ProfessorAcademias.professor_id' => $Professor->id])
                ->andWhere(['ProfessorAcademias.academia_id' => $id])
                ->andWhere(['ProfessorAcademias.status_id IN' => [1,3]])
                ->first();

            if ($this->request->is(['patch', 'post', 'put', 'get'])) {


                debug($prof_academia);

                //$this->request->data['status_id'] = 2;
                $prof_academia->status_id = 2;

                $prof_academia = $ProfessorAcademia->patchEntity($prof_academia, $this->request->data);

                debug($prof_academia);

                //$prof_academia->status_id = 2;
                if ($ProfessorAcademia->save($prof_academia)) {
                    $this->Flash->success('Academia desligada com sucesso!');
                    return $this->redirect('/professores/admin/listar-minhas-academias/');
                } else {
                    $this->Flash->error('Falha ao desligar academia. Tente novamente.');
                }
            }
            $this->viewBuilder()->layout('admin_professor');
        }
    }

    public function admin_view_academias($id = null)
    {
        $session    = $this->request->session();
        $Professor   = $session->read('AdminProfessor');

        $Prof_Academia = TableRegistry::get('ProfessorAcademias');

        $Academias = TableRegistry::get('Academias');

        $academias = $Academias
            ->find('list')
            ->all();

        $prof_academia = $Prof_Academia
            ->find('all')
            ->contain(['Academias', 'Professores', 'Academias.Cities'])
            ->where(['ProfessorAcademias.professor_id' => $Professor->id])
            ->where(['ProfessorAcademias.academia_id' => $id])
            ->andWhere(['ProfessorAcademias.status_id IN' => [1,3]])
            ->order(['Academias.shortname'])
            ->first();

        $states = TableRegistry::get('States')->find('list');
        $cities = TableRegistry::get('Cities')->find('list');

        $Cities = TableRegistry::get('Cities');
        
        $cidade_query = $Cities
            ->find('all')
            ->where(['id' => $prof_academia->academia->city_id_acad])
            ->first();

        $cidade_acad = $cidade_query->name;
        $estado_acad = $cidade_query->uf;

        $this->set(compact('academias','prof_academia', 'states', 'cities', 'cidade_acad', 'estado_acad'));
        $this->viewBuilder()->layout('admin_professor');
    }

    public function admin_aluno_detalhes($id = null){

        $session    = $this->request->session();
        $Professor   = $session->read('AdminProfessor');
        if(!$Professor){
            $this->redirect(['action' => 'login']);
        }

        $Alunos = TableRegistry::get('Clientes');

        $cliente = $Alunos->get($id, [
            'contain' => ['Cities', 'Academias', 'Status', 'Pedidos']
        ]);

        if($cliente->professor_id != $Professor->id) {
            $this->Flash->error('Falha ao ver dados do aluno, pois ele está relacionado a outro professor...');
            $this->redirect(['action' => 'login']);
        }

        $this->set('cliente', $cliente);
        $this->set('_serialize', ['cliente']);

        $this->viewBuilder()->layout('admin_professor');
    }

    public function admin_alunos_convidar(){
        $session    = $this->request->session();
        $Professor   = $session->read('AdminProfessor');

        if(!$Professor){
            $this->redirect('/professor/acesso/');
        }

        if ($this->request->is('post')) {
            
            $Clientes         = TableRegistry::get('Clientes');
            $Academias        = TableRegistry::get('Academias');
            $ClienteSugestoes = TableRegistry::get('ClienteSugestoes');

            if(isset($this->request->data['email'])) {
                $cliente = $Clientes
                    ->find('all')
                    ->where(['Clientes.email' => $this->request->data['email']])
                    ->first();

                $Academia = $Academias
                    ->find('all')
                    ->where(['id' => $this->request->data['academia_id']])
                    ->first();

                $this->request->data['aceitou']      = 0;
                $this->request->data['city_id']      = $Professor->city_id;
                $this->request->data['professor_id'] = $Professor->id;

                $new_cliente_sugesto = $ClienteSugestoes->newEntity();
                $new_cliente_sugesto = $ClienteSugestoes->patchEntity($new_cliente_sugesto, $this->request->data);

                if ($sugesto_save = $ClienteSugestoes->save($new_cliente_sugesto)) {
                    Email::configTransport('cliente', [
                        'className' => 'Smtp',
                        'host' => 'mail.logfitness.com.br',
                        'port' => 587,
                        'timeout' => 30,
                        'username' => 'uhull@logfitness.com.br',
                        'password' => 'Lvrt6174?',
                        'client' => null,
                        'tls' => null,
                    ]);

                    $email = new Email();

                    $email
                        ->transport('cliente')
                        ->template('aluno_indicado_professor')
                        ->emailFormat('html')
                        ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                        ->to($sugesto_save->email)
                        ->subject($Professor->name.' - LogFitness - Você foi convidado!')
                        ->set([
                            'name'              => $sugesto_save->name,
                            'professor'         => $Professor->name,
                        ])
                        ->send();

                    $this->Flash->success('Aluno convidado com sucesso!');
                    return $this->redirect('/professores/admin/alunos/');
                } else {
                    $this->Flash->error('Falha ao convidar aluno. Tente novamente.');
                }
            } else {
                $new_cliente_sugesto = $ClienteSugestoes->newEntity();

                $this->Flash->info('Informe o arquivo a ser importado. <br>
                            Importante: O mesmo deve estar no formato CSV com valores separados por vírgulas "," não formatado
                            e estar populado com dados em ordem');

                $messages['success'] = '<strong>Alunos Convidados:</strong><br/>';
                $messages['ignore'] = '<br/><strong>Alunos Ignorados:</strong><br/>';
                $messages['error'] = '<br/><strong>Erros encontrados:</strong><br/>';

                $c_success = 0;
                $c_ignore = 0;
                $c_error = 0;

                $row = 0;
                $handle = fopen($this->request->data['csv']['tmp_name'], "r");
                $csv_array = [];
                $separator = ';';

                if ($handle) {

                    Email::configTransport('cliente', [
                            'className' => 'Smtp',
                            'host' => 'mail.logfitness.com.br',
                            'port' => 587,
                            'timeout' => 30,
                            'username' => 'uhull@logfitness.com.br',
                            'password' => 'Lvrt6174?',
                            'client' => null,
                            'tls' => null,
                        ]);

                        $email = new Email();

                    while (($line = fgetcsv($handle, 1000, $separator)) !== false) {
                        $line = array_map('utf8_encode', $line);
                        $data = $line;

                        $num = count($data);

                        if ($num == 3 && $row >= 1) {

                            if(!filter_var($data[1], FILTER_VALIDATE_EMAIL)) {
                                $messages['ignore'] .= ' Falha ao salvar -> '.$data[0].' '.$data[1].' '.$data[2].'<br/>';
                                    $c_error++;
                            } else {
                                $this->request->data['name']         = $data[0];
                                $this->request->data['email']        = $data[1];
                                $this->request->data['aceitou']      = 0;
                                $this->request->data['city_id']      = $Professor->city_id;
                                $this->request->data['telephone']    = $data[2];
                                $this->request->data['professor_id'] = $Professor->id;

                                $new_cliente_sugesto = $ClienteSugestoes->newEntity();
                                $new_cliente_sugesto = $ClienteSugestoes->patchEntity($new_cliente_sugesto, $this->request->data);

                                if ($sugesto_save = $ClienteSugestoes->save($new_cliente_sugesto)) {
                                    
                                    $messages['success'] .= ' -> '.$data[0].' '.$data[1].' '.$data[2].'<br/>';
                                    $c_success++;

                                    $email
                                        ->transport('cliente')
                                        ->template('aluno_indicado_professor')
                                        ->emailFormat('html')
                                        ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                                        ->to($sugesto_save->email)
                                        ->subject('LogFitness - Você foi convidado!')
                                        ->set([
                                            'name'              => $sugesto_save->name,
                                            'professor'          => $Professor->name
                                        ])
                                        ->send();
                                } else {
                                    $messages['error'] .= ' Falha ao salvar -> '.$data[0].' '.$data[1].' '.$data[2].'<br/>';
                                    $c_error++;
                                }

                                $csv_array[] = $data;
                            }
                        }
                        $row++;
                    }
                } else {
                    $this->Flash->error('Falha ao carregar arquivo CSV. Tente novamente...');
                }

                fclose($handle);

                !$c_success ? $messages['success'] .= ' :: Nenhuma <br/>' : $messages['success'] .= ' :: TOTAL = ' . $c_success . '<br/>';
                !$c_error ? $messages['error'] .= ' :: Nenhum  <br/>' : $messages['error'] .= ' :: TOTAL = ' . $c_error . '<br/>';
                !$c_ignore ? $messages['ignore'] .= ' :: Nenhuma  <br/>' : $messages['ignore'] .= ' :: TOTAL = ' . $c_ignore . '<br/>';

                $this->Flash->info($messages['success'] . $messages['ignore'] . $messages['error']);
                return $this->redirect('/professores/admin/alunos/convidar');
            }
        }

        $professor_academias = TableRegistry::get('ProfessorAcademias')
            ->find('all')
            ->contain(['Academias'])
            ->where(['ProfessorAcademias.professor_id' => $Professor->id])
            ->all();

        foreach ($professor_academias as $professor_academia) {
            $academias_list[$professor_academia->academia_id] = $professor_academia->academia->shortname;
        }

        $this->set(compact('academias_list'));

        $this->viewBuilder()->layout('admin_professor');
    }

    public function admin_alunos_convidar_cpf(){
        $session    = $this->request->session();
        $Professor   = $session->read('AdminProfessor');

        if(!$Professor){
            $this->redirect('/professor/acesso/');
        }

        if ($this->request->is('post')) {
            
            $Clientes = TableRegistry::get('Clientes');
            $Academias = TableRegistry::get('Academias');
            $Mensagens = TableRegistry::get('Mensagens');
            $CupomDesconto = TableRegistry::get('CupomDesconto');

            $cliente = $Clientes
                ->find('all')
                ->contain(['Academias'])
                ->where(['Clientes.cpf' => $this->request->data['cpf']])
                ->first();

            $Academia = $Academias
                ->find('all')
                ->where(['id' => $this->request->data['academia_id']])
                ->first();

            if($cliente) {

                Email::configTransport('cliente', [
                    'className' => 'Smtp',
                    'host' => 'mail.logfitness.com.br',
                    'port' => 587,
                    'timeout' => 30,
                    'username' => 'uhull@logfitness.com.br',
                    'password' => 'Lvrt6174?',
                    'client' => null,
                    'tls' => null,
                ]);

                $email = new Email();

                $email
                    ->transport('cliente')
                    ->template('aluno_indicado_professor')
                    ->emailFormat('html')
                    ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                    ->to($cliente->email)
                    ->subject('LogFitness - Você foi convidado!')
                    ->set([
                        'name'      => $cliente->name,
                        'professor' => $Professor->name
                    ])
                    ->send();


                $prefixo = explode('(', $cliente->mobile);
                $prefixo = explode(') ', $prefixo[1]);
                $posfixo = explode('-', $prefixo[1]);
                $posfixo = $posfixo[0].$posfixo[1];

                $numero = '55'.$prefixo[0].$posfixo;

                $nome_aluno = explode(' ', $cliente->name)[0];

                $mensagem = $Mensagens->newEntity();

                $content_msg = "LOGFITNESS.COM.BR/".$Academia->slug.": Fala ".$nome_aluno.", compre no site e retire na nossa academia com frete grátis.";

                $data_mensagem = [
                    'remetente' => '.',
                    'destinatario' => $numero,
                    'quem' => 'Aluno',
                    'msg' => $content_msg
                ];

                $mensagem = $Mensagens->patchEntity($mensagem, $data_mensagem);

                $mensagem = $Mensagens->save($mensagem);

                $body[] = [
                    "id" => $mensagem->id,
                    "from" => ".",
                    "to" => $numero,
                    "msg" => $content_msg
                ];

                $data = [
                    'sendSmsMultiRequest' => [
                        "sendSmsRequestList" => $body
                    ]
                ];

                $data = json_encode($data);

                $ch = curl_init();

                $header = [
                    "Accept: application/json",
                    "Content-Type: application/json",
                    "Authorization: Basic Y2FjaXF1ZS53ZWI6REd2dHdMckNTZA=="
                ];

                $options = array(CURLOPT_URL => "https://api-rest.zenvia360.com.br/services/send-sms-multiple",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $resp_send_sms = json_decode($response);

                foreach ($resp_send_sms->sendSmsMultiResponse->sendSmsResponseList as $key => $msg_resposta) {
                    if($msg_resposta->statusCode == '00') {
                        $mensagem_enviada = $Mensagens->get($msg_resposta->parts[0]->partId);

                        $mensagem_enviada = $Mensagens->patchEntity($mensagem_enviada, ['enviado' => 1]);
                        $Mensagens->save($mensagem_enviada);
                    }
                }

                $this->Flash->info('Esse aluno já está cadastrado na LOG, enviamos um e-mail e um sms para avisar do seu convite!');
                return $this->redirect('/professores/admin/alunos/');
            } else {
                function randString($size){
                    $basic = '0123456789';

                    $return= "";

                    for($count= 0; $size > $count; $count++){
                        $return.= $basic[rand(0, strlen($basic) - 1)];
                    }

                    return $return;
                }

                $senha = randString(6);

                $data = [
                    'name'          => $this->request->data['name'],
                    'telephone'     => $this->request->data['telephone'],
                    'mobile'        => $this->request->data['telephone'],
                    'email'         => $this->request->data['email'],
                    'cpf'           => $this->request->data['cpf'],
                    'password'      => $senha,
                    'status_id'     => 1,
                    'academia_id'   => $Academia->id,
                    'professor_id'  => $Professor->id,
                    'city_id'       => $Academia->city_id,
                    "cep"           => $Academia->cep,
                    "number"        => $Academia->number,
                    "address"       => $Academia->address,
                    "area"          => $Academia->area
                ];

                $new_cliente = $Clientes->newEntity();
                $new_cliente = $Clientes->patchEntity($new_cliente, $data);

                if ($cliente_saved = $Clientes->save($new_cliente)) {

                    $cliente = $Clientes
                        ->find('all')
                        ->contain(['Academias'])
                        ->where(['Clientes.id' => $cliente_saved->id])
                        ->first();

                    Email::configTransport('cliente', [
                        'className' => 'Smtp',
                        'host' => 'mail.logfitness.com.br',
                        'port' => 587,
                        'timeout' => 30,
                        'username' => 'uhull@logfitness.com.br',
                        'password' => 'Lvrt6174?',
                        'client' => null,
                        'tls' => null,
                    ]);

                    $email = new Email();

                    $email
                        ->transport('cliente')
                        ->template('aluno_indicado_professor')
                        ->emailFormat('html')
                        ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                        ->to($cliente->email)
                        ->subject('LogFitness - Você foi convidado!')
                        ->set([
                            'name'      => $cliente->name,
                            'professor' => $Professor->name
                        ])
                        ->send();

                    $cupom_desconto = $CupomDesconto->newEntity();

                    $limite_cupom = Time::now();
                    $limite_cupom->addMonths(1);

                    $now = Time::now();

                    $data = [
                        'descricao' => 'Cupom de primeira compra',
                        'codigo' => 'sms'.$cliente->id,
                        'valor' => 10,
                        'tipo' => 1,
                        'quantidade' => 1,
                        'status' => 1,
                        'limite' => $limite_cupom,
                        'created' => $now,
                        'modified' => $now
                    ];

                    $cupom_desconto = $CupomDesconto->patchEntity($cupom_desconto, $data);

                    if($cupom_saved = $CupomDesconto->save($cupom_desconto)) {
                        $prefixo = explode('(', $cliente->mobile);
                        $prefixo = explode(') ', $prefixo[1]);
                        $posfixo = explode('-', $prefixo[1]);
                        $posfixo = $posfixo[0].$posfixo[1];

                        $numero = '55'.$prefixo[0].$posfixo;

                        $nome_aluno = explode(' ', $cliente->name)[0];

                        $mensagem = $Mensagens->newEntity();

                        $content_msg = "LOGFITNESS.COM.BR/".$Academia->slug.": Fala ".$nome_aluno.", compre no site e retire na nossa academia com frete grátis. Cupom ".$cupom_saved->codigo." com 10% de desconto!";

                        $data_mensagem = [
                            'remetente' => '.',
                            'destinatario' => $numero,
                            'quem' => 'Aluno',
                            'msg' => $content_msg
                        ];

                        $mensagem = $Mensagens->patchEntity($mensagem, $data_mensagem);

                        $mensagem = $Mensagens->save($mensagem);

                        $body[] = [
                            "id" => $mensagem->id,
                            "from" => ".",
                            "to" => $numero,
                            "msg" => $content_msg
                        ];

                        $data = [
                            'sendSmsMultiRequest' => [
                                "sendSmsRequestList" => $body
                            ]
                        ];

                        $data = json_encode($data);

                        $ch = curl_init();

                        $header = [
                            "Accept: application/json",
                            "Content-Type: application/json",
                            "Authorization: Basic Y2FjaXF1ZS53ZWI6REd2dHdMckNTZA=="
                        ];

                        $options = array(CURLOPT_URL => "https://api-rest.zenvia360.com.br/services/send-sms-multiple",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                        );

                        curl_setopt_array($ch, $options);

                        $response = curl_exec($ch);
                        curl_close($ch);

                        $resp_send_sms = json_decode($response);

                        foreach ($resp_send_sms->sendSmsMultiResponse->sendSmsResponseList as $key => $msg_resposta) {
                            if($msg_resposta->statusCode == '00') {
                                $mensagem_enviada = $Mensagens->get($msg_resposta->parts[0]->partId);

                                $mensagem_enviada = $Mensagens->patchEntity($mensagem_enviada, ['enviado' => 1]);
                                $Mensagens->save($mensagem_enviada);
                            }
                        }
                    }

                    $this->Flash->success('Parabéns! Esse aluno foi cadastrado e receberá um sms com um cupom de desconto para sua primeira compra!');
                    return $this->redirect('/professores/admin/alunos/');
                } else {
                    $this->Flash->error('Falha ao convidar aluno. Tente novamente.');
                }
            }


        }

        return $this->redirect('/professores/admin/alunos/');

        $this->viewBuilder()->layout('admin_professor');
    }

    public function admin_relatorios_academia(){
        $session    = $this->request->session();
        $Professor   = $session->read('AdminProfessor');
        if(!$Professor){
            $this->redirect('/professor/acesso/');
        }

        $this->set('select_date', true);

        if ($this->request->is(['post', 'put'])) {

            $ano = $this->request->data['ano'];
            $mes = $this->request->data['mes'];

            if($mes != null && $ano != null) {
                $select_date = false;

                $ProfessorAcademias = TableRegistry::get('ProfessorAcademias');
                $Academias          = TableRegistry::get('Academias');
                $ProfessorPedidos   = TableRegistry::get('Pedidos');

                $professor_academias = $ProfessorAcademias
                    ->find('all')
                    ->contain(['Academias'])
                    ->where(['ProfessorAcademias.professor_id' => $Professor->id])
                    ->andWhere(['ProfessorAcademias.status_id' => 1])
                    ->andWhere(['ProfessorAcademias.created <' => $ano . '-' . $mes . '-30 00:00:00'])
                    ->all();

                foreach ($professor_academias as $pa) {
                    $academias_professor_id[] = $pa->academia_id;
                }

                if(!empty($academias_professor_id)) {
                    $acad_relatorio_name = $Academias
                        ->find('all')
                        ->where(['Academias.id IN' => $academias_professor_id])
                        ->andWhere(['Academias.status_id' => 1])
                        ->all();

                    $professor_acad_relatorio = $ProfessorPedidos
                        ->find('all')
                        ->contain(['Academias'])
                        ->where(['Pedidos.professor_id' => $Professor->id])
                        ->andWhere(['Pedidos.academia_id IN' => $academias_professor_id])
                        ->andWhere(['Pedidos.created LIKE' => $ano . '-' . $mes . '%'])
                        ->andWhere(['Pedidos.pedido_status_id >='     => 3])
                        ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
                        ->andWhere(['Academias.status_id' => 1])
                        ->order(['Pedidos.created' => 'desc'])
                        ->all();
                } else {
                    $acad_relatorio_name = [];
                    $professor_acad_relatorio = [];
                }

                $this->set(compact('professor_academias', 'professor_acad_relatorio', 'academias_professor_id', 'acad_relatorio_name', 'mes', 'ano', 'select_date'));
            }
        }

        $this->viewBuilder()->layout('admin_professor');
    }

    public function verifica_professor() {
        $data = $_GET['data'];
        if($data != null) {
            $professor = $this->Professores
                ->find('all')
                ->where(['cpf' => $data])
                ->orWhere(['email' => $data])
                ->first();

            if($professor) {
                $resposta = 'existe';
            } else {
                $resposta = 'não existe';
            }

            echo json_encode($resposta);
        }

        $this->viewBuilder()->layout('ajax');
        $this->render(false);
    }

    public function salvar_dados_bancarios_cnpj ($comissao = 0) {
        $session    = $this->request->session();
        $Professor  = $session->read('AdminProfessor');
        if(!$Professor){
            $this->redirect('/professor/acesso/');
        }

        if($this->request->is(['post', 'put'])){

            $this->request->data['is_cpf'] = 0;

            $professor = $this->Professores
                ->find('all')
                ->where(['Professores.id' => $Professor->id])
                ->limit(1)
                ->first();

            $professor = $this->Professores->patchEntity($professor, $this->request->data);

            if($this->Professores->save($professor)){
                $this->Flash->success('Dados bancários atualizados com sucesso!');
            }else{
                $this->Flash->error('Falha ao atualizar dados bancários. Tente novamente...');
            }
        }

        if($comissao == 1) {
            return $this->redirect('/professores/admin/comissoes');
        }

        return $this->redirect('/professores/admin/dados-bancarios');

        $this->viewBuilder()->layout('admin_professor');
    }

    public function salvar_dados_bancarios_cpf ($comissao = 0) {
        $session    = $this->request->session();
        $Professor  = $session->read('AdminProfessor');
        if(!$Professor){
            $this->redirect('/professor/acesso/');
        }

        if($this->request->is(['post', 'put'])){

            $this->request->data['is_cpf'] = 1;

            $professor = $this->Professores
                ->find('all')
                ->where(['Professores.id' => $Professor->id])
                ->limit(1)
                ->first();

            $professor = $this->Professores->patchEntity($professor, $this->request->data);

            if($this->Professores->save($professor)){
                $this->Flash->success('Dados bancários atualizados com sucesso!');
            }else{
                $this->Flash->error('Falha ao atualizar dados bancários. Tente novamente...');
            }
        }

        if($comissao == 1) {
            return $this->redirect('/professores/admin/comissoes');
        }

        return $this->redirect('/professores/admin/dados-bancarios');

        $this->viewBuilder()->layout('admin_professor');
    }
}
