<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Class FunctionsController
 * @package App\Controller
 */
class FunctionsController extends AppController{

    /**
     * SANITIZE STRING
     * @return mixed|string
     */
    public function sanitize_string(){

        if($this->request->is(['post', 'put'])){

            $str = $this->request->data['str'];

            $str = str_replace('%', 'porcento', $str);

            $str = preg_replace('/[áàãâä]/ui', 'a', $str);
            $str = preg_replace('/[éèêë]/ui', 'e', $str);
            $str = preg_replace('/[íìîï]/ui', 'i', $str);
            $str = preg_replace('/[óòõôö]/ui', 'o', $str);
            $str = preg_replace('/[úùûü]/ui', 'u', $str);
            $str = preg_replace('/[ç]/ui', 'c', $str);
            // $str = preg_replace('/[,(),;:|!"#$%&/=?~^><ªº-]/', '_', $str);
            $str = preg_replace('/[^a-z0-9]/i', '_', $str);
            $str = preg_replace('/_+/', '-', $str); // ideia do Bacco :)

            $this->viewBuilder()->layout('ajax');

            echo $str;
        }
    }

}