<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\I18n\Time;

class ApisController extends AppController {

    //url: /api/fechar-comissoes?api_key=LOGFITNESS_API_KEY
    public function api_fechar_comissoes() {

        $api_key = $_GET['api_key'];

        if($api_key == LOGFITNESS_API_KEY) {
            $Pedidos                  = TableRegistry::get('Pedidos');
            $Academias                = TableRegistry::get('Academias');
            $AcademiaComissoes        = TableRegistry::get('AcademiaComissoes');
            $AcademiaComissoesPedidos = TableRegistry::get('AcademiaComissoesPedidos');

            $academia_comissao_pedidos = $AcademiaComissoesPedidos
                ->find('list', [
                    'keyField'      => 'pedido_id',
                    'valueField'    => 'pedido_id'
                ])
                ->toArray();

            if(empty($academia_comissao_pedidos)){
                $academia_comissao_pedidos = [0];
            }

            $mes_anterior = Time::now();
            $mes_anterior->subMonth(1);

            $ano = $mes_anterior->format('Y');
            $mes = $mes_anterior->format('m');

            $max_date = new \DateTime(date('Y-m').'-01');

            $todos_pedidos = $Pedidos
                ->find('all')
                ->where(['comissao_status' => 1])
                ->andWhere(['pedido_status_id >=' => 3])
                ->andWhere(['pedido_status_id NOT IN' => [7, 8]])
                ->distinct('academia_id')
                ->all();

            foreach ($todos_pedidos as $todo_pedido) {
                $todas_academias_ids[] = $todo_pedido->academia_id;
            }

            if(count($todas_academias_ids) <= 0) {
                $todas_academias_ids = [0];
            }

            $academia_comissoes_naopegar = $AcademiaComissoes
                ->find('all')
                ->where(['mes' => $mes])
                ->andWhere(['ano' => $ano])
                ->distinct(['academia_id'])
                ->all();

            foreach ($academia_comissoes_naopegar as $academia_comissao_naopegar) {
                $jafoi_ids[] = $academia_comissao_naopegar->academia_id;
            }

            if(count($jafoi_ids) <= 0) {
                $jafoi_ids = [0];
            }

            $outras_academias = $Academias
                ->find('all')
                ->andWhere(['id IN' => $todas_academias_ids])
                ->andWhere(['id NOT IN' => $jafoi_ids])
                ->all();

            $max_date = new \DateTime(date('Y-m').'-01');

            foreach ($outras_academias as $outra_academia) {

                $pedidos = $Pedidos
                    ->find('all')
                    ->contain([
                        'Clientes',
                        'Academias',
                        'Academias.Cities',
                        'PedidoStatus',
                    ])
                    ->where(['Pedidos.academia_id'              => $outra_academia->id])
                    ->andWhere(['Pedidos.created <'             => $max_date])
                    ->andWhere(['Pedidos.comissao_status'         => 1])
                    ->andWhere(['Pedidos.pedido_status_id >='   => 3])
                    ->andWhere(['Pedidos.pedido_status_id NOT IN'   => [7, 8]])
                    ->andWhere(['Pedidos.id NOT IN'             => $academia_comissao_pedidos])
                    ->order(['Pedidos.id' => 'desc'])
                    ->all();

                $valor = 0.0;
                $comissao_valor = 0.0;

                foreach ($pedidos as $pedido) {
                    $valor += $pedido->valor;

                    $comissao = getPercentCalc($pedido->valor, $outra_academia->taxa_comissao);
                    $comissao_valor += $comissao;
                }

                if($comissao_valor >= 1.0) {
                    $data = [
                        'academia_id'   =>  $outra_academia->id,
                        'ano'           =>  $ano,
                        'mes'           =>  $mes,
                        'meta'          =>  0,
                        'vendas'        =>  $valor,
                        'comissao'      =>  $comissao_valor,
                        'aceita'        =>  1,
                        'paga'          =>  0,
                    ];

                    $comissao = $AcademiaComissoes->newEntity();
                    $comissao = $AcademiaComissoes->patchEntity($comissao, $data);

                    if($academia_comissao = $AcademiaComissoes->save($comissao)){

                        foreach($pedidos as $pedido){

                            $data_comissao_pedido = [
                                'academia_comissao_id'  => $academia_comissao->id,
                                'pedido_id'             => $pedido->id,
                            ];

                            $academia_comissao_pedido = $AcademiaComissoesPedidos->newEntity();
                            $academia_comissao_pedido = $AcademiaComissoesPedidos->patchEntity($academia_comissao_pedido, $data_comissao_pedido);

                            $AcademiaComissoesPedidos->save($academia_comissao_pedido);

                        }
                    }
                }
            }

            $Professores               = TableRegistry::get('Professores');
            $ProfessorComissoes        = TableRegistry::get('ProfessorComissoes');
            $ProfessorComissoesPedidos = TableRegistry::get('ProfessorComissoesPedidos');

            $professor_comissao_pedidos = $ProfessorComissoesPedidos
                ->find('list', [
                    'keyField'      => 'pedido_id',
                    'valueField'    => 'pedido_id'
                ])
                ->toArray();

            if(empty($professor_comissao_pedidos)){
                $professor_comissao_pedidos = [0];
            }

            $mes_anterior = Time::now();
            $mes_anterior->subMonth(1);

            $ano = $mes_anterior->format('Y');
            $mes = $mes_anterior->format('m');

            $max_date = new \DateTime(date('Y-m').'-01');

            $todos_pedidos = $Pedidos
                ->find('all')
                ->where(['comissao_status' => 1])
                ->andWhere(['pedido_status_id >=' => 3])
                ->andWhere(['pedido_status_id NOT IN' => [7, 8]])
                ->distinct('professor_id')
                ->all();

            foreach ($todos_pedidos as $todo_pedido) {
                $todos_professores_ids[] = $todo_pedido->professor_id;
            }

            if(count($todos_professores_ids) <= 0) {
                $todos_professores_ids = [0];
            }

            $professor_comissoes_naopegar = $ProfessorComissoes
                ->find('all')
                ->where(['mes' => $mes])
                ->andWhere(['ano' => $ano])
                ->distinct(['professor_id'])
                ->all();

            foreach ($professor_comissoes_naopegar as $professor_comissao_naopegar) {
                $jafoi_ids[] = $professor_comissao_naopegar->professor_id;
            }

            if(count($jafoi_ids) <= 0) {
                $jafoi_ids = [0];
            }

            $outros_professores = $Professores
                ->find('all')
                ->andWhere(['id IN' => $todos_professores_ids])
                ->andWhere(['id NOT IN' => $jafoi_ids])
                ->all();

            $max_date = new \DateTime(date('Y-m').'-01');

            foreach ($outros_professores as $outro_professor) {

                $pedidos = $Pedidos
                    ->find('all')
                    ->contain([
                        'Clientes',
                        'Professores',
                        'Professores.Cities',
                        'PedidoStatus',
                    ])
                    ->where(['Pedidos.professor_id'              => $outro_professor->id])
                    ->andWhere(['Pedidos.created <'             => $max_date])
                    ->andWhere(['Pedidos.comissao_status'         => 1])
                    ->andWhere(['Pedidos.pedido_status_id >='   => 3])
                    ->andWhere(['Pedidos.pedido_status_id NOT IN'   => [7, 8]])
                    ->andWhere(['Pedidos.id NOT IN'             => $professor_comissao_pedidos])
                    ->order(['Pedidos.id' => 'desc'])
                    ->all();

                $valor = 0.0;
                $comissao_valor = 0.0;

                foreach ($pedidos as $pedido) {
                    $valor += $pedido->valor;

                    $comissao = getPercentCalc($pedido->valor, $outro_professor->taxa_comissao);
                    $comissao_valor += $comissao;
                }

                if($comissao_valor >= 1.0) {
                    $data = [
                        'professor_id'  =>  $outro_professor->id,
                        'ano'           =>  $ano,
                        'mes'           =>  $mes,
                        'meta'          =>  0,
                        'vendas'        =>  $valor,
                        'comissao'      =>  $comissao_valor,
                        'aceita'        =>  1,
                        'paga'          =>  0,
                    ];

                    $comissao = $ProfessorComissoes->newEntity();
                    $comissao = $ProfessorComissoes->patchEntity($comissao, $data);

                    if($professor_comissao = $ProfessorComissoes->save($comissao)){

                        foreach($pedidos as $pedido){

                            $data_comissao_pedido = [
                                'professor_comissao_id'  => $professor_comissao->id,
                                'pedido_id'             => $pedido->id,
                            ];

                            $professor_comissao_pedido = $ProfessorComissoesPedidos->newEntity();
                            $professor_comissao_pedido = $ProfessorComissoesPedidos->patchEntity($professor_comissao_pedido, $data_comissao_pedido);

                            $ProfessorComissoesPedidos->save($professor_comissao_pedido);

                        }
                    }
                }
            }
        }

        $this->viewBuilder()->layout(false);
        $this->render(false);
    }

    //url: /api/buscar-produto/$produto_id?api_key=LOGFITNESS_API_KEY
    public function api_buscar_produto($produto_id) {
        $api_key = $_GET['api_key'];

        //Verifica se a API_KEY é inválida
        if($api_key != LOGFITNESS_API_KEY) {
            $api_resposta = [
                'status_id' => 02,
                'status' => 'Erro',
                'retorno_cod' => 'api-key-invalida',
                'mensagem' => 'Chave da API invalida'
            ];

            exit(json_encode($api_resposta));
        }

        $produto = TableRegistry::get('Produtos')
            ->find('all')
            ->contain(['ProdutoBase'])
            ->where(['Produtos.id' => $produto_id])
            ->first();

        $api_resposta = [
            'status_id' => 01,
            'status' => 'Sucesso',
            'retorno_cod' => 'sucesso',
            'produto' => json_encode($produto)
        ];

        echo json_encode($api_resposta);

        $this->viewBuilder()->layout(false);
        $this->render(false);
    }
}