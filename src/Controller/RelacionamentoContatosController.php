<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * RelacionamentoContatos Controller
 *
 * @property \App\Model\Table\RelacionamentoContatosTable $RelacionamentoContatos
 */
class RelacionamentoContatosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index(){

    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add(){

        $relacionamento_contato = $this->RelacionamentoContatos->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {

            $this->request->data['status_id'] = 2;
            $relacionamento_contato = $this->RelacionamentoContatos->patchEntity($relacionamento_contato, $this->request->data);
            if ($this->RelacionamentoContatos->save($relacionamento_contato)) {
                echo 'Contato enviado com sucesso!';
            } else {
                echo 'Falha ao enviar contato... Tente novamente';
            }
        }
    }

}
