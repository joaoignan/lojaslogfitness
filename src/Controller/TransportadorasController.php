<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Transportadoras Controller
 *
 * @property \App\Model\Table\TransportadorasTable $Transportadoras
 */
class TransportadorasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index(){
        $states = $this->Transportadoras->Cities->States->find('list');

        $this->set(compact('states'));
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $transportadora = $this->Transportadoras->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {

            $transportadoras = $this->Transportadoras
                ->find('all')
                ->where(['cnpj' => $this->request->data['cnpj']])
                ->count();

            if($transportadoras >= 1){
                echo 'Já existe uma transportadora cadastrada com o CNPJ informado...';
            }else {
                $this->request->data['status_id'] = 2;
                $transportadora = $this->Transportadoras->patchEntity($transportadora, $this->request->data);
                if ($this->Transportadoras->save($transportadora)) {
                    echo 'Transportadora cadastrada com sucesso!';
                } else {
                    echo 'Falha ao cadastrar transportadora... Tente novamente';
                }
            }

            //debug($this->request->data);
        }
    }

}
