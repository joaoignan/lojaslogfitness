<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Chronos\Date;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use DateTime;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use SoapClient;

/**
 * Clientes Controller
 *
 * @property \App\Model\Table\ClientesTable $Clientes
 */
class ClientesController extends AppController {

    /**
     * @param null $redirect
     * LOGIN
     */
    public function login($redirect = null){

        $session = $this->request->session();

        $Academia = $session->read('Academia');

        $Usuario_logado = $session->read('Cliente');
        if($session->read('CupomDesconto')) {
            $session->destroy('CupomDesconto');
        }

        if($Usuario_logado) {
            $this->redirect(SLUG . DS . 'home');
        }

        $FB_email = $session->read('FB.email');

        if($this->request->is(['post', 'patch', 'put']) || $FB_email){

            $FB_email ? $email = $FB_email : $email = $this->request->data['email'];

            if(strpos($email, '@')) {
                $cliente = $this->Clientes
                    ->find('all', ['contain' => []])
                    ->leftJoinWith('Cities')
                    ->leftJoinWith('Cities.States')
                    ->where(['Clientes.email' => $email])
                    ->andWhere(['Clientes.status_id' => 1])
                    ->first();
            } else if(strpos($email, '.')) {
                $cliente = $this->Clientes
                    ->find('all', ['contain' => []])
                    ->leftJoinWith('Cities')
                    ->leftJoinWith('Cities.States')
                    ->where(['Clientes.cpf' => $email])
                    ->andWhere(['Clientes.status_id' => 1])
                    ->first();
            } else {
                $parte_um     = substr($email, 0, 3);
                $parte_dois   = substr($email, 3, 3);
                $parte_tres   = substr($email, 6, 3);
                $parte_quatro = substr($email, 9, 2);

                $cpf = $parte_um.'.'.$parte_dois.'.'.$parte_tres.'-'.$parte_quatro;

                $cliente = $this->Clientes
                    ->find('all', ['contain' => []])
                    ->leftJoinWith('Cities')
                    ->leftJoinWith('Cities.States')
                    ->where(['Clientes.cpf' => $cpf])
                    ->andWhere(['Clientes.status_id' => 1])
                    ->first();
            }
            
            $hasher = new DefaultPasswordHasher();
            $verify = new DefaultPasswordHasher;

            if($cliente) {

                isset($this->request->data['password']) ? $password = $this->request->data['password'] : $password = '';
                if ($verify->check($password, $cliente->password) || $FB_email) {
                    $session = $this->request->session();
                    $session->write('Cliente', $cliente);

                    if ($cliente->academia_id >= 1) {
                        $academia = $this->Clientes->Academias->get($cliente->academia_id);
                        $session->write('Academia', $academia);
                    }

                    if($redirect == 'return-page') {
                        $this->redirect($this->referer());
                    } else if ($redirect != '') {
                        if ($FB_email) {
                            $this->redirect(SLUG . DS . 'home');
                        } else {
                            echo 'Sucesso!';
                            echo '<script>location.href="'.WEBROOT_URL . SLUG . $redirect . '";</script>';
                        }
                    } else {
                        if ($FB_email) {
                            $this->redirect(SLUG . DS . 'home');
                        } else {
                            echo 'Sucesso!';
                            echo '<script>location.href="'.WEBROOT_URL . SLUG . 'home";</script>';
                        }
                    }
                    $this->render('ajax_login');
                    $this->viewBuilder()->layout(false);
                } else {
                    echo '<style> #info-cadastro-main-com {color: red;} </style>'.'Usuário ou senha não encontrado.';
                    $this->render('ajax_login');
                    $this->viewBuilder()->layout(false);
                }
            }else{
                echo '<style> #info-cadastro-main-com {color: red;} </style>'.'Usuário ou senha não encontrado.';
                $this->render('ajax_login');
                $this->viewBuilder()->layout(false);
            }
        }

        $academias      = TableRegistry::get('Academias')->find('list')->order(['Academias.shortname' => 'asc']);
        $academias_map  = TableRegistry::get('Academias')
            ->find('all', ['contain' => ['Cities']])
            ->where(['Academias.status_id' => 1])
            ->all();
        $states         = TableRegistry::get('States')->find('list');

        if($redirect == 'fb-login-error'){
            $this->set('fb_login_error', true);
        }

        $links_breadcrumb[] = [
            'url' => '/'.$Academia->slug,
            'name' => $Academia->shortname.' - loja'
        ];

        $links_breadcrumb[] = [
            'url' => '/login',
            'name' => 'Login'
        ];

        $this->set(compact('objetivos_list', 'esportes_list', 'academias', 'academias_map', 'states', 'redirect', 'links_breadcrumb'));
    }

    /**
     * FACEBOOK LOGIN
     * @route /fb-login
     */
    public function fb_login(){

        $fb = new Facebook([
            'app_id' => FB_APP_ID, // Replace {app-id} with your app id
            'app_secret' => FB_APP_SECRET,
            'default_graph_version' => FB_API_VERSION,
            'persistent_data_handler'=>'session'
        ]);

        $helper = $fb->getRedirectLoginHelper();

        // Optional permissions for more permission you need to send your application for review
        $permissions = [
            'email'
        ];

        $loginUrl = $helper->getLoginUrl('https://www.logfitness.com.br/fb-login/next', $permissions);

        $this->redirect($loginUrl);

        //echo '<a href="' . htmlspecialchars($loginUrl) . '">Acessar com Facebook!</a>';

        $disable_modal_login = true;

        $this->set(compact('disable_modal_login'));
    }

    /**
     * FACEBOOK LOGIN CALLBACK
     * @route /fb-login/next
     */
    public function fb_login_callback(){

        if(isset($_GET['error'])){
            if($_GET['error'] == 'access_denied'){
                $this->redirect('/login/fb-login-error');
            }
            $this->redirect('/login/fb-login-error');
        }else{

            $session = $this->request->session();
            //$session->start();

            $fb = new Facebook([
                'app_id' => FB_APP_ID, // Replace {app-id} with your app id
                'app_secret' => FB_APP_SECRET,
                'default_graph_version' => FB_API_VERSION,
                'persistent_data_handler'=>'session'
            ]);

            $helper = $fb->getRedirectLoginHelper();

            try {
                $accessToken = $helper->getAccessToken();
            } catch(FacebookResponseException $e) {
                // When Graph returns an error

                echo 'Graph retornou um erro: ' . $e->getMessage();
                exit;
            } catch(FacebookSDKException $e) {
                // When validation fails or other local issues

                echo 'Facebook SDK retornou um erro: ' . $e->getMessage();
                exit;
            }

            try {
                // Get the Facebook\GraphNodes\GraphUser object for the current user.
                // If you provided a 'default_access_token', the '{access-token}' is optional.
                $response = $fb->get('/me?fields=id,name,email,first_name,last_name', $accessToken->getValue());
                //  print_r($response);
            } catch(FacebookResponseException $e) {
                // When Graph returns an error
                echo 'ERRO: Graph ' . $e->getMessage();
                exit;
            } catch(FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'ERRO: falha na validação ' . $e->getMessage();
                exit;
            }
            $me = $response->getGraphUser();

            $cliente = $this->Clientes
                ->find('all')
                ->where(['email'        => $me->getProperty('email')])
                ->andWhere(['status_id' => 1])
                ->first();

            if(count($cliente) >= 1){

                $session->write('FB.email', $cliente->email);

                $this->redirect(SSLUG.'/login');

            }else{
                $cliente    = $this->Clientes->newEntity();

                $data       = [
                    'name'      => $me->getProperty('name'),
                    'email'     => $me->getProperty('email'),
                    //'birth'     => $me->getBirthday()->format('Y-m-d'),
                    'fbid'      => $me->getProperty('id'),
                    'status_id' => 1,
                ];

                $cliente = $this->Clientes->patchEntity($cliente, $data);

                if($this->Clientes->save($cliente)){
                    //$this->redirect('/minha-conta/alterar-senha/'.$data['fbid']);
                    $this->redirect('/fb-login');
                }else{
                    $this->redirect('/login');
                }
            }
        }
    }

    /**
     * LOGOUT
     */
    public function logout(){
        $session = $this->request->session();

        $Academia = $session->read('Academia');
        
        $session->destroy('Cliente');
        $session->destroy('ClienteObjetivos');
        $session->destroy('ClienteEsportes');
        $session->destroy('FB');

        if($Academia) {
            return $this->redirect(SSLUG.'/'.$Academia->slug);
        } else {
            return $this->redirect(SSLUG.'/');
        }
    }

    /**
     * ALTERAR SENHA
     */
    public function view_passwd($fbid = null){

        $session    = $this->request->session();
        $cliente    = $session->read('Cliente');
        $Academia   = $session->read('Academia');

        if($cliente){

            $cliente = $this->Clientes->get($cliente->id);

            if($cliente){
                $this->set('cliente', $cliente);
                $this->set('_serialize', ['cliente']);

                if($this->request->is(['post', 'put', 'patch'])){
                    $hasher = new DefaultPasswordHasher();
                    $verify = new DefaultPasswordHasher;

                    if($fbid != null){
                        //if($cliente->fbid == str_replace(array('#', '_', '='), '', $fbid)){
                        if($verify->check($this->request->data['confirm_new_password'],
                            $hasher->hash($this->request->data['password']))){
                            $cliente->password = $this->request->data['password'];
                            if($this->Clientes->save($cliente)){
                                $this->Flash->client_success('Senha cadastrada com sucesso!');
                                $this->redirect(SSLUG.'/minha-conta/alterar-senha');
                            }else{
                                $this->Flash->client_error('Falha ao cadastrar a senha :(');
                            }
                        }
                        //}
                    }else{
                        if($verify->check($this->request->data['old_password'], $cliente->password)){
                            if($verify->check($this->request->data['confirm_new_password'],
                                $hasher->hash($this->request->data['password']))){
                                $cliente->password = $this->request->data['password'];
                                if($this->Clientes->save($cliente)){
                                    $this->Flash->client_success('Senha alterada com sucesso!');
                                    $this->redirect(SSLUG.'/minha-conta/alterar-senha');
                                }else{
                                    $this->Flash->client_error('Falha ao alterar a senha :(');
                                }
                            }
                        }else{
                            $this->redirect(SSLUG.'/login/minha-conta');
                        }
                    }
                }

            }else{
                $this->redirect(SSLUG.'/login/minha-conta');
            }
        }else{
            $this->redirect(SSLUG.'/login/minha-conta');
        }

        $disable_modal_login = true;

        $Produtos = TableRegistry::get('Produtos');

        $this->paginate = [

        ];
        $produtos = $Produtos
            ->find('all', ['contain' => ['ProdutoBase']])
            ->where(['Produtos.status_id' => 1])
            ->andWhere(['Produtos.preco > ' => 0.1])
            ->distinct('ProdutoBase.id')
            //->limit(16)
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->order('rand()')
            ->all();

        $session    = $this->request->session();

        $session_produto = $session->read('Carrinho');

        $session_cliente = $session->read('Cliente');

        $session_produto_ids = [];
        if(!empty($session_produto)) {
            foreach ($session_produto as $key => $item) {
                $session_produto_ids[] = $key;
            }
        }

        if(!empty($session_produto_ids)) {
            $s_produtos = $Produtos
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->where(['Produtos.id IN' => $session_produto_ids])
                ->all();
        }else{
            $s_produtos = [];
        }

        $links_breadcrumb[] = [
            'url' => '/'.$Academia->slug,
            'name' => $Academia->shortname.' - loja'
        ];

        $links_breadcrumb[] = [
            'url' => '/minha-conta/alterar-senha/',
            'name' => 'Alterar Senha'
        ];

        $this->set(compact('fbid', 'disable_modal_login', 's_produtos', 'session_produto', 'session_cliente', 'produtos', 'pedidos', 'links_breadcrumb'));
    }

    /**
     * ESQUECI A SENHA
     * @param bool $solicitacao
     */
    public function forgot_passwd($solicitacao = false, $email = false){

        if($this->request->is(['post', 'put'])){

            $this->request->data['email'] ? $email = $this->request->data['email'] : $email = '';

            if(strpos($email, '@')) {
                $cliente = $this->Clientes
                    ->find('all')
                    ->where(['Clientes.email' => $email])
                    ->andWhere(['Clientes.status_id' => 1])
                    ->first();
            } else if(strpos($email, '.')) {
                $cliente = $this->Clientes
                    ->find('all')
                    ->where(['Clientes.cpf' => $email])
                    ->andWhere(['Clientes.status_id' => 1])
                    ->first();
            } else {
                $parte_um     = substr($email, 0, 3);
                $parte_dois   = substr($email, 3, 3);
                $parte_tres   = substr($email, 6, 3);
                $parte_quatro = substr($email, 9, 2);

                $cpf = $parte_um.'.'.$parte_dois.'.'.$parte_tres.'-'.$parte_quatro;

                $cliente = $this->Clientes
                    ->find('all')
                    ->where(['Clientes.cpf' => $cpf])
                    ->andWhere(['Clientes.status_id' => 1])
                    ->first();
            }

            if($cliente->fbid != null){
                $solicitacao = 'facebook';
            }
            else{
                if(count($cliente) >= 1){

                    $ClientePasswd = TableRegistry::get('ClientePasswd');

                    function randString($size){
                        $basic = '0123456789abcdefghijklmnopqrstuvwxyz';

                        $return= "";

                        for($count= 0; $size > $count; $count++){
                            $return.= $basic[rand(0, strlen($basic) - 1)];
                        }

                        return $return;
                    }

                    $senha = randString(16);

                    $data = [
                        'cliente_id'      => $cliente->id,
                        'recover_key'     => $senha,
                        'passwd'          => 0,
                    ];

                    $passwd = $ClientePasswd->newEntity();
                    $passwd = $ClientePasswd->patchEntity($passwd, $data);

                    if($ClientePasswd->save($passwd)){

                        Email::configTransport('cliente', [
                            'className' => 'Smtp',
                            'host' => 'mail.logfitness.com.br',
                            'port' => 587,
                            'timeout' => 30,
                            'username' => 'sos@logfitness.com.br',
                            'password' => 'Lvrt6174?',
                            'client' => null,
                            'tls' => null,
                        ]);

                        $email = new Email();

                        $email
                            ->transport('cliente')
                            ->template('cliente_passwd')
                            ->emailFormat('html')
                            ->from(['sos@logfitness.com.br' => 'LogFitness'])
                            ->to($cliente->email)
                            ->subject('LogFitness - Recuperar Senha')
                            ->set([
                                'name'              => $cliente->name,
                                'email'             => $cliente->email,
                                'recover_key'      => $data['recover_key'],
                            ])
                            ->send('cliente_passwd');

                        $this->redirect(SSLUG.'/login/esqueci-a-senha/solicitacao-enviada/');
                    }
                }else{
                    $solicitacao = 'falha';
                }
            }
        }

        $disable_modal_login = true;

        $this->set(compact('disable_modal_login', 'solicitacao', 'email'));

    }

    /**
     * RECUPERAR SENHA
     * @param null $email
     * @param null $recover_key
     * @param bool $sucesso
     */
    public function recover_passwd($email = null, $recover_key = null, $sucesso = false){

        if($email == null || $recover_key == null){
            $this->redirect(SSLUG.'/home');
        }else {


            if ($this->request->is(['post', 'put'])) {

                $cliente = $this->Clientes
                    ->find('all')
                    ->where(['email' => $email])
                    ->first();

                if (count($cliente) >= 1) {

                    $ClientePasswd = TableRegistry::get('ClientePasswd');

                    $cliente_passwd = $ClientePasswd
                        ->find('all')
                        ->where(['cliente_id' => $cliente->id])
                        ->andWhere(['recover_key' => $recover_key])
                        ->first();

                    if (count($cliente_passwd) >= 1) {

                        $data_passwd = [
                            'password' => $this->request->data['password']
                        ];

                        $passwd_cliente = $this->Clientes->patchEntity($cliente, $data_passwd);
                        if ($this->Clientes->save($passwd_cliente)) {

                            $passwd = $ClientePasswd->patchEntity($cliente_passwd, ['passwd' => 1]);

                            if ($ClientePasswd->save($passwd)) {
                                $this->redirect(SSLUG . '/login/recuperar-a-senha/'.$email.'/'.$recover_key.'/sucesso');
                            }
                        }
                    }

                }
            }
        }

        $disable_modal_login = true;

        $this->set(compact('disable_modal_login', 'sucesso'));

    }


    public function esqueci_facebook(){
       $teste = 10;

    }

    /**
     * MINHA CONTA
     * MEUS PEDIDOS
     * @route /minha-conta
     */
    public function view(){

        $session    = $this->request->session();
        $cliente    = $session->read('Cliente');

        if($cliente){

            $pedidos = $this->Clientes->Pedidos
                ->find('all', ['contain' => [
                    'PedidoItens',
                    'PedidoItens.Produtos',
                    'PedidoItens.Produtos.ProdutoBase',
                    'PedidoStatus'
                ]
                ])
                ->where(['Pedidos.cliente_id' => $cliente->id])
                ->order(['Pedidos.created' => 'desc'])
                ->all();

            $Produtos = TableRegistry::get('Produtos');

            $this->paginate = [

            ];
            $produtos = $Produtos
                ->find('all', ['contain' => ['ProdutoBase']])
                ->where(['Produtos.status_id' => 1])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->distinct('ProdutoBase.id')
                //->limit(16)
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->order('rand()')
                ->all();

            $session    = $this->request->session();

            $session_produto = $session->read('Carrinho');

            $session_cliente = $session->read('Cliente');

            $session_produto_ids = [];
            if(!empty($session_produto)) {
                foreach ($session_produto as $key => $item) {
                    $session_produto_ids[] = $key;
                }
            }

            if(!empty($session_produto_ids)) {
                $s_produtos = $Produtos
                    ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                    ->andWhere(['Produtos.preco > ' => 0.1])
                    ->where(['Produtos.id IN' => $session_produto_ids])
                    ->all();
            }else{
                $s_produtos = [];
            }

            $this->set(compact('s_produtos', 'session_produto', 'session_cliente', 'produtos', 'pedidos'));

        }else{
            $this->redirect(SSLUG.'/login/minha-conta');
        }
    }

    public function view_pedidos(){

        $session    = $this->request->session();
        $cliente    = $session->read('Cliente');
        $Academia   = $session->read('Academia');

        if($cliente){

            $pedidos = $this->Clientes->Pedidos
                ->find('all', ['contain' => [
                    'PedidoItens',
                    'PedidoItens.Produtos',
                    'PedidoItens.Produtos.ProdutoBase',
                    'PedidoStatus',
                ]
                ])
                ->where(['Pedidos.cliente_id' => $cliente->id])
                ->andWhere(['Pedidos.valor >' => 0.1])
                ->andWhere(['Pedidos.pedido_status_id !=' => 8])
                ->andWhere(['Pedidos.comissao_status' => 1])
                ->order(['Pedidos.created' => 'desc'])
                ->all();

            $cupom_desconto = TableRegistry::get('CupomDesconto')
                ->find('all')
                ->all();

            $Produtos = TableRegistry::get('Produtos');

            $this->paginate = [

            ];
            $produtos = $Produtos
                ->find('all', ['contain' => ['ProdutoBase']])
                ->where(['Produtos.status_id' => 1])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->distinct('ProdutoBase.id')
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->order('rand()')
                ->all();

            $session    = $this->request->session();

            $session_produto = $session->read('Carrinho');

            $session_cliente = $session->read('Cliente');

            $session_produto_ids = [];
            if(!empty($session_produto)) {
                foreach ($session_produto as $key => $item) {
                    $session_produto_ids[] = $key;
                }
            }

            if(!empty($session_produto_ids)) {
                $s_produtos = $Produtos
                    ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                    ->andWhere(['Produtos.preco > ' => 0.1])
                    ->where(['Produtos.id IN' => $session_produto_ids])
                    ->all();
            }else{
                $s_produtos = [];
            }

            $links_breadcrumb[] = [
                'url' => '/'.$Academia->slug,
                'name' => $Academia->shortname.' - loja'
            ];

            $links_breadcrumb[] = [
                'url' => '/minha-conta/meus-pedidos/',
                'name' => 'Meus Pedidos'
            ];

            $this->set(compact('cupom_desconto','s_produtos', 'session_produto', 'session_cliente', 'produtos', 'pedidos', 'links_breadcrumb'));

        }else{
            $this->redirect(SSLUG.'/login/minha-conta');
        }
    }

    /**
     * MINHA CONTA
     * DADOS DE FATURAMENTO
     * @route /minha-conta/dados-faturamento
     */
    public function view_dados(){

        $session    = $this->request->session();
        $cliente    = $session->read('Cliente');
        $Academia   = $session->read('Academia');

        if(!$cliente){
            $this->redirect(SSLUG.'/login/minha-conta');
        }else{

            if($this->request->is(['post', 'put', 'patch'])){
                $cliente_save = $this->Clientes->get($cliente->id, [
                    'contain' => []
                ]);

                if($this->Clientes->exists(['cpf' => $this->request->data['cpf']]) && $this->request->data['cpf'] != $cliente->cpf){
                    $this->Flash->warning('Falha ao salvar: CPF já cadastrado...');
                    $this->redirect(SSLUG.'/minha-conta/dados-faturamento');
                } else if($this->Clientes->exists(['email' => $this->request->data['email']]) && $this->request->data['email'] != $cliente->email){
                    $this->Flash->warning('Falha ao salvar: Email já cadastrado...');
                    $this->redirect(SSLUG.'/minha-conta/dados-faturamento');
                } else {

                    $birth = Time::now();
                    $birth_date = array_reverse(explode('/', $this->request->data['birth']));
                    $birth->setDate($birth_date[0], $birth_date[1], $birth_date[2]);

                    $this->request->data['birth'] = $birth;

                    $cliente_save = $this->Clientes->patchEntity($cliente_save, $this->request->data);

                    if ($CLIENTE = $this->Clientes->save($cliente_save)) {


                        $session->write('Cliente', $CLIENTE);

                        $Academia = $this->Clientes->Academias
                            ->find('all')
                            ->where(['id' => $CLIENTE->academia_id])
                            ->first();

                        $session->write('Academia', $Academia);

                        $this->Flash->success('Cadastro atualizado com sucesso!');
                        $this->redirect(SSLUG . '/minha-conta/dados-faturamento');
                    } else {
                        $this->Flash->error('Falha ao atualizar cadastro. Tente novamente...');
                        $this->redirect(SSLUG . '/minha-conta/dados-faturamento');
                    }
                }
            }

            $cliente_dados = $this->Clientes
                ->find()
                ->select(['Clientes.city_id', 'Cities.id', 'Cities.state_id'])
                ->leftJoin(
                    ['Cities' => 'cities'],
                    ['Cities.id = Clientes.city_id'])
                ->where(['Clientes.id' => $cliente->id])
                ->first();

            if($cliente->city_id >= 1){

                $cities     = $this->Clientes->Cities
                    ->find('list')
                    ->where(['Cities.state_id' => $cliente_dados->Cities['state_id']]);
            }else{
                $cities = [];
            }

            $states     = $this->Clientes->Cities->States->find('list');
            $academia  = $this->Clientes->Academias
                ->find('all', ['contain' => ['Cities', 'Cities.States']])
                ->where(['Academias.id' => $cliente->academia_id])
                ->limit(1)
                ->first();

            if($academia) {

                $academia_cities = $this->Clientes->Cities
                    ->find('list')
                    ->innerJoin(
                        ['Academias' => 'academias'],
                        ['Academias.city_id = Cities.id'])
                    ->where(['state_id' => $academia->city->state_id])
                    ->all()
                    ->toArray();

                $academias = $this->Clientes->Academias
                    ->find('list', [
                        'keyField' => 'id',
                        'valueField' => 'shortname'
                    ])
                    ->innerJoin(
                        ['Cities' => 'cities'],
                        ['Cities.id = Academias.city_id'])
                    //->contain(['Cities'])
                    ->where(['Academias.status_id' => 1])
                    //->andWhere(['Cities.state_id' => $academia->city->state_id])
                    ->andWhere(['Cities.id' => $academia->city->id])
                    ->all()
                    ->toArray();
            }else{
                $academia_cities = [];
                $academias       = [];
            }


            $Produtos = TableRegistry::get('Produtos');

            $this->paginate = [

            ];
            $produtos = $Produtos
                ->find('all', ['contain' => ['ProdutoBase']])
                ->where(['Produtos.status_id' => 1])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->distinct('ProdutoBase.id')
                //->limit(16)
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->order('rand()')
                ->all();

            $session    = $this->request->session();

            $session_produto = $session->read('Carrinho');

            $session_cliente = $session->read('Cliente');

            $session_produto_ids = [];
            if(!empty($session_produto)) {
                foreach ($session_produto as $key => $item) {
                    $session_produto_ids[] = $key;
                }
            }

            if(!empty($session_produto_ids)) {
                $s_produtos = $Produtos
                    ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                    ->andWhere(['Produtos.preco > ' => 0.1])
                    ->where(['Produtos.id IN' => $session_produto_ids])
                    ->all();
            }else{
                $s_produtos = [];
            }

            $links_breadcrumb[] = [
                'url' => '/'.$Academia->slug,
                'name' => $Academia->shortname.' - loja'
            ];

            $links_breadcrumb[] = [
                'url' => '/minha-conta/dados-faturamento/',
                'name' => 'Meus Dados'
            ];

            // print_r($cliente_dados);

            $this->set(compact('states', 'cities',
                's_produtos',
                'session_produto',
                'session_cliente',
                'produtos',
                'academia',
                'academia_cities',
                'academias',
                'cliente_dados',
                'links_breadcrumb'));

            $this->set('cliente', $cliente);
            $this->set('_serialize', ['cliente']);
        }
    }

    /**
     * MINHA CONTA
     * MEUS OBJETIVOS
     * @route /minha-conta/meus-dados
     */
    public function view_meus_dados(){

        $session    = $this->request->session();
        $cliente    = $session->read('Cliente');
        $Academia   = $session->read('Academia');

        if(!$cliente){
            $this->redirect(SSLUG.'/login/minha-conta');
        }else{

            $esportes_select    = TableRegistry::get('ClienteEsportes')
                ->find('list')
                ->where(['ClienteEsportes.cliente_id' => $cliente->id])
                ->all()
                ->toArray();

            if($this->request->is(['post', 'put', 'patch'])){
                $cliente_save = $this->Clientes->get($cliente->id, [
                    'contain' => []
                ]);

                $cliente_save = $this->Clientes->patchEntity($cliente_save, $this->request->data);

                if ($CLIENTE = $this->Clientes->save($cliente_save)) {

                    $ClienteEsportes   = TableRegistry::get('ClienteEsportes');


                    foreach ($this->request->data['esportes'] as $ko => $esporte) {
                        $esporte_data = [
                            'cliente_id'    => $CLIENTE->id,
                            'esporte_id'   => $ko,
                        ];

                        if($esporte == 'Y'){
                            if(!$ClienteEsportes->exists($esporte_data)){
                                $EsporteSave = $ClienteEsportes->newEntity();
                                $EsporteSave = $ClienteEsportes->patchEntity($EsporteSave, $esporte_data);

                                $ClienteEsportes->save($EsporteSave);
                            }
                        }else{
                            if($ClienteEsportes->exists($esporte_data)){
                                $esporte_delete = $ClienteEsportes
                                    ->find()
                                    ->where($esporte_data)
                                    ->first();

                                $esporte_deleted = $ClienteEsportes->delete($esporte_delete);
                            }
                        }
                    }

                    $session->write('Cliente', $CLIENTE);

                    $this->redirect(SSLUG.'/minha-conta/meus-dados');
                } else {

                    $this->redirect(SSLUG.'/minha-conta/meus-dados');
                }
            }

            $cliente_dados = $this->Clientes
                ->find()
                ->select(['Clientes.city_id', 'Cities.id', 'Cities.state_id'])
                ->leftJoin(
                    ['Cities' => 'cities'],
                    ['Cities.id = Clientes.city_id'])
                ->where(['Clientes.id' => $cliente->id])
                ->first();

            if($cliente->city_id >= 1){

                $cities     = $this->Clientes->Cities
                    ->find('list')
                    ->where(['Cities.state_id' => $cliente_dados->Cities['state_id']]);
            }else{
                $cities = [];
            }


            $states     = $this->Clientes->Cities->States->find('list');
            $academias  = $this->Clientes->Academias
                ->find('all', ['contain' => ['Cities', 'Cities.States']])
                ->where(['Academias.status_id' => 1])
                ->first();

            $academias_list  = $this->Clientes->Academias
                ->find('list')
                ->where(['Academias.status_id' => 1])
                ->all()
                ->toArray();


            $professores_list  = TableRegistry::get('Professores')
                ->find('list')
                ->innerJoin(
                    ['ProfessorAcademias' => 'professor_academias'],
                    ['ProfessorAcademias.professor_id = Professores.id'])
                ->where(['Professores.status_id' => 1])
                ->andWhere(['ProfessorAcademias.academia_id' => $academias->id])
                ->all()
                ->toArray();


            $objetivos_list     = TableRegistry::get('Objetivos')
                ->find('list')
                ->where(['status_id' => 1])
                ->all()
                ->toArray();


            $esportes_list      = TableRegistry::get('Esportes')
                ->find('list')
                ->all()
                ->toArray();

            $Produtos = TableRegistry::get('Produtos');

            $this->paginate = [

            ];
            $produtos = $Produtos
                ->find('all', ['contain' => ['ProdutoBase']])
                ->where(['Produtos.status_id' => 1])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->distinct('ProdutoBase.id')
                //->limit(16)
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->order('rand()')
                ->all();

            $session    = $this->request->session();

            $session_produto = $session->read('Carrinho');

            $session_cliente = $session->read('Cliente');

            $session_produto_ids = [];
            if(!empty($session_produto)) {
                foreach ($session_produto as $key => $item) {
                    $session_produto_ids[] = $key;
                }
            }

            if(!empty($session_produto_ids)) {
                $s_produtos = $Produtos
                    ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                    ->andWhere(['Produtos.preco > ' => 0.1])
                    ->where(['Produtos.id IN' => $session_produto_ids])
                    ->all();
            }else{
                $s_produtos = [];
            }

            $links_breadcrumb[] = [
                'url' => '/'.$Academia->slug,
                'name' => $Academia->shortname.' - loja'
            ];

            $this->set(compact('states', 'cities',
                's_produtos',
                'session_produto',
                'session_cliente',
                'produtos',
                'academias',
                'academias_list',
                'professores_list',
                'cliente_dados',
                'esportes_list',
                'esportes_select',
                'links_breadcrumb'));

            $this->set('cliente', $cliente);
            $this->set('_serialize', ['cliente']);
        }
    }

    /**
     * MINHA CONTA
     * MINHA ACADEMIA
     * @route /minha-academia
     */
    public function view_academia(){

        $session    = $this->request->session();
        $cliente    = $session->read('Cliente');
        $Academia   = $session->read('Academia');

        if(!$cliente){
            $this->redirect(WEBROOT_URL.'/login/minha-conta');
        }else{

            if($this->request->is(['post', 'put', 'patch'])){
                $cliente_save = $this->Clientes->get($cliente->id);

                $cliente_save = $this->Clientes->patchEntity($cliente_save, $this->request->data);

                if ($CLIENTE = $this->Clientes->save($cliente_save)) {

                    $session->write('Cliente', $CLIENTE);

                    $Academia = $this->Clientes->Academias
                        ->find('all')
                        ->where(['id' => $CLIENTE->academia_id])
                        ->first();



                    $session->write('Academia', $Academia);

                    $this->Flash->success('Cadastro atualizado com sucesso!');
                    $this->redirect(WEBROOT_URL.'minha-academia/');
                } else {
                    $this->Flash->error('Falha ao atualizar cadastro. Tente novamente...');
                    $this->redirect(WEBROOT_URL.'minha-academia/');
                }

            }


            $cliente_dados = $this->Clientes
                ->find()
                ->select(['Clientes.city_id', 'Cities.id', 'Cities.state_id'])
                ->leftJoin(
                    ['Cities' => 'cities'],
                    ['Cities.id = Clientes.city_id'])
                ->where(['Clientes.id' => $cliente->id])
                ->first();

            $states     = $this->Clientes->Cities->States->find('list');

            $academia  = $this->Clientes->Academias
                ->find('all', ['contain' => ['Cities', 'Cities.States']])
                ->where(['Academias.id' => $cliente->academia_id])
                ->limit(1)
                ->first();

            if($academia) {

                $academia_cities = $this->Clientes->Cities
                    ->find('list')
                    ->innerJoin(
                        ['Academias' => 'academias'],
                        ['Academias.city_id = Cities.id'])
                    ->where(['state_id' => $academia->city->state_id])
                    ->all()
                    ->toArray();

                $academias = $this->Clientes->Academias
                    ->find('list', [
                        'keyField' => 'id',
                        'valueField' => 'shortname'
                    ])
                    ->innerJoin(
                        ['Cities' => 'cities'],
                        ['Cities.id = Academias.city_id'])
                    //->contain(['Cities'])
                    ->where(['Academias.status_id' => 1])
                    //->andWhere(['Cities.state_id' => $academia->city->state_id])
                    ->andWhere(['Cities.id' => $academia->city->id])
                    ->order(['Academias.shortname' => 'asc'])
                    ->all()
                    ->toArray();

                $professores_list  = TableRegistry::get('Professores')
                    ->find('list')
                    ->innerJoin(
                        ['ProfessorAcademias' => 'professor_academias'],
                        ['ProfessorAcademias.professor_id = Professores.id'])
                    ->where(['Professores.status_id' => 1])
                    ->andWhere(['ProfessorAcademias.academia_id' => $cliente->academia_id])
                    ->andWhere(['ProfessorAcademias.status_id' => 1])
                    ->order(['Professores.name' => 'asc'])
                    ->all()
                    ->toArray();

                $Cities = TableRegistry::get('Cities');
        
                $cidade_query = $Cities
                    ->find('all')
                    ->where(['id' => $academia->city_id_acad])
                    ->first();

                $cidade_acad = $cidade_query->id;
                $estado_acad = $cidade_query->state_id;

            }else{
                $academia_cities = [];
                $academias       = [];
            }

            $Produtos = TableRegistry::get('Produtos');

            $this->paginate = [

            ];
            $produtos = $Produtos
                ->find('all', ['contain' => ['ProdutoBase']])
                ->where(['Produtos.status_id' => 1])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->distinct('ProdutoBase.id')
                //->limit(16)
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->order('rand()')
                ->all();

            $session    = $this->request->session();

            $session_produto = $session->read('Carrinho');

            $session_cliente = $session->read('Cliente');

            $session_produto_ids = [];
            if(!empty($session_produto)) {
                foreach ($session_produto as $key => $item) {
                    $session_produto_ids[] = $key;
                }
            }

            if(!empty($session_produto_ids)) {
                $s_produtos = $Produtos
                    ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                    ->andWhere(['Produtos.preco > ' => 0.1])
                    ->where(['Produtos.id IN' => $session_produto_ids])
                    ->all();
            }else{
                $s_produtos = [];
            }

            $links_breadcrumb[] = [
                'url' => '/'.$Academia->slug,
                'name' => $Academia->shortname.' - loja'
            ];

            $links_breadcrumb[] = [
                'url' => '/minha-conta/minha-academia/',
                'name' => 'Minha Academia'
            ];

            $this->set(compact(
                    's_produtos',
                    'session_produto',
                    'session_cliente',
                    'produtos',
                    'states',
                    'academia',
                    'academia_cities',
                    'academias',
                    'professores_list',
                    'cliente_dados',
                    'links_breadcrumb',
                    'cidade_acad',
                    'estado_acad'
                )
            );

            $this->set('cliente', $cliente);
            $this->set('_serialize', ['cliente']);
        }
    }

    public function view_indicacoes(){

        $session    = $this->request->session();
        $cliente    = $session->read('Cliente');
        $Academia   = $session->read('Academia');            

        if(!$cliente){
            $this->redirect(SSLUG.'/login/minha-conta');
        }else {

            $PreOrders = TableRegistry::get('PreOrders');

            $pre_orders = $PreOrders
                ->find('all')
                ->contain([
                    'Professores',
                    'Academias',
                    'PreOrderItems',
                    'PreOrderItems.Produtos',
                    'PreOrderItems.Produtos.ProdutoBase'
                ])
                ->where(['PreOrders.cliente_id' => $cliente->id])
                ->order(['PreOrders.id' => 'desc'])
                ->all();

            //debug($pre_orders);

            $Produtos = TableRegistry::get('Produtos');

            $this->paginate = [

            ];
            $produtos = $Produtos
                ->find('all', ['contain' => ['ProdutoBase']])
                ->where(['Produtos.status_id' => 1])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->distinct('ProdutoBase.id')
                //->limit(16)
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->order('rand()')
                ->all();

            $session    = $this->request->session();

            $session_produto = $session->read('Carrinho');

            $session_cliente = $session->read('Cliente');

            $session_produto_ids = [];
            if(!empty($session_produto)) {
                foreach ($session_produto as $key => $item) {
                    $session_produto_ids[] = $key;
                }
            }

            if(!empty($session_produto_ids)) {
                $s_produtos = $Produtos
                    ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                    ->andWhere(['Produtos.preco > ' => 0.1])
                    ->where(['Produtos.id IN' => $session_produto_ids])
                    ->all();
            }else{
                $s_produtos = [];
            }

            $links_breadcrumb[] = [
                'url' => '/'.$Academia->slug,
                'name' => $Academia->shortname.' - loja'
            ];

            $links_breadcrumb[] = [
                'url' => '/minha-conta/indicacao-de-produtos/',
                'name' => 'Minhas Indicações'
            ];

            $this->set(compact('s_produtos', 'session_produto', 'session_cliente', 'produtos', 'pedidos', 'pre_orders', 'links_breadcrumb'));

        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($academia_id = null, $redirect = 'home'){

        $cliente = $this->Clientes->newEntity();
        if ($this->request->is(['post', 'put', 'patch'])) {

            $this->request->data['status_id']   = 1;
            $this->request->data['academia_id'] = $academia_id;
            $this->request->data['password_confirm'] = 1;

            $nome_completo = $this->request->data['name'];

            $nome_completo = explode(' ', $nome_completo);

            if($this->request->data['academia_id'] != 'Selecione uma academia...') {
                if(isset($nome_completo[1])) {

                    $Academias = TableRegistry::get('Academias');

                    $academia = $Academias
                        ->find('all')
                        ->where(['id' => $this->request->data['academia_id']])
                        ->first();

                    $this->request->data['cep'] = $academia->cep;
                    $this->request->data['address'] = $academia->address;
                    $this->request->data['number'] = $academia->number;
                    $this->request->data['area'] = $academia->area;
                    $this->request->data['city_id'] = $academia->city_id;

                    $this->request->data['mobile'] ? : $this->request->data['mobile'] = $this->request->data['telephone'];

                    $cliente = $this->Clientes->patchEntity($cliente, $this->request->data);

                    if ($CLIENTE = $this->Clientes->save($cliente)) {

                        $ClienteObjetivos  = TableRegistry::get('ClienteObjetivos');
                        $ClienteEsportes   = TableRegistry::get('ClienteEsportes');
                        $ClienteSugestoes = TableRegistry::get('ClienteSugestoes');

                        $sugestao = $ClienteSugestoes
                            ->find('all')
                            ->where(['ClienteSugestoes.email' => $this->request->data['email']])
                            ->first();

                        if(!empty($sugestao)) {
                            $data = [
                                'aceitou' => 1
                            ];

                            $sugestao = $ClienteSugestoes->patchEntity($sugestao, $data);
                            $sugestao = $ClienteSugestoes->save($sugestao);
                        }

                        if(isset($this->request->data['objetivos'][0])) {
                            foreach ($this->request->data['objetivos'] as $objetivo) {
                                $ObjetivoSave = $ClienteObjetivos->newEntity();
                                $ObjetivoSave = $ClienteObjetivos->patchEntity($ObjetivoSave, [
                                    'cliente_id' => $CLIENTE->id,
                                    'objetivo_id' => $objetivo,
                                ]);

                                $ClienteObjetivos->save($ObjetivoSave);
                            }
                        }

                        if(isset($this->request->data['esportes'][0])) {
                            foreach($this->request->data['esportes'] as $esporte){
                                $EsporteSave = $ClienteEsportes->newEntity();
                                $EsporteSave = $ClienteEsportes->patchEntity($EsporteSave, [
                                    'cliente_id'    => $CLIENTE->id,
                                    'esporte_id'    => $esporte,
                                ]);

                                $ClienteEsportes->save($EsporteSave);
                            }
                        }

                        $session    = $this->request->session();

                        $session->write('Cliente', $CLIENTE);

                        if($CLIENTE->academia_id >= 1){
                            $academia = $this->Clientes->Academias->get($CLIENTE->academia_id);
                            $session->write('Academia', $academia);
                        }
                        //if($session->write('Cliente.'.$cliente->id)){
                        if($redirect != ''){
                            echo 'Sucesso! <meta http-equiv="refresh" content="0;URL='.WEBROOT_URL.SLUG.$redirect.'" >';
                            echo '<script>';
                            echo 'location.href = "'.WEBROOT_URL.SLUG.$redirect.'"';
                            echo '</script>';
                        }else{
                            echo 'Sucesso! <meta http-equiv="refresh" content="0;URL='.WEBROOT_URL.SLUG.'mochila" >';
                            echo '<script>';
                            echo 'location.href = "'.WEBROOT_URL.SLUG.'mochila"';
                            echo '</script>';
                        }
                    } else {
                        echo 'Falha ao realizar cadastro... Tente novamente';
                    }
                } else {
                    echo '<span style="color: red">Você precisa colocar seu nome completo!</span>';
                }
            } else {
                echo '<span style="color: red">Você precisa selecionar sua academia!</span>';
            }

        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cliente = $this->Clientes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $birth = Time::now();
            $birth_date = array_reverse(explode('/', $this->request->data['birth']));
            $birth->setDate($birth_date[0], $birth_date[1], $birth_date[2]);

            $this->request->data['birth'] = $birth;
            //$this->request->data['birth'] = join('-', array_reverse(explode('/', $this->request->data['birth'])));
            $cliente = $this->Clientes->patchEntity($cliente, $this->request->data);
            if ($CLIENTE = $this->Clientes->save($cliente)) {

                $session    = $this->request->session();
                $session->write('Cliente', $CLIENTE);

                echo 'Sucesso! <meta http-equiv="refresh" content="0;URL='.WEBROOT_URL.'mochila/fechar-o-ziper" >';

            } else {
                echo 'Falha ao concluir o cadastro... Tente novamente';
            }
        }
    }


    /**
     * AJAX
     * CHECAR EMAIL JÁ CADASTRADO
     */
    public function check_email(){

        $email = false;

        if($this->request->is(['post', 'put'])){

            $cliente = $this->Clientes
                ->find('all')
                ->where(['Clientes.email' => $this->request->data['email']])
                ->first();

            if(count($cliente) >= 1){
                $email = true;
            }
        }

        echo $email;

        $this->viewBuilder()->layout('ajax');
        $this->render('add');
    }

    /**
     * AJAX
     * FILTRAR ALUNOS POR ACADEMIA
     * @param null $academia_id
     */
    public function filtro_academia($academia_id = null){

        $alunos = [];

        if($academia_id != null){
            $alunos = $this->Clientes
                ->find('list')
                ->where(['Clientes.academia_id' => $academia_id])
                ->toArray();
        }

        $this->set(compact('alunos'));

        $this->viewBuilder()->layout('ajax');
    }

    /**
     * AJAX
     * FILTRAR ALUNOS POR ACADEMIA E PROFESSOR
     * @param null $academia_id
     * @param null $professor_id
     */
    public function filtro_academia_professor($academia_id = null, $professor_id = null){

        $alunos = [];

        if($academia_id != null && $professor_id != null){
            $alunos = $this->Clientes
                ->find('list')
                ->where(['Clientes.academia_id' => $academia_id])
                ->andWhere(['Clientes.professor_id' => $professor_id])
                ->toArray();
        }

        $this->set(compact('alunos'));

        $this->viewBuilder()->layout('ajax');

        $this->render('filtro_academia');
    }

    public function avaliar_academia(){

        $session  = $this->request->session();
        $Cliente = $session->read('Cliente');

        if(!$Cliente) {
            $this->Flash->error('Você precisa estar logado para avaliar uma academia.');
            $this->redirect($this->referer());
        }

        if($this->request->is(['post', 'put'])){

            $Avaliacoes = TableRegistry::get('Avaliacoes');

            $ja_avaliou = $Avaliacoes
                ->find('all')
                ->where(['cliente_id' => $Cliente->id])
                ->andWhere(['academia_id' => $this->request->data['academia_id']])
                ->first();

            if(!$ja_avaliou) {
                $avaliacao = $Avaliacoes->newEntity();

                $this->request->data['cliente_id'] = $Cliente->id;

                $avaliacao = $Avaliacoes->patchEntity($avaliacao, $this->request->data);

                if($Avaliacoes->save($avaliacao)){
                    $this->Flash->success('Avaliação salva com sucesso!');
                    $this->redirect($this->referer());
                }else{
                    $this->Flash->error('Falha ao salvar avaliação. Tente novamente...');
                    $this->redirect($this->referer());
                }
            } else {
                $this->Flash->error('Você já avaliou essa academia.');
                $this->redirect($this->referer());
            }

        }

        $this->redirect($this->referer());
    }

    public function view_mensalidades(){
        $session  = $this->request->session();
        $Cliente  = $session->read('Cliente');
        $Academia = $session->read('Academia');

        if(!$Cliente) {
            $this->redirect('/login');
        }

        $PlanosClientes = TableRegistry::get('PlanosClientes');
        $Pedidos        = TableRegistry::get('Pedidos');

        $planos_ativos = $PlanosClientes
            ->find('all')
            ->contain(['AcademiaMensalidadesPlanos'])
            ->where(['PlanosClientes.cliente_id' => $Cliente->id])
            ->andWhere(['PlanosClientes.status' => 1])
            ->all();

        foreach ($planos_ativos as $pa) {
            $ids_planos_ativos[] = $pa->id;
        }

        if($ids_planos_ativos) {
            $pedidos_ativos = $Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.plano_cliente_id IN' => $ids_planos_ativos])
                ->all();
        }

        $now = Time::now();

        $planos_inativos = $PlanosClientes
            ->find('all')
            ->contain(['AcademiaMensalidadesPlanos'])
            ->where(['PlanosClientes.cliente_id' => $Cliente->id])
            ->andWhere(['PlanosClientes.status' => 0])
            ->orWhere(['PlanosClientes.data_vencimento <=' => $now])
            ->andWhere(['PlanosClientes.cliente_id' => $Cliente->id])
            ->all();

        foreach ($planos_inativos as $pi) {
            $ids_planos_inativos[] = $pi->id;
        }

        if($ids_planos_inativos) {
            $pedidos_inativos = $Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.plano_cliente_id IN' => $ids_planos_inativos])
                ->all();
        }

        $links_breadcrumb[] = [
            'url' => '/'.$Academia->slug,
            'name' => $Academia->shortname.' - loja'
        ];

        $links_breadcrumb[] = [
            'url' => '/minha-conta/mensalidades',
            'name' => 'Minhas Mensalidades'
        ];

        $this->set(compact('planos_ativos', 'pedidos_ativos', 'planos_inativos', 'pedidos_inativos', 'now', 'links_breadcrumb'));
    }

    /**
     * CANCELAR ASSINATURA
     */
    public function cancelar_plano($id = null) {

        $session  = $this->request->session();
        $Cliente = $session->read('Cliente');

        if(!$Cliente) {
            $this->redirect('/login');
        }

        $PlanosClientes = TableRegistry::get('PlanosClientes');

        $plano = $PlanosClientes
            ->find('all')
            ->where(['id' => $id])
            ->andWhere(['cliente_id' => $Cliente->id])
            ->first();

        if($plano) { 

            if($plano->type == 'Assinatura') {
                $ch = curl_init();

                $header = [
                    "Content-Type: application/json"
                ];

                $data = [
                    "api_token"  => IUGU_API_TOKEN,
                    "suspended"  => 'true'
                ];

                $data = json_encode($data);

                $options = array(CURLOPT_URL => "https://api.iugu.com/v1/subscriptions/".$plano->iugu_assinatura_id,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_CUSTOMREQUEST => "PUT",
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $header
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $resp_iugu_cancelar_assinatura = json_decode($response);

                if(!$resp_iugu_cancelar_assinatura->errors) {
                    $data = [
                        'status' => 0
                    ];

                    $plano = $PlanosClientes->patchEntity($plano, $data);

                    if($PlanosClientes->save($plano)) {
                        $this->Flash->success('Assinatura cancelada com sucesso!');
                        return $this->redirect('/mensalidades');
                    } else {
                        $this->Flash->error('Não foi possível cancelar a assinatura... Tente novamente...');
                        return $this->redirect('/mensalidades');
                    }
                } else {
                    $this->Flash->error('Não foi possível cancelar a assinatura... Tente novamente...');
                    return $this->redirect('/mensalidades');
                }
            } else {
                $data = [
                    'status' => 0
                ];

                $plano = $PlanosClientes->patchEntity($plano, $data);

                if($PlanosClientes->save($plano)) {
                    $this->Flash->success('Plano cancelado com sucesso!');
                    return $this->redirect('/mensalidades');
                } else {
                    $this->Flash->error('Não foi possível cancelar o plano... Tente novamente...');
                    return $this->redirect('/mensalidades');
                }
            }

        }

        return $this->redirect('/mensalidades');
        $this->render(false);
    }

    public function completar_cadastro() {

        $session  = $this->request->session();
        $Cliente = $session->read('Cliente');

        if ($this->request->is(['patch', 'post', 'put'])) {
            $cpf = $this->request->data('cpf');

            $cliente = $this->Clientes
                ->find('all')
                ->where(['cpf' => $cpf])
                ->andWhere(['id NOT IN' => $Cliente->id])
                ->first();

            if(!$cliente) {

                $Cliente = $this->Clientes->patchEntity($Cliente, $this->request->data);

                if($cliente_saved = $this->Clientes->save($Cliente)) {
                    $session->write('Cliente', $cliente_saved);

                    $resposta = [
                        'retorno' => 'sucesso'
                    ];
                } else {
                    $resposta = [
                        'retorno' => 'erro'
                    ];
                }
                
            } else {
                $email = explode('@', $cliente->email);
                $before_email = substr($email[0], -3);
                $after_email = substr($email[1], 0, 4);
                $email_escondido = '***'.$before_email.'@'.$after_email.'***';

                $resposta = [
                    'retorno' => 'erro',
                    'cliente_email' => $email_escondido
                ];
            }

            echo json_encode($resposta);
        }

        $this->viewBuilder()->layout('ajax');
        $this->render(false);
    }
}
