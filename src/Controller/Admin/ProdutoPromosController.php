<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ProdutoPromos Controller
 *
 * @property \App\Model\Table\ProdutoPromosTable $ProdutoPromos
 */
class ProdutoPromosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Produtos', 'Status']
        ];
        $produtoPromos = $this->paginate($this->ProdutoPromos);

        $this->set(compact('produtoPromos'));
        $this->set('_serialize', ['produtoPromos']);
    }

    /**
     * View method
     *
     * @param string|null $id Produto Promo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $produtoPromo = $this->ProdutoPromos->get($id, [
            'contain' => ['Produtos', 'Status']
        ]);

        $this->set('produtoPromo', $produtoPromo);
        $this->set('_serialize', ['produtoPromo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $produtoPromo = $this->ProdutoPromos->newEntity();
        if ($this->request->is('post')) {
            $produtoPromo = $this->ProdutoPromos->patchEntity($produtoPromo, $this->request->data);
            if ($this->ProdutoPromos->save($produtoPromo)) {
                $this->Flash->success(__('The produto promo has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The produto promo could not be saved. Please, try again.'));
            }
        }
        $produtos = $this->ProdutoPromos->Produtos->find('list', ['limit' => 200]);
        $status = $this->ProdutoPromos->Status->find('list', ['limit' => 200]);
        $this->set(compact('produtoPromo', 'produtos', 'status'));
        $this->set('_serialize', ['produtoPromo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Produto Promo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $produtoPromo = $this->ProdutoPromos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $produtoPromo = $this->ProdutoPromos->patchEntity($produtoPromo, $this->request->data);
            if ($this->ProdutoPromos->save($produtoPromo)) {
                $this->Flash->success(__('The produto promo has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The produto promo could not be saved. Please, try again.'));
            }
        }
        $produtos = $this->ProdutoPromos->Produtos->find('list', ['limit' => 200]);
        $status = $this->ProdutoPromos->Status->find('list', ['limit' => 200]);
        $this->set(compact('produtoPromo', 'produtos', 'status'));
        $this->set('_serialize', ['produtoPromo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Produto Promo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $produtoPromo = $this->ProdutoPromos->get($id);
        if ($this->ProdutoPromos->delete($produtoPromo)) {
            $this->Flash->success(__('The produto promo has been deleted.'));
        } else {
            $this->Flash->error(__('The produto promo could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
