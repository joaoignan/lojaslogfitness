<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Database\Schema\Table;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

/**
 * Produtos Controller
 *
 * @property \App\Model\Table\ProdutosTable $Produtos
 */
class ProdutosController extends AppController
{
    public function sincronizar_tudo() {
        $todos_marcas = TableRegistry::get('Marcas')
            ->find('all')
            ->all();

        foreach ($todos_marcas as $marca) {
            $marcas_ids_array[] = $marca->id;
        } 

        $todos_objetivos = TableRegistry::get('Objetivos')
            ->find('all')
            ->all();

        foreach ($todos_objetivos as $objetivo) {
            $objetivos_ids_array[] = $objetivo->id;
        } 

        $todas_categorias = TableRegistry::get('Categorias')
            ->find('all')
            ->all();

        foreach ($todas_categorias as $categoria) {
            $categorias_ids_array[] = $categoria->id;
        }

        $todas_subcategorias = TableRegistry::get('Subcategorias')
            ->find('all')
            ->all();

        foreach ($todas_subcategorias as $subcategoria) {
            $subcategorias_ids_array[] = $subcategoria->id;
        }

        $todos_substancias = TableRegistry::get('Substancias')
            ->find('all')
            ->all();

        foreach ($todos_substancias as $substancia) {
            $substancias_ids_array[] = $substancia->id;
        }

        $todos_produtos_base = TableRegistry::get('ProdutoBase')
            ->find('all')
            ->all();

        foreach ($todos_produtos_base as $produto_base) {
            $produtos_base_ids_array[] = $produto_base->id;
        }

        $todos_produtos = TableRegistry::get('Produtos')
            ->find('all')
            ->all();

        foreach ($todos_produtos as $produto) {
            $produtos_ids_array[] = $produto->id;
        }

        $todos_produto_objetivos = TableRegistry::get('ProdutoObjetivos')
            ->find('all')
            ->all();

        foreach ($todos_produto_objetivos as $produto_objetivo) {
            $produto_objetivos_ids_array[] = $produto_objetivo->id;
        }

        $todos_produto_categorias = TableRegistry::get('ProdutoCategorias')
            ->find('all')
            ->all();

        foreach ($todos_produto_categorias as $produto_categoria) {
            $produto_categorias_ids_array[] = $produto_categoria->id;
        }

        $todos_produto_subcategorias = TableRegistry::get('ProdutoSubcategorias')
            ->find('all')
            ->all();

        foreach ($todos_produto_subcategorias as $produto_subcategoria) {
            $produto_subcategorias_ids_array[] = $produto_subcategoria->id;
        }

        $todos_produto_substancias = TableRegistry::get('ProdutoSubstancias')
            ->find('all')
            ->all();

        foreach ($todos_produto_substancias as $produto_substancia) {
            $produto_substancias_ids_array[] = $produto_substancia->id;
        }

        do {
            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $data = [
                "marcas_ids_array" => $marcas_ids_array,
                "objetivos_ids_array" => $objetivos_ids_array,
                "categorias_ids_array" => $categorias_ids_array,
                "subcategorias_ids_array" => $subcategorias_ids_array,
                "substancias_ids_array" => $substancias_ids_array,
                "produtos_base_ids_array" => $produtos_base_ids_array,
                "produtos_ids_array" => $produtos_ids_array,
                "produto_objetivos_ids_array" => $produto_objetivos_ids_array,
                "produto_categorias_ids_array" => $produto_categorias_ids_array,
                "produto_subcategorias_ids_array" => $produto_subcategorias_ids_array,
                "produto_substancias_ids_array" => $produto_substancias_ids_array,
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.logfitness.com.br/tudo/sync?api_key=".LOGFITNESS_API_KEY,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_SSL_VERIFYPEER => false
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $api_novos = json_decode($response);

            if($api_novos->status_id == 1 && $api_novos->retorno_cod == 'sucesso') {
                $falha = 0;

                foreach ($api_novos->marcas as $marca) {
                    $new_marca_data = [
                        'id' => $marca->id,
                        'cod_interno' => $marca->cod_interno,
                        'name' => $marca->name,
                        'slug' => $marca->slug,
                        'image' => $marca->image,
                        'banner_fundo' => $marca->banner_fundo,
                        'status_id' => $marca->status_id
                    ];

                    $new_marca = TableRegistry::get('Marcas')->newEntity($new_marca_data);
                    
                    if(!TableRegistry::get('Marcas')->save($new_marca)) {
                        $falha = 1;
                    }
                }

                foreach ($api_novos->objetivos as $objetivo) {
                    $new_objetivo_data = [
                        'id' => $objetivo->id,
                        'name' => $objetivo->name,
                        'slug' => $objetivo->slug,
                        'texto_tarja' => $objetivo->texto_tarja,
                        'image' => $objetivo->image,
                        'color' => $objetivo->color,
                        'banner_lateral' => $objetivo->banner_lateral,
                        'banner_fundo' => $objetivo->banner_fundo,
                        'status_id' => $objetivo->status_id
                    ];

                    $new_objetivo = TableRegistry::get('Objetivos')->newEntity($new_objetivo_data);
                    
                    if(!TableRegistry::get('Objetivos')->save($new_objetivo)) {
                        $falha = 1;
                    }
                }

                foreach ($api_novos->categorias as $categoria) {
                    $new_categoria_data = [
                        'id' => $categoria->id,
                        'objetivo_id' => $categoria->objetivo_id,
                        'cod_interno_classe' => $categoria->cod_interno_classe,
                        'cod_interno_cat' => $categoria->cod_interno_cat,
                        'name' => $categoria->name,
                        'descricao' => $categoria->descricao,
                        'slug' => $categoria->slug,
                        'banner_lateral' => $categoria->banner_lateral,
                        'banner_fundo' => $categoria->banner_fundo,
                        'status_id' => $categoria->status_id
                    ];

                    $new_categoria = TableRegistry::get('Categorias')->newEntity($new_categoria_data);
                    
                    if(!TableRegistry::get('Categorias')->save($new_categoria)) {
                        $falha = 1;
                    }
                }

                foreach ($api_novos->subcategorias as $subcategoria) {
                    $new_subcategoria_data = [
                        'id' => $subcategoria->id,
                        'categoria_id' => $subcategoria->categoria_id,
                        'name' => $subcategoria->name,
                        'descricao' => $subcategoria->descricao,
                        'slug' => $subcategoria->slug,
                        'banner_lateral' => $subcategoria->banner_lateral,
                        'banner_fundo' => $subcategoria->banner_fundo,
                        'status_id' => $subcategoria->status_id
                    ];

                    $new_subcategoria = TableRegistry::get('Subcategorias')->newEntity($new_subcategoria_data);
                    
                    if(!TableRegistry::get('Subcategorias')->save($new_subcategoria)) {
                        $falha = 1;
                    }
                }

                foreach ($api_novos->substancias as $substancia) {
                    $new_substancia_data = [
                        'id' => $substancia->id,
                        'name' => $substancia->name,
                        'unidade_medida' => $substancia->unidade_medida,
                        'tipo' => $substancia->tipo
                    ];

                    $new_substancia = TableRegistry::get('Substancias')->newEntity($new_substancia_data);
                    
                    if(!TableRegistry::get('Substancias')->save($new_substancia)) {
                        $falha = 1;
                    }
                }

                foreach ($api_novos->produtos_base as $produto_base) {
                    $new_produto_base_data = [
                        'id' => $produto_base->id,
                        'name' => $produto_base->name,
                        'marca_id' => $produto_base->marca_id,
                        'tipo_produto_id' => $produto_base->tipo_produto_id,
                        'atributos' => $produto_base->atributos,
                        'dicas' => $produto_base->dicas,
                        'beneficios' => $produto_base->beneficios,
                        'combina' => $produto_base->combina,
                        'embalagem_conteudo' => $produto_base->embalagem_conteudo,
                        'forma_consumo' => $produto_base->forma_consumo,
                        'quando_tomar' => $produto_base->quando_tomar,
                        'peso' => $produto_base->peso,
                        'largura' => $produto_base->largura,
                        'altura' => $produto_base->altura,
                        'profundidade' => $produto_base->profundidade,
                        'propriedade_id' => $produto_base->propriedade_id,
                        'status_id' => $produto_base->status_id,
                    ];

                    $new_produto_base = TableRegistry::get('ProdutoBase')->newEntity($new_produto_base_data);
                    
                    if(!TableRegistry::get('ProdutoBase')->save($new_produto_base)) {
                        $falha = 1;
                    }
                }

                foreach ($api_novos->produtos as $produto) {
                    $new_produto_data = [
                        'id' => $produto->id,
                        'cod' => $produto->cod,
                        'cod_barras' => $produto->cod_barras,
                        'cod_interno' => $produto->cod_interno,
                        'produto_base_id' => $produto->produto_base_id,
                        'custo' => $produto->custo,
                        'porcentagem' => $produto->porcentagem,
                        'preco' => $produto->preco,
                        'preco_promo' => $produto->preco_promo,
                        'propriedade' => $produto->propriedade,
                        'porcao' => $produto->porcao,
                        'doses' => $produto->doses,
                        'embalagem' => $produto->embalagem,
                        'tamanho' => $produto->tamanho,
                        'cor' => $produto->cor,
                        'unidade_medida' => $produto->unidade_medida,
                        'ingredientes' => $produto->ingredientes,
                        'como_tomar' => $produto->como_tomar,
                        'fotos' => $produto->fotos,
                        'estoque' => 0,
                        'estoque_min' => $produto->estoque_min,
                        'visivel' => 0,
                        'status_id' => 1,
                        'type' => $produto->type,
                        'slug' => $produto->slug,
                        'alterado' => $produto->alterado,
                        'erp_promotion_setted' => $produto->erp_promotion_setted,
                        'promo_ini' => $produto->promo_ini,
                        'promo_fim' => $produto->promo_fim,
                        'views' => 0
                    ];

                    $new_produto = TableRegistry::get('Produtos')->newEntity($new_produto_data);
                    
                    if(!TableRegistry::get('Produtos')->save($new_produto)) {
                        $falha = 1;
                    }
                }

                foreach ($api_novos->produto_objetivos as $produto_objetivo) {
                    $new_produto_objetivo_data = [
                        'id' => $produto_objetivo->id,
                        'produto_base_id' => $produto_objetivo->produto_base_id,
                        'objetivo_id' => $produto_objetivo->objetivo_id,
                    ];

                    $new_produto_objetivo = TableRegistry::get('ProdutoObjetivos')->newEntity($new_produto_objetivo_data);
                    
                    if(!TableRegistry::get('ProdutoObjetivos')->save($new_produto_objetivo)) {
                        $falha = 1;
                    }
                }

                foreach ($api_novos->produto_categorias as $produto_categoria) {
                    $new_produto_categoria_data = [
                        'id' => $produto_categoria->id,
                        'produto_base_id' => $produto_categoria->produto_base_id,
                        'categoria_id' => $produto_categoria->categoria_id,
                    ];

                    $new_produto_categoria = TableRegistry::get('ProdutoCategorias')->newEntity($new_produto_categoria_data);
                    
                    if(!TableRegistry::get('ProdutoCategorias')->save($new_produto_categoria)) {
                        $falha = 1;
                    }
                }

                foreach ($api_novos->produto_subcategorias as $produto_subcategoria) {
                    $new_produto_subcategoria_data = [
                        'id' => $produto_subcategoria->id,
                        'produto_base_id' => $produto_subcategoria->produto_base_id,
                        'subcategoria_id' => $produto_subcategoria->subcategoria_id,
                    ];

                    $new_produto_subcategoria = TableRegistry::get('ProdutoSubcategorias')->newEntity($new_produto_subcategoria_data);
                    
                    if(!TableRegistry::get('ProdutoSubcategorias')->save($new_produto_subcategoria)) {
                        $falha = 1;
                    }
                }

                // foreach ($api_novos->produto_substancias as $produto_substancia) {
                //     $new_produto_substancia_data = [
                //         'id' => $produto_substancia->id,
                //         'produto_id' => $produto_substancia->produto_id,
                //         'substancia_id' => $produto_substancia->substancia_id,
                //         'valor' => $produto_substancia->valor,
                //     ];

                //     $ProdutoSubstancias = TableRegistry::get('ProdutoSubstancias');
                //     $prod_subs = $ProdutoSubstancias
                //         ->find('all')
                //         ->where(['ProdutoSubstancias.id' => $api_novos->produto_substancias->id])
                //         ->first();

                //     foreach ($prod_subs as $prod_sub) {
                //         $ProdutoSubstancias->delete($prod_sub);
                //     }

                //     $new_produto_substancia = TableRegistry::get('ProdutoSubstancias')->newEntity($new_produto_substancia_data);
                    
                //     if(!TableRegistry::get('ProdutoSubstancias')->save($new_produto_substancia)) {
                //         $falha = 1;
                //     }
                // }

                if($falha == 0) {
                    $this->Flash->success('Produtos sincronizados com sucesso!');
                } else {
                    $this->Flash->error('Falha ao sincronizar produtos, atualize a página!');
                }
            }
        } while($api_novos->status_id < 1);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($search_query = null){
        $this->sincronizar_tudo();

        if($this->request->is(['post', 'put'])){
            $this->redirect(['action' => 'index', $this->request->data['search']]);
        }

        if($search_query != null){

            $objetivos = TableRegistry::get('Objetivos')
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$search_query.'%'])
                ->toArray();

            $categorias = TableRegistry::get('Categorias')
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$search_query.'%'])
                ->toArray();

            $subcategorias = TableRegistry::get('Subcategorias')
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$search_query.'%'])
                ->toArray();

            if(count($objetivos) <= 0){
                $objetivos = [0];
            }

            if(count($categorias) <= 0){
                $categorias = [0];
            }

            if(count($subcategorias) <= 0){
                $subcategorias = [0];
            }

            $produto_id = $this->Produtos
                ->find('all')
                ->contain([
                    'Status',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->where(['Produtos.id' => $search_query])
                ->distinct('Produtos.id')
                ->first();

            $produto_cod = $this->Produtos
                ->find('all')
                ->contain([
                'Status',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoObjetivos.Objetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->where(['Produtos.cod_interno' => $search_query])
            ->distinct('Produtos.id')
            ->first();

            $search = [
                'OR' => [
                    'Marcas.name               LIKE'          => '%'.$search_query.'%',
                    'ProdutoBase.quando_tomar  LIKE'          => '%'.$search_query.'%',
                    'Produtos.embalagem        LIKE'          => '%'.$search_query.'%',
                    'Produtos.propriedade      LIKE'          => '%'.$search_query.'%',
                    'Produtos.como_tomar       LIKE'          => '%'.$search_query.'%',
                    'Produtos.cod_barras       LIKE'          => '%'.$search_query.'%',
                    'ProdutoBase.name          LIKE'          => '%'.$search_query.'%',
                    'ProdutoObjetivos.objetivo_id IN'         => $objetivos,
                    'ProdutoCategorias.categoria_id IN'       => $categorias,
                    'ProdutoSubcategorias.subcategoria_id IN' => $subcategorias,
                ]
            ];

            $produtos_find_query = $this->Produtos
                ->find('all')
                ->contain([
                    'Status',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->distinct('Produtos.id')
                ->where($search)
                ->first();


            if(!$produtos_find_query) {
                if ($produto_id) {
                    $search = [
                        'Produtos.id' => $search_query
                    ];
                } else {
                    $search = [
                        'Produtos.cod_interno LIKE' => '%'.$search_query
                    ];
                }
            }
        }else{
            $search = [];
        }

        $produtos_find = $this->Produtos
            ->find('all')
            ->contain([
                'Status',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoObjetivos.Objetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->distinct('Produtos.id')
            ->where($search);

        $produtos = $this->paginate($produtos_find);

        $this->set(compact('produtos'));
        $this->set('_serialize', ['produtos']);
    }

    public function gerenciador($search_query = null)
    {
        $this->sincronizar_tudo();

        $this->paginate = [
            'contain' => ['ProdutoBase', 'Status']
        ];

        $produtos = $this->paginate($this->Produtos);

        $this->set(compact('produtos'));
        $this->set('_serialize', ['produtos']);

        if($this->request->is(['post', 'put'])){

            if($this->request->data['preco'] != null){
                foreach($this->request->data['preco'] as $key => $item){
                    $produto = $this->Produtos->get($key);

                    $data       = [
                        'preco'         => parseFloatBR($this->request->data['preco'][$key]),
                        'preco_promo'   => parseFloatBR($this->request->data['preco_promo'][$key]),
                        'estoque'       => $this->request->data['estoque'][$key],
                        'visivel'       => $this->request->data['visivel'][$key],
                    ];

                    $produto = $this->Produtos->patchEntity($produto, $data);

                    if($this->Produtos->save($produto)){

                    }
                }

                $this->Flash->info('Atualização concluída');
                $this->redirect(['action' => 'gerenciador']);
            }
            else{
                $this->redirect(['action' => 'gerenciador', $this->request->data['search']]);
            }
        }


        if($search_query != null){

            $objetivos = TableRegistry::get('Objetivos')
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$search_query.'%'])
                ->toArray();

            $categorias = TableRegistry::get('Categorias')
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$search_query.'%'])
                ->toArray();

            $subcategorias = TableRegistry::get('Subcategorias')
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$search_query.'%'])
                ->toArray();

            if(count($objetivos) <= 0){
                $objetivos = [0];
            }

            if(count($categorias) <= 0){
                $categorias = [0];
            }

            if(count($subcategorias) <= 0){
                $subcategorias = [0];
            }

            $produto_id = $this->Produtos
                ->find('all')
                ->contain([
                    'Status',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->where(['Produtos.id' => $search_query])
                ->distinct('Produtos.id')
                ->first();

            $produto_cod = $this->Produtos
                ->find('all')
                ->contain([
                'Status',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoObjetivos.Objetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->where(['Produtos.cod_interno' => $search_query])
            ->distinct('Produtos.id')
            ->first();

            $search = [
                'OR' => [
                    'Marcas.name               LIKE'          => '%'.$search_query.'%',
                    'ProdutoBase.quando_tomar  LIKE'          => '%'.$search_query.'%',
                    'Produtos.embalagem        LIKE'          => '%'.$search_query.'%',
                    'Produtos.propriedade      LIKE'          => '%'.$search_query.'%',
                    'Produtos.como_tomar       LIKE'          => '%'.$search_query.'%',
                    'Produtos.cod_barras       LIKE'          => '%'.$search_query.'%',
                    'ProdutoBase.name          LIKE'          => '%'.$search_query.'%',
                    'ProdutoObjetivos.objetivo_id IN'         => $objetivos,
                    'ProdutoCategorias.categoria_id IN'       => $categorias,
                    'ProdutoSubcategorias.subcategoria_id IN' => $subcategorias,
                ]
            ];

            $produtos_find_query = $this->Produtos
                ->find('all')
                ->contain([
                    'Status',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->distinct('Produtos.id')
                ->where($search)
                ->first();


            if(!$produtos_find_query) {
                if ($produto_id) {
                    $search = [
                        'Produtos.id' => $search_query
                    ];
                } else {
                    $search = [
                        'Produtos.cod_interno LIKE' => '%'.$search_query
                    ];
                }
            }
        }else{
            $search = [];
        }

        $produtos_find = $this->Produtos
            ->find('all')
            ->contain([
                'Status',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoObjetivos.Objetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->distinct('Produtos.id')
            ->where($search);

        $this->paginate = [
            'contain' => ['ProdutoBase', 'Status', 'ProdutoBase.Marcas'],
            'order'   => ['Marcas.name' => 'asc'],
        ];

        $this->paginate['sortWhitelist'] = ['Marcas.name', 'Produtos.cod_barras', 'Produtos.preco', 'Produtos.preco_promo', 'Produtos.estoque', 'Produtos.visivel'];

        $produtos = $this->paginate($produtos_find);

        $this->set(compact('produtos', 'search_query'));
        $this->set('_serialize', ['produtos']);

    }


    /**
     * View method
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $produto = $this->Produtos->get($id, [
            'contain' => ['ProdutoBase', 'Status', 'PedidoItens', 'ProdutoComentarios']
        ]);

        $this->set('produto', $produto);
        $this->set('_serialize', ['produto']);
    }

    public function ajax_view($id = null){

        $ProdutoSubstancias = TableRegistry::get('ProdutoSubstancias');

        $produto = $this->Produtos->ProdutoBase
            ->find('all')
            ->contain(['ProdutoObjetivos', 'ProdutoCategorias', 'ProdutoSubcategorias'])
            ->where(['ProdutoBase.id' => $id])
            ->first();

        $produtos_substancia = $this->Produtos
            ->find('all')
            ->where(['produto_base_id' => $produto->id])
            ->all();

        $substancias = [];

        foreach ($produtos_substancia as $produto_substancia) {
            if(count($substancias) <= 0) {
                $substancias = $ProdutoSubstancias
                    ->find('all')
                    ->where(['produto_id' => $produto_substancia->id])
                    ->all();
            }
        }

        $textos = [];

        foreach ($produtos_substancia as $produto_substancia) {
            if(count($textos) <= 0) {
                $textos = [
                    'ingredientes' => $produto_substancia->ingredientes,
                    'como_tomar' => $produto_substancia->como_tomar,
                ];

                $produto['textos'] = $textos;
            }
        }

        foreach ($substancias as $substancia) {
            $substancias_list[] = [
                'substancia' => $substancia->substancia_id,
                'valor' => $substancia->valor
            ];
            $produto['substancias'] = $substancias_list;
        }


        $this->viewBuilder()->layout('ajax');

        echo $produto;

        $this->set('produto', $produto);
        $this->set('_serialize', ['produto']);
    }

    public function tabela_nutricional($id = null){

        $ProdutoSubstancias = TableRegistry::get('ProdutoSubstancias');

        $substancias = $ProdutoSubstancias
            ->find('all')
            ->contain('Substancias')
            ->where(['produto_id' => $id])
            ->all();

        $produto_substancias_list = [];

        if(count($substancias) >= 1) {
            foreach ($substancias as $substancia) {
                $produto_substancias_list[] = [
                    'substancia' => $substancia->substancia_id,
                    'valor' => $substancia->valor,
                    'unidade_medida' => $substancia->substancia->unidade_medida
                ];
            }
        }

        echo json_encode($produto_substancias_list);
        
        $this->viewBuilder()->layout('ajax');
        $this->render(false);
    }

    public function busca_ingredientes($id = null){

        $ProdutoIngredientes = TableRegistry::get('ProdutoIngredientes');

        $produto_ingredientes = $ProdutoIngredientes
            ->find('all')
            ->contain('Ingredientes')
            ->where(['ProdutoIngredientes.produto_id' => $id])
            ->order(['ProdutoIngredientes.ordem' => 'asc'])
            ->all();

        $produto_ingredientes_list = [];

        if(count($produto_ingredientes) >= 1) {
            foreach ($produto_ingredientes as $produto_ingrediente) {
                $produto_ingredientes_list[] = [
                    'ingrediente' => $produto_ingrediente->ingrediente_id
                ];
            }
        }

        echo json_encode($produto_ingredientes_list);
        
        $this->viewBuilder()->layout('ajax');
        $this->render(false);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($cod_barras) {
        $this->sincronizar_tudo();

        if(!$cod_barras) {
            return $this->redirect(['action' => 'gerenciador']);
        }

        $produto_existe = $this->Produtos
            ->find('all')
            ->where(['cod_barras' => $cod_barras])
            ->first();

        if($produto_existe) {
            $this->Flash->info('Este código de barras foi cadastrado com sucesso!');
            return $this->redirect(['action' => 'gerenciador', $cod_barras]);
        }

        if($this->request->is(['post'])) {
            $produto_base_id = $this->request->data['produto_base_id'];

            if($produto_base_id) {
                $ProdutoBase = TableRegistry::get('ProdutoBase');

                $produto_base = $ProdutoBase
                    ->find('all')
                    ->contain(['Marcas'])
                    ->where(['ProdutoBase.id' => $produto_base_id])
                    ->first();

                $produto_base_data = [
                    'forma_consumo' => $this->request->data['forma_consumo'],
                    'quando_tomar' => $this->request->data['quando_tomar'],
                    'atributos' => $this->request->data['atributos'],
                    'dicas' => $this->request->data['dicas'],
                    'beneficios' => $this->request->data['beneficios'],
                ];

                $produto_base = $ProdutoBase->patchEntity($produto_base, $produto_base_data);
                $ProdutoBase->save($produto_base);

                $tamanho = $this->request->data['tamanho'];
                $unidade_medida = $this->request->data['unidade_medida'];
                $embalagem = $this->request->data['embalagem'];
                $this->request->data['propriedade'] != null ? 
                    $propriedade = $this->request->data['propriedade'] : 
                    $propriedade = 'Sem Sabor';

                $preco = str_replace('.', '', $this->request->data['preco']);
                $preco = str_replace(',', '.', $preco);

                $preco_promo = str_replace('.', '', $this->request->data['preco_promo']);
                $preco_promo = str_replace(',', '.', $preco_promo);

                function montarSlugProduto($name, $tamanho, $unidade_medida, $embalagem, $marca, $propriedade) {
                    $string = $name.' '.$tamanho.' '.$unidade_medida.' '.$embalagem.' '.$marca.' '.$propriedade;

                    $slug = preg_replace(array("/(á|à|ã|â|ä|Á|À|Ã|Â|Ä)/","/(ç|Ç)/","/(é|è|ê|ë|É|È|Ê|Ë)/","/(í|ì|î|ï|Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö|Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü|Ú|Ù|Û|Ü)/","/(ñ|Ñ)/"),explode(" ","a c e i o u n"),$string);

                    $slug = str_replace('%','',$slug);
                    $slug = str_replace(' ', '-', $slug);

                    $slug = strtolower($slug);

                    return $slug;
                }

                $slug_produto = montarSlugProduto($produto_base->name, $tamanho, $unidade_medida, $embalagem, $produto_base->marca->name, $propriedade);

                foreach ($this->request->data['fotos'] as $foto) {
                    if($foto['error'] == 1) {
                        $this->Flash->error('Por favor, coloque imagens de até 2mb');
                        return $this->redirect('/admin/produtos/add/'.$cod_barras);
                    }
                }

                $foto_name = $slug_produto;
                $fotos = serialize($this->Produtos->newUploadImage2($this->request->data['fotos'], $foto_name));

                $data = [
                    "loja_id" => LOJA_ID,
                    "produto_base_id" => $produto_base_id,
                    "cod_barras" => $cod_barras,
                    "cod" => 21600000+$produto_base_id,
                    "cod_interno" => $this->request->data['cod_interno'],
                    "custo" => 0.00,
                    "porcentagem" => 0.00,
                    "preco" => $preco,
                    "preco_promo" => $preco_promo,
                    "propriedade" => $propriedade,
                    "porcao" => $this->request->data['porcao'],
                    "doses" => $this->request->data['doses'],
                    "embalagem" => $embalagem,
                    "tamanho" => $tamanho,
                    "unidade_medida" => $unidade_medida,
                    "ingredientes" => $this->request->data['ingredientes'],
                    "como_tomar" => $this->request->data['como_tomar'],
                    "substancias" => $this->request->data['substancias'],
                    "substancias_valor" => $this->request->data['substancias_valor'],
                    "fotos" => $fotos,
                    "estoque" => 0,
                    "estoque_min" => 1,
                    "visivel" => 0,
                    "status_id" => 1,
                    "type" => 1,
                    "slug" => $slug_produto
                ];

                $data = json_encode($data);
            } else {
                $name = $this->request->data['name'];
                $marca_id = $this->request->data['marca_id'];
                $tamanho = $this->request->data['tamanho'];
                $unidade_medida = $this->request->data['unidade_medida'];
                $propriedade = $this->request->data['propriedade'];
                $embalagem = $this->request->data['embalagem'];

                $preco = str_replace('.', '', $this->request->data['preco']);
                $preco = str_replace(',', '.', $preco);
                

                $preco_promo = str_replace('.', '', $this->request->data['preco_promo']);
                $preco_promo = str_replace(',', '.', $preco_promo);

                $marca = TableRegistry::get('Marcas')
                    ->find('all')
                    ->where(['id' => $marca_id])
                    ->first();

                function montarSlugProduto($name, $tamanho, $unidade_medida, $embalagem, $marca, $propriedade) {
                    $string = $name.' '.$tamanho.' '.$unidade_medida.' '.$embalagem.' '.$marca.' '.$propriedade;

                    $slug = preg_replace(array("/(á|à|ã|â|ä|Á|À|Ã|Â|Ä)/","/(ç|Ç)/","/(é|è|ê|ë|É|È|Ê|Ë)/","/(í|ì|î|ï|Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö|Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü|Ú|Ù|Û|Ü)/","/(ñ|Ñ)/"),explode(" ","a c e i o u n"),$string);

                    $slug = str_replace('%','',$slug);
                    $slug = str_replace(' ', '-', $slug);

                    $slug = strtolower($slug);

                    return $slug;
                }

                $slug_produto = montarSlugProduto($name, $tamanho, $unidade_medida, $embalagem, $marca->name, $propriedade);

                $foto_name = $slug_produto;
                $fotos = serialize($this->Produtos->newUploadImage2($this->request->data['fotos'], $foto_name));

                $data = [
                    "loja_id" => LOJA_ID,
                    "name" => $name,
                    "marca_id" => $marca_id,
                    "atributos" => $this->request->data['atributos'],
                    "dicas" => $this->request->data['dicas'],
                    "beneficios" => $this->request->data['beneficios'],
                    "combina" => 's:0:"";',
                    "embalagem_conteudo" => 'UN',
                    "forma_consumo" => $this->request->data['forma_consumo'],
                    "quando_tomar" => $this->request->data['quando_tomar'],
                    "peso" => 0,
                    "largura" => 1,
                    "altura" => 1,
                    "profundidade" => 1,
                    "propriedade_id" => 2,
                    "cod_barras" => $cod_barras,
                    "cod" => 21600000+$produto_base_id,
                    "cod_interno" => $this->request->data['cod_interno'],
                    "custo" => 0.00,
                    "porcentagem" => 0.00,
                    "preco" => $preco,
                    "preco_promo" => $preco_promo,
                    "propriedade" => $propriedade,
                    "porcao" => $this->request->data['porcao'],
                    "doses" => $this->request->data['doses'],
                    "embalagem" => $embalagem,
                    "tamanho" => $tamanho,
                    "unidade_medida" => $unidade_medida,
                    "ingredientes" => $this->request->data['ingredientes'],
                    "como_tomar" => $this->request->data['como_tomar'],
                    "objetivos" => $this->request->data['objetivos'],
                    "categorias" => $this->request->data['categorias'],
                    "subcategorias" => $this->request->data['subcategorias'],
                    "substancias" => $this->request->data['substancias'],
                    "substancias_valor" => $this->request->data['substancias_valor'],
                    "fotos" => $fotos,
                    "estoque" => 0,
                    "estoque_min" => 1,
                    "visivel" => 0,
                    "status_id" => 1,
                    "type" => 1,
                    "slug" => $slug_produto
                ];

                $data = json_encode($data);
            }

            $count = 0;

            //Salva produto na API
            do {

                $ch = curl_init();

                $header = [
                    "Content-Type: application/json"
                ];

                $options = array(CURLOPT_URL => "https://api.logfitness.com.br/produtos/add?api_key=".LOGFITNESS_API_KEY,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $header,
                    CURLOPT_SSL_VERIFYPEER => false
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $api_novo_produto = json_decode($response);

                if($api_novo_produto->status_id == 1 && $api_novo_produto->retorno_cod == 'sucesso') {
                    $this->Flash->success($api_novo_produto->mensagem);
                    return $this->redirect('/admin/produtos/gerenciador/'.$cod_barras);
                } else if($api_novo_produto->status_id == 2) {
                    $this->Flash->info($api_novo_produto->mensagem);
                    return $this->redirect('/admin/produtos/add/'.$cod_barras);
                }

                $count++;
            } while($api_novo_produto->status_id < 1 && $count < 100);
        }

        $objetivos = TableRegistry::get('Objetivos')
            ->find('all')
            ->contain(['Categorias', 'Categorias.Subcategorias'])
            ->leftJoin(
                ['Categorias' => 'categorias'],
                ['Categorias.objetivo_id = Objetivos.id']
            )
            ->leftJoin(
                ['Subcategorias' => 'subcategorias'],
                ['Subcategorias.categoria_id = Categorias.id']
            )
            ->distinct('Objetivos.id')
            ->all();

        $substancias = TableRegistry::get('Substancias')
            ->find('all')
            ->order(['name' => 'asc'])
            ->all();

        $produtos_list_array = $this->Produtos
            ->find('all')
            ->contain(['ProdutoBase', 'ProdutoSubstancias', 'ProdutoBase.Marcas'])
            ->all();

        foreach ($produtos_list_array as $produto_list_array) {
            if(!empty($produto_list_array->produto_substancias)) {
                $produtos_list[$produto_list_array->id] = $produto_list_array->produto_base->marca->name.' - '.$produto_list_array->produto_base->name.' '.$produto_list_array->propriedade.' '.$produto_list_array->tamanho.$produto_list_array->unidade_medida;
            }
        }

        $produtos_base_array = $this->Produtos->ProdutoBase
            ->find('all')
            ->contain(['Marcas'])
            ->order(['ProdutoBase.name' => 'asc'])
            ->all();

        foreach ($produtos_base_array as $produto_base_array) {
            $produtos_base_list[$produto_base_array->id] = $produto_base_array->name.' - '.$produto_base_array->marca->name;
        }

        $marcas_list = TableRegistry::get('Marcas')->find('list')->order(['name' => 'asc']);
        $substancias_list = TableRegistry::get('Substancias')->find('list')->order(['name' => 'asc']);

        $this->set(compact('cod_barras', 'produtos_base_list', 'marcas_list', 'objetivos', 'substancias_list', 'substancias', 'produtos_list'));
    }

    public function buscar_cod_barras() {
        $this->sincronizar_tudo();

        if($this->request->is(['post'])) {
            $cod_barras = trim($this->request->data['cod_barras']);

            $produto = $this->Produtos
                ->find('all')
                ->where(['cod_barras' => $cod_barras])
                ->first();

            if($produto) {
                $this->Flash->info('Esse produto já está cadastrado!');
                return $this->redirect(['action' => 'gerenciador', $cod_barras]);
            } else {
                return $this->redirect(['action' => 'escolha_tipo', $cod_barras]);
            }
        }
    }

    public function escolha_tipo($cod_barras) {
        $this->sincronizar_tudo();

        $this->set(compact('cod_barras'));
    }

    public function add_acessorio($cod_barras) {
        $this->sincronizar_tudo();

        if(!$cod_barras) {
            return $this->redirect(['action' => 'gerenciador']);
        }

        $produto_existe = $this->Produtos
            ->find('all')
            ->where(['cod_barras' => $cod_barras])
            ->first();

        if($produto_existe) {
            $this->Flash->error('Este código de barras foi cadastrado com sucesso');
            return $this->redirect(['action' => 'gerenciador', $cod_barras]);
        }

        if($this->request->is(['post'])) {
            $produto_base_id = $this->request->data['produto_base_id'];

            if($produto_base_id) {
                $ProdutoBase = TableRegistry::get('ProdutoBase');

                $produto_base = $ProdutoBase
                    ->find('all')
                    ->contain(['Marcas'])
                    ->where(['ProdutoBase.id' => $produto_base_id])
                    ->first();

                $produto_base_data = [
                    'atributos' => $this->request->data['atributos'],
                    'dicas' => $this->request->data['dicas'],
                    'beneficios' => $this->request->data['beneficios'],
                ];

                $produto_base = $ProdutoBase->patchEntity($produto_base, $produto_base_data);
                $produto_base_saved = $ProdutoBase->save($produto_base);

                $name = $produto_base_saved->name;
                $marca_id = $produto_base_saved->marca_id;

                $preco = str_replace('.', '', $this->request->data['preco']);
                $preco = str_replace(',', '.', $preco);
                
                $preco_promo = str_replace('.', '', $this->request->data['preco_promo']);
                $preco_promo = str_replace(',', '.', $preco_promo);

                $marca = TableRegistry::get('Marcas')
                    ->find('all')
                    ->where(['id' => $marca_id])
                    ->first();

                function randString($size){
                    $basic = 'abcdefghijklmnopqrstuvwxyz0123456789';

                    $return= "";

                    for($count= 0; $size > $count; $count++){
                        $return.= $basic[rand(0, strlen($basic) - 1)];
                    }

                    return $return;
                }

                $rand = randString(2);

                function montarSlugProduto($name, $marca, $rand) {
                    $string = $name.' '.$marca.' '.$rand;

                    $slug = preg_replace(array("/(á|à|ã|â|ä|Á|À|Ã|Â|Ä)/","/(ç|Ç)/","/(é|è|ê|ë|É|È|Ê|Ë)/","/(í|ì|î|ï|Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö|Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü|Ú|Ù|Û|Ü)/","/(ñ|Ñ)/"),explode(" ","a c e i o u n"),$string);

                    $slug = str_replace('%','',$slug);
                    $slug = str_replace(' ', '-', $slug);

                    $slug = strtolower($slug);

                    return $slug;
                }

                $slug_produto = montarSlugProduto($name, $marca->name, $rand);

                foreach ($this->request->data['fotos'] as $foto) {
                    if($foto['error'] == 1) {
                        $this->Flash->error('Por favor, coloque imagens de até 2mb');
                        return $this->redirect('/admin/produtos/add_acessorio/'.$cod_barras);
                    }
                }

                $foto_name = $slug_produto;
                $fotos = serialize($this->Produtos->newUploadImage2($this->request->data['fotos'], $foto_name));

                $data = [
                    "loja_id" => LOJA_ID,
                    "produto_base_id" => $produto_base_id,
                    "cod_barras" => $cod_barras,
                    "cod" => 21600000+$produto_base_id,
                    "cod_interno" => $this->request->data['cod_interno'],
                    "custo" => 0.00,
                    "porcentagem" => 0.00,
                    "preco" => $preco,
                    "preco_promo" => $preco_promo,
                    "propriedade" => $this->request->data['propriedade'],
                    "tamanho" => $this->request->data['tamanho'],
                    "cor" => $this->request->data['cor'],
                    "fotos" => $fotos,
                    "estoque" => 0,
                    "estoque_min" => 1,
                    "visivel" => 0,
                    "status_id" => 1,
                    "type" => 1,
                    "slug" => $slug_produto
                ];

                $data = json_encode($data);
            } else {
                $name = $this->request->data['name'];
                $marca_id = $this->request->data['marca_id'];

                $preco = str_replace('.', '', $this->request->data['preco']);
                $preco = str_replace(',', '.', $preco);
                
                $preco_promo = str_replace('.', '', $this->request->data['preco_promo']);
                $preco_promo = str_replace(',', '.', $preco_promo);

                $marca = TableRegistry::get('Marcas')
                    ->find('all')
                    ->where(['id' => $marca_id])
                    ->first();

                function randString($size){
                    $basic = 'abcdefghijklmnopqrstuvwxyz0123456789';

                    $return= "";

                    for($count= 0; $size > $count; $count++){
                        $return.= $basic[rand(0, strlen($basic) - 1)];
                    }

                    return $return;
                }

                $rand = randString(2);

                function montarSlugProduto($name, $marca, $rand) {
                    $string = $name.' '.$marca.' '.$rand;

                    $slug = preg_replace(array("/(á|à|ã|â|ä|Á|À|Ã|Â|Ä)/","/(ç|Ç)/","/(é|è|ê|ë|É|È|Ê|Ë)/","/(í|ì|î|ï|Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö|Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü|Ú|Ù|Û|Ü)/","/(ñ|Ñ)/"),explode(" ","a c e i o u n"),$string);

                    $slug = str_replace('%','',$slug);
                    $slug = str_replace(' ', '-', $slug);

                    $slug = strtolower($slug);

                    return $slug;
                }

                $slug_produto = montarSlugProduto($name, $marca->name, $rand);

                $foto_name = $slug_produto;
                $fotos = serialize($this->Produtos->newUploadImage2($this->request->data['fotos'], $foto_name));

                $data = [
                    "loja_id" => LOJA_ID,
                    "name" => $name,
                    "marca_id" => $marca_id,
                    "atributos" => $this->request->data['atributos'],
                    "dicas" => $this->request->data['dicas'],
                    "beneficios" => $this->request->data['beneficios'],
                    "combina" => 's:0:"";',
                    "embalagem_conteudo" => 'UN',
                    "peso" => 0,
                    "largura" => 1,
                    "altura" => 1,
                    "profundidade" => 1,
                    "propriedade_id" => 2,
                    "cod_barras" => $cod_barras,
                    "cod" => 21600000+$produto_base_id,
                    "cod_interno" => $this->request->data['cod_interno'],
                    "custo" => 0.00,
                    "porcentagem" => 0.00,
                    "preco" => $preco,
                    "preco_promo" => $preco_promo,
                    "propriedade" => $this->request->data['propriedade'],
                    "tamanho" => $this->request->data['tamanho'],
                    "cor" => $this->request->data['cor'],
                    "tipo_produto_id" => 2,
                    "objetivos" => $this->request->data['objetivos'],
                    "categorias" => $this->request->data['categorias'],
                    "subcategorias" => $this->request->data['subcategorias'],
                    "substancias" => [],
                    "substancias_valor" => [],
                    "fotos" => $fotos,
                    "estoque" => 0,
                    "estoque_min" => 1,
                    "visivel" => 0,
                    "status_id" => 1,
                    "type" => 1,
                    "slug" => $slug_produto
                ];

                $data = json_encode($data);
            }

            $count = 0;

            //Salva produto na API
            do {

                $ch = curl_init();

                $header = [
                    "Content-Type: application/json"
                ];

                $options = array(CURLOPT_URL => "https://api.logfitness.com.br/produtos/add?api_key=".LOGFITNESS_API_KEY,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $header,
                    CURLOPT_SSL_VERIFYPEER => false
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $api_novo_acessorio = json_decode($response);

                if($api_novo_acessorio->status_id == 1 && $api_novo_acessorio->retorno_cod == 'sucesso') {
                    $this->Flash->success($api_novo_acessorio->mensagem);
                    return $this->redirect('/admin/produtos/gerenciador/'.$cod_barras);
                } else if($api_novo_acessorio->status_id == 2) {
                    $this->Flash->info($api_novo_acessorio->mensagem);
                    return $this->redirect('/admin/produtos/add_acessorio/'.$cod_barras);
                }

                $count++;
            } while($api_novo_acessorio->status_id < 1 && $count < 100);
        }

        $objetivos = TableRegistry::get('Objetivos')
            ->find('all')
            ->contain(['Categorias', 'Categorias.Subcategorias'])
            ->leftJoin(
                ['Categorias' => 'categorias'],
                ['Categorias.objetivo_id = Objetivos.id']
            )
            ->leftJoin(
                ['Subcategorias' => 'subcategorias'],
                ['Subcategorias.categoria_id = Categorias.id']
            )
            ->distinct('Objetivos.id')
            ->all();

        $produtos_base_array = $this->Produtos->ProdutoBase
            ->find('all')
            ->contain(['Marcas'])
            ->where(['ProdutoBase.tipo_produto_id' => 2])
            ->order(['ProdutoBase.name' => 'asc'])
            ->all();

        foreach ($produtos_base_array as $produto_base_array) {
            $produtos_base_list[$produto_base_array->id] = $produto_base_array->name.' - '.$produto_base_array->marca->name;
        }

        $marcas_list = TableRegistry::get('Marcas')->find('list')->order(['name' => 'asc']);

        $this->set(compact('cod_barras', 'produtos_base_list', 'marcas_list', 'objetivos'));
        
    }    

    /**
     * Edit method
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null){

        $produto = $this->Produtos->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['post', 'put'])) {

            $this->request->data['combina'] = serialize($this->request->data['combina_list']);

            $status_id = $this->request->data['status_id'];

            $this->request->data['status_id'] = 1;
            $this->request->data['alterado'] = 1;
            $produto_base = $this->Produtos->ProdutoBase->get($produto->produto_base_id);
            $produto_base = $this->Produtos->ProdutoBase->patchEntity($produto_base, $this->request->data);

            $ProdutoBase = $this->Produtos->ProdutoBase->save($produto_base);
            if($ProdutoBase){

            }

            $Objetivos            = TableRegistry::get('Objetivos');
            $Categorias           = TableRegistry::get('Categorias');
            $Subcategorias        = TableRegistry::get('Subcategorias');
            $ProdutoObjetivos     = TableRegistry::get('ProdutoObjetivos');
            $ProdutoCategorias    = TableRegistry::get('ProdutoCategorias');
            $ProdutoSubcategorias = TableRegistry::get('ProdutoSubcategorias');
            
            // RELACIONANDO PRODUTO -> OBJETIVOS

            if (isset($this->request->data['categorias_ids'][0])) {
                $objetivos_array = [];
                foreach ($this->request->data['categorias_ids'] as $objetivo) {
                    $objetivo = explode('-', $objetivo);

                    if($objetivo[0] == 'o') {
                        $objetivos_array[] = $objetivo[1];

                        $produto_objetivo = $ProdutoObjetivos
            ->find('all')
                            ->where(['produto_base_id' => $produto_base->id])
                            ->andWhere(['objetivo_id' => $objetivo[1]])
                            ->all();

                        if (count($produto_objetivo) <= 0) {
                            $produtoObjetivo = $ProdutoObjetivos->newEntity();
                            $produtoObjetivo = $ProdutoObjetivos->patchEntity($produtoObjetivo, [
                                'produto_base_id' => $produto_base->id,
                                'objetivo_id' => $objetivo[1]
                            ]);

                            $ProdutoObjetivos->save($produtoObjetivo);
                        }
                    }
                }

                // REMOVER OBJETIVOS DESMARCADOS

                if(count($objetivos_array) >= 1) {
                    $produto_objetivo = $ProdutoObjetivos
                        ->find('all')
                        ->where(['produto_base_id' => $produto_base->id])
                        ->andWhere(['objetivo_id NOT IN' => $objetivos_array])
                        ->all();

                    if (count($produto_objetivo) >= 1) {
                        foreach ($produto_objetivo as $produto_objetivo_delete) {
                            $ProdutoObjetivos->delete($produto_objetivo_delete);
                        }
                    }
                }
            }else{
                $produto_objetivo = $ProdutoObjetivos
                    ->find('all')
                    ->where(['produto_base_id' => $produto_base->id])
                    ->all();
                if(count($produto_objetivo) >= 1){
                    foreach($produto_objetivo as $produto_objetivo_delete){
                        $ProdutoObjetivos->delete($produto_objetivo_delete);
                    }
                }
            }

            // RELACIONANDO PRODUTO -> CATEGORIAS

            if (isset($this->request->data['categorias_ids'][0])) {
                $categorias_array = [];
                foreach ($this->request->data['categorias_ids'] as $categoria) {
                    $categoria = explode('-', $categoria);
                    
                    if($categoria[0] == 'c') {
                        $categorias_array[] = $categoria[1];

                        $produto_categoria = $ProdutoCategorias
                            ->find('all')
                            ->where(['produto_base_id' => $produto_base->id])
                            ->andWhere(['categoria_id' => $categoria[1]])
                            ->all();

                        if (count($produto_categoria) <= 0) {
                            $produtoCategoria = $ProdutoCategorias->newEntity();
                            $produtoCategoria = $ProdutoCategorias->patchEntity($produtoCategoria, [
                                'produto_base_id' => $produto_base->id,
                                'categoria_id' => $categoria[1]
                            ]);

                            $ProdutoCategorias->save($produtoCategoria);

                            $categoria = $Categorias
                                ->find('all')
                                ->where(['id' => $categoria[1]])
                                ->first();

                            $produtoObjetivo = $ProdutoObjetivos->newEntity();
                            $produtoObjetivo = $ProdutoObjetivos->patchEntity($produtoObjetivo, [
                                'produto_base_id' => $produto_base->id,
                                'objetivo_id' => $categoria->objetivo_id
                            ]);

                            $ProdutoObjetivos->save($produtoObjetivo);
                        }
                    }
                }

                // REMOVER CATEGORIAS DESMARCADAS
                
                if(count($categorias_array) >= 1) {
                    $produto_categoria = $ProdutoCategorias
                        ->find('all')
                        ->where(['produto_base_id' => $produto_base->id])
                        ->andWhere(['categoria_id NOT IN' => $categorias_array])
                        ->all();

                    if (count($produto_categoria) >= 1) {
                        foreach ($produto_categoria as $produto_categoria_delete) {
                            $ProdutoCategorias->delete($produto_categoria_delete);
                        }
                    }
                }
            }else{
                $produto_categoria = $ProdutoCategorias
                    ->find('all')
                    ->where(['produto_base_id' => $produto_base->id])
                    ->all();
                if(count($produto_categoria) >= 1){
                    foreach($produto_categoria as $produto_categoria_delete){
                        $ProdutoCategorias->delete($produto_categoria_delete);
                    }
                }
            }


            // RELACIONANDO PRODUTO -> SUBCATEGORIAS

            if (isset($this->request->data['categorias_ids'][0])) {
                $subcategorias_array = [];
                foreach ($this->request->data['categorias_ids'] as $subcategoria) {
                    $subcategoria = explode('-', $subcategoria);
                    
                    if($subcategoria[0] == 's') {
                        $subcategorias_array[] = $subcategoria[1];

                        $produto_subcategoria = $ProdutoSubcategorias
            ->find('all')
                            ->where(['produto_base_id' => $produto_base->id])
                            ->andWhere(['subcategoria_id' => $subcategoria[1]])
            ->all();

                        if (count($produto_subcategoria) <= 0) {
                            $produtoSubcategoria = $ProdutoSubcategorias->newEntity();
                            $produtoSubcategoria = $ProdutoSubcategorias->patchEntity($produtoSubcategoria, [
                                'produto_base_id' => $produto_base->id,
                                'subcategoria_id' => $subcategoria[1]
                                ]);

                            $ProdutoSubcategorias->save($produtoSubcategoria);

                            $subcategoria = $Subcategorias
                                ->find('all')
                                ->where(['id' => $subcategoria[1]])
                                ->first();

                            $produtoCategoria = $ProdutoCategorias->newEntity();
                            $produtoCategoria = $ProdutoCategorias->patchEntity($produtoCategoria, [
                                'produto_base_id' => $produto_base->id,
                                'categoria_id' => $subcategoria->categoria_id
                                ]);

                            $ProdutoCategorias->save($produtoCategoria);

                            $categoria = $Categorias
                                ->find('all')
                                ->where(['id' => $subcategoria->categoria_id])
                                ->first();

                            $produtoObjetivo = $ProdutoObjetivos->newEntity();
                            $produtoObjetivo = $ProdutoObjetivos->patchEntity($produtoObjetivo, [
                                'produto_base_id' => $produto_base->id,
                                'objetivo_id' => $categoria->objetivo_id
                                 ]);

                            $ProdutoObjetivos->save($produtoObjetivo);
                        }
                    }
                }

                // REMOVER SUBCATEGORIAS DESMARCADAS

                if(count($subcategorias_array) >= 1) {
                    $produto_subcategoria = $ProdutoSubcategorias
                        ->find('all')
                        ->where(['produto_base_id' => $produto_base->id])
                        ->andWhere(['subcategoria_id NOT IN' => $subcategorias_array])
                        ->all();

                    if (count($produto_subcategoria) >= 1) {
                        foreach ($produto_subcategoria as $produto_subcategoria_delete) {
                            $ProdutoSubcategorias->delete($produto_subcategoria_delete);
                        }
                    }
                }
            }else{
                $produto_subcategoria = $ProdutoSubcategorias
                    ->find('all')
                    ->where(['produto_base_id' => $produto_base->id])
                    ->all();
                if(count($produto_subcategoria) >= 1){
                    foreach($produto_subcategoria as $produto_subcategoria_delete){
                        $ProdutoSubcategorias->delete($produto_subcategoria_delete);
                    }
                }
            }

            /**
             * RELACIONANDO PRODUTO -> SUBSTANCIAS
             */
            $ProdutoSubstancias = TableRegistry::get('ProdutoSubstancias');

            $produto_substancias = $ProdutoSubstancias
                ->find('all')
                ->where(['produto_id' => $produto->id])
                ->all();

            foreach ($produto_substancias as $produto_substancia) {
                $ProdutoSubstancias->delete($produto_substancia);
            }

            if (isset($this->request->data['valor_substancia'][0]) && $this->request->data['valor_substancia'][0] != null)
            {
                $count = 0;
                foreach ($this->request->data['substancia'] as $substancia) {
                    if($this->request->data['valor_substancia'][$count] != null) {
                        $produtoSubstancia = $ProdutoSubstancias->newEntity();
                        $produtoSubstancia = $ProdutoSubstancias->patchEntity($produtoSubstancia, [
                            'produto_id' => $produto->id,
                            'substancia_id' => $substancia,
                            'valor' => $this->request->data['valor_substancia'][$count]
                        ]);

                        $ProdutoSubstancias->save($produtoSubstancia);
                         }

                    $count++;
                }
            }

            $this->request->data['status_id'] = $status_id;
            $produto = $this->Produtos->patchEntity($produto, $this->request->data);

            if ($this->Produtos->save($produto)) {
                $this->Flash->success(__('Produto salvo com sucesso!'));
                return $this->redirect(['action' => 'edit', $id]);
            } else {
                $this->Flash->error(__('Falha ao salvar produto. Tente novamente...'));
                }
            }

        $marcas         = $this->Produtos->ProdutoBase->Marcas->find('list')->where(['status_id' => 1]);
        $propriedades   = $this->Produtos->ProdutoBase->Propriedades->find('list');
        $produtoBase    = $this->Produtos->ProdutoBase->find('list');
        $produto_base   = $this->Produtos->ProdutoBase
            ->find('all')
            ->where(['id' => $produto->produto_base_id])
            ->first();

        $categorias = TableRegistry::get('Categorias')
            ->find('list')
            ->all();

        $produto_objetivos_selected = TableRegistry::get('ProdutoObjetivos')
            ->find('all')
            ->where(['produto_base_id' => $produto_base->id])
            ->all();

        $produto_categorias_selected = TableRegistry::get('ProdutoCategorias')
            ->find('all')
            ->contain(['Categorias'])
            ->where(['ProdutoCategorias.produto_base_id' => $produto_base->id])
            ->all();

        $produto_subcategorias_selected = TableRegistry::get('ProdutoSubcategorias')
            ->find('all')
            ->contain(['Subcategorias'])
            ->where(['ProdutoSubcategorias.produto_base_id' => $produto_base->id])
            ->all();

        foreach ($produto_objetivos_selected as $pos) {
            $categorias_selected[] = 'o-'.$pos->objetivo_id;
            }

        $cont1 = 0;

        foreach ($produto_categorias_selected as $pcs) {
            $categorias_selected[] = 'c-'.$pcs->categoria_id;
            if($cont1 == 0) {
                $categorias_descricoes = (string)$pcs->categoria->descricao;
                } else {
                $categorias_descricoes = $categorias_descricoes.'<br>'.(string)$pcs->categoria->descricao;
                }
            $cont1++;
        }

        foreach ($produto_subcategorias_selected as $pss) {
            $categorias_selected[] = 's-'.$pss->subcategoria_id;
            if($cont1 == 0) {
                $categorias_descricoes = (string)$pss->subcategoria->descricao;
                } else {
                $categorias_descricoes = $categorias_descricoes.'<br>'.(string)$pss->subcategoria->descricao;
            }
            $cont1++;
        }

        $produto_objetivos = TableRegistry::get('Objetivos')
            ->find('all')
            ->all();

        $categorias = TableRegistry::get('Categorias')
            ->find('all')
            ->contain(['Objetivos'])
            ->all();

        $subcategorias = TableRegistry::get('Subcategorias')
            ->find('all')
            ->contain(['Categorias', 'Categorias.Objetivos'])
            ->all();

        foreach ($produto_objetivos as $key => $produto_objetivo) {
            $categorias_list['Objetivos']['o-'.$produto_objetivo->id] = $produto_objetivo->name;
        }

        foreach ($categorias as $key => $categoria) {
            $categorias_list['Categorias']['c-'.$categoria->id] = $categoria->objetivo->name.' - '.$categoria->name;
                }

        foreach ($subcategorias as $key => $subcategoria) {
            $categorias_list['Subcategorias']['s-'.$subcategoria->id] = $subcategoria->categoria->objetivo->name.' - '.$subcategoria->categoria->name.' - '.$subcategoria->name;
        }

        $substancias = TableRegistry::get('Substancias')
            ->find('all')
            ->order(['name' => 'asc'])
            ->all();

        $substancias_list = TableRegistry::get('Substancias')->find('list')->order(['name' => 'asc']);;

        $produto_substancias = TableRegistry::get('ProdutoSubstancias')
            ->find('all')
            ->contain('Substancias')
            ->where(['ProdutoSubstancias.produto_id' => $id])
            ->all();

        $produto_substancias_list = [];

        if(count($produto_substancias) >= 1) {
            foreach ($produto_substancias as $produto_substancia) {
                $produto_substancias_list[] = [
                    'substancia' => $produto_substancia->substancia_id,
                    'valor' => $produto_substancia->valor,
                    'unidade_medida' => $produto_substancia->substancia->unidade_medida
                ];
            }
        } else {

            $produtos_base = $this->Produtos
                ->find('all')
                ->where(['produto_base_id' => $produto->produto_base_id])
                ->all();

            foreach ($produtos_base as $produto_b) {
                if(count($produto_substancias) <= 0) {
                    $produto_substancias = TableRegistry::get('ProdutoSubstancias')
                        ->find('all')
                        ->contain('Substancias')
                        ->where(['ProdutoSubstancias.produto_id' => $produto_b->id])
                        ->all();
                } else {
                    if($produto->porcao == null || $produto->doses == null) {
                        $porcao_base = $produto_b->porcao;
                        $doses_base = $produto_b->doses;
            } else {
                        $porcao_base = $produto->porcao;
                        $doses_base = $produto->doses;
                    }
                }
            }

            if(count($produto_substancias) >= 1) {
                foreach ($produto_substancias as $produto_substancia) {
                    $produto_substancias_list[] = [
                        'substancia' => $produto_substancia->substancia_id,
                        'valor' => $produto_substancia->valor,
                        'unidade_medida' => $produto_substancia->substancia->unidade_medida
                    ];
                }
            }
        }

        if($produto->ingredientes == null || $produto->como_tomar == null || $produto->porcao == null || $produto->unidade_medida == null ) {
            $produtos_base = $this->Produtos
                ->find('all')
                ->where(['produto_base_id' => $produto->produto_base_id])
                ->all();

            foreach ($produtos_base as $produto_b) {
                if($produto->ingredientes == null) {
                    if($produto_b->ingredientes != null) {
                        $produto->ingredientes = $produto_b->ingredientes;
                    }
                }

                if($produto->porcao == null) {
                    if($produto_b->porcao != null) {
                        $produto->porcao = $produto_b->porcao;
                    }
                }

                if($produto->unidade_medida == null) {
                    if($produto_b->unidade_medida != null) {
                        $produto->unidade_medida = $produto_b->unidade_medida;
                    }
                }

                if($produto->como_tomar == null) {
                    if($produto_b->como_tomar != null) {
                        $produto->como_tomar = $produto_b->como_tomar;
                    }
                }
            }
        }

        $produtos_list_array = $this->Produtos
            ->find('all')
            ->contain(['ProdutoBase', 'ProdutoSubstancias'])
            ->all();

        foreach ($produtos_list_array as $produto_list_array) {
            if(!empty($produto_list_array->produto_substancias)) {
                $produtos_list[$produto_list_array->id] = $produto_list_array->produto_base->name.' '.$produto_list_array->propriedade;
            }
        }

        $categorias_combina_selected = unserialize($produto_base->combina);

        $sabores_list = TableRegistry::get('Sabores')->find('list')->order(['name' => 'asc']);
        $status = $this->Produtos->Status->find('list', ['limit' => 200]);
        $this->set(compact(
            'marcas',
            'produto',
            'produtoBase',
            'produto_base',
            'porcao_base',
            'doses_base',
            'status',
            'propriedades',
            'categorias',
            'substancias',
            'produtos_list',
            'substancias_list',
            'sabores_list',
            'categorias_list',
            'categorias_descricoes',
            'produto_substancias_list',
            'categorias_combina_selected',
            'categorias_selected',
            'produto_objetivos'));
        $this->set('_serialize', ['produto']);
    }

    public function edit_acessorio($id = null){

        $loja = TableRegistry::get('Academias')
            ->find('all')
            ->where(['id' => 601])
            ->first();

        $produto = $this->Produtos->get($id, [
            'contain' => ['ProdutoBase', 'ProdutoBase.Marcas']
        ]);

        $produto_base = $this->Produtos->ProdutoBase->get($produto->produto_base_id, [
            'contain' => []
        ]);

        if($this->request->is(['post', 'put', 'patch'])) {

            $tem_foto = false;

            foreach ($this->request->data['fotos'] as $foto) {
                if($foto['error'] != 4) {
                    $tem_foto = true;
                }
            }

            $produto_base = $this->Produtos->ProdutoBase->patchEntity($produto_base, $this->request->data);

            if($this->Produtos->ProdutoBase->save($produto_base)) {

                if($tem_foto) {
                    $this->request->data['fotos'] = serialize($this->Produtos->newUploadImage($this->request->data['fotos'], $produto->slug));
                } else {
                    $this->request->data['fotos'] = $produto->fotos;
                }

                $this->request->data['preco'] = str_replace('.', '', $this->request->data['preco']);
                $this->request->data['preco'] = str_replace(',', '.', $this->request->data['preco']);

                $this->request->data['preco_promo'] = str_replace('.', '', $this->request->data['preco_promo']);
                $this->request->data['preco_promo'] = str_replace(',', '.', $this->request->data['preco_promo']);

                $produto = $this->Produtos->patchEntity($produto, $this->request->data);

                if($produto_saved = $this->Produtos->save($produto)) {

                    
                    Email::configTransport('cliente', [
                        'className' => 'Smtp',
                        'host' => 'mail.logfitness.com.br',
                        'port' => 587,
                        'timeout' => 30,
                        'username' => EMAIL_USER,
                        'password' => EMAIL_SENHA,
                        'client' => null,
                        'tls' => null,
                    ]);

                    $email = new Email();

                    $email
                        ->transport('cliente')
                        ->template('produto_editado')
                        ->emailFormat('html')
                        ->from([EMAIL_USER => EMAIL_NAME])
                        ->to('log001@logfitness.com.br')
                        ->subject('Edição de produto')
                        ->set([
                            'linkProduto'       => WEBROOT_URL.$loja->slug.'/produto/'.$produto->slug,
                            'idProduto'         => $produto->id
                        ])
                        ->send();

                    $this->Flash->success('Acessório editado com sucesso!');
                    return $this->redirect('/admin/produtos/gerenciador/'.$produto->cod_barras);
                } else {
                    $this->Flash->error('Falha ao editar acessório, tente novamente');
                    return $this->redirect('/admin/produtos/edit_acessorio/'.$id);
                }
            } else {
                $this->Flash->error('Falha ao editar acessório, tente novamente');
                return $this->redirect('/admin/produtos/edit_acessorio/'.$id);
            }
        }

        $this->set(compact('produto'));
        $this->set('_serialize', ['produto']);
    }

    public function import_csv(){

        $produto_save = $this->Produtos->newEntity();

        $this->Flash->info('Informe o arquivo a ser importado. <br>
                    Importante: O mesmo deve estar no formato CSV com valores separados por vírgulas "," não formatado
                    e estar populado com dados em ordem');

        if ($this->request->is('post')) {

            function customfgetcsv($handle, $length, $separator = ';')
            {

                if (($buffer = fgets($handle, $length)) !== false) {
                    //return explode($separator, iconv("ISO-8859-15", "UTF-8", $buffer));
                    return explode($separator, iconv("ISO-8859-15", "UTF-8", $buffer));
                }
                return false;
            }

            $messages['success'] = '<strong>Produtos Adicionadas:</strong><br/>';
            $messages['ignore'] = '<br/><strong>Produtos Ignoradas:</strong><br/>';
            $messages['error'] = '<br/><strong>Erros encontrados:</strong><br/>';

            $c_success = 0;
            $c_ignore = 0;
            $c_error = 0;

            $row = 0;
            $csv_array = [];
            $fotos = serialize(['default.jpg']);

            $handle = fopen($this->request->data['csv']['tmp_name'], "r");
            $separator = ';';

            if ($handle) {
                while (($line = fgetcsv($handle, 1000, $separator)) !== false) {
                    $line = array_map('utf8_encode', $line);
                    $data = $line;

                    $num = count($data);

                    if ($num == 14 && $row >= 1) {

                        $marca = $this->Produtos->ProdutoBase->Marcas
                            ->find('all')
                            ->where(['name LIKE' => '%' . $data[1] . '%'])
                            ->first();

                        $propriedade = $this->Produtos->ProdutoBase->Propriedades
                            ->find('all')
                            ->where(['name LIKE' => '%' . $data[4] . '%'])
                            ->first();

                        if (count($marca) >= 1) {

                            $dataSave = [
                                'name'              => $data[0],
                                'marca_id'          => $marca->id,
                                'atributos'         => $data[12],
                                'dicas'             => $data[13],
                                'embalagem_conteudo'=> $data[2],
                                'doses'             => $data[3],
                                'propriedade_id'    => count($propriedade) >= 1 ? $propriedade->id : '',
                                'status_id'         => 1,
                            ];

                            $produto_base = $this->Produtos->ProdutoBase
                                ->find('all')
                                ->where([
                                    'name'                  => $dataSave['name'],
                                    'marca_id'              => $dataSave['marca_id'],
                                    'embalagem_conteudo'    => $dataSave['embalagem_conteudo'],
                                    //'doses'                 => $dataSave['doses'],
                                    'propriedade_id'        => $dataSave['propriedade_id'],
                                ])
                                ->first();

                            //debug($produto_base);

                            if (count($produto_base) <= 0) {
                                $ProdutoBase = TableRegistry::get('ProdutoBase');

                                $produto_base_save = $ProdutoBase->newEntity();
                                $produto_base_save = $ProdutoBase->patchEntity($produto_base_save, $dataSave);

                                $PRODUTO_BASE = $ProdutoBase->save($produto_base_save);

                                $produto_base = $PRODUTO_BASE;
                            }

                            if($produto_base) {

                                $this->Produtos->exists(['slug' => sanitizeString($data[0] . '-' . $data[2] . '-' . $data[5] . '-' . $marca->name)]) ?
                                    $slug = sanitizeString(date('YmdHis').rand() . '-' . $data[0] . '-' . $data[2] . '-' . $data[5] . '-' . $marca->name) :
                                    $slug = sanitizeString($data[0] . '-' . $data[2] . '-' . $data[5] . '-' . $marca->name);

                                $dataSave = [
                                    'cod' => (21600000 + $produto_base->id),
                                    'produto_base_id' => $produto_base->id,
                                    'custo' => $data[6],
                                    'preco' => $data[7],
                                    'preco_promo' => $data[8],
                                    'propriedade' => $data[5],
                                    'visivel' => 1,
                                    'status_id' => 1,
                                    'fotos' => $fotos,
                                    'slug' => $slug,
                                ];

                                $produto = $this->Produtos->newEntity();
                                $produto = $this->Produtos->patchEntity($produto, $dataSave);

                                if ($PRODUTO = $this->Produtos->save($produto)) {
                                    $messages['success'] .= ' -> ' . $data[0] . ' ' . $data[2] . ' ' . $data[5] . ' ' . $marca->name . '<br/>';
                                    $c_success++;

                                    /**
                                     * RELACIONANDO PRODUTO -> CATEGORIAS
                                     */
                                    $ProdutoCategorias = TableRegistry::get('ProdutoCategorias');
                                    $Categorias = TableRegistry::get('Categorias');

                                    if (isset($data[11])) {
                                        //$data[11] = iconv("ISO-8859-15", "UTF-8", $data[11]);
                                        $categorias_array = explode('+', $data[11]);
                                        if (!empty(array_filter($categorias_array))) {
                                            $categorias = $Categorias
                                                ->find('all')
                                                ->where(['Categorias.name IN' => $categorias_array])
                                                ->all();

                                            foreach ($categorias as $categoria) {

                                                $produto_categoria = $ProdutoCategorias
                                                    ->find('all')
                                                    ->where(['produto_base_id' => $PRODUTO->produto_base_id])
                                                    ->andWhere(['categoria_id' => $categoria->id])
                                                    ->all();

                                                if (count($produto_categoria) <= 0) {
                                                    $produtoCategoria = $ProdutoCategorias->newEntity();
                                                    $produtoCategoria = $ProdutoCategorias->patchEntity($produtoCategoria, [
                                                        'produto_base_id' => $PRODUTO->produto_base_id,
                                                        'categoria_id' => $categoria->id
                                                    ]);

                                                    if ($ProdutoCategorias->save($produtoCategoria)) {

                                                    }
                                                }
                                            }
                                        }
                                    }

                                    /**
                                     * RELACIONANDO PRODUTO -> OBJETIVOS
                                     */
                                    $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');
                                    $Objetivos = TableRegistry::get('Objetivos');

                                    if (isset($data[10])) {
                                        //$data[10] = iconv("ISO-8859-15", "UTF-8", $data[10]);
                                        $objetivos_array = explode('+', $data[10]);
                                        if (!empty(array_filter($objetivos_array))) {
                                            if (!isset($objetivos_array[1])) {
                                                $objetivos_array[] = $data[10];
                                            }
                                            $objetivos = $Objetivos
                                                ->find('all')
                                                ->where(['Objetivos.name IN' => $objetivos_array])
                                                ->all();

                                            foreach ($objetivos as $objetivo) {

                                                $produto_objetivo = $ProdutoObjetivos
                                                    ->find('all')
                                                    ->where(['produto_base_id' => $PRODUTO->produto_base_id])
                                                    ->andWhere(['objetivo_id' => $objetivo->id])
                                                    ->all();

                                                if (count($produto_objetivo) <= 0) {
                                                    $produtoObjetivo = $ProdutoObjetivos->newEntity();
                                                    $produtoObjetivo = $ProdutoObjetivos->patchEntity($produtoObjetivo, [
                                                        'produto_base_id' => $PRODUTO->produto_base_id,
                                                        'objetivo_id' => $objetivo->id
                                                    ]);

                                                    if ($ProdutoObjetivos->save($produtoObjetivo)) {

                                                    }
                                                }
                                            }
                                        }
                                    }

                                } else {
                                    $messages['error'] .= ' Falha ao salvar -> ' . $data[0] . ' ' . $data[2] . ' ' . $data[5] . ' ' . $marca->name . '<br/>';
                                    $c_error++;
                                }
                            }
                        } else {
                            $messages['error'] .= ' Marca não encontrada -> ' . $data[1] . '<br/>';
                            $c_ignore++;
                        }

                        $csv_array[] = $data;
                    }
                    $row++;
                }

            } else {
                $this->Flash->error('Falha ao carregar arquivo CSV. Tente novamente...');
            }

            fclose($handle);

            !$c_success ? $messages['success']  .= ' :: Nenhum <br/>' : $messages['success']   .= ' :: TOTAL = ' . $c_success  . '<br/>';
            !$c_error   ? $messages['error']    .= ' :: Nenhum <br/>' : $messages['error']     .= ' :: TOTAL = ' . $c_error    . '<br/>';
            !$c_ignore  ? $messages['ignore']   .= ' :: Nenhum <br/>' : $messages['ignore']    .= ' :: TOTAL = ' . $c_ignore   . '<br/>';

            $this->Flash->info($messages['success'] . $messages['ignore'] . $messages['error']);
            //$this->redirect(['action' => 'import_csv']);
        }
    }

    public function visualizacoes() {
        $produtos_find = $this->Produtos
            ->find('all')
            ->contain([
                'Status',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoObjetivos.Objetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->distinct('Produtos.id')
            ->where(['Produtos.views >=' => 1])
            ->order(['Produtos.views' => 'desc']);

        $produtos = $this->paginate($produtos_find);

        $this->set(compact('produtos'));
        $this->set('_serialize', ['produtos']);
    }

    public function envia_foto() {
        
        $foto = $this->Produtos->newEntity();
        if ($this->request->is('post')) {

            //apenas para debug
            // var_dump( $_FILES );

            $servidor = 'ftp.logfitness.com.br';
            $caminho_absoluto = '/api.logfitness.com.br/webroot/img/produtos/';
            $arquivo = $_FILES['arquivo'];

            $con_id = ftp_connect($servidor) or die( 'Não conectou em: '.$servidor );
            ftp_login( $con_id, 'apilogfitness', 'zBv7u$34' );

            ftp_put( $con_id, $caminho_absoluto.$arquivo['name'], $arquivo['tmp_name'], FTP_BINARY );

            if ($this->Produtos->save($foto)) {
                $this->Flash->success('Foto salva com sucesso.');
                return $this->redirect(['action' => 'envia_foto']);
            }else {
                $this->Flash->error('Falha ao salvar foto. Tente novamente.');
                return $this->redirect(['action' => 'envia_foto']);
            }
        }
    }
}