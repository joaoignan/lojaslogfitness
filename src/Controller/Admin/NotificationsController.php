<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Notifications Controller
 *
 * @property \App\Model\Table\NotificationsTable $Notifications
 */
class NotificationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index(){

        $this->paginate = [
            'contain'   => ['Status'],
            'order'     => ['Notifications.id' => 'desc']
        ];
        $notifications = $this->paginate($this->Notifications);

        $this->set(compact('notifications'));
        $this->set('_serialize', ['notifications']);
    }

    /**
     * View method
     *
     * @param string|null $id Notification id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null){

        $notification = $this->Notifications->get($id, [
            'contain' => ['Status', 'NotificationEsportes', 'NotificationObjetivos', 'NotificationViews']
        ]);

        $this->set('notification', $notification);
        $this->set('_serialize', ['notification']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $notification = $this->Notifications->newEntity();
        if ($this->request->is('post')) {
            $notification = $this->Notifications->patchEntity($notification, $this->request->data);
            if ($Notification = $this->Notifications->save($notification)) {

                $NotificationEsportes   = TableRegistry::get('NotificationEsportes');
                $NotificationObjetivos  = TableRegistry::get('NotificationObjetivos');

                /**
                 * NOTIFICAÇÕES ATRELADAS A OBJETIVOS
                 */
                if(isset($this->request->data['objetivos'][0]) && $this->request->data['global'] == 0) {
                    foreach ($this->request->data['objetivos'] as $objetivo) {

                        /*$notification_objetivo = $NotificationObjetivos
                            ->find('all')
                            ->where(['notification_id' => $Notification->id])
                            ->andWhere(['objetivo_id' => $objetivo])
                            ->all();

                        if (count($notification_objetivo) <= 0) {*/
                        $ObjetivoSave = $NotificationObjetivos->newEntity();
                        $ObjetivoSave = $NotificationObjetivos->patchEntity($ObjetivoSave, [
                            'notification_id' => $notification->id,
                            'objetivo_id' => $objetivo,
                        ]);

                        $NotificationObjetivos->save($ObjetivoSave);
                        //}
                    }
                }

                /**
                 * NOTIFICAÇÕES ATRELADAS A ESPORTES
                 */
                if(isset($this->request->data['esportes'][0]) && $this->request->data['global'] == 0) {
                    foreach ($this->request->data['esportes'] as $esporte) {
                        $EsporteSave = $NotificationEsportes->newEntity();
                        $EsporteSave = $NotificationEsportes->patchEntity($EsporteSave, [
                            'notification_id' => $notification->id,
                            'esporte_id' => $esporte,
                        ]);

                        $NotificationEsportes->save($EsporteSave);
                    }
                }

                $this->Flash->success(__('Notificação salva com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha ao salvar notificação. Tente novamente...'));
            }
        }

        $status = $this->Notifications->Status->find('list');
        $objetivos          = TableRegistry::get('Objetivos')->find('list');
        $esportes           = TableRegistry::get('Esportes')->find('list');

        $this->set(compact('notification', 'status', 'objetivos', 'esportes'));
        $this->set('_serialize', ['notification']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Notification id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $notification = $this->Notifications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $notification = $this->Notifications->patchEntity($notification, $this->request->data);
            if ($this->Notifications->save($notification)) {
                $this->Flash->success(__('Notificação salva com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha ao salvar notificação. Tente novamente...'));
            }
        }

        $status             = $this->Notifications->Status->find('list');
        $objetivos          = TableRegistry::get('Objetivos')->find('list');
        $esportes           = TableRegistry::get('Esportes')->find('list');

        $selected_esportes  = TableRegistry::get('NotificationEsportes')
            ->find('list', [
                'keyField'      => 'esporte_id',
                'valueField'    => 'esporte_id'
            ])
            ->toArray();

        $selected_objetivos  = TableRegistry::get('NotificationObjetivos')
            ->find('list', [
                'keyField'      => 'esporte_id',
                'valueField'    => 'esporte_id'
            ])
            ->toArray();

        $this->set(compact('notification', 'status', 'objetivos', 'esportes', 'selected_esportes', 'selected_objetivos'));
        $this->set('_serialize', ['notification']);
    }

}
