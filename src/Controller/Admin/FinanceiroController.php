<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Financeiro Controller
 */
class FinanceiroController extends AppController
{

    public function index()
    {
        $session  = $this->request->session();
        $session->delete('TransactionsAdmin');
        
        $TransactionsSession = $session->read('TransactionsAdmin');

        $Pedidos        = TableRegistry::get('Pedidos');
        $PlanosClientes = TableRegistry::get('PlanosClientes');

        $ch = curl_init();

        $header = [
            "Content-Type: application/json"
        ];

        $options = array(CURLOPT_URL => "https://api.iugu.com/v1/financial_transaction_requests?api_token=".IUGU_API_TOKEN."&limit=200",
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => FALSE,
            CURLOPT_HTTPHEADER => $header
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $resp_iugu_transactions = json_decode($response);

        foreach ($resp_iugu_transactions->items as $transaction) {
            $ids_transactions[$transaction->id] = $transaction->invoice_id;
            $data_transactions[$transaction->id] = $transaction;
        }

        $pedidos = $Pedidos
            ->find('all')
            ->where(['pedido_status_id >=' => 3])
            ->andWhere(['pedido_status_id NOT IN' => [7, 8]])
            ->andWhere(['comissao_status IN' => [1, 2]])
            ->all();

        foreach ($ids_transactions as $key => $id_transaction) {

            foreach ($pedidos as $pedido) {
                if($id_transaction == $pedido->tid) {
                    $transactions[] = $data_transactions[$key];

                    $time = new Time($data_transactions[$key]->scheduled_date);
                    $today = time::now();

                    $dias_antecipados = $time->diff($today);

                    $dias_antecipados = $dias_antecipados->days + 1;

                    $taxa_antecipacao = pow((1 + (2.5 / 100)),($dias_antecipados/30));
                    
                    $valor = explode(' ', $data_transactions[$key]->client_share);
                    if($valor[1] == 'BRL') {
                        $valor = str_replace(',', '.', $valor[0]);
                    } else {
                        $valor = str_replace(',', '.', $valor[1]);
                    }
                    $valor = (double)$valor;
                    $qtd_parcelas = (int)$data_transactions[$key]->number_of_installments;
                    $parcela_atual = (int)$data_transactions[$key]->installment;

                    $custo = $valor - ($valor/$taxa_antecipacao);

                    $custo = number_format($custo, 2);

                    $custo_final[$data_transactions[$key]->id] = $custo;

                    $valor_final[$data_transactions[$key]->id] = $valor - $custo;

                    $transactions_id[$data_transactions[$key]->id] = $data_transactions[$key]->id;
                }
            }

        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            foreach ($this->request->data as $key => $value) {
                $transaction = explode('_', $key);
                $transactionsAdmin[] = $transaction[1];
            }

            $session->write('TransactionsAdmin.'.$transactionsAdmin, $transactionsAdmin);

            return $this->redirect('/admin/financeiro/confirmar');
        }

        $this->set(compact('transactions', 'custo_final', 'valor_final'));
    }

    public function confirmar()
    {
        $session  = $this->request->session();
        $TransactionsAdmin = $session->read('TransactionsAdmin');

        $ch = curl_init();

        $header = [
            "Content-Type: application/json"
        ];

        $options = array(CURLOPT_URL => "https://api.iugu.com/v1/financial_transaction_requests?api_token=".IUGU_API_TOKEN."&limit=100",
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => FALSE,
            CURLOPT_HTTPHEADER => $header
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $resp_iugu_transactions = json_decode($response);

        foreach ($resp_iugu_transactions->items as $transaction) {
            $ids_transactions[$transaction->id] = $transaction->invoice_id;
            $data_transactions[$transaction->id] = $transaction;
        }

        foreach ($ids_transactions as $key => $id_transaction) {
            foreach($TransactionsAdmin as $transaction_admin) {
                if(in_array($key, $transaction_admin)) {
                    $transactions[] = $data_transactions[$key];

                    $time = new Time($data_transactions[$key]->scheduled_date);
                    $today = time::now();

                    $dias_antecipados = $time->diff($today);

                    $dias_antecipados = $dias_antecipados->days + 1;

                    $taxa_antecipacao = pow((1 + (2.5 / 100)),($dias_antecipados/30));
                    
                    $valor = explode(' ', $data_transactions[$key]->client_share);
                    if($valor[1] == 'BRL') {
                        $valor = str_replace(',', '.', $valor[0]);
                    } else {
                        $valor = str_replace(',', '.', $valor[1]);
                    }
                    $valor = (double)$valor;
                    $qtd_parcelas = (int)$data_transactions[$key]->number_of_installments;
                    $parcela_atual = (int)$data_transactions[$key]->installment;

                    $custo = $valor - ($valor/$taxa_antecipacao);

                    $custo = number_format($custo, 2);

                    $custo_final[$data_transactions[$key]->id] = $custo;

                    $valor_final[$data_transactions[$key]->id] = $valor - $custo;

                    $transactions_id[$data_transactions[$key]->id] = $data_transactions[$key]->id;
                }
            }
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            foreach ($TransactionsAdmin as $tid) {
                foreach ($tid as $trans_id) {
                    $transaction_list[] = $trans_id;
                }
            }

            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $data = [
                "api_token"    => IUGU_API_TOKEN,
                "transactions" => $transaction_list
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/financial_transaction_requests/advance",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $this->Flash->success('Fatura(s) antecipada(s) com sucesso!');
            return $this->redirect('/admin/financeiro');
        }

        $this->set(compact('transactions', 'custo_final', 'valor_final'));
    }

    public function saque()
    {
        $ch = curl_init();

        $header = [
            "Content-Type: application/json"
        ];

        $options = array(CURLOPT_URL => "https://api.iugu.com/v1/accounts/".IUGU_ID."/?api_token=".IUGU_API_TOKEN,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => FALSE,
            CURLOPT_HTTPHEADER => $header
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $resp_iugu_conta_info = json_decode($response);

        if($this->request->is(['patch', 'post', 'put'])) {

            $valor_saque = $this->request->data['valor_transferencia'];

            $valor_saque = str_replace(".","",$valor_saque);
            $valor_saque = str_replace(",",".",$valor_saque);

            if($valor_saque >= 5.0) {
                $ch = curl_init();

                $header = [
                    "Content-Type: application/json"
                ];

                $valor_saque = number_format($valor_saque, 2, '.', ',');

                $data = [
                    "api_token" => IUGU_API_TOKEN,
                    "amount"    => $valor_saque
                ];

                $data = json_encode($data);

                $options = array(CURLOPT_URL => "https://api.iugu.com/v1/accounts/".IUGU_ID."/request_withdraw",
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $header
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $resp_iugu_saque = json_decode($response);

                if(!$resp_iugu_saque->errors) {
                    $this->Flash->success('Saque no valor de '.$valor_saque.' realizado com sucesso!');
                    return $this->redirect('/admin/financeiro/saque');
                } else {
                    $this->Flash->error('Falha ao tentar realizar saque... Tente novamente...');
                    return $this->redirect('/admin/financeiro/saque');
                }
            } else {
                $this->Flash->error('Falha ao tentar realizar saque... Valor deve ser mais que R$5');
                return $this->redirect('/admin/financeiro/saque');
            }
        }

        $this->set(compact('resp_iugu_conta_info'));
    }
}
