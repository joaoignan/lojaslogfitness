<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;

/**
 * Fornecedores Controller
 *
 * @property \App\Model\Table\FornecedoresTable $Fornecedores
 */
class FranqueadosController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {

        $Users = TableRegistry::get('Users');

        $this->paginate = [
            'contain'    => [],
            'order'      => ['Users.id' => 'desc'],
            'conditions' => ['Users.group_id' => 7]
        ];

        $this->set('franqueados', $this->paginate($Users));
        $this->set('_serialize', ['franqueados']);
    }

    public function comissoes_academias_franqueado($mes = null, $ano = null)
    {

        $Users = TableRegistry::get('Users');
        $user = $Users->get($this->Auth->user('id'));

        if($this->request->is(['post'])){
            $this->redirect(['action' => 'comissoes_academias_franqueado', $this->request->data['mes'], $this->request->data['ano']]);
        }

        if($mes == null || $ano == null){
            $this->set('select_date', true);
        }else {
            $Pedidos = TableRegistry::get('Pedidos');

            $pedidos_comissoes = $Pedidos
                ->find('all')
                ->contain('Academias')
                ->select([
                    'Pedidos.academia_id',
                    'Academias.shortname',
                    'venda' => 'SUM(Pedidos.valor)'
                ])
                ->where(['Pedidos.user_id' => $user->id])
                ->andWhere(['Pedidos.pedido_status_id >=' => 3])
                ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
                ->andWhere(['Pedidos.comissao_status IN' => [1, 2]])
                ->andWhere(['Pedidos.created LIKE' => $ano . '-' . $mes . '%'])
                ->group('academia_id')
                ->order(['Academias.name' => 'asc'])
                ->all();

            $mes_anterior = $mes;
            $ano_anterior = $ano;

            if($mes_anterior == 1) {
                $ano_anterior -= 1;
                $mes_anterior = 12;
            } else {
                $mes_anterior -= 1;
                if($mes_anterior <= 9) {
                    $mes_anterior = '0'.$mes_anterior;    
                }
            }

            $vendas_mes_anterior = $Pedidos
                ->find('all')
                ->contain('Academias')
                ->select([
                    'Pedidos.academia_id',
                    'venda' => 'SUM(Pedidos.valor)'
                ])
                ->where(['Pedidos.user_id' => $user->id])
                ->andWhere(['Pedidos.pedido_status_id >=' => 3])
                ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
                ->andWhere(['Pedidos.comissao_status IN' => [1, 2]])
                ->andWhere(['Pedidos.created LIKE' => $ano_anterior . '-' . $mes_anterior . '%'])
                ->group('academia_id')
                ->order(['Academias.name' => 'asc'])
                ->all();

            $this->set(compact('pedidos_comissoes', 'vendas_mes_anterior'));
        }

        $this->set(compact('mes', 'ano'));
    }

    public function comissoes_franqueado()
    {
        $Users = TableRegistry::get('Users');
        $user = $Users->get($this->Auth->user('id'));

        $FranqueadoComissoes = TableRegistry::get('FranqueadoComissoes');

        $comissoes = $FranqueadoComissoes
            ->find('all')
            ->where(['user_id' => $user->id])
            ->all();

        $this->set(compact('comissoes'));
    }

    public function comissoes_academias($mes = null, $ano = null)
    {

        if($this->request->is(['post'])){
            $this->redirect(['action' => 'comissoes_academias', $this->request->data['mes'], $this->request->data['ano']]);
        }

        if($mes == null || $ano == null){
            $this->set('select_date', true);
        }else {
            $Pedidos = TableRegistry::get('Pedidos');

            $pedidos_comissoes = $Pedidos
                ->find('all')
                ->contain('Users')
                ->select([
                    'Pedidos.user_id',
                    'Users.name',
                    'venda' => 'SUM(Pedidos.valor)'
                ])
                ->where(['Pedidos.pedido_status_id >=' => 3])
                ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
                ->andWhere(['Pedidos.comissao_status IN' => [1, 2]])
                ->andWhere(['Pedidos.created LIKE' => $ano . '-' . $mes . '%'])
                ->group('user_id')
                ->order(['Users.name' => 'asc'])
                ->all();

            $mes_anterior = $mes;
            $ano_anterior = $ano;

            if($mes_anterior == 1) {
                $ano_anterior -= 1;
                $mes_anterior = 12;
            } else {
                $mes_anterior -= 1;
                if($mes_anterior <= 9) {
                    $mes_anterior = '0'.$mes_anterior;    
                }
            }

            $vendas_mes_anterior = $Pedidos
                ->find('all')
                ->contain('Users')
                ->select([
                    'Pedidos.user_id',
                    'venda' => 'SUM(Pedidos.valor)'
                ])
                ->where(['Pedidos.pedido_status_id >=' => 3])
                ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
                ->andWhere(['Pedidos.comissao_status IN' => [1, 2]])
                ->andWhere(['Pedidos.created LIKE' => $ano_anterior . '-' . $mes_anterior . '%'])
                ->group('user_id')
                ->order(['Users.name' => 'asc'])
                ->all();

            $this->set(compact('pedidos_comissoes', 'vendas_mes_anterior'));
        }

        $this->set(compact('mes', 'ano'));
    }

    public function comissoes()
    {
        $Users               = TableRegistry::get('Users');
        $FranqueadoComissoes = TableRegistry::get('FranqueadoComissoes');

        $comissoes = $FranqueadoComissoes
            ->find('all')
            ->contain(['Users'])
            ->all();

        if($this->request->is(['post', 'put'])) {
            Email::configTransport('franqueado', [
                'className' => 'Smtp',
                'host' => 'mail.logfitness.com.br',
                'port' => 587,
                'timeout' => 30,
                'username' => EMAIL_USER,
                'password' => EMAIL_SENHA,
                'client' => null,
                'tls' => null,
            ]);

            $deu_certo = 0;

            foreach($this->request->data['paga'] as $key => $item){
                $franqueado_comissao = $FranqueadoComissoes->get($key);
                $data       = [
                    'paga'  => $item,
                ];

                $paga_antigo = $franqueado_comissao->paga;

                $franqueado_comissao = $FranqueadoComissoes->patchEntity($franqueado_comissao, $data);

                $franqueado_comissao_paga = $Users
                    ->find('all')
                    ->where(['id' => $franqueado_comissao->user_id])
                    ->first();

                if($FranqueadoComissoes->save($franqueado_comissao)){
                    if($paga_antigo == 0 && $item == 1) {
                        $email = new Email();

                        $email
                            ->transport('franqueado')
                            ->template('comissao_paga_franqueado')
                            ->emailFormat('html')
                            ->from([EMAIL_USER => EMAIL_NAME])
                            ->to($franqueado_comissao_paga->email)
                            ->subject(EMAIL_NAME' - Comissão paga!')
                            ->set([
                                'name' => $franqueado_comissao_paga->name
                            ])
                            ->send();
                    }
                    $deu_certo = 1;
                }
            }

            if($deu_certo == 1) {
                $this->Flash->success('Informações alteradas com sucesso!');
                $this->redirect('/admin/franqueados/comissoes');
            } else {
                $this->Flash->error('Falha ao salvar informações.. Tente novamente..');
                $this->redirect('/admin/franqueados/comissoes');
            }
        }

        $this->set(compact('comissoes'));
    }

    public function fechar_comissoes()
    {
        $Users                      = TableRegistry::get('Users');
        $Pedidos                    = TableRegistry::get('Pedidos');
        $FranqueadoComissoes        = TableRegistry::get('FranqueadoComissoes');
        $FranqueadoComissoesPedidos = TableRegistry::get('FranqueadoComissoesPedidos');

        $franqueado_comissao_pedidos = $FranqueadoComissoesPedidos
            ->find('list', [
                'keyField'      => 'pedido_id',
                'valueField'    => 'pedido_id'
            ])
            ->toArray();

        if(empty($franqueado_comissao_pedidos)){
            $franqueado_comissao_pedidos = [0];
        }

        $franqueados = $Users
            ->find('all')
            ->where(['group_id' => 7])
            ->all();

        foreach ($franqueados as $franqueado) {

            $max_date = new \DateTime(date('Y-m').'-01');

            $pedidos = $Pedidos
                ->find('all')
                ->contain([
                    'Clientes',
                    'Academias',
                    'Academias.Cities',
                    'PedidoStatus',
                ])
                ->where(['Pedidos.user_id'                    => $franqueado->id])
                ->andWhere(['Pedidos.created <'               => $max_date])
                ->andWhere(['Pedidos.comissao_status IN'      => [1, 2]])
                ->andWhere(['Pedidos.pedido_status_id >='     => 3])
                ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
                ->andWhere(['Pedidos.id NOT IN'               => $franqueado_comissao_pedidos])
                ->all();

            $valor = 0.0;
            $comissao_valor = 0.0;

            foreach ($pedidos as $pedido) {
                $valor += $pedido->valor;

                $comissao_valor += $pedido->valor * .05;
            }

            $max_date->modify('-1 months');

            if($comissao_valor >= 1.0) {
                $data = [
                    'user_id'       =>  $franqueado->id,
                    'ano'           =>  $max_date->format('Y'),
                    'mes'           =>  $max_date->format('m'),
                    'vendas'        =>  $valor,
                    'comissao'      =>  $comissao_valor,
                    'paga'          =>  0
                ];

                $comissao = $FranqueadoComissoes->newEntity();
                $comissao = $FranqueadoComissoes->patchEntity($comissao, $data);

                if($franqueado_comissao = $FranqueadoComissoes->save($comissao)){

                    foreach($pedidos as $pedido){

                        $data_comissao_pedido = [
                            'franqueado_comissao_id'  => $franqueado_comissao->id,
                            'pedido_id'             => $pedido->id
                        ];

                        $franqueado_comissao_pedido = $FranqueadoComissoesPedidos->newEntity();
                        $franqueado_comissao_pedido = $FranqueadoComissoesPedidos->patchEntity($franqueado_comissao_pedido, $data_comissao_pedido);

                        $FranqueadoComissoesPedidos->save($franqueado_comissao_pedido);

                    }

                    $this->Flash->info('Comissões fechadas!');
                    return $this->redirect(['action' => 'comissoes']);
                }else{
                    $this->Flash->error('Falha ao fechar comissões..');
                    return $this->redirect(['action' => 'comissoes']);
                } 
            }
        }

        $this->redirect(['action' => 'comissoes']);
    }
}
