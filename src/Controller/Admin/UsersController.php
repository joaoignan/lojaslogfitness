<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['logout']);
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                //return $this->redirect($this->Auth->redirectUrl());
                return $this->redirect('/admin/');
            }
            $this->Flash->auth(__('Invalid username or password, try again'));
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function passwd(){
        $user = $this->Users->get($this->Auth->user('id'));

        if($user){
            if($this->request->is(['post', 'put'])){
                $hasher = new DefaultPasswordHasher();
                $verify = new DefaultPasswordHasher;
                if($verify->check($this->request->data['old_password'], $user->password)){
                    if($verify->check($this->request->data['confirm_new_password'],
                        $hasher->hash($this->request->data['password']))){
                        $user->password = $this->request->data['password'];
                        if($this->Users->save($user)){
                            $this->Flash->success('Senha alterada com sucesso!');
                            $this->redirect(['controller' => 'Pages', 'action' => 'index']);
                        }else{
                            $this->Flash->error('Falha ao alterar a senha :(');
                        }
                    }
                }
            }
        }

        $this->set(compact('user'));
    }

    /**
     * ESQUECI A SENHA
     * @param bool $solicitacao
     */
    public function forgot_passwd($solicitacao = false){

        if($this->request->is(['post', 'put'])){

            $user = $this->Users
                ->find('all')
                ->where(['email' => $this->request->data['email']])
                ->first();

            if(count($user) >= 1 && ($user->email != null && $user->email != ""))
            {
                $UserPasswd = TableRegistry::get('UserPasswd');

                $data = [
                    'user_id'      => $user->id,
                    'recover_key'  => sha1($user->id.date('YmdHis')),
                    'passwd'       => 0,
                ];

                $passwd = $UserPasswd->newEntity();
                $passwd = $UserPasswd->patchEntity($passwd, $data);

                if($UserPasswd->save($passwd)){

                    Email::configTransport('user', [
                        'className' => 'Smtp',
                        //'host' => 'ssl://smtp.gmail.com',
                        'host' => 'mail.logfitness.com.br',
                        //'host' => 'localhost',
                        'port' => 587,
                        'timeout' => 30,
                        //'username' => 'm4web.mkt@gmail.com',
                        'username' => 'sos@logfitness.com.br',
                        //'password' => '2VRKHUXBSj2Xyfjw',
                        'password' => 'Lvrt6174?',
                        //'password' => 'sos@2016',
                        'client' => null,
                        'tls' => null,
                    ]);

                    $email = new Email();
                    $email
                        ->transport('user')
                        ->template('user_passwd')
                        ->emailFormat('html')
                        ->from(['sos@logfitness.com.br' => 'LogFitness'])
                        ->to($user->email)
                        ->subject('LogFitness - Recuperar Senha')
                        ->set([
                            'name'              => $user->name,
                            'email'             => $user->email,
                            'recover_key'      => $data['recover_key'],
                        ])
                        ->send('user_passwd');

                    $this->Flash->success('Solicitação enviada com sucesso. Sigas as instruções no seu email');
                    $this->redirect(['action' => 'forgot_passwd', 'solicitacao-enviada']);
                }else{
                    $this->Flash->error('Falha ao solicitar alteração. Tente novamente...');
                }
            }else{
                $this->Flash->error('E-mail/Usuário não encontrado...');
                $solicitacao = 'falha';
            }
        }

        $this->set(compact('disable_modal_login', 'solicitacao'));

    }

    public function recover_passwd($email = null, $recover_key = null, $sucesso = false){

        if($email == null || $recover_key == null){
            $this->redirect(SSLUG.'/');
        }else {


            if ($this->request->is(['post', 'put'])) {

                $user = $this->Users
                    ->find('all')
                    ->where(['email' => $email])
                    ->first();

                if (count($user) >= 1) {

                    $UserPasswd = TableRegistry::get('UserPasswd');

                    $user_passwd = $UserPasswd
                        ->find('all')
                        ->where(['user_id' => $user->id])
                        ->andWhere(['recover_key' => $recover_key])
                        ->first();

                    if (count($user_passwd) >= 1) {

                        $data_passwd = [
                            'password' => $this->request->data['password']
                        ];

                        $passwd_user = $this->Users->patchEntity($user, $data_passwd);
                        if ($this->Users->save($passwd_user)) {

                            $passwd = $UserPasswd->patchEntity($user_passwd, ['passwd' => 1]);

                            if ($UserPasswd->save($passwd)) {
                                $this->Flash->success('Recuperação realizada com sucesso. Faça login para continuar');
                                $this->redirect(['action' => 'login', 'sucesso']);
                            }
                        }
                    }

                }
            }
        }

        $disable_modal_login = true;

        $this->set(compact('disable_modal_login', 'sucesso'));

    }

    /**
     * Index method
     *
     * @return void
     */
    public function index(){

        $condition = ['Users.group_id >' => 0];
        if($this->Auth->user('group_id') > 1){
            $condition = ['Users.group_id >' => 1];
        }

        $this->paginate = [
            'contain'       => ['Groups'],
            'conditions'    => $condition
        ];
        $this->set('users', $this->paginate($this->Users));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Status', 'Groups']
        ]);
        if($user->group_id == 1){
            $this->redirect(['action' => 'index']);
        }
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success('The user has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The user could not be saved. Please, try again.');
            }
        }
        //$status = $this->Users->Status->find('list', ['limit' => 200]);
        $status = TableRegistry::get('Status');
        $status = $status->find('list', ['limit' => 10]);
        $groups = $this->Users->Groups->find('list', ['limit' => 200])->toArray();
        if($this->Auth->user('group_id') > 1){
            unset($groups[1]);
        }
        $this->set(compact('user', 'status', 'groups'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        if($user->group_id == 1){
            $this->redirect(['action' => 'index']);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success('The user has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The user could not be saved. Please, try again.');
            }
        }
        $status = $this->Users->Status->find('list', ['limit' => 200]);
        $groups = $this->Users->Groups->find('list', ['limit' => 200])->toArray();
        if($this->Auth->user('group_id') > 1){
            unset($groups[1]);
        }
        $this->set(compact('user', 'status', 'groups'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {

        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if($user->group_id == 1){
            $this->redirect(['action' => 'index']);
        }
        if ($this->Users->delete($user)) {
            $this->Flash->success('The user has been deleted.');
        } else {
            $this->Flash->error('The user could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
