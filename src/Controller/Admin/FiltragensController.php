<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Filtragens Controller
 *
 * @property \App\Model\Table\FiltragensTable $Filtragens
 */
class FiltragensController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain'   => [],
            'order'     => ['Filtragens.qtd' => 'desc']
        ];
        $this->set('filtragens', $this->paginate($this->Filtragens));
        $this->set('_serialize', ['filtragens']);
    }
}
