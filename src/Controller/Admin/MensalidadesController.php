<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Database\Schema\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * CupomDesconto Controller
 *
 * @property \App\Model\Table\CupomDesconto $CupomDesconto
 */
class MensalidadesController extends AppController {

    public function index () {
        $AcademiaMensalidades = TableRegistry::get('AcademiaMensalidades');

        $this->paginate = [];

        $mensalidades = $this->paginate($AcademiaMensalidades);

        $this->set(compact('mensalidades'));
        $this->set('_serialize', ['mensalidades']);
    }

    public function transferencias($mes = null, $ano = null){

        if($this->request->is(['post'])){
            $this->redirect(['action' => 'transferencias', $this->request->data['mes'], $this->request->data['ano']]);
        }

        if($mes == null || $ano == null){
            $this->set('select_date', true);
        }else {
            $Transferencias = TableRegistry::get('Transferencias');

            $time = new Time('2017-05-14 18:00:00');
            $time->setDate($ano, $mes, 01);

            $transferencias = $Transferencias
                ->find('all')
                ->contain('Academias')
                ->where(['Transferencias.created LIKE' => $ano . '-' . $mes . '%'])
                ->all();

            $this->set(compact('transferencias'));
        }

        $this->set(compact('mes', 'ano'));

    }

    public function planos () {
        $Pedidos = TableRegistry::get('Pedidos');

        $planos = $Pedidos
            ->find('all')
            ->contain(['PlanosClientes', 'Academias', 'Clientes'])
            ->where(['Pedidos.comissao_status' => 4])
            ->andWhere(['Pedidos.pedido_status_id >=' => 3])
            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
            ->andWhere(['PlanosClientes.type' => 'Plano'])
            ->order(['Pedidos.created' => 'desc']);

        $this->set('planos', $this->paginate($planos));
        $this->set('_serialize', ['planos']);
    }

    public function assinaturas () {
        $Pedidos = TableRegistry::get('Pedidos');

        $assinaturas = $Pedidos
            ->find('all')
            ->contain(['PlanosClientes', 'Academias', 'Clientes'])
            ->where(['Pedidos.comissao_status' => 4])
            ->andWhere(['Pedidos.pedido_status_id >=' => 3])
            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
            ->andWhere(['PlanosClientes.type' => 'Assinatura'])
            ->order(['Pedidos.created' => 'desc']);


        $this->set('assinaturas', $this->paginate($assinaturas));
        $this->set('_serialize', ['assinaturas']);
    }

    public function personalizados () {
        $Pedidos = TableRegistry::get('Pedidos');

        $query = $Pedidos
            ->find('all')
            ->contain(['Clientes', 'Academias'])
            ->where(['Pedidos.comissao_status' => 5])
            ->andWhere(['Pedidos.pedido_status_id >=' => 3])
            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]]);

        $this->set('personalizados', $this->paginate($query));
        $this->set('_serialize', ['personalizados']);
    }

    public function transferencias_manual(){
        $AcademiaMensalidades = TableRegistry::get('AcademiaMensalidades');

        $academia_mensalidades = $AcademiaMensalidades
            ->find('all')
            ->where(['account_verify' => 1])
            ->all();

        $list_academia_mensalidades_tranferir[IUGU_API_TOKEN] = 'Conta Mestre';
        $list_academia_mensalidades_receber[IUGU_ID] = 'Conta Mestre';

        foreach ($academia_mensalidades as $academia_mensalidade) {
            $list_academia_mensalidades_tranferir[$academia_mensalidade->iugu_live_api_token] = $academia_mensalidade->iugu_name;
            $list_academia_mensalidades_receber[$academia_mensalidade->iugu_account_id] = $academia_mensalidade->iugu_name;
        }

        if($this->request->is(['patch', 'post', 'put'])) {

            $valor_transferencia = str_replace(".","",$this->request->data['valor_transferencia']);
            $valor_transferencia = str_replace(",","",$valor_transferencia);

            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $data = [
                "api_token"      => $this->request->data['academia_transferir'],
                "receiver_id"    => $this->request->data['academia_receber'],
                "amount_cents"   => $valor_transferencia
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/transfers",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $this->Flash->info($response);
            return $this->redirect('/admin/mensalidades/transferencias_manual');
        }

        $this->set(compact('list_academia_mensalidades_tranferir', 'list_academia_mensalidades_receber'));
    }
}