<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Fornecedores Controller
 *
 * @property \App\Model\Table\FornecedoresTable $Fornecedores
 */
class FornecedoresController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain'   => ['Cities', 'Users', 'Users.Status', 'Marcas'],
            'order'     => ['Fornecedores.id' => 'desc']
        ];
        $this->set('fornecedores', $this->paginate($this->Fornecedores));
        $this->set('_serialize', ['fornecedores']);
    }

    /**
     * View method
     *
     * @param string|null $id Fornecedore id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fornecedore = $this->Fornecedores->get($id, [
            'contain' => ['Cities', 'Users', 'Users.Status', 'Marcas']
        ]);
        $this->set('fornecedore', $fornecedore);
        $this->set('_serialize', ['fornecedore']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $Users = TableRegistry::get('Users');
        
        if ($this->request->is('post')) {    

            $user = $Users->newEntity();

            $data = [
                'name'      => $this->request->data['name'],
                'username'  => $this->request->data['username'],
                'email'     => $this->request->data['email'],
                'status_id' => $this->request->data['status_id'],
                'group_id'  => 6
            ];

            $user = $Users->patchEntity($user, $data);
            if ($Users->save($user)) {

                $fornecedore = $this->Fornecedores->newEntity();

                $data = [
                    'cnpj'       => $this->request->data['cnpj'],
                    'ie'         => $this->request->data['ie'],
                    'phone'      => $this->request->data['phone'],
                    'user_id'    => $user->id,
                    'marca_id'   => $this->request->data['marca_id'],
                    'cep'        => $this->request->data['cep'],
                    'address'    => $this->request->data['address'],
                    'number'     => $this->request->data['number'],
                    'complement' => $this->request->data['complement'],
                    'area'       => $this->request->data['area'],
                    'city_id'    => $this->request->data['city_id'],
                ];

                $fornecedore = $this->Fornecedores->patchEntity($fornecedore, $data);
                if ($this->Fornecedores->save($fornecedore)) {
                    $this->Flash->success('Fornecedor salvo com sucesso.');
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error('Falha ao salvar fornecedor. Tente novamente.');
                }
            } else {
                $this->Flash->error('Falha ao salvar usuário. Tente novamente.');
            }
        }

        $states = $this->Fornecedores->Cities->States->find('list');
        $status = $Users->Status->find('list');
        $marcas = $this->Fornecedores->Marcas->find('list', ['conditions' => ['status_id' => 1]]);
        $this->set(compact('fornecedore', 'states', 'status', 'marcas'));
        $this->set('_serialize', ['fornecedore']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fornecedore id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $fornecedore = $this->Fornecedores->get($id, [
            'contain' => ['Cities', 'Users', 'Users.Status', 'Marcas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fornecedore = $this->Fornecedores->patchEntity($fornecedore, $this->request->data);
            if ($this->Fornecedores->save($fornecedore)) {
                $this->Flash->success('fornecedore salvo com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar fornecedore. Tente novamente.');
            }
        }

        $cities = $this->Fornecedores->Cities->find('list')->where(['state_id' => $fornecedore->city->state_id]);
        $states = $this->Fornecedores->Cities->States->find('list');
        //$status = $this->Fornecedores->Status->find('list');
        $marcas = $this->Fornecedores->Marcas->find('list', ['conditions' => ['status_id' => 1]]);
        $this->set(compact('fornecedore', 'cities', 'states', 'status', 'marcas'));
        $this->set('_serialize', ['fornecedore']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fornecedore id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fornecedore = $this->Fornecedores->get($id);
        if ($this->Fornecedores->delete($fornecedore)) {
            $this->Flash->success('fornecedore apagado com sucesso.');
        } else {
            $this->Flash->error('Falha ao apagar fornecedore. Tente novamente.');
        }
        return $this->redirect(['action' => 'index']);
    }

    public function lista_produtos($search_query = null) {
        if($this->request->is(['post', 'put'])){
            $this->redirect(['action' => 'lista_produtos', $this->request->data['search']]);
        }

        $Fornecedores = TableRegistry::get('Fornecedores');
        $Produtos = TableRegistry::get('Produtos');

        $fornecedor = $Fornecedores
            ->find('all')
            ->where(['user_id' => $this->Auth->user('id')])
            ->first();

        if($search_query != null){

            $search = [
                'OR' => [
                    'ProdutoBase.name     LIKE' => '%'.$search_query.'%',
                    'Produtos.propriedade LIKE' => '%'.$search_query.'%'
                ]
            ];
        }else{
            $search = [];
        }

        $produtos_find = $Produtos
            ->find('all')
            ->contain([
                'ProdutoBase'
            ])
            ->where(['ProdutoBase.marca_id' => $fornecedor->marca_id])
            ->andWhere($search)
            ->distinct('Produtos.id');

        $produtos = $this->paginate($produtos_find);

        $this->set(compact('produtos'));
        $this->set('_serialize', ['produtos']);
    }
}
