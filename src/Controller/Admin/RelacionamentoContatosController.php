<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * RelacionamentoContatos Controller
 *
 * @property \App\Model\Table\RelacionamentoContatosTable $RelacionamentoContatos
 */
class RelacionamentoContatosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain'   => ['Status'],
            'order'     => ['RelacionamentoContatos.id' => 'desc']
        ];
        $relacionamentoContatos = $this->paginate($this->RelacionamentoContatos);

        $this->set(compact('relacionamentoContatos'));
        $this->set('_serialize', ['relacionamentoContatos']);
    }

    /**
     * View method
     *
     * @param string|null $id Relacionamento Contato id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $relacionamentoContato = $this->RelacionamentoContatos->get($id, [
            'contain' => ['Status']
        ]);

        $this->set('relacionamentoContato', $relacionamentoContato);
        $this->set('_serialize', ['relacionamentoContato']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $relacionamentoContato = $this->RelacionamentoContatos->newEntity();
        if ($this->request->is('post')) {
            $relacionamentoContato = $this->RelacionamentoContatos->patchEntity($relacionamentoContato, $this->request->data);
            if ($this->RelacionamentoContatos->save($relacionamentoContato)) {
                $this->Flash->success(__('The relacionamento contato has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The relacionamento contato could not be saved. Please, try again.'));
            }
        }
        $status = $this->RelacionamentoContatos->Status->find('list', ['limit' => 200]);
        $this->set(compact('relacionamentoContato', 'status'));
        $this->set('_serialize', ['relacionamentoContato']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Relacionamento Contato id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $relacionamentoContato = $this->RelacionamentoContatos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $relacionamentoContato = $this->RelacionamentoContatos->patchEntity($relacionamentoContato, $this->request->data);
            if ($this->RelacionamentoContatos->save($relacionamentoContato)) {
                $this->Flash->success(__('The relacionamento contato has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The relacionamento contato could not be saved. Please, try again.'));
            }
        }
        $status = $this->RelacionamentoContatos->Status->find('list', ['limit' => 200]);
        $this->set(compact('relacionamentoContato', 'status'));
        $this->set('_serialize', ['relacionamentoContato']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Relacionamento Contato id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $relacionamentoContato = $this->RelacionamentoContatos->get($id);
        if ($this->RelacionamentoContatos->delete($relacionamentoContato)) {
            $this->Flash->success(__('The relacionamento contato has been deleted.'));
        } else {
            $this->Flash->error(__('The relacionamento contato could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
