<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Banners Controller
 *
 * @property \App\Model\Table\BannersTable $Banners
 */
class LojasController extends AppController
{

	public function index(){
		$Academias = TableRegistry::get('Academias');

		$lojas = $Academias
			->find('all')
			->contain(['Cities', 'Cities.States', 'Status'])
			->where(['master_loja' => 1])
			->all();

		$this->set(compact('lojas'));
	}

	public function add(){

	}

	public function edit(){

	}

	public function view(){

	}


}