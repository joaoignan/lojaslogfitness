<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use ReflectionClass;
use ReflectionMethod;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\View\Helper;

/**
 * Cca Controller
 *
 * @property \App\Model\Table\CcaTable $Cca
 */
class CcaController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index(){

    }

    public function getControllers() {
        $files = scandir('../src/Controller/Admin/');
        $results = [];
        $ignoreList = [
            '.',
            '..',
            'Component',
            'AppController.php',
        ];
        foreach($files as $file){
            if(!in_array($file, $ignoreList)) {
                $controller = explode('.', $file)[0];
                array_push($results, str_replace('Controller', '', $controller));
            }
        }
        return $results;

    }

    public function getActions($controllerName) {
        $className = 'App\\Controller\\Admin\\'.$controllerName.'Controller';
        $class = new ReflectionClass($className);
        $actions = $class->getMethods(ReflectionMethod::IS_PUBLIC);
        $results = [$controllerName => []];
        $ignoreList = ['beforeFilter', 'afterFilter', 'initialize'];
        foreach($actions as $action){
            if($action->class == $className && !in_array($action->name, $ignoreList)){
                array_push($results[$controllerName], $action->name);
            }
        }
        return $results;
    }

    public function getResources(){
        $controllers = $this->getControllers();
        $resources = [];
        foreach($controllers as $controller){
            $actions = $this->getActions($controller);
            array_push($resources, $actions);
        }
        return $resources;
    }


    function flatten($arr, $base = "", $divider_char = "/") {
        $ret = array();
        if(is_array($arr)) {
            foreach($arr as $k => $v) {
                if(is_array($v)) {
                    $tmp_array = $this->flatten($v, $base.$k.$divider_char, $divider_char);
                    $ret = array_merge($ret, $tmp_array);
                } else {
                    $ret[$base.$k] = $v;
                }
            }
    }
        return $ret;
    }

    function inflate($arr, $divider_char = "/") {
        if(!is_array($arr)) {
            return false;
        }

        $split = '/' . preg_quote($divider_char, '/') . '/';

        $ret = array();
        foreach ($arr as $key => $val) {
            $parts = preg_split($split, $key, -1, PREG_SPLIT_NO_EMPTY);
            $leafpart = array_pop($parts);
            $parent = &$ret;
            foreach ($parts as $part) {
                if (!isset($parent[$part])) {
                    $parent[$part] = array();
                } elseif (!is_array($parent[$part])) {
                    $parent[$part] = array();
                }
                $parent = &$parent[$part];
            }

            if (empty($parent[$leafpart])) {
                $parent[$leafpart] = $val;
            }
        }
    return $ret;
}

    /**
     * SINCRONIZAÇÃO DAS ACTIONS/CONTROLLERS EM LISTA PARA CONSULTA NA DEFINIÇÃO DE PERMISSÕES
     */
    public function cca_sync(){
        $actions = $this->getResources();

        $cca = $this->Cca->CcaActions->find()->all();
        $ccas = $cca->toArray();

        $cca_sync = array();
        //ADD ACTIONS TO CCA ACTIONS LIST
        foreach($actions as $action){
            foreach($action as $key => $actions_list){
                foreach($actions_list as $sync){
                    $cca_sync[] = [$key, $sync, 'admin'];
                    $data = [
                        'controller'    => $key,
                        'action'        => $sync,
                        'prefix'        => 'admin'
                    ];
                    if(!$this->Cca->CcaActions->exists($data)){
                        $CcaActions = $this->Cca->CcaActions->newEntity();
                        $CcaActions = $this->Cca->CcaActions->patchEntity($CcaActions, $data);
                        if($this->Cca->CcaActions->save($CcaActions)){
                            $messages[] = 'Adicionado: Admin/'.$key.'/'.$sync.'<br />';
                        }
                    }
                }
            }
        }

        //REMOVE ACTIONS FROM CCA ACTIONS LIST
        $cca_actions = array();
        foreach($ccas as $cca){
            $cca_actions[$cca['id']] = [$cca['controller'], $cca['action'], $cca['prefix']];
        }

        $arr1_flat = $this->flatten($cca_actions);
        $arr2_flat = $this->flatten($cca_sync);

        $dif = array_diff($arr1_flat, $arr2_flat);
        $dif = $this->inflate($dif);

       foreach($dif as $k => $sync_del){

            $CcaActions = $this->Cca->CcaActions->get($k);

            if($this->Cca->deleteAll(['cca_action_id' => $k])) {

                if ($this->Cca->CcaActions->delete($CcaActions)) {
                    $messages[] = 'Removido: Admin/' . $cca_actions[$k][0] . '/' . $sync_del[0] . '<br />';
                }
            }
        }

        $messages[] = '<br />';
        $messages[] = 'Sincronização concluída.';

        $this->set(compact('messages'));
    }

    /**
     * SELECIONAR GRUPO A SER GERENCIADO
     */
    public function cca_roles_step_1(){

        if($this->request->is(['post', 'put'])){
            $this->redirect(['action' => 'cca_roles_step_2', $this->request->data['group_id']]);
        }

        $groups = $this->Cca->Groups->find('list')->all();
        $this->set(compact('groups', 'cca'));
    }

    /**
     * @param $group
     * DEFINIR PERMISSÕES DO GRUPO GERENCIADO
     */
    public function cca_roles_step_2($group_id){
        if(!$group_id){
            $this->Flash->error(__('Grupo inválido'));
            $this->redirect(['controller' => 'Pages', 'action' => 'index']);
        }

        $messages = [];
        if($this->request->is('post')){
            foreach($this->request->data['CcaAction'] as $key => $form_data){
                $data = ['cca_action_id' => $key, 'group_id' => $group_id];
                if($form_data == 1){
                    if(!$this->Cca->exists($data)){
                        $Cca = $this->Cca->newEntity();
                        $Cca->cca_action_id = $key;
                        $Cca->group_id = $group_id;
                         if($this->Cca->save($Cca)){
                            $messages[] = 'Adicionado: Ação ['.$key.'] - Grupo ['.$group_id.']<br />';
                        }
                    }
                }else{
                    if($this->Cca->exists($data)){
                        $Cca = $this->Cca->findByCcaActionIdAndGroupId($key, $group_id)->first();
                        if($this->Cca->delete($Cca)){
                            $messages[] = 'Removido: Ação ['.$key.'] - Grupo ['.$group_id.']<br />';
                        }
                    }
                }
            }

            $messages[] = '<br /><br />';
            $this->Flash->success(__('Permissões salvas com sucesso!'));
        }

        $cca_actions = $this->Cca->CcaActions->find()->all();
        $cca_group = $this->Cca->Groups->get($group_id);

        $cca_group['User'] = $this->Cca->Groups->Users->find()
            ->where(['group_id' => $group_id])
            ->all();

        $this->set(compact('cca_group', 'cca_actions', 'messages'));
    }

}
