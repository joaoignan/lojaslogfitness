<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Navegacoes Controller
 *
 * @property \App\Model\Table\NavegacoesTable $Navegacoes
 */
class NavegacoesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index($user_agent = null, $tipo = null, $academia = null, $data_inicio = null, $data_fim = null)
    {

        if($user_agent == '0') {
            $user_agent = '';
        }

        if($tipo == '0') {
            $tipo = '';
        }

        if($academia == '0') {
            $academia = '';
        }

        if($data_inicio == '0') {
            $data_inicio = '';
        }

        if($data_fim == '0') {
            $data_fim = '';
        }

        if($user_agent != null) {
            $user_agent_search = ['Navegacoes.user_agent LIKE' => '%'.$user_agent.'%'];
        } else {
            $user_agent_search = [];
        }

        if($tipo != null) {
            $tipo_search = ['Navegacoes.tipo' => $tipo];
        } else {
            $tipo_search = [];
        }

        if($academia != null) {
            $academia_search = ['Navegacoes.academia' => $academia];
        } else {
            $academia_search = [];
        }

        if($data_inicio != null) {
            $time_inicio = new Time($data_inicio);
            $data_inicio_search = ['Navegacoes.created >=' => $time_inicio];
        } else {
            $data_inicio_search = [];
        }

        if($data_fim != null) {
            $time_fim = new Time($data_fim.'23:59:00');
            $data_fim_search = ['Navegacoes.created <=' => $time_fim];
        } else {
            $data_fim_search = [];
        }

        if($user_agent != null || $tipo != null || $academia != null || $data_inicio != null || $data_fim != null){
            $search = [
                'AND' => [
                    $user_agent_search,
                    $tipo_search,
                    $academia_search,
                    $data_inicio_search,
                    $data_fim_search
                ]
            ];
        }else{
            $search = [];
        }

        $this->paginate = [
            'conditions' => $search,
            'order'      => ['Navegacoes.id' => 'desc']
        ];

        $tipos_navegacao = $this->Navegacoes
            ->find('all')
            ->distinct('tipo')
            ->all();

        $count_home = $this->Navegacoes
            ->find('all')
            ->where(['tipo' => 'Home'])
            ->andWhere($search)
            ->count();

        $count_admin_acad = $this->Navegacoes
            ->find('all')
            ->where(['tipo' => 'Admin Academia'])
            ->andWhere($search)
            ->count();

        $count_admin_time = $this->Navegacoes
            ->find('all')
            ->where(['tipo' => 'Admin Time'])
            ->andWhere($search)
            ->count();

        $count_catalogo = $this->Navegacoes
            ->find('all')
            ->where(['tipo' => 'Catalogo Geral'])
            ->andWhere($search)
            ->count();

        $count_mais_vendidos = $this->Navegacoes
            ->find('all')
            ->where(['tipo' => 'Mais Vendidos'])
            ->andWhere($search)
            ->count();

        $count_promocoes = $this->Navegacoes
            ->find('all')
            ->where(['tipo' => 'Promoções'])
            ->andWhere($search)
            ->count();

        $count_combo = $this->Navegacoes
            ->find('all')
            ->where(['tipo' => 'Montar combo'])
            ->andWhere($search)
            ->count();

        $count_facebook = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.user_agent LIKE' => '%facebook%'])
            ->andWhere($search)
            ->count();

        $count_whatsapp = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.user_agent LIKE' => '%whatsapp%'])
            ->andWhere($search)
            ->count();

        $count_adaptogen = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%adaptogen%'])
            ->andWhere($search)
            ->count();

        $count_atlhetica = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%atlhetica%'])
            ->andWhere($search)
            ->count();

        $count_black_skull = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%black-skull%'])
            ->andWhere($search)
            ->count();

        $count_body_action = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%body-action%'])
            ->andWhere($search)
            ->count();

        $count_bodybuilders = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%bodybuilders%'])
            ->andWhere($search)
            ->count();

        $count_gt_nutrition = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%gt-nutrition%'])
            ->andWhere($search)
            ->count();

        $count_integral_medica = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%integral-medica%'])
            ->andWhere($search)
            ->count();

        $count_iridium_labs = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%iridium-labs%'])
            ->andWhere($search)
            ->count();

        $count_labrada = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%labrada%'])
            ->andWhere($search)
            ->count();

        $count_logfitness = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%/logfitness%'])
            ->andWhere($search)
            ->count();

        $count_max_titanium = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%max-titanium%'])
            ->andWhere($search)
            ->count();

        $count_midway = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%midway%'])
            ->andWhere($search)
            ->count();

        $count_new_millen = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%new-millen%'])
            ->andWhere($search)
            ->count();

        $count_optimum_nutrition = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%optimum-nutrition%'])
            ->andWhere($search)
            ->count();

        $count_power_suplements = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%power-suplements%'])
            ->andWhere($search)
            ->count();

        $count_probiotica = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%probiotica%'])
            ->andWhere($search)
            ->count();

        $count_universal = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%universal%'])
            ->andWhere($search)
            ->count();

        $count_usplabs = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%usplabs%'])
            ->andWhere($search)
            ->count();

        $count_vitaminlife = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%vitaminlife%'])
            ->andWhere($search)
            ->count();

        $count_vpx = $this->Navegacoes
            ->find('all')
            ->where(['Navegacoes.url_destino LIKE' => '%pesquisa%vpx%'])
            ->andWhere($search)
            ->count();

        $tipos_navegacao_list[0] = 'Todos';

        foreach ($tipos_navegacao as $tipo_navegacao) {
            if($tipo_navegacao->tipo != null) {
                $tipos_navegacao_list[$tipo_navegacao->tipo] = $tipo_navegacao->tipo;
            }
        }

        $academias_navegacao = $this->Navegacoes
            ->find('all')
            ->distinct('academia')
            ->all();

        $academias_navegacao_list[0] = 'Todos';

        foreach ($academias_navegacao as $academia_navegacao) {
            if($academia_navegacao->academia != null) {
                $academias_navegacao_list[$academia_navegacao->academia] = $academia_navegacao->academia;
            }
        }

        $this->set(compact('user_agent', 'tipo', 'academia', 'tipos_navegacao_list', 'academias_navegacao_list', 'data_inicio', 'data_fim', 'count_home', 'count_admin_acad', 'count_admin_time', 'count_catalogo', 'count_mais_vendidos', 'count_promocoes', 'count_combo', 'count_facebook', 'count_whatsapp', 'count_adaptogen', 'count_atlhetica', 'count_black_skull', 'count_body_action', 'count_bodybuilders', 'count_gt_nutrition', 'count_integral_medica', 'count_iridium_labs', 'count_labrada', 'count_logfitness', 'count_max_titanium', 'count_midway', 'count_new_millen', 'count_optimum_nutrition', 'count_power_suplements', 'count_probiotica', 'count_universal', 'count_usplabs', 'count_vitaminlife', 'count_vpx'));
        $this->set('navegacoes', $this->paginate($this->Navegacoes));
        $this->set('_serialize', ['navegacoes']);
    }

    public function apagar_lojamodelo() {
        $navegacoes_lojamodelo = $this->Navegacoes
            ->find('all')
            ->where(['academia' => 'lojamodelo'])
            ->orWhere(['url_destino LIKE' => '%lojamodelo%'])
            ->all();

        foreach ($navegacoes_lojamodelo as $navegacao_lojamodelo) {
            $this->Navegacoes->delete($navegacao_lojamodelo);
        }

        $this->Flash->info('As navegações da lojamodelo foram apagadas!');
        return $this->redirect($this->referer());
    }
}
