<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * MensalidadesCategorias Controller
 *
 * @property \App\Model\Table\DiferenciaisTable $Diferenciais
 */
class MensalidadesCategoriasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [

        ];

        $categorias = $this->paginate($this->MensalidadesCategorias);

        $this->set(compact('categorias'));
        $this->set('_serialize', ['categorias']);
    }

    /**
     * View method
     *
     * @param string|null $id Categoria id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $categoria = $this->MensalidadesCategorias->get($id, [

        ]);

        $this->set('categoria', $categoria);
        $this->set('_serialize', ['categoria']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $categoria = $this->MensalidadesCategorias->newEntity();
        if ($this->request->is('post')) {
            $categoria = $this->MensalidadesCategorias->patchEntity($categoria, $this->request->data);
            if ($this->MensalidadesCategorias->save($categoria)) {
                $this->Flash->success(__('A categoria foi salva.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('A categoria não pode ser salva... Tente novamente...'));
            }
        }
        $this->set(compact('categoria'));
        $this->set('_serialize', ['categoria']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Categoria id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $categoria = $this->MensalidadesCategorias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $categoria = $this->MensalidadesCategorias->patchEntity($categoria, $this->request->data);
            if ($this->MensalidadesCategorias->save($categoria)) {
                $this->Flash->success(__('A categoria foi alterada.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('A categoria não pode ser alterada... Tente novamente...'));
            }
        }
        $this->set(compact('categoria'));
        $this->set('_serialize', ['categoria']);
    }
}
