<?php
namespace App\Controller\Admin;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Mailer\Email;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController{

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function index(){

        $AcademiaComissoes  = TableRegistry::get('AcademiaComissoes');
        $ProfessorComissoes = TableRegistry::get('ProfessorComissoes');
        $Professores        = TableRegistry::get('Professores');
        $Academias          = TableRegistry::get('Academias');
        $Clientes           = TableRegistry::get('Clientes');
        $Pedidos            = TableRegistry::get('Pedidos');
        $Users              = TableRegistry::get('Users');

        $user = $Users->get($this->Auth->user('id'));

        $today = Time::now();
        $today = $today->format('Y-m-d');

        $academia_comissoes_fechadas    = $AcademiaComissoes
            ->find('all')
            ->contain(['Academias'])
            ->where(['AcademiaComissoes.paga' => 0])
            ->andWhere(['AcademiaComissoes.aceita' => 2])
            ->all();

        $academia_comissoes_pagas       = $AcademiaComissoes
            ->find('all')
            ->contain(['Academias'])
            ->where(['AcademiaComissoes.paga' => 1])
            ->all();

        $academia_comissoes_pendentes   = $AcademiaComissoes
            ->find('all')
            ->contain(['Academias'])
            ->where(['AcademiaComissoes.paga' => 0])
            ->andWhere(['AcademiaComissoes.aceita' => 1])
            ->all();

        $academia_comissoes_contestadas   = $AcademiaComissoes
            ->find('all')
            ->contain(['Academias'])
            ->where(['AcademiaComissoes.paga' => 0])
            ->andWhere(['AcademiaComissoes.aceita' => 0])
            ->all();

        $professor_comissoes_fechadas    = $ProfessorComissoes
            ->find('all')
            ->contain(['Professores'])
            ->where(['ProfessorComissoes.paga' => 0])
            ->andWhere(['ProfessorComissoes.aceita' => 2])
            ->all();

        $professor_comissoes_pagas       = $ProfessorComissoes
            ->find('all')
            ->contain(['Professores'])
            ->where(['ProfessorComissoes.paga' => 1])
            ->all();

        $professor_comissoes_pendentes   = $ProfessorComissoes
            ->find('all')
            ->contain(['Professores'])
            ->where(['ProfessorComissoes.paga' => 0])
            ->andWhere(['ProfessorComissoes.aceita' => 1])
            ->all();

        $professor_comissoes_contestadas   = $ProfessorComissoes
            ->find('all')
            ->contain(['Professores'])
            ->where(['ProfessorComissoes.paga' => 0])
            ->andWhere(['ProfessorComissoes.aceita' => 0])
            ->all();
            
        $academia_ativas = $Academias
            ->find('all')
            ->where(['Academias.status_id' => 1])
            ->count();

        $professores_ativos = $Professores
            ->find('all')
            ->where(['Professores.status_id' => 1])
            ->count();

        $academias_inativas = $Academias
            ->find('all')
            ->where(['Academias.status_id' => 3])
            ->count();

        $professores_inativos = $Professores
            ->find('all')
            ->where(['Professores.status_id' => 3])
            ->count();

        $clientes_cadastro = $Clientes
            ->find('all')
            ->where(['Clientes.status_id' => 1])
            ->andWhere(['Clientes.created <' => $today])
            ->andWhere(['Clientes.first_email' => 0])
            ->all();

        $clientes_facebook = $Clientes
            ->find('all')
            ->where(['Clientes.status_id' => 1])
            ->andWhere(['Clientes.fbid IS NOT' => null])
            ->count();

        $clientes_email = $Clientes
            ->find('all')
            ->where(['Clientes.status_id' => 1])
            ->andWhere(['Clientes.fbid IS' => null])
            ->count();

        if($user->group_id == 7) {

            $FranqueadoComissoes        = TableRegistry::get('FranqueadoComissoes');
            $FranqueadoComissoesPedidos = TableRegistry::get('FranqueadoComissoesPedidos');

            $max_date = new \DateTime(date('Y-m').'-01');

            $academias_qtd = $Academias
                ->find('all')
                ->where(['Academias.user_id' => $user->id])
                ->andWhere(['Academias.status_id' => 1])
                ->all();

            $franqueado_comissoes = $FranqueadoComissoes
                ->find('all')
                ->where(['user_id' => $user->id])
                ->all();

            if(count($franqueado_comissoes) >= 1) {
                foreach ($franqueado_comissoes as $franqueado_comissao) {
                    $franqueado_comissoes_id[] = $franqueado_comissao->id;
                }

                if(!empty($franqueado_comissoes_id)) {
                    $franqueado_comissoes_pedidos = $FranqueadoComissoesPedidos
                        ->find('all')
                        ->where(['franqueado_comissao_id IN' => $franqueado_comissoes_id])
                        ->all();
                }

                if(count($franqueado_comissoes_pedidos) >= 1) {
                    foreach ($franqueado_comissoes_pedidos as $franqueado_comissoes_pedido) {
                        $franqueado_comissoes_pedidos_id[] = $franqueado_comissoes_pedido->pedido_id;
                    }

                    $pedidos_mes = $Pedidos
                        ->find('all')
                        ->contain(['Academias'])
                        ->where(['Pedidos.user_id' => $user->id])
                        ->andWhere(['Pedidos.pedido_status_id >=' => 3])
                        ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
                        ->andWhere(['Pedidos.comissao_status IN' => [1, 2]])
                        ->andWhere(['Pedidos.id NOT IN' => $franqueado_comissoes_pedidos_id])
                        ->andWhere(['Pedidos.created >=' => $max_date])
                        ->all();
                } else {
                    $pedidos_mes = $Pedidos
                        ->find('all')
                        ->contain(['Academias'])
                        ->where(['Pedidos.user_id' => $user->id])
                        ->andWhere(['Pedidos.pedido_status_id >=' => 3])
                        ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
                        ->andWhere(['Pedidos.comissao_status IN' => [1, 2]])
                        ->andWhere(['Pedidos.created >=' => $max_date])
                        ->all();
                }
            } else {
                $pedidos_mes = $Pedidos
                    ->find('all')
                    ->contain(['Academias'])
                    ->where(['Pedidos.user_id' => $user->id])
                    ->andWhere(['Pedidos.pedido_status_id >=' => 3])
                    ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
                    ->andWhere(['Pedidos.comissao_status IN' => [1, 2]])
                    ->andWhere(['Pedidos.created >=' => $max_date])
                    ->all();
            }

            foreach ($pedidos_mes as $pedido_mes) {
                $valor_vendido_mes += $pedido_mes->valor;
            }
        }

        $mes_anterior = Time::now();
        $mes_anterior->subDays(30);

        $pedidos_30_dias = $Pedidos
            ->find('all')
            ->where(['data_pagamento >=' => $mes_anterior])
            ->andWhere(['pedido_status_id >=' => 3])
            ->andWhere(['pedido_status_id NOT IN' => [7, 8]])
            ->all();

        $vendas_30_dias = [];

        $vendas_total = 0.0;
        for ($i = 0; $i < 30; $i++) { 
            $vendas = 0.0;

            $dia = Time::now()->subDays($i)->format('d');
            $mes = Time::now()->subDays($i)->format('m');

            foreach ($pedidos_30_dias as $pedido_30_dias) {
                if($pedido_30_dias->data_pagamento->format('d-m') == $dia.'-'.$mes) {
                    $vendas += $pedido_30_dias->valor;
                }
            }

            $vendas_total += $vendas;

            $vendas_30_dias[] = [
                'data' => $dia.'/'.$mes,
                'vendas' => $vendas
            ];
        }

        $vendas_30_dias = array_reverse($vendas_30_dias);

        // Email::configTransport('cliente', [
        //     'className' => 'Smtp',
        //     'host' => 'mail.logfitness.com.br',
        //     'port' => 587,
        //     'timeout' => 30,
        //     'username' => 'log001@logfitness.com.br',
        //     'password' => 'Xt7154@',
        //     'client' => null,
        //     'tls' => null,
        // ]);

        // foreach ($clientes_cadastro as $cc) {

        //     $email = new Email();

        //     $email
        //         ->transport('cliente')
        //         ->template('bem_vindo')
        //         ->emailFormat('html')
        //         ->from(['log001@logfitness.com.br' => 'LogFitness'])
        //         ->to($cc->email)
        //         ->subject('Logfitness =)')
        //         ->set([
        //             'name' => $cc->name
        //         ])
        //         ->send();

        //     $data = [
        //         'first_email' => 1
        //     ];

        //     $cc = $Clientes->patchEntity($cc, $data);
            
        //     $email_enviado = $Clientes->save($cc);
        // }

        $this->set(compact(
            'vendas_30_dias',
            'vendas_total',
            'professor_comissoes_contestadas',
            'academia_comissoes_contestadas',
            'professor_comissoes_pendentes',
            'academia_comissoes_pendentes',
            'professor_comissoes_fechadas',
            'academia_comissoes_fechadas',
            'professor_comissoes_pagas',
            'academia_comissoes_pagas',
            'professores_inativos',
            'professores_ativos',
            'academias_inativas',
            'clientes_facebook',
            'valor_vendido_mes',
            'academia_ativas',
            'clientes_email',
            'academias_qtd'
        ));
    }
}