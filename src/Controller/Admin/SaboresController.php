<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Sabores Controller
 *
 * @property \App\Model\Table\SaboresTable $Sabores
 */
class SaboresController extends AppController
{

    public function sincronizar_marcas() {
        $todos_sabores = $this->Sabores
            ->find('all')
            ->all();

        foreach ($todos_sabores as $sabor) {
            $sabores_ids_array[] = $sabor->id;
        } 

        $ch = curl_init();

        $header = [
            "Content-Type: application/json"
        ];

        $data = [
            "sabores_ids_array" => $sabores_ids_array
        ];

        $data = json_encode($data);

        $options = array(CURLOPT_URL => "https://api.logfitness.com.br/sabores/sync?api_key=".LOGFITNESS_API_KEY,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $api_novos_sabores = json_decode($response);

        if($api_novos_sabores->status_id == 1 && $api_novos_sabores->retorno_cod == 'sucesso') {
            $falha = 0;

            foreach ($api_novos_sabores->sabores as $sabor) {

                $new_sabor_data = [
                    'id' => $sabor->id,
                    'name' => $sabor->name,
                ];

                $new_sabor = $this->Sabores->newEntity($new_sabor_data);
                
                if(!$this->Sabores->save($new_sabor)) {
                    $falha = 1;
                }
            }

            if($falha == 0) {
                $this->Flash->success('Sabores sincronizados com sucesso!');
            } else {
                $this->Flash->error('Falha ao atualizar banco de dados, atualize a página!');
            }
        }
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->sincronizar_marcas();

        $this->paginate = [
        ];
        $this->set('sabores', $this->paginate($this->Sabores));
        $this->set('_serialize', ['sabores']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->sincronizar_marcas();
    }

    /**
     * Edit method
     *
     * @param string|null $id sabor id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sabor = $this->Sabores->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sabor = $this->Sabores->patchEntity($sabor, $this->request->data);
            if ($this->Sabores->save($sabor)) {
                $this->Flash->success('sabor salvo com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar sabor. Tente novamente.');
            }
        }
        $this->set(compact('sabor'));
        $this->set('_serialize', ['sabor']);
    }

    public function delete($id = null)
    {
        $sabor = $this->Sabores->get($id, [
            'contain' => []
        ]);
        
        if ($this->Sabores->delete($sabor)) {
            $this->Flash->success('sabor deletado com sucesso.');
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error('Falha ao deletar sabor. Tente novamente.');
            return $this->redirect(['action' => 'index']);
        }
    }

}
