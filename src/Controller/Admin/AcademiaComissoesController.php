<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\I18n\Time;

/**
 * AcademiaComissoes Controller
 *
 * @property \App\Model\Table\AcademiaComissoesTable $AcademiaComissoes
 */
class AcademiaComissoesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($filter = 0, $mes = null, $ano = null){

        if($filter == 0) {
            //todas
            $condition_filter = [];
        } else if($filter == 1) {
            //a pagar
            $condition_filter = ['AcademiaComissoes.paga' => 0];
        } else if($filter == 2) {
            //pagas
            $condition_filter = ['AcademiaComissoes.paga' => 1];
        }

        $data_atual = Time::now();
        $ano_atual = $data_atual->format('Y');
        $mes_atual = $data_atual->format('m');

        $this->set(compact('ano_atual', 'mes_atual'));

        if($mes == null && $ano == null) {
            return $this->redirect(['action' => 'index', $filter, $mes_atual, $ano_atual]);
        }

        if($mes == $mes_atual && $ano == $ano_atual) {
            $Pedidos = TableRegistry::get('Pedidos');

            $pedidos = $Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.data_pagamento >=' => $ano.'-'.$mes.'-01%'])
                ->andWhere(['Pedidos.pedido_status_id >=' => 3])
                ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
                ->order(['Academias.id' => 'asc'])
                ->all();

            $vendas_por_academia = [];

            foreach($pedidos as $pedido) {
                $vendas_por_academia[$pedido->academia_id] = $vendas_por_academia[$pedido->academia_id] + $pedido->valor;
            }

            $pedido_academias = $Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.data_pagamento >=' => $ano.'-'.$mes.'-01%'])
                ->andWhere(['Pedidos.pedido_status_id >=' => 3])
                ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
                ->distinct(['Academias.id'])
                ->all();

            $mes_anterior = Time::now();
            $mes_anterior->subMonth(1);

            foreach ($pedido_academias as $pedido_academia) {

                $comissao_mes_anterior = $this->AcademiaComissoes
                    ->find('all')
                    ->where(['academia_id' => $pedido_academia->academia_id])
                    ->andWhere(['mes' => $mes_anterior->format('m')])
                    ->andWhere(['ano' => $mes_anterior->format('Y')])
                    ->first();

                if($comissao_mes_anterior) {
                    $vendas_mes_anterior = $comissao_mes_anterior->vendas;
                } else {
                    $vendas_mes_anterior = 0.0;
                }

                $taxa_comissao = $pedido_academia->academia->taxa_comissao * 0.01;

                $informacoes[$pedido_academia->academia_id] = [
                    'academia' => $pedido_academia->academia,
                    'vendas_mes_anterior' => $vendas_mes_anterior,
                    'vendas_mes_atual' => $vendas_por_academia[$pedido_academia->academia_id],
                    'comissoes_mes_atual' => $vendas_por_academia[$pedido_academia->academia_id] * $taxa_comissao,
                ];
            }

            $this->set(compact('informacoes', 'filter', 'mes', 'ano'));
            $this->set('_serialize', ['informacoes']);
        } else {
            $condition_mes = ['AcademiaComissoes.mes' => $mes];
            $condition_ano = ['AcademiaComissoes.ano' => $ano];

            $this->paginate = [
                'contain'    => ['Academias'],
                'order'      => ['AcademiaComissoes.paga' => 'asc', 'AcademiaComissoes.id' => 'desc'],
                'conditions' => [$condition_filter, $condition_mes, $condition_ano]
            ];

            $this->paginate['sortWhitelist'] = ['AcademiaComissoes.id', 'Academias.name', 'Academias.banco', 'AcademiaComissoes.meta', 'AcademiaComissoes.vendas', 'AcademiaComissoes.comissao', 'AcademiaComissoes.aceita', 'AcademiaComissoes.paga'];

            $academiaComissoes = $this->paginate($this->AcademiaComissoes);

            $vendas_mes_anterior = [];

            foreach($academiaComissoes as $academiaComissao) {
                $mes_anterior = new Time($academiaComissao->ano.'-'.$academiaComissao->mes);
                $mes_anterior->subMonth(1);

                $academiaComissoesMesAnterior = $this->AcademiaComissoes
                    ->find('all')
                    ->where(['academia_id' => $academiaComissao->academia_id])
                    ->andWhere(['mes' => $mes_anterior->format('m')])
                    ->andWhere(['ano' => $mes_anterior->format('Y')])
                    ->first();

                if($academiaComissoesMesAnterior) {
                    $vendas_mes_anterior[$academiaComissao->academia_id] = $academiaComissoesMesAnterior->vendas;
                } else {
                    $vendas_mes_anterior[$academiaComissao->academia_id] = 0.00;
                }
            }

            $this->set(compact('academiaComissoes', 'filter', 'mes', 'ano', 'vendas_mes_anterior'));
            $this->set('_serialize', ['academiaComissoes']);
        }

        if($this->request->is(['post', 'put'])){

            $Academias = TableRegistry::get('Academias');
                
            Email::configTransport('academia', [
                'className' => 'Smtp',
                'host' => 'mail.logfitness.com.br',
                'port' => 587,
                'timeout' => 30,
                'username' => EMAIL_USER,
                'password' => EMAIL_SENHA,
                'client' => null,
                'tls' => null,
            ]);

            foreach($this->request->data['paga'] as $key => $item){
                $academia_comissao = $this->AcademiaComissoes->get($key);
                $data       = [
                    'paga'  => $item,
                ];

                $paga_antigo = $academia_comissao->paga;

                $academia_comissao = $this->AcademiaComissoes->patchEntity($academia_comissao, $data);

                $academias_comissao_paga = $Academias
                    ->find('all')
                    ->where(['Academias.status_id'        => 1])
                    ->andWhere(['Academias.id' => $academia_comissao->academia_id])
                    ->first();

                if($this->AcademiaComissoes->save($academia_comissao)){
                    if($paga_antigo == 0 && $item == 1) {
                        $email = new Email();

                        $email
                            ->transport('academia')
                            ->template('pagamento_realizado')
                            ->emailFormat('html')
                            ->from([EMAIL_USER => EMAIL_NAME])
                            ->to($academias_comissao_paga->email)
                            ->subject(EMAIL_NAME' - Comissão paga!')
                            ->set([
                                'name'          => $academias_comissao_paga->contact,
                                'academia'      => $academias_comissao_paga->shortname,
                                'logo_academia' => $academias_comissao_paga->image
                            ])
                            ->send();
                    }
                }
            }

            $this->Flash->info('Atualização concluída');
            $this->redirect(['action' => 'index']);                
        }
    }

    public function filtrar_index() {
        if($this->request->is(['post'])){
            $this->redirect(['action' => 'index', $this->request->data['filter'], $this->request->data['mes'], $this->request->data['ano']]);
        }

        $this->viewBuilder()->layout(false);
        $this->render(false);
    }

    /**
     * COMISSÕES X PEDIDOS
     */
    public function comissoes_pedidos($mes = null, $ano = null){

        if($this->request->is(['post'])){
            $this->redirect(['action' => 'comissoes_pedidos', $this->request->data['mes'], $this->request->data['ano']]);
        }

        if($mes == null || $ano == null){
            $this->set('select_date', true);
        }else {
            $Configurations = TableRegistry::get('Configurations');
            $configurations = $Configurations->find('all')
                ->where(['status_id' => 1])
                ->order(['id' => 'desc'])
                ->limit(1)
                ->first();

            $config = unserialize($configurations->configuration);

            $Pedidos            = TableRegistry::get('Pedidos');

            $pedidos = $Pedidos
                ->find('all')
                ->contain('Academias');

            $pedidos
                ->select([
                    'DISTINCT Pedidos.id',
                    'Pedidos.academia_id',
                    'Pedidos.professor_id',
                    'Pedidos.valor',
                    'Academias.id',
                    'Academias.shortname',
                    'Academias.name',
                    'Academias.cnpj',
                    'Academias.contact',
                    'Academias.phone',
                    'Academias.email',
                    'Academias.favorecido',
                    'Academias.cpf_favorecido',
                    'Academias.banco',
                    'Academias.tipo_conta',
                    'Academias.agencia',
                    'Academias.agencia_dv',
                    'Academias.conta',
                    'Academias.conta_dv',
                    'Academias.banco_obs',
                    'AcademiaComissoes.id',
                    'AcademiaComissoes.comissao',
                    'AcademiaComissoes.aceita',
                    'AcademiaComissoes.paga',
                    'AcademiaComissoes.meta',
                ])
                ->leftJoin(
                    ['AcademiaComissoesPedidos' => 'academia_comissoes_pedidos'],
                    [
                        'AcademiaComissoesPedidos.pedido_id = Pedidos.id',
                    ])
                ->leftJoin(
                    ['AcademiaComissoes' => 'academia_comissoes'],
                    [
                        'AcademiaComissoes.academia_id = Pedidos.academia_id',
                        'AcademiaComissoes.ano = ' . $ano,
                        'AcademiaComissoes.mes = ' . $mes,
                        'AcademiaComissoesPedidos.academia_comissao_id = AcademiaComissoes.id',
                    ])
                ->where(['Pedidos.created LIKE' => $ano . '-' . $mes . '%'])
                ->andWhere(['Pedidos.pedido_status_id >=' => 3])
                ->andWhere(['Pedidos.comissao_status' => 1])
                ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
                ->order(['Academias.name' => 'asc'])
            ;

            $comissoes = [];
            $ped = [];
            foreach ($pedidos as $pedidos_comissao):

                if(!isset($ped[$pedidos_comissao['DISTINCT Pedidos']['id']])):
                    $ped[$pedidos_comissao['DISTINCT Pedidos']['id']] = true;

                    $percent_comissao = $config['academias_comissao'];

                    if(!isset($comissoes['comissao'][$pedidos_comissao->academia_id])):
                        $comissoes['comissao'][$pedidos_comissao->academia_id] = 0;
                    endif;

                    $comissoes['comissao'][$pedidos_comissao->academia_id] += (getPercentCalc($pedidos_comissao->valor, $percent_comissao));

                endif;
            endforeach;

            $pedidos_comissoes = $pedidos
                ->select([
                    'total'     => $pedidos->func()->sum('valor')
                ])
                ->leftJoin(
                    ['AcademiaComissoes' => 'academia_comissoes'],
                    [
                        'AcademiaComissoes.academia_id = Pedidos.academia_id',
                        /*'AcademiaComissoes.ano = ' . $ano,
                        'AcademiaComissoes.mes = ' . $mes,*/
                        'AcademiaComissoesPedidos.academia_comissao_id = AcademiaComissoes.id',
                    ])
                ->group(['Pedidos.academia_id'])
                ->all();

            $academia_comissoes = $pedidos
                ->select([
                    'AcademiaComissoes.aceita',
                    'AcademiaComissoes.paga',
                    'AcademiaComissoes.created',
                    'AcademiaComissoes.ano',
                    'AcademiaComissoes.mes',
                    'AcademiaComissoesPedidos.academia_comissao_id'
                ])
                ->leftJoin(
                    ['AcademiaComissoesPedidos' => 'academia_comissoes_pedidos'],
                    [
                        'AcademiaComissoesPedidos.pedido_id = Pedidos.id',
                    ])
                ->leftJoin(
                    ['AcademiaComissoes' => 'academia_comissoes'],
                    [
                        'AcademiaComissoes.academia_id = Pedidos.academia_id',
                        'AcademiaComissoesPedidos.academia_comissao_id = AcademiaComissoes.id',
                    ])
                ->where(['Pedidos.created LIKE' => $ano . '-' . $mes . '%'])
                ->andWhere(['Pedidos.pedido_status_id >=' => 3])
                ->andWhere(['Pedidos.comissao_status' => 1])
                ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
                ->order(['Academias.name' => 'asc'])
                ->group(['Pedidos.academia_id'])
                ->all();

            foreach ($academia_comissoes as $academia_comissao):

                $date_comissao = new \DateTime();
                $date_comissao->setDate(
                    $academia_comissao->AcademiaComissoes['ano'],
                    $academia_comissao->AcademiaComissoes['mes'] + 1,
                    1
                );

                $date_filtro = new \DateTime();
                $date_filtro->setDate(
                    $ano,
                    $mes,
                    1
                );

                //if($date_comissao > $date_filtro):
                    //debug($academia_comissao->AcademiaComissoes['created']);
                    $comissoes['aceita'][$academia_comissao->academia_id]   = $academia_comissao->AcademiaComissoes['aceita'];
                    $comissoes['paga'][$academia_comissao->academia_id]     = $academia_comissao->AcademiaComissoes['paga'];
                /*else:
                    $comissoes['aceita'][$academia_comissao->academia_id]   = null;
                    $comissoes['paga'][$academia_comissao->academia_id]     = null;
                endif;*/
            endforeach;

            //debug($comissoes);

            $this->set(compact('pedidos_comissoes', 'comissoes', 'config', 'academia_comissoes'));
        }

        $this->set(compact('mes', 'ano'));

    }

    /**
     * View method
     *
     * @param string|null $id Academia Comisso id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $academiaComisso = $this->AcademiaComissoes->get($id, [
            'contain' => ['Academias']
        ]);

        $this->set('academiaComisso', $academiaComisso);
        $this->set('_serialize', ['academiaComisso']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $academiaComisso = $this->AcademiaComissoes->newEntity();
        if ($this->request->is('post')) {
            $academiaComisso = $this->AcademiaComissoes->patchEntity($academiaComisso, $this->request->data);
            if ($this->AcademiaComissoes->save($academiaComisso)) {
                $this->Flash->success(__('The academia comisso has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The academia comisso could not be saved. Please, try again.'));
            }
        }
        $academias = $this->AcademiaComissoes->Academias->find('list', ['limit' => 200]);
        $this->set(compact('academiaComisso', 'academias'));
        $this->set('_serialize', ['academiaComisso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Academia Comisso id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $academiaComisso = $this->AcademiaComissoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $academiaComisso = $this->AcademiaComissoes->patchEntity($academiaComisso, $this->request->data);
            if ($this->AcademiaComissoes->save($academiaComisso)) {
                $this->Flash->success(__('The academia comisso has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The academia comisso could not be saved. Please, try again.'));
            }
        }
        $academias = $this->AcademiaComissoes->Academias->find('list', ['limit' => 200]);
        $this->set(compact('academiaComisso', 'academias'));
        $this->set('_serialize', ['academiaComisso']);
    }

    public function export_csv() {

        $this->response->download('export.csv');

        $ProfessorComissoes = TableRegistry::get('ProfessorComissoes');

        $academiaComissoes = $this->AcademiaComissoes
            ->find('all')
            ->contain(['Academias', 'Academias.Cities'])
            ->where(['AcademiaComissoes.paga' => 0])
            ->andWhere(['Academias.card_status' => 1])
            ->all();

        $professorComissoes = $ProfessorComissoes
            ->find('all')
            ->contain(['Professores', 'Professores.Cities'])
            ->where(['ProfessorComissoes.paga' => 0])
            ->andWhere(['Professores.card_status' => 1])
            ->all();

        $this->set(compact('academiaComissoes', 'professorComissoes'));

        $this->viewBuilder()->layout('ajax');

        return;
    }

}