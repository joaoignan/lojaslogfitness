<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;

/**
 * Banners Controller
 *
 * @property \App\Model\Table\ProdutoAviseme $ProdutoAviseme
 */
class ProdutoAvisemeController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        if ($this->request->is('post')) {
            $aviseme = $this->ProdutoAviseme
                ->find('all')
                ->contain(['Produtos', 'Produtos.ProdutoBase'])
                ->where(['ProdutoAviseme.status' => 0])
                ->andWhere(['Produtos.status_id' => 1])
                ->all();

            Email::configTransport('cliente', [
                'className' => 'Smtp',
                'host' => 'mail.logfitness.com.br',
                'port' => 587,
                'timeout' => 30,
                'username' => EMAIL_USER,
                'password' => EMAIL_SENHA,
                'client' => null,
                'tls' => null,
            ]);

            foreach ($aviseme as $ame) {
                $data = ['status' => 1];

                $ame = $this->ProdutoAviseme->patchEntity($ame, $data);
                if($this->ProdutoAviseme->save($ame)) {
                    $email = new Email();

                    $email
                        ->transport('cliente')
                        ->template('avise_me')
                        ->emailFormat('html')
                        ->from([EMAIL_USER => EMAIL_NAME])
                        ->to($ame->email)
                        ->subject(EMAIL_NAME' - O produto que você queria está disponível!')
                        ->set([
                            'name' => $ame->name,
                            'nome_produto' => $ame->produto->produto_base->name,
                            'slug' => $ame->produto->slug,
                        ])
                        ->send();
                }
            }
        }

        $this->paginate = [
            'contain' => ['Produtos']
        ];
        $this->set('produto_aviseme', $this->paginate($this->ProdutoAviseme));
        $this->set('_serialize', ['produto_aviseme']);
    }
}
