<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Clientes Controller
 *
 * @property \App\Model\Table\ClientesTable $Clientes
 */
class ClientesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $Users = TableRegistry::get('Users');

        $user = $Users->get($this->Auth->user('id'));

        if($user->group_id != 7) {
            $this->paginate = [
                'contain'   => ['Cities', 'Academias', 'Status'],
                'order'     => ['Clientes.id' => 'desc']
            ];
        } else {
            $this->paginate = [
                'contain'    => ['Cities', 'Academias', 'Status'],
                'order'      => ['Clientes.id' => 'desc'],
                'conditions' => ['Academias.user_id' => $user->id]
            ];
        }

        
        $this->set('clientes', $this->paginate($this->Clientes));
        $this->set('_serialize', ['clientes']);
    }

    /**
     * View method
     *
     * @param string|null $id Cliente id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cliente = $this->Clientes->get($id, [
            'contain' => ['Cities', 'Academias', 'Status', 'Pedidos']
        ]);
        $this->set('cliente', $cliente);
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cliente = $this->Clientes->newEntity();
        if ($this->request->is('post')) {
            $cliente = $this->Clientes->patchEntity($cliente, $this->request->data);
            if ($this->Clientes->save($cliente)) {
                $this->Flash->success('cliente salvo com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar cliente. Tente novamente.');
            }
        }
        $cities = $this->Clientes->Cities->find('list', ['limit' => 200]);
        $academias = $this->Clientes->Academias->find('list', ['limit' => 200]);
        $status = $this->Clientes->Status->find('list', ['limit' => 200]);
        $this->set(compact('cliente', 'cities', 'academias', 'status'));
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cliente = $this->Clientes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cliente = $this->Clientes->patchEntity($cliente, $this->request->data);
            if ($this->Clientes->save($cliente)) {
                $this->Flash->success('cliente salvo com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar cliente. Tente novamente.');
            }
        }
        $cities = $this->Clientes->Cities->find('list', ['limit' => 200]);
        $academias = $this->Clientes->Academias->find('list', ['limit' => 200]);
        $status = $this->Clientes->Status->find('list', ['limit' => 200]);
        $this->set(compact('cliente', 'cities', 'academias', 'status'));
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cliente = $this->Clientes->get($id);
        if ($this->Clientes->delete($cliente)) {
            $this->Flash->success('cliente apagado com sucesso.');
        } else {
            $this->Flash->error('Falha ao apagar cliente. Tente novamente.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
