<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\I18n\Time;

class IngredientesController extends AppController
{
    public function sincronizar_ingredientes() {
        $todos_ingredientes = $this->Ingredientes
            ->find('all')
            ->all();

        foreach ($todos_ingredientes as $ingrediente) {
            $ingredientes_ids_array[] = $ingrediente->id;
        } 

        $ch = curl_init();

        $header = [
            "Content-Type: application/json"
        ];

        $data = [
            "ingredientes_ids_array" => $ingredientes_ids_array
        ];

        $data = json_encode($data);

        $options = array(CURLOPT_URL => "https://api.logfitness.com.br/ingredientes/sync?api_key=".LOGFITNESS_API_KEY,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $api_novos_ingredientes = json_decode($response);

        if($api_novos_ingredientes->status_id == 1 && $api_novos_ingredientes->retorno_cod == 'sucesso') {
            $falha = 0;

            foreach ($api_novos_ingredientes->ingredientes as $ingrediente) {

                $new_ingrediente_data = [
                    'id' => $ingrediente->id,
                    'name' => $ingrediente->name,
                ];

                $new_ingrediente = $this->Ingredientes->newEntity($new_ingrediente_data);
                
                if(!$this->Ingredientes->save($new_ingrediente)) {
                    $falha = 1;
                }
            }

            if($falha == 0) {
                $this->Flash->success('Ingredientes sincronizadas com sucesso!');
            } else {
                $this->Flash->error('Falha ao atualizar banco de dados, atualize a página!');
            }
        }
    }

	public function index(){

        $this->sincronizar_ingredientes();

        $this->paginate = [
        ];
        $this->set('ingredientes', $this->paginate($this->Ingredientes));
        $this->set('_serialize', ['ingredientes']);
    }

    public function add(){

        $this->sincronizar_ingredientes();

    	if ($this->request->is('post')) {

            do {
                //Add ingrediente no BD da API LogFitness
                $ch = curl_init();

                $header = [
                    "Content-Type: application/json"
                ];

                $data = [
                    "name" => $this->request->data['name'],
                    "created" => Time::now(),
                    "modified" => Time::now(),
                ];

                $data = json_encode($data);

                $options = array(CURLOPT_URL => "https://api.logfitness.com.br/ingredientes/add?api_key=".LOGFITNESS_API_KEY,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $header,
                    CURLOPT_SSL_VERIFYPEER => false
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $api_novo_ingrediente = json_decode($response);

                if($api_novo_ingrediente->status_id == 2) {
                    $this->Flash->error($api_novo_ingrediente->mensagem);
                    return $this->redirect(['action' => 'add']);
                }

                if($api_novo_ingrediente->status_id == 1) {
                    $this->Flash->success($api_novo_ingrediente->mensagem);
                    return $this->redirect(['action' => 'index']);
                }
            } while($api_novo_ingrediente->status_id < 1);
        }
    }

    public function edit($id = null){
        $ingrediente = $this->Ingredientes->get($id, [
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ingrediente = $this->Ingredientes->patchEntity($ingrediente, $this->request->data);
            if ($this->Ingredientes->save($ingrediente)) {
                $this->Flash->success('Ingrediente salva com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar ingrediente. Tente novamente.');
            }
        }
        $this->set(compact('ingrediente'));
        $this->set('_serialize', ['ingrediente']);
    }
}