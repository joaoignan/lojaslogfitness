<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Leads Controller
 *
 * @property \App\Model\Table\LeadsTable $Leads
 */
class LeadsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain'   => ['Cities'],
            'order'     => ['Leads.id' => 'desc']
        ];

        $leads = $this->paginate($this->Leads);

        if($this->request->is(['post', 'put'])) {
            $deu_certo = 0;
            foreach($this->request->data['paga'] as $key => $item){
                $leads = $this->Leads->get($key);
                $data       = [
                    'status'  => $item,
                ];

                $leads = $this->Leads->patchEntity($leads, $data);

                if($this->Leads->save($leads)){
                    $deu_certo = 1;
                }
            }

            if($deu_certo == 1) {
                $this->Flash->success('Informações alteradas com sucesso!');
                $this->redirect('/admin/leads');
            } else {
                $this->Flash->error('Falha ao salvar informações.. Tente novamente..');
                $this->redirect('/admin/leads');
            }
        }

        $this->set(compact('leads'));
        $this->set('_serialize', ['leads']);
    }
}
