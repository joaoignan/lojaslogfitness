<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
/**
 * Act Controller
 *
 * @property \App\Model\Table\ActTable $Act
 */
class ActController extends AppController
{
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain'   => ['Status'],
            'order'     => ['Act.id' => 'desc'],
        ];
        $this->set('act', $this->paginate($this->Act));
        $this->set('_serialize', ['act']);

        //shell_exec(WWW_ROOT);
    }

    /**
     * View method
     *
     * @param string|null $id Act id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $act = $this->Act->get($id, [
            'contain' => ['Status']
        ]);
        $this->set('act', $act);
        $this->set('_serialize', ['act']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $act = $this->Act->newEntity();
        if ($this->request->is('post')) {
            /**
             * PREPARA O ARQUIVO PARA SALVAR NA PASTA
             * VERIFICAR SE A PASTA TEM PERMISSÃO DE SALVAR
             */
            $info = pathinfo($this->request->data['arquivo']['name']);
            $ext = $info['extension']; // get the extension of the file
            $newname = sanitizeString(str_replace($ext,"",$this->request->data['arquivo']['name']))."".date("YmdHis").".".$ext;
            $target = WWW_ROOT.'files'.DS.$newname;
            move_uploaded_file( $this->request->data['arquivo']['tmp_name'], $target);
            $this->request->data['arquivo'] = $newname;
            $act = $this->Act->patchEntity($act, $this->request->data);
            if ($this->Act->save($act)) {
                $this->Flash->success('Material salvo com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar material. Tente novamente.');
            }
        }
        $status             = $this->Act->Status->find('list');


        $this->set(compact('act', 'status'));
        $this->set('_serialize', ['act']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Act id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $act = $this->Act->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $act = $this->Act->patchEntity($act, $this->request->data);
            if ($this->Act->save($act)) {
                $this->Flash->success('Material salvo com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar material. Tente novamente.');
            }
        }
        $status = $this->Act->Status->find('list', ['limit' => 200]);
        $this->set(compact('act', 'status'));
        $this->set('_serialize', ['act']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Act id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $act = $this->Act->get($id);
        if ($this->Act->delete($act)) {
            $this->Flash->success('Material apagado com sucesso.');
        } else {
            $this->Flash->error('Falha ao apagar material. Tente novamente.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
