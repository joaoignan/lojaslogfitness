<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Propriedades Controller
 *
 * @property \App\Model\Table\PropriedadesTable $Propriedades
 */
class PropriedadesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Status']
        ];
        $propriedades = $this->paginate($this->Propriedades);

        $this->set(compact('propriedades'));
        $this->set('_serialize', ['propriedades']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $propriedade = $this->Propriedades->newEntity();
        if ($this->request->is('post')) {
            $propriedade = $this->Propriedades->patchEntity($propriedade, $this->request->data);
            if ($this->Propriedades->save($propriedade)) {
                $this->Flash->success(__('The propriedade has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The propriedade could not be saved. Please, try again.'));
            }
        }
        $status = $this->Propriedades->Status->find('list', ['limit' => 200]);
        $this->set(compact('propriedade', 'status'));
        $this->set('_serialize', ['propriedade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Propriedade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $propriedade = $this->Propriedades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $propriedade = $this->Propriedades->patchEntity($propriedade, $this->request->data);
            if ($this->Propriedades->save($propriedade)) {
                $this->Flash->success(__('The propriedade has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The propriedade could not be saved. Please, try again.'));
            }
        }
        $status = $this->Propriedades->Status->find('list', ['limit' => 200]);
        $this->set(compact('propriedade', 'status'));
        $this->set('_serialize', ['propriedade']);
    }

}
