<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * AcademiasSugestoes Controller
 *
 * @property \App\Model\Table\AcademiasSugestoesTable $AcademiasSugestoes
 */
class AcademiasSugestoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain'   => ['Cities', 'Professores'],
            'order'     => ['AcademiasSugestoes.id' => 'desc']
        ];

        $academiasSugestoes = $this->paginate($this->AcademiasSugestoes);

        $this->set(compact('academiasSugestoes'));
        $this->set('_serialize', ['academiasSugestoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Academias Sugesto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $academiasSugesto = $this->AcademiasSugestoes->get($id, [
            'contain' => ['Cities']
        ]);

        $this->set('academiasSugesto', $academiasSugesto);
        $this->set('_serialize', ['academiasSugesto']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $academiasSugesto = $this->AcademiasSugestoes->newEntity();
        if ($this->request->is('post')) {
            $academiasSugesto = $this->AcademiasSugestoes->patchEntity($academiasSugesto, $this->request->data);
            if ($this->AcademiasSugestoes->save($academiasSugesto)) {
                $this->Flash->success(__('The academias sugesto has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The academias sugesto could not be saved. Please, try again.'));
            }
        }
        $cities = $this->AcademiasSugestoes->Cities->find('list', ['limit' => 200]);
        $this->set(compact('academiasSugesto', 'cities'));
        $this->set('_serialize', ['academiasSugesto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Academias Sugesto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $academiasSugesto = $this->AcademiasSugestoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $academiasSugesto = $this->AcademiasSugestoes->patchEntity($academiasSugesto, $this->request->data);
            if ($this->AcademiasSugestoes->save($academiasSugesto)) {
                $this->Flash->success(__('The academias sugesto has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The academias sugesto could not be saved. Please, try again.'));
            }
        }
        $cities = $this->AcademiasSugestoes->Cities->find('list', ['limit' => 200]);
        $this->set(compact('academiasSugesto', 'cities'));
        $this->set('_serialize', ['academiasSugesto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Academias Sugesto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $academiasSugesto = $this->AcademiasSugestoes->get($id);
        if ($this->AcademiasSugestoes->delete($academiasSugesto)) {
            $this->Flash->success(__('The academias sugesto has been deleted.'));
        } else {
            $this->Flash->error(__('The academias sugesto could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
