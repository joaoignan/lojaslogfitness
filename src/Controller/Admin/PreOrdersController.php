<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * PreOrders Controller
 *
 * @property \App\Model\Table\PreOrdersTable $PreOrders
 */
class PreOrdersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'Professores',
                'Academias',
                'Clientes',
                'Pedidos',
                'PreOrderItems',
                'PreOrderItems.Produtos',
                'PreOrderItems.Produtos.ProdutoBase'
            ],
            'order' => ['PreOrders.pedido_id' => 'desc', 'PreOrders.id' => 'desc']
        ];
        $preOrders = $this->paginate($this->PreOrders);

        $this->set(compact('preOrders'));
        $this->set('_serialize', ['preOrders']);
    }

    /**
     * View method
     *
     * @param string|null $id Pre Order id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $preOrder = $this->PreOrders->get($id, [
            'contain' => ['Professores', 'Academias', 'Clientes', 'Pedidos', 'PreOrderItems']
        ]);

        $this->set('preOrder', $preOrder);
        $this->set('_serialize', ['preOrder']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $preOrder = $this->PreOrders->newEntity();
        if ($this->request->is('post')) {
            $preOrder = $this->PreOrders->patchEntity($preOrder, $this->request->data);
            if ($this->PreOrders->save($preOrder)) {
                $this->Flash->success(__('The pre order has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pre order could not be saved. Please, try again.'));
            }
        }
        $professores = $this->PreOrders->Professores->find('list', ['limit' => 200]);
        $academias = $this->PreOrders->Academias->find('list', ['limit' => 200]);
        $clientes = $this->PreOrders->Clientes->find('list', ['limit' => 200]);
        $pedidos = $this->PreOrders->Pedidos->find('list', ['limit' => 200]);
        $this->set(compact('preOrder', 'professores', 'academias', 'clientes', 'pedidos'));
        $this->set('_serialize', ['preOrder']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pre Order id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $preOrder = $this->PreOrders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $preOrder = $this->PreOrders->patchEntity($preOrder, $this->request->data);
            if ($this->PreOrders->save($preOrder)) {
                $this->Flash->success(__('The pre order has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pre order could not be saved. Please, try again.'));
            }
        }
        $professores = $this->PreOrders->Professores->find('list', ['limit' => 200]);
        $academias = $this->PreOrders->Academias->find('list', ['limit' => 200]);
        $clientes = $this->PreOrders->Clientes->find('list', ['limit' => 200]);
        $pedidos = $this->PreOrders->Pedidos->find('list', ['limit' => 200]);
        $this->set(compact('preOrder', 'professores', 'academias', 'clientes', 'pedidos'));
        $this->set('_serialize', ['preOrder']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pre Order id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $preOrder = $this->PreOrders->get($id);
        if ($this->PreOrders->delete($preOrder)) {
            $this->Flash->success(__('The pre order has been deleted.'));
        } else {
            $this->Flash->error(__('The pre order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
