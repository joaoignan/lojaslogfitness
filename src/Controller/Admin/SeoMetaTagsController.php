<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Core\App;

/**
 * SeoMetaTags Controller
 *
 * @property \App\Model\Table\SeoMetaTagsTable $SeoMetaTags
 */
class SeoMetaTagsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        if($this->request->is('post')){
            $controller_action = explode('/', $this->request->data['controller_action']);
            $this->redirect(array('action' => 'edit', $controller_action[0], $controller_action[1]));
        }

        $controllers = $this->getControllers();

        $actions = [];
        foreach($controllers as $controller):

            $controllers_actions = $this->getActions($controller);

            foreach($controllers_actions[$controller] as $keyA => $Action) {
                if ($controller != 'Wi') {
                    $actions[] = array(
                        'text' => $controller . '/'   . $Action . ' ::: [' . __($controller) . '/' . __($Action) . ']',
                        'value' => $controller . '/'    . $Action,
                    );
                }
            }

        endforeach;

        $this->set(compact('actions'));
    }

    /**
     * @param null $controller
     * @param null $action
     */
    public function edit($controller = null, $action = null){
        if (is_null($controller) || is_null($action)) {
            $this->Flash->warning('Parâmetros Inválidos!');
            $this->redirect(['action' => 'index']);
        }

        $tags = $this->SeoMetaTags
            ->find('all')
            ->where([
                'SeoMetaTags.controller'	=> $controller,
                'SeoMetaTags.action'		=> $action
            ])
            ->all();

        if ($this->request->is(['post', 'put'])) {
            $messages = [];
            $this->request->data = array_filter($this->request->data);
            $data_seo = [];
            foreach($this->request->data['name'] as $key => $tag){
                $data['seo_meta_attribute_id']  = $this->request->data['seo_meta_attribute_id'][$key];
                $data['name']                   = $this->request->data['name'][$key];
                $data['content']                = $this->request->data['content'][$key];
                $data['controller']             = $controller;
                $data['action']                 = $action;

                if(!empty($data['name']) || !empty($data['content'])) {
                    if($this->request->data['cod'][$key] > 0){
                        $data['id']             = $this->request->data['cod'][$key];
                        $data_seo[$data['id']]  = $data;

                        $seoMetaTag = $this->SeoMetaTags->get($data['id'], [
                            'contain' => []
                        ]);
                        $seoMetaTag = $this->SeoMetaTags->patchEntity($seoMetaTag, $data);
                    }else{
                        if(isset($data['id'])){unset($data['id']);}
                        $data_seo['_'.$key] = $data;
                        $seoMetaTag = $this->SeoMetaTags->newEntity();
                        $seoMetaTag = $this->SeoMetaTags->patchEntity($seoMetaTag, $data);
                    }
                    if ($this->SeoMetaTags->save($seoMetaTag)) {
                        $messages[] =
                            'Sucesso -> ' .
                            $data['seo_meta_attribute_id'] . ' = ' .
                            $data['name'] .
                            ' :: Content = ' .
                            $data['content'];
                    } else {
                        $messages[] =
                            'Erro -> ' .
                            $data['seo_meta_attribute_id'] . ' = ' .
                            $data['name'] .
                            ' :: Content = ' .
                            $data['content'];
                    }
                }
            }

            foreach($tags as $t => $tag){
                if(!isset($data_seo[$tag['id']])){
                    $tag_delete = $this->SeoMetaTags->get($tag['id']);
                    if($this->SeoMetaTags->delete($tag_delete)){
                        $messages[] =
                            'Removido -> ' .
                            $tag['name'] . ' = ' .
                            $tag['name'] .
                            ' :: Content = ' .
                            $tag['content'];
                    }
                }
            }

            $messages = implode('<br>', $messages);
            $this->Flash->success($messages);
            $this->redirect(['action' => 'edit', $controller, $action]);
        }

        $seoMetaAttributes = $this->SeoMetaTags->SeoMetaAttributes->find('list');
        $this->set(compact('seoMetaAttributes', 'controller', 'action', 'tags'));
    }

}
