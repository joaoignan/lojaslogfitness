<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Slides Controller
 *
 * @property \App\Model\Table\SlidesTable $Slides
 */
class SlidesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Status']
        ];
        $this->set('slides', $this->paginate($this->Slides));
        $this->set('_serialize', ['slides']);
    }

    /**
     * View method
     *
     * @param string|null $id Slide id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $slide = $this->Slides->get($id, [
            'contain' => ['Status']
        ]);
        $this->set('slide', $slide);
        $this->set('_serialize', ['slide']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $slide = $this->Slides->newEntity();
        if ($this->request->is('post')) {
            $slide = $this->Slides->patchEntity($slide, $this->request->data);
            if ($this->Slides->save($slide)) {
                $this->Flash->success('The slide has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The slide could not be saved. Please, try again.');
            }
        }
        $status = $this->Slides->Status->find('list', ['limit' => 200]);
        $this->set(compact('slide', 'status'));
        $this->set('_serialize', ['slide']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Slide id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $slide = $this->Slides->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $slide = $this->Slides->patchEntity($slide, $this->request->data);
            if ($this->Slides->save($slide)) {
                $this->Flash->success('The slide has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The slide could not be saved. Please, try again.');
            }
        }
        $status = $this->Slides->Status->find('list', ['limit' => 200]);
        $this->set(compact('slide', 'status'));
        $this->set('_serialize', ['slide']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Slide id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $slide = $this->Slides->get($id);
        if ($this->Slides->delete($slide)) {
            $this->Flash->success('The slide has been deleted.');
        } else {
            $this->Flash->error('The slide could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
}