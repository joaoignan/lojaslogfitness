<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ClienteEsportes Controller
 *
 * @property \App\Model\Table\ClienteEsportesTable $ClienteEsportes
 */
class ClienteEsportesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Clientes', 'Esportes']
        ];
        $this->set('clienteEsportes', $this->paginate($this->ClienteEsportes));
        $this->set('_serialize', ['clienteEsportes']);
    }

    /**
     * View method
     *
     * @param string|null $id Cliente Esporte id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $clienteEsporte = $this->ClienteEsportes->get($id, [
            'contain' => ['Clientes', 'Esportes']
        ]);
        $this->set('clienteEsporte', $clienteEsporte);
        $this->set('_serialize', ['clienteEsporte']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $clienteEsporte = $this->ClienteEsportes->newEntity();
        if ($this->request->is('post')) {
            $clienteEsporte = $this->ClienteEsportes->patchEntity($clienteEsporte, $this->request->data);
            if ($this->ClienteEsportes->save($clienteEsporte)) {
                $this->Flash->success('cliente esporte salvo com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar cliente esporte. Tente novamente.');
            }
        }
        $clientes = $this->ClienteEsportes->Clientes->find('list', ['limit' => 200]);
        $esportes = $this->ClienteEsportes->Esportes->find('list', ['limit' => 200]);
        $this->set(compact('clienteEsporte', 'clientes', 'esportes'));
        $this->set('_serialize', ['clienteEsporte']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente Esporte id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $clienteEsporte = $this->ClienteEsportes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $clienteEsporte = $this->ClienteEsportes->patchEntity($clienteEsporte, $this->request->data);
            if ($this->ClienteEsportes->save($clienteEsporte)) {
                $this->Flash->success('cliente esporte salvo com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar cliente esporte. Tente novamente.');
            }
        }
        $clientes = $this->ClienteEsportes->Clientes->find('list', ['limit' => 200]);
        $esportes = $this->ClienteEsportes->Esportes->find('list', ['limit' => 200]);
        $this->set(compact('clienteEsporte', 'clientes', 'esportes'));
        $this->set('_serialize', ['clienteEsporte']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente Esporte id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $clienteEsporte = $this->ClienteEsportes->get($id);
        if ($this->ClienteEsportes->delete($clienteEsporte)) {
            $this->Flash->success('cliente esporte apagado com sucesso.');
        } else {
            $this->Flash->error('Falha ao apagar cliente esporte. Tente novamente.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
