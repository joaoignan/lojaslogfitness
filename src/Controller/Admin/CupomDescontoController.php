<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Database\Schema\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * CupomDesconto Controller
 *
 * @property \App\Model\Table\CupomDesconto $CupomDesconto
 */
class CupomDescontoController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index(){
        $this->paginate = [
            'contain'   => [],
            'order'     => ['CupomDesconto.id' => 'desc']
        ];

        $data_atual = Time::now();


        $this->set('cupons', $this->paginate($this->CupomDesconto));
        $this->set('data_atual', $data_atual);
        $this->set('_serialize', ['cupons']);
    }

	public function add(){
        $cupom = $this->CupomDesconto->newEntity();
        if ($this->request->is('post')) {

            $this->request->data['status'] = 1;
            $this->request->data['created'] = Time::now();
            $this->request->data['modified'] = Time::now();

            $cupom = $this->CupomDesconto->patchEntity($cupom, $this->request->data);
            if ($this->CupomDesconto->save($cupom)) {
                $this->Flash->success('Cupom criado com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao criar cupom. Tente novamente.');
            }
        }
        $this->set(compact('cupom'));
        $this->set('_serialize', ['cupom']);
	}

    public function edit($id = null){
        $cupom = $this->CupomDesconto->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['modified'] = Time::now();
            
            if($this->request->data['status'] == 0) {
                $this->request->data['status'] = 1;
            } else {
                $this->request->data['status'] = 2;
            }

            $cupom = $this->CupomDesconto->patchEntity($cupom, $this->request->data);
            if ($this->CupomDesconto->save($cupom)) {
                $this->Flash->success('Cupom alterado com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao alterar cupom. Tente novamente.');
            }
        }
        $this->set(compact('cupom'));
        $this->set('_serialize', ['cupom']);
    }
}