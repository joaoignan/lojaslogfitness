<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Diferenciais Controller
 *
 * @property \App\Model\Table\DiferenciaisTable $Diferenciais
 */
class DiferenciaisController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [

        ];

        $diferenciais = $this->paginate($this->Diferenciais);

        $this->set(compact('diferenciais'));
        $this->set('_serialize', ['diferenciais']);
    }

    /**
     * View method
     *
     * @param string|null $id Categoria id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $diferencial = $this->Diferenciais->get($id, [

        ]);

        $this->set('diferencial', $diferencial);
        $this->set('_serialize', ['diferencial']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $diferenciais = $this->Diferenciais->newEntity();
        if ($this->request->is('post')) {
            $diferenciais = $this->Diferenciais->patchEntity($diferenciais, $this->request->data);
            if ($this->Diferenciais->save($diferenciais)) {
                $this->Flash->success(__('O diferencial foi salvo.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O diferencial não pode ser salvo... Tente novamente...'));
            }
        }
        $this->set(compact('diferencial'));
        $this->set('_serialize', ['diferencial']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Categoria id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $diferencial = $this->Diferenciais->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $diferencial = $this->Diferenciais->patchEntity($diferencial, $this->request->data);
            if ($this->Diferenciais->save($diferencial)) {
                $this->Flash->success(__('O diferencial foi alterado.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O diferencial não pode ser alterado... Tente novamente...'));
            }
        }
        $this->set(compact('diferencial'));
        $this->set('_serialize', ['diferencial']);
    }
}
