<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Objetivos Controller
 *
 * @property \App\Model\Table\ObjetivosTable $Objetivos
 */
class ObjetivosController extends AppController
{

    public function sincronizar_objetivos() {
        $todos_objetivos = $this->Objetivos
            ->find('all')
            ->all();

        foreach ($todos_objetivos as $objetivo) {
            $objetivos_ids_array[] = $objetivo->id;
        } 

        $ch = curl_init();

        $header = [
            "Content-Type: application/json"
        ];

        $data = [
            "objetivos_ids_array" => $objetivos_ids_array
        ];

        $data = json_encode($data);

        $options = array(CURLOPT_URL => "https://api.logfitness.com.br/objetivos/sync?api_key=".LOGFITNESS_API_KEY,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $api_novos_objetivos = json_decode($response);

        if($api_novos_objetivos->status_id == 1 && $api_novos_objetivos->retorno_cod == 'sucesso') {
            $falha = 0;

            foreach ($api_novos_objetivos->objetivos as $objetivo) {

                $new_objetivo_data = [
                    'id' => $objetivo->id,
                    'name' => $objetivo->name,
                    'slug' => $objetivo->slug,
                    'texto_tarja' => $objetivo->texto_tarja,
                    'image' => $objetivo->image,
                    'color' => $objetivo->color,
                    'banner_lateral' => $objetivo->banner_lateral,
                    'banner_fundo' => $objetivo->banner_fundo,
                    'status_id' => $objetivo->status_id
                ];

                $new_objetivo = $this->Objetivos->newEntity($new_objetivo_data);
                
                if(!$this->Objetivos->save($new_objetivo)) {
                    $falha = 1;
                }
            }

            if($falha == 0) {
                $this->Flash->success('Objetivos sincronizados com sucesso!');
            } else {
                $this->Flash->error('Falha ao atualizar banco de dados, atualize a página!');
            }
        }
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->sincronizar_objetivos();

        $this->paginate = [
            'contain' => ['Status']
        ];
        $this->set('objetivos', $this->paginate($this->Objetivos));
        $this->set('_serialize', ['objetivos']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->sincronizar_objetivos();

        $VariaveisGlobais = TableRegistry::get('VariaveisGlobais');

        $modelo_banner = $VariaveisGlobais
            ->find('all')
            ->Where(['name' => 'tipo_banner'])
            ->first();

        $tipo_banner = $modelo_banner->valor;

        if ($this->request->is('post')) {

            $banner_lateral = $this->Objetivos->newUploadImageBannerLateral($this->request->data['banner_lateral'], $this->request->data['name'], $this->request->data['banner_lateral_cropx'], $this->request->data['banner_lateral_cropy'], $this->request->data['banner_lateral_cropw'], $this->request->data['banner_lateral_croph']);

            if ($tipo_banner == 0) {
                $banner_fundo = $this->Objetivos->newUploadImageBannerFundo($this->request->data['banner_fundo'], $this->request->data['name'], $this->request->data['banner_fundo_cropx'], $this->request->data['banner_fundo_cropy'], $this->request->data['banner_fundo_cropw'], $this->request->data['banner_fundo_croph']);
            } elseif ($tipo_banner == 1) {
                $banner_fundo = $this->Objetivos->newUploadImageBannerFundoCurto($this->request->data['banner_fundo'], $this->request->data['name'], $this->request->data['banner_fundo_cropx'], $this->request->data['banner_fundo_cropy'], $this->request->data['banner_fundo_cropw'], $this->request->data['banner_fundo_croph']);
            }

            $count = 0;

            do {
                //Add marca no BD da API LogFitness
                $ch = curl_init();

                $header = [
                    "Content-Type: application/json"
                ];

                $data = [
                    "name" => $this->request->data['name'],
                    "texto_tarja" => $this->request->data['texto_tarja'],
                    "banner_lateral" => $banner_lateral,
                    "banner_fundo" => $banner_fundo
                ];

                $data = json_encode($data);

                $options = array(CURLOPT_URL => "https://api.logfitness.com.br/objetivos/add?api_key=".LOGFITNESS_API_KEY,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $header,
                    CURLOPT_SSL_VERIFYPEER => false
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $api_novo_objetivo = json_decode($response);

                if($api_novo_objetivo->status_id == 2) {
                    $this->Flash->error($api_novo_objetivo->mensagem);
                    return $this->redirect(['action' => 'add']);
                }

                if($api_novo_objetivo->status_id == 1) {
                    $this->Flash->success($api_novo_objetivo->mensagem);
                    return $this->redirect(['action' => 'index']);
                }

                $count++;
            } while($api_novo_objetivo->status_id < 1 && $count < 100);
        }

        $this->set(compact('tipo_banner'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Objetivo id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $objetivo = $this->Objetivos->get($id, [
            'contain' => []
        ]);

        $VariaveisGlobais = TableRegistry::get('VariaveisGlobais');

        $modelo_banner = $VariaveisGlobais
            ->find('all')
            ->Where(['name' => 'tipo_banner'])
            ->first();

        $tipo_banner = $modelo_banner->valor;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $objetivo = $this->Objetivos->patchEntity($objetivo, $this->request->data);
            if ($this->Objetivos->save($objetivo)) {
                $this->Flash->success('objetivo salvo com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar objetivo. Tente novamente.');
            }
        }
        $this->set(compact('objetivo', 'tipo_banner'));
        $this->set('_serialize', ['objetivo']);
    }

}
