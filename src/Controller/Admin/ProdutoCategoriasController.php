<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ProdutoCategorias Controller
 *
 * @property \App\Model\Table\ProdutoCategoriasTable $ProdutoCategorias
 */
class ProdutoCategoriasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ProdutoBase', 'Categorias']
        ];
        $produtoCategorias = $this->paginate($this->ProdutoCategorias);

        $this->set(compact('produtoCategorias'));
        $this->set('_serialize', ['produtoCategorias']);
    }

    /**
     * View method
     *
     * @param string|null $id Produto Categoria id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $produtoCategoria = $this->ProdutoCategorias->get($id, [
            'contain' => ['ProdutoBase', 'Categorias']
        ]);

        $this->set('produtoCategoria', $produtoCategoria);
        $this->set('_serialize', ['produtoCategoria']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $produtoCategoria = $this->ProdutoCategorias->newEntity();
        if ($this->request->is('post')) {
            $produtoCategoria = $this->ProdutoCategorias->patchEntity($produtoCategoria, $this->request->data);
            if ($this->ProdutoCategorias->save($produtoCategoria)) {
                $this->Flash->success(__('The produto categoria has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The produto categoria could not be saved. Please, try again.'));
            }
        }
        $produtoBase = $this->ProdutoCategorias->ProdutoBase->find('list', ['limit' => 200]);
        $categorias = $this->ProdutoCategorias->Categorias->find('list', ['limit' => 200]);
        $this->set(compact('produtoCategoria', 'produtoBase', 'categorias'));
        $this->set('_serialize', ['produtoCategoria']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Produto Categoria id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $produtoCategoria = $this->ProdutoCategorias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $produtoCategoria = $this->ProdutoCategorias->patchEntity($produtoCategoria, $this->request->data);
            if ($this->ProdutoCategorias->save($produtoCategoria)) {
                $this->Flash->success(__('The produto categoria has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The produto categoria could not be saved. Please, try again.'));
            }
        }
        $produtoBase = $this->ProdutoCategorias->ProdutoBase->find('list', ['limit' => 200]);
        $categorias = $this->ProdutoCategorias->Categorias->find('list', ['limit' => 200]);
        $this->set(compact('produtoCategoria', 'produtoBase', 'categorias'));
        $this->set('_serialize', ['produtoCategoria']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Produto Categoria id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $produtoCategoria = $this->ProdutoCategorias->get($id);
        if ($this->ProdutoCategorias->delete($produtoCategoria)) {
            $this->Flash->success(__('The produto categoria has been deleted.'));
        } else {
            $this->Flash->error(__('The produto categoria could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
