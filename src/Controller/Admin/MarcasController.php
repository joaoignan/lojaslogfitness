<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Marcas Controller
 *
 * @property \App\Model\Table\MarcasTable $Marcas
 */
class MarcasController extends AppController
{

    public function sincronizar_marcas() {
        $todas_marcas = $this->Marcas
            ->find('all')
            ->all();

        foreach ($todas_marcas as $marca) {
            $marcas_ids_array[] = $marca->id;
        } 

        $ch = curl_init();

        $header = [
            "Content-Type: application/json"
        ];

        $data = [
            "marcas_ids_array" => $marcas_ids_array
        ];

        $data = json_encode($data);

        $options = array(CURLOPT_URL => "https://api.logfitness.com.br/marcas/sync?api_key=".LOGFITNESS_API_KEY,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $api_novas_marcas = json_decode($response);

        if($api_novas_marcas->status_id == 1 && $api_novas_marcas->retorno_cod == 'sucesso') {
            $falha = 0;

            foreach ($api_novas_marcas->marcas as $marca) {

                $new_marca_data = [
                    'id' => $marca->id,
                    'cod_interno' => $marca->cod_interno,
                    'name' => $marca->name,
                    'slug' => $marca->slug,
                    'image' => $marca->image,
                    'banner_fundo' => $marca->banner_fundo,
                    'status_id' => $marca->status_id
                ];

                $new_marca = $this->Marcas->newEntity($new_marca_data);
                
                if(!$this->Marcas->save($new_marca)) {
                    $falha = 1;
                }
            }

            if($falha == 0) {
                $this->Flash->success('Marcas sincronizadas com sucesso!');
            } else {
                $this->Flash->error('Falha ao atualizar banco de dados, atualize a página!');
            }
        }
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        
        $this->sincronizar_marcas();

        $this->paginate = [
            'contain' => ['Status']
        ];
        $this->set('marcas', $this->paginate($this->Marcas));
        $this->set('_serialize', ['marcas']);
    }

    /**
     * View method
     *
     * @param string|null $id Marca id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $marca = $this->Marcas->get($id, [
            'contain' => ['Status']
        ]);
        $this->set('marca', $marca);
        $this->set('_serialize', ['marca']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->sincronizar_marcas();

        $VariaveisGlobais = TableRegistry::get('VariaveisGlobais');

        $modelo_banner = $VariaveisGlobais
            ->find('all')
            ->Where(['name' => 'tipo_banner'])
            ->first();

        $tipo_banner = $modelo_banner->valor;

        if ($this->request->is('post')) {

            $count = 0;

            $image = $this->Marcas->newUploadImage($this->request->data['image'], $this->request->data['name']);
            
            if ($tipo_banner == 0) {
                $banner_fundo = $this->Marcas->newUploadImageBannerFundo($this->request->data['banner_fundo'], $this->request->data['name'], $this->request->data['banner_fundo_cropx'], $this->request->data['banner_fundo_cropy'], $this->request->data['banner_fundo_cropw'], $this->request->data['banner_fundo_croph']);
            } else if ($tipo_banner == 1) {
                $banner_fundo = $this->Marcas->newUploadImageBannerFundoCurto($this->request->data['banner_fundo'], $this->request->data['name'], $this->request->data['banner_fundo_cropx'], $this->request->data['banner_fundo_cropy'], $this->request->data['banner_fundo_cropw'], $this->request->data['banner_fundo_croph']);
            }

            do {
                //Add marca no BD da API LogFitness
                $ch = curl_init();

                $header = [
                    "Content-Type: application/json"
                ];


                $data = [
                    "name" => $this->request->data['name'],
                    "image" => $image,
                    "banner_fundo" => $banner_fundo,
                ];

                $data = json_encode($data);

                $options = array(CURLOPT_URL => "https://api.logfitness.com.br/marcas/add?api_key=".LOGFITNESS_API_KEY,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $header,
                    CURLOPT_SSL_VERIFYPEER => false
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $api_nova_marca = json_decode($response);

                if($api_nova_marca->status_id == 2) {
                    $this->Flash->error($api_nova_marca->mensagem);
                    return $this->redirect(['action' => 'add']);
                }

                if($api_nova_marca->status_id == 1) {
                    $this->Flash->success($api_nova_marca->mensagem);
                    return $this->redirect(['action' => 'index']);
                }

                $count++;
            } while($api_nova_marca->status_id < 1 && $count < 100);
        }

        $this->set(compact('tipo_banner'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Marca id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $marca = $this->Marcas->get($id, [
            'contain' => []
        ]);

        $VariaveisGlobais = TableRegistry::get('VariaveisGlobais');

        $modelo_banner = $VariaveisGlobais
            ->find('all')
            ->Where(['name' => 'tipo_banner'])
            ->first();

        $tipo_banner = $modelo_banner->valor;

        if ($this->request->is(['patch', 'post', 'put'])) {

            if($this->request->data['image']['error'] == 0){
                $this->request->data['image'] = $this->Marcas->newUploadImage($this->request->data['image'], $this->request->data['name']);
            } else {
                $this->request->data['image'] = $marca->image;
            }
                
            if($this->request->data['banner_fundo']['error'] == 0){
                if ($tipo_banner == 0) {
                    $this->request->data['banner_fundo'] = $this->Marcas->newUploadImageBannerFundo($this->request->data['banner_fundo'], $this->request->data['name'], $this->request->data['banner_fundo_cropx'], $this->request->data['banner_fundo_cropy'], $this->request->data['banner_fundo_cropw'], $this->request->data['banner_fundo_croph']);
                } elseif ($tipo_banner == 1) {
                    $this->request->data['banner_fundo'] = $this->Marcas->newUploadImageBannerFundoCurto($this->request->data['banner_fundo'], $this->request->data['name'], $this->request->data['banner_fundo_cropx'], $this->request->data['banner_fundo_cropy'], $this->request->data['banner_fundo_cropw'], $this->request->data['banner_fundo_croph']);
                }
            } else {
                $this->request->data['banner_fundo'] = $marca->banner_fundo;
            }
            
            $marca = $this->Marcas->patchEntity($marca, $this->request->data);
            if ($this->Marcas->save($marca)) {
                $this->Flash->success('marca salvo com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar marca. Tente novamente.');
            }
        }
        $status = $this->Marcas->Status->find('list', ['limit' => 200]);
        $this->set(compact('marca', 'status', 'tipo_banner'));
        $this->set('_serialize', ['marca']);
    }

}
