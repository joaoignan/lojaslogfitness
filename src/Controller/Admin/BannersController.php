<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Banners Controller
 *
 * @property \App\Model\Table\BannersTable $Banners
 */
class BannersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        $banners = $this->Banners
            ->find('all')
            ->all();

        if($this->request->is(['post', 'put'])){

            $falha = 0;
            
            foreach($this->request->data['visivel'] as $key => $item){
                $banner = $this->Banners->get($key);

                $banner = $this->Banners->patchEntity($banner, ['visivel' => $item]);

                if(!$this->Banners->save($banner)){
                    $falha = 1;
                }
            }

            if($falha) {
                $this->Flash->info('Falha ao atualizar banners');
            } else {
                $this->Flash->success('Atualização concluída');
            }
            $this->redirect(['action' => 'index']);
        }

        $this->set(compact('banners', 'tipo_banner'));
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add(){
        
        $banner = $this->Banners->newEntity();
        if ($this->request->is('post')) {
            $banner = $this->Banners->patchEntity($banner, $this->request->data);
            if ($this->Banners->save($banner)) {
                $this->Flash->success('The banner has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The banner could not be saved. Please, try again.');
            }
        }


        $this->set(compact('banner'));
        $this->set('_serialize', ['banner']);
    }

    /**
     * View method
     *
     * @param string|null $id Banner id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null){
        
        $banner = $this->Banners->get($id);
        $this->set('banner', $banner);
        $this->set('_serialize', ['banner']);
    }


    /**
     * Edit method
     *
     * @param string|null $id Banner id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        
        $banner = $this->Banners->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $banner = $this->Banners->patchEntity($banner, $this->request->data);
            if ($this->Banners->save($banner)) {


                $this->Flash->success('The banner has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The banner could not be saved. Please, try again.');
            }
        }

        $this->set(compact('banner'));
        $this->set('_serialize', ['banner']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Banner id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $banner = $this->Banners->get($id);
        if ($this->Banners->delete($banner)) {
            $this->Flash->success('The banner has been deleted.');
        } else {
            $this->Flash->error('The banner could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }



}
