<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\I18n\Time;

/**
 * ProfessorComissoes Controller
 *
 * @property \App\Model\Table\ProfessorComissoesTable $ProfessorComissoes
 */
class ProfessorComissoesController extends AppController
{

    /**
     * Index method
     * @return \Cake\Network\Response|null
     */
    public function index($filter = 0, $mes = null, $ano = null){

        if($filter == 0) {
            //todas
            $condition_filter = [];
        } else if($filter == 1) {
            //a pagar
            $condition_filter = ['ProfessorComissoes.paga' => 0];
        } else if($filter == 2) {
            //pagas
            $condition_filter = ['ProfessorComissoes.paga' => 1];
        }

        $data_atual = Time::now();
        $ano_atual = $data_atual->format('Y');
        $mes_atual = $data_atual->format('m');

        $this->set(compact('ano_atual', 'mes_atual'));

        if($mes == null && $ano == null) {
            return $this->redirect(['action' => 'index', $filter, $mes_atual, $ano_atual]);
        }

        if($mes == $mes_atual && $ano == $ano_atual) {
            $Pedidos = TableRegistry::get('Pedidos');

            $pedidos = $Pedidos
                ->find('all')
                ->contain(['Professores'])
                ->where(['Pedidos.data_pagamento >=' => $ano.'-'.$mes.'-01%'])
                ->andWhere(['Pedidos.pedido_status_id >=' => 3])
                ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
                ->order(['Professores.id' => 'asc'])
                ->all();

            $vendas_por_professor = [];

            foreach($pedidos as $pedido) {
                $vendas_por_professor[$pedido->professor_id] = $vendas_por_professor[$pedido->professor_id] + $pedido->valor;
            }

            $pedido_professores = $Pedidos
                ->find('all')
                ->contain(['Professores'])
                ->where(['Pedidos.data_pagamento >=' => $ano.'-'.$mes.'-01%'])
                ->andWhere(['Pedidos.pedido_status_id >=' => 3])
                ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
                ->distinct(['Professores.id'])
                ->all();

            $mes_anterior = Time::now();
            $mes_anterior->subMonth(1);

            foreach ($pedido_professores as $pedido_professor) {

                $comissao_mes_anterior = $this->ProfessorComissoes
                    ->find('all')
                    ->where(['professor_id' => $pedido_professor->professor_id])
                    ->andWhere(['mes' => $mes_anterior->format('m')])
                    ->andWhere(['ano' => $mes_anterior->format('Y')])
                    ->first();

                if($comissao_mes_anterior) {
                    $vendas_mes_anterior = $comissao_mes_anterior->vendas;
                } else {
                    $vendas_mes_anterior = 0.0;
                }

                $taxa_comissao = $pedido_professor->professore->taxa_comissao * 0.01;

                $informacoes[$pedido_professor->professor_id] = [
                    'professor' => $pedido_professor->professore,
                    'vendas_mes_anterior' => $vendas_mes_anterior,
                    'vendas_mes_atual' => $vendas_por_professor[$pedido_professor->professor_id],
                    'comissoes_mes_atual' => $vendas_por_professor[$pedido_professor->professor_id] * $taxa_comissao,
                ];
            }

            $this->set(compact('informacoes', 'filter', 'mes', 'ano'));
            $this->set('_serialize', ['informacoes']);
        } else {
            $condition_mes = ['ProfessorComissoes.mes' => $mes];
            $condition_ano = ['ProfessorComissoes.ano' => $ano];

            $this->paginate = [
                'contain'    => ['Professores'],
                'order'      => ['ProfessorComissoes.paga' => 'asc', 'ProfessorComissoes.id' => 'desc'],
                'conditions' => [$condition_filter, $condition_mes, $condition_ano]
            ];

            $this->paginate['sortWhitelist'] = ['ProfessorComissoes.id', 'Professores.name', 'Professores.banco', 'ProfessorComissoes.meta', 'ProfessorComissoes.vendas', 'ProfessorComissoes.comissao', 'ProfessorComissoes.aceita', 'ProfessorComissoes.paga'];

            $professorComissoes = $this->paginate($this->ProfessorComissoes);

            $vendas_mes_anterior = [];

            foreach($professorComissoes as $professorComissao) {
                $mes_anterior = new Time($professorComissao->ano.'-'.$professorComissao->mes);
                $mes_anterior->subMonth(1);

                $professorComissoesMesAnterior = $this->ProfessorComissoes
                    ->find('all')
                    ->where(['professor_id' => $professorComissao->professor_id])
                    ->andWhere(['mes' => $mes_anterior->format('m')])
                    ->andWhere(['ano' => $mes_anterior->format('Y')])
                    ->first();

                if($professorComissoesMesAnterior) {
                    $vendas_mes_anterior[$professorComissao->professor_id] = $professorComissoesMesAnterior->vendas;
                } else {
                    $vendas_mes_anterior[$professorComissao->professor_id] = 0.00;
                }
            }

            $this->set(compact('ProfessorComissoes', 'filter', 'mes', 'ano', 'vendas_mes_anterior'));
            $this->set('_serialize', ['ProfessorComissoes']);
        }

        if($this->request->is(['post', 'put'])){

            $Professores = TableRegistry::get('Professores');
                
            // Email::configTransport('academia', [
            //     'className' => 'Smtp',
            //     'host' => 'mail.logfitness.com.br',
            //     'port' => 587,
            //     'timeout' => 30,
            //     'username' => EMAIL_USER,
            //     'password' => EMAIL_SENHA,
            //     'client' => null,
            //     'tls' => null,
            // ]);

            foreach($this->request->data['paga'] as $key => $item){
                $professor_comissao = $this->ProfessorComissoes->get($key);
                $data       = [
                    'paga'  => $item,
                ];

                $paga_antigo = $professor_comissao->paga;

                $professor_comissao = $this->ProfessorComissoes->patchEntity($professor_comissao, $data);

                $academias_comissao_paga = $Professores
                    ->find('all')
                    ->where(['Professores.status_id'        => 1])
                    ->andWhere(['Professores.id' => $professor_comissao->academia_id])
                    ->first();

                if($this->ProfessorComissoes->save($professor_comissao)){
                    if($paga_antigo == 0 && $item == 1) {
                        $email = new Email();

                        // $email
                        //     ->transport('academia')
                        //     ->template('pagamento_realizado')
                        //     ->emailFormat('html')
                        //     ->from([EMAIL_USER => EMAIL_NAME])
                        //     ->to($academias_comissao_paga->email)
                        //     ->subject(EMAIL_NAME' - Comissão paga!')
                        //     ->set([
                        //         'name'          => $academias_comissao_paga->contact,
                        //         'academia'      => $academias_comissao_paga->shortname,
                        //         'logo_academia' => $academias_comissao_paga->image
                        //     ])
                        //     ->send();
                    }
                }
            }

            $this->Flash->info('Atualização concluída');
            $this->redirect(['action' => 'index']);                
        }
    }

    public function filtrar_index() {
        if($this->request->is(['post'])){
            $this->redirect(['action' => 'index', $this->request->data['filter'], $this->request->data['mes'], $this->request->data['ano']]);
        }

        $this->viewBuilder()->layout(false);
        $this->render(false);
    }

    /**
     * COMISSÕES X PEDIDOS
     */
    public function comissoes_pedidos($mes = null, $ano = null){

        //$this->assign('title', 'Teste');

        if($this->request->is(['post'])){
            $this->redirect(['action' => 'comissoes_pedidos', $this->request->data['mes'], $this->request->data['ano']]);
        }

        if($mes == null || $ano == null){
            $this->set('select_date', true);
        }else {
            $Configurations = TableRegistry::get('Configurations');
            $configurations = $Configurations->find('all')
                ->where(['status_id' => 1])
                ->order(['id' => 'desc'])
                ->limit(1)
                ->first();

            $config = unserialize($configurations->configuration);

            $Pedidos = TableRegistry::get('Pedidos');

            $pedidos = $Pedidos
                ->find('all')
                ->contain('Professores');

            $pedidos
                ->select([
                    'total' => $pedidos->func()->sum('valor'),
                    'Pedidos.id',
                    'Pedidos.professor_id',
                    'Professores.id',
                    'Professores.name',
                    'Professores.phone',
                    'Professores.email',
                    'Professores.cpf',
                    'Professores.cnpj_bancario',
                    'Professores.razao_bancario',
                    'Professores.favorecido',
                    'Professores.cpf_favorecido',
                    'Professores.banco',
                    'Professores.tipo_conta',
                    'Professores.agencia',
                    'Professores.agencia_dv',
                    'Professores.conta',
                    'Professores.conta_dv',
                    'Professores.banco_obs',
                    'Professores.card_status',
                    'Professores.card_birth',
                    'Professores.card_mother',
                    'Professores.card_gender',
                    'Professores.card_id',
                    'Professores.card_number',
                    'ProfessorComissoes.comissao',
                    'ProfessorComissoes.aceita',
                    'ProfessorComissoes.paga',
                    'ProfessorComissoes.meta',
                ])
                ->leftJoin(
                    ['ProfessorComissoes' => 'professor_comissoes'],
                    [
                        'ProfessorComissoes.professor_id = Pedidos.professor_id',
                        'ProfessorComissoes.ano >= ' . $ano,
                        'ProfessorComissoes.mes >= ' . $mes
                    ])
                ->where(['Pedidos.created LIKE' => $ano . '-' . $mes . '%'])
                ->andWhere(['Pedidos.pedido_status_id >=' => 3])
                ->andWhere(['Pedidos.comissao_status' => 1])
                ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
                ->andWhere(['Pedidos.professor_id >=' => 1])
                ->order(['Professores.name' => 'asc'])
                ->group('Pedidos.professor_id');

            $pedidos_comissoes = $pedidos->all();

            //debug($pedidos_comissoes);

            $this->set(compact('pedidos_comissoes', 'config'));
        }

        $this->set(compact('mes', 'ano'));

    }

    /**
     * View method
     *
     * @param string|null $id Professor Comisso id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $professorComisso = $this->ProfessorComissoes->get($id, [
            'contain' => ['Professores', 'Pedidos']
        ]);

        $this->set('professorComisso', $professorComisso);
        $this->set('_serialize', ['professorComisso']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $professorComisso = $this->ProfessorComissoes->newEntity();
        if ($this->request->is('post')) {
            $professorComisso = $this->ProfessorComissoes->patchEntity($professorComisso, $this->request->data);
            if ($this->ProfessorComissoes->save($professorComisso)) {
                $this->Flash->success(__('The professor comisso has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The professor comisso could not be saved. Please, try again.'));
            }
        }
        $professores = $this->ProfessorComissoes->Professores->find('list', ['limit' => 200]);
        $pedidos = $this->ProfessorComissoes->Pedidos->find('list', ['limit' => 200]);
        $this->set(compact('professorComisso', 'professores', 'pedidos'));
        $this->set('_serialize', ['professorComisso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Professor Comisso id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $professorComisso = $this->ProfessorComissoes->get($id, [
            'contain' => ['Pedidos']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $professorComisso = $this->ProfessorComissoes->patchEntity($professorComisso, $this->request->data);
            if ($this->ProfessorComissoes->save($professorComisso)) {
                $this->Flash->success(__('The professor comisso has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The professor comisso could not be saved. Please, try again.'));
            }
        }
        $professores = $this->ProfessorComissoes->Professores->find('list', ['limit' => 200]);
        $pedidos = $this->ProfessorComissoes->Pedidos->find('list', ['limit' => 200]);
        $this->set(compact('professorComisso', 'professores', 'pedidos'));
        $this->set('_serialize', ['professorComisso']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Professor Comisso id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $professorComisso = $this->ProfessorComissoes->get($id);
        if ($this->ProfessorComissoes->delete($professorComisso)) {
            $this->Flash->success(__('The professor comisso has been deleted.'));
        } else {
            $this->Flash->error(__('The professor comisso could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
