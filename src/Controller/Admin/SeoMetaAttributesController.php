<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * SeoMetaAttributes Controller
 *
 * @property \App\Model\Table\SeoMetaAttributesTable $SeoMetaAttributes
 */
class SeoMetaAttributesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('seoMetaAttributes', $this->paginate($this->SeoMetaAttributes));
        $this->set('_serialize', ['seoMetaAttributes']);
    }

    /**
     * View method
     *
     * @param string|null $id Seo Meta Attribute id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $seoMetaAttribute = $this->SeoMetaAttributes->get($id, [
            'contain' => ['SeoMetaTags']
        ]);
        $this->set('seoMetaAttribute', $seoMetaAttribute);
        $this->set('_serialize', ['seoMetaAttribute']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $seoMetaAttribute = $this->SeoMetaAttributes->newEntity();
        if ($this->request->is('post')) {
            $seoMetaAttribute = $this->SeoMetaAttributes->patchEntity($seoMetaAttribute, $this->request->data);
            if ($this->SeoMetaAttributes->save($seoMetaAttribute)) {
                $this->Flash->success(__('The seo meta attribute has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The seo meta attribute could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('seoMetaAttribute'));
        $this->set('_serialize', ['seoMetaAttribute']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Seo Meta Attribute id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $seoMetaAttribute = $this->SeoMetaAttributes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $seoMetaAttribute = $this->SeoMetaAttributes->patchEntity($seoMetaAttribute, $this->request->data);
            if ($this->SeoMetaAttributes->save($seoMetaAttribute)) {
                $this->Flash->success(__('The seo meta attribute has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The seo meta attribute could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('seoMetaAttribute'));
        $this->set('_serialize', ['seoMetaAttribute']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Seo Meta Attribute id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $seoMetaAttribute = $this->SeoMetaAttributes->get($id);
        if ($this->SeoMetaAttributes->delete($seoMetaAttribute)) {
            $this->Flash->success(__('The seo meta attribute has been deleted.'));
        } else {
            $this->Flash->error(__('The seo meta attribute could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
