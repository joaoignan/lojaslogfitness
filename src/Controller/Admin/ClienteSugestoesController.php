<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * PreOrders Controller
 *
 * @property \App\Model\Table\PreOrdersTable $PreOrders
 */
class ClienteSugestoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
        	'contain'       => ['Cities', 'Academias', 'Professores'],
            'order' => ['ClienteSugestoes.id' => 'desc']
        ];
        $cliente_sugestos = $this->paginate($this->ClienteSugestoes);

        $qtd_indicacoes = $this->ClienteSugestoes
        	->find('all')
        	->all();

        $qtd_ind = count($qtd_indicacoes);

        $qtd_indicacoes_academias = $this->ClienteSugestoes
        	->find('all')
        	->where(['ClienteSugestoes.academia_id IS NOT' => null])
        	->all();

        $qtd_ind_acad = count($qtd_indicacoes_academias);

        $qtd_indicacoes_professores = $this->ClienteSugestoes
        	->find('all')
        	->where(['ClienteSugestoes.professor_id IS NOT' => null])
        	->all();

        $qtd_ind_prof = count($qtd_indicacoes_professores);

        $qtd_indicacoes_aceitas = $this->ClienteSugestoes
            ->find('all')
            ->where(['ClienteSugestoes.aceitou' => 1])
            ->all();

        $qtd_ind_aceitas = count($qtd_indicacoes_aceitas);

        $this->set(compact('cliente_sugestos', 'qtd_ind', 'qtd_ind_acad', 'qtd_ind_prof', 'qtd_ind_aceitas'));
        $this->set('_serialize', ['cliente_sugestos']);
    }
}
