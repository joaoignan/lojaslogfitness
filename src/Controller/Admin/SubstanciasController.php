<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\I18n\Time;

class SubstanciasController extends AppController
{
    public function sincronizar_substancias() {
        $todas_substancias = $this->Substancias
            ->find('all')
            ->all();

        foreach ($todas_substancias as $substancia) {
            $substancias_ids_array[] = $substancia->id;
        } 

        $ch = curl_init();

        $header = [
            "Content-Type: application/json"
        ];

        $data = [
            "substancias_ids_array" => $substancias_ids_array
        ];

        $data = json_encode($data);

        $options = array(CURLOPT_URL => "https://api.logfitness.com.br/substancias/sync?api_key=".LOGFITNESS_API_KEY,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $api_novas_substancias = json_decode($response);

        if($api_novas_substancias->status_id == 1 && $api_novas_substancias->retorno_cod == 'sucesso') {
            $falha = 0;

            foreach ($api_novas_substancias->substancias as $substancia) {

                $new_substancia_data = [
                    'id' => $substancia->id,
                    'name' => $substancia->name,
                    'unidade_medida' => $substancia->unidade_medida,
                    'tipo' => $substancia->tipo
                ];

                $new_substancia = $this->Substancias->newEntity($new_substancia_data);
                
                if(!$this->Substancias->save($new_substancia)) {
                    $falha = 1;
                }
            }

            if($falha == 0) {
                $this->Flash->success('Substancias sincronizadas com sucesso!');
            } else {
                $this->Flash->error('Falha ao atualizar banco de dados, atualize a página!');
            }
        }
    }

	public function index(){

        $this->sincronizar_substancias();

        $this->paginate = [
        ];
        $this->set('substancias', $this->paginate($this->Substancias));
        $this->set('_serialize', ['substancias']);
    }

    public function add(){
        $this->sincronizar_substancias();

        if ($this->request->is('post')) {

            do {
                //Add substancia no BD da API LogFitness
                $ch = curl_init();

                $header = [
                    "Content-Type: application/json"
                ];

                $data = [
                    "name" => $this->request->data['name'],
                    "unidade_medida" => $this->request->data['unidade_medida'],
                    "tipo" => $this->request->data['tipo'],
                    "created" => Time::now()
                ];

                $data = json_encode($data);

                $options = array(CURLOPT_URL => "https://api.logfitness.com.br/substancias/add?api_key=".LOGFITNESS_API_KEY,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $header,
                    CURLOPT_SSL_VERIFYPEER => false
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $api_nova_substancia = json_decode($response);

                if($api_nova_substancia->status_id == 2) {
                    $this->Flash->error($api_nova_substancia->mensagem);
                    return $this->redirect(['action' => 'add']);
                }

                if($api_nova_substancia->status_id == 1) {
                    $this->Flash->success($api_nova_substancia->mensagem);
                    return $this->redirect(['action' => 'index']);
                }
            } while($api_nova_substancia->status_id < 1);
        }
    }

    public function edit($id = null){
        $substancia = $this->Substancias->get($id, [
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $substancia = $this->Substancias->patchEntity($substancia, $this->request->data);
            if ($this->Substancias->save($substancia)) {
                $this->Flash->success('Substância salva com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar substância. Tente novamente.');
            }
        }
        $this->set(compact('substancia'));
        $this->set('_serialize', ['substancia']);
    }
}