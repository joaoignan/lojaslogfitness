<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Esportes Controller
 *
 * @property \App\Model\Table\EsportesTable $Esportes
 */
class EsportesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index(){
        $this->paginate = [
            'order' => ['name' => 'asc']
        ];
        $this->set('esportes', $this->paginate($this->Esportes));
        $this->set('_serialize', ['esportes']);
    }

    /**
     * View method
     *
     * @param string|null $id Esporte id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $esporte = $this->Esportes->get($id, [
            'contain' => []
        ]);
        $this->set('esporte', $esporte);
        $this->set('_serialize', ['esporte']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $esporte = $this->Esportes->newEntity();
        if ($this->request->is('post')) {
            $esporte = $this->Esportes->patchEntity($esporte, $this->request->data);
            if ($this->Esportes->save($esporte)) {
                $this->Flash->success('esporte salvo com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar esporte. Tente novamente.');
            }
        }
        $this->set(compact('esporte'));
        $this->set('_serialize', ['esporte']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Esporte id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $esporte = $this->Esportes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $esporte = $this->Esportes->patchEntity($esporte, $this->request->data);
            if ($this->Esportes->save($esporte)) {
                $this->Flash->success('esporte salvo com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar esporte. Tente novamente.');
            }
        }
        $this->set(compact('esporte'));
        $this->set('_serialize', ['esporte']);
    }

}
