<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Configurations Controller
 *
 * @property \App\Model\Table\ConfigurationsTable $Configurations
 */
class ConfigurationsController extends AppController {

    /**
     * CONFIGURAÇÕES GERAIS
     * @return \Cake\Network\Response|null
     */
    public function index(){

        $config = $this->Configurations
            ->find('all')
            ->order(['id' => 'desc'])
            ->limit(1)
            ->first();

        $VariaveisGlobais = TableRegistry::get('VariaveisGlobais');

        $modelo_banner = $VariaveisGlobais
            ->find('all')
            ->Where(['name' => 'tipo_banner'])
            ->first();

        $tipo_banner = $modelo_banner->valor;

        $variavel_desconto = $VariaveisGlobais
            ->find('all')
            ->where(['name' => 'desconto'])
            ->first();

        $variavel_promocao = $VariaveisGlobais
            ->find('all')
            ->where(['name' => 'parcelas_promo'])
            ->first();

        $parcelas_promo = $variavel_promocao->valor;

        $tipo_gateway = $VariaveisGlobais
            ->find('all')
            ->where(['name' => 'gateway_pagamento'])
            ->first();

        $variavel_entrega = $VariaveisGlobais
            ->find('all')
            ->where(['name' => 'entregas'])
            ->first();

        $entregas = $variavel_entrega->valor;


        $configuration = $this->Configurations->newEntity();
        

        $this->set(compact('configuration', 'status', 'config', 'variavel_desconto', 'parcelas_promo', 'tipo_gateway', 'tipo_banner', 'entregas'));
        $this->set('_serialize', ['configuration']);
    }

    /**
     * CONFIGURAÇÃO DAS FAIXAS DE PARCELAMENTO
     * @return \Cake\Network\Response|null
     */
    public function parcelamento(){

        $config = $this->Configurations
            ->find('all')
            ->order(['id' => 'desc'])
            ->limit(1)
            ->first();

        $configs = unserialize($config->configuration);

        $configuration = $this->Configurations->newEntity();

        if ($this->request->is('post')) {

            $data_save = array_merge($configs, $this->request->data);

            $this->request->data['configuration']   = serialize($data_save);
            $this->request->data['status_id']       = 1;

            $configuration = $this->Configurations->patchEntity($configuration, $this->request->data);
            if ($this->Configurations->save($configuration)) {
                $this->Flash->success(__('Configuração de parcelamento atualizada com sucesso'));
                return $this->redirect(['action' => 'parcelamento']);
            } else {
                $this->Flash->error(__('Falha ao atualizar configuração de parcelamento. Tente novamente...'));
            }
        }

        $this->set(compact('configuration', 'status', 'config'));
        $this->set('_serialize', ['configuration']);
    }

    /**
     * SALVAR VARIÁVEIS GLOBAIS
     * @return \Cake\Network\Response|null
     */
    public function salvar_variaveis_globais(){

        if ($this->request->is('post')) {

            $VariaveisGlobais = TableRegistry::get('VariaveisGlobais');

            $variavel_desconto = $VariaveisGlobais
                ->find('all')
                ->where(['name' => 'desconto'])
                ->first();

            $modelo_banner = $VariaveisGlobais
                ->find('all')
                ->Where(['name' => 'tipo_banner'])
                ->first();
            
            $parcelas_promo = $VariaveisGlobais
                ->find('all')
                ->where(['name' => 'parcelas_promo'])
                ->first();

            $variavel_entrega = $VariaveisGlobais
                ->find('all')
                ->where(['name' => 'entregas'])
                ->first();

            $parcelas_promo = $VariaveisGlobais->patchEntity($parcelas_promo, ['valor' => $this->request->data['parcelas_promo']]);
            $modelo_banner = $VariaveisGlobais->patchEntity($modelo_banner, ['valor' => $this->request->data['tipo_banner']]);
            $variavel_desconto = $VariaveisGlobais->patchEntity($variavel_desconto, ['valor' => $this->request->data['variavel_desconto']]);
            $variavel_entrega = $VariaveisGlobais->patchEntity($variavel_entrega, ['valor' => $this->request->data['entregas']]);

            if ($VariaveisGlobais->save($variavel_desconto) && $VariaveisGlobais->save($modelo_banner) && $VariaveisGlobais->save($parcelas_promo) && $VariaveisGlobais->save($variavel_entrega) ){
                $this->Flash->success(__('Informações alteradas com sucesso!'));
                return $this->redirect('/admin/configurations'); }
            else {
                $this->Flash->error(__('Falha ao atualizar variáveis globais. Tente novamente...'));
            }
        }

        return $this->redirect('/admin/configurations');
    }
}
