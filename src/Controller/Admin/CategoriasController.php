<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Categorias Controller
 *
 * @property \App\Model\Table\CategoriasTable $Categorias
 */
class CategoriasController extends AppController
{

    public function sincronizar_objetivos() {
        $todos_objetivos = TableRegistry::get('Objetivos')
            ->find('all')
            ->all();

        foreach ($todos_objetivos as $objetivo) {
            $objetivos_ids_array[] = $objetivo->id;
        } 

        do {
            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $data = [
                "objetivos_ids_array" => $objetivos_ids_array
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.logfitness.com.br/objetivos/sync?api_key=".LOGFITNESS_API_KEY,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_SSL_VERIFYPEER => false
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $api_novos_objetivos = json_decode($response);

            if($api_novos_objetivos->status_id == 1 && $api_novos_objetivos->retorno_cod == 'sucesso') {
                $falha = 0;

                foreach ($api_novos_objetivos->objetivos as $objetivo) {

                    $new_objetivo_data = [
                        'id' => $objetivo->id,
                        'name' => $objetivo->name,
                        'slug' => $objetivo->slug,
                        'texto_tarja' => $objetivo->texto_tarja,
                        'image' => $objetivo->image,
                        'color' => $objetivo->color,
                        'banner_lateral' => $objetivo->banner_lateral,
                        'banner_fundo' => $objetivo->banner_fundo,
                        'status_id' => $objetivo->status_id
                    ];

                    $new_objetivo = TableRegistry::get('Objetivos')->newEntity($new_objetivo_data);
                    
                    if(!TableRegistry::get('Objetivos')->save($new_objetivo)) {
                        $falha = 1;
                    }
                }

                if($falha == 0) {
                    $this->Flash->success('Objetivos sincronizados com sucesso!');
                } else {
                    $this->Flash->error('Falha ao atualizar banco de dados, atualize a página!');
                }
            }
        } while($api_novos_objetivos->status_id < 1);
    }

    public function sincronizar_categorias() {
        $this->sincronizar_objetivos();

        $todas_categorias = $this->Categorias
            ->find('all')
            ->all();

        foreach ($todas_categorias as $categoria) {
            $categorias_ids_array[] = $categoria->id;
        } 

        do {
            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $data = [
                "categorias_ids_array" => $categorias_ids_array
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.logfitness.com.br/categorias/sync?api_key=".LOGFITNESS_API_KEY,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_SSL_VERIFYPEER => false
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $api_novas_categorias = json_decode($response);

            if($api_novas_categorias->status_id == 1 && $api_novas_categorias->retorno_cod == 'sucesso') {
                $falha = 0;

                foreach ($api_novas_categorias->categorias as $categoria) {

                    $new_categoria_data = [
                        'id' => $categoria->id,
                        'objetivo_id' => $categoria->objetivo_id,
                        'cod_interno_classe' => $categoria->cod_interno_classe,
                        'cod_interno_cat' => $categoria->cod_interno_cat,
                        'name' => $categoria->name,
                        'descricao' => $categoria->descricao,
                        'slug' => $categoria->slug,
                        'banner_lateral' => $categoria->banner_lateral,
                        'banner_fundo' => $categoria->banner_fundo,
                        'status_id' => $categoria->status_id
                    ];

                    $new_categoria = $this->Categorias->newEntity($new_categoria_data);
                    
                    if(!$this->Categorias->save($new_categoria)) {
                        $falha = 1;
                    }
                }

                if($falha == 0) {
                    $this->Flash->success('Categorias sincronizadas com sucesso!');
                } else {
                    $this->Flash->error('Falha ao atualizar banco de dados, atualize a página!');
                }
            }
        } while($api_novas_categorias->status_id < 1);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->sincronizar_categorias();

        $this->paginate = [
            'contain' => ['Status', 'Objetivos']
        ];
        $categorias = $this->paginate($this->Categorias);

        $this->set(compact('categorias'));
        $this->set('_serialize', ['categorias']);
    }

    /**
     * View method
     *
     * @param string|null $id Categoria id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $categoria = $this->Categorias->get($id, [
            'contain' => ['Status', 'Objetivos', 'ProdutoCategorias']
        ]);

        $this->set('categoria', $categoria);
        $this->set('_serialize', ['categoria']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->sincronizar_categorias();

        $VariaveisGlobais = TableRegistry::get('VariaveisGlobais');

        $modelo_banner = $VariaveisGlobais
            ->find('all')
            ->Where(['name' => 'tipo_banner'])
            ->first();

        $tipo_banner = $modelo_banner->valor;

        if ($this->request->is('post')) {

            $banner_lateral = $this->Categorias->newUploadImageBannerLateral($this->request->data['banner_lateral'], $this->request->data['name'], $this->request->data['banner_lateral_cropx'], $this->request->data['banner_lateral_cropy'], $this->request->data['banner_lateral_cropw'], $this->request->data['banner_lateral_croph']);


            if ($tipo_banner == 0) {
                $banner_fundo = $this->Categorias->newUploadImageBannerFundo($this->request->data['banner_fundo'], $this->request->data['name'], $this->request->data['banner_fundo_cropx'], $this->request->data['banner_fundo_cropy'], $this->request->data['banner_fundo_cropw'], $this->request->data['banner_fundo_croph']);
            } elseif ($tipo_banner == 1) {
                $banner_fundo = $this->Categorias->newUploadImageBannerFundoCurto($this->request->data['banner_fundo'], $this->request->data['name'], $this->request->data['banner_fundo_cropx'], $this->request->data['banner_fundo_cropy'], $this->request->data['banner_fundo_cropw'], $this->request->data['banner_fundo_croph']);
            }


            $count = 0;

            do {
                //Add marca no BD da API LogFitness
                $ch = curl_init();

                $header = [
                    "Content-Type: application/json"
                ];

                $data = [
                    "name" => $this->request->data['name'],
                    "objetivo_id" => $this->request->data['objetivo_id'],
                    "banner_lateral" => $banner_lateral,
                    "banner_fundo" => $banner_fundo
                ];

                $data = json_encode($data);

                $options = array(CURLOPT_URL => "https://api.logfitness.com.br/categorias/add?api_key=".LOGFITNESS_API_KEY,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $header,
                    CURLOPT_SSL_VERIFYPEER => false
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $api_nova_categoria = json_decode($response);

                if($api_nova_categoria->status_id == 2) {
                    $this->Flash->error($api_nova_categoria->mensagem);
                    return $this->redirect(['action' => 'add']);
                }

                if($api_nova_categoria->status_id == 1) {
                    $this->Flash->success($api_nova_categoria->mensagem);
                    return $this->redirect(['action' => 'index']);
                }

                $count++;
            } while($api_nova_categoria->status_id < 1 && $count < 100);
        }


        $objetivos = TableRegistry::get('Objetivos')->find('list', ['limit' => 200]);
        $this->set(compact('objetivos', 'tipo_banner'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Categoria id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $Objetivos = Tableregistry::get('Objetivos');
        
        $categoria = $this->Categorias->get($id, [
            'contain' => []
        ]);

        $VariaveisGlobais = TableRegistry::get('VariaveisGlobais');

        $modelo_banner = $VariaveisGlobais
            ->find('all')
            ->Where(['name' => 'tipo_banner'])
            ->first();

        $tipo_banner = $modelo_banner->valor;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $categoria = $this->Categorias->patchEntity($categoria, $this->request->data);
            if ($this->Categorias->save($categoria)) {
                $this->Flash->success(__('The categoria has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The categoria could not be saved. Please, try again.'));
            }
        }
        $objetivos = $Objetivos->find('list', ['limit' => 200]);
        $this->set(compact('categoria', 'status', 'objetivos', 'tipo_banner'));
        $this->set('_serialize', ['categoria']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Categoria id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $categoria = $this->Categorias->get($id);
        if ($this->Categorias->delete($categoria)) {
            $this->Flash->success(__('The categoria has been deleted.'));
        } else {
            $this->Flash->error(__('The categoria could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
