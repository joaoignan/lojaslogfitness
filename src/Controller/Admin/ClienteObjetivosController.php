<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ClienteObjetivos Controller
 *
 * @property \App\Model\Table\ClienteObjetivosTable $ClienteObjetivos
 */
class ClienteObjetivosController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Clientes', 'Objetivos']
        ];
        $this->set('clienteObjetivos', $this->paginate($this->ClienteObjetivos));
        $this->set('_serialize', ['clienteObjetivos']);
    }

    /**
     * View method
     *
     * @param string|null $id Cliente Objetivo id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $clienteObjetivo = $this->ClienteObjetivos->get($id, [
            'contain' => ['Clientes', 'Objetivos']
        ]);
        $this->set('clienteObjetivo', $clienteObjetivo);
        $this->set('_serialize', ['clienteObjetivo']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $clienteObjetivo = $this->ClienteObjetivos->newEntity();
        if ($this->request->is('post')) {
            $clienteObjetivo = $this->ClienteObjetivos->patchEntity($clienteObjetivo, $this->request->data);
            if ($this->ClienteObjetivos->save($clienteObjetivo)) {
                $this->Flash->success('cliente objetivo salvo com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar cliente objetivo. Tente novamente.');
            }
        }
        $clientes = $this->ClienteObjetivos->Clientes->find('list', ['limit' => 200]);
        $objetivos = $this->ClienteObjetivos->Objetivos->find('list', ['limit' => 200]);
        $this->set(compact('clienteObjetivo', 'clientes', 'objetivos'));
        $this->set('_serialize', ['clienteObjetivo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente Objetivo id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $clienteObjetivo = $this->ClienteObjetivos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $clienteObjetivo = $this->ClienteObjetivos->patchEntity($clienteObjetivo, $this->request->data);
            if ($this->ClienteObjetivos->save($clienteObjetivo)) {
                $this->Flash->success('cliente objetivo salvo com sucesso.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Falha ao salvar cliente objetivo. Tente novamente.');
            }
        }
        $clientes = $this->ClienteObjetivos->Clientes->find('list', ['limit' => 200]);
        $objetivos = $this->ClienteObjetivos->Objetivos->find('list', ['limit' => 200]);
        $this->set(compact('clienteObjetivo', 'clientes', 'objetivos'));
        $this->set('_serialize', ['clienteObjetivo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente Objetivo id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $clienteObjetivo = $this->ClienteObjetivos->get($id);
        if ($this->ClienteObjetivos->delete($clienteObjetivo)) {
            $this->Flash->success('cliente objetivo apagado com sucesso.');
        } else {
            $this->Flash->error('Falha ao apagar cliente objetivo. Tente novamente.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
