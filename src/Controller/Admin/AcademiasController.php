<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Academias Controller
 *
 * @property \App\Model\Table\AcademiasTable $Academias
 */
class AcademiasController extends AppController {

    /**
     * INDEX - ACADEMIAS
     * @param null $status
     */
    public function index($status = 'todas', $search = null){

        $Users = TableRegistry::get('Users');

        $user = $Users->get($this->Auth->user('id'));

        if($this->request->is(['post', 'put'])){
            $this->redirect(['action' => 'index', $status, $this->request->data['search']]);
        }

        if($search != null){
            $search = [
                'OR' => [
                    'Academias.name         LIKE' => '%'.$search.'%',
                    'Academias.shortname    LIKE' => '%'.$search.'%',
                    'Academias.email        LIKE' => '%'.$search.'%',
                    'Academias.cnpj         LIKE' => '%'.$search.'%',
                    'Academias.contact      LIKE' => '%'.$search.'%',
                    'Academias.phone        LIKE' => '%'.$search.'%',
                    'Academias.mobile       LIKE' => '%'.$search.'%',
                    'Cities.name            LIKE' => '%'.$search.'%',
                    'Cities.uf              LIKE' => '%'.$search.'%',
                ]
            ];
        }else{
            $search = [];
        }

        if($user->group_id != 7) {
            if($status == 'todas'){
                $this->paginate = [
                    'contain'       => ['Cities', 'Status'],
                    'order'         => ['Academias.id' => 'desc'],
                    'conditions'    => [$search]
                ];
            }elseif($status == 'ativas'){
                $this->paginate = [
                    'contain'       => ['Cities', 'Status'],
                    'order'         => ['Academias.id' => 'desc'],
                    'conditions'    => ['Academias.status_id' => 1, $search]
                ];
            }elseif($status == 'inativas'){
                $this->paginate = [
                    'contain'       => ['Cities', 'Status'],
                    'order'         => ['Academias.id' => 'desc'],
                    'conditions'    => ['Academias.status_id' => 2, $search]
                ];
            }elseif($status == 'nao_aceitaram'){
                $this->paginate = [
                    'contain'       => ['Cities', 'Status'],
                    'order'         => ['Academias.id' => 'desc'],
                    'conditions'    => ['Academias.status_id' => 3, $search]
                ];
            }
        } else {
            if($status == 'todas'){
                $this->paginate = [
                    'contain'       => ['Cities', 'Status'],
                    'order'         => ['Academias.id' => 'desc'],
                    'conditions'    => ['Academias.user_id' => $user->id, $search]
                ];
            }
        }

        $this->set('academias', $this->paginate($this->Academias));
        $this->set('_serialize', ['academias']);
    }

    public function salvar_status_index() {
        if($this->request->is(['post', 'put'])){

            $falha = 0;
            
            foreach($this->request->data['status_id'] as $key => $item){
                $academia = $this->Academias->get($key);

                if($item == 0) {
                    $item = 2;
                }

                $academia = $this->Academias->patchEntity($academia, ['status_id' => $item]);

                if(!$this->Academias->save($academia)){
                    $falha = 1;
                }
            }

            if($falha) {
                $this->Flash->info('Falha ao atualizar academias');
            } else {
                $this->Flash->info('Atualização concluída');
            }
            $this->redirect(['action' => 'index']);
        }
        
        $this->redirect(['action' => 'index']);

        $this->viewBuilder()->layout(false);
        $this->render(false);
    }

    /**
     * View method
     *
     * @param string|null $id Academia id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null){

        $academia = $this->Academias->get($id, [
            'contain' => ['Cities', 'Status', 'Clientes']
        ]);

        $Pedidos = TableRegistry::get('Pedidos');
        $ProfessorAcademias   = TableRegistry::get('ProfessorAcademias');

        $qtd_time = $ProfessorAcademias
            ->find('all')
            ->where(['academia_id' => $academia->id])
            ->andWhere(['status_id' => 1])
            ->count();

        $Cities = TableRegistry::get('Cities');
        
        $cidade_query = $Cities
            ->find('all')
            ->where(['id' => $academia->city_id_acad])
            ->first();

        $cidade_acad = $cidade_query->name;
        $estado_acad = $cidade_query->uf;

        $min_date = Time::now();
        $min_date->subYear(1);

        $pedidos_grafico = $Pedidos
            ->find('all')
            ->where(['Pedidos.academia_id' => $academia->id])
            ->andWhere(['Pedidos.created >' => $min_date])
            ->andWhere(['Pedidos.pedido_status_id >=' => 3])
            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
            ->andWhere(['Pedidos.comissao_status' => 1])
            ->all();

        $data_grafico_vendas[0] = 0.0;
        $data_grafico_vendas[1] = 0.0;
        $data_grafico_vendas[2] = 0.0;
        $data_grafico_vendas[3] = 0.0;
        $data_grafico_vendas[4] = 0.0;
        $data_grafico_vendas[5] = 0.0;
        $data_grafico_vendas[6] = 0.0;
        $data_grafico_vendas[7] = 0.0;
        $data_grafico_vendas[8] = 0.0;
        $data_grafico_vendas[9] = 0.0;
        $data_grafico_vendas[10] = 0.0;
        $data_grafico_vendas[11] = 0.0;

        $ctrl = 0;
        for($i = 12; $i >= 1; $i--) {
            $data_grafico = Time::now();
            $data_grafico->subMonth($i);

            $data_grafico_mes[$ctrl] = $data_grafico->format('m-y');

            $ctrl++;
        }

        foreach ($pedidos_grafico as $key => $pedido_grafico) {
            for ($i=0; $i < 12; $i++) { 
                if($pedido_grafico->created->format('m-y') == $data_grafico_mes[$i]) {
                    $data_grafico_vendas[$i] += $pedido_grafico->valor;
                }
            }
        }

        $this->set(compact('academia', 'qtd_time', 'cidade_acad', 'estado_acad', 'data_grafico_vendas'));
        $this->set('_serialize', ['academia']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add(){
        $academia = $this->Academias->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {

            function montarSlugAcademia($nome_fantasia) {

                $slug = preg_replace(array("/(á|à|ã|â|ä)/","/(ç)/","/(é|è|ê|ë)/","/(í|ì|î|ï)/","/(ó|ò|õ|ô|ö)/","/(ú|ù|û|ü)/","/(ñ)/"),explode(" ","a c e i o u n"),$nome_fantasia);

                $slug = str_replace('%','',$slug);
                $slug = str_replace(' ', '', $slug);

                $slug = strtolower($slug);

                return $slug;
            }

            $this->request->data['slug'] = montarSlugAcademia($this->request->data['shortname']);

            $academia_cnpj_cpf = 0;

            if($this->request->data['cnpj']) {
                $academias = $this->Academias
                    ->find('all')
                    ->where(['cnpj' => $this->request->data['cnpj']])
                    ->count();

                if($academias >= 1) {
                    $academia_cnpj_cpf = 1;
                }
            }

            if($this->request->data['cnpj'] == ''){
                $this->request->data['cnpj'] = null;
            }

            if($this->request->data['cpf']) {
                $academias_cpf = $this->Academias
                    ->find('all')
                    ->where(['cpf' => $this->request->data['cpf']])
                    ->count();

                if($academias_cpf >= 1) {
                    $academia_cnpj_cpf = 1;
                }
            }

            $academias_email = $this->Academias
                ->find('all')
                ->where(['email' => $this->request->data['email']])
                ->count();

            $academias_slug = $this->Academias
                ->find('all')
                ->where(['slug' => $this->request->data['slug']])
                ->count();

            if($academia_cnpj_cpf == 1 || $academias_email >=1 || $academias_slug >= 1){
                $this->Flash->error('Já existe uma academia cadastrada com o CPF/CNPJ e/ou Email informado');
                return $this->redirect('/admin/academias/add');
            }else {

                $hasher = new DefaultPasswordHasher();


                $this->request->data['status_id'] = 1;

                $this->request->data['mobile'] ? : $this->request->data['mobile'] = $this->request->data['phone'];

                $academia = $this->Academias->patchEntity($academia, $this->request->data);

                

                if ($this->Academias->save($academia)) {




                    $this->Flash->success('Academia cadastrada com sucesso!');
                    return $this->redirect('/admin/academias/index/todas');
                } else {
                    //$this->Flash->error('Falha ao cadastrar academia, tente novamente');
                    $this->Flash->error(json_encode($academia->errors()));
                    return $this->redirect('/admin/academias/add');
                }
            }
        }

        $states = $this->Academias->Cities->States->find('list');
        $this->set(compact('academia', 'states'));
        $this->set('_serialize', ['academia']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Academia id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null){

        $academia = $this->Academias->get($id, [
            'contain' => ['Cities']
        ]);

        if($this->request->data['cnpj'] == ''){
            $this->request->data['cnpj'] = null;
        }

        if($this->request->data['cpf'] == ''){
            $this->request->data['cpf'] = null;
        }


        if ($this->request->is(['patch', 'post', 'put'])) {

            $academia_slug = $this->Academias
                ->find('all')
                ->where(['slug' => $this->request->data['slug']])
                ->andWhere(['id NOT IN' => $id])
                ->count();

            //Se já existir academia com esse slug
            if($academia_slug >= 1) {
                $this->Flash->error('Já existe uma academia cadastrada com a url escolhida');
                return $this->redirect(['action' => 'edit', $id]);
            }

            $academia = $this->Academias->patchEntity($academia, $this->request->data);
            if ($this->Academias->save($academia)) {

                $this->Flash->success('Academia salva com sucesso.');
                $this->redirect(['action' => 'edit', $id]);
            } else {
                $this->Flash->error('Falha ao salvar academia. Tente novamente.');
            }
        }


        $cities = $this->Academias->Cities->find('list')->where(['Cities.state_id' => $academia->city->state_id]);
        $states = $this->Academias->Cities->States->find('list');
        $status = $this->Academias->Status->find('list');
        $users  = $this->Academias->Users->find('list')->where(['Users.group_id' => 7])->order(['Users.name' => 'asc']);
        $this->set(compact('academia', 'cities', 'states', 'users', 'status'));
        $this->set('_serialize', ['academia']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Academia id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function add_galeria($id = null){

        $academia = $this->Academias->get($id, [
            'contain' => ['Cities']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $AcademiaGaleria = TableRegistry::get('AcademiaGaleria');

            $academia_galeria = $AcademiaGaleria->newEntity();

            $this->request->data['academia_id'] = $id;
            $this->request->data['status'] = 1;
            
            $academia_galeria = $AcademiaGaleria->patchEntity($academia_galeria, $this->request->data);
            
            if ($AcademiaGaleria->save($academia_galeria)) {
                $this->Flash->success('Foto salva na galeria com sucesso.');
                $this->redirect(['action' => 'edit', $id]);
            } else {
                $this->Flash->error('Falha ao salvar foto na galeria. Tente novamente.');
            }
        }

        $this->set(compact('academia'));

    }

    /**
     * Edit method
     *
     * @param string|null $id Academia id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function exclude_galeria($id = null){
            
        $AcademiaGaleria = TableRegistry::get('AcademiaGaleria');

        $academia_galeria = $AcademiaGaleria
            ->find('all')
            ->where(['id' => $id])
            ->first();

        $data = [
            'status' => 2
        ];
        
        $academia_galeria = $AcademiaGaleria->patchEntity($academia_galeria, $data);
        
        if ($AcademiaGaleria->save($academia_galeria)) {
            $this->Flash->success('Foto excluída com sucesso.');
            $this->redirect($this->referer());
        } else {
            $this->Flash->error('Falha ao excluír foto. Tente novamente.');
        }

        $this->redirect($this->referer());
    }

    /**
     * @param null $product_id
     */
    public function import_csv(){

        $academia_save = $this->Academias->newEntity();

        $this->Flash->info('Informe o arquivo a ser importado. <br>
                    Importante: O mesmo deve estar no formato CSV com valores separados por vírgulas "," não formatado
                    e estar populado com dados em ordem');

        if ($this->request->is('post')) {

            /*function customfgetcsv($handle, $length, $separator = ';'){

                if (($buffer = fgets($handle, $length)) !== false) {
                    //return explode($separator, iconv("ISO-8859-15", "UTF-8", $buffer));
                    return explode($separator, iconv("ISO-8859-15", "UTF-8", $buffer));
                }
                return false;
            }*/

            $messages['success'] = '<strong>Academias Adicionadas:</strong><br/>';
            $messages['ignore'] = '<br/><strong>Academias Ignoradas:</strong><br/>';
            $messages['error'] = '<br/><strong>Erros encontrados:</strong><br/>';

            $c_success = 0;
            $c_ignore = 0;
            $c_error = 0;

            $row = 0;
            $handle = fopen($this->request->data['csv']['tmp_name'], "r");
            $csv_array = [];
            $separator = ';';

            if ($handle) {
                while (($line = fgetcsv($handle, 1000, $separator)) !== false) {
                    $line = array_map('utf8_encode', $line);
                    $data = $line;

                    $num = count($data);

                    //while (($data = fgetcsv($handle, 1000, ";")) !== false) {
                    //  $num = count($data);
                    $row++;

                    if ($num >= 17 && $row > 1) {

                        //$data[1] = iconv("ISO-8859-15", "UTF-8", $data[1]);

                        if($data[2] == 'NULL' || empty($data[2]) || $data[2] == null){
                            $messages['error'] .= ' CNPJ Inválido/Faltando -> ' . $data[1] . '<br/>';
                            $c_error++;
                        }else{

                            $states = $this->Academias->Cities->States
                                ->find('all')
                                ->where(['name' => trim($data[13])])
                                ->first();

                            if(count($states) <= 0){
                                $states = $this->Academias->Cities->States
                                    ->find('all')
                                    ->where(['uf' => trim($data[13])])
                                    ->first();
                            }

                            if(count($states) >= 1) {
                                $cities = $this->Academias->Cities
                                    ->find('all')
                                    ->where(['Cities.name' => trim((($data[12])))])
                                    ->andWhere(['Cities.state_id' => $states->id])
                                    //->andWhere(['Cities.uf' => trim($data[2])])
                                    ->first();

                                $password = 'logfitness123';
                                //$password = $this->request->data['password'];

                                if ($cities != null && $states != null) {
                                    $dataSave = [
                                        'name'      => $data[1],
                                        'shortname' => substr($data[4], 0, 22),
                                        'slug'      => sanitizeString($data[0] . '-' . $data[1]),
                                        'cnpj'      => $data[2],
                                        'ie'        => $data[3],
                                        'contact'   => $data[5],
                                        'phone'     => str_replace('NULL', ' ', $data[15]),
                                        'mobile'    => str_replace('NULL', ' ', $data[16]),
                                        'email'     => $data[14],
                                        'password'  => $password,
                                        'address'   => $data[7],
                                        'number'    => $data[8],
                                        'complement'=> $data[9],
                                        'area'      => $data[11],
                                        'city_id'   => $cities->id,
                                        'cep'       => $data[10],
                                        'meta'      => 0,
                                        'alunos'    => $data[6],
                                        'user_id'   => $this->Auth->user('id'),
                                        'status_id' => 1,
                                    ];

                                    $dataCheck = [
                                        'slug' => sanitizeString($data[0] . '-' . $data[1]),
                                        'cnpj' => $data[2],
                                        'email' => $data[14],
                                    ];

                                    if ($this->Academias->exists(['slug' => $dataCheck['slug']])) {
                                        $dataCheck['slug'] = date('YmdHis-').rand().$dataCheck['slug'];
                                        $dataSave['slug']  = date('YmdHis-').rand().$dataCheck['slug'];
                                    }

                                    if ($this->Academias->exists(['cnpj' => $data[2]])) {
                                        $messages['ignore'] .= ' CNPJ Existente -> ' . $dataSave['name'] . '<br/>';
                                        $c_error++;
                                    } elseif ($this->Academias->exists(['email' => $data[14]])) {
                                        $messages['ignore'] .= ' Email já utilizado -> ' . $dataSave['name'] . '<br/>';
                                        $c_error++;
                                    } else {
                                        $academia_save = $this->Academias->newEntity();
                                        //unset($dataSave['password']);
                                        $academia_save = $this->Academias->patchEntity($academia_save, $dataSave);
                                        //$academia_save->password = 'logfitness123';
                                        unset($academia_save->password);
                                        //debug($academia_save);

                                        if ($this->Academias->save($academia_save)) {
                                            $messages['success'] .= ' -> ' . $dataSave['name'] . '<br/>';
                                            $c_success++;
                                        } else {
                                            $messages['error'] .= ' Falha -> ' . $dataSave['name'] . '<br/>';
                                            $c_error++;
                                        }
                                    }

                                } else {
                                    $messages['error'] .= ' Cidade/Estado não encontrado -> ' . $data[1] . '<br/>';
                                    $c_ignore++;
                                }

                                $csv_array[] = $data;
                            }else{
                                $messages['error'] .= ' Estado não encontrado -> ' . $data[13] . '<br/>';
                                $c_ignore++;
                            }
                        }
                    } else {
                        $messages['error'] .= ' -> ' .$data[1] . '<br/>';
                        $c_error++;
                    }
                }
            }
            fclose($handle);

            !$c_success ? $messages['success'] .= ' :: Nenhuma <br/>' : $messages['success'] .= ' :: TOTAL = ' . $c_success . '<br/>';
            !$c_error ? $messages['error'] .= ' :: Nenhum  <br/>' : $messages['error'] .= ' :: TOTAL = ' . $c_error . '<br/>';
            !$c_ignore ? $messages['ignore'] .= ' :: Nenhuma  <br/>' : $messages['ignore'] .= ' :: TOTAL = ' . $c_ignore . '<br/>';

            $this->Flash->info($messages['success'] . $messages['ignore'] . $messages['error']);
            $this->redirect(['action' => 'import_csv']);
        }

        $this->set(compact('cities'));

    }
}
