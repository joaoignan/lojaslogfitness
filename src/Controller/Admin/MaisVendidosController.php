<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;

/**
 * MaisVendidos Controller
 */
class MaisVendidosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index(){
    	$mais_vendidos = $this->MaisVendidos
    		->find('all')
    		->contain(['Produtos', 'Produtos.ProdutoBase'])
    		->order(['MaisVendidos.posicao' => 'asc'])
    		->all();

    	$this->set(compact('mais_vendidos'));
    }

    public function excluir_mais_vendido($mais_vendido_id) {
        $mais_vendido = $this->MaisVendidos
            ->find('all')
            ->where(['id' => $mais_vendido_id])
            ->first();

        $this->MaisVendidos->delete($mais_vendido);

        return $this->redirect('/admin/mais_vendidos');
    }

}

?>