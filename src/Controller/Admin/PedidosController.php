<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

/**
 * Pedidos Controller
 *
 * @property \App\Model\Table\PedidosTable $Pedidos
 */
class PedidosController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($status = 0, $search = null)
    {

        $Users = TableRegistry::get('Users');

        $user = $Users->get($this->Auth->user('id'));

        if($search != null){
            $search_val = $search;

            $search = [
                'OR' => [
                    'Pedidos.id                 ' => $search,
                    'Clientes.name          LIKE' => '%'.$search.'%',
                    'Academias.name         LIKE' => '%'.$search.'%',
                    'Academias.shortname    LIKE' => '%'.$search.'%',
                    'Pedidos.payment_method LIKE' => '%'.$search.'%',
                    'Pedidos.rastreamento   LIKE' => '%'.$search.'%',
                    'Pedidos.valor          LIKE' => '%'.$search.'%',
                    'Cities.name            LIKE' => '%'.$search.'%',
                    'Cities.uf              LIKE' => '%'.$search.'%',
                ]
            ];
        }else{
            $search = [];
        }

        if($user->group_id != 7) {
            if($status == 0){
                $this->paginate = [
                    'contain'   => [
                        'Clientes',
                        'Academias',
                        'Academias.Cities',
                        'Professores',
                        'Academias.Cities',
                        'PedidoStatus',
                        'PedidoItens'
                    ],
                    'conditions' => ['Pedidos.comissao_status IN' => [1,2,3], $search],
                    'order'      => ['Pedidos.id' => 'desc']
                ];
            }else {
                $this->paginate = [
                    'contain'   => [
                        'Clientes',
                        'Academias',
                        'Academias.Cities',
                        'Professores',
                        'Academias.Cities',
                        'PedidoStatus',
                        'PedidoItens'
                    ],
                    'conditions' => ['Pedidos.comissao_status IN' => [1,2,3], $search, 'Pedidos.pedido_status_id' => $status],
                    'order'      => ['Pedidos.id' => 'desc']
                ];
            }
        } else {
            if($status == 0){
                $this->paginate = [
                    'contain'   => [
                        'Clientes',
                        'Academias',
                        'Professores',
                        'Academias.Cities',
                        'PedidoStatus',
                        'PedidoItens'
                    ],
                    'conditions' => ['Pedidos.comissao_status IN' => [1,2], 'Pedidos.user_id' => $user->id, $search],
                    'order'      => ['Pedidos.id' => 'desc']
                ];
            }else {
                $this->paginate = [
                    'contain'   => [
                        'Clientes',
                        'Academias',
                        'Professores',
                        'Academias.Cities',
                        'PedidoStatus',
                        'PedidoItens'
                    ],
                    'conditions' => ['Pedidos.comissao_status IN' => [1,2], 'Pedidos.user_id' => $user->id, $search, 'Pedidos.pedido_status_id' => $status],
                    'order'      => ['Pedidos.id' => 'desc']
                ];
            }
        }

        /*Cancela pedidos pendentes e aguardando pagamento a 7 dias ou mais*/
        $pedidos_pendentes_aguardando = $this->Pedidos
            ->find('all')
            ->where(['Pedidos.pedido_status_id' => 1])
            ->andWhere(['Pedidos.comissao_status IN' => [1, 2]])
            ->orWhere(['Pedidos.pedido_status_id' => 2])
            ->andWhere(['Pedidos.comissao_status IN' => [1, 2]])
            ->all();

        $time = Time::now();
        $time->subDays(7);

        $now = new Time('2017-05-14 23:59:00');

        $now->setDate($time->format('Y'), $time->format('m'), $time->format('d'));

        foreach ($pedidos_pendentes_aguardando as $ppa) {
            if($ppa->modified <= $now) {
                $pedido_can = $this->Pedidos->get($ppa->id);
                $data       = [
                    'pedido_status_id'  => 7,
                ];

                $pedido_can = $this->Pedidos->patchEntity($pedido_can, $data);

                if($this->Pedidos->save($pedido_can)){
                    
                }
            }
        }

        if($user->group_id != 7) {
            $pedido_realizado = $this->Pedidos
                ->find('all')
                ->where(['Pedidos.pedido_status_id' => 1])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2,3]])
                ->count();

            $pedido_aguardando = $this->Pedidos
                ->find('all')
                ->where(['Pedidos.pedido_status_id' => 2])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2,3]])
                ->count();

            $pedido_pago = $this->Pedidos
                ->find('all')
                ->where(['Pedidos.pedido_status_id' => 3])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2,3]])
                ->count();

            $pedido_preparando = $this->Pedidos
                ->find('all')
                ->where(['Pedidos.pedido_status_id' => 4])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2,3]])
                ->count();

            $pedido_transporte = $this->Pedidos
                ->find('all')
                ->where(['Pedidos.pedido_status_id' => 5])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2,3]])
                ->count();

            $pedido_entregue = $this->Pedidos
                ->find('all')
                ->where(['Pedidos.pedido_status_id' => 6])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2,3]])
                ->count();

            $pedido_cancelado = $this->Pedidos
                ->find('all')
                ->where(['Pedidos.pedido_status_id' => 7])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2,3]])
                ->count();

            $pedido_excluido = $this->Pedidos
                ->find('all')
                ->where(['Pedidos.pedido_status_id' => 8])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2,3]])
                ->count();

            $pedido_aguardando_retirada = $this->Pedidos
                ->find('all')
                ->where(['Pedidos.pedido_status_id' => 9])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2,3]])
                ->count();

            $pedido_em_faturamento = $this->Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.pedido_status_id' => 10])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2]])
                ->count();
        } else {
            $pedido_realizado = $this->Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.pedido_status_id' => 1])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2]])
                ->andWhere(['Pedidos.user_id' => $user->id])
                ->count();

            $pedido_aguardando = $this->Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.pedido_status_id' => 2])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2]])
                ->andWhere(['Pedidos.user_id' => $user->id])
                ->count();

            $pedido_pago = $this->Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.pedido_status_id' => 3])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2]])
                ->andWhere(['Pedidos.user_id' => $user->id])
                ->count();

            $pedido_preparando = $this->Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.pedido_status_id' => 4])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2]])
                ->andWhere(['Pedidos.user_id' => $user->id])
                ->count();

            $pedido_transporte = $this->Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.pedido_status_id' => 5])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2]])
                ->andWhere(['Pedidos.user_id' => $user->id])
                ->count();

            $pedido_entregue = $this->Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.pedido_status_id' => 6])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2]])
                ->andWhere(['Pedidos.user_id' => $user->id])
                ->count();

            $pedido_cancelado = $this->Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.pedido_status_id' => 7])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2]])
                ->andWhere(['Pedidos.user_id' => $user->id])
                ->count();

            $pedido_excluido = $this->Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.pedido_status_id' => 8])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2]])
                ->andWhere(['Pedidos.user_id' => $user->id])
                ->count();

            $pedido_aguardando_retirada = $this->Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.pedido_status_id' => 9])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2]])
                ->andWhere(['Pedidos.user_id' => $user->id])
                ->count();

            $pedido_em_faturamento = $this->Pedidos
                ->find('all')
                ->contain(['Academias'])
                ->where(['Pedidos.pedido_status_id' => 10])
                ->andWhere(['Pedidos.comissao_status IN' => [1,2]])
                ->andWhere(['Pedidos.user_id' => $user->id])
                ->count();
        }

        if($this->request->is(['post', 'put'])){

            if(isset($this->request->data['filtro_status']) || isset($this->request->data['filtro_search'])){
                $this->redirect(['action' => 'index', $this->request->data['filtro_status'], $this->request->data['filtro_search']]);
            }else{

                foreach($this->request->data['pedido_status_id'] as $key => $item){
                    $pedido = $this->Pedidos
                        ->find('all')
                        ->contain(['PedidoItens'])
                        ->where(['Pedidos.id' => $key])
                        ->first();

                    $Produtos = TableRegistry::get('Produtos');

                    if(
                        (
                            $pedido->pedido_status_id == 1 ||
                            $pedido->pedido_status_id == 2 ||
                            $pedido->pedido_status_id == 7
                        ) && 
                        (
                            $item >= 3 &&
                            $item != 7 &&
                            $item != 8
                        )
                    ) {
                        $data = [
                            'data_pagamento' => Time::now()
                        ];

                        $pedido = $this->Pedidos->patchEntity($pedido, $data);

                        $this->Pedidos->save($pedido);
                    }

                    if(
                        (
                            $pedido->pedido_status_id == 1 ||
                            $pedido->pedido_status_id == 7 ||
                            $pedido->pedido_status_id == 8
                        ) && 
                        (
                            $item == 2 ||
                            $item == 3
                        )
                    ) {
                        foreach ($pedido->pedido_itens as $pi) {
                            $produto_estoque = $Produtos
                                ->find('all')
                                ->where(['id' => $pi->produto_id])
                                ->first();

                            $estoque_atualizado = ['estoque' => ($produto_estoque->estoque - $pi->quantidade)];

                            $produto_estoque = $Produtos->patchEntity($produto_estoque, $estoque_atualizado);
                            $Produtos->save($produto_estoque);
                        }
                    }

                    if(
                        (
                            $pedido->pedido_status_id >= 2 &&
                            $pedido->pedido_status_id != 7 &&
                            $pedido->pedido_status_id != 8
                        ) && 
                        (
                            $item == 1 ||
                            $item == 7
                        )
                    ) {
                        foreach ($pedido->pedido_itens as $pi) {
                            $produto_estoque = $Produtos
                                ->find('all')
                                ->where(['id' => $pi->produto_id])
                                ->first();

                            $estoque_atualizado = ['estoque' => ($produto_estoque->estoque + $pi->quantidade)];

                            $produto_estoque = $Produtos->patchEntity($produto_estoque, $estoque_atualizado);
                            $Produtos->save($produto_estoque);
                        }
                    }


                    $data = [
                        'pedido_status_id'  => $item,
                    ];

                    $pedido = $this->Pedidos->patchEntity($pedido, $data);

                    $this->Pedidos->save($pedido);
                }


                $this->Flash->info('Atualização concluída');

            }
        }

        $cupom_desconto = TableRegistry::get('CupomDesconto')
            ->find('all')
            ->all();

        $this->paginate['sortWhitelist'] = ['Pedidos.id', 'Pedidos.valor', 'Pedidos.created', 'Pedidos.pedido_status_id', 'Academias.name', 'Clientes.name', 'Pedidos.transportadora_id', 'Cities.transportadora_id'];

        $pedidos = $this->paginate($this->Pedidos);

        $pedido_status = $this->Pedidos->PedidoStatus->find('list');

        $this->set(compact(
            'status',
            'search_val',
            'pedidos',
            'pedido_status',
            'pedido_realizado',
            'pedido_aguardando',
            'pedido_pago',
            'pedido_preparando',
            'pedido_transporte',
            'pedido_entregue',
            'pedido_cancelado',
            'pedido_excluido',
            'pedido_aguardando_retirada',
            'pedido_em_faturamento',
            'cupom_desconto'
        ));
        $this->set('_serialize', ['pedidos']);
    }

    public function excluir_pedido($id) {
        $pedido = $this->Pedidos->get($id);

        $pedido = $this->Pedidos->patchEntity($pedido, ['pedido_status_id' => 8]);
        if($this->Pedidos->save($pedido)) {
            $this->Flash->success('Pedido excluído com sucesso!');
        } else {
            $this->Flash->error('Falha ao excluir pedido, tente novamente');
        }

        return $this->redirect('/admin/pedidos');

        $this->viewBuilder()->layout(false);
        $this->render(false);
    }

    /**
     * View method
     *
     * @param string|null $id Pedido id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pedido = $this->Pedidos->get($id, [
            'contain' => [
                'Clientes',
                'Academias',
                'PedidoStatus',
                'PedidoItens',
                'PedidoItens.Produtos',
                'PedidoItens.Produtos.ProdutoBase',
                'PedidoItens.Produtos.ProdutoBase.Marcas'
            ]
        ]);

        $this->set('pedido', $pedido);
        $this->set('_serialize', ['pedido']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pedido id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pedido = $this->Pedidos->get($id, [
            'contain' => ['Clientes', 'Academias', 'PedidoItens']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $entrega = null;
            if(!empty($this->request->data['entrega'])) {
                $entrega = Time::now();
                $entrega_date = array_reverse(explode('/', $this->request->data['entrega']));
                $entrega->setDate($entrega_date[0], $entrega_date[1], $entrega_date[2]);
            }

            $status_antigo = $pedido->pedido_status_id;

            $this->request->data['entrega'] = $entrega;
            
            $pedido = $this->Pedidos->patchEntity($pedido, $this->request->data);

            if ($new_pedido_edit = $this->Pedidos->save($pedido)) {
                $status_novo = $pedido->pedido_status_id;

                $Produtos = TableRegistry::get('Produtos');

                if(
                    (
                        $status_antigo == 1 ||
                        $status_antigo == 2 ||
                        $status_antigo == 7
                    ) && 
                    (
                        $status_novo >= 3 &&
                        $status_novo != 7 &&
                        $status_novo != 8
                    )
                ) {
                    $data = [
                        'data_pagamento' => Time::now()
                    ];

                    $new_pedido_edit = $this->Pedidos->patchEntity($new_pedido_edit, $data);

                    $this->Pedidos->save($new_pedido_edit);
                }

                if(
                    (
                        $status_antigo == 1 ||
                        $status_antigo == 7 ||
                        $status_antigo == 8
                    ) && 
                    (
                        $status_novo == 2 ||
                        $status_novo == 3
                    )
                ) {
                    foreach ($pedido->pedido_itens as $pi) {
                        $produto_estoque = $Produtos
                            ->find('all')
                            ->where(['id' => $pi->produto_id])
                            ->first();

                        $estoque_atualizado = ['estoque' => ($produto_estoque->estoque - $pi->quantidade)];

                        $produto_estoque = $Produtos->patchEntity($produto_estoque, $estoque_atualizado);
                        $Produtos->save($produto_estoque);
                    }
                }

                if(
                    (
                        $status_antigo >= 2 &&
                        $status_antigo != 7 &&
                        $status_antigo != 8
                    ) && 
                    (
                        $status_novo == 1 ||
                        $status_novo == 7
                    )
                ) {
                    foreach ($pedido->pedido_itens as $pi) {
                        $produto_estoque = $Produtos
                            ->find('all')
                            ->where(['id' => $pi->produto_id])
                            ->first();

                        $estoque_atualizado = ['estoque' => ($produto_estoque->estoque + $pi->quantidade)];

                        $produto_estoque = $Produtos->patchEntity($produto_estoque, $estoque_atualizado);
                        $Produtos->save($produto_estoque);
                    }
                }

                if($status_antigo != 3 && $status_novo == 3) {

                    $data_pagamento = Time::now();

                    $data = [
                        'data_pagamento'    => $data_pagamento
                    ];

                    $new_pedido_edit = $this->Pedidos->patchEntity($new_pedido_edit, $data);
                    $this->Pedidos->save($new_pedido_edit);
                }

                if($status_antigo != 5 && $status_novo == 5) {

                    $data_transporte = Time::now();

                    $data = [
                        'data_transporte'    => $data_transporte
                    ];

                    $new_pedido_edit = $this->Pedidos->patchEntity($new_pedido_edit, $data);
                    $this->Pedidos->save($new_pedido_edit);

                    // Email::configTransport('cliente', [
                    //     'className' => 'Smtp',
                    //     'host' => 'mail.logfitness.com.br',
                    //     'port' => 587,
                    //     'timeout' => 30,
                    //     'username' => EMAIL_USER,
                    //     'password' => EMAIL_SENHA,
                    //     'client' => null,
                    //     'tls' => null,
                    // ]);

                    // $email = new Email();

                    // $email
                    //     ->transport('cliente')
                    //     ->template('pedido_transporte')
                    //     ->emailFormat('html')
                    //     ->from([EMAIL_USER => EMAIL_NAME])
                    //     ->to($pedido->cliente->email)
                    //     ->subject(EMAIL_NAME' - Pedido a caminho!')
                    //     ->set([
                    //         'id'                => $pedido->id,
                    //         'name'              => $pedido->cliente->name,
                    //         'academia'          => $pedido->academia->shortname,
                    //         'logo_academia'     => $pedido->academia->image,
                    //         'codigo_rastreio'   => $pedido->rastreamento
                    //     ])
                    //     ->send();

                    // Email::configTransport('academia', [
                    //     'className' => 'Smtp',
                    //     'host' => 'mail.logfitness.com.br',
                    //     'port' => 587,
                    //     'timeout' => 30,
                    //     'username' => EMAIL_USER,
                    //     'password' => EMAIL_SENHA,
                    //     'client' => null,
                    //     'tls' => null,
                    // ]);

                    // $email = new Email();

                    // $email
                    //     ->transport('academia')
                    //     ->template('pedido_transporte_academia')
                    //     ->emailFormat('html')
                    //     ->from([EMAIL_USER => EMAIL_NAME])
                    //     ->to($pedido->academia->email)
                    //     ->subject(EMAIL_NAME' - Pedido a caminho!')
                    //     ->set([
                    //         'id'                => $pedido->id,
                    //         'name'              => $pedido->academia->contact,
                    //         'name_aluno'        => $pedido->cliente->name,   
                    //         'academia'          => $pedido->academia->shortname,
                    //         'logo_academia'     => $pedido->academia->image,
                    //         'codigo_rastreio'   => $pedido->rastreamento
                    //     ])
                    //     ->send();

                    if($pedido->professor_id != null) {
                        $Professor = TableRegistry::get('Professores');

                        $professor = $Professor->get($pedido->professor_id);

                        // Email::configTransport('professor', [
                        //     'className' => 'Smtp',
                        //     'host' => 'mail.logfitness.com.br',
                        //     'port' => 587,
                        //     'timeout' => 30,
                        //     'username' => EMAIL_USER,
                        //     'password' => EMAIL_SENHA,
                        //     'client' => null,
                        //     'tls' => null,
                        // ]);

                        // $email = new Email();

                        // $email
                        //     ->transport('professor')
                        //     ->template('pedido_transporte_professor')
                        //     ->emailFormat('html')
                        //     ->from([EMAIL_USER => EMAIL_NAME])
                        //     ->to($professor->email)
                        //     ->subject(EMAIL_NAME' - Pedido a caminho!')
                        //     ->set([
                        //         'id'                => $pedido->id,
                        //         'name'              => $professor->name,
                        //         'name_aluno'        => $pedido->cliente->name,
                        //         'academia'          => $pedido->academia->shortname,
                        //         'logo_academia'     => $pedido->academia->image,
                        //         'codigo_rastreio'   => $pedido->rastreamento
                        //     ])
                        //     ->send();
                    }
                }

                if($status_antigo != 6 && $status_novo == 6) {

                    // Email::configTransport('cliente', [
                    //     'className' => 'Smtp',
                    //     'host' => 'mail.logfitness.com.br',
                    //     'port' => 587,
                    //     'timeout' => 30,
                    //     'username' => EMAIL_USER,
                    //     'password' => EMAIL_SENHA,
                    //     'client' => null,
                    //     'tls' => null,
                    // ]);

                    // $email = new Email();

                    // $email
                    //     ->transport('cliente')
                    //     ->template('pedido_entregue')
                    //     ->emailFormat('html')
                    //     ->from([EMAIL_USER => EMAIL_NAME])
                    //     ->to($pedido->cliente->email)
                    //     ->subject(EMAIL_NAME' - Pedido entregue!')
                    //     ->set([
                    //         'id'                => $pedido->id,
                    //         'name'              => $pedido->cliente->name,
                    //         'academia'          => $pedido->academia->shortname,
                    //         'logo_academia'     => $pedido->academia->image
                    //     ])
                    //     ->send();

                    // Email::configTransport('academia', [
                    //     'className' => 'Smtp',
                    //     'host' => 'mail.logfitness.com.br',
                    //     'port' => 587,
                    //     'timeout' => 30,
                    //     'username' => EMAIL_USER,
                    //     'password' => EMAIL_SENHA,
                    //     'client' => null,
                    //     'tls' => null,
                    // ]);

                    // $email = new Email();

                    // $email
                    //     ->transport('academia')
                    //     ->template('pedido_entregue_academia')
                    //     ->emailFormat('html')
                    //     ->from([EMAIL_USER => EMAIL_NAME])
                    //     ->to($pedido->academia->email)
                    //     ->subject(EMAIL_NAME' - Pedido entregue!')
                    //     ->set([
                    //         'id'                => $pedido->id,
                    //         'name_aluno'        => $pedido->cliente->name,
                    //         'name'              => $pedido->academia->contact,
                    //         'academia'          => $pedido->academia->shortname,
                    //         'logo_academia'     => $pedido->academia->image
                    //     ])
                    //     ->send();

                    if($pedido->professor_id != null) {
                        $Professor = TableRegistry::get('Professores');

                        $professor = $Professor->get($pedido->professor_id);

                        // Email::configTransport('professor', [
                        //     'className' => 'Smtp',
                        //     'host' => 'mail.logfitness.com.br',
                        //     'port' => 587,
                        //     'timeout' => 30,
                        //     'username' => EMAIL_USER,
                        //     'password' => EMAIL_SENHA,
                        //     'client' => null,
                        //     'tls' => null,
                        // ]);

                        // $email = new Email();

                        // $email
                        //     ->transport('professor')
                        //     ->template('pedido_entregue_professor')
                        //     ->emailFormat('html')
                        //     ->from([EMAIL_USER => EMAIL_NAME])
                        //     ->to($professor->email)
                        //     ->subject(EMAIL_NAME' - Pedido entregue!')
                        //     ->set([
                        //         'id'                => $pedido->id,
                        //         'name'              => $professor->name,
                        //         'name_aluno'        => $pedido->cliente->name,
                        //         'academia'          => $pedido->academia->shortname,
                        //         'logo_academia'     => $pedido->academia->image
                        //     ])
                        //     ->send();
                    }
                }

                if($status_antigo != 9 && $status_novo == 9) {

                    // Email::configTransport('cliente', [
                    //     'className' => 'Smtp',
                    //     'host' => 'mail.logfitness.com.br',
                    //     'port' => 587,
                    //     'timeout' => 30,
                    //     'username' => EMAIL_USER,
                    //     'password' => EMAIL_SENHA,
                    //     'client' => null,
                    //     'tls' => null,
                    // ]);

                    // $email = new Email();

                    // $email
                    //     ->transport('cliente')
                    //     ->template('pedido_transporte')
                    //     ->emailFormat('html')
                    //     ->from([EMAIL_USER => EMAIL_NAME])
                    //     ->to($pedido->cliente->email)
                    //     ->subject(EMAIL_NAME' - Pedido a caminho!')
                    //     ->set([
                    //         'id'                => $pedido->id,
                    //         'name'              => $pedido->cliente->name,
                    //         'academia'          => $pedido->academia->shortname,
                    //         'logo_academia'     => $pedido->academia->image,
                    //         'observacoes'       => $pedido->observacao
                    //     ])
                    //     ->send();

                    // Email::configTransport('academia', [
                    //     'className' => 'Smtp',
                    //     'host' => 'mail.logfitness.com.br',
                    //     'port' => 587,
                    //     'timeout' => 30,
                    //     'username' => EMAIL_USER,
                    //     'password' => EMAIL_SENHA,
                    //     'client' => null,
                    //     'tls' => null,
                    // ]);

                    // $email = new Email();

                    // $email
                    //     ->transport('academia')
                    //     ->template('pedido_transporte_academia')
                    //     ->emailFormat('html')
                    //     ->from([EMAIL_USER => EMAIL_NAME])
                    //     ->to($pedido->academia->email)
                    //     ->subject(EMAIL_NAME' - Pedido a caminho!')
                    //     ->set([
                    //         'id'                => $pedido->id,
                    //         'name'              => $pedido->academia->contact,
                    //         'name_aluno'        => $pedido->cliente->name,   
                    //         'academia'          => $pedido->academia->shortname,
                    //         'logo_academia'     => $pedido->academia->image,
                    //         'observacoes'       => $pedido->observacao
                    //     ])
                    //     ->send();

                    if($pedido->professor_id != null) {
                        $Professor = TableRegistry::get('Professores');

                        $professor = $Professor->get($pedido->professor_id);

                        // Email::configTransport('professor', [
                        //     'className' => 'Smtp',
                        //     'host' => 'mail.logfitness.com.br',
                        //     'port' => 587,
                        //     'timeout' => 30,
                        //     'username' => EMAIL_USER,
                        //     'password' => EMAIL_SENHA,
                        //     'client' => null,
                        //     'tls' => null,
                        // ]);

                        // $email = new Email();

                        // $email
                        //     ->transport('professor')
                        //     ->template('pedido_transporte_professor')
                        //     ->emailFormat('html')
                        //     ->from([EMAIL_USER => EMAIL_NAME])
                        //     ->to($professor->email)
                        //     ->subject(EMAIL_NAME' - Pedido a caminho!')
                        //     ->set([
                        //         'id'                => $pedido->id,
                        //         'name'              => $professor->name,
                        //         'name_aluno'        => $pedido->cliente->name,
                        //         'academia'          => $pedido->academia->shortname,
                        //         'logo_academia'     => $pedido->academia->image,
                        //         'observacoes'       => $pedido->observacao
                        //     ])
                        //     ->send();
                    }
                }

                if($status_novo == 5 && $new_pedido_edit->rastreamento && $pedido->sms_status == 0) {

                    $nome_aluno = explode(' ', $pedido->cliente->name)[0];

                    function resume_string($var, $limite){  
                        if (strlen($var) > $limite) {       
                            $var = substr($var, 0, $limite);        
                            $var = trim($var) . "...";  
                        }

                        return $var;
                    }

                    if($pedido->cliente->mobile) {
                        
                        // $ch = curl_init();

                        // $header = [
                        //     "Accept: application/json",
                        //     "Content-Type: application/json",
                        //     "Authorization: Basic Y2FjaXF1ZS53ZWI6REd2dHdMckNTZA=="
                        // ];

                        // $prefixo = explode('(', $pedido->cliente->telephone);
                        // $prefixo = explode(') ', $prefixo[1]);
                        // $posfixo = explode('-', $prefixo[1]);
                        // $posfixo = $posfixo[0].$posfixo[1];

                        // $numero_cliente = '55'.$prefixo[0].$posfixo;

                        // $Mensagens = TableRegistry::get('Mensagens');

                        // $mensagem = $Mensagens->newEntity();

                        // $data_mensagem = [
                        //     'remetente' => EMAIL_NAME,
                        //     'destinatario' => $numero_cliente,
                        //     'msg' => "O seu pedido ".$pedido->id." está a caminho da ".resume_string($pedido->academia->shortname, 22).". Para rastrear entre ".$transportadora->site." e use o código ".$new_pedido_edit->rastreamento,
                        //     'quem' => 'Aluno'
                        // ];

                        // $mensagem = $Mensagens->patchEntity($mensagem, $data_mensagem);

                        // $mensagem = $Mensagens->save($mensagem);

                        // $body = [
                        //     "id" => $mensagem->id,
                        //     "from" => "LOGFITNESS",
                        //     "to" => $numero_cliente,
                        //     "msg" => "O seu pedido ".$pedido->id." está a caminho da ".resume_string($pedido->academia->shortname, 22).". Para rastrear entre ".$transportadora->site." e use o código ".$new_pedido_edit->rastreamento
                        // ];

                        // $data = [
                        //     "sendSmsRequest" => $body
                        // ];

                        // $data = json_encode($data);

                        // $options = array(CURLOPT_URL => "https://api-rest.zenvia360.com.br/services/send-sms",
                        //     CURLOPT_RETURNTRANSFER => TRUE,
                        //     CURLOPT_HEADER => FALSE,
                        //     CURLOPT_POST => TRUE,
                        //     CURLOPT_POSTFIELDS => $data,
                        //     CURLOPT_HTTPHEADER => $header
                        // );

                        // curl_setopt_array($ch, $options);

                        // $response = curl_exec($ch);
                        // curl_close($ch);

                        // $resp_send_sms = json_decode($response);

                        // $new_pedido_edit = $this->Pedidos->patchEntity($new_pedido_edit, ['sms_response' => serialize($response)]);

                        // if($resp_send_sms->sendSmsResponse->statusCode == '00') {
                        //     $new_pedido_edit = $this->Pedidos->patchEntity($new_pedido_edit, ['sms_status' => 1]);

                        //     $this->Pedidos->save($new_pedido_edit);

                        //     $mensagem = $Mensagens->patchEntity($mensagem, ['enviado' => 1]);

                        //     $mensagem = $Mensagens->save($mensagem);

                        //     $this->Flash->success(__('SMS enviado para o aluno com sucesso!'));
                        // } else {
                        //     $this->Flash->error('error aluno:'.$response);
                        // }
                    }

                    if($pedido->academia->mobile) {
                        $prefixo = explode('(', $pedido->academia->mobile);
                        $prefixo = explode(') ', $prefixo[1]);
                        $posfixo = explode('-', $prefixo[1]);
                        $posfixo = $posfixo[0].$posfixo[1];

                        $numero_academia = '55'.$prefixo[0].$posfixo;

                        if($numero_academia != $numero_cliente) {
                            // $ch = curl_init();

                            // $header = [
                            //     "Accept: application/json",
                            //     "Content-Type: application/json",
                            //     "Authorization: Basic Y2FjaXF1ZS53ZWI6REd2dHdMckNTZA=="
                            // ];

                            // $Mensagens = TableRegistry::get('Mensagens');

                            // $mensagem = $Mensagens->newEntity();

                            // $data_mensagem = [
                            //     'remetente' => EMAIL_NAME,
                            //     'destinatario' => $numero_academia,
                            //     'msg' => "O pedido ".$pedido->id." do ".resume_string($nome_aluno, 22)." está a caminho. Para rastrear entre ".$transportadora->site." e use o código ".$new_pedido_edit->rastreamento,
                            //     'quem' => 'Academia'
                            // ];

                            // $mensagem = $Mensagens->patchEntity($mensagem, $data_mensagem);

                            // $mensagem = $Mensagens->save($mensagem);

                            // $body = [
                            //     "id" => $mensagem->id,
                            //     "from" => "LOGFITNESS",
                            //     "to" => $numero_academia,
                            //     "msg" => "O pedido ".$pedido->id." do ".resume_string($nome_aluno, 22)." está a caminho. Para rastrear entre ".$transportadora->site." e use o código ".$new_pedido_edit->rastreamento
                            // ];

                            // $data = [
                            //     "sendSmsRequest" => $body
                            // ];

                            // $data = json_encode($data);

                            // $options = array(CURLOPT_URL => "https://api-rest.zenvia360.com.br/services/send-sms",
                            //     CURLOPT_RETURNTRANSFER => TRUE,
                            //     CURLOPT_HEADER => FALSE,
                            //     CURLOPT_POST => TRUE,
                            //     CURLOPT_POSTFIELDS => $data,
                            //     CURLOPT_HTTPHEADER => $header
                            // );

                            // curl_setopt_array($ch, $options);

                            // $response = curl_exec($ch);
                            // curl_close($ch);

                            // $resp_send_sms = json_decode($response);

                            // $new_pedido_edit = $this->Pedidos->patchEntity($new_pedido_edit, ['sms_response' => serialize($response)]);

                            // if($resp_send_sms->sendSmsResponse->statusCode == '00') {
                            //     $new_pedido_edit = $this->Pedidos->patchEntity($new_pedido_edit, ['sms_status' => 1]);

                            //     $this->Pedidos->save($new_pedido_edit);

                            //     $mensagem = $Mensagens->patchEntity($mensagem, ['enviado' => 1]);

                            //     $mensagem = $Mensagens->save($mensagem);

                            //     $this->Flash->success(__('SMS enviado para a academia com sucesso!'));
                            // } else {
                            //     $this->Flash->error('error academia:'.$response);
                            // }
                        }
                    }

                    $Professores = TableRegistry::get('Professores');

                    $professor = $Professores
                        ->find('all')
                        ->where(['id' => $pedido->professor_id])
                        ->first();

                    if($professor->mobile) {

                        $prefixo = explode('(', $professor->mobile);
                        $prefixo = explode(') ', $prefixo[1]);
                        $posfixo = explode('-', $prefixo[1]);
                        $posfixo = $posfixo[0].$posfixo[1];

                        $numero_professor = '55'.$prefixo[0].$posfixo;

                        if($numero_professor != $numero_cliente && $numero_professor != $numero_academia) {
                            // $ch = curl_init();

                            // $header = [
                            //     "Accept: application/json",
                            //     "Content-Type: application/json",
                            //     "Authorization: Basic Y2FjaXF1ZS53ZWI6REd2dHdMckNTZA=="
                            // ];

                            // $Mensagens = TableRegistry::get('Mensagens');

                            // $mensagem = $Mensagens->newEntity();

                            // $data_mensagem = [
                            //     'remetente' => EMAIL_NAME,
                            //     'destinatario' => $numero_professor,
                            //     'msg' => "O pedido ".$pedido->id." do ".resume_string($nome_aluno, 22)." está a caminho. Para rastrear entre ".$transportadora->site." e use o código ".$new_pedido_edit->rastreamento,
                            //     'quem' => 'Time'
                            // ];

                            // $mensagem = $Mensagens->patchEntity($mensagem, $data_mensagem);

                            // $mensagem = $Mensagens->save($mensagem);

                            // $body = [
                            //     "id" => $mensagem->id,
                            //     "from" => "LOGFITNESS",
                            //     "to" => $numero_professor,
                            //     "msg" => "O pedido ".$pedido->id." do ".resume_string($nome_aluno, 22)." está a caminho. Para rastrear entre ".$transportadora->site." e use o código ".$new_pedido_edit->rastreamento
                            // ];

                            // $data = [
                            //     "sendSmsRequest" => $body
                            // ];

                            // $data = json_encode($data);

                            // $options = array(CURLOPT_URL => "https://api-rest.zenvia360.com.br/services/send-sms",
                            //     CURLOPT_RETURNTRANSFER => TRUE,
                            //     CURLOPT_HEADER => FALSE,
                            //     CURLOPT_POST => TRUE,
                            //     CURLOPT_POSTFIELDS => $data,
                            //     CURLOPT_HTTPHEADER => $header
                            // );

                            // curl_setopt_array($ch, $options);

                            // $response = curl_exec($ch);
                            // curl_close($ch);

                            // $resp_send_sms = json_decode($response);

                            // $new_pedido_edit = $this->Pedidos->patchEntity($new_pedido_edit, ['sms_response' => serialize($response)]);

                            // if($resp_send_sms->sendSmsResponse->statusCode == '00') {
                            //     $new_pedido_edit = $this->Pedidos->patchEntity($new_pedido_edit, ['sms_status' => 1]);

                            //     $this->Pedidos->save($new_pedido_edit);

                            //     $mensagem = $Mensagens->patchEntity($mensagem, ['enviado' => 1]);

                            //     $mensagem = $Mensagens->save($mensagem);

                            //     $this->Flash->success(__('SMS enviado para o professor com sucesso!'));
                            // } else {
                            //     $this->Flash->error('error professor:'.$response);
                            // }
                        }
                    }
                }

                $this->Flash->success(__('Pedido salvo com sucesso!'));
                //return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Falha ao salvar pedido... Tente novamente'));
            }
        }

        // $transportadora_padrao = $pedido->transportadora_id;

        // if(!$transportadora_padrao) {
        //     $cidade = TableRegistry::get('Cities')
        //         ->find('all')
        //         ->where(['id' => $pedido->academia->city_id])
        //         ->first();
            
        //     if($cidade->transportadora_id) {
        //         $transportadora_padrao = $cidade->transportadora_id;
        //     } else {
        //         $transportadora_padrao = null;
        //     }
        // }

        $clientes = $this->Pedidos->Clientes->find('list', ['limit' => 200]);
        $academias = $this->Pedidos->Academias->find('list', ['limit' => 200]);
        $pedidoStatus = $this->Pedidos->PedidoStatus->find('list', ['limit' => 200]);
        $this->set(compact('pedido', 'clientes', 'academias', 'transportadoras', 'pedidoStatus', 'transportadora_padrao'));
        $this->set('_serialize', ['pedido']);
    }

    public function carteira_log($academia_id = null)
    {
        $this->paginate = [
            'contain' => ['Academias'],
            'conditions' => ['Pedidos.comissao_status' => 3],
            'order' => ['Pedidos.carteira_status' => 'asc']
        ];

        if($academia_id != null) {
            $pedidos_pago = $this->Pedidos
                ->find('all')
                ->where(['Pedidos.comissao_status' => 3])
                ->andWhere(['Pedidos.carteira_status' => 0])
                ->andWhere(['Pedidos.academia_id' => $academia_id])
                ->all();

            $ok = 0;
            $error = 0;

            foreach ($pedidos_pago as $pp) {
                $data = [
                    'carteira_status' => 1
                ];

                $pp = $this->Pedidos->patchEntity($pp, $data);
                if ($this->Pedidos->save($pp)) {
                    $ok = 1;
                } else {
                    $error = 1;
                }
            }

            if($error == 1 || $ok == 0) {
                $this->Flash->error('Falha ao pagar valor. Tente novamente.');
            } else if($ok == 1 && $error == 0) {
                $this->Flash->success('Valor pago com sucesso.');
                return $this->redirect(['action' => 'carteira_log']);
            }
        }

        $pedidos_total = $this->Pedidos
            ->find('all');

        $pedidos_total
            ->select([
                'academia_id' => 'Academias.id',
                'academia_name' => 'Academias.shortname',
                'total' => $pedidos_total->func()->sum('valor')
            ])
            ->leftJoin(
                ['Academias' => 'academias'],
                [
                    'Academias.id = Pedidos.academia_id'
                ])
            ->where(['Pedidos.comissao_status' => 3])
            ->andWhere(['Pedidos.carteira_status' => 0]);

        $pedidos_total->all();

        $this->set('pedidos_total', $pedidos_total);
        $this->set('pedidos', $this->paginate($this->Pedidos));
        $this->set('_serialize', ['pedidos']);
    }

    public function reenviar_sms_email($id) {
        $pedido = $this->Pedidos
            ->find('all')
            ->contain(['Clientes', 'Academias'])
            ->where(['Pedidos.id' => $id])
            ->first();

        if($pedido->pedido_status_id == 5 && $pedido->rastreamento && $pedido->transportadora_id) {
            
            //Envio emails
            // Email::configTransport('cliente', [
            //     'className' => 'Smtp',
            //     'host' => 'mail.logfitness.com.br',
            //     'port' => 587,
            //     'timeout' => 30,
            //     'username' => EMAIL_USER,
            //     'password' => EMAIL_SENHA,
            //     'client' => null,
            //     'tls' => null,
            // ]);

            // $email = new Email();

            // $email
            //     ->transport('cliente')
            //     ->template('pedido_transporte')
            //     ->emailFormat('html')
            //     ->from([EMAIL_USER => EMAIL_NAME])
            //     ->to($pedido->cliente->email)
            //     ->subject(EMAIL_NAME' - Pedido a caminho!')
            //     ->set([
            //         'id'                => $pedido->id,
            //         'name'              => $pedido->cliente->name,
            //         'academia'          => $pedido->academia->shortname,
            //         'logo_academia'     => $pedido->academia->image,
            //         'codigo_rastreio'   => $pedido->rastreamento
            //     ])
            //     ->send();

            // Email::configTransport('academia', [
            //     'className' => 'Smtp',
            //     'host' => 'mail.logfitness.com.br',
            //     'port' => 587,
            //     'timeout' => 30,
            //     'username' => EMAIL_USER,
            //     'password' => EMAIL_SENHA,
            //     'client' => null,
            //     'tls' => null,
            // ]);

            // $email = new Email();

            // $email
            //     ->transport('academia')
            //     ->template('pedido_transporte_academia')
            //     ->emailFormat('html')
            //     ->from([EMAIL_USER => EMAIL_NAME])
            //     ->to($pedido->academia->email)
            //     ->subject(EMAIL_NAME' - Pedido a caminho!')
            //     ->set([
            //         'id'                => $pedido->id,
            //         'name'              => $pedido->academia->contact,
            //         'name_aluno'        => $pedido->cliente->name,   
            //         'academia'          => $pedido->academia->shortname,
            //         'logo_academia'     => $pedido->academia->image,
            //         'codigo_rastreio'   => $pedido->rastreamento
            //     ])
            //     ->send();

            if($pedido->professor_id != null) {
                $Professor = TableRegistry::get('Professores');

                $professor = $Professor->get($pedido->professor_id);

                // Email::configTransport('professor', [
                //     'className' => 'Smtp',
                //     'host' => 'mail.logfitness.com.br',
                //     'port' => 587,
                //     'timeout' => 30,
                //     'username' => EMAIL_USER,
                //     'password' => EMAIL_SENHA,
                //     'client' => null,
                //     'tls' => null,
                // ]);

                // $email = new Email();

                // $email
                //     ->transport('professor')
                //     ->template('pedido_transporte_professor')
                //     ->emailFormat('html')
                //     ->from([EMAIL_USER => EMAIL_NAME])
                //     ->to($professor->email)
                //     ->subject(EMAIL_NAME' - Pedido a caminho!')
                //     ->set([
                //         'id'                => $pedido->id,
                //         'name'              => $professor->name,
                //         'name_aluno'        => $pedido->cliente->name,
                //         'academia'          => $pedido->academia->shortname,
                //         'logo_academia'     => $pedido->academia->image,
                //         'codigo_rastreio'   => $pedido->rastreamento
                //     ])
                //     ->send();
            }

            //Envio sms
            // $Transportadoras = TableRegistry::get('Transportadoras');

            // $transportadora = $Transportadoras
            //     ->find('all')
            //     ->where(['id' => $pedido->transportadora_id])
            //     ->first();

            $nome_aluno = explode(' ', $pedido->cliente->name)[0];

            function resume_string($var, $limite){  
                if (strlen($var) > $limite) {       
                    $var = substr($var, 0, $limite);        
                    $var = trim($var) . "...";  
                }

                return $var;
            }

            if($pedido->cliente->mobile) {
                
                // $ch = curl_init();

                // $header = [
                //     "Accept: application/json",
                //     "Content-Type: application/json",
                //     "Authorization: Basic Y2FjaXF1ZS53ZWI6REd2dHdMckNTZA=="
                // ];

                // $prefixo = explode('(', $pedido->cliente->telephone);
                // $prefixo = explode(') ', $prefixo[1]);
                // $posfixo = explode('-', $prefixo[1]);
                // $posfixo = $posfixo[0].$posfixo[1];

                // $numero_cliente = '55'.$prefixo[0].$posfixo;

                // $Mensagens = TableRegistry::get('Mensagens');

                // $mensagem = $Mensagens->newEntity();

                // $data_mensagem = [
                //     'remetente' => EMAIL_NAME,
                //     'destinatario' => $numero_cliente,
                //     'msg' => "O seu pedido ".$pedido->id." está a caminho da ".resume_string($pedido->academia->shortname, 22).". Para rastrear entre ".$transportadora->site." e use o código ".$pedido->rastreamento,
                //     'quem' => 'Aluno'
                // ];

                // $mensagem = $Mensagens->patchEntity($mensagem, $data_mensagem);

                // $mensagem = $Mensagens->save($mensagem);

                // $body = [
                //     "id" => $mensagem->id,
                //     "from" => "LOGFITNESS",
                //     "to" => $numero_cliente,
                //     "msg" => "O seu pedido ".$pedido->id." está a caminho da ".resume_string($pedido->academia->shortname, 22).". Para rastrear entre ".$transportadora->site." e use o código ".$pedido->rastreamento
                // ];

                // $data = [
                //     "sendSmsRequest" => $body
                // ];

                // $data = json_encode($data);

                // $options = array(CURLOPT_URL => "https://api-rest.zenvia360.com.br/services/send-sms",
                //     CURLOPT_RETURNTRANSFER => TRUE,
                //     CURLOPT_HEADER => FALSE,
                //     CURLOPT_POST => TRUE,
                //     CURLOPT_POSTFIELDS => $data,
                //     CURLOPT_HTTPHEADER => $header
                // );

                // curl_setopt_array($ch, $options);

                // $response = curl_exec($ch);
                // curl_close($ch);

                // $resp_send_sms = json_decode($response);

                // $pedido = $this->Pedidos->patchEntity($pedido, ['sms_response' => serialize($response)]);

                // if($resp_send_sms->sendSmsResponse->statusCode == '00') {
                //     $pedido = $this->Pedidos->patchEntity($pedido, ['sms_status' => 1]);

                //     $this->Pedidos->save($pedido);

                //     $mensagem = $Mensagens->patchEntity($mensagem, ['enviado' => 1]);

                //     $mensagem = $Mensagens->save($mensagem);

                //     $this->Flash->success(__('SMS enviado para o aluno com sucesso!'));
                // } else {
                //     $this->Flash->error('error aluno:'.$response);
                // }
            }

            if($pedido->academia->mobile) {
                $prefixo = explode('(', $pedido->academia->mobile);
                $prefixo = explode(') ', $prefixo[1]);
                $posfixo = explode('-', $prefixo[1]);
                $posfixo = $posfixo[0].$posfixo[1];

                $numero_academia = '55'.$prefixo[0].$posfixo;

                if($numero_academia != $numero_cliente) {
                    // $ch = curl_init();

                    // $header = [
                    //     "Accept: application/json",
                    //     "Content-Type: application/json",
                    //     "Authorization: Basic Y2FjaXF1ZS53ZWI6REd2dHdMckNTZA=="
                    // ];

                    // $Mensagens = TableRegistry::get('Mensagens');

                    // $mensagem = $Mensagens->newEntity();

                    // $data_mensagem = [
                    //     'remetente' => EMAIL_NAME,
                    //     'destinatario' => $numero_academia,
                    //     'msg' => "O pedido ".$pedido->id." do ".resume_string($nome_aluno, 22)." está a caminho. Para rastrear entre ".$transportadora->site." e use o código ".$pedido->rastreamento,
                    //     'quem' => 'Academia'
                    // ];

                    // $mensagem = $Mensagens->patchEntity($mensagem, $data_mensagem);

                    // $mensagem = $Mensagens->save($mensagem);

                    // $body = [
                    //     "id" => $mensagem->id,
                    //     "from" => "LOGFITNESS",
                    //     "to" => $numero_academia,
                    //     "msg" => "O pedido ".$pedido->id." do ".resume_string($nome_aluno, 22)." está a caminho. Para rastrear entre ".$transportadora->site." e use o código ".$pedido->rastreamento
                    // ];

                    // $data = [
                    //     "sendSmsRequest" => $body
                    // ];

                    // $data = json_encode($data);

                    // $options = array(CURLOPT_URL => "https://api-rest.zenvia360.com.br/services/send-sms",
                    //     CURLOPT_RETURNTRANSFER => TRUE,
                    //     CURLOPT_HEADER => FALSE,
                    //     CURLOPT_POST => TRUE,
                    //     CURLOPT_POSTFIELDS => $data,
                    //     CURLOPT_HTTPHEADER => $header
                    // );

                    // curl_setopt_array($ch, $options);

                    // $response = curl_exec($ch);
                    // curl_close($ch);

                    // $resp_send_sms = json_decode($response);

                    // $pedido = $this->Pedidos->patchEntity($pedido, ['sms_response' => serialize($response)]);

                    // if($resp_send_sms->sendSmsResponse->statusCode == '00') {
                    //     $pedido = $this->Pedidos->patchEntity($pedido, ['sms_status' => 1]);

                    //     $this->Pedidos->save($pedido);

                    //     $mensagem = $Mensagens->patchEntity($mensagem, ['enviado' => 1]);

                    //     $mensagem = $Mensagens->save($mensagem);

                    //     $this->Flash->success(__('SMS enviado para a academia com sucesso!'));
                    // } else {
                    //     $this->Flash->error('error academia:'.$response);
                    // }
                }
            }

            $Professores = TableRegistry::get('Professores');

            $professor = $Professores
                ->find('all')
                ->where(['id' => $pedido->professor_id])
                ->first();

            if($professor->mobile) {

                $prefixo = explode('(', $professor->mobile);
                $prefixo = explode(') ', $prefixo[1]);
                $posfixo = explode('-', $prefixo[1]);
                $posfixo = $posfixo[0].$posfixo[1];

                $numero_professor = '55'.$prefixo[0].$posfixo;

                if($numero_professor != $numero_cliente && $numero_professor != $numero_academia) {
                    // $ch = curl_init();

                    // $header = [
                    //     "Accept: application/json",
                    //     "Content-Type: application/json",
                    //     "Authorization: Basic Y2FjaXF1ZS53ZWI6REd2dHdMckNTZA=="
                    // ];

                    // $Mensagens = TableRegistry::get('Mensagens');

                    // $mensagem = $Mensagens->newEntity();

                    // $data_mensagem = [
                    //     'remetente' => EMAIL_NAME,
                    //     'destinatario' => $numero_professor,
                    //     'msg' => "O pedido ".$pedido->id." do ".resume_string($nome_aluno, 22)." está a caminho. Para rastrear entre ".$transportadora->site." e use o código ".$pedido->rastreamento,
                    //     'quem' => 'Time'
                    // ];

                    // $mensagem = $Mensagens->patchEntity($mensagem, $data_mensagem);

                    // $mensagem = $Mensagens->save($mensagem);

                    // $body = [
                    //     "id" => $mensagem->id,
                    //     "from" => "LOGFITNESS",
                    //     "to" => $numero_professor,
                    //     "msg" => "O pedido ".$pedido->id." do ".resume_string($nome_aluno, 22)." está a caminho. Para rastrear entre ".$transportadora->site." e use o código ".$pedido->rastreamento
                    // ];

                    // $data = [
                    //     "sendSmsRequest" => $body
                    // ];

                    // $data = json_encode($data);

                    // $options = array(CURLOPT_URL => "https://api-rest.zenvia360.com.br/services/send-sms",
                    //     CURLOPT_RETURNTRANSFER => TRUE,
                    //     CURLOPT_HEADER => FALSE,
                    //     CURLOPT_POST => TRUE,
                    //     CURLOPT_POSTFIELDS => $data,
                    //     CURLOPT_HTTPHEADER => $header
                    // );

                    // curl_setopt_array($ch, $options);

                    // $response = curl_exec($ch);
                    // curl_close($ch);

                    // $resp_send_sms = json_decode($response);

                    // $pedido = $this->Pedidos->patchEntity($pedido, ['sms_response' => serialize($response)]);

                    // if($resp_send_sms->sendSmsResponse->statusCode == '00') {
                    //     $pedido = $this->Pedidos->patchEntity($pedido, ['sms_status' => 1]);

                    //     $this->Pedidos->save($pedido);

                    //     $mensagem = $Mensagens->patchEntity($mensagem, ['enviado' => 1]);

                    //     $mensagem = $Mensagens->save($mensagem);

                    //     $this->Flash->success(__('SMS enviado para o professor com sucesso!'));
                    // } else {
                    //     $this->Flash->error('error professor:'.$response);
                    // }
                }
            }
        }
        return $this->redirect('/admin/pedidos/edit/'.$pedido->id);
        $this->render(false);
    }

}