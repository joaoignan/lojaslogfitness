<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Symfony\Component\Console\Helper\Table;

/**
 * Professores Controller
 *
 * @property \App\Model\Table\ProfessoresTable $Professores
 */
class ProfessoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($status = 'todos', $search = null){

        if($this->request->is(['post', 'put'])){
            $this->redirect(['action' => 'index', $status, $this->request->data['search']]);
        }

        if($search != null){
            $search = [
                'OR' => [
                    'Professores.name         LIKE' => '%'.$search.'%',
                    'Professores.email        LIKE' => '%'.$search.'%',
                    'Professores.cpf          LIKE' => '%'.$search.'%',
                    'Professores.phone        LIKE' => '%'.$search.'%',
                    'Professores.mobile       LIKE' => '%'.$search.'%',
                    'Cities.name            LIKE' => '%'.$search.'%',
                    'Cities.uf              LIKE' => '%'.$search.'%',
                ]
            ];
        }else{
            $search = [];
        }

        if($status == 'todos'){
            $this->paginate = [
                'contain'       => ['Cities', 'Status'],
                'order'         => ['Professores.id' => 'desc'],
                'conditions'    => [$search]
            ];
        }elseif($status == 'ativos'){
            $this->paginate = [
                'contain'       => ['Cities', 'Status'],
                'order'         => ['Professores.id' => 'desc'],
                'conditions'    => ['Professores.status_id' => 1, $search]
            ];
        }elseif($status == 'inativos'){
            $this->paginate = [
                'contain'       => ['Cities', 'Status'],
                'order'         => ['Professores.id' => 'desc'],
                'conditions'    => ['Professores.status_id' => 2, $search]
            ];
        }elseif($status == 'nao_aceitaram'){
            $this->paginate = [
                'contain'       => ['Cities', 'Status'],
                'order'         => ['Professores.id' => 'desc'],
                'conditions'    => ['Professores.status_id' => 3, $search]
            ];
        }


        $professores = $this->paginate($this->Professores);

        $this->set(compact('professores'));
        $this->set('_serialize', ['professores']);
    }

    /**
     * View method
     *
     * @param string|null $id Professore id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $professore = $this->Professores->get($id, [
            'contain' => ['Cities', 'Status','ProfessorAcademias','ProfessorAcademias.Academias'],
        ]);

        $this->set('professore', $professore);
        $this->set('_serialize', ['professore']);
    }

    public function listar_professores(){

        $Users = TableRegistry::get('Users');

        $user = $Users->get($this->Auth->user('id'));

        $Prof_Academia = TableRegistry::get('Professor_Academias');

        $Academias = TableRegistry::get('Academias');

        $academias = $Academias
            ->find('list')
            ->all();

        if($user->group_id != 7) {
            $prof_academia = $Prof_Academia
                ->find('all')
                ->contain(['Academias', 'Professores','Academias.Cities', 'Professores.Cities'])
                ->andWhere(['Professor_Academias.status_id IN' => [1,3]])
                ->order(['Professores.name']);

            $this->paginate = [];

            /**
             * MENSAGEM DE ALERTA PARA PROFESSORES AGUARDANDO ACEITE
             */
            $prof_alert = $Prof_Academia
                ->find('all')
                ->contain(['Academias', 'Professores','Academias.Cities', 'Professores.Cities'])
                ->andWhere(['Professor_Academias.status_id IN' => [1,3]])
                ->order(['Professores.name']);
        } else {
            $prof_academia = $Prof_Academia
                ->find('all')
                ->contain(['Academias', 'Professores','Academias.Cities', 'Professores.Cities'])
                ->andWhere(['Professor_Academias.status_id IN' => [1,3]])
                ->andWhere(['Academias.user_id' => $user->id])
                ->order(['Professores.name']);

            $this->paginate = [];

            /**
             * MENSAGEM DE ALERTA PARA PROFESSORES AGUARDANDO ACEITE
             */
            $prof_alert = $Prof_Academia
                ->find('all')
                ->contain(['Academias', 'Professores','Academias.Cities', 'Professores.Cities'])
                ->andWhere(['Professor_Academias.status_id IN' => [1,3]])
                ->andWhere(['Academias.user_id' => $user->id])
                ->order(['Professores.name']);
        }


        if(count($prof_alert) >= 1){
            $this->Flash->info_laranja('Existe professor pendente de aceite da academia.');
        }

        $prof_academia = $this->paginate($prof_academia);

        // debug($prof_academia);

        $this->set(compact('academias','prof_academia', 'professores','states', 'cities'));


    }

    /**
     * Edit method
     *
     * @param string|null $id Professor id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null){

        $professor = $this->Professores->get($id, [
            'contain' => ['Cities']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $professor = $this->Professores->patchEntity($professor, $this->request->data);
            if ($this->Professores->save($professor)) {
                $this->Flash->success('Professor salvo com sucesso.');
                $this->redirect(['action' => 'edit', $id]);
            } else {
                $this->Flash->error('Falha ao salvar professor. Tente novamente.');
            }
        }
        
        $cities = $this->Professores->Cities->find('list')->where(['Cities.state_id' => $professor->city->state_id]);
        $states = $this->Professores->Cities->States->find('list');
        $status = $this->Professores->Status->find('list');
        $this->set(compact('professor', 'cities', 'states', 'status'));
        $this->set('_serialize', ['professor']);
    }

}