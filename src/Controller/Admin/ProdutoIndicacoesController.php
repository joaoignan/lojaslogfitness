<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * PreOrders Controller
 *
 * @property \App\Model\Table\PreOrdersTable $PreOrders
 */
class ProdutoIndicacoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'order' => ['ProdutoIndicacoes.id' => 'desc']
        ];
        $indic_prod = $this->paginate($this->ProdutoIndicacoes);

        $qtd_prod = $this->ProdutoIndicacoes
        	->find('all')
        	->all();

        $qtd_prod = count($qtd_prod);

        $qtd_aceitos = $this->ProdutoIndicacoes
        	->find('all')
        	->where(['ProdutoIndicacoes.aceitou' => 1])
        	->all();

        $qtd_aceitos = count($qtd_aceitos);

        $this->set(compact('indic_prod', 'qtd_prod', 'qtd_aceitos'));
        $this->set('_serialize', ['indic_prod']);
    }
}
