<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * BannersMarcas Controller
 *
 * @property \App\Model\Table\BannersMarcasTable $BannersMarcas
 */
class BannersMarcasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Marcas', 'Status']
        ];
        $bannersMarcas = $this->paginate($this->BannersMarcas);

        $this->set(compact('bannersMarcas'));
        $this->set('_serialize', ['bannersMarcas']);
    }

    /**
     * View method
     *
     * @param string|null $id Banners Marca id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bannersMarca = $this->BannersMarcas->get($id, [
            'contain' => ['Marcas', 'Status']
        ]);

        $this->set('bannersMarca', $bannersMarca);
        $this->set('_serialize', ['bannersMarca']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bannersMarca = $this->BannersMarcas->newEntity();
        if ($this->request->is('post')) {
            $bannersMarca = $this->BannersMarcas->patchEntity($bannersMarca, $this->request->data);
            if ($this->BannersMarcas->save($bannersMarca)) {
                $this->Flash->success(__('The banners marca has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The banners marca could not be saved. Please, try again.'));
            }
        }
        $marcas = $this->BannersMarcas->Marcas->find('list', ['limit' => 200]);
        $status = $this->BannersMarcas->Status->find('list', ['limit' => 200]);
        $this->set(compact('bannersMarca', 'marcas', 'status'));
        $this->set('_serialize', ['bannersMarca']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Banners Marca id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bannersMarca = $this->BannersMarcas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bannersMarca = $this->BannersMarcas->patchEntity($bannersMarca, $this->request->data);
            if ($this->BannersMarcas->save($bannersMarca)) {
                $this->Flash->success(__('The banners marca has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The banners marca could not be saved. Please, try again.'));
            }
        }
        $marcas = $this->BannersMarcas->Marcas->find('list', ['limit' => 200]);
        $status = $this->BannersMarcas->Status->find('list', ['limit' => 200]);
        $this->set(compact('bannersMarca', 'marcas', 'status'));
        $this->set('_serialize', ['bannersMarca']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Banners Marca id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bannersMarca = $this->BannersMarcas->get($id);
        if ($this->BannersMarcas->delete($bannersMarca)) {
            $this->Flash->success(__('The banners marca has been deleted.'));
        } else {
            $this->Flash->error(__('The banners marca could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
