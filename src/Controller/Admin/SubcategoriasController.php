<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Subcategorias Controller
 *
 * @property \App\Model\Table\CategoriasTable $Categorias
 */
class SubcategoriasController extends AppController
{

    public function sincronizar_objetivos() {
        $todos_objetivos = TableRegistry::get('Objetivos')
            ->find('all')
            ->all();

        foreach ($todos_objetivos as $objetivo) {
            $objetivos_ids_array[] = $objetivo->id;
        } 

        do {
            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $data = [
                "objetivos_ids_array" => $objetivos_ids_array
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.logfitness.com.br/objetivos/sync?api_key=".LOGFITNESS_API_KEY,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_SSL_VERIFYPEER => false
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $api_novos_objetivos = json_decode($response);

            if($api_novos_objetivos->status_id == 1 && $api_novos_objetivos->retorno_cod == 'sucesso') {
                $falha = 0;

                foreach ($api_novos_objetivos->objetivos as $objetivo) {

                    $new_objetivo_data = [
                        'id' => $objetivo->id,
                        'name' => $objetivo->name,
                        'slug' => $objetivo->slug,
                        'texto_tarja' => $objetivo->texto_tarja,
                        'image' => $objetivo->image,
                        'color' => $objetivo->color,
                        'banner_lateral' => $objetivo->banner_lateral,
                        'banner_fundo' => $objetivo->banner_fundo,
                        'status_id' => $objetivo->status_id
                    ];

                    $new_objetivo = TableRegistry::get('Objetivos')->newEntity($new_objetivo_data);
                    
                    if(!TableRegistry::get('Objetivos')->save($new_objetivo)) {
                        $falha = 1;
                    }
                }

                if($falha == 0) {
                    $this->Flash->success('Objetivos sincronizados com sucesso!');
                } else {
                    $this->Flash->error('Falha ao atualizar banco de dados, atualize a página!');
                }
            }
        } while($api_novos_objetivos->status_id < 1);
    }

    public function sincronizar_categorias() {
        $this->sincronizar_objetivos();

        $todas_categorias = TableRegistry::get('Categorias')
            ->find('all')
            ->all();

        foreach ($todas_categorias as $categoria) {
            $categorias_ids_array[] = $categoria->id;
        } 

        do {
            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $data = [
                "categorias_ids_array" => $categorias_ids_array
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.logfitness.com.br/categorias/sync?api_key=".LOGFITNESS_API_KEY,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_SSL_VERIFYPEER => false
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $api_novas_categorias = json_decode($response);

            if($api_novas_categorias->status_id == 1 && $api_novas_categorias->retorno_cod == 'sucesso') {
                $falha = 0;

                foreach ($api_novas_categorias->categorias as $categoria) {

                    $new_categoria_data = [
                        'id' => $categoria->id,
                        'objetivo_id' => $categoria->objetivo_id,
                        'cod_interno_classe' => $categoria->cod_interno_classe,
                        'cod_interno_cat' => $categoria->cod_interno_cat,
                        'name' => $categoria->name,
                        'descricao' => $categoria->descricao,
                        'slug' => $categoria->slug,
                        'banner_lateral' => $categoria->banner_lateral,
                        'banner_fundo' => $categoria->banner_fundo,
                        'status_id' => $categoria->status_id
                    ];

                    $new_categoria = TableRegistry::get('Categorias')->newEntity($new_categoria_data);
                    
                    if(!TableRegistry::get('Categorias')->save($new_categoria)) {
                        $falha = 1;
                    }
                }

                if($falha == 0) {
                    $this->Flash->success('Categorias sincronizadas com sucesso!');
                } else {
                    $this->Flash->error('Falha ao atualizar banco de dados, atualize a página!');
                }
            }
        } while($api_novas_categorias->status_id < 1);
    }

    public function sincronizar_subcategorias() {
        $this->sincronizar_categorias();

        $todas_subcategorias = $this->Subcategorias
            ->find('all')
            ->all();

        foreach ($todas_subcategorias as $subcategoria) {
            $subcategorias_ids_array[] = $subcategoria->id;
        } 

        do {
            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $data = [
                "subcategorias_ids_array" => $subcategorias_ids_array
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.logfitness.com.br/subcategorias/sync?api_key=".LOGFITNESS_API_KEY,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_SSL_VERIFYPEER => false
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $api_novas_subcategorias = json_decode($response);

            if($api_novas_subcategorias->status_id == 1 && $api_novas_subcategorias->retorno_cod == 'sucesso') {
                $falha = 0;

                foreach ($api_novas_subcategorias->subcategorias as $subcategoria) {

                    $new_subcategoria_data = [
                        'id' => $subcategoria->id,
                        'categoria_id' => $subcategoria->categoria_id,
                        'name' => $subcategoria->name,
                        'descricao' => $subcategoria->descricao,
                        'slug' => $subcategoria->slug,
                        'banner_lateral' => $subcategoria->banner_lateral,
                        'banner_fundo' => $subcategoria->banner_fundo,
                        'status_id' => $subcategoria->status_id
                    ];

                    $new_subcategoria = $this->Subcategorias->newEntity($new_subcategoria_data);
                    
                    if(!$this->Subcategorias->save($new_subcategoria)) {
                        $falha = 1;
                    }
                }

                if($falha == 0) {
                    $this->Flash->success('Subcategorias sincronizadas com sucesso!');
                } else {
                    $this->Flash->error('Falha ao atualizar banco de dados, atualize a página!');
                }
            }
        } while($api_novas_subcategorias->status_id < 1);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->sincronizar_subcategorias();

        $this->paginate = [
            'contain' => ['Status', 'Categorias', 'Categorias.Objetivos']
        ];
        $subcategorias = $this->paginate($this->Subcategorias);

        $this->set(compact('subcategorias'));
        $this->set('_serialize', ['subcategorias']);
    }

    /**
     * View method
     *
     * @param string|null $id Categoria id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $subcategoria = $this->Subcategorias->get($id, [
            'contain' => ['Status', 'Categorias', 'Categorias.Objetivos']
        ]);

        $this->set('subcategoria', $subcategoria);
        $this->set('_serialize', ['subcategoria']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->sincronizar_subcategorias();

        if ($this->request->is('post')) {

            $banner_lateral = $this->Subcategorias->newUploadImageBannerLateral($this->request->data['banner_lateral'], $this->request->data['name'], $this->request->data['banner_lateral_cropx'], $this->request->data['banner_lateral_cropy'], $this->request->data['banner_lateral_cropw'], $this->request->data['banner_lateral_croph']);
            
            $banner_fundo = $this->Subcategorias->newUploadImageBannerFundo($this->request->data['banner_fundo'], $this->request->data['name'], $this->request->data['banner_fundo_cropx'], $this->request->data['banner_fundo_cropy'], $this->request->data['banner_fundo_cropw'], $this->request->data['banner_fundo_croph']);

            $count = 0;

            do {
                //Add subcategoria no BD da API LogFitness
                $ch = curl_init();

                $header = [
                    "Content-Type: application/json"
                ];

                $data = [
                    "name" => $this->request->data['name'],
                    "categoria_id" => $this->request->data['categoria_id'],
                    "banner_lateral" => $banner_lateral,
                    "banner_fundo" => $banner_fundo
                ];

                $data = json_encode($data);

                $options = array(CURLOPT_URL => "https://api.logfitness.com.br/subcategorias/add?api_key=".LOGFITNESS_API_KEY,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $header,
                    CURLOPT_SSL_VERIFYPEER => false
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $api_nova_subcategoria = json_decode($response);

                if($api_nova_subcategoria->status_id == 2) {
                    $this->Flash->error($api_nova_subcategoria->mensagem);
                    return $this->redirect(['action' => 'add']);
                }

                if($api_nova_subcategoria->status_id == 1) {
                    $this->Flash->success($api_nova_subcategoria->mensagem);
                    return $this->redirect(['action' => 'index']);
                }

                $count++;
            } while($api_nova_subcategoria->status_id < 1 && $count < 100);
        }
        
        $objetivos = $this->Subcategorias->Categorias->Objetivos->find('list', ['limit' => 200]);
        $this->set(compact('objetivos'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Categoria id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $subcategoria = $this->Subcategorias->get($id, [
            'contain' => ['Status', 'Categorias']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $subcategoria = $this->Subcategorias->patchEntity($subcategoria, $this->request->data);
            if ($this->Subcategorias->save($subcategoria)) {
                $this->Flash->success(__('The subcategoria has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The subcategoria could not be saved. Please, try again.'));
            }
        }
        $status = $this->Subcategorias->Status->find('list', ['limit' => 200]);
        $objetivos = $this->Subcategorias->Categorias->Objetivos->find('list', ['limit' => 200]);
        $categorias = $this->Subcategorias->Categorias->find('list', ['limit' => 200])->where(['Categorias.objetivo_id' => $subcategoria->categoria->objetivo_id]);
        $this->set(compact('subcategoria', 'status', 'objetivos', 'categorias'));
        $this->set('_serialize', ['subcategoria']);
    }

    public function busca_categorias($id = null)
    {
        if($id != null) {
            $this->set('categorias', $this->Subcategorias->Categorias
                ->find('list')
                ->where(['Categorias.objetivo_id' => $id])
                ->order(['Categorias.name' => 'asc'])
                ->all()
                ->toArray()
            );
        }
    }
}
