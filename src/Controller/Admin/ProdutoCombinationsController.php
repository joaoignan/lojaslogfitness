<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * ProdutoCombinations Controller
 *
 * @property \App\Model\Table\ProdutoCombinationsTable $ProdutoCombinations
 */
class ProdutoCombinationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($search_query = null)
    {


//        ////////////$ProdutoBase = $this->ProdutoCombinations->ProdutoBase;
//        $ProdutoBase = TableRegistry::get('ProdutoBase');
//
//        $produtoCombinations = $this->paginate($ProdutoBase);
//
//        $this->set(compact('produtoCombinations'));
//        /////////////$this->set('_serialize', ['produtoCombinations']);


        /////////////////////////// ORIGINAL ACIMA////////////////////////////////////
        $ProdutoBase = TableRegistry::get('ProdutoBase');

        $produtoCombinations = $this->paginate($ProdutoBase);

        $this->set(compact('produtoCombinations'));

        if($this->request->is(['post', 'put'])){
            $this->redirect(['action' => 'index', $this->request->data['search']]);
        }

        if($search_query != null){

            $objetivos = TableRegistry::get('Objetivos')
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$search_query.'%'])
                ->toArray();

            $categorias = TableRegistry::get('Categorias')
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$search_query.'%'])
                ->toArray();

            if(count($objetivos) <= 0){
                $objetivos = [0];
            }

            if(count($categorias) <= 0){
                $categorias = [0];
            }

            $search = [
                'OR' => [
                    'Marcas.name              LIKE'     => '%'.$search_query.'%',
                    'ProdutoBase.name         LIKE'     => '%'.$search_query.'%',
                    'Produtos.propriedade     LIKE'     => '%'.$search_query.'%',
                    'ProdutoObjetivos.objetivo_id IN'   => $objetivos,
                    'ProdutoCategorias.categoria_id IN' => $categorias,
                ]
            ];
        }else{
            $search = [];
        }



        $produtos_find = TableRegistry::get('Produtos')
            ->find('all')
            ->contain([
                'Status',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoCategorias',
            ])
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->distinct('Produtos.id')
            ->where($search);


        $produtoCombinations = $this->paginate($produtos_find);

        $this->set(compact('produtoCombinations'));


    }

    /**
     * View method
     *
     * @param string|null $id Produto Combination id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $produtoCombination = $this->ProdutoCombinations->get($id, [
            'contain' => ['ProdutoBase']
        ]);

        $this->set('produtoCombination', $produtoCombination);
        $this->set('_serialize', ['produtoCombination']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($id = null){
        if($id == null) {
            $this->Flash->warning(__('Identificador de produto inválido...'));
            return $this->redirect(['action' => 'index']);
        }else{
            $produto = $this->ProdutoCombinations->ProdutoBase->get($id);

            if($produto == null){
                $this->Flash->warning(__('Produto inexistente...'));
                return $this->redirect(['action' => 'index']);
            }else {

                $produtoCombination = $this->ProdutoCombinations->newEntity();
                if ($this->request->is('post')) {

                    $combinations = $this->ProdutoCombinations
                        ->find('all')
                        ->where(['produto_base1_id' => $id])
                        ->all();

                    $combinations_array = [];
                    foreach($combinations as $combination){
                        $combinations_array[] = $combination->produto_base2_id;
                    }

                    $this->request->data['produto_base1_id'] = $id;
                    //debug($this->request->data);
                    $messages = [];

                    if(empty($this->request->data['produto_base2_id'])){

                    }else {
                        $combina_array = [];
                        foreach ($this->request->data['produto_base2_id'] as $combina) {
                            $combina_array[] = $combina;

                            if (!in_array($combina, $combinations_array)) {
                                $this->request->data['produto_base2_id'] = $combina;

                                $produtoCombination = $this->ProdutoCombinations->newEntity();
                                $produtoCombination = $this->ProdutoCombinations->patchEntity($produtoCombination, $this->request->data);
                                if ($this->ProdutoCombinations->save($produtoCombination)) {
                                    $messages['success'][] = $combina;
                                } else {
                                    $messages['error'][] = $combina;
                                }
                            }
                        }

                        $combina_delete = array_diff($combinations_array, $combina_array);

                        foreach($combina_delete as $delete){
                            $produtoCombinationDelete = $this->ProdutoCombinations
                                ->find('all')
                                ->where(['produto_base1_id'     => $id])
                                ->andWhere(['produto_base2_id'  => $delete])
                                ->first();

                            if ($this->ProdutoCombinations->delete($produtoCombinationDelete)) {
                                $messages['deleted'][] = $delete;
                            }
                        }
                    }
                }

                $produtos = $this->ProdutoCombinations->ProdutoBase
                    ->find('list');
                    //->where(['id <>' => $produto->id]);
                $this->set(compact('produtoCombination', 'produtos', 'produto'));
                $this->set('_serialize', ['produtoCombination']);
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Produto Combination id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $produtoCombination = $this->ProdutoCombinations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $produtoCombination = $this->ProdutoCombinations->patchEntity($produtoCombination, $this->request->data);
            if ($this->ProdutoCombinations->save($produtoCombination)) {
                $this->Flash->success(__('The produto combination has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The produto combination could not be saved. Please, try again.'));
            }
        }
        $produtos = $this->ProdutoCombinations->Produtos->find('list', ['limit' => 200]);
        $produto2s = $this->ProdutoCombinations->Produto2s->find('list', ['limit' => 200]);
        $this->set(compact('produtoCombination', 'produtos', 'produto2s'));
        $this->set('_serialize', ['produtoCombination']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Produto Combination id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $produtoCombination = $this->ProdutoCombinations->get($id);
        if ($this->ProdutoCombinations->delete($produtoCombination)) {
            $this->Flash->success(__('The produto combination has been deleted.'));
        } else {
            $this->Flash->error(__('The produto combination could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
