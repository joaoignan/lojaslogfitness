<?php

namespace App\Controller;

use Cake\Cache\Cache;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
//use Phinx\Db\Table;
use ReflectionClass;
use ReflectionMethod;
use Cake\Utility\Xml;
use SimpleXMLElement;


class AppController extends Controller {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller'=> 'Pages',
                'action'    => 'index',
            ],
            'logoutRedirect' => [
                'controller'=> 'Users',
                'action'    => 'login',
            ]
        ]);

        define('WEBROOT_URL',
            //$_SERVER['REQUEST_SCHEME'].'://'.
            'https://'.
            $_SERVER['HTTP_HOST'].
            str_replace('webroot/index.php', '', $_SERVER['PHP_SELF']));

        define('IMG_URL_API',               'https://api.logfitness.com.br/img'.DS);
        define('IMG_URL',                   WEBROOT_URL.'img'.DS);
        define('JS_URL',                    WEBROOT_URL.'js'.DS);
        define('CSS_URL',                   WEBROOT_URL.'css'.DS);
        define('FONT_URL',                  WEBROOT_URL.'fonts'.DS);

        //Biblioteca WideImage
        define('WIDE_IMAGE',                ROOT . DS . 'plugins' . DS . 'WideImage' . DS . 'WideImage.php');

        require_once ROOT . DS . 'plugins' . DS . 'PagSeguroLibrary' . DS . 'PagSeguroLibrary.php';

        define('DOM_PDF',                   ROOT . DS . 'plugins' . DS . 'dompdf' . DS . 'dompdf_config.inc.php');

        define('WI_RESIZE',                 WEBROOT_URL.'Wi'.DS.'resize'.DS);
        define('WI_RESIZE_CROP',            WEBROOT_URL.'Wi'.DS.'resize_crop'.DS);
        define('WI_RESIZE_CROP_SMOOTH',     WEBROOT_URL.'Wi'.DS.'resize_crop_smooth'.DS);


    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function beforeFilter(Event $event)
    {
        /**
         * DEBUG VIA URL
         */
        if(((isset($_GET['debug']) && $_GET['debug'] == 0))):
            $debugar = false;
        elseif(((isset($_GET['debug']) && $_GET['debug'] == 1) || (isset($_COOKIE['debugar']) && $_COOKIE['debugar'] == 1))):
            $debugar = true;
        else:
            $debugar = false;
        endif;

        setcookie('debugar', $debugar, time() + (1440 * 1), '/' );
        $_COOKIE['debugar'] = $debugar;

        Configure::write('debug', filter_var(env('DEBUG', $debugar), FILTER_VALIDATE_BOOLEAN));


        debug('MODO DEBUG ATIVADO');

        $badges = [];

        define('FULL_URL',
            //$_SERVER['REQUEST_SCHEME'].
            'https://'.
            $_SERVER['HTTP_HOST'].'://'.$_SERVER['SERVER_NAME'].str_replace('webroot/index.php', '', $_SERVER['PHP_SELF'])
        );

        $this->Auth->allow(['display', 'login', 'logout', 'forgot_passwd', 'recover_passwd']);

        $controller = $this->request->params['controller'];
        $action     = $this->request->params['action'];

        $session    = $this->request->session();

        $this->set('ControllerName', $this->name);
        $this->set('ActionName',     $action);

        $select_academias_list = TableRegistry::get('Academias')
            ->find('all')
            ->contain(['Cities', 'Cities.States'])
            ->where(['Academias.status_id' => 1])
            ->andWhere(['Academias.master_loja' => 0])
            ->all();

        $this->set(compact('select_academias_list'));

        $session_produto = $session->read('Carrinho');

        $session_produto_ids = [];
        if(!empty($session_produto)) {
            foreach ($session_produto as $key => $item) {
                $session_produto_ids[] = $key;
            }
        }

        if(!empty($session_produto_ids)) {
            $s_produtos = TableRegistry::get('Produtos')
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->where(['Produtos.id IN' => $session_produto_ids])
                ->all();
        }else{
            $s_produtos = [];
        }

        $this->set(compact('session', 's_produtos', 'session_produto', 'session_produto_ids'));

        //Comparador
        $ComparadorProdutos = $session->read('Comparador');

        $qtd_comparador = 0;

        foreach ($ComparadorProdutos as $key => $value) {
            $comparador_ids[] = $key;
        }

        if(count($comparador_ids) >= 1) {
            $Produtos = TableRegistry::get('Produtos');

            $produtos_comparador = $Produtos
                ->find('all')
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias',
                    'ProdutoSubstancias',
                    'ProdutoSubstancias.Substancias',
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoSubstancias' => 'produto_substancias'],
                    ['ProdutoSubstancias.produto_id = Produtos.id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Produtos.visivel' => 1])
                ->andWhere(['Produtos.id IN' => $comparador_ids])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('Produtos.id')
                ->all();

            $qtd_comparador = count($produtos_comparador);
        }

        $this->set(compact('produtos_comparador', 'qtd_comparador'));

        define('BROWSER_URL',
            //$_SERVER['REQUEST_SCHEME'].
            'https://'.
            $_SERVER['HTTP_HOST'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);

        /**
         * ADMIN
         */
        if (isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin') {
            $this->Auth->deny();
            $this->Auth->allow(['display', 'login', 'logout', 'forgot_passwd', 'recover_passwd']);

            if($controller == 'Schedules' && $action == 'call'){
                $this->Auth->allow();
            }

            $check = $this->Auth->user('id');
            if(isset($check) && $check != null) {
                //############################################################################################################//
                //Camada de Controle de Acesso - CCA
                $CCA = TableRegistry::get('Cca');
                $prefix = 'admin';

                $group_id = $this->Auth->user('group_id');

                $cca = $CCA->cca($prefix, $controller, $action, $group_id, false);
                if ($group_id != 1) {
                    if ($cca == false) {
                        $this->Flash->warning(__('Você não tem permissão para acessar esta área.'));
                        $this->redirect(['controller' => 'Pages', 'action' => 'index']);
                    }
                }

                //VARIAVEIS
                $this->set('cca', $CCA);
                $this->set('group_id', $group_id);
                //$this->set('auth', $this->Auth);
                $this->set('user_image', $this->Auth->user('image'));
                $this->set('user_name', $this->Auth->user('name'));
                $this->set('user_id', $this->Auth->user('id'));
                $this->set('group_name', TableRegistry::get('Groups')->get($this->Auth->user('group_id'))->name);

            }
            //############################################################################################################//
            /**
             * CLIENTE NO SITE
             */
        } else {
            if($this->request->is('mobile')){
                //$this->viewBuilder()->layout('mobile');
            }

            /**
             * SLUG ACADEMIA - VERIFICAR SE O ACESSO É FEITO POR MEIO DA URL PERSONALIZADA DA ACADEMIA
             */
            isset($this->request->params['slug']) ? $Slug = $this->request->params['slug'] : $Slug = false;

            $UsuarioNavegacao = $session->read('UsuarioNavegacao');

            if(!$UsuarioNavegacao) {
                function randString($size){
                    $basic = '0123456789abcdefghijklmnopqrstuvwxyz';

                    $return= "";

                    for($count= 0; $size > $count; $count++){
                        $return.= $basic[rand(0, strlen($basic) - 1)];
                    }

                    return $return;
                }

                $usuario_id = randString(8);

                $session->write('UsuarioNavegacao', $usuario_id);
            }

            $combo_tipos = TableRegistry::get('ComboTipos')
                ->find('all')
                ->all();

            foreach ($combo_tipos as $combo_tipo) {

                $Categorias = TableRegistry::get('Categorias');
                $Subcategorias = TableRegistry::get('Subcategorias');
                $ProdutoCategorias = TableRegistry::get('ProdutoCategorias');
                $ProdutoSubcategorias = TableRegistry::get('ProdutoSubcategorias');
                $ProdutosCombo = TableRegistry::get('Produtos');

                $categoria1 = explode('-', $combo_tipo->categoria_id1);
                $categoria2 = explode('-', $combo_tipo->categoria_id2);
                $categoria3 = explode('-', $combo_tipo->categoria_id3);

                if($categoria1[0] == 'c') {
                    $primeira_categoria = $Categorias
                        ->find('all')
                        ->where(['id' => $categoria1[1]])
                        ->first();

                    $contaProdutos1 = $ProdutoCategorias
                        ->find('all')
                        ->where(['categoria_id' => $categoria1[1]])
                        ->all();

                    $totalitem1 = 0;
                    foreach ($contaProdutos1 as $contaProduto1) {
                        $contaEstoque = $ProdutosCombo
                            ->find('all')
                            ->where(['produto_base_id' => $contaProduto1->produto_base_id])
                            ->andWhere(['estoque >=' => 1])
                            ->all();

                            foreach ($contaEstoque as $item) {
                                $totalitem1 += intval($item->estoque);
                                // echo $item->estoque."-";
                            }
                        // print_r($contaEstoque);
                    }
                    // print_r($totalitem1);

                } else {
                    $primeira_categoria = $Subcategorias
                        ->find('all')
                        ->where(['id' => $categoria1[1]])
                        ->first();

                    $contaProdutos1 = $ProdutoSubcategorias
                        ->find('all')
                        ->where(['subcategoria_id' => $categoria1[1]])
                        ->all();

                    $totalitem1 = 0;
                    foreach ($contaProdutos1 as $contaProduto1) {
                        $contaEstoque = $ProdutosCombo
                            ->find('all')
                            ->where(['produto_base_id' => $contaProduto1->produto_base_id])
                            ->andWhere(['estoque >=' => 1])
                            ->all();

                            foreach ($contaEstoque as $item) {
                                $totalitem1 += intval($item->estoque);
                            }
                    }
                }

                if($categoria2[0] == 'c') {
                    $segunda_categoria = $Categorias
                        ->find('all')
                        ->where(['id' => $categoria2[1]])
                        ->first();

                    $contaProdutos2 = $ProdutoCategorias
                        ->find('all')
                        ->where(['categoria_id' => $categoria2[1]])
                        ->all();

                    $totalitem2 = 0;
                    foreach ($contaProdutos2 as $contaProduto2) {
                        $contaEstoque2 = $ProdutosCombo
                            ->find('all')
                            ->where(['produto_base_id' => $contaProduto2->produto_base_id])
                            ->andWhere(['estoque >=' => 1])
                            ->all();

                        foreach ($contaEstoque2 as $item2) {
                            $totalitem2 += intval($item2->estoque);
                        }
                    }

                } else {
                    $segunda_categoria = $Subcategorias
                        ->find('all')
                        ->where(['id' => $categoria2[1]])
                        ->first();

                    $contaProdutos2 = $ProdutoSubcategorias
                        ->find('all')
                        ->where(['subcategoria_id' => $categoria2[1]])
                        ->all();

                    $totalitem2 = 0;
                    foreach ($contaProdutos2 as $contaProduto2) {
                        $contaEstoque2 = $ProdutosCombo
                            ->find('all')
                            ->where(['produto_base_id' => $contaProduto2->produto_base_id])
                            ->andWhere(['estoque >=' => 1])
                            ->all();

                            foreach ($contaEstoque2 as $item2) {
                                $totalitem2 += intval($item2->estoque);
                            }
                    }
                }

                if($categoria3[0] == 'c') {
                    $terceira_categoria = $Categorias
                        ->find('all')
                        ->where(['id' => $categoria3[1]])
                        ->first();

                    $contaProdutos3 = $ProdutoCategorias
                        ->find('all')
                        ->where(['categoria_id' => $categoria3[1]])
                        ->all();

                    $totalitem3 = 0;
                    foreach ($contaProdutos3 as $contaProduto3) {
                        $contaEstoque3 = $ProdutosCombo
                            ->find('all')
                            ->where(['produto_base_id' => $contaProduto3->produto_base_id])
                            ->andWhere(['estoque >=' => 1])
                            ->all();

                            foreach ($contaEstoque3 as $item3) {
                                $totalitem3 += intval($item2->estoque);
                            }
                    }

                } else {
                    $terceira_categoria = $Subcategorias
                        ->find('all')
                        ->where(['id' => $categoria3[1]])
                        ->first();

                    $contaProdutos3 = $ProdutoSubcategorias
                        ->find('all')
                        ->where(['subcategoria_id' => $categoria3[1]])
                        ->all();

                    $totalitem3 = 0;
                    foreach ($contaProdutos3 as $contaProduto3) {
                        $contaEstoque3 = $ProdutosCombo
                            ->find('all')
                            ->where(['produto_base_id' => $contaProduto3->produto_base_id])
                            ->andWhere(['estoque >=' => 1])
                            ->all();

                            foreach ($contaEstoque3 as $item3) {
                                $totalitem3 += intval($item2->estoque);
                            }
                    }
                }
                // print_r($totalitem1.' ');
                // print_r($totalitem2.'-');
                // print_r($totalitem3.'/');

                if ($totalitem1 >= 1 && $totalitem2 >= 1 && $totalitem3 >= 1) {
                    $tipo_combos[] = [
                        'id' => $combo_tipo->id,
                        'name' => $combo_tipo->name,
                        'categoria1' => $primeira_categoria->name,
                        'categoria2' => $segunda_categoria->name,
                        'categoria3' => $terceira_categoria->name,
                        'qtd_disponivel_1' => $contaProdutos1
                    ];
                }
                
            }

            $this->set(compact('tipo_combos'));

            $modelo_banner = TableRegistry::get('VariaveisGlobais')
                ->find('all')
                ->Where(['name' => 'tipo_banner'])
                ->first();

            $tipo_banner = $modelo_banner->valor;

            $this->set(compact('tipo_banner'));

            $variavel_desconto = TableRegistry::get('VariaveisGlobais')
                ->find('all')
                ->where(['name' => 'desconto'])
                ->first();

            $desconto_geral = 1 - ($variavel_desconto->valor * 0.01);

            $this->set(compact('desconto_geral'));

            if($session->read('Cliente')){
                $Cliente = $session->read('Cliente');

                if(!$Cliente->cpf || !$Cliente->telephone) {
                    $this->set('cadastro_incompleto', 1);
                } else {
                    $this->set('cadastro_incompleto', 0);
                }
            }

            if($Slug){
                //if(!$Cliente) {
                    $SSlug = '/'.$Slug;

                    $academia_slug = TableRegistry::get('Academias')
                        ->find('all')
                        ->contain(['Cities'])
                        ->where(['Academias.slug' => $Slug])
                        ->andWhere(['Academias.status_id' => 1])
                        ->limit(1)
                        ->first();

                    if(count($academia_slug) <= 0){
                        $this->redirect('/');
                    }else{
                        $academia_slug_cities = TableRegistry::get('Cities')
                            ->find('list')
                            ->where(['Cities.state_id' => $academia_slug->city->state_id]);

                        $professores_slug = TableRegistry::get('ProfessorAcademias')
                            ->find('all')
                            ->contain(['Professores'])
                            ->where(['ProfessorAcademias.academia_id' => $academia_slug->id])
                            ->andWhere(['Professores.status_id' => 1])
                            ->andWhere(['ProfessorAcademias.status_id' => 1])
                            ->all();

                        $disable_modal_login = true;
                        $ACADEMIA = $academia_slug;

                        $this->set(compact('academia_slug', 'academia_slug_cities', 'professores_slug', 'disable_modal_login', 'ACADEMIA'));
                        define('ACADEMIA', $ACADEMIA);
                        $session->write('Academia', $ACADEMIA);
                    }
                /*} else {
                    $academia_slug = TableRegistry::get('Academias')
                        ->find('all')
                        ->contain(['Cities'])
                        ->where(['Academias.id' => $Cliente->academia_id])
                        ->andWhere(['status_id' => 1])
                        ->limit(1)
                        ->first();

                    if(count($academia_slug) <= 0){
                        $this->redirect(['controller' => $controller, 'action' => $action]);
                    }else{
                        $academia_slug_cities = TableRegistry::get('Cities')
                            ->find('list')
                            ->where(['Cities.state_id' => $academia_slug->city->state_id]);

                        $disable_modal_login = true;
                        $ACADEMIA = $academia_slug;

                        $this->set(compact('academia_slug', 'academia_slug_cities', 'disable_modal_login', 'ACADEMIA'));
                        define('ACADEMIA', $ACADEMIA);
                        $session->write('Academia', $ACADEMIA);
                    }
                }*/
            }else{
                $Slug   = false;
                $SSlug  = false;
                $ACADEMIA = false;
            }

            define('SLUG', $Slug);
            define('SSLUG', $SSlug);

            $this->set(compact('Slug', 'SSlug'));

            if(!$session->started()) {
                $session->start();
            }

            /**
             * LER XML COM DADOS DA PÁGINA ATUAL
             */
            $this->Auth->allow();
            @header('Content-Type: text/html; charset=utf-8');
            $xml = \simplexml_load_file('pages_titles.xml');
            //$xml = Xml::build('pages_titles.xml');
            $xml = json_encode($xml);
            $xml = json_decode($xml, true);

            $xml_array = [];
            foreach($xml['controller'] as $Controller){
                $xml_array[$Controller['name']][$Controller['action']] = $Controller['title'];
            }

            /**
             * DEFINIR TÍTULO DA PÁGINA CONFORME XML CARREGADO
             */
            $page_title = '';
            if(isset($xml_array[$controller][$action]) && !empty($xml_array[$controller][$action])){
                $page_title = ' : '.$xml_array[$controller][$action];
            }
            $this->set('PageTitle', $page_title);


            /**
             * BUSCAR CONFIGURAÇÕES E DEFINIR AS MESMAS NA SESSÃO
             */
            // if(!$session->read('Configurations')){
            $Configurations = TableRegistry::get('Configurations');
            $configurations = $Configurations->find('all')
                ->where(['status_id' => 1])
                ->order(['Configurations.id' => 'desc'])
                ->limit(1)
                ->first();

            $configurations = unserialize($configurations->configuration);

            $session->write('Configurations', $configurations);
            //}
            $this->set('config', $session->read('Configurations'));

            define('IMAGES_URL', $this->request->webroot.'img'.DS);

            if($session->read('Cliente')){
                $this->set('CLIENTE', $session->read('Cliente'));
                $Cliente = $session->read('Cliente');
            }else{
                $academias      = TableRegistry::get('Academias')->find('list');

                $academias_map  = TableRegistry::get('Academias')
                    ->find('all', ['contain' => ['Cities']])
                    ->where(['Academias.status_id' => 1])
                    ->all();
                $states         = TableRegistry::get('States')->find('list');

                $this->set(compact('academias', 'academias_map', 'states'));
            }

            /**
             * ACADEMIA
             */
            if($session->read('Academia')){
                $this->set('academia', $session->read('Academia'));
            }

            /**
             * OBJETIVOS
             */
            $objetivos = TableRegistry::get('Objetivos')
                ->find('all')
                ->where(['status_id' => 1])
                ->all();

            if(count($objetivos) <= 0){
                $objetivos = [];
            }

            $produtos_objetivos = TableRegistry::get('ProdutoObjetivos')
                ->find('list')
                ->all()
                ->toArray();

            $qtd_produtos_objetivos = TableRegistry::get('Produtos')->find();

            $qtd_produtos_objetivos
                ->select([
                    'id' => 'Objetivos.id',
                    'quantidade' => $qtd_produtos_objetivos->func()->count('Produtos.id')
                ])
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->innerJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id']
                )
                ->innerJoin(
                    ['Objetivos' => 'objetivos'],
                    ['Objetivos.id = ProdutoObjetivos.objetivo_id']
                )
                ->where(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->group(['ProdutoObjetivos.objetivo_id'])
                ->all();

            /**
             * CATEGORIAS
             */
            $categorias = TableRegistry::get('Categorias')
                ->find('all')
                ->where(['status_id' => 1])
                ->order(['name' => 'asc'])
                ->all();

            $ProdutoCategorias = TableRegistry::get('ProdutoCategorias');

            $produtos_categorias = [];
            foreach($objetivos as $ob => $objetivo){
                $produtos_categorias[$objetivo->id] = $ProdutoCategorias
                    ->find('list', ['contain' => ['ProdutoBase']])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoCategorias.produto_base_id'])
                    ->innerJoin(
                        ['Produtos' => 'produtos'],
                        ['Produtos.produto_base_id = ProdutoCategorias.produto_base_id'])
                    ->where(['ProdutoObjetivos.objetivo_id' => $objetivo->id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->andWhere(['Produtos.visivel' => 1])
                    //->distinct('ProdutoBase.id')
                    ->all()
                    ->toArray();
            }

            $produtos_categorias_filtro = TableRegistry::get('Produtos')->find();

            $produtos_categorias_filtro
                ->select([
                    'id' => 'Categorias.id',
                    'name' => 'Categorias.name',
                    'slug' => 'Categorias.slug',
                    'objetivo_id' => 'Categorias.objetivo_id',
                    'banner_lateral' => 'Categorias.banner_lateral',
                    'quantidade' => $produtos_categorias_filtro->func()->count('Produtos.id')
                ])
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoCategorias.Categorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->innerJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id']
                )
                ->innerJoin(
                    ['Categorias' => 'categorias'],
                    ['Categorias.id = ProdutoCategorias.categoria_id']
                )
                ->where(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->group(['ProdutoCategorias.categoria_id'])
                ->all();

            /**
             * SUBCATEGORIAS
             */
            $subcategorias = TableRegistry::get('Subcategorias')
                ->find('all')
                ->where(['status_id' => 1])
                ->order(['name' => 'asc'])
                ->all();

            $produtos_subcategorias_filtro = TableRegistry::get('Produtos')->find();

            $produtos_subcategorias_filtro
                ->select([
                    'id' => 'Subcategorias.id',
                    'name' => 'Subcategorias.name',
                    'slug' => 'Subcategorias.slug',
                    'categoria_id' => 'Subcategorias.categoria_id',
                    'banner_lateral' => 'Subcategorias.banner_lateral',
                    'quantidade' => $produtos_subcategorias_filtro->func()->count('Produtos.id')
                ])
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.ProdutoSubcategorias',
                    'ProdutoBase.ProdutoSubcategorias.Subcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->innerJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = Produtos.produto_base_id']
                )
                ->innerJoin(
                    ['Subcategorias' => 'subcategorias'],
                    ['Subcategorias.id = ProdutoSubcategorias.subcategoria_id']
                )
                ->where(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->group(['ProdutoSubcategorias.subcategoria_id'])
                ->all();

            /**
             * MARCAS
             */
            $marcas = TableRegistry::get('Marcas')
                ->find('all')
                ->where(['status_id' => 1])
                ->order(['name' => 'asc'])
                ->all();

            if(count($marcas) <= 0){
                $marcas = [];
            }

            $produtos_marcas_filtro = TableRegistry::get('Produtos')->find();

            $produtos_marcas_filtro
                ->select([
                    'id' => 'Marcas.id',
                    'name' => 'Marcas.name',
                    'slug' => 'Marcas.slug',
                    'image' => 'Marcas.image',
                    'banner_fundo' => 'Marcas.banner_fundo',
                    'quantidade' => $produtos_marcas_filtro->func()->count('Produtos.id')
                ])
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->where(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->andWhere(['Marcas.status_id' => 1])
                ->group(['ProdutoBase.marca_id'])
                ->order(['Marcas.name'])
                ->all();

            /**
             * SETAR VARIAVEIS PARA A VIEW
             */
            $this->set(compact('marcas', 'objetivos', 'categorias', 'subcategorias', 'produtos_objetivos', 'produtos_categorias', 'produtos_marcas_filtro', 'qtd_produtos_objetivos', 'produtos_categorias_filtro', 'produtos_subcategorias_filtro'));

            /***********************************************************************************************************
             * AMIN ACADEMIA
             **********************************************************************************************************/
            if($session->read('AdminAcademia')){

                $AdminAcademia = $session->read('AdminAcademia');

                $this->set('adm_academia', $AdminAcademia);
            }

            /***********************************************************************************************************
             * AMIN PROFESSOR
             **********************************************************************************************************/
            if($session->read('AdminProfessor')){
                $Professor = $session->read('AdminProfessor');

                $this->set('adm_professor', $Professor);
            }
        }

        return parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }
}
