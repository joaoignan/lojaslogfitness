<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Atletas Controller
 *
 * @property \App\Model\Table\AtletasTable $Atletas
 */
class AtletasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index(){

        if($this->request->is(['post', 'put'])){
            $AcademiaSugestoes = TableRegistry::get('AcademiaSugestoes');

            $academia = $AcademiaSugestoes->newEntity();

            $academia = $AcademiaSugestoes->patchEntity($academia, $this->request->data);

            if($AcademiaSugestoes->save($academia)){
                $this->redirect(SSLUG.'/atletas');
                $this->Flash->success('Indicação enviada com sucesso! Em breve entraremos em contato com a academia indicada.');
            }

        }

        $states = $this->Atletas->Cities->States->find('list');

        $disable_modal_login = true;
        $this->set(compact('states', 'disable_modal_login'));
    }

}
