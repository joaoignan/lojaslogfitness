<?php

namespace App\Controller;

use Cake\Core\Plugin;
use WideImage;
use Cake\Core\Configure;

class WiController extends AppController {

    public function resize_crop($w = 1, $h = 1, $image, $m = 'outside'){
        $this->layout = null;
        header("Content-type: image/jpeg");
        include WIDE_IMAGE;


        $image = str_replace("*", DS, $image);
        $image = explode('/', $image);

        if(isset($image[1])){
            $subdir     = $image[0].DS;
            $filename   = $image[1];
        }else{
            $subdir     = '';
            $filename   = $image[0];
        }

        $prefix = 'wi_rsz'.$w.'x'.$h.$m.'__';

        $image_url          = IMG_URL.$subdir.$filename;
        $cache_image_url    = IMG_URL.'_cache'.DS.$subdir.$prefix.$filename;
        $image_file         = WWW_ROOT.'img'.DS.$subdir.$filename;
        $cache_image_file   = WWW_ROOT.'img'.DS.'_cache'.DS.$subdir.''.$prefix.$filename;

        //VERIFICAR SE A IMAGEM EXISTE
        if(file_exists($image_file)){
            $extension  = explode('.', $filename);
            $extension  = strtolower(end($extension));
            if(!file_exists($cache_image_file)){

                //SALVAR IMAGEM NO CACHE
                $img = WideImage::load($image_url);
                $img = $img->resize($w, $h, $m);
                $img = $img->crop('center', 'center', $w, $h);
                if($extension == 'jpg' || $extension == 'jpeg'){
                    $img->saveToFile($cache_image_file, 100);
                }elseif(strtolower($extension) == 'png'){
                    $img->saveToFile($cache_image_file, 6);
                }
            }

            $img = WideImage::load($cache_image_url);
            if($extension == 'png'){
                $img->output('png');
            }else{
                $img->output('jpg');
            }
        }else{
            //EXIBIR IMAGEM GENÉRICA NA FALTA DA IMAGEM SOLICITADA
            $image_url = IMG_URL.'img.png';
            $img = WideImage::load($image_url);
            $img = $img->resize($w, $h, $m);
            $img = $img->crop('center', 'center', $w, $h);
            $img->output('png', 6);
        }

    }

    public function resize_crop_smooth($w = 1, $h = 1, $s = 1,$image){
        $this->layout = null;
        header("Content-type: image/jpeg");
        include WIDE_IMAGE;

        $image = str_replace("*", DS, $image);
        $image = explode('/', $image);

        if(isset($image[1])){
            $subdir     = $image[0].DS;
            $filename   = $image[1];
        }else{
            $subdir     = '';
            $filename   = $image[0];
        }

        $prefix = 'wi_rszcrpsmth'.$w.'x'.$h.'__';

        $image_url          = IMG_URL.$subdir.$filename;
        $cache_image_url    = IMG_URL.'_cache'.DS.$subdir.$prefix.$filename;
        $image_file         = WWW_ROOT.'img'.DS.$subdir.$filename;
        $cache_image_file   = WWW_ROOT.'img'.DS.'_cache'.DS.$subdir.''.$prefix.$filename;

        //VERIFICAR SE A IMAGEM EXISTE
        if(file_exists($image_file)){
            $extension  = explode('.', $filename);
            $extension  = strtolower(end($extension));
            if(!file_exists($cache_image_file)){

                //SALVAR IMAGEM NO CACHE
                $img = WideImage::load($image_url);
                $img = $img->resize($w, $h, 'outside');
                $img = $img->crop('center', 'center', $w, $h);
                for($i=1; $i<=$s; $i++){
                    $img = $img->applyFilter(IMG_FILTER_SMOOTH, 0);
                }
                if($extension == 'jpg' || $extension == 'jpeg'){
                    $img->saveToFile($cache_image_file, 98);
                }elseif(strtolower($extension) == 'png'){
                    $img->saveToFile($cache_image_file, 6);
                }
            }

            $img = WideImage::load($cache_image_url);
            if($extension == 'png'){
                $img->output('png');
            }else{
                $img->output('jpg');
            }
        }else{
            //EXIBIR IMAGEM GENÉRICA NA FALTA DA IMAGEM SOLICITADA
            $image_url = IMG_URL.'img.png';
            $img = WideImage::load($image_url);
            $img = $img->resize($w, $h, 'outside');
            $img = $img->crop('center', 'center', $w, $h);
            $img->output('png', 6);
        }

    }

    public function resize($w = 1, $h = 1, $image, $m = 'outside'){
        $this->layout = null;
        header("Content-type: image/jpeg");

        include WIDE_IMAGE;

        $image = str_replace("*", DS, $image);
        $image = explode('/', $image);

        if(isset($image[1])){
            $subdir     = $image[0].DS;
            $filename   = $image[1];
        }else{
            $subdir     = '';
            $filename   = $image[0];
        }

        $prefix = 'wi_rsz'.$w.'x'.$h.$m.'__';

        $image_url          = IMG_URL.$subdir.$filename;
        $cache_image_url    = IMG_URL.'_cache'.DS.$subdir.$prefix.$filename;
        $image_file         = WWW_ROOT.'img'.DS.$subdir.$filename;
        $cache_image_file   = WWW_ROOT.'img'.DS.'_cache'.DS.$subdir.''.$prefix.$filename;

        //VERIFICAR SE A IMAGEM EXISTE
        if(file_exists($image_file)){
            $extension  = explode('.', $filename);
            $extension  = strtolower(end($extension));
            if(!file_exists($cache_image_file)){

                //SALVAR IMAGEM NO CACHE
                $img = WideImage::load($image_url);
                $img = $img->resize($w, $h, $m);
                if($extension == 'jpg' || $extension == 'jpeg'){
                    $img->saveToFile($cache_image_file, 100);
                }elseif(strtolower($extension) == 'png'){
                    $img->saveToFile($cache_image_file, 6);
                }
            }

            //EXIBIR IMAGEM DO CACHE
            /*if($extension == 'jpg' || $extension == 'jpeg'){
                imagejpeg(imagecreatefromjpeg($cache_image_url));
            }elseif(strtolower($extension) == 'png'){
                imagepng(imagecreatefrompng($cache_image_url));
            }*/

            $img = WideImage::load($cache_image_url);
            if($extension == 'png'){
                $img->output('png');
            }else{
                $img->output('jpg');
            }
        }else{
            //EXIBIR IMAGEM GENÉRICA NA FALTA DA IMAGEM SOLICITADA
            $image_url = IMG_URL.'img.png';
            $img = WideImage::load($image_url);
            $img = $img->resize($w, $h, $m);
            $img->output('png', 6);
        }
    }
}