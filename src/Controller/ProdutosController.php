<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Model\Entity\WIUploadTrait;
use Cake\Network\Session;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Exception;
//use Moip;

//use PagarMe;
//use PagarMe_Transaction;


/**
 * Transportadoras Controller
 *
 * @property \App\Model\Table\ProdutosTable $Produtos
 */
class ProdutosController extends AppController
{
    use WIUploadTrait;

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index(){

        $this->paginate = [
            'contain' => ['ProdutoBase', 'Status']
        ];
        $produtos = $this->paginate($this->Produtos);

        $this->set(compact('produtos'));
        $this->set('_serialize', ['produtos']);
    }

    public function beforeRender() {
        $session = $this->request->session();
        $cupom_desconto = $session->read('CupomDesconto');

        $this->set(compact('cupom_desconto'));
    }

    /**
     * View method
     *
     * @param string|null $id Produto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($slug = null){

        $session  = $this->request->session();

        $academia = $session->read('Academia');

        if(!$academia) {
            $url_atual = explode('/produto', $_SERVER['REQUEST_URI']);
            return $this->redirect('/comparador/produto'.$url_atual[1]);
        }

        if(empty($this->request->params['slug'])) {
            $url_atual = explode('/produto', $_SERVER['REQUEST_URI']);
            return $this->redirect('/'.$academia->slug.'/produto'.$url_atual[1]);
        }

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        if($slug == null){
            if($academia) {
                return $this->redirect('/'.$academia->slug);
            } else {
                return $this->redirect('/');
            }
        }

        if(isset($_GET['ind_id']) && $_GET['ind_id'] > 0) {
            $session->write('Indicacao', $_GET['ind_id']);
        }

        $produto = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                    'ProdutoSubstancias',
                    'ProdutoSubstancias.Substancias',
                ]
            ])
            ->where(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.preco > ' => 0.1])
            ->andWhere(['Produtos.slug' => $slug])
            ->first();

        if(count($produto) <= 0) {
            if($academia) {
                return $this->redirect('/'.$academia->slug);
            } else {
                return $this->redirect('/');
            }
        }

        $produtoView = $session->read('produtoView-'.$produto->id);

        if(!$produtoView) {
            if(!strpos($_SERVER['HTTP_USER_AGENT'],"bot")) {
                $produto = $this->Produtos->patchEntity($produto, ['views' => $produto->views+1]);
                if($this->Produtos->save($produto)) {
                    $session->write('produtoView-'.$produto->id, 1);
                }
            }
        }

        $produtos = $this->Produtos
            ->find('all', [
                'contain' => [
                    'Status',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                ]
            ])
            ->where(['Produtos.visivel'            => 1])
            ->andWhere(['Produtos.preco > '        => 0.1])
            ->andWhere(['Produtos.produto_base_id' => $produto->produto_base_id])
            ->andWhere(['Produtos.id <>'           => $produto->id])
            ->all();

        $produto_tamanhos = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                ]
            ])
            ->where(['Produtos.visivel'           => 1])
            ->andWhere(['Produtos.preco > '         => 0.1])
            ->andWhere(['Produtos.produto_base_id'  => $produto->produto_base_id])
            ->andWhere(['Produtos.id <>'            => $produto->id])
            ->distinct(['Produtos.tamanho', 'Produtos.embalagem'])
            ->all();

        $combina_com = unserialize($produto->produto_base->combina);

        if($combina_com != null){

            //separar cada objetivo/categoria/subcategoria do combina_com

            $produto_combinations = $this->Produtos
                ->find('all')
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Produtos.preco > '     => 0.1])
                ->andWhere(['Produtos.status_id' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                //procurar produtos que estão nos objetivos/categorias/subcategorias do combina_com
                ->distinct(['ProdutoBase.id'])
                ->order('rand()')
                ->limit(9)
                ->all();
        }else{

            $produto_objetivos = TableRegistry::get('ProdutoObjetivos')
                ->find('all')
                ->where(['produto_base_id' => $produto->produto_base_id])
                ->all();

            foreach ($produto_objetivos as $produto_objetivo) {
                $objetivos[] = $produto_objetivo->objetivo_id;
            }

            if(count($objetivos) <= 0) {
                $objetivos = [0];
            }

            $produto_categorias = TableRegistry::get('ProdutoCategorias')
                ->find('all')
                ->where(['produto_base_id' => $produto->produto_base_id])
                ->all();

            foreach ($produto_categorias as $produto_categoria) {
                $categoria_produtos = $this->Produtos
                    ->find('all')
                    ->contain([
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->andWhere(['ProdutoCategorias.categoria_id IN' => $produto_categoria->categoria_id])
                    ->all();

                foreach ($categoria_produtos as $categoria_produto) {
                    $produtos_nao_exibir_ids[] = $categoria_produto->id;
                }
            }

            if(count($produtos_nao_exibir_ids) <= 0) {
                $produtos_nao_exibir_ids = [0];
            }

            $produto_subcategorias = TableRegistry::get('ProdutoSubcategorias')
                ->find('all')
                ->where(['produto_base_id' => $produto->produto_base_id])
                ->all();

            foreach ($produto_subcategorias as $produto_subcategoria) {
                $subcategoria_produtos = $this->Produtos
                    ->find('all')
                    ->contain([
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->andWhere(['ProdutoSubcategorias.subcategoria_id IN' => $produto_subcategoria->subcategoria_id])
                    ->all();

                foreach ($subcategoria_produtos as $subcategoria_produto) {
                    $produtos_nao_exibir_ids[] = $subcategoria_produto->id;
                }
            }

            if(count($produtos_nao_exibir_ids) <= 0) {
                $produtos_nao_exibir_ids = [0];
            }

            $produto_combinations = $this->Produtos
                ->find('all')
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Produtos.visivel' => 1])
                ->andWhere(['Produtos.status_id' => 1])
                ->andWhere(['Produtos.id NOT IN' => $produtos_nao_exibir_ids])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivos])
                ->distinct(['ProdutoBase.id'])
                ->order('rand()')
                ->limit(9)
                ->all();
        }

        $session    = $this->request->session();

        $session_produto = $session->read('Carrinho');

        $session_cliente = $session->read('Cliente');

        $session_produto_ids = [];
        if(!empty($session_produto)) {
            foreach ($session_produto as $key => $item) {
                $session_produto_ids[] = $key;
            }
        }

        if(!empty($session_produto_ids)) {
            $s_produtos = $this->Produtos
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->where(['Produtos.id IN' => $session_produto_ids])
                ->all();
        }else{
            $s_produtos = [];
        }

        $prod_objetivos = $produtos;

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();

        $cupom_desconto = $session->read('CupomDesconto');

        $links_breadcrumb[] = [
            'url' => '',
            'name' => $academia->shortname.' - loja'
        ];
        $links_breadcrumb[] = [
            'url' => '/pesquisa',
            'name' => 'Produtos'
        ];
        $links_breadcrumb[] = [
            'url' => '/pesquisa/0/'.$produto->produto_base->marca->slug,
            'name' => $produto->produto_base->marca->name
        ];
        $links_breadcrumb[] = [
            'url' => '/produto/'.$produto->slug,
            'name' => $produto->produto_base->name
        ];

        $this->set(compact('academia', 'cupom_desconto', 'prod_objetivos', 's_produtos', 'session_produto', 'session_cliente', 'produto', 'produtos', 'produto_combinations', 'produto_tamanhos', 'links_breadcrumb'));
        $this->set('_serialize', ['produto']);
    }

    public function atualizar_produtos_express() {

        $session = $this->request->session();

        $p_exibidos = $session->read('ProdutosExibidos');

        $qtd_exibidos = $session->read('QtdProdutosExibidos');

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1); 

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        if($this->request->is(['post', 'put'])){

            $query     = $this->request->data['query'];
            $valor_min = $this->request->data['valor_min'];
            $valor_max = $this->request->data['valor_max'];
            $categoria = $this->request->data['categoria'];
            $marca     = $this->request->data['marca'];
            $objetivo  = $this->request->data['objetivo'];
            $ordenar   = $this->request->data['ordenar'];

            $marca_selected = TableRegistry::get('Marcas')
                ->find('all')
                ->where(['Marcas.id' => $marca])
                ->first();

            $objetivo_selected = TableRegistry::get('Objetivos')
                ->find('all')
                ->where(['Objetivos.id' => $objetivo])
                ->first();

            $categoria_selected = TableRegistry::get('Categorias')
                ->find('all')
                ->where(['Categorias.id' => $categoria])
                ->first();

            if($ordenar == 1) {
                $ordenar = 'desc';
            } else if($ordenar == 2) {
                $ordenar = 'asc';
            } else if($ordenar == 3) {
                $ordenar = 'novidades';
            } else if($ordenar == 4) {
                $ordenar = 'promocoes';
            } else {
                $ordenar = 'rand()';
            }

            if($query != null) {

                if($objetivo == 0 || $categoria == 0){

                    if($marca == 0) {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(12)
                                ->all();
                        }
                    } else {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(12)
                                ->all();
                        }
                    }

                }else{

                    if($ordenar == 'rand()') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.status_id' => 'asc', 'rand()'])
                            ->limit(12)
                            ->all();
                    } else if($ordenar == 'novidades') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.created' => 'desc'])
                            ->limit(12)
                            ->all();
                    } else if($ordenar == 'promocoes') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.preco_promo' => 'desc'])
                            ->limit(12)
                            ->all();
                    } else {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                            ->limit(12)
                            ->all();
                    }
                }

            } else {
                $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

                if($objetivo == 0 || $categoria == 0){
                    if($marca == 0) {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(12)
                                ->all();
                        }
                    } else {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(12)
                                ->all();
                        }
                    }

                }else{

                    if($ordenar == 'rand()') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.status_id' => 'asc', 'rand()'])
                            ->limit(12)
                            ->all();
                    } else if($ordenar == 'novidades') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.created' => 'desc'])
                            ->limit(12)
                            ->all();
                    } else if($ordenar == 'promocoes') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.preco_promo' => 'desc'])
                            ->limit(12)
                            ->all();
                    } else {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                            ->limit(12)
                            ->all();
                    }
                }
            }

            $produtos_exibidos = [];

            foreach ($p_exibidos as $pe) {
                $produtos_exibidos[] = $pe;
            }

            $contador = 0;

            foreach ($produtos as $key) {
                $contador++;
                $produtos_exibidos[] = $key->id;
            }

            $prod_objetivos = $produtos;

            $prod_objetivos = $ProdutoObjetivos
                ->find('all', ['contain' => ['Objetivos']])
                ->all();   

        }else{
            $produtos = [];
        }

        $session->write('QtdProdutosExibidos', $qtd_exibidos+12);

        $session->write('ProdutosExibidos', $produtos_exibidos); 

        $this->set(compact('contador', 'qtd_exibidos', 'marca_selected', 'prod_objetivos', 'max_date_novidade', 'produtos', 'query', 'objetivo_selected', 'categoria_selected'));


        $this->viewBuilder()->layout('ajax');
    }

    public function atualizar_produtos_indicacao() {

        $session = $this->request->session();

        $p_exibidos = $session->read('ProdutosExibidos');

        $qtd_exibidos = $session->read('QtdProdutosExibidos');

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1); 

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        if($this->request->is(['post', 'put'])){

            $query     = $this->request->data['query'];
            $valor_min = $this->request->data['valor_min'];
            $valor_max = $this->request->data['valor_max'];
            $categoria = $this->request->data['categoria'];
            $marca     = $this->request->data['marca'];
            $objetivo  = $this->request->data['objetivo'];
            $ordenar   = $this->request->data['ordenar'];

            $marca_selected = TableRegistry::get('Marcas')
                ->find('all')
                ->where(['Marcas.id' => $marca])
                ->first();

            $objetivo_selected = TableRegistry::get('Objetivos')
                ->find('all')
                ->where(['Objetivos.id' => $objetivo])
                ->first();

            $categoria_selected = TableRegistry::get('Categorias')
                ->find('all')
                ->where(['Categorias.id' => $categoria])
                ->first();

            if($ordenar == 1) {
                $ordenar = 'desc';
            } else if($ordenar == 2) {
                $ordenar = 'asc';
            } else if($ordenar == 3) {
                $ordenar = 'novidades';
            } else if($ordenar == 4) {
                $ordenar = 'promocoes';
            } else {
                $ordenar = 'rand()';
            }

            if($query != null) {

                if($objetivo == 0 || $categoria == 0){

                    if($marca == 0) {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(12)
                                ->all();
                        }
                    } else {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(12)
                                ->all();
                        }
                    }

                }else{

                    if($ordenar == 'rand()') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.status_id' => 'asc', 'rand()'])
                            ->limit(12)
                            ->all();
                    } else if($ordenar == 'novidades') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.created' => 'desc'])
                            ->limit(12)
                            ->all();
                    } else if($ordenar == 'promocoes') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.preco_promo' => 'desc'])
                            ->limit(12)
                            ->all();
                    } else {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                            ->andWhere(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                            ->limit(12)
                            ->all();
                    }
                }

            } else {
                $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

                if($objetivo == 0 || $categoria == 0){
                    if($marca == 0) {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(12)
                                ->all();
                        }
                    } else {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(12)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(12)
                                ->all();
                        }
                    }

                }else{

                    if($ordenar == 'rand()') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.status_id' => 'asc', 'rand()'])
                            ->limit(12)
                            ->all();
                    } else if($ordenar == 'novidades') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.created' => 'desc'])
                            ->limit(12)
                            ->all();
                    } else if($ordenar == 'promocoes') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.preco_promo' => 'desc'])
                            ->limit(12)
                            ->all();
                    } else {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->andWhere(['Produtos.id NOT IN' =>  $p_exibidos])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                            ->limit(12)
                            ->all();
                    }
                }
            }

            $produtos_exibidos = [];

            foreach ($p_exibidos as $pe) {
                $produtos_exibidos[] = $pe;
            }

            $contador = 0;

            foreach ($produtos as $key) {
                $contador++;
                $produtos_exibidos[] = $key->id;
            }

            $prod_objetivos = $produtos;

            $prod_objetivos = $ProdutoObjetivos
                ->find('all', ['contain' => ['Objetivos']])
                ->all();   

        }else{
            $produtos = [];
        }

        $session->write('QtdProdutosExibidos', $qtd_exibidos+12);

        $session->write('ProdutosExibidos', $produtos_exibidos); 

        $this->set(compact('contador', 'qtd_exibidos', 'marca_selected', 'prod_objetivos', 'max_date_novidade', 'produtos', 'query', 'objetivo_selected', 'categoria_selected'));


        $this->viewBuilder()->layout('ajax');
    }

    public function view_indicacao($slug = null){

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        if($slug == null){
            $this->redirect('/');
        }

        $session = $this->request->session();

        $Professor = $session->read('AdminProfessor');
        
        if (!$Professor) {
            $this->redirect('/professores/acesso/');
        }

        $produto = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                    'ProdutoComentarios',
                ]
            ])
            ->where(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.preco > ' => 0.1])
            ->andWhere(['Produtos.slug' => $slug])
            ->first();


        if($produto == null){
            $this->redirect('/');
        }

        $produtos = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                    'ProdutoComentarios',
                ]
            ])
            ->where(['Produtos.visivel'             => 1])
            ->andWhere(['Produtos.preco > '         => 0.1])
            ->andWhere(['Produtos.produto_base_id'  => $produto->produto_base_id])
            ->andWhere(['Produtos.id <>'            => $produto->id])
            ->all();

        $produto_objetivos = TableRegistry::get('ProdutoObjetivos')
            ->find('list')
            ->where(['produto_base_id' => $produto->produto_base_id])
            ->all()
            ->toArray();

        /*$produto_combinations = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'ProdutoBase.ProdutoCombinations',
                    'ProdutoBase',
                ]
            ])
            ->innerJoin(
                ['ProdutoCombinations' => 'produto_combinations'],
                ['ProdutoCombinations.produto_base1_id = Produtos.produto_base_id'])
            ->where(['ProdutoCombinations.produto_base1_id'     => $produto->produto_base_id])
            ->orWhere(['ProdutoCombinations.produto_base2_id'   => $produto->produto_base_id])
            ->limit(4)
            ->all();*/

        if(count($produto_objetivos) >= 1){
            $produto_combinations = $this->Produtos
                ->find('all', [
                    'contain' => [
                        'ProdutoBase.Marcas',
                        'ProdutoBase.Propriedades',
                        'ProdutoBase',
                    ]
                ])
                ->innerJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                ->where(['Produtos.visivel' => 1])
                ->where(['ProdutoObjetivos.objetivo_id IN' => $produto_objetivos])
                ->distinct(['ProdutoBase.id'])
                ->limit(4)
                ->all();
        }else{
            $produto_combinations = [];
        }

        $session    = $this->request->session();

        $session_produto = $session->read('Carrinho');

        $session_cliente = $session->read('Cliente');

        $session_produto_ids = [];
        if(!empty($session_produto)) {
            foreach ($session_produto as $key => $item) {
                $session_produto_ids[] = $key;
            }
        }

        if(!empty($session_produto_ids)) {
            $s_produtos = $this->Produtos
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->where(['Produtos.id IN' => $session_produto_ids])
                ->all();
        }else{
            $s_produtos = [];
        }

        $prod_objetivos = $produtos;

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();

        $this->set(compact('prod_objetivos', 's_produtos', 'session_produto', 'session_cliente', 'produto', 'produtos', 'produto_combinations', 'Professor'));
        $this->set('_serialize', ['produto']);

        $this->viewBuilder()->layout('default5');
    }

    public function view_express($slug = null){

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        if($slug == null){
            $this->redirect('/');
        }

        $session = $this->request->session();

        $Academia = $session->read('AdminAcademia');
        
        if (!$Academia) {
            $this->redirect('/academia/acesso/');
        }

        $produto = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                    'ProdutoComentarios',
                ]
            ])
            ->where(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.preco > ' => 0.1])
            ->andWhere(['Produtos.slug' => $slug])
            ->first();


        if($produto == null){
            $this->redirect('/');
        }

        $produtos = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                    'ProdutoComentarios',
                ]
            ])
            ->where(['Produtos.visivel'           => 1])
            ->andWhere(['Produtos.preco > '         => 0.1])
            ->andWhere(['Produtos.produto_base_id'  => $produto->produto_base_id])
            ->andWhere(['Produtos.id <>'            => $produto->id])
            ->all();

        $produto_objetivos = TableRegistry::get('ProdutoObjetivos')
            ->find('list')
            ->where(['produto_base_id' => $produto->produto_base_id])
            ->all()
            ->toArray();

        /*$produto_combinations = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'ProdutoBase.ProdutoCombinations',
                    'ProdutoBase',
                ]
            ])
            ->innerJoin(
                ['ProdutoCombinations' => 'produto_combinations'],
                ['ProdutoCombinations.produto_base1_id = Produtos.produto_base_id'])
            ->where(['ProdutoCombinations.produto_base1_id'     => $produto->produto_base_id])
            ->orWhere(['ProdutoCombinations.produto_base2_id'   => $produto->produto_base_id])
            ->limit(4)
            ->all();*/

        if(count($produto_objetivos) >= 1){
            $produto_combinations = $this->Produtos
                ->find('all', [
                    'contain' => [
                        'ProdutoBase.Marcas',
                        'ProdutoBase.Propriedades',
                        'ProdutoBase',
                    ]
                ])
                ->innerJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                ->where(['Produtos.visivel' => 1])
                ->where(['ProdutoObjetivos.objetivo_id IN' => $produto_objetivos])
                ->distinct(['ProdutoBase.id'])
                ->limit(4)
                ->all();
        }else{
            $produto_combinations = [];
        }

        $session    = $this->request->session();

        $session_produto = $session->read('Carrinho');

        $session_cliente = $session->read('Cliente');

        $session_produto_ids = [];
        if(!empty($session_produto)) {
            foreach ($session_produto as $key => $item) {
                $session_produto_ids[] = $key;
            }
        }

        if(!empty($session_produto_ids)) {
            $s_produtos = $this->Produtos
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->where(['Produtos.id IN' => $session_produto_ids])
                ->all();
        }else{
            $s_produtos = [];
        }

        $prod_objetivos = $produtos;

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();

        $this->set(compact('prod_objetivos', 's_produtos', 'session_produto', 'session_cliente', 'produto', 'produtos', 'produto_combinations', 'Academia'));
        $this->set('_serialize', ['produto']);

        $this->viewBuilder()->layout('default6');
    }

    /**
     * PROMOÇÃO
     * @param null $slug
     */
    public function view_promo($slug = null){

        if($slug == null){
            $this->redirect('/');
        }else{
            $produto = $this->Produtos
                ->find('all', ['contain' => [
                    'ProdutoBase'
                ]])
                ->where(['Produtos.status_id'   => 1])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->andWhere(['Produtos.slug'     => $slug])
                ->first();

            if($produto){

                $produto_combinations = $this->Produtos
                    ->find('all', [
                        'contain' => [
                            'ProdutoBase.Marcas',
                            'ProdutoBase.Propriedades',
                            'ProdutoBase.ProdutoCombinations',
                            'ProdutoBase',
                        ]
                    ])
                    ->innerJoin(
                        ['ProdutoCombinations' => 'produto_combinations'],
                        ['ProdutoCombinations.produto_base1_id = Produtos.produto_base_id'])
                    ->where(['ProdutoCombinations.produto_base1_id'     => $produto->produto_base_id])
                    ->orWhere(['ProdutoCombinations.produto_base2_id'   => $produto->produto_base_id])
                    ->limit(4)
                    ->all();

                $this->set(compact('produto', 'produto_combinations'));
            }else{
                $this->redirect('/');
            }

        }

    }

    /**
     * BOTAO COMPRAR - ADICIONA ITEM AO CARRINHO
     * @param null $slug
     * @param int $quantidade
     */
    public function comprar($slug = null, $quantidade = 1){

        $produto = $this->Produtos
            ->find('all')
            ->andWhere(['Produtos.preco > ' => 0.1])
            ->where(['Produtos.slug' => $slug])
            ->first();

        if(count($produto) == 0){
            $this->redirect('/');
        }else{

            $session    = $this->request->session();

            $session->start();

            $session_produto = $session->read('Carrinho.'.$produto->id);
            $Academia = $session->read('Academia');

            $SSlug = '/'.$Academia->slug;

            if($session_produto){
                if($session_produto < $produto->estoque) {
                    $quantidade = $session_produto + 1;
                    $session->write('Carrinho.'.$produto->id, $quantidade);
                }
            }else{
                $session->write('Carrinho.'.$produto->id, $quantidade);
            }

            $session->delete('PedidoFinalizado');
            $session->delete('PedidoFinalizadoItem');
            $session->delete('ClienteFinalizado');

            $session    = $this->request->session();

            $session_produto = $session->read('Carrinho');

            $session_cliente = $session->read('Cliente');

            $session_produto_ids = [];
            if(!empty($session_produto)) {
                foreach ($session_produto as $key => $item) {
                    $session_produto_ids[] = $key;
                }
            }

            if(!empty($session_produto_ids)) {
                $s_produtos = $this->Produtos
                    ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                    ->andWhere(['Produtos.preco > ' => 0.1])
                    ->where(['Produtos.id IN' => $session_produto_ids])
                    ->all();
            }else{
                $s_produtos = [];
            }

            $cupom_desconto = $session->read('CupomDesconto');

            $this->set(compact('cupom_desconto', 's_produtos', 'session_produto', 'session_cliente', 'SSlug'));

            $this->viewBuilder()->layout('ajax');

            //$this->redirect('/produto/'.$produto->slug);

        }

    }

    /**
     * BOTAO COMPRAR - ADICIONA ITEM AO CARRINHO EXPRESS
     * @param null $slug
     * @param int $quantidade
     */
    public function comprar_express($slug = null, $quantidade = 1){

        $produto = $this->Produtos
            ->find('all')
            ->andWhere(['Produtos.preco > ' => 0.1])
            ->where(['Produtos.slug' => $slug])
            ->first();

        if(count($produto) == 0){
            $session    = $this->request->session();

            $session->start();

            $session->delete('PedidoFinalizado');
            $session->delete('PedidoFinalizadoItem');
            $session->delete('ClienteFinalizado');

            $session    = $this->request->session();

            $session_produto = $session->read('Carrinho');

            $session_cliente = $session->read('Cliente');

            $session_produto_ids = [];
            if(!empty($session_produto)) {
                foreach ($session_produto as $key => $item) {
                    $session_produto_ids[] = $key;
                }
            }

            if(!empty($session_produto_ids)) {
                $s_produtos = $this->Produtos
                    ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                    ->andWhere(['Produtos.preco > ' => 0.1])
                    ->where(['Produtos.id IN' => $session_produto_ids])
                    ->all();
            }else{
                $s_produtos = [];
            }

            $cupom_desconto = $session->read('CupomDesconto');

            $this->set(compact('cupom_desconto', 's_produtos', 'session_produto', 'session_cliente'));

            $this->viewBuilder()->layout('ajax');
        }else{

            $session    = $this->request->session();

            $session->start();

            $session_produto = $session->read('Carrinho.'.$produto->id);

            if($session_produto){
                if($session_produto < $produto->estoque) {
                    $quantidade = $session_produto + 1;
                    $session->write('Carrinho.'.$produto->id, $quantidade);
                }
            }else{
                $session->write('Carrinho.'.$produto->id, $quantidade);
            }

            $session->delete('PedidoFinalizado');
            $session->delete('PedidoFinalizadoItem');
            $session->delete('ClienteFinalizado');

            $session    = $this->request->session();

            $session_produto = $session->read('Carrinho');

            $session_cliente = $session->read('Cliente');

            $session_produto_ids = [];
            if(!empty($session_produto)) {
                foreach ($session_produto as $key => $item) {
                    $session_produto_ids[] = $key;
                }
            }

            if(!empty($session_produto_ids)) {
                $s_produtos = $this->Produtos
                    ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                    ->andWhere(['Produtos.preco > ' => 0.1])
                    ->where(['Produtos.id IN' => $session_produto_ids])
                    ->all();
            }else{
                $s_produtos = [];
            }

            $cupom_desconto = $session->read('CupomDesconto');

            $this->set(compact('cupom_desconto', 's_produtos', 'session_produto', 'session_cliente'));

            $this->viewBuilder()->layout('ajax');

            //$this->redirect('/produto/'.$produto->slug);

        }

    }

    /**
     * BOTAO COMPRAR - ADICIONA ITEM AO CARRINHO
     * @param null $slug
     * @param int $quantidade
     */
    public function comprar_ind($id = null, $quantidade = 1){
        
        $produto = $this->Produtos
            ->find('all')
            ->andWhere(['Produtos.preco > ' => 0.1])
            ->where(['Produtos.id' => $id])
            ->first();

        if(count($produto) == 0){
            $this->redirect('/');
        }else{

            $session    = $this->request->session();

            $session->start();

            $session_produto = $session->read('Carrinho.'.$produto->id);

            if($session_produto){
                if($session_produto < $produto->estoque) {
                    $quantidade = $session_produto + 1;
                    $session->write('Carrinho.'.$produto->id, $quantidade);
                }
            }else{
                $session->write('Carrinho.'.$produto->id, $quantidade);
            }

            $session->delete('PedidoFinalizado');
            $session->delete('PedidoFinalizadoItem');
            $session->delete('ClienteFinalizado');

            $session    = $this->request->session();

            $session_produto = $session->read('Carrinho');

            $session_cliente = $session->read('Cliente');

            $session_produto_ids = [];
            if(!empty($session_produto)) {
                foreach ($session_produto as $key => $item) {
                    $session_produto_ids[] = $key;
                }
            }

            if(!empty($session_produto_ids)) {
                $s_produtos = $this->Produtos
                    ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                    ->andWhere(['Produtos.preco > ' => 0.1])
                    ->where(['Produtos.id IN' => $session_produto_ids])
                    ->all();
            }else{
                $s_produtos = [];
            }

            $cupom_desconto = $session->read('CupomDesconto');

            $this->set(compact('cupom_desconto', 's_produtos', 'session_produto', 'session_cliente'));

            $this->viewBuilder()->layout('ajax');
        }

    }

    public function comprar_indicar($slug = null, $quantidade = 1){

        $produto = $this->Produtos
            ->find('all')
            ->andWhere(['Produtos.preco > ' => 0.1])
            ->where(['Produtos.slug' => $slug])
            ->first();

        if(count($produto) == 0){
            $this->redirect('/');
        }else{

            $session    = $this->request->session();

            $session->start();

            $session_produto = $session->read('Carrinho.'.$produto->id);

            if($session_produto){
                if($session_produto < $produto->estoque) {
                    $quantidade = $session_produto + 1;
                    $session->write('Carrinho.'.$produto->id, $quantidade);
                }
            }else{
                $session->write('Carrinho.'.$produto->id, $quantidade);
            }

            $session->delete('PedidoFinalizado');
            $session->delete('PedidoFinalizadoItem');
            $session->delete('ClienteFinalizado');

            $session    = $this->request->session();

            $session_produto = $session->read('Carrinho');

            $session_cliente = $session->read('Cliente');

            $session_produto_ids = [];
            if(!empty($session_produto)) {
                foreach ($session_produto as $key => $item) {
                    $session_produto_ids[] = $key;
                }
            }

            if(!empty($session_produto_ids)) {
                $s_produtos = $this->Produtos
                    ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                    ->andWhere(['Produtos.preco > ' => 0.1])
                    ->where(['Produtos.id IN' => $session_produto_ids])
                    ->all();
            }else{
                $s_produtos = [];
            }


            $this->set(compact('s_produtos', 'session_produto', 'session_cliente'));

            $this->viewBuilder()->layout('ajax');

            //$this->redirect('/produto/'.$produto->slug);

        }

    }

    /**
     * CARRINHO
     */
    public function carrinho(){

        $session    = $this->request->session();

        $session_produto = $session->read('Carrinho');

        $session_cliente = $session->read('Cliente');

        $session_produto_ids = [];
        if(!empty($session_produto)) {
            foreach ($session_produto as $key => $item) {
                $session_produto_ids[] = $key;
            }
        }

        if(!empty($session_produto_ids)) {
            $produtos = $this->Produtos
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->where(['Produtos.id IN' => $session_produto_ids])
                ->all();
        }else{
            $produtos = [];
        }

        $this->set(compact('produtos', 'session_produto', 'session_cliente'));
    }

    /**
     * INDICAÇÃO DE PRODUTOS - ADD CARRINHO
     * @param null $pre_order_id
     */
    public function carrinho_indicacao($pre_order_id = null){

        if($pre_order_id != null){
            $session    = $this->request->session();

            $session_produto    = $session->read('Carrinho');
            $cliente            = $session->read('Cliente');

            $PreOrders = TableRegistry::get('PreOrders');

            $pre_orders = $PreOrders
                ->find('all')
                ->contain([
                    'PreOrderItems',
                ])
                ->where(['PreOrders.cliente_id' => $cliente->id])
                ->andWhere(['PreOrders.id' => $pre_order_id])
                ->first();

            $session->delete('Carrinho');

            foreach ($pre_orders->pre_order_items as $item):
                if($item->quantidade > 0):
                    $session->write('Carrinho.'.$item->produto_id, $item->quantidade);
                endif;
            endforeach;

            $session->write('PreOrder', $pre_order_id);
        }

        $session_produto    = $session->read('Carrinho');

        $session_produto_ids = [];
        if(!empty($session_produto)) {
            foreach ($session_produto as $key => $item) {
                $session_produto_ids[] = $key;
            }
        }

        if(!empty($session_produto_ids)) {
            $s_produtos = $this->Produtos
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->where(['Produtos.id IN' => $session_produto_ids])
                ->all();
        }else{
            $s_produtos = [];
        }

        $this->set(compact('s_produtos', 'session_produto', 'session_cliente'));

        $this->render('comprar');
        $this->viewBuilder()->layout('ajax');
    }

    /**
     * RECOLOCAR NO CARRINHO
     * @param null $pedidos_id
     */
    public function recolocar_pedido($pedidos_id = null){

        if($pedidos_id != null){
            $session    = $this->request->session();

            $cliente            = $session->read('Cliente');

            $Pedidos = TableRegistry::get('Pedidos');

            $s_pedidos = $Pedidos
                ->find('all')
                ->contain([
                    'PedidoItens',
                ])
                ->where(['Pedidos.cliente_id' => $cliente->id])
                ->andWhere(['Pedidos.id' => $pedidos_id])
                ->first();

            $session->delete('Carrinho');

            foreach ($s_pedidos->pedido_itens as $item):
                if($item->quantidade > 0):
                    $qtd_produtos_pedido = $qtd_produtos_pedido + 1;
                    $session->write('Carrinho.'.$item->produto_id, $item->quantidade);
                endif;
            endforeach;

            $session->write('Pedido', $pedidos_id);
        }      

        $session_produto    = $session->read('Carrinho');

        $session_produto_ids = [];
        if(!empty($session_produto)) {
            foreach ($session_produto as $key => $item) {
                $session_produto_ids[] = $key;
            }
        }

        $produtos_inativos = TableRegistry::get('Produtos')
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->andWhere(['Produtos.status_id' => 2])
                ->where(['Produtos.id IN' => $session_produto_ids])
                ->all();

        foreach ($produtos_inativos as $pi) {
            $session->delete('Carrinho.'.$pi->id);
        }  

        if(!empty($session_produto_ids)) {
            $s_produtos = $this->Produtos
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->andWhere(['Produtos.status_id' => 1])
                ->where(['Produtos.id IN' => $session_produto_ids])
                ->all();
        }else{
            $s_produtos = [];
        }

        foreach ($s_produtos as $sp):
            $qtd_produtos_ativos = $qtd_produtos_ativos + 1;
        endforeach;

        if($qtd_produtos_pedido != $qtd_produtos_ativos) {
            $alerta_inativo = 1;
        } else {
            $alerta_inativo = 0;
        }

        $session->delete('CupomDesconto');
        $session->delete('PedidoFinalizado');
        $session->delete('PedidoFinalizadoItem');
        $session->delete('ClienteFinalizado');

        $this->set(compact('alerta_inativo', 's_produtos', 'session_produto', 'session_cliente'));

        $this->render('comprar');
        $this->viewBuilder()->layout('ajax');
    }

    /**
     * CHECAR ESTOQUE DO ITEM DO CARRINHO
     * @param null $id
     */
    public function carrinho_estoque($id = null){

        $produto = TableRegistry::get('Produtos')
            ->find('all')
            ->where(['Produtos.id' => $id])
            ->first();
        
        echo $produto->estoque;

        $this->viewBuilder()->layout('ajax');
    }

    /**
     * EDITAR ITENS DO CARRINHO
     * @param null $id
     * @param null $qtd
     */
    public function carrinho_edit($id = null, $qtd = null){

        if($id != null && $qtd != null){
            $session    = $this->request->session();

            $session_produto = $session->read('Carrinho');
            if(isset($session_produto[$id])){
                if($qtd <= 0){
                    $session->delete('Carrinho.'.$id);
                }else{
                    $session->write('Carrinho.'.$id, $qtd);

                    $session->delete('PedidoFinalizado');
                    $session->delete('PedidoFinalizadoItem');
                    $session->delete('ClienteFinalizado');
                }
            }
        }
    }

    /**
     * CRIAR COBRANÇA DIRETA
     */
    public function iugu_cobranca_direta($pedido_id = null) {
        if($pedido_id != null) { 
            if($this->request->is(['post', 'put'])) {
                $Pedidos = TableRegistry::get('Pedidos');

                $variavel_desconto = TableRegistry::get('VariaveisGlobais')
                    ->find('all')
                    ->where(['name' => 'desconto'])
                    ->first();

                $desconto_geral = 1 - ($variavel_desconto->valor * 0.01);

                $PedidoCliente  = $Pedidos
                    ->find('all', ['contain' => [
                        'Academias',
                        'Academias.Cities',
                        'Clientes',
                        'PedidoItens',
                        'PedidoItens.Produtos',
                        'PedidoItens.Produtos.ProdutoBase',
                        'PedidoItens.Produtos.ProdutoBase.Marcas'
                    ]
                    ])
                    ->where(['Pedidos.id' => $pedido_id])
                    ->first(); 

                if($PedidoCliente->cupom_desconto_id != null) {
                    $cupom_desconto_pedido = TableRegistry::get('CupomDesconto')
                        ->find('all')
                        ->where(['CupomDesconto.id' => $PedidoCliente->cupom_desconto_id])
                        ->first();
                }

                foreach ($PedidoCliente->pedido_itens as $pi) {

                    $pi->preco =  $pi->preco * $desconto_geral;

                    if(count($cupom_desconto_pedido) >= 1) {
                        $desconto  = $cupom_desconto_pedido->valor * .01;
                        $desconto  = $pi->preco * $desconto;
                        $pi->preco = $pi->preco - $desconto;
                    }

                    if(isset($this->request->data['parcelas'])) {
                        if($this->request->data['parcelas'] == 1) {
                            $valor = number_format($pi->preco, 2, '', '');
                        } else if($this->request->data['parcelas'] >= 2) {
                            $valor = number_format($pi->preco, 2, '', '') + number_format($pi->preco * 0.089, 2, '', ''); 
                        }
                    } else {
                        $valor = number_format($pi->preco, 2, '', '');
                    }

                    $valor = str_replace('.', '', $valor);

                    $items_data[] = [
                        "description" => $pi->produto->produto_base->name,
                        "quantity" => $pi->quantidade,
                        "price_cents" => $valor
                    ];

                    $total_parcelado += $pi->quantidade * $valor;
                }

                $total_parcelado = $total_parcelado * 0.01;

                if(isset($this->request->data['token'])) {
                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $prefixo = explode('(', $PedidoCliente->cliente->telephone);
                    $prefixo = explode(')', $prefixo[1]);

                    $address_data = [
                        "street" => $PedidoCliente->academia->address,
                        "number" => $PedidoCliente->academia->number,
                        "city" => $PedidoCliente->academia->city->name,
                        "state" => $PedidoCliente->academia->city->uf,
                        "country" => 'Brasil',
                        "zip_code" => $PedidoCliente->academia->cep
                    ];

                    $payer_data = [
                        "name" => $PedidoCliente->cliente->name,
                        "phone_prefix" => $prefixo[0],
                        "phone" => $prefixo[1],
                        "email" => $PedidoCliente->cliente->email,
                        "address" => $address_data,
                        "cpf_cnpj" => $PedidoCliente->cliente->cpf
                    ];

                    $data = [
                        /*Live*/
                        "api_token"      => IUGU_API_TOKEN,
                        /*Test*/
                        //"api_token"      => 'ff3d2cd16bf9dbcbc828cdab8d133d23',
                        "token"          => $this->request->data['token'],
                        "email"          => $PedidoCliente->cliente->email,
                        "months"         => $this->request->data['parcelas'],
                        "items"          => $items_data,
                        "payer"          => $payer_data
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/charge",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    var_dump($response);

                    $resp_iugu_cartao = json_decode($response);

                    if($resp_iugu_cartao->success) {
                        if($resp_iugu_cartao->message == 'Autorizado') {
                            $PedidoCliente->data_pagamento   = Time::now();
                            $PedidoCliente->pedido_status_id = 3;
                        } else {
                            $PedidoCliente->pedido_status_id = 2;
                        }
                        $PedidoCliente->tid              = $resp_iugu_cartao->invoice_id;
                        $PedidoCliente->iugu_token       = $this->request->data['token'];
                        $PedidoCliente->parcelas         = $this->request->data['parcelas'];
                        $PedidoCliente->valor_parcelado  = $total_parcelado;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_cartao->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_cartao->pdf;
                        $PedidoCliente->iugu_brand       = $this->request->data['iugu_brand'];
                        $PedidoCliente->identification   = $resp_iugu_cartao->identification;
                        $PedidoCliente->portador_name    = $this->request->data['portador_name'];
                        $PedidoCliente->portador_birth   = $this->request->data['portador_birth'];
                        $PedidoCliente->portador_phone   = $this->request->data['portador_phone'];
                        $PedidoCliente->portador_cpf     = $this->request->data['portador_cpf'];
                        $PedidoCliente->payment_method   = 'CartaoDeCredito';
                        $PedidoCliente->transaction_data = serialize($response);
                        $Pedidos->save($PedidoCliente);
                    
                        $this->redirect('/mochila/finalizado/'.$PedidoCliente->id);
                    } else {
                        $PedidoCliente->tid              = $resp_iugu_cartao->invoice_id;
                        $PedidoCliente->iugu_token       = $this->request->data['token'];
                        $PedidoCliente->parcelas         = $this->request->data['parcelas'];
                        $PedidoCliente->valor_parcelado  = $total_parcelado;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_cartao->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_cartao->pdf;
                        $PedidoCliente->iugu_brand       = $this->request->data['iugu_brand'];
                        $PedidoCliente->identification   = $resp_iugu_cartao->identification;
                        $PedidoCliente->portador_name    = $this->request->data['portador_name'];
                        $PedidoCliente->portador_birth   = $this->request->data['portador_birth'];
                        $PedidoCliente->portador_phone   = $this->request->data['portador_phone'];
                        $PedidoCliente->portador_cpf     = $this->request->data['portador_cpf'];
                        $PedidoCliente->pedido_status_id = 1;
                        $PedidoCliente->transaction_data = serialize($response);
                        $Pedidos->save($PedidoCliente);

                        $this->Flash->error('Falha ao passar cartão, tente novamente!');
                        $this->redirect('/mochila/fechar-o-ziper');
                    }

                } else {
                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $prefixo = explode('(', $PedidoCliente->cliente->telephone);
                    $prefixo = explode(')', $prefixo[1]);

                    $address_data = [
                        "street" => $PedidoCliente->academia->address,
                        "number" => $PedidoCliente->academia->number,
                        "district" => $PedidoCliente->academia->area,
                        "city" => $PedidoCliente->academia->city->name,
                        "state" => $PedidoCliente->academia->city->uf,
                        "country" => 'Brasil',
                        "zip_code" => $PedidoCliente->academia->cep
                    ];

                    $payer_data = [
                        "name" => $PedidoCliente->cliente->name,
                        "phone_prefix" => $prefixo[0],
                        "phone" => $prefixo[1],
                        "email" => $PedidoCliente->cliente->email,
                        "address" => $address_data,
                        "cpf_cnpj" => $PedidoCliente->cliente->cpf
                    ];

                    $data = [
                        /*Live*/
                        "api_token"      => IUGU_API_TOKEN,
                        /*Test*/
                        //"api_token"      => 'ff3d2cd16bf9dbcbc828cdab8d133d23',
                        "method"         => 'bank_slip',
                        "email"          => $PedidoCliente->cliente->email,
                        "months"         => $this->request->data['parcelas'],
                        "items"          => $items_data,
                        "payer"          => $payer_data
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/charge",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    $resp_iugu_boleto = json_decode($response);

                    if($resp_iugu_boleto->success) {
                        $PedidoCliente->tid              = $resp_iugu_boleto->invoice_id;
                        $PedidoCliente->identification   = $resp_iugu_boleto->identification;
                        $PedidoCliente->payment_method   = 'BoletoBancario';
                        $PedidoCliente->parcelas         = 1;
                        $PedidoCliente->valor_parcelado  = null;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_boleto->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_boleto->pdf;
                        $PedidoCliente->pedido_status_id = 2;
                        $PedidoCliente->transaction_data = serialize($response);
                        if($save_pedido = $Pedidos->save($PedidoCliente)) {
                            // Email::configTransport('cliente', [
                            //     'className' => 'Smtp',
                            //     'host' => 'mail.logfitness.com.br',
                            //     'port' => 587,
                            //     'timeout' => 30,
                            //     'username' => 'uhull@logfitness.com.br',
                            //     'password' => 'Lvrt6174?',
                            //     'client' => null,
                            //     'tls' => null,
                            // ]);

                            // $email = new Email();

                            // $email
                            //     ->transport('cliente')
                            //     ->template('boleto_produto')
                            //     ->emailFormat('html')
                            //     ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                            //     ->to($PedidoCliente->cliente->email)
                            //     ->subject($PedidoCliente->academia->shortname.' - LogFitness - Boleto')
                            //     ->set([
                            //         'name'              => $PedidoCliente->cliente->name,
                            //         'academia'          => $PedidoCliente->academia->shortname,
                            //         'logo_academia'     => $PedidoCliente->academia->image,
                            //         'id'                => $PedidoCliente->id,
                            //         'boleto'            => $resp_iugu_boleto->pdf
                            //     ])
                            //     ->send();

                        }
                        
                        $this->redirect('/mochila/finalizado/'.$PedidoCliente->id);
                    
                    } else {
                        $PedidoCliente->tid              = $resp_iugu_boleto->invoice_id;
                        $PedidoCliente->identification   = $resp_iugu_boleto->identification;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_boleto->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_boleto->pdf;
                        $PedidoCliente->pedido_status_id = 1;
                        $PedidoCliente->transaction_data = serialize($response);
                        $Pedidos->save($PedidoCliente);

                        $this->Flash->error('Falha ao gerar boleto, tente novamente!');
                        $this->redirect('/mochila/fechar-o-ziper');
                    }
                }
            }
        }
    }

    /**
     * CARRINHO - PASSO 1 - VERIFICAR SE CLIENTE ESTÁ LOGADO
     * CARRINHO - PASSO 2 - VERIFICAR SE OS DADOS ESTÃO COMPLETOS
     * ROTA::/mochila/fechar-o-ziper
     */
    public function carrinho_identifica($retormar_pedido = false, $pedido_id = null){
            
        $session    = $this->request->session();

        $DescontoCombo   = $session->read('DescontoCombo');

        $RetiradaLoja = $session->read('RetiradaLoja');

        if($RetiradaLoja) {
            $loja_retirada = TableRegistry::get('Academias')
                ->find('all')
                ->contain(['Cities'])
                ->where(['Academias.id' => $RetiradaLoja])
                ->first();

            $this->set(compact('loja_retirada'));
        }

        $gateway_pagamento = TableRegistry::get('VariaveisGlobais')
            ->find('all')
            ->where(['name' => 'gateway_pagamento'])
            ->first();

        $tipo_gateway = $gateway_pagamento->valor;

        $parcelas_promo = TableRegistry::get('VariaveisGlobais')
            ->find('all')
            ->where(['name' => 'parcelas_promo'])
            ->first();

        $parcelas_promo_valor = $parcelas_promo->valor;

        if ($tipo_gateway == 2) {

            $ch = curl_init();

            $header = [
                "Content-Type: application/x-www-form-urlencoded; charset=ISO-8859-1",
            ];

            $data['email']= PAGSEGURO_EMAIL;
            $data['token']= PAGSEGURO_TOKEN;

            $options = array(CURLOPT_URL => PAGSEGURO_URL."sessions",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => http_build_query($data),
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $separa_response = simplexml_load_string($response);

            $session_id = $separa_response->id;

            $session->write('TokenPagseguro', $session_id);
            
            $this->set(compact('session_id'));
        }

        //$session_cliente = $session->read('Cliente');
        $PreOrders = TableRegistry::get('PreOrders');
        
        $ProdutoIndicacoes = TableRegistry::get('ProdutoIndicacoes');
        
        $Indicacao = $session->read('Indicacao');
        $AcademiaSession = $session->read('Academia');

        if(!empty($Indicacao)) {
            $p_indicacao = $ProdutoIndicacoes
                ->find('all')
                ->where(['ProdutoIndicacoes.id' => $Indicacao])
                ->first();

            if(count($p_indicacao >= 1)) {
                $aceitou = ['aceitou' => 1];

                $p_indicacao = $ProdutoIndicacoes->patchEntity($p_indicacao, $aceitou);
                $ProdutoIndicacoes->save($p_indicacao);
            }
        }

        $variavel_desconto = TableRegistry::get('VariaveisGlobais')
            ->find('all')
            ->where(['name' => 'desconto'])
            ->first();

        $desconto_geral = 1 - ($variavel_desconto->valor * 0.01);

        $this->set(compact('cidade_acad', 'estado_acad', 'RetiradaLoja', 'tipo_gateway', 'parcelas_promo_valor'));
        $session->read('PreOrder') ? $pre_order_id = (int)$session->read('PreOrder') : $pre_order_id = 0;

        $config = $session->read('Configurations');
        isset($config['transferir_taxa_parcelamento'])
            ? $taxa_parcelamento = (boolean)$config['transferir_taxa_parcelamento']
            : $taxa_parcelamento = true;

        $academia_url = explode('/', $_SERVER['REQUEST_URI']);

        if($academia_url[1] == 'mochila') {
            $academia_url[1] = '';
        }

        $session_cliente = $session->read('UsuarioDados');

        /**
         * CLIENTE NÃO LOGADO
         */
        if(empty($session_cliente) && !$retormar_pedido){
            return $this->redirect('/mochila/usuario-preencher-dados/');
        }
        /**
         * CLIENTE LOGADO NO SISTEMA
         */
        else{
            $Pedidos = TableRegistry::get('Pedidos');

            $cliente = TableRegistry::get('Clientes')
                ->find('all')
                ->where(['Clientes.id' => $session_cliente->id])
                ->contain(['Cities', 'Academias', 'Academias.Cities'])
                ->first();

            /**
             * RETORMAR PEDIDO JÁ REALIZADO
             */
            if($retormar_pedido == 'retormar-pedido' && $pedido_id >= 1){
                $this->set('completar_cadastro', false);

                $PedidoCliente  = $Pedidos
                    ->find('all', ['contain' => [
                        'Academias',
                        'Academias.Cities',
                        'Clientes',
                        'Professores',
                        'PedidoItens',
                        'PedidoItens.Produtos',
                        'PedidoItens.Produtos.ProdutoBase',
                        'PedidoItens.Produtos.ProdutoBase.Marcas'
                    ]
                    ])
                    ->where(['Pedidos.id' => $pedido_id])
                    ->first(); 

                if($PedidoCliente->pedido_status_id != 1 && $PedidoCliente->pedido_status_id != 2 && $PedidoCliente->pedido_status_id != 7) {
                    return $this->redirect('/mochila/finalizado/'.$PedidoCliente->id);
                }

                $p_indisponivel = 0;

                if($PedidoCliente->pedido_status_id == 1) {
                    foreach ($PedidoCliente->pedido_itens as $p_itens) {
                        if($p_itens->produto->status_id == 2 || $p_itens->produto->estoque < $p_itens->produto->estoque_min) {
                            $p_indisponivel = 1;
                            $this->Flash->error('O pedido '.$pedido_id.' tem produtos que acabaram, refaça seu pedido com outros produtos!');
                            return $this->redirect('/'.$AcademiaSession->slug);
                        }
                    }
                } 

                if($p_indisponivel == 1) {

                    $session = $this->request->session();

                    $cliente = $session->read('Cliente');

                    $Pedidos = TableRegistry::get('Pedidos');

                    $s_pedidos = $Pedidos
                        ->find('all')
                        ->contain([
                            'PedidoItens',
                        ])
                        ->where(['Pedidos.cliente_id' => $cliente->id])
                        ->andWhere(['Pedidos.id' => $pedido_id])
                        ->first();

                    $session->delete('Carrinho');

                    foreach ($s_pedidos->pedido_itens as $item):
                        if($item->quantidade > 0):
                            $qtd_produtos_pedido = $qtd_produtos_pedido + 1;
                            $session->write('Carrinho.'.$item->produto_id, $item->quantidade);
                        endif;
                    endforeach;

                    $session->write('Pedido', $pedido_id);
     
                    $session_produto    = $session->read('Carrinho');

                    $session_produto_ids = [];
                    if(!empty($session_produto)) {
                        foreach ($session_produto as $key => $item) {
                            $session_produto_ids[] = $key;
                        }
                    }

                    $produtos_inativos = TableRegistry::get('Produtos')
                            ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                            ->andWhere(['Produtos.preco > ' => 0.1])
                            ->andWhere(['Produtos.status_id' => 2])
                            ->where(['Produtos.id IN' => $session_produto_ids])
                            ->all();

                    foreach ($produtos_inativos as $pi) {
                        $session->delete('Carrinho.'.$pi->id);
                    }  

                    if(!empty($session_produto_ids)) {
                        $s_produtos = $this->Produtos
                            ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                            ->andWhere(['Produtos.preco > ' => 0.1])
                            ->andWhere(['Produtos.status_id' => 1])
                            ->where(['Produtos.id IN' => $session_produto_ids])
                            ->all();
                    }else{
                        $s_produtos = [];
                    }

                    $this->set(compact('alerta_inativo', 's_produtos', 'session_produto', 'session_cliente'));
                    $this->redirect('/home');
                } else {
                    if(count($PedidoCliente) >= 1) {

                        $cupom_valido = $PedidoCliente->cupom_desconto_id;

                        if($PedidoCliente->pedido_status_id != 1 && $PedidoCliente->pedido_status_id != 2 && $PedidoCliente->pedido_status_id != 7) {
                            return $this->redirect('/mochila/finalizado/'.$PedidoCliente->id);
                        }

                        $this->iugu_cobranca_direta($PedidoCliente->id);

                        $this->set(compact('PedidoCliente', 'cliente', 'token', 'cupom_valido'));
                    }else{
                        $this->redirect('/mochila');

                    }
                }
            }
            /**
             * CONTINUAR COM PEDIDO NOVO
             */
            else{
                /**
                 * CLIENTE COM CADASTRO INCOMPLETO
                 */
                if(
                    !isset($session_cliente->cpf) ||
                    !isset($session_cliente->academia_id)
                ){
                    $this->set('completar_cadastro', true);

                    $states     = TableRegistry::get('States')->find('list');

                    if(isset($cliente->city->state_id)) {
                        $cities = TableRegistry::get('Cities')
                            ->find('list')
                            ->where(['state_id' => $cliente->city->state_id]);
                    }else{
                        $cities = [];
                    }
                    $academias  = TableRegistry::get('Academias')
                        ->find('list');

                    $this->set(compact('states', 'cliente', 'academias', 'cities'));
                }
                /**
                 * CLIENTE COM CADASTRO OK - PROSSEGUIR COM A FINALIZAÇÃO
                 */
                else{
                    $this->set('completar_cadastro', false);
                    $session_carrinho = $session->read('Carrinho');

                    // if(count($session_carrinho) >= 1) {
                    $pedido = $Pedidos->newEntity();

                    $valor = 0;

                    $produto_carrinho_total = 0;

                    foreach ($session_carrinho as $ks => $item):
                        $valor = $this->Produtos->find('all')->where(['id' => $ks])->first('preco_promo');
                        if($valor->preco_promo < 1) {
                            $produto_carrinho = $this->Produtos->find('all')->where(['id' => $ks])->sumOf('preco') * $desconto_geral;
                        } else {
                            $produto_carrinho = $this->Produtos->find('all')->where(['id' => $ks])->sumOf('preco_promo') * $desconto_geral;
                        }

                        $produto_carrinho_total += ($item * $produto_carrinho);
                        $valor += $produto_carrinho_total - 1;
                    endforeach;

                    $DescontoCombo = $session->read('DescontoCombo');

                    $cupom = $session->read('CupomDesconto');

                    if($DescontoCombo) {
                        $cupom = [];
                    }

                    $cupom_usado = TableRegistry::get('Pedidos')
                        ->find('all')
                        ->where(['Pedidos.cliente_id' => $cliente->id])
                        ->andWhere(['Pedidos.cupom_desconto_id' => $cupom->id])
                        ->first();

                    $cupom_invalido = 0;
                    $cupom_valido = 0;

                    //select no banco para trazer o cupom com id referente ao id usado na compra
                    // $repetir_cupom_cpf = TableRegistry::get('Cupom_desconto')
                    // ->find('all')
                    // ->where(['Cupom_desconto.id' => $cupom->id])
                    // ->first();

                    // print_r($cupom);
                    //aqui procuro o cupom que já foi usado e se ele já foi usado por aquele cpf, 
                    //permito ou não o uso do cupom novamente, onde repetir_cupom (0) = não e (1) = sim
                    // if($cupom->repetir_cupom == 0){
                    //     if(count($cupom_usado) >= 1) {
                    //         $cupom = [];
                    //         $cupom_invalido = 1;
                    //     }
                    // }


                    if($cupom->repetir_cupom == 0){
                        if(count($cupom_usado) >= 1) {
                            $cupom = [];
                            $cupom_invalido = 1;
                        }
                    }

                    $qtd_cupons = TableRegistry::get('Pedidos')
                        ->find('all')
                        ->where(['Pedidos.cupom_desconto_id' => $cupom->id])
                        ->all();

                    if(count($qtd_cupons) > $cupom->quantidade) {
                        $cupom = [];
                        $cupom_invalido = 1;
                    }

                    $type = '';

                    if($DescontoCombo) {

                        $cupom = TableRegistry::get('CupomDesconto')
                            ->find('all')
                            ->where(['id' => 176])
                            ->first();

                        $type = 'combo';
                    }

                    if(count($cupom) > 0) {
                        if($cupom->tipo == 1) {
                            $desconto = 1.0 - ($cupom->valor * .01);
                            $valor = $valor * $desconto;
                        } else {
                            $valor = $valor - $cupom->valor;
                        }

                        $cupom_valido = 1;
                    }

                    $this->set(compact('cupom_invalido', 'cupom_valido', 'repetir_cupom_cpf'));

                    debug($valor);

                    $academia_user = TableRegistry::get('Academias')
                        ->find('all')
                        ->where(['id' => $session_cliente->academia_id])
                        ->first();

                    if($academia_user->user_id != null) {
                        $user = TableRegistry::get('Users')
                            ->find('all')
                            ->where(['id' => $academia_user->user_id])
                            ->first();

                        if($user->group_id == 7) {
                            $usuario_id = $user->id;
                        } else {
                            $usuario_id = null;
                        }
                    }

                    $Clientes = TableRegistry::get('Clientes');

                    $clientes = $Clientes
                        ->find('all')
                        ->contain(['Cities', 'Academias', 'Academias.Cities'])
                        ->where(['Clientes.id' => $session_cliente->id])
                        ->first();

                    $data = [
                        'cliente_id'        => $session_cliente->id,
                        'cep'               => $clientes->cep,
                        'address'           => $clientes->address,
                        'number'            => $clientes->number,
                        'area'              => $clientes->area,
                        'city_name'         => $clientes->city->name,
                        'uf'                => $clientes->city->uf,
                        'pedido_status_id'  => 1,
                        'cupom_desconto_id' => $cupom->id,
                        'frete'             => 0,
                        'indicacao'         => $_COOKIE['indicacao'] ? $_COOKIE['indicacao'] : '',
                        'academia_id'       => $AcademiaSession->id,
                        'professor_id'      => $session_cliente->professor_id,
                        'user_id'           => $usuario_id,
                        'valor'             => (float)$valor,
                        'type'              => $type,
                        'retirada_loja'     => $RetiradaLoja
                    ];

                    $pedido = $Pedidos->patchEntity($pedido, $data);
                    //}

                    $session->delete('CupomDesconto');

                    /**
                     * SE O PEDIDO AINDA NÃO FOI REGISTRADO NO SISTEMA
                     */
                    if(!$session->read('PedidoFinalizado') &&
                        !$session->read('PedidoFinalizadoItem') &&
                        !$session->read('ClienteFinalizado') && 
                        !$retormar_pedido == 'retormar-pedido'){

                        if($valor >= 1) {
                            if ($PEDIDO_SAVE = $Pedidos->save($pedido)){

                                $PEDIDO_SAVE_ID = ['id' => (20600000 + $PEDIDO_SAVE->id)];

                                $PEDIDO_SAVE = $Pedidos->patchEntity($PEDIDO_SAVE, $PEDIDO_SAVE_ID);
                                if($PEDIDO = $Pedidos->save($PEDIDO_SAVE)){

                                    $session->write('PedidoFinalizado', $PEDIDO);

                                    $PedidoItens = TableRegistry::get('PedidoItens');

                                    //$total = 0;

                                    if($session_carrinho && count($session_carrinho) >= 1){

                                        foreach ($session_carrinho as $key => $item){

                                            $produto = $this->Produtos
                                                ->find('all')
                                                ->where(['Produtos.id' => $key])
                                                ->first();

                                            if ($produto) {
                                                $produto->preco_promo ?
                                                    $preco = $produto->preco_promo * $desconto_geral :
                                                    $preco = $produto->preco * $desconto_geral;

                                                $pedido_item = $PedidoItens->newEntity();
                                                $item_data = [
                                                    'pedido_id' => $PEDIDO->id,
                                                    'produto_id' => $key,
                                                    'quantidade' => $item,
                                                    'desconto' => 0,
                                                    'preco' => $preco,
                                                ];

                                                $pedido_item = $PedidoItens->patchEntity($pedido_item, $item_data);

                                                if ($PEDIDO_ITEM_SAVE = $PedidoItens->save($pedido_item)) {
                                                    $session->write('PedidoFinalizadoItem.'.$PEDIDO_ITEM_SAVE->id, $PEDIDO_ITEM_SAVE);
                                                    //$total += ($item_data['quantidade'] * $item_data['preco']) - $item_data['desconto'];

                                                    if($pre_order_id > 0):
                                                        $pre_order = $PreOrders
                                                            ->find('all')
                                                            ->where(['PreOrders.id' => $pre_order_id])
                                                            ->first();

                                                        $pre_order_data = ['pedido_id' => $PEDIDO->id];
                                                        $pre_order = $PreOrders->patchEntity($pre_order, $pre_order_data);

                                                        if($pre_order_save = $PreOrders->save($pre_order));
                                                    endif;
                                                }
                                            }
                                        }

                                        //$total = number_format($total, 2, '.', '');
                                        $total = number_format($valor, 2, '.', '');

                                        $PedidoCliente = $Pedidos
                                            ->find('all', ['contain' => [
                                                'Academias',
                                                'Academias.Cities',
                                                'Clientes',
                                                'Professores',
                                                'PedidoItens',
                                                'PedidoItens.Produtos',
                                                'PedidoItens.Produtos.ProdutoBase',
                                                'PedidoItens.Produtos.ProdutoBase.Marcas'
                                            ]
                                            ])
                                            ->where(['Pedidos.id' => $PEDIDO->id])
                                            ->first();

                                        if($PedidoCliente->pedido_status_id != 1 && $PedidoCliente->pedido_status_id != 2 && $PedidoCliente->pedido_status_id != 7) {
                                            return $this->redirect('/mochila/finalizado/'.$PedidoCliente->id);
                                        }

                                        // Email::configTransport('cliente', [
                                        //     'className' => 'Smtp',
                                        //     'host' => 'mail.logfitness.com.br',
                                        //     'port' => 587,
                                        //     'timeout' => 30,
                                        //     'username' => EMAIL_USER,
                                        //     'password' => EMAIL_SENHA,
                                        //     'client' => null,
                                        //     'tls' => null,
                                        // ]);

                                        // $email = new Email();

                                        // $email
                                        //     ->transport('cliente')
                                        //     ->template('mochila_fechada_aluno')
                                        //     ->emailFormat('html')
                                        //     ->from([EMAIL_USER => EMAIL_NAME])
                                        //     ->to($PedidoCliente->cliente->email)
                                        //     ->subject($PedidoCliente->academia->shortname.' - '.EMAIL_NAME.' - Mochila Fechada')
                                        //     ->set([
                                        //         'id'                => $PedidoCliente->id,
                                        //         'name'              => $PedidoCliente->cliente->name,
                                        //         'academia'          => $PedidoCliente->academia->shortname,
                                        //         'logo_academia'     => $PedidoCliente->academia->image,
                                        //         'academia_id'       => $PedidoCliente->academia->id,
                                        //         'url'               => WEBROOT_URL.$PedidoCliente->academia->slug.'/mochila/fechar-o-ziper/retormar-pedido/'.$PedidoCliente->id
                                        //     ])
                                        //     ->send();

                                        $session->write('ClienteFinalizado', $cliente);

                                        $this->iugu_cobranca_direta($PedidoCliente->id);

                                        $this->set(compact('PedidoCliente', 'cliente', 'token'));
                                    }else{
                                        $this->redirect('/mochila');
                                    }
                                }
                            }else{
                                $this->redirect('/mochila/');
                            }
                        }
                    }else{

                        if($session->read('PedidoFinalizado.id') > 1 && $session->read('ClienteFinalizado')) {

                            debug($session->read('PedidoFinalizado.id'));

                            $PedidoCliente = $Pedidos
                                ->find('all', ['contain' => [
                                    'Academias',
                                    'Academias.Cities',
                                    'Clientes',
                                    'Professores',
                                    'PedidoItens',
                                    'PedidoItens.Produtos',
                                    'PedidoItens.Produtos.ProdutoBase',
                                    'PedidoItens.Produtos.ProdutoBase.Marcas'
                                ]
                                ])
                                ->where(['Pedidos.id' => $session->read('PedidoFinalizado.id')])
                                ->first();

                            $cupom_valido = $PedidoCliente->cupom_desconto_id;

                            if($PedidoCliente->pedido_status_id != 1 && $PedidoCliente->pedido_status_id != 2 && $PedidoCliente->pedido_status_id != 7) {
                                return $this->redirect('/mochila/finalizado/'.$PedidoCliente->id);
                            }

                            $this->iugu_cobranca_direta($PedidoCliente->id);

                            $this->set(compact('PedidoCliente', 'cliente', 'token', 'cupom_valido'));

                        }elseif ($PEDIDO_SAVE = $Pedidos->save($pedido)){

                            $PEDIDO_SAVE_ID = ['id' => (20600000 + $PEDIDO_SAVE->id)];

                            $PEDIDO_SAVE = $Pedidos->patchEntity($PEDIDO_SAVE, $PEDIDO_SAVE_ID);
                            if($PEDIDO = $Pedidos->save($PEDIDO_SAVE)){



                                $session->write('PedidoFinalizado', $PEDIDO);

                                $PedidoItens = TableRegistry::get('PedidoItens');
                                $session_carrinho = $session->read('Carrinho');

                                //$total = 0;

                                if($session_carrinho) {


                                    foreach ($session_carrinho as $key => $item){
                                        //$produto = $this->Produtos->get($key);
                                        $produto = $this->Produtos
                                            ->find('all')
                                            ->where(['Produtos.id' => $key])
                                            ->first();

                                        if ($produto) {
                                            $produto->preco_promo ?
                                                $preco = $produto->preco_promo :
                                                $preco = $produto->preco;

                                            $pedido_item = $PedidoItens->newEntity();
                                            $item_data = [
                                                'pedido_id' => $PEDIDO->id,
                                                'produto_id' => $key,
                                                'quantidade' => $item,
                                                'desconto' => 0,
                                                'preco' => $preco,
                                            ];

                                            $pedido_item = $PedidoItens->patchEntity($pedido_item, $item_data);

                                            if ($PEDIDO_ITEM_SAVE = $PedidoItens->save($pedido_item)) {
                                                $session->write('PedidoFinalizadoItem.'.$PEDIDO_ITEM_SAVE->id, $PEDIDO_ITEM_SAVE);
                                                //$total += ($item_data['quantidade'] * $item_data['preco']) - $item_data['desconto'];

                                                if($pre_order_id > 0):
                                                    $pre_order = $PreOrders
                                                        ->find('all')
                                                        ->where(['PreOrders.id' => $pre_order_id])
                                                        ->first();

                                                    $pre_order_data = ['pedido_id' => $PEDIDO->id];
                                                    $pre_order = $PreOrders->patchEntity($pre_order, $pre_order_data);

                                                    if($pre_order_save = $PreOrders->save($pre_order));
                                                endif;
                                            }
                                        }
                                    }

                                    //$total = number_format($total, 2, '.', '');
                                    $total = number_format($valor, 2, '.', '');

                                    $PedidoCliente = $Pedidos
                                        ->find('all', ['contain' => [
                                            'Academias',
                                            'Academias.Cities',
                                            'Professores',
                                            'PedidoItens',
                                            'PedidoItens.Produtos',
                                            'PedidoItens.Produtos.ProdutoBase',
                                            'PedidoItens.Produtos.ProdutoBase.Marcas'
                                        ]
                                        ])
                                        ->where(['Pedidos.id' => $PEDIDO->id])
                                        ->first();

                                    if($PedidoCliente->pedido_status_id != 1 && $PedidoCliente->pedido_status_id != 2 && $PedidoCliente->pedido_status_id != 7) {
                                        return $this->redirect('/mochila/finalizado/'.$PedidoCliente->id);
                                    }

                                    $cliente = TableRegistry::get('Clientes')
                                        ->find('all')
                                        ->where(['Clientes.id' => $session_cliente->id])
                                        ->contain(['Cities', 'Academias', 'Academias.Cities'])
                                        ->first();

                                    $session->write('ClienteFinalizado', $cliente);

                                    $this->iugu_cobranca_direta($PedidoCliente->id);

                                    $this->set(compact('PedidoCliente', 'cliente', 'token'));
                                }else{

                                    $Pedidos->delete($PEDIDO_SAVE);
                                    $Pedidos->delete($PEDIDO);

                                    $session->delete('PedidoFinalizado');
                                    $session->delete('PedidoFinalizadoItem');
                                    $session->delete('ClienteFinalizado');
                                    $session->delete('UsuarioDados');

                                    $this->redirect('/mochila');

                                }
                            }
                        }
                    }

                    $session->delete('Carrinho');
                }

                $this->set('professores_list', TableRegistry::get('Professores')
                    ->find('list')
                    ->innerJoin(
                        ['ProfessorAcademias' => 'professor_academias'],
                        ['ProfessorAcademias.professor_id = Professores.id'])
                    ->where(['Professores.status_id' => 1])
                    ->andWhere(['ProfessorAcademias.academia_id' => $cliente->academia_id])
                    ->andWhere(['ProfessorAcademias.status_id' => 1])
                    ->all()
                    ->toArray()
                );
            }

            $this->set('professores_list', TableRegistry::get('Professores')
                ->find('list')
                ->innerJoin(
                    ['ProfessorAcademias' => 'professor_academias'],
                    ['ProfessorAcademias.professor_id = Professores.id'])
                ->where(['Professores.status_id' => 1])
                ->andWhere(['ProfessorAcademias.academia_id' => $cliente->academia_id])
                ->andWhere(['ProfessorAcademias.status_id' => 1])
                ->all()
                ->toArray()
            );

            $this->set(compact('cupom', 'parcelas'));
        }
        
        $Cities = TableRegistry::get('Cities');
        
        $cidade_query = $Cities
            ->find('all')
            ->where(['id' => $AcademiaSession->city_id_acad])
            ->first();

        $cidade_acad = $cidade_query->name;
        $estado_acad = $cidade_query->uf;

        $this->set(compact('cidade_acad', 'estado_acad', 'DescontoCombo'));

    }


    public function pagar_cielo_credito() {
        if($this->request->is(['post', 'put'])) {
            $card_numero = str_replace(' ', '', $this->request->data['card_numero']);
            $card_validade = $this->request->data['card_validade'];
            $card_cvv = $this->request->data['card_cvv'];
            $card_titular = $this->request->data['card_titular'];
            $card_bandeira = $this->request->data['card_bandeira'];
            $card_cpf = $this->request->data['card_cpf'];
            $card_nascimento = $this->request->data['card_nascimento'];
            $card_telefone = $this->request->data['card_telefone'];
            $parcelas = $this->request->data['parcelas'];
            $valor_total = $this->request->data['valor_total'];
            $pedido_id = $this->request->data['pedido_id'];
            $comprador = $this->request->data['comprador'];

            $Pedidos = TableRegistry::get('Pedidos');

            $ch = curl_init();

            $header = [
                "Content-Type: application/json",
                "MerchantId: ".CIELO_ID,
                "MerchantKey: ".CIELO_KEY
            ];

            $customer = [
                "Name" => $comprador
            ];

            $creditCard = [
                "CardNumber" => $card_numero,
                "Holder" => $card_titular,
                "ExpirationDate" => $card_validade,
                "SecurityCode" => $card_cvv,
                "Brand" => $card_bandeira
            ];

            $payment = [
                "Type" => "CreditCard",
                "Amount" => $valor_total,
                "Installments" => $parcelas,
                "CreditCard" => $creditCard
            ];

            $data = [
                "MerchantOrderId" => $pedido_id,
                "Customer" => $customer,
                "Payment" => $payment
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.cieloecommerce.cielo.com.br/1/sales/",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $retorno_cielo = json_decode($response);

            $autorizado = 0;

            if($retorno_cielo->MerchantOrderId >= 1) {
                $pedido = $Pedidos
                    ->find('all')
                    ->where(['id' => $pedido_id])
                    ->first();

                if($retorno_cielo->Payment->ReturnCode == 4 || $retorno_cielo->Payment->ReturnCode == 6) {
                    $data = [
                        'pedido_status_id' => 3,
                        'tid' => $retorno_cielo->Payment->PaymentId,
                        'transaction_data' => serialize($retorno_cielo),
                        'parcelas' => $retorno_cielo->Payment->Installments,
                        'valor_parcelado' => $valor_total * 0.01,
                        'iugu_brand' => $card_bandeira,
                        'portador_name' => $card_titular,
                        'portador_birth' => $card_nascimento,
                        'portador_phone' => $card_telefone,
                        'portador_cpf' => $card_cpf,
                        'payment_method' => 'CartaoDeCredito',
                        'data_pagamento' => Time::now()
                    ];

                    $autorizado = 1;
                } else {
                    $data = [
                        'pedido_status_id' => 7,
                        'tid' => $retorno_cielo->Payment->PaymentId,
                        'transaction_data' => serialize($retorno_cielo),
                        'parcelas' => $retorno_cielo->Payment->Installments,
                        'valor_parcelado' => $valor_total * 0.01,
                        'iugu_brand' => $card_bandeira,
                        'portador_name' => $card_titular,
                        'portador_birth' => $card_nascimento,
                        'portador_phone' => $card_telefone,
                        'portador_cpf' => $card_cpf,
                        'payment_method' => 'CartaoDeCredito'
                    ];
                }

                $pedido = $Pedidos->patchEntity($pedido, $data);

                $Pedidos->save($pedido);
            }

            echo $autorizado;
        }
        
        $this->viewBuilder()->layout('ajax');
        $this->render(false);
    }

    public function pagar_cielo_debito() {
        if($this->request->is(['post', 'put'])) {
            $card_numero = str_replace(' ', '', $this->request->data['card_numero']);
            $card_validade = $this->request->data['card_validade'];
            $card_cvv = $this->request->data['card_cvv'];
            $card_titular = $this->request->data['card_titular'];
            $card_bandeira = $this->request->data['card_bandeira'];
            $valor_total = $this->request->data['valor_total'];
            $pedido_id = $this->request->data['pedido_id'];
            $comprador = $this->request->data['comprador'];

            $Pedidos = TableRegistry::get('Pedidos');

            $ch = curl_init();

            $header = [
                "Content-Type: application/json",
                "MerchantId: ".CIELO_ID,
                "MerchantKey: ".CIELO_KEY
            ];

            $customer = [
                "Name" => $comprador
            ];

            $debitCard = [
                "CardNumber" => $card_numero,
                "Holder" => $card_titular,
                "ExpirationDate" => $card_validade,
                "SecurityCode" => $card_cvv,
                "Brand" => $card_bandeira
            ];

            $payment = [
                "Type" => "DebitCard",
                "Amount" => $valor_total,
                "ReturnUrl" => WEBROOT_URL.'/mochila/finalizado/debito/'.$pedido_id,
                "DebitCard" => $debitCard
            ];

            $data = [
                "MerchantOrderId" => $pedido_id,
                "Customer" => $customer,
                "Payment" => $payment
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.cieloecommerce.cielo.com.br/1/sales/",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $retorno_cielo = json_decode($response);

            $autorizado = 0;

            if($retorno_cielo->MerchantOrderId >= 1) {
                $pedido = $Pedidos
                    ->find('all')
                    ->where(['id' => $pedido_id])
                    ->first();

                $data = [
                    'pedido_status_id' => 2,
                    'tid' => $retorno_cielo->Payment->PaymentId,
                    'transaction_data' => serialize($retorno_cielo),
                    'iugu_brand' => $retorno_cielo->Payment->DebitCard->Brand,
                    'payment_method' => 'CartaoDeDebito',
                    'iugu_payment_url' => $retorno_cielo->Payment->AuthenticationUrl
                ];

                $pedido = $Pedidos->patchEntity($pedido, $data);

                $Pedidos->save($pedido);

                $autorizado = $retorno_cielo->Payment->AuthenticationUrl;
            } else {
                $pedido = $Pedidos
                    ->find('all')
                    ->where(['id' => $pedido_id])
                    ->first();

                $data = [
                    'transaction_data' => serialize($retorno_cielo)
                ];

                $pedido = $Pedidos->patchEntity($pedido, $data);

                $Pedidos->save($pedido);
            }

            echo $autorizado;
        }
        
        $this->viewBuilder()->layout('ajax');
        $this->render(false);
    }

    public function carrinho_finalizado_debito($pedido_id) {

        $session = $this->request->session();

        $AcademiaSession = $session->read('Academia');

        $Pedidos = TableRegistry::get('Pedidos');

        $pedido = $Pedidos
            ->find('all', ['contain' => [
                'Academias',
                'Clientes',
                'PedidoStatus',
                'PedidoItens',
                'PedidoItens.Produtos',
                'PedidoItens.Produtos.ProdutoBase',
                'PedidoItens.Produtos.ProdutoBase.Marcas'
            ]
            ])
            ->where(['Pedidos.id' => $pedido_id])
            ->first(); 

        $ch = curl_init();

        $header = [
            "Content-Type: application/json",
            "MerchantId: ".CIELO_ID,
            "MerchantKey: ".CIELO_KEY
        ];

        $options = array(CURLOPT_URL => "https://apiquery.cieloecommerce.cielo.com.br/1/sales/".$pedido->tid,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => FALSE,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $retorno_cielo = json_decode($response);

        if($retorno_cielo->MerchantOrderId >= 1) {
            if($retorno_cielo->Payment->Status == 1 || $retorno_cielo->Payment->Status == 2) {
                $data = [
                    'pedido_status_id' => 3,
                    'transaction_data' => serialize($retorno_cielo),
                    'data_pagamento' => Time::now()
                ];

                $pedido = $Pedidos->patchEntity($pedido, $data);

                $Pedidos->save($pedido);
            } else if($retorno_cielo->Payment->Status == 0 || $retorno_cielo->Payment->Status == 12) {
                $data = [
                    'pedido_status_id' => 2,
                    'transaction_data' => serialize($retorno_cielo)
                ];

                $pedido = $Pedidos->patchEntity($pedido, $data);

                $Pedidos->save($pedido);
            } else {
                $data = [
                    'pedido_status_id' => 7,
                    'transaction_data' => serialize($retorno_cielo)
                ];

                $pedido = $Pedidos->patchEntity($pedido, $data);

                $Pedidos->save($pedido);
            }
        }

        $pedido = $Pedidos
            ->find('all', ['contain' => [
                'Academias',
                'Clientes',
                'PedidoStatus',
                'PedidoItens',
                'PedidoItens.Produtos',
                'PedidoItens.Produtos.ProdutoBase',
                'PedidoItens.Produtos.ProdutoBase.Marcas'
            ]
            ])
            ->where(['Pedidos.id' => $pedido_id])
            ->first();

        $this->set(compact('pedido', 'AcademiaSession'));
    }

    public function pagar_iugu_boleto() {
        if($this->request->is(['post', 'put'])) {
            $valor_total = $this->request->data['valor_total'];
            $pedido_id = $this->request->data['pedido_id'];

            $Pedidos = TableRegistry::get('Pedidos');

            $PedidoCliente = $Pedidos
                ->find('all', ['contain' => [
                    'Academias',
                    'Academias.Cities',
                    'Clientes',
                    'PedidoItens',
                    'PedidoItens.Produtos',
                    'PedidoItens.Produtos.ProdutoBase',
                    'PedidoItens.Produtos.ProdutoBase.Marcas'
                ]
                ])
                ->where(['Pedidos.id' => $pedido_id])
                ->first();

            foreach ($PedidoCliente->pedido_itens as $pi) {

                $valor = number_format($pi->preco, 2, '', '');

                $valor = str_replace('.', '', $valor);

                $items_data[] = [
                    "description" => $pi->produto->produto_base->name,
                    "quantity" => $pi->quantidade,
                    "price_cents" => $valor
                ];

                $valor_total_produtos += $pi->quantidade * $valor;
            }

            $desconto = $valor_total_produtos - $valor_total;

            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $prefixo = explode('(', $PedidoCliente->cliente->telephone);
            $prefixo = explode(')', $prefixo[1]);

            $address_data = [
                "street" => $PedidoCliente->academia->address,
                "number" => $PedidoCliente->academia->number,
                "district" => $PedidoCliente->academia->area,
                "city" => $PedidoCliente->academia->city->name,
                "state" => $PedidoCliente->academia->city->uf,
                "country" => 'Brasil',
                "zip_code" => $PedidoCliente->academia->cep
            ];

            $payer_data = [
                "name" => $PedidoCliente->cliente->name,
                "phone_prefix" => $prefixo[0],
                "phone" => $prefixo[1],
                "email" => $PedidoCliente->cliente->email,
                "address" => $address_data,
                "cpf_cnpj" => $PedidoCliente->cliente->cpf
            ];

            $data = [
                "api_token"            => IUGU_API_TOKEN,
                "method"               => 'bank_slip',
                "email"                => $PedidoCliente->cliente->email,
                "months"               => 1,
                "discount_cents"       => $desconto,
                "items"                => $items_data,
                "payer"                => $payer_data
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/charge",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_iugu_boleto = json_decode($response);

            $aprovado = 0;

            if($resp_iugu_boleto->success) {
                if(
                    $PedidoCliente->pedido_status_id == 1 ||
                    $PedidoCliente->pedido_status_id == 7 ||
                    $PedidoCliente->pedido_status_id == 8
                ) {
                    foreach ($PedidoCliente->pedido_itens as $pi) {
                        $produto_estoque = $this->Produtos
                            ->find('all')
                            ->where(['id' => $pi->produto_id])
                            ->first();

                        $estoque_atualizado = ['estoque' => ($produto_estoque->estoque - $pi->quantidade)];

                        $produto_estoque = $this->Produtos->patchEntity($produto_estoque, $estoque_atualizado);
                        $this->Produtos->save($produto_estoque);
                    }
                }

                $PedidoCliente->tid              = $resp_iugu_boleto->invoice_id;
                $PedidoCliente->identification   = $resp_iugu_boleto->identification;
                $PedidoCliente->payment_method   = 'BoletoBancario';
                $PedidoCliente->parcelas         = 1;
                $PedidoCliente->valor_parcelado  = null;
                $PedidoCliente->iugu_payment_url = $resp_iugu_boleto->url;
                $PedidoCliente->iugu_payment_pdf = $resp_iugu_boleto->pdf;
                $PedidoCliente->pedido_status_id = 2;
                $PedidoCliente->transaction_data = serialize($response);
                if($save_pedido = $Pedidos->save($PedidoCliente)) {
                    // Email::configTransport('cliente', [
                    //     'className' => 'Smtp',
                    //     'host' => 'mail.logfitness.com.br',
                    //     'port' => 587,
                    //     'timeout' => 30,
                    //     'username' => 'uhull@logfitness.com.br',
                    //     'password' => 'Lvrt6174?',
                    //     'client' => null,
                    //     'tls' => null,
                    // ]);

                    // $email = new Email();

                    // $email
                    //     ->transport('cliente')
                    //     ->template('boleto_produto')
                    //     ->emailFormat('html')
                    //     ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                    //     ->to($PedidoCliente->cliente->email)
                    //     ->subject($PedidoCliente->academia->shortname.' - LogFitness - Boleto')
                    //     ->set([
                    //         'name'              => $PedidoCliente->cliente->name,
                    //         'academia'          => $PedidoCliente->academia->shortname,
                    //         'logo_academia'     => $PedidoCliente->academia->image,
                    //         'id'                => $PedidoCliente->id,
                    //         'boleto'            => $resp_iugu_boleto->pdf
                    //     ])
                    //     ->send();

                }

                $aprovado = 1;
            } else {
                if(
                    $PedidoCliente->pedido_status_id >= 2 &&
                    $PedidoCliente->pedido_status_id != 7 &&
                    $PedidoCliente->pedido_status_id != 8
                ) {
                    foreach ($PedidoCliente->pedido_itens as $pi) {
                        $produto_estoque = $this->Produtos
                            ->find('all')
                            ->where(['id' => $pi->produto_id])
                            ->first();

                        $estoque_atualizado = ['estoque' => ($produto_estoque->estoque + $pi->quantidade)];

                        $produto_estoque = $this->Produtos->patchEntity($produto_estoque, $estoque_atualizado);
                        $this->Produtos->save($produto_estoque);
                    }
                }

                $PedidoCliente->tid              = $resp_iugu_boleto->invoice_id;
                $PedidoCliente->identification   = $resp_iugu_boleto->identification;
                $PedidoCliente->iugu_payment_url = $resp_iugu_boleto->url;
                $PedidoCliente->iugu_payment_pdf = $resp_iugu_boleto->pdf;
                $PedidoCliente->pedido_status_id = 1;
                $PedidoCliente->transaction_data = serialize($response);
                $Pedidos->save($PedidoCliente);

                $aprovado = 0;
            }

            echo $aprovado;
        }
        
        $this->viewBuilder()->layout('ajax');
        $this->render(false);
    }

    public function pagar_pagseguro_boleto() {
        if($this->request->is(['post', 'put'])) {
            $session = $this->request->session();

            $TokenPagseguro = $session->read('TokenPagseguro');

            $valor_total = $this->request->data['valor_total'];
            $pedido_id = $this->request->data['pedido_id'];
            $hash = $this->request->data['hash'];

            $Pedidos = TableRegistry::get('Pedidos');

            $PedidoCliente = $Pedidos
                ->find('all', ['contain' => [
                    'Academias',
                    'Academias.Cities',
                    'Clientes',
                    'PedidoItens',
                    'PedidoItens.Produtos',
                    'PedidoItens.Produtos.ProdutoBase',
                    'PedidoItens.Produtos.ProdutoBase.Marcas'
                ]
                ])
                ->where(['Pedidos.id' => $pedido_id])
                ->first();

            $items = '';

            foreach ($PedidoCliente->pedido_itens as $pi) {

                $valor = number_format($pi->preco, 2, '', '');

                $valor = str_replace('.', '', $valor);

                $items_data[] = [
                    "description" => $pi->produto->produto_base->name,
                    "quantity" => $pi->quantidade,
                    "price_cents" => $valor
                ];
                $items .=   '<item>
                              <id>'.$pi->produto->id.'</id>
                              <description>'.$pi->produto->produto_base->name.'</description>
                              <amount>'.number_format($PedidoCliente->valor, 2, '.', '').'</amount>
                              <quantity>'.$pi->quantidade.'</quantity>
                            </item>';

                $valor_total_produtos += $pi->quantidade * $valor;
            }

            $desconto = $valor_total_produtos - $valor_total;

            $ch = curl_init();

            $header = [
                "Content-Type: application/xml; charset=ISO-8859-1"
            ];

            $prefixo = explode('(', $PedidoCliente->cliente->telephone);
            $prefixo = explode(')', $prefixo[1]);

            $address_data = [
                "street" => $PedidoCliente->address,
                "number" => $PedidoCliente->number,
                "district" => $PedidoCliente->area,
                "city" => $PedidoCliente->city_name,
                "state" => $PedidoCliente->uf,
                "country" => 'Brasil',
                "zip_code" => $PedidoCliente->cep
            ];

            $payer_data = [
                "name" => $PedidoCliente->cliente->name,
                "phone_prefix" => $prefixo[0],
                "phone" => $prefixo[1],
                "email" => $PedidoCliente->cliente->email,
                "address" => $address_data,
                "cpf_cnpj" => $PedidoCliente->cliente->cpf
            ];

            $data =     
                '<payment>
                  <mode>default</mode>
                  <currency>BRL</currency>
                  <notificationURL>'.WEBROOT_URL.'pagsegurowebhooks</notificationURL>
                  <sender>
                    <hash>'.$hash.'</hash>
                    <name>'.$PedidoCliente->cliente->name.'</name>
                    <email>'.$PedidoCliente->cliente->email.'</email>
                    <phone>
                      <areaCode>'.$prefixo[0].'</areaCode>
                      <number>'.trim(str_replace('-', '', $prefixo[1])).'</number>
                    </phone>
                    <documents>
                      <document>
                        <type>CPF</type>
                        <value>'.str_replace('-', '', str_replace('.', '', $PedidoCliente->cliente->cpf)).'</value>
                      </document>
                    </documents>
                  </sender>
                  <items>
                    '.$items.'
                  </items>
                  <method>boleto</method>
                  <shipping>
                    <address>
                      <street>'.$PedidoCliente->address.'</street>
                      <number>'.$PedidoCliente->number.'</number>
                      <complement>'.$PedidoCliente->academia->complement.'</complement>
                      <district>'.$PedidoCliente->area.'</district>
                      <city>'.$PedidoCliente->city_name.'</city>
                      <state>'.$PedidoCliente->uf.'</state>
                      <country>Brazil</country>
                      <postalCode>'.str_replace('-', '', str_replace('.', '', $PedidoCliente->cep)).'</postalCode>
                    </address>
                  </shipping>
                </payment>';

            $options = array(CURLOPT_URL => PAGSEGURO_URL."transactions?email=".PAGSEGURO_EMAIL."&token=".PAGSEGURO_TOKEN,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_pagseguro_boleto = simplexml_load_string($response);

            $aprovado = 0;

            if($resp_pagseguro_boleto->status == 1) {
                if(
                    $PedidoCliente->pedido_status_id == 1 ||
                    $PedidoCliente->pedido_status_id == 7 ||
                    $PedidoCliente->pedido_status_id == 8
                ) {
                    foreach ($PedidoCliente->pedido_itens as $pi) {
                        $produto_estoque = $this->Produtos
                            ->find('all')
                            ->where(['id' => $pi->produto_id])
                            ->first();

                        $estoque_atualizado = ['estoque' => ($produto_estoque->estoque - $pi->quantidade)];

                        $produto_estoque = $this->Produtos->patchEntity($produto_estoque, $estoque_atualizado);
                        $this->Produtos->save($produto_estoque);
                    }
                }

                $PedidoCliente->tid              = $resp_pagseguro_boleto->code;
                $PedidoCliente->payment_method   = 'BoletoBancario';
                $PedidoCliente->parcelas         = 1;
                $PedidoCliente->valor_parcelado  = null;
                $PedidoCliente->iugu_payment_url = $resp_pagseguro_boleto->paymentLink;
                $PedidoCliente->iugu_payment_pdf = $resp_pagseguro_boleto->paymentLink;
                $PedidoCliente->pedido_status_id = 2;
                $PedidoCliente->transaction_data = serialize($response);
                if($save_pedido = $Pedidos->save($PedidoCliente)) {
                    // Email::configTransport('cliente', [
                    //     'className' => 'Smtp',
                    //     'host' => 'mail.logfitness.com.br',
                    //     'port' => 587,
                    //     'timeout' => 30,
                    //     'username' => 'uhull@logfitness.com.br',
                    //     'password' => 'Lvrt6174?',
                    //     'client' => null,
                    //     'tls' => null,
                    // ]);

                    // $email = new Email();

                    // $email
                    //     ->transport('cliente')
                    //     ->template('boleto_produto')
                    //     ->emailFormat('html')
                    //     ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                    //     ->to($PedidoCliente->cliente->email)
                    //     ->subject($PedidoCliente->academia->shortname.' - LogFitness - Boleto')
                    //     ->set([
                    //         'name'              => $PedidoCliente->cliente->name,
                    //         'academia'          => $PedidoCliente->academia->shortname,
                    //         'logo_academia'     => $PedidoCliente->academia->image,
                    //         'id'                => $PedidoCliente->id,
                    //         'boleto'            => $resp_pagseguro_boleto->pdf
                    //     ])
                    //     ->send();

                }

                $aprovado = 1;
            } else {
                if(
                    $PedidoCliente->pedido_status_id >= 2 &&
                    $PedidoCliente->pedido_status_id != 7 &&
                    $PedidoCliente->pedido_status_id != 8
                ) {
                    foreach ($PedidoCliente->pedido_itens as $pi) {
                        $produto_estoque = $this->Produtos
                            ->find('all')
                            ->where(['id' => $pi->produto_id])
                            ->first();

                        $estoque_atualizado = ['estoque' => ($produto_estoque->estoque + $pi->quantidade)];

                        $produto_estoque = $this->Produtos->patchEntity($produto_estoque, $estoque_atualizado);
                        $this->Produtos->save($produto_estoque);
                    }
                }

                $PedidoCliente->tid              = $resp_pagseguro_boleto->code;
                $PedidoCliente->iugu_payment_url = $resp_pagseguro_boleto->paymentLink;
                $PedidoCliente->iugu_payment_pdf = $resp_pagseguro_boleto->paymentLink;
                $PedidoCliente->pedido_status_id = 1;
                $PedidoCliente->transaction_data = serialize($response);
                $Pedidos->save($PedidoCliente);

                $aprovado = 0;
            }

            echo $aprovado;
        }
        
        $this->viewBuilder()->layout('ajax');
        $this->render(false);
    }

    public function pagar_pagseguro_credito() {
        if($this->request->is(['post', 'put'])) {
            $parcelas_promo = TableRegistry::get('VariaveisGlobais')
            ->find('all')
            ->where(['name' => 'parcelas_promo'])
            ->first();

            $parcelas_promo_valor = $parcelas_promo->valor;

            $session = $this->request->session();

            $TokenPagseguro = $session->read('TokenPagseguro');
            $hash = $this->request->data['hash'];

            $card_token = $this->request->data['card_token'];
            $card_titular = $this->request->data['card_titular'];
            $card_bandeira = $this->request->data['card_bandeira'];
            $card_cpf = $this->request->data['card_cpf'];
            $card_nascimento = $this->request->data['card_nascimento'];
            $card_telefone = $this->request->data['card_telefone'];
            $parcelas = $this->request->data['parcelas'];
            $valor_total = $this->request->data['valor_total'];
            $pedido_id = $this->request->data['pedido_id'];
            $comprador = $this->request->data['comprador'];

            $Pedidos = TableRegistry::get('Pedidos');

            $PedidoCliente = $Pedidos
                ->find('all', ['contain' => [
                    'Academias',
                    'Academias.Cities',
                    'Clientes',
                    'PedidoItens',
                    'PedidoItens.Produtos',
                    'PedidoItens.Produtos.ProdutoBase',
                    'PedidoItens.Produtos.ProdutoBase.Marcas'
                ]
                ])
                ->where(['Pedidos.id' => $pedido_id])
                ->first();
                
            foreach ($PedidoCliente->pedido_itens as $pi) {

                $valor = number_format($pi->preco, 2, '', '');

                $valor = str_replace('.', '', $valor);

                $items_data[] = [
                    "description" => $pi->produto->produto_base->name,
                    "quantity" => $pi->quantidade,
                    "price_cents" => $valor
                ];
                $items .=   '<item>
                              <id>'.$pi->produto->id.'</id>
                              <description>'.$pi->produto->produto_base->name.'</description>
                              <amount>'.number_format($PedidoCliente->valor, 2, '.', '').'</amount>
                              <quantity>'.$pi->quantidade.'</quantity>
                            </item>';

                $valor_total_produtos += $pi->quantidade * $valor;
            }

            $desconto = $valor_total_produtos - $valor_total;

            $ch = curl_init();

            $header = [
                "Content-Type: application/xml; charset=ISO-8859-1"
            ];

            $prefixo = explode('(', $PedidoCliente->cliente->telephone);
            $prefixo = explode(')', $prefixo[1]);

            $address_data = [
                "street" => $PedidoCliente->academia->address,
                "number" => $PedidoCliente->academia->number,
                "district" => $PedidoCliente->academia->area,
                "city" => $PedidoCliente->academia->city->name,
                "state" => $PedidoCliente->academia->city->uf,
                "country" => 'Brasil',
                "zip_code" => $PedidoCliente->academia->cep
            ];

            $payer_data = [
                "name" => $PedidoCliente->cliente->name,
                "phone_prefix" => $prefixo[0],
                "phone" => $prefixo[1],
                "email" => $PedidoCliente->cliente->email,
                "address" => $address_data,
                "cpf_cnpj" => $PedidoCliente->cliente->cpf
            ];

            $nascimento_cliente = date("d-m-Y", strtotime($PedidoCliente->cliente->birth));

            
            // <noInterestInstallmentQuantity>3</noInterestInstallmentQuantity>
            $data =     
                '<?xml version="1.0"?>
                <payment>
                  <mode>default</mode>
                  <currency>BRL</currency>
                  <notificationURL>'.WEBROOT_URL.'pagsegurowebhooks</notificationURL>
                  <receiverEmail>'.PAGSEGURO_EMAIL.'</receiverEmail>
                  <sender>
                    <hash>'.$hash.'</hash>
                    <email>'.$PedidoCliente->cliente->email.'</email>
                    <phone>
                      <areaCode>'.$prefixo[0].'</areaCode>
                      <number>'.trim(str_replace('-', '', $prefixo[1])).'</number>
                    </phone>
                    <bornDate>'.$card_nascimento.'</bornDate>
                    <documents>
                      <document>
                        <type>CPF</type>
                        <value>'.str_replace('-', '', str_replace('.', '', $PedidoCliente->cliente->cpf)).'</value>
                      </document>
                    </documents>
                    <name>'.$PedidoCliente->cliente->name.'</name>
                  </sender>
                  <items>
                    '.$items.'
                  </items>
                  <shipping>
                    <address></address>
                    <cost>0.00</cost>
                    <addressRequired>false</addressRequired>
                  </shipping>
                  <method>creditCard</method>
                  <creditCard>
                    <token>'.$card_token.'</token>
                    <installment>
                      <quantity>'.$parcelas.'</quantity>
                      <noInterestInstallmentQuantity>'.$parcelas_promo->valor.'</noInterestInstallmentQuantity>
                      <value>'.number_format($valor_total, 2, '.', ',').'</value>
                    </installment>
                    <holder>
                      <name>'.$PedidoCliente->cliente->name.'</name>
                      <documents>
                        <document>
                          <type>CPF</type>
                          <value>'.str_replace('-', '', str_replace('.', '', $PedidoCliente->cliente->cpf)).'</value>
                        </document>
                      </documents>
                      <birthDate>'.str_replace('-', '/', $nascimento_cliente).'</birthDate>
                      <phone>
                        <areaCode>'.$prefixo[0].'</areaCode>
                      <number>'.trim(str_replace('-', '', $prefixo[1])).'</number>
                      </phone>
                    </holder>
                    <billingAddress>
                      <street>'.$PedidoCliente->academia->address.'</street>
                      <number>'.$PedidoCliente->academia->number.'</number>
                      <complement>'.$PedidoCliente->academia->complement.'</complement>
                      <district>'.$PedidoCliente->academia->area.'</district>
                      <city>'.$PedidoCliente->academia->city->name.'</city>
                      <state>'.$PedidoCliente->academia->city->uf.'</state>
                      <country>Brazil</country>
                      <postalCode>'.str_replace('-', '', str_replace('.', '', $PedidoCliente->academia->cep)).'</postalCode>
                    </billingAddress>
                  </creditCard>
                </payment>';

            $options = array(CURLOPT_URL => PAGSEGURO_URL."transactions?email=".PAGSEGURO_EMAIL."&token=".PAGSEGURO_TOKEN,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_pagseguro_cartao = simplexml_load_string($response);

            // echo $response;
            // echo " /SEPARA RESPONSE do RESP/ ";
            // print_r($resp_pagseguro_cartao);
            // echo " /SEPARA RESP do aprovado/ ";

            $aprovado = 0;
            
            if($resp_pagseguro_cartao->status == 1) {
                if(
                    $PedidoCliente->pedido_status_id == 1 ||
                    $PedidoCliente->pedido_status_id == 7 ||
                    $PedidoCliente->pedido_status_id == 8
                ) {
                    foreach ($PedidoCliente->pedido_itens as $pi) {
                        $produto_estoque = $this->Produtos
                            ->find('all')
                            ->where(['id' => $pi->produto_id])
                            ->first();

                        $estoque_atualizado = ['estoque' => ($produto_estoque->estoque - $pi->quantidade)];

                        $produto_estoque = $this->Produtos->patchEntity($produto_estoque, $estoque_atualizado);
                        $this->Produtos->save($produto_estoque);
                    }
                }

                if($resp_pagseguro_cartao->message == 'Autorizado') {
                    $PedidoCliente->data_pagamento   = Time::now();
                    $PedidoCliente->pedido_status_id = 3;
                } else {
                    $PedidoCliente->pedido_status_id = 2;
                }
                $PedidoCliente->tid              = $resp_pagseguro_cartao->code;
                $PedidoCliente->iugu_token       = $card_token;
                $PedidoCliente->parcelas         = $parcelas;
                $PedidoCliente->valor_parcelado  = $valor_total * 0.01;
                $PedidoCliente->iugu_payment_url = $resp_pagseguro_cartao->url;
                $PedidoCliente->iugu_payment_pdf = $resp_pagseguro_cartao->pdf;
                $PedidoCliente->iugu_brand       = $card_bandeira;
                $PedidoCliente->identification   = $resp_pagseguro_cartao->identification;
                $PedidoCliente->portador_name    = $card_titular;
                $PedidoCliente->portador_birth   = $card_nascimento;
                $PedidoCliente->portador_phone   = $card_telefone;
                $PedidoCliente->portador_cpf     = $card_cpf;
                $PedidoCliente->payment_method   = 'CartaoDeCredito';
                $PedidoCliente->transaction_data = serialize($response);
                $Pedidos->save($PedidoCliente);

                $aprovado = 1;
            } else {
                if(
                    $PedidoCliente->pedido_status_id >= 2 &&
                    $PedidoCliente->pedido_status_id != 7 &&
                    $PedidoCliente->pedido_status_id != 8
                ) {
                    foreach ($PedidoCliente->pedido_itens as $pi) {
                        $produto_estoque = $this->Produtos
                            ->find('all')
                            ->where(['id' => $pi->produto_id])
                            ->first();

                        $estoque_atualizado = ['estoque' => ($produto_estoque->estoque + $pi->quantidade)];

                        $produto_estoque = $this->Produtos->patchEntity($produto_estoque, $estoque_atualizado);
                        $this->Produtos->save($produto_estoque);
                    }
                }
                
                $PedidoCliente->tid              = $resp_pagseguro_cartao->invoice_id;
                $PedidoCliente->iugu_token       = $card_token;
                $PedidoCliente->parcelas         = $parcelas;
                $PedidoCliente->valor_parcelado  = $valor_total * 0.01;
                $PedidoCliente->iugu_payment_url = $resp_pagseguro_cartao->url;
                $PedidoCliente->iugu_payment_pdf = $resp_pagseguro_cartao->pdf;
                $PedidoCliente->iugu_brand       = $card_bandeira;
                $PedidoCliente->identification   = $resp_pagseguro_cartao->identification;
                $PedidoCliente->portador_name    = $card_titular;
                $PedidoCliente->portador_birth   = $card_nascimento;
                $PedidoCliente->portador_phone   = $card_telefone;
                $PedidoCliente->portador_cpf     = $card_cpf;
                $PedidoCliente->pedido_status_id = 1;
                $PedidoCliente->transaction_data = serialize($response);
                $Pedidos->save($PedidoCliente);

                $aprovado = 0;
            }
            

            echo $aprovado;
        }
        
        $this->viewBuilder()->layout('ajax');
        $this->render(false);
    }

    public function pagar_dinheiro() {
        if($this->request->is(['post', 'put'])) {
            $pedido_id = $this->request->data['pedido_id'];

            $Pedidos = TableRegistry::get('Pedidos');

            $PedidoCliente = $Pedidos
                ->find('all', ['contain' => [
                    'Academias',
                    'Academias.Cities',
                    'Clientes',
                    'PedidoItens',
                    'PedidoItens.Produtos',
                    'PedidoItens.Produtos.ProdutoBase',
                    'PedidoItens.Produtos.ProdutoBase.Marcas'
                ]
                ])
                ->where(['Pedidos.id' => $pedido_id])
                ->first();

            if(
                $PedidoCliente->pedido_status_id == 1 ||
                $PedidoCliente->pedido_status_id == 7 ||
                $PedidoCliente->pedido_status_id == 8
            ) {
                foreach ($PedidoCliente->pedido_itens as $pi) {
                    $produto_estoque = $this->Produtos
                        ->find('all')
                        ->where(['id' => $pi->produto_id])
                        ->first();

                    $estoque_atualizado = ['estoque' => ($produto_estoque->estoque - $pi->quantidade)];

                    $produto_estoque = $this->Produtos->patchEntity($produto_estoque, $estoque_atualizado);
                    $this->Produtos->save($produto_estoque);
                }
            }

            $PedidoCliente->payment_method   = 'Dinheiro';
            $PedidoCliente->parcelas         = 1;
            $PedidoCliente->valor_parcelado  = null;
            $PedidoCliente->pedido_status_id = 2;
            if($save_pedido = $Pedidos->save($PedidoCliente)) {
                // Email::configTransport('cliente', [
                //     'className' => 'Smtp',
                //     'host' => 'mail.logfitness.com.br',
                //     'port' => 587,
                //     'timeout' => 30,
                //     'username' => 'uhull@logfitness.com.br',
                //     'password' => 'Lvrt6174?',
                //     'client' => null,
                //     'tls' => null,
                // ]);

                // $email = new Email();

                // $email
                //     ->transport('cliente')
                //     ->template('boleto_produto')
                //     ->emailFormat('html')
                //     ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                //     ->to($PedidoCliente->cliente->email)
                //     ->subject($PedidoCliente->academia->shortname.' - LogFitness - Boleto')
                //     ->set([
                //         'name'              => $PedidoCliente->cliente->name,
                //         'academia'          => $PedidoCliente->academia->shortname,
                //         'logo_academia'     => $PedidoCliente->academia->image,
                //         'id'                => $PedidoCliente->id,
                //         'boleto'            => $resp_iugu_boleto->pdf
                //     ])
                //     ->send();

                return $this->redirect('/mochila/finalizado/'.$save_pedido->id);
            } else {
                $this->Flash->error('Falha ao realizar pedido, tente novamente');
                return $this->redirect('/mochila/fechar-o-ziper');
            }
        }
        
        $this->viewBuilder()->layout(false);
        $this->render(false);
    }

    public function pagar_iugu_credito() {
        if($this->request->is(['post', 'put'])) {
            $card_token = $this->request->data['card_token'];
            $card_titular = $this->request->data['card_titular'];
            $card_bandeira = $this->request->data['card_bandeira'];
            $card_cpf = $this->request->data['card_cpf'];
            $card_nascimento = $this->request->data['card_nascimento'];
            $card_telefone = $this->request->data['card_telefone'];
            $parcelas = $this->request->data['parcelas'];
            $valor_total = $this->request->data['valor_total'];
            $pedido_id = $this->request->data['pedido_id'];
            $comprador = $this->request->data['comprador'];

            $Pedidos = TableRegistry::get('Pedidos');

            $PedidoCliente = $Pedidos
                ->find('all', ['contain' => [
                    'Academias',
                    'Academias.Cities',
                    'Clientes',
                    'PedidoItens',
                    'PedidoItens.Produtos',
                    'PedidoItens.Produtos.ProdutoBase',
                    'PedidoItens.Produtos.ProdutoBase.Marcas'
                ]
                ])
                ->where(['Pedidos.id' => $pedido_id])
                ->first();
                
            foreach ($PedidoCliente->pedido_itens as $pi) {

                $valor = number_format($pi->preco, 2, '', '');

                if($parcelas >= 2) {
                    $valor = number_format($pi->preco, 2, '', '') + number_format($pi->preco * 0.089, 2, '', ''); 
                }

                $valor = str_replace('.', '', $valor);

                $items_data[] = [
                    "description" => $pi->produto->produto_base->name,
                    "quantity" => $pi->quantidade,
                    "price_cents" => $valor
                ];

                $valor_total_produtos += $pi->quantidade * $valor;
            }

            $desconto = $valor_total_produtos - $valor_total;

            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $prefixo = explode('(', $PedidoCliente->cliente->telephone);
            $prefixo = explode(')', $prefixo[1]);

            $address_data = [
                "street" => $PedidoCliente->academia->address,
                "number" => $PedidoCliente->academia->number,
                "district" => $PedidoCliente->academia->area,
                "city" => $PedidoCliente->academia->city->name,
                "state" => $PedidoCliente->academia->city->uf,
                "country" => 'Brasil',
                "zip_code" => $PedidoCliente->academia->cep
            ];

            $payer_data = [
                "name" => $PedidoCliente->cliente->name,
                "phone_prefix" => $prefixo[0],
                "phone" => $prefixo[1],
                "email" => $PedidoCliente->cliente->email,
                "address" => $address_data,
                "cpf_cnpj" => $PedidoCliente->cliente->cpf
            ];

            $data = [
                "api_token"      => IUGU_API_TOKEN,
                "token"          => $card_token,
                "email"          => $PedidoCliente->cliente->email,
                "months"         => $parcelas,
                "discount_cents" => $desconto,
                "items"          => $items_data,
                "payer"          => $payer_data
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/charge",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_iugu_cartao = json_decode($response);

            $aprovado = 0;
            
            if($resp_iugu_cartao->success) {
                if(
                    $PedidoCliente->pedido_status_id == 1 ||
                    $PedidoCliente->pedido_status_id == 7 ||
                    $PedidoCliente->pedido_status_id == 8
                ) {
                    foreach ($PedidoCliente->pedido_itens as $pi) {
                        $produto_estoque = $this->Produtos
                            ->find('all')
                            ->where(['id' => $pi->produto_id])
                            ->first();

                        $estoque_atualizado = ['estoque' => ($produto_estoque->estoque - $pi->quantidade)];

                        $produto_estoque = $this->Produtos->patchEntity($produto_estoque, $estoque_atualizado);
                        $this->Produtos->save($produto_estoque);
                    }
                }

                if($resp_iugu_cartao->message == 'Autorizado') {
                    $PedidoCliente->data_pagamento   = Time::now();
                    $PedidoCliente->pedido_status_id = 3;
                } else {
                    $PedidoCliente->pedido_status_id = 2;
                }
                $PedidoCliente->tid              = $resp_iugu_cartao->invoice_id;
                $PedidoCliente->iugu_token       = $card_token;
                $PedidoCliente->parcelas         = $parcelas;
                $PedidoCliente->valor_parcelado  = $valor_total * 0.01;
                $PedidoCliente->iugu_payment_url = $resp_iugu_cartao->url;
                $PedidoCliente->iugu_payment_pdf = $resp_iugu_cartao->pdf;
                $PedidoCliente->iugu_brand       = $card_bandeira;
                $PedidoCliente->identification   = $resp_iugu_cartao->identification;
                $PedidoCliente->portador_name    = $card_titular;
                $PedidoCliente->portador_birth   = $card_nascimento;
                $PedidoCliente->portador_phone   = $card_telefone;
                $PedidoCliente->portador_cpf     = $card_cpf;
                $PedidoCliente->payment_method   = 'CartaoDeCredito';
                $PedidoCliente->transaction_data = serialize($response);
                $Pedidos->save($PedidoCliente);

                $aprovado = 1;
            } else {
                if(
                    $PedidoCliente->pedido_status_id >= 2 &&
                    $PedidoCliente->pedido_status_id != 7 &&
                    $PedidoCliente->pedido_status_id != 8
                ) {
                    foreach ($PedidoCliente->pedido_itens as $pi) {
                        $produto_estoque = $this->Produtos
                            ->find('all')
                            ->where(['id' => $pi->produto_id])
                            ->first();

                        $estoque_atualizado = ['estoque' => ($produto_estoque->estoque + $pi->quantidade)];

                        $produto_estoque = $this->Produtos->patchEntity($produto_estoque, $estoque_atualizado);
                        $this->Produtos->save($produto_estoque);
                    }
                }
                
                $PedidoCliente->tid              = $resp_iugu_cartao->invoice_id;
                $PedidoCliente->iugu_token       = $card_token;
                $PedidoCliente->parcelas         = $parcelas;
                $PedidoCliente->valor_parcelado  = $valor_total * 0.01;
                $PedidoCliente->iugu_payment_url = $resp_iugu_cartao->url;
                $PedidoCliente->iugu_payment_pdf = $resp_iugu_cartao->pdf;
                $PedidoCliente->iugu_brand       = $card_bandeira;
                $PedidoCliente->identification   = $resp_iugu_cartao->identification;
                $PedidoCliente->portador_name    = $card_titular;
                $PedidoCliente->portador_birth   = $card_nascimento;
                $PedidoCliente->portador_phone   = $card_telefone;
                $PedidoCliente->portador_cpf     = $card_cpf;
                $PedidoCliente->pedido_status_id = 1;
                $PedidoCliente->transaction_data = serialize($response);
                $Pedidos->save($PedidoCliente);

                $aprovado = 0;
            }

            echo $aprovado;
        }
        
        $this->viewBuilder()->layout('ajax');
        $this->render(false);
    }

    /**
     * AJAX
     * CHECAR CPF JÁ CADASTRADO
     */
    public function check_cpf(){

        $cpf = false;

        if($this->request->is(['post', 'put'])){

            $cpf_check = TableRegistry::get('Clientes')
                ->find('all')
                ->where(['Clientes.cpf' => $this->request->data['cpf']])
                ->first();

            if(count($cpf_check) >= 1){
                $cpf = true;
            }
        }

        echo $cpf;

        $this->viewBuilder()->layout('ajax');
        $this->render('check_cpf');

    }

    /**
     * AJAX
     * CHECAR CPF PROFESSOR JÁ CADASTRADO
     */
    public function check_cpf_professor(){

        $cpf = false;

        if($this->request->is(['post', 'put'])){

            $cpf_check = TableRegistry::get('Professores')
                ->find('all')
                ->where(['Professores.cpf' => $this->request->data['cpf']])
                ->first();

            if(count($cpf_check) >= 1){
                $cpf = true;
            }
        }

        echo $cpf;

        $this->viewBuilder()->layout('ajax');
        $this->render('check_cpf_professor');

    }

    /**
     * AJAX
     * CHECAR CUPOM DE DESCONTO
     */
    public function check_cupom_desconto(){

        $cupom_status = 0;

        $usado = 0;

        $data_atual = Time::now();

        if($this->request->is(['post', 'put'])){

            $session    = $this->request->session();

            $session_cliente = $session->read('Cliente');

            $cupom_desconto = TableRegistry::get('CupomDesconto')
                ->find('all')
                ->where(['CupomDesconto.codigo' => $this->request->data['codigo']])
                ->andWhere(['CupomDesconto.status' => 1])
                ->andWhere(['CupomDesconto.limite >' => $data_atual])
                ->first();
        
            $cupom_usado = TableRegistry::get('Pedidos')
                ->find('all')
                ->where(['Pedidos.cliente_id' => $session_cliente->id])
                ->andWhere(['Pedidos.cupom_desconto_id' => $cupom_desconto->id])
                ->andWhere(['Pedidos.pedido_status_id >=' => 3])
                ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
                ->first();

            if(count($cupom_usado) >= 1) {
                $usado = 1;
            }

            $cupom_usado_qtd = TableRegistry::get('Pedidos')
                ->find('all')
                ->where(['Pedidos.cupom_desconto_id' => $cupom_desconto->id])
                ->all();

            if(count($cupom_usado_qtd) >= $cupom_desconto->quantidade) {
                $usado = 1;
            }
        }

        $session = $this->request->session();

        $session->write('CupomDesconto', $cupom_desconto);

        $this->set(compact('cupom_desconto', 'usado', 'msg'));
        $this->viewBuilder()->layout('ajax');
    }

    /**
     * DEFINIR PROFESSOR ESCOLHIDO NA TELA DE FINALIZAÇÃO DO PEDIDO
     * @param null $pedido_id
     * @param null $professor_id
     */
    public function pedido_set_professor($pedido_id = null, $professor_id = null){

        //if($pedido_id != null && $professor_id != null){
        if($pedido_id != null){

            $Pedidos = TableRegistry::get('Pedidos');

            //$pedido = $Pedidos->get($pedido_id);
            $pedido = $Pedidos
                ->find('all')
                ->where(['Pedidos.id' => $pedido_id])
                ->first();

            $pedido->professor_id = $professor_id;

            if($Pedidos->save($pedido)){
                echo true;
            }
        }
        echo false;

        $this->viewBuilder()->layout('ajax');
        $this->render('comprar');
    }

    /**
     * GERAR BOLETO VIA AJAX NA TELA DE FINALIZAÇÃO DO PEDIDO
     */
    public function carrinho_get_boleto(){

        if($this->request->is(['post', 'put', 'patch'])){

            // create a new cURL resource
            $ch = curl_init();

            // set URL and other appropriate options
            /*curl_setopt($ch, CURLOPT_URL, $url_transacao);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);*/

            $credentials = MOIP_TOKEN.':'.MOIP_KEY;

            $header[] = "Expect:";
            $header[] = "Authorization: Basic " . base64_encode($credentials);

            $ch = curl_init();
            $options = array(CURLOPT_URL => $this->request->data['url'],
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POST => false,
                //CURLOPT_POSTFIELDS => $xml,
                CURLOPT_RETURNTRANSFER => true,
                CURLINFO_HEADER_OUT => true
            );

            curl_setopt_array($ch, $options);

            // grab URL and pass it to the browser
            $detalhes = curl_exec($ch);
            $ret = curl_exec($ch);
            $err = curl_error($ch);
            $info = curl_getinfo($ch);

            // close cURL resource, and free up system resources
            curl_close($ch);
        }

        $this->viewBuilder()->layout('ajax');
    }

    /**
     * CARRINHO - CHECKOUT
     * ROTA::/mochila/checkout
     */
    public function carrinho_checkout($pedido_id = null, $cliente_id = null, $token = null){

        if($pedido_id == null || $cliente_id == null || $token == null) {
            $this->redirect('/home');
        }else{

            $Pedidos = TableRegistry::get('Pedidos');
            //$pedido = $Pedidos->get($pedido_id);
            $pedido = $Pedidos
                ->find('all')
                ->where(['Pedidos.id' => $pedido_id])
                ->first();

            $url_transacao = MOIP_AMBIENTE.'ws/alpha/ConsultarInstrucao/'.$token;

            $credentials = MOIP_TOKEN.':'.MOIP_KEY;

            $header[] = "Expect:";
            $header[] = "Authorization: Basic " . base64_encode($credentials);

            $ch = curl_init();
            $options = array(CURLOPT_URL => $url_transacao,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POST => false,
                //CURLOPT_POSTFIELDS => $xml,
                CURLOPT_RETURNTRANSFER => true,
                CURLINFO_HEADER_OUT => true
            );
            curl_setopt_array($ch, $options);

            // grab URL and pass it to the browser
            $detalhes = curl_exec($ch);
            $ret = curl_exec($ch);
            $err = curl_error($ch);
            $info = curl_getinfo($ch);

            // close cURL resource, and free up system resources
            curl_close($ch);

            @header('Content-Type: text/html; charset=utf-8');
            $xml = \simplexml_load_string($detalhes);
            $xml = json_encode($xml);
            $xml = json_decode($xml, true);

            $data_pago = [];

            if(isset($xml['RespostaConsultar']['Autorizacao']['Pagamento']['Status'])){
                $status_pedido = $xml['RespostaConsultar']['Autorizacao']['Pagamento']['Status'];
                if($status_pedido == 'Iniciado' || $status_pedido == 'BoletoImpresso' || $status_pedido == 'EmAnalise'){
                    $status_pedido = 2;
                }elseif($status_pedido == 'Autorizado' || $status_pedido == 'Concluido'){
                    $status_pedido = 3;
                    $data_pago = Time::now();
                }elseif($status_pedido == 'Cancelado'){
                    $status_pedido = 7;
                }else{
                    $status_pedido = 1;
                }
            }else{
                $status_pedido = 2;
            }

            $data = [
                'data_pagamento'    => $data_pago,
                'pedido_status_id'  => $status_pedido,
                'payment_method'    => $xml['RespostaConsultar']['Autorizacao']['Pagamento']['FormaPagamento'],
                'transaction_data'  => serialize($detalhes),
            ];

            $pedido = $Pedidos->patchEntity($pedido, $data);

            $session = $this->request->session();

            if($PedidoSave = $Pedidos->save($pedido)){

                $session->delete('PedidoFinalizado');
                $session->delete('PedidoFinalizadoItem');
                $session->delete('ClienteFinalizado');

                $this->redirect('/mochila/finalizado/'.$pedido_id);
            }else{
                $this->redirect('/mochila/fechar-o-ziper');
            }

            $this->set(compact('token'));
        }
    }

    /**
     * CARRINHO - FINALIZADO
     * @param null $pedido_id
     */
    public function carrinho_finalizado($pedido_id = null){
        $Pedidos = TableRegistry::get('Pedidos');

        $session = $this->request->session();

        $AcademiaSession = $session->read('Academia');

        $pedido = $Pedidos
            ->find('all', ['contain' => [
                'Academias',
                'Clientes',
                'PedidoStatus',
                'PedidoItens',
                'PedidoItens.Produtos',
                'PedidoItens.Produtos.ProdutoBase',
                'PedidoItens.Produtos.ProdutoBase.Marcas'
            ]
            ])
            ->where(['Pedidos.id' => $pedido_id])
            ->first(); 

        $this->set(compact('pedido', 'AcademiaSession'));

    }

    /**
     * CARRINHO - FINALIZADO
     * @param null $pedido_id
     */
    public function carrinho_finalizado_express($pedido_id = null){
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $Pedidos = TableRegistry::get('Pedidos');

        $pedido = $Pedidos
            ->find('all', ['contain' => [
                'Academias',
                'Clientes',
                'PedidoStatus',
                'PedidoItens',
                'PedidoItens.Produtos',
                'PedidoItens.Produtos.ProdutoBase',
                'PedidoItens.Produtos.ProdutoBase.Marcas'
            ]
            ])
            ->where(['Pedidos.id' => $pedido_id])
            ->first(); 

        $this->set(compact('pedido', 'Academia'));
        $this->viewBuilder()->layout('default6');
    }

    public function search(){

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1); 

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        $marca_selected = TableRegistry::get('Marcas')
            ->find('all')
            ->where(['Marcas.id' => $marca_id])
            ->first();

        $objetivo_selected = TableRegistry::get('Objetivos')
            ->find('all')
            ->where(['Objetivos.id' => $objetivo_id])
            ->first();

        $categoria_selected = TableRegistry::get('Categorias')
            ->find('all')
            ->where(['Categorias.id' => $categoria_id])
            ->first();

        if($this->request->is(['post', 'put'])){

            $query = $this->request->data['query'];

            if($query != null) {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->orWhere([
                        ['ProdutoBase.name LIKE' => '%'.$query.'%'],
                    ])
                    ->order(['Produtos.estoque' => 'desc', 'Produtos.propriedade' => 'asc'])
                    ->all();
            } else {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->order('rand()')
                    ->all();
            }

            $prod_objetivos = $produtos;

            $prod_objetivos = $ProdutoObjetivos
                ->find('all', ['contain' => ['Objetivos']])
                ->all();   

        }else{
            $produtos = [];
        }
        $this->set(compact('prod_objetivos', 'max_date_novidade', 'produtos', 'marca_selected', 'objetivo_selected', 'categoria_selected'));


        $this->viewBuilder()->layout('ajax');
    }

    public function limpar_filtro(){

        $session = $this->request->session();

        $Academia = $session->read('Academia');

        $SSlug = $Academia->slug;

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1); 

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        $produtos = TableRegistry::get('Produtos')
            ->find('all', ['contain' => [
                'ProdutoBase',
                'ProdutoBase.Propriedades',
            ]])
            ->where(['Produtos.preco > '     => 0.1])
            ->andWhere(['Produtos.visivel'      => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->order(['Produtos.status_id' => 'asc', 'rand()'])
            ->limit(24)
            ->all();

        $produtos_exibidos = [];

        foreach ($produtos as $key) {
            $produtos_exibidos[] = $key->id;
        }

        $session = $this->request->session();

        $session->write('ProdutosExibidos', $produtos_exibidos);     

        $prod_objetivos = $produtos;

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();       

        $this->set(compact('prod_objetivos', 'max_date_novidade', 'produtos', 'SSlug'));

        $this->viewBuilder()->layout('ajax');
    }

    public function limpar_filtro_express(){

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1); 

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        $produtos = TableRegistry::get('Produtos')
            ->find('all', ['contain' => [
                'ProdutoBase',
                'ProdutoBase.Propriedades',
            ]])
            ->where(['Produtos.preco > '     => 0.1])
            ->andWhere(['Produtos.visivel'      => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->order(['Produtos.status_id' => 'asc', 'rand()'])
            ->limit(24)
            ->all();

        $produtos_exibidos = [];

        foreach ($produtos as $key) {
            $produtos_exibidos[] = $key->id;
        }

        $session = $this->request->session();

        $session->write('ProdutosExibidos', $produtos_exibidos);     

        $prod_objetivos = $produtos;

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();       

        $this->set(compact('prod_objetivos', 'max_date_novidade', 'produtos'));

        $this->viewBuilder()->layout('ajax');
    }

    public function filter_objetivo_categoria($objetivo_id = null, $categoria_id = null){

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1); 

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        $marca_selected = TableRegistry::get('Marcas')
            ->find('all')
            ->where(['Marcas.id' => $marca_id])
            ->first();

        $objetivo_selected = TableRegistry::get('Objetivos')
            ->find('all')
            ->where(['Objetivos.id' => $objetivo_id])
            ->first();

        $categoria_selected = TableRegistry::get('Categorias')
            ->find('all')
            ->where(['Categorias.id' => $categoria_id])
            ->first();

        if($objetivo_id == null || $categoria_id == null){
            $produtos = [];
        }else{

            $produtos = $this->Produtos
                ->find('all', ['contain' => [
                    'ProdutoBase'
                ]])
                ->innerJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                ->innerJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                ->where(['Produtos.status_id'       => 1])
                ->andWhere(['Produtos.preco > '     => 0.1])
                ->andWhere(['Produtos.visivel'      => 1])
                ->andWhere(['ProdutoCategorias.categoria_id' => $categoria_id])
                ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo_id])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->order(['Produtos.preco' => 'asc', 'Produtos.propriedade' => 'asc'])
                ->all();  
        }

        $prod_objetivos = $produtos;

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();

        $this->set(compact('prod_objetivos', 'max_date_novidade', 'produtos', 'marca_selected', 'objetivo_selected', 'categoria_selected'));

        $this->viewBuilder()->layout('ajax');
    }

    public function filter_objetivo_categoria_ordenar($objetivo_id = null, $categoria_id = null, $ordenar_id = null, $valor_minimo = null, $valor_maximo = null){

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1); 

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        $marca_selected = TableRegistry::get('Marcas')
            ->find('all')
            ->where(['Marcas.id' => $marca_id])
            ->first();

        $objetivo_selected = TableRegistry::get('Objetivos')
            ->find('all')
            ->where(['Objetivos.id' => $objetivo_id])
            ->first();

        $categoria_selected = TableRegistry::get('Categorias')
            ->find('all')
            ->where(['Categorias.id' => $categoria_id])
            ->first();

        if($objetivo_id == null || $categoria_id == null){
            $produtos = [];
        }else{

            if($ordenar_id == 1) {
                $ordenar_id = 'desc';
            } else if($ordenar_id == 2) {
                $ordenar_id = 'asc';
            } else if($ordenar_id == 3) {
                $ordenar_id = 'novidades';
            } else if($ordenar_id == 4) {
                $ordenar_id = 'promocoes';
            } else {
                $ordenar_id = 'rand()';
            }

            /*if($ordenar_id != 2) {
                $ordenar_id = 'desc';
            } else {
                $ordenar_id = 'asc';
            }*/

            if($ordenar_id == 'rand()') {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoCategorias.categoria_id' => $categoria_id])
                    ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo_id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order($ordenar_id)
                    ->all();
            } else if($ordenar_id == 'novidades') {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoCategorias.categoria_id' => $categoria_id])
                    ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo_id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order(['Produtos.created' => 'desc'])
                    ->all();
            } else if($ordenar_id == 'promocoes') {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoCategorias.categoria_id' => $categoria_id])
                    ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo_id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order(['Produtos.preco_promo' => 'desc'])
                    ->all();
            } else {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoCategorias.categoria_id' => $categoria_id])
                    ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo_id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order(['Produtos.preco' => $ordenar_id, 'Produtos.propriedade' => 'asc'])
                    ->all();
            }
        }

        $prod_objetivos = $produtos;

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();

        $this->set(compact('prod_objetivos', 'max_date_novidade', 'produtos', 'marca_selected', 'objetivo_selected', 'categoria_selected'));

        $this->viewBuilder()->layout('ajax');
    }

    public function search_completo(){

        $session = $this->request->session();

        $Academia = $session->read('Academia');

        $SSlug = $Academia->slug;

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1); 

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        if($this->request->is(['post', 'put'])){

            $query     = $this->request->data['query'];
            $valor_min = $this->request->data['valor_min'];
            $valor_max = $this->request->data['valor_max'];
            $categoria = $this->request->data['categoria'];
            $marca     = $this->request->data['marca'];
            $objetivo  = $this->request->data['objetivo'];
            $ordenar   = $this->request->data['ordenar'];

            $marca_selected = TableRegistry::get('Marcas')
                ->find('all')
                ->where(['Marcas.id' => $marca])
                ->first();

            $objetivo_selected = TableRegistry::get('Objetivos')
                ->find('all')
                ->where(['Objetivos.id' => $objetivo])
                ->first();

            $categoria_selected = TableRegistry::get('Categorias')
                ->find('all')
                ->where(['Categorias.id' => $categoria])
                ->first();

            if($ordenar == 1) {
                $ordenar = 'desc';
            } else if($ordenar == 2) {
                $ordenar = 'asc';
            } else if($ordenar == 3) {
                $ordenar = 'novidades';
            } else if($ordenar == 4) {
                $ordenar = 'promocoes';
            } else {
                $ordenar = 'rand()';
            }

            if($query != null) {

                $query = str_replace(' ', '%', $query);

                $categorias_busca = TableRegistry::get('Categorias')
                    ->find('all')
                    ->where(['Categorias.status_id' => 1])
                    ->andWhere(['Categorias.name LIKE' => '%'.$query.'%'])
                    ->first();

                if(count($categorias_busca) >= 1) {
                    $categoria = $categorias_busca->id;
                    $objetivo = [1, 2, 3, 4];
                }

                $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

                if($objetivo == 0 || $categoria == 0){

                    if($marca == 0) {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    } else {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    }

                }else{

                    if(count($categorias_busca) >= 1) {
                        if($ordenar == 'rand()') {
                            $produtos_a = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all(); 

                            $produtos_b = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all(); 

                            foreach ($produtos_a as $pa) {
                                $ja_ta_no_array = 0;
                                foreach ($produtos as $prod) {
                                    if($prod->id == $pa->id) {
                                        $ja_ta_no_array = 1;
                                    }
                                }

                                if($ja_ta_no_array == 0) {
                                    $produtos[] = $pa;
                                }
                            }

                            foreach ($produtos_b as $pb) {
                                $ja_ta_no_array = 0;
                                foreach ($produtos as $prod) {
                                    if($prod->id == $pb->id) {
                                        $ja_ta_no_array = 1;
                                    }
                                }

                                if($ja_ta_no_array == 0) {
                                    $produtos[] = $pb;
                                }
                            }

                        } else if($ordenar == 'novidades') {
                            $produtos_a = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();

                            $produtos_b = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all(); 

                            foreach ($produtos_a as $pa) {
                                $ja_ta_no_array = 0;
                                foreach ($produtos as $prod) {
                                    if($prod->id == $pa->id) {
                                        $ja_ta_no_array = 1;
                                    }
                                }

                                if($ja_ta_no_array == 0) {
                                    $produtos[] = $pa;
                                }
                            }

                            foreach ($produtos_b as $pb) {
                                $ja_ta_no_array = 0;
                                foreach ($produtos as $prod) {
                                    if($prod->id == $pb->id) {
                                        $ja_ta_no_array = 1;
                                    }
                                }

                                if($ja_ta_no_array == 0) {
                                    $produtos[] = $pb;
                                }
                            }


                        } else if($ordenar == 'promocoes') {
                            $produtos_a = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();

                            $produtos_b = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all(); 

                            foreach ($produtos_a as $pa) {
                                $ja_ta_no_array = 0;
                                foreach ($produtos as $prod) {
                                    if($prod->id == $pa->id) {
                                        $ja_ta_no_array = 1;
                                    }
                                }

                                if($ja_ta_no_array == 0) {
                                    $produtos[] = $pa;
                                }
                            }

                            foreach ($produtos_b as $pb) {
                                $ja_ta_no_array = 0;
                                foreach ($produtos as $prod) {
                                    if($prod->id == $pb->id) {
                                        $ja_ta_no_array = 1;
                                    }
                                }

                                if($ja_ta_no_array == 0) {
                                    $produtos[] = $pb;
                                }
                            }


                        } else {
                            $produtos_a = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();

                            $produtos_b = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all(); 

                            foreach ($produtos_a as $pa) {
                                $ja_ta_no_array = 0;
                                foreach ($produtos as $prod) {
                                    if($prod->id == $pa->id) {
                                        $ja_ta_no_array = 1;
                                    }
                                }

                                if($ja_ta_no_array == 0) {
                                    $produtos[] = $pa;
                                }
                            }

                            foreach ($produtos_b as $pb) {
                                $ja_ta_no_array = 0;
                                foreach ($produtos as $prod) {
                                    if($prod->id == $pb->id) {
                                        $ja_ta_no_array = 1;
                                    }
                                }

                                if($ja_ta_no_array == 0) {
                                    $produtos[] = $pb;
                                }
                            }
                        }
                    } else {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all(); 
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    }

                }

            } else {
                $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

                if($objetivo == 0 || $categoria == 0){
                    if($marca == 0) {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    } else {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    }

                }else{

                    if($ordenar == 'rand()') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.status_id' => 'asc', 'rand()'])
                            ->limit(24)
                            ->all();
                    } else if($ordenar == 'novidades') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.created' => 'desc'])
                            ->limit(24)
                            ->all();
                    } else if($ordenar == 'promocoes') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.preco_promo' => 'desc'])
                            ->limit(24)
                            ->all();
                    } else {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                            ->limit(24)
                            ->all();
                    }
                }
            }

            $produtos_exibidos = [];

            $contador = 0;

            foreach ($produtos as $key) {
                $contador++;
                $produtos_exibidos[] = $key->id;
            }

            $prod_objetivos = $produtos;

            $prod_objetivos = $ProdutoObjetivos
                ->find('all', ['contain' => ['Objetivos']])
                ->all();   

            $query = str_replace('%', ' ', $query);

        }else{
            $produtos = [];
        }

        $session = $this->request->session();

        $session->write('ProdutosExibidos', $produtos_exibidos);

        $this->set(compact('contador', 'marca_selected', 'prod_objetivos', 'max_date_novidade', 'produtos', 'query', 'objetivo_selected', 'categoria_selected', 'SSlug'));


        $this->viewBuilder()->layout('ajax');
    }

    public function search_professor(){

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1); 

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        if($this->request->is(['post', 'put'])){

            $query     = $this->request->data['query'];
            $valor_min = $this->request->data['valor_min'];
            $valor_max = $this->request->data['valor_max'];
            $categoria = $this->request->data['categoria'];
            $marca     = $this->request->data['marca'];
            $objetivo  = $this->request->data['objetivo'];
            $ordenar   = $this->request->data['ordenar'];

            $marca_selected = TableRegistry::get('Marcas')
                ->find('all')
                ->where(['Marcas.id' => $marca])
                ->first();

            $objetivo_selected = TableRegistry::get('Objetivos')
                ->find('all')
                ->where(['Objetivos.id' => $objetivo])
                ->first();

            $categoria_selected = TableRegistry::get('Categorias')
                ->find('all')
                ->where(['Categorias.id' => $categoria])
                ->first();

            if($ordenar == 1) {
                $ordenar = 'desc';
            } else if($ordenar == 2) {
                $ordenar = 'asc';
            } else if($ordenar == 3) {
                $ordenar = 'novidades';
            } else if($ordenar == 4) {
                $ordenar = 'promocoes';
            } else {
                $ordenar = 'rand()';
            }

            if($query != null) {

                $categorias_busca = TableRegistry::get('Categorias')
                    ->find('all')
                    ->where(['Categorias.status_id' => 1])
                    ->andWhere(['Categorias.name LIKE' => '%'.$query.'%'])
                    ->first();

                if(count($categorias_busca) >= 1) {
                    $categoria = $categorias_busca->id;
                    $objetivo = [1, 2, 3, 4];
                }

                $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

                if($objetivo == 0 || $categoria == 0){

                    if($marca == 0) {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    } else {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    }

                }else{

                    if(count($categorias_busca) >= 1) {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all(); 
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    } else {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all(); 
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    }
                }

            } else {
                $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

                if($objetivo == 0 || $categoria == 0){
                    if($marca == 0) {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    } else {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    }

                }else{

                    if($ordenar == 'rand()') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.status_id' => 'asc', 'rand()'])
                            ->limit(24)
                            ->all();
                    } else if($ordenar == 'novidades') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.created' => 'desc'])
                            ->limit(24)
                            ->all();
                    } else if($ordenar == 'promocoes') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.preco_promo' => 'desc'])
                            ->limit(24)
                            ->all();
                    } else {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                            ->limit(24)
                            ->all();
                    }
                }
            }

            $produtos_exibidos = [];

            $contador = 0;

            foreach ($produtos as $key) {
                $contador++;
                $produtos_exibidos[] = $key->id;
            }

            $prod_objetivos = $produtos;

            $prod_objetivos = $ProdutoObjetivos
                ->find('all', ['contain' => ['Objetivos']])
                ->all();   

        }else{
            $produtos = [];
        }

        $session = $this->request->session();

        $session->write('ProdutosExibidos', $produtos_exibidos);

        $this->set(compact('contador', 'marca_selected', 'prod_objetivos', 'max_date_novidade', 'produtos', 'query', 'objetivo_selected', 'categoria_selected'));


        $this->viewBuilder()->layout('ajax');
    }

    public function search_express(){

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1); 

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        if($this->request->is(['post', 'put'])){

            $query     = $this->request->data['query'];
            $valor_min = $this->request->data['valor_min'];
            $valor_max = $this->request->data['valor_max'];
            $categoria = $this->request->data['categoria'];
            $marca     = $this->request->data['marca'];
            $objetivo  = $this->request->data['objetivo'];
            $ordenar   = $this->request->data['ordenar'];

            $marca_selected = TableRegistry::get('Marcas')
                ->find('all')
                ->where(['Marcas.id' => $marca])
                ->first();

            $objetivo_selected = TableRegistry::get('Objetivos')
                ->find('all')
                ->where(['Objetivos.id' => $objetivo])
                ->first();

            $categoria_selected = TableRegistry::get('Categorias')
                ->find('all')
                ->where(['Categorias.id' => $categoria])
                ->first();

            if($ordenar == 1) {
                $ordenar = 'desc';
            } else if($ordenar == 2) {
                $ordenar = 'asc';
            } else if($ordenar == 3) {
                $ordenar = 'novidades';
            } else if($ordenar == 4) {
                $ordenar = 'promocoes';
            } else {
                $ordenar = 'rand()';
            }

            if($query != null) {

                $categorias_busca = TableRegistry::get('Categorias')
                    ->find('all')
                    ->where(['Categorias.status_id' => 1])
                    ->andWhere(['Categorias.name LIKE' => '%'.$query.'%'])
                    ->first();

                if(count($categorias_busca) >= 1) {
                    $categoria = $categorias_busca->id;
                    $objetivo = [1, 2, 3, 4];
                }

                $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

                if($objetivo == 0 || $categoria == 0){

                    if($marca == 0) {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    } else {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['ProdutoBase.name LIKE' => '%'.$query.'%'])
                                ->distinct(['Produtos.id'])
                                ->orWhere(['Produtos.propriedade LIKE' => '%'.$query.'%'])
                                ->andWhere(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    }

                }else{

                    if(count($categorias_busca) >= 1) {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all(); 
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    } else {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all(); 
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoCategorias.categoria_id IN' => $categoria])
                                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivo])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->andWhere(['OR' => ['ProdutoBase.name LIKE' => '%'.$query.'%', 
                                    'Produtos.propriedade LIKE' => '%'.$query.'%']])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    }
                }

            } else {
                $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

                if($objetivo == 0 || $categoria == 0){
                    if($marca == 0) {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    } else {
                        if($ordenar == 'rand()') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'novidades') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.created' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else if($ordenar == 'promocoes') {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco_promo' => 'desc'])
                                ->limit(24)
                                ->all();
                        } else {
                            $produtos = $this->Produtos
                                ->find('all', ['contain' => [
                                    'ProdutoBase'
                                ]])
                                ->innerJoin(
                                    ['ProdutoCategorias' => 'produto_categorias'],
                                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                                ->innerJoin(
                                    ['ProdutoObjetivos' => 'produto_objetivos'],
                                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                                ->where(['Produtos.preco >= '     => $valor_min])
                                ->andWhere(['Produtos.preco <= '     => $valor_max])
                                ->andWhere(['Produtos.visivel'      => 1])
                                ->andWhere(['ProdutoBase.marca_id' => $marca])
                                ->andWhere(['ProdutoBase.status_id' => 1])
                                ->distinct(['Produtos.id'])
                                ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                                ->limit(24)
                                ->all();
                        }
                    }

                }else{

                    if($ordenar == 'rand()') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.status_id' => 'asc', 'rand()'])
                            ->limit(24)
                            ->all();
                    } else if($ordenar == 'novidades') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.created' => 'desc'])
                            ->limit(24)
                            ->all();
                    } else if($ordenar == 'promocoes') {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.preco_promo' => 'desc'])
                            ->limit(24)
                            ->all();
                    } else {
                        $produtos = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->innerJoin(
                                ['ProdutoCategorias' => 'produto_categorias'],
                                ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                            ->innerJoin(
                                ['ProdutoObjetivos' => 'produto_objetivos'],
                                ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                            ->where(['Produtos.preco >= '     => $valor_min])
                            ->andWhere(['Produtos.preco <= '     => $valor_max])
                            ->andWhere(['Produtos.visivel'      => 1])
                            ->andWhere(['ProdutoCategorias.categoria_id' => $categoria])
                            ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo])
                            ->andWhere(['ProdutoBase.status_id' => 1])
                            ->distinct(['Produtos.id'])
                            ->order(['Produtos.preco' => $ordenar, 'Produtos.propriedade' => 'asc'])
                            ->limit(24)
                            ->all();
                    }
                }
            }

            $produtos_exibidos = [];

            $contador = 0;

            foreach ($produtos as $key) {
                $contador++;
                $produtos_exibidos[] = $key->id;
            }

            $prod_objetivos = $produtos;

            $prod_objetivos = $ProdutoObjetivos
                ->find('all', ['contain' => ['Objetivos']])
                ->all();   

        }else{
            $produtos = [];
        }

        $session = $this->request->session();

        $session->write('ProdutosExibidos', $produtos_exibidos);

        $this->set(compact('contador', 'marca_selected', 'prod_objetivos', 'max_date_novidade', 'produtos', 'query', 'objetivo_selected', 'categoria_selected'));


        $this->viewBuilder()->layout('ajax');
    }

    public function filter_valor($ordenar_id = null, $valor_minimo = null, $valor_maximo = null){

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1); 

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        $marca_selected = TableRegistry::get('Marcas')
            ->find('all')
            ->where(['Marcas.id' => $marca_id])
            ->first();

        $objetivo_selected = TableRegistry::get('Objetivos')
            ->find('all')
            ->where(['Objetivos.id' => $objetivo_id])
            ->first();

        $categoria_selected = TableRegistry::get('Categorias')
            ->find('all')
            ->where(['Categorias.id' => $categoria_id])
            ->first();

        if($ordenar_id == 1) {
            $ordenar_id = 'desc';
        } else if($ordenar_id == 2) {
            $ordenar_id = 'asc';
        } else if($ordenar_id == 3) {
            $ordenar_id = 'novidades';
        } else if($ordenar_id == 4) {
            $ordenar_id = 'promocoes';
        } else {
            $ordenar_id = 'rand()';
        }

        if($valor_minimo == null || $valor_maximo == null){
            $produtos = [];
        }else{

            if($ordenar_id == 'rand()') {
                $produtos = $this->Produtos
                ->find('all', ['contain' => [
                    'ProdutoBase'
                ]])
                ->innerJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                ->innerJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                ->where(['Produtos.status_id'       => 1])
                ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                ->andWhere(['Produtos.visivel'      => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->order($ordenar_id)
                ->all();
            } else if($ordenar_id == 'novidades') {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order(['Produtos.created' => 'desc'])
                    ->all();
            } else if($ordenar_id == 'promocoes') {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order(['Produtos.preco_promo' => 'desc'])
                    ->all();
            } else {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order(['Produtos.preco' => $ordenar_id, 'Produtos.propriedade' => 'asc'])
                    ->all();
            }
        }

        $prod_objetivos = $produtos;

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();

        $this->set(compact('prod_objetivos', 'max_date_novidade', 'produtos', 'marca_selected', 'objetivo_selected', 'categoria_selected'));

        $this->viewBuilder()->layout('ajax');
    }

    public function filter_objetivo($objetivo_id = null){

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1); 

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        $marca_selected = TableRegistry::get('Marcas')
            ->find('all')
            ->where(['Marcas.id' => $marca_id])
            ->first();

        $objetivo_selected = TableRegistry::get('Objetivos')
            ->find('all')
            ->where(['Objetivos.id' => $objetivo_id])
            ->first();

        $categoria_selected = TableRegistry::get('Categorias')
            ->find('all')
            ->where(['Categorias.id' => $categoria_id])
            ->first();

        if($objetivo_id == null){
            $produtos = [];
        }else{

            $produtos = $this->Produtos
                ->find('all', ['contain' => [
                    'ProdutoBase'
                ]])
                ->innerJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                ->innerJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                ->where(['Produtos.status_id'       => 1])
                ->andWhere(['Produtos.preco > '     => 0.1])
                ->andWhere(['Produtos.visivel'      => 1])
                ->andWhere(['ProdutoObjetivos.objetivo_id' => $objetivo_id])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->order(['Produtos.preco' => 'asc', 'Produtos.propriedade' => 'asc'])
                ->all();
        }

        $prod_objetivos = $produtos;

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();

        $this->set(compact('prod_objetivos', 'max_date_novidade', 'produtos', 'marca_selected', 'objetivo_selected', 'categoria_selected'));

        $this->viewBuilder()->layout('ajax');
    }

    public function filter_ordenar($ordenar_id = null, $valor_minimo = null, $valor_maximo = null){

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1); 

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        $marca_selected = TableRegistry::get('Marcas')
            ->find('all')
            ->where(['Marcas.id' => $marca_id])
            ->first();

        $objetivo_selected = TableRegistry::get('Objetivos')
            ->find('all')
            ->where(['Objetivos.id' => $objetivo_id])
            ->first();

        $categoria_selected = TableRegistry::get('Categorias')
            ->find('all')
            ->where(['Categorias.id' => $categoria_id])
            ->first();

        if($ordenar_id == 1) {
            $ordenar_id = 'desc';
        } else if($ordenar_id == 2) {
            $ordenar_id = 'asc';
        } else if($ordenar_id == 3) {
            $ordenar_id = 'novidades';
        } else if($ordenar_id == 4) {
            $ordenar_id = 'promocoes';
        } else {
            $ordenar_id = 'rand()';
        }

        if($ordenar_id == null){
            $produtos = [];
        }else{

            if($ordenar_id == 'rand()') {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order($ordenar_id)
                    ->all();
            } else if($ordenar_id == 'novidades') {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order(['Produtos.created' => 'desc'])
                    ->all();
            } else if($ordenar_id == 'promocoes') {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order(['Produtos.preco_promo' => 'desc'])
                    ->all();
            } else {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->innerJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = Produtos.produto_base_id'])
                    ->innerJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = Produtos.produto_base_id'])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order(['Produtos.preco' => $ordenar_id, 'Produtos.propriedade' => 'asc'])
                    ->all();
            }
        }

        $prod_objetivos = $produtos;

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();

        $this->set(compact('prod_objetivos', 'max_date_novidade', 'produtos', 'marca_selected', 'objetivo_selected', 'categoria_selected'));

        $this->viewBuilder()->layout('ajax');
    }

    public function filter_marca($marca_id = null, $ordenar_id = null, $valor_minimo = null, $valor_maximo = null){

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1);   

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        $marca_selected = TableRegistry::get('Marcas')
            ->find('all')
            ->where(['Marcas.id' => $marca_id])
            ->first();

        $objetivo_selected = TableRegistry::get('Objetivos')
            ->find('all')
            ->where(['Objetivos.id' => $objetivo_id])
            ->first();

        $categoria_selected = TableRegistry::get('Categorias')
            ->find('all')
            ->where(['Categorias.id' => $categoria_id])
            ->first();

        if($ordenar_id == 1) {
            $ordenar_id = 'desc';
        } else if($ordenar_id == 2) {
            $ordenar_id = 'asc';
        } else if($ordenar_id == 3) {
            $ordenar_id = 'novidades';
        } else if($ordenar_id == 4) {
            $ordenar_id = 'promocoes';
        } else {
            $ordenar_id = 'rand()';
        }

        if($marca_id == null){
            $produtos = [];
        } if($marca_id != null && $ordenar_id == null && $valor_minimo == null && $valor_maximo == null) {
            $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco > '     => 0.1])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.marca_id'  => $marca_id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order('rand()')
                    ->all();

        } else{
            if($ordenar_id == 'rand()') {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.marca_id'  => $marca_id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order($ordenar_id)
                    ->all();
            } else if($ordenar_id == 'novidades') {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.marca_id'  => $marca_id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order(['Produtos.created' => 'desc'])
                    ->all();
            } else if($ordenar_id == 'promocoes') {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.marca_id'  => $marca_id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order(['Produtos.preco_promo' => 'desc'])
                    ->all();
            } else {
                $produtos = $this->Produtos
                    ->find('all', ['contain' => [
                        'ProdutoBase'
                    ]])
                    ->where(['Produtos.status_id'       => 1])
                    ->andWhere(['Produtos.preco >= '     => $valor_minimo])
                    ->andWhere(['Produtos.preco <= '     => $valor_maximo])
                    ->andWhere(['Produtos.visivel'      => 1])
                    ->andWhere(['ProdutoBase.marca_id'  => $marca_id])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->order(['Produtos.preco' => $ordenar_id, 'Produtos.propriedade' => 'asc'])
                    ->all();
            }
        }

        $prod_objetivos = $produtos;

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();

        $this->set(compact('prod_objetivos', 'max_date_novidade', 'produtos', 'marca_selected', 'objetivo_selected', 'categoria_selected'));

        $this->viewBuilder()->layout('ajax');
    }

    public function search_categoria($categoria = null, $objetivo = null){

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1);

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        $marca_selected = TableRegistry::get('Marcas')
            ->find('all')
            ->where(['Marcas.id' => $marca_id])
            ->first();

        $objetivo_selected = TableRegistry::get('Objetivos')
            ->find('all')
            ->where(['Objetivos.id' => $objetivo_id])
            ->first();

        $categoria_selected = TableRegistry::get('Categorias')
            ->find('all')
            ->where(['Categorias.id' => $categoria_id])
            ->first();

        $produtos = $this->Produtos
            ->find('all', ['contain' => [
                'ProdutoBase'
            ]])
            ->where(['Produtos.status_id' => 1])
            ->andWhere(['Produtos.visivel' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])

            ->order(['Produtos.estoque' => 'desc', 'Produtos.preco' => 'asc', 'Produtos.propriedade' => 'asc'])
            ->all();

        $prod_objetivos = $produtos;

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();

        $this->set(compact('prod_objetivos', 'max_date_novidade', 'produtos', 'marca_selected', 'objetivo_selected', 'categoria_selected'));

        $this->viewBuilder()->layout('ajax');
    }

    /**
     * MÉTODO DE PAGAMENTO - MOIP
     * @param null $pedido_id
     * @param float $total
     * @param $cliente
     * @return \Moip
     */
    private function _pagamento_moip($cliente, $pedido_id = null, $total = 0.00, $parcelas = 1, $transferir_taxa = true){

        //REQUISIÇÃO - ENVIO (REQUEST)
        include_once VENDOR . 'moiplabs' . DS . 'moip-php' . DS . 'lib' . DS . 'Moip.php';
        include_once VENDOR . 'moiplabs' . DS . 'moip-php' . DS . 'lib' . DS . 'MoipClient.php';
        include_once VENDOR . 'moiplabs' . DS . 'moip-php' . DS . 'lib' . DS . 'MoipStatus.php';

        $moip = new \Moip();
        $moip->setEnvironment();
        $moip->setCredential(array(
            'key' => MOIP_KEY,
            'token' => MOIP_TOKEN
        ));

        $moip->setUniqueID(date('Ymd').'_'.$pedido_id);
        $moip->setValue($total);
        $moip->setReason('LogFitness - Pedido #'.$pedido_id);

        $moip->setPayer(array(
            'name'              => $cliente->name,
            'email'             => $cliente->email,
            'payerId'           => $cliente->id,
            'billingAddress'    => array(
                'address'       => $cliente->address,
                'number'        => $cliente->number,
                'complement'    => $cliente->complement,
                'neighborhood'  => $cliente->area,
                'city'          => $cliente->city->name,
                'state'         => $cliente->city->uf,
                'country'       => 'BRA',
                'zipCode'       => str_replace('.', '', $cliente->cep),
                'phone'         => $cliente->telephone,
            ),
        ));

        if($parcelas > 1) {
            $moip->addParcel(1, $parcelas, null, $transferir_taxa);
        }

        $moip->validate('Identification');

        $moip->send();

        return $moip;

    }

    /**
     * FAIXA DE PARCELAMENTO - QUANTIDADE MÁXIMA DE PARCELAS
     * @param float $pedido_valor
     * @return int
     */
    private function _parcelas($pedido_valor = 0.00){
        /**
         * DEFINIR QUANTIDADE DE PARCELAS DE ACORDO COM O VALOR DA COMPRA
         */
        $session = $this->request->session();
        $config = $session->read('Configurations');

        $parcelas = 1;
        //if(isset($config['parcelas']) && isset($PedidoCliente)){
        if(isset($config['parcelas']) && $pedido_valor > 0){
            foreach ($config['parcelas'] as $p => $parcela):
                if(
                    $pedido_valor >= $config['parcelamento_inicial'][$p] &&
                    $pedido_valor <= $config['parcelamento_final'][$p]
                ):
                    $parcelas = $parcela;
                endif;
            endforeach;
        }

        return $parcelas;
        //$this->set(compact('parcelas'));
    }

    public function overlay_indicar_produtos() {

        $ProdutoIndicacoes = TableRegistry::get('ProdutoIndicacoes');
        $Produtos = TableRegistry::get('Produtos');

        if($this->request->is(['post', 'put'])){

            $session = $this->request->session();

            $academia = $session->read('Academia');

            $produto = $Produtos
                ->find('all', ['contain' => [
                    'ProdutoBase'
                ]])
                ->where(['Produtos.visivel'      => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->andWhere(['Produtos.slug' => $this->request->data['produto_slug']])
                ->first();

            $this->request->data['aceitou'] = 0;
            $url_antiga = $this->request->data['url'];

            $p_indicacao = $ProdutoIndicacoes->newEntity();
            $p_indicacao = $ProdutoIndicacoes->patchEntity($p_indicacao, $this->request->data);
            
            if($indicacao_save = $ProdutoIndicacoes->save($p_indicacao)) {
                $url = $this->request->data['url'].'?ind_id='.$indicacao_save->id;

                $data = [
                    'url' => $url
                ];

                $indicacao_save = $ProdutoIndicacoes->patchEntity($indicacao_save, $data);

                if($ProdutoIndicacoes->save($indicacao_save)) {
                    $msg = 'Produto indicado com sucesso!';
                } else {
                    $msg = 'Falha ao indicar o produto, tente novamente!';
                }  

                Email::configTransport('cliente', [
                    'className' => 'Smtp',
                    'host' => 'mail.logfitness.com.br',
                    'port' => 587,
                    'timeout' => 30,
                    'username' => EMAIL_USER,
                    'password' => EMAIL_SENHA,
                    'client' => null,
                    'tls' => null,
                ]);

                $email = new Email();

                $email
                    ->transport('cliente')
                    ->template('indicacao_produto_single')
                    ->emailFormat('html')
                    ->from([EMAIL_USER => EMAIL_NAME])
                    ->to($this->request->data['email'])
                    ->subject($academia->shortname.' - '.EMAIL_NAME.' - Indicação de produto')
                    ->set([
                        'quem_indicou'      => $indicacao_save->name,
                        'academia'          => $academia->shortname,
                        'logo_academia'     => $academia->image,
                        'produto'           => $produto,
                        'academia_id'       => $academia->id,
                        'url'               => $indicacao_save->url,
                        'email_contato'     => EMAIL_CONTATO
                    ])
                    ->send();
            }
        }
        
        $this->set(compact('msg', 'url_antiga'));

        $this->viewBuilder()->layout('ajax');
    }

    public function overlay_indicar_produtos_home() {

        $ProdutoIndicacoes = TableRegistry::get('ProdutoIndicacoes');
        $Produtos = TableRegistry::get('Produtos');

        if($this->request->is(['post', 'put'])){

            $status = 0;

            $session = $this->request->session();

            $session->start();

            $academia = $session->read('Academia');

            $produto = $Produtos
                ->find('all', ['contain' => [
                    'ProdutoBase'
                ]])
                ->where(['Produtos.visivel'      => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->andWhere(['Produtos.id' => $this->request->data['produto_id']])
                ->first();

            Email::configTransport('cliente', [
                'className' => 'Smtp',
                'host' => 'mail.logfitness.com.br',
                'port' => 587,
                'timeout' => 30,
                'username' => EMAIL_USER,
                'password' => EMAIL_SENHA,
                'client' => null,
                'tls' => null,
            ]);

            $email = new Email();

            $email
                ->transport('cliente')
                ->template('indicacao_produto_single')
                ->emailFormat('html')
                ->from([EMAIL_USER => EMAIL_NAME])
                ->to($this->request->data['email'])
                ->subject($academia->shortname.' - '.EMAIL_NAME.' - Indicação de produto')
                ->set([
                    'quem_indicou'      => $this->request->data['name'],
                    'academia'          => $academia->shortname,
                    'logo_academia'     => $academia->image,
                    'produto'           => $produto,
                    'academia_id'       => $academia->id,
                    'url'               => WEBROOT_URL.$academia->slug.'/produto/'.$produto->slug,
                    'email_contato'     => EMAIL_CONTATO
                ])
                ->send();

            $status = 1;
        }
        
        $this->set(compact('status'));

        $this->viewBuilder()->layout('ajax');
    }

    public function produto_acabou() {

        if($this->request->is(['post', 'put'])){

            $ProdutoAviseme = TableRegistry::get('ProdutoAviseme');
            $Produtos = TableRegistry::get('Produtos');

            $p_aviseme = $ProdutoAviseme->newEntity();
            $p_aviseme = $ProdutoAviseme->patchEntity($p_aviseme, $this->request->data);

            if($aviseme_save = $ProdutoAviseme->save($p_aviseme)) {
                $msg = $aviseme_save->produto_id;
            } else {
                $msg = 0;
            }
            
            $this->set(compact('msg'));

            $this->viewBuilder()->layout('ajax');
        }
    }

    public function log_express_pagamento() {

        $session = $this->request->session();
        $session_cliente = $session->read('Cliente');
        
        if(empty($session_cliente)) {
            $session_cliente = $session->read('UsuarioDados');
        }

        /**
         * CLIENTE NÃO LOGADO
         */
        if(empty($session_cliente)){
            return $this->redirect('/mochila/usuario-preencher-dados/1?valor_express='.$this->request->data['valor_express']);
        }


        $valor_express = $_GET['valor_express'];

        if($this->request->data['pagarExpress'] == 1) {
            $this->iugu_cobranca_direta($this->request->data['pedidoExpressPagar']);
        } else if(!empty($valor_express) && $valor_express > 0.0) {

            $valor_express = str_replace(".","",$valor_express);
            $valor_express = str_replace(",",".",$valor_express);

            $session->delete('Carrinho');
            $session->delete('ClienteFinalizado');

            $session    = $this->request->session();

            $PreOrders = TableRegistry::get('PreOrders');

            $session->read('PreOrder') ? $pre_order_id = (int)$session->read('PreOrder') : $pre_order_id = 0;

            $config = $session->read('Configurations');
            isset($config['transferir_taxa_parcelamento'])
                ? $taxa_parcelamento = (boolean)$config['transferir_taxa_parcelamento']
                : $taxa_parcelamento = true;

            $academia_url = explode('/', $_SERVER['REQUEST_URI']);

            if($academia_url[1] == 'mochila') {
                $academia_url[1] = '';
            }

            /**
             * CLIENTE NÃO LOGADO
             */
            if(empty($session_cliente)){
                $this->redirect($academia_url[1].'/login/mochila__fechar-o-ziper');
            }
            /**
             * CLIENTE LOGADO NO SISTEMA
             */
            else{

                $Pedidos = TableRegistry::get('Pedidos');

                $cliente = TableRegistry::get('Clientes')
                    ->find('all')
                    ->where(['Clientes.id' => $session_cliente->id])
                    ->contain(['Cities', 'Academias', 'Academias.Cities'])
                    ->first();

                /**
                 * RETORMAR PEDIDO JÁ REALIZADO
                 */
                if($retormar_pedido == 'retormar-pedido' && $pedido_id >= 1){
                    $this->set('completar_cadastro', false);

                    $PedidoCliente  = $Pedidos
                        ->find('all', ['contain' => [
                            'Academias',
                            'Academias.Cities',
                            'Clientes',
                            'PedidoItens',
                            'PedidoItens.Produtos',
                            'PedidoItens.Produtos.ProdutoBase',
                            'PedidoItens.Produtos.ProdutoBase.Marcas'
                        ]
                        ])
                        ->where(['Pedidos.id' => $pedido_id])
                        ->first(); 

                    $p_indisponivel = 0;

                    foreach ($PedidoCliente->pedido_itens as $p_itens) {
                        if($p_itens->produto->status_id == 2) {
                            $p_indisponivel = 1;
                        }
                    }

                    if($p_indisponivel == 1) {

                        $session = $this->request->session();

                        $cliente = $session->read('Cliente');

                        $Pedidos = TableRegistry::get('Pedidos');

                        $s_pedidos = $Pedidos
                            ->find('all')
                            ->contain([
                                'PedidoItens',
                            ])
                            ->where(['Pedidos.cliente_id' => $cliente->id])
                            ->andWhere(['Pedidos.id' => $pedido_id])
                            ->first();

                        $session->delete('Carrinho');

                        foreach ($s_pedidos->pedido_itens as $item):
                            if($item->quantidade > 0):
                                $qtd_produtos_pedido = $qtd_produtos_pedido + 1;
                                $session->write('Carrinho.'.$item->produto_id, $item->quantidade);
                            endif;
                        endforeach;

                        $session->write('Pedido', $pedido_id);
         
                        $session_produto    = $session->read('Carrinho');

                        $session_produto_ids = [];
                        if(!empty($session_produto)) {
                            foreach ($session_produto as $key => $item) {
                                $session_produto_ids[] = $key;
                            }
                        }

                        $produtos_inativos = TableRegistry::get('Produtos')
                                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                                ->andWhere(['Produtos.preco > ' => 0.1])
                                ->andWhere(['Produtos.status_id' => 2])
                                ->where(['Produtos.id IN' => $session_produto_ids])
                                ->all();

                        foreach ($produtos_inativos as $pi) {
                            $session->delete('Carrinho.'.$pi->id);
                        }  

                        if(!empty($session_produto_ids)) {
                            $s_produtos = $this->Produtos
                                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                                ->andWhere(['Produtos.preco > ' => 0.1])
                                ->andWhere(['Produtos.status_id' => 1])
                                ->where(['Produtos.id IN' => $session_produto_ids])
                                ->all();
                        }else{
                            $s_produtos = [];
                        }

                        $this->set(compact('alerta_inativo', 's_produtos', 'session_produto', 'session_cliente'));
                        $this->redirect('/home');
                    } else {
                        if(count($PedidoCliente) >= 1) {
                            //$this->iugu_cobranca_direta($PedidoCliente->id);
                            $this->set(compact('PedidoCliente', 'cliente', 'token'));
                        }else{
                            $this->redirect('/mochila');

                        }
                    }
                }
                /**
                 * CONTINUAR COM PEDIDO NOVO
                 */
                else{
                    /**
                     * CLIENTE COM CADASTRO INCOMPLETO
                     */
                    if(
                        !isset($session_cliente->cpf) ||
                        !isset($session_cliente->academia_id) ||
                        !isset($session_cliente->city_id)
                    ){
                        $this->set('completar_cadastro', true);

                        $states     = TableRegistry::get('States')->find('list');

                        if(isset($cliente->city->state_id)) {
                            $cities = TableRegistry::get('Cities')
                                ->find('list')
                                ->where(['state_id' => $cliente->city->state_id]);
                        }else{
                            $cities = [];
                        }
                        $academias  = TableRegistry::get('Academias')
                            ->find('list');

                        $this->set(compact('states', 'cliente', 'academias', 'cities'));
                    }
                    /**
                     * CLIENTE COM CADASTRO OK - PROSSEGUIR COM A FINALIZAÇÃO
                     */
                    else{
                        $this->set('completar_cadastro', false);
                        $session_carrinho = $session->read('Carrinho');

                        $session_carrinho = $this->Produtos
                            ->find('all', ['contain' => [
                                'ProdutoBase'
                            ]])
                            ->where(['Produtos.slug' => 'produto-express'])
                            ->first();

                        $pedido = $Pedidos->newEntity();

                        $valor = 0;

                        $produto_carrinho_total = 0;

                        $valor = $this->Produtos->find('all')->where(['id' => $session_carrinho->id])->first('preco_promo');
                        if($valor->preco_promo < 1) {
                            $produto_carrinho = $this->Produtos->find('all')->where(['id' => $session_carrinho->id])->sumOf('preco');
                        } else {
                            $produto_carrinho = $this->Produtos->find('all')->where(['id' => $session_carrinho->id])->sumOf('preco_promo');
                        }

                        $produto_carrinho_total += ($item * $produto_carrinho);
                        $valor += $produto_carrinho_total - 1;

                        $cupom = $session->read('CupomDesconto');

                        $cupom_usado = TableRegistry::get('Pedidos')
                            ->find('all')
                            ->where(['Pedidos.cliente_id' => $cliente->id])
                            ->andWhere(['Pedidos.cupom_desconto_id' => $cupom->id])
                            ->andWhere(['Pedidos.pedido_status_id >=' => 3])
                            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
                            ->first();

                        if(count($cupom_usado) >= 1) {
                            $cupom = [];
                        }

                        if(count($cupom) > 0) {
                            $desconto = 1.0 - ($cupom->valor * .01);
                            $valor = $valor * $desconto;
                        }

                        debug($valor);

                        $academia_user = TableRegistry::get('Academias')
                            ->find('all')
                            ->where(['id' => $session_cliente->academia_id])
                            ->first();

                        if($academia_user->user_id != null) {
                            $user = TableRegistry::get('Users')
                                ->find('all')
                                ->where(['id' => $academia_user->user_id])
                                ->first();

                            if($user->group_id == 7) {
                                $usuario_id = $user->id;
                            } else {
                                $usuario_id = null;
                            }
                        }

                        $data = [
                            'cliente_id'        => $session_cliente->id,
                            'pedido_status_id'  => 1,
                            'cupom_desconto_id' => $cupom->id,
                            'frete'             => 0,
                            'indicacao'         => $_COOKIE['indicacao'] ? $_COOKIE['indicacao'] : '',
                            'academia_id'       => $session_cliente->academia_id,
                            'professor_id'      => $session_cliente->professor_id,
                            'user_id'           => $usuario_id,
                            'valor'             => (float)$valor_express,
                            'comissao_status'   => 3
                        ];

                        $pedido = $Pedidos->patchEntity($pedido, $data);

                        $session->delete('CupomDesconto');

                        /**
                         * SE O PEDIDO AINDA NÃO FOI REGISTRADO NO SISTEMA
                         */
                        if(!$session->read('PedidoFinalizado') &&
                            !$session->read('PedidoFinalizadoItem') &&
                            !$session->read('ClienteFinalizado') && 
                            !$retormar_pedido == 'retormar-pedido'){

                            if ($PEDIDO_SAVE = $Pedidos->save($pedido)){

                                $PEDIDO_SAVE_ID = ['id' => (20600000 + $PEDIDO_SAVE->id)];

                                $PEDIDO_SAVE = $Pedidos->patchEntity($PEDIDO_SAVE, $PEDIDO_SAVE_ID);
                                if($PEDIDO = $Pedidos->save($PEDIDO_SAVE)){

                                    $session->write('PedidoFinalizado', $PEDIDO);

                                    $PedidoItens = TableRegistry::get('PedidoItens');

                                    //$total = 0;

                                    $produto = $this->Produtos
                                        ->find('all')
                                        ->where(['Produtos.slug' => 'produto-express'])
                                        ->first();

                                    if ($produto) {
                                        $produto->preco_promo ?
                                            $preco = $produto->preco_promo :
                                            $preco = $produto->preco;

                                        $pedido_item = $PedidoItens->newEntity();
                                        $item_data = [
                                            'pedido_id' => $PEDIDO->id,
                                            'produto_id' => $produto->id,
                                            'quantidade' => 1,
                                            'desconto' => 0,
                                            'preco' => $valor_express,
                                        ];

                                        $pedido_item = $PedidoItens->patchEntity($pedido_item, $item_data);

                                        if ($PEDIDO_ITEM_SAVE = $PedidoItens->save($pedido_item)) {
                                            $session->write('PedidoFinalizadoItem.'.$PEDIDO_ITEM_SAVE->id, $PEDIDO_ITEM_SAVE);
                                            //$total += ($item_data['quantidade'] * $item_data['preco']) - $item_data['desconto'];

                                            if($pre_order_id > 0):
                                                $pre_order = $PreOrders
                                                    ->find('all')
                                                    ->where(['PreOrders.id' => $pre_order_id])
                                                    ->first();

                                                $pre_order_data = ['pedido_id' => $PEDIDO->id];
                                                $pre_order = $PreOrders->patchEntity($pre_order, $pre_order_data);

                                                if($pre_order_save = $PreOrders->save($pre_order));
                                            endif;
                                        }
                                    }

                                    $total = number_format($valor, 2, '.', '');

                                    $PedidoCliente = $Pedidos
                                        ->find('all', ['contain' => [
                                            'Academias',
                                            'Academias.Cities',
                                            'Clientes',
                                            'PedidoItens',
                                            'PedidoItens.Produtos',
                                            'PedidoItens.Produtos.ProdutoBase',
                                            'PedidoItens.Produtos.ProdutoBase.Marcas'
                                        ]
                                        ])
                                        ->where(['Pedidos.id' => $PEDIDO->id])
                                        ->first();

                                    // Email::configTransport('cliente', [
                                    //     'className' => 'Smtp',
                                    //     'host' => 'mail.logfitness.com.br',
                                    //     'port' => 587,
                                    //     'timeout' => 30,
                                    //     'username' => 'uhull@logfitness.com.br',
                                    //     'password' => 'Lvrt6174?',
                                    //     'client' => null,
                                    //     'tls' => null,
                                    // ]);

                                    // $email = new Email();

                                    // $email
                                    //     ->transport('cliente')
                                    //     ->template('mochila_fechada_aluno')
                                    //     ->emailFormat('html')
                                    //     ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                                    //     ->to($PedidoCliente->cliente->email)
                                    //     ->subject($PedidoCliente->academia->shortname.' - LogFitness - Mochila Fechada')
                                    //     ->set([
                                    //         'id'                => $PedidoCliente->id,
                                    //         'name'              => $PedidoCliente->cliente->name,
                                    //         'academia'          => $PedidoCliente->academia->shortname,
                                    //         'logo_academia'     => $PedidoCliente->academia->image,
                                    //         'academia_id'       => $PedidoCliente->academia->id,
                                    //         'url'               => WEBROOT_URL.$PedidoCliente->academia->slug.'/mochila/fechar-o-ziper/retormar-pedido/'.$PedidoCliente->id
                                    //     ])
                                    //     ->send();

                                    $session->write('ClienteFinalizado', $cliente);

                                    //$this->iugu_cobranca_direta($PedidoCliente->id);

                                    $this->set(compact('PedidoCliente', 'cliente', 'token'));
                                }
                            }else{
                                $this->redirect('/mochila/');
                            }
                        }else{

                            if($session->read('PedidoFinalizado.id') > 1 && $session->read('ClienteFinalizado')) {

                                debug($session->read('PedidoFinalizado.id'));

                                $PedidoCliente = $Pedidos
                                    ->find('all', ['contain' => [
                                        'Academias',
                                        'Academias.Cities',
                                        'Clientes',
                                        'PedidoItens',
                                        'PedidoItens.Produtos',
                                        'PedidoItens.Produtos.ProdutoBase',
                                        'PedidoItens.Produtos.ProdutoBase.Marcas'
                                    ]
                                    ])
                                    ->where(['Pedidos.id' => $session->read('PedidoFinalizado.id')])
                                    ->first();

                                //$this->iugu_cobranca_direta($PedidoCliente->id);

                                $this->set(compact('PedidoCliente', 'cliente', 'token'));

                            }elseif ($PEDIDO_SAVE = $Pedidos->save($pedido)){

                                $PEDIDO_SAVE_ID = ['id' => (20600000 + $PEDIDO_SAVE->id)];

                                $PEDIDO_SAVE = $Pedidos->patchEntity($PEDIDO_SAVE, $PEDIDO_SAVE_ID);
                                if($PEDIDO = $Pedidos->save($PEDIDO_SAVE)){

                                    $session->write('PedidoFinalizado', $PEDIDO);

                                    $PedidoItens = TableRegistry::get('PedidoItens');
                                    $session_carrinho = $session->read('Carrinho');

                                    $produto = $this->Produtos
                                        ->find('all')
                                        ->where(['Produtos.slug' => 'produto-express'])
                                        ->first();

                                    if ($produto) {
                                        $produto->preco_promo ?
                                            $preco = $produto->preco_promo :
                                            $preco = $produto->preco;

                                        $pedido_item = $PedidoItens->newEntity();
                                        $item_data = [
                                            'pedido_id' => $PEDIDO->id,
                                            'produto_id' => $produto->id,
                                            'quantidade' => 1,
                                            'desconto' => 0,
                                            'preco' => $valor_express,
                                        ];

                                        $pedido_item = $PedidoItens->patchEntity($pedido_item, $item_data);

                                        if ($PEDIDO_ITEM_SAVE = $PedidoItens->save($pedido_item)) {
                                            $session->write('PedidoFinalizadoItem.'.$PEDIDO_ITEM_SAVE->id, $PEDIDO_ITEM_SAVE);
                                            //$total += ($item_data['quantidade'] * $item_data['preco']) - $item_data['desconto'];

                                            if($pre_order_id > 0):
                                                $pre_order = $PreOrders
                                                    ->find('all')
                                                    ->where(['PreOrders.id' => $pre_order_id])
                                                    ->first();

                                                $pre_order_data = ['pedido_id' => $PEDIDO->id];
                                                $pre_order = $PreOrders->patchEntity($pre_order, $pre_order_data);

                                                if($pre_order_save = $PreOrders->save($pre_order));
                                            endif;
                                        }
                                    }

                                    //$total = number_format($total, 2, '.', '');
                                    $total = number_format($valor, 2, '.', '');

                                    $PedidoCliente = $Pedidos
                                        ->find('all', ['contain' => [
                                            'Academias',
                                            'Academias.Cities',
                                            'PedidoItens',
                                            'PedidoItens.Produtos',
                                            'PedidoItens.Produtos.ProdutoBase',
                                            'PedidoItens.Produtos.ProdutoBase.Marcas'
                                        ]
                                        ])
                                        ->where(['Pedidos.id' => $PEDIDO->id])
                                        ->first();

                                    $cliente = TableRegistry::get('Clientes')
                                        ->find('all')
                                        ->where(['Clientes.id' => $session_cliente->id])
                                        ->contain(['Cities', 'Academias', 'Academias.Cities'])
                                        ->first();

                                    $session->write('ClienteFinalizado', $cliente);

                                    //$this->iugu_cobranca_direta($PedidoCliente->id);

                                    $this->set(compact('PedidoCliente', 'cliente', 'token'));
                                    
                                }
                            }


                        }

                        /**
                         * ESVAZIAR O CARRINHO - CLIENTE COM CADASTRO COMPLETO
                         */
                        $session->delete('Carrinho');
                    }

                    //debug($moip);

                    $this->set('professores_list', TableRegistry::get('Professores')
                        ->find('list')
                        ->innerJoin(
                            ['ProfessorAcademias' => 'professor_academias'],
                            ['ProfessorAcademias.professor_id = Professores.id'])
                        ->where(['Professores.status_id' => 1])
                        ->andWhere(['ProfessorAcademias.academia_id' => $cliente->academia_id])
                        ->andWhere(['ProfessorAcademias.status_id' => 1])
                        ->all()
                        ->toArray()
                    );


                }

                $this->set('professores_list', TableRegistry::get('Professores')
                    ->find('list')
                    ->innerJoin(
                        ['ProfessorAcademias' => 'professor_academias'],
                        ['ProfessorAcademias.professor_id = Professores.id'])
                    ->where(['Professores.status_id' => 1])
                    ->andWhere(['ProfessorAcademias.academia_id' => $cliente->academia_id])
                    ->andWhere(['ProfessorAcademias.status_id' => 1])
                    ->all()
                    ->toArray()
                );



                $this->set(compact('cupom', 'parcelas'));
            }

            $this->set(compact('pedidoExpressPagar', 'parcelas'));
        } else {
            $this->Flash->error('Insira um valor válido!');
            $this->redirect('/home');
        }

        $this->viewBuilder()->layout('default');
    }

    public function erp_uploadImages($id = null){
        if ( $id != null ) {
            $produto = $this->Produtos->get($id, [
                'contain' => []
            ]);

            $fotos = [];
            $this->request->data['status_id'] = 1;
            $produto_base = $this->Produtos->ProdutoBase->get($produto->produto_base_id);

            $dir = WWW_ROOT.'img' . DS . 'temp' . DS . $id;
            $files = scandir($dir);
            try{
                $index = 0;
                $produto->fotos = 'a:'. (count($files)-2) .':{';
                foreach ($files as $value){
                    if (($value !== '.') && ($value !== '..')) {
                        $image = [];
                        $image['name'] = $value;
                        $image['tmp_name'] = file_get_contents($dir . DS . $value);
                        $name = str_replace(' ', '-', strtolower(trim($produto->slug))) . '-' . $index;
                        $name = $this->Produtos->uploadImage($image, $name);
                        $produto->fotos .= 'i:'.$index.';s:'. strlen($name).':"'.$name.'";';
                        $index++;
                    }
                }
                $produto->fotos .= '}';
                $this->Produtos->save($produto);
            }catch (Exception $e){
                echo 'Error code: '. $e->getCode() . '<br>';
                echo 'Error code: '. $e->getMessage() . '<br>';
                die();
            }
            $this->response->statusCode(200);
            echo 'Imagem Processada';
            die();
        }
    }

    public function carrinho_preencher_dados($express = 0){

        $session         = $this->request->session();
        $DescontoCombo   = $session->read('DescontoCombo');
        $cliente         = $session->read('Cliente');
        $AcademiaSession = $session->read('Academia');

        $Academias          = TableRegistry::get('Academias');
        $ProfessorAcademias = TableRegistry::get('ProfessorAcademias');

        $academia_slug = $Academias
            ->find('all')
            ->contain(['Cities'])
            ->where(['Academias.id' => $AcademiaSession->id])
            ->first();

        $lojas = $Academias
            ->find('all')
            ->contain(['Cities'])
            ->where(['Academias.master_loja' => 1])
            ->all();

        $professores_academia = $ProfessorAcademias
            ->find('all')
            ->contain(['Professores'])
            ->where(['ProfessorAcademias.academia_id' => $AcademiaSession->id])
            ->andWhere(['ProfessorAcademias.status_id' => 1])
            ->all();

        foreach ($professores_academia as $professor_academia) {
            $professores_slug[$professor_academia->professore->id] = $professor_academia->professore->name;
        }

        $cities = TableRegistry::get('Cities')
            ->find('list')
            ->where(['state_id' => $academia_slug->city->state_id]);

        if($this->request->is(['put', 'post'])) {

            if($this->request->data['cpf'] && $this->request->data['email'] && $this->request->data['telephone'] && $this->request->data['name'] && $this->request->data['city_id'] && $this->request->data['academia_id'] != 'Selecione uma academia...') {
                $Clientes = TableRegistry::get('Clientes');

                $cliente = $Clientes
                    ->find('all')
                    ->contain(['Cities', 'Academias', 'Academias.Cities'])
                    ->where(['Clientes.cpf' => $this->request->data['cpf']])
                    ->first();

                if(!$cliente) {
                    $cliente = $Clientes
                        ->find('all')
                        ->contain(['Cities', 'Academias', 'Academias.Cities'])
                        ->where(['Clientes.email' => $this->request->data['email']])
                        ->first();
                }

                $academia = $Academias
                    ->find('all')
                    ->where(['id' => $this->request->data['academia_id']])
                    ->first();

                $session->write('RetiradaLoja', $this->request->data['retirada_loja']);

                if(!$cliente) {
                    function randString($size){
                        $basic = '0123456789';

                        $return= "";

                        for($count= 0; $size > $count; $count++){
                            $return.= $basic[rand(0, strlen($basic) - 1)];
                        }

                        return $return;
                    }

                    $senha = randString(8);

                    if($this->request->data['retirada_loja'] == 10){

                    $data = [
                        'name'          => $this->request->data['name'],
                        'telephone'     => $this->request->data['telephone'],
                        'mobile'        => $this->request->data['telephone'],
                        'email'         => $this->request->data['email'],
                        'cpf'           => $this->request->data['cpf'],
                        'password'      => $senha,
                        'status_id'     => 1,
                        'academia_id'   => $academia->id,
                        'city_id'       => $academia_slug->city_id,
                        "cep"           => $academia_slug->cep,
                        "number"        => $academia_slug->number,
                        "address"       => $academia_slug->address,
                        "area"          => $academia_slug->area
                    ];

                     } elseif ($this->request->data['retirada_loja'] == 601){

                        $RetiradaLoja = $session->read('RetiradaLoja');

                        $loja_retirada = TableRegistry::get('Academias')
                            ->find('all')
                            ->contain(['Cities'])
                            ->where(['Academias.id' => $RetiradaLoja])
                            ->first();

                        $data = [
                            'name'          => $this->request->data['name'],
                            'telephone'     => $this->request->data['telephone'],
                            'mobile'        => $this->request->data['telephone'],
                            'email'         => $this->request->data['email'],
                            'cpf'           => $this->request->data['cpf'],
                            'password'      => $senha,
                            'status_id'     => 1,
                            'academia_id'   => $loja_retirada->id,
                            'city_id'       => $loja_retirada->city_id,
                            "cep"           => $loja_retirada->cep,
                            "number"        => $loja_retirada->number,
                            "address"       => $loja_retirada->address,
                            "area"          => $loja_retirada->area
                        ];

                        
                    }elseif ($this->request->data['retirada_loja'] == 20){

                        $data = [
                            'name'          => $this->request->data['name'],
                            'telephone'     => $this->request->data['telephone'],
                            'mobile'        => $this->request->data['telephone'],
                            'email'         => $this->request->data['email'],
                            'cpf'           => $this->request->data['cpf'],
                            'password'      => $senha,
                            'status_id'     => 1,
                            'academia_id' => $this->request->data['academia_id'],
                            'city_id'       => $this->request->data['city_id'],
                            "cep"           => $this->request->data['cep'],
                            "number"        => $this->request->data['number'],
                            "address"       => $this->request->data['address'],
                            "area"          => $this->request->data['area']
                        ];

                    }

                    $new_cliente = $Clientes->newEntity();
                    $new_cliente = $Clientes->patchEntity($new_cliente, $data);

                    if ($cliente = $Clientes->save($new_cliente)) {
                        // Email::configTransport('cliente', [
                        //     'className' => 'Smtp',
                        //     'host' => 'mail.logfitness.com.br',
                        //     'port' => 587,
                        //     'timeout' => 30,
                        //     'username' => 'uhull@logfitness.com.br',
                        //     'password' => 'Lvrt6174?',
                        //     'client' => null,
                        //     'tls' => null,
                        // ]);

                        // $email = new Email();

                        // $email
                        //     ->transport('cliente')
                        //     ->template('convite_aluno_mensalidade')
                        //     ->emailFormat('html')
                        //     ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                        //     ->to($cliente->email)
                        //     ->subject($academia->shortname.' - LogFitness - Você foi convidado por uma academia!')
                        //     ->set([
                        //         'logo_academia' => $academia->image,
                        //         'academia'      => $academia->shortname,
                        //         'senha'         => $senha,
                        //         'name'          => $cliente->name,
                        //         'slug'          => $academia->slug
                        //     ])
                        //     ->send();

                        $session = $this->request->session();
                        $session->write('UsuarioDados', $cliente);

                        return $this->redirect('/mochila/fechar-o-ziper');
                    } else {
                        $this->Flash->error('Falha ao cadastrar aluno. Tente novamente.');
                    }
                } else if ($cliente->city_id == null || $cliente->academia_id == null) {
                    if($cliente->cep == null) {
                        $data = [
                            'cpf' => $this->request->data['cpf'],
                            'name' => $this->request->data['name'],
                            'telephone' => $this->request->data['telephone'],
                            'city_id' => $this->request->data['city_id'],
                            'academia_id' => $this->request->data['academia_id'],
                            'cep' => $academia->cep,
                            'number' => $academia->number,
                            'address' => $academia->address,
                            'area' => $academia->area
                        ];
                    } else {
                        $data = [
                            'cpf' => $this->request->data['cpf'],
                            'name' => $this->request->data['name'],
                            'telephone' => $this->request->data['telephone'],
                            'city_id' => $this->request->data['city_id'],
                            'academia_id' => $this->request->data['academia_id'],
                            'cep' => $this->request->data['cep'],
                            'number' => $this->request->data['number'],
                            'address' => $this->request->data['address'],
                            'area' => $this->request->data['area']
                        ];
                    }

                    $cliente = $Clientes->patchEntity($cliente, $data);
                    $cliente = $Clientes->save($cliente);

                    $session = $this->request->session();
                    $session->write('UsuarioDados', $cliente);

                    return $this->redirect('/mochila/fechar-o-ziper');


                    if($express == 0) {
                        return $this->redirect('/mochila/fechar-o-ziper');
                    } else {
                        return $this->redirect('/log_express_pagamento?valor_express='.$_GET['valor_express']);
                    }
                }
            } else {
                $this->Flash->error('Preencha todos os dados!');
                return $this->redirect('/mochila/usuario-preencher-dados');
            }
        }

        $Cities = TableRegistry::get('Cities');
        
        $cidade_query = $Cities
            ->find('all')
            ->where(['id' => $academia_slug->city_id_acad])
            ->first();

        $cidade_acad = $cidade_query->name;
        $estado_acad = $cidade_query->uf;



        if($this->request->data['retirada_loja'] == 10){

            $data = [
                'cpf' => $this->request->data['cpf'],
                'name' => $this->request->data['name'],
                'telephone' => $this->request->data['telephone'],
                'city_id' => $this->request->data['city_id'],
                'academia_id'   => $academia->id,
                'city_id'       => $academia_slug->city_id,
                "cep"           => $academia_slug->cep,
                "number"        => $academia_slug->number,
                "address"       => $academia_slug->address,
                "area"          => $academia_slug->area
            ];

            $cliente = $Clientes->patchEntity($cliente, $data);
            $cliente = $Clientes->save($cliente);

            $session = $this->request->session();
            $session->write('UsuarioDados', $cliente);

            return $this->redirect('/mochila/fechar-o-ziper');


            } elseif ($this->request->data['retirada_loja'] == 601){

            $RetiradaLoja = $session->read('RetiradaLoja');

                $loja_retirada = TableRegistry::get('Academias')
                    ->find('all')
                    ->contain(['Cities'])
                    ->where(['Academias.id' => $RetiradaLoja])
                    ->first();
    
            

            $data = [
                'cpf' => $this->request->data['cpf'],
                'name' => $this->request->data['name'],
                'telephone' => $this->request->data['telephone'],
                'city_id' => $this->request->data['city_id'],
                'academia_id'   => $loja_retirada->id,
                'professor_id'  => $this->request->data['professor_id'],
                'city_id'       => $loja_retirada->city_id,
                "cep"           => $loja_retirada->cep,
                "number"        => $loja_retirada->number,
                "address"       => $loja_retirada->address,
                "area"          => $loja_retirada->area
            ];

            $cliente = $Clientes->patchEntity($cliente, $data);
            $cliente = $Clientes->save($cliente);

            $session = $this->request->session();
            $session->write('UsuarioDados', $cliente);

            return $this->redirect('/mochila/fechar-o-ziper');

        }elseif ($this->request->data['retirada_loja'] == 20){

            $data = [
                'cpf' => $this->request->data['cpf'],
                'name' => $this->request->data['name'],
                'telephone' => $this->request->data['telephone'],
                'city_id' => $this->request->data['city_id'],
                'academia_id' => $this->request->data['academia_id'],
                'city_id'       => $this->request->data['city_id'],
                "cep"           => $this->request->data['cep'],
                "number"        => $this->request->data['number'],
                "address"       => $this->request->data['address'],
                "area"          => $this->request->data['area']
            ];

            $cliente = $Clientes->patchEntity($cliente, $data);
            $cliente = $Clientes->save($cliente);

            $session = $this->request->session();
            $session->write('UsuarioDados', $cliente);

            return $this->redirect('/mochila/fechar-o-ziper');

        }





             

        $this->set(compact('academia_slug', 'lojas', 'professores_slug', 'cities', 'cliente', 'cidade_acad', 'estado_acad', 'DescontoCombo'));
        $this->viewBuilder()->layout('default');
    }

    //www.logfitness.com.br/:slug/pesquisa/:query/:marca/:tipo_filtro/:filtro/:ordenacao
    //ex: www.logfitness.com.br/lojamodelo/pesquisa/whey isolado/max-titanium/categoria/proteinas/promocoes
    public function busca($query = null, $marca = null, $tipo_filtro = null, $filtro = null, $ordenacao = null) {
        function tirarAcentos($string){
            return preg_replace(array("/(á|à|ã|â|ä)/","/(ç)/","/(é|è|ê|ë)/","/(í|ì|î|ï)/","/(ó|ò|õ|ô|ö)/","/(ú|ù|û|ü)/","/(ñ)/"),explode(" ","a c e i o u n"),$string);
        }

        $session = $this->request->session();

        $Academia = $session->read('Academia');

        $Marcas = TableRegistry::get('Marcas');
        $Objetivos = TableRegistry::get('Objetivos');
        $Categorias = TableRegistry::get('Categorias');
        $Subcategorias = TableRegistry::get('Subcategorias');

        $query_orig = $query;
        $marca_orig = $marca;
        $tipo_filtro_orig = $tipo_filtro;
        $filtro_orig = $filtro;
        $ordenacao_orig = $ordenacao;

        $marca_escolhida = 0;
        $objetivo_escolhida = 0;
        $categoria_escolhida = 0;
        $subcategoria_escolhida = 0;

        if($query == '0') {
            $query = '';
            $query_orig = '';
        }

        if($marca == '0') {
            $marca = '';
            $marca_orig = '';
        }

        if($tipo_filtro == '0') {
            $tipo_filtro = '';
            $tipo_filtro_orig = '';
        }

        if($filtro == '0') {
            $filtro = '';
            $filtro_slug = '';
            $filtro_orig = '';
        }

        if($query != null){

            $query = str_replace(' ', '%', $query);

            $objetivos = $Objetivos
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            $categorias = $Categorias
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            $subcategorias = $Subcategorias
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            if(count($objetivos) <= 0){
                $objetivos = [0];
            }

            if(count($categorias) <= 0){
                $categorias = [0];
            }

            if(count($subcategorias) <= 0){
                $subcategorias = [0];
            }

            $search = [
                'OR' => [
                    'Produtos.id                   '          => $query,
                    'Produtos.cod_barras           '          => $query,
                    'ProdutoBase.name          LIKE'          => '%'.$query.'%',
                    'Marcas.name               LIKE'          => '%'.$query.'%',
                    'ProdutoObjetivos.objetivo_id IN'         => $objetivos,
                    'ProdutoCategorias.categoria_id IN'       => $categorias,
                    'ProdutoSubcategorias.subcategoria_id IN' => $subcategorias,
                    'Produtos.propriedade      LIKE'          => '%'.$query.'%',
                    'Produtos.embalagem        LIKE'          => '%'.$query.'%',
                    'ProdutoBase.quando_tomar  LIKE'          => '%'.$query.'%',
                    'ProdutoBase.dicas         LIKE'          => '%'.$query.'%',
                    'ProdutoBase.atributos     LIKE'          => '%'.$query.'%',
                    'ProdutoBase.beneficios    LIKE'          => '%'.$query.'%',
                    'ProdutoBase.forma_consumo LIKE'          => '%'.$query.'%',
                    'Produtos.como_tomar       LIKE'          => '%'.$query.'%',
                    'Produtos.slug             LIKE'          => '%'.$query.'%',
                ]
            ];

        }else{
            $search = [];
        }

        if($marca != null) {
            $marca_id = $Marcas
                ->find('all')
                ->where(['slug' => $marca])
                ->andWhere(['status_id' => 1])
                ->first();

            if($marca_id) {
                $marca_escolhida = $marca_id->id;

                $marca_filtro = ['Marcas.id' => $marca_id->id];

                $marca_banner = $marca_id->banner_fundo;
                $this->set(compact('marca_banner'));
            } else {
                return $this->redirect('/'.$Academia->slug.'/pesquisa');
            }
        }

        $objetivo_da_categoria = '';
        $categoria_da_subcategoria = '';

        if($tipo_filtro != null && $filtro != null){

            if($tipo_filtro == 'objetivo') {
                $filtro_id = $Objetivos
                    ->find('all')
                    ->where(['slug' => $filtro])
                    ->andWhere(['status_id' => 1])
                    ->first();

                if($filtro_id) {
                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $objetivo_escolhida = $filtro_id->id;

                    $filtragem = ['ProdutoObjetivos.objetivo_id' => $filtro_id->id];
                } else {
                    return $this->redirect('/'.$Academia->slug.'/pesquisa');
                }

            } else if($tipo_filtro == 'categoria') {
                $filtro_id = $Categorias
                    ->find('all')
                    ->contain(['Objetivos'])
                    ->where(['Categorias.slug' => $filtro])
                    ->andWhere(['Categorias.status_id' => 1])
                    ->first();

                if($filtro_id) {
                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $objetivo_da_categoria = $filtro_id->objetivo->name;

                    $objetivo_escolhida = $filtro_id->objetivo->id;
                    $categoria_escolhida = $filtro_id->id;

                    $filtragem = ['ProdutoCategorias.categoria_id' => $filtro_id->id];
                } else {
                    return $this->redirect('/'.$Academia->slug.'/pesquisa');
                }

            } else if($tipo_filtro == 'subcategoria') {
                $filtro_id = $Subcategorias
                    ->find('all')
                    ->contain(['Categorias', 'Categorias.Objetivos'])
                    ->where(['Subcategorias.slug' => $filtro])
                    ->andWhere(['Subcategorias.status_id' => 1])
                    ->first();

                if($filtro_id) {
                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $categoria_da_subcategoria = $filtro_id->categoria->name;
                    $objetivo_da_categoria = $filtro_id->categoria->objetivo->name;

                    $objetivo_escolhida = $filtro_id->categoria->objetivo->id;
                    $categoria_escolhida = $filtro_id->categoria->id;
                    $subcategoria_escolhida = $filtro_id->id;

                    $filtragem = ['ProdutoSubcategorias.subcategoria_id' => $filtro_id->id];
                } else {
                    return $this->redirect('/'.$Academia->slug.'/pesquisa');
                }
            }

            $filtro_banner = $filtro_id->banner_fundo;
            $this->set(compact('filtro_banner'));
        }else{
            $filtragem = [];
        }

        if($ordenacao != null) {
            if($ordenacao == 'randomico') {
                if($query != null) {
                    $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'peso' => 'desc'];
                } else {
                    $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'rand()'];
                }
            } else if($ordenacao == 'promocoes') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco_promo' => 'desc'];
            } else if($ordenacao == 'maior-valor') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco' => 'desc', 'Produtos.preco_promo' => 'desc'];
            } else if($ordenacao == 'menor-valor') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco' => 'asc',  'Produtos.preco_promo' => 'asc'];
            }
        } else {
            if($query != null) {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'peso' => 'desc'];
            } else {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'rand()'];
            }
        }

        $produtos = $this->Produtos->find('all');
        
        if($query != null) {
            $peso = $produtos->newExpr()
                ->addCase(
                    [
                        $produtos->newExpr()->add(['Produtos.id' => $query]),
                        $produtos->newExpr()->add(['Produtos.cod_barras' => $query]),
                        $produtos->newExpr()->add(['ProdutoBase.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Marcas.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoObjetivos.objetivo_id IN' => $objetivos]),
                        $produtos->newExpr()->add(['ProdutoCategorias.categoria_id IN' => $categorias]),
                        $produtos->newExpr()->add(['ProdutoSubcategorias.subcategoria_id IN' => $subcategorias]),
                        $produtos->newExpr()->add(['Produtos.propriedade LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Produtos.embalagem LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.quando_tomar LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.dicas LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.atributos LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.beneficios LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.forma_consumo LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Produtos.como_tomar LIKE' => '%'.$query.'%'])
                    ],
                    [9, 8, 7, 6, 5, 5, 5, 4, 3, 2, 1, 1, 1, 1, 1],
                    ['integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer']
                );

            $produtos
                ->select($this->Produtos)
                ->select(['peso' => $peso])
                ->select($this->Produtos->ProdutoBase)
                ->select($this->Produtos->ProdutoBase->Marcas)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos->Objetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoCategorias)
                ->select($this->Produtos->ProdutoBase->ProdutoSubcategorias)
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['Objetivos' => 'objetivos'],
                    ['ProdutoObjetivos.objetivo_id = Objetivos.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where($search)
                ->andWhere($filtragem)
                ->andWhere($marca_filtro)
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('Produtos.id')
                ->order($ordenacao_filtro)
                ->limit(12)
                ->all();
        } else {
            $produtos
                ->select($this->Produtos)
                ->select($this->Produtos->ProdutoBase)
                ->select($this->Produtos->ProdutoBase->Marcas)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos->Objetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoCategorias)
                ->select($this->Produtos->ProdutoBase->ProdutoSubcategorias)
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['Objetivos' => 'objetivos'],
                    ['ProdutoObjetivos.objetivo_id = Objetivos.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where($search)
                ->andWhere($filtragem)
                ->andWhere($marca_filtro)
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('Produtos.id')
                ->order($ordenacao_filtro)
                ->limit(12)
                ->all();
        }

        $count_produtos = $this->Produtos
            ->find('all')
            ->contain([
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->where($search)
            ->andWhere($filtragem)
            ->andWhere($marca_filtro)
            ->andWhere(['Produtos.visivel' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->distinct('Produtos.id')
            ->order($ordenacao_filtro)
            ->count();

        if($count_produtos <= 0) {
            if($query_orig != null){
                $query = str_replace(' ', '%', $query_orig);

                $objetivos = $Objetivos
                    ->find('list', [
                        'keyField'      => 'id',
                        'valueField'    => 'id'
                    ])
                    ->where(['name LIKE' => '%'.$query.'%'])
                    ->toArray();

                $categorias = $Categorias
                    ->find('list', [
                        'keyField'      => 'id',
                        'valueField'    => 'id'
                    ])
                    ->where(['name LIKE' => '%'.$query.'%'])
                    ->toArray();

                $subcategorias = $Subcategorias
                    ->find('list', [
                        'keyField'      => 'id',
                        'valueField'    => 'id'
                    ])
                    ->where(['name LIKE' => '%'.$query.'%'])
                    ->toArray();

                if(count($objetivos) <= 0){
                    $objetivos = [0];
                }

                if(count($categorias) <= 0){
                    $categorias = [0];
                }

                if(count($subcategorias) <= 0){
                    $subcategorias = [0];
                }

                $search = [
                    'OR' => [
                        'Marcas.name               LIKE'          => '%'.$query.'%',
                        'ProdutoBase.name          LIKE'          => '%'.$query.'%',
                        'ProdutoBase.dicas         LIKE'          => '%'.$query.'%',
                        'ProdutoBase.atributos     LIKE'          => '%'.$query.'%',
                        'ProdutoBase.beneficios    LIKE'          => '%'.$query.'%',
                        'ProdutoBase.forma_consumo LIKE'          => '%'.$query.'%',
                        'ProdutoBase.quando_tomar  LIKE'          => '%'.$query.'%',
                        'Produtos.embalagem        LIKE'          => '%'.$query.'%',
                        'Produtos.propriedade      LIKE'          => '%'.$query.'%',
                        'Produtos.como_tomar       LIKE'          => '%'.$query.'%',
                        'ProdutoObjetivos.objetivo_id IN'         => $objetivos,
                        'ProdutoCategorias.categoria_id IN'       => $categorias,
                        'ProdutoSubcategorias.subcategoria_id IN' => $subcategorias,
                    ]
                ];

                $similares_query_name = str_replace('%', ' ', $query);

                $similares_query = $this->Produtos
                    ->find('all')
                    ->contain([
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where($search)
                    ->andWhere(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->order('rand()')
                    ->limit(8)
                    ->all();

                if(count($similares_query) <= 0) {
                    return $this->redirect('/'.$Academia->slug.'/pesquisa');
                }
            }

            if($tipo_filtro_orig != null && $filtro_orig != null){
                if($tipo_filtro_orig == 'objetivo') {

                    $filtro_id = $Objetivos
                        ->find('all')
                        ->where(['slug' => $filtro_orig])
                        ->andWhere(['status_id' => 1])
                        ->first();

                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $objetivo_escolhida = $filtro_id->id;

                    $search = ['ProdutoObjetivos.objetivo_id' => $filtro_id->id];

                    $similares_objetivo_name = $filtro_id->name;
                    $similares_objetivo_slug = $filtro_id->slug;
                    
                    $similares_objetivo = $this->Produtos
                        ->find('all')
                        ->contain([
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();

                } else if($tipo_filtro_orig == 'categoria') {
                    $filtro_id = $Categorias
                        ->find('all')
                        ->contain(['Objetivos'])
                        ->where(['Categorias.slug' => $filtro_orig])
                        ->andWhere(['Categorias.status_id' => 1])
                        ->first();

                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $objetivo_da_categoria = $filtro_id->objetivo->name;

                    $objetivo_escolhida = $filtro_id->objetivo->id;
                    $categoria_escolhida = $filtro_id->id;

                    $search = ['ProdutoCategorias.categoria_id' => $filtro_id->id];
                    $search2 = ['ProdutoObjetivos.objetivo_id' => $filtro_id->objetivo->id];

                    $similares_categoria_name = $filtro_id->name;
                    $similares_categoria_slug = $filtro_id->slug;

                    $similares_categoria = $this->Produtos
                        ->find('all')
                        ->contain([
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();

                    $similares_objetivo_name = $filtro_id->objetivo->name;
                    $similares_objetivo_slug = $filtro_id->objetivo->slug;

                    $similares_objetivo = $this->Produtos
                        ->find('all')
                        ->contain([
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search2)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();
                } else if($tipo_filtro_orig == 'subcategoria') {
                    $filtro_id = $Subcategorias
                        ->find('all')
                        ->contain(['Categorias', 'Categorias.Objetivos'])
                        ->where(['Subcategorias.slug' => $filtro_orig])
                        ->andWhere(['Subcategorias.status_id' => 1])
                        ->first();

                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $categoria_da_subcategoria = $filtro_id->categoria->name;
                    $objetivo_da_categoria = $filtro_id->categoria->objetivo->name;

                    $objetivo_escolhida = $filtro_id->categoria->objetivo->id;
                    $categoria_escolhida = $filtro_id->categoria->id;
                    $subcategoria_escolhida = $filtro_id->id;

                    $search = ['ProdutoSubcategorias.subcategoria_id' => $filtro_id->id];
                    $search2 = ['ProdutoCategorias.categoria_id' => $filtro_id->categoria->id];
                    $search3 = ['ProdutoObjetivos.objetivo_id' => $filtro_id->categoria->objetivo->id];

                    $similares_subcategoria_name = $filtro_id->name;
                    $similares_subcategoria_slug = $filtro_id->slug;

                    $similares_subcategoria = $this->Produtos
                        ->find('all')
                        ->contain([
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();

                    $similares_categoria_name = $filtro_id->categoria->name;
                    $similares_categoria_slug = $filtro_id->categoria->slug;

                    $similares_categoria = $this->Produtos
                        ->find('all')
                        ->contain([
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search2)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();

                    $similares_objetivo_name = $filtro_id->categoria->objetivo->name;
                    $similares_objetivo_slug = $filtro_id->categoria->objetivo->slug;

                    $similares_objetivo = $this->Produtos
                        ->find('all')
                        ->contain([
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search3)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();
                }
            }

            if($marca_orig != null) {

                $marca_id = $Marcas
                    ->find('all')
                    ->where(['slug' => $marca_orig])
                    ->andWhere(['status_id' => 1])
                    ->first();

                $marca_escolhida = $marca_id->id;

                $search = ['Marcas.id' => $marca_id->id];

                $similares_marca_name = $marca_id->name;
                $similares_marca_slug = $marca_id->slug;

                $similares_marca = $this->Produtos
                    ->find('all')
                    ->contain([
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where($search)
                    ->andWhere(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->order('rand()')
                    ->limit(8)
                    ->all();
            }
        } else {
            $similares_query = [];
            $similares_query_name = [];
            $similares_subcategoria = [];
            $similares_subcategoria_name = [];
            $similares_subcategoria_slug = [];
            $similares_categoria = [];
            $similares_categoria_name = [];
            $similares_categoria_slug = [];
            $similares_objetivo = [];
            $similares_objetivo_name = [];
            $similares_objetivo_slug = [];
            $similares_marca = [];
            $similares_marca_name = [];
            $similares_marca_slug = [];
        }

        $query = str_replace('%', ' ', $query);

        $links_breadcrumb[] = [
            'url' => '',
            'name' => $Academia->shortname.' - loja'
        ];
        $links_breadcrumb[] = [
            'url' => '/pesquisa',
            'name' => 'Produtos'
        ];
        if($marca != null) {
            $links_breadcrumb[] = [
                'url' => '/pesquisa/0/'.$marca_id->slug,
                'name' => $marca_id->name
            ];
        }

        $banners_busca = TableRegistry::get('Banners')
            ->find('all')
            ->where(['type' => 'busca'])
            ->andWhere(['visivel' => 1])
            ->all();

        $this->set(compact('banners_busca','query', 'marca', 'tipo_filtro', 'filtro', 'ordenacao', 'produtos', 'count_produtos', 'categoria_da_subcategoria', 'objetivo_da_categoria', 'links_breadcrumb', 'marca_escolhida', 'objetivo_escolhida', 'categoria_escolhida', 'subcategoria_escolhida', 'similares_query', 'similares_subcategoria', 'similares_categoria', 'similares_objetivo', 'similares_marca', 'similares_query_name', 'similares_subcategoria_name', 'similares_categoria_name', 'similares_objetivo_name', 'similares_marca_name', 'similares_subcategoria_slug', 'similares_categoria_slug', 'similares_objetivo_slug', 'similares_marca_slug', 'filtro_slug', 'Academia'));
        $this->viewBuilder()->layout('default');
    }

    /** 
     * Atualizar produtos (ver mais)
     * Route: /produtos/atualizar-produtos/
     */ 
    public function atualizar_produtos() {

        function tirarAcentos($string){
            return preg_replace(array("/(á|à|ã|â|ä)/","/(ç)/","/(é|è|ê|ë)/","/(í|ì|î|ï)/","/(ó|ò|õ|ô|ö)/","/(ú|ù|û|ü)/","/(ñ)/"),explode(" ","a c e i o u n"),$string);
        }

        $session = $this->request->session();

        $Academia = $session->read('Academia');

        $SSlug = $Academia->slug;

        $query = $this->request->data['query'];
        $marca = $this->request->data['marca'];
        $tipo_filtro = $this->request->data['tipo_filtro'];
        $filtro = $this->request->data['filtro'];
        $ordenacao = $this->request->data['ordenacao'];
        $produtos_ids = $this->request->data['produtos_ids'];

        if($query == '0') {
            $query = '';
        }

        if($marca == '0') {
            $marca = '';
        }

        if($tipo_filtro == '0') {
            $tipo_filtro = '';
        }

        if($filtro == '0') {
            $filtro = '';
        }

        $Marcas = TableRegistry::get('Marcas');
        $Objetivos = TableRegistry::get('Objetivos');
        $Categorias = TableRegistry::get('Categorias');
        $Subcategorias = TableRegistry::get('Subcategorias');

        if($query != null){

            $query = str_replace(' ', '%', $query);

            $objetivos = $Objetivos
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            $categorias = $Categorias
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            $subcategorias = $Subcategorias
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            if(count($objetivos) <= 0){
                $objetivos = [0];
            }

            if(count($categorias) <= 0){
                $categorias = [0];
            }

            if(count($subcategorias) <= 0){
                $subcategorias = [0];
            }

            $search = [
                'OR' => [
                    'Produtos.id                   '          => $query,
                    'Produtos.cod_barras           '          => $query,
                    'ProdutoBase.name          LIKE'          => '%'.$query.'%',
                    'Marcas.name               LIKE'          => '%'.$query.'%',
                    'ProdutoObjetivos.objetivo_id IN'         => $objetivos,
                    'ProdutoCategorias.categoria_id IN'       => $categorias,
                    'ProdutoSubcategorias.subcategoria_id IN' => $subcategorias,
                    'Produtos.propriedade      LIKE'          => '%'.$query.'%',
                    'Produtos.embalagem        LIKE'          => '%'.$query.'%',
                    'ProdutoBase.quando_tomar  LIKE'          => '%'.$query.'%',
                    'ProdutoBase.dicas         LIKE'          => '%'.$query.'%',
                    'ProdutoBase.atributos     LIKE'          => '%'.$query.'%',
                    'ProdutoBase.beneficios    LIKE'          => '%'.$query.'%',
                    'ProdutoBase.forma_consumo LIKE'          => '%'.$query.'%',
                    'Produtos.como_tomar       LIKE'          => '%'.$query.'%',
                ]
            ];
        }else{
            $search = [];
        }

        if($marca != null) {

            $marca_id = $Marcas
                ->find('all')
                ->where(['slug' => $marca])
                ->andWhere(['status_id' => 1])
                ->first();

            $marca_filtro = ['Marcas.id' => $marca_id->id];
        }

        if($tipo_filtro != null && $filtro != null){

            if($tipo_filtro == 'objetivo') {
                $filtro_id = $Objetivos
                    ->find('all')
                    ->where(['slug' => $filtro])
                    ->andWhere(['status_id' => 1])
                    ->first();

                $filtragem = ['ProdutoObjetivos.objetivo_id' => $filtro_id->id];
            } else if($tipo_filtro == 'categoria') {
                $filtro_id = $Categorias
                    ->find('all')
                    ->where(['slug' => $filtro])
                    ->andWhere(['status_id' => 1])
                    ->first();

                $filtragem = ['ProdutoCategorias.categoria_id' => $filtro_id->id];
            } else if($tipo_filtro == 'subcategoria') {
                $filtro_id = $Subcategorias
                    ->find('all')
                    ->where(['slug' => $filtro])
                    ->andWhere(['status_id' => 1])
                    ->first();

                $filtragem = ['ProdutoSubcategorias.subcategoria_id' => $filtro_id->id];
            }
        }else{
            $filtragem = [];
        }

        if($ordenacao != null) {
            if($ordenacao == 'randomico') {
                if($query != null) {
                    $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'peso' => 'desc'];
                } else {
                    $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'rand()'];
                }
            } else if($ordenacao == 'promocoes') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco_promo' => 'desc'];
            } else if($ordenacao == 'maior-valor') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco' => 'desc', 'Produtos.preco_promo' => 'desc'];
            } else if($ordenacao == 'menor-valor') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco' => 'asc',  'Produtos.preco_promo' => 'asc'];
            }
        } else {
            if($query != null) {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'peso' => 'desc'];
            } else {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'rand()'];
            }
        }

        $produtos = $this->Produtos->find('all');

        if($query != null) {
            $peso = $produtos->newExpr()
                ->addCase(
                    [
                        $produtos->newExpr()->add(['Produtos.id' => $query]),
                        $produtos->newExpr()->add(['Produtos.cod_barras' => $query]),
                        $produtos->newExpr()->add(['ProdutoBase.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Marcas.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoObjetivos.objetivo_id IN' => $objetivos]),
                        $produtos->newExpr()->add(['ProdutoCategorias.categoria_id IN' => $categorias]),
                        $produtos->newExpr()->add(['ProdutoSubcategorias.subcategoria_id IN' => $subcategorias]),
                        $produtos->newExpr()->add(['Produtos.propriedade LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Produtos.embalagem LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.quando_tomar LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.dicas LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.atributos LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.beneficios LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.forma_consumo LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Produtos.como_tomar LIKE' => '%'.$query.'%'])
                    ],
                    [9, 8, 7, 6, 5, 5, 5, 4, 3, 2, 1, 1, 1, 1, 1],
                    ['integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer']
                );
                
            $produtos
                ->select($this->Produtos)
                ->select(['peso' => $peso])
                ->select($this->Produtos->ProdutoBase)
                ->select($this->Produtos->ProdutoBase->Marcas)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos->Objetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoCategorias)
                ->select($this->Produtos->ProdutoBase->ProdutoSubcategorias)
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['Objetivos' => 'objetivos'],
                    ['ProdutoObjetivos.objetivo_id = Objetivos.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where($search)
                ->andWhere($filtragem)
                ->andWhere($marca_filtro)
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->andWhere(['Produtos.id NOT IN' => $produtos_ids])
                ->distinct('Produtos.id')
                ->order($ordenacao_filtro)
                ->limit(12)
                ->all();
        } else {
            $produtos
                ->select($this->Produtos)
                ->select($this->Produtos->ProdutoBase)
                ->select($this->Produtos->ProdutoBase->Marcas)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos->Objetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoCategorias)
                ->select($this->Produtos->ProdutoBase->ProdutoSubcategorias)
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['Objetivos' => 'objetivos'],
                    ['ProdutoObjetivos.objetivo_id = Objetivos.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where($search)
                ->andWhere($filtragem)
                ->andWhere($marca_filtro)
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->andWhere(['Produtos.id NOT IN' => $produtos_ids])
                ->distinct('Produtos.id')
                ->order($ordenacao_filtro)
                ->limit(12)
                ->all();
        }

        $this->set(compact('SSlug', 'produtos', 'Academia'));
        $this->viewBuilder()->layout('ajax');
    }

    /** 
     * Filtrar produtos (marcas, categorias, query, ordenação,etc)
     * Route: /produtos/filtrar/
     */ 
    public function filtrar($query = null, $marca = null, $tipo_filtro = null, $filtro = null, $ordenacao = null) {

        function tirarAcentos($string){
            return preg_replace(array("/(á|à|ã|â|ä)/","/(ç)/","/(é|è|ê|ë)/","/(í|ì|î|ï)/","/(ó|ò|õ|ô|ö)/","/(ú|ù|û|ü)/","/(ñ)/"),explode(" ","a c e i o u n"),$string);
        }

        $session = $this->request->session();

        $Academia = $session->read('Academia');

        $Marcas = TableRegistry::get('Marcas');
        $Objetivos = TableRegistry::get('Objetivos');
        $Categorias = TableRegistry::get('Categorias');
        $Subcategorias = TableRegistry::get('Subcategorias');

        if(!strpos($_SERVER['HTTP_USER_AGENT'],"bot")) {
            $Navegacoes = TableRegistry::get('Navegacoes');

            $navegacao = $Navegacoes->newEntity();

            if($Academia) {
                $navegacao_academia = $Academia->shortname;
            } else {
                $navegacao_academia = '';
            }

            if(isset($_SERVER['HTTP_REFERER'])) {
                $navegacao_origem = $_SERVER['HTTP_REFERER'];
            } else {
                $navegacao_origem = '';
            }

            $UsuarioNavegacao = $session->read('UsuarioNavegacao');

            if($UsuarioNavegacao) {
                $navegacao_usuario = $UsuarioNavegacao;
            } else {
                $navegacao_usuario = '';
            }

            $navegacao = $Navegacoes->patchEntity($navegacao, [
                'user_agent'  => $_SERVER['HTTP_USER_AGENT'],
                'tipo'        => 'Busca',
                'usuario'     => $navegacao_usuario,
                'url_origem'  => $navegacao_origem,
                'url_destino' => $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
                'academia'    => $navegacao_academia
            ]);
            $Navegacoes->save($navegacao);
        }

        $query_orig = $query;
        $marca_orig = $marca;
        $tipo_filtro_orig = $tipo_filtro;
        $filtro_orig = $filtro;
        $ordenacao_orig = $ordenacao;

        $marca_escolhida = 0;
        $objetivo_escolhida = 0;
        $categoria_escolhida = 0;
        $subcategoria_escolhida = 0;

        if($query == '0') {
            $query = '';
            $query_orig = '';
        }

        if($marca == '0') {
            $marca = '';
            $marca_orig = '';
        }

        if($tipo_filtro == '0') {
            $tipo_filtro = '';
            $tipo_filtro_orig = '';
        }

        if($filtro == '0') {
            $filtro = '';
            $filtro_orig = '';
            $filtro_slug = '';
        }

        if($query != null){

            if($query != 'fonts') {
                $Filtragens = TableRegistry::get('Filtragens');

                $filtragem = $Filtragens
                    ->find('all')
                    ->where(['palavra_chave' => $query])
                    ->first();

                if($filtragem) {
                    $filtragem = $Filtragens->patchEntity($filtragem, ['qtd' => $filtragem->qtd+1]);
                    $Filtragens->save($filtragem);
                } else {
                    $new_filtragem = $Filtragens->newEntity();

                    $data = [
                        'palavra_chave' => $query,
                        'qtd' => 1
                    ];

                    $new_filtragem = $Filtragens->patchEntity($new_filtragem, $data);
                    $Filtragens->save($new_filtragem);
                }
            }

            $query = str_replace(' ', '%', $query);

            $objetivos = $Objetivos
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            $categorias = $Categorias
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            $subcategorias = $Subcategorias
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            if(count($objetivos) <= 0){
                $objetivos = [0];
            }

            if(count($categorias) <= 0){
                $categorias = [0];
            }

            if(count($subcategorias) <= 0){
                $subcategorias = [0];
            }

            $search = [
                'OR' => [
                    'Produtos.id                   '          => $query,
                    'ProdutoBase.name          LIKE'          => '%'.$query.'%',
                    'Sabores.name              LIKE'          => '%'.$query.'%',
                    'Marcas.name               LIKE'          => '%'.$query.'%',
                    'ProdutoObjetivos.objetivo_id IN'         => $objetivos,
                    'ProdutoCategorias.categoria_id IN'       => $categorias,
                    'ProdutoSubcategorias.subcategoria_id IN' => $subcategorias,
                    'Produtos.propriedade      LIKE'          => '%'.$query.'%',
                    'Produtos.embalagem        LIKE'          => '%'.$query.'%',
                    'ProdutoBase.quando_tomar  LIKE'          => '%'.$query.'%',
                    'ProdutoBase.dicas         LIKE'          => '%'.$query.'%',
                    'ProdutoBase.atributos     LIKE'          => '%'.$query.'%',
                    'ProdutoBase.beneficios    LIKE'          => '%'.$query.'%',
                    'ProdutoBase.forma_consumo LIKE'          => '%'.$query.'%',
                    'Produtos.como_tomar       LIKE'          => '%'.$query.'%',
                ]
            ];
        }else{
            $search = [];
        }

        if($marca != null) {

            $marca_id = $Marcas
                ->find('all')
                ->where(['slug' => $marca])
                ->andWhere(['status_id' => 1])
                ->first();

            $marca_escolhida = $marca_id->id;

            $marca_banner = $marca_id->banner_fundo;
            $this->set(compact('marca_banner'));
            
            $marca_filtro = ['Marcas.id' => $marca_id->id];
        }

        $objetivo_da_categoria = '';
        $categoria_da_subcategoria = '';

        if($tipo_filtro != null && $filtro != null){

            if($tipo_filtro == 'objetivo') {
                $filtro_id = $Objetivos
                    ->find('all')
                    ->where(['slug' => $filtro])
                    ->andWhere(['status_id' => 1])
                    ->first();

                $filtro = tirarAcentos(strtolower($filtro_id->name));
                $filtro_slug = $filtro_id->slug;

                $objetivo_escolhida = $filtro_id->id;

                $filtragem = ['ProdutoObjetivos.objetivo_id' => $filtro_id->id];
            } else if($tipo_filtro == 'categoria') {
                $filtro_id = $Categorias
                    ->find('all')
                    ->contain(['Objetivos'])
                    ->where(['Categorias.slug' => $filtro])
                    ->andWhere(['Categorias.status_id' => 1])
                    ->first();

                $filtro = tirarAcentos(strtolower($filtro_id->name));
                $filtro_slug = $filtro_id->slug;

                $objetivo_da_categoria = $filtro_id->objetivo->name;

                $objetivo_escolhida = $filtro_id->objetivo->id;
                $categoria_escolhida = $filtro_id->id;

                $filtragem = ['ProdutoCategorias.categoria_id' => $filtro_id->id];
            } else if($tipo_filtro == 'subcategoria') {
                $filtro_id = $Subcategorias
                    ->find('all')
                    ->contain(['Categorias', 'Categorias.Objetivos'])
                    ->where(['Subcategorias.slug' => $filtro])
                    ->andWhere(['Subcategorias.status_id' => 1])
                    ->first();

                $filtro = tirarAcentos(strtolower($filtro_id->name));
                $filtro_slug = $filtro_id->slug;

                $categoria_da_subcategoria = $filtro_id->categoria->name;
                $objetivo_da_categoria = $filtro_id->categoria->objetivo->name;

                $objetivo_escolhida = $filtro_id->categoria->objetivo->id;
                $categoria_escolhida = $filtro_id->categoria->id;
                $subcategoria_escolhida = $filtro_id->id;

                $filtragem = ['ProdutoSubcategorias.subcategoria_id' => $filtro_id->id];
            }

            $filtro_banner = $filtro_id->banner_fundo;
            $this->set(compact('filtro_banner'));
        }else{
            $filtragem = [];
        }

        if($ordenacao != null) {
            if($ordenacao == 'randomico') {
                if($query != null) {
                    $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'peso' => 'desc'];
                } else {
                    $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'rand()'];
                }
            } else if($ordenacao == 'promocoes') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco_promo' => 'desc'];
            } else if($ordenacao == 'maior-valor') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco' => 'desc', 'Produtos.preco_promo' => 'desc'];
            } else if($ordenacao == 'menor-valor') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco' => 'asc',  'Produtos.preco_promo' => 'asc'];
            }
        } else {
            if($query != null) {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'peso' => 'desc'];
            } else {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'rand()'];
            }
        }

        $produtos = $this->Produtos->find('all');

        if($query != null) {
            $peso = $produtos->newExpr()
                ->addCase(
                    [
                        $produtos->newExpr()->add(['Produtos.id' => $query]),
                        $produtos->newExpr()->add(['ProdutoBase.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Sabores.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Marcas.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoObjetivos.objetivo_id IN' => $objetivos]),
                        $produtos->newExpr()->add(['ProdutoCategorias.categoria_id IN' => $categorias]),
                        $produtos->newExpr()->add(['ProdutoSubcategorias.subcategoria_id IN' => $subcategorias]),
                        $produtos->newExpr()->add(['Produtos.propriedade LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Produtos.embalagem LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.quando_tomar LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.dicas LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.atributos LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.beneficios LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.forma_consumo LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Produtos.como_tomar LIKE' => '%'.$query.'%'])
                    ],
                    [9, 8, 7, 6, 5, 5, 5, 4, 3, 2, 1, 1, 1, 1, 1],
                    ['integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer']
                );
                
            $produtos
                ->select($this->Produtos)
                ->select(['peso' => $peso])
                ->select($this->Produtos->ProdutoBase)
                ->select($this->Produtos->ProdutoBase->Marcas)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos->Objetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoCategorias)
                ->select($this->Produtos->ProdutoBase->ProdutoSubcategorias)
                ->contain([
                    'Sabores',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['Sabores' => 'sabores'],
                    ['Sabores.id = Produtos.sabor_id']
                )
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['Objetivos' => 'objetivos'],
                    ['ProdutoObjetivos.objetivo_id = Objetivos.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where($search)
                ->andWhere($filtragem)
                ->andWhere($marca_filtro)
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('Produtos.id')
                ->order($ordenacao_filtro)
                ->limit(12)
                ->all();
        } else {
            $produtos
                ->select($this->Produtos)
                ->select($this->Produtos->ProdutoBase)
                ->select($this->Produtos->ProdutoBase->Marcas)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos->Objetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoCategorias)
                ->select($this->Produtos->ProdutoBase->ProdutoSubcategorias)
                ->contain([
                    'Sabores',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['Sabores' => 'sabores'],
                    ['Sabores.id = Produtos.sabor_id']
                )
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['Objetivos' => 'objetivos'],
                    ['ProdutoObjetivos.objetivo_id = Objetivos.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where($search)
                ->andWhere($filtragem)
                ->andWhere($marca_filtro)
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('Produtos.id')
                ->order($ordenacao_filtro)
                ->limit(12)
                ->all();
        }

        $count_produtos = $this->Produtos
            ->find('all')
            ->contain([
                'Sabores',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['Sabores' => 'sabores'],
                ['Sabores.id = Produtos.sabor_id']
            )
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->where($search)
            ->andWhere($filtragem)
            ->andWhere($marca_filtro)
            ->andWhere(['Produtos.visivel' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->distinct('Produtos.id')
            ->order($ordenacao_filtro)
            ->count();

        if($count_produtos <= 0) {
            if($query_orig != null){
                $query = str_replace(' ', '%', $query_orig);

                $objetivos = $Objetivos
                    ->find('list', [
                        'keyField'      => 'id',
                        'valueField'    => 'id'
                    ])
                    ->where(['name LIKE' => '%'.$query.'%'])
                    ->toArray();

                $categorias = $Categorias
                    ->find('list', [
                        'keyField'      => 'id',
                        'valueField'    => 'id'
                    ])
                    ->where(['name LIKE' => '%'.$query.'%'])
                    ->toArray();

                $subcategorias = $Subcategorias
                    ->find('list', [
                        'keyField'      => 'id',
                        'valueField'    => 'id'
                    ])
                    ->where(['name LIKE' => '%'.$query.'%'])
                    ->toArray();

                if(count($objetivos) <= 0){
                    $objetivos = [0];
                }

                if(count($categorias) <= 0){
                    $categorias = [0];
                }

                if(count($subcategorias) <= 0){
                    $subcategorias = [0];
                }

                $search = [
                    'OR' => [
                        'Marcas.name               LIKE'          => '%'.$query.'%',
                        'Sabores.name              LIKE'          => '%'.$query.'%',
                        'ProdutoBase.name          LIKE'          => '%'.$query.'%',
                        'ProdutoBase.dicas         LIKE'          => '%'.$query.'%',
                        'ProdutoBase.atributos     LIKE'          => '%'.$query.'%',
                        'ProdutoBase.beneficios    LIKE'          => '%'.$query.'%',
                        'ProdutoBase.forma_consumo LIKE'          => '%'.$query.'%',
                        'ProdutoBase.quando_tomar  LIKE'          => '%'.$query.'%',
                        'Produtos.embalagem        LIKE'          => '%'.$query.'%',
                        'Produtos.propriedade      LIKE'          => '%'.$query.'%',
                        'Produtos.como_tomar       LIKE'          => '%'.$query.'%',
                        'ProdutoObjetivos.objetivo_id IN'         => $objetivos,
                        'ProdutoCategorias.categoria_id IN'       => $categorias,
                        'ProdutoSubcategorias.subcategoria_id IN' => $subcategorias,
                    ]
                ];

                $similares_query_name = str_replace('%', ' ', $query);

                $similares_query = $this->Produtos
                    ->find('all')
                    ->contain([
                        'Sabores',
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['Sabores' => 'sabores'],
                        ['Sabores.id = Produtos.sabor_id']
                    )
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where($search)
                    ->andWhere(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->order('rand()')
                    ->limit(8)
                    ->all();
            }

            if($tipo_filtro_orig != null && $filtro_orig != null){
                if($tipo_filtro_orig == 'objetivo') {
                    $filtro_id = $Objetivos
                        ->find('all')
                        ->where(['slug' => $filtro_orig])
                        ->andWhere(['status_id' => 1])
                        ->first();

                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $objetivo_escolhida = $filtro_id->id;

                    $search = ['ProdutoObjetivos.objetivo_id' => $filtro_id->id];

                    $similares_objetivo_name = $filtro_id->name;
                    $similares_objetivo_slug = $filtro_id->slug;
                    
                    $similares_objetivo = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();
                } else if($tipo_filtro_orig == 'categoria') {
                    $filtro_id = $Categorias
                        ->find('all')
                        ->contain(['Objetivos'])
                        ->where(['Categorias.slug' => $filtro_orig])
                        ->andWhere(['Categorias.status_id' => 1])
                        ->first();

                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $objetivo_da_categoria = $filtro_id->objetivo->name;

                    $objetivo_escolhida = $filtro_id->objetivo->id;
                    $categoria_escolhida = $filtro_id->id;

                    $search = ['ProdutoCategorias.categoria_id' => $filtro_id->id];
                    $search2 = ['ProdutoObjetivos.objetivo_id' => $filtro_id->objetivo->id];

                    $similares_categoria_name = $filtro_id->name;
                    $similares_categoria_slug = $filtro_id->slug;

                    $similares_categoria = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();

                    $similares_objetivo_name = $filtro_id->objetivo->name;
                    $similares_objetivo_slug = $filtro_id->objetivo->slug;

                    $similares_objetivo = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search2)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();
                } else if($tipo_filtro_orig == 'subcategoria') {
                    $filtro_id = $Subcategorias
                        ->find('all')
                        ->contain(['Categorias', 'Categorias.Objetivos'])
                        ->where(['Subcategorias.slug' => $filtro_orig])
                        ->andWhere(['Subcategorias.status_id' => 1])
                        ->first();

                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $categoria_da_subcategoria = $filtro_id->categoria->name;
                    $objetivo_da_categoria = $filtro_id->categoria->objetivo->name;

                    $objetivo_escolhida = $filtro_id->categoria->objetivo->id;
                    $categoria_escolhida = $filtro_id->categoria->id;
                    $subcategoria_escolhida = $filtro_id->id;

                    $search = ['ProdutoSubcategorias.subcategoria_id' => $filtro_id->id];
                    $search2 = ['ProdutoCategorias.categoria_id' => $filtro_id->categoria->id];
                    $search3 = ['ProdutoObjetivos.objetivo_id' => $filtro_id->categoria->objetivo->id];

                    $similares_subcategoria_name = $filtro_id->name;
                    $similares_subcategoria_slug = $filtro_id->slug;

                    $similares_subcategoria = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();

                    $similares_categoria_name = $filtro_id->categoria->name;
                    $similares_categoria_slug = $filtro_id->categoria->slug;

                    $similares_categoria = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search2)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();

                    $similares_objetivo_name = $filtro_id->categoria->objetivo->name;
                    $similares_objetivo_slug = $filtro_id->categoria->objetivo->slug;

                    $similares_objetivo = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search3)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();
                }
            }

            if($marca_orig != null) {

                $marca_id = $Marcas
                    ->find('all')
                    ->where(['slug' => $marca_orig])
                    ->andWhere(['status_id' => 1])
                    ->first();

                $marca_escolhida = $marca_id->id;

                $search = ['Marcas.id' => $marca_id->id];

                $similares_marca_name = $marca_id->name;
                $similares_marca_slug = $marca_id->slug;

                $similares_marca = $this->Produtos
                    ->find('all')
                    ->contain([
                        'Sabores',
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['Sabores' => 'sabores'],
                        ['Sabores.id = Produtos.sabor_id']
                    )
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where($search)
                    ->andWhere(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->order('rand()')
                    ->limit(8)
                    ->all();
            }
        } else {
            $similares_query = [];
            $similares_query_name = [];
            $similares_subcategoria = [];
            $similares_subcategoria_name = [];
            $similares_subcategoria_slug = [];
            $similares_categoria = [];
            $similares_categoria_name = [];
            $similares_categoria_slug = [];
            $similares_objetivo = [];
            $similares_objetivo_name = [];
            $similares_objetivo_slug = [];
            $similares_marca = [];
            $similares_marca_name = [];
            $similares_marca_slug = [];
        }

        $query = str_replace('%', ' ', $query);
        $marca = str_replace(' ', '-', $marca);
        $filtro = str_replace(' ', '-', $filtro);

        $links_breadcrumb[] = [
            'url' => '',
            'name' => $Academia->shortname.' - loja'
        ];
        $links_breadcrumb[] = [
            'url' => '/pesquisa',
            'name' => 'Produtos'
        ];
        if($marca != null) {
            $links_breadcrumb[] = [
                'url' => '/pesquisa/0/'.$marca_id->slug,
                'name' => $marca_id->name
            ];
        }

        $banners_busca = TableRegistry::get('Banners')
        ->find('all')
        ->where(['type' => 'busca'])
        ->andWhere(['visivel' => 1])
        ->all();

        $this->set(compact('banners_busca', 'query', 'marca', 'tipo_filtro', 'filtro', 'ordenacao', 'produtos', 'count_produtos', 'categoria_da_subcategoria', 'objetivo_da_categoria', 'links_breadcrumb', 'marca_escolhida', 'objetivo_escolhida', 'categoria_escolhida', 'subcategoria_escolhida', 'similares_query', 'similares_subcategoria', 'similares_categoria', 'similares_objetivo', 'similares_marca', 'similares_query_name', 'similares_subcategoria_name', 'similares_categoria_name', 'similares_objetivo_name', 'similares_marca_name', 'similares_subcategoria_slug', 'similares_categoria_slug', 'similares_objetivo_slug', 'similares_marca_slug', 'filtro_slug', 'Academia'));
        $this->viewBuilder()->layout('ajax');
    }

    //www.logfitness.com.br/:slug/similares/:slug_produtos
    //ex: www.logfitness.com.br/lojamodelo/similares/top-whey-3w
    public function similares($slug_produto = null) {

        if($slug_produto) {
            $session = $this->request->session();

            $Academia = $session->read('Academia');
            $Marcas = TableRegistry::get('Marcas');
            $Objetivos = TableRegistry::get('Objetivos');
            $Categorias = TableRegistry::get('Categorias');
            $Subcategorias = TableRegistry::get('Subcategorias');

            $produto_escolhido = $this->Produtos
                ->find('all')
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoCategorias.Categorias',
                    'ProdutoBase.ProdutoSubcategorias',
                    'ProdutoBase.ProdutoSubcategorias.Subcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Produtos.slug' => $slug_produto])
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->first();

            foreach ($produto_escolhido->produto_base->produto_objetivos as $produto_escolhido_objetivos) {
                $produto_objetivos[] = $produto_escolhido_objetivos->objetivo;
            }

            if(count($produto_objetivos) <= 0) {
                $produto_objetivos = [];
            }

            foreach ($produto_escolhido->produto_base->produto_categorias as $produto_escolhido_categorias) {
                $produto_categorias[] = $produto_escolhido_categorias->categoria;
            }

            if(count($produto_categorias) <= 0) {
                $produto_categorias = [];
            }

            foreach ($produto_escolhido->produto_base->produto_subcategorias as $produto_escolhido_subcategorias) {
                $produto_subcategorias[] = $produto_escolhido_subcategorias->subcategoria;
            }

            if(count($produto_subcategorias) <= 0) {
                $produto_subcategorias = [];
            }

            $similares_produto_base = $this->Produtos
                ->find('all')
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Produtos.produto_base_id' => $produto_escolhido->produto_base_id])
                ->andWhere(['Produtos.id NOT IN' => $produto_escolhido->id])
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['Produtos.status_id' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->all();

            $similares_marca = $this->Produtos
                ->find('all')
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Marcas.id' => $produto_escolhido->produto_base->marca_id])
                ->andWhere(['Produtos.id NOT IN' => $produto_escolhido->id])
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['Produtos.status_id' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('Produtos.id')
                ->order('rand()')
                ->limit(8)
                ->all();

            $similares_marca_name = $produto_escolhido->produto_base->marca->name;
            $similares_marca_slug = $produto_escolhido->produto_base->marca->slug;

            $ctrl = 0;
            foreach ($produto_objetivos as $produto_objetivo) {

                $similares_objetivo[] = $this->Produtos
                    ->find('all')
                    ->contain([
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where(['ProdutoObjetivos.objetivo_id' => $produto_objetivo->id])
                    ->andWhere(['Produtos.id NOT IN' => $produto_escolhido->id])
                    ->andWhere(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->order('rand()')
                    ->limit(8)
                    ->all();

                $similares_objetivo_name[$ctrl] = $produto_objetivo->name;
                $similares_objetivo_slug[$ctrl] = $produto_objetivo->slug;

                $ctrl++;
            }

            $ctrl = 0;
            foreach ($produto_categorias as $produto_categoria) {

                $similares_categoria[] = $this->Produtos
                    ->find('all')
                    ->contain([
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where(['ProdutoCategorias.categoria_id' => $produto_categoria->id])
                    ->andWhere(['Produtos.id NOT IN' => $produto_escolhido->id])
                    ->andWhere(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->order('rand()')
                    ->limit(8)
                    ->all();

                $similares_categoria_name[$ctrl] = $produto_categoria->name;
                $similares_categoria_slug[$ctrl] = $produto_categoria->slug;

                $ctrl++;
            }

            $ctrl = 0;
            foreach ($produto_subcategorias as $produto_subcategoria) {

                $similares_subcategoria[] = $this->Produtos
                    ->find('all')
                    ->contain([
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where(['ProdutoSubcategorias.subcategoria_id' => $produto_subcategoria->id])
                    ->andWhere(['Produtos.id NOT IN' => $produto_escolhido->id])
                    ->andWhere(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->order('rand()')
                    ->limit(8)
                    ->all();

                $similares_subcategoria_name[$ctrl] = $produto_subcategoria->name;
                $similares_subcategoria_slug[$ctrl] = $produto_subcategoria->slug;

                $ctrl++;
            }

            $links_breadcrumb[] = [
                'url' => '',
                'name' => $Academia->shortname.' - loja'
            ];
            $links_breadcrumb[] = [
                'url' => '/pesquisa',
                'name' => 'Produtos'
            ];
            $links_breadcrumb[] = [
                'url' => '/similares/'.$produto_escolhido->slug,
                'name' => 'Similares - '.$produto_escolhido->produto_base->name
            ];

            $this->set(compact('links_breadcrumb', 'similares_produto_base', 'similares_subcategoria', 'similares_categoria', 'similares_objetivo', 'similares_marca', 'similares_subcategoria_name', 'similares_categoria_name', 'similares_objetivo_name', 'similares_marca_name', 'similares_subcategoria_slug', 'similares_categoria_slug', 'similares_objetivo_slug', 'similares_marca_slug', 'produto_escolhido', 'Academia'));
            $this->viewBuilder()->layout('default');
        } else {
            return $this->redirect('/pesquisa');
        }
    }

    /*
     * COMPARADOR
     */
    public function comparador($query = null, $marca = null, $tipo_filtro = null, $filtro = null, $ordenacao = null) {

        function tirarAcentos($string){
            return preg_replace(array("/(á|à|ã|â|ä)/","/(ç)/","/(é|è|ê|ë)/","/(í|ì|î|ï)/","/(ó|ò|õ|ô|ö)/","/(ú|ù|û|ü)/","/(ñ)/"),explode(" ","a c e i o u n"),$string);
        }

        $session = $this->request->session();

        $Academia = $session->read('Academia');
        $Marcas = TableRegistry::get('Marcas');
        $Objetivos = TableRegistry::get('Objetivos');
        $Categorias = TableRegistry::get('Categorias');
        $Subcategorias = TableRegistry::get('Subcategorias');

        if($Academia) {
            if($query == null && $marca == null && $tipo_filtro == null && $filtro == null && $ordenacao == null) {
                $url_atual = explode('/comparador', $_SERVER['REQUEST_URI']);
                return $this->redirect('/'.$Academia->slug);
            } else {
                $url_atual = explode('/comparador', $_SERVER['REQUEST_URI']);
                return $this->redirect('/'.$Academia->slug.'/pesquisa'.$url_atual[1]);
            }
        }

        $query_orig = $query;
        $marca_orig = $marca;
        $tipo_filtro_orig = $tipo_filtro;
        $filtro_orig = $filtro;
        $ordenacao_orig = $ordenacao;

        $marca_escolhida = 0;
        $objetivo_escolhida = 0;
        $categoria_escolhida = 0;
        $subcategoria_escolhida = 0;

        if($query == '0') {
            $query = '';
            $query_orig = '';
        }

        if($marca == '0') {
            $marca = '';
            $marca_orig = '';
        }

        if($tipo_filtro == '0') {
            $tipo_filtro = '';
            $tipo_filtro_orig = '';
        }

        if($filtro == '0') {
            $filtro = '';
            $filtro_slug = '';
            $filtro_orig = '';
        }

        if($query != null){

            $query = str_replace(' ', '%', $query);

            $objetivos = $Objetivos
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            $categorias = $Categorias
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            $subcategorias = $Subcategorias
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            if(count($objetivos) <= 0){
                $objetivos = [0];
            }

            if(count($categorias) <= 0){
                $categorias = [0];
            }

            if(count($subcategorias) <= 0){
                $subcategorias = [0];
            }

            $search = [
                'OR' => [
                    'Produtos.id                    '          => $query,
                    'ProdutoBase.name          LIKE'          => '%'.$query.'%',
                    'Sabores.name              LIKE'          => '%'.$query.'%',
                    'Marcas.name               LIKE'          => '%'.$query.'%',
                    'ProdutoObjetivos.objetivo_id IN'         => $objetivos,
                    'ProdutoCategorias.categoria_id IN'       => $categorias,
                    'ProdutoSubcategorias.subcategoria_id IN' => $subcategorias,
                    'Produtos.propriedade      LIKE'          => '%'.$query.'%',
                    'Produtos.embalagem        LIKE'          => '%'.$query.'%',
                    'ProdutoBase.quando_tomar  LIKE'          => '%'.$query.'%',
                    'ProdutoBase.dicas         LIKE'          => '%'.$query.'%',
                    'ProdutoBase.atributos     LIKE'          => '%'.$query.'%',
                    'ProdutoBase.beneficios    LIKE'          => '%'.$query.'%',
                    'ProdutoBase.forma_consumo LIKE'          => '%'.$query.'%',
                    'Produtos.como_tomar       LIKE'          => '%'.$query.'%',
                ]
            ];

        }else{
            $search = [];
        }

        if($marca != null) {

            $marca_id = $Marcas
                ->find('all')
                ->where(['slug' => $marca])
                ->andWhere(['status_id' => 1])
                ->first();

            $marca_escolhida = $marca_id->id;

            $marca_filtro = ['Marcas.id' => $marca_id->id];
        }

        $objetivo_da_categoria = '';
        $categoria_da_subcategoria = '';

        if($tipo_filtro != null && $filtro != null){

            if($tipo_filtro == 'objetivo') {
                $filtro_id = $Objetivos
                    ->find('all')
                    ->where(['slug' => $filtro])
                    ->andWhere(['status_id' => 1])
                    ->first();

                if($filtro_id) {
                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $objetivo_escolhida = $filtro_id->id;

                    $filtragem = ['ProdutoObjetivos.objetivo_id' => $filtro_id->id];
                } else {
                    return $this->redirect('/'.$Academia->slug.'/pesquisa');
                }

            } else if($tipo_filtro == 'categoria') {
                $filtro_id = $Categorias
                    ->find('all')
                    ->contain(['Objetivos'])
                    ->where(['Categorias.slug' => $filtro])
                    ->andWhere(['Categorias.status_id' => 1])
                    ->first();

                if($filtro_id) {
                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $objetivo_da_categoria = $filtro_id->objetivo->name;

                    $objetivo_escolhida = $filtro_id->objetivo->id;
                    $categoria_escolhida = $filtro_id->id;

                    $filtragem = ['ProdutoCategorias.categoria_id' => $filtro_id->id];
                } else {
                    return $this->redirect('/'.$Academia->slug.'/pesquisa');
                }

            } else if($tipo_filtro == 'subcategoria') {
                $filtro_id = $Subcategorias
                    ->find('all')
                    ->contain(['Categorias', 'Categorias.Objetivos'])
                    ->where(['Subcategorias.slug' => $filtro])
                    ->andWhere(['Subcategorias.status_id' => 1])
                    ->first();

                if($filtro_id) {
                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $categoria_da_subcategoria = $filtro_id->categoria->name;
                    $objetivo_da_categoria = $filtro_id->categoria->objetivo->name;

                    $objetivo_escolhida = $filtro_id->categoria->objetivo->id;
                    $categoria_escolhida = $filtro_id->categoria->id;
                    $subcategoria_escolhida = $filtro_id->id;

                    $filtragem = ['ProdutoSubcategorias.subcategoria_id' => $filtro_id->id];
                } else {
                    return $this->redirect('/'.$Academia->slug.'/pesquisa');
                }
            }
        }else{
            $filtragem = [];
        }

        if($ordenacao != null) {
            if($ordenacao == 'randomico') {
                if($query != null) {
                    $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'peso' => 'desc'];
                } else {
                    $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'rand()'];
                }
            } else if($ordenacao == 'promocoes') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco_promo' => 'desc'];
            } else if($ordenacao == 'maior-valor') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco' => 'desc', 'Produtos.preco_promo' => 'desc'];
            } else if($ordenacao == 'menor-valor') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco' => 'asc',  'Produtos.preco_promo' => 'asc'];
            }
        } else {
            if($query != null) {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'peso' => 'desc'];
            } else {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'rand()'];
            }
        }

        $produtos = $this->Produtos->find('all');
        
        if($query != null) {
            $peso = $produtos->newExpr()
                ->addCase(
                    [
                        $produtos->newExpr()->add(['Produtos.id' => $query]),
                        $produtos->newExpr()->add(['ProdutoBase.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Sabores.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Marcas.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoObjetivos.objetivo_id IN' => $objetivos]),
                        $produtos->newExpr()->add(['ProdutoCategorias.categoria_id IN' => $categorias]),
                        $produtos->newExpr()->add(['ProdutoSubcategorias.subcategoria_id IN' => $subcategorias]),
                        $produtos->newExpr()->add(['Produtos.propriedade LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Produtos.embalagem LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.quando_tomar LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.dicas LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.atributos LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.beneficios LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.forma_consumo LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Produtos.como_tomar LIKE' => '%'.$query.'%'])
                    ],
                    [9, 8, 7, 6, 5, 5, 5, 4, 3, 2, 1, 1, 1, 1, 1],
                    ['integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer']
                );

            $produtos
                ->select($this->Produtos)
                ->select(['peso' => $peso])
                ->select($this->Produtos->ProdutoBase)
                ->select($this->Produtos->ProdutoBase->Marcas)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos->Objetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoCategorias)
                ->select($this->Produtos->ProdutoBase->ProdutoSubcategorias)
                ->contain([
                    'Sabores',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['Sabores' => 'sabores'],
                    ['Sabores.id = Produtos.sabor_id']
                )
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['Objetivos' => 'objetivos'],
                    ['ProdutoObjetivos.objetivo_id = Objetivos.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where($search)
                ->andWhere($filtragem)
                ->andWhere($marca_filtro)
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('Produtos.id')
                ->order($ordenacao_filtro)
                ->limit(12)
                ->all();
        } else {
            $produtos
                ->select($this->Produtos)
                ->select($this->Produtos->ProdutoBase)
                ->select($this->Produtos->ProdutoBase->Marcas)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos->Objetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoCategorias)
                ->select($this->Produtos->ProdutoBase->ProdutoSubcategorias)
                ->contain([
                    'Sabores',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['Sabores' => 'sabores'],
                    ['Sabores.id = Produtos.sabor_id']
                )
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['Objetivos' => 'objetivos'],
                    ['ProdutoObjetivos.objetivo_id = Objetivos.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where($search)
                ->andWhere($filtragem)
                ->andWhere($marca_filtro)
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('Produtos.id')
                ->order($ordenacao_filtro)
                ->limit(12)
                ->all();
        }

        $count_produtos = $this->Produtos
            ->find('all')
            ->contain([
                'Sabores',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['Sabores' => 'sabores'],
                ['Sabores.id = Produtos.sabor_id']
            )
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->where($search)
            ->andWhere($filtragem)
            ->andWhere($marca_filtro)
            ->andWhere(['Produtos.visivel' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->distinct('Produtos.id')
            ->order($ordenacao_filtro)
            ->count();


        if($count_produtos <= 0) {
            if($query_orig != null){
                $query = str_replace(' ', '%', $query_orig);

                $objetivos = $Objetivos
                    ->find('list', [
                        'keyField'      => 'id',
                        'valueField'    => 'id'
                    ])
                    ->where(['name LIKE' => '%'.$query.'%'])
                    ->toArray();

                $categorias = $Categorias
                    ->find('list', [
                        'keyField'      => 'id',
                        'valueField'    => 'id'
                    ])
                    ->where(['name LIKE' => '%'.$query.'%'])
                    ->toArray();

                $subcategorias = $Subcategorias
                    ->find('list', [
                        'keyField'      => 'id',
                        'valueField'    => 'id'
                    ])
                    ->where(['name LIKE' => '%'.$query.'%'])
                    ->toArray();

                if(count($objetivos) <= 0){
                    $objetivos = [0];
                }

                if(count($categorias) <= 0){
                    $categorias = [0];
                }

                if(count($subcategorias) <= 0){
                    $subcategorias = [0];
                }

                $search = [
                    'OR' => [
                        'Marcas.name               LIKE'          => '%'.$query.'%',
                        'Sabores.name              LIKE'          => '%'.$query.'%',
                        'ProdutoBase.name          LIKE'          => '%'.$query.'%',
                        'ProdutoBase.dicas         LIKE'          => '%'.$query.'%',
                        'ProdutoBase.atributos     LIKE'          => '%'.$query.'%',
                        'ProdutoBase.beneficios    LIKE'          => '%'.$query.'%',
                        'ProdutoBase.forma_consumo LIKE'          => '%'.$query.'%',
                        'ProdutoBase.quando_tomar  LIKE'          => '%'.$query.'%',
                        'Produtos.embalagem        LIKE'          => '%'.$query.'%',
                        'Produtos.propriedade      LIKE'          => '%'.$query.'%',
                        'Produtos.como_tomar       LIKE'          => '%'.$query.'%',
                        'ProdutoObjetivos.objetivo_id IN'         => $objetivos,
                        'ProdutoCategorias.categoria_id IN'       => $categorias,
                        'ProdutoSubcategorias.subcategoria_id IN' => $subcategorias,
                    ]
                ];

                $similares_query_name = str_replace('%', ' ', $query);

                $similares_query = $this->Produtos
                    ->find('all')
                    ->contain([
                        'Sabores',
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['Sabores' => 'sabores'],
                        ['Sabores.id = Produtos.sabor_id']
                    )
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where($search)
                    ->andWhere(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->order('rand()')
                    ->limit(8)
                    ->all();

                if(count($similares_query) <= 0) {
                    return $this->redirect('/'.$Academia->slug.'/pesquisa');
                }
            }

            if($tipo_filtro_orig != null && $filtro_orig != null){
                if($tipo_filtro_orig == 'objetivo') {

                    $filtro_id = $Objetivos
                        ->find('all')
                        ->where(['slug' => $filtro_orig])
                        ->andWhere(['status_id' => 1])
                        ->first();

                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $objetivo_escolhida = $filtro_id->id;

                    $search = ['ProdutoObjetivos.objetivo_id' => $filtro_id->id];

                    $similares_objetivo_name = $filtro_id->name;
                    $similares_objetivo_slug = $filtro_id->slug;
                    
                    $similares_objetivo = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();

                } else if($tipo_filtro_orig == 'categoria') {
                    $filtro_id = $Categorias
                        ->find('all')
                        ->contain(['Objetivos'])
                        ->where(['Categorias.slug' => $filtro_orig])
                        ->andWhere(['Categorias.status_id' => 1])
                        ->first();

                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $objetivo_da_categoria = $filtro_id->objetivo->name;

                    $objetivo_escolhida = $filtro_id->objetivo->id;
                    $categoria_escolhida = $filtro_id->id;

                    $search = ['ProdutoCategorias.categoria_id' => $filtro_id->id];
                    $search2 = ['ProdutoObjetivos.objetivo_id' => $filtro_id->objetivo->id];

                    $similares_categoria_name = $filtro_id->name;
                    $similares_categoria_slug = $filtro_id->slug;

                    $similares_categoria = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();

                    $similares_objetivo_name = $filtro_id->objetivo->name;
                    $similares_objetivo_slug = $filtro_id->objetivo->slug;

                    $similares_objetivo = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search2)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();
                } else if($tipo_filtro_orig == 'subcategoria') {
                    $filtro_id = $Subcategorias
                        ->find('all')
                        ->contain(['Categorias', 'Categorias.Objetivos'])
                        ->where(['Subcategorias.slug' => $filtro_orig])
                        ->andWhere(['Subcategorias.status_id' => 1])
                        ->first();

                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $categoria_da_subcategoria = $filtro_id->categoria->name;
                    $objetivo_da_categoria = $filtro_id->categoria->objetivo->name;

                    $objetivo_escolhida = $filtro_id->categoria->objetivo->id;
                    $categoria_escolhida = $filtro_id->categoria->id;
                    $subcategoria_escolhida = $filtro_id->id;

                    $search = ['ProdutoSubcategorias.subcategoria_id' => $filtro_id->id];
                    $search2 = ['ProdutoCategorias.categoria_id' => $filtro_id->categoria->id];
                    $search3 = ['ProdutoObjetivos.objetivo_id' => $filtro_id->categoria->objetivo->id];

                    $similares_subcategoria_name = $filtro_id->name;
                    $similares_subcategoria_slug = $filtro_id->slug;

                    $similares_subcategoria = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();

                    $similares_categoria_name = $filtro_id->categoria->name;
                    $similares_categoria_slug = $filtro_id->categoria->slug;

                    $similares_categoria = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search2)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();

                    $similares_objetivo_name = $filtro_id->categoria->objetivo->name;
                    $similares_objetivo_slug = $filtro_id->categoria->objetivo->slug;

                    $similares_objetivo = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search3)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();
                }
            }

            if($marca_orig != null) {

                $marca_id = $Marcas
                    ->find('all')
                    ->where(['slug' => $marca_orig])
                    ->andWhere(['status_id' => 1])
                    ->first();

                $marca_escolhida = $marca_id->id;

                $search = ['Marcas.id' => $marca_id->id];

                $similares_marca_name = $marca_id->name;
                $similares_marca_slug = $marca_id->slug;

                $similares_marca = $this->Produtos
                    ->find('all')
                    ->contain([
                        'Sabores',
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['Sabores' => 'sabores'],
                        ['Sabores.id = Produtos.sabor_id']
                    )
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where($search)
                    ->andWhere(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->order('rand()')
                    ->limit(8)
                    ->all();
            }
        } else {
            $similares_query = [];
            $similares_query_name = [];
            $similares_subcategoria = [];
            $similares_subcategoria_name = [];
            $similares_subcategoria_slug = [];
            $similares_categoria = [];
            $similares_categoria_name = [];
            $similares_categoria_slug = [];
            $similares_objetivo = [];
            $similares_objetivo_name = [];
            $similares_objetivo_slug = [];
            $similares_marca = [];
            $similares_marca_name = [];
            $similares_marca_slug = [];
        }

        $sabores_list = TableRegistry::get('Sabores')->find('list');

        $query = str_replace('%', ' ', $query);

        $links_breadcrumb[] = [
            'url' => '/comparador',
            'name' => 'Comparador - Produtos'
        ];
        if($marca != null) {
            $links_breadcrumb[] = [
                'url' => '/comparador/0/'.$marca_id->slug,
                'name' => $marca_id->name
            ];
        }

        $this->set(compact('query', 'marca', 'tipo_filtro', 'filtro', 'ordenacao', 'produtos', 'count_produtos', 'categoria_da_subcategoria', 'objetivo_da_categoria', 'links_breadcrumb', 'marca_escolhida', 'objetivo_escolhida', 'categoria_escolhida', 'subcategoria_escolhida', 'sabores_list', 'similares_query', 'similares_subcategoria', 'similares_categoria', 'similares_objetivo', 'similares_marca', 'similares_query_name', 'similares_subcategoria_name', 'similares_categoria_name', 'similares_objetivo_name', 'similares_marca_name', 'similares_subcategoria_slug', 'similares_categoria_slug', 'similares_objetivo_slug', 'similares_marca_slug', 'filtro_slug'));
        $this->viewBuilder()->layout('default-comparador');
    }

    public function atualizar_produtos_comparador() {

        function tirarAcentos($string){
            return preg_replace(array("/(á|à|ã|â|ä)/","/(ç)/","/(é|è|ê|ë)/","/(í|ì|î|ï)/","/(ó|ò|õ|ô|ö)/","/(ú|ù|û|ü)/","/(ñ)/"),explode(" ","a c e i o u n"),$string);
        }

        $session = $this->request->session();

        $Academia = $session->read('Academia');

        $SSlug = $Academia->slug;

        $query = $this->request->data['query'];
        $marca = $this->request->data['marca'];
        $tipo_filtro = $this->request->data['tipo_filtro'];
        $filtro = $this->request->data['filtro'];
        $ordenacao = $this->request->data['ordenacao'];
        $produtos_ids = $this->request->data['produtos_ids'];

        if($query == '0') {
            $query = '';
        }

        if($marca == '0') {
            $marca = '';
        }

        if($tipo_filtro == '0') {
            $tipo_filtro = '';
        }

        if($filtro == '0') {
            $filtro = '';
        }

        $Marcas = TableRegistry::get('Marcas');
        $Objetivos = TableRegistry::get('Objetivos');
        $Categorias = TableRegistry::get('Categorias');
        $Subcategorias = TableRegistry::get('Subcategorias');

        if($query != null){

            $query = str_replace(' ', '%', $query);

            $objetivos = $Objetivos
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            $categorias = $Categorias
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            $subcategorias = $Subcategorias
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            if(count($objetivos) <= 0){
                $objetivos = [0];
            }

            if(count($categorias) <= 0){
                $categorias = [0];
            }

            if(count($subcategorias) <= 0){
                $subcategorias = [0];
            }

            $search = [
                'OR' => [
                    'Produtos.id                   '          => $query,
                    'ProdutoBase.name          LIKE'          => '%'.$query.'%',
                    'Sabores.name              LIKE'          => '%'.$query.'%',
                    'Marcas.name               LIKE'          => '%'.$query.'%',
                    'ProdutoObjetivos.objetivo_id IN'         => $objetivos,
                    'ProdutoCategorias.categoria_id IN'       => $categorias,
                    'ProdutoSubcategorias.subcategoria_id IN' => $subcategorias,
                    'Produtos.propriedade      LIKE'          => '%'.$query.'%',
                    'Produtos.embalagem        LIKE'          => '%'.$query.'%',
                    'ProdutoBase.quando_tomar  LIKE'          => '%'.$query.'%',
                    'ProdutoBase.dicas         LIKE'          => '%'.$query.'%',
                    'ProdutoBase.atributos     LIKE'          => '%'.$query.'%',
                    'ProdutoBase.beneficios    LIKE'          => '%'.$query.'%',
                    'ProdutoBase.forma_consumo LIKE'          => '%'.$query.'%',
                    'Produtos.como_tomar       LIKE'          => '%'.$query.'%',
                ]
            ];
        }else{
            $search = [];
        }

        if($marca != null) {

            $marca_id = $Marcas
                ->find('all')
                ->where(['slug' => $marca])
                ->andWhere(['status_id' => 1])
                ->first();

            $marca_filtro = ['Marcas.id' => $marca_id->id];
        }

        if($tipo_filtro != null && $filtro != null){

            if($tipo_filtro == 'objetivo') {
                $filtro_id = $Objetivos
                    ->find('all')
                    ->where(['slug' => $filtro])
                    ->andWhere(['status_id' => 1])
                    ->first();

                $filtragem = ['ProdutoObjetivos.objetivo_id' => $filtro_id->id];
            } else if($tipo_filtro == 'categoria') {
                $filtro_id = $Categorias
                    ->find('all')
                    ->where(['slug' => $filtro])
                    ->andWhere(['status_id' => 1])
                    ->first();

                $filtragem = ['ProdutoCategorias.categoria_id' => $filtro_id->id];
            } else if($tipo_filtro == 'subcategoria') {
                $filtro_id = $Subcategorias
                    ->find('all')
                    ->where(['slug' => $filtro])
                    ->andWhere(['status_id' => 1])
                    ->first();

                $filtragem = ['ProdutoSubcategorias.subcategoria_id' => $filtro_id->id];
            }
        }else{
            $filtragem = [];
        }

        if($ordenacao != null) {
            if($ordenacao == 'randomico') {
                if($query != null) {
                    $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'peso' => 'desc'];
                } else {
                    $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'rand()'];
                }
            } else if($ordenacao == 'promocoes') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco_promo' => 'desc'];
            } else if($ordenacao == 'maior-valor') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco' => 'desc', 'Produtos.preco_promo' => 'desc'];
            } else if($ordenacao == 'menor-valor') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco' => 'asc',  'Produtos.preco_promo' => 'asc'];
            }
        } else {
            if($query != null) {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'peso' => 'desc'];
            } else {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'rand()'];
            }
        }

        $produtos = $this->Produtos->find('all');

        if($query != null) {
            $peso = $produtos->newExpr()
                ->addCase(
                    [
                        $produtos->newExpr()->add(['Produtos.id' => $query]),
                        $produtos->newExpr()->add(['ProdutoBase.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Sabores.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Marcas.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoObjetivos.objetivo_id IN' => $objetivos]),
                        $produtos->newExpr()->add(['ProdutoCategorias.categoria_id IN' => $categorias]),
                        $produtos->newExpr()->add(['ProdutoSubcategorias.subcategoria_id IN' => $subcategorias]),
                        $produtos->newExpr()->add(['Produtos.propriedade LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Produtos.embalagem LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.quando_tomar LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.dicas LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.atributos LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.beneficios LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.forma_consumo LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Produtos.como_tomar LIKE' => '%'.$query.'%'])
                    ],
                    [9, 8, 7, 6, 5, 5, 5, 4, 3, 2, 1, 1, 1, 1, 1],
                    ['integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer']
                );
                
            $produtos
                ->select($this->Produtos)
                ->select(['peso' => $peso])
                ->select($this->Produtos->ProdutoBase)
                ->select($this->Produtos->ProdutoBase->Marcas)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos->Objetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoCategorias)
                ->select($this->Produtos->ProdutoBase->ProdutoSubcategorias)
                ->contain([
                    'Sabores',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['Sabores' => 'sabores'],
                    ['Sabores.id = Produtos.sabor_id']
                )
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['Objetivos' => 'objetivos'],
                    ['ProdutoObjetivos.objetivo_id = Objetivos.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where($search)
                ->andWhere($filtragem)
                ->andWhere($marca_filtro)
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->andWhere(['Produtos.id NOT IN' => $produtos_ids])
                ->distinct('Produtos.id')
                ->order($ordenacao_filtro)
                ->limit(12)
                ->all();
        } else {
            $produtos
                ->select($this->Produtos)
                ->select($this->Produtos->ProdutoBase)
                ->select($this->Produtos->ProdutoBase->Marcas)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos->Objetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoCategorias)
                ->select($this->Produtos->ProdutoBase->ProdutoSubcategorias)
                ->contain([
                    'Sabores',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['Sabores' => 'sabores'],
                    ['Sabores.id = Produtos.sabor_id']
                )
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['Objetivos' => 'objetivos'],
                    ['ProdutoObjetivos.objetivo_id = Objetivos.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where($search)
                ->andWhere($filtragem)
                ->andWhere($marca_filtro)
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->andWhere(['Produtos.id NOT IN' => $produtos_ids])
                ->distinct('Produtos.id')
                ->order($ordenacao_filtro)
                ->limit(12)
                ->all();
        }

        $this->set(compact('SSlug', 'produtos'));
        $this->viewBuilder()->layout('ajax');
    }

    public function filtrar_comparador($query = null, $marca = null, $tipo_filtro = null, $filtro = null, $ordenacao = null) {

        function tirarAcentos($string){
            return preg_replace(array("/(á|à|ã|â|ä)/","/(ç)/","/(é|è|ê|ë)/","/(í|ì|î|ï)/","/(ó|ò|õ|ô|ö)/","/(ú|ù|û|ü)/","/(ñ)/"),explode(" ","a c e i o u n"),$string);
        }

        $session = $this->request->session();

        $Academia = $session->read('Academia');

        $Marcas = TableRegistry::get('Marcas');
        $Objetivos = TableRegistry::get('Objetivos');
        $Categorias = TableRegistry::get('Categorias');
        $Subcategorias = TableRegistry::get('Subcategorias');

        $query_orig = $query;
        $marca_orig = $marca;
        $tipo_filtro_orig = $tipo_filtro;
        $filtro_orig = $filtro;
        $ordenacao_orig = $ordenacao;

        $marca_escolhida = 0;
        $objetivo_escolhida = 0;
        $categoria_escolhida = 0;
        $subcategoria_escolhida = 0;

        if($query == '0') {
            $query = '';
            $query_orig = '';
        }

        if($marca == '0') {
            $marca = '';
            $marca_orig = '';
        }

        if($tipo_filtro == '0') {
            $tipo_filtro = '';
            $tipo_filtro_orig = '';
        }

        if($filtro == '0') {
            $filtro = '';
            $filtro_orig = '';
            $filtro_slug = '';
        }

        if($query != null){

            $query = str_replace(' ', '%', $query);

            $objetivos = $Objetivos
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            $categorias = $Categorias
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            $subcategorias = $Subcategorias
                ->find('list', [
                    'keyField'      => 'id',
                    'valueField'    => 'id'
                ])
                ->where(['name LIKE' => '%'.$query.'%'])
                ->toArray();

            if(count($objetivos) <= 0){
                $objetivos = [0];
            }

            if(count($categorias) <= 0){
                $categorias = [0];
            }

            if(count($subcategorias) <= 0){
                $subcategorias = [0];
            }

            $search = [
                'OR' => [
                    'Produtos.id                   '          => $query,
                    'ProdutoBase.name          LIKE'          => '%'.$query.'%',
                    'Sabores.name              LIKE'          => '%'.$query.'%',
                    'Marcas.name               LIKE'          => '%'.$query.'%',
                    'ProdutoObjetivos.objetivo_id IN'         => $objetivos,
                    'ProdutoCategorias.categoria_id IN'       => $categorias,
                    'ProdutoSubcategorias.subcategoria_id IN' => $subcategorias,
                    'Produtos.propriedade      LIKE'          => '%'.$query.'%',
                    'Produtos.embalagem        LIKE'          => '%'.$query.'%',
                    'ProdutoBase.quando_tomar  LIKE'          => '%'.$query.'%',
                    'ProdutoBase.dicas         LIKE'          => '%'.$query.'%',
                    'ProdutoBase.atributos     LIKE'          => '%'.$query.'%',
                    'ProdutoBase.beneficios    LIKE'          => '%'.$query.'%',
                    'ProdutoBase.forma_consumo LIKE'          => '%'.$query.'%',
                    'Produtos.como_tomar       LIKE'          => '%'.$query.'%',
                ]
            ];
        }else{
            $search = [];
        }

        if($marca != null) {

            $marca_id = $Marcas
                ->find('all')
                ->where(['slug' => $marca])
                ->andWhere(['status_id' => 1])
                ->first();

            $marca_escolhida = $marca_id->id;
            
            $marca_filtro = ['Marcas.id' => $marca_id->id];
        }

        $objetivo_da_categoria = '';
        $categoria_da_subcategoria = '';

        if($tipo_filtro != null && $filtro != null){

            if($tipo_filtro == 'objetivo') {
                $filtro_id = $Objetivos
                    ->find('all')
                    ->where(['slug' => $filtro])
                    ->andWhere(['status_id' => 1])
                    ->first();

                $filtro = tirarAcentos(strtolower($filtro_id->name));
                $filtro_slug = $filtro_id->slug;

                $objetivo_escolhida = $filtro_id->id;

                $filtragem = ['ProdutoObjetivos.objetivo_id' => $filtro_id->id];
            } else if($tipo_filtro == 'categoria') {
                $filtro_id = $Categorias
                    ->find('all')
                    ->contain(['Objetivos'])
                    ->where(['Categorias.slug' => $filtro])
                    ->andWhere(['Categorias.status_id' => 1])
                    ->first();

                $filtro = tirarAcentos(strtolower($filtro_id->name));
                $filtro_slug = $filtro_id->slug;

                $objetivo_da_categoria = $filtro_id->objetivo->name;

                $objetivo_escolhida = $filtro_id->objetivo->id;
                $categoria_escolhida = $filtro_id->id;

                $filtragem = ['ProdutoCategorias.categoria_id' => $filtro_id->id];
            } else if($tipo_filtro == 'subcategoria') {
                $filtro_id = $Subcategorias
                    ->find('all')
                    ->contain(['Categorias', 'Categorias.Objetivos'])
                    ->where(['Subcategorias.slug' => $filtro])
                    ->andWhere(['Subcategorias.status_id' => 1])
                    ->first();

                $filtro = tirarAcentos(strtolower($filtro_id->name));
                $filtro_slug = $filtro_id->slug;

                $categoria_da_subcategoria = $filtro_id->categoria->name;
                $objetivo_da_categoria = $filtro_id->categoria->objetivo->name;

                $objetivo_escolhida = $filtro_id->categoria->objetivo->id;
                $categoria_escolhida = $filtro_id->categoria->id;
                $subcategoria_escolhida = $filtro_id->id;

                $filtragem = ['ProdutoSubcategorias.subcategoria_id' => $filtro_id->id];
            }
        }else{
            $filtragem = [];
        }

        if($ordenacao != null) {
            if($ordenacao == 'randomico') {
                if($query != null) {
                    $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'peso' => 'desc'];
                } else {
                    $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'rand()'];
                }
            } else if($ordenacao == 'promocoes') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco_promo' => 'desc'];
            } else if($ordenacao == 'maior-valor') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco' => 'desc', 'Produtos.preco_promo' => 'desc'];
            } else if($ordenacao == 'menor-valor') {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'Produtos.preco' => 'asc',  'Produtos.preco_promo' => 'asc'];
            }
        } else {
            if($query != null) {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'peso' => 'desc'];
            } else {
                $ordenacao_filtro = ['Produtos.status_id' => 'asc', 'rand()'];
            }
        }

        $produtos = $this->Produtos->find('all');

        if($query != null) {
            $peso = $produtos->newExpr()
                ->addCase(
                    [
                        $produtos->newExpr()->add(['Produtos.id' => $query]),
                        $produtos->newExpr()->add(['ProdutoBase.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Sabores.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Marcas.name LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoObjetivos.objetivo_id IN' => $objetivos]),
                        $produtos->newExpr()->add(['ProdutoCategorias.categoria_id IN' => $categorias]),
                        $produtos->newExpr()->add(['ProdutoSubcategorias.subcategoria_id IN' => $subcategorias]),
                        $produtos->newExpr()->add(['Produtos.propriedade LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Produtos.embalagem LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.quando_tomar LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.dicas LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.atributos LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.beneficios LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['ProdutoBase.forma_consumo LIKE' => '%'.$query.'%']),
                        $produtos->newExpr()->add(['Produtos.como_tomar LIKE' => '%'.$query.'%'])
                    ],
                    [9, 8, 7, 6, 5, 5, 5, 4, 3, 2, 1, 1, 1, 1, 1],
                    ['integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer', 'integer']
                );
                
            $produtos
                ->select($this->Produtos)
                ->select(['peso' => $peso])
                ->select($this->Produtos->ProdutoBase)
                ->select($this->Produtos->ProdutoBase->Marcas)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos->Objetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoCategorias)
                ->select($this->Produtos->ProdutoBase->ProdutoSubcategorias)
                ->contain([
                    'Sabores',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['Sabores' => 'sabores'],
                    ['Sabores.id = Produtos.sabor_id']
                )
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['Objetivos' => 'objetivos'],
                    ['ProdutoObjetivos.objetivo_id = Objetivos.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where($search)
                ->andWhere($filtragem)
                ->andWhere($marca_filtro)
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('Produtos.id')
                ->order($ordenacao_filtro)
                ->limit(12)
                ->all();
        } else {
            $produtos
                ->select($this->Produtos)
                ->select($this->Produtos->ProdutoBase)
                ->select($this->Produtos->ProdutoBase->Marcas)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoObjetivos->Objetivos)
                ->select($this->Produtos->ProdutoBase->ProdutoCategorias)
                ->select($this->Produtos->ProdutoBase->ProdutoSubcategorias)
                ->contain([
                    'Sabores',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['Sabores' => 'sabores'],
                    ['Sabores.id = Produtos.sabor_id']
                )
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['Objetivos' => 'objetivos'],
                    ['ProdutoObjetivos.objetivo_id = Objetivos.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where($search)
                ->andWhere($filtragem)
                ->andWhere($marca_filtro)
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('Produtos.id')
                ->order($ordenacao_filtro)
                ->limit(12)
                ->all();
        }

        $count_produtos = $this->Produtos
            ->find('all')
            ->contain([
                'Sabores',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['Sabores' => 'sabores'],
                ['Sabores.id = Produtos.sabor_id']
            )
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->where($search)
            ->andWhere($filtragem)
            ->andWhere($marca_filtro)
            ->andWhere(['Produtos.visivel' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->distinct('Produtos.id')
            ->order($ordenacao_filtro)
            ->count();

        if($count_produtos <= 0) {
            if($query_orig != null){
                $query = str_replace(' ', '%', $query_orig);

                $objetivos = $Objetivos
                    ->find('list', [
                        'keyField'      => 'id',
                        'valueField'    => 'id'
                    ])
                    ->where(['name LIKE' => '%'.$query.'%'])
                    ->toArray();

                $categorias = $Categorias
                    ->find('list', [
                        'keyField'      => 'id',
                        'valueField'    => 'id'
                    ])
                    ->where(['name LIKE' => '%'.$query.'%'])
                    ->toArray();

                $subcategorias = $Subcategorias
                    ->find('list', [
                        'keyField'      => 'id',
                        'valueField'    => 'id'
                    ])
                    ->where(['name LIKE' => '%'.$query.'%'])
                    ->toArray();

                if(count($objetivos) <= 0){
                    $objetivos = [0];
                }

                if(count($categorias) <= 0){
                    $categorias = [0];
                }

                if(count($subcategorias) <= 0){
                    $subcategorias = [0];
                }

                $search = [
                    'OR' => [
                        'Marcas.name               LIKE'          => '%'.$query.'%',
                        'Sabores.name              LIKE'          => '%'.$query.'%',
                        'ProdutoBase.name          LIKE'          => '%'.$query.'%',
                        'ProdutoBase.dicas         LIKE'          => '%'.$query.'%',
                        'ProdutoBase.atributos     LIKE'          => '%'.$query.'%',
                        'ProdutoBase.beneficios    LIKE'          => '%'.$query.'%',
                        'ProdutoBase.forma_consumo LIKE'          => '%'.$query.'%',
                        'ProdutoBase.quando_tomar  LIKE'          => '%'.$query.'%',
                        'Produtos.embalagem        LIKE'          => '%'.$query.'%',
                        'Produtos.propriedade      LIKE'          => '%'.$query.'%',
                        'Produtos.como_tomar       LIKE'          => '%'.$query.'%',
                        'ProdutoObjetivos.objetivo_id IN'         => $objetivos,
                        'ProdutoCategorias.categoria_id IN'       => $categorias,
                        'ProdutoSubcategorias.subcategoria_id IN' => $subcategorias,
                    ]
                ];

                $similares_query_name = str_replace('%', ' ', $query);

                $similares_query = $this->Produtos
                    ->find('all')
                    ->contain([
                        'Sabores',
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['Sabores' => 'sabores'],
                        ['Sabores.id = Produtos.sabor_id']
                    )
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where($search)
                    ->andWhere(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->order('rand()')
                    ->limit(8)
                    ->all();
            }

            if($tipo_filtro_orig != null && $filtro_orig != null){
                if($tipo_filtro_orig == 'objetivo') {
                    $filtro_id = $Objetivos
                        ->find('all')
                        ->where(['slug' => $filtro_orig])
                        ->andWhere(['status_id' => 1])
                        ->first();

                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $objetivo_escolhida = $filtro_id->id;

                    $search = ['ProdutoObjetivos.objetivo_id' => $filtro_id->id];

                    $similares_objetivo_name = $filtro_id->name;
                    $similares_objetivo_slug = $filtro_id->slug;
                    
                    $similares_objetivo = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();
                } else if($tipo_filtro_orig == 'categoria') {
                    $filtro_id = $Categorias
                        ->find('all')
                        ->contain(['Objetivos'])
                        ->where(['Categorias.slug' => $filtro_orig])
                        ->andWhere(['Categorias.status_id' => 1])
                        ->first();

                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $objetivo_da_categoria = $filtro_id->objetivo->name;

                    $objetivo_escolhida = $filtro_id->objetivo->id;
                    $categoria_escolhida = $filtro_id->id;

                    $search = ['ProdutoCategorias.categoria_id' => $filtro_id->id];
                    $search2 = ['ProdutoObjetivos.objetivo_id' => $filtro_id->objetivo->id];

                    $similares_categoria_name = $filtro_id->name;
                    $similares_categoria_slug = $filtro_id->slug;

                    $similares_categoria = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();

                    $similares_objetivo_name = $filtro_id->objetivo->name;
                    $similares_objetivo_slug = $filtro_id->objetivo->slug;

                    $similares_objetivo = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search2)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();
                } else if($tipo_filtro_orig == 'subcategoria') {
                    $filtro_id = $Subcategorias
                        ->find('all')
                        ->contain(['Categorias', 'Categorias.Objetivos'])
                        ->where(['Subcategorias.slug' => $filtro_orig])
                        ->andWhere(['Subcategorias.status_id' => 1])
                        ->first();

                    $filtro = tirarAcentos(strtolower($filtro_id->name));
                    $filtro_slug = $filtro_id->slug;

                    $categoria_da_subcategoria = $filtro_id->categoria->name;
                    $objetivo_da_categoria = $filtro_id->categoria->objetivo->name;

                    $objetivo_escolhida = $filtro_id->categoria->objetivo->id;
                    $categoria_escolhida = $filtro_id->categoria->id;
                    $subcategoria_escolhida = $filtro_id->id;

                    $search = ['ProdutoSubcategorias.subcategoria_id' => $filtro_id->id];
                    $search2 = ['ProdutoCategorias.categoria_id' => $filtro_id->categoria->id];
                    $search3 = ['ProdutoObjetivos.objetivo_id' => $filtro_id->categoria->objetivo->id];

                    $similares_subcategoria_name = $filtro_id->name;
                    $similares_subcategoria_slug = $filtro_id->slug;

                    $similares_subcategoria = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();

                    $similares_categoria_name = $filtro_id->categoria->name;
                    $similares_categoria_slug = $filtro_id->categoria->slug;

                    $similares_categoria = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search2)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();

                    $similares_objetivo_name = $filtro_id->categoria->objetivo->name;
                    $similares_objetivo_slug = $filtro_id->categoria->objetivo->slug;

                    $similares_objetivo = $this->Produtos
                        ->find('all')
                        ->contain([
                            'Sabores',
                            'ProdutoBase',
                            'ProdutoBase.Marcas',
                            'ProdutoBase.ProdutoObjetivos',
                            'ProdutoBase.ProdutoObjetivos.Objetivos',
                            'ProdutoBase.ProdutoCategorias',
                            'ProdutoBase.ProdutoSubcategorias'
                        ])
                        ->innerJoin(
                            ['Sabores' => 'sabores'],
                            ['Sabores.id = Produtos.sabor_id']
                        )
                        ->innerJoin(
                            ['ProdutoBase' => 'produto_base'],
                            ['ProdutoBase.id = Produtos.produto_base_id']
                        )
                        ->leftJoin(
                            ['ProdutoObjetivos' => 'produto_objetivos'],
                            ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoCategorias' => 'produto_categorias'],
                            ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->leftJoin(
                            ['ProdutoSubcategorias' => 'produto_subcategorias'],
                            ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                        )
                        ->where($search3)
                        ->andWhere(['Produtos.visivel' => 1])
                        ->andWhere(['Produtos.status_id' => 1])
                        ->andWhere(['ProdutoBase.status_id' => 1])
                        ->distinct('Produtos.id')
                        ->order('rand()')
                        ->limit(8)
                        ->all();
                }
            }

            if($marca_orig != null) {

                $marca_id = $Marcas
                    ->find('all')
                    ->where(['slug' => $marca_orig])
                    ->andWhere(['status_id' => 1])
                    ->first();

                $marca_escolhida = $marca_id->id;

                $search = ['Marcas.id' => $marca_id->id];

                $similares_marca_name = $marca_id->name;
                $similares_marca_slug = $marca_id->slug;

                $similares_marca = $this->Produtos
                    ->find('all')
                    ->contain([
                        'Sabores',
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['Sabores' => 'sabores'],
                        ['Sabores.id = Produtos.sabor_id']
                    )
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where($search)
                    ->andWhere(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->order('rand()')
                    ->limit(8)
                    ->all();
            }
        } else {
            $similares_query = [];
            $similares_query_name = [];
            $similares_subcategoria = [];
            $similares_subcategoria_name = [];
            $similares_subcategoria_slug = [];
            $similares_categoria = [];
            $similares_categoria_name = [];
            $similares_categoria_slug = [];
            $similares_objetivo = [];
            $similares_objetivo_name = [];
            $similares_objetivo_slug = [];
            $similares_marca = [];
            $similares_marca_name = [];
            $similares_marca_slug = [];
        }

        $query = str_replace('%', ' ', $query);
        $marca = str_replace(' ', '-', $marca);
        $filtro = str_replace(' ', '-', $filtro);

        $links_breadcrumb[] = [
            'url' => '/comparador',
            'name' => 'Comparador - Produtos'
        ];
        if($marca != null) {
            $links_breadcrumb[] = [
                'url' => '/comparador/0/'.$marca_id->slug,
                'name' => $marca_id->name
            ];
        }

        $this->set(compact('query', 'marca', 'tipo_filtro', 'filtro', 'ordenacao', 'produtos', 'count_produtos', 'categoria_da_subcategoria', 'objetivo_da_categoria', 'links_breadcrumb', 'marca_escolhida', 'objetivo_escolhida', 'categoria_escolhida', 'subcategoria_escolhida', 'similares_query', 'similares_subcategoria', 'similares_categoria', 'similares_objetivo', 'similares_marca', 'similares_query_name', 'similares_subcategoria_name', 'similares_categoria_name', 'similares_objetivo_name', 'similares_marca_name', 'similares_subcategoria_slug', 'similares_categoria_slug', 'similares_objetivo_slug', 'similares_marca_slug', 'filtro_slug'));
        $this->viewBuilder()->layout('ajax');
    }

    public function similares_comparador($slug_produto = null) {

        if($slug_produto) {
            $session = $this->request->session();

            $Academia = $session->read('Academia');
            $Marcas = TableRegistry::get('Marcas');
            $Objetivos = TableRegistry::get('Objetivos');
            $Categorias = TableRegistry::get('Categorias');
            $Subcategorias = TableRegistry::get('Subcategorias');

            $produto_escolhido = $this->Produtos
                ->find('all')
                ->contain([
                    'Sabores',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoCategorias.Categorias',
                    'ProdutoBase.ProdutoSubcategorias',
                    'ProdutoBase.ProdutoSubcategorias.Subcategorias'
                ])
                ->innerJoin(
                    ['Sabores' => 'sabores'],
                    ['Sabores.id = Produtos.sabor_id']
                )
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Produtos.slug' => $slug_produto])
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->first();

            foreach ($produto_escolhido->produto_base->produto_objetivos as $produto_escolhido_objetivos) {
                $produto_objetivos[] = $produto_escolhido_objetivos->objetivo;
            }

            if(count($produto_objetivos) <= 0) {
                $produto_objetivos = [];
            }

            foreach ($produto_escolhido->produto_base->produto_categorias as $produto_escolhido_categorias) {
                $produto_categorias[] = $produto_escolhido_categorias->categoria;
            }

            if(count($produto_categorias) <= 0) {
                $produto_categorias = [];
            }

            foreach ($produto_escolhido->produto_base->produto_subcategorias as $produto_escolhido_subcategorias) {
                $produto_subcategorias[] = $produto_escolhido_subcategorias->subcategoria;
            }

            if(count($produto_subcategorias) <= 0) {
                $produto_subcategorias = [];
            }

            $similares_produto_base = $this->Produtos
                ->find('all')
                ->contain([
                    'Sabores',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['Sabores' => 'sabores'],
                    ['Sabores.id = Produtos.sabor_id']
                )
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Produtos.produto_base_id' => $produto_escolhido->produto_base_id])
                ->andWhere(['Produtos.id NOT IN' => $produto_escolhido->id])
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['Produtos.status_id' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->all();

            $similares_marca = $this->Produtos
                ->find('all')
                ->contain([
                    'Sabores',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['Sabores' => 'sabores'],
                    ['Sabores.id = Produtos.sabor_id']
                )
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Marcas.id' => $produto_escolhido->produto_base->marca_id])
                ->andWhere(['Produtos.id NOT IN' => $produto_escolhido->id])
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['Produtos.status_id' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('Produtos.id')
                ->order('rand()')
                ->limit(8)
                ->all();

            $similares_marca_name = $produto_escolhido->produto_base->marca->name;
            $similares_marca_slug = $produto_escolhido->produto_base->marca->slug;

            $ctrl = 0;
            foreach ($produto_objetivos as $produto_objetivo) {

                $similares_objetivo[] = $this->Produtos
                    ->find('all')
                    ->contain([
                        'Sabores',
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['Sabores' => 'sabores'],
                        ['Sabores.id = Produtos.sabor_id']
                    )
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where(['ProdutoObjetivos.objetivo_id' => $produto_objetivo->id])
                    ->andWhere(['Produtos.id NOT IN' => $produto_escolhido->id])
                    ->andWhere(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->order('rand()')
                    ->limit(8)
                    ->all();

                $similares_objetivo_name[$ctrl] = $produto_objetivo->name;
                $similares_objetivo_slug[$ctrl] = $produto_objetivo->slug;

                $ctrl++;
            }

            $ctrl = 0;
            foreach ($produto_categorias as $produto_categoria) {

                $similares_categoria[] = $this->Produtos
                    ->find('all')
                    ->contain([
                        'Sabores',
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['Sabores' => 'sabores'],
                        ['Sabores.id = Produtos.sabor_id']
                    )
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where(['ProdutoCategorias.categoria_id' => $produto_categoria->id])
                    ->andWhere(['Produtos.id NOT IN' => $produto_escolhido->id])
                    ->andWhere(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->order('rand()')
                    ->limit(8)
                    ->all();

                $similares_categoria_name[$ctrl] = $produto_categoria->name;
                $similares_categoria_slug[$ctrl] = $produto_categoria->slug;

                $ctrl++;
            }

            $ctrl = 0;
            foreach ($produto_subcategorias as $produto_subcategoria) {

                $similares_subcategoria[] = $this->Produtos
                    ->find('all')
                    ->contain([
                        'Sabores',
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['Sabores' => 'sabores'],
                        ['Sabores.id = Produtos.sabor_id']
                    )
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where(['ProdutoSubcategorias.subcategoria_id' => $produto_subcategoria->id])
                    ->andWhere(['Produtos.id NOT IN' => $produto_escolhido->id])
                    ->andWhere(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->order('rand()')
                    ->limit(8)
                    ->all();

                $similares_subcategoria_name[$ctrl] = $produto_subcategoria->name;
                $similares_subcategoria_slug[$ctrl] = $produto_subcategoria->slug;

                $ctrl++;
            }

            $links_breadcrumb[] = [
                'url' => '',
                'name' => 'Comparador - Produtos'
            ];

            $this->set(compact('links_breadcrumb', 'similares_produto_base', 'similares_subcategoria', 'similares_categoria', 'similares_objetivo', 'similares_marca', 'similares_subcategoria_name', 'similares_categoria_name', 'similares_objetivo_name', 'similares_marca_name', 'similares_subcategoria_slug', 'similares_categoria_slug', 'similares_objetivo_slug', 'similares_marca_slug', 'produto_escolhido'));
            $this->viewBuilder()->layout('default-comparador');
        } else {
            return $this->redirect('/comparador');
        }
    }

    public function view_comparador($slug = null){

        $session  = $this->request->session();

        $Academia = $session->read('Academia');
        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        if($Academia) {
            $url_atual = explode('/comparador', $_SERVER['REQUEST_URI']);
            return $this->redirect('/'.$Academia->slug.$url_atual[1]);
        }

        if($slug == null){
            $this->redirect('/');
        }

        if(isset($_GET['ind_id']) && $_GET['ind_id'] > 0) {
            $session->write('Indicacao', $_GET['ind_id']);
        }

        $produto = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                    'ProdutoComentarios',
                    'ProdutoSubstancias',
                    'ProdutoSubstancias.Substancias'
                ]
            ])
            ->where(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.preco > ' => 0.1])
            ->andWhere(['Produtos.slug' => $slug])
            ->first();

        if(count($produto) <= 0) {
            $this->redirect('/');
        }

        $produtoView = $session->read('produtoView-'.$produto->id);

        if(!$produtoView) {
            $produto = $this->Produtos->patchEntity($produto, ['views' => $produto->views+1]);
            if($this->Produtos->save($produto)) {
                $session->write('produtoView-'.$produto->id, 1);
            }
        }

        $produtos = $this->Produtos
            ->find('all', [
                'contain' => [
                    'Status',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                    'ProdutoComentarios',
                ]
            ])
            ->where(['Produtos.visivel'            => 1])
            ->andWhere(['Produtos.preco > '        => 0.1])
            ->andWhere(['Produtos.produto_base_id' => $produto->produto_base_id])
            ->andWhere(['Produtos.id <>'           => $produto->id])
            ->all();

        $produto_tamanhos = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                    'ProdutoComentarios',
                ]
            ])
            ->where(['Produtos.visivel'           => 1])
            ->andWhere(['Produtos.preco > '         => 0.1])
            ->andWhere(['Produtos.produto_base_id'  => $produto->produto_base_id])
            ->andWhere(['Produtos.id <>'            => $produto->id])
            ->distinct(['Produtos.tamanho', 'Produtos.embalagem'])
            ->all();

        $combina_com = unserialize($produto->produto_base->combina);

        if($combina_com != null){

            //separar cada objetivo/categoria/subcategoria do combina_com

            $produto_combinations = $this->Produtos
                ->find('all')
                ->contain([
                    'Sabores',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['Sabores' => 'sabores'],
                    ['Sabores.id = Produtos.sabor_id']
                )
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Produtos.preco > '     => 0.1])
                ->andWhere(['Produtos.status_id' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                //procurar produtos que estão nos objetivos/categorias/subcategorias do combina_com
                ->distinct(['ProdutoBase.id'])
                ->order('rand()')
                ->limit(9)
                ->all();
        }else{

            $produto_objetivos = TableRegistry::get('ProdutoObjetivos')
                ->find('all')
                ->where(['produto_base_id' => $produto->produto_base_id])
                ->all();

            foreach ($produto_objetivos as $produto_objetivo) {
                $objetivos[] = $produto_objetivo->objetivo_id;
            }

            if(count($objetivos) <= 0) {
                $objetivos = [0];
            }

            $produto_categorias = TableRegistry::get('ProdutoCategorias')
                ->find('all')
                ->where(['produto_base_id' => $produto->produto_base_id])
                ->all();

            foreach ($produto_categorias as $produto_categoria) {
                $categoria_produtos = $this->Produtos
                    ->find('all')
                    ->contain([
                        'Sabores',
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['Sabores' => 'sabores'],
                        ['Sabores.id = Produtos.sabor_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->andWhere(['ProdutoCategorias.categoria_id IN' => $produto_categoria->categoria_id])
                    ->all();

                foreach ($categoria_produtos as $categoria_produto) {
                    $produtos_nao_exibir_ids[] = $categoria_produto->id;
                }
            }

            if(count($produtos_nao_exibir_ids) <= 0) {
                $produtos_nao_exibir_ids = [0];
            }

            $produto_subcategorias = TableRegistry::get('ProdutoSubcategorias')
                ->find('all')
                ->where(['produto_base_id' => $produto->produto_base_id])
                ->all();

            foreach ($produto_subcategorias as $produto_subcategoria) {
                $subcategoria_produtos = $this->Produtos
                    ->find('all')
                    ->contain([
                        'Sabores',
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['Sabores' => 'sabores'],
                        ['Sabores.id = Produtos.sabor_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->andWhere(['ProdutoSubcategorias.subcategoria_id IN' => $produto_subcategoria->subcategoria_id])
                    ->all();

                foreach ($subcategoria_produtos as $subcategoria_produto) {
                    $produtos_nao_exibir_ids[] = $subcategoria_produto->id;
                }
            }

            if(count($produtos_nao_exibir_ids) <= 0) {
                $produtos_nao_exibir_ids = [0];
            }

            $produto_combinations = $this->Produtos
                ->find('all')
                ->contain([
                    'Sabores',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['Sabores' => 'sabores'],
                    ['Sabores.id = Produtos.sabor_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Produtos.visivel' => 1])
                ->andWhere(['Produtos.status_id' => 1])
                ->andWhere(['Produtos.id NOT IN' => $produtos_nao_exibir_ids])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivos])
                ->distinct(['ProdutoBase.id'])
                ->order('rand()')
                ->limit(9)
                ->all();
        }

        $session    = $this->request->session();

        $session_produto = $session->read('Carrinho');

        $session_cliente = $session->read('Cliente');

        $session_produto_ids = [];
        if(!empty($session_produto)) {
            foreach ($session_produto as $key => $item) {
                $session_produto_ids[] = $key;
            }
        }

        if(!empty($session_produto_ids)) {
            $s_produtos = $this->Produtos
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->where(['Produtos.id IN' => $session_produto_ids])
                ->all();
        }else{
            $s_produtos = [];
        }

        $prod_objetivos = $produtos;

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();

        $cupom_desconto = $session->read('CupomDesconto');

        $links_breadcrumb[] = [
            'url' => '/comparador',
            'name' => 'Comparador - Produtos'
        ];
        $links_breadcrumb[] = [
            'url' => '/comparador/0/'.$produto->produto_base->marca->slug,
            'name' => $produto->produto_base->marca->name
        ];
        $links_breadcrumb[] = [
            'url' => '/comparador/produto/'.$produto->slug,
            'name' => $produto->produto_base->name
        ];

        $this->set(compact('academia', 'cupom_desconto', 'prod_objetivos', 's_produtos', 'session_produto', 'session_cliente', 'produto', 'produtos', 'produto_combinations', 'produto_tamanhos', 'links_breadcrumb'));
        $this->set('_serialize', ['produto']);
        $this->viewBuilder()->layout('default-comparador');
    }

    /**
     * BOTAO COMPARAR - ADICIONA ITEM AO COMPARADOR
     * @param $id
     */
    public function adicionar_comparador($id){
        if($id) {
            $session = $this->request->session();
            $Academia = $session->read('Academia');

            $SSlug = '/'.$Academia->slug;

            $session->write('Comparador.'.$id, 1);

            $ComparadorProdutos = $session->read('Comparador');

            $qtd_comparador = 0;

            foreach ($ComparadorProdutos as $key => $value) {
                $comparador_ids[] = $key;
                $qtd_comparador++;
            }

            $produtos_comparador = $this->Produtos
                ->find('all')
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias',
                    'ProdutoSubstancias',
                    'ProdutoSubstancias.Substancias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoSubstancias' => 'produto_substancias'],
                    ['ProdutoSubstancias.produto_id = Produtos.id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Produtos.visivel' => 1])
                ->andWhere(['Produtos.id IN' => $comparador_ids])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('Produtos.id')
                ->all();

            $this->set(compact('produtos_comparador', 'qtd_comparador', 'SSlug'));

            $this->viewBuilder()->layout('ajax');
        }
    }

    /**
     * BOTAO REMOVER DO COMPARADOR - REMOVE ITEM DO COMPARADOR
     * @param $id
     */
    public function remover_comparador($id){
        if($id) {
            $session = $this->request->session();
            $Academia = $session->read('Academia');

            $SSlug = '/'.$Academia->slug;

            $ComparadorProdutos = $session->read('Comparador');

            if(isset($ComparadorProdutos[$id])){
                $session->delete('Comparador.'.$id);
            }

            $ComparadorProdutos = $session->read('Comparador');

            $qtd_comparador = 0;

            foreach ($ComparadorProdutos as $key => $value) {
                $comparador_ids[] = $key;
                $qtd_comparador++;
            }

            if(count($comparador_ids) >= 1) {
                $produtos_comparador = $this->Produtos
                    ->find('all')
                    ->contain([
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.Propriedades',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias',
                        'ProdutoSubstancias',
                        'ProdutoSubstancias.Substancias'
                    ])
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoSubstancias' => 'produto_substancias'],
                        ['ProdutoSubstancias.produto_id = Produtos.id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.id IN' => $comparador_ids])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->all();
            } else {
                $produtos_comparador = [];
            }

            $this->set(compact('produtos_comparador', 'qtd_comparador', 'SSlug'));

            $this->viewBuilder()->layout('ajax');
        }
    }

    /**
     * BOTAO COMPARAR - ADICIONA ITEM AO COMPARADOR
     * @param $id
     */
    public function adicionar_comparador_comparador($id){
        if($id) {
            $session = $this->request->session();

            $session->write('Comparador.'.$id, 1);

            $ComparadorProdutos = $session->read('Comparador');

            $qtd_comparador = 0;

            foreach ($ComparadorProdutos as $key => $value) {
                $comparador_ids[] = $key;
                $qtd_comparador++;
            }

            $produtos_comparador = $this->Produtos
                ->find('all')
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias',
                    'ProdutoSubstancias',
                    'ProdutoSubstancias.Substancias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoSubstancias' => 'produto_substancias'],
                    ['ProdutoSubstancias.produto_id = Produtos.id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Produtos.visivel' => 1])
                ->andWhere(['Produtos.id IN' => $comparador_ids])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('Produtos.id')
                ->all();

            $this->set(compact('produtos_comparador', 'qtd_comparador'));

            $this->viewBuilder()->layout('ajax');
        }
    }

    /**
     * BOTAO REMOVER DO COMPARADOR - REMOVE ITEM DO COMPARADOR
     * @param $id
     */
    public function remover_comparador_comparador($id){
        if($id) {
            $session = $this->request->session();

            $ComparadorProdutos = $session->read('Comparador');

            if(isset($ComparadorProdutos[$id])){
                $session->delete('Comparador.'.$id);
            }

            $ComparadorProdutos = $session->read('Comparador');

            $qtd_comparador = 0;

            foreach ($ComparadorProdutos as $key => $value) {
                $comparador_ids[] = $key;
                $qtd_comparador++;
            }

            if(count($comparador_ids) >= 1) {
                $produtos_comparador = $this->Produtos
                    ->find('all')
                    ->contain([
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.Propriedades',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias',
                        'ProdutoSubstancias',
                        'ProdutoSubstancias.Substancias'
                    ])
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoSubstancias' => 'produto_substancias'],
                        ['ProdutoSubstancias.produto_id = Produtos.id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.id IN' => $comparador_ids])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->distinct('Produtos.id')
                    ->all();
            } else {
                $produtos_comparador = [];
            }

            $this->set(compact('produtos_comparador', 'qtd_comparador'));

            $this->viewBuilder()->layout('ajax');
        }
    }

    public function promocoes() {

        $session = $this->request->session();

        $Academia = $session->read('Academia');

        $produtos = $this->Produtos
            ->find('all')
            ->contain([
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoObjetivos.Objetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['Objetivos' => 'objetivos'],
                ['ProdutoObjetivos.objetivo_id = Objetivos.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->where(['Produtos.preco_promo >=' => 1.0])
            ->where(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.estoque >=' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->distinct('Produtos.id')
            ->order(['rand()'])
            ->limit(12)
            ->all();

        $links_breadcrumb[] = [
            'url' => '',
            'name' => $Academia->shortname.' - loja'
        ];
        $links_breadcrumb[] = [
            'url' => '/promocoes',
            'name' => 'Promoções'
        ];

        $banners_promocoes = TableRegistry::get('Banners')
            ->find('all')
            ->where(['type' => 'promocoes'])
            ->andWhere(['visivel' => 1])
            ->all();

        $this->set(compact('banners_promocoes','produtos', 'links_breadcrumb', 'Academia'));
        $this->viewBuilder()->layout('default');
    }

    public function promocoes_atualizar_produtos() {

        $session = $this->request->session();

        $Academia = $session->read('Academia');

        $produtos_ids = $this->request->data['produtos_ids'];

        $produtos = $this->Produtos
            ->find('all')
            ->contain([
                'Sabores',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoObjetivos.Objetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['Sabores' => 'sabores'],
                ['Sabores.id = Produtos.sabor_id']
            )
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['Objetivos' => 'objetivos'],
                ['ProdutoObjetivos.objetivo_id = Objetivos.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->where(['Produtos.preco_promo >=' => 1.0])
            ->where(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.estoque >=' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->andWhere(['Produtos.id NOT IN' => $produtos_ids])
            ->distinct('Produtos.id')
            ->order(['rand()'])
            ->limit(12)
            ->all();

        $this->set(compact('produtos', 'Academia'));
        $this->viewBuilder()->layout('ajax');
    }

    public function promocoes_comparador() {

        $produtos = $this->Produtos
            ->find('all')
            ->contain([
                'Sabores',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoObjetivos.Objetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['Sabores' => 'sabores'],
                ['Sabores.id = Produtos.sabor_id']
            )
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['Objetivos' => 'objetivos'],
                ['ProdutoObjetivos.objetivo_id = Objetivos.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            //->where(['Produtos.preco_promo >=' => 1.0])
            ->where(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.status_id' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->distinct('Produtos.id')
            ->order(['rand()'])
            ->limit(12)
            ->all();

        $links_breadcrumb[] = [
            'url' => '',
            'name' => 'Comparador'
        ];
        $links_breadcrumb[] = [
            'url' => '/comparador/promocoes',
            'name' => 'Promoções'
        ];

        $this->set(compact('produtos', 'links_breadcrumb'));
        $this->viewBuilder()->layout('default-comparador');
    }

    public function promocoes_comparador_atualizar_produtos() {

        $produtos_ids = $this->request->data['produtos_ids'];

        $produtos = $this->Produtos
            ->find('all')
            ->contain([
                'Sabores',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoObjetivos.Objetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['Sabores' => 'sabores'],
                ['Sabores.id = Produtos.sabor_id']
            )
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['Objetivos' => 'objetivos'],
                ['ProdutoObjetivos.objetivo_id = Objetivos.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            //->where(['Produtos.preco_promo >=' => 1.0])
            ->where(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.status_id' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->andWhere(['Produtos.id NOT IN' => $produtos_ids])
            ->distinct('Produtos.id')
            ->order(['rand()'])
            ->limit(12)
            ->all();

        $this->set(compact('produtos'));
        $this->viewBuilder()->layout('ajax');
    }

    public function mais_vendidos() {
        $session = $this->request->session();

        $Academia = $session->read('Academia');

        $MaisVendidos = TableRegistry::get('MaisVendidos');

        $mais_vendidos = $MaisVendidos
            ->find('all')
            ->contain([
                'Produtos',
                'Produtos.ProdutoBase',
                'Produtos.ProdutoBase.Marcas',
                'Produtos.ProdutoBase.ProdutoObjetivos',
                'Produtos.ProdutoBase.ProdutoObjetivos.Objetivos',
                'Produtos.ProdutoBase.ProdutoCategorias',
                'Produtos.ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['Produtos' => 'produtos'],
                ['Produtos.id = MaisVendidos.produto_id']
            )
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['Objetivos' => 'objetivos'],
                ['ProdutoObjetivos.objetivo_id = Objetivos.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->andWhere(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.status_id' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->distinct('Produtos.id')
            ->order(['posicao' => 'asc'])
            ->all();

        foreach ($mais_vendidos as $mais_vendido) {
            $produtos[] = $mais_vendido->produto;
        }

        $links_breadcrumb[] = [
            'url' => '',
            'name' => $Academia->shortname.' - loja'
        ];
        $links_breadcrumb[] = [
            'url' => '/promocoes',
            'name' => 'Promoções'
        ];

        $this->set(compact('produtos', 'links_breadcrumb', 'Academia'));
        $this->viewBuilder()->layout('default');
    }

    public function mais_vendidos_comparador() {

        $MaisVendidos = TableRegistry::get('MaisVendidos');

        $mais_vendidos = $MaisVendidos
            ->find('all')
            ->contain([
                'Produtos',
                'Produtos.Sabores',
                'Produtos.ProdutoBase',
                'Produtos.ProdutoBase.Marcas',
                'Produtos.ProdutoBase.ProdutoObjetivos',
                'Produtos.ProdutoBase.ProdutoObjetivos.Objetivos',
                'Produtos.ProdutoBase.ProdutoCategorias',
                'Produtos.ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['Produtos' => 'produtos'],
                ['Produtos.id = MaisVendidos.produto_id']
            )
            ->innerJoin(
                ['Sabores' => 'sabores'],
                ['Sabores.id = Produtos.sabor_id']
            )
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['Objetivos' => 'objetivos'],
                ['ProdutoObjetivos.objetivo_id = Objetivos.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->andWhere(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.status_id' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->distinct('Produtos.id')
            ->order(['posicao' => 'asc'])
            ->all();

        foreach ($mais_vendidos as $mais_vendido) {
            $produtos[] = $mais_vendido->produto;
        }

        $links_breadcrumb[] = [
            'url' => '',
            'name' => $Academia->shortname.' - loja'
        ];
        $links_breadcrumb[] = [
            'url' => '/promocoes',
            'name' => 'Promoções'
        ];

        $this->set(compact('produtos', 'links_breadcrumb'));
        $this->viewBuilder()->layout('default-comparador');
    }

    public function catalogo() {

        $session = $this->request->session();
        $academia = $session->read('Academia');

        if(!$academia) {
            return $this->redirect('/');
        }

        $produtos_desk = $this->Produtos
            ->find('all')
            ->contain([
                'Sabores',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoObjetivos.Objetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['Sabores' => 'sabores'],
                ['Sabores.id = Produtos.sabor_id']
            )
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['Objetivos' => 'objetivos'],
                ['ProdutoObjetivos.objetivo_id = Objetivos.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->where(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.status_id' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->distinct('Produtos.id')
            ->order(['Marcas.id' => 'asc', 'ProdutoObjetivos.objetivo_id' => 'asc'])
            ->all();

        $produtos = $this->Produtos
            ->find('all')
            ->contain([
                'Sabores',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoObjetivos.Objetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['Sabores' => 'sabores'],
                ['Sabores.id = Produtos.sabor_id']
            )
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['Objetivos' => 'objetivos'],
                ['ProdutoObjetivos.objetivo_id = Objetivos.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->where(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.status_id' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->distinct('Produtos.id')
            ->order(['Marcas.id' => 'asc', 'ProdutoObjetivos.objetivo_id' => 'asc'])
            ->limit(12)
            ->all();

        $this->set(compact('produtos_desk', 'produtos'));

        $this->viewBuilder()->layout('default-catalogo');
    }

    public function catalogo_atualizar_produtos() {

        $session = $this->request->session();
        $academia = $session->read('Academia');

        $produtos_ids = $this->request->data['produtos_ids'];

        $produtos = $this->Produtos
            ->find('all')
            ->contain([
                'Sabores',
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoObjetivos.Objetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['Sabores' => 'sabores'],
                ['Sabores.id = Produtos.sabor_id']
            )
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['Objetivos' => 'objetivos'],
                ['ProdutoObjetivos.objetivo_id = Objetivos.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->where(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.status_id' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->andWhere(['Produtos.id NOT IN' => $produtos_ids])
            ->distinct('Produtos.id')
            ->order(['Marcas.id' => 'asc', 'ProdutoObjetivos.objetivo_id' => 'asc'])
            ->limit(12)
            ->all();

        $this->set(compact('produtos'));

        $this->render('catalogo_atualizar_produtos');
        $this->viewBuilder()->layout(false);
    }

    public function montar_catalogo() {

        $session = $this->request->session();
        $academia = $session->read('Academia');

        if(!$academia) {
            return $this->redirect('/');
        }

        $this->viewBuilder()->layout('default-catalogo');
    }

    public function catalogo_preview() { 

        $session = $this->request->session();
        $academia = $session->read('Academia');

        if(!$academia) {
            return $this->redirect('/');
        }

        if(isset($_GET['marcas']) && isset($_GET['estoque']) && isset($_GET['valor'])) {
            $marcas = explode(',', $_GET['marcas']);
            $tem_estoque = $_GET['estoque'];
            $tem_valor = $_GET['valor'];

            $marcas[] = 49;

            if($tem_estoque) {
                $estoque = ['Produtos.status_id' => 1];
            } else {
                $estoque = [];
            }

            $produtos = $this->Produtos
                ->find('all')
                ->contain([
                    'Sabores',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['Sabores' => 'sabores'],
                    ['Sabores.id = Produtos.sabor_id']
                )
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['Objetivos' => 'objetivos'],
                    ['ProdutoObjetivos.objetivo_id = Objetivos.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Marcas.id IN' => $marcas])
                ->andWhere($estoque)
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->distinct('Produtos.id')
                ->order(['Marcas.id' => 'asc', 'ProdutoObjetivos.objetivo_id' => 'asc'])
                ->all();

            $this->set(compact('tem_valor', 'produtos'));
        } else {
            return $this->redirect('/'.$academia->slug.'/catalogo');
        }

        $this->viewBuilder()->layout('default-catalogo');
    }

    public function teste_preview() {

        
         $this->viewBuilder()->layout('default-catalogo');
    }

    public function montar_combo($number = 1) {

        $session = $this->request->session();

        $academia = $session->read('Academia');
        $TipoCombo = $session->read('TipoCombo');

        $Categorias = TableRegistry::get('Categorias');
        $Subcategorias = TableRegistry::get('Subcategorias');

        if(!$academia) {
            return $this->redirect('/');
        }

        if($TipoCombo) {
            $ProdutoCombo1 = $session->read('ProdutoCombo1');
            $ProdutoCombo2 = $session->read('ProdutoCombo2');
            $ProdutoCombo3 = $session->read('ProdutoCombo3');

            if($number == 1) {
                if(isset($TipoCombo['categoria1']['objetivo_id'])) {
                    $search = ['ProdutoCategorias.categoria_id' => $TipoCombo['categoria1']['id']];
                } else {
                    $search = ['ProdutoSubcategorias.subcategoria_id' => $TipoCombo['categoria1']['id']];
                }
            } else if($number == 2) {
                if(isset($TipoCombo['categoria2']['objetivo_id'])) {
                    $search = ['ProdutoCategorias.categoria_id' => $TipoCombo['categoria2']['id']];
                } else {
                    $search = ['ProdutoSubcategorias.subcategoria_id' => $TipoCombo['categoria2']['id']];
                }
            } else {
                if(isset($TipoCombo['categoria3']['objetivo_id'])) {
                    $search = ['ProdutoCategorias.categoria_id' => $TipoCombo['categoria3']['id']];
                } else {
                    $search = ['ProdutoSubcategorias.subcategoria_id' => $TipoCombo['categoria3']['id']];
                }
            }

            $produtos = $this->Produtos
                ->find('all')
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['Objetivos' => 'objetivos'],
                    ['ProdutoObjetivos.objetivo_id = Objetivos.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where($search)
                ->andWhere(['Produtos.visivel' => 1])
                ->andWhere(['Produtos.status_id' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->andWhere(['ProdutoBase.name NOT LIKE' => '%combo%'])
                ->distinct('Produtos.id')
                ->order(['Produtos.status_id' => 'asc', 'rand()'])
                ->limit(12)
                ->all();
        } else {
            return $this->redirect('/home');
        }

        $this->set(compact('TipoCombo', 'ProdutoCombo1', 'ProdutoCombo2', 'ProdutoCombo3', 'produtos', 'number'));
        $this->viewBuilder()->layout('default-combo');
    }

    public function montar_combo_atualizar_produtos($number = 1) {

        $produtos_ids = $this->request->data['produtos_ids'];
        
        $session = $this->request->session();

        $academia = $session->read('Academia');
        $TipoCombo = $session->read('TipoCombo');

        $Categorias = TableRegistry::get('Categorias');
        $Subcategorias = TableRegistry::get('Subcategorias');

        $ProdutoCombo1 = $session->read('ProdutoCombo1');
        $ProdutoCombo2 = $session->read('ProdutoCombo2');
        $ProdutoCombo3 = $session->read('ProdutoCombo3');

        if($number == 1) {
            if(isset($TipoCombo['categoria1']['objetivo_id'])) {
                $search = ['ProdutoCategorias.categoria_id' => $TipoCombo['categoria1']['id']];
            } else {
                $search = ['ProdutoSubcategorias.subcategoria_id' => $TipoCombo['categoria1']['id']];
            }
        } else if($number == 2) {
            if(isset($TipoCombo['categoria2']['objetivo_id'])) {
                $search = ['ProdutoCategorias.categoria_id' => $TipoCombo['categoria2']['id']];
            } else {
                $search = ['ProdutoSubcategorias.subcategoria_id' => $TipoCombo['categoria2']['id']];
            }
        } else {
            if(isset($TipoCombo['categoria3']['objetivo_id'])) {
                $search = ['ProdutoCategorias.categoria_id' => $TipoCombo['categoria3']['id']];
            } else {
                $search = ['ProdutoSubcategorias.subcategoria_id' => $TipoCombo['categoria3']['id']];
            }
        }

        $produtos = $this->Produtos
            ->find('all')
            ->contain([
                'ProdutoBase',
                'ProdutoBase.Marcas',
                'ProdutoBase.ProdutoObjetivos',
                'ProdutoBase.ProdutoObjetivos.Objetivos',
                'ProdutoBase.ProdutoCategorias',
                'ProdutoBase.ProdutoSubcategorias'
            ])
            ->innerJoin(
                ['ProdutoBase' => 'produto_base'],
                ['ProdutoBase.id = Produtos.produto_base_id']
            )
            ->leftJoin(
                ['ProdutoObjetivos' => 'produto_objetivos'],
                ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['Objetivos' => 'objetivos'],
                ['ProdutoObjetivos.objetivo_id = Objetivos.id']
            )
            ->leftJoin(
                ['ProdutoCategorias' => 'produto_categorias'],
                ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
            )
            ->leftJoin(
                ['ProdutoSubcategorias' => 'produto_subcategorias'],
                ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
            )
            ->where($search)
            ->andWhere(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.status_id' => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->andWhere(['Produtos.id NOT IN' => $produtos_ids])
            ->andWhere(['ProdutoBase.name NOT LIKE' => '%combo%'])
            ->distinct('Produtos.id')
            ->order(['Produtos.status_id' => 'asc', 'rand()'])
            ->limit(12)
            ->all();

        $this->set(compact('produtos'));
        $this->render('montar_combo_atualizar_produtos');
        $this->viewBuilder()->layout('default-combo');
    }

    public function selecionar_tipo_combo($id) {

        $session = $this->request->session();

        $session->delete('ProdutoCombo1');
        $session->delete('ProdutoCombo2');
        $session->delete('ProdutoCombo3');

        $academia = $session->read('Academia');

        $combo_tipo = TableRegistry::get('ComboTipos')
            ->find('all')
            ->where(['id' => $id])
            ->first();

        $Categorias = TableRegistry::get('Categorias');
        $Subcategorias = TableRegistry::get('Subcategorias');

        $categoria1 = explode('-', $combo_tipo->categoria_id1);
        $categoria2 = explode('-', $combo_tipo->categoria_id2);
        $categoria3 = explode('-', $combo_tipo->categoria_id3);

        if($categoria1[0] == 'c') {
            $primeira_categoria = $Categorias
                ->find('all')
                ->where(['id' => $categoria1[1]])
                ->first();
        } else {
            $primeira_categoria = $Subcategorias
                ->find('all')
                ->where(['id' => $categoria1[1]])
                ->first();
        }

        if($categoria2[0] == 'c') {
            $segunda_categoria = $Categorias
                ->find('all')
                ->where(['id' => $categoria2[1]])
                ->first();
        } else {
            $segunda_categoria = $Subcategorias
                ->find('all')
                ->where(['id' => $categoria2[1]])
                ->first();
        }

        if($categoria3[0] == 'c') {
            $terceira_categoria = $Categorias
                ->find('all')
                ->where(['id' => $categoria3[1]])
                ->first();
        } else {
            $terceira_categoria = $Subcategorias
                ->find('all')
                ->where(['id' => $categoria3[1]])
                ->first();
        }


        $tipo_combo = [
            'id' => $combo_tipo->id,
            'name' => $combo_tipo->name,
            'categoria1' => $primeira_categoria,
            'categoria2' => $segunda_categoria,
            'categoria3' => $terceira_categoria
        ];

        $session->write('TipoCombo', $tipo_combo);

        return $this->redirect('/'.$academia->slug.'/montar-combo');
        $this->render(false);
    }

    public function salvar_combo($number, $produto_id) {

        $session = $this->request->session();

        $academia = $session->read('Academia');

        $Produtos = TableRegistry::get('Produtos');

        $produto = $Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                    'ProdutoSubstancias',
                    'ProdutoSubstancias.Substancias',
                ]
            ])
            ->where(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.preco > ' => 0.1])
            ->andWhere(['Produtos.id' => $produto_id])
            ->first();

        if($number == 1) {
            $session->write('ProdutoCombo1', $produto);
        } else if($number == 2) {
            $session->write('ProdutoCombo2', $produto);
        } else {
            $session->write('ProdutoCombo3', $produto);
        }

        $ProdutoCombo1 = $session->read('ProdutoCombo1');
        $ProdutoCombo2 = $session->read('ProdutoCombo2');
        $ProdutoCombo3 = $session->read('ProdutoCombo3');

        if($ProdutoCombo1 && $ProdutoCombo2 && $ProdutoCombo3) {
            $prox_number = 4;
        } else {
            $prox_number = $number+1;
        }

        return $this->redirect('/'.$academia->slug.'/montar-combo/'.$prox_number);
        $this->render(false);
    }

    public function fechar_combo() {
        $session = $this->request->session();

        $academia = $session->read('Academia');

        $ProdutoCombo1 = $session->read('ProdutoCombo1');
        $ProdutoCombo2 = $session->read('ProdutoCombo2');
        $ProdutoCombo3 = $session->read('ProdutoCombo3');

        $session->delete('Carrinho');
        $session->delete('PedidoFinalizado');
        $session->delete('PedidoFinalizadoItem');
        $session->delete('ClienteFinalizado');
        $session->delete('UsuarioDados');

        $session->write('Carrinho.'.$ProdutoCombo1->id, 1);
        $session->write('Carrinho.'.$ProdutoCombo2->id, 1);
        $session->write('Carrinho.'.$ProdutoCombo3->id, 1);

        $session->write('DescontoCombo', 1);

        return $this->redirect('/'.$academia->slug.'/mochila/fechar-o-ziper');
        $this->render(false);
    }
    
    public function view_combo($number, $slug = null){

        $session  = $this->request->session();

        $academia = $session->read('Academia');

        if(!$academia) {
            $url_atual = explode('/produto', $_SERVER['REQUEST_URI']);
            return $this->redirect('/comparador/produto'.$url_atual[1]);
        }

        if(empty($this->request->params['slug'])) {
            $url_atual = explode('/produto', $_SERVER['REQUEST_URI']);
            return $this->redirect('/'.$academia->slug.'/produto'.$url_atual[1]);
        }

        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');

        if($slug == null){
            if($academia) {
                return $this->redirect('/'.$academia->slug);
            } else {
                return $this->redirect('/');
            }
        }

        if(isset($_GET['ind_id']) && $_GET['ind_id'] > 0) {
            $session->write('Indicacao', $_GET['ind_id']);
        }

        $produto = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                    'ProdutoSubstancias',
                    'ProdutoSubstancias.Substancias',
                ]
            ])
            ->where(['Produtos.visivel' => 1])
            ->andWhere(['Produtos.preco > ' => 0.1])
            ->andWhere(['Produtos.slug' => $slug])
            ->first();

        if(count($produto) <= 0) {
            if($academia) {
                return $this->redirect('/'.$academia->slug);
            } else {
                return $this->redirect('/');
            }
        }

        $produtoView = $session->read('produtoView-'.$produto->id);

        if(!$produtoView) {
            if(!strpos($_SERVER['HTTP_USER_AGENT'],"bot")) {
                $produto = $this->Produtos->patchEntity($produto, ['views' => $produto->views+1]);
                if($this->Produtos->save($produto)) {
                    $session->write('produtoView-'.$produto->id, 1);
                }
            }
        }

        $produtos = $this->Produtos
            ->find('all', [
                'contain' => [
                    'Status',
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                ]
            ])
            ->where(['Produtos.visivel'            => 1])
            ->andWhere(['Produtos.preco > '        => 0.1])
            ->andWhere(['Produtos.produto_base_id' => $produto->produto_base_id])
            ->andWhere(['Produtos.id <>'           => $produto->id])
            ->all();

        $produto_tamanhos = $this->Produtos
            ->find('all', [
                'contain' => [
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.Propriedades',
                    'PedidoItens',
                ]
            ])
            ->where(['Produtos.visivel'           => 1])
            ->andWhere(['Produtos.preco > '         => 0.1])
            ->andWhere(['Produtos.produto_base_id'  => $produto->produto_base_id])
            ->andWhere(['Produtos.id <>'            => $produto->id])
            ->distinct(['Produtos.tamanho', 'Produtos.embalagem'])
            ->all();

        $combina_com = unserialize($produto->produto_base->combina);

        if($combina_com != null){

            //separar cada objetivo/categoria/subcategoria do combina_com

            $produto_combinations = $this->Produtos
                ->find('all')
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Produtos.preco > '     => 0.1])
                ->andWhere(['Produtos.status_id' => 1])
                ->andWhere(['ProdutoBase.status_id' => 1])
                //procurar produtos que estão nos objetivos/categorias/subcategorias do combina_com
                ->distinct(['ProdutoBase.id'])
                ->order('rand()')
                ->limit(9)
                ->all();
        }else{

            $produto_objetivos = TableRegistry::get('ProdutoObjetivos')
                ->find('all')
                ->where(['produto_base_id' => $produto->produto_base_id])
                ->all();

            foreach ($produto_objetivos as $produto_objetivo) {
                $objetivos[] = $produto_objetivo->objetivo_id;
            }

            if(count($objetivos) <= 0) {
                $objetivos = [0];
            }

            $produto_categorias = TableRegistry::get('ProdutoCategorias')
                ->find('all')
                ->where(['produto_base_id' => $produto->produto_base_id])
                ->all();

            foreach ($produto_categorias as $produto_categoria) {
                $categoria_produtos = $this->Produtos
                    ->find('all')
                    ->contain([
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->andWhere(['ProdutoCategorias.categoria_id IN' => $produto_categoria->categoria_id])
                    ->all();

                foreach ($categoria_produtos as $categoria_produto) {
                    $produtos_nao_exibir_ids[] = $categoria_produto->id;
                }
            }

            if(count($produtos_nao_exibir_ids) <= 0) {
                $produtos_nao_exibir_ids = [0];
            }

            $produto_subcategorias = TableRegistry::get('ProdutoSubcategorias')
                ->find('all')
                ->where(['produto_base_id' => $produto->produto_base_id])
                ->all();

            foreach ($produto_subcategorias as $produto_subcategoria) {
                $subcategoria_produtos = $this->Produtos
                    ->find('all')
                    ->contain([
                        'ProdutoBase',
                        'ProdutoBase.Marcas',
                        'ProdutoBase.ProdutoObjetivos',
                        'ProdutoBase.ProdutoObjetivos.Objetivos',
                        'ProdutoBase.ProdutoCategorias',
                        'ProdutoBase.ProdutoSubcategorias'
                    ])
                    ->innerJoin(
                        ['ProdutoBase' => 'produto_base'],
                        ['ProdutoBase.id = Produtos.produto_base_id']
                    )
                    ->leftJoin(
                        ['ProdutoObjetivos' => 'produto_objetivos'],
                        ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoCategorias' => 'produto_categorias'],
                        ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->leftJoin(
                        ['ProdutoSubcategorias' => 'produto_subcategorias'],
                        ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                    )
                    ->where(['Produtos.visivel' => 1])
                    ->andWhere(['Produtos.status_id' => 1])
                    ->andWhere(['ProdutoBase.status_id' => 1])
                    ->andWhere(['ProdutoSubcategorias.subcategoria_id IN' => $produto_subcategoria->subcategoria_id])
                    ->all();

                foreach ($subcategoria_produtos as $subcategoria_produto) {
                    $produtos_nao_exibir_ids[] = $subcategoria_produto->id;
                }
            }

            if(count($produtos_nao_exibir_ids) <= 0) {
                $produtos_nao_exibir_ids = [0];
            }

            $produto_combinations = $this->Produtos
                ->find('all')
                ->contain([
                    'ProdutoBase',
                    'ProdutoBase.Marcas',
                    'ProdutoBase.ProdutoObjetivos',
                    'ProdutoBase.ProdutoObjetivos.Objetivos',
                    'ProdutoBase.ProdutoCategorias',
                    'ProdutoBase.ProdutoSubcategorias'
                ])
                ->innerJoin(
                    ['ProdutoBase' => 'produto_base'],
                    ['ProdutoBase.id = Produtos.produto_base_id']
                )
                ->leftJoin(
                    ['ProdutoObjetivos' => 'produto_objetivos'],
                    ['ProdutoObjetivos.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoCategorias' => 'produto_categorias'],
                    ['ProdutoCategorias.produto_base_id = ProdutoBase.id']
                )
                ->leftJoin(
                    ['ProdutoSubcategorias' => 'produto_subcategorias'],
                    ['ProdutoSubcategorias.produto_base_id = ProdutoBase.id']
                )
                ->where(['Produtos.visivel' => 1])
                ->andWhere(['Produtos.status_id' => 1])
                ->andWhere(['Produtos.id NOT IN' => $produtos_nao_exibir_ids])
                ->andWhere(['ProdutoBase.status_id' => 1])
                ->andWhere(['ProdutoObjetivos.objetivo_id IN' => $objetivos])
                ->distinct(['ProdutoBase.id'])
                ->order('rand()')
                ->limit(9)
                ->all();
        }

        $session    = $this->request->session();

        $session_produto = $session->read('Carrinho');

        $session_cliente = $session->read('Cliente');

        $session_produto_ids = [];
        if(!empty($session_produto)) {
            foreach ($session_produto as $key => $item) {
                $session_produto_ids[] = $key;
            }
        }

        if(!empty($session_produto_ids)) {
            $s_produtos = $this->Produtos
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->where(['Produtos.id IN' => $session_produto_ids])
                ->all();
        }else{
            $s_produtos = [];
        }

        $prod_objetivos = $produtos;

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();

        $cupom_desconto = $session->read('CupomDesconto');

        $links_breadcrumb[] = [
            'url' => '',
            'name' => $academia->shortname.' - loja'
        ];
        $links_breadcrumb[] = [
            'url' => '/pesquisa',
            'name' => 'Produtos'
        ];
        $links_breadcrumb[] = [
            'url' => '/pesquisa/0/'.$produto->produto_base->marca->slug,
            'name' => $produto->produto_base->marca->name
        ];
        $links_breadcrumb[] = [
            'url' => '/produto/'.$produto->slug,
            'name' => $produto->produto_base->name
        ];

        $this->set(compact('academia', 'cupom_desconto', 'prod_objetivos', 's_produtos', 'session_produto', 'session_cliente', 'produto', 'produtos', 'produto_combinations', 'produto_tamanhos', 'links_breadcrumb', 'number'));
        $this->set('_serialize', ['produto']);
        $this->viewBuilder()->layout('default-combo');
    }
}
