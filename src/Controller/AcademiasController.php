<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * Academias Controller
 *
 * @property \App\Model\Table\AcademiasTable $Academias
 *
 */
class AcademiasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index(){

        $states         = $this->Academias->Cities->States->find('list');
        $esportes_list  = TableRegistry::get('Esportes')->find('list');

        $disable_modal_login = true;

        $this->set(compact('states', 'esportes_list', 'disable_modal_login'));

        $academia = $this->Academias->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {

            $academias = $this->Academias
                ->find('all')
                ->where(['cnpj' => $this->request->data['cnpj']])
                ->count();

            if($academias >= 1){
                $this->Flash->success('Já existe uma academia cadastrada com o CNPJ informado...');
            }else {
                $this->request->data['status_id'] = 2;
                $academia = $this->Academias->patchEntity($academia, $this->request->data);
                if ($ACADEMIA = $this->Academias->save($academia)) {

                    $AcademiaEsportes   = TableRegistry::get('AcademiaEsportes');

                    if(isset($this->request->data['esportes'][0])) {
                        foreach($this->request->data['esportes'] as $esporte){
                            $EsporteSave = $AcademiaEsportes->newEntity();
                            $EsporteSave = $AcademiaEsportes->patchEntity($EsporteSave, [
                                'academia_id'   => $ACADEMIA->id,
                                'esporte_id'    => $esporte,
                            ]);

                            $AcademiaEsportes->save($EsporteSave);
                        }
                    }

                    $this->Flash->success('Seu pré-cadastro foi realizado com sucesso!<br>Aguarde a validação dos dados, em breve entraremos em contato');
                    $this->redirect('/academias');
                } else {
                    $this->Flash->success('Falha ao cadastrar academia... Tente novamente');
                }
            }

        }

    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $academia = $this->Academias->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {

            $academias = $this->Academias
                ->find('all')
                ->where(['cnpj' => $this->request->data['cnpj']])
                ->count();

            $academias_email = $this->Academias
                ->find('all')
                ->where(['email' => $this->request->data['email']])
                ->count();

            $academias_slug = $this->Academias
                ->find('all')
                ->where(['slug' => $this->request->data['slug']])
                ->count();

            if($academias >= 1 || $academias_email >=1 || $academias_slug >= 1){
                echo 'Já existe uma academia cadastrada com o CNPJ e/ou Email e/ou URL Personalizada informado(s)...';
            }else {
                $this->request->data['status_id'] = 2;

                $this->request->data['mobile'] ? : $this->request->data['mobile'] = $this->request->data['phone'];

                if($this->request->data['password'] == $this->request->data['password_confirm']) {
                    $this->request->data['password_confirm'] = 1;
                    $confirmado = 1;
                } else {
                    $confirmado = 0;
                }
                $academia = $this->Academias->patchEntity($academia, $this->request->data);
                //debug($academia);
                if($this->request->data['valid_name'] == null) {
                    if($confirmado == 1) {
                        if ($this->Academias->save($academia)) {
                            echo 'Academia cadastrada com sucesso! <br />'
                                .'Em breve entraremos em contato...'   ;
                        } else {
                            echo 'Falha ao cadastrar academia... Tente novamente';
                        }
                    } else {
                        echo 'Senhas não correspondem';
                    }
                }
            }

            debug($this->request->data);
        }
    }

    public function ajax_options($state_id = 0, $city_id = 0){

        if($city_id > 0) {
            $this->set('academias', $this->Academias
                ->find('list')
                ->where(['Academias.city_id' => $city_id])
                ->andWhere(['Academias.status_id' => 1])
                ->order(['Academias.shortname' => 'asc'])
                ->all()
                ->toArray()
            );
        }elseif($state_id > 0 && $city_id <= 0){
            $this->set('academias', $this->Academias
                ->find('list', ['contain' => ['Cities']])
                //->where(['Academias.city_id' => $city_id])
                ->where(['Cities.state_id' => $state_id])
                ->andWhere(['Academias.status_id' => 1])
                ->order(['Academias.shortname' => 'asc'])
                ->all()
                ->toArray()
            );
        }else{
            $this->set('academias', $this->Academias
                ->find('list')
                ->where(['Academias.status_id' => 1])
                ->order(['Academias.shortname' => 'asc'])
                ->all()
                ->toArray()
            );
        }
    }

    public function view_ajax($id = null){

        if($id != null){
            $academia = $this->Academias
                ->find('all')
                ->contain(['Cities', 'Cities.States'])
                ->where(['Academias.id' => $id])
                ->limit(1)
                ->first();

            $this->set('academia', $academia);
            $this->set('_serialize', ['academia']);

            $this->viewBuilder()->layout('ajax');
        }

    }

    public function busca_professor($id = null){

        if($id != null){
            $ProfessorAcademia = TableRegistry::get('ProfessorAcademias');

            $professores = $ProfessorAcademia
                ->find('all')
                ->contain(['Professores'])
                ->where(['ProfessorAcademias.academia_id' => $id])
                ->andWhere(['Professores.status_id' => 1])
                ->andWhere(['ProfessorAcademias.status_id' => 1])
                ->all();

            $this->set('professores', $professores);
        }

    }

    public function verifica_professor_cpf($cpf = null){

        if($cpf != null){
            $Professores = TableRegistry::get('Professores');

            $professor = $Professores
                ->find('all')
                ->where(['Professores.cpf' => $cpf])
                ->first();

            if($professor) {
                echo json_encode($professor);
            } else {
                echo 2;
            }

            $this->viewBuilder()->layout('ajax');
            $this->render(false);
        }

    }

    public function verifica_professor_email(){

        if($_GET['email']){
            $Professores = TableRegistry::get('Professores');

            $professor = $Professores
                ->find('all')
                ->where(['Professores.email' => $_GET['email']])
                ->first();

            if($professor) {
                echo json_encode($professor);
            } else {
                echo 2;
            }

            $this->viewBuilder()->layout('ajax');
            $this->render(false);
        }

    }

    public function maps_cordinates_add($academia_id = null, $latitude = null, $longitude = null){

        if($academia_id != null && $latitude != null && $longitude != null){
            $academia = $this->Academias
                ->find('all')
                ->contain(['Cities', 'Cities.States'])
                ->where(['Academias.id' => $academia_id])
                ->limit(1)
                ->first();

            if(count($academia) >= 1){
                $academia->latitude     = $latitude;
                $academia->longitude    = $longitude;

                if($this->Academias->save($academia)){
                    echo 'OK';
                }
            }

            $this->viewBuilder()->layout('ajax');
        }

    }



    /*******************************************************************************************************************
     * ADMIN ACADEMIA
     ******************************************************************************************************************/

    public function beforeRender() {
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        $AcademiaConta = $session->read('AcademiaConta');

        $AcademiaProfessores        = TableRegistry::get('ProfessorAcademias');
        $AcademiasContas            = TableRegistry::get('AcademiasContas');
        $AcademiaClientes           = TableRegistry::get('Clientes');
        $AcademiaPedidos            = TableRegistry::get('Pedidos');

        $professores_navbar = $AcademiaProfessores
            ->find('all')
            ->contain(['Professores'])
            ->where(['ProfessorAcademias.academia_id' => $Academia->id])
            ->andWhere(['ProfessorAcademias.status_id' => 1])
            ->all();

        $clientes_navbar = $AcademiaClientes
            ->find('all')
            ->where(['Clientes.academia_id' => $Academia->id])
            ->andWhere(['Clientes.status_id' => 1])
            ->order(['Clientes.created' => 'desc'])
            ->all();

        $pedidos_navbar = $AcademiaPedidos
            ->find('all')
            ->contain(['Clientes'])
            ->where(['Pedidos.academia_id' => $Academia->id])
            ->andWhere(['Pedidos.pedido_status_id <>' => 8])
            ->andWhere(['Pedidos.comissao_status IN' => [1, 2]])
            ->order(['Pedidos.modified' => 'desc'])
            ->all();

        $academias_contas = $AcademiasContas
            ->find('all')
            ->where(['email' => $AcademiaConta->email])
            ->orWhere(['cpf' => $AcademiaConta->cpf])
            ->all();

        $academias_contas_ids = [];
        foreach ($academias_contas as $academia_conta) {
            $academias_contas_ids[] = $academia_conta->academia_id;
        }

        if(count($academias_contas_ids) >= 1) {
            $conta_academias = $this->Academias
                ->find('all')
                ->where(['id IN' => $academias_contas_ids])
                ->all();
        }

        $this->set(compact(
            'professores_navbar',
            'clientes_navbar',
            'pedidos_navbar',
            'Academia',
            'conta_academias'
        ));
    }

    public function admin_mudar_academia($academia_id){
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        $AcademiaConta = $session->read('AcademiaConta');

        if(!$Academia) {
            return $this->redirect('/academia/acesso');
        }

        $AcademiasContas = TableRegistry::get('AcademiasContas');

        $academia_conta = $AcademiasContas
            ->find('all')
            ->where(['email' => $AcademiaConta->email])
            ->andWhere(['academia_id' => $academia_id])
            ->orWhere(['cpf' => $AcademiaConta->cpf])
            ->andWhere(['academia_id' => $academia_id])
            ->first();

        $academia = $this->Academias
            ->find('all')
            ->where(['id IN' => $academia_conta->academia_id])
            ->first();

        $session->write('AdminAcademia', $academia);

        $this->redirect(['action' => 'admin_index']);

        $this->viewBuilder()->layout(false);
        $this->render(false);
    }

    public function login($redirect = null, $pedido_id = null){

        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if($Academia){
            if($redirect) {
                if($redirect == 'express') {
                    $this->redirect('/academia/admin/express/'.$pedido_id);
                }
            } else {
                $this->redirect(['action' => 'admin_index']);
            }
        }

        if($this->request->is(['post', 'patch', 'put'])){
            $academia_nao_aceitou = $this->Academias
                ->find('all')
                ->where(['Academias.email' => $this->request->data['email']])
                ->andWhere(['Academias.status_id' => 3])
                ->first();

            if($academia_nao_aceitou) {
                $this->Flash->auth(__('Academia inativada, entre em contato para saber mais...'));
            } else {
                $this->request->data['email'] ? $email = $this->request->data['email'] : $email = '';

                if(strpos($email, '@')) {
                    $academia = $this->Academias
                        ->find('all')
                        ->contain(['Cities'])
                        ->where(['Academias.email' => $email])
                        ->andWhere(['Academias.status_id'   => 1])
                        ->first();
                } else if(strpos($email, '.')) {
                    $academia = $this->Academias
                        ->find('all')
                        ->contain(['Cities'])
                        ->where(['Academias.cnpj' => $email])
                        ->andWhere(['Academias.status_id'   => 1])
                        ->first();
                } else {
                    $parte_um     = substr($email, 0, 2);
                    $parte_dois   = substr($email, 2, 3);
                    $parte_tres   = substr($email, 5, 3);
                    $parte_quatro = substr($email, 8, 4);
                    $parte_cinco  = substr($email, 12, 2);

                    $cpnj = $parte_um.'.'.$parte_dois.'.'.$parte_tres.'/'.$parte_quatro.'-'.$parte_cinco;

                    $academia = $this->Academias
                        ->find('all')
                        ->contain(['Cities'])
                        ->where(['Academias.cnpj' => $cpnj])
                        ->andWhere(['Academias.status_id'   => 1])
                        ->first();
                }

                $hasher = new DefaultPasswordHasher();
                $verify = new DefaultPasswordHasher;
                if($verify->check($this->request->data['password'], $academia->password)){
                    $session    = $this->request->session();
                    $session->write('AdminAcademia', $academia);

                    if($redirect) {
                        if($redirect == 'express') {
                            $this->redirect('/academia/admin/express/'.$pedido_id);
                        }
                    } else {
                        $this->redirect(['action' => 'admin_index']);
                    }
                }else{
                    $this->Flash->auth(__('Email/CNJP ou senha inválidos. Tente novamente...'));
                }
            }
        }

    }

    /**
     * LOGOUT
     */
    public function logout(){
        $session    = $this->request->session();
        $session->destroy('AdminAcademia');
        $session->destroy('DonoAcademia');
        $session->destroy('AcademiaConta');

        $this->redirect(['action' => 'login']);
    }

    /**
     * ALTERAR SENHA
     */
    public function passwd(){
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $academia = $this->Academias->get($Academia->id);

        if($academia){
            $this->set('academia', $academia);
            $this->set('_serialize', ['academia']);

            if($this->request->is(['post', 'put', 'patch'])){
                $hasher = new DefaultPasswordHasher();
                $verify = new DefaultPasswordHasher;
                if($verify->check($this->request->data['old_password'], $academia->password)){
                    if($verify->check($this->request->data['confirm_new_password'],
                        $hasher->hash($this->request->data['password']))){
                        $academia->password = $this->request->data['password'];
                        if($this->Academias->save($academia)){
                            $this->Flash->success('Senha alterada com sucesso!');
                            return $this->redirect(['action' => 'admin_index']);
                        }else{
                            $this->Flash->error('Falha ao alterar a senha :(');
                        }
                    }
                }
            }

        }else{
            $this->redirect('/academia/acesso');
        }

        $this->viewBuilder()->layout('admin_academia');
    }
    
    public function admin_index(){

        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect(['action' => 'login']);
        }


        $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');
        $AcademiaComissoesPedidos   = TableRegistry::get('AcademiaComissoesPedidos');
        $MensalidadesCategorias     = TableRegistry::get('MensalidadesCategorias');
        $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
        $AcademiaProfessores        = TableRegistry::get('ProfessorAcademias');
        $AcademiaComissoes          = TableRegistry::get('AcademiaComissoes');
        $AcademiaClientes           = TableRegistry::get('Clientes');
        $ProdutoObjetivos           = TableRegistry::get('ProdutoObjetivos');
        $AcademiaPedidos            = TableRegistry::get('Pedidos');
        $AcademiaBanners            = TableRegistry::get('AcademiaBanners');
        $AcademiaSaques             = TableRegistry::get('AcademiaSaques');
        $PlanosClientes             = TableRegistry::get('PlanosClientes');
        $Professores                = TableRegistry::get('Professores');
        $Produtos                   = TableRegistry::get('Produtos');
        $Pedidos                    = TableRegistry::get('Pedidos');
        $max_date = new \DateTime(date('Y-m').'-01');

        $academia_comissao_pedidos = $AcademiaComissoesPedidos
            ->find('list', [
                'keyField'      => 'pedido_id',
                'valueField'    => 'pedido_id'
            ])
            ->toArray();

        if(empty($academia_comissao_pedidos)){
            $academia_comissao_pedidos = [0];
        }

        $academia_comissoes_pagas       = $AcademiaComissoes
            ->find('all')
            ->contain(['Academias'])
            ->where(['AcademiaComissoes.paga' => 1])
            ->andWhere(['AcademiaComissoes.academia_id' => $Academia->id])
            ->all();

        $data_atual = Time::now();
        $mes_atual = $data_atual->format('m');
        $ano_atual = $data_atual->format('Y');

        $academia_comissoes_pendentes   = $AcademiaPedidos
            ->find('all')
            ->where(['Pedidos.academia_id'              => $Academia->id])
            ->andWhere(['Pedidos.data_pagamento >='   => $ano_atual.'-'.$mes_atual.'-01%'])
            ->andWhere(['Pedidos.pedido_status_id >='   => 3])
            ->andWhere(['Pedidos.pedido_status_id NOT IN'   => [7,8]])
            ->andWhere(['Pedidos.comissao_status IN'   => 1])
            ->andWhere(['Pedidos.id NOT IN'             => $academia_comissao_pedidos])
            ->all();

        $academia_comissoes_receber   = $AcademiaComissoes
            ->find('all')
            ->contain(['Academias'])
            ->where(['AcademiaComissoes.paga' => 0])
            ->andWhere(['AcademiaComissoes.academia_id' => $Academia->id])
            ->all();

        $academia_comissoes_button   = $AcademiaPedidos
            ->find('all')
            ->where(['Pedidos.academia_id'              => $Academia->id])
            ->andWhere(['Pedidos.created <'               => $max_date])
            ->andWhere(['Pedidos.pedido_status_id >='     => 3])
            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
            ->andWhere(['Pedidos.comissao_status'   => 1])
            ->andWhere(['Pedidos.id NOT IN'             => $academia_comissao_pedidos])
            ->all();

        $academia_professores = $AcademiaProfessores
            ->find('all')
            ->contain(['Professores'])
            ->where(['ProfessorAcademias.academia_id' => $Academia->id])
            ->andWhere(['ProfessorAcademias.status_id' => 1])
            ->all();

        $academia_clientes = $AcademiaClientes
            ->find('all')
            ->where(['Clientes.academia_id' => $Academia->id])
            ->andWhere(['Clientes.status_id' => 1])
            ->order(['Clientes.created' => 'desc'])
            ->all();

        $academia_pedidos = $AcademiaPedidos
            ->find('all')
            ->contain(['Clientes'])
            ->where(['Pedidos.academia_id' => $Academia->id])
            ->andWhere(['Pedidos.pedido_status_id <>' => 8])
            ->order(['Pedidos.modified' => 'desc'])
            ->all();

        $academia = $this->Academias
            ->find('all')
            ->where(['Academias.id' => $Academia->id])
            ->limit(1)
            ->first();

        $produtos_promocoes = $Produtos
            ->find('all', ['contain' => [
                'ProdutoBase'
            ]])
            ->where(['Produtos.status_id'          => 1])
            ->andWhere(['Produtos.visivel'         => 1])
            ->andWhere(['ProdutoBase.status_id'    => 1])
            ->andWhere(['Produtos.preco_promo >'   => 0])
            ->distinct(['ProdutoBase.id'])
            ->order(['rand()'])
            ->limit(5)
            ->all();

        if(!count($produtos_promocoes) == 0) {
            foreach ($produtos_promocoes as $pp) {
                $produtos_promocoes_id[] = $pp->produto_base_id;
            }

            $produtos_promocoes_objetivos = $ProdutoObjetivos
            ->find('all')
            ->contain(['Objetivos'])
            ->where(['ProdutoObjetivos.produto_base_id IN' => $produtos_promocoes_id])
            ->all();
        }

        foreach ($academia_professores as $ap) {
            $professores_academia_id[] = $ap->professor_id;
        }

        if(empty($professores_academia_id)) {
            $professores_academia_id = [0];
        }

        $prof_relatorio_name = $Professores
            ->find('all')
            ->where(['Professores.id IN' => $professores_academia_id])
            ->andWhere(['Professores.status_id' => 1])
            ->all();

        $min_date = new \DateTime(date('Y-m').'-01');

        $prof_academia_relatorio = $AcademiaPedidos
            ->find('all')
            ->contain(['Professores'])
            ->where(['Pedidos.academia_id' => $Academia->id])
            ->andWhere(['Pedidos.professor_id IN' => $professores_academia_id])
            ->andWhere(['Pedidos.created >' => $min_date])
            ->andWhere(['Pedidos.pedido_status_id >='     => 3])
            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
            ->andWhere(['Pedidos.comissao_status'   => 1])
            ->andWhere(['Professores.status_id' => 1])
            ->order(['Pedidos.created' => 'desc'])
            ->all();

        $time = Time::now();

        $min_date = Time::now();
        $min_date->subYear(1);

        $pedidos_grafico = $AcademiaPedidos
            ->find('all')
            ->where(['Pedidos.academia_id' => $Academia->id])
            ->andWhere(['Pedidos.created >' => $min_date])
            ->andWhere(['Pedidos.pedido_status_id >=' => 3])
            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
            ->andWhere(['Pedidos.comissao_status' => 1])
            ->all();

        $data_grafico_vendas[0] = 0.0;
        $data_grafico_vendas[1] = 0.0;
        $data_grafico_vendas[2] = 0.0;
        $data_grafico_vendas[3] = 0.0;
        $data_grafico_vendas[4] = 0.0;
        $data_grafico_vendas[5] = 0.0;
        $data_grafico_vendas[6] = 0.0;
        $data_grafico_vendas[7] = 0.0;
        $data_grafico_vendas[8] = 0.0;
        $data_grafico_vendas[9] = 0.0;
        $data_grafico_vendas[10] = 0.0;
        $data_grafico_vendas[11] = 0.0;

        $ctrl = 0;
        for($i = 12; $i >= 1; $i--) {
            $data_grafico = Time::now();
            $data_grafico->subMonth($i);

            $data_grafico_mes[$ctrl] = $data_grafico->format('m-y');

            $ctrl++;
        }

        foreach ($pedidos_grafico as $key => $pedido_grafico) {
            for ($i=0; $i < 12; $i++) { 
                if($pedido_grafico->created->format('m-y') == $data_grafico_mes[$i]) {
                    $data_grafico_vendas[$i] += $pedido_grafico->valor;
                }
            }
        }

        if ($this->request->is(['post', 'put'])) {

            $session->write('AdminAcademia', $academia);

            $termos = $this->request->data['aceite_termos'];

            $academia = $this->Academias->patchEntity($academia, $this->request->data);

            if ($acad_salvo = $this->Academias->save($academia)) {
                if($termos == 1) {
                    $this->Flash->success('Dados atualizados com sucesso!');
                    $this->redirect(['action' => 'admin_index']);
                } else {
                    $status_id = ['status_id' => 3];
                    $acad_salvo = $this->Academias->patchEntity($acad_salvo, $status_id);

                    if($this->Academias->save($acad_salvo)) {
                        $session->destroy('AdminAcademia');
                        $this->Flash->error('Conta inativada! Para saber mais entre em contato..');
                        $this->redirect('/academia/acesso');
                    }
                }
            }
        }

        $this->set(compact(
            'produtos_mais_vendidos_objetivos',
            'academia_comissoes_pendentes',
            'produtos_promocoes_objetivos',
            'academia_comissoes_receber',
            'academia_comissoes_button',
            'academia_comissoes_pagas',
            'prof_academia_relatorio',
            'professores_academia_id',
            'produtos_mais_vendidos',
            'academia_professores',
            'prof_relatorio_name',
            'data_grafico_vendas',
            'produtos_promocoes',
            'academia_clientes',
            'academia_banners',
            'academia_pedidos',
            'valor_total',
            'time'
        ));

        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_dados_bancarios($comissao = 0){
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect(['action' => 'login']);
        }

        $academia = $this->Academias
            ->find('all')
            ->where(['Academias.id' => $Academia->id])
            ->limit(1)
            ->first();

        if($comissao == 1) {
            $this->Flash->info('Preencha seus dados bancários para fechar suas comissões!');
        }

        if($this->request->is(['post', 'put'])){

            $academia = $this->Academias->patchEntity($academia, $this->request->data);

            if($this->Academias->save($academia)){
                $this->Flash->success('Dados bancários atualizados com sucesso!');
                $this->redirect(['action' => 'admin_dados_bancarios']);
            }else{
                $this->Flash->error('Falha ao atualizar dados bancários. Tente novamente...');
            }
        }

        $this->set(compact('academia', 'comissao'));

        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_dados(){
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect(['action' => 'login']);
        }

        $AcademiaEsportes = TableRegistry::get('AcademiaEsportes');

        $academia = $this->Academias
            ->find('all')
            ->contain(['Cities', 'Cities.States'])
            ->where(['Academias.id' => $Academia->id])
            ->limit(1)
            ->first();

        if($this->request->is(['post', 'put'])){

            if($this->request->data['imagem']['size'] == 0 && $this->request->data['imagem']['error'] == 1) {
                $this->Flash->error('Por favor, coloque uma imagem de até 2mb');
                $this->redirect(['action' => 'admin_dados']);
            } else {
                $academia = $this->Academias->patchEntity($academia, $this->request->data);

                if($academia_saved = $this->Academias->save($academia)){

                    $session->write('AdminAcademia', $academia_saved);

                    $this->Flash->success('Dados atualizados com sucesso!');
                    $this->redirect(['action' => 'admin_dados']);
                }else{
                    $this->Flash->error('Falha ao atualizar dados. Tente novamente...');
                }
            }

        }

        $cities = $this->Academias->Cities->find('list')->where(['Cities.state_id' => $academia->city->state_id]);
        $states = $this->Academias->Cities->States->find('list');

        $this->set(compact('academia', 'cities', 'states'));

        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_alunos(){
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $Alunos     = TableRegistry::get('Clientes');

        $this->paginate = [
            'order'         => ['Clientes.name' => 'asc'],
            'conditions'    => ['Clientes.status_id' => 1, 'Clientes.academia_id' => $Academia->id]
        ];

        $alunos = $this->paginate($Alunos);

        $this->set(compact('alunos'));

        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_aluno_detalhes($id = null){

        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $Alunos = TableRegistry::get('Clientes');

        $cliente = $Alunos->get($id, [
            'contain' => ['Cities', 'Academias', 'Status', 'Pedidos']
        ]);

        if($cliente->academia_id != $Academia->id) {
            $this->Flash->error('Falha ao ver dados do aluno, pois ele está relacionado a outra academia...');
            $this->redirect('/academia/admin/alunos');
        }

        $this->set('cliente', $cliente);
        $this->set('_serialize', ['cliente']);

        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_professores(){

        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $Prof_Academia = TableRegistry::get('ProfessorAcademias');

        $Academias = TableRegistry::get('Academias');

        $academias = $Academias
            ->find('list')
            ->all();

        $prof_academia = $Prof_Academia
            ->find('all')
            ->contain(['Academias', 'Professores','Academias.Cities', 'Professores.Cities'])
            ->where(['ProfessorAcademias.academia_id' => $Academia->id])
            ->andWhere(['ProfessorAcademias.status_id IN' => [1,3]])
            ->all();


        /**
         * MENSAGEM DE ALERTA PARA PROFESSORES AGUARDANDO ACEITE
         */
        $prof_alert = $Prof_Academia
            ->find('all')
            ->contain(['Academias', 'Professores','Academias.Cities', 'Professores.Cities'])
            ->where(['ProfessorAcademias.academia_id' => $Academia->id])
            ->andWhere(['ProfessorAcademias.status_id' => 3])
            ->all();

        if(count($prof_alert) >= 1){
            $this->Flash->info_laranja('Existe professor aguardando o seu aceite.');
        }

        $this->paginate = [
            'order'         => ['Professores.name' => 'asc'],
            'conditions'    => ['ProfessorAcademias.academia_id' => $Academia->id]
        ];

        $this->paginate($Prof_Academia);

        $this->set(compact('academias','prof_academia', 'professores','states', 'cities'));

        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_pedidos(){
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $Pedidos    = TableRegistry::get('Pedidos');

        $this->paginate = [
            'contain' => [
                'Clientes',
                'PedidoItens.Produtos',
                'PedidoItens.Produtos.ProdutoBase',
                'PedidoStatus',
                'PedidoItens',
                'Professores',
            ],
            'leftJoin' => ['Professores' => 'professores', 'Professores.id = Pedido.professor_id'],
            'conditions'    => ['Pedidos.academia_id' => $Academia->id, 'Pedidos.pedido_status_id !=' => 8, 'Pedidos.comissao_status IN' => [1,2]],
            'order'         => ['Pedidos.id' => 'desc']
        ];
        $pedidos = $this->paginate($Pedidos);

        $this->set(compact('pedidos'));
        $this->set('_serialize', ['pedidos']);

        $this->viewBuilder()->layout('admin_academia');
    }

    public function excluir_pedido($id) {

        $Pedidos = TableRegistry::get('Pedidos');

        $pedido = $Pedidos->get($id);

        $pedido = $Pedidos->patchEntity($pedido, ['pedido_status_id' => 8]);
        if($Pedidos->save($pedido)) {
            $this->Flash->success('Pedido excluído com sucesso!');
        } else {
            $this->Flash->error('Falha ao excluir pedido, tente novamente');
        }

        return $this->redirect('/academia/admin/pedidos');

        $this->viewBuilder()->layout(false);
        $this->render(false);
    }

    public function admin_pedido_view($id = null){

        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $Pedidos    = TableRegistry::get('Pedidos');

        $pedido = $Pedidos
            ->find('all')
            ->contain([
                'Clientes', 'Academias', 'Transportadoras', 'PedidoStatus', 'PedidoItens',
                'PedidoItens.Produtos',
                'PedidoItens.Produtos.ProdutoBase', 'PedidoPagamentos'
            ])
            ->where(['Pedidos.id' => $id])
            ->andWhere(['Pedidos.academia_id' => $Academia->id])
            ->first();

        if ($pedido != null) {
            $this->set('pedido', $pedido);
            $this->set('_serialize', ['pedido']);

            $this->viewBuilder()->layout('admin_academia');
        } else {
            $this->Flash->warning('Pedido #' . $id . ' inexiste ou não relacionado a esta academia.');
            $this->redirect('/academia/admin/pedidos');
        }
    }

    /**
     * ADMIN ACADEMIA - COMISSÕES
     */
    public function admin_comissoes_index(){

        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $academia = $this->Academias
            ->find('all')
            ->contain(['Cities', 'Cities.States'])
            ->where(['Academias.id' => $Academia->id])
            ->limit(1)
            ->first();

        $AcademiaComissoes = TableRegistry::get('AcademiaComissoes');

        $Pedidos           = TableRegistry::get('Pedidos');

        $AcademiaComissoesPedidos = TableRegistry::get('AcademiaComissoesPedidos');

        $academia_comissao_pedidos = $AcademiaComissoesPedidos
            ->find('list', [
                'keyField'      => 'pedido_id',
                'valueField'    => 'pedido_id'
            ])
            ->toArray();

        if(empty($academia_comissao_pedidos)){
            $academia_comissao_pedidos = [0];
        }

        $max_date = new \DateTime(date('Y-m').'-01');

        $pedidos = $Pedidos
            ->find('all')
            ->contain([
                'Clientes',
                'Academias',
                'Academias.Cities',
                'PedidoStatus',
            ])
            ->where(['Pedidos.academia_id'                => $Academia->id])
            ->andWhere(['Pedidos.created <'               => $max_date])
            ->andWhere(['Pedidos.comissao_status'         => 1])
            ->andWhere(['Pedidos.pedido_status_id >='     => 3])
            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
            ->andWhere(['Pedidos.comissao_status'   => 1])
            //->andWhere(['Pedidos.id NOT IN'               => $academia_comissao_pedidos])
            ->order(['Pedidos.id' => 'desc'])
            ->all();

        $acadComissoes = $AcademiaComissoes
            ->find('all')
            ->contain(['Academias'])
            ->where(['AcademiaComissoes.academia_id'         => $Academia->id])
            ->all();

        $this->paginate = [
            'contain' => ['Academias'],
            'conditions' => ['AcademiaComissoes.academia_id' => $Academia->id]
        ];
        $academiaComissoes = $this->paginate($AcademiaComissoes);

        $this->set(compact('academia', 'pedidos', 'acadComissoes', 'academiaComissoes'));
        $this->set('_serialize', ['academiaComissoes']);

        $this->viewBuilder()->layout('admin_academia');
    }

    /**
     * ADMIN ACADEMIA - ACEITE DE COMISSÕES
     * @param null $ano
     * @param null $mes
     */
    public function admin_comissao($comissao_id){

        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $academia = $this->Academias
            ->find('all')
            ->contain(['Cities', 'Cities.States'])
            ->where(['Academias.id' => $Academia->id])
            ->limit(1)
            ->first();

        $Pedidos    = TableRegistry::get('Pedidos');
        $AcademiaComissoes = TableRegistry::get('AcademiaComissoes');
        $AcademiaComissoesPedidos = TableRegistry::get('AcademiaComissoesPedidos');

        $comissao = $AcademiaComissoes
            ->find('all')
            ->where(['id' => $comissao_id])
            ->andWhere(['aceita' => 2])
            ->andWhere(['paga' => 0])
            ->first();

        if(!$comissao) {
            $this->redirect(['action' => 'admin_comissoes_index']);
        }

        $comissao_pedidos = $AcademiaComissoesPedidos
            ->find('all')
            ->where(['academia_comissao_id' => $comissao->id])
            ->all();

        foreach ($comissao_pedidos as $comissao_pedido) {
            $comissao_pedidos_id[] = $comissao_pedido->pedido_id;
        }

        $pedidos = $Pedidos
            ->find('all')
            ->contain([
                'Clientes',
                'Academias',
                'Academias.Cities',
                'PedidoStatus',
            ])
            ->where(['Pedidos.academia_id' => $Academia->id])
            ->andWhere(['Pedidos.id IN' => $comissao_pedidos_id])
            ->order(['Pedidos.id' => 'desc'])
            ->all();

        if($this->request->is(['post', 'put'])){

            if($this->request->data['card_name']) {

                $data = [
                    'card_name'   => $this->request->data['card_name'],
                    'card_cpf'    => $this->request->data['card_cpf'],
                    'card_birth'  => $this->request->data['card_birth'],
                    'card_mother' => $this->request->data['card_mother'],
                    'card_gender' => $this->request->data['card_gender']
                ];

                $academia_card = $this->Academias->patchEntity($Academia, $data);

                $this->Academias->save($academia_card);

                $session->write('AdminAcademia', $Academia);
            }

            if($academia->is_cpf == 0) {
                $nome = $academia->name;
                $cpf = $academia->cnpj;
            } else {
                $nome = $academia->favorecido;
                $cpf = $academia->cpf_favorecido;
            }

            $data = [
                'aceita' => $this->request->data['aceite'],
                'nome_titular' => $nome,
                'cpf_titular' => $cpf,
                'tipo_conta' => $academia->tipo_conta,
                'banco' => $academia->banco,
                'agencia' => $academia->agencia,
                'agencia_dv' => $academia->agencia_dv,
                'conta' => $academia->conta,
                'conta_dv' => $academia->conta_dv,
                'data_aceite' => Time::now()
            ];

            $comissao = $AcademiaComissoes->patchEntity($comissao, $data);

            if($academia_comissao = $AcademiaComissoes->save($comissao)){
                $this->Flash->success('Relatório enviado com sucesso!');
                $this->redirect(['action' => 'admin_comissoes_index']);
            }else{
                $this->Flash->error('Falha ao enviar relatório. Tente novamente...');
            }

        }



        $this->set(compact('academia', 'pedidos', 'ano', 'mes'));
        $this->set('_serialize', ['pedidos']);

        $this->viewBuilder()->layout('admin_academia');

    }

    //logfitness123
    //$2y$10$tTsAk3Z0eSwAaoUK5EFm1.JGJ5SXgOMlUFLl98nLX2thAoQYLEoNO

    /**
     * ADMIN ACADEMIA - BANNERS
     */
    public function admin_banners($id = null){

        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');

        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $this->paginate = [
            'contain'       => ['Status'],
            'conditions'    => [
                ['AcademiaBanners.academia_id' => $Academia->id],
                ['AcademiaBanners.status_id' => 1]
            ]
        ];

        $AcademiaBanners = TableRegistry::get('AcademiaBanners');

        $banner = $AcademiaBanners
            ->find('all')
            ->where(['id' => $id])
            ->andWhere(['academia_id' => $Academia->id])
            ->first();

        if($banner != null) {
            $data = [
                'status_id' => 2
            ];
            
            $banner = $AcademiaBanners->patchEntity($banner, $data);
            if ($AcademiaBanners->save($banner)) {

                $this->Flash->success('Banner excluído com sucesso!');
                return $this->redirect('/academia/admin/banners');
            } else {
                $this->Flash->error('Falha ao excluir banner. Tente novamente...');
            }
        }

        $this->set('banners', $this->paginate(TableRegistry::get('AcademiaBanners')));
        $this->set('_serialize', ['banners']);

        $this->viewBuilder()->layout('admin_academia');

    }

    /**
     * ADMIN ACADEMIA - NOVO BANNER
     * @return \Cake\Network\Response|null
     */
    public function admin_banners_add(){

        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');

        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }
        $AcademiaBanners = TableRegistry::get('AcademiaBanners');

        $banner = $AcademiaBanners->newEntity();

        if ($this->request->is(['patch', 'post', 'put'])) {

            if($this->request->data['imagem']['size'] == 0 && $this->request->data['imagem']['error'] == 1) {
                $this->Flash->error('Por favor, coloque uma imagem de até 2mb');
                $this->redirect('/academia/admin/banners/novo');
            } else {
                $this->request->data['academia_id'] = $Academia->id;
                $this->request->data['status_id'] = 1;
                $banner = $AcademiaBanners->patchEntity($banner, $this->request->data);
                if ($AcademiaBanners->save($banner)) {

                    $this->Flash->success('Banner salvo com sucesso!');
                    return $this->redirect('/academia/admin/banners');
                } else {
                    $this->Flash->error('Falha ao salvar banner. Tente novamente...');
                }
            }
        }

        $status     = $AcademiaBanners->Status->find('list');

        $this->set(compact('banner', 'status'));
        $this->set('_serialize', ['banner']);

        $this->viewBuilder()->layout('admin_academia');

    }

    /**
     * EDITAR BANNER
     * @param null $id
     * @return \Cake\Network\Response|null
     */
    public function admin_banners_edit($id = null){

        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }
        $AcademiaBanners = TableRegistry::get('AcademiaBanners');

        $banner = $AcademiaBanners
            ->find('all')
            ->contain([])
            ->where(['AcademiaBanners.id' => $id])
            ->andWhere(['AcademiaBanners.academia_id' => $Academia->id])
            ->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $banner = $AcademiaBanners->patchEntity($banner, $this->request->data);
            if ($AcademiaBanners->save($banner)) {

                $this->Flash->success('Banner salvo com sucesso!');
                return $this->redirect('/academia/admin/banners/editar/'.$id);
            } else {
                $this->Flash->error('Falha ao salvar banner. Tente novamente...');
            }
        }

        $status     = $AcademiaBanners->Status->find('list');

        $this->set(compact('banner', 'status'));
        $this->set('_serialize', ['banner']);

        $this->viewBuilder()->layout('admin_academia');

    }

    /**
     * VISUALIZAR BANNER
     * @param null $id
     */
    public function admin_banners_view($id = null){

        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $banner = TableRegistry::get('AcademiaBanners')
            ->find('all')
            ->contain(['Status'])
            ->where(['AcademiaBanners.id' => $id])
            ->andWhere(['AcademiaBanners.academia_id' => $Academia->id])
            ->first();

        $this->set('banner', $banner);
        $this->set('_serialize', ['banner']);

        $this->viewBuilder()->layout('admin_academia');

    }

    /**
     * ESQUECI A SENHA
     * @param bool $solicitacao
     */
    public function forgot_passwd($solicitacao = false){

        if($this->request->is(['post', 'put'])){

            $this->request->data['email'] ? $email = $this->request->data['email'] : $email = '';

            if(strpos($email, '@')) {
                $academia = $this->Academias
                    ->find('all')
                    ->contain(['Cities'])
                    ->where(['Academias.email' => $email])
                    ->andWhere(['Academias.status_id'   => 1])
                    ->first();
            } else if(strpos($email, '.')) {
                $academia = $this->Academias
                    ->find('all')
                    ->contain(['Cities'])
                    ->where(['Academias.cnpj' => $email])
                    ->andWhere(['Academias.status_id'   => 1])
                    ->first();
            } else {
                $parte_um     = substr($email, 0, 2);
                $parte_dois   = substr($email, 2, 3);
                $parte_tres   = substr($email, 5, 3);
                $parte_quatro = substr($email, 8, 4);
                $parte_cinco  = substr($email, 12, 2);

                $cpnj = $parte_um.'.'.$parte_dois.'.'.$parte_tres.'/'.$parte_quatro.'-'.$parte_cinco;

                $academia = $this->Academias
                    ->find('all')
                    ->contain(['Cities'])
                    ->where(['Academias.cnpj' => $cpnj])
                    ->andWhere(['Academias.status_id'   => 1])
                    ->first();
            }

            if(count($academia) >= 1){

                $AcademiaPasswd = TableRegistry::get('AcademiaPasswd');

                $data = [
                    'academia_id'     => $academia->id,
                    'recover_key'     => sha1($academia->id.date('YmdHis')),
                    'passwd'          => 0,
                ];

                $passwd = $AcademiaPasswd->newEntity();
                $passwd = $AcademiaPasswd->patchEntity($passwd, $data);

                if($AcademiaPasswd->save($passwd)){

                    Email::configTransport('academia', [
                        'className' => 'Smtp',
                        'host' => 'mail.logfitness.com.br',
                        'port' => 587,
                        'timeout' => 30,
                        'username' => EMAIL_USER,
                        'password' => EMAIL_SENHA,
                        'client' => null,
                        'tls' => null,
                    ]);

                    $email = new Email();
                    $email
                        ->transport('academia')
                        ->template('academia_passwd')
                        ->emailFormat('html')
                        ->from([EMAIL_USER => EMAIL_NAME])
                        ->to($academia->email)
                        ->subject(EMAIL_NAME.' - Recuperar Senha')
                        ->set([
                            'name'              => $academia->name,
                            'email'             => $academia->email,
                            'recover_key'      => $data['recover_key'],
                        ])
                        ->send('academia_passwd');

                    $this->Flash->success('Solicitação enviada com sucesso. Sigas as instruções no seu e-mail');
                    $this->redirect(['action' => 'forgot_passwd']);
                }else{
                    $this->Flash->error('Falha ao solicitar alteração. Tente novamente...');
                }
            }else{
                $this->Flash->error('Não foi encontrada academia com o e-mail ou CNPJ informado...');
                $solicitacao = 'falha';
            }
        }
        $this->set(compact('solicitacao'));
    }


    public function recover_passwd($email = null, $recover_key = null, $sucesso = false){

        if($email == null || $recover_key == null){
            $this->redirect(['action' => 'login']);
        }else {

            if ($this->request->is(['post', 'put'])) {

                $academia = $this->Academias
                    ->find('all')
                    ->where(['email' => $email])
                    ->first();

                if (count($academia) >= 1) {

                    $AcademiaPasswd = TableRegistry::get('AcademiaPasswd');

                    $academia_passwd = $AcademiaPasswd
                        ->find('all')
                        ->where(['academia_id'      => $academia->id])
                        ->andWhere(['recover_key'   => $recover_key])
                        ->first();

                    if (count($academia_passwd) >= 1) {

                        /*$data_passwd = [
                            'password' => $this->request->data['password']
                        ];*/

                        //$passwd_academia = $this->Academias->patchEntity($academia, $data_passwd);
                        $academia->password = $this->request->data['password'];

                        if ($this->Academias->save($academia)) {

                            //$passwd = $AcademiaPasswd->patchEntity($academia_passwd, ['passwd' => 1]);

                            $academia_passwd->passwd = 1;

                            if ($AcademiaPasswd->save($academia_passwd)) {
                                $this->Flash->success('Recuperação realizada com sucesso. Faça login para continuar');
                                $this->redirect(['action' => 'login']);
                            }
                        }
                    }

                }
            }
        }

        $this->set(compact('sucesso'));

    }

    /**
     * ADMIN ACADEMIA - ACT
     */
    public function admin_act(){

        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $act     = TableRegistry::get('Act')
            ->find('all')
            ->where(['Act.is_visible IN' => [0,1]])
            ->andWhere(['Act.status_id' => 1])
            ->orderDesc('Act.modified')
            ->all();


        $this->paginate = [
            'contain'   => ['Status']
        ];
        $this->set('_serialize', ['act']);

        $this->set(compact('act'));

        $this->viewBuilder()->layout('admin_academia');

    }

    /**
     * ADMIN ACADEMIA - INDICAÇÕES DE PRODUTOS
     */
    public function admin_indicacoes_produtos(){
        $session = $this->request->session();
        $Academia = $session->read('AdminAcademia');
        if (!$Academia) {
            $this->redirect('/academia/acesso/');
        }

        $PreOrders  = TableRegistry::get('PreOrders');

        $pre_orders = $PreOrders
            ->find('all')
            ->contain(['Clientes', 'Professores', 'PreOrderItems', 'PreOrderItems.Produtos', 'PreOrderItems.Produtos.ProdutoBase'])
            ->where(['PreOrders.academia_id' => $Academia->id])
            ->order(['PreOrders.pedido_id' => 'desc', 'PreOrders.cliente_id' => 'asc'])
        ;

        $pre_orders = $this->paginate($pre_orders);

        $this->set(compact('pre_orders'));

        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_professores_view($id = null)

    {
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');


        $ProfessorAcademia =  TableRegistry::get('ProfessorAcademias');

        $Professores = TableRegistry::get('Professores');

        $professores = $Professores
            ->find('list')
            ->all();

        $prof_academia =  $ProfessorAcademia
            ->find('all')
            ->contain(['Academias', 'Professores', 'Academias.Cities', 'Professores.Cities'])
            ->where(['ProfessorAcademias.academia_id' => $Academia->id])
            ->andWhere(['ProfessorAcademias.professor_id' => $id])
            ->andWhere(['ProfessorAcademias.status_id IN' => [1,3]])
            ->first();

        $states = TableRegistry::get('States')->find('list');
        $cities = TableRegistry::get('Cities')->find('list');

        $this->set(compact('prof_academia','professores', 'academias', 'status', 'states', 'cities'));
        $this->set('_serialize', ['prof_Academia']);

        $this->viewBuilder()->layout('admin_academia');
    }

    /** ACEITE PROFESSORES
     * @param
     * @return \Cake\Network\Response|null
     */
    public function admin_add_professores($id = null)
    {
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $academias = $this->Academias
            ->find('list')
            ->all();

        $ProfessorAcademia =  TableRegistry::get('Professor_Academias');

        $prof_academia =  $ProfessorAcademia
            ->find('all')
            ->where(['Professor_Academias.academia_id' => $Academia->id])
            ->andWhere(['Professor_Academias.professor_id' => $id])
            ->andWhere(['Professor_Academias.status_id' => 3])
            ->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $prof_academia->status_id = 1;
            if ($ProfessorAcademia->save($prof_academia)) {
                $this->Flash->success('Aceite realizado com sucesso!');
                return $this->redirect('/academia/admin/professores/');
            } else {
                $this->Flash->error('Falha ao aceitar professor. Tente novamente.');
            }
        }
        $this->set(compact('prof_Academia', 'academias', 'status'));
        $this->set('_serialize', ['prof_Academia']);

        $this->viewBuilder()->layout('admin_academia');
    }


    /** DESLIGAR PROFESSORES
     * @param
     * @return \Cake\Network\Response|null
     */
    public function admin_edit_professores($id = null)
    {
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect(['action' => 'login']);
        }

        $academias = $this->Academias
            ->find('list')
            ->all();

        $ProfessorAcademia =  TableRegistry::get('Professor_Academias');

        $prof_academia =  $ProfessorAcademia
            ->find('all')
            ->where(['Professor_Academias.academia_id' => $Academia->id])
            ->andWhere(['Professor_Academias.professor_id' => $id])
            ->andWhere(['Professor_Academias.status_id IN' => [1,3]])
            ->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $prof_academia->status_id = 2;
            if ($ProfessorAcademia->save($prof_academia)) {
                $this->Flash->success('Professor desligado com sucesso!');
                return $this->redirect(['controller' => 'Academias', 'action' => 'admin_professores']);
            } else {
                $this->Flash->error('Falha ao desligar professor. Tente novamente.');
            }
        }
        $this->set(compact('prof_Academia', 'academias', 'status'));
        $this->set('_serialize', ['prof_Academia']);

        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_professores_convidar(){
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect(['action' => 'login']);
        }

        if ($this->request->is('post')) {

            $Professores       = TableRegistry::get('Professores');
            $ProfessorAcademia = TableRegistry::get('Professor_Academias');

            $professor = $Professores
                ->find('all')
                ->where(['cpf' => $this->request->data['cpf']])
                ->first();

            if(!$professor) {
                $professor = $Professores
                    ->find('all')
                    ->where(['email' => $this->request->data['email']])
                    ->first();

                if($professor) {
                    if($professor->cpf == null) {
                        $data = [
                            'cpf' => $this->request->data['cpf']
                        ];

                        $professor = $Professores->patchEntity($professor, $data);
                        $Professores->save($professor);
                    }
                }
            }

            if($professor) {
                $professor_academia = $ProfessorAcademia
                    ->find('all')
                    ->where(['professor_id' => $professor->id])
                    ->andWhere(['academia_id' => $Academia->id])
                    ->first();

                if($professor_academia) {
                    if($professor_academia->status_id != 1) {
                        $data = [
                            'status_id' => 1
                        ];

                        $professor_academia = $ProfessorAcademia->patchEntity($professor_academia, $data);
                        if($ProfessorAcademia->save($professor_academia)) {
                            Email::configTransport('professor', [
                                'className' => 'Smtp',
                                'host' => 'mail.logfitness.com.br',
                                'port' => 587,
                                'timeout' => 30,
                                'username' => EMAIL_USER,
                                'password' => EMAIL_SENHA,
                                'client' => null,
                                'tls' => null,
                            ]);

                            $email = new Email();

                            $email
                                ->transport('professor')
                                ->template('professor_invited')
                                ->emailFormat('html')
                                ->from([EMAIL_USER => EMAIL_NAME])
                                ->to($professor->email)
                                ->subject($Academia->shortname.' - '.EMAIL_NAME.' - Você foi convidado por uma academia!')
                                ->set([
                                    'name'              => $professor->name,
                                    'academia'          => $Academia->shortname,
                                    'logo_academia'     => $Academia->image,
                                    'academia_master'   => $Academia->master_loja,
                                    'url_academia'      => $Academia->slug
                                ])
                                ->send();

                            $this->Flash->success('Professor adicionado a sua academia com sucesso!');
                            return $this->redirect(['action' => 'admin_professores']);
                        }
                    } else {
                        $this->Flash->error('Esse professor já está na sua academia');
                        return $this->redirect(['action' => 'admin_professores']);
                    }
                } else {
                    $data = [
                        'professor_id' => $professor->id,
                        'academia_id'  => $Academia->id,
                        'status_id'    => 1
                    ];

                    $new_professor_academia = $ProfessorAcademia->newEntity();
                    $new_professor_academia = $ProfessorAcademia->patchEntity($new_professor_academia, $data);
                    if($ProfessorAcademia->save($new_professor_academia)) {
                        Email::configTransport('professor', [
                            'className' => 'Smtp',
                            'host' => 'mail.logfitness.com.br',
                            'port' => 587,
                            'timeout' => 30,
                            'username' => EMAIL_USER,
                            'password' => EMAIL_SENHA,
                            'client' => null,
                            'tls' => null,
                        ]);

                        $email = new Email();

                        $email
                            ->transport('professor')
                            ->template('professor_invited')
                            ->emailFormat('html')
                            ->from([EMAIL_USER => EMAIL_NAME])
                            ->to($professor->email)
                            ->subject($Academia->shortname.' - '.EMAIL_NAME.' - Você foi convidado por uma academia!')
                            ->set([
                                'name'              => $professor->name,
                                'academia'          => $Academia->shortname,
                                'logo_academia'     => $Academia->image,
                                'academia_master'   => $Academia->master_loja,
                                'url_academia'      => $Academia->slug
                            ])
                            ->send();

                        $this->Flash->success('Professor adicionado a sua academia com sucesso!');
                        return $this->redirect(['action' => 'admin_professores']);
                    }
                }
            } else {
                $nome_completo = $this->request->data['name'];
                $nome_completo = explode(' ', $nome_completo);

                if(isset($nome_completo[1])) {
                    function randString($size){
                        $basic = '0123456789';

                        $return= "";

                        for($count= 0; $size > $count; $count++){
                            $return.= $basic[rand(0, strlen($basic) - 1)];
                        }

                        return $return;
                    }

                    $senha = randString(8);

                    $data = [
                        'cpf'           => $this->request->data['cpf'],
                        'email'         => $this->request->data['email'],
                        'name'          => $this->request->data['name'],
                        'phone'         => $this->request->data['phone'],
                        'mobile'        => $this->request->data['phone'],
                        'funcao'        => $this->request->data['funcao'],
                        'password'      => $senha,
                        'status_id'     => 1,
                        'aceite_termos' => 0,
                        'city_id'       => $Academia->city_id
                    ];

                    $new_professor = $Professores->newEntity();
                    $new_professor = $Professores->patchEntity($new_professor, $data);

                    if ($prof_saved = $Professores->save($new_professor)) {
                        $data = [
                            'professor_id' => $prof_saved->id,
                            'academia_id'  => $Academia->id,
                            'status_id'    => 1
                        ];

                        $professor_academia = $ProfessorAcademia->newEntity();
                        $professor_academia = $ProfessorAcademia->patchEntity($professor_academia, $data);

                        if ($ProfessorAcademia->save($professor_academia)) {
                            Email::configTransport('professor', [
                                'className' => 'Smtp',
                                'host' => 'mail.logfitness.com.br',
                                'port' => 587,
                                'timeout' => 30,
                                'username' => EMAIL_USER,
                                'password' => EMAIL_SENHA,
                                'client' => null,
                                'tls' => null,
                            ]);

                            $email = new Email();

                            $email
                                ->transport('professor')
                                ->template('professor_invited_email')
                                ->emailFormat('html')
                                ->from([EMAIL_USER => EMAIL_NAME])
                                ->to($prof_saved->email)
                                ->subject($Academia->shortname.' - '.EMAIL_NAME.' - Você foi convidado por uma academia!')
                                ->set([
                                    'name'              => $prof_saved->name,
                                    'academia'          => $Academia->shortname,
                                    'logo_academia'     => $Academia->image,
                                    'url_academia'      => $Academia->slug,
                                    'academia_master'   => $Academia->master_loja,
                                    'senha'             => $senha
                                ])
                                ->send();

                            $this->Flash->success('Professor convidado e adicionado com sucesso!');
                            return $this->redirect(['action' => 'admin_professores']);
                        } else {
                            $this->Flash->error('Falha ao convidar professor. Tente novamente.');
                        }
                    } else {
                        $this->Flash->error('Falha ao cadastrar professor. Tente novamente.');
                    }
                } else {
                    $this->Flash->error('Você precisa colocar o nome completo do professor.');
                }
            }
        }

        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_alunos_convidar(){
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');

        if(!$Academia){
            $this->redirect(['action' => 'login']);
        }

        if ($this->request->is('post')) {
            
            $Clientes = TableRegistry::get('Clientes');
            $ClienteSugestoes = TableRegistry::get('ClienteSugestoes');

            if(isset($this->request->data['email'])) {
                $cliente = $Clientes
                    ->find('all')
                    ->where(['Clientes.email' => $this->request->data['email']])
                    ->first();

                $this->request->data['aceitou']     = 0;
                $this->request->data['city_id']     = $Academia->city_id;
                $this->request->data['academia_id'] = $Academia->id;

                $new_cliente = $Clientes->newEntity();
                $new_cliente = $Clientes->patchEntity($new_cliente, $this->request->data);

                $new_cliente_sugesto = $ClienteSugestoes->newEntity();
                $new_cliente_sugesto = $ClienteSugestoes->patchEntity($new_cliente_sugesto, $this->request->data);

                if ($sugesto_save = $ClienteSugestoes->save($new_cliente_sugesto)) {
                    Email::configTransport('cliente', [
                        'className' => 'Smtp',
                        'host' => 'mail.logfitness.com.br',
                        'port' => 587,
                        'timeout' => 30,
                        'username' => 'uhull@logfitness.com.br',
                        'password' => 'Lvrt6174?',
                        'client' => null,
                        'tls' => null,
                    ]);

                    $email = new Email();

                    $email
                        ->transport('cliente')
                        ->template('aluno_indicado_academia')
                        ->emailFormat('html')
                        ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                        ->to($sugesto_save->email)
                        ->subject($Academia->shortname.' - LogFitness - Você foi convidado!')
                        ->set([
                            'name'              => $sugesto_save->name,
                            'academia'          => $Academia->shortname,
                            'logo_academia'     => $Academia->image,
                            'url_academia'      => $Academia->slug
                        ])
                        ->send();

                    $this->Flash->success('Aluno convidado com sucesso!');
                    return $this->redirect(['action' => 'admin_alunos']);
                } else {
                    $this->Flash->error('Falha ao convidar aluno. Tente novamente.');
                }
            } else {

                $new_cliente_sugesto = $ClienteSugestoes->newEntity();

                $this->Flash->info('Informe o arquivo a ser importado. <br>
                            Importante: O mesmo deve estar no formato CSV com valores separados por ponto e vírgula ";" não formatado
                            e estar populado com dados em ordem');

                $messages['success'] = '<strong>Alunos Convidados:</strong><br/>';
                $messages['ignore'] = '<br/><strong>Alunos Ignorados:</strong><br/>';
                $messages['error'] = '<br/><strong>Erros encontrados:</strong><br/>';

                $c_success = 0;
                $c_ignore = 0;
                $c_error = 0;

                $row = 0;
                $handle = fopen($this->request->data['csv']['tmp_name'], "r");
                $csv_array = [];
                $separator = ';';

                if ($handle) {

                    Email::configTransport('cliente', [
                            'className' => 'Smtp',
                            'host' => 'mail.logfitness.com.br',
                            'port' => 587,
                            'timeout' => 30,
                            'username' => 'uhull@logfitness.com.br',
                            'password' => 'Lvrt6174?',
                            'client' => null,
                            'tls' => null,
                        ]);

                        $email = new Email();

                    while (($line = fgetcsv($handle, 1000, $separator)) !== false) {
                        $line = array_map('utf8_encode', $line);
                        $data = $line;

                        $num = count($data);

                        if ($num == 3 && $row >= 1) {

                            if(!filter_var($data[1], FILTER_VALIDATE_EMAIL)) {
                                $messages['ignore'] .= ' Falha ao salvar -> '.$data[0].' '.$data[1].' '.$data[2].'<br/>';
                                    $c_error++;
                            } else {
                                $this->request->data['name']        = $data[0];
                                $this->request->data['email']       = $data[1];
                                $this->request->data['aceitou']     = 0;
                                $this->request->data['city_id']     = $Academia->city_id;
                                $this->request->data['telephone']   = $data[2];
                                $this->request->data['mobile']      = $data[2];
                                $this->request->data['academia_id'] = $Academia->id;

                                $new_cliente_sugesto = $ClienteSugestoes->newEntity();
                                $new_cliente_sugesto = $ClienteSugestoes->patchEntity($new_cliente_sugesto, $this->request->data);

                                if ($sugesto_save = $ClienteSugestoes->save($new_cliente_sugesto)) {
                                    
                                    $messages['success'] .= ' -> '.$data[0].' '.$data[1].' '.$data[2].'<br/>';
                                    $c_success++;

                                    $email
                                        ->transport('cliente')
                                        ->template('aluno_indicado_academia')
                                        ->emailFormat('html')
                                        ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                                        ->to($sugesto_save->email)
                                        ->subject($Academia->shortname.' - LogFitness - Você foi convidado por uma academia!')
                                        ->set([
                                            'name'              => $sugesto_save->name,
                                            'academia'          => $Academia->shortname,
                                            'logo_academia'     => $Academia->image,
                                            'url_academia'      => $Academia->slug
                                        ])
                                        ->send();
                                } else {
                                    $messages['ignore'] .= ' Falha ao salvar -> '.$data[0].' '.$data[1].' '.$data[2].'<br/>';
                                    $c_error++;
                                }

                                $csv_array[] = $data;
                            }
                        }
                        $row++;
                    }
                } else {
                    $this->Flash->error('Falha ao carregar arquivo CSV. Tente novamente...');
                }

                fclose($handle);

                !$c_success ? $messages['success'] .= ' :: Nenhuma <br/>' : $messages['success'] .= ' :: TOTAL = ' . $c_success . '<br/>';
                !$c_error ? $messages['error'] .= ' :: Nenhum  <br/>' : $messages['error'] .= ' :: TOTAL = ' . $c_error . '<br/>';
                !$c_ignore ? $messages['ignore'] .= ' :: Nenhuma  <br/>' : $messages['ignore'] .= ' :: TOTAL = ' . $c_ignore . '<br/>';

                $this->Flash->info($messages['success'] . $messages['ignore'] . $messages['error']);
                return $this->redirect(['action' => 'admin_alunos_convidar']);
            }
        }

        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_alunos_convidar_cpf(){
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');

        if(!$Academia){
            $this->redirect(['action' => 'login']);
        }

        if ($this->request->is('post')) {
            
            $Clientes = TableRegistry::get('Clientes');
            $Mensagens = TableRegistry::get('Mensagens');
            $CupomDesconto = TableRegistry::get('CupomDesconto');

            $cliente = $Clientes
                ->find('all')
                ->contain(['Academias'])
                ->where(['Clientes.cpf' => $this->request->data['cpf']])
                ->first();

            if($cliente) {

                Email::configTransport('cliente', [
                    'className' => 'Smtp',
                    'host' => 'mail.logfitness.com.br',
                    'port' => 587,
                    'timeout' => 30,
                    'username' => 'uhull@logfitness.com.br',
                    'password' => 'Lvrt6174?',
                    'client' => null,
                    'tls' => null,
                ]);

                $email = new Email();

                $email
                    ->transport('cliente')
                    ->template('aluno_indicado_academia')
                    ->emailFormat('html')
                    ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                    ->to($cliente->email)
                    ->subject($Academia.' - LogFitness - Você foi convidado!')
                    ->set([
                        'name'              => $cliente->name,
                        'academia'          => $Academia->shortname,
                        'logo_academia'     => $Academia->image,
                        'url_academia'      => $Academia->slug
                    ])
                    ->send();


                $prefixo = explode('(', $cliente->mobile);
                $prefixo = explode(') ', $prefixo[1]);
                $posfixo = explode('-', $prefixo[1]);
                $posfixo = $posfixo[0].$posfixo[1];

                function sanitizeString($string) {
                    $what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Í','Ó','Ú','ñ','Ñ','ç','Ç','-','(',')',',',';',':','|','!','"','#','$','%','&','/','=','?','~','^','>','<','ª','º' );

                    $by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','I','O','U','n','n','c','C','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_' );

                    return str_replace($what, $by, $string);
                }

                $numero = '55'.$prefixo[0].$posfixo;

                $nome_aluno = explode(' ', $cliente->name)[0];

                $mensagem = $Mensagens->newEntity();

                $content_msg = "LOGFITNESS.COM.BR/".$Academia->slug.": Fala ".sanitizeString($nome_aluno).", compre no site e retire na nossa academia com frete grátis.";

                $data_mensagem = [
                    'remetente' => '.',
                    'destinatario' => $numero,
                    'quem' => 'Aluno',
                    'msg' => $content_msg
                ];

                $mensagem = $Mensagens->patchEntity($mensagem, $data_mensagem);

                $mensagem = $Mensagens->save($mensagem);

                $body[] = [
                    "id" => $mensagem->id,
                    "from" => ".",
                    "to" => $numero,
                    "msg" => $content_msg
                ];

                $data = [
                    'sendSmsMultiRequest' => [
                        "sendSmsRequestList" => $body
                    ]
                ];

                $data = json_encode($data);

                $ch = curl_init();

                $header = [
                    "Accept: application/json",
                    "Content-Type: application/json",
                    "Authorization: Basic Y2FjaXF1ZS53ZWI6REd2dHdMckNTZA=="
                ];

                $options = array(CURLOPT_URL => "https://api-rest.zenvia360.com.br/services/send-sms-multiple",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $resp_send_sms = json_decode($response);

                foreach ($resp_send_sms->sendSmsMultiResponse->sendSmsResponseList as $key => $msg_resposta) {
                    if($msg_resposta->statusCode == '00') {
                        $mensagem_enviada = $Mensagens->get($msg_resposta->parts[0]->partId);

                        $mensagem_enviada = $Mensagens->patchEntity($mensagem_enviada, ['enviado' => 1]);
                        $Mensagens->save($mensagem_enviada);
                    }
                }

                $this->Flash->info('Esse aluno já está cadastrado na LOG, enviamos um e-mail e um sms para conhecer a sua academia!');
                return $this->redirect(['action' => 'admin_alunos']);
            } else {
                function randString($size){
                    $basic = '0123456789';

                    $return= "";

                    for($count= 0; $size > $count; $count++){
                        $return.= $basic[rand(0, strlen($basic) - 1)];
                    }

                    return $return;
                }

                $senha = randString(6);

                $data = [
                    'name'          => $this->request->data['name'],
                    'telephone'     => $this->request->data['telephone'],
                    'mobile'        => $this->request->data['telephone'],
                    'email'         => $this->request->data['email'],
                    'cpf'           => $this->request->data['cpf'],
                    'password'      => $senha,
                    'status_id'     => 1,
                    'academia_id'   => $Academia->id,
                    'city_id'       => $Academia->city_id,
                    "cep"           => $Academia->cep,
                    "number"        => $Academia->number,
                    "address"       => $Academia->address,
                    "area"          => $Academia->area
                ];

                $new_cliente = $Clientes->newEntity();
                $new_cliente = $Clientes->patchEntity($new_cliente, $data);

                if ($cliente_saved = $Clientes->save($new_cliente)) {

                    $cliente = $Clientes
                        ->find('all')
                        ->contain(['Academias'])
                        ->where(['Clientes.id' => $cliente_saved->id])
                        ->first();

                    Email::configTransport('cliente', [
                        'className' => 'Smtp',
                        'host' => 'mail.logfitness.com.br',
                        'port' => 587,
                        'timeout' => 30,
                        'username' => 'uhull@logfitness.com.br',
                        'password' => 'Lvrt6174?',
                        'client' => null,
                        'tls' => null,
                    ]);

                    $email = new Email();

                    $email
                        ->transport('cliente')
                        ->template('aluno_indicado_academia')
                        ->emailFormat('html')
                        ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                        ->to($cliente->email)
                        ->subject($cliente->academia->shortname.' - LogFitness - Você foi convidado!')
                        ->set([
                            'name'              => $cliente->name,
                            'academia'          => $cliente->academia->shortname,
                            'logo_academia'     => $cliente->academia->image,
                            'url_academia'      => $cliente->academia->slug
                        ])
                        ->send();

                    $cupom_desconto = $CupomDesconto->newEntity();

                    $limite_cupom = Time::now();
                    $limite_cupom->addMonths(1);

                    $now = Time::now();

                    $data = [
                        'descricao' => 'Cupom de primeira compra',
                        'codigo' => 'sms'.$cliente->id,
                        'valor' => 10,
                        'tipo' => 1,
                        'quantidade' => 1,
                        'status' => 1,
                        'limite' => $limite_cupom,
                        'created' => $now,
                        'modified' => $now
                    ];

                    $cupom_desconto = $CupomDesconto->patchEntity($cupom_desconto, $data);

                    if($cupom_saved = $CupomDesconto->save($cupom_desconto)) {
                        $prefixo = explode('(', $cliente->mobile);
                        $prefixo = explode(') ', $prefixo[1]);
                        $posfixo = explode('-', $prefixo[1]);
                        $posfixo = $posfixo[0].$posfixo[1];

                        $numero = '55'.$prefixo[0].$posfixo;

                        $nome_aluno = explode(' ', $cliente->name)[0];

                        $mensagem = $Mensagens->newEntity();

                        $content_msg = "LOGFITNESS.COM.BR/".$cliente->academia->slug.": Fala ".$nome_aluno.", compre no site e retire na nossa academia com frete grátis. Cupom ".$cupom_saved->codigo." com 10% de desconto!";

                        $data_mensagem = [
                            'remetente' => '.',
                            'destinatario' => $numero,
                            'quem' => 'Aluno',
                            'msg' => $content_msg
                        ];

                        $mensagem = $Mensagens->patchEntity($mensagem, $data_mensagem);

                        $mensagem = $Mensagens->save($mensagem);

                        $body[] = [
                            "id" => $mensagem->id,
                            "from" => ".",
                            "to" => $numero,
                            "msg" => $content_msg
                        ];

                        $data = [
                            'sendSmsMultiRequest' => [
                                "sendSmsRequestList" => $body
                            ]
                        ];

                        $data = json_encode($data);

                        $ch = curl_init();

                        $header = [
                            "Accept: application/json",
                            "Content-Type: application/json",
                            "Authorization: Basic Y2FjaXF1ZS53ZWI6REd2dHdMckNTZA=="
                        ];

                        $options = array(CURLOPT_URL => "https://api-rest.zenvia360.com.br/services/send-sms-multiple",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                        );

                        curl_setopt_array($ch, $options);

                        $response = curl_exec($ch);
                        curl_close($ch);

                        $resp_send_sms = json_decode($response);

                        foreach ($resp_send_sms->sendSmsMultiResponse->sendSmsResponseList as $key => $msg_resposta) {
                            if($msg_resposta->statusCode == '00') {
                                $mensagem_enviada = $Mensagens->get($msg_resposta->parts[0]->partId);

                                $mensagem_enviada = $Mensagens->patchEntity($mensagem_enviada, ['enviado' => 1]);
                                $Mensagens->save($mensagem_enviada);
                            }
                        }
                    }

                    $this->Flash->success('Parabéns! Esse aluno foi cadastrado e receberá um sms com um cupom de desconto para sua primeira compra!');
                    return $this->redirect(['action' => 'admin_alunos']);
                } else {
                    $this->Flash->error('Falha ao convidar aluno. Tente novamente.');
                }
            }

        }

        return $this->redirect(['action' => 'admin_alunos']);

        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_express($pedido_id = null)
    {
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        if($pedido_id) {
            $session->delete('Carrinho');

            $pedido = TableRegistry::get('Pedidos')
                ->find('all')
                ->contain(['PedidoItens'])
                ->where(['Pedidos.id' => $pedido_id])
                ->first();

            foreach ($pedido->pedido_itens as $item) {
                $session->write('Carrinho.'.$item->produto_id, $item->quantidade);
            }
        }

        $max_date_novidade = Time::now();
        $max_date_novidade->subMonth(1); 

        $Produtos = TableRegistry::get('Produtos');
        $ProdutoObjetivos = TableRegistry::get('ProdutoObjetivos');   

        $this->paginate = [

        ];

        $produtos = $Produtos
            ->find('all', ['contain' => [
                'ProdutoBase',
                'ProdutoBase.Propriedades',
            ]])
            ->where(['Produtos.preco > '     => 0.1])
            ->andWhere(['Produtos.visivel'      => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->order(['Produtos.status_id' => 'asc', 'rand()'])
            ->limit(24)
            ->all(); 

        $prod_objetivos = $ProdutoObjetivos
            ->find('all', ['contain' => ['Objetivos']])
            ->all();   

        $produtos_min = $Produtos
            ->find('all', ['contain' => ['ProdutoBase']])
            ->where(['Produtos.preco > ' => 0.1])
            ->distinct('ProdutoBase.id')
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->order(['Produtos.preco' => 'asc'])
            ->limit(1)
            ->all();

        $produtos_max = $Produtos
            ->find('all', ['contain' => ['ProdutoBase']])
            ->where(['Produtos.preco > ' => 0.1])
            ->distinct('ProdutoBase.id')
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->order(['Produtos.preco' => 'desc'])
            ->limit(1)
            ->all();

        $produtos_exibidos = [];

        foreach ($produtos as $key) {
            $produtos_exibidos[] = $key->id;
        } 

        $session    = $this->request->session();

        $session->write('ProdutosExibidos', $produtos_exibidos);

        $session->write('QtdProdutosExibidos', count($produtos_exibidos));

        $session_produto = $session->read('Carrinho');

        $session_produto_ids = [];
        if(!empty($session_produto)) {
            foreach ($session_produto as $key => $item) {
                $session_produto_ids[] = $key;
            }
        }

        if(!empty($session_produto_ids)) {
            $s_produtos = $Produtos
                ->find('all', ['contain' => ['ProdutoBase', 'ProdutoBase.Marcas']])
                ->andWhere(['Produtos.preco > ' => 0.1])
                ->where(['Produtos.id IN' => $session_produto_ids])
                ->all();
        }else{
            $s_produtos = [];
        }

        $Banners            = TableRegistry::get('Banners');
        $AcademiaBanners    = TableRegistry::get('AcademiaBanners');

        /**
         * BANNERS PADRÃO - LOGFITNESS
         */
        $banners = $Banners
            ->find('all')
            ->where(['Banners.status_id' => 1])
            ->order('rand()')
            ->limit(6)
            ->all()
            ->toArray();

        if(count($banners) <= 0){
            $banners = [];
        }

        /**
         * BANNERS ACADEMIA
         */
        if($Academia) {
            $banners_academia = $AcademiaBanners
                ->find('all')
                ->where(['AcademiaBanners.status_id' => 1])
                ->andWhere(['AcademiaBanners.academia_id' => $Academia->id])
                ->order('rand()')
                ->limit(4)
                ->all()
                ->toArray();

            if (count($banners_academia) <= 0) {
                $banners_academia = [];
            }
        }else{
            $banners_academia = [];
        }

        $this->set(compact('prod_objetivos', 'max_date_novidade', 'produtos_max', 'produtos_min', 's_produtos', 'session_produto', 'produtos', 'banners', 'banners_academia', 'pedido_id'));
        $this->set('_serialize', ['produtos']);

        $this->viewBuilder()->layout('default6');
    }

    public function admin_express_fechar()
    {

        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $session_dono = $session->read('DonoAcademia');
        $PreOrders = TableRegistry::get('PreOrders');

        $variavel_desconto = TableRegistry::get('VariaveisGlobais')
            ->find('all')
            ->where(['name' => 'desconto'])
            ->first();

        $desconto_geral = 1 - ($variavel_desconto->valor * 0.01);

        /**
         * CLIENTE NÃO LOGADO
         */
        if(empty($session_dono)){
            $this->redirect('academia/admin/express/login/');       
        }
        /**
         * CLIENTE LOGADO NO SISTEMA
         */
        else{
            $Pedidos = TableRegistry::get('Pedidos');
            $Produtos = TableRegistry::get('Produtos');

            $cliente = TableRegistry::get('Clientes')
                ->find('all')
                ->where(['Clientes.id' => $session_dono->id])
                ->contain(['Cities', 'Academias', 'Academias.Cities'])
                ->first();

            $session_carrinho = $session->read('Carrinho');

            $pedido = $Pedidos->newEntity();

            $valor = 0;

            $produto_carrinho_total = 0;

            foreach ($session_carrinho as $ks => $item):
                $valor = $Produtos->find('all')->where(['id' => $ks])->first('preco_promo');
                if($valor->preco_promo < 1) {
                    $produto_carrinho = $Produtos->find('all')->where(['id' => $ks])->sumOf('preco') * $desconto_geral;
                } else {
                    $produto_carrinho = $Produtos->find('all')->where(['id' => $ks])->sumOf('preco_promo') * $desconto_geral;
                }

                $produto_carrinho_total += ($item * $produto_carrinho);
                $valor += $produto_carrinho_total - 1;
            endforeach;

            $valor = $valor * .75;

            debug($valor);

            $data = [
                'cliente_id'        => $cliente->id,
                'pedido_status_id'  => 1,
                'frete'             => 0,
                'academia_id'       => $Academia->id,
                'valor'             => (float)$valor,
                'comissao_status'   => 2
            ];

            $pedido = $Pedidos->patchEntity($pedido, $data);

            /**
             * SE O PEDIDO AINDA NÃO FOI REGISTRADO NO SISTEMA
             */
            if(!$session->read('PedidoFinalizado') &&
                !$session->read('PedidoFinalizadoItem') &&
                !$session->read('ClienteFinalizado')){

                if ($PEDIDO_SAVE = $Pedidos->save($pedido)){

                    $PEDIDO_SAVE_ID = ['id' => (20600000 + $PEDIDO_SAVE->id)];

                    $PEDIDO_SAVE = $Pedidos->patchEntity($PEDIDO_SAVE, $PEDIDO_SAVE_ID);
                    if($PEDIDO = $Pedidos->save($PEDIDO_SAVE)){

                        $session->write('PedidoFinalizado', $PEDIDO);

                        $PedidoItens = TableRegistry::get('PedidoItens');

                        if($session_carrinho && count($session_carrinho) >= 1){

                            foreach ($session_carrinho as $key => $item){

                                $produto = $Produtos
                                    ->find('all')
                                    ->where(['Produtos.id' => $key])
                                    ->first();

                                if ($produto) {

                                    $produto->preco_promo ?
                                        $preco = $produto->preco_promo * .75 :
                                        $preco = $produto->preco * .75;

                                    $pedido_item = $PedidoItens->newEntity();
                                    $item_data = [
                                        'pedido_id' => $PEDIDO->id,
                                        'produto_id' => $key,
                                        'quantidade' => $item,
                                        'desconto' => 0,
                                        'preco' => $preco,
                                    ];

                                    $pedido_item = $PedidoItens->patchEntity($pedido_item, $item_data);

                                    if ($PEDIDO_ITEM_SAVE = $PedidoItens->save($pedido_item)) {
                                        $session->write('PedidoFinalizadoItem.'.$PEDIDO_ITEM_SAVE->id, $PEDIDO_ITEM_SAVE);
                                    }
                                }
                            }

                            $total = number_format($valor, 2, '.', '');

                            $PedidoCliente = $Pedidos
                                ->find('all', ['contain' => [
                                    'Academias',
                                    'Academias.Cities',
                                    'Clientes',
                                    'PedidoItens',
                                    'PedidoItens.Produtos',
                                    'PedidoItens.Produtos.ProdutoBase',
                                    'PedidoItens.Produtos.ProdutoBase.Marcas'
                                ]
                                ])
                                ->where(['Pedidos.id' => $PEDIDO->id])
                                ->first();

                            Email::configTransport('cliente', [
                                'className' => 'Smtp',
                                'host' => 'mail.logfitness.com.br',
                                'port' => 587,
                                'timeout' => 30,
                                'username' => 'uhull@logfitness.com.br',
                                'password' => 'Lvrt6174?',
                                'client' => null,
                                'tls' => null,
                            ]);

                            $email = new Email();

                            $email
                                ->transport('cliente')
                                ->template('mochila_fechada_express')
                                ->emailFormat('html')
                                ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                                ->to($PedidoCliente->cliente->email)
                                ->subject($PedidoCliente->academia->shortname.' - LogFitness - Vitrine Express')
                                ->set([
                                    'id'                => $PedidoCliente->id,
                                    'name'              => $PedidoCliente->cliente->name,
                                    'academia'          => $PedidoCliente->academia->shortname,
                                    'logo_academia'     => $PedidoCliente->academia->image
                                ])
                                ->send();

                            $session->write('ClienteFinalizado', $cliente);

                            $this->iugu_cobranca_direta($PedidoCliente->id);

                            $this->set(compact('PedidoCliente', 'cliente', 'token'));
                        }else{
                            $this->redirect('/mochila');
                        }
                    }
                }else{
                    $this->redirect('/mochila/');
                }
            }else{

                if($session->read('PedidoFinalizado.id') > 1 && $session->read('ClienteFinalizado')) {

                    debug($session->read('PedidoFinalizado.id'));

                    $PedidoCliente = $Pedidos
                        ->find('all', ['contain' => [
                            'Academias',
                            'Academias.Cities',
                            'Clientes',
                            'PedidoItens',
                            'PedidoItens.Produtos',
                            'PedidoItens.Produtos.ProdutoBase',
                            'PedidoItens.Produtos.ProdutoBase.Marcas'
                        ]
                        ])
                        ->where(['Pedidos.id' => $session->read('PedidoFinalizado.id')])
                        ->first();

                    $this->iugu_cobranca_direta($PedidoCliente->id);

                    $this->set(compact('PedidoCliente', 'cliente', 'token'));

                }elseif ($PEDIDO_SAVE = $Pedidos->save($pedido)){

                    $PEDIDO_SAVE_ID = ['id' => (20600000 + $PEDIDO_SAVE->id)];

                    $PEDIDO_SAVE = $Pedidos->patchEntity($PEDIDO_SAVE, $PEDIDO_SAVE_ID);
                    if($PEDIDO = $Pedidos->save($PEDIDO_SAVE)){

                        $session->write('PedidoFinalizado', $PEDIDO);

                        $PedidoItens = TableRegistry::get('PedidoItens');
                        $session_carrinho = $session->read('Carrinho');

                        if($session_carrinho) {

                            foreach ($session_carrinho as $key => $item){
                                $produto = $Produtos
                                    ->find('all')
                                    ->where(['Produtos.id' => $key])
                                    ->first();

                                if ($produto) {                                        
                                    $produto->preco_promo ?
                                        $preco = $produto->preco_promo * .75 :
                                        $preco = $produto->preco * .75;

                                    $pedido_item = $PedidoItens->newEntity();
                                    $item_data = [
                                        'pedido_id' => $PEDIDO->id,
                                        'produto_id' => $key,
                                        'quantidade' => $item,
                                        'desconto' => 0,
                                        'preco' => $preco,
                                    ];

                                    $pedido_item = $PedidoItens->patchEntity($pedido_item, $item_data);

                                    if ($PEDIDO_ITEM_SAVE = $PedidoItens->save($pedido_item)) {
                                        $session->write('PedidoFinalizadoItem.'.$PEDIDO_ITEM_SAVE->id, $PEDIDO_ITEM_SAVE);
                                    }
                                }
                            }

                            $total = number_format($valor, 2, '.', '');

                            $PedidoCliente = $Pedidos
                                ->find('all', ['contain' => [
                                    'Academias',
                                    'Academias.Cities',
                                    'PedidoItens',
                                    'PedidoItens.Produtos',
                                    'PedidoItens.Produtos.ProdutoBase',
                                    'PedidoItens.Produtos.ProdutoBase.Marcas'
                                ]
                                ])
                                ->where(['Pedidos.id' => $PEDIDO->id])
                                ->first();

                            $cliente = TableRegistry::get('Clientes')
                                ->find('all')
                                ->where(['Clientes.id' => $session_dono->id])
                                ->contain(['Cities', 'Academias', 'Academias.Cities'])
                                ->first();

                            $session->write('ClienteFinalizado', $cliente);

                            $this->iugu_cobranca_direta($PedidoCliente->id);

                            $this->set(compact('PedidoCliente', 'cliente', 'token'));
                        }else{

                            $Pedidos->delete($PEDIDO_SAVE);
                            $Pedidos->delete($PEDIDO);

                            $session->delete('PedidoFinalizado');
                            $session->delete('PedidoFinalizadoItem');
                            $session->delete('ClienteFinalizado');

                            $this->redirect('/mochila');

                        }
                    }
                }
            }
            $session->delete('Carrinho');
        }

        $this->viewBuilder()->layout('default6');
    }


    public function iugu_cobranca_direta($pedido_id = null) {
        if($pedido_id != null) { 
            if($this->request->is(['post', 'put'])) {
                $Pedidos = TableRegistry::get('Pedidos');

                $PedidoCliente  = $Pedidos
                    ->find('all', ['contain' => [
                        'Academias',
                        'Academias.Cities',
                        'Clientes',
                        'PedidoItens',
                        'PedidoItens.Produtos',
                        'PedidoItens.Produtos.ProdutoBase',
                        'PedidoItens.Produtos.ProdutoBase.Marcas'
                    ]
                    ])
                    ->where(['Pedidos.id' => $pedido_id])
                    ->first(); 

                $variavel_desconto = TableRegistry::get('VariaveisGlobais')
                    ->find('all')
                    ->where(['name' => 'desconto'])
                    ->first();

                $desconto_geral = 1 - ($variavel_desconto->valor * 0.01);

                if($PedidoCliente->cupom_desconto_id != null) {
                    $cupom_desconto_pedido = TableRegistry::get('CupomDesconto')
                        ->find('all')
                        ->where(['CupomDesconto.id' => $PedidoCliente->cupom_desconto_id])
                        ->first();
                }

                foreach ($PedidoCliente->pedido_itens as $pi) {

                    if(count($cupom_desconto_pedido) >= 1) {
                        $desconto  = $cupom_desconto_pedido->valor * .01;
                        $desconto  = $pi->preco * $desconto;
                        $pi->preco = $pi->preco - $desconto;
                    }

                    if(isset($this->request->data['parcelas'])) {
                        if($this->request->data['parcelas'] == 1) {
                            $valor = number_format($pi->preco * $desconto_geral, 2, '', '');
                        } else if($this->request->data['parcelas'] >= 2) {
                            $valor = number_format($pi->preco * $desconto_geral, 2, '', '') + number_format($pi->preco * $desconto_geral * 0.089, 2, '', ''); 
                        }
                    } else {
                        $valor = number_format($pi->preco * $desconto_geral, 2, '', '');
                    }

                    $valor = str_replace('.', '', $valor);

                    $items_data[] = [
                        "description" => $pi->produto->produto_base->name,
                        "quantity" => $pi->quantidade,
                        "price_cents" => $valor
                    ];

                    $total_parcelado += $pi->quantidade * $valor;
                }

                $total_parcelado = $total_parcelado * 0.01;

                if(isset($this->request->data['token'])) {
                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $prefixo = explode('(', $PedidoCliente->cliente->telephone);
                    $prefixo = explode(')', $prefixo[1]);

                    $address_data = [
                        "street" => $PedidoCliente->academia->address,
                        "number" => $PedidoCliente->academia->number,
                        "city" => $PedidoCliente->academia->city->name,
                        "state" => $PedidoCliente->academia->city->uf,
                        "country" => 'Brasil',
                        "zip_code" => $PedidoCliente->academia->cep
                    ];

                    $payer_data = [
                        "name" => $PedidoCliente->cliente->name,
                        "phone_prefix" => $prefixo[0],
                        "phone" => $prefixo[1],
                        "email" => $PedidoCliente->cliente->email,
                        "address" => $address_data,
                        "cpf_cnpj" => $PedidoCliente->cliente->cpf
                    ];

                    $data = [
                        /*Live*/
                        "api_token"      => IUGU_API_TOKEN,
                        /*Test*/
                        //"api_token"      => IUGU_API_TOKEN,
                        "token"          => $this->request->data['token'],
                        "email"          => $PedidoCliente->cliente->email,
                        "months"         => $this->request->data['parcelas'],
                        "items"          => $items_data,
                        "payer"          => $payer_data
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/charge",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    var_dump($response);

                    $resp_iugu_cartao = json_decode($response);

                    if($resp_iugu_cartao->success) {
                        if($resp_iugu_cartao->message == 'Autorizado') {
                            $PedidoCliente->data_pagamento   = Time::now();
                            $PedidoCliente->pedido_status_id = 3;
                        } else {
                            $PedidoCliente->pedido_status_id = 2;
                        }
                        $PedidoCliente->tid              = $resp_iugu_cartao->invoice_id;
                        $PedidoCliente->iugu_token       = $this->request->data['token'];
                        $PedidoCliente->parcelas         = $this->request->data['parcelas'];
                        $PedidoCliente->valor_parcelado  = $total_parcelado;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_cartao->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_cartao->pdf;
                        $PedidoCliente->iugu_brand       = $this->request->data['iugu_brand'];
                        $PedidoCliente->identification   = $resp_iugu_cartao->identification;
                        $PedidoCliente->portador_name    = $this->request->data['portador_name'];
                        $PedidoCliente->portador_birth   = $this->request->data['portador_birth'];
                        $PedidoCliente->portador_phone   = $this->request->data['portador_phone'];
                        $PedidoCliente->portador_cpf     = $this->request->data['portador_cpf'];
                        $PedidoCliente->payment_method   = 'CartaoDeCredito';
                        $PedidoCliente->transaction_data = serialize($response);
                        $Pedidos->save($PedidoCliente);
                    
                        $this->redirect('/mochila/finalizado-express/'.$PedidoCliente->id);
                    } else {
                        $PedidoCliente->tid              = $resp_iugu_cartao->invoice_id;
                        $PedidoCliente->iugu_token       = $this->request->data['token'];
                        $PedidoCliente->parcelas         = $this->request->data['parcelas'];
                        $PedidoCliente->valor_parcelado  = $total_parcelado;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_cartao->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_cartao->pdf;
                        $PedidoCliente->iugu_brand       = $this->request->data['iugu_brand'];
                        $PedidoCliente->identification   = $resp_iugu_cartao->identification;
                        $PedidoCliente->portador_name    = $this->request->data['portador_name'];
                        $PedidoCliente->portador_birth   = $this->request->data['portador_birth'];
                        $PedidoCliente->portador_phone   = $this->request->data['portador_phone'];
                        $PedidoCliente->portador_cpf     = $this->request->data['portador_cpf'];
                        $PedidoCliente->pedido_status_id = 1;
                        $PedidoCliente->transaction_data = serialize($response);
                        $Pedidos->save($PedidoCliente);

                        $this->Flash->error('Falha ao passar cartão, tente novamente!');
                        $this->redirect('/academia/admin/express-fechar');
                    }

                } else {
                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $prefixo = explode('(', $PedidoCliente->cliente->telephone);
                    $prefixo = explode(')', $prefixo[1]);

                    $address_data = [
                        "street" => $PedidoCliente->academia->address,
                        "number" => $PedidoCliente->academia->number,
                        "city" => $PedidoCliente->academia->city->name,
                        "state" => $PedidoCliente->academia->city->uf,
                        "country" => 'Brasil',
                        "zip_code" => $PedidoCliente->academia->cep,
                        "district" => $PedidoCliente->academia->area
                    ];

                    $payer_data = [
                        "name" => $PedidoCliente->cliente->name,
                        "phone_prefix" => $prefixo[0],
                        "phone" => $prefixo[1],
                        "email" => $PedidoCliente->cliente->email,
                        "address" => $address_data,
                        "cpf_cnpj" => $PedidoCliente->cliente->cpf
                    ];

                    $data = [
                        /*Live*/
                        "api_token"      => IUGU_API_TOKEN,
                        /*Test*/
                        //"api_token"      => IUGU_API_TOKEN,
                        "method"         => 'bank_slip',
                        "email"          => $PedidoCliente->cliente->email,
                        "months"         => $this->request->data['parcelas'],
                        "items"          => $items_data,
                        "payer"          => $payer_data
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/charge",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    var_dump($response);

                    $resp_iugu_boleto = json_decode($response);

                    if($resp_iugu_boleto->success) {
                        $PedidoCliente->tid              = $resp_iugu_boleto->invoice_id;
                        $PedidoCliente->identification   = $resp_iugu_boleto->identification;
                        $PedidoCliente->payment_method   = 'BoletoBancario';
                        $PedidoCliente->parcelas         = 1;
                        $PedidoCliente->valor_parcelado  = null;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_boleto->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_boleto->pdf;
                        $PedidoCliente->pedido_status_id = 2;
                        $PedidoCliente->transaction_data = serialize($response);
                        $Pedidos->save($PedidoCliente);

                        $this->redirect('/mochila/finalizado-express/'.$PedidoCliente->id);
                    } else {
                        $PedidoCliente->tid              = $resp_iugu_boleto->invoice_id;
                        $PedidoCliente->identification   = $resp_iugu_boleto->identification;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_boleto->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_boleto->pdf;
                        $PedidoCliente->pedido_status_id = 1;
                        $PedidoCliente->transaction_data = serialize($response);
                        $Pedidos->save($PedidoCliente);

                        $this->Flash->error('Falha ao gerar boleto, tente novamente!');
                        $this->redirect('/academia/admin/express-fechar');
                    }
                }
            }
        }

        $this->viewBuilder()->layout('default6');
    }

    public function admin_express_login(){

        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $session = $this->request->session();

        $FB_email = $session->read('FB.email');

        if($this->request->is(['post', 'patch', 'put']) || $FB_email){

            $FB_email ? $email = $FB_email : $email = $this->request->data['email'];

            $Clientes = TableRegistry::get('Clientes');

            if(strpos($email, '@')) {
                $cliente = $Clientes
                    ->find('all', ['contain' => []])
                    ->leftJoinWith('Cities')
                    ->leftJoinWith('Cities.States')
                    ->where(['Clientes.email' => $email])
                    ->andWhere(['Clientes.status_id' => 1])
                    ->first();
            } else if(strpos($email, '.')) {
                $cliente = $Clientes
                    ->find('all', ['contain' => []])
                    ->leftJoinWith('Cities')
                    ->leftJoinWith('Cities.States')
                    ->where(['Clientes.cpf' => $email])
                    ->andWhere(['Clientes.status_id' => 1])
                    ->first();
            } else {
                $parte_um     = substr($email, 0, 3);
                $parte_dois   = substr($email, 3, 3);
                $parte_tres   = substr($email, 6, 3);
                $parte_quatro = substr($email, 9, 2);

                $cpf = $parte_um.'.'.$parte_dois.'.'.$parte_tres.'-'.$parte_quatro;

                $cliente = $Clientes
                    ->find('all', ['contain' => []])
                    ->leftJoinWith('Cities')
                    ->leftJoinWith('Cities.States')
                    ->where(['Clientes.cpf' => $cpf])
                    ->andWhere(['Clientes.status_id' => 1])
                    ->first();
            }
            
            $hasher = new DefaultPasswordHasher();
            $verify = new DefaultPasswordHasher;

            if($cliente) {

                if(isset($cliente->cpf) &&
                isset($cliente->academia_id) &&
                isset($cliente->city_id)) {
                    isset($this->request->data['password']) ? $password = $this->request->data['password'] : $password = '';
                    if ($verify->check($password, $cliente->password) || $FB_email) {
                        $session = $this->request->session();
                        $session->write('DonoAcademia', $cliente);

                        if ($FB_email) {
                            $this->redirect(SLUG . DS . 'academia/admin/express-fechar');
                        } else {
//                            echo 'Sucesso! <meta http-equiv="refresh" content="0;URL=' . WEBROOT_URL . SLUG . DS . 'home" >';
                            echo 'Sucesso!';
                            echo '<script>location.href="'.WEBROOT_URL . SLUG . 'academia/admin/express-fechar";</script>';
                        }

                        $this->render('ajax_login_express');
                        $this->viewBuilder()->layout('ajax');
                    } else {
                        echo '<style> #info-cadastro-main-com {color: red;} </style>'.'Usuário ou senha não encontrado.';
                        $this->render('ajax_login_express');
                        $this->viewBuilder()->layout('ajax');
                    }
                } else {
                    echo '<style> #info-cadastro-main-com {color: red;} </style>'.'O usuário está com o cadastro imcompleto!<br>Para finalizar a compra é necessário preencher o cadastro e voltar aqui!';
                    $this->render('ajax_login_express');
                    $this->viewBuilder()->layout('ajax');
                }

            }else{
                echo '<style> #info-cadastro-main-com {color: red;} </style>'.'Usuário ou senha não encontrado.';
                $this->render('ajax_login_express');
                $this->viewBuilder()->layout('ajax');
            }
        }

        if($redirect == 'fb-login-error'){
            $this->set('fb_login_error', true);
        }

        $this->set(compact('objetivos_list', 'esportes_list', 'academias', 'academias_map', 'states'));
        $this->viewBuilder()->layout('default6');
    }

    public function admin_perfil(){
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');

        $academia_perfil = $this->Academias
            ->find('all')
            ->contain(['Cities', 'Cities.States'])
            ->where(['Academias.id' => $Academia->id])
            ->first();

        
        $this->set(compact('academia_perfil'));
        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_relatorios_professor(){
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect('/academia/acesso/');
        }

        $this->set('select_date', true);

        if ($this->request->is(['post', 'put'])) {

            $ano = $this->request->data['ano'];
            $mes = $this->request->data['mes'];

            if($mes != null && $ano != null) {
                $select_date = false;

                $AcademiaProfessores = TableRegistry::get('ProfessorAcademias');
                $Professores         = TableRegistry::get('Professores');
                $AcademiaPedidos     = TableRegistry::get('Pedidos');

                $academia_professores = $AcademiaProfessores
                    ->find('all')
                    ->contain(['Professores'])
                    ->where(['ProfessorAcademias.academia_id' => $Academia->id])
                    ->andWhere(['ProfessorAcademias.status_id' => 1])
                    ->andWhere(['ProfessorAcademias.created <' => $ano . '-' . $mes . '-30 00:00:00'])
                    ->all();

                foreach ($academia_professores as $ap) {
                    $professores_academia_id[] = $ap->professor_id;
                }

                if(!empty($professores_academia_id)) {
                    $prof_relatorio_name = $Professores
                        ->find('all')
                        ->where(['Professores.id IN' => $professores_academia_id])
                        ->andWhere(['Professores.status_id' => 1])
                        ->all();

                    $prof_academia_relatorio = $AcademiaPedidos
                        ->find('all')
                        ->contain(['Professores'])
                        ->where(['Pedidos.academia_id' => $Academia->id])
                        ->andWhere(['Pedidos.professor_id IN' => $professores_academia_id])
                        ->andWhere(['Pedidos.created LIKE' => $ano . '-' . $mes . '%'])
                        ->andWhere(['Pedidos.pedido_status_id >='     => 3])
                        ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7,8]])
                        ->andWhere(['Pedidos.comissao_status IN'   => 1])
                        ->andWhere(['Professores.status_id' => 1])
                        ->order(['Pedidos.created' => 'desc'])
                        ->all();
                } else {
                    $prof_relatorio_name = [];
                    $prof_academia_relatorio = [];
                }

                $this->set(compact('academia_professores', 'prof_academia_relatorio', 'professores_academia_id', 'prof_relatorio_name', 'mes', 'ano', 'select_date'));
            }
        }

        $this->viewBuilder()->layout('admin_academia');
    }

    public function profile_acad($slug = null){

        $session  = $this->request->session();

        $Cliente  = $session->read('Cliente');
        $Academia = $session->read('Academia');

        if($slug == null && !$Academia){
            $this->redirect('/');
        }

        $Avaliacoes_anchor = $session->read('Avaliacoes-anchor');
        $session->delete('Avaliacoes-anchor');

        if($slug) {
            $academia = $this->Academias
                ->find('all')
                ->contain(['Cities'])
                ->where(['Academias.slug' => $slug])
                ->andWhere(['Academias.status_id' => 1])
                ->first();
        } else if($Academia) {
            $academia = $this->Academias
                ->find('all')
                ->contain(['Cities'])
                ->where(['Academias.id' => $Academia->id])
                ->andWhere(['Academias.status_id' => 1])
                ->first();
        }

        $session->write('CidadeAcademia', $academia->city_id);
        $session->write('EstadoAcademia', $academia->city->state_id);

        $Clientes                   = TableRegistry::get('Clientes');
        $Avaliacoes                 = TableRegistry::get('Avaliacoes');
        $Diferenciais               = TableRegistry::get('Diferenciais');
        $AcademiaEsportes           = TableRegistry::get('AcademiaEsportes');
        $AcademiaDiferenciais       = TableRegistry::get('AcademiaDiferenciais');
        $ProfessorAcademias         = TableRegistry::get('ProfessorAcademias');
        $AcademiaCalendario         = TableRegistry::get('AcademiaCalendario');
        $AcademiaGaleria            = TableRegistry::get('AcademiaGaleria');
        $AcademiaBanners            = TableRegistry::get('AcademiaBanners');
        $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
        $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');
        $PlanosClientes             = TableRegistry::get('PlanosClientes');
        $Produtos                   = TableRegistry::get('Produtos');

        $cliente_acad = $Clientes
            ->find('all')
            ->contain(['Academias'])
            ->where(['Clientes.id' => $Cliente->id])
            ->first();

        $modalidades = $AcademiaEsportes
            ->find('all')
            ->contain(['Esportes'])
            ->where(['AcademiaEsportes.academia_id' => $academia->id])
            ->all();

        $diferenciais = $Diferenciais
            ->find('all')
            ->where(['status' => 1])
            ->all();

        $acad_diferenciais = $AcademiaDiferenciais
            ->find('all')
            ->where(['AcademiaDiferenciais.academia_id' => $academia->id])
            ->all();

        $instrutores = $ProfessorAcademias
            ->find('all')
            ->contain(['Professores'])
            ->where(['ProfessorAcademias.academia_id' => $academia->id])
            ->andWhere(['ProfessorAcademias.status_id' => 1])
            ->all();

        $academia_galeria = $AcademiaGaleria
            ->find('all')
            ->where(['academia_id' => $academia->id])
            ->andWhere(['status' => 1])
            ->all();

        $academia_banners = $AcademiaBanners
            ->find('all')
            ->where(['academia_id' => $academia->id])
            ->andWhere(['status_id' => 1])
            ->all();

        foreach ($academia_banners as $a_banner) {
            $galeria[$a_banner->id][0] = $a_banner;
            $galeria[$a_banner->id][1] = 'banners/';
        }

        foreach ($academia_galeria as $a_galeria) {
            $galeria[$a_galeria->id][0] = $a_galeria;
            $galeria[$a_galeria->id][1] = 'galeria/';
        }

        shuffle($galeria);

        $produtos = $Produtos
            ->find('all', ['contain' => [
                'ProdutoBase',
                'ProdutoBase.Propriedades',
            ]])
            ->where(['Produtos.preco > '        => 0.1])
            ->andWhere(['Produtos.visivel'      => 1])
            ->andWhere(['ProdutoBase.status_id' => 1])
            ->order(['Produtos.status_id' => 'asc', 'rand()'])
            ->limit(12)
            ->all(); 

        $calendario = $AcademiaCalendario
            ->find('all')
            ->where(['academia_id' => $academia->id])
            ->order(['horario_inicio' => 'asc'])
            ->all();

        $segunda = 0;
        $terca   = 1;
        $quarta  = 2;
        $quinta  = 3;
        $sexta   = 4;
        $sabado  = 5;
        $domingo = 6;

        $cdia_segunda = 0;
        $cdia_terca   = 0;
        $cdia_quarta  = 0;
        $cdia_quinta  = 0;
        $cdia_sexta   = 0;
        $cdia_sabado  = 0;
        $cdia_domingo = 0;

        foreach ($calendario as $cdias) {
            if($cdias->dia == 0) {
                $cdia_segunda++;
            }
            if($cdias->dia == 1) {
                $cdia_terca++;
            }
            if($cdias->dia == 2) {
                $cdia_quarta++;
            }
            if($cdias->dia == 3) {
                $cdia_quinta++;
            }
            if($cdias->dia == 4) {
                $cdia_sexta++;
            }
            if($cdias->dia == 5) {
                $cdia_sabado++;
            }
            if($cdias->dia == 6) {
                $cdia_domingo++;
            }
        }

        $linhas = $cdia_segunda;

        if($cdia_terca > $linhas) {
            $linhas = $cdia_terca;
        }
        if($cdia_quarta > $linhas) {
            $linhas = $cdia_quarta;
        }
        if($cdia_quinta > $linhas) {
            $linhas = $cdia_quinta;
        }
        if($cdia_sexta > $linhas) {
            $linhas = $cdia_sexta;
        }
        if($cdia_sabado > $linhas) {
            $linhas = $cdia_sabado;
        }
        if($cdia_domingo > $linhas) {
            $linhas = $cdia_domingo;
        }

        $linhas = $linhas * 7;

        for($i = 0; $i < $linhas; $i++) {
            $lista_calendario[] = 'vazio';
        }

        foreach ($calendario as $c) {
            if($c->dia == 0) {
                $lista_calendario[$segunda] = $c;
                $segunda += 7;
            }
            if($c->dia == 1) {
                $lista_calendario[$terca] = $c;
                $terca += 7;
            }
            if($c->dia == 2) {
                $lista_calendario[$quarta] = $c;
                $quarta += 7;
            }
            if($c->dia == 3) {
                $lista_calendario[$quinta] = $c;
                $quinta += 7;
            }
            if($c->dia == 4) {
                $lista_calendario[$sexta] = $c;
                $sexta += 7;
            }
            if($c->dia == 5) {
                $lista_calendario[$sabado] = $c;
                $sabado += 7;
            }
            if($c->dia == 6) {
                $lista_calendario[$domingo] = $c;
                $domingo += 7;
            }
        }

        $avaliacoes = $Avaliacoes
            ->find('all')
            ->contain('Clientes')
            ->where(['Avaliacoes.academia_id' => $academia->id])
            ->all();

        foreach ($avaliacoes as $avaliacao) {
            $estrelas += $avaliacao->rating;
        }

        $total_estrelas = $estrelas / count($avaliacoes);

        $academia_mensalidade = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $academia->id])
            ->first();

        $acad_mensalidade = $academia_mensalidade;

        $planos = $AcademiaMensalidadesPlanos
            ->find('all')
            ->where(['AcademiaMensalidadesPlanos.academia_mensalidades_id' => $academia_mensalidade->id])
            ->andWhere(['AcademiaMensalidadesPlanos.visivel' => 1])
            ->all();

        foreach ($planos as $plano) {
            $ids_produtos_planos[] = $plano->produto_id;
        }

        if(count($ids_produtos_planos) >= 1) {
            $produtos_planos = TableRegistry::get('Produtos')
                ->find('all')
                ->where(['estoque >=' => 1])
                ->andWhere(['id IN' => $ids_produtos_planos])
                ->all();
        }

        foreach ($planos as $plano) {
            foreach ($produtos_planos as $produto_plano) {
                if($plano->produto_id == $produto_plano->id) {
                    $ids_planos[] = $plano->id;
                }
            }
        }

        if(count($ids_planos) >= 1) {
            $planos = $AcademiaMensalidadesPlanos
                ->find('all')
                ->where(['academia_mensalidades_id' => $academia_mensalidade->id])
                ->andWhere(['visivel' => 1])
                ->andWhere(['id IN' => $ids_planos])
                ->all();
        }

        $planos_clientes = $PlanosClientes
            ->find('all')
            ->where(['status' => 1])
            ->all();

        $Cities = TableRegistry::get('Cities');
        
        $cidade_query = $Cities
            ->find('all')
            ->where(['id' => $academia->city_id])
            ->first();

        $cidade_acad = $cidade_query->name;
        $estado_acad = $cidade_query->uf;

        $this->set(compact('slug', 'academia', 'modalidades', 'diferenciais', 'acad_diferenciais', 'instrutores', 'galeria', 'lista_calendario', 'Cliente', 'avaliacoes', 'total_estrelas', 'Avaliacoes_anchor', 'cliente_acad', 'planos', 'planos_clientes', 'produtos', 'acad_mensalidade', 'cidade_acad', 'estado_acad'));
        $this->viewBuilder()->layout('profiles');
    }

     public function admin_profile_edit(){

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $Esportes             = TableRegistry::get('Esportes');
        $Avaliacoes           = TableRegistry::get('Avaliacoes');
        $Diferenciais         = TableRegistry::get('Diferenciais');
        $AcademiaEsportes     = TableRegistry::get('AcademiaEsportes');
        $AcademiaDiferenciais = TableRegistry::get('AcademiaDiferenciais');
        $AcademiaCalendario   = TableRegistry::get('AcademiaCalendario');
        $ProfessorAcademias   = TableRegistry::get('ProfessorAcademias');
        $AcademiaGaleria      = TableRegistry::get('AcademiaGaleria');
        $AcademiaMensalidades = TableRegistry::get('AcademiaMensalidades');

        $academia_mensalidade = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $academia->id])
            ->first();

        $acad_mensalidade = $academia_mensalidade;


        $academia = $this->Academias
            ->find('all')
            ->where(['id' => $Academia->id])
            ->first();

        if($this->request->is(['post', 'put'])){

            $ok = 0;

            if(empty($this->request->data['fechada-entre-semana']) || $this->request->data['horario_semana'] == '00:00 - 00:00') {
                $this->request->data['intervalo_semana'] = '00:00 - 00:00';
            }
            if(empty($this->request->data['fechada-entre-sabado']) || $this->request->data['horario_sabado'] == '00:00 - 00:00') {
                $this->request->data['intervalo_sabado'] = '00:00 - 00:00';
            }
            if(empty($this->request->data['fechada-entre-domingo']) || $this->request->data['horario_domingo'] == '00:00 - 00:00') {
                $this->request->data['intervalo_domingo'] = '00:00 - 00:00';
            }

            $academia = $this->Academias->patchEntity($academia, $this->request->data);

            if($this->Academias->save($academia)){

                foreach ($this->request->data['modalidade'] as $ko => $esporte) {
                    $esporte_data = [
                        'academia_id'   => $academia->id,
                        'esporte_id'    => $ko
                    ];

                    if($esporte){
                        if(!$AcademiaEsportes->exists($esporte_data)){
                            $EsporteSave = $AcademiaEsportes->newEntity();
                            $EsporteSave = $AcademiaEsportes->patchEntity($EsporteSave, $esporte_data);

                            $AcademiaEsportes->save($EsporteSave);
                        }
                    }else{
                        if($AcademiaEsportes->exists($esporte_data)){
                            $esporte_delete = $AcademiaEsportes
                                ->find()
                                ->where($esporte_data)
                                ->first();

                            $esporte_deleted = $AcademiaEsportes->delete($esporte_delete);
                        }
                    }
                }

                foreach ($this->request->data['diferencial'] as $ko => $diferencial) {
                    $diferencial_data = [
                        'academia_id'    => $academia->id,
                        'diferencial_id' => $ko
                    ];

                    if($diferencial){
                        if(!$AcademiaDiferenciais->exists($diferencial_data)){
                            $DiferencialSave = $AcademiaDiferenciais->newEntity();
                            $DiferencialSave = $AcademiaDiferenciais->patchEntity($DiferencialSave, $diferencial_data);

                            $AcademiaDiferenciais->save($DiferencialSave);
                        }
                    }else{
                        if($AcademiaDiferenciais->exists($diferencial_data)){
                            $diferencial_delete = $AcademiaDiferenciais
                                ->find()
                                ->where($diferencial_data)
                                ->first();

                            $diferencial_deleted = $AcademiaDiferenciais->delete($diferencial_delete);
                        }
                    }
                }

                $session->write('AdminAcademia', $academia);
                $ok = 1;
            }else{
                $ok = 2;
            }

            if($ok == 1) {
                $this->Flash->success('Perfil Atualizado com sucesso!');
                $this->redirect('/academia/admin/perfil/editar');
            } else if($ok == 2) {
                $this->Flash->error('Falha ao atualizar dados. Tente novamente...');
                $this->redirect('/academia/admin/perfil/editar');
            }
        }

        $modalidades = $AcademiaEsportes
            ->find('list')
            ->where(['academia_id' => $academia->id])
            ->all()
            ->toArray();

        $modalidades_list = $Esportes
            ->find('all')
            ->all();

        $diferenciais_acad = $AcademiaDiferenciais
            ->find('all')
            ->where(['academia_id' => $academia->id])
            ->all();

        if(!empty($diferenciais_acad)) {
            foreach ($diferenciais_acad as $da) {
                $diferenciais[] = $da->diferencial_id;
            }
        } else {
            $diferenciais = [];
        }

        $diferenciais_list = $Diferenciais
            ->find('all')
            ->all();

        $instrutores = $ProfessorAcademias
            ->find('all')
            ->contain(['Professores'])
            ->where(['ProfessorAcademias.academia_id' => $academia->id])
            ->andWhere(['ProfessorAcademias.status_id' => 1])
            ->all();

        $galeria = $AcademiaGaleria
            ->find('all')
            ->where(['academia_id' => $academia->id])
            ->andWhere(['status' => 1])
            ->all();

        $calendario = $AcademiaCalendario
            ->find('all')
            ->where(['academia_id' => $academia->id])
            ->order(['horario_inicio' => 'asc'])
            ->all();

        $segunda = 0;
        $terca   = 1;
        $quarta  = 2;
        $quinta  = 3;
        $sexta   = 4;
        $sabado  = 5;
        $domingo = 6;

        $cdia_segunda = 0;
        $cdia_terca   = 0;
        $cdia_quarta  = 0;
        $cdia_quinta  = 0;
        $cdia_sexta   = 0;
        $cdia_sabado  = 0;
        $cdia_domingo = 0;

        foreach ($calendario as $cdias) {
            if($cdias->dia == 0) {
                $cdia_segunda++;
            }
            if($cdias->dia == 1) {
                $cdia_terca++;
            }
            if($cdias->dia == 2) {
                $cdia_quarta++;
            }
            if($cdias->dia == 3) {
                $cdia_quinta++;
            }
            if($cdias->dia == 4) {
                $cdia_sexta++;
            }
            if($cdias->dia == 5) {
                $cdia_sabado++;
            }
            if($cdias->dia == 6) {
                $cdia_domingo++;
            }
        }

        $linhas = $cdia_segunda;

        if($cdia_terca > $linhas) {
            $linhas = $cdia_terca;
        }
        if($cdia_quarta > $linhas) {
            $linhas = $cdia_quarta;
        }
        if($cdia_quinta > $linhas) {
            $linhas = $cdia_quinta;
        }
        if($cdia_sexta > $linhas) {
            $linhas = $cdia_sexta;
        }
        if($cdia_sexta > $linhas) {
            $linhas = $cdia_sexta;
        }
        if($cdia_domingo > $linhas) {
            $linhas = $cdia_domingo;
        }

        $linhas = $linhas * 7;

        for($i = 0; $i < $linhas; $i++) {
            $lista_calendario[] = 'vazio';
        }

        foreach ($calendario as $c) {
            if($c->dia == 0) {
                $lista_calendario[$segunda] = $c;
                $segunda += 7;
            }
            if($c->dia == 1) {
                $lista_calendario[$terca] = $c;
                $terca += 7;
            }
            if($c->dia == 2) {
                $lista_calendario[$quarta] = $c;
                $quarta += 7;
            }
            if($c->dia == 3) {
                $lista_calendario[$quinta] = $c;
                $quinta += 7;
            }
            if($c->dia == 4) {
                $lista_calendario[$sexta] = $c;
                $sexta += 7;
            }
            if($c->dia == 5) {
                $lista_calendario[$sabado] = $c;
                $sabado += 7;
            }
            if($c->dia == 6) {
                $lista_calendario[$domingo] = $c;
                $domingo += 7;
            }
        }

        $avaliacoes = $Avaliacoes
            ->find('all')
            ->contain('Clientes')
            ->where(['Avaliacoes.academia_id' => $academia->id])
            ->all();

        foreach ($avaliacoes as $avaliacao) {
            $estrelas += $avaliacao->rating;
        }

        $total_estrelas = $estrelas / count($avaliacoes);
        
        $this->set(compact('academia', 'modalidades', 'modalidades_list', 'instrutores', 'diferenciais', 'diferenciais_list', 'galeria', 'lista_calendario', 'avaliacoes', 'total_estrelas'));
        $this->viewBuilder()->layout('profiles');
    }

    public function admin_mensalidades(){
        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $AcademiaMensalidades = TableRegistry::get('AcademiaMensalidades');

        $academiamensalidade = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->first();

        if($academiamensalidade) {
            $this->Flash->error('Sua academia já possui LOGMensalidades');
            return $this->redirect('/academia/admin/admin_mensalidades/config');
        }

        if($this->request->is(['patch', 'post', 'put'])) {

            $newLogMensalidade = $AcademiaMensalidades->newEntity();

            $data = [
                'academia_id'                => $Academia->id,
                'opt_conta_cpf'              => $this->request->data['opt_conta_cpf'],
                'email_notificacoes'         => $Academia->email,
                'mensalidades_categorias_id' => 1
            ];

            $newLogMensalidade = $AcademiaMensalidades->patchEntity($newLogMensalidade, $data);

            if ($AcademiaMensalidades->save($newLogMensalidade)) {
                $this->iugu_criar_subconta($Academia->id);
            } else {
                $this->Flash->error('Falha ao adquirir o LOGMensalidades... Tente novamente...');
                return $this->redirect('/academia/admin/admin_mensalidades');
            }
        }

        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_mensalidades_config(){
        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $academia = $this->Academias
            ->find('all')
            ->where(['id' => $Academia->id])
            ->first();
            
        $AcademiaMensalidades   = TableRegistry::get('AcademiaMensalidades');
        $MensalidadesCategorias = TableRegistry::get('MensalidadesCategorias');

        $academiamensalidade = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $academia->id])
            ->first();

        if(!$academiamensalidade) {
            $this->Flash->error('Sua academia ainda não possui LOGMensalidades');
            return $this->redirect('/academia/admin/admin_mensalidades');
        }

        $mensalidade_categoria = $MensalidadesCategorias
            ->find('all')
            ->where(['id' => $academiamensalidade->mensalidades_categorias_id])
            ->first();

        $ch = curl_init();

        $header = [
            "Content-Type: application/json"
        ];

        $options = array(CURLOPT_URL => "https://api.iugu.com/v1/bank_verification?api_token=".$academiamensalidade->iugu_live_api_token."&hl=pt-BR",
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => FALSE,
            CURLOPT_HTTPHEADER => $header
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $resposta_verificao_subconta = json_decode($response);

        if($resposta_verificao_subconta[0]->status == 'pending') {
            $this->Flash->info('Seus dados bancários estão em verificação!');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            if($this->request->data['imagem_comprovante']['size'] == 0 && $this->request->data['imagem_comprovante']['error'] == 1) {
                $this->Flash->error('Por favor, coloque uma imagem de até 2mb');
                $this->redirect('/academia/admin/admin_mensalidades/config');
            } else {
                $academia = $this->Academias->patchEntity($academia, $this->request->data);
                if ($academia_save = $this->Academias->save($academia)) {
                    if($academiamensalidade->account_verify == 0) {
                        if(!$academia_save->mens_name || !$academia_save->mens_cpf) {
                            $this->Flash->success('Agora preencha o nome do responsável e CPF!');
                            $this->redirect('/academia/admin/admin_mensalidades/config');
                        } else {
                            $this->iugu_verificar_subconta($academia_save->id);
                        }
                    } else if ($academiamensalidade->account_verify == 1) {
                        $this->iugu_editar_dados_bancarios_subconta($academia_save->id);
                    }
                } else {
                    $this->Flash->error('Falha ao atualizar dados... Tente novamente...');
                    return $this->redirect('/academia/admin/admin_mensalidades/config');
                }
            }
            
        }

        $this->set(compact('academia', 'academiamensalidade', 'mensalidade_categoria'));
        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_mensalidades_assinaturas(){
        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        if($Academia->account_verify == 0) {
            if(!$Academia->mens_name || !$Academia->mens_cpf) {
                $this->Flash->error('Precisamos do nome do responsável e CPF!');
                $this->redirect('/academia/admin/admin_mensalidades/config');
            } else if(!$Academia->mens_tipo_conta || !$Academia->mens_banco || !$Academia->mens_agencia || 
                        !$Academia->mens_conta) {
                $this->Flash->error('Precisamos dos seus dados bancários!');
                $this->redirect('/academia/admin/admin_mensalidades/config');
            }
        }

        $PlanosClientes             = TableRegistry::get('PlanosClientes');
        $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
        $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

        $academia_mensalidades = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->first();

        $academia_mensalidades_planos = $AcademiaMensalidadesPlanos
            ->find('all')
            ->where(['academia_mensalidades_id' => $academia_mensalidades->id])
            ->andWhere(['type' => 2])
            ->all();

        $academia_mensalidades_planos_ativos = $AcademiaMensalidadesPlanos
            ->find('all')
            ->where(['academia_mensalidades_id' => $academia_mensalidades->id])
            ->andWhere(['type' => 2])
            ->andWhere(['visivel' => 1])
            ->all();

        $academia_mensalidades_planos_inativos = $AcademiaMensalidadesPlanos
            ->find('all')
            ->where(['academia_mensalidades_id' => $academia_mensalidades->id])
            ->andWhere(['type' => 2])
            ->andWhere(['visivel' => 0])
            ->all();

        foreach ($academia_mensalidades_planos as $amp) {
            $planos_id[] = $amp->id;
        }

        if(count($planos_id) >= 1) {

            $today_ativos = time::now();
            $today_ativos->addDays(5);

            $today = time::now();

            $planos_clientes_ativos = $PlanosClientes
                ->find('all')
                ->contain(['AcademiaMensalidadesPlanos', 'Clientes'])
                ->where(['PlanosClientes.academia_mensalidades_planos_id IN' => $planos_id])
                ->andWhere(['PlanosClientes.data_vencimento > ' => $today_ativos])
                ->andWhere(['PlanosClientes.status' => 1])
                ->all();

            $planos_clientes_vencer = $PlanosClientes
                ->find('all')
                ->contain(['AcademiaMensalidadesPlanos', 'Clientes'])
                ->where(['PlanosClientes.academia_mensalidades_planos_id IN' => $planos_id])
                ->andWhere(['PlanosClientes.data_vencimento >= ' => $today])
                ->andWhere(['PlanosClientes.data_vencimento <= ' => $today_ativos])
                ->andWhere(['PlanosClientes.status' => 1])
                ->all();

            $planos_clientes_vencidos = $PlanosClientes
                ->find('all')
                ->contain(['AcademiaMensalidadesPlanos', 'Clientes'])
                ->where(['PlanosClientes.academia_mensalidades_planos_id IN' => $planos_id])
                ->andWhere(['PlanosClientes.data_vencimento < ' => $today])
                ->andWhere(['PlanosClientes.status' => 1])
                ->all();
        } else {
            $planos_clientes_ativos   = [];
            $planos_clientes_vencer   = [];
            $planos_clientes_vencidos = [];
        }

        foreach ($planos_clientes_ativos as $plano_cliente_ativo) {
            $qtd_alunos_ativos[$plano_cliente_ativo->academia_mensalidades_planos_id] += 1; 
        }

        foreach ($planos_clientes_vencer as $plano_cliente_vencer) {
            $qtd_alunos_vencer[$plano_cliente_vencer->academia_mensalidades_planos_id] += 1; 
        }

        foreach ($planos_clientes_vencidos as $plano_cliente_vencidos) {
            $qtd_alunos_vencidos[$plano_cliente_vencidos->academia_mensalidades_planos_id] += 1; 
        }

        $this->set(compact('academia_mensalidades_planos_ativos', 'academia_mensalidades_planos_inativos', 'planos_clientes_ativos', 'planos_clientes_vencer', 'planos_clientes_vencidos', 'qtd_alunos_ativos', 'qtd_alunos_vencer', 'qtd_alunos_vencidos'));
        $this->viewBuilder()->layout('admin_academia');
    }


    public function admin_mensalidades_planos(){
        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        if($Academia->account_verify == 0) {
            if(!$Academia->mens_name || !$Academia->mens_cpf) {
                $this->Flash->error('Precisamos do nome do responsável e CPF!');
                $this->redirect('/academia/admin/admin_mensalidades/config');
            } else if(!$Academia->mens_tipo_conta || !$Academia->mens_banco || !$Academia->mens_agencia || 
                        !$Academia->mens_conta) {
                $this->Flash->error('Precisamos dos seus dados bancários!');
                $this->redirect('/academia/admin/admin_mensalidades/config');
            }
        }

        $Pedidos                    = TableRegistry::get('Pedidos');
        $PlanosClientes             = TableRegistry::get('PlanosClientes');
        $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
        $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

        $pedidos = $Pedidos
            ->find('all')
            ->where(['comissao_status IN' => [4, 5]])
            ->all();

        $academia_mensalidades = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->first();

        $academia_mensalidades_planos = $AcademiaMensalidadesPlanos
            ->find('all')
            ->where(['academia_mensalidades_id' => $academia_mensalidades->id])
            ->andWhere(['type' => 1])
            ->all();

        foreach ($academia_mensalidades_planos as $amp) {
            $planos_id[] = $amp->id;
        }

        if(count($planos_id) >= 1) {

            $today_ativos = time::now();
            $today_ativos->addDays(5);

            $today = time::now();

            $planos_clientes_ativos = $PlanosClientes
                ->find('all')
                ->contain(['AcademiaMensalidadesPlanos', 'Clientes'])
                ->where(['PlanosClientes.academia_mensalidades_planos_id IN' => $planos_id])
                ->andWhere(['PlanosClientes.data_vencimento > ' => $today_ativos])
                ->andWhere(['PlanosClientes.status' => 1])
                ->all();

            $planos_clientes_vencer = $PlanosClientes
                ->find('all')
                ->contain(['AcademiaMensalidadesPlanos', 'Clientes'])
                ->where(['PlanosClientes.academia_mensalidades_planos_id IN' => $planos_id])
                ->andWhere(['PlanosClientes.data_vencimento >= ' => $today])
                ->andWhere(['PlanosClientes.data_vencimento <= ' => $today_ativos])
                ->andWhere(['PlanosClientes.status' => 1])
                ->all();

            $planos_clientes_vencidos = $PlanosClientes
                ->find('all')
                ->contain(['AcademiaMensalidadesPlanos', 'Clientes'])
                ->where(['PlanosClientes.academia_mensalidades_planos_id IN' => $planos_id])
                ->andWhere(['PlanosClientes.data_vencimento < ' => $today])
                ->andWhere(['PlanosClientes.status' => 1])
                ->all();
        } else {
            $planos_clientes_ativos   = [];
            $planos_clientes_vencer   = [];
            $planos_clientes_vencidos = [];
        }

        foreach ($planos_clientes_ativos as $plano_cliente_ativo) {
            $qtd_alunos_ativos[$plano_cliente_ativo->academia_mensalidades_planos_id] += 1; 
        }

        foreach ($planos_clientes_vencer as $plano_cliente_vencer) {
            $qtd_alunos_vencer[$plano_cliente_vencer->academia_mensalidades_planos_id] += 1; 
        }

        foreach ($planos_clientes_vencidos as $plano_cliente_vencidos) {
            $qtd_alunos_vencidos[$plano_cliente_vencidos->academia_mensalidades_planos_id] += 1; 
        }

        $pedidos_personalizados = $Pedidos
            ->find('all')
            ->contain(['Clientes'])
            ->where(['Pedidos.academia_id' => $Academia->id])
            ->andWhere(['Pedidos.pedido_status_id >=' => 3])
            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
            ->andWhere(['Pedidos.comissao_status' => 5])
            ->all();

        $pedidos_aguardando = $Pedidos
            ->find('all')
            ->contain(['Clientes'])
            ->where(['Pedidos.academia_id' => $Academia->id])
            ->andWhere(['Pedidos.pedido_status_id' => 2])
            ->andWhere(['Pedidos.comissao_status IN' => [4, 5]])
            ->order(['Pedidos.created' => 'desc'])
            ->all();

        $this->set(compact('academia_mensalidades_planos', 'planos_clientes_ativos', 'planos_clientes_vencer', 'planos_clientes_vencidos', 'qtd_alunos_ativos', 'qtd_alunos_vencer', 'qtd_alunos_vencidos', 'pedidos_personalizados', 'pedidos', 'pedidos_aguardando'));
        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_mensalidades_planos_enviar_boleto($id){
        if($id != null) {

            $session  = $this->request->session();
            $Academia = $session->read('AdminAcademia');

            $pedido = TableRegistry::get('Pedidos')
                ->find('all')
                ->contain(['Clientes', 'Academias'])
                ->where(['Pedidos.id' => $id])
                ->andWhere(['Pedidos.academia_id' => $Academia->id])
                ->first();

            Email::configTransport('cliente', [
                'className' => 'Smtp',
                'host' => 'mail.logfitness.com.br',
                'port' => 587,
                'timeout' => 30,
                'username' => 'uhull@logfitness.com.br',
                'password' => 'Lvrt6174?',
                'client' => null,
                'tls' => null,
            ]);

            $email = new Email();

            $email
                ->transport('cliente')
                ->template('boleto_plano')
                ->emailFormat('html')
                ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                ->to($pedido->cliente->email)
                ->subject($Academia->shortname.' - LogFitness - Boleto')
                ->set([
                    'name'              => $pedido->cliente->name,
                    'academia'          => $pedido->academia->shortname,
                    'logo_academia'     => $pedido->academia->image,
                    'boleto'            => $pedido->iugu_payment_pdf
                ])
                ->send();

            return $this->redirect('/academia/admin/admin_mensalidades/extrato');
        }

        return $this->redirect('/academia/admin/admin_mensalidades/extrato');
    }

    public function admin_mensalidades_planos_add(){

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');
            $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
            $Produtos                   = TableRegistry::get('Produtos');

            $produto = $Produtos->newEntity();

            $success = false;

            $data = [
                'cod' => '999999989',
                'cod_interno' => date('YmdHis'),
                'produto_base_id' => 842,
                'custo' => 0.00,
                'porcentagem' => 0.00,
                'preco' => $this->request->data['valor_total'],
                'preco_promo' => 0.00,
                'fotos' => 'a:1:{i:0;s:21:"produto-express-0.png";}',
                'estoque' => $this->request->data['limite_vendas'] >= 1 ? $this->request->data['limite_vendas'] : 9999,
                'estoque_min' => 1,
                'visivel' => 0,
                'status_id' => 1,
                'type' => 2,
                'slug' => $this->request->data['name'].date('YmdHis')
            ];

            

            $produto = $Produtos->patchEntity($produto, $data);

            if ($produto_save = $Produtos->save($produto)) {
                $success = true;
            }

            if($success) {
                $academiamensalidade = $AcademiaMensalidades
                    ->find('all')
                    ->where(['academia_id' => $Academia->id])
                    ->first();

                $academiamensalidadeplanos = $AcademiaMensalidadesPlanos->newEntity();

                $this->request->data['academia_mensalidades_id'] = $academiamensalidade->id;
                $this->request->data['produto_id'] = $produto_save->id;

                if($this->request->data['opt_limite_vendas'] != 1) {
                    $this->request->data['limite_vendas'] = 9999;
                }

                $academiamensalidadeplanos = $AcademiaMensalidadesPlanos->patchEntity($academiamensalidadeplanos, $this->request->data);

                if ($novo_plano = $AcademiaMensalidadesPlanos->save($academiamensalidadeplanos)) {
                    $this->iugu_criar_plano($novo_plano->id);
                } else {
                    $this->Flash->error('Falha ao adicionar plano... tente novamente...');
                    return $this->redirect('/academia/admin/admin_mensalidades/planos_add');
                }
            } else {
                $this->Flash->error('Falha ao adicionar plano... tente novamente...');
                return $this->redirect('/academia/admin/admin_mensalidades/planos_add');
            }
        }

        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_mensalidades_planos_editar_vencimento(){

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $PlanosClientes = TableRegistry::get('PlanosClientes');
                
            $planos_clientes = $PlanosClientes
                ->find('all')
                ->where(['id' => $this->request->data['plano_id']])
                ->first();

            $nova_data = Time::now();
            $data_vencimento = explode('-', $this->request->data['data_vencimento']);
            $nova_data->setDate($data_vencimento[0], $data_vencimento[1], $data_vencimento[2]);

            $data = ['data_vencimento' => $nova_data];

            $planos_clientes = $PlanosClientes->patchEntity($planos_clientes, $data);

            if ($plano_cliente_saved = $PlanosClientes->save($planos_clientes)) {
                $this->iugu_alterar_vencimento_assinatura($plano_cliente_saved->iugu_assinatura_id, $this->request->data['data_vencimento']);
            } else {
                $this->Flash->error('Falha ao alterar data de vencimento... tente novamente...');
                return $this->redirect('/academia/admin/admin_mensalidades/assinaturas');
            }
        }

        return $this->redirect('/academia/admin/admin_mensalidades/planos');
        $this->render(false);
    }

    public function admin_mensalidades_planos_edit($id){

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $AcademiaMensalidades = TableRegistry::get('AcademiaMensalidades');
        $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

        $academia_mensalidade = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->first();

        $academia_mensalidades_planos = $AcademiaMensalidadesPlanos
            ->find('all')
            ->where(['id' => $id])
            ->andWhere(['academia_mensalidades_id' => $academia_mensalidade->id])
            ->first();

        if(!$academia_mensalidades_planos) {
            $this->Flash->error('Você não pode editar esse plano!');
            return $this->redirect('/academia/admin/admin_mensalidades/planos');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            if($this->request->data['opt_limite_vendas'] != 1) {
                $this->request->data['limite_vendas'] = 9999;
            }

            $academia_mensalidades_planos = $AcademiaMensalidadesPlanos->patchEntity($academia_mensalidades_planos, $this->request->data);

            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $valor = number_format($valor = $academia_mensalidades_planos->valor_total/$academia_mensalidades_planos->time_months, 2, '', ' ');

            $data = [
                "api_token"     => IUGU_API_TOKEN,
                "value_cents"   => $valor
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/plans/".$academia_mensalidades_planos->iugu_plano_id,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_iugu_edit_plan = json_decode($response);

            if ($AcademiaMensalidadesPlanos->save($academia_mensalidades_planos) && !$resp_iugu_edit_plan->errors) {
                $this->Flash->success('Plano alterado com sucesso!');
                return $this->redirect('/academia/admin/admin_mensalidades/planos');
            } else {
                $this->Flash->error('Falha ao alterar plano... tente novamente...');
                return $this->redirect('/academia/admin/admin_mensalidades/planos');
            }
        }

        $this->set(compact('academia_mensalidades_planos'));
        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_mensalidades_assinaturas_edit($id){

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $AcademiaMensalidades = TableRegistry::get('AcademiaMensalidades');
        $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

        $academia_mensalidade = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->first();

        $academia_mensalidades_planos = $AcademiaMensalidadesPlanos
            ->find('all')
            ->where(['id' => $id])
            ->andWhere(['academia_mensalidades_id' => $academia_mensalidade->id])
            ->first();

        if(!$academia_mensalidades_planos) {
            $this->Flash->error('Você não pode editar essa assinatura!');
            return $this->redirect('/academia/admin/admin_mensalidades/assinaturas');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            if($this->request->data['opt_limite_vendas'] != 1) {
                $this->request->data['limite_vendas'] = 9999;
            }

            $academia_mensalidades_planos = $AcademiaMensalidadesPlanos->patchEntity($academia_mensalidades_planos, $this->request->data);

            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $valor = number_format($valor = $academia_mensalidades_planos->valor_total/$academia_mensalidades_planos->time_months, 2, '', ' ');

            $data = [
                "api_token"     => IUGU_API_TOKEN,
                "value_cents"   => $valor
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/plans/".$academia_mensalidades_planos->iugu_plano_id,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_iugu_edit_plan = json_decode($response);

            if ($AcademiaMensalidadesPlanos->save($academia_mensalidades_planos) && !$resp_iugu_edit_plan->errors) {
                $this->Flash->success('Plano alterado com sucesso!');
                return $this->redirect('/academia/admin/admin_mensalidades/assinaturas');
            } else {
                $this->Flash->error('Falha ao alterar plano... tente novamente...');
                return $this->redirect('/academia/admin/admin_mensalidades/assinaturas');
            }
        }

        $this->set(compact('academia_mensalidades_planos'));
        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_mensalidades_planos_delete($id){

        if($id != null) {
            $session  = $this->request->session();
            $Academia = $session->read('AdminAcademia');

            if(!$Academia) {
                $this->redirect('/academia/acesso');
            }

            $AcademiaMensalidades = TableRegistry::get('AcademiaMensalidades');
            $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

            $academia_mensalidade = $AcademiaMensalidades
                ->find('all')
                ->where(['academia_id' => $Academia->id])
                ->first();

            $academia_mensalidades_planos = $AcademiaMensalidadesPlanos
                ->find('all')
                ->where(['id' => $id])
                ->andWhere(['academia_mensalidades_id' => $academia_mensalidade->id])
                ->first();

            $type = $academia_mensalidades_planos->type;

            if(!$academia_mensalidades_planos) {
                if ($type == 1) {
                    $this->Flash->error('Você não pode ativar/inativar esse plano!');
                    return $this->redirect('/academia/admin/admin_mensalidades/planos');
                } else {
                    $this->Flash->error('Você não pode ativar/inativar essa assinatura!');
                    return $this->redirect('/academia/admin/admin_mensalidades/assinaturas');
                }
            }

            if($academia_mensalidades_planos->visivel == 1) {
                $visivel = 0;
            } else {
                $visivel = 1;
            }

            $data = [
                'visivel' => $visivel
            ];

            $academia_mensalidades_planos = $AcademiaMensalidadesPlanos->patchEntity($academia_mensalidades_planos, $data);

            if ($AcademiaMensalidadesPlanos->save($academia_mensalidades_planos)) {
                if($type == 1) {
                    $this->Flash->success('Plano ativado/inativado com sucesso!');
                    return $this->redirect('/academia/admin/admin_mensalidades/planos');
                } else {
                    $this->Flash->success('Assinatura ativada/inativada com sucesso!');
                    return $this->redirect('/academia/admin/admin_mensalidades/assinaturas');
                }
            } else {
                if($type == 1) {
                    $this->Flash->error('Falha ao ativar/inativar plano... tente novamente...');
                    return $this->redirect('/academia/admin/admin_mensalidades/planos');
                } else {
                    $this->Flash->error('Falha ao ativar/inativar assinatura... tente novamente...');
                    return $this->redirect('/academia/admin/admin_mensalidades/assinaturas');
                }
            }
        }

        return $this->redirect('/academia/admin/admin_mensalidades/planos');
        $this->viewBuilder()->layout('ajax');
        $this->render(false);
    }

    public function galeria_add() {

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $AcademiaGaleria = TableRegistry::get('AcademiaGaleria');

            $galeria = $AcademiaGaleria->newEntity();

            if($this->request->data['imagem']['size'] == 0 && $this->request->data['imagem']['error'] == 1) {
                $this->Flash->error('Por favor, coloque uma imagem de até 2mb');
                $this->redirect('/academia/admin/perfil/editar');
            } else {
                $this->request->data['academia_id'] = $Academia->id;
                $this->request->data['status'] = 1;
                $galeria = $AcademiaGaleria->patchEntity($galeria, $this->request->data);
                if ($AcademiaGaleria->save($galeria)) {
                    $this->Flash->success('Nova foto adicionada a galeria!');
                    return $this->redirect('/academia/admin/perfil/editar');
                } else {
                    $this->Flash->error('Falha ao salvar nova foto na galeria. Tente novamente...');
                }
            }
        }

        $this->layout = 'ajax'; 
        $this->render(false);
    }

    public function galeria_remove($id) {

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $AcademiaGaleria = TableRegistry::get('AcademiaGaleria');

        $imagem = $AcademiaGaleria
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->andWhere(['id' => $id])
            ->first();

        if($imagem) {
            $this->request->data['status'] = 2;
            $imagem = $AcademiaGaleria->patchEntity($imagem, $this->request->data);
            if ($AcademiaGaleria->save($imagem)) {
                $this->Flash->success('Foto excluída da galeria!');
                return $this->redirect('/academia/admin/perfil/editar');
            } else {
                $this->Flash->error('Falha ao exluir foto da galeria. Tente novamente...');
            }
        } else {
            $this->Flash->error('Você não pode apagar essa imagem!');
            $this->redirect('/academia/admin/perfil/editar');
        }

        $this->layout = 'ajax'; 
        $this->render(false);
    }

    public function calendario_add() {

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $AcademiaCalendario = TableRegistry::get('AcademiaCalendario');

            $calendario = $AcademiaCalendario->newEntity();

            $this->request->data['academia_id'] = $Academia->id;

            $calendario = $AcademiaCalendario->patchEntity($calendario, $this->request->data);
            if ($AcademiaCalendario->save($calendario)) {
                echo 'Sucesso';
            } else {
                echo 'Falha';
            }
        }

        $calendario = $AcademiaCalendario
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->order(['horario_inicio' => 'asc'])
            ->all();

        $segunda = 0;
        $terca   = 1;
        $quarta  = 2;
        $quinta  = 3;
        $sexta   = 4;
        $sabado  = 5;
        $domingo = 6;

        $cdia_segunda = 0;
        $cdia_terca   = 0;
        $cdia_quarta  = 0;
        $cdia_quinta  = 0;
        $cdia_sexta   = 0;
        $cdia_sabado  = 0;
        $cdia_domingo = 0;

        foreach ($calendario as $cdias) {
            if($cdias->dia == 0) {
                $cdia_segunda++;
            }
            if($cdias->dia == 1) {
                $cdia_terca++;
            }
            if($cdias->dia == 2) {
                $cdia_quarta++;
            }
            if($cdias->dia == 3) {
                $cdia_quinta++;
            }
            if($cdias->dia == 4) {
                $cdia_sexta++;
            }
            if($cdias->dia == 5) {
                $cdia_sabado++;
            }
            if($cdias->dia == 6) {
                $cdia_domingo++;
            }
        }

        $linhas = $cdia_segunda;

        if($cdia_terca > $linhas) {
            $linhas = $cdia_terca;
        }
        if($cdia_quarta > $linhas) {
            $linhas = $cdia_quarta;
        }
        if($cdia_quinta > $linhas) {
            $linhas = $cdia_quinta;
        }
        if($cdia_sexta > $linhas) {
            $linhas = $cdia_sexta;
        }
        if($cdia_sexta > $linhas) {
            $linhas = $cdia_sexta;
        }
        if($cdia_domingo > $linhas) {
            $linhas = $cdia_domingo;
        }

        $linhas = $linhas * 7;

        for($i = 0; $i < $linhas; $i++) {
            $lista_calendario[] = 'vazio';
        }

        foreach ($calendario as $c) {
            if($c->dia == 0) {
                $lista_calendario[$segunda] = $c;
                $segunda += 7;
            }
            if($c->dia == 1) {
                $lista_calendario[$terca] = $c;
                $terca += 7;
            }
            if($c->dia == 2) {
                $lista_calendario[$quarta] = $c;
                $quarta += 7;
            }
            if($c->dia == 3) {
                $lista_calendario[$quinta] = $c;
                $quinta += 7;
            }
            if($c->dia == 4) {
                $lista_calendario[$sexta] = $c;
                $sexta += 7;
            }
            if($c->dia == 5) {
                $lista_calendario[$sabado] = $c;
                $sabado += 7;
            }
            if($c->dia == 6) {
                $lista_calendario[$domingo] = $c;
                $domingo += 7;
            }
        }

        $this->set(compact('lista_calendario'));
        $this->viewBuilder()->layout('ajax');
    }

    public function calendario_remove($id) {

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $AcademiaCalendario = TableRegistry::get('AcademiaCalendario');

        $horario = $AcademiaCalendario
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->andWhere(['id' => $id])
            ->first();

        if($horario) {
            if ($AcademiaCalendario->delete($horario)) {
                $this->Flash->success('Horário excluído do calendário!');
                return $this->redirect('/academia/admin/perfil/editar');
            } else {
                $this->Flash->error('Falha ao exluir horário do calendário. Tente novamente...');
            }
        } else {
            $this->Flash->error('Você não pode apagar esse horário!');
            $this->redirect('/academia/admin/perfil/editar');
        }

        $this->layout = 'ajax'; 
        $this->render(false);
    }

    public function reportar_avaliacao() {

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $Avaliacoes = TableRegistry::get('Avaliacoes');
        $AvaliacoesReportadas = TableRegistry::get('AvaliacoesReportadas');

        $avaliacao = $Avaliacoes
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->andWhere(['id' => $this->request->data['avaliacao_id']])
            ->first();

        if($avaliacao) {
            $avaliacao_reportada = $AvaliacoesReportadas->newEntity();

            $avaliacao_reportada = $AvaliacoesReportadas->patchEntity($avaliacao_reportada, $this->request->data);

            if ($AvaliacoesReportadas->save($avaliacao_reportada)) {
                $this->Flash->success('Avaliação Reportada com sucesso!');
                return $this->redirect('/academia/admin/perfil/editar');
            } else {
                $this->Flash->error('Falha ao reportar avaliação. Tente novamente...');
                return $this->redirect('/academia/admin/perfil/editar');
            }
        } else {
            $this->Flash->error('Você não pode reportar essa avaliação.');
            return $this->redirect('/academia/admin/perfil/editar');
        }

    }

    public function comprar_pacote($id){

        if($id != null) {
            $session  = $this->request->session();
            $Cliente  = $session->read('Cliente');

            if(!$Cliente) {
                $this->redirect('/login');
            }

            $Academias                  = TableRegistry::get('Academias');
            $Avaliacoes                 = TableRegistry::get('Avaliacoes');
            $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
            $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

            $academia = $Academias
                ->find('all')
                ->contain(['Cities'])
                ->where(['Academias.id' => $Cliente->academia_id])
                ->where(['Academias.status_id' => 1])
                ->first();

            $plano = $AcademiaMensalidadesPlanos
                ->find('all')
                ->contain(['Produtos'])
                ->where(['AcademiaMensalidadesPlanos.id' => $id])
                ->andWhere(['AcademiaMensalidadesPlanos.visivel' => 1])
                ->andWhere(['Produtos.estoque >=' => 1])
                ->first();

            if(!$plano) {
                $this->Flash->error('Esse plano acabou!');
                return $this->redirect('/perfil');
            }

            $academia_mensalidades = $AcademiaMensalidades
                ->find('all')
                ->where(['id' => $plano->academia_mensalidades_id])
                ->first();

            if($academia_mensalidades->academia_id != $Cliente->academia_id) {
                $this->Flash->error('Você não pode comprar um pacote de outra academia!');
                $this->redirect('/perfil');
            }

            $avaliacoes = $Avaliacoes
                ->find('all')
                ->contain('Clientes')
                ->where(['Avaliacoes.academia_id' => $academia->id])
                ->all();

            foreach ($avaliacoes as $avaliacao) {
                $estrelas += $avaliacao->rating;
            }

            $total_estrelas = $estrelas / count($avaliacoes);

            $this->set(compact('academia', 'plano', 'academia_mensalidades', 'total_estrelas'));
            $this->viewBuilder()->layout('profiles');
        } else {
            $this->redirect('/perfil');
        }
    }

    public function mensalidade_comprar($id){
        $session  = $this->request->session();
        $Cliente  = $session->read('Cliente');
        $AcademiaMensalidade = $session->read('AcademiaMensalidade');

        if($id != null) {

            if($id == 99522) {
                $valor = $_GET['valor'];

                $session  = $this->request->session();
                $Academia = $session->read('Academia');

                if(!$Academia) {
                    $this->redirect('/home');
                }

                $Academias                  = TableRegistry::get('Academias');
                $Avaliacoes                 = TableRegistry::get('Avaliacoes');
                $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');

                if ($AcademiaMensalidade) {
                    $academia = $this->Academias
                        ->find('all')
                        ->contain(['Cities'])
                        ->where(['Academias.id' => $AcademiaMensalidade->id])
                        ->where(['Academias.status_id' => 1])
                        ->first();
                } else {
                    $academia = $this->Academias
                        ->find('all')
                        ->contain(['Cities'])
                        ->where(['Academias.id' => $Academia->id])
                        ->where(['Academias.status_id' => 1])
                        ->first();
                }

                $academia_mensalidades = $AcademiaMensalidades
                    ->find('all')
                    ->where(['academia_id' => $academia->id])
                    ->first();

                $this->set(compact('valor'));
            } else {
                $session  = $this->request->session();
                $Academia = $session->read('Academia');

                $Academias                  = TableRegistry::get('Academias');
                $Avaliacoes                 = TableRegistry::get('Avaliacoes');
                $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
                $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

                $plano = $AcademiaMensalidadesPlanos
                    ->find('all')
                    ->contain(['Produtos'])
                    ->where(['AcademiaMensalidadesPlanos.id' => $id])
                    ->andWhere(['AcademiaMensalidadesPlanos.visivel' => 1])
                    ->andWhere(['Produtos.estoque >=' => 1])
                    ->first();

                $academia_mensalidades = $AcademiaMensalidades
                    ->find('all')
                    ->where(['id' => $plano->academia_mensalidades_id])
                    ->first();

                $academia = $this->Academias
                    ->find('all')
                    ->contain(['Cities', 'Cities.Transportadoras'])
                    ->where(['Academias.id' => $academia_mensalidades->academia_id])
                    ->where(['Academias.status_id' => 1])
                    ->first();

                if(!$Academia) {
                    $session->write('Academia', $academia);

                    $Academia = $session->read('Academia');
                }

                if(!$plano) {
                    $this->Flash->error('Esse plano acabou!');
                    return $this->redirect('/perfil');
                }

            }

            $avaliacoes = $Avaliacoes
                ->find('all')
                ->contain('Clientes')
                ->where(['Avaliacoes.academia_id' => $academia->id])
                ->all();

            foreach ($avaliacoes as $avaliacao) {
                $estrelas += $avaliacao->rating;
            }

            $total_estrelas = $estrelas / count($avaliacoes);

            $this->set(compact('academia', 'plano', 'academia_mensalidades', 'total_estrelas', 'Cliente'));
            $this->viewBuilder()->layout('profiles');
        } else {
            $this->redirect('/perfil');
        }
    }

    public function pagar_plano_personalizado() {
        
        if ($this->request->is(['patch', 'post', 'put'])) {

            $session  = $this->request->session();
            $Cliente  = $session->read('Cliente');
            $Academia = $session->read('Academia');
            $AcademiaMensalidade = $session->read('AcademiaMensalidade');

            if ($AcademiaMensalidade) {
                $academia = $this->Academias
                    ->find('all')
                    ->where(['id' => $AcademiaMensalidade->id])
                    ->first();
            } else {
                $academia = $this->Academias
                    ->find('all')
                    ->where(['id' => $Academia->id])
                    ->first();
            }

            $Pedidos  = TableRegistry::get('Pedidos');
            $Produtos = TableRegistry::get('Produtos');
            $Clientes = TableRegistry::get('Clientes');

            if($this->request->data['cpf_aluno']) {
                $cliente = $Clientes
                    ->find('all')
                    ->contain(['Cities', 'Academias', 'Academias.Cities'])
                    ->where(['Clientes.cpf' => $this->request->data['cpf_aluno']])
                    ->first();

                if(!$cliente) {
                    $cliente = $Clientes
                        ->find('all')
                        ->contain(['Cities', 'Academias', 'Academias.Cities'])
                        ->where(['Clientes.email' => $this->request->data['email_aluno']])
                        ->first();
                }

                if($cliente) {
                    $data = [
                        'cpf' => $this->request->data['cpf_aluno'],
                        'telephone' => $this->request->data['phone_aluno']
                    ];

                    $cliente = $Clientes->patchEntity($cliente, $data);

                    $cliente = $Clientes->save($cliente);
                }
            } else {
                $cliente = $Clientes
                    ->find('all')
                    ->contain(['Cities', 'Academias', 'Academias.Cities'])
                    ->where(['Clientes.id' => $Cliente->id])
                    ->first();
            }

            if(!$cliente) {
                function randString($size){
                    $basic = '0123456789';

                    $return= "";

                    for($count= 0; $size > $count; $count++){
                        $return.= $basic[rand(0, strlen($basic) - 1)];
                    }

                    return $return;
                }

                $senha = randString(8);

                $data = [
                    'name'          => $this->request->data['name_aluno'],
                    'telephone'     => $this->request->data['phone_aluno'],
                    'mobile'        => $this->request->data['phone_aluno'],
                    'email'         => $this->request->data['email_aluno'],
                    'cpf'           => $this->request->data['cpf_aluno'],
                    'password'      => $senha,
                    'status_id'     => 1,
                    'academia_id'   => $academia->id,
                    'city_id'       => $academia->city_id,
                    "cep"           => $academia->cep,
                    "number"        => $academia->number,
                    "address"       => $academia->address,
                    "area"          => $academia->area
                ];

                $new_cliente = $Clientes->newEntity();
                $new_cliente = $Clientes->patchEntity($new_cliente, $data);

                if ($cliente = $Clientes->save($new_cliente)) {
                    Email::configTransport('cliente', [
                        'className' => 'Smtp',
                        'host' => 'mail.logfitness.com.br',
                        'port' => 587,
                        'timeout' => 30,
                        'username' => 'uhull@logfitness.com.br',
                        'password' => 'Lvrt6174?',
                        'client' => null,
                        'tls' => null,
                    ]);

                    $email = new Email();

                    $email
                        ->transport('cliente')
                        ->template('convite_aluno_mensalidade')
                        ->emailFormat('html')
                        ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                        ->to($cliente->email)
                        ->subject($academia->shortname.' - LogFitness - Você foi convidado por uma academia!')
                        ->set([
                            'logo_academia' => $academia->image,
                            'academia'      => $academia->shortname,
                            'senha'         => $senha,
                            'name'          => $cliente->name,
                            'slug'          => $academia->slug
                        ])
                        ->send();

                    $Mensagens = TableRegistry::get('Mensagens');
                    $CupomDesconto = TableRegistry::get('CupomDesconto');
                    
                    $cupom_desconto = $CupomDesconto->newEntity();

                    $limite_cupom = Time::now();
                    $limite_cupom->addMonths(1);

                    $now = Time::now();

                    $data = [
                        'descricao' => 'Cupom de primeira compra',
                        'codigo' => 'sms'.$cliente->id,
                        'valor' => 10,
                        'tipo' => 1,
                        'quantidade' => 1,
                        'status' => 1,
                        'limite' => $limite_cupom,
                        'created' => $now,
                        'modified' => $now
                    ];

                    $cupom_desconto = $CupomDesconto->patchEntity($cupom_desconto, $data);

                    if($cupom_saved = $CupomDesconto->save($cupom_desconto)) {
                        $prefixo = explode('(', $cliente->mobile);
                        $prefixo = explode(') ', $prefixo[1]);
                        $posfixo = explode('-', $prefixo[1]);
                        $posfixo = $posfixo[0].$posfixo[1];

                        $numero = '55'.$prefixo[0].$posfixo;

                        $nome_aluno = explode(' ', $cliente->name)[0];

                        $mensagem = $Mensagens->newEntity();

                        $content_msg = "LOGFITNESS.COM.BR/".$academia->slug.": Fala ".$nome_aluno.", compre no site e retire na nossa academia com frete grátis. Cupom ".$cupom_saved->codigo." com 10% de desconto!";

                        $data_mensagem = [
                            'remetente' => '.',
                            'destinatario' => $numero,
                            'quem' => 'Aluno',
                            'msg' => $content_msg
                        ];

                        $mensagem = $Mensagens->patchEntity($mensagem, $data_mensagem);

                        $mensagem = $Mensagens->save($mensagem);

                        $body[] = [
                            "id" => $mensagem->id,
                            "from" => ".",
                            "to" => $numero,
                            "msg" => $content_msg
                        ];

                        $data = [
                            'sendSmsMultiRequest' => [
                                "sendSmsRequestList" => $body
                            ]
                        ];

                        $data = json_encode($data);

                        $ch = curl_init();

                        $header = [
                            "Accept: application/json",
                            "Content-Type: application/json",
                            "Authorization: Basic Y2FjaXF1ZS53ZWI6REd2dHdMckNTZA=="
                        ];

                        $options = array(CURLOPT_URL => "https://api-rest.zenvia360.com.br/services/send-sms-multiple",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                        );

                        curl_setopt_array($ch, $options);

                        $response = curl_exec($ch);
                        curl_close($ch);

                        $resp_send_sms = json_decode($response);

                        foreach ($resp_send_sms->sendSmsMultiResponse->sendSmsResponseList as $key => $msg_resposta) {
                            if($msg_resposta->statusCode == '00') {
                                $mensagem_enviada = $Mensagens->get($msg_resposta->parts[0]->partId);

                                $mensagem_enviada = $Mensagens->patchEntity($mensagem_enviada, ['enviado' => 1]);
                                $Mensagens->save($mensagem_enviada);
                            }
                        }
                    }
                } else {
                    echo json_encode(array(0 => 'erro'));
                    $this->viewBuilder()->layout('ajax');
                    $this->render(false);
                    return;
                }
            }

            if(!isset($cliente->cpf) || !isset($cliente->academia_id)) {
                $this->Flash->error('Prencha os seus dados!');
                return $this->redirect('/minha-conta/dados-faturamento');
            }

            $pedido = $Pedidos->newEntity();

            $valor = $this->request->data['valor_total'];

            $data = [
                'cliente_id'        => $cliente->id,
                'pedido_status_id'  => 1,
                'comissao_status'   => 5,
                'frete'             => 0,
                'academia_id'       => $academia->id,
                'valor'             => (float)$valor,
                'moeda_status'      => 1
            ];

            $pedido = $Pedidos->patchEntity($pedido, $data);

            if($valor >= 1) {

                if ($PEDIDO_SAVE = $Pedidos->save($pedido)){

                    $PEDIDO_SAVE_ID = ['id' => (20600000 + $PEDIDO_SAVE->id)];

                    $PEDIDO_SAVE = $Pedidos->patchEntity($PEDIDO_SAVE, $PEDIDO_SAVE_ID);

                    if($PEDIDO = $Pedidos->save($PEDIDO_SAVE)){

                        $session->write('PedidoFinalizado', $PEDIDO);

                        $PedidoItens = TableRegistry::get('PedidoItens');

                        $produto = $Produtos
                            ->find('all')
                            ->where(['Produtos.id' => $plano->produto_id])
                            ->first();

                        if ($produto) {
                            $preco = $produto->preco;

                            $pedido_item = $PedidoItens->newEntity();

                            $item_data = [
                                'pedido_id' => $PEDIDO->id,
                                'produto_id' => $produto->id,
                                'quantidade' => 1,
                                'desconto' => 0,
                                'preco' => $preco,
                            ];

                            $pedido_item = $PedidoItens->patchEntity($pedido_item, $item_data);

                            if ($PEDIDO_ITEM_SAVE = $PedidoItens->save($pedido_item)) {
                                $session->write('PedidoFinalizadoItem.'.$PEDIDO_ITEM_SAVE->id, $PEDIDO_ITEM_SAVE);
                            }
                        }

                        $total = number_format($valor, 2, '.', '');

                        $PedidoCliente = $Pedidos
                            ->find('all', ['contain' => [
                                'Academias',
                                'Academias.Cities',
                                'Clientes',
                                'PedidoItens',
                                'PedidoItens.Produtos',
                                'PedidoItens.Produtos.ProdutoBase',
                                'PedidoItens.Produtos.ProdutoBase.Marcas'
                            ]
                            ])
                            ->where(['Pedidos.id' => $PEDIDO->id])
                            ->first();

                        $session->write('ClienteFinalizado', $cliente);

                        $retorno = $this->iugu_cobranca_direta_plano_personalizado($PedidoCliente->id);

                        if($retorno['boleto'] == 'boleto') {
                            $data = [
                                0 => 'sucesso',
                                1 => $retorno['url_pdf']
                            ];
                            echo json_encode($data);
                            $this->viewBuilder()->layout('ajax');
                            $this->render(false);
                            return;
                        } else if ($retorno == 'cartao1') {
                            $this->viewBuilder()->layout('ajax');
                            return $this->render('mensalidade_cartao_autorizado');
                        } else if ($retorno == 'cartao2') {
                            $this->viewBuilder()->layout('ajax');
                            return $this->render('mensalidade_cartao_recusado');
                        } else if ($retorno == 'boleto2') {
                            echo json_encode(array(0 => 'erro'));
                            $this->viewBuilder()->layout('ajax');
                            $this->render(false);
                            return;
                        }
                    }
                }else{
                    $this->redirect('/comprar_pacote/'.$plano->id);
                }
            }
        }

        return $this->redirect('/perfil');
        $this->render(false);
    }

    public function pagar_plano() {
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $session  = $this->request->session();
            $Cliente  = $session->read('Cliente');
            $Academia = $session->read('Academia');
            $AcademiaMensalidade = $session->read('AcademiaMensalidade');

            if ($AcademiaMensalidade) {
                $academia = $this->Academias
                    ->find('all')
                    ->where(['id' => $AcademiaMensalidade->id])
                    ->first();
            } else {
                $academia = $this->Academias
                    ->find('all')
                    ->where(['id' => $Academia->id])
                    ->first();
            }

            $Pedidos  = TableRegistry::get('Pedidos');
            $Produtos = TableRegistry::get('Produtos');
            $Clientes = TableRegistry::get('Clientes');

            if($this->request->data['cpf_aluno']) {
                $cliente = $Clientes
                    ->find('all')
                    ->contain(['Cities', 'Academias', 'Academias.Cities'])
                    ->where(['Clientes.cpf' => $this->request->data['cpf_aluno']])
                    ->first();

                if(!$cliente) {
                    $cliente = $Clientes
                        ->find('all')
                        ->contain(['Cities', 'Academias', 'Academias.Cities'])
                        ->where(['Clientes.email' => $this->request->data['email_aluno']])
                        ->first();
                }

                if($cliente) {
                    $data = [
                        'cpf' => $this->request->data['cpf_aluno'],
                        'telephone' => $this->request->data['phone_aluno']
                    ];

                    $cliente = $Clientes->patchEntity($cliente, $data);

                    $cliente = $Clientes->save($cliente);
                }
            } else {
                $cliente = $Clientes
                    ->find('all')
                    ->contain(['Cities', 'Academias', 'Academias.Cities'])
                    ->where(['Clientes.id' => $Cliente->id])
                    ->first();
            }

            if(!$cliente) {
                function randString($size){
                    $basic = '0123456789';

                    $return= "";

                    for($count= 0; $size > $count; $count++){
                        $return.= $basic[rand(0, strlen($basic) - 1)];
                    }

                    return $return;
                }

                $senha = randString(8);

                $data = [
                    'name'          => $this->request->data['name_aluno'],
                    'telephone'     => $this->request->data['phone_aluno'],
                    'mobile'        => $this->request->data['phone_aluno'],
                    'email'         => $this->request->data['email_aluno'],
                    'cpf'           => $this->request->data['cpf_aluno'],
                    'password'      => $senha,
                    'status_id'     => 1,
                    'academia_id'   => $academia->id,
                    'city_id'       => $academia->city_id,
                    "cep"           => $academia->cep,
                    "number"        => $academia->number,
                    "address"       => $academia->address,
                    "area"          => $academia->area
                ];

                $new_cliente = $Clientes->newEntity();
                $new_cliente = $Clientes->patchEntity($new_cliente, $data);

                if ($cliente = $Clientes->save($new_cliente)) {
                    Email::configTransport('cliente', [
                        'className' => 'Smtp',
                        'host' => 'mail.logfitness.com.br',
                        'port' => 587,
                        'timeout' => 30,
                        'username' => 'uhull@logfitness.com.br',
                        'password' => 'Lvrt6174?',
                        'client' => null,
                        'tls' => null,
                    ]);

                    $email = new Email();

                    $email
                        ->transport('cliente')
                        ->template('professor_invited_email')
                        ->emailFormat('html')
                        ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                        ->to($cliente->email)
                        ->subject($academia->shortname.' - LogFitness - Você foi convidado por uma academia!')
                        ->set([
                            'name'              => $cliente->name,
                            'academia'          => $academia->shortname,
                            'logo_academia'     => $academia->image,
                            'url_academia'      => $academia->slug,
                            'senha'             => $senha
                        ])
                        ->send();

                    $Mensagens = TableRegistry::get('Mensagens');
                    $CupomDesconto = TableRegistry::get('CupomDesconto');
                    
                    $cupom_desconto = $CupomDesconto->newEntity();

                    $limite_cupom = Time::now();
                    $limite_cupom->addMonths(1);

                    $now = Time::now();

                    $data = [
                        'descricao' => 'Cupom de primeira compra',
                        'codigo' => 'sms'.$cliente->id,
                        'valor' => 10,
                        'tipo' => 1,
                        'quantidade' => 1,
                        'status' => 1,
                        'limite' => $limite_cupom,
                        'created' => $now,
                        'modified' => $now
                    ];

                    $cupom_desconto = $CupomDesconto->patchEntity($cupom_desconto, $data);

                    if($cupom_saved = $CupomDesconto->save($cupom_desconto)) {
                        $prefixo = explode('(', $cliente->mobile);
                        $prefixo = explode(') ', $prefixo[1]);
                        $posfixo = explode('-', $prefixo[1]);
                        $posfixo = $posfixo[0].$posfixo[1];

                        $numero = '55'.$prefixo[0].$posfixo;

                        $nome_aluno = explode(' ', $cliente->name)[0];

                        $mensagem = $Mensagens->newEntity();

                        $content_msg = "LOGFITNESS.COM.BR/".$academia->slug.": Fala ".$nome_aluno.", compre no site e retire na nossa academia com frete grátis. Cupom ".$cupom_saved->codigo." com 10% de desconto!";

                        $data_mensagem = [
                            'remetente' => '.',
                            'destinatario' => $numero,
                            'quem' => 'Aluno',
                            'msg' => $content_msg
                        ];

                        $mensagem = $Mensagens->patchEntity($mensagem, $data_mensagem);

                        $mensagem = $Mensagens->save($mensagem);

                        $body[] = [
                            "id" => $mensagem->id,
                            "from" => ".",
                            "to" => $numero,
                            "msg" => $content_msg
                        ];

                        $data = [
                            'sendSmsMultiRequest' => [
                                "sendSmsRequestList" => $body
                            ]
                        ];

                        $data = json_encode($data);

                        $ch = curl_init();

                        $header = [
                            "Accept: application/json",
                            "Content-Type: application/json",
                            "Authorization: Basic Y2FjaXF1ZS53ZWI6REd2dHdMckNTZA=="
                        ];

                        $options = array(CURLOPT_URL => "https://api-rest.zenvia360.com.br/services/send-sms-multiple",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                        );

                        curl_setopt_array($ch, $options);

                        $response = curl_exec($ch);
                        curl_close($ch);

                        $resp_send_sms = json_decode($response);

                        foreach ($resp_send_sms->sendSmsMultiResponse->sendSmsResponseList as $key => $msg_resposta) {
                            if($msg_resposta->statusCode == '00') {
                                $mensagem_enviada = $Mensagens->get($msg_resposta->parts[0]->partId);

                                $mensagem_enviada = $Mensagens->patchEntity($mensagem_enviada, ['enviado' => 1]);
                                $Mensagens->save($mensagem_enviada);
                            }
                        }
                    }
                } else {
                    $this->Flash->error('Falha ao cadastrar aluno. Tente novamente.');
                }
            }

            $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

            $plano = $AcademiaMensalidadesPlanos
                ->find('all')
                ->where(['id' => $this->request->data['plano_id']])
                ->first();

            $parcelas_boleto = $this->request->data['parcelas_boleto'];

            Email::configTransport('cliente2', [
                'className' => 'Smtp',
                'host' => 'mail.logfitness.com.br',
                'port' => 587,
                'timeout' => 30,
                'username' => 'uhull@logfitness.com.br',
                'password' => 'Lvrt6174?',
                'client' => null,
                'tls' => null,
            ]);

            if($parcelas_boleto >= 2) {
                for ($i = 0; $i < $parcelas_boleto; $i++) { 
                    $pedido = $Pedidos->newEntity();

                    $valor = $plano->valor_total/$parcelas_boleto;

                    $data = [
                        'cliente_id'        => $cliente->id,
                        'pedido_status_id'  => 1,
                        'comissao_status'   => 4,
                        'frete'             => 0,
                        'academia_id'       => $academia->id,
                        'valor'             => (float)$valor,
                        'moeda_status'      => 1
                    ];

                    $pedido = $Pedidos->patchEntity($pedido, $data);

                    if($valor >= 1) {
                        if ($PEDIDO_SAVE = $Pedidos->save($pedido)){

                            $PEDIDO_SAVE_ID = ['id' => (20600000 + $PEDIDO_SAVE->id)];

                            $PEDIDO_SAVE = $Pedidos->patchEntity($PEDIDO_SAVE, $PEDIDO_SAVE_ID);
                            if($PEDIDO = $Pedidos->save($PEDIDO_SAVE)){

                                $session->write('PedidoFinalizado', $PEDIDO);

                                $PedidoItens = TableRegistry::get('PedidoItens');

                                $produto = $Produtos
                                    ->find('all')
                                    ->where(['Produtos.id' => $plano->produto_id])
                                    ->first();

                                if ($produto) {
                                    $preco = $produto->preco;

                                    $pedido_item = $PedidoItens->newEntity();

                                    $item_data = [
                                        'pedido_id' => $PEDIDO->id,
                                        'produto_id' => $produto->id,
                                        'quantidade' => 1,
                                        'desconto' => 0,
                                        'preco' => $preco,
                                    ];

                                    $pedido_item = $PedidoItens->patchEntity($pedido_item, $item_data);

                                    if ($PEDIDO_ITEM_SAVE = $PedidoItens->save($pedido_item)) {
                                        $session->write('PedidoFinalizadoItem.'.$PEDIDO_ITEM_SAVE->id, $PEDIDO_ITEM_SAVE);
                                    }
                                }

                                $total = number_format($valor, 2, '.', '');

                                $PedidoCliente = $Pedidos
                                    ->find('all', ['contain' => [
                                        'Academias',
                                        'Academias.Cities',
                                        'Clientes',
                                        'PedidoItens',
                                        'PedidoItens.Produtos',
                                        'PedidoItens.Produtos.ProdutoBase',
                                        'PedidoItens.Produtos.ProdutoBase.Marcas'
                                    ]
                                    ])
                                    ->where(['Pedidos.id' => $PEDIDO->id])
                                    ->first();

                                $session->write('ClienteFinalizado', $cliente);

                                $retorno = $this->iugu_cobranca_direta_plano($PedidoCliente->id, $i, $parcelas_boleto);

                                if($retorno['boleto'] == 'boleto') {
                                    $retorno_array[] = $retorno['url_pdf'];
                                } 
                            }
                        }
                    }
                }
                $email = new Email();

                $email
                    ->transport('cliente2')
                    ->template('boleto_plano_boletos')
                    ->emailFormat('html')
                    ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                    ->to($PedidoCliente->cliente->email)
                    ->subject($PedidoCliente->academia->shortname.' - LogFitness - Boleto')
                    ->set([
                        'name'              => $PedidoCliente->cliente->name,
                        'academia'          => $PedidoCliente->academia->shortname,
                        'logo_academia'     => $PedidoCliente->academia->image,
                        'slug'              => $PedidoCliente->academia->slug,
                        'boletos'           => $retorno_array
                    ])
                    ->send();

                $pedido_plano = $Pedidos
                    ->find('all')
                    ->where(['Pedidos.id' => $PedidoCliente->id])
                    ->first();

                $array[] = 'boletos';
                $array[] = $pedido_plano->plano_cliente_id;

                echo json_encode($array);
                $this->viewBuilder()->layout('ajax');
                $this->render(false);
                return;
            } else {
                $pedido = $Pedidos->newEntity();

                $valor = $this->request->data['valor_total'];

                $data = [
                    'cliente_id'        => $cliente->id,
                    'pedido_status_id'  => 1,
                    'comissao_status'   => 4,
                    'frete'             => 0,
                    'academia_id'       => $academia->id,
                    'valor'             => (float)$valor,
                    'moeda_status'      => 1
                ];

                $pedido = $Pedidos->patchEntity($pedido, $data);

                if($valor >= 1) {
                    if ($PEDIDO_SAVE = $Pedidos->save($pedido)){

                        $PEDIDO_SAVE_ID = ['id' => (20600000 + $PEDIDO_SAVE->id)];

                        $PEDIDO_SAVE = $Pedidos->patchEntity($PEDIDO_SAVE, $PEDIDO_SAVE_ID);
                        if($PEDIDO = $Pedidos->save($PEDIDO_SAVE)){

                            $session->write('PedidoFinalizado', $PEDIDO);

                            $PedidoItens = TableRegistry::get('PedidoItens');

                            $produto = $Produtos
                                ->find('all')
                                ->where(['Produtos.id' => $plano->produto_id])
                                ->first();

                            if ($produto) {
                                $preco = $produto->preco;

                                $pedido_item = $PedidoItens->newEntity();

                                $item_data = [
                                    'pedido_id' => $PEDIDO->id,
                                    'produto_id' => $produto->id,
                                    'quantidade' => 1,
                                    'desconto' => 0,
                                    'preco' => $preco,
                                ];

                                $pedido_item = $PedidoItens->patchEntity($pedido_item, $item_data);

                                if ($PEDIDO_ITEM_SAVE = $PedidoItens->save($pedido_item)) {
                                    $session->write('PedidoFinalizadoItem.'.$PEDIDO_ITEM_SAVE->id, $PEDIDO_ITEM_SAVE);
                                }
                            }

                            $total = number_format($valor, 2, '.', '');

                            $PedidoCliente = $Pedidos
                                ->find('all', ['contain' => [
                                    'Academias',
                                    'Academias.Cities',
                                    'Clientes',
                                    'PedidoItens',
                                    'PedidoItens.Produtos',
                                    'PedidoItens.Produtos.ProdutoBase',
                                    'PedidoItens.Produtos.ProdutoBase.Marcas'
                                ]
                                ])
                                ->where(['Pedidos.id' => $PEDIDO->id])
                                ->first();

                            $session->write('ClienteFinalizado', $cliente);

                            $retorno = $this->iugu_cobranca_direta_plano($PedidoCliente->id);

                            if($retorno['boleto'] == 'boleto') {
                                $data = [
                                    0 => 'sucesso',
                                    1 => $retorno['url_pdf']
                                ];
                                echo json_encode($data);
                                $this->viewBuilder()->layout('ajax');
                                $this->render(false);
                                return;
                            } else if ($retorno == 'cartao1') {
                                $this->viewBuilder()->layout('ajax');
                                return $this->render('mensalidade_cartao_autorizado');
                            } else if ($retorno == 'cartao2') {
                                $this->viewBuilder()->layout('ajax');
                                return $this->render('mensalidade_cartao_recusado');
                            } else if ($retorno == 'boleto2') {
                                echo json_encode(array(0 => 'erro'));
                                $this->viewBuilder()->layout('ajax');
                                $this->render(false);
                                return;
                            }
                        }
                    }else{
                        $this->redirect('/comprar_pacote/'.$plano->id);
                    }
                }
            }
        }

        return $this->redirect('/perfil');
        $this->render(false);
    }

    public function mensalidade_finalizado ($plano_id) {
        if($plano_id != null) {
            $Pedidos                    = TableRegistry::get('Pedidos');
            $Academias                  = TableRegistry::get('Academias');
            $Avaliacoes                 = TableRegistry::get('Avaliacoes');
            $PlanosClientes             = TableRegistry::get('PlanosClientes');
            $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
            $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

            $plano_cliente = $PlanosClientes
                ->find('all')
                ->where(['id' => $plano_id])
                ->first();

            $plano = $AcademiaMensalidadesPlanos
                ->find('all')
                ->contain(['Produtos'])
                ->where(['AcademiaMensalidadesPlanos.id' => $plano_cliente->academia_mensalidades_planos_id])
                ->andWhere(['AcademiaMensalidadesPlanos.visivel' => 1])
                ->andWhere(['Produtos.estoque >=' => 1])
                ->first();

            $academia_mensalidades = $AcademiaMensalidades
                ->find('all')
                ->where(['id' => $plano->academia_mensalidades_id])
                ->first();

            $academia = $this->Academias
                ->find('all')
                ->contain(['Cities'])
                ->where(['Academias.id' => $academia_mensalidades->academia_id])
                ->where(['Academias.status_id' => 1])
                ->first();

            $avaliacoes = $Avaliacoes
                ->find('all')
                ->contain('Clientes')
                ->where(['Avaliacoes.academia_id' => $academia->id])
                ->all();

            foreach ($avaliacoes as $avaliacao) {
                $estrelas += $avaliacao->rating;
            }

            $total_estrelas = $estrelas / count($avaliacoes);

            $pedidos = $Pedidos
                ->find('all', ['contain' => [
                    'Academias',
                    'Academias.Cities',
                    'Clientes',
                    'PedidoItens',
                    'PedidoItens.Produtos',
                    'PedidoItens.Produtos.ProdutoBase',
                    'PedidoItens.Produtos.ProdutoBase.Marcas'
                ]
                ])
                ->where(['Pedidos.plano_cliente_id' => $plano_id])
                ->all();

            $this->set(compact('pedidos', 'academia', 'plano', 'academia_mensalidades', 'total_estrelas'));
        } else {
            return $this->redirect('/perfil');
        }
        $this->viewBuilder()->layout('profiles');
    }

    public function mensalidade_cartao_autorizado () {
    }

    public function mensalidade_cartao_recusado () {
    }

    public function pagar_assinatura() {
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $session  = $this->request->session();
            $Cliente  = $session->read('Cliente');
            $Academia = $session->read('Academia');
            $AcademiaMensalidade = $session->read('AcademiaMensalidade');

            if ($AcademiaMensalidade) {
                $academia = $this->Academias
                    ->find('all')
                    ->where(['id' => $AcademiaMensalidade->id])
                    ->first();
            } else {
                $academia = $this->Academias
                    ->find('all')
                    ->where(['id' => $Academia->id])
                    ->first();
            }

            $Pedidos  = TableRegistry::get('Pedidos');
            $Produtos = TableRegistry::get('Produtos');
            $Clientes = TableRegistry::get('Clientes');

            if($this->request->data['cpf_aluno']) {
                $cliente = $Clientes
                    ->find('all')
                    ->contain(['Cities', 'Academias', 'Academias.Cities'])
                    ->where(['Clientes.cpf' => $this->request->data['cpf_aluno']])
                    ->first();

                if(!$cliente) {
                    $cliente = $Clientes
                        ->find('all')
                        ->contain(['Cities', 'Academias', 'Academias.Cities'])
                        ->where(['Clientes.email' => $this->request->data['email_aluno']])
                        ->first();
                }

                if($cliente) {
                    $data = [
                        'cpf' => $this->request->data['cpf_aluno'],
                        'telephone' => $this->request->data['phone_aluno']
                    ];

                    $cliente = $Clientes->patchEntity($cliente, $data);

                    $cliente = $Clientes->save($cliente);
                }
            } else {
                $cliente = $Clientes
                    ->find('all')
                    ->contain(['Cities', 'Academias', 'Academias.Cities'])
                    ->where(['Clientes.id' => $Cliente->id])
                    ->first();
            }

            if(!$cliente) {
                function randString($size){
                    $basic = '0123456789';

                    $return= "";

                    for($count= 0; $size > $count; $count++){
                        $return.= $basic[rand(0, strlen($basic) - 1)];
                    }

                    return $return;
                }

                $senha = randString(8);

                $data = [
                    'name'          => $this->request->data['name_aluno'],
                    'telephone'     => $this->request->data['phone_aluno'],
                    'mobile'        => $this->request->data['phone_aluno'],
                    'email'         => $this->request->data['email_aluno'],
                    'cpf'           => $this->request->data['cpf_aluno'],
                    'password'      => $senha,
                    'status_id'     => 1,
                    'academia_id'   => $academia->id,
                    'city_id'       => $academia->city_id,
                    "cep"           => $academia->cep,
                    "number"        => $academia->number,
                    "address"       => $academia->address,
                    "area"          => $academia->area
                ];

                $new_cliente = $Clientes->newEntity();
                $new_cliente = $Clientes->patchEntity($new_cliente, $data);

                if ($cliente = $Clientes->save($new_cliente)) {
                    Email::configTransport('professor', [
                        'className' => 'Smtp',
                        'host' => 'mail.logfitness.com.br',
                        'port' => 587,
                        'timeout' => 30,
                        'username' => 'uhull@logfitness.com.br',
                        'password' => 'Lvrt6174?',
                        'client' => null,
                        'tls' => null,
                    ]);

                    $email = new Email();

                    $email
                        ->transport('professor')
                        ->template('professor_invited_email')
                        ->emailFormat('html')
                        ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                        ->to($cliente->email)
                        ->subject($academia->shortname.' - LogFitness - Você foi convidado por uma academia!')
                        ->set([
                            'name'              => $cliente->name,
                            'academia'          => $academia->shortname,
                            'logo_academia'     => $academia->image,
                            'url_academia'      => $academia->slug,
                            'senha'             => $senha
                        ])
                        ->send();

                    $Mensagens = TableRegistry::get('Mensagens');
                    $CupomDesconto = TableRegistry::get('CupomDesconto');
                    
                    $cupom_desconto = $CupomDesconto->newEntity();

                    $limite_cupom = Time::now();
                    $limite_cupom->addMonths(1);

                    $now = Time::now();

                    $data = [
                        'descricao' => 'Cupom de primeira compra',
                        'codigo' => 'sms'.$cliente->id,
                        'valor' => 10,
                        'tipo' => 1,
                        'quantidade' => 1,
                        'status' => 1,
                        'limite' => $limite_cupom,
                        'created' => $now,
                        'modified' => $now
                    ];

                    $cupom_desconto = $CupomDesconto->patchEntity($cupom_desconto, $data);

                    if($cupom_saved = $CupomDesconto->save($cupom_desconto)) {
                        $prefixo = explode('(', $cliente->mobile);
                        $prefixo = explode(') ', $prefixo[1]);
                        $posfixo = explode('-', $prefixo[1]);
                        $posfixo = $posfixo[0].$posfixo[1];

                        $numero = '55'.$prefixo[0].$posfixo;

                        $nome_aluno = explode(' ', $cliente->name)[0];

                        $mensagem = $Mensagens->newEntity();

                        $content_msg = "LOGFITNESS.COM.BR/".$academia->slug.": Fala ".$nome_aluno.", compre no site e retire na nossa academia com frete grátis. Cupom ".$cupom_saved->codigo." com 10% de desconto!";

                        $data_mensagem = [
                            'remetente' => '.',
                            'destinatario' => $numero,
                            'quem' => 'Aluno',
                            'msg' => $content_msg
                        ];

                        $mensagem = $Mensagens->patchEntity($mensagem, $data_mensagem);

                        $mensagem = $Mensagens->save($mensagem);

                        $body[] = [
                            "id" => $mensagem->id,
                            "from" => ".",
                            "to" => $numero,
                            "msg" => $content_msg
                        ];

                        $data = [
                            'sendSmsMultiRequest' => [
                                "sendSmsRequestList" => $body
                            ]
                        ];

                        $data = json_encode($data);

                        $ch = curl_init();

                        $header = [
                            "Accept: application/json",
                            "Content-Type: application/json",
                            "Authorization: Basic Y2FjaXF1ZS53ZWI6REd2dHdMckNTZA=="
                        ];

                        $options = array(CURLOPT_URL => "https://api-rest.zenvia360.com.br/services/send-sms-multiple",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                        );

                        curl_setopt_array($ch, $options);

                        $response = curl_exec($ch);
                        curl_close($ch);

                        $resp_send_sms = json_decode($response);

                        foreach ($resp_send_sms->sendSmsMultiResponse->sendSmsResponseList as $key => $msg_resposta) {
                            if($msg_resposta->statusCode == '00') {
                                $mensagem_enviada = $Mensagens->get($msg_resposta->parts[0]->partId);

                                $mensagem_enviada = $Mensagens->patchEntity($mensagem_enviada, ['enviado' => 1]);
                                $Mensagens->save($mensagem_enviada);
                            }
                        }
                    }
                } else {
                    $this->Flash->error('Falha ao cadastrar aluno. Tente novamente.');
                }
            }

            if(!isset($cliente->cpf) || !isset($cliente->academia_id)) {
                $this->Flash->error('Prencha os seus dados!');
                return $this->redirect('/minha-conta/dados-faturamento');
            }

            $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

            $plano = $AcademiaMensalidadesPlanos
                ->find('all')
                ->where(['id' => $this->request->data['plano_id']])
                ->first();

            if(!$plano) {
                return $this->redirect('/perfil');
            }

            $pedido = $Pedidos->newEntity();

            $valor = $this->request->data['valor_total'];

            $data = [
                'cliente_id'        => $cliente->id,
                'pedido_status_id'  => 1,
                'comissao_status'   => 4,
                'frete'             => 0,
                'academia_id'       => $academia->id,
                'valor'             => (float)$valor,
                'moeda_status'      => 1
            ];

            $pedido = $Pedidos->patchEntity($pedido, $data);
       

            if($valor >= 1) {
                if ($PEDIDO_SAVE = $Pedidos->save($pedido)){

                    $PEDIDO_SAVE_ID = ['id' => (20600000 + $PEDIDO_SAVE->id)];

                    $PEDIDO_SAVE = $Pedidos->patchEntity($PEDIDO_SAVE, $PEDIDO_SAVE_ID);
                    if($PEDIDO = $Pedidos->save($PEDIDO_SAVE)){

                        $session->write('PedidoFinalizado', $PEDIDO);

                        $PedidoItens = TableRegistry::get('PedidoItens');

                        $produto = $Produtos
                            ->find('all')
                            ->where(['Produtos.id' => $plano->produto_id])
                            ->first();

                        if ($produto) {
                            $preco = $produto->preco;

                            $pedido_item = $PedidoItens->newEntity();

                            $item_data = [
                                'pedido_id' => $PEDIDO->id,
                                'produto_id' => $produto->id,
                                'quantidade' => 1,
                                'desconto' => 0,
                                'preco' => $preco,
                            ];

                            $pedido_item = $PedidoItens->patchEntity($pedido_item, $item_data);

                            if ($PEDIDO_ITEM_SAVE = $PedidoItens->save($pedido_item)) {
                                $session->write('PedidoFinalizadoItem.'.$PEDIDO_ITEM_SAVE->id, $PEDIDO_ITEM_SAVE);
                            }
                        }

                        $total = number_format($valor, 2, '.', '');

                        $PedidoCliente = $Pedidos
                            ->find('all', ['contain' => [
                                'Academias',
                                'Academias.Cities',
                                'Clientes',
                                'PedidoItens',
                                'PedidoItens.Produtos',
                                'PedidoItens.Produtos.ProdutoBase',
                                'PedidoItens.Produtos.ProdutoBase.Marcas'
                            ]
                            ])
                            ->where(['Pedidos.id' => $PEDIDO->id])
                            ->first();

                        $session->write('ClienteFinalizado', $cliente);

                        $retorno = $this->iugu_assinatura_plano($PedidoCliente->id);

                        if($retorno['boleto'] == 'boleto') {
                            echo $retorno['url_pdf'];
                            $this->viewBuilder()->layout('ajax');
                            $this->render(false);
                            return;
                        } else if ($retorno == 'cartao1') {
                            $this->viewBuilder()->layout('ajax');
                            return $this->render('mensalidade_cartao_autorizado');
                        } else if ($retorno == 'cartao2') {
                            $this->viewBuilder()->layout('ajax');
                            return $this->render('mensalidade_cartao_recusado');
                        } else if ($retorno == 'boleto2') {
                            echo json_encode(array(0 => 'erro'));
                            $this->viewBuilder()->layout('ajax');
                            $this->render(false);
                            return;
                        }
                    }
                }else{
                    $this->redirect('/comprar_pacote/'.$plano->id);
                }
            }
        }

        return $this->redirect('/perfil');
        $this->render(false);
    }

    public function iugu_assinatura_plano ($pedido_id = null) {        
        if($pedido_id != null) { 
            if($this->request->is(['post', 'put'])) {
                $Pedidos  = TableRegistry::get('Pedidos');
                $Clientes = TableRegistry::get('Clientes');

                $PedidoCliente  = $Pedidos
                    ->find('all', ['contain' => [
                        'Academias',
                        'Academias.Cities',
                        'Clientes',
                        'PedidoItens',
                        'PedidoItens.Produtos',
                        'PedidoItens.Produtos.ProdutoBase',
                        'PedidoItens.Produtos.ProdutoBase.Marcas'
                    ]
                    ])
                    ->where(['Pedidos.id' => $pedido_id])
                    ->first(); 
                
                if($PedidoCliente->cliente->iugu_cliente_id != null) {
                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/customers/".$PedidoCliente->cliente->iugu_cliente_id."/?api_token=".IUGU_API_TOKEN,
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => FALSE,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    $resp_iugu_cliente = json_decode($response);
                } else {
                    $cliente = $Clientes
                        ->find('all')
                        ->contain(['Cities'])
                        ->where(['Clientes.id' => $PedidoCliente->cliente_id])
                        ->first();

                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $data = [
                        "api_token" => IUGU_API_TOKEN,
                        "email"     => $cliente->email,
                        "name"      => $cliente->name,
                        "cpf_cnpj"  => str_replace('.', '', $cliente->cpf),
                        "zip_code"  => str_replace('.', '', $PedidoCliente->academia->cep),
                        "number"    => $PedidoCliente->academia->number,
                        "street"    => $PedidoCliente->academia->address,
                        "city"      => $PedidoCliente->academia->city->name,
                        "state"     => $PedidoCliente->academia->city->uf,
                        "district"  => $PedidoCliente->academia->area
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/customers",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    $resp_iugu_cliente = json_decode($response);

                    $data = [
                        'iugu_cliente_id' => $resp_iugu_cliente->id
                    ];

                    $cliente = $Clientes->patchEntity($cliente, $data);
                    $Clientes->save($cliente);

                    $PedidoCliente->cliente->iugu_cliente_id = $resp_iugu_cliente->id;
                }

                foreach ($PedidoCliente->pedido_itens as $pi) {

                    $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

                    $plano = $AcademiaMensalidadesPlanos
                        ->find('all')
                        ->where(['produto_id' => $pi->produto->id])
                        ->first(); 
                }

                if(isset($this->request->data['token'])) {

                    /* CRIA MÉTODO DE PAGAMENTO DO CLIENTE */
                    if($PedidoCliente->cliente->iugu_cliente_payment_id != null) {
                        $ch = curl_init();

                        $header = [
                            "Content-Type: application/json"
                        ];

                        $data = json_encode($data);

                        $options = array(CURLOPT_URL => "https://api.iugu.com/v1/customers/".$PedidoCliente->cliente->iugu_cliente_id."/payment_methods/".$PedidoCliente->cliente->iugu_cliente_payment_id."/?api_token=".IUGU_API_TOKE,
                            CURLOPT_RETURNTRANSFER => TRUE,
                            CURLOPT_HEADER => FALSE,
                            CURLOPT_POST => FALSE,
                            CURLOPT_HTTPHEADER => $header
                        );

                        curl_setopt_array($ch, $options);

                        $response = curl_exec($ch);
                        curl_close($ch);

                        $resp_iugu_cliente_payment = json_decode($response);
                    } else {

                        $cliente = $Clientes
                            ->find('all')
                            ->contain(['Cities'])
                            ->where(['Clientes.id' => $PedidoCliente->cliente_id])
                            ->first();

                        $ch = curl_init();

                        $header = [
                            "Content-Type: application/json"
                        ];

                        $data = [
                            "api_token"   => IUGU_API_TOKEN,
                            "description" => 'Meu cartão de crédito',
                            "token"       => $this->request->data['token']
                        ];

                        $data = json_encode($data);

                        $options = array(CURLOPT_URL => "https://api.iugu.com/v1/customers/".$PedidoCliente->cliente->iugu_cliente_id."/payment_methods",
                            CURLOPT_RETURNTRANSFER => TRUE,
                            CURLOPT_HEADER => FALSE,
                            CURLOPT_POST => TRUE,
                            CURLOPT_POSTFIELDS => $data,
                            CURLOPT_HTTPHEADER => $header
                        );

                        curl_setopt_array($ch, $options);

                        $response = curl_exec($ch);
                        curl_close($ch);

                        $resp_iugu_cliente_payment = json_decode($response);

                        $data = [
                            'iugu_cliente_payment_id' => $resp_iugu_cliente_payment->id
                        ];

                        $cliente = $Clientes->patchEntity($cliente, $data);
                        $Clientes->save($cliente);
                    }

                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $identificador_plano = $plano->id.'-'.$plano->name;

                    $data = [
                        "api_token"              => IUGU_API_TOKEN,
                        "customer_id"            => $resp_iugu_cliente->id,
                        "plan_identifier"        => $identificador_plano,
                        "only_on_charge_success" => 'true',
                        "payable_with"           => 'all'
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/subscriptions",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    $resp_iugu_assinatura = json_decode($response);

                    $PedidoCliente->tid              = $resp_iugu_assinatura->recent_invoices[0]->id;
                    $PedidoCliente->iugu_token       = $this->request->data['token'];
                    $PedidoCliente->iugu_brand       = $this->request->data['iugu_brand'];
                    $PedidoCliente->portador_name    = $this->request->data['portador_name'];
                    $PedidoCliente->portador_birth   = $this->request->data['portador_birth'];
                    $PedidoCliente->portador_phone   = $this->request->data['portador_phone'];
                    $PedidoCliente->portador_cpf     = $this->request->data['portador_cpf'];
                    $PedidoCliente->payment_method   = 'CartaoDeCredito';
                    $PedidoCliente->transaction_data = serialize($response);
                    
                    if($Pedidos->save($PedidoCliente) && !$resp_iugu_assinatura->errors) {

                        $PlanosClientes = TableRegistry::get('PlanosClientes');

                        $plano_cliente = $PlanosClientes->newEntity();

                        $time = new Time($resp_iugu_assinatura->expires_at);

                        $data = [
                            'academia_mensalidades_planos_id' => $plano->id,
                            'cliente_id' => $PedidoCliente->cliente_id,
                            'iugu_assinatura_id' => $resp_iugu_assinatura->id,
                            'type' => 'Assinatura',
                            'status' => 0,
                            'data_vencimento' => $time
                        ];

                        $plano_cliente = $PlanosClientes->patchEntity($plano_cliente, $data);

                        if($plano_saved = $PlanosClientes->save($plano_cliente)) {

                            $data = [
                                'plano_cliente_id' => $plano_saved->id,
                                'data_pagamento' => Time::now(),
                                'pedido_status_id' => 3
                            ];

                            $PedidoCliente = $Pedidos->patchEntity($PedidoCliente, $data);
                            $Pedidos->save($PedidoCliente);

                            $this->Flash->success('Assinatura '.$plano->name.' comprada com sucesso!');
                            return 'cartao1';
                        } else {
                            $this->Flash->error('Falha ao comprar assinatura... Tente novamente...');
                            return 'cartao2';
                        }
                    
                    } else {
                        $PedidoCliente->tid              = $resp_iugu_cartao->invoice_id;
                        $PedidoCliente->iugu_token       = $this->request->data['token'];
                        $PedidoCliente->parcelas         = $this->request->data['parcelas'];
                        $PedidoCliente->valor_parcelado  = $total_parcelado;
                        $PedidoCliente->parcelas         = 1;
                        $PedidoCliente->valor_parcelado  = null;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_cartao->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_cartao->pdf;
                        $PedidoCliente->iugu_brand       = $this->request->data['iugu_brand'];
                        $PedidoCliente->identification   = $resp_iugu_cartao->identification;
                        $PedidoCliente->portador_name    = $this->request->data['portador_name'];
                        $PedidoCliente->portador_birth   = $this->request->data['portador_birth'];
                        $PedidoCliente->portador_phone   = $this->request->data['portador_phone'];
                        $PedidoCliente->portador_cpf     = $this->request->data['portador_cpf'];
                        $PedidoCliente->pedido_status_id = 1;
                        $PedidoCliente->transaction_data = serialize($response);
                        $Pedidos->save($PedidoCliente);

                        $this->Flash->error('Falha ao passar cartão, tente novamente!');
                        return 'cartao2';
                    }
                }
            }
        }
    }

    public function iugu_cobranca_direta_plano_personalizado ($pedido_id = null) {
        if($pedido_id != null) { 
            if($this->request->is(['post', 'put'])) {
                $Pedidos  = TableRegistry::get('Pedidos');
                $Clientes = TableRegistry::get('Clientes');

                $PedidoCliente  = $Pedidos
                    ->find('all', ['contain' => [
                        'Academias',
                        'Academias.Cities',
                        'Clientes',
                        'PedidoItens',
                        'PedidoItens.Produtos',
                        'PedidoItens.Produtos.ProdutoBase',
                        'PedidoItens.Produtos.ProdutoBase.Marcas'
                    ]
                    ])
                    ->where(['Pedidos.id' => $pedido_id])
                    ->first(); 
                if($PedidoCliente->cliente->iugu_cliente_id != null) {
                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/customers/".$PedidoCliente->cliente->iugu_cliente_id."/?api_token=".IUGU_API_TOKE,
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => FALSE,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    $resp_iugu_cliente = json_decode($response);
                } else {
                    $cliente = $Clientes
                        ->find('all')
                        ->contain(['Cities'])
                        ->where(['Clientes.id' => $PedidoCliente->cliente_id])
                        ->first();

                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $data = [
                        "api_token" => IUGU_API_TOKEN,
                        "email"     => $cliente->email,
                        "name"      => $cliente->name,
                        "cpf_cnpj"  => str_replace('.', '', $cliente->cpf),
                        "zip_code"  => str_replace('.', '', $PedidoCliente->academia->cep),
                        "number"    => $PedidoCliente->academia->number,
                        "street"    => $PedidoCliente->academia->address,
                        "city"      => $PedidoCliente->academia->city->name,
                        "state"     => $PedidoCliente->academia->city->uf,
                        "district"  => $PedidoCliente->academia->area
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/customers",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    $resp_iugu_cliente = json_decode($response);

                    $data = [
                        'iugu_cliente_id' => $resp_iugu_cliente->id
                    ];

                    $cliente = $Clientes->patchEntity($cliente, $data);
                    $Clientes->save($cliente);
                }
                
                $valor = number_format($PedidoCliente->valor, 2, '', '');

                $valor = str_replace('.', '', $valor);

                $items_data[] = [
                    "description" => 'Plano Personalizado',
                    "quantity" => 1,
                    "price_cents" => $valor
                ];

                $total_parcelado = $valor * 0.01;

                if(isset($this->request->data['token'])) {

                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $prefixo = explode('(', $PedidoCliente->cliente->telephone);
                    $prefixo = explode(')', $prefixo[1]);

                    $address_data = [
                        "street" => $PedidoCliente->academia->address,
                        "number" => $PedidoCliente->academia->number,
                        "city" => $PedidoCliente->academia->city->name,
                        "state" => $PedidoCliente->academia->city->uf,
                        "country" => 'Brasil',
                        "zip_code" => $PedidoCliente->academia->cep,
                        "district" => $PedidoCliente->academia->area
                    ];

                    $payer_data = [
                        "name" => $PedidoCliente->cliente->name,
                        "phone_prefix" => $prefixo[0],
                        "phone" => $prefixo[1],
                        "email" => $PedidoCliente->cliente->email,
                        "address" => $address_data,
                        "cpf_cnpj" => $PedidoCliente->cliente->cpf
                    ];

                    $custom_variables_data[] = [
                        "name" => 'tipo',
                        "value" => 'LOGMensalidades'
                    ];

                    $data = [
                        "api_token"        => IUGU_API_TOKEN,
                        "token"            => $this->request->data['token'],
                        "customer_id"      => $resp_iugu_cliente->id,
                        "email"            => $PedidoCliente->cliente->email,
                        "months"           => $this->request->data['parcelas'],
                        "items"            => $items_data,
                        "payer"            => $payer_data,
                        "custom_variables" => $custom_variables_data
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/charge",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    $resp_iugu_cartao = json_decode($response);

                    if($resp_iugu_cartao->success) {
                        if($resp_iugu_cartao->message == 'Autorizado') {
                            $PedidoCliente->data_pagamento   = Time::now();
                            $PedidoCliente->pedido_status_id = 3;
                        } else {
                            $PedidoCliente->pedido_status_id = 2;
                        }
                        $PedidoCliente->tid              = $resp_iugu_cartao->invoice_id;
                        $PedidoCliente->iugu_token       = $this->request->data['token'];
                        $PedidoCliente->parcelas         = $this->request->data['parcelas'];
                        $PedidoCliente->valor_parcelado  = $total_parcelado;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_cartao->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_cartao->pdf;
                        $PedidoCliente->iugu_brand       = $this->request->data['iugu_brand'];
                        $PedidoCliente->identification   = $resp_iugu_cartao->identification;
                        $PedidoCliente->portador_name    = $this->request->data['portador_name'];
                        $PedidoCliente->portador_birth   = $this->request->data['portador_birth'];
                        $PedidoCliente->portador_phone   = $this->request->data['portador_phone'];
                        $PedidoCliente->portador_cpf     = $this->request->data['portador_cpf'];
                        $PedidoCliente->payment_method   = 'CartaoDeCredito';
                        $PedidoCliente->transaction_data = serialize($response);
                        
                        if($save_pedido = $Pedidos->save($PedidoCliente)) {
                            Email::configTransport('cliente6', [
                                'className' => 'Smtp',
                                'host' => 'mail.logfitness.com.br',
                                'port' => 587,
                                'timeout' => 30,
                                'username' => 'uhull@logfitness.com.br',
                                'password' => 'Lvrt6174?',
                                'client' => null,
                                'tls' => null,
                            ]);

                            $email = new Email();

                            $email
                                ->transport('cliente6')
                                ->template('plano_adquirido')
                                ->emailFormat('html')
                                ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                                ->to($PedidoCliente->cliente->email)
                                ->subject($PedidoCliente->academia->shortname.' - LogFitness - É hora de treinar')
                                ->set([
                                    'name'              => $PedidoCliente->cliente->name,
                                    'academia'          => $PedidoCliente->academia->shortname,
                                    'logo_academia'     => $PedidoCliente->academia->image,
                                    'slug'              => $PedidoCliente->academia->slug,
                                    'plano_name'        => 'personalizado',
                                    'data_compra'       => $PedidoCliente->created->format('d/m/Y')
                                ])
                                ->send();

                            return 'cartao1';
                        } else {
                            return 'cartao2';
                        }
                    } else {
                        $PedidoCliente->tid              = $resp_iugu_cartao->invoice_id;
                        $PedidoCliente->iugu_token       = $this->request->data['token'];
                        $PedidoCliente->parcelas         = $this->request->data['parcelas'];
                        $PedidoCliente->valor_parcelado  = $total_parcelado;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_cartao->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_cartao->pdf;
                        $PedidoCliente->iugu_brand       = $this->request->data['iugu_brand'];
                        $PedidoCliente->identification   = $resp_iugu_cartao->identification;
                        $PedidoCliente->portador_name    = $this->request->data['portador_name'];
                        $PedidoCliente->portador_birth   = $this->request->data['portador_birth'];
                        $PedidoCliente->portador_phone   = $this->request->data['portador_phone'];
                        $PedidoCliente->portador_cpf     = $this->request->data['portador_cpf'];
                        $PedidoCliente->pedido_status_id = 1;
                        $PedidoCliente->transaction_data = serialize($response);
                        $Pedidos->save($PedidoCliente);

                        return 'cartao2';
                    }

                } else {
                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $prefixo = explode('(', $PedidoCliente->cliente->telephone);
                    $prefixo = explode(')', $prefixo[1]);

                    $address_data = [
                        "street" => $PedidoCliente->academia->address,
                        "number" => $PedidoCliente->academia->number,
                        "city" => $PedidoCliente->academia->city->name,
                        "state" => $PedidoCliente->academia->city->uf,
                        "country" => 'Brasil',
                        "zip_code" => $PedidoCliente->academia->cep,
                        "district" => $PedidoCliente->academia->area
                    ];

                    $payer_data = [
                        "name" => $PedidoCliente->cliente->name,
                        "phone_prefix" => $prefixo[0],
                        "phone" => $prefixo[1],
                        "email" => $PedidoCliente->cliente->email,
                        "address" => $address_data,
                        "cpf_cnpj" => $PedidoCliente->cliente->cpf
                    ];

                    $custom_variables_data[] = [
                        "name" => 'tipo',
                        "value" => 'LOGMensalidades'
                    ];

                    $data = [
                        "api_token"        => IUGU_API_TOKEN,
                        "method"           => 'bank_slip',
                        "email"            => $PedidoCliente->cliente->email,
                        "months"           => 1,
                        "items"            => $items_data,
                        "payer"            => $payer_data,
                        "custom_variables" => $custom_variables_data
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/charge",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    $resp_iugu_boleto = json_decode($response);

                    if($resp_iugu_boleto->success) {
                        $PedidoCliente->tid              = $resp_iugu_boleto->invoice_id;
                        $PedidoCliente->identification   = $resp_iugu_boleto->identification;
                        $PedidoCliente->payment_method   = 'BoletoBancario';
                        $PedidoCliente->parcelas         = 1;
                        $PedidoCliente->valor_parcelado  = null;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_boleto->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_boleto->pdf;
                        $PedidoCliente->pedido_status_id = 2;
                        $PedidoCliente->transaction_data = serialize($response);
                        if($save_pedido = $Pedidos->save($PedidoCliente)) {
                            Email::configTransport('cliente2', [
                                'className' => 'Smtp',
                                'host' => 'mail.logfitness.com.br',
                                'port' => 587,
                                'timeout' => 30,
                                'username' => 'uhull@logfitness.com.br',
                                'password' => 'Lvrt6174?',
                                'client' => null,
                                'tls' => null,
                            ]);

                            $email = new Email();

                            $email
                                ->transport('cliente2')
                                ->template('boleto_plano')
                                ->emailFormat('html')
                                ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                                ->to($PedidoCliente->cliente->email)
                                ->subject($PedidoCliente->academia->shortname.' - LogFitness - Boleto')
                                ->set([
                                    'name'              => $PedidoCliente->cliente->name,
                                    'academia'          => $PedidoCliente->academia->shortname,
                                    'logo_academia'     => $PedidoCliente->academia->image,
                                    'boleto'            => $resp_iugu_boleto->pdf
                                ])
                                ->send();

                            return array('boleto' => 'boleto', 'url_pdf' => $resp_iugu_boleto->pdf);
                        } else {
                            return 'boleto2';
                        }
                    } else {
                        $PedidoCliente->tid              = $resp_iugu_boleto->invoice_id;
                        $PedidoCliente->identification   = $resp_iugu_boleto->identification;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_boleto->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_boleto->pdf;
                        $PedidoCliente->pedido_status_id = 1;
                        $PedidoCliente->transaction_data = serialize($response);
                        $Pedidos->save($PedidoCliente);

                        return 'boleto2';
                    }
                }
            }
        }
    }

    public function iugu_cobranca_direta_plano ($pedido_id = null, $qtd_boletos = 0, $qtd_boletos_total = 1) {
        if($pedido_id != null) { 
            if($this->request->is(['post', 'put'])) {
                $Pedidos  = TableRegistry::get('Pedidos');
                $Clientes = TableRegistry::get('Clientes');

                $PedidoCliente  = $Pedidos
                    ->find('all', ['contain' => [
                        'Academias',
                        'Academias.Cities',
                        'Clientes',
                        'PedidoItens',
                        'PedidoItens.Produtos',
                        'PedidoItens.Produtos.ProdutoBase',
                        'PedidoItens.Produtos.ProdutoBase.Marcas'
                    ]
                    ])
                    ->where(['Pedidos.id' => $pedido_id])
                    ->first(); 
                if($PedidoCliente->cliente->iugu_cliente_id != null) {
                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/customers/".$PedidoCliente->cliente->iugu_cliente_id."/?api_token=".IUGU_API_TOKE,
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => FALSE,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    $resp_iugu_cliente = json_decode($response);
                } else {
                    $cliente = $Clientes
                        ->find('all')
                        ->contain(['Cities'])
                        ->where(['Clientes.id' => $PedidoCliente->cliente_id])
                        ->first();

                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $data = [
                        "api_token" => IUGU_API_TOKEN,
                        "email"     => $cliente->email,
                        "name"      => $cliente->name,
                        "cpf_cnpj"  => str_replace('.', '', $cliente->cpf),
                        "zip_code"  => str_replace('.', '', $PedidoCliente->academia->cep),
                        "number"    => $PedidoCliente->academia->number,
                        "street"    => $PedidoCliente->academia->address,
                        "city"      => $PedidoCliente->academia->city->name,
                        "state"     => $PedidoCliente->academia->city->uf,
                        "district"  => $PedidoCliente->academia->area
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/customers",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    $resp_iugu_cliente = json_decode($response);

                    $data = [
                        'iugu_cliente_id' => $resp_iugu_cliente->id
                    ];

                    $cliente = $Clientes->patchEntity($cliente, $data);
                    $Clientes->save($cliente);
                }

                foreach ($PedidoCliente->pedido_itens as $pi) {

                    $valor = number_format($pi->preco, 2, '', '');
                    
                    $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

                    $plano = $AcademiaMensalidadesPlanos
                        ->find('all')
                        ->where(['produto_id' => $pi->produto->id])
                        ->first(); 

                    $items_data[] = [
                        "description" => $PedidoCliente->academia->shortname.' - '.$plano->name,
                        "quantity" => $pi->quantidade,
                        "price_cents" => number_format($plano->valor_total/$qtd_boletos_total, 2, '', '')
                    ];

                    $total_parcelado += $pi->quantidade * $valor;
                }

                $total_parcelado = $total_parcelado * 0.01;

                if(isset($this->request->data['token'])) {

                    // CRIA MÉTODO DE PAGAMENTO DO CLIENTE
                    if($PedidoCliente->cliente->iugu_cliente_payment_id != null) {
                        $ch = curl_init();

                        $header = [
                            "Content-Type: application/json"
                        ];

                        $data = json_encode($data);

                        $options = array(CURLOPT_URL => "https://api.iugu.com/v1/customers/".$PedidoCliente->cliente->iugu_cliente_id."/payment_methods/".$PedidoCliente->cliente->iugu_cliente_payment_id."/?api_token=".IUGU_API_TOKE,
                            CURLOPT_RETURNTRANSFER => TRUE,
                            CURLOPT_HEADER => FALSE,
                            CURLOPT_POST => FALSE,
                            CURLOPT_HTTPHEADER => $header
                        );

                        curl_setopt_array($ch, $options);

                        $response = curl_exec($ch);
                        curl_close($ch);

                        $resp_iugu_cliente_payment = json_decode($response);
                    } else {

                        $cliente = $Clientes
                            ->find('all')
                            ->contain(['Cities'])
                            ->where(['Clientes.id' => $PedidoCliente->cliente_id])
                            ->first();

                        $ch = curl_init();

                        $header = [
                            "Content-Type: application/json"
                        ];

                        $data = [
                            "api_token"   => IUGU_API_TOKEN,
                            "description" => 'Meu cartão de crédito',
                            "token"       => $this->request->data['token']
                        ];

                        $data = json_encode($data);

                        $options = array(CURLOPT_URL => "https://api.iugu.com/v1/customers/".$PedidoCliente->cliente->iugu_cliente_id."/payment_methods",
                            CURLOPT_RETURNTRANSFER => TRUE,
                            CURLOPT_HEADER => FALSE,
                            CURLOPT_POST => TRUE,
                            CURLOPT_POSTFIELDS => $data,
                            CURLOPT_HTTPHEADER => $header
                        );

                        curl_setopt_array($ch, $options);

                        $response = curl_exec($ch);
                        curl_close($ch);

                        $resp_iugu_cliente_payment = json_decode($response);

                        $data = [
                            'iugu_cliente_payment_id' => $resp_iugu_cliente_payment->id
                        ];

                        $cliente = $Clientes->patchEntity($cliente, $data);
                        $Clientes->save($cliente);
                    }

                    // REALIZA PAGAMENTO

                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $prefixo = explode('(', $PedidoCliente->cliente->telephone);
                    $prefixo = explode(')', $prefixo[1]);

                    $address_data = [
                        "street" => $PedidoCliente->academia->address,
                        "number" => $PedidoCliente->academia->number,
                        "city" => $PedidoCliente->academia->city->name,
                        "state" => $PedidoCliente->academia->city->uf,
                        "country" => 'Brasil',
                        "zip_code" => $PedidoCliente->academia->cep,
                        "district" => $PedidoCliente->academia->area
                    ];

                    $payer_data = [
                        "name" => $PedidoCliente->cliente->name,
                        "phone_prefix" => $prefixo[0],
                        "phone" => $prefixo[1],
                        "email" => $PedidoCliente->cliente->email,
                        "address" => $address_data,
                        "cpf_cnpj" => $PedidoCliente->cliente->cpf
                    ];

                    $custom_variables_data[] = [
                        "name" => 'tipo',
                        "value" => 'LOGMensalidades'
                    ];

                    $data = [
                        "api_token"        => IUGU_API_TOKEN,
                        "token"            => $this->request->data['token'],
                        "customer_id"      => $resp_iugu_cliente->id,
                        "email"            => $PedidoCliente->cliente->email,
                        "months"           => $this->request->data['parcelas'],
                        "items"            => $items_data,
                        "payer"            => $payer_data,
                        "custom_variables" => $custom_variables_data
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/charge",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    $resp_iugu_cartao = json_decode($response);

                    if($resp_iugu_cartao->success) {
                        if($resp_iugu_cartao->message == 'Autorizado') {
                            $PedidoCliente->data_pagamento   = Time::now();
                            $PedidoCliente->pedido_status_id = 3;
                        } else {
                            $PedidoCliente->pedido_status_id = 2;
                        }
                        $PedidoCliente->tid              = $resp_iugu_cartao->invoice_id;
                        $PedidoCliente->iugu_token       = $this->request->data['token'];
                        $PedidoCliente->parcelas         = $this->request->data['parcelas'];
                        $PedidoCliente->valor_parcelado  = $total_parcelado;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_cartao->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_cartao->pdf;
                        $PedidoCliente->iugu_brand       = $this->request->data['iugu_brand'];
                        $PedidoCliente->identification   = $resp_iugu_cartao->identification;
                        $PedidoCliente->portador_name    = $this->request->data['portador_name'];
                        $PedidoCliente->portador_birth   = $this->request->data['portador_birth'];
                        $PedidoCliente->portador_phone   = $this->request->data['portador_phone'];
                        $PedidoCliente->portador_cpf     = $this->request->data['portador_cpf'];
                        $PedidoCliente->payment_method   = 'CartaoDeCredito';
                        $PedidoCliente->transaction_data = serialize($response);
                        
                        if($save_pedido = $Pedidos->save($PedidoCliente)) {

                            $PlanosClientes = TableRegistry::get('PlanosClientes');

                            $plano_cliente = $PlanosClientes->newEntity();

                            $duracao = $plano->time_months;

                            $now = Time::now();
                            $now->addMonths($duracao);

                            $data = [
                                'academia_mensalidades_planos_id' => $plano->id,
                                'cliente_id' => $save_pedido->cliente_id,
                                'type' => 'Plano',
                                'status' => 0,
                                'data_vencimento' => $now
                            ];

                            $plano_cliente = $PlanosClientes->patchEntity($plano_cliente, $data);

                            if($plano_saved = $PlanosClientes->save($plano_cliente)) {

                                $plano_mensalidade = TableRegistry::get('AcademiaMensalidadesPlanos')
                                    ->find('all')
                                    ->where(['id' => $plano->id])
                                    ->first();

                                $email = new Email();

                                $email
                                    ->transport('cliente2')
                                    ->template('plano_adquirido')
                                    ->emailFormat('html')
                                    ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                                    ->to($PedidoCliente->cliente->email)
                                    ->subject($PedidoCliente->academia->shortname.' - LogFitness - É hora de treinar')
                                    ->set([
                                        'name'              => $PedidoCliente->cliente->name,
                                        'academia'          => $PedidoCliente->academia->shortname,
                                        'logo_academia'     => $PedidoCliente->academia->image,
                                        'slug'              => $PedidoCliente->academia->slug,
                                        'plano_name'        => $plano_mensalidade->name,
                                        'data_compra'       => $PedidoCliente->created->format('d/m/Y')
                                    ])
                                    ->send();

                                $data = [
                                    'plano_cliente_id' => $plano_saved->id
                                ];

                                $Pedidos->patchEntity($PedidoCliente, $data);
                                $Pedidos->save($PedidoCliente);

                                return 'cartao1';
                            } else {
                                return 'cartao2';
                            }
                        } else {
                            return 'cartao2';
                        }
                    
                    } else {
                        $PedidoCliente->tid              = $resp_iugu_cartao->invoice_id;
                        $PedidoCliente->iugu_token       = $this->request->data['token'];
                        $PedidoCliente->parcelas         = $this->request->data['parcelas'];
                        $PedidoCliente->valor_parcelado  = $total_parcelado;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_cartao->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_cartao->pdf;
                        $PedidoCliente->iugu_brand       = $this->request->data['iugu_brand'];
                        $PedidoCliente->identification   = $resp_iugu_cartao->identification;
                        $PedidoCliente->portador_name    = $this->request->data['portador_name'];
                        $PedidoCliente->portador_birth   = $this->request->data['portador_birth'];
                        $PedidoCliente->portador_phone   = $this->request->data['portador_phone'];
                        $PedidoCliente->portador_cpf     = $this->request->data['portador_cpf'];
                        $PedidoCliente->pedido_status_id = 1;
                        $PedidoCliente->transaction_data = serialize($response);
                        $Pedidos->save($PedidoCliente);

                        return 'cartao2';
                    }

                } else {
                    $ch = curl_init();

                    $header = [
                        "Content-Type: application/json"
                    ];

                    $prefixo = explode('(', $PedidoCliente->cliente->telephone);
                    $prefixo = explode(')', $prefixo[1]);

                    $address_data = [
                        "street" => $PedidoCliente->academia->address,
                        "number" => $PedidoCliente->academia->number,
                        "city" => $PedidoCliente->academia->city->name,
                        "state" => $PedidoCliente->academia->city->uf,
                        "country" => 'Brasil',
                        "zip_code" => $PedidoCliente->academia->cep,
                        "district" => $PedidoCliente->academia->area
                    ];

                    $payer_data = [
                        "name" => $PedidoCliente->cliente->name,
                        "phone_prefix" => $prefixo[0],
                        "phone" => $prefixo[1],
                        "email" => $PedidoCliente->cliente->email,
                        "address" => $address_data,
                        "cpf_cnpj" => $PedidoCliente->cliente->cpf
                    ];

                    $custom_variables_data[] = [
                        "name" => 'tipo',
                        "value" => 'LOGMensalidades'
                    ];

                    $extra_days = 3 + (30 * $qtd_boletos);

                    $data = [
                        "api_token"            => IUGU_API_TOKEN,
                        "method"               => 'bank_slip',
                        "email"                => $PedidoCliente->cliente->email,
                        "months"               => 1,
                        "bank_slip_extra_days" => $extra_days,
                        "items"                => $items_data,
                        "payer"                => $payer_data,
                        "custom_variables"     => $custom_variables_data
                    ];

                    $data = json_encode($data);

                    $options = array(CURLOPT_URL => "https://api.iugu.com/v1/charge",
                        CURLOPT_RETURNTRANSFER => TRUE,
                        CURLOPT_HEADER => FALSE,
                        CURLOPT_POST => TRUE,
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => $header
                    );

                    curl_setopt_array($ch, $options);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    $resp_iugu_boleto = json_decode($response);

                    if($resp_iugu_boleto->success) {
                        $PedidoCliente->tid              = $resp_iugu_boleto->invoice_id;
                        $PedidoCliente->identification   = $resp_iugu_boleto->identification;
                        $PedidoCliente->payment_method   = 'BoletoBancario';
                        $PedidoCliente->parcelas         = $qtd_boletos_total;
                        $PedidoCliente->valor_parcelado  = null;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_boleto->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_boleto->pdf;
                        $PedidoCliente->pedido_status_id = 2;
                        $PedidoCliente->transaction_data = serialize($response);
                        if($save_pedido = $Pedidos->save($PedidoCliente)) {

                            $PlanosClientes = TableRegistry::get('PlanosClientes');

                            if($qtd_boletos == 0) {

                                $plano_cliente = $PlanosClientes->newEntity();

                                $duracao = ($academia_mensalidade_plano->time_months * 30) / $PedidoCliente->parcelas;

                                $time = new Time($plano_cliente->data_vencimento);
                                $vencimento = $time->addDays($duracao);

                                $data = [
                                    'academia_mensalidades_planos_id' => $plano->id,
                                    'cliente_id' => $save_pedido->cliente_id,
                                    'type' => 'Plano',
                                    'status' => 0,
                                    'data_vencimento' => $vencimento
                                ];

                                $plano_cliente = $PlanosClientes->patchEntity($plano_cliente, $data);

                                if($plano_saved = $PlanosClientes->save($plano_cliente)) {

                                    $data = [
                                        'plano_cliente_id' => $plano_saved->id
                                    ];

                                    $Pedidos->patchEntity($PedidoCliente, $data);
                                    $Pedidos->save($PedidoCliente);

                                    if($qtd_boletos_total <= 1) {
                                        $email = new Email();

                                        $email
                                            ->transport('cliente2')
                                            ->template('boleto_plano')
                                            ->emailFormat('html')
                                            ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                                            ->to($PedidoCliente->cliente->email)
                                            ->subject($PedidoCliente->academia->shortname.' - LogFitness - Boleto')
                                            ->set([
                                                'name'              => $PedidoCliente->cliente->name,
                                                'academia'          => $PedidoCliente->academia->shortname,
                                                'logo_academia'     => $PedidoCliente->academia->image,
                                                'slug'              => $PedidoCliente->academia->slug,
                                                'boleto'            => $resp_iugu_boleto->pdf
                                            ])
                                            ->send();
                                    }

                                    return array('boleto' => 'boleto', 'url_pdf' => $resp_iugu_boleto->pdf);
                                } else {
                                    return 'boleto2';
                                }
                            } else {

                                $plano_criado = $PlanosClientes
                                    ->find('all')
                                    ->where(['cliente_id' => $save_pedido->cliente_id])
                                    ->andWhere(['academia_mensalidades_planos_id' => $plano->id])
                                    ->last();

                                $data = [
                                    'plano_cliente_id' => $plano_criado->id
                                ];

                                $Pedidos->patchEntity($PedidoCliente, $data);
                                $Pedidos->save($PedidoCliente);

                                if($qtd_boletos_total <= 1) {
                                    $email = new Email();

                                    $email
                                        ->transport('cliente2')
                                        ->template('boleto_plano')
                                        ->emailFormat('html')
                                        ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                                        ->to($PedidoCliente->cliente->email)
                                        ->subject($PedidoCliente->academia->shortname.' - LogFitness - Boleto')
                                        ->set([
                                            'name'              => $PedidoCliente->cliente->name,
                                            'academia'          => $PedidoCliente->academia->shortname,
                                            'logo_academia'     => $PedidoCliente->academia->image,
                                            'slug'              => $PedidoCliente->academia->slug,
                                            'boleto'            => $resp_iugu_boleto->pdf
                                        ])
                                        ->send();
                                }

                                return array('boleto' => 'boleto', 'url_pdf' => $resp_iugu_boleto->pdf);
                            }
                        } else {
                            return 'boleto2';
                        }
                    } else {
                        $PedidoCliente->tid              = $resp_iugu_boleto->invoice_id;
                        $PedidoCliente->identification   = $resp_iugu_boleto->identification;
                        $PedidoCliente->iugu_payment_url = $resp_iugu_boleto->url;
                        $PedidoCliente->iugu_payment_pdf = $resp_iugu_boleto->pdf;
                        $PedidoCliente->pedido_status_id = 1;
                        $PedidoCliente->transaction_data = serialize($response);
                        $Pedidos->save($PedidoCliente);

                        return 'boleto2';
                    }
                }
            }
        }
    }

    public function mensalidade($slug = null){
        $session  = $this->request->session();
        $Academia = $session->read('Academia');

        $Avaliacoes                 = TableRegistry::get('Avaliacoes');
        $PlanosClientes             = TableRegistry::get('PlanosClientes');
        $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
        $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

        if($slug == null && !$Academia){
            $this->redirect('/');
        }

        if($slug) {
            $academia = $this->Academias
                ->find('all')
                ->contain(['Cities'])
                ->where(['Academias.slug' => $slug])
                ->andWhere(['Academias.status_id' => 1])
                ->first();

            $session->write('AcademiaMensalidade', $academia);

            if(!$Academia) {
                $session->write('Academia', $academia);
                $Academia = $session->read('Academia');
            }
        } else if($Academia) {
            $academia = $this->Academias
                ->find('all')
                ->contain(['Cities'])
                ->where(['Academias.id' => $Academia->id])
                ->andWhere(['Academias.status_id' => 1])
                ->first();
        }

        if(!$academia) {
            return $this->redirect('/home');
        }

        $mensalidade = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $academia->id])
            ->first();

        $planos = $AcademiaMensalidadesPlanos
            ->find('all')
            ->where(['academia_mensalidades_id' => $mensalidade->id])
            ->andWhere(['visivel' => 1])
            ->all();

        foreach ($planos as $plano) {
            $ids_produtos[] = $plano->produto_id;
        }

        if(count($ids_produtos) >= 1) {
            $produtos = TableRegistry::get('Produtos')
                ->find('all')
                ->where(['id IN' => $ids_produtos])
                ->andWhere(['estoque >=' => 1])
                ->all();
        }

        foreach ($produtos as $produto) {
            foreach ($planos as $plano) {
                if($plano->produto_id == $produto->id) {
                    $ids_planos[] = $plano->id;
                }
            }
        }

        if(count($ids_planos) >= 1) {
            $planos = $AcademiaMensalidadesPlanos
                ->find('all')
                ->where(['id IN' => $ids_planos])
                ->all();
        }

        $avaliacoes = $Avaliacoes
            ->find('all')
            ->contain('Clientes')
            ->where(['Avaliacoes.academia_id' => $academia->id])
            ->all();

        $planos_clientes = $PlanosClientes
            ->find('all')
            ->where(['status' => 1])
            ->all();

        foreach ($avaliacoes as $avaliacao) {
            $estrelas += $avaliacao->rating;
        }

        $total_estrelas = $estrelas / count($avaliacoes);

        $this->set(compact('academia', 'mensalidade', 'planos', 'total_estrelas', 'planos_clientes'));
        $this->viewBuilder()->layout('profiles');
    }

    public function admin_mensalidades_dashboard(){

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $Pedidos                    = TableRegistry::get('Pedidos');
        $AcademiaSaques             = TableRegistry::get('AcademiaSaques');
        $PlanosClientes             = TableRegistry::get('PlanosClientes');
        $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
        $MensalidadesCategorias     = TableRegistry::get('MensalidadesCategorias');
        $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

        $date = new \DateTime(date('Y-m').'-01');

        $academia_mensalidades = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->first();

        $mensalidade_categoria = $MensalidadesCategorias
            ->find('all')
            ->where(['id' => $academia_mensalidades->mensalidades_categorias_id])
            ->first();

        $academia_mensalidades_planos = $AcademiaMensalidadesPlanos
            ->find('all')
            ->where(['academia_mensalidades_id' => $academia_mensalidades->id])
            ->all();

        $ch = curl_init();

        $header = [
            "Content-Type: application/json"
        ];

        $options = array(CURLOPT_URL => "https://api.iugu.com/v1/bank_verification?api_token=".$academia_mensalidades->iugu_live_api_token."&hl=pt-BR",
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => FALSE,
            CURLOPT_HTTPHEADER => $header
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $resposta_verificao_subconta = json_decode($response);

        if($resposta_verificao_subconta[0]->status == 'pending') {
            $this->Flash->info('Seus dados bancários estão em verificação!');
        }

        foreach ($academia_mensalidades_planos as $amp) {
            $planos_id[] = $amp->id;
        }

        if(count($planos_id) >= 1) {

            $planos_vendidos_array1 = $PlanosClientes
                ->find('all')
                ->where(['created >=' => $date])
                ->andWhere(['status' => 1])
                ->andWhere(['academia_mensalidades_planos_id IN' => $planos_id])
                ->all();

            foreach ($planos_vendidos_array1 as $pva1) {
                $planos_vendidos[] = $pva1;
            }

            $today_ativos = time::now();
            $today_ativos->addDays(5);

            $today = time::now();

            $planos_clientes_ativos = $PlanosClientes
                ->find('all')
                ->contain(['AcademiaMensalidadesPlanos', 'Clientes'])
                ->where(['PlanosClientes.academia_mensalidades_planos_id IN' => $planos_id])
                ->andWhere(['PlanosClientes.data_vencimento > ' => $today_ativos])
                ->andWhere(['PlanosClientes.status' => 1])
                ->all();

            $planos_clientes_vencer = $PlanosClientes
                ->find('all')
                ->contain(['AcademiaMensalidadesPlanos', 'Clientes'])
                ->where(['PlanosClientes.academia_mensalidades_planos_id IN' => $planos_id])
                ->andWhere(['PlanosClientes.data_vencimento >= ' => $today])
                ->andWhere(['PlanosClientes.data_vencimento <= ' => $today_ativos])
                ->andWhere(['PlanosClientes.status' => 1])
                ->all();

            $planos_clientes_vencidos = $PlanosClientes
                ->find('all')
                ->contain(['AcademiaMensalidadesPlanos', 'Clientes'])
                ->where(['PlanosClientes.academia_mensalidades_planos_id IN' => $planos_id])
                ->andWhere(['PlanosClientes.data_vencimento < ' => $today])
                ->andWhere(['PlanosClientes.status' => 1])
                ->all();
        } else {
            $planos_clientes_ativos   = [];
            $planos_clientes_vencer   = [];
            $planos_clientes_vencidos = [];
        }

        $planos_vendidos_array2 = $Pedidos
            ->find('all')
            ->where(['created >=' => $date])
            ->andWhere(['pedido_status_id >=' => 3])
            ->andWhere(['pedido_status_id NOT IN' => [7, 8]])
            ->andWhere(['academia_id' => $Academia->id])
            ->andWhere(['comissao_status' => 5])
            ->all();

        foreach ($planos_vendidos_array2 as $pva2) {
            $planos_vendidos[] = $pva2;
        }

        foreach ($planos_clientes_ativos as $plano_cliente_ativo) {
            $qtd_alunos_ativos++; 
        }

        foreach ($planos_clientes_vencer as $plano_cliente_vencer) {
            $qtd_alunos_vencer++; 
        }

        foreach ($planos_clientes_vencidos as $plano_cliente_vencidos) {
            $qtd_alunos_vencidos++; 
        }

        $ch = curl_init();

        $header = [
            "Content-Type: application/json"
        ];

        $options = array(CURLOPT_URL => "https://api.iugu.com/v1/accounts/".$academia_mensalidades->iugu_account_id."/?api_token=".$academia_mensalidades->iugu_live_api_token.'&hl=pt-BR',
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => FALSE,
            CURLOPT_HTTPHEADER => $header
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $resp_iugu_subconta_info = json_decode($response);

        for ($i = 0; $i <= 100; $i = $i + 100) {
            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/financial_transaction_requests?api_token=".IUGU_API_TOKEN."&limit=100&hl=pt-BR&start=".$i,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => FALSE,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_iugu_transactions = json_decode($response);

            foreach ($resp_iugu_transactions->items as $transaction) {
                $ids_transactions[$transaction->id] = $transaction->invoice_id;
                $data_transactions[$transaction->id] = $transaction;
            }
        }
        
        if(count($planos_id) >= 1) {
            $todos_planos = $PlanosClientes
                ->find('all')
                ->where(['PlanosClientes.academia_mensalidades_planos_id IN' => $planos_id])
                ->all();

            foreach ($todos_planos as $tplano) {
                $planos_clientes_id[] = $tplano->id;
            }
            
            if(count($planos_clientes_id) >= 1) {
                $pedidos1 = $Pedidos
                    ->find('all')
                    ->where(['pedido_status_id >=' => 3])
                    ->andWhere(['academia_id' => $Academia->id])
                    ->andWhere(['pedido_status_id NOT IN' => [7, 8]])
                    ->andWhere(['plano_cliente_id IN' => $planos_clientes_id])
                    ->all();

                foreach ($pedidos1 as $p1) {
                    $pedidos[] = $p1;
                }
            }
        }

        $pedidos2 = $Pedidos
            ->find('all')
            ->where(['pedido_status_id >=' => 3])
            ->andWhere(['academia_id' => $Academia->id])
            ->andWhere(['pedido_status_id NOT IN' => [7, 8]])
            ->andWhere(['comissao_status' => 5])
            ->all();

        foreach ($pedidos2 as $p2) {
            $pedidos[] = $p2;
        }

        $valor_total = 0;




        foreach ($pedidos as $pedido) {
            foreach ($ids_transactions as $key => $id_transaction) {
                if($id_transaction == $pedido->tid) {

                    $valor = explode(' ', $data_transactions[$key]->total);
                    if($valor[1] == 'BRL') {
                        $valor = str_replace(',', '.', $valor[0]);
                    } else {
                        $valor = str_replace(',', '.', $valor[1]);
                    }
                    $valor = (double)$valor;
                    $qtd_parcelas = (int)$data_transactions[$key]->number_of_installments;
                    $parcela_atual = (int)$data_transactions[$key]->installment;
                    if($qtd_parcelas == 1) {
                        $taxa = ($valor * $mensalidade_categoria->tax_card_porcent1) * .01;
                        $taxa = $taxa + $mensalidade_categoria->tax_card_valor1;
                    } else if($qtd_parcelas >= 2 && $qtd_parcelas <= 3) {
                        $taxa = ($valor * $mensalidade_categoria->tax_card_porcent2) * .01;
                        if($parcela_atual == 1) {
                            $taxa = $taxa + $mensalidade_categoria->tax_card_valor2;
                        }
                    } else if($qtd_parcelas >= 4 && $qtd_parcelas <= 6) {
                        $taxa = ($valor * $mensalidade_categoria->tax_card_porcent3) * .01;
                        if($parcela_atual == 1) {
                            $taxa = $taxa + $mensalidade_categoria->tax_card_valor3;
                        }
                    } else if($qtd_parcelas >= 7) {
                        $taxa = ($valor * $mensalidade_categoria->tax_card_porcent4) * .01;
                        if($parcela_atual == 1) {
                            $taxa = $taxa + $mensalidade_categoria->tax_card_valor4;
                        }
                    }
                    $total = $valor - $taxa;
                    $valor_total = $valor_total + $total;
                }
            }
        }

        $now = Time::now();
        $now->subDays(1);

        $time = new Time('2017-05-14 18:00:00');

        $time->setDate($now->format('Y'), $now->format('m'), $now->format('d'));

        $em_transito = $AcademiaSaques
            ->find('all')
            ->where(['academia_mensalidades_id' => $academia_mensalidades->id])
            ->andWhere(['created >=' => $time])
            ->all();

        $valor_transito = 0.0;

        foreach ($em_transito as $transito) {
            $valor_transito += $transito->valor;
        }

        $this->set(compact('planos_vendidos', 'qtd_alunos_ativos', 'qtd_alunos_vencer', 'qtd_alunos_vencidos', 'resp_iugu_subconta_info', 'valor_total', 'valor_transito'));
        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_mensalidades_transferencia(){
        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $AcademiaMensalidades = TableRegistry::get('AcademiaMensalidades');

        $academia_mensalidades = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->first();

        $ch = curl_init();

        $header = [
            "Content-Type: application/json"
        ];

        $options = array(CURLOPT_URL => "https://api.iugu.com/v1/accounts/".$academia_mensalidades->iugu_account_id."/?api_token=".$academia_mensalidades->iugu_live_api_token,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => FALSE,
            CURLOPT_HTTPHEADER => $header
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $resp_iugu_subconta_info = json_decode($response);

        $ch = curl_init();

        $header = [
            "Content-Type: application/json"
        ];

        $options = array(CURLOPT_URL => "https://api.iugu.com/v1/bank_verification?api_token=".$academia_mensalidades->iugu_live_api_token."&hl=pt-BR",
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => FALSE,
            CURLOPT_HTTPHEADER => $header
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $resposta_verificao_subconta = json_decode($response);

        if($resposta_verificao_subconta[0]->status == 'pending') {
            $this->Flash->info('Seus dados bancários estão em verificação!');
        }

        $this->set(compact('resp_iugu_subconta_info'));
        $this->viewBuilder()->layout('admin_academia');
    }

    public function realizar_saque(){
        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        if($this->request->is(['patch', 'post', 'put'])) {

            $AcademiaMensalidades = TableRegistry::get('AcademiaMensalidades');

            $academia_mensalidades = $AcademiaMensalidades
                ->find('all')
                ->where(['academia_id' => $Academia->id])
                ->first();

            $valor_saque = $this->request->data['valor_transferencia'];

            $valor_saque = str_replace(".","",$valor_saque);
            $valor_saque = str_replace(",",".",$valor_saque);

            if($valor_saque >= 5.0) {
                $ch = curl_init();

                $header = [
                    "Content-Type: application/json"
                ];

                $data = [
                    "api_token"      => $academia_mensalidades->iugu_live_api_token,
                    "receiver_id"    => IUGU_ID,
                    "amount_cents"   => 200
                ];

                $data = json_encode($data);

                $options = array(CURLOPT_URL => "https://api.iugu.com/v1/transfers",
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $header
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $ch = curl_init();

                $header = [
                    "Content-Type: application/json"
                ];

                $valor_saque = number_format($valor_saque, 2, '.', '');
                
                $data = [
                    "api_token" => $academia_mensalidades->iugu_live_api_token,
                    "amount"    => $valor_saque
                ];

                $data = json_encode($data);

                $options = array(CURLOPT_URL => "https://api.iugu.com/v1/accounts/".$academia_mensalidades->iugu_account_id."/request_withdraw",
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $header
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $resp_iugu_saque = json_decode($response);

                if(!$resp_iugu_saque->errors) {
                    $Transferencias = TableRegistry::get('Transferencias');
                    $AcademiaSaques = TableRegistry::get('AcademiaSaques');

                    $transferencia = $Transferencias->newEntity();

                    $data = [
                        'academia_id' => $Academia->id,
                        'valor' => 2.00
                    ];

                    $transferencia = $Transferencias->patchEntity($transferencia, $data);

                    $Transferencias->save($transferencia);

                    $academia_saque = $AcademiaSaques->newEntity();

                    $data = [
                        'academia_mensalidades_id' => $academia_mensalidades->id,
                        'valor' => $valor_saque
                    ];

                    $academia_saque = $AcademiaSaques->patchEntity($academia_saque, $data);

                    if($AcademiaSaques->save($academia_saque)) {
                        $this->Flash->success('Saque no valor de R$'.number_format($valor_saque, 2, ',', '.').' realizado com sucesso!');
                        return $this->redirect('/academia/admin/admin_mensalidades/transferencia');
                    }
                } else {
                    $this->Flash->error('Falha ao tentar realizar saque... Tente novamente...');
                    return $this->redirect('/academia/admin/admin_mensalidades/transferencia');
                }
            } else {
                $this->Flash->error('Falha ao tentar realizar saque... Valor deve ser mais que R$5');
                return $this->redirect('/academia/admin/admin_mensalidades/transferencia');
            }
        }

        return $this->redirect('/academia/admin/admin_mensalidades/transferencia');
        $this->render(false);
    }

    public function admin_mensalidades_antecipar_recebiveis(){
        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $session->delete('Transactions');

        $Pedidos                    = TableRegistry::get('Pedidos');
        $PlanosClientes             = TableRegistry::get('PlanosClientes');
        $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
        $MensalidadesCategorias     = TableRegistry::get('MensalidadesCategorias');
        $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

        $academia_mensalidades = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->first();

        $mensalidade_categoria = $MensalidadesCategorias
            ->find('all')
            ->where(['id' => $academia_mensalidades->mensalidades_categorias_id])
            ->first();

        $academia_mensalidades_planos = $AcademiaMensalidadesPlanos
            ->find('all')
            ->where(['academia_mensalidades_id' => $academia_mensalidades->id])
            ->all();

        $ch = curl_init();

        $header = [
            "Content-Type: application/json"
        ];

        $options = array(CURLOPT_URL => "https://api.iugu.com/v1/bank_verification?api_token=".$academia_mensalidades->iugu_live_api_token."&hl=pt-BR",
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => FALSE,
            CURLOPT_HTTPHEADER => $header
        );

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);

        $resposta_verificao_subconta = json_decode($response);

        if($resposta_verificao_subconta[0]->status == 'pending') {
            $this->Flash->info('Seus dados bancários estão em verificação!');
        }

        foreach ($academia_mensalidades_planos as $amp) {
            $planos_id[] = $amp->id;
        }

        for ($i = 0; $i <= 100; $i = $i + 100) {
            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/financial_transaction_requests?api_token=".IUGU_API_TOKEN."&limit=100&hl=pt-BR&start=".$i,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => FALSE,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_iugu_transactions = json_decode($response);

            foreach ($resp_iugu_transactions->items as $transaction) {
                $ids_transactions[$transaction->id] = $transaction->invoice_id;
                $data_transactions[$transaction->id] = $transaction;
            }
        }

        if(count($planos_id) >= 1) {
            $todos_planos = $PlanosClientes
                ->find('all')
                ->where(['PlanosClientes.academia_mensalidades_planos_id IN' => $planos_id])
                ->all();

            foreach ($todos_planos as $tplano) {
                $planos_clientes_id[] = $tplano->id;
            }
 
        }
            
        if($planos_clientes_id) {
            $pedidos1 = $Pedidos
                ->find('all')
                ->where(['pedido_status_id >=' => 3])
                ->andWhere(['academia_id' => $Academia->id])
                ->andWhere(['pedido_status_id NOT IN' => [7, 8]])
                ->andWhere(['plano_cliente_id IN' => $planos_clientes_id])
                ->andWhere(['comissao_status' => 4])
                ->distinct('id')
                ->all();

            foreach ($pedidos1 as $p1) {
                $pedidos[] = $p1;
            }
        }

        $pedidos2 = $Pedidos
            ->find('all')
            ->where(['pedido_status_id >=' => 3])
            ->andWhere(['academia_id' => $Academia->id])
            ->andWhere(['pedido_status_id NOT IN' => [7, 8]])
            ->andWhere(['comissao_status' => 5])
            ->distinct('id')
            ->all();

        foreach ($pedidos2 as $p2) {
            $pedidos[] = $p2;
        }

        foreach ($pedidos as $pedido) {
            foreach ($ids_transactions as $key => $id_transaction) {
                if($id_transaction == $pedido->tid) {
                    $transactions[] = $data_transactions[$key];

                    $time = new Time($data_transactions[$key]->scheduled_date);
                    $today = time::now();

                    $dias_antecipados = $time->diff($today);

                    $dias_antecipados = $dias_antecipados->days + 1;

                    $taxa_antecipacao = pow((1 + ($mensalidade_categoria->tax_antecipacao / 100)),($dias_antecipados/30));
                    
                    $valor = explode(' ', $data_transactions[$key]->total);
                    if($valor[1] == 'BRL') {
                        $valor = str_replace(',', '.', $valor[0]);
                    } else {
                        $valor = str_replace(',', '.', $valor[1]);
                    }
                    $valor = (double)$valor;
                    $qtd_parcelas = (int)$data_transactions[$key]->number_of_installments;
                    $parcela_atual = (int)$data_transactions[$key]->installment;
                    if($qtd_parcelas == 1) {
                        $taxa = ($valor * $mensalidade_categoria->tax_card_porcent1) * .01;
                        $taxa = $taxa + $mensalidade_categoria->tax_card_valor1;
                    } else if($qtd_parcelas >= 2 && $qtd_parcelas <= 3) {
                        $taxa = ($valor * $mensalidade_categoria->tax_card_porcent2) * .01;
                        if($parcela_atual == 1) {
                            $taxa = $taxa + $mensalidade_categoria->tax_card_valor2;
                        }
                    } else if($qtd_parcelas >= 4 && $qtd_parcelas <= 6) {
                        $taxa = ($valor * $mensalidade_categoria->tax_card_porcent3) * .01;
                        if($parcela_atual == 1) {
                            $taxa = $taxa + $mensalidade_categoria->tax_card_valor3;
                        }
                    } else if($qtd_parcelas >= 7) {
                        $taxa = ($valor * $mensalidade_categoria->tax_card_porcent4) * .01;
                        if($parcela_atual == 1) {
                            $taxa = $taxa + $mensalidade_categoria->tax_card_valor4;
                        }
                    }
                    $total = $valor - $taxa;

                    $custo = $total - ($total/$taxa_antecipacao);

                    $custo = number_format($custo, 2);

                    $custo_final[$data_transactions[$key]->id] = $custo;

                    $valor_final[$data_transactions[$key]->id] = $total - $custo;

                    $transactions_id[$data_transactions[$key]->id] = $data_transactions[$key]->id;
                }
            }
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            foreach ($this->request->data as $key => $value) {
                $transaction = explode('_', $key);
                $transactions[] = $transaction[1];
            }

            $session->write('Transactions.'.$transactions, $transactions);

            return $this->redirect('/academia/admin/admin_mensalidades/recebiveis_confirmar');
        }

        $this->set(compact('planos_vendidos', 'qtd_alunos_ativos', 'qtd_alunos_vencer', 'qtd_alunos_vencidos', 'resp_iugu_subconta_info', 'valor_total', 'transactions', 'mensalidade_categoria', 'custo_final', 'valor_final'));
        $this->viewBuilder()->layout('admin_academia');
    }

    public function admin_mensalidades_antecipar_recebiveis_confirmar(){
        $session = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $transactions = $session->read('Transactions');

        $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
        $MensalidadesCategorias     = TableRegistry::get('MensalidadesCategorias');

        $academia_mensalidades = $AcademiaMensalidades
            ->find('all')
            ->contain(['Academias'])
            ->where(['AcademiaMensalidades.academia_id' => $Academia->id])
            ->first();

        $mensalidade_categoria = $MensalidadesCategorias
            ->find('all')
            ->where(['id' => $academia_mensalidades->mensalidades_categorias_id])
            ->first();


        for ($i = 0; $i <= 100; $i = $i + 100) {
            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/financial_transaction_requests?api_token=".IUGU_API_TOKEN."&limit=100&hl=pt-BR&start=".$i,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => FALSE,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_iugu_transactions = json_decode($response);

            foreach ($resp_iugu_transactions->items as $resp_transaction) {
                foreach ($transactions as $transaction) {
                    foreach ($transaction as $value) {
                        if ($resp_transaction->id == $value) {
                            $transactions_list[$resp_transaction->id] = $resp_transaction;

                            $time = new Time($resp_transaction->scheduled_date);
                            $today = time::now();

                            $dias_antecipados = $time->diff($today);

                            $dias_antecipados = $dias_antecipados->days + 1;

                            $taxa_antecipacao = pow((1 + ($mensalidade_categoria->tax_antecipacao / 100)),($dias_antecipados/30));
                            
                            $valor = explode(' ', $resp_transaction->total);
                            if($valor[1] == 'BRL') {
                                $valor = str_replace(',', '.', $valor[0]);
                            } else {
                                $valor = str_replace(',', '.', $valor[1]);
                            }
                            $valor = (double)$valor;
                            $qtd_parcelas = (int)$resp_transaction->number_of_installments;
                            $parcela_atual = (int)$resp_transaction->installment;
                            if($qtd_parcelas == 1) {
                                $taxa = ($valor * $mensalidade_categoria->tax_card_porcent1) * .01;
                                $taxa = $taxa + $mensalidade_categoria->tax_card_valor1;

                                $taxa_iugu_transferencia = ($valor * 2.51) * .01;
                            } else if($qtd_parcelas >= 2 && $qtd_parcelas <= 3) {
                                $taxa = ($valor * $mensalidade_categoria->tax_card_porcent2) * .01;
                                $taxa_iugu_transferencia = ($valor * 3.21) * .01;
                                if($parcela_atual == 1) {
                                    $taxa = $taxa + $mensalidade_categoria->tax_card_valor2;
                                } 
                            } else if($qtd_parcelas >= 4 && $qtd_parcelas <= 6) {
                                $taxa = ($valor * $mensalidade_categoria->tax_card_porcent3) * .01;
                                $taxa_iugu_transferencia = ($valor * 3.21) * .01;
                                if($parcela_atual == 1) {
                                    $taxa = $taxa + $mensalidade_categoria->tax_card_valor3;
                                }
                            } else if($qtd_parcelas >= 7) {
                                $taxa = ($valor * $mensalidade_categoria->tax_card_porcent4) * .01;
                                $taxa_iugu_transferencia = ($valor * 3.55) * .01;
                                if($parcela_atual == 1) {
                                    $taxa = $taxa + $mensalidade_categoria->tax_card_valor4;
                                }
                            }

                            $total = $valor - $taxa;

                            $custo = $total - ($total/$taxa_antecipacao);

                            $custo = number_format($custo, 2);

                            $custo_final[$resp_transaction->id] = $custo;

                            $valor_final[$resp_transaction->id] = $total - $custo;

                            $transactions_id[$resp_transaction->id] = $resp_transaction->id;
                            $transactions_invoice_id[$resp_transaction->id] = $resp_transaction->invoice_id;

                            $valor_a_receber += $valor_final[$resp_transaction->id];

                            $taxa_transferencia = $taxa - $taxa_iugu_transferencia;
                            $taxa_total += $taxa_transferencia;
                        }
                    }
                }
            }
        }

        $valor_a_receber = number_format($valor_a_receber, 2, '', '');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $Pedidos        = TableRegistry::get('Pedidos');
            $PlanosClientes = TableRegistry::get('PlanosClientes');

            $antecipado = 0;

            foreach ($transactions_invoice_id as $tiid) {

                $pedido = $Pedidos
                    ->find('all')
                    ->where(['tid' => $tiid])
                    ->first();

                if($pedido->comissao_status == 4) {
                    $plano_cliente = $PlanosClientes
                        ->find('all')
                        ->where(['id' => $pedido->plano_cliente_id])
                        ->first();

                    $data = [
                        'parcelas_antecipadas' => $plano_cliente->parcelas_antecipadas+1
                    ];      

                    $plano_cliente = $PlanosClientes->patchEntity($plano_cliente, $data);

                    if($PlanosClientes->save($plano_cliente)) {
                        $antecipado = 1;
                    }   
                } else if($pedido->comissao_status == 5) {
                    $data = [
                        'pedido_status_id' => 4
                    ];

                    $pedido = $Pedidos->patchEntity($pedido, $data);

                    if($Pedidos->save($pedido)) {
                        $antecipado = 1;
                    }
                }
            }

            // Antecipar faturas
            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            foreach ($transactions_id as $tid) {
                $transactions_final[] = $tid;
            }

            $data = [
                "api_token"    => IUGU_API_TOKEN,
                "transactions" => $transactions_final
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/financial_transaction_requests/advance",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            // Transferir valor
            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $data = [
                "api_token"      => IUGU_API_TOKEN,
                "receiver_id"    => $academia_mensalidades->iugu_account_id,
                "amount_cents"   => $valor_a_receber
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/transfers",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $Transferencias = TableRegistry::get('Transferencias');

            $transferencia = $Transferencias->newEntity();

            $data = [
                'academia_id' => $Academia->id,
                'valor' => $taxa_total
            ];

            $transferencia = $Transferencias->patchEntity($transferencia, $data);

            $Transferencias->save($transferencia);

            Email::configTransport('academia', [
                'className' => 'Smtp',
                'host' => 'mail.logfitness.com.br',
                'port' => 587,
                'timeout' => 30,
                'username' => 'uhull@logfitness.com.br',
                'password' => 'Lvrt6174?',
                'client' => null,
                'tls' => null,
            ]);

            $email = new Email();

            $email
                ->transport('academia')
                ->template('antecipacao')
                ->emailFormat('html')
                ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                ->to($academia_mensalidades->email_notificacoes)
                ->subject('LogFitness - Antecipação realizada com sucesso!')
                ->set([
                    'academia'      => $academia_mensalidades->academia->shortname,
                    'logo_academia' => $academia_mensalidades->academia->image,
                    'name'          => $academia_mensalidades->academia->shortname
                ])
                ->send();

            if($antecipado == 1) {
                $this->Flash->success('Fatura(s) antecipada(s) com sucesso!');
                return $this->redirect('/academia/admin/admin_mensalidades/recebiveis');
            } else { 
                $this->Flash->error('Não foi possível antecipar as faturas selecionadas... Tente novamente...');
                return $this->redirect('/academia/admin/admin_mensalidades/recebiveis');
            }
        }

        $this->set(compact('transactions_list', 'mensalidade_categoria', 'custo_final', 'valor_final'));
        $this->viewBuilder()->layout('admin_academia');
    }

    public function salvar_mensalidades_config_geral() {

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $academia = $this->Academias
                ->find('all')
                ->where(['id' => $Academia->id])
                ->first();

            $academia = $this->Academias->patchEntity($academia, $this->request->data);
            $academia_save = $this->Academias->save($academia);

            $AcademiaMensalidades = TableRegistry::get('AcademiaMensalidades');

            $academiamensalidade = $AcademiaMensalidades
                ->find('all')
                ->where(['academia_id' => $Academia->id])
                ->first();

            if($this->request->data['active_auto_transf'] == 'on') {
                $this->request->data['active_auto_transf'] = 1;
            } else {
                $this->request->data['active_auto_transf'] = 0;
            }

            if($this->request->data['email_notificacoes_toggle'] != 'on') {
                $this->request->data['email_notificacoes'] = '';
            }

            if($this->request->data['active_auto_antecipate'] != 'on') {
                $this->request->data['opt_antecipate'] = 0;
            }

            if($this->request->data['opt_antecipate'] == 1) {
                $this->request->data['days_antecipate'] = 0;
            } else if($this->request->data['opt_antecipate'] == 2) {
                $this->request->data['days_antecipate'] = $this->request->data['days_semanal_antecipate'];
            } else if($this->request->data['opt_antecipate'] == 3) {
                $this->request->data['days_antecipate'] = $this->request->data['days_mensal_antecipate'];
            } else if($this->request->data['opt_antecipate'] == 4) {
                $this->request->data['days_antecipate'] = $this->request->data['days_dias_antecipate'];
            }

            $academiamensalidade = $AcademiaMensalidades->patchEntity($academiamensalidade, $this->request->data);

            if ($academiamensalidade_save = $AcademiaMensalidades->save($academiamensalidade)) {
                if($academiamensalidade_save->account_verify == 0) {
                    if(!$academia_save->mens_tipo_conta || !$academia_save->mens_banco || !$academia_save->mens_agencia || 
                        !$academia_save->mens_conta) {
                        $this->Flash->success('Agora precisamos dos seus dados bancários!');
                        $this->redirect('/academia/admin/admin_mensalidades/config');
                    } else {
                        $this->iugu_verificar_subconta($academia_save->id);
                    }
                } else if($academiamensalidade_save->account_verify == 1) {
                    $this->iugu_configurar_subconta($academia_save->id);
                }
            } else {
                $this->Flash->error('Falha ao alterar configurações gerais... tente novamente...');
                return $this->redirect('/academia/admin/admin_mensalidades/config');
            }
        }

        return $this->redirect('/academia/admin/admin_mensalidades/config');
        $this->render(false);
    }

    public function salvar_mensalidades_config_boleto() {

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $AcademiaMensalidades   = TableRegistry::get('AcademiaMensalidades');

            $academiamensalidade = $AcademiaMensalidades
                ->find('all')
                ->where(['academia_id' => $Academia->id])
                ->first();

            if($this->request->data['opt_boleto'] == 'on') {
                $this->request->data['opt_boleto'] = 1;
            } else {
                $this->request->data['opt_boleto'] = 0;
            }

            $academiamensalidade = $AcademiaMensalidades->patchEntity($academiamensalidade, $this->request->data);

            if ($academiamensalidade_save = $AcademiaMensalidades->save($academiamensalidade)) {
                if($academiamensalidade_save->account_verify == 1) {
                    $this->iugu_configurar_subconta($academiamensalidade_save->academia_id);
                } else {
                    $this->Flash->success('Configurações de boleto alteradas com sucesso!');
                    return $this->redirect('/academia/admin/admin_mensalidades/config');
                }
            } else {
                $this->Flash->error('Falha ao alterar configurações de boleto... tente novamente...');
                return $this->redirect('/academia/admin/admin_mensalidades/config');
            }
        }

        return $this->redirect('/academia/admin/admin_mensalidades/config');
        $this->render(false);
    }

    public function salvar_mensalidades_config_card() {

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $AcademiaMensalidades   = TableRegistry::get('AcademiaMensalidades');

            $academiamensalidade = $AcademiaMensalidades
                ->find('all')
                ->where(['academia_id' => $Academia->id])
                ->first();

            if($this->request->data['opt_card'] == 'on') {
                $this->request->data['opt_card'] = 1;
            } else {
                $this->request->data['opt_card'] = 0;
            }

            if($this->request->data['active_parcelas_cartao'] != 'on') {
                $this->request->data['parcelas_card'] = 0;
            }

            if($this->request->data['opt_repasse'] == 'on') {
                $this->request->data['opt_repasse'] = 1;
            } else {
                $this->request->data['opt_repasse'] = 0;
            }

            $academiamensalidade = $AcademiaMensalidades->patchEntity($academiamensalidade, $this->request->data);

            if ($academiamensalidade_save = $AcademiaMensalidades->save($academiamensalidade)) {
                if($academiamensalidade_save->account_verify == 1) {
                    $this->iugu_configurar_subconta($academiamensalidade_save->academia_id);
                } else {
                    $this->Flash->success('Configurações de cartão alteradas com sucesso!');
                    return $this->redirect('/academia/admin/admin_mensalidades/config');
                }
            } else {
                $this->Flash->error('Falha ao alterar configurações de cartão... tente novamente...');
                return $this->redirect('/academia/admin/admin_mensalidades/config');
            }
        }

        return $this->redirect('/academia/admin/admin_mensalidades/config');
        $this->render(false);
    }

    public function planos_enviar_cobranca_avencer($id) {

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $PlanosClientes             = TableRegistry::get('PlanosClientes');
        $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
        $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

        $academia_mensalidades = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->first();

        $academia_mensalidades_planos = $AcademiaMensalidadesPlanos
            ->find('all')
            ->where(['academia_mensalidades_id' => $academia_mensalidades->id])
            ->all();

        foreach ($academia_mensalidades_planos as $amp) {
            $planos_id[] = $amp->id;
        }

        if(count($planos_id) >= 1) {

            $today_ativos = time::now();
            $today_ativos->addDays(5);

            $today = time::now();

            $planos_clientes_vencer = $PlanosClientes
                ->find('all')
                ->contain(['AcademiaMensalidadesPlanos', 'Clientes'])
                ->where(['PlanosClientes.id' => $id])
                ->andWhere(['PlanosClientes.academia_mensalidades_planos_id IN' => $planos_id])
                ->andWhere(['PlanosClientes.data_vencimento >= ' => $today])
                ->andWhere(['PlanosClientes.data_vencimento <= ' => $today_ativos])
                ->andWhere(['PlanosClientes.status' => 1])
                ->first();

            if($planos_clientes_vencer) {
                Email::configTransport('cliente', [
                    'className' => 'Smtp',
                    'host' => 'mail.logfitness.com.br',
                    'port' => 587,
                    'timeout' => 30,
                    'username' => 'uhull@logfitness.com.br',
                    'password' => 'Lvrt6174?',
                    'client' => null,
                    'tls' => null,
                ]);

                $email = new Email();

                $email
                    ->transport('cliente')
                    ->template('mensalidade_a_vencer')
                    ->emailFormat('html')
                    ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                    ->to($planos_clientes_vencer->cliente->email)
                    ->subject($Academia->shortname.' LogFitness - Plano à vencer')
                    ->set([
                        'name'              => $planos_clientes_vencer->cliente->name,
                        'data_vencimento'   => $planos_clientes_vencer->data_vencimento,
                        'academia'          => $Academia->shortname,
                        'logo_academia'     => $Academia->image,
                        'slug'              => $Academia->slug
                    ])
                    ->send();

                $this->Flash->success('Email enviado com sucesso!');
                return $this->redirect('/academia/admin/admin_mensalidades/planos');
            } else {
                $this->Flash->error('Falha ao enviar email!');
                return $this->redirect('/academia/admin/admin_mensalidades/planos');
            }
        }

        return $this->redirect('/academia/admin/admin_mensalidades/planos');
        $this->render(false);
    }

    public function planos_enviar_cobranca_vencido($id) {

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $PlanosClientes             = TableRegistry::get('PlanosClientes');
        $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
        $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

        $academia_mensalidades = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->first();

        $academia_mensalidades_planos = $AcademiaMensalidadesPlanos
            ->find('all')
            ->where(['academia_mensalidades_id' => $academia_mensalidades->id])
            ->all();

        foreach ($academia_mensalidades_planos as $amp) {
            $planos_id[] = $amp->id;
        }

        if(count($planos_id) >= 1) {

            $today = time::now();

            $planos_clientes_vencido = $PlanosClientes
                ->find('all')
                ->contain(['AcademiaMensalidadesPlanos', 'Clientes'])
                ->where(['PlanosClientes.id' => $id])
                ->andWhere(['PlanosClientes.academia_mensalidades_planos_id IN' => $planos_id])
                ->andWhere(['PlanosClientes.data_vencimento < ' => $today])
                ->andWhere(['PlanosClientes.status' => 1])
                ->first();

            if($planos_clientes_vencido) {
                Email::configTransport('cliente', [
                    'className' => 'Smtp',
                    'host' => 'mail.logfitness.com.br',
                    'port' => 587,
                    'timeout' => 30,
                    'username' => 'uhull@logfitness.com.br',
                    'password' => 'Lvrt6174?',
                    'client' => null,
                    'tls' => null,
                ]);

                $email = new Email();

                $email
                    ->transport('cliente')
                    ->template('mensalidade_vencida')
                    ->emailFormat('html')
                    ->from(['uhull@logfitness.com.br' => 'LogFitness'])
                    ->to($planos_clientes_vencido->cliente->email)
                    ->subject($Academia->shortname.' - LogFitness - Plano venceu!')
                    ->set([
                        'name'              => $planos_clientes_vencido->cliente->name,
                        'data_vencimento'   => $planos_clientes_vencido->data_vencimento,
                        'academia'          => $Academia->shortname,
                        'logo_academia'     => $Academia->image,
                        'slug'              => $Academia->slug
                    ])
                    ->send();

                $this->Flash->success('Email enviado com sucesso!');
                return $this->redirect('/academia/admin/admin_mensalidades/planos');
            } else {
                $this->Flash->success('Falha ao enviar email!');
                return $this->redirect('/academia/admin/admin_mensalidades/planos');
            }
        }

        return $this->redirect('/academia/admin/admin_mensalidades/planos');
        $this->render(false);
    }

    /**
     * CRIAR SUBCONTA
     */
    public function iugu_criar_subconta($academia_id = null) {
        if($academia_id != null) { 

            $academia = $this->Academias
                ->find('all')
                ->where(['id' => $academia_id])
                ->first();

            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $subconta_name = '#'.$academia->id.'-'.$academia->slug;

            $data = [
                "api_token" => IUGU_API_TOKEN,
                "name"      => $subconta_name
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/marketplace/create_account",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_iugu_subconta = json_decode($response);

            $AcademiaMensalidades = TableRegistry::get('AcademiaMensalidades');

            $academia_mensalidades = $AcademiaMensalidades
                ->find('all')
                ->where(['academia_id' => $academia->id])
                ->first();

            $data = [
                'iugu_account_id'     => $resp_iugu_subconta->account_id,
                'iugu_name'           => $resp_iugu_subconta->name,
                'iugu_live_api_token' => $resp_iugu_subconta->live_api_token,
                'iugu_test_api_token' => $resp_iugu_subconta->test_api_token,
                'iugu_user_token'     => $resp_iugu_subconta->user_token,
                'account_verify'      => 0,
                'feedback'            => serialize($response)
            ];

            $academia_mensalidades = $AcademiaMensalidades->patchEntity($academia_mensalidades, $data);

            if($AcademiaMensalidades->save($academia_mensalidades)) {
                return $this->redirect('/academia/admin/admin_mensalidades/config');
            } else {
                $this->Flash->error('Falha ao adquirir o LOGMensalidades... Tente novamente...');
                return $this->redirect('/academia/admin/admin_mensalidades');
            }
        }
    }

    /**
     * CANCELAR ASSINATURA
     */
    public function cancelar_plano($id = null) {

        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $PlanosClientes = TableRegistry::get('PlanosClientes');

        $plano = $PlanosClientes
            ->find('all')
            ->where(['id' => $id])
            ->first();

        if($plano) { 

            if($plano->type == 'Assinatura') {
                $ch = curl_init();

                $header = [
                    "Content-Type: application/json"
                ];

                $data = [
                    "api_token"  => IUGU_API_TOKEN,
                    "suspended"  => 'true'
                ];

                $data = json_encode($data);

                $options = array(CURLOPT_URL => "https://api.iugu.com/v1/subscriptions/".$plano->iugu_assinatura_id,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_CUSTOMREQUEST => "PUT",
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $header
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $resp_iugu_cancelar_assinatura = json_decode($response);

                if(!$resp_iugu_cancelar_assinatura->errors) {
                    $data = [
                        'status' => 0
                    ];

                    $plano = $PlanosClientes->patchEntity($plano, $data);

                    if($PlanosClientes->save($plano)) {
                        $this->Flash->success('Assinatura cancelada com sucesso!');
                        return $this->redirect('/academia/admin/admin_mensalidades/assinaturas');
                    } else {
                        $this->Flash->error('Não foi possível cancelar a assinatura... Tente novamente...');
                        return $this->redirect('/academia/admin/admin_mensalidades/assinaturas');
                    }
                } else {
                    $this->Flash->error('Não foi possível cancelar a assinatura... Tente novamente...');
                    return $this->redirect('/academia/admin/admin_mensalidades/assinaturas');
                }
            } else {
                $data = [
                    'status' => 0
                ];

                $plano = $PlanosClientes->patchEntity($plano, $data);

                if($PlanosClientes->save($plano)) {
                    $this->Flash->success('Plano cancelado com sucesso!');
                    return $this->redirect('/academia/admin/admin_mensalidades/planos');
                } else {
                    $this->Flash->error('Não foi possível cancelar o plano... Tente novamente...');
                    return $this->redirect('/academia/admin/admin_mensalidades/planos');
                }
            }

        }

        return $this->redirect('/academia/admin/admin_mensalidades/planos');
        $this->render(false);
    }

    /**
     * CANCELAR TODAS ASSINATURAS
     */
    public function cancelar_todas_assinaturas($id) {

        if($id != null) {
            $session  = $this->request->session();
            $Academia = $session->read('AdminAcademia');

            if(!$Academia) {
                $this->redirect('/academia/acesso');
            }

            $PlanosClientes             = TableRegistry::get('PlanosClientes');
            $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
            $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

            $academia_mensalidade = $AcademiaMensalidades
                ->find('all')
                ->where(['academia_id' => $Academia->id])
                ->first();

            $academia_mensalidades_planos = $AcademiaMensalidadesPlanos
                ->find('all')
                ->where(['id' => $id])
                ->andWhere(['academia_mensalidades_id' => $academia_mensalidade->id])
                ->first();

            if(!$academia_mensalidades_planos) {
                $this->Flash->error('Você não pode ativar/inativar essa assinatura!');
                return $this->redirect('/academia/admin/admin_mensalidades/assinaturas');
            }

            $planos_clientes = $PlanosClientes
                ->find('all')
                ->where(['academia_mensalidades_planos_id' => $academia_mensalidades_planos->id])
                ->all();

            foreach ($planos_clientes as $plano) {
                $ch = curl_init();

                $header = [
                    "Content-Type: application/json"
                ];

                $data = [
                    "api_token"  => IUGU_API_TOKEN,
                    "suspended"  => 'true'
                ];

                $data = json_encode($data);

                $options = array(CURLOPT_URL => "https://api.iugu.com/v1/subscriptions/".$plano->iugu_assinatura_id,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_CUSTOMREQUEST => "PUT",
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => $header
                );

                curl_setopt_array($ch, $options);

                $response = curl_exec($ch);
                curl_close($ch);

                $resp_iugu_cancelar_assinatura = json_decode($response);

                if(!$resp_iugu_cancelar_assinatura->errors) {
                    $data = [
                        'status' => 0
                    ];

                    $plano = $PlanosClientes->patchEntity($plano, $data);

                    if($PlanosClientes->save($plano)) {
                        $success = 1;
                    } else {
                        $success = 0;
                    }
                } else {
                    $success = 0;
                }
            }

            if($success == 1) {
                $data = [
                    'visivel' => 0
                ];

                $academia_mensalidades_planos = $AcademiaMensalidadesPlanos->patchEntity($academia_mensalidades_planos, $data);

                if ($AcademiaMensalidadesPlanos->save($academia_mensalidades_planos)) {
                    $this->Flash->success('Assinaturas canceladas com sucesso!');
                    return $this->redirect('/academia/admin/admin_mensalidades/assinaturas');
                }
            } else {
                $this->Flash->error('Falha ao cancelar todas as assinaturas... tente novamente...');
                return $this->redirect('/academia/admin/admin_mensalidades/assinaturas');
            }
        }

        return $this->redirect('/academia/admin/admin_mensalidades/planos');
        $this->viewBuilder()->layout('ajax');
        $this->render(false);
    }

    /**
     * ALTERAR DATA DE VENCIMENTO DE ASSINATURA
     */
    public function iugu_alterar_vencimento_assinatura($iugu_assinatura_id = null, $nova_data = null) {
        if($iugu_assinatura_id != null && $nova_data != null) { 

            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $data = [
                "api_token"  => IUGU_API_TOKEN,
                "expires_at" => $nova_data
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/subscriptions/".$iugu_assinatura_id,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_iugu_alterar_vencimento_assinatura = json_decode($response);

            if(!$resp_iugu_alterar_vencimento_assinatura->errors) {
                $this->Flash->success('Data de expiração alterada com sucesso!');
                return $this->redirect('/academia/admin/admin_mensalidades/planos');
            } else {
                $this->Flash->error('Não foi possível alterar a data de expiração... Tente novamente...');
                return $this->redirect('/academia/admin/admin_mensalidades/planos');
            }
        }
    }

    /**
     * VERIFICAR SUBCONTA
     */
    public function iugu_verificar_subconta($academia_id = null) {
        if($academia_id != null) { 

            $AcademiaMensalidades = TableRegistry::get('AcademiaMensalidades');

            $academia_mensalidade = $AcademiaMensalidades
                ->find('all')
                ->where(['academia_id' => $academia_id])
                ->first();

            $academia = $this->Academias
                ->find('all')
                ->contain(['Cities', 'Cities.States'])
                ->where(['Academias.id' => $academia_id])
                ->first();

            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            if($academia->mens_banco == 'Banco do Brasil' || $academia->mens_banco == 'Bradesco') {
                $agencia = $academia->mens_agencia.'-'.$academia->mens_agencia_dv;
            } else {
                $agencia = $academia->mens_agencia;
            }
            
            $conta = $academia->mens_conta.'-'.$academia->mens_conta_dv;

            if($academia_mensalidade->opt_conta_cpf == 1) {
                $data = [
                    "price_range"        => "Até R$ 100,00",
                    "physical_products"  => 0,
                    "business_type"      => "Academia",
                    "person_type"        => "Pessoa Física",
                    "automatic_transfer" => $academia_mensalidade->active_auto_transf == 1 ? "true" : "false",
                    "cpf"                => $academia->mens_cpf,
                    "name"               => $academia->mens_name,
                    "address"            => $academia->address,
                    "cep"                => $academia->cep,
                    "city"               => $academia->city->name,
                    "state"              => $academia->city->state->name,
                    "telephone"          => $academia->phone,
                    "bank"               => $academia->mens_banco == "Caixa" ? "Caixa Econômica" : $academia->mens_banco,
                    "bank_ag"            => $agencia,
                    "account_type"       => $academia->mens_tipo_conta == "Conta Poupança" ? "Poupança" : "Corrente",
                    "bank_cc"            => $conta
                ];
            } else {
                $data = [
                    "price_range"        => "Até R$ 100,00",
                    "physical_products"  => 0,
                    "business_type"      => "Academia",
                    "person_type"        => "Pessoa Jurídica",
                    "automatic_transfer" => $academia_mensalidade->active_auto_transf == 1 ? "true" : "false",
                    "cnpj"               => $academia->cnpj,
                    "company_name"       => $academia->name,
                    "address"            => $academia->address,
                    "cep"                => $academia->cep,
                    "city"               => $academia->city->name,
                    "state"              => $academia->city->state->name,
                    "telephone"          => $academia->phone,
                    "resp_name"          => $academia->mens_name,
                    "resp_cpf"           => $academia->mens_cpf,
                    "bank"               => $academia->mens_banco == "Caixa" ? "Caixa Econômica" : $academia->mens_banco,
                    "bank_ag"            => $agencia,
                    "account_type"       => $academia->mens_tipo_conta == "Conta Poupança" ? "Poupança" : "Corrente",
                    "bank_cc"            => $conta
                ];
            }

            $dados = [
                "api_token" => $academia_mensalidade->iugu_user_token,
                "data" => $data,
                "automatic_validation" => false
            ];

            $dados = json_encode($dados);

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/accounts/".$academia_mensalidade->iugu_account_id."/request_verification",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $dados,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_iugu_verify_subconta = json_decode($response);

            if($resp_iugu_verify_subconta->id) {
                $data = [
                    'account_verify' => 2
                ];

                $academia_mensalidade = $AcademiaMensalidades->patchEntity($academia_mensalidade, $data);

                $AcademiaMensalidades->save($academia_mensalidade);

                $this->Flash->success('Sua conta foi enviada para análise, vamos te avisar quando acabar!');
                return $this->redirect('/academia/admin/admin_mensalidades/config');
            } else {
                return $this->redirect('/academia/admin/admin_mensalidades/config');
            }
        }
    }

    /**
     * CONFIGURAR SUBCONTA
     */
    public function iugu_configurar_subconta($academia_id = null) {
        if($academia_id != null) { 

            $AcademiaMensalidades = TableRegistry::get('AcademiaMensalidades');

            $academia_mensalidade = $AcademiaMensalidades
                ->find('all')
                ->where(['academia_id' => $academia_id])
                ->first();

            $academia = $this->Academias
                ->find('all')
                ->contain(['Cities', 'Cities.States'])
                ->where(['Academias.id' => $academia_id])
                ->first();

            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $bank_slip_data = [
                "active"    => $academia_mensalidade->opt_boleto == 1 ? "true" : "false",
                "extra_due" => $academia_mensalidade->venc_boleto
            ];

            $credit_card_data = [
                "active"           => $academia_mensalidade->opt_card == 1 ? "true" : "false",
                "installments"     => $academia_mensalidade->parcelas_card >= 1 ? "true" : "false",
                "max_installments" => $academia_mensalidade->parcelas_card
            ];

            if($academia_mensalidade->opt_antecipate == 1) {
                $auto_advance_type = "daily";
            } else if($academia_mensalidade->opt_antecipate == 2) {
                $auto_advance_type = "weekly";
            } else if($academia_mensalidade->opt_antecipate == 3) {
                $auto_advance_type = "monthly";
            } else if($academia_mensalidade->opt_antecipate == 4) {
                $auto_advance_type = "days_after_payment";
            }

            $data = [
                "api_token"            => $academia_mensalidade->iugu_live_api_token,
                "auto_withdraw"        => $academia_mensalidade->active_auto_transf == 1 ? "true" : "false",
                "auto_advance"         => $academia_mensalidade->opt_antecipate >= 1 ? "true" : "false",
                "auto_advance_type"    => $auto_advance_type,
                "auto_advance_option"  => $academia_mensalidade->days_antecipate,
                "bank_slip"            => $bank_slip_data,
                "credit_card"          => $credit_card_data,
                "automatic_validation" => false
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/accounts/configuration",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_iugu_config_subconta = json_decode($response);

            if($resp_iugu_config_subconta->id) {
                $this->Flash->success('Sua conta foi configurada com sucesso!');
                return $this->redirect('/academia/admin/admin_mensalidades/config');
            } else if($resp_iugu_config_subconta->errors = 'Unauthorized') {
                $this->Flash->error('Configuração não autorizada!');
                return $this->redirect('/academia/admin/admin_mensalidades/config');
            } else {
                $this->Flash->error('Não foi possível configurar sua conta... Tente novamente...');
                return $this->redirect('/academia/admin/admin_mensalidades/config');
            }
        }
    }

    /**
     * EDITAR DADOS BANCÁRIOS SUBCONTA
     */
    public function iugu_editar_dados_bancarios_subconta($academia_id = null) {
        if($academia_id != null) { 

            $AcademiaMensalidades = TableRegistry::get('AcademiaMensalidades');

            $academia_mensalidade = $AcademiaMensalidades
                ->find('all')
                ->where(['academia_id' => $academia_id])
                ->first();

            $academia = $this->Academias
                ->find('all')
                ->contain(['Cities', 'Cities.States'])
                ->where(['Academias.id' => $academia_id])
                ->first();

            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            if($academia->mens_banco == 'Banco do Brasil' || $academia->mens_banco == 'Bradesco') {
                $agencia = $academia->agencia.'-'.$academia->mens_agencia_dv;
            } else {
                $agencia = $academia->mens_agencia;
            }
            
            $conta = $academia->mens_conta.'-'.$academia->mens_conta_dv;

            if($academia->mens_banco == "Banco do Brasil") {
                $banco = "001";
            } else if($academia->mens_banco == "Santander") {
                $banco = "033";
            } else if($academia->mens_banco == "Caixa") {
                $banco = "104";
            } else if($academia->mens_banco == "Bradesco") {
                $banco = "237";
            } else if($academia->mens_banco == "Itaú") {
                $banco = "341";
            }

            if($academia->image_comprovante) {
                $data = [
                    "api_token"            => $academia_mensalidade->iugu_live_api_token,
                    "agency"               => $agencia,
                    "account"              => $conta,
                    "account_type"         => $academia->mens_tipo_conta == "Conta Poupança" ? "cp" : "cc",
                    "bank"                 => $banco,
                    "document"             => WEBROOT_URL.'img/comprovantes/'.$academia->image_comprovante,
                    "automatic_validation" => false
                ];
            } else {
                $data = [
                    "api_token"            => $academia_mensalidade->iugu_live_api_token,
                    "agency"               => $agencia,
                    "account"              => $conta,
                    "account_type"         => $academia->mens_tipo_conta == "Conta Poupança" ? "cp" : "cc",
                    "bank"                 => $banco,
                    "automatic_validation" => false
                ];
            }

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/bank_verification",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_iugu_verify_subconta = json_decode($response);

            if(!$resp_iugu_verify_subconta->errors) {
                $this->Flash->success('Dados bancários alterados com sucesso!');
                return $this->redirect('/academia/admin/admin_mensalidades/config');
            } else {
                $this->Flash->error('Dados bancários em verificação... Tente novamente mais tarde...');
                return $this->redirect('/academia/admin/admin_mensalidades/config');
            }
        }
    }

    /**
     * CRIAR PLANO
     */
    public function iugu_criar_plano($plano_id = null) {
        if($plano_id != null) { 

            $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

            $plano = $AcademiaMensalidadesPlanos
                ->find('all')
                ->where(['id' => $plano_id])
                ->first();

            $type = $plano->type;

            $ch = curl_init();

            $header = [
                "Content-Type: application/json"
            ];

            $valor = number_format($valor = $plano->valor_total/$plano->time_months, 2, '', ' ');

            $data = [
                "api_token"     => IUGU_API_TOKEN,
                "name"          => $plano->name,
                "identifier"    => $plano->id.'-'.$plano->name,
                "interval"      => 1,
                "interval_type" => 'months',
                "currency"      => 'BRL',
                "value_cents"   => $valor
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "https://api.iugu.com/v1/plans",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);

            $resp_iugu_plano = json_decode($response);

            if(!$resp_iugu_plano->errors) {

                $data = [
                    'iugu_plano_id' => $resp_iugu_plano->id
                ];

                $plano = $AcademiaMensalidadesPlanos->patchEntity($plano, $data);

                if($plano_saved = $AcademiaMensalidadesPlanos->save($plano)) {
                    if($plano_saved->type == 1) {
                        $this->Flash->success('Plano '.$plano->name.' criado com sucesso!');
                        return $this->redirect('/academia/admin/admin_mensalidades/planos');
                    } else {
                        $this->Flash->success('Assinatura '.$plano->name.' criada com sucesso!');
                        return $this->redirect('/academia/admin/admin_mensalidades/assinaturas');
                    }
                } else {
                    $this->Flash->error('Falha ao adicionar plano... tente novamente...');
                    return $this->redirect('/academia/admin/admin_mensalidades/planos_add');
                }
            } else {
                $this->Flash->error('Falha ao adicionar plano... tente novamente...');
                return $this->redirect('/academia/admin/admin_mensalidades/planos_add');
            }
        }
    }

    public function buscar_aluno($cpf = null) {

        if($cpf != null) {
            $Clientes = TableRegistry::get('Clientes');
            
            $cliente = $Clientes
                ->find('all')
                ->contain(['Cities'])
                ->where(['Clientes.cpf' => $cpf])
                ->first();

            if($cliente) {
                echo $cliente;
            } else {
                echo '2';
            }
        }

        $this->viewBuilder()->layout('ajax');
    }

    public function buscar_aluno_email() {
        $email = $_GET['email'];

        if($email != null) {
            $Clientes = TableRegistry::get('Clientes');
            
            $cliente = $Clientes
                ->find('all')
                ->contain(['Cities'])
                ->where(['Clientes.email' => $email])
                ->first();

            if($cliente) {
                echo $cliente;
            } else {
                echo '2';
            }
        }

        $this->viewBuilder()->layout('ajax');
    }

    public function videos(){

        $this->viewBuilder()->layout('admin_academia');
    }

    public function listar_ionic(){
        header("Access-Control-Allow-Origin: *");

        $academiasIonic = $this->Academias
            ->find('all')
            ->contain(['Cities'])
            ->select([
                'id'        => 'Academias.id',
                'shortname' => 'Academias.shortname',
                'slug'      => 'Academias.slug',
                'cidade'    => 'Cities.name',
                'estado'    => 'Cities.uf'
            ])
            ->where(['Academias.status_id' => 1])
            ->order(['rand()'])
            ->all();

        $academias = json_encode($academiasIonic);

        echo $academias;

        $this->viewBuilder()->layout('ajax');
        $this->render(false);
    }

    public function admin_mensalidades_extrato(){
        $session  = $this->request->session();
        $Academia = $session->read('AdminAcademia');

        if(!$Academia) {
            $this->redirect('/academia/acesso');
        }

        $Pedidos                    = TableRegistry::get('Pedidos');
        $AcademiaSaques             = TableRegistry::get('AcademiaSaques');
        $PlanosClientes             = TableRegistry::get('PlanosClientes');
        $AcademiaMensalidades       = TableRegistry::get('AcademiaMensalidades');
        $AcademiaMensalidadesPlanos = TableRegistry::get('AcademiaMensalidadesPlanos');

        $pedidos = $Pedidos
            ->find('all')
            ->where(['comissao_status IN' => [4, 5]])
            ->all();

        $academia_mensalidades = $AcademiaMensalidades
            ->find('all')
            ->where(['academia_id' => $Academia->id])
            ->first();

        $academia_mensalidades_planos = $AcademiaMensalidadesPlanos
            ->find('all')
            ->where(['academia_mensalidades_id' => $academia_mensalidades->id])
            ->all();

        foreach ($academia_mensalidades_planos as $amp) {
            $planos_id[] = $amp->id;
        }

        if($planos_id) {
            $planos_clientes = $PlanosClientes
                ->find('all')
                ->where(['academia_mensalidades_planos_id IN' => $planos_id])
                ->all();
        }

        foreach ($planos_clientes as $plano_cliente) {
            $planos_clientes_id[] = $plano_cliente->id;
        }

        if($planos_clientes_id) {
            $planos = $Pedidos
                ->find('all')
                ->contain(['Clientes', 'PedidoStatus'])
                ->where(['Pedidos.academia_id' => $Academia->id])
                ->andWhere(['Pedidos.pedido_status_id >=' => 2])
                ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
                ->andWhere(['Pedidos.comissao_status IN' => 4])
                ->andWhere(['Pedidos.plano_cliente_id IN' => $planos_clientes_id])
                ->order(['Pedidos.created' => 'desc'])
                ->all();

            foreach ($planos as $plano) {
                $pedidos_extrato[] = $plano;
            }
        }

        $planos_personalizados = $Pedidos
            ->find('all')
            ->contain(['Clientes', 'PedidoStatus'])
            ->where(['Pedidos.academia_id' => $Academia->id])
            ->andWhere(['Pedidos.pedido_status_id >=' => 2])
            ->andWhere(['Pedidos.pedido_status_id NOT IN' => [7, 8]])
            ->andWhere(['Pedidos.comissao_status IN' => 5])
            ->order(['Pedidos.created' => 'desc'])
            ->all();

        foreach ($planos_personalizados as $plano_personalizado) {
            $pedidos_extrato[] = $plano_personalizado;
        }

        $pedidos_aguardando = $Pedidos
            ->find('all')
            ->contain(['Clientes'])
            ->where(['Pedidos.academia_id' => $Academia->id])
            ->andWhere(['Pedidos.pedido_status_id' => 2])
            ->andWhere(['Pedidos.comissao_status IN' => [4, 5]])
            ->andWhere(['Pedidos.payment_method' => 'BoletoBancario'])
            ->order(['Pedidos.created' => 'desc'])
            ->all();

        $saques = $AcademiaSaques
            ->find('all')
            ->where(['academia_mensalidades_id' => $academia_mensalidades->id])
            ->order(['created' => 'desc'])
            ->all();

        $this->set(compact('pedidos_aguardando', 'planos_clientes', 'pedidos_extrato', 'academia_mensalidades_planos', 'saques'));
        $this->viewBuilder()->layout('admin_academia');
    }

    public function salvar_dados_bancarios_cnpj ($comissao = 0) {
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect(['action' => 'login']);
        }

        if($this->request->is(['post', 'put'])){

            $this->request->data['is_cpf'] = 0;

            $academia = $this->Academias
                ->find('all')
                ->where(['Academias.id' => $Academia->id])
                ->limit(1)
                ->first();

            $academia = $this->Academias->patchEntity($academia, $this->request->data);

            if($this->Academias->save($academia)){
                $this->Flash->success('Dados bancários atualizados com sucesso!');
            }else{
                $this->Flash->error('Falha ao atualizar dados bancários. Tente novamente...');
            }
        }

        if($comissao == 1) {
            return $this->redirect(['action' => 'admin_comissoes_index']);
        }

        return $this->redirect(['action' => 'admin_dados_bancarios']);

        $this->viewBuilder()->layout('admin_academia');
    }

    public function salvar_dados_bancarios_cpf ($comissao = 0) {
        $session    = $this->request->session();
        $Academia   = $session->read('AdminAcademia');
        if(!$Academia){
            $this->redirect(['action' => 'login']);
        }

        if($this->request->is(['post', 'put'])){

            $academia = $this->Academias
                ->find('all')
                ->where(['Academias.id' => $Academia->id])
                ->limit(1)
                ->first();

            $this->request->data['is_cpf'] = 1;

            $academia = $this->Academias->patchEntity($academia, $this->request->data);

            if($this->Academias->save($academia)){
                $this->Flash->success('Dados bancários atualizados com sucesso!');
            }else{
                $this->Flash->error('Falha ao atualizar dados bancários. Tente novamente...');
            }
        }

        if($comissao == 1) {
            return $this->redirect(['action' => 'admin_comissoes_index']);
        }

        return $this->redirect(['action' => 'admin_dados_bancarios']);

        $this->viewBuilder()->layout('admin_academia');
    }
}