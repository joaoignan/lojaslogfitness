<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProdutoComentarios Controller
 *
 * @property \App\Model\Table\ProdutoComentariosTable $ProdutoComentarios
 */
class ProdutoComentariosController extends AppController
{
    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add(){

        $produtoComentario = $this->ProdutoComentarios->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {

            $produto_comentatio = $this->ProdutoComentarios
                ->find('all')
                ->where(['email' => $this->request->data['email']])
                ->count();

            if($produto_comentatio >= 1){
                echo 'Você já avaliou este produto...';
            }else {
                $this->request->data['status_id'] = 2;
                $produtoComentario = $this->ProdutoComentarios->patchEntity($produtoComentario, $this->request->data);
                if ($this->ProdutoComentarios->save($produtoComentario)) {
                    echo 'Produto avaliado com sucesso!';
                } else {
                    echo
                        '<button id="aval-submit" type="button" class="button-enviar">ENVIAR</button>' .
                        '<span>Falha ao enviar avaliação... Tente novamente</span>';
                }
            }
        }
    }
}
