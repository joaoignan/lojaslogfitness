<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fornecedores Controller
 *
 * @property \App\Model\Table\FornecedoresTable $Fornecedores
 */
class FornecedoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index(){
        $states = $this->Fornecedores->Cities->States->find('list');

        $this->set(compact('states'));
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $parceiro = $this->Fornecedores->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {

            $parceiros = $this->Fornecedores
                ->find('all')
                ->where(['cnpj' => $this->request->data['cnpj']])
                ->count();

            if($parceiros >= 1){
                echo 'Já existe uma parceiro cadastrado com o CNPJ informado...';
            }else {
                $this->request->data['status_id'] = 2;
                $parceiro = $this->Fornecedores->patchEntity($parceiro, $this->request->data);
                if ($this->Fornecedores->save($parceiro)) {
                    echo 'Parceiro cadastrado com sucesso!';
                } else {
                    echo 'Falha ao cadastrar parceiro... Tente novamente';
                }
            }

            //debug($this->request->data);
        }
    }

}
