<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Cities Controller
 *
 * @property \App\Model\Table\CitiesTable $Cities
 */
class CitiesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index($state_id = null, $academias = false) {
        if($state_id != null) {
            if($academias){
                $this->set('cities', $this->Cities
                    ->find('list')
                    ->innerJoin(
                        ['Academias' => 'academias'],
                        ['Academias.city_id = Cities.id'])
                    ->where(['Cities.state_id' => $state_id])
                    ->andWhere(['Academias.status_id' => 1])
                    ->order(['Cities.name' => 'asc'])
                    ->all()
                    ->toArray()
                );
            }else{
                $this->set('cities', $this->Cities
                    ->find('list')
                    ->where(['Cities.state_id' => $state_id])
                    ->order(['Cities.name' => 'asc'])
                    ->all()
                    ->toArray()
                );
            }
        }
    }
}
