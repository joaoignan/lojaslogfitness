<script>
    var redirect = '<?= $redirect ? $redirect : '' ?>';
</script>

<style>
    #form-cadastro-main,
    #info-cadastro-main-sem,
    #info-cadastro-main-com,
    #escolher-academia-box,
    #fb-login,
    #form-login-main {
        display: inline-block;
    }
    .title-login {
        font-size: 2em;
        text-align: center;
        margin: 0 0 10px;
        color: #5087c7;
    }
    .button-acessar {
        border: 0;
        font-weight: bold;
        color: white;
        background: #8DC73F;
        float: none;
        width: 70%;
        height: 45px;
        text-shadow: 1px 1px 1px #0F2538;
        margin-right: 0px;
        margin-left: 0px;
    }
    .fundo-branco {
        display: none;
    }
    .button-cadastrar {
        width: 70%;
        background: #8DC73F;
    }
    .button-nao-faco-parte {
        width: 70%;
        margin-left: 0;
        margin-right: 0;
    }
    .separador-cadastrar {
        width: 70%;
        border: 1px solid #5087c7;
        margin-top: 0;
        margin-bottom: 0;
    }
    .botoes-facebook {
        width: 70%;
    }
    .separador-cadastrar-mobile {
        display: none;
        width: 70%;
        border: 1px solid #5087c7;
        margin-top: 0;
        margin-bottom: 0;
    }
    .checkbox-div {
        margin-top: 15px;
        margin-left: 60px;
    }
    #submit-login-main, .button-nao-faco-parte{
        margin: 0!important;
    }
    @media all and (max-width: 430px) {
        .botoes-facebook {
            width: 100%;
        }
        .button-acessar{
            width: 90%;
        }
        .input-cadastro-academia{
            width: 100%;
        }
        .button-cadastrar{
            width: 90%;
        }
        .button-nao-faco-parte{
            width: 90%;
            font-size: 11px;
        }
        .separador-cadastrar-mobile {
            display: block;
        }
        .fb-font{
            font-size: 12px;
        }
    }
    @media all and (max-width: 786px) {
        .fundo-branco {
            display: none;
        }
    }
</style>

<div class="container logins" id="modal-cliente-login" style="padding-top: 150px;">
    <div class="row" style=" ">
        <div class="col-xs-12">
            <p class="text-center hidden-xs" style="margin: 15px 0 15px 0;">
                <span class="text-center font-bold">
                    <?= isset($fb_login_error)
                        ?
                        '<h5 class="font-bold text-center">
                        Ocorreu um erro ao realizar a conexão com o Facebook.<br> 
                        Verifique se você concedeu permissão de acesso ao aplicativo da LogFitness ou tente novamente dentro 
                        de alguns minutos.
                        </h5>'
                        :
                        '<p id="info-cadastro-main-com" class="text-center"></p>
                         <p id="info-cadastro-main-sem" class="text-center"></p>'
                    ?>
                </span>
            </p>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="row">
                <h3 class="title-login">Fazer Login</h3>
                <?= $this->Form->create(null, ['id' => 'form-login-main'])?>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <?= $this->Form->input('email', [
                                'div'           => false,
                                'label'         => false,
                                'class'         => 'input-cadastro form-control validate[required]',
                                'placeholder'   => 'EMAIL ou CPF',
                            ])?>
                        </div>
                        <div class="col-sm-12 form-group">
                            <?= $this->Form->input('password', [
                                'id'            => 'login-password',
                                'div'           => false,
                                'label'         => false,
                                'class'         => 'input-cadastro form-control validate[required]',
                                'placeholder'   => 'SENHA',
                                'autocomplete'  => 'off',
                            ])?>
                        </div>
                        <div class="col-sm-12 text-center">
                            <?= $this->Html->link('Não sei a minha senha',
                                $SSlug.'/login/esqueci-a-senha',
                                ['class' => 'btn'])?>
                        </div>
                        <div class="col-sm-12 text-center">
                            <?= $this->Form->button('ACESSAR', [
                                'id'            => 'submit-login-main',
                                'type'          => 'button',
                                'class'         => 'button-acessar',
                            ])?>
                        </div>
                    </div>
                <?= $this->Form->end()?>

                    <script type="text/javascript">
                        $(document).ready(function() {
                            $(document).keypress(function(e) {
                                if($('#login-password').is(':focus')) {
                                    if (e.which == 13) { 
                                        $('#submit-login-main').click();
                                    }; 
                                }
                            });
                        });
                    </script>

                    <p class="text-center" style="margin: 10px 0"><b>OU</b></p>
                    <div class="row">
                        <div class="col-xs-12 text-center margin-bottom-30" id="fb-login" >
                        <?= $this->Html->link('<i class="fa fa-facebook-square"></i> Acessar com Facebook',
                                    '/fb-login/',
                                ['class' => 'btn btn-primary font-bold botoes-facebook fb-font', 'escape' => false]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <hr class="separador-cadastrar-mobile">

        <div class="col-xs-12 col-sm-6">
            <h3 class="title-login">Cadastrar</h3>
            <?= $this->Form->create(null, ['id' => 'form-cadastro-main', 'default' => false])?>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <?= $this->Form->input('name', [
                            'div'           => false,
                            'label'         => false,
                            'class'         => 'input-cadastro form-control validate[required]',
                            'placeholder'   => 'NOME COMPLETO',
                        ])?>
                    </div>
                    <div class="col-sm-12 form-group">
                        <?= $this->Form->input('email', [
                            'div'           => false,
                            'label'         => false,
                            'class'         => 'input-cadastro form-control validate[required, custom[email]] ck-c-email',
                            'placeholder'   => 'EMAIL',
                        ])?>
                    </div>
                    <div class="col-sm-12 form-group">
                        <?= $this->Form->input('password', [

                            'id'            => 'senhalogin',
                            'div'           => false,
                            'label'         => false,
                            'class'         => 'input-cadastro form-control validate[required]',
                            'placeholder'   => 'SENHA',
                            'autocomplete'  => 'off',
                        ])?>
                    </div>
                    <div class="col-sm-12 form-group">
                        <?= $this->Form->input('password_confirm', [

                            'id'            => 'conf_senhalogin',
                            'div'           => false,
                            'label'         => false,
                            'class'         => 'input-cadastro form-control validate[required]',
                            'placeholder'   => 'CONFIRMAR SENHA',
                            'type'          => 'password',
                            'autocomplete'  => 'off',
                        ])?>
                    </div>
                    <div class="col-sm-12 form-group">
                        <?= $this->Form->input('telephone', [
                            'div'           => false,
                            'label'         => false,
                            'class'         => 'input-cadastro form-control validate[required] phone-mask',
                            'placeholder'   => 'TELEFONE',
                        ])?>
                    </div>
                    <div class="col-sm-12 form-group">
                        <?= $this->Form->input('cpf', [
                            'div'           => false,
                            'label'         => false,
                            'class'         => 'input-cadastro form-control validate[required,custom[cpf]] form-control cpf ck-c-cpf',
                            'placeholder'   => 'CPF',
                        ])?>
                    </div>                        
                </div>
                <div class="row" id="escolher-academia-box" >
                    <div class="col-sm-12 text-center">
                        <h5><strong>Informe sua academia<strong></h5>
                    </div>
                    <div class="col-sm-12 form-group">
                        <?= $this->Form->input('uf_id', [
                            'value'         => SLUG ? $academia_slug->city->state_id : '',
                            'id'            => 'states_academias',
                            'div'           => false,
                            'label'         => false,
                            'options'       => $states,
                            'empty'         => 'Estado...',
                            'class'         => 'validate[required] form-control estado-academia input-cadastro-academia',
                        ]) ?>
                    </div>

                    <div class="col-sm-12 form-group" >
                        <?= $this->Form->input('city_id', [
                            'value'         => SLUG ? $academia_slug->city_id : '',
                            'id'            => 'city',
                            'div'           => false,
                            'options'       => SLUG ? $academia_slug_cities : [],
                            'label'         => false,
                            'empty'         => 'Selecione uma cidade',
                            'class'         => 'validate[required] form-control cidade-academia input-cadastro-academia',
                        ]) ?>
                    </div>

                    <div class="col-sm-12 form-group" >
                        <?= $this->Form->input('academia_id', [
                            'id'            => 'select-academia',
                            'options'       => $academias,
                            'value'         => SLUG ? $academia_slug->id : '',
                            'div'           => false,
                            'empty'         => 'Selecione uma academia...',
                            'label'         => false,
                            'class'         => 'form-control input-cadastro-academia validate[required] academia-professor',
                        ])?>
                    </div>

                    <?php
                        $options = [];                        
                        if(SLUG) {
                            foreach ($professores_slug as $prof_slug) {
                                $options[$prof_slug->id] = $prof_slug->professore->name;
                            }
                        }
                    ?>

                    <div class="col-sm-12 form-group" >
                        <?= $this->Form->input('professor_id', [
                            'id'            => 'select-professor',
                            'options'       => $options,
                            'div'           => false,
                            'empty'         => 'Selecione um professor...',
                            'label'         => false,
                            'class'         => 'form-control input-cadastro validate[optional] input-cadastro-width'
                        ]) ?>
                    </div>

                    <div class="col-sm-12 checkbox-div">
                        <?= $this->Form->input('email_notificacao', [
                            'type'      => 'checkbox',
                            'class'     => 'checkbox-login',
                            'checked'   => true,
                            'label'     => " Quero receber novidades no meu email!",
                            'div'       => false,
                        ])?>
                    </div>

                    <div class="col-sm-12 text-center">
                        <?= $this->Form->button('CADASTRAR', [
                            'id'            => 'btn-submit-cadastro-main',
                            'type'          => 'button',
                            'class'         => 'button-cadastrar',
                            'style'         => 'margin-bottom: 15px',
                        ])?>
                    </div>
                </div>
            </div>
            <?= $this->Form->end()?>

            <p class="text-center" style="margin: 15px 0"><b>OU</b></p>

            <div class="col-sm-12 text-center">
                <div class="col-md-12 text-center margin-bottom-30" id="fb-login" >
                <?= $this->Html->link('<i class="fa fa-facebook-square"></i> Cadastro com Facebook',
                            '/fb-login/',
                        ['class' => 'btn btn-primary font-bold botoes-facebook fb-font', 'escape' => false]) ?>
                </div>
            </div>

            <hr class="separador-cadastrar">

            <div class="col-sm-12 text-center" style="margin-top: 25px;">
                <?= $this->Html->link('<button class="button-nao-faco-parte" type="button">NÃO ENCONTREI MINHA ACADEMIA</button>', 
                    '/',
                    ['escape' => false]
                )?>
            </div>
        </div>
    </div>
</div>






