<style>
    .footer-total {
        display: none;
    }
    .conteudo{
        margin-top: 20px;
    }
    .btn-menu{
        text-align: center;
        height: 150px;
        width: 140px;
        color: #5089cf;
        border: 2px solid;
        border-color: #5087c7;
        border-radius: 10px;
        background-color: white;
        font-family: nexa_boldregular;
        box-shadow: 1px 1px 5px 0px rgba(0,0,0,0.75);
    }
    .botoes .fa{
        color: #5089cf;
        margin-top: 20px; 
        margin-bottom: 10px;
        margin-right: 0;
    }
    .botoes p{
        color: #5089cf;
        margin-top: 10px;
        font-size: 16px;
    }
    .mochila_icon{
        width: 56px;
        margin-left: 5px;
    }
    .item-painel{
            height: 150px;
            width: 23%;
            float: left;
            margin-left: 8%;
            margin-top: 20px;
        }
    @media all and (max-width: 1024px) and (min-width: 769px){
        .cliente-painel{
                margin-top: 50px;
                margin-bottom: 120px;
            }
    }
    @media all and (max-width: 768px) and (max-height: 1024px){
        .cliente-painel{
            margin-top: 20px;
            margin-bottom: 445px;
        }
    }
    @media all and (max-width: 768px) and (min-width: 431px){
        .cliente-painel{
            margin-top: 20px;
            margin-bottom: 25px;
        }
    }
    @media all and (max-width: 430px) {
        .conteudo{
        margin-top: 70px;
        }
        .btn-menu {
            height: 110px;
            width: 110px;
        }
        .conteudo a{
            margin-top: 0px;
            margin-bottom: 0px;
            padding-right: 0px;
            padding-left: 0px;
            padding-top: 0px;
            padding-bottom: 0px;
        }
        .item-painel{
            height: 110px;
            width: 35%;
            float: left;
            margin-left: 10%;
            margin-top: 20px;
        }
    }
</style>

<div class="col-sm-12 col-md-8 col-md-offset-2 cliente-painel">

    <div class="conteudo">
        <div class="item-painel">
            <?= $this->Html->link(
                '<button class="btn-menu">'.
                    $this->Html->image('mochila_log.png',
                        ['class' => 'mochila_icon']
                    ).'
                    <br/>
                    <p>meus pedidos</p>
                </button>',
                $SSlug.'/minha-conta/meus_pedidos',
                ['class' => '', 'escape' =>false]);
            ?>
        </div>
        <div class="item-painel">
            <?= $this->Html->link(
                '<button class="btn-menu">
                    <span class="fa fa-comment-o fa-4x"></span>
                    <br/>
                    <p>indicações</p>
                </button>',
                $SSlug.'/minha-conta/indicacao-de-produtos',
                ['class' => '', 'escape' =>false]);
            ?>
        </div>
        <div class="item-painel">
            <?= $this->Html->link(
                '<button class="btn-menu">
                    <span class="fa fa-building-o fa-4x"></span>
                    <br/>
                    <p>minha academia</p>
                </button>',
                $SSlug.'/minha-conta/minha-academia',
                ['class' => '', 'escape' =>false]);
            ?>
        </div>
        <div class="item-painel">
            <?= $this->Html->link(
                '<button class="btn-menu">
                    <span class="fa fa-line-chart fa-4x"></span>
                    <br/>
                    <p>meus objetivos</p>
                </button>',
                $SSlug.'/minha-conta/meus-dados',
                ['class' => '', 'escape' =>false]);
            ?>
        </div>
        <div class="item-painel">
            <?= $this->Html->link(
                '<button class="btn-menu">
                    <span class="fa fa-user fa-4x"></span>
                    <br/>
                    <p>meus dados</p>
                </button>',
                $SSlug.'/minha-conta/dados-faturamento',
                ['class' => '', 'escape' =>false]);
            ?>
        </div>
        <div class="item-painel">
            <?= $this->Html->link(
                '<button class="btn-menu">
                    <span class="fa fa-key fa-4x"></span>
                    <br/>
                    <p>alterar senha</p>
                </button>',
                $SSlug.'/minha-conta/alterar-senha',
                ['class' => '', 'escape' =>false]);
            ?>
        </div>
    </div>
</div>