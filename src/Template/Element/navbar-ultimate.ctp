<script>
    function openIndique() {
        document.getElementById("indique").style.height = "100%";
        $("html,body").css({"overflow":"hidden"});
    }
    function closeIndique() {
        document.getElementById("indique").style.height = "0%";
        $("html,body").css({"overflow":"auto"});
    }
    function enviar(){
        document.getElementById('sucesso').style.display = "block"
    }
</script>
<?= $this->Element('overlayEscolherLoja'); ?>
<div class="navbar ultimate">
	<div class="container">
        <div class="row">
            <div class="col-xs-2 filtro-navbar-btn text-center">
                <i class="fa fa-search"></i><span class="txt-wrap"><br>Produtos</span>
            </div>
            <div class="col-md-3 col-lg-3 text-center tablet-hidden ">
                <div class="navegacao">
        			<div class="botoes-navegacao">
                        <div class="botao-login">
                            <a class="button" id="btnlogin">Login <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                            <div class="dropdown-login">
                                <ul>
                                    <li><?= $this->Html->link('Aluno','/login/'.$SSlug); ?></li>
                                    <li><?= $this->Html->link('Perfil',['controller' => 'Academias', 'action' => 'login'], ['target' => '_blank']); ?></li>
                                    <!-- <li><?= $this->Html->link('Time',['controller' => 'Professores', 'action' => 'login'], ['target' => '_blank']); ?></li> -->
                                </ul>
                            </div>
                        </div>
        			</div>
                    <?php if($CLIENTE) { ?>
                    <?php $name_aluno = explode(' ', $CLIENTE->name) ?>
                    <div class="name-logado tablet-hidden">
                        <a class="name-link">Bem vindo, <?= $name_aluno[0] ?> <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                        <div class="dropdown-logado text-right">
                            <ul>
                                <li><?= $this->Html->link('Início','/'.$academia->slug); ?></li>
                                <li><?= $this->Html->link('Meus Dados','/minha-conta/dados-faturamento'); ?></li>
                                <li><?= $this->Html->link('Meus Pedidos','/minha-conta/meus-pedidos'); ?></li>
                                <li><?= $this->Html->link('Minha Academia','/minha-conta/minha-academia'); ?></li>
                                <li><?= $this->Html->link('Alterar Senha','/minha-conta/alterar-senha'); ?></li>
                                <li><?= $this->Html->link('Sair','/logout'); ?></li>
                            </ul>
                        </div>
                    </div>
                    <?php } else { ?>
                        <div class="name-logado tablet-hidden">
                        </div>
                    <?php } ?>
                </div> <!-- navegacao -->
            </div>      
    		<div class="col-xs-8 col-sm-4 col-md-6 col-lg-6 logo-academias">
                <?= $this->Html->link($this->Html->image('logo_master.png'),'/'.$academia->slug,['escape' => false]); ?>
    			<?php 
                    if ($academia->id != 601) {
                        if($academia->image != null){
                            echo $this->Html->link($this->Html->image('academias/'.$academia->image, ['alt' => $academia->shortname]),'/'.$academia->slug,['escape' => false]);
                        } else{
                            echo '<h4>'.$academia->shortname.'</h4>';
                        }
                    }
            	?>
    		</div> <!-- logo-academias -->
            <div class="col-xs-2 col-sm-3 col-md-3 col-lg-3 text-center acoes-loja">
                <!-- <div class="col-xs-6 text-center">
                    <a class="button produto-comparar-btn abrir-comparador">
                        <i class="fa fa-balance-scale" aria-hidden="true"></i> (<span id="contador_comparador"><?= $qtd_comparador ?></span>) <span class="txt-wrap">Comparador</span> 
                    </a>
                </div> -->
                <div class="col-xs-12 text-center">
                    <a id="abrir-mochila" class="button"> <?= $this->Html->image('mochila_log_branca.png',['alt' => 'Mochila LOG']
                ) ?> (<span id="contador_mochila">0</span>) <span class="txt-wrap">Mochila</span> </a>
                </div>
                <div class="w-100"></div>
                <div class="col-xs-12 text-center tablet-hidden">
                    <span class="btnEscolherLoja escolherNavbar">Escolher loja</span>
                </div>
            </div>
        </div>
	</div>
</div>

<div class="total-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="breadcrumb flat text-center breadcrumb-content tablet-hidden">
                    <?php foreach($links_breadcrumb as $link_breadcrumb) { ?>
                        <a href="<?= WEBROOT_URL.$SSlug ?><?= $link_breadcrumb['url'] ?>"><?= $link_breadcrumb['name'] ?></a>
                    <?php } ?>
                </div>
                <div class="btn-voltar-div">
                    <a class="btn-voltar"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
                </div>
                <?php if ($this->request->is('mobile')) { ?>
                    <div class="btn-voltar-div pull-right" style="margin-right: 0px;">
                        <span class="btnEscolherLoja">Escolher loja</span>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<div id="indique" class="overlay">
    <div class="overlay-content text-center">
        <a href="javascript:void(0)" class="closebtn" onclick="closeIndique()">&times;</a>
        <h3>Não encontrou uma academia que você conhece?<br> Indique ela pra gente!</h3>
        <hr>
        <div class="col-xs-12 col-md-6" id="explicativo">
            <h4>Meu primeiro LOG</h4>
            <p>Indique a sua academia e quando ela se cadastrar você ganha um suplemento. <?= $this->Html->image('mini_medal.png',['title' => 'logo-academia', 'alt' => 'Logo']) ?></p>
            <p>Enviamos direto para você retirar na sua academia! <?= $this->Html->image('mini_box.png',['title' => 'logo-academia', 'alt' => 'Logo']) ?></p>
            <p>Só ganha o primeiro a indicar a academia... <?= $this->Html->image('mini_run.png',['title' => 'logo-academia', 'alt' => 'Logo']) ?> CORRA!!</p>
        </div>
        <div class="col-xs-12 col-md-6">
            <?= $this->Form->create(null, [
                'id'        => 'form-indicar-academia',
                'role'      => 'form',
                'default'   => false
            ]) ?>
            <h4>Formulário de indicaçãos</h4>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('responsavel', [
                    'div'           => false,
                    'label'         => false,
                    'class'         => 'form-control',
                    'placeholder'   => 'Academia ou Responsável*',
                    'required'      => true
                ])?>
            </div>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('telephone', [
                    'id'            => 'telephone',
                    'div'           => false,
                    'label'         => false,
                    'class'         => 'form-control',
                    'placeholder'   => 'Telefone do responsável*',
                    'type'          => 'tel',
                    'required'      => true
                ])?>
            </div>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('uf_id', [
                    'label'         => false,
                    'id'            => 'states-indica',
                    'div'           => false,
                    'options'       => $states,
                    'empty'         => 'Selecione um estado',
                    'placeholder'   => 'Estado',
                    'class'         => 'form-control',
                    'required'      => true
                ]) ?>
            </div>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('city_id', [
                    'label'         => false,
                    'id'            => 'city-indica',
                    'div'           => false,
                    'empty'         => 'Selecione uma cidade',
                    'placeholder'   => 'Cidade',
                    'class'         => 'form-control',
                    'required'      =>  true
                ]) ?>
            </div>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('email_indicador', [
                    'div'           => false,
                    'label'         => false,
                    'class'         => 'form-control',
                    'placeholder'   => 'Seu e-mail*',
                    'required'      => true,
                    'type'          => 'e-mail'
                ])?>
            </div>
        </div>
        <div class="col-xs-12">
            <?= $this->Form->button('Enviar', [
                'id'            => 'submit-indicar-academia',
                'type'          => 'button',
                'class'         => 'btn btn-success',
                'escape'        => false,
                // 'onclick'    => 'enviar()'
            ]) ?>
        <?= $this->Form->end()?>
        
        <p id="message-academia-indica"></p>
        </div>
    </div>
</div>
<div class="overlay-comparador">
    <div class="overlay-content text-center">
        <h2>Comparador</h2>
        <p>Sabemos que escolher entre tantas marcas e modelos de suplementos não é um exercício fácil.</p>
        <p>Por isso estamos criando uma ferramenta que te ajudará a tomar a melhor decisão para o seu treino.</p>
        <br>
        <button class="btn btn-success">Entendi!</button>
    </div>
</div>
<?php if(isset($CLIENTE)){ ?>
    <div class="nav-inferior">
        <div class="col-xs-12 text-center">
            <div class="row">
                <div class="item-nav-inferior">
                    <?= $this->Html->link('
                        <i class="fa fa-id-card-o fa-2x"></i>
                        <p>Dados</p>'
                    ,'/minha-conta/dados-faturamento', ['escape' => false]) ?>
                </div>
                <div class="item-nav-inferior">
                    <?= $this->Html->link('
                        <i class="fa fa-2x fa-list"></i>
                        <p>Pedidos</p>'
                    ,'/minha-conta/meus_pedidos', ['escape' => false]) ?>
                </div>
                <div class="item-nav-inferior">
                    <span id="abrir-submenu">
                        <i class="fa fa-2x fa-user"></i>
                        <p>Eu</p>
                    </span>
                </div>
                <div class="item-nav-inferior">
                    <?= $this->Html->link('
                        <i class="fa fa-calendar fa-2x"></i>
                        <p>Mensalidades</p>'
                    ,'/minha-conta/mensalidades',['escape' => false]) ?>
                </div>
                 <div class="item-nav-inferior">
                    <?= $this->Html->link('
                        <i class="fa fa-building-o fa-2x"></i>
                        <p>Academia</p>'
                    ,'minha-conta/minha-academia',['escape' => false]) ?>
                </div>
            </div>
        </div>
    </div>
    <div id="nav-inferior-submenu">
        <div class="conteudo-submenu">
            <?php $client_name = explode(' ', $CLIENTE->name);?>
            <div class="row text-center usuario-submenu">
                <span id="fechar-submenu"><i class="fa fa-close"></i></span>
                <p>Olá <?= array_shift($client_name) ?></p>
            </div>
            <ul>
                <li>
                    <?= $this->Html->link('<p><i class="fa fa-home"></i>&nbsp;&nbsp;Início</p>',$SSlug.'/home', ['escape' => false]);  ?>    
                </li>
                <li>
                    <?= $this->Html->link('<p><i class="fa fa-user"></i>&nbsp;&nbsp;Meus Dados</p>','/minha-conta/dados-faturamento', ['escape' => false]);  ?>
                </li>
                <li>
                    <?= $this->Html->link('<p>'.$this->Html->image('mochila_log.png',['style' => 'margin-top: -5px;', 'alt' => 'Mochila LOG']
                        ).'&nbsp;&nbsp;Meus Pedidos</p>','/minha-conta/meus_pedidos', ['escape' => false]);  ?>
                </li>
                <li>
                    <?= $this->Html->link('<p><i class="fa fa-building-o"></i>&nbsp;&nbsp;Minha Academia</p>','/minha-academia', ['escape' => false]);  ?>
                </li>
                <li>
                    <?= $this->Html->link('<p><i class="fa fa-sign-out"></i>&nbsp;&nbsp;Deslogar</p>','/logout', ['escape' => false]);  ?>
                </li>
            </ul>
        </div>
    </div>
<?php } ?>
<?= $this->Element('comparador'); ?>

<script>
    $(document).ready(function() {
        $('.btnEscolherLoja').click(function(){
            $('#overlayEscolherLoja').animate({'height' : '100%'}, 500);
            $('html,body').css({"overflow":"hidden"});
        });
        $('#overlayEscolherLoja').click(function(){
            $('#overlayEscolherLoja').animate({'height' : '0%'}, 500);
            $('html,body').css({"overflow":"auto"});
        });
    	//abrir o menu
    	$('#abrir-menu').click(function(){
        	$('.menu-completo').css('display', 'block');
    		setTimeout(function(){
    			$('.menu-barra').animate({'width' : '300px'}, 500);
    			$('.menu-completo').animate({'opacity' : '1'}, 500);
    	        $("body,html").addClass('total-block');
    	        setTimeout(function() {
    	        	$('.itens-menu').animate({'opacity' : '1'}, 200);
    	        	$('.menu-barra').animate({'opacity' : '1'}, 200);
    	        }, 500);
    		}, 300);
        });

        //fechar o menu
        $('#fechar-menu, .menu-completo').click(function() {
        	$('.itens-menu').animate({'opacity' : '0'}, 200);
    		setTimeout(function() {
    			$('.menu-completo').animate({'opacity' : '0'}, 300);
    			$('.menu-barra').animate({'width' : '0'}, 500);
    			setTimeout(function(){
    				$('.menu-completo').css('display', 'none');
        			$("body,html").removeClass('total-block'); 
    			}, 500);
    		}, 300);
        });

        //ao clicar no menu, não fechar overlay
        $('.menu-barra').click(function(e){
        	e.stopPropagation();
        });

        //indicar academia
        $('#states-indica').on('change', function(){
            var state_id = $(this).val();
            $.get(WEBROOT_URL + '/cidades/' + state_id,
                function(data){
                    $('#city-indica').html(data);
                });
        });
        $('#submit-indicar-academia').click(function(){
            var form = $('#form-indicar-academia');
            var form_data = form.serialize();
            $.ajax({
                type: "POST",
                url: WEBROOT_URL + 'indicar-academia',
                data: form_data
            })
            .done(function (data) {
                $('#submit-indicar-academia').fadeOut();
                $('#message-academia-indica').html('Indicação enviada com sucesso!');
            });
        });

        //abrir submenu
        $('#abrir-submenu').click(function(){
            $('#filtro-mobile-btn').css('display', 'none');
            $('#nav-inferior-submenu').css('display', 'block');
            setTimeout(function(){
                $('#nav-inferior-submenu').animate({'top' : '0px'}, 1000);
                $("body,html").addClass('total-block');
            }, 300);
        });

        //fechar o submenu
        $('#fechar-submenu, #nav-inferior-submenu').click(function() {
            $('#nav-inferior-submenu').animate({'top' : '2000px'}, 500);
            setTimeout(function() {
                $("body,html").removeClass('total-block');
                $('#filtro-mobile-btn').css('display', 'block'); 
            }, 300);
        });

        //ao clicar no submenu, não fechar overlay
        $('.conteudo-submenu').click(function(e){
            e.stopPropagation();
        });

        // abrir overlay-comparador
        // $('.produto-comparar-btn, .compare').click(function() {
        //     $('.overlay-comparador').animate({'top' : '0'}, 500);
        // });
        // fechar overlay-comparador
        // $('.overlay-comparador').click(function() {
        //     $('.overlay-comparador').animate({'top' : '2000px'}, 500);
        // });
    });
</script>

<?php if(isset($_GET['indicar'])) { ?>
    <script type="text/javascript">
        openIndique();
    </script>
<?php } ?>

<?php if($academia->slug) { ?>
    <script type="text/javascript">
        var SSLUG = '<?= $academia->slug ?>';
    </script>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('.btn-voltar').click(function() {
            
            var url_atual = window.location.href;

            if(url_atual.indexOf("finalizado") != -1) {
                loading.show();
                location.href = WEBROOT_URL + SSLUG;
            } else {
                history.back();
            }
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var breadcrumb = $(".total-breadcrumb");
        var cur = breadcrumb.scrollLeft();
        var max = breadcrumb[0].scrollWidth - breadcrumb.parent().width();

        breadcrumb.scrollLeft(max);
    });
</script>