<style>
.total-item-combo-select{
	margin-bottom: 15px;
}
.item-combo-select{
	border-radius: 5px;
    border: 2px solid transparent;
    padding: 5px 10px;
    height: 215px;
}
.item-combo-normal{
	border-color: #dadada;
}
.item-combo-active{
	border-color: #5089cf;
}
.titulo-combo{
	margin-bottom: 5px;
}
.titulo-combo span{
	font-weight: 700;
	color: #5089cf;
	font-size: 18px;
}
.desc-combo-select p{
	font-size: 14px;
}
.desc-combo-select .fa{
	color: rgba(72, 159, 54, 1);
}
.btn-selecionar-combo{
	background-color: rgb(80, 137, 207);
    color: rgb(255,255,255);
}
.btn-selecionar-combo:hover{
	background-color: rgba(60, 102, 152, 1);
    color: rgb(255,255,255);
}
.content-btn-selecionar-combo{
    position: absolute;
    bottom: 10px;
    left: 30px;
    right: 30px
}
</style>

<script>
	$(document).ready(function(){
		// abre mudança de combo
		$('.change-combo').click(function(){
			$('#change-combo-overlay').animate({'height' : '100%'}, 500);
			$('html,body').css({"overflow":"hidden"});
		});
		// não fechar overlay quando clica na área branca	
		$('.overlay-content').click(function(e){
			e.stopPropagation();
		});
		// fecha mudança de combo
		$('.close_overlay, #change-combo-overlay').click(function(){
			$('.overlay').animate({'height' : '0'}, 100);
			setTimeout(function() {
				$('html,body').css({"overflow":"auto"});
			}, 1000);
		});
	});
</script>

<div id="change-combo-overlay" class="overlay">
    <div class="overlay-content">
    	<a href="javascript:void(0)" class="closebtn close_overlay">&times;</a>
    	<br><br>
    	<?php foreach ($tipo_combos as $tipo_combo) { ?>
    		<?php $tipo_combo['id'] == $TipoCombo['id'] ? $class_apply = 'item-combo-active' : $class_apply = 'item-combo-normal' ?>
	    	<div class="col-xs-12 col-sm-6 col-md-4 total-item-combo-select">
	    		<?= $this->Html->link(	
				'<div class="item-combo-select '.$class_apply.'">
					<div class="titulo-combo text-center">
						<span>Combo <br>'
						.$tipo_combo['name'].'</span>
					</div>
					<div class="desc-combo-select text-center">
						<p class="text-left"><i class="fa fa-check" aria-hidden="true"></i>'.$tipo_combo['categoria1'].'</p>
						<p class="text-left"><i class="fa fa-check" aria-hidden="true"></i>'.$tipo_combo['categoria2'].'</p>
						<p class="text-left"><i class="fa fa-check" aria-hidden="true"></i>'.$tipo_combo['categoria3'].'</p>
					</div>
					<div class="text-center content-btn-selecionar-combo">
						<button class="btn btn-block btn-selecionar-combo">Selecionar</button>
					</div>
				</div>'
				, $SSlug.'/selecionar-combo/'.$tipo_combo['id'],['escape' => false])  ?>
	    	</div>

    	<?php } ?>
    </div>
</div>