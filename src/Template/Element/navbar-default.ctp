<script>
    var WEBROOT_URL  = "<?= WEBROOT_URL ?>";
    function OpenBaixoNavbar() {
        document.getElementById("navbar-content").style.height = "100%";
        $("html,body").css({"overflow":"hidden"});
    }
    function closeBaixoNavbar() {
        document.getElementById("navbar-content").style.height = "0%";
        $("html,body").css({"overflow":"auto"});
    }
</script>
<script>
    function openIndique() {
        closeBaixoNavbar();
        document.getElementById("indique").style.height = "100%";
        $("html,body").css({"overflow":"hidden"});
    }
    function closeIndique() {
        document.getElementById("indique").style.height = "0%";
        $("html,body").css({"overflow":"auto"});
    }
    function enviar(){
        document.getElementById('sucesso').style.display = "block"
    }
</script>
<div id="nav_browser" class="nav">
    <div class="indique-content">
        <button class="btn btn-success-new" type="button" onclick="openIndique()" id="indique-mobile">Indicar</button>
        <button class="btn btn-success-new" type="button" onclick="openIndique()" id="indique-desktop">Indique sua academia</button>
        <!-- <button class="btn btn-success" type="button" onclick="openBanner()">1º acesso</button> -->
    </div>
    <div class="logo-content">
        <?= $this->Html->image('new-logfitness-email.jpg',['title' => 'Logfitness', 'alt' => 'Logfitness']) ?>
    </div>
    <div class="menu-content">
        <ul class="menu">
            <li><?= $this->Html->link('academias',WEBROOT_URL.'buscar_academia',['escape'=>false, 'id' => 'academias']) ?></li>
            <li><?= $this->Html->link('entenda a LOG',WEBROOT_URL,['escape'=>false, 'id' => 'entenda']) ?></li>
            <li><?= $this->Html->link('área academia',WEBROOT_URL.'academia/acesso',['escape'=>false, 'id' => 'admin_academia']) ?></li>
            <li><?= $this->Html->link('espaço do time',WEBROOT_URL.'professor/acesso',['escape'=>false, 'id' => 'admin_professor']) ?></li>
        </ul>
    </div>
</div>
<div id="nav_mobile" class="nav">
    <div class="col-xs-12 text-center">
        <div class="logo-mobile text-left">
            <?= $this->Html->image('logobranco.png',['title' => 'Logfitness', 'alt' => 'Logfitness']) ?>
        </div>
        <div class="botao" onclick="OpenBaixoNavbar()">
            <i class="fa fa-arrow-circle-up" aria-hidden="true"></i><br><p>menu</p>
        </div>
    </div>
</div>
<div id="navbar-content" class="overlay">
    <div class="overlay-content">
    <a href="javascript:void(0)" class="closebtn" onclick="closeBaixoNavbar()">&times;</a>
        <ul class="menu-content-mobile">
            <li><?= $this->Html->link('academias',WEBROOT_URL.'buscar_academia',['escape'=>false]) ?></li>
            <li id="btn-indicar"><a href="#" onclick="openIndique()">indicar</a></li>
            <li><?= $this->Html->link('entenda a LOG',WEBROOT_URL,['escape'=>false]) ?></li>
            <li><?= $this->Html->link('admin academia',WEBROOT_URL.'academia/acesso',['escape'=>false]) ?></li>
            <li><?= $this->Html->link('admin professor',WEBROOT_URL.'professor/acesso',['escape'=>false]) ?></li>
        </ul>
    </div>
</div>
<div id="indique" class="overlay">
    <div class="overlay-content text-center">
        <a href="javascript:void(0)" class="closebtn" onclick="closeIndique()">&times;</a>
        <h3>Não encontrou uma academia que você conhece?<br> Indique ela pra gente!</h3>
        <hr>
        <div class="col-xs-12 col-md-6" id="explicativo">
            <h4>Meu primeiro LOG</h4>
            <p>Indique a sua academia e quando ela se cadastrar você ganha um suplemento. <?= $this->Html->image('mini_medal.png',['title' => 'logo-academia', 'alt' => 'Logo']) ?></p>
            <p>Enviamos direto para você retirar na sua academia! <?= $this->Html->image('mini_box.png',['title' => 'logo-academia', 'alt' => 'Logo']) ?></p>
            <p>Só ganha o primeiro a indicar a academia... <?= $this->Html->image('mini_run.png',['title' => 'logo-academia', 'alt' => 'Logo']) ?> CORRA!!</p>
        </div>
        <div class="col-xs-12 col-md-6">
            <?= $this->Form->create(null, [
                'id'        => 'form-indicar-academia',
                'role'      => 'form',
                'default'   => false
            ]) ?>
            <h4>Formulário de indicaçãos</h4>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('responsavel', [
                    'div'           => false,
                    'label'         => false,
                    'class'         => 'form-control',
                    'placeholder'   => 'Academia ou Responsável*',
                    'required'      => true
                ])?>
            </div>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('telephone', [
                    'id'            => 'telephone',
                    'div'           => false,
                    'label'         => false,
                    'class'         => 'form-control',
                    'placeholder'   => 'Telefone do responsável*',
                    'type'          => 'tel',
                    'required'      => true
                ])?>
            </div>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('uf_id', [
                    'label'         => false,
                    'id'            => 'states-indica',
                    'div'           => false,
                    'options'       => $states,
                    'empty'         => 'Selecione um estado',
                    'placeholder'   => 'Estado',
                    'class'         => 'form-control',
                    'required'      => true
                ]) ?>
            </div>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('city_id', [
                    'label'         => false,
                    'id'            => 'city-indica',
                    'div'           => false,
                    'empty'         => 'Selecione uma cidade',
                    'placeholder'   => 'Cidade',
                    'class'         => 'form-control',
                    'required'      =>  true
                ]) ?>
            </div>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('email_indicador', [
                    'div'           => false,
                    'label'         => false,
                    'class'         => 'form-control',
                    'placeholder'   => 'Seu e-mail*',
                    'required'      => true,
                    'type'          => 'e-mail'
                ])?>
            </div>
        </div>
        <div class="col-xs-12">
            <?= $this->Form->button('Enviar', [
                'id'            => 'submit-indicar-academia',
                'type'          => 'button',
                'class'         => 'btn btn-success',
                'escape'        => false,
                // 'onclick'    => 'enviar()'
            ]) ?>
        <?= $this->Form->end()?>
        
        <p id="message-academia-indica"></p>
        </div>
    </div>
</div>
<script>
    $('#states-indica').on('change', function(){
        var state_id = $(this).val();
        $.get(WEBROOT_URL + '/cidades/' + state_id,
            function(data){
                $('#city-indica').html(data);
            });
    });
    $('#submit-indicar-academia').click(function(){
        var form = $('#form-indicar-academia');
        var form_data = form.serialize();
        $.ajax({
            type: "POST",
            url: WEBROOT_URL + 'indicar-academia',
            data: form_data
        })
        .done(function (data) {
            $('#submit-indicar-academia').fadeOut();
            $('#message-academia-indica').html('Indicação enviada com sucesso!');
        });
    });
</script>

<?php if(isset($_GET['indicar'])) { ?>
    <script type="text/javascript">
        openIndique();
    </script>
<?php } ?>