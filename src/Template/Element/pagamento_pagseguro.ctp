<script>
    $(document).ready(function(){
        $('.data-pagseguro').mask('00/00/0000');
    });
</script>

<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 total-pagamentos">
    <ul class="nav nav-tabs">
        <li class="active">
            <a data-toggle="tab" href="#credit_card">
                <i class="fa fa-credit-card fa-2x"></i>
                <br class="clear">
                Cartão de
                <br class="clear">
                crédito
            </a>
        </li>
        <li id="tabBoleto">
            <a data-toggle="tab" href="#boleto_card">
                <i class="fa fa-barcode fa-2x"></i>
                <br class="clear">
                Boleto
                <br class="clear">
                &nbsp 
            </a>
        </li>
        <?php if($RetiradaLoja) { ?>
            <li>
                <a data-toggle="tab" href="#money_card">
                    <i class="fa fa-money fa-2x"></i>
                    <br class="clear">
                    Pagar na
                    <br class="clear">
                    loja
                </a>
            </li>
        <?php } ?>
    </ul>
</div>
<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 total-tabs">
    <div class="tab-content">
        <div id="credit_card" class="tab-pane fade in active">
            <?= $this->Form->create(null, ['class' => 'credit-card-form'])?>
            <div class="row">
                <div class="col-xs-12 bandeiras">
                    <ul>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="visa"> <?= $this->Html->image('bandeiras/visa.png');  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="mastercard"> <?= $this->Html->image('bandeiras/mastercard.png')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="elo"> <?= $this->Html->image('bandeiras/elo.png')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value='diners'> <?= $this->Html->image('bandeiras/dinners.png')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="amex"> <?= $this->Html->image('bandeiras/american_express.jpg')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="aura"> <?= $this->Html->image('bandeiras/aura.png')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="banesecard"> <?= $this->Html->image('bandeiras/banesecard.jpg')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="brasilcard"> <?= $this->Html->image('bandeiras/brasilcard.png')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="cabal"> <?= $this->Html->image('bandeiras/cabal.png')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="fortbrasil"> <?= $this->Html->image('bandeiras/fortbrasil.png')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="grandcard"> <?= $this->Html->image('bandeiras/grandcard.jpg')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="hipercard"> <?= $this->Html->image('bandeiras/hipercard.png')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="mais"> <?= $this->Html->image('bandeiras/mais.jpg')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="personalcard"> <?= $this->Html->image('bandeiras/personal.jpg')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="sorocred"> <?= $this->Html->image('bandeiras/sorocred.png')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="upbrasil"> <?= $this->Html->image('bandeiras/upbrasil.jpg')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="valecard"> <?= $this->Html->image('bandeiras/valecard.jpg')  ?>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10 col-sm-4 col-md-4 form-group">
                    <?= $this->Form->input(null, [
                        'div'           => false,
                        'label'         => 'Número do cartão',
                        'class'         => 'form-control credit-card-obrigatorio credit-card-numero card-number-mask',
                        'placeholder'   => 'Ex: 0000 0000 0000 0000',
                    ])?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-3 col-md-2 form-group">
                    <label>Validade</label>
                    <select class="form-control credit-card-obrigatorio credit-card-validade-mes" autocomplete="off">
                        <option></option>
                        <?php for($m = 1; $m <= 12; $m++) { ?>
                            <option value="<?= str_pad($m, 2, 0, STR_PAD_LEFT) ?>"><?= str_pad($m, 2, 0, STR_PAD_LEFT) ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-2 form-group">
                    <label for="">&nbsp</label>
                    <select class="form-control credit-card-obrigatorio credit-card-validade-ano" autocomplete="off">
                        <option></option>
                        <?php for($m = 0; $m < 30; $m++) { ?>
                            <option value="<?= $m + date('Y') ?>"><?= $m + date('Y') ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-xs-10 col-sm-3 col-md-3 form-group">
                    <?= $this->Form->input(null, [
                        'div'           => false,
                        'label'         => 'Código de Segurança',
                        'class'         => 'form-control credit-card-obrigatorio cvv-mask credit-card-cvv',
                        'placeholder'   => '000',
                        'maxlength'     =>  '4',
                    ])?>
                </div>
                <div class="col-xs-2 form-group div-card">
                    <label for="">&nbsp</label>
                    <span style=" margin-left: 5px;"><i class="fa fa-credit-card fa-2x"></i></span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 form-group">
                    <?= $this->Form->input(null, [
                        'div'           => false,
                        'label'         => 'Nome impresso no cartão',
                        'class'         => 'form-control credit-card-obrigatorio credit-card-titular',
                        'placeholder'   => 'Nome Sobrenome',
                    ])?>
                </div>
                <div class="col-xs-12 col-sm-6 form-group">
                    <?= $this->Form->input(null, [
                        'div'           => false,
                        'label'         => 'CPF do titular',
                        'class'         => 'form-control credit-card-obrigatorio cpf-mask credit-card-cpf',
                        'placeholder'   => '000.000.000-00',
                    ])?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 form-group">
                    <?= $this->Form->input(null, [
                        'div'           => false,
                        'label'         => 'Nascimento',
                        'class'         => 'form-control credit-card-obrigatorio data-pagseguro credit-card-nascimento',
                        'placeholder'   => 'dd/mm/aaaa',
                    ])?>
                </div>
                <div class="col-xs-12 col-sm-6 form-group">
                    <?= $this->Form->input(null, [
                        'div'           => false,
                        'label'         => 'Telefone',
                        'class'         => 'form-control credit-card-obrigatorio phone-mask credit-card-telefone',
                        'placeholder'   => '(00) 00000-0000'
                    ])?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 form-group">
                    <label>Número de parcelas</label>
                    <select class="form-control credit-card-obrigatorio credit-card-parcelas">
                        <option value="">Selecione a bandeira</option>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6 form-group">
                    <p><strong>Resumo de compra</strong></p>
                    <?php if($cupom_invalido) { ?>
                        <p style="color: red">Cupom inválido. Já utilizado por esse CPF</p>
                    <?php } else if($cupom_valido) { ?>
                        <p style="color: green">Cupom de desconto aplicado! =)</p>
                    <?php } ?>
                    <?php if ($DescontoCombo != null) { ?>
                        <p>Desconto combo: <span class="pull-right">R$ <?= number_format(($PedidoCliente->valor / 0.95) * 0.05, 2, ',', '.') ?></span></p>
                    <?php } ?>
                    <p>Total dos produtos: <span class="pull-right">R$ <?= number_format($PedidoCliente->valor, 2, ',', '.') ?></span></p>
                    <p>Frete grátis: <span class="pull-right">R$ 0,00</span></p>
                    
                    <?php if($RetiradaLoja == 601) { ?>
                        <p class="show-endereco">Entrega: <span class="pull-right"><?= $loja_retirada->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                        <p class="detalhe-endereco"><?= $loja_retirada->address." - ".$loja_retirada->number." - ".$loja_retirada->area ?><br>
                            <?=  $loja_retirada->city->name." - ".$loja_retirada->city->uf  ?><br>
                            <?= $loja_retirada->phone ?>
                        </p>
                    <?php } elseif ($RetiradaLoja == 10) { ?>
                        <p class="show-endereco">Entrega: <span class="pull-right"><?= $PedidoCliente->academia->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                        <p class="detalhe-endereco"><?= $PedidoCliente->academia->address." - ".$PedidoCliente->academia->number." - ".$PedidoCliente->academia->area ?><br>
                            <?=  $PedidoCliente->academia->city->name." - ".$PedidoCliente->academia->city->uf  ?><br>
                            <?= $PedidoCliente->academia->phone ?>
                        </p>
                        
                    <?php } elseif ($RetiradaLoja == 20) { ?>
                        <p class="show-endereco">Entrega: <span class="pull-right"><?= $PedidoCliente->cliente->name ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                        <p class="detalhe-endereco"><?= $PedidoCliente->cliente->address." - ".$PedidoCliente->cliente->number." - ".$PedidoCliente->cliente->area ?><br>
                            <?=  $PedidoCliente->cliente->city->name." - ".$PedidoCliente->cliente->city->uf  ?><br>
                            <?= $PedidoCliente->cliente->mobile ?>
                        </p>
                    <?php } ?>
                    
                    
                    <p>Membro do time: <span class="pull-right"><?= $PedidoCliente->professore->name ?></span></p>
                </div>
            </div>
            <input type="hidden" class="credit-card-pedido" value="<?= $PedidoCliente->id ?>">
            <input type="hidden" class="credit-card-aluno" value="<?= $PedidoCliente->cliente->name ?>">
            <input type="hidden" class="credit-card-token">
            <div class="row"> 
                <div class="col-xs-12 form-group text-center">
                    <button type="button" class="btn btn-verde credit-card-btn">
                        <i class="fa fa-credit-card-alt"></i> pagar
                    </button>
                    <br>
                </div>
            </div>
            <?= $this->Form->end()?>
        </div>
        <div id="boleto_card" class="tab-pane fade text-left">
            <div class="row">
                <?= $this->Form->create(null, ['class' => 'boleto-form']) ?>
                    <div class="col-xs-12 col-md-6 text-center form-group">
                        <br>
                        <br>
                        <button type="submit" class="btn btn-verde boleto-btn">
                            <i class="fa fa-barcode"></i>
                            Gerar Boleto
                        </button>

                        <!-- <?= json_encode($PedidoCliente) ?> -->
                        
                    </div>
                    <input type="hidden" class="boleto-pedido" value="<?= $PedidoCliente->id ?>">
                    <input type="hidden" class="boleto-valor" value="<?= number_format($PedidoCliente->valor, 2, '', '') ?>">
                <?= $this->Form->end() ?>
                <div class="col-xs-12 col-md-6 form-group">
                    <p><strong>Resumo de compra</strong></p>
                    <?php if($cupom_invalido) { ?>
                        <p style="color: red">Cupom inválido. Já utilizado por esse CPF</p>
                    <?php } else if($cupom_valido) { ?>
                        <p style="color: green">Cupom de desconto aplicado! =)</p>
                    <?php } ?>
                    <?php if ($DescontoCombo != null) { ?>
                    <p>Desconto combo: <span class="pull-right">R$ <?= number_format(($PedidoCliente->valor / 0.95) * 0.05, 2, ',', '.') ?></span></p>
                    <?php } ?>
                    <p>Total dos produtos: <span class="pull-right">R$ <?= number_format($PedidoCliente->valor, 2, ',', '.') ?></span></p>
                    <p>Frete grátis: <span class="pull-right">R$ 0,00</span></p>
                    <?php if($RetiradaLoja) { ?>
                        <p class="show-endereco">Entrega: <span class="pull-right"><?= $loja_retirada->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                        <p class="detalhe-endereco"><?= $loja_retirada->address." - ".$loja_retirada->number." - ".$loja_retirada->area ?><br>
                            <?=  $loja_retirada->city->name." - ".$loja_retirada->city->uf  ?><br>
                            <?= $loja_retirada->phone ?>
                        </p>
                    <?php } else { ?>
                        <p class="show-endereco">Entrega: <span class="pull-right"><?= $PedidoCliente->academia->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                        <p class="detalhe-endereco"><?= $PedidoCliente->academia->address." - ".$PedidoCliente->academia->number." - ".$PedidoCliente->academia->area ?><br>
                            <?=  $PedidoCliente->academia->city->name." - ".$PedidoCliente->academia->city->uf  ?><br>
                            <?= $PedidoCliente->academia->phone ?>
                        </p>
                    <?php } ?>
                    <p>Membro do time: <span class="pull-right"><?= $PedidoCliente->professore->name ?></span></p>
                </div>
            </div>
            <p><strong>Data de vencimento:</strong> verifique a data de vencimento do boleto que é de 3(três) dias após ser gerado. Caso não seja pago até a data informada o pedido será automaticamente cancelado.</p>
            <p><strong>Prazo de entrega:</strong> é contado a partir da confirmação de pagamento pelo banco. A confirmação pode levar até 2 dias úteis.</p>
            <p><strong>Pagamento:</strong> pode ser feito pela internet e aplicativo do seu banco utilizando o código de barras ou diretamente em bancos, lotéricas e correios com a apresentação o boleto impresso.</p>
            <h4>ATENÇÃO</h4>
            <ul>
                <li>Não será enviada uma cópia impressa do boleto para seu endereço.</li>
                <li>Desabilite o recurso anti pop-up caso você use.</li>
            </ul>
            <br>
            <br>
        </div>
        <?php if($RetiradaLoja) { ?>
            <div id="money_card" class="tab-pane fade text-left">
                <div class="row">
                    <?= $this->Form->create(null, ['url' => ['controller' => 'produtos', 'action' => 'pagar_dinheiro'], 'class' => 'pagar-loja-form']) ?>
                        <div class="col-xs-12 col-md-6 text-center form-group">
                            <h5>Clique no botão para separarmos seu pedido do estoque e você retirá-lo com a gente.</h5>

                            <br>
                            <button type="submit" class="btn btn-verde">
                                <i class="fa fa-money"></i>
                                Pagar na loja
                            </button>
                        </div>
                        <input type="hidden" name="pedido_id" value="<?= $PedidoCliente->id ?>">
                    <?= $this->Form->end() ?>
                    <div class="col-xs-12 col-md-6 form-group">
                        <p><strong>Resumo de compra</strong></p>
                        <?php if($cupom_invalido) { ?>
                            <p style="color: red">Cupom inválido. Já utilizado por esse CPF</p>
                        <?php } else if($cupom_valido) { ?>
                            <p style="color: green">Cupom de desconto aplicado! =)</p>
                        <?php } ?>
                        <?php if ($DescontoCombo != null) { ?>
                        <p>Desconto combo: <span class="pull-right">R$ <?= number_format(($PedidoCliente->valor / 0.95) * 0.05, 2, ',', '.') ?></span></p>
                        <?php } ?>
                        <p>Total dos produtos: <span class="pull-right">R$ <?= number_format($PedidoCliente->valor, 2, ',', '.') ?></span></p>
                        <p>Frete grátis: <span class="pull-right">R$ 0,00</span></p>
                        <?php if($RetiradaLoja) { ?>
                            <p class="show-endereco">Entrega: <span class="pull-right"><?= $loja_retirada->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                            <p class="detalhe-endereco"><?= $loja_retirada->address." - ".$loja_retirada->number." - ".$loja_retirada->area ?><br>
                                <?=  $loja_retirada->city->name." - ".$loja_retirada->city->uf  ?><br>
                                <?= $loja_retirada->phone ?>
                            </p>
                        <?php } else { ?>
                            <p class="show-endereco">Entrega: <span class="pull-right"><?= $PedidoCliente->academia->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                            <p class="detalhe-endereco"><?= $PedidoCliente->academia->address." - ".$PedidoCliente->academia->number." - ".$PedidoCliente->academia->area ?><br>
                                <?=  $PedidoCliente->academia->city->name." - ".$PedidoCliente->academia->city->uf  ?><br>
                                <?= $PedidoCliente->academia->phone ?>
                            </p>
                        <?php } ?>
                        
                        <p>Membro do time: <span class="pull-right"><?= $PedidoCliente->professore->name ?></span></p>
                        
                    </div>
                </div>
                <br>
                <br>
            </div>
        <?php } ?>
    </div>
    </div>
</div>

<!-- PRODUÇÃO -->
<script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
<!-- SANDBOX -->
<!-- <script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script> -->
<!-- RECEBE A SESSÃO GERADA ACIMA -->
<script type="text/javascript">PagSeguroDirectPayment.setSessionId('<?= $session_id ?>');</script>
<script>
 	$(document).ready(function() {
        // console.log(WEBROOT_URL);
        loading.fadeIn();
 		PagSeguroDirectPayment.getPaymentMethods({
		    success: function(response) {
                console.log(response);
                if (response.paymentMethods.BOLETO.options.BOLETO.status != "AVAILABLE") {
                    $('#tabBoleto').hide();
                }
		    },
		    error: function(response) {
		        //tratamento do erro
		    },
		    complete: function(response) {
		    	//tratamento comum para todas chamadas
		    }
		})
        loading.fadeOut();
        
        var valor_mochila = "<?= number_format($PedidoCliente->valor, 2, '.', '') ?>";
        // console.log(valor_mochila);

        $(document).on('click', '.credit-card-bandeira', function() {
            var bandeira_value = $(this).val();
            console.log(bandeira_value);
            $(".credit-card-parcelas option").remove();
            // console.log(valor_mochila);

            PagSeguroDirectPayment.getInstallments(
                {
                    amount: valor_mochila,
                    maxInstallmentNoInterest: '<?= $parcelas_promo_valor ?>',
                    brand: bandeira_value,
                    success: function (response) {
                        console.log(response);
                        var array = response.installments[bandeira_value];
                        array.map(function(value, index){
                            if (value.quantity == 1) {
                                var html = '<option data-id="'+ value.totalAmount.toFixed(2) +'" value="' +value.quantity+ '">'+value.quantity+'x R$'+ value.installmentAmount.toFixed(2) +' = R$'+value.totalAmount.toFixed(2) +' </option>'
                            } else {
                                var html = '<option data-id="'+ value.installmentAmount.toFixed(2) +'" value="' +value.quantity+ '">'+value.quantity+'x R$'+ value.installmentAmount.toFixed(2) +' = R$'+value.totalAmount.toFixed(2) +' </option>'
                            }

                            $('.credit-card-parcelas').append(html);
                        });
                    },
                    error: function (response) {
                        console.log(response);
                    }
                })
        });


        $(document).on('click', '.credit-card-btn', function() {
            var btn = $(this);
            var campos_obrigatorios = $('.credit-card-obrigatorio');
            var bandeira = $('input[name=credit-card-radio-bandeira]:checked').val();
            var numero_cartao = $('.credit-card-numero').val();
                numero_cartao = numero_cartao.replace(/\s/g, '');
            var validade_mes = $('.credit-card-validade-mes option:selected').val();
            var validade_ano = $('.credit-card-validade-ano option:selected').val();
            var validade = validade_mes+'/'+validade_ano;
            var codigo_cvv = $('.credit-card-cvv').val();
            var titular_cartao = $('.credit-card-titular').val();
            var cpf_cartao = $('.credit-card-cpf').val();
            var nascimento_cartao = $('.credit-card-nascimento').val();
            var telefone_cartao = $('.credit-card-telefone').val();
            var parcelas = $('.credit-card-parcelas option:selected').val();
            var pedido_id = $('.credit-card-pedido').val();
            var valor_total = $('.credit-card-parcelas option:selected').attr('data-id');
            var comprador = $('.credit-card-aluno').val();
            var hash = PagSeguroDirectPayment.getSenderHash();

            $('.alert-error').remove();
            loading.fadeIn();

			var param = {
			    cardNumber: numero_cartao,
			    cvv: codigo_cvv,
			    expirationMonth: validade_mes,
			    expirationYear: validade_ano,
			    brand: bandeira,
			    complete: function(response) {
                    $.ajax({
                        type: "POST",
                        url: WEBROOT_URL + 'pagar/pagseguro/credito',
                        async: false,
                        data: {
                            valor_total: valor_total,
                            pedido_id: pedido_id,
                            parcelas: parcelas,
                            hash: hash,
                            card_token: response['card']['token']
                        },
                        success: function(data){
                            if(data == 1) {
                                setTimeout(function() {
                                    loading.fadeOut();
                                    location.href = WEBROOT_URL + 'mochila/finalizado/' + pedido_id;
                                }, 8000);
                            }
                        },
                        error: function(){
                            $('.credit-card-btn').click();
                        }
                    });
			    }
			}

			PagSeguroDirectPayment.createCardToken(param);

            return false;


         });
         

 		$(document).on('click', '.boleto-btn', function() {            
            var btn = $(this);

            var pedido_id = $('.boleto-pedido').val();
            var valor_total = $('.boleto-valor').val();
            var hash = PagSeguroDirectPayment.getSenderHash();

            
            $('.alert-error').remove();

            loading.fadeIn();
            
            
            setTimeout(function(){

            $.ajax({
                type: "POST",
                url: WEBROOT_URL + 'pagar/pagseguro/boleto',
                async: false,
                data: {
                    valor_total: valor_total,
                    pedido_id: pedido_id,
                    hash: hash
                },
                success: function(data){
                    
                    
                    if(data == 0) {
                        loading.fadeOut();
                        btn.before('<p class="alert-error" style="color: red">Falha ao gerar boleto. Tente novamente!</p>');
                    }

                    if(data == 1) {
                        setTimeout(function() {
                        loading.fadeOut();
                        location.href = WEBROOT_URL + 'mochila/finalizado/' + pedido_id;
                    }, 4000);
                    }
                },
                error:function(){
                    $('.boleto-btn').click();
                }
            });

            }, 1000);

            return false;
            
        });
	});
</script>