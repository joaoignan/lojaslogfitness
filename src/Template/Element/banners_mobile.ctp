<?php
$count_banner = count($banners);
if($count_banner >= 1) { ?>
    <div class="col-xs-12">
        <div id="banners_a" style="padding:0;" class="carousel slide col-md-12 col-xs-12 img2-home banners_home" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php
                $i = 0;
                foreach ($banners as $banner):
                    $i++;
                    $i == 1 ? $active = 'active' : $active = '';
                    echo '<li data-target="#banners_a" data-slide-to="'.$i.'" class="'.$active.'"></li>';
                endforeach;
                ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php
                $i = 0;
                foreach ($banners as $banner):
                    $i++;
                    $i == 1 ? $active = 'active' : $active = '';
                    !empty($banner->link) ? $href = 'href="'.$banner->link.'" target="_blank"' : $href = '';
                    echo '<div class="item '.$active.'">';
                    echo '<a '.$href.' >';
                    echo $this->Html->image('banners/'.$banner->image, [
                        'alt' => $banner->name,
                        'class' => 'col-md-12 col-xs-12 img2-home'
                    ]);
                    echo '</a>';
                    echo '</div>';
                endforeach;
                ?>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#banners_a" role="button" data-slide="prev">
                <span class="fa fa-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="right carousel-control" href="#banners_a" role="button" data-slide="next">
                <span class="fa fa-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Próximo</span>
            </a>
        </div>
    </div>
    <?php
} ?>