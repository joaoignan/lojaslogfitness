<style>
    .listaEscolherLoja{
        padding-left: 0;
    }
    .listaEscolherLoja li{
        padding-left: 0px;       
    }
	.listaEscolherLoja img{
		max-width: 70px;
		max-height: 70px;
	}
    .logoEscolherLoja, .dadosEscolherLoja, .acaoEscolherLoja{
        display: inline-block;
        float: left;
    }
    .logoEscolherLoja, .acaoEscolherLoja{
        width: 80px;
        height: 70px;
    }
    .dadosEscolherLoja{
        width: calc(97% - 160px);
        padding: 0 5px;
    }
    .nomeLoja{
        font-weight: 700;
        font-size: 16px;
        display: block;
    }
    .enderecoLoja{
        font-weight: normal;
        font-size: 14px;
        display: block;
    }
    .acessarLoja{
        border-radius: 4px;
        color: rgba(255,255,255,1);
        background-color: #489f36;
    }
    .acessarLoja:hover{
        color: rgba(255,255,255,1);
        background-color: #37772a;
    }
@media all and (max-width: 500px){
    .dadosEscolherLoja{
        padding-top: 0px;
        width: calc(95% - 80px);
    }
    .acaoEscolherLoja{
        margin-top: 7px;
        width: 100%;
    }
    .nomeLoja{
        font-weight: 700;
        font-size: 14px;
    }
    .enderecoLoja{
        font-weight: normal;
        font-size: 12px;
    }
}
</style>

<div id="overlayEscolherLoja" class="overlay">
    <div class="overlay-content">
    	<a href="javascript:void(0)" class="closebtn close_overlay">&times;</a>
    	<div class="row">
    		<div class="col-xs-12">
    			<br>
    			<br>
    			<ul class="listaEscolherLoja">
                    <?php foreach ($select_academias_list as $select_academia_list){ ?>     
                        <li>
                            <div class="logoEscolherLoja">
                                <?php if ($select_academia_list->image != null) { 
                                    echo $this->Html->image('academias/'.$select_academia_list->image, ['alt' => 'logo '.$select_academia_list->shortname]);
                                } ?>
                            </div>
                            <div class="dadosEscolherLoja">
                                <span class="nomeLoja"><?= $select_academia_list->shortname ?></span>
                                <span class="enderecoLoja"><?= $select_academia_list->address.', '.$select_academia_list->number.' - '.$select_academia_list->area.' - '.$select_academia_list->city->name.' - '.$select_academia_list->city->uf ?></span>
                            </div>
                            <br>
                            <div class="acaoEscolherLoja text-center">
                                <?= $this->Html->link('<button class="btn acessarLoja">Acessar</button>', '/'.$select_academia_list->slug,['escape' => false]); ?>
                            </div>
                        </li>
                            <hr>
                    <?php } ?>
    			</ul>
    		</div>
    	</div>
    </div>
</div>