<style>

/*Configurações Padrões*/
ul.filtrar, .filtrar li, .filtrar a{ margin:0; padding:0; list-style:none; text-decoration:none; z-index: 1; font-size: 14px;}
ul.filtrar ul{ position:absolute;left: 0; display:none;}
 
/* Configurações nivel 1*/
ul.filtrar{ float:left; width: 100%; border-radius:5px; padding:0 5px; }
.filtrar li{ float:left; width:auto; position:relative; display: table-cell;}
.filtrar li a{ display:block; padding:0 9px; line-height:45px; height:45px; float:left; transition:all 0.1s linear; }
 
/* Configurações nivel 2*/
.filtrar li:hover > ul.submenu-1{ display:block; top:45px; position: absolute; left:0; padding:5px; width:200px; border-radius:0 0 5px 5px;   }
.filtrar ul.submenu-1 a{  width:190px; padding:0 20px; border-radius:5px;  }
 
/*Configurações de cores*/
 
/*nivel 1*/
.filtrar{background:#5089cf; }
.filtrar a{ color:#fff;}
.filtrar li:hover > a{ background:rgba(50, 86, 130, 1.00);  color:#fff;}
 
/*nivel 2*/
.submenu-1{ background:#fff;}
.submenu-1 a{color:#5089cf;}
.submenu-1 li:hover > a{ background:rgba(50, 86, 130, 1.00) }
</style>

<div class="container" style="display: none;">
    <ul class="filtrar"> <!-- Esse é o 1 nivel ou o nivel principal -->
    <li><a href="#">marcas  <span class="caret"></span></a>
        <ul class="submenu-1">
            <?php foreach($marcas as $marca): ?>
            <li>
                <a data-id="<?= $marca->id; ?>" class=""><?= ucwords(strtolower($marca->name)); ?></a>
            </li>
            <?php endforeach; ?>
        </ul>
    </li>
    <?php foreach($objetivos as $objetivo): ?>
    <li>
        <a href="#"><?= $objetivo->slug ?> <span class="caret"></span></a>
        <ul class="submenu-1">
            <?php foreach($categorias as $produto_categoria):
            if(in_array($produto_categoria->id, $produtos_categorias[$objetivo->id])):
            ?>
            <li>
                <a data-cid="<?= $produto_categoria->id; ?>" data-oid="<?= $objetivo->id; ?>"><?= $produto_categoria->name; ?></a>
            </li>
            <?php
            endif;
            endforeach;
            ?>
        </ul>
    </li>
    <?php endforeach; ?> 
    </ul>
</div>
    
    <!-- <nav class="navbar nav-filtro">
        <div class="container-fluid container-filtro">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#dropdown-filtro" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-bars"></i>
              </button>
            </div>
            <div class="collapse navbar-collapse" id="dropdown-filtro">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">marcas
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php foreach($marcas as $marca): ?>
                            <li>
                                <a data-id="<?= $marca->id; ?>" class=""><?= ucwords(strtolower($marca->name)); ?></a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                    <?php foreach($objetivos as $objetivo): ?>
                    <li>
                        <a href="#"><?= $objetivo->slug ?> <span class="caret"></span></a>
                        <ul class="submenui-1">
                        <?php foreach($categorias as $produto_categoria):
                            if(in_array($produto_categoria->id, $produtos_categorias[$objetivo->id])):
                                ?>
                                <li>
                                    <a data-cid="<?= $produto_categoria->id; ?>"
                                       data-oid="<?= $objetivo->id; ?>"
                                       class="link-produto-categoria">
                                        <?= $produto_categoria->name; ?>
                                    </a>
                                </li>
                                <?php
                            endif;
                        endforeach;
                        ?>
                        </ul>
                    </li>
                    <?php endforeach; ?>
                </ul>
                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Buscar">
                    </div>
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
    </nav> -->