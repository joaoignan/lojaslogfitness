<style>
    .logo-express{
      float: right;
      display: flex;
      align-items: center;
      justify-content: center;
      height: 110px;
    }
    .avatar-aluno{
      height: 40px;
      width: 40px;
      border: 2px solid #aaa;
      float: left;
      border-radius: 25px;
    }
    .logo-trustvox img{
      width: 80px;
      margin-top: 5px;
      cursor: pointer;
    }
    @media all and (min-width: 769px) {
      .sub-menu {
          color: white;
      }
      .btn-login-nav {
          padding: 1px 6px;
          margin-top: 6px;
          line-height: 1.3em !important;
          border-color: #8dc73f;
          background-color: #8dc73f;
          color: white;
      }
      .icones {
          margin-top: 40px;
          float: left;
      }
      .notify {
          right: -216px;
          top: 50px;
      }
      .profile {
          display: none;
          position: absolute;
          width: 200px;
          background: #FFF;
          z-index: 9;
          right: -20px;
          top: 38px;
          color: #333;
          padding: 2px 10px;
          border-radius: 0 0 5px 5px;
          border: 1px solid rgba(80,135,199,0.59);
          box-shadow: -1px 1px 8px #AAA;
      }
      .profile::before {
          content: "";
          width: 0px;
          height: 0px;
          border-style: solid;
          border-width: 6px 6px 6px 6px;
          border-color: transparent transparent #5089cf transparent;
          position: absolute;
          right: 42px;
          top: -12px;
      }
      .profile::after {
          content: "";
          width: 200px;
          height: 12px;
          background-color: transparent;
          position: absolute;
          right: 0px;
          top: -12px;
      }
      .dropdown-user {
          position: relative;
          display: inline-block;
      }
      .dropdown-user:hover .profile {
          display: block;
      }
      .profile span {
          display: block;
          font-size: 20px;
          line-height: 30px;
          font-weight: 700;
          position: relative;
      }
      .sub-icones i:hover {
          color: #23527c;
      }
      #bem-vindo {
          color: #5089cf;
          font-size: 2em!important;
          margin-top: -2px;
      }
      .menu-principal {
          width: 100%;
          box-shadow: 1px 1px 5px 0px rgba(0,0,0,0.75);
      }
      .icones-deslogado {
          float: left;
          right: 40px;
          margin-top: 15px;
      }
      .icones-logado {
          float: left;
          margin-right: 20px;
          margin-top: 32px;
      }
      .navbar-header {
          text-align: center;
      }
      .logo-topo {
          margin-top: 7px;
      }
      .logo-topo-logfitness {
          margin-top: 15px;
      }
      .menu-principal {
          height: 110px;
      }
      .button-busca{
          background-color: #5089CF;
          border: 1px solid #5089CF;
          padding: 1px 15px 3px 15px;
          margin: 0;
          right: 0;
          position: absolute;
          float: right;
          color: #fff;
      }
      .logo-logfitness img{
          width: 100%;
      }
      .logo-margin {
          margin-left: 70px!important;
      }
      nav {
          position: fixed;
          z-index: 200;
          width: 100%;
      }
      .reparo-nav {
          height: 110px;
          width: 100%;
          background-color: white;
      }
      .sub-menu {
          color: #5089cf;
      }
      .reparo-nav {
        display: block;
      }
      #menu-principal {
        display: block;
      }
      .search-navbar-mobile,
      .menu-mobile,
      .navbar-mobile,
      .fundo-branco {
        display: none;
      }
      .busca-completa {
        margin-top: 25px;
      }
    }
    
    @media all and (max-width: 1200px) and (min-width: 780px) {
        .form-busca {
            width: 80%!important;
        }
    }
    @media all and (max-width: 950px) {
        .icones-logado {
            width: 23%;
        }
        .navbar-header {
            width: 180px;
        }
    }

    @media all and (max-width: 768px) {
        #menu-principal {
            display: none!important;
        }
        .navbar-mobile,
        .fundo-branco {
            display: block!important;
        }
        .reparo-nav {
            display: none!important;
        }
        .form-busca {
          width: 91%!important;
        }
        .navbar-mobile {
          width: 100%;
          height: 50px;
          background-color: white;
          z-index: 3100;
          position: fixed;
          top: 0;
          box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
        }

        .menu-mobile {
          display: none; 
          width: 100%;
          background-color: white;
          z-index: 4000;
          position: fixed;
          text-align: left;
          top: 50px;
          bottom: 45px;
          box-shadow: 0px 3px 5px 0px rgba(50,50,50,0.75);
          padding-top: 10px;
          padding-bottom: 50px;
          overflow-y: auto;
        }

        .menu-mobile p,
        .menu-mobile span {
          color: #5089cf;
          font-weight: 700;
          font-size: 18px;
          padding-left: 25px;
        }

        .menu-mobile i,
        .menu-mobile img {
          width: 20px;
        }

        .search-navbar-mobile {
          display: none; 
          width: 100%;
          height: 40px;
          background-color: white;
          z-index: 3000;
          position: fixed;
          top: 50px;
          box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
        }
        
        .c-hamburger {
          display: block;
          position: relative;
          overflow: hidden;
          margin: 0;
          margin-left: 7px;
          padding: 0;
          width: 50px;
          height: 50px;
          font-size: 0;
          text-indent: -9999px;
          appearance: none;
          box-shadow: none;
          border-radius: none;
          border: none;
          cursor: pointer;
          transition: background 0.3s;
          z-index: 400;
        }

        .c-hamburger:focus {
          outline: none;
        }

        .c-hamburger span {
          display: block;
          position: absolute;
          top: 21px;
          left: 8px;
          right: 8px;
          height: 6px;
          background: #5089cf;
        }

        .c-hamburger span::before,
        .c-hamburger span::after {
          position: absolute;
          display: block;
          left: 0;
          width: 100%;
          height: 6px;
          background-color: #5089cf;
          content: "";
        }

        .c-hamburger span::before {
          top: -12px;
        }

        .c-hamburger span::after {
          bottom: -12px;
        }

        .c-hamburger--htx {
          background-color: transparent;
        }

        .c-hamburger--htx span {
          transition: background 0s 0.3s;
        }

        .c-hamburger--htx span::before,
        .c-hamburger--htx span::after {
          transition-duration: 0.3s, 0.3s;
          transition-delay: 0.3s, 0s;
        }

        .c-hamburger--htx span::before {
          transition-property: top, transform;
        }

        .c-hamburger--htx span::after {
          transition-property: bottom, transform;
        }

        .c-hamburger--htx.is-active span {
          background: none;
        }

        .c-hamburger--htx.is-active span::before {
          top: 0;
          transform: rotate(45deg);
        }

        .c-hamburger--htx.is-active span::after {
          bottom: 0;
          transform: rotate(-45deg);
        }

        .c-hamburger--htx.is-active span::before,
        .c-hamburger--htx.is-active span::after {
          transition-delay: 0s, 0.3s;
        }

        .logo-topo {
            max-height: 40px;
            width: auto;
        }
        .box-search-mobile {
            float: right;
            height: 50px;
            width: 60px;
            line-height: 60px;
            position: absolute;
            text-align: center;
            top: 0;
            right: 0;
            z-index: 400;
        }
        .box-logar-mobile {
            float: left;
            height: 50px;
            line-height: 60px;
            text-align: center;
            z-index: 400;
        }
        .box-logo-mobile {
            width: 100%;
            height: 50px;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .academia-logo {
            font-size: 1.2em;
            line-height: 20px;
        }
        .button-busca {
            right: 15px;
            height: 34px;
        }
        .form-search {
            border-radius: 0;
            position: absolute;
            width: 91%!important;
        }
        .navbar-mobile .col-xs-3,
        .navbar-mobile .col-xs-6 {
            padding: 0;
        }
        .btn-login-nav {
            padding: 1px 6px;
            margin-top: 6px;
            line-height: 1.3em !important;
            border-color: #8dc73f;
            background-color: #8dc73f;
            color: white;
            width: 130px;
        }
        .separador {
            width: 100%;
            border: 1px solid #5087c7;
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .notify {
            width: 100%;
            left: 0;
            top: 50px;
            position: fixed;
            border: none;
            min-height: inherit;
        }
        #notificacoes-mobile {
            color: #5089cf;
        }
        .icones-fundo-branco {
          width: 20%;
          text-align: center;
          float: left;
          height: 45px;
        }
        .icones-fundo-branco .fa {
          color: white;
          line-height: 30px;
        }
        .icones-fundo-branco .fa:hover {
          color: rgba(255,255,255,.7);
        }
        .close-filter, .close-mochila {
          position: relative!important;
          line-height: 30px;
          height: 30px;
          top: 0!important;
          left: 0!important;
          bottom: 0!important;
          right: 0!important;
          font-size: 2em!important;
          display: inline!important;
        }
        .texto-icones-fundo {
          margin: 0;
          font-size: 13px;
          color: white;
          line-height: 13px;
          text-transform: lowercase;
          margin-top: -1px;
        }
        .fechar-menu-mobile {
          position: absolute;
          right: 20px;
          top: 15px;
        }
    }
    @media all and (max-width: 500px) {
        .form-busca {
            width: 90%;
        }
    }
    @media all and (min-width: 769px) {
        .notify-mobile {
            display: none!important;
        }
    }
</style>

<script>
    $(document).ready(function() {
        function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
        }
        
        var notif = getCookie("notif");
        var notif_pos = <?= count($notifications) ?>;

        if (notif_pos != notif) {
            $('.fa-globe').addClass('notify-badge badge-position');
            $('#notify-button').click(function() {
                $('.fa-globe').removeClass('notify-badge badge-position');
                setCookie("notif", notif_pos, 600);
            });
        } else {
            $('#notify-button').click(function() {
                $('.fa-globe').removeClass('notify-badge badge-position');
                setCookie("notif", notif_pos, 600);
            });
        }
        $('#notify-button').click(function(event) {
            event.stopPropagation();
            $('.notify').fadeToggle();
        });

        $('.notify').click(function(event) {
            event.stopPropagation();
        });

        var ctrl_hamburger = 0;
        var search_aberto = 0;
        var medida_tela = $(window).width();

        if(medida_tela <= 768) {

          $('#eu-menu').click(function(e) {
            e.preventDefault();
            if(ctrl_hamburger == 0) {
                $(this).addClass('is-active');
                $('.menu-mobile').slideDown();
                $('body').css('overflow', 'hidden');

                if($('.filtro').width() > 1) {
                  $('.close-filter').click();
                }

                if($('.mochila').width() > 1) {
                  $('.close-mochila').click();
                }

                if(search_aberto == 1) {
                  $('#botao-search').click();
                }

                ctrl_hamburger = 1;
            } else {
                $(this).removeClass('is-active');
                $('.menu-mobile').slideUp(600); 
                $('body').css('overflow', 'auto');
                ctrl_hamburger = 0;
            }

            if(search_aberto == 1 || ctrl_hamburger == 1) {
              $('.navbar-mobile').css('box-shadow', 'none');
            } else {
              $('.navbar-mobile').css('box-shadow', '0px 0px 5px 1px rgba(50,50,50,0.75)');
            }
          });

          $('#botao-search').click(function(e) {
            e.preventDefault();
            if(search_aberto == 0) {
              $('.search-navbar-mobile').slideDown();

              if($('.filtro').width() > 1) {
                $('.close-filter').click();
              }

              if(ctrl_hamburger == 1) {
                $('#eu-menu').click();
              }

              if($('.mochila').width() > 1) {
                $('.close-mochila').click();
              }

              search_aberto = 1;
            } else {
              $('.search-navbar-mobile').slideUp(600);
              search_aberto = 0;
            }

            if(search_aberto == 1 || ctrl_hamburger == 1) {
              $('.navbar-mobile').css('box-shadow', 'none');
            } else {
              $('.navbar-mobile').css('box-shadow', '0px 0px 5px 1px rgba(50,50,50,0.75)');
            }
          });

          $('.close-filter, .close-mochila').click(function() {
          if(search_aberto == 1) {
            $('#botao-search').click();
          }
          if(ctrl_hamburger == 1) {
            $('#eu-menu').click();
          }
          });

          $('.close-mochila').click(function() {
          if($('.filtro').width() > 1) {
            $('.close-filter').click();
          }
          });

          $('.close-filter').click(function() {
          if($('.mochila').width() > 1) {
            $('.close-mochila').click();
          }
          });

          $('.fechar-menu-mobile').click(function() {
            $('#eu-menu').click();
          });

          $('#notificacoes-mobile').click(function() {
            $('.notify').slideToggle();
          });

          var contador = 0;

          $('.produto-qtd').each(function() {
              contador = contador + parseInt($(this).val());
          });

          $('.contador-itens').html(contador);
        }
    });
</script>

<nav class="navbar-default" id="menu-principal">
    <div class="container menu-principal" >
        <div id="logo-esquerdo" class="navbar-header logo-margin">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="helper"></span>
            <?php
                if(isset($adm_academia)){
                if($adm_academia->image != null &&  $adm_academia->image != '' && $adm_academia->image != 'default.png'){
                    echo $this->Html->link(
                        $this->Html->image('academias/'.$adm_academia->image, ['alt' => 'LogFitness', 'class' => 'logo-topo']),
                        '/academia/admin/express',
                        ['escape' => false, 'class' => 'inline-block']
                    );
                    ?>
                    <script>
                        $('#logo-esquerdo').removeClass('logo-margin');
                    </script>
                    <?php
                }else{
                    echo $this->Html->link(
                        '<span class="academia-logo">'
                        .str_replace(array('academia', 'Academia'), '', $adm_academia->shortname)
                        .'</span>',
                        '/academia/admin/express',
                        ['escape' => false, 'class' => 'inline-block link-logo']
                    );
                }
            }else{
                echo $this->Html->link(
                    $this->Html->image('logfitness-express.png', ['alt' => 'LogFitness', 'class' => 'logo-topo']),
                    '/academia/admin/express',
                    ['escape' => false, 'class' => 'inline-block']
                );
            }
            ?>
        </div>
        
        <div class="navbar-header logo-logfitness logo-express">
            <span class="helper"></span>
                <?php
                echo $this->Html->link(
                    $this->Html->image('logfitness-express.png', ['alt' => 'LogFitness', 'class' => 'logo-topo']),
                    WEBROOT_URL.'academia/admin/express',
                    ['escape' => false, 'class' => 'inline-block']
                );
            ?>
        </div>
        <div class="col-lg-4 col-sm-3 left busca-completa">
            <?= $this->Form->create(null, [
                'class'     => 'navbar-form search-form-express',
                'default'   => false,
                'onsubmit'  => 'return false;'
            ])?>

            <?php $academia_name = explode(' ', $Academia->name); ?>
            <input class="form-busca form-control search-query" placeholder="<?= array_shift($academia_name) ?>, quer ajuda?" type="text"/>

            <button class="button-busca"
                    type="button"
                    id="search-button-express"
                    style="height: 34px;">
                <i class="fa fa-search"></i>
            </button>
            <?= $this->Form->end()?>
        </div>

        <div class="icones-logado col-sm-2 text-right">
            <div class="dropdown-user">
              <?php $academia_name = explode(' ', $Academia->name);
              echo '<span id="bem-vindo" class="usuario text-uppercase fa fa-user"></span>'.$this->Html->link('<span>&nbsp'.array_shift($academia_name).'</span>',
                  '#',
                  ['escape' => false, 'id' => 'logado','class' => 'sub-menu usuario text-uppercase', 'title' => '']
              ); ?>

              <div class="profile text-left" style="width: 90px">
                  <?= $this->Html->link('<p><i class="fa fa-undo"></i>&nbsp;&nbsp;Voltar</p>',
                      WEBROOT_URL.'/academia/admin/dashboard', ['escape' => false]
                  ); ?>
              </div>
            </div>
         </div>
        <div class="col-sm-1 logo-trustvox ts-modal-trigger" id="trustvox-selo-site-sincero" style="height: 110px; float: right;">
          <?= $this->Html->image('selo-trustvox.png', ['alt' => 'trustvox']); ?>
        </div>
        <script type="text/javascript">
          var _trustvox_certificate = _trustvox_certificate || [];
          _trustvox_certificate.push(['_certificateId', 'logfitness']);
          (function() {
            var tv = document.createElement('script'); tv.type = 'text/javascript'; tv.async = true;
            tv.src = '//s3-sa-east-1.amazonaws.com/trustvox-certificate-modal-js/widget.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tv, s);
          })();
        </script>
</nav>

<div class="reparo-nav"></div>


<div class="navbar-mobile">
    <div class="col-xs-3">
      <button class="c-hamburger c-hamburger--htx">
        <span>toggle menu</span>
      </button>
    </div>
    <div class="col-xs-6" style="margin-top: 9px;">
      <?php
          echo $this->Html->link(
              $this->Html->image('logfitness.png', ['alt' => 'LogFitness', 'class' => 'logo-topo']),
              WEBROOT_URL.'professor-indicacao',
              ['escape' => false, 'class' => 'inline-block']
          );
      ?>
    </div>
    <div class="box-search-mobile col-xs-3">
        <?= $this->Html->link('<i class="fa fa-2x fa-search"></i> <span class="hidden-xs"></span>',
            '#',
            [
                'escape'    => false,
                'title'     => 'Busca',
                'id'        => 'botao-search'
            ]
        );?>
    </div>
</div>

<div class="menu-mobile" style="text-align: left">
  <?php
      $academia_name = explode(' ', $Academia->name);?>
      
      <p style="font-size: 22px; text-align: center; padding-left: 0">Olá <?= array_shift($academia_name) ?></p>

      <hr class="separador" style="width: 90%">
      
      <?php
      echo $this->Html->link('<p><i class="fa fa-undo"></i>&nbsp;&nbsp;Voltar</p>',
          WEBROOT_URL.'/professores/admin/aluno/indicacoes-de-produtos', ['escape' => false]
      );
  ?>
</div>

<div class="search-navbar-mobile">
    <div>
        <?= $this->Form->create(null, [
            'class'     => 'navbar-form search-form-express',
            'default'   => false,
            'onsubmit'  => 'return false;'
        ])?>
        <?php $client_name = explode(' ', $CLIENTE->name); ?>
        <input class="form-busca form-control search-query" placeholder="<?= array_shift($client_name) ?>, quer ajuda?" type="text"/>

        <button class="button-busca"
                type="button"
                id="search-button-express">
            <i class="fa fa-search"></i>
        </button>
        <?= $this->Form->end()?>
    </div>
</div>

<div class="fundo-branco">
    <span class="fa fa-sliders fa-2x close-filter-icon close-filter"></span>

    <div>
      <?php echo $this->Html->link(
          '<span class="academia-logo" style="font-size: 1.5em; line-height: 10px; color: #f3d117;">ADMINISTRATIVO</span>',
          WEBROOT_URL.'professor-indicacao',
          ['escape' => false, 'class' => 'inline-block link-logo']
      ); ?>  
    </div>

    <svg class="close-mochila-icon close-mochila" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"><g><polygon points="8.669,48.706 5,62.65 6.565,64.773 10.008,54.727  "/><polygon points="93.422,64.79 95,62.65 91.331,48.706 89.991,54.727  "/><polygon points="41.311,14.947 39.661,15.937 37.462,30.863 37.071,40.502 39.622,40.182 41.47,14.947  "/><polygon points="62.928,40.51 62.538,30.863 60.338,15.937 58.689,14.947 58.529,14.947 60.378,40.188  "/><polygon points="29.01,42.523 21.586,43.458 9.53,47.966 11.045,54.785 5.5,70.967 5.5,88.654 27.14,88.654  "/><polygon points="36.48,55.079 35.119,88.654 64.879,88.654 63.519,55.079  "/><polygon points="52.475,40.195 47.463,40.199 37.03,41.513 36.716,49.244 63.282,49.244 62.969,41.516  "/><polygon points="88.954,54.785 90.47,47.966 78.413,43.458 70.989,42.524 72.859,88.654 94.5,88.654 94.5,70.967  "/><path d="M28.12,89.154h5.978l1.422-35.075h28.96L65.9,89.154h5.979l-2.368-58.421L65.62,15.752l-7.219-4.906h-7.104h-2.595h-7.105   l-7.218,4.906l-3.891,14.981L28.12,89.154z M51.578,13.947h7.388l2.256,1.354l2.312,15.433l0.79,19.511H35.675l0.792-19.511   l2.311-15.433l2.257-1.354h7.386H51.578z"/></g></svg>
</div>