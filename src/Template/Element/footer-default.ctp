<?= $this->Html->css('footer.css') ?>

<div class="text-center footer-total">
    <div class="container">
        <div class="col-xs-12 text-center">
            <span>Copyright ©<?= date("Y") ?> www.logfitness.com.br. TODOS OS DIREITOS RESERVADOS.
                Todo o conteúdo do site, todas as fotos, imagens, logotipos, marcas, dizeres, som, software, conjunto imagem,
                layout, trade dress, aqui veiculados são de propriedade exclusiva da LogFitness. É vedada qualquer reprodução,
                total ou parcial, de qualquer elemento de identidade, sem expressa autorização. A violação de qualquer direito mencionado
                implicará na responsabilização cível e criminal nos termos da Lei.
                CACIQUE DIGITAL REPRESENTACOES LTDA - ME - CNPJ: 22.921.802/0001-77 Rua Antonio Munia, 141, Sala 01 - Jardim Nazareth - CEP 15054-160 - São José do Rio Preto - SP
            </span>
        </div>
    </div>
</div>