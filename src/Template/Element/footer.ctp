<!-- RODAPÉ -->
<style>
    .links-footer{
      font-size: 13px;
      font-family: nexa_boldregular;
      color: #5089cf;
    }
    .area-restrita p{
      font-family: nexa;
      margin-bottom: 15px;
    }
    .rodape{
        display: block;
        padding-left: 170px;
        padding-right: 170px;
    }
    .footer-total{
        margin-top: 30px;
    }
    .list-inline{
        background-color: #5089cf;
        margin: -30px -15px 10px -15px;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .fa-footer{
        color: #f5d737;
    }
    @media all and (max-width: 1024px) {
        .rodape{
            padding-left: 200px;
            padding-right: 200px;
        }
    }
    @media all and (max-width: 768px) {
        .rodape{
            padding-left: 10px;
            padding-right: 10px;
            margin-bottom: 50px;
        }
      }    
</style>

<?= $this->Html->script('jquery.mask.min.js')?>

<div class="col-xs-12 text-center footer-total">
    <div class="container">
        <footer>
          <div class="col-sm-12 rodape font-10 text-center">
            <p>
              Copyright ©<?= date("Y") ?> <span>www.logfitness.com.br</span>. TODOS OS DIREITOS RESERVADOS.
              Todo o conteúdo do site, todas as fotos, imagens, logotipos, marcas, dizeres, som, software, conjunto imagem,
              layout, trade dress, aqui veiculados são de propriedade exclusiva da LogFitness. É vedada qualquer reprodução,
              total ou parcial, de qualquer elemento de identidade, sem expressa autorização. A violação de qualquer direito mencionado
              implicará na responsabilização cível e criminal nos termos da Lei.

              CACIQUE DIGITAL REPRESENTACOES LTDA - ME - CNPJ: 22.921.802/0001-77 Rua Antonio Munia, 141, Sala 01 - Jardim Nazareth - CEP 15054-160 - São José do Rio Preto - SP   -   
            </p>
          </div>
        </footer>
    </div>
</div>