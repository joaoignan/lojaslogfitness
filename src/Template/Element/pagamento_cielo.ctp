<script>
    $(document).ready(function() {

    $(document).on('click', '.debit-card-btn', function() {
        var btn = $(this);
        var campos_obrigatorios = $('.debit-card-obrigatorio');
        var bandeira = $('input[name=debit-card-radio-bandeira]:checked').val();

        var numero_cartao = $('.debit-card-numero').val();
        var validade_mes = $('.debit-card-validade-mes option:selected').val();
        var validade_ano = $('.debit-card-validade-ano option:selected').val();
        var validade = validade_mes+'/'+validade_ano;
        var codigo_cvv = $('.debit-card-cvv').val();
        var titular_cartao = $('.debit-card-titular').val();
        var valor_total = $('.debit-card-valor').val();
        var pedido_id = $('.debit-card-pedido').val();
        var comprador = $('.debit-card-aluno').val();

        loading.fadeIn();

        var campos_nulos = 0;

        $('.alert-error').remove();

        campos_obrigatorios.each(function() {
            if($(this).val() == '' || $(this).val() == null) {
                campos_nulos = 1;
            }
        })

        if(bandeira == '' || bandeira == null) {
            campos_nulos = 1;
        }

        if(campos_nulos == 1) {
            btn.before('<p class="alert-error" style="color: red">Preencha todos os dados!</p>');
            loading.fadeOut();
        }

        if(campos_nulos != 1) {
            if(bandeira == 'Logcard') {
                bandeira = 'Visa';
            }

            $.ajax({
                type: "POST",
                url: WEBROOT_URL + 'pagar/cielo/debito',
                data: {
                    card_numero: numero_cartao,
                    card_validade: validade,
                    card_cvv: codigo_cvv,
                    card_titular: titular_cartao,
                    card_bandeira: bandeira,
                    valor_total: valor_total,
                    pedido_id: pedido_id,
                    comprador: comprador
                }
            })
                .done(function (data) {
                    
                    loading.fadeOut();
                        
                    if(data == 0) {
                        btn.before('<p class="alert-error" style="color: red">Pagamento não autorizado.</p><br><p class="alert-error" style="color: red">Confira os dados, tente novamente e se o erro persistir, contate a emissora do seu cartão.</p>');

                        campos_obrigatorios.each(function() {
                            $(this).val('');
                        });
                    } else {
                        location.href = data;
                    }
                });
        }

        return false;
    });

    $.ajax({
        type: "POST",
        url: WEBROOT_URL + 'pagar/cielo/credito',
        data: {
            card_numero: numero_cartao,
            card_validade: validade,
            card_cvv: codigo_cvv,
            card_titular: titular_cartao,
            card_bandeira: bandeira,
            card_cpf: cpf_cartao,
            card_nascimento: nascimento_cartao,
            card_telefone: telefone_cartao,
            parcelas: parcelas,
            valor_total: valor_total,
            pedido_id: pedido_id,
            comprador: comprador
        }
    })
        .done(function (data) {
            
            loading.fadeOut();
            
            if(data == 0) {
                btn.before('<p class="alert-error" style="color: red">Pagamento não autorizado.</p><br><p class="alert-error" style="color: red">Confira os dados, tente novamente e se o erro persistir, contate a emissora do seu cartão.</p>');

                campos_obrigatorios.each(function() {
                    $(this).val('');
                });
            }

            if(data == 1) {
                location.href = WEBROOT_URL + 'mochila/finalizado/' + pedido_id;
            }
        });

    })
</script>