<div class="total-banner">	
	<div class="owl-carousel owl-carousel-banner">
		<?php foreach($banners_promocoes as $banner_promocao) { ?>
			<?php if ($banner_promocao->target == 1) { ?>
				<div class="item">
	            	<?= $this->Html->link('<div class="bannerModelo1" style="background-image: url(../../img/banners-loja/'.$banner_promocao->image.')"></div>', $SSlug.$banner_promocao->link,['escape' => false, 'target' => '_blank']) ?>
	        	</div>
			<?php } else { ?>
				<div class="item">
					<div class="bannerModelo1" style="background-image: url(../../img/banners-loja/<?= $banner_promocao->image ?>)"></div>
				</div> 
			<?php } ?>   	        
		<?php } ?>
	</div>
</div>

<style>
	.total-banner{
		position: absolute;
		left: 0;
		right: 0;
		top: 90px;
	}
	.owl-carousel-banner .owl-wrapper-outer{
		padding: 0!important;
	}
	.bannerModelo1{
		width: 100%;
		height: 700px;
		background-position: center top;
		margin-bottom: 15px;
	}
</style>