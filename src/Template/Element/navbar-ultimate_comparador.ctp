<script>
    function openIndique() {
        document.getElementById("indique").style.height = "100%";
        $("html,body").css({"overflow":"hidden"});
    }
    function closeIndique() {
        document.getElementById("indique").style.height = "0%";
        $("html,body").css({"overflow":"auto"});
    }
    function enviar(){
        document.getElementById('sucesso').style.display = "block"
    }
</script>
<?= $this->Element('comparador_comparador'); ?>
<div class="navbar ultimate">
	<div class="container">
        <div class="row">
    		<div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 text-center menubtn">
                <div class="col-xs-6 col-md-12 text-left" id="abrir-menu">
                    <button ><i class="fa fa-bars"></i><span class="txt-wrap"><br>Menu</span></button>
                </div>
                <div class="col-xs-6 col-md-12 filtro-navbar-btn text-center">
                    <i class="fa fa-search"></i><span class="txt-wrap"><br>Produtos</span>
                </div>
    		</div> <!-- menubtn -->
    		<div class="col-xs-4 col-sm-4 col-md-6 col-lg-6 logo-academias">
                <?= $this->Html->link($this->Html->image('academias/comparadorlog.jpg', ['alt' => 'Comparador', 'title' => 'Comparador de suplementos', 'class' => 'tablet-hidden']),'/comparador',['escape' => false]); ?>
    			<?= $this->Html->link($this->Html->image('academias/comparadorlog_mobile.jpg', ['alt' => 'Comparador', 'title' => 'Comparador de suplementos', 'class' => 'hidden-desk']),'/comparador',['escape' => false]); ?>
    		</div> <!-- logo-academias -->
            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 text-left acoes-loja">
                <div class="col-xs-6 col-xs-offset-6 text-center">
                    <a class="button produto-comparar-btn abrir-comparador">
                        <i class="fa fa-balance-scale" aria-hidden="true"></i> (<span id="contador_comparador"><?= $qtd_comparador ?></span>) <span class="txt-wrap">Comparador</span> 
                    </a>
                </div>
            </div>
        </div>
	</div>
</div>

<div class="total-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="breadcrumb flat text-center breadcrumb-content tablet-hidden">
                    <?php foreach($links_breadcrumb as $link_breadcrumb) { ?>
                        <a href="<?= WEBROOT_URL.$SSlug ?><?= $link_breadcrumb['url'] ?>"><?= $link_breadcrumb['name'] ?></a>
                    <?php } ?>
                </div>

                <div class="btn-voltar-div">
                    <a class="btn-voltar"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="menu-completo">
	<div class="menu-barra">
		<div class="itens-menu">
			<div class="fechar-menu">
				<span id="fechar-menu"><i class="fa fa-close"></i> Fechar</span>
			</div>
            <div class="content-lista">
                <ul id="lista-menu">
                    <li class="item-menu"><?= $this->Html->link('Inicio', $SSlug) ?></li>
                    <li class="item-menu"><?= $this->Html->link('Outras academias','/index') ?></li>
                    <li class="item-menu"><?= $this->Html->link('Login/Cadastrar','/login/'.$SSlug) ?></li>
                    <li class="item-menu"><?= $this->Html->link('Entenda a LOG','/entenda') ?></li>
                    <li class="item-menu"><a href="#" onclick="openIndique()">Indicar academia</a></li>
                    <li class="item-menu"><?= $this->Html->link('Franquia Logfitness','/franquia') ?></li>
                    <li class="item-menu"><?= $this->Html->link('Área academia','/academia/acesso') ?></li>
                    <li class="item-menu"><?= $this->Html->link('Espaço do time','professor/acesso') ?></li>
                </ul>
            </div>
			<div class="social">
				<hr>
				<?= $this->Html->image('logo-nova-loja.jpg',['alt' => 'Logfitness']); ?>
				<ul>
					<li><a href="https://goo.gl/uuNZKp" target="_blank"><i class="fa fa-rss"></i></a></li>
					<li><a href="https://goo.gl/5FMeVM" target="_blank"><i class="fa fa-spotify"></i></a></li>
					<li><a href="https://goo.gl/UMc7xb" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li><a href="https://goo.gl/kVZ7Ly" target="_blank"><i class="fa fa-instagram"></i></a></li>
					<li><a href="https://goo.gl/MbWiyx" target="_blank"><i class="fa fa-twitter"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div id="indique" class="overlay">
    <div class="overlay-content text-center">
        <a href="javascript:void(0)" class="closebtn" onclick="closeIndique()">&times;</a>
        <h3>Não encontrou uma academia que você conhece?<br> Indique ela pra gente!</h3>
        <hr>
        <div class="col-xs-12 col-md-6" id="explicativo">
            <h4>Meu primeiro LOG</h4>
            <p>Indique a sua academia e quando ela se cadastrar você ganha um suplemento. <?= $this->Html->image('mini_medal.png',['title' => 'logo-academia', 'alt' => 'Logo']) ?></p>
            <p>Enviamos direto para você retirar na sua academia! <?= $this->Html->image('mini_box.png',['title' => 'logo-academia', 'alt' => 'Logo']) ?></p>
            <p>Só ganha o primeiro a indicar a academia... <?= $this->Html->image('mini_run.png',['title' => 'logo-academia', 'alt' => 'Logo']) ?> CORRA!!</p>
        </div>
        <div class="col-xs-12 col-md-6">
            <?= $this->Form->create(null, [
                'id'        => 'form-indicar-academia',
                'role'      => 'form',
                'default'   => false
            ]) ?>
            <h4>Formulário de indicaçãos</h4>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('responsavel', [
                    'div'           => false,
                    'label'         => false,
                    'class'         => 'form-control',
                    'placeholder'   => 'Academia ou Responsável*',
                    'required'      => true
                ])?>
            </div>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('telephone', [
                    'id'            => 'telephone',
                    'div'           => false,
                    'label'         => false,
                    'class'         => 'form-control',
                    'placeholder'   => 'Telefone do responsável*',
                    'type'          => 'tel',
                    'required'      => true
                ])?>
            </div>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('uf_id', [
                    'label'         => false,
                    'id'            => 'states-indica',
                    'div'           => false,
                    'options'       => $states,
                    'empty'         => 'Selecione um estado',
                    'placeholder'   => 'Estado',
                    'class'         => 'form-control',
                    'required'      => true
                ]) ?>
            </div>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('city_id', [
                    'label'         => false,
                    'id'            => 'city-indica',
                    'div'           => false,
                    'empty'         => 'Selecione uma cidade',
                    'placeholder'   => 'Cidade',
                    'class'         => 'form-control',
                    'required'      =>  true
                ]) ?>
            </div>
            <div class="col-xs-12 formulario-indicacao">
                <?= $this->Form->input('email_indicador', [
                    'div'           => false,
                    'label'         => false,
                    'class'         => 'form-control',
                    'placeholder'   => 'Seu e-mail*',
                    'required'      => true,
                    'type'          => 'e-mail'
                ])?>
            </div>
        </div>
        <div class="col-xs-12">
            <?= $this->Form->button('Enviar', [
                'id'            => 'submit-indicar-academia',
                'type'          => 'button',
                'class'         => 'btn btn-success',
                'escape'        => false,
                // 'onclick'    => 'enviar()'
            ]) ?>
        <?= $this->Form->end()?>
        
        <p id="message-academia-indica"></p>
        </div>
    </div>
</div>
<div class="overlay-comprar">
    <div class="overlay-content text-center">
        <h2>Como comprar?</h2>
        <p>Compre pelo site e retire na sua academia com frete grátis!</p>
        <p>Caso sua academia não esteja na lista você pode indicá-la ou selecionar uma academia próxima.</p>
        <br>
        <?= $this->Html->link('<button class="btn btn-success">Procurar academia</button>', '/index',['escape' => false]) ?>
    </div>
</div>
<?php if(isset($CLIENTE)){ ?>
    <div class="nav-inferior">
        <div class="col-xs-12 text-center">
            <div class="row">
                <div class="item-nav-inferior">
                    <?= $this->Html->link('
                        <i class="fa fa-id-card-o fa-2x"></i>
                        <p>Dados</p>'
                    ,'/minha-conta/dados-faturamento', ['escape' => false]) ?>
                </div>
                <div class="item-nav-inferior">
                    <?= $this->Html->link('
                        <i class="fa fa-2x fa-list"></i>
                        <p>Pedidos</p>'
                    ,'/minha-conta/meus_pedidos', ['escape' => false]) ?>
                </div>
                <div class="item-nav-inferior">
                    <span id="abrir-submenu">
                        <i class="fa fa-2x fa-user"></i>
                        <p>Eu</p>
                    </span>
                </div>
                <div class="item-nav-inferior">
                    <?= $this->Html->link('
                        <i class="fa fa-calendar fa-2x"></i>
                        <p>Mensalidades</p>'
                    ,'/minha-conta/mensalidades',['escape' => false]) ?>
                </div>
                 <div class="item-nav-inferior">
                    <?= $this->Html->link('
                        <i class="fa fa-building-o fa-2x"></i>
                        <p>Academia</p>'
                    ,'minha-conta/minha-academia',['escape' => false]) ?>
                </div>
            </div>
        </div>
    </div>
    <div id="nav-inferior-submenu">
        <div class="conteudo-submenu">
            <?php $client_name = explode(' ', $CLIENTE->name);?>
            <div class="row text-center usuario-submenu">
                <span id="fechar-submenu"><i class="fa fa-close"></i></span>
                <p>Olá <?= array_shift($client_name) ?></p>
            </div>
            <ul>
                <li>
                    <?= $this->Html->link('<p><i class="fa fa-home"></i>&nbsp;&nbsp;Início</p>',$SSlug.'/home', ['escape' => false]);  ?>    
                </li>
                <li>
                    <?= $this->Html->link('<p><i class="fa fa-comment-o"></i>&nbsp;&nbsp;Indicações</p>','/minha-conta/indicacao-de-produtos', ['escape' => false]); ?>
                </li>
                <li>
                    <?= $this->Html->link('<p><i class="fa fa-user"></i>&nbsp;&nbsp;Meus Dados</p>','/minha-conta/dados-faturamento', ['escape' => false]);  ?>
                </li>
                <li>
                    <?= $this->Html->link('<p><i class="fa fa-line-chart"></i>&nbsp;&nbsp;Meus Objetivos</p>','/minha-conta/meus-dados', ['escape' => false]);  ?>
                </li>
                <li>
                    <?= $this->Html->link('<p>'.$this->Html->image('mochila_log.png',['style' => 'margin-top: -5px;', 'alt' => 'Mochila LOG']
                        ).'&nbsp;&nbsp;Meus Pedidos</p>','/minha-conta/meus_pedidos', ['escape' => false]);  ?>
                </li>
                <li>
                    <?= $this->Html->link('<p><i class="fa fa-calendar"></i>&nbsp;&nbsp;Mensalidades</p>','/minha-conta/mensalidades', ['escape' => false]); ?>
                </li>
                <li>
                    <?= $this->Html->link('<p><i class="fa fa-building-o"></i>&nbsp;&nbsp;Minha Academia</p>','/minha-academia', ['escape' => false]);  ?>
                </li>
                <li>
                    <?= $this->Html->link('<p><i class="fa fa-bars"></i>&nbsp;&nbsp;Minha Conta</p>','/minha-conta', ['escape' => false]);  ?>
                </li>
                <li>
                    <?= $this->Html->link('<p><i class="fa fa-sign-out"></i>&nbsp;&nbsp;Deslogar</p>','/logout', ['escape' => false]);  ?>
                </li>
            </ul>
        </div>
    </div>
<?php } ?>
    

<script>
    $(document).ready(function() {
    	//abrir o menu
    	$('#abrir-menu').click(function(){
        	$('.menu-completo').css('display', 'block');
    		setTimeout(function(){
    			$('.menu-barra').animate({'width' : '300px'}, 500);
    			$('.menu-completo').animate({'opacity' : '1'}, 500);
    	        $("body,html").addClass('total-block');
    	        setTimeout(function() {
    	        	$('.itens-menu').animate({'opacity' : '1'}, 200);
    	        	$('.menu-barra').animate({'opacity' : '1'}, 200);
    	        }, 500);
    		}, 300);
        });

        //fechar o menu
        $('#fechar-menu, .menu-completo').click(function() {
        	$('.itens-menu').animate({'opacity' : '0'}, 200);
    		setTimeout(function() {
    			$('.menu-completo').animate({'opacity' : '0'}, 300);
    			$('.menu-barra').animate({'width' : '0'}, 500);
    			setTimeout(function(){
    				$('.menu-completo').css('display', 'none');
        			$("body,html").removeClass('total-block'); 
    			}, 500);
    		}, 300);
        });

        //ao clicar no menu, não fechar overlay
        $('.menu-barra').click(function(e){
        	e.stopPropagation();
        });

        //indicar academia
        $('#states-indica').on('change', function(){
            var state_id = $(this).val();
            $.get(WEBROOT_URL + '/cidades/' + state_id,
                function(data){
                    $('#city-indica').html(data);
                });
        });
        $('#submit-indicar-academia').click(function(){
            var form = $('#form-indicar-academia');
            var form_data = form.serialize();
            $.ajax({
                type: "POST",
                url: WEBROOT_URL + 'indicar-academia',
                data: form_data
            })
            .done(function (data) {
                $('#submit-indicar-academia').fadeOut();
                $('#message-academia-indica').html('Indicação enviada com sucesso!');
            });
        });

        //abrir submenu
        $('#abrir-submenu').click(function(){
            $('#filtro-mobile-btn').css('display', 'none');
            $('#nav-inferior-submenu').css('display', 'block');
            setTimeout(function(){
                $('#nav-inferior-submenu').animate({'top' : '0px'}, 1000);
                $("body,html").addClass('total-block');
            }, 300);
        });

        //fechar o submenu
        $('#fechar-submenu, #nav-inferior-submenu').click(function() {
            $('#nav-inferior-submenu').animate({'top' : '2000px'}, 500);
            setTimeout(function() {
                $("body,html").removeClass('total-block');
                $('#filtro-mobile-btn').css('display', 'block'); 
            }, 300);
        });

        //ao clicar no submenu, não fechar overlay
        $('.conteudo-submenu').click(function(e){
            e.stopPropagation();
        });

        // abrir overlay-comprar
        $('.btn-comprar-overlay').click(function() {
            $('.overlay-comprar').animate({'top' : '0'}, 500);
        });
        
        //ao clicar no conteudo do overlay, não fechar overlay
        $('.overlay-content').click(function(e){
            e.stopPropagation();
        });

        // fechar overlay-comprar
        $('.overlay-comprar').click(function() {
            $('.overlay-comprar').animate({'top' : '2000px'}, 500);
        });
    });
</script>

<?php if(isset($_GET['indicar'])) { ?>
    <script type="text/javascript">
        openIndique();
    </script>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('.btn-voltar').click(function() {
            loading.show();
            console.log(history.go(-1));
            window.location.reload();

        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var breadcrumb = $(".total-breadcrumb");
        var cur = breadcrumb.scrollLeft();
        var max = breadcrumb[0].scrollWidth - breadcrumb.parent().width();

        breadcrumb.scrollLeft(max);
    });
</script>