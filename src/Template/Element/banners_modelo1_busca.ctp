<?php 
    if($query != null || $marca != null || $tipo_filtro != null || $filtro != null) {
        $filtragem = 1;
    } else {
        echo '<script>setCookie("image_bg", WEBROOT_URL+"/img/banners-loja/padrao.jpg", 365);</script>';
        $filtragem = 0;
    }
?>

<?php if($marca != null && $filtro == null) { ?>
    <script type="text/javascript">
        $(document).ready(function() {
            setCookie("image_bg", marca_url_banner, 365);
            // console.log(getCookie("image_bg"));
        });
    </script>
<?php } ?>

<?php if($marca == null && $filtro != null) { ?>
    <script type="text/javascript">
        $(document).ready(function() {
            setCookie("image_bg", filtro_url_banner, 365);
            // console.log('Não tem marca busca');
        });
    </script>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function() {
        var image_bg = getCookie("image_bg");

        if(image_bg == '') {
            image_bg = WEBROOT_URL+'/img/banners-loja/padrao.jpg';
        } 

        $('.banner-bg').css('background-image', 'url('+image_bg+')');
    });
</script>

<div class="total-banner">	
	<div class="owl-carousel owl-carousel-banner">
		<?php foreach($banners_busca as $banner_busca) { ?>	  
			<?php if ($banner_busca->target == 1) { ?>
				<div class="item">
	            	<?= $this->Html->link('<div class="bannerModelo1" style="background-image: url('.WEBROOT_URL.'/img/banners-loja/'.$banner_busca->image.')"></div>', $SSlug.$banner_busca->link,['escape' => false, 'target' => '_blank']) ?>
	        	</div>
			<?php } else { ?>
				<div class="item">
					<div class="bannerModelo1" style="background-image: url('<?= WEBROOT_URL ?>/img/banners-loja/<?= $banner_busca->image  ?>')"></div>
				</div>
			<?php } ?> 	        
		<?php } ?>
		<div class="item">
			<div class="banner-bg"></div>
		</div>
	</div>
</div>

<style>
	.total-banner{
		position: absolute;
		left: 0;
		right: 0;
		top: 90px;
	}
	.owl-carousel-banner .owl-wrapper-outer{
		padding: 0!important;
	}
	.bannerModelo1{
		width: 100%;
		height: 700px;
		background-position: center top;
		margin-bottom: 15px;
	}
</style>