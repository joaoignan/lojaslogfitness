<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="pt_BR"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="pt_BR"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="pt_BR"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt_BR"> <!--<![endif]-->

<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>LOGFITNESS <?php /*= $this->fetch('title')*/ ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0;">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= WEBROOT_URL?>img/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= WEBROOT_URL?>img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <style>
        <?= file_get_contents(CSS_URL.'academia_new_animate.min.css') ?>
        <?= file_get_contents(CSS_URL.'academia_new_bootstrap.min.css') ?>
        <?= file_get_contents(CSS_URL.'font-awesome.min.css') ?>
        <?= file_get_contents(CSS_URL.'ninja-slider.css') ?>
        <?= file_get_contents(CSS_URL.'bootstrap.min.css') ?>
        <?= file_get_contents(CSS_URL.'bootstrap-theme.min.css') ?>
        <?= file_get_contents(CSS_URL.'ion.rangeSlider.css') ?>
        <?= file_get_contents(CSS_URL.'ion.rangeSlider.skinHTML5.css') ?>
    </style>
    <?php /*= $this->Html->css('bootstrap.min.css')*/ ?>
    <?php /*= $this->Html->css('bootstrap-theme.min.css')*/ ?>
    <?= $this->Html->css('main.css') ?>
    <?= $this->Html->css('media_queries.min.css') ?>

    <script>
        <?= file_get_contents(JS_URL.'ninja-slider.js') ?>
        <?= file_get_contents(JS_URL.'vendor/jquery-1.8.2.min.js') ?>
        <?= file_get_contents(JS_URL.'vendor/jquery-1.11.2.min.js') ?>
        <?= file_get_contents(JS_URL.'vendor/modernizr-2.8.3-respond-1.4.2.min.js') ?>   
        <?= file_get_contents(JS_URL.'ion.rangeSlider.min.js') ?>
    </script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <?php /*= $this->Html->script('vendor/modernizr-2.8.3-respond-1.4.2.min.js')*/ ?>

    <?php /*= $this->Html->meta('favicon')*/ ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script>
        // var HTTP_REFERER = "<?php //= $_SERVER['HTTP_REFERER'] ?>";
        var WEBROOT_URL  = "<?= WEBROOT_URL?>";
    </script>

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">
    Você está utilizando um navegador obsoleto. Vitiste <a href="http://browsehappy.com/"> e atualize seu browser
    para uma melhor experiência web.
</p>
<![endif]-->

<?php header('Access-Control-Allow-Origin: *'); ?>


<div id="fb-root"></div>


<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1321574044525013',
            xfbml      : true,
            version    : 'v2.7'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<style>
    #instagram-rodape {
        box-shadow: none;
    }
    .laterais {
        border: 1px solid; 
        width: 230px; 
        height: 100%; 
        position: fixed;
        z-index: 100;
        border-color: #5089cf;
        background-color: white;
    }
    .fundo-titulo{
        background-color: #5089cf;
        height: 42px;
    }
    .laterais .titulos{
        font-size: 2em;
        text-align: left;
        margin: 0 0 10px;
        color: white;
        font-family: nexa_boldregular;
        display: flex;
        align-items: center;
        justify-content: center;
        height: 45px;
    }
    .mochila {
        right: 0;
        box-shadow: -1px 1px 5px 0px rgba(0,0,0,0.75);
        opacity: 0;
        width: 0;
    }
    .filtro {
        left: 0;
        box-shadow: 1px 1px 5px 0px rgba(0,0,0,0.75);
        opacity: 0;
        width: 0;
    }
    .btn-fecha-mochila{
        width: 230px;
        position: fixed;
        bottom: 0;
        height: 50px;
        right: 0;
        border: none;
        background-color: #8DC73F;
        font-size: 1.4em;
        color: #FFF;
    }
    .btn-limpar-mochila{
       width: 100%;
        height: 40px;
        border: none;
        background-color: #5087C7;
        font-size: 1.4em;
        color: #FFF;
        border-radius: 10px;
    }
    .btn-fecha-mochila a{
        padding: 0;
        font-size: 1.2em;
    }
    .total-txt{
        font-size: 1.4em;
        color: #5089cf;
        text-align: center;
    }
    .frete-txt{
        font-size: 1.1em;
        color: #5089cf;
        text-align: center;
        margin: 0;
    }
    .total-compra{
        position: fixed;
        bottom: 50px;
        right: 0;
        z-index: 20;
        width: 230px;
    }
    .produtos-mochila{
        width: 0;
        opacity: 0;
        position: fixed;
        top: 153px;
        bottom: 160px;
        right: 1px;
        overflow-y: auto!important;
        overflow-x: hidden;
    }
    .cupom-desconto p{
        text-align: center;
    }
    .limpar-mochila{
        padding-bottom: 10px;
        display: none;
    }
    .cupom-desconto{
        padding: 10px 0;
        position: fixed;
        bottom: 120px;
        right: 0;
        z-index: 20;
        width: 230px;
    }
    .btn-cupom{
        border: 2px solid;
        padding-top: 2px;
        border-color: #5087c7;
        background-color: white;
        font-family: nexa_boldregular;
        color: #5087c7;
        height: 26px;
    }
    .btn-cupon: hover{
        color: #5087c7!important;
    }
    .seta-mochila{
        margin: 5px 0 10px;
    }
    .fundo-carrinho {
        background: #ffffff;
        padding: 0 5px;
        height: 70px;
        margin-left: 250px;
    }
    .fundo-filtro {
        padding: 1px 15px;
    }
    .box-filtro {
        padding: 15px 15px;
        overflow: auto;
        overflow-x: hidden;
        position: fixed;
        left: -1px;
        top: 153px;
        width: 230px;
        bottom: 0;
    }
    .subtitle {
        color: #5089cf;
        font-weight: 700;
        font-size: 20px;
        padding-top: 15px;
        text-transform: uppercase;
        text-align: center;
    }
    li {
        list-style: none;
        padding-left: 10px;
    }
    #nav-marcas {
        color: #337ab7!important;
        background-color: transparent!important;
    }
    #nav-marcas:hover,
    #nav-marcas:focus,
    #nav-marcas:active {
        color: #23527c!important;
        text-decoration: none!important;
    }
    .imagem-produto-carrinho {
        float: left;
        padding: 0px 0 0 0;
        margin-right: 4px;
        text-align: center;
        width: 60px;
    }
    .dimensao-produto-carrinho {
        max-width: 60px;
        max-height: 60px;
    }
    .qtd-produtos{
        float: left;
        margin-top: 6px;
        padding-left: 7px;
    }
    .valor-produto{
        float: left;
        display: none;
        margin-top: 10px;
        padding-left: 50px;
    }
    .total-produto{
        float: right;
        margin-top: 8px;
        font-size: 15px;
    }
    .detalhes{
        font-size: 13px;
        float: left;
        margin-right: 15px;
        margin-top: 5px;
    }
    .lixo{
        float: right;
        margin-top: 2px;
        margin-right: 8px;
    }
    .lixo-div{
        float: right;
        margin-top: 2px;
        margin-right: 8px;
    }
    input[type^='button'] {
        background: white;
        border: none;
        font-size: 16px;
        color: #5089cf;
        font-weight: 700;
        padding: 0;
    }
    .sub-menu-lateral {
        margin-left: 40px;
        width: 150px;
    }
    .div-objetivos {
        display: none;
    }

    #filtro-ordenar {
        border: 2px solid;
        border-color: #5089cf;
    }
    .valor-box {
        text-align: center;
        padding-left: 15px;
        padding-right: 15px;
    }
    .irs-max, .irs-min {
        display: none;
    }
    input[type="button"]:focus {
        box-shadow: 0 0 0 0;
        border: 0 none;
        outline: 0;
    } 
    .ui-draggable-dragging {
        z-index: 300;
    }
    body {
        overflow-x: hidden;
    }
    .nav-objetivos svg {
        width: 20px;
        height: 20px;
    }
    #aplicar-valor {
        display: none;
    }
    .mobile-not {
        display: block;
    }
    @media all and (max-width: 768px) {
        .mobile-not {
            display: none;
        }
    }
</style>

<script>
    $(document).ready(function() {
        var valor_minimo = 0;
        var valor_maximo = 0;

        var valor_minimo =<?php foreach($produtos_min as $p): echo $p->preco; endforeach; ?>;
        var valor_maximo =<?php foreach($produtos_max as $p): echo $p->preco; endforeach; ?>;

        $('#value-slider').ionRangeSlider({
            type: "double",
            min: valor_minimo,
            max: valor_maximo,
            from: valor_minimo,
            to: valor_maximo,
            prefix: "R$"
        });
    });
</script>

<div class="box-close-filter">
    <span class="fa fa-sliders fa-2x close-filter close-filter-icon mobile-not"></span>
</div>

<!-- Chamando o Navbar -->
<?= $this->Element('navbar-final'); ?>
<!-- <?= $this->request->is('mobile') ? $this->Element('navbar_mobile') : $this->Element('navbar', ['SSlug' => $SSlug]); ?> -->
<!-- Fim navbar -->
<div class="laterais filtro">
    <i class="fa fa-close fechar-filtro"></i>
    <div class="fundo-titulo fundo-filtro">
        <p class="titulos">filtro</p>
    </div>
    <div class="box-filtro">

        <?php if($produtos_min > 0) { ?>
            <p id="ordenar_subtitle" class="subtitle" style="padding-top: 0">Ordenar por:</p>

            <div id="ordenar-box" style="text-align: center">
                <select id="filtro-ordenar">
                    <option value="0">Estou com sorte</option>
                    <option value="1">Maior Valor</option>
                    <option value="2">Menor Valor</option>
                </select>
            </div>
            
            <p id="valor_subtitle" class="subtitle">Valor</p>
            <div class="valor-box">
                <input type="text" id="value-slider" name="value_slider" value="" />
            
                <button id="aplicar-valor" class="btn btn-cupom" style="margin: auto">Aplicar</button>
            </div>
        <?php } ?>

        <p class="subtitle">Categorias</p>
 
        <li><a id="nav-marcas" class="text-uppercase font-bold">MARCAS</a></li>

        <!--MENU MARCAS-->
        <div id="div-marcas" class="container">
            <div class="row">
                <?php foreach($marcas as $marca): ?>
                    <li class="sub-menu-lateral">
                        <a data-id="<?= $marca->id; ?>" class="link-marca"><?= $marca->name; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </div>
        </div>

        <?php foreach($objetivos as $objetivo): ?>
            <li>
                <a class="nav-objetivos text-uppercase inline-block font-bold"
                   data-objetivo="<?= $objetivo->slug ?>"
                   data-oid="<?= $objetivo->id ?>">
                   <?= $objetivo->image ?> <?= strtoupper($objetivo->name) ?>
                </a>
            </li>

            <div id="div-objetivo-<?= $objetivo->slug; ?>" class="div-objetivos container">
                <div class="row">
                    <?php
                    foreach($categorias as $produto_categoria):
                        if(in_array($produto_categoria->id, $produtos_categorias[$objetivo->id])):
                            ?>
                            <li class="sub-menu-lateral">
                                <a data-cid="<?= $produto_categoria->id; ?>"
                                   data-oid="<?= $objetivo->id; ?>"
                                   class="link-produto-categoria">
                                    <span><?= $produto_categoria->name; ?></span>
                                </a>
                            </li>
                            <?php
                        endif;
                    endforeach;
                    ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<style>
    @keyframes seta-mochila {
        from {color: white;transform: rotate(0deg)}
        to   {color: #f3d012;transform: rotate(540deg)}
    }

    @keyframes seta-mochila-fechar {
        from {color: #f3d012;transform: rotate(540deg)}
        to   {color: white;transform: rotate(0deg)}
    }

    .seta-animate-abrir {
        -webkit-animation: seta-mochila 2s linear;
        -moz-animation: seta-mochila 2s linear;
        -o-animation: seta-mochila 2s linear;
        animation: seta-mochila 2s linear;
        transform: rotate(180deg)!important;
        color: #f3d012!important;
    }

    .seta-animate-fechar {
        -webkit-animation: seta-mochila-fechar 2s linear;
        -moz-animation: seta-mochila-fechar 2s linear;
        -o-animation: seta-mochila-fechar 2s linear;
        animation: seta-mochila-fechar 2s linear;
        transform: rotate(0deg)!important;
        color: white!important;
    }

    .info-prod {
        display: none;
        margin-left: 85px;
    }
    .info-direita-mochila {
        float: right; 
        clear: right;
        width: 130px;
    }
    #expandir-mochila {
        cursor: pointer;
        float: left;
        margin-top: 7px;
        transform: rotate(0deg);
    }
    #expandir-filter {
        cursor: pointer;
        margin-top: 7px;
        transform: rotate(0deg);
    }
    .produto-qtd {
        text-align: center;
        font-family: nexa_boldregular;
        border: 1px solid #aaa;
        height: 20px;
        width: 18px;
        border-radius: 0;
    }
    .academia-logo {
        margin-top: 0;
    }
    #logo-esquerdo {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 110px;
    }
    #menu-principal {
        display: block;
    }
    .logo-margin {
        margin-left: 0!important;
    }
    .logo-topo {
        margin-top: 0;
    }
    .mochila-titulo {
        float: left;
        padding-left: 30px;
    }
    .close-mochila-icon {
        cursor: pointer;
        position: fixed;
        right: 10px;
        z-index: 200;
        top: 111px;
        fill: white;
        width: 40px;
    }
    .close-filter-icon {
        cursor: pointer;
        position: fixed;
        left: 16px;
        z-index: 200;
        top: 111px;
        fill: white;
        width: 40px;
    }
    .box-close-mochila {
        width: 0;
        height: 42px;
        position: fixed;
        top: 111px;
        z-index: 150;
        right: 0px;
        background-color: #5089cf;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
    }
    .box-close-filter {
        width: 0;
        height: 42px;
        position: fixed;
        top: 111px;
        z-index: 150;
        left: 0px;
        background-color: #5089cf;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
    }
    .box-close-mochila:hover {
        background-color: #457abb;
    }
    .box-close-filter:hover {
        background-color: #457abb;
    }
    .close-mochila {
        right: 10px;
    }
    .cupom-desconto, 
    .total-compra, 
    .btn-fecha-mochila {
        right: -250px;
    }
    .box-close-mochila {
        width: 60px;
    }
    .close-filter {
        left: 16px;
        font-size: 38px;
    }
    .close-filter-mobile {
        left: 16px;
        font-size: 38px;
    }
    .box-close-filter {
        width: 60px;
    }
    .box-filtro {
        left: -250px;
    }
    .info-direita-mochila {
        display: none;
    }

    .close-filter,
    .close-mochila {
        color: white;
        fill: white;
    }

    .close-filter-mobile,
    .close-mochila-mobile {
        color: white;
        fill: white;
    }

    .fundo-branco {
        display: none;
    }

    .fechar-filtro,
    .fechar-mochila-x {
        display: none;
    }

    @media all and (max-width: 768px) {
        .menu-principal, .reparo-nav {
            height: 50px;
        }
        .box-close-filter,
        .box-close-mochila {
            background-color: transparent;
            top: inherit;
            box-shadow: none;
        }
        .close-filter,
        .close-mochila {
            top: inherit;
            bottom: 4px;
        }
        .close-filter-mobile,
        .close-mochila-mobile {
            top: inherit;
            bottom: 4px;
        }
        .produtos-mochila,
        .box-filtro {
            top: 90px;
        }
        #expandir-mochila {
            display: none;
        }
        .laterais {
            top: 50px;
        }
        .cupom-desconto input {
            border: 1px solid #aaa;
            height: 26px;
            border-radius: 0;
        }
        #aplicar-valor {
            display: block;
        }
        .fundo-branco {
            display: block;
            width: 100%;
            height: 45px;
            position: fixed;
            bottom: 0;
            background-color: #5089cf;
            z-index: 40;
        }
        .mochila-titulo {
            float: none;
            padding-left: 0;
        }
        .fechar-filtro,
        .fechar-mochila-x {
            cursor:pointer;
            color: white;
            position:absolute;
            display: block;      
            top: 9px;
            z-index: 40;   
            font-size: 23px;   
        }
        .fechar-mochila-x {
            left: 10px;
        }
        .fechar-filtro {
            right: 10px;
        }
        .btn-fecha-mochila {
            font-size: 25px;
        }
        .info-direita-mochila {
            width: 140px;
        }
        .imagem-produto-carrinho {
            width: 50px;
        }
        .dimensao-produto-carrinho {
            max-width: 50px;
            max-height: 50px;
        }
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        $('.fechar-filtro').click(function() {
            $('.close-filter').click();
        });

        $('.fechar-mochila-x').click(function() {
            $('.close-mochila').click();
        });
        $('.btn-fecha-mochila').click(function() {
            location.href =  WEBROOT_URL + 'mochila/fechar-o-ziper';
        });
    });
</script>

<div class="box-close-mochila mobile-not">
<svg class="close-mochila close-mochila-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"><g><polygon points="8.669,48.706 5,62.65 6.565,64.773 10.008,54.727  "/><polygon points="93.422,64.79 95,62.65 91.331,48.706 89.991,54.727  "/><polygon points="41.311,14.947 39.661,15.937 37.462,30.863 37.071,40.502 39.622,40.182 41.47,14.947  "/><polygon points="62.928,40.51 62.538,30.863 60.338,15.937 58.689,14.947 58.529,14.947 60.378,40.188  "/><polygon points="29.01,42.523 21.586,43.458 9.53,47.966 11.045,54.785 5.5,70.967 5.5,88.654 27.14,88.654  "/><polygon points="36.48,55.079 35.119,88.654 64.879,88.654 63.519,55.079  "/><polygon points="52.475,40.195 47.463,40.199 37.03,41.513 36.716,49.244 63.282,49.244 62.969,41.516  "/><polygon points="88.954,54.785 90.47,47.966 78.413,43.458 70.989,42.524 72.859,88.654 94.5,88.654 94.5,70.967  "/><path d="M28.12,89.154h5.978l1.422-35.075h28.96L65.9,89.154h5.979l-2.368-58.421L65.62,15.752l-7.219-4.906h-7.104h-2.595h-7.105   l-7.218,4.906l-3.891,14.981L28.12,89.154z M51.578,13.947h7.388l2.256,1.354l2.312,15.433l0.79,19.511H35.675l0.792-19.511   l2.311-15.433l2.257-1.354h7.386H51.578z"/></g></svg>
</div>

<div class="laterais mochila">
    <i class="fa fa-close fechar-mochila-x"></i>
    <div class="fundo-titulo">
        <div class="col-sm-12">
            <span id="expandir-mochila" class="fa fa-arrow-left fa-2x expandir" style="color: white;"></span>
            <div class="titulos mochila-titulo"> mochila</div>
        </div>
    </div>
    <div class="produtos-mochila">
        <?php
            foreach ($s_produtos as $s_produto):
                $fotos = unserialize($s_produto->fotos);
                ?>

                <div id="fundo-<?= $s_produto->id ?>" class="col-md-12 fundo-carrinho">
                    <div class="imagem-produto-carrinho">
                        <?= $this->Html->link(
                            $this->Html->image('produtos/' . $fotos[0],
                                ['class' => 'dimensao-produto-carrinho', 'alt' => $s_produto->produto_base->name . ' ' . $s_produto->propriedade]
                            ),
                            '/produto/' . $s_produto->slug,
                            ['escape' => false]
                        ) ?>
                    </div>
                    <div class="col-md-4 text-center info-prod">
                        <?= $this->Html->link(
                            '<h1 class="font-14 h1-carrinho"><span>'
                            . $s_produto->produto_base->name . ' ' . $s_produto->produto_base->embalagem_conteudo . ' ' . $s_produto->propriedade .
                            '</span></h1>',
                            '/produto/' . $s_produto->slug,
                            ['escape' => false]
                        ) ?>
                        <p class="font-10"><span><?= strtoupper($s_produto->cod); ?></span></p>
                    </div>
                    <div class="text-right valor-produto">
                        <?= $s_produto->preco_promo ?
                            '<p><span><s>R$ ' . number_format($s_produto->preco, 2, ',', '.') . '</s></span></p>' :
                            ''; ?>
                        <p>
                        <span><b>R$ <?= $s_produto->preco_promo ?
                                    number_format($preco = $s_produto->preco_promo, 2, ',', '.') :
                                    number_format($preco = $s_produto->preco, 2, ',', '.'); ?></b>
                        </span>
                        </p>
                    </div>
                    <div class="info-direita-mochila">
                        <div class="text-right">
                            <b id="total-produto-<?= $s_produto->id ?>"
                               class="total-produto"
                               data-total="<?= number_format(($preco * $session_produto[$s_produto->id]), 2, '.', '') ?>">
                                R$ <?= number_format($total_produto = ($preco * $session_produto[$s_produto->id]), 2, ',', '.') ?>
                                <?php $subtotal += $total_produto; ?>
                            </b>
                        </div>
                        <div class="text-center qtd-produtos">
                            <input type="button" value='-' class="produto-menos" data-id="<?= $s_produto->id ?>"/>
                            <input class="text produto-qtd"
                                   id="produto-<?= $s_produto->id ?>"
                                   size="1"
                                   type="text"
                                   value="<?= $session_produto[$s_produto->id]; ?>"
                                   data-preco="<?= $s_produto->preco_promo ?
                                       number_format($s_produto->preco_promo, 2, '.', '') :
                                       number_format($s_produto->preco, 2, '.', ''); ?>"
                                   maxlength="2"/>
                            <input type="button" value='+' class="produto-mais" data-id="<?= $s_produto->id ?>"/>
                        </div>
                    </div>
                    <div class="info-direita-mochila">
                        <div class="lixo-div">
                            <a class="lixo" href="#" data-id="<?= $s_produto->id ?>"><span class="fa fa-trash" style="color: darkgray"></span></a>
                        </div>

                        <div class="detalhes">
                            <a href="/produto/<?= $s_produto->slug ?>" class="detalhes-btn"><span style="font-size: 14px; font-weight: 700">+</span> detalhes</a>
                        </div>
                    </div>
                </div>
                <?php
            endforeach;
        ?>
    </div>

    <div class="limpar-mochila col-sm-12">
        <button class="btn-limpar-mochila"><span class="fa fa-trash"></span> limpar mochila</button>    </div>
    <div class="cupom-desconto col-sm-12">
        <div class="col-xs-7">
            <input type="text" name="cupon-desconto" placeholder=" cupom desconto">
        </div>
        <div class="col-xs-4">
            <button class="btn-cupom" type="">aplicar</button>
        </div>
    </div>
    <div class="total-compra col-sm-12">
        <p class="frete-txt"><b>*FRETE GRÁTIS</b></p>
        <p class="total-txt"><b id="carrinho-total" data-total="<?= number_format($subtotal, 2, '.', '')?>">TOTAL: R$ <?= number_format($subtotal, 2, ',', '.')?></b></p>
    </div>
    <div class="fechar-mochila">
    <?= $subtotal > 0 ? 
        $this->Html->link(
        '<button class="btn-fecha-mochila">fechar mochila</button>',
        '/mochila/fechar-o-ziper',
        ['escape' => false, 'class' => 'btn-fecha-mochila']
    ) : $this->Html->link(
        '<button class="btn-fecha-mochila" disabled>fechar mochila</button>',
        '/mochila/fechar-o-ziper',
        ['escape' => false, 'class' => 'btn-fecha-mochila']
    ) ?>
    </div>
</div>

<div class="loading text-center">
    <span class="helper"></span>
    <i class="fa fa-refresh fa-spin fa-3x fa-fw margin-bottom"></i>
    <span class="">Atualizando conteúdo...</span>
</div>

<?= $this->fetch('content') ?>

<?= !$this->request->isMobile() ? $this->Element('instagram') : ''; ?>

<?= $this->Element('footer'); ?>

<?= $this->Form->create(null)?>
<?= isset($_GET['redir']) ?
    $this->Form->input('redirect', [
        'id'            => 'redirect',
        'type'          => 'hidden',
        'value'         => $_GET['redir'],
    ]):
    $this->Form->input('redirect', [
        'id'            => 'redirect',
        'type'          => 'hidden',
        'value'         => 'home',
    ]);
?>
<?= $this->Form->end()?>

<?= $this->Html->css('star-rating.min.css') ?>
<?= $this->Html->css('validationEngine.jquery.custom.min.css') ?>
<?= $this->Html->css('bootstrap-multiselect.css') ?>
<?= $this->Html->css('jquery.fancybox.min.css') ?>
<?= $this->Html->css('nanoscroller.css') ?>
<?= $this->Html->css(JS_URL.'owl-carousel/owl.carousel.css') ?>
<?= $this->Html->css(JS_URL.'owl-carousel/owl.theme.css') ?>
<?= $this->Html->css('m4w-instagram') ?>

<script>

    <?= file_get_contents(JS_URL.'vendor/bootstrap.min.js') ?>
    <?= file_get_contents(JS_URL.'plugins.js') ?>
    <?= file_get_contents(JS_URL.'star-rating.min.js') ?>
    <?= file_get_contents(JS_URL.'star-rating_locale_pt-br.min.js') ?>
    <?= file_get_contents(JS_URL.'jquery.validationEngine.min.js') ?>
    <?= file_get_contents(JS_URL.'jquery.validationEngine-br.min.js') ?>
    <?= file_get_contents(JS_URL.'jquery.mask.min.js') ?>
</script>

<?= $this->Html->script('jquery.form.min.js') ?>
<?= $this->Html->script('jquery.countdown.min.js') ?>
<?= $this->Html->script('jquery-ui-1.11.4.custom/jquery-ui.min.js') ?>
<?= $this->Html->script('jquery.fancybox.min.js') ?>
<?= $this->Html->script('bootstrap-multiselect.js') ?>
<?= $this->Html->script('chosen.jquery.js') ?>
<?= $this->Html->script('jquery.nanoscroller.min.js') ?>
<?= $this->Html->script('owl-carousel/owl.carousel.js') ?>
<?= $this->Html->script('m4w-instagram') ?>

<?= $this->Html->script('main.js') ?>

<?= $this->fetch('css') ?>
<?= $this->fetch('script') ?>


<?php if(isset($_GET['oid']) && $_GET['oid'] > 0 && !isset($_GET['cid'])){ ?>
    <script>
        var loading = $('.loading');
        var grade_produtos  = $('#grade-produtos');
        loading.show(1);
        function get_produtos_objetivo(objetivo_id){
            $.get(WEBROOT_URL + 'produtos/filtro-objetivo/' + objetivo_id,
                function (data) {
                    $('html, body').animate({
                        scrollTop: grade_produtos.offset().top - 125
                    }, 700);

                    grade_produtos.html(data);
                    loading.hide(1);
                });
        }
        get_produtos_objetivo(<?= $_GET['oid']; ?>);
    </script>
<?php }?>


<?php if(isset($_GET['cid']) && $_GET['cid'] > 0){ ?>
    <script>
        var loading = $('.loading');
        var grade_produtos  = $('#grade-produtos');
        loading.show(1);
        function get_produtos_objetivo(objetivo, categoria){
            $.get(WEBROOT_URL + 'produtos/filtro-objetivo-categoria/' + objetivo + '/' + categoria,
                function (data) {
                    $('html, body').animate({
                        scrollTop: grade_produtos.offset().top - 125
                    }, 700);

                    grade_produtos.html(data);
                    loading.hide(1);
                });
        }
        get_produtos_objetivo(<?= $_GET['oid']; ?>, <?= $_GET['cid']; ?>);
    </script>
<?php }?>

<?php if(isset($_GET['m']) && $_GET['m'] > 0){ ?>
    <script>
        var loading = $('.loading');
        var grade_produtos  = $('#grade-produtos');
        loading.show(1);

        function get_produtos_marca(marca_id){
            $.get(WEBROOT_URL + '/produtos/filtro-marca/' + marca_id,
                function (data) {
                    $('html, body').animate({
                        scrollTop: grade_produtos.offset().top - 125
                    }, 700);
                    grade_produtos.html(data);
                    loading.hide(1);
                });
        }
        get_produtos_marca(<?= $_GET['m']; ?>);
    </script>
<?php }?>

<?php if(isset($_GET['busca']) && $_GET['busca'] != ''){ ?>
    <script>
        var loading = $('.loading');
        var grade_produtos  = $('#grade-produtos');
        loading.show(1);

        function search() {
            loading.show(1);

            $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/busca/',
                data: {query: "<?= $_GET['busca']?>"}
            })
                .done(function (data) {
                    $('html, body').animate({
                        scrollTop: grade_produtos.offset().top - 125
                    }, 700);
                    grade_produtos.html(data);
                    loading.hide(1);
                });
        }
        search();
    </script>
<?php }?>

<!-- Google Analytics -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = '//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-64522723-3', 'auto');
    ga('send', 'pageview');
</script>

<?php if(isset($academias_map)): ?>
    <script>

        $(document).ready(function () {
            var map;
            var elevator;
            var myOptions = {
                zoomControl: true,
                scaleControl: false,
                scrollwheel: false,
                disableDoubleClickZoom: false,
                zoom: 4,
                center: new google.maps.LatLng(-15.7217175,-48.0783242),
                mapTypeId: 'terrain'
            };
            map = new google.maps.Map($('#map_canvas')[0], myOptions);

            <?php
            $academias_address  = '';
            $academias_info     = '';

            foreach($academias_map as $academia):
            $academias_address =
                "'"
                .$academia->address.','
                .$academia->number.','
                .$academia->area.','
                .str_replace("'", '´', $academia->city->name).','
                .$academia->city->uf
                ."'";

            $academias_info =

                str_replace("'", '´', $academia->shortname).' :: '.str_replace("'", '´', $academia->city->name).'-'.$academia->city->uf
            ;

            $pass = 0;

            if(!empty($academia->latitude) && !empty($academia->longitude)){
            ?>
            var latlng = new google.maps.LatLng(<?= $academia->latitude; ?>, <?= $academia->longitude; ?>);
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: "<?= $academias_info; ?>",
                icon:       WEBROOT_URL + 'img/map-marker.png'
                //shadow:     WEBROOT_URL + 'img/map-marker.png'
            });

            google.maps.event.addListener(marker, 'click', function () {
                //infowindow.open(map,marker);
                select_academia(<?= $academia->id; ?>);
            });

            <?php
            }else{
            //$pass = $pass + 100;
            /*if($pass >= 10) {
                //sleep(1);
                $pass = 0;
            }*/
            ?>
            //setTimeout(function () {
            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + <?= $academias_address; ?> +'&sensor=false',
                null,
                function (data) {
                    //console.log(data);
                    var p = [];
                    if(data.results[0].geometry.location){
                        p = data.results[0].geometry.location;
                    }else{
                        p = data.results[0]['geometry']['location'];
                    }

                    var latlng = new google.maps.LatLng(p.lat, p.lng);
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: "<?= $academias_info; ?>",
                        icon:       WEBROOT_URL + 'img/map-marker.png'
                        //shadow:     WEBROOT_URL + 'img/map-marker.png'
                    });

                    google.maps.event.addListener(marker, 'click', function () {
                        //infowindow.open(map,marker);
                        select_academia(<?= $academia->id; ?>);
                    });

                    $.ajax({
                        type: "POST",
                        url: WEBROOT_URL + "academia-cordinates-add/" + <?= $academia->id; ?> + '/' + p.lat + '/' + p.lng,
                        data: {}
                    }).done(function (data_academia) {
                        //console.log(data_academia);
                    });
                });
            //}, <?= $pass; ?>);
            <?php
            }
            endforeach;
            ?>
        });
    </script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyCba7qYdWhCMKwndRe8DC6TgHYq14jcRy4"></script>
<?php endif; ?>
</body>
</html>