<div>
    <div class="col-md-4 informacoes-blocos academias">
        <?= $this->Html->link(
            $this->Html->image('academias.png', ['alt' => 'Academias']),
            '/academias',
            ['escape' => false]
        )?>
        <p><span class="titulo">ACADEMIAS</span></p>
        <p class="info">
                <span>
                    Saiba como fazer parte da nossa rede
                </span>
        </p>
    </div>
</div>