<script>
    console.log('<?= $SSlug ?>');
</script>
<div class="cobertura-loja">
	<div class="mochila-nova">
		<div class="botoes-mochila continuar-comprando">
			<button id="continuar-comprando" class="button">
				<div class="botoes-mochila-titulo">continuar comprando</div>
				<div class="botoes-mochila-sub">colocar mais itens</div>
			</button>
		</div>
		<div class="lista-produtos-mochila">
			<?php foreach ($s_produtos as $s_produto) {
                $fotos = unserialize($s_produto->fotos);
            	$s_produto->preco_promo ? $preco = $s_produto->preco_promo * $desconto_geral : $preco = $s_produto->preco * $desconto_geral;
                ?>

                <div id="fundo-<?= $s_produto->id ?>" class="produto-mochila">
					<div class="produto-imagem">
						<?= $this->Html->link(
                            $this->Html->image($fotos[0],
                                ['alt' => $s_produto->produto_base->name.' '.$s_produto->propriedade]
                            ),
                            $SSlug.'/produto/'.$s_produto->slug,
                            ['escape' => false]
                        ) ?>
                        <?= $this->Html->link('<div class="tarja-detalhes">+ detalhes</div>',
                            $SSlug.'/produto/'.$s_produto->slug,
                            ['escape' => false]
                        ) ?>
					</div>
					<div class="produto-detalhes">
						<?= $this->Html->link('<p title="'.$s_produto->produto_base->name.'"><strong>'.$s_produto->produto_base->name.'</strong></p>',
                            $SSlug.'/produto/'.$s_produto->slug,
                            ['escape' => false]
                        ) ?>
						
						<p class="sabor-produto"><?= $s_produto->propriedade ?></p>
						<p class="tamanho-produto"><?= $s_produto->tamanho.' '.$s_produto->unidade_medida ?></p>
						<div class="conjunto-botoes">
							<input type="button" value='-' class="produto-menos btn-quantidade" data-id="<?= $s_produto->id ?>"/>
							<span class="text produto-qtd quantidade-produtos" id="produto-<?= $s_produto->id ?>" data-preco="<?= number_format($preco, 2, '.', ''); ?>"><?= $session_produto[$s_produto->id]; ?></span>
							<input type="button" value='+' class="produto-mais btn-quantidade" data-id="<?= $s_produto->id ?>" eid="<?= $s_produto->estoque ?>"/>
							<span id="total-produto-<?= $s_produto->id ?>" class="total-quantidade total-produto" data-total="<?= number_format(($preco * $session_produto[$s_produto->id]), 2, '.', '') ?>">R$ <?= number_format($total_produto = ($preco * $session_produto[$s_produto->id]), 2, ',', '.') ?></span>
							<?php $subtotal += $total_produto; ?>
						</div>
						<p><a class="lixo" href="#" data-id="<?= $s_produto->id ?>"><span class="btn-remover"><span class="fa fa-trash" style="color: darkgray"></span> remover</span></a></p>
					</div>
				</div>
            <?php } ?>
		</div>

		<div class="rodape-mochila">
			<div class="area-cupom">
				<?php if(count($cupom_desconto) > 0) { ?>
					<input type="text" class="cupom-value" data-id="<?= $cupom_desconto->valor ?>" tipo-id="<?= $cupom_desconto->tipo ?>" placeholder=" cupom válido!">
		        <?php } else { ?>
					<input type="text" class="cupom-value" data-id="<?= $cupom_desconto->valor ?>" tipo-id="<?= $cupom_desconto->tipo ?>" placeholder=" insira o cupom">
		        <?php } ?>
				<button class="aplicar-cupom">aplicar</button>
			</div>
			<div class="valor-total-compra text-left">
	        	<h4>frete: <span>R$0,00</span></h4>
			</div>
			<div class="valor-total-compra text-left">
				<?php if($cupom_desconto) { ?>
		            <?php 
		            	if($cupom_desconto->tipo == 1) {
			            	$desconto = 1.0 - ($cupom_desconto->valor * .01); $subtotal_desc = $subtotal * $desconto; 
		            	} else {
			            	$subtotal_desc = $subtotal - $cupom_desconto->valor; 
		            	}
		            ?>
		            <h4 id="carrinho-total" data-total="<?= number_format($subtotal_desc, 2, '.', '')?>">valor total: <span>R$<?= number_format($subtotal_desc, 2, ',', '.')?></span></h4>
		        <?php } else { ?>
		        	<h4 id="carrinho-total" data-total="<?= number_format($subtotal, 2, '.', '')?>">valor total: <span>R$<?= number_format($subtotal, 2, ',', '.')?></span></h4>
		        <?php } ?>
			</div>
			<?php if($subtotal >= 99.0) { ?>
				<div class="botoes-mochila proximo-passo">
					<?= $this->Html->link(
				        '<button class="button">
							<div class="botoes-mochila-titulo">próximo passo</div>
							<div class="botoes-mochila-sub">pagamento</div>
						</button>',
				        $SSlug.'/mochila/fechar-o-ziper',
				        ['escape' => false, 'class' => 'finalizar-mochila']
				    ) ?>
				</div>
			<?php } else { ?>
				<div class="botoes-mochila proximo-passo">
					<?= $this->Html->link(
				        '<button class="button inativo">
							<div class="botoes-mochila-titulo">próximo passo</div>
							<div class="botoes-mochila-sub">*valor mínimo: R$99</div>
						</button>',
				        $SSlug.'/mochila/fechar-o-ziper',
				        ['escape' => false, 'class' => 'finalizar-mochila']
				    ) ?>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		//abrir mochila
	    $('#abrir-mochila').click(function(){
			$('.cobertura-loja').css('display', 'block');

	    	setTimeout(function() {
				$('.cobertura-loja').animate({'opacity' : '1'}, 700);
				$('.lista-produtos-mochila').animate({'opacity' : '1'}, 500);
	        	$('.mochila-nova').animate({'width' : '300px'}, 500);
	        	$("body,html").addClass('total-block');
	    		setTimeout(function() {
	    			$('.lista-produtos-mochila').animate({'opacity' : '1'}, 200);
	    		}, 500);
			});
	    });

	    //continuar comprando(fechar mochila)
		$('#continuar-comprando, .cobertura-loja').click(function() {
	        $('.lista-produtos-mochila').animate({'opacity' : '0'}, 200);
	        setTimeout(function() {
		        $('.mochila-nova').animate({'width' : '0'}, 400);
		        setTimeout(function() {
		        	$('.cobertura-loja').animate({'opacity' : '0'}, 700);
		        	setTimeout(function() {
		        		$('.cobertura-loja').css('display', 'none');
			        	$("body,html").removeClass('total-block');
		        	}, 500);
		        }, 500);
	        }, 300);
	    });

		//contador de itens
		function atualizar_contador() {
		    var contador = 0;

		    $('.produto-qtd').each(function() {
		        contador = contador + parseInt($(this).html());
		    });

		    $('#contador_mochila').html(contador);
			console.log('contador atualizado');
			
		}
		
		atualizar_contador();

	    //ao clicar na mochila, não fechar overlay
	    $('.mochila-nova').click(function(e){
	    	e.stopPropagation();
		});
		
		//verificar se o valor total é maior que 0 antes de finalizar a mochila
		$('.finalizar-mochila').click(function() {
		var valor_total = $('#carrinho-total').attr('data-total');
		console.log(valor_total)
		if(valor_total < 99.0) {
			return false;
		}
		});
		
	});
</script>