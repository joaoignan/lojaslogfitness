<style>
	.alert-erro{
		color: #861d1d;
		background-color: #ffe4e4!important;
		background-image: none;
		border: 2px solid #c73f3f;
	}
	.alert-conteudo h4{
		text-transform: uppercase;
		font-weight: bolder;
	}
	.alert-conteudo p{
		font-size: 16px;
		font-weight: bold;
		margin-bottom: 0px;
	}
	.close-alert a{
		color: #861d1d;
	}
</style>

<div class="alert alert-erro">
	<div class="close-alert pull-right">
		<a href="#" class="fechar-alerta"><i class="fa fa-close"></i></a>
	</div>
    <div class="alert-conteudo">
        <h4 class="alert-title"><i class="fa fa-exclamation fa-2x" aria-hidden="true"></i> <?= __('Erro') ?></h4>
        <p><?= h($message) ?></p>
    </div>
</div>

<script>
	$('.fechar-alerta').click(function(){
		$('.alert-erro').animate({'height' : '0', 'opacity' : 0, 'display' : 'none' }, 1000);
	});
	setTimeout(function () {
		$('.alert-erro').animate({'height' : '0', 'opacity' : 0, 'display' : 'none' }, 1000);
   	}, 15000);	
</script>