<div class="alert alert-close alert-warning">
    <a href="#" title="Close" class="glyph-icon alert-close-btn icon-remove"></a>
    <div class="bg-orange alert-icon">
        <i class="glyph-icon icon-warning"></i>
    </div>
    <div class="alert-content">
        <h4 class="alert-title"><?= __('Alerta') ?></h4>
        <p><?= ($message) ?></p>
    </div>
</div>
