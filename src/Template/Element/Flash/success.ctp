
<style>
	.alert-successo{
		color: #1d861f;
		background-color: #f3ffe4!important;
		background-image: none;
		border: 2px solid #8dc73f;
	}
	.alert-conteudo h4{
		text-transform: uppercase;
		font-weight: bolder;
	}
	.alert-conteudo p{
		font-size: 16px;
		font-weight: bold;
		margin-bottom: 0px;
	}
	.close-alert a{
		color: #3c763d;
	}
</style>

<div class="alert alert-successo">
	<div class="close-alert pull-right">
		<a href="#" class="fechar-alerta"><i class="fa fa-close"></i></a>
	</div>
    <div class="alert-conteudo">
        <h4 class="alert-title"><i class="fa fa-2x fa-check"></i> <?= __('Sucesso') ?></h4>
        <p><?= ($message) ?></p>
    </div>
</div>

<script>
	$('.fechar-alerta').click(function(){
		$('.alert').animate({'height' : '0', 'opacity' : 0, 'display' : 'none' }, 1000);
	});
	setTimeout(function () {
		$('.alert').animate({'height' : '0', 'opacity' : 0, 'display' : 'none' }, 1000);
   	}, 15000);	
</script>