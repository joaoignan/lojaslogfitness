<style>
.total-mochila-combo{
	display: inline-block;
	background-color: #fff;
	width: 158px;
	border-radius: 5px;
    box-shadow: 0px 0px 5px #545454;
    position: relative;
	z-index: 18;
}
.mochila-fixa{
	position: fixed;
	top: 125px;
}
.total-produtos-mochila-combo, .total-botoes-mochila-combo{
	padding: 5px;
}
.total-produtos-mochila-combo ul{
	padding-left: 0;
}
.total-produtos-mochila-combo ul li{
	padding-left: 0;
}
.total-item-combo{
    width: 100%;
    height: 100%;
    display: inline-block;
    padding: 5px 0 0;
    border: 2px solid transparent;
    cursor: pointer;
}
.total-item-combo:hover{
    border: 2px solid #5089cf;
}
.total-item-combo .imagem-item-combo{
	min-width: 50px;
	width: 35%;
	height: 50px;
	display: inline-block;
	text-align: center;
	float: left;
	margin-bottom: 7px;
	position: relative;
}
.total-item-combo .imagem-item-combo img{
	height: 100%;
	width: auto; 
}
.total-item-combo .desc-item-combo{
	width: 62%;
	height: 50px;
	display: inline-block;
	float: left;
	margin-bottom: 7px;
	padding-left: 5px;
}
.total-item-combo a{
	color: rgb(120,120,120);
}
.total-item-combo a:hover{
    color: #5089cf;
}
.total-item-combo .desc-item-combo p{
	width: 100%;
	overflow:hidden;
    text-overflow:ellipsis;
    white-space: nowrap; 
    margin-bottom: 3px;
}
.total-item-combo-active {
	border: 2px solid #5089cf;
}
.total-item-combo-active a {
	color: #5089cf;
}
.btn-finalizar-combo{
    background-color: rgba(72, 159, 54, 1);
    color: rgb(255,255,255);
}
.btn-finalizar-combo:hover{
    color: rgb(255,255,255);
    background-color: rgba(55, 119, 42, 1);
}
.change-combo{
	cursor: pointer;
}
.change-combo:hover{
	font-weight: 700;
	color: #5089cf;
}
#combo-mobile-btn{
    display: none;
}
.change-hover{
	background-color: rgba(80, 137, 207, 0.8);
	color: #fff;
	position: absolute;
	width: 100%;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	padding-top: 18px;
	margin-top: -5px;
	margin-bottom: -7px;
	display: none;
}
.total-item-combo:hover .change-hover{
	display: block;
}
.fechar-overlay-mochila-combo{
	display: none;
}
@media all and (max-width: 992px){
    #combo-mobile-btn{
        display: block;
        width: 70px;
        height: 70px;
        background-color: #5089cf;
        position: fixed;
        bottom: 65px;
        right: -80px;
        border-radius: 50%;
        color: white;
        text-align: center;
        padding-top: 12px;
        font-size: 12px;
        cursor: pointer;
        z-index: 5;
    }
    #combo-mobile-btn .fa{
        font-size: 25px;
    }
    .fundo-mochila-combo{
    	overflow-y: hidden;
	    background-color: rgba(51,51,51,0.9);
	    z-index: 16;
	    position: fixed;
	    top: 0;
	    right: 0;
	    bottom: 0;
	    width: 100%;
	    display: block;
    }
    .total-mochila-combo{
    	position: fixed;
    	left: 0px;
    	top: 15px;
    	width: 230px;
    	z-index: 18;
    	border-radius: 0px;
    	box-shadow: none;
    }
    .fechar-overlay-mochila-combo {
	   	display: block;
		position: fixed;
		color: white;
		font-size: 20px;
		top: 25px;
		z-index: 18;
		cursor: pointer;
	}
    .btn-fechar-mochila-combo{
        padding: 10px;
	    background-color: red;
	    line-height: 1.1;
	    font-size: 12px;
	    position: fixed;
	    left: 230px;
	    top: 15px;
	    width: 50px;
	    text-align: center;
	    height: 50px;
    }

}
</style>

<?php if ($number == 4) { ?>
	<style>
		.fundo-mochila-combo{
			overflow-y: hidden;
		    background-color: rgba(51,51,51,0.9);
		    z-index: 16;
		    position: fixed;
		    top: 0;
		    right: 0;
		    bottom: 0;
		    width: 100%;
		    display: block;
		}
	</style>
<?php } ?>

<?php if ($this->request->is('mobile')) { ?>
	<script>
		$(document).ready(function(){
			$('#combo-mobile-btn').click(function(){
				$('#combo-mobile-btn').animate({'right' : '-80px'});
				$('html,body').css({"overflow":"hidden"});
				setTimeout(function() {
					$('.fundo-mochila-combo').fadeIn();
					$('.btn-fechar-mochila-combo').fadeIn();
					$('.total-mochila-combo').animate({'left' : '0px'});
				}, 500);	
			});
			$('.fundo-mochila-combo, .btn-fechar-mochila-combo').click(function(){
				$('.total-mochila-combo').animate({'left' : '-600px'});
				$('.btn-fechar-mochila-combo').fadeOut();
				setTimeout(function() {
					$('.fundo-mochila-combo').fadeOut();
					$('#combo-mobile-btn').animate({'right' : '10px'});
				}, 500);
				$('html,body').css({"overflow":"auto"});
			});
			$('.total-mochila-combo').click(function(e){
				e.stopPropagation();
			});
		});
	</script>
<?php } ?>
<script>
	$(document).ready(function(){
		$(document).scroll(function(){
			var posicaoScroll = $(document).scrollTop();
			var tamanhoTela = screen.width;
			if (tamanhoTela > 500){
				var somaScroll = 90;
			}
			else{
				var somaScroll = 60;
			}
			if (posicaoScroll > 230){
				$('.total-mochila-combo').addClass("mochila-fixa");
			}
			else{
				$('.total-mochila-combo').removeClass("mochila-fixa");
			}
		});
	});
</script>

<div class="fechar-overlay-mochila-combo">
    <div class="btn-fechar-mochila-combo">
        <span><i class="fa fa-close"></i>fechar</span>
    </div>
</div>

<div id="combo-mobile-btn">
    <i class="fa fa-plus"></i><br>
    <span>Combo</span>
</div>
<div class="fundo-mochila-combo"></div>
<div class="total-mochila-combo">
	<div class="total-produtos-mochila-combo">
		<ul>
			<?php if($ProdutoCombo1) { ?>
                <?php $foto = unserialize($ProdutoCombo1->fotos)[0]; ?>
				<li>
					<div class="total-item-combo <?= $number == 1 ? 'total-item-combo-active' : '' ?>">	
						<?= $this->Html->link('
						<div class="imagem-item-combo">'
							.$this->Html->image($foto).'
							<div class="change-hover">
								<span>Trocar</span>
							</div>
						</div>
						<div class="desc-item-combo">
							<p title="'.$ProdutoCombo1->produto_base->name.'"><strong>'.$ProdutoCombo1->produto_base->name.'</strong></p>
							<p title="'.$ProdutoCombo1->tamanho.' '.$ProdutoCombo1->unidade_medida.', '.$ProdutoCombo1->propriedade.'">'.$ProdutoCombo1->tamanho.' '.$ProdutoCombo1->unidade_medida.', '.$ProdutoCombo1->propriedade.'</p>
						</div>', $SSlug.'/montar-combo/',['escape' => false])  ?>
					</div>
				</li>
			<?php } else { ?>
				<li>
					<div class="total-item-combo <?= $number == 1 ? 'total-item-combo-active' : '' ?>">	
						<div class="imagem-item-combo">
							<?= $this->Html->image('proteinsprof.png'); ?>
						</div>
						<?= $this->Html->link('
						<div class="desc-item-combo">
							<p><strong>Escolher</strong></p>
							<p>'.$TipoCombo['categoria1']['name'].'</p>
						</div>', $SSlug.'/montar-combo/',['escape' => false])  ?>
					</div>
				</li>
			<?php } ?>

			<?php if($ProdutoCombo2) { ?>
                <?php $foto = unserialize($ProdutoCombo2->fotos)[0]; ?>
				<li>
					<div class="total-item-combo <?= $number == 2 ? 'total-item-combo-active' : '' ?>">	
						<?= $this->Html->link('
						<div class="imagem-item-combo">'
							.$this->Html->image($foto).'
							<div class="change-hover">
								<span>Trocar</span>
							</div>
						</div>
						<div class="desc-item-combo">
							<p title="'.$ProdutoCombo2->produto_base->name.'"><strong>'.$ProdutoCombo2->produto_base->name.'</strong></p>
							<p title="'.$ProdutoCombo2->tamanho.' '.$ProdutoCombo2->unidade_medida.', '.$ProdutoCombo2->propriedade.'">'.$ProdutoCombo2->tamanho.' '.$ProdutoCombo2->unidade_medida.', '.$ProdutoCombo2->propriedade.'</p>
						</div>', $SSlug.'/montar-combo/2',['escape' => false])  ?>
					</div>
				</li>
			<?php } else { ?>
				<li>
					<div class="total-item-combo <?= $number == 2 ? 'total-item-combo-active' : '' ?>">	
						<div class="imagem-item-combo">
							<?= $this->Html->image('proteinsprof.png'); ?>
						</div>
						<?= $this->Html->link('
						<div class="desc-item-combo">
							<p><strong>Escolher</strong></p>
							<p>'.$TipoCombo['categoria2']['name'].'</p>
						</div>', $SSlug.'/montar-combo/2',['escape' => false])  ?>
					</div>
				</li>
			<?php } ?>

			<?php if($ProdutoCombo3) { ?>
                <?php $foto = unserialize($ProdutoCombo3->fotos)[0]; ?>
				<li>
					<div class="total-item-combo <?= $number == 3 ? 'total-item-combo-active' : '' ?>">	
						<?= $this->Html->link('
						<div class="imagem-item-combo">'
							.$this->Html->image($foto).'
							<div class="change-hover">
								<span>Trocar</span>
							</div>
						</div>
						<div class="desc-item-combo">
							<p title="'.$ProdutoCombo3->produto_base->name.'"><strong>'.$ProdutoCombo3->produto_base->name.'</strong></p>
							<p title="'.$ProdutoCombo3->tamanho.' '.$ProdutoCombo3->unidade_medida.', '.$ProdutoCombo3->propriedade.'">'.$ProdutoCombo3->tamanho.' '.$ProdutoCombo3->unidade_medida.', '.$ProdutoCombo3->propriedade.'</p>
						</div>', $SSlug.'/montar-combo/3',['escape' => false])  ?>
					</div>
				</li>
			<?php } else { ?>
				<li>
					<div class="total-item-combo <?= $number == 3 ? 'total-item-combo-active' : '' ?>">	
						<div class="imagem-item-combo">
							<?= $this->Html->image('proteinsprof.png'); ?>
						</div>
						<?= $this->Html->link('
						<div class="desc-item-combo">
							<p><strong>Escolher</strong></p>
							<p>'.$TipoCombo['categoria3']['name'].'</p>
						</div>', $SSlug.'/montar-combo/3',['escape' => false])  ?>
					</div>
				</li>
			<?php } ?>
		</ul>
	</div>
	
	<?php 
		$valor = 0.0;
		if($ProdutoCombo1) {
			$ProdutoCombo1->preco_promo > 1 ? $preco = $ProdutoCombo1->preco_promo * $desconto_geral : $preco = $ProdutoCombo1->preco * $desconto_geral;
			$valor+= $preco;
		}
		if($ProdutoCombo2) {
			$ProdutoCombo2->preco_promo > 1 ? $preco = $ProdutoCombo2->preco_promo * $desconto_geral : $preco = $ProdutoCombo2->preco * $desconto_geral;
			$valor+= $preco;
		}
		if($ProdutoCombo3) {
			$ProdutoCombo3->preco_promo > 1 ? $preco = $ProdutoCombo3->preco_promo * $desconto_geral : $preco = $ProdutoCombo3->preco * $desconto_geral;
			$valor+= $preco;
		}
	?>
	
	<div class="total-botoes-mochila-combo text-left">
		<p><strong>Valor</strong> <span class="pull-right">R$ <?= number_format($valor, 2, ',', '.') ?></span></p>
		<p><strong>Desconto</strong> <span class="pull-right"> R$ <?= number_format($valor * 0.05, 2, ',', '.') ?></span></p>
		<p><strong>Total</strong> <span class="pull-right">R$ <?= number_format($valor * 0.95, 2, ',', '.') ?></span></p>
		<p class="text-center"><span class="change-combo"><i class="fa fa-undo" aria-hidden="true"></i> mudar combo</span></p>
		<p class="text-center">
			<?php if ($ProdutoCombo1 != null && $ProdutoCombo2 != null && $ProdutoCombo3 != null) { ?>
				<?= $this->Html->link('<button class="btn btn-block btn-finalizar-combo"> Finalizar </button>', $SSlug.'/fechar-combo/',['escape' => false]) ?>
			<?php }
			else{ ?>
				<button type="button" disabled="true" class="btn btn-block btn-finalizar-combo"> Finalizar </button>
			<?php } ?>
		</p>
	</div>
</div>


