<!-- INSTAGRAM -->
<?php

$userid = "2281776621";
$accessToken = "2281776621.a126682.11085d1789c744ab86f10049ea7adb2f";

// Gets our data
function fetchData($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
?>
<style type="text/css">
    #instagram-rodape {
        box-shadow: none;
    }
</style>

<?php
// Pulls and parses data.
$result = fetchData("https://api.instagram.com/v1/users/{$userid}/media/recent/?access_token={$accessToken}");
$result = json_decode($result);
?>
<!-- Renders images. @Options (thumbnail,low_resoulution, high_resolution, standard_resolution) -->

<div class="row" id="instagram-rodape">
    <div class="col-md-12" id="demo">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 ">
                    <div id="owl" class="owl-carousel">
                        <?php
                        foreach ($result->data as $post):
                            ?>
                            <div class="item">
                                <a href="<?= $post->link ?>" target="_blank"
                                   title="Veja mais...">
                                    <img src="<?= $post->images->thumbnail->url ?>"
                                         alt="Instagram <?= count($post->tags) > 0 ? '#'.implode(' #', $post->tags) : '' ?>" />
                                </a>
                            </div>
                            <?php
                        endforeach
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <?php
        /*
        <!-- SnapWidget -->
        <iframe src="http://snapwidget.com/sc/?h=Y2FjaXF1ZWRpZ2l0YWx8aW58MzMwfDN8M3x8bm98NXxub25lfG9uU3RhcnR8eWVzfG5v&ve=010216"
                 title="Instagram Widget" class="snapwidget-widget" allowTransparency="true" frameborder="0"
                 scrolling="no" style="border:none; overflow:hidden; width:100%; height:330px"></iframe>
        */
        ?>
    </div>
</div>