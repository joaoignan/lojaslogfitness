<style>
    @media all and (max-width: 500px){
        .total-produto-box-mobile{
            width: 265px;
            position: relative;
            height: 150px;
            margin: 0 auto;
            box-shadow: 0px 0px 5px #545454
        }
        .codigo-imagem-box-mobile, .informacoes-box-mobile{
            display: inline-block;
            position: relative;
            float: left;
            height: 150px;
            padding: 5px;
        }
        .codigo-imagem-box-mobile{
            width: 100px;
            font-size: 12px;
        }
        .codigo-imagem-box-mobile img{
            max-height: 100px;
            max-width: 100px;
        }
        .informacoes-box-mobile{
            width: 160px;
        }
        .informacoes-box-mobile p{
            margin-bottom: 2px;
        }
        .informacoes-box-mobile .marca, .informacoes-box-mobile .treinos{
            font-size: 10px;
        }
        .informacoes-box-mobile .parcelas{
            font-size: 12px;
        }
        .informacoes-box-mobile .marca{
            text-transform: uppercase;
        }
        .opcoes-box-mobile{
            position: absolute;
            box-shadow: inset 2px 0px 5px 0px #a7a7a7;
            top: 0;
            right: 0;
            bottom: 0;
            width: 1px;
            z-index: 3;
            background-color: rgb(245, 245, 245);
            transition: all 1s;
        }
        .aberta{
            width: 100%!important;
        }
        .span_objetivo{
            display: inline-block;
            padding: 0px 5px;
            font-size: 10px;
            margin: 0 3px 0 0;
            color: #fff;
            height: 16px;
        }
        .nome_box_mobile{
            width: 100%;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }
        .propriedade_box_mobile{
            text-transform: capitalize;
            width: 100%;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }
        .total-tarjas{
            top: 123px;
        }
        .button-abrir-opcoes{
            width: 25px;
            float: left;
            padding: 65px 8px;
        }
        .total-abrir-opcoes{
            float: left;
            width: 100%;
            display: none;
            padding: 10px;
            color: #fff;
        }
        .linha1{
            margin-bottom: 15px;
            width: 100%;
            margin: 15px 0;
        }
        .linha2{
            width: 100%;
            margin: 0 0 15px 0;
        }
        .btn-verde-box-mobile{
            background-color: rgba(72, 159, 54, 1);
            color: #fff;
        }
        .btn-verde-box-mobile:hover, .btn-verde-box-mobile:active, .btn-verde-box-mobile:focus {
            background-color: rgba(55, 119, 42, 1);
            color: #fff;
        }
        .objetivos_box_mobile{
            height: 20px;
        }
        .fa-minus-circle{
            display: none;
        }
        .active .fa-plus-circle{
            display: none!important
        }
        .active .fa-minus-circle{
            display: block!important;
        }
    }
</style>

<script>
    $(document).ready(function() {
        $('.total-produto-box-mobile').click(function (e) {
            if (!$(this).find('.opcoes-box-mobile').hasClass('active')) {
                var id = $(this).find('.opcoes-box-mobile').attr('data-id');
                $('.opcoes-box-mobile').removeClass('active');
                $('.opcoes-box-mobile').css("width", "1px");
                $(this).find('.opcoes-box-mobile').css("width", "100%");
                $(this).find('.opcoes-box-mobile').css("background-color", "rgba(56, 56, 56, 0.85);");
                $(this).find('.opcoes-box-mobile').addClass('active');
                $('.total-abrir-opcoes').fadeOut(100);
                $('.total-abrir-opcoes-'+id).fadeIn(2000);
                console.log('abriu');
            }
            else{
                $('.total-abrir-opcoes').fadeOut();
                $(this).find('.opcoes-box-mobile').css("width", "1px");
                $(this).find('.opcoes-box-mobile').css("background-color", "rgba(245, 245, 245, 1);");
                $(this).find('.opcoes-box-mobile').removeClass('active');
                console.log('fechou');
            }
        });
    });
</script>

<?php if(count($produtos) >= 1) { ?>
    <div class="title-section">
        <h3><?= $title ?></h3>
    </div>
    <div class="owl-carousel">
        <?php if ($this->request->is('mobile')) { ?>
            <?php $i = 1; ?>
            <?php foreach($produtos as $produto) { ?>
                <div class="item">
                    <?php 
                        $fotos = unserialize($produto->fotos); 

                        $primeira_foto = explode('produtos/', $fotos[0]);

                        $foto = $primeira_foto[0].'produtos/md-'.$primeira_foto[1];
                    ?>
                    <div class="total-produto-box-mobile">
                        <div class="total-tarjas">
                            <?php if ($produto->estoque == 1) { ?>
                                <div class="tarja tarja-ultimo">
                                    <span>Último</span>
                                </div>
                            <?php } else if ($produto->estoque >= 1 && $produto->preco_promo) { ?>
                                <div class="tarja tarja-promocao">
                                    <span>Promoção</span>
                                </div>
                            <?php } else if($produto->estoque >= 1 && $produto->created > $max_date_novidade) { ?>
                                <div class="tarja tarja-novidade">
                                    <span>Novidade</span>
                                </div>
                            <?php } ?>               
                        </div>
                        <div class="codigo-imagem-box-mobile text-center">
                            <span>Código: <?= $produto->id ?></span>
                            <?= $this->Html->image($foto[0],
                            ['val' => $produto->slug, 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]); ?>
                        </div>
                        <div class="informacoes-box-mobile text-right">
                            <?php if($produto->preco_promo || $desconto_geral) { ?>
                                <?php 
                                    $produto->preco_promo ?
                                        number_format($preco = $produto->preco_promo * $desconto_geral, 2, ',','.') :
                                        number_format($preco = $produto->preco * $desconto_geral, 2, ',','.'); 
                                ?>

                                <?php if($produto->preco_promo != null) { ?>
                                    <?php if($produto->preco_promo < 100.00) { ?>
                                        <?php $parcel = ($produto->preco_promo * $desconto_geral) * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/2?>
                                        <?php $maximo_parcelas = "2x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto->preco_promo > 100.00 && $produto->preco_promo < 200.00) { ?>
                                        <?php $parcel = ($produto->preco_promo * $desconto_geral) * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/3?>
                                        <?php $maximo_parcelas = "3x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto->preco_promo > 200.00 && $produto->preco_promo < 300.00) { ?>
                                        <?php $parcel = ($produto->preco_promo * $desconto_geral) * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/5?>
                                        <?php $maximo_parcelas = "5x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto->preco_promo > 300.00) { ?>
                                        <?php $parcel = ($produto->preco_promo * $desconto_geral) * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/6?>
                                        <?php $maximo_parcelas = "6x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } ?>
                                <?php } else { ?>
                                    <?php if($produto->preco < 100.00) { ?>
                                        <?php $parcel = ($produto->preco * $desconto_geral) * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/2?>
                                         <?php $maximo_parcelas = "2x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto->preco > 100.00 && $produto->preco < 200.00) { ?>
                                        <?php $parcel = ($produto->preco * $desconto_geral) * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/3?>
                                        <?php $maximo_parcelas = "3x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto->preco > 200.00 && $produto->preco < 300.00) { ?>
                                        <?php $parcel = ($produto->preco * $desconto_geral) * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/5?>
                                        <?php $maximo_parcelas = "5x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto->preco > 300.00) { ?>
                                        <?php $parcel = ($produto->preco * $desconto_geral) * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/6?>
                                        <?php $maximo_parcelas = "6x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <div class="objetivos_box_mobile">
                                <?php foreach ($produto->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                    <span class="span_objetivo" style="background-color: <?= $produto_objetivo->objetivo->color ?>"><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                <?php } ?>
                            </div>
                            <?php if($produto->produto_base->tipo_produto_id == 1) { ?>
                                <p class="marca"><?= strtolower($produto->produto_base->marca->name); ?></p>
                                <p class="nome_box_mobile"><strong><?= $produto->produto_base->name ?></strong></p>
                                <p class="treinos"><?= $produto->tamanho.' '.$produto->unidade_medida.' / '.$produto->doses; ?> treinos</p>
                                <p class="propriedade_box_mobile"><?= strtolower($produto->propriedade); ?></p>
                                <p><strong><?= $maximo_parcelas." de R$".$valor_parcela ?></strong></p>
                                <p class="parcelas">ou R$ <?= number_format($preco, 2, ',','.') ?></p>
                           <?php } else if($produto->produto_base->tipo_produto_id == 2) { ?>
                                <p class="marca"><?= strtolower($produto->produto_base->marca->name); ?></p>
                                <p class="nome_box_mobile"><strong><?= $produto->produto_base->name ?></strong></p>
                                <p class="treinos"><?= $produto->tamanho ? $produto->tamanho : '&nbsp' ?></p>
                                <p class="propriedade_box_mobile"><?= $produto->cor ? strtolower($produto->cor) : '&nbsp' ?></p>
                                <p><strong><?= $maximo_parcelas." de R$".$valor_parcela ?></strong></p>
                                <p class="parcelas">ou R$ <?= number_format($preco, 2, ',','.') ?></p>
                            <?php } ?>
                        </div>
                        <div class="opcoes-box-mobile text-center" data-id="<?= $produto->id ?>">
                            <div class="total-abrir-opcoes total-abrir-opcoes-<?= $produto->id ?>">
                                <div class="row">
                                    <div class="col-xs-12 text-left">
                                        <strong>X Fechar</strong>
                                    </div>
                                </div>
                                <div class="row linha1">
                                    <div class="col-xs-12">
                                        <button type="button" slug-id="<?= $SSlug ?>" val="<?= $produto->slug ?>" data-id="<?= $produto->id ?>" class="btn btn-verde-box-mobile btn-block incluir-mochila">Comprar</button>
                                    </div>
                                </div>
                                <div class="row linha2">
                                    <div class="col-xs-6">
                                        <?= $this->Html->link('<button type="button" class="btn btn-verde-box-mobile btn-block">Detalhes</button>',$SSlug.'/produto/'.$produto->slug,['escape' => false]); ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <button type="button" class="btn btn-verde-box-mobile btn-block btn-indique" data-id="<?= $produto->id ?>">Indicar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            $('.btn-indique').click(function () {
                                    var id_produto = $(this).attr('data-id');
                                    $('#overlay-indicacao-'+id_produto).height('100%');
                                    $("html,body").css({"overflow":"hidden"});
                            });
                            function closeIndica() {
                                $('.overlay-indicacao').height('0%');
                                $("html,body").css({"overflow":"auto"});
                            }
                        </script>
                        <script type="text/javascript">
                            $('.send-indicacao-home-btn').click(function(e) {
                                e.preventDefault();
                                var botao = $(this);
                            
                                botao.html('Enviando...');
                                botao.attr('disabled', 'disabled');

                                var id = $(this).attr('data-id');
                                 var slug = '<?= $SSlug ?>';

                                var valid = $('.indicar_prod_home_'+id).validationEngine("validate");
                                if (valid == true) {
                                    $.ajax({
                                        type: "POST",
                                        url: WEBROOT_URL + slug + '/produtos/overlay-indicar-produtos-home/',
                                        data: {
                                            name: $('.name-'+id).val(),
                                            email: $('.email-'+id).val(), 
                                            produto_id: $('.id_produto-'+id).val()
                                        }
                                    })
                                    .done(function (data) {
                                        if(data == 1) {
                                            botao.html('Enviado!');
                                            setTimeout(function () {
                                                botao.html('Enviar');
                                                botao.removeAttr('disabled');
                                            }, 2200);
                                        } else {
                                            botao.html('Falha ao enviar... Tente novamente...');
                                            setTimeout(function () {
                                                botao.html('Enviar');
                                                botao.removeAttr('disabled');
                                            }, 2200);
                                        }
                                    });
                                }else{
                                    $('.indicar_prod_home_'+id).validationEngine({
                                        updatePromptsPosition: true,
                                        promptPosition: 'inline',
                                        scroll: false
                                    });
                                }
                            });
                        </script>
                    </div>
                </div>
            <?php } ?>
        <?php } else { ?>
            <?php $i = 1; ?>
            <?php foreach($produtos as $produto) { ?>
                <div class="item">
                    <div class="box-completa-produto" itemtype="http://schema.org/Product">
                        <div class="tarja-objetivo">
                            <?php
                                $contador_objetivos = 0; 
                                $novo_top = 0;
                            ?>
                            <?php foreach ($produto->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                <?php if ($contador_objetivos < 2) { ?>
                                <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                                    <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                </div>
                                <?php } ?>
                                <?php
                                    $contador_objetivos++;
                                    $novo_top = $novo_top + 19;
                                 ?>
                            <?php } ?>
                        </div>
                        <div class="total-tarjas">
                            <?php if ($produto->estoque == 1) { ?>
                                <div class="tarja tarja-ultimo">
                                    <span>Último</span>
                                </div>
                            <?php } else if ($produto->estoque >= 1 && $produto->preco_promo) { ?>
                                <div class="tarja tarja-promocao">
                                    <span>Promoção</span>
                                </div>
                            <?php } else if($produto->estoque >= 1 && $produto->created > $max_date_novidade) { ?>
                                <div class="tarja tarja-novidade">
                                    <span>Novidade</span>
                                </div>
                            <?php } ?>               
                        </div>
                        <div class="info-produto">
                            <div class="produto-superior">
                                <a href="<?= $SSlug.'/produto/'.$produto->slug ?>">
                                    <?php 
                                        $fotos = unserialize($produto->fotos); 

                                        $primeira_foto = explode('produtos/', $fotos[0]);

                                        $foto = $primeira_foto[0].'produtos/md-'.$primeira_foto[1];
                                    ?>
                                    <div class="foto-produto" id="produto-<?= $i ?>" >
                                        <?= $this->Html->image(
                                            $foto[0],
                                            ['val' => $produto->slug, 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]); ?>
                                    </div>
                                    <div class="detalhes-produto">
                                        <div class="informacoes-detalhes text-left">
                                            <?php if($produto->produto_base->tipo_produto_id == 1) { ?>
                                                <p>Código: <?= $produto->id ?></p>
                                                <p><?= strtolower($produto->produto_base->marca->name); ?></p>
                                                <p><?= $produto->tamanho.' '.$produto->unidade_medida; ?></p>
                                                <p>para <?= $produto->doses; ?> treinos</p>
                                                <p><?= strtolower($produto->propriedade); ?></p>
                                            <?php } else if ($produto->produto_base->tipo_produto_id == 2) { ?>
                                                <p>Código: <?= $produto->id ?></p>
                                                <p><?= strtolower($produto->produto_base->marca->name); ?></p>
                                                <p><?= $produto->tamanho ?></p>
                                                <p><?= $produto->cor ?></p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="produto-inferior">
                                <?php if($produto->preco_promo || $desconto_geral) { ?>
                                    <?php 
                                        $produto->preco_promo ?
                                            number_format($preco = $produto->preco_promo * $desconto_geral, 2, ',','.') :
                                            number_format($preco = $produto->preco * $desconto_geral, 2, ',','.'); 
                                    ?>

                                    <?php if($produto->preco_promo != null) { ?>
                                        <?php if($produto->preco_promo < 100.00) { ?>
                                            <?php $parcel = ($produto->preco_promo * $desconto_geral) * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/2?>
                                            <?php $maximo_parcelas = "2x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php } else if($produto->preco_promo > 100.00 && $produto->preco_promo < 200.00) { ?>
                                            <?php $parcel = ($produto->preco_promo * $desconto_geral) * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/3?>
                                            <?php $maximo_parcelas = "3x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php } else if($produto->preco_promo > 200.00 && $produto->preco_promo < 300.00) { ?>
                                            <?php $parcel = ($produto->preco_promo * $desconto_geral) * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/5?>
                                            <?php $maximo_parcelas = "5x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php } else if($produto->preco_promo > 300.00) { ?>
                                            <?php $parcel = ($produto->preco_promo * $desconto_geral) * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/6?>
                                            <?php $maximo_parcelas = "6x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <?php if($produto->preco < 100.00) { ?>
                                            <?php $parcel = ($produto->preco * $desconto_geral) * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/2?>
                                             <?php $maximo_parcelas = "2x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php } else if($produto->preco > 100.00 && $produto->preco < 200.00) { ?>
                                            <?php $parcel = ($produto->preco * $desconto_geral) * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/3?>
                                            <?php $maximo_parcelas = "3x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php } else if($produto->preco > 200.00 && $produto->preco < 300.00) { ?>
                                            <?php $parcel = ($produto->preco * $desconto_geral) * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/5?>
                                            <?php $maximo_parcelas = "5x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php } else if($produto->preco > 300.00) { ?>
                                            <?php $parcel = ($produto->preco * $desconto_geral) * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/6?>
                                            <?php $maximo_parcelas = "6x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                                <?php 
                                    if($produto->preco_promo) {
                                        echo $this->Html->link("
                                            <h4>".$produto->produto_base->name."</h4>
                                            <p><span class='preco-corte'> R$".number_format($produto->preco, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                            $SSlug.'/produto/'.$produto->slug,
                                            ['escape' => false]);
                                    } else {
                                        echo $this->Html->link("
                                            <h4>".$produto->produto_base->name."</h4>
                                            <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                            $SSlug.'/produto/'.$produto->slug,
                                            ['escape' => false]);
                                    }
                                ?>
                            </div>
                            <div class="hover-produto <?= $produto->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                                <div class="col-xs-12">
                                    <div class="hexagono-indicar">
                                        <?php if($produto->status_id == 1) { ?>
                                            <div class="hexagon-disponivel btn-indique" data-id="<?= $produto->id ?>">
                                                <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                                <p>indique</p>
                                            </div>
                                            <script>
                                                $('.btn-indique').click(function () {
                                                        var id_produto = $(this).attr('data-id');
                                                        $('#overlay-indicacao-'+id_produto).height('100%');
                                                        $("html,body").css({"overflow":"hidden"});
                                                });
                                                function closeIndica() {
                                                    $('.overlay-indicacao').height('0%');
                                                    $("html,body").css({"overflow":"auto"});
                                                }
                                            </script>
                                        <?php } else { ?>
                                            <div class="hexagon produto-acabou-btn" data-id="<?= $produto->id ?>">
                                                <span><i class="fa fa-envelope-o"></i></span>
                                                <p>avise-me</p>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                                <?php if($produto->status_id == 1) { ?>
                                    <div class="col-xs-6">
                                        <?= $this->Html->link('
                                            <div class="hexagon-disponivel">
                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                <p>detalhes</p>
                                            </div>'
                                            ,$SSlug.'/produto/'.$produto->slug,
                                            ['escape' => false]);
                                        ?>
                                    </div>
                                    <div class="col-xs-6 text-center compare produto-comparar-btn">
                                        <div class="hexagon-disponivel btn-comparar-action" data-id="<?= $produto->id ?>">
                                            <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                            <p>comparar</p>
                                        </div>
                                    </div>
                                <?php }
                                else { ?>
                                    <div class="col-xs-6">
                                        <?= $this->Html->link('
                                            <div class="hexagon">
                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                <p>detalhes</p>
                                            </div>'
                                            ,$SSlug.'/produto/'.$produto->slug,
                                            ['escape' => false]);
                                        ?>
                                    </div>
                                    <div class="col-xs-6 text-center compare">
                                        <div class="hexagon btn-comparar-action" data-id="<?= $produto->id ?>">
                                            <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                            <p>comparar</p>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <script type="text/javascript">
                            $('.send-indicacao-home-btn').click(function(e) {
                                e.preventDefault();
                                var botao = $(this);
                            
                                botao.html('Enviando...');
                                botao.attr('disabled', 'disabled');

                                var id = $(this).attr('data-id');
                                var slug = '<?= $SSlug ?>';

                                var valid = $('.indicar_prod_home_'+id).validationEngine("validate");
                                if (valid == true) {
                                    $.ajax({
                                        type: "POST",
                                        url: WEBROOT_URL + slug + '/produtos/overlay-indicar-produtos-home/',
                                        data: {
                                            name: $('.name-'+id).val(),
                                            email: $('.email-'+id).val(), 
                                            produto_id: $('.id_produto-'+id).val()
                                        }
                                    })
                                    .done(function (data) {
                                        if(data == 1) {
                                            botao.html('Enviado!');
                                            setTimeout(function () {
                                                botao.html('Enviar');
                                                botao.removeAttr('disabled');
                                            }, 2200);
                                        } else {
                                            botao.html('Falha ao enviar... Tente novamente...');
                                            setTimeout(function () {
                                                botao.html('Enviar');
                                                botao.removeAttr('disabled');
                                            }, 2200);
                                        }
                                    });
                                }else{
                                    $('.indicar_prod_home_'+id).validationEngine({
                                        updatePromptsPosition: true,
                                        promptPosition: 'inline',
                                        scroll: false
                                    });
                                }
                            });
                        </script>
                        <div class="botoes-comprar-similiar">
                            <?php if($produto->visivel == 1 && $produto->status_id == 1) { ?>
                                <?php if($Academia->prazo_medio > 0 && $Academia->prazo_medio <= 72) { ?>
                                    <?php 
                                        if($Academia->prazo_medio <= 12) {
                                            $prazo_medio = 12;
                                        } else if($Academia->prazo_medio > 12 && $Academia->prazo_medio <= 24) {
                                            $prazo_medio = 24;
                                        } else if($Academia->prazo_medio > 24 && $Academia->prazo_medio <= 48) {
                                            $prazo_medio = 48;
                                        } else if($Academia->prazo_medio > 48) {
                                            $prazo_medio = 72;
                                        }
                                    ?>
                                    <p class="texto-prazo"><i class="fa fa-truck" aria-hidden="true"></i> Entrega em até <?= $prazo_medio ?>h</p>
                                <?php } ?>
                                <button class="btn btn-incluir incluir-mochila" slug-id="<?= $SSlug ?>" val="<?= $produto->slug ?>" data-id="<?= $produto->id ?>">Comprar</button>
                                <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $produto->id ?>">Indique</button>
                                <button class="desktop-hidden btn btn-incluir produto-comparar-btn btn-comparar-action" data-id="<?= $produto->id ?>">Comparar</button>
                            <?php } else if($produto->visivel == 1 && $produto->status_id == 2) { ?>
                                <button class="btn btn-similares">Similares</button>            
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php $i++; ?>
            <?php } ?>
        <div class="item">
        <?= $this->Html->link('
            <div class="box-completa-produto" id="produto-<?= $i ?>">
                <div class="col-md-12 col-xs-12 text-center bg-white-produto prdt">
                    <div class="text-center" style="margin-top: 155px;">
                        <span class="fa fa-plus fa-2x"></span><br>
                        <span>ver mais</span>
                    </div>
                </div>
            </div>',$SSlug.$link_produtos, ['escape' => false]); ?>
        </div>
        <?php } ?>
    </div>
    <?php foreach($produtos as $produto) { ?>
        <div id="aviseMe-<?= $produto->id ?>" class="overlay overlay-avise">
            <div class="overlay-avise-content overlay-avise-<?= $produto->id ?> text-center">
                <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                <br />
                <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$produto->id])?>
                <?= $this->Form->input('produto_id', [
                    'div'           => false,
                    'label'         => false,
                    'type'          => 'hidden',
                    'val'           => $produto->id,
                    'id'            => 'produto_id_acabou-'.$produto->id
                ])?>
                <?= $this->Form->input('name', [
                    'div'           => false,
                    'label'         => false,
                    'id'            => 'name_acabou-'.$produto->id,
                    'class'         => 'form-control validate[optional]',
                    'placeholder'   => 'Qual o seu nome?',
                ])?>
                <br/>
                <?= $this->Form->input('email', [
                    'div'           => false,
                    'label'         => false,
                    'id'            => 'email_acabou-'.$produto->id,
                    'class'         => 'form-control validate[required, custom[email]]',
                    'placeholder'   => 'Qual o seu email?',
                ])?>
                <br/>
                <?= $this->Form->button('Enviar', [
                    'type'          => 'button',
                    'data-id'       => $produto->id,
                    'class'         => 'btn btn-success send-acabou-btn'
                ])?>
                <br />
                <br />
                <?= $this->Form->end()?>
            </div>
        </div> 
        <div id="overlay-indicacao-<?= $produto->id ?>" class="overlay overlay-indicacao">
            <div class="overlay-content text-center">
                <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                <h4>Lembrou de alguém quando viu isso?</h4>
                <h4>Indique este produto!</h4>
                <br>
                <div class="row">
                    <div class="col-xs-12 share-whastapp share-buttons">
                        <a href="whatsapp://send?text=Isso é a sua cara! <?= WEBROOT_URL.$SSlug.'/produto/'.$produto->slug ?>">
                            <button class="btn button-blue"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</button>
                        </a>
                    </div>
                    <div class="col-xs-12 share-whastapp share-buttons">
                        <button class="btn button-blue" id="share-<?= $produto->id ?>"><i class="fa fa-comment-o" aria-hidden="true"></i> Messenger</button>
                        <script>
                            $(document).ready(function() {
                                $('#share-<?= $produto->id ?>').click(function(){
                                    var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$produto->slug ?>";
                                    var app_id = '1321574044525013';
                                    window.open('fb-messenger://share?link=' + link + '&app_id=' + app_id);
                                });
                            });
                        </script>
                    </div>
                    <div class="tablet-hidden col-xs-12 col-md-3 share-buttons">
                        <button data-id="whatsweb-<?= $produto->id ?>" class="btn button-blue abrir-whats"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp Web</button>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                        <a href="javascript: void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u=<?= WEBROOT_URL.$SSlug.'/produto/'.$produto->slug ?>','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');">
                            <button class="btn button-blue fb-share-button"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</button>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                        <button class="btn button-blue" id="copiar-clipboard-<?= $produto->id ?>"><i class="fa fa-clone" aria-hidden="true"></i> Copiar link</button>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                        <button  class="btn button-blue abrir-email"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail</button>
                    </div>
                </div>
                <div class="row">
                    <div id="collapse-whats-<?=$produto->id ?>" class="enviar-whats tablet-hidden">
                        <div class="col-xs-4 text-center form-group">
                            <?= $this->Form->input('whats-web-form', [
                                'div'           => false,
                                'id'            => 'nro-whatsweb-'.$produto->id,
                                'label'         => false,
                                'class'         => 'form-control form-indicar',
                                'placeholder'   => "Celular*: (00)00000-0000"
                            ])?>
                        </div>
                        <div class="col-xs-4 text-left obs-overlay">
                            <p>*Para enviar pra mais de um contato deixe este campo vazio.</p>
                        </div>
                        <div class="col-xs-4 text-center form-group">
                            <?= $this->Form->button('Enviar', [
                            'type'          => 'button',
                            'data-id'       => $produto->id,
                            'class'         => 'btn button-blue',
                            'id'            => 'enviar-whatsweb-'.$produto->id
                            ])?>
                        </div>
                        <script>
                            $(document).ready(function() {
                                $('#enviar-whatsweb-<?=$produto->id ?>').click(function(){

                                    var numero;
                                    var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$produto->slug ?>";

                                    if($('#nro-whatsweb-<?= $produto->id ?>').val() != '') {
                                        numero = '55'+$('#nro-whatsweb-<?= $produto->id ?>').val();
                                    }
                                    
                                    window.open('https://api.whatsapp.com/send?phone=' + numero + '&text=Isso é a sua cara! ' + link);
                                });
                            });
                        </script>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 enviar-email" id="collapse-email-<?=$produto->id ?>">
                        <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$produto->id])?>
                        <?= $this->Form->input('name', [
                            'div'           => false,
                            'label'         => false,
                            'class'         => 'form-control validate[optional] form-indicar name-'.$produto->id,
                            'placeholder'   => 'Quem indicou?',
                        ])?>
                        <br>
                        <?= $this->Form->input('email', [
                            'div'           => false,
                            'label'         => false,
                            'class'         => 'form-control validate[required] form-indicar email-'.$produto->id,
                            'placeholder'   => 'E-mail do destinatário',
                        ])?>
                        <?= $this->Form->input('id_produto', [
                            'div'           => false,
                            'class'         => 'id_produto-'.$produto->id,
                            'value'         => $produto->id,
                            'label'         => false,
                            'type'          => 'hidden'
                        ])?>
                        <br/>
                        <?= $this->Form->button('Enviar', [
                            'type'          => 'button',
                            'data-id'       => $produto->id,
                            'class'         => 'btn button-blue send-indicacao-home-btn form-control'
                        ])?>
                        <br> 
                        <?= $this->Form->end()?>
                    </div>
                </div>
                <div class="row"> 
                    <div class="col-xs-12 clipboard">
                        <?= $this->Form->input('url', [
                            'div'           => false,
                            'id'            => 'url-clipboard-'.$produto->id,
                            'label'         => false,
                            'value'         => WEBROOT_URL.$SSlug.'/produto/'.$produto->slug,
                            'readonly'      => true,
                            'class'         => 'form-control form-indicar'
                        ])?>
                    </div>  
                    <p><span class="copy-alert hide" id="copy-alert-<?=$produto->id ?>">Copiado!</span></p>
                    <script>
                        $(document).ready(function() {
                            // Copy to clipboard 
                            document.querySelector("#copiar-clipboard-<?=$produto->id ?>").onclick = () => {
                              // Select the content
                                document.querySelector("#url-clipboard-<?=$produto->id ?>").select();
                                // Copy to the clipboard
                                document.execCommand('copy');
                                // Exibe span de sucesso
                                $('#copy-alert-<?=$produto->id ?>').removeClass("hide");
                                // Oculta span de sucesso
                                setTimeout(function() {
                                    $('#copy-alert-<?=$produto->id ?>').addClass("hide");
                                }, 1500);
                            };
                        });
                    </script>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>