<div>
    <div class="col-md-4 informacoes-blocos seja-um-parceiro">
        <?= $this->Html->link(
            $this->Html->image('parceiro.png', ['alt' => 'Seja um parceiro']),
            '/seja-um-parceiro',
            ['escape' => false]
        )?>
        <p><span class="titulo">SEJA UM PARCEIRO </span></p>
        <p class="info">
            <span>
                    Quer a sua marca na logfitness?
                </span>
        </p>
    </div>
</div>