<div>
    <div class="col-md-4 informacoes-blocos central-relacionamento">
        <?= $this->Html->link(
            $this->Html->image('central-de-relacionamento.png', ['alt' => 'Central de Relacionamento']),
            '/central-de-relacionamento',
            ['escape' => false]
        )?>
        <p><span class="titulo">CENTRAL DE RELACIONAMENTO</span></p>
        <p class="info">
              <span>
                  Nós adoramos conversar!
               </span>
        </p>
    </div>
</div>