<style>
.tarja-em-breve{
    padding: 3px 5px;
    background-color: yellow;
    text-transform: uppercase;
    color: rgb(255,255,255);
    font-weight: 700;
    font-size: 10px;
    top: 0!important;
    right: 0!important;
    margin-top: 0px!important;
}
.total-filtro{
    box-shadow: 0px 0px 5px 1px #545454;
    border-radius: 3px;
    width: 165px;
    background-color: rgba(80, 137, 207, 1);
    display: inline-block;
    z-index: 10;
}
.total-filtro hr{
    margin-top: 0px;
    margin-bottom: 0px;
    border: 0;
    border-top: 1px solid #fff;
}
.fixed_filtro {
    position: fixed;
    width: 195px;
    top: 130px;
    z-index: 4;
}
.fixed_subfiltro {
    position: fixed;
    top: 80px;
    z-index: 3;
    padding-top: 40px;
    background: white;
    display: none;
    color: black;
}
@media all and (max-width: 992px) {
    .fixed_subfiltro {
        top: 50px;
        margin-left: -15px;
    }
}
.dropbtn {
    background-color: transparent;
    color: #fff;
    padding: 5px 8px;
    font-size: 14px;
    border: none;
    cursor: pointer;
    width: 100%;
    text-align: left;
    font-weight: 700;
    text-transform: uppercase;
    height: 100%;
}
.dropdown {
    background-color: rgba(80, 137, 207, 1);
    position: relative;
    display: inline-block;
    width: 100%;
    height: 50px;
}
.dropdown-promocoes{
    background-color: #f39c12;
}
.dropdown-promocoes:hover,
.dropdown-promocoes:active,
.dropdown-promocoes:visited,
.dropdown-promocoes:focus {
    background-color: rgba(220, 142, 18, 1)!important;
}
.dropdown span{
    position: absolute;
    right: 5px;
    top: 50%;
    margin-top: -10px;
}
.dropdown .input-group {
    margin: 15px 5px;
}
.dropdown .input-group span{
    position: relative!important;
}
.dropdown-content  {
    display: none;
    position: absolute;
    background-color: #fff;
    top: -120px;
    left: 190px;
    padding: 5px;
    width: 510px;
    height: 500px;
    z-index: 15;
    border: 2px solid #5089cf;
}
.dropdown-content a {
    color: rgba(90, 144, 210, 1);
    text-decoration: none;
    display: block;
    text-transform: capitalize;
}
.dropdown-marcas {
    width: 600px;
}
.total-marca{
    position: relative;
}
.marcas-btn{
    margin-bottom: 5px;
}
.logo-da-marca{
    position: absolute;
    top: 0;
    right: 0;
    display: none;
}
.dropdown-content a:hover {
    /*border-color: rgba(80, 137, 207, 1.0);*/
    /*-webkit-filter: grayscale(0%);*/
}
.dropdown-content img{
    height: 95px;
    padding: 5px;
}
.dropbtn:hover,
.dropbtn:active,
.dropbtn:visited,
.dropbtn:focus {
    outline: 0;
}
.dropdown:hover,
.dropdown:active,
.dropdown:visited,
.dropdown:focus {
    background-color: rgba(60, 102, 152, 1);
}
.lista-de-categorias, .lista-subcategorias{
    background-color: pink;
    padding: 0px 15px;
}
.textos-explicativos{
    background-color: lightblue;
    height: 100%;
}
.categoriabtn{
    width: 100%;
}
.dropdown-categoria{
    margin-right: 5px;
}
.dropdown-categoria:hover .dropdown-content-categoria{
    display: inline-block;
}
.dropdown-content-categoria{
    display: none;
    position: absolute;
    left: 35%;
    top: 0;
    padding: 0 15px;
    background-color: blue;
    width: 35%;
}
.categorias-objetivo {
    border: 2px solid #5089cf;
    padding: 5px;
    position: absolute;
    left: 190px;
    top: -120px;
    background: white;
    opacity: 0;
    display: none;
    z-index: 50;
    width: 510px;
    height: 500px;
}
.categorias-objetivo ul{
    padding-left: 0px;
    margin-left: -5px;
}
.div-lista{
    width: 262px;
    height: 100%;
    float: left;
    display: inline-block;
    position: relative;
}
.div-lista-marcas {
    width: 352px;
}
.div-lista li{
    color: #5089cf;
    position: relative;
    margin-bottom: 5px;
    padding-right: 5px;
    cursor: pointer
}
.div-lista li:hover {
    background: #5089cf;
}
.div-lista li:hover a {
    color: white;
}
.div-lista li:hover path {
    fill: white;
}
.div-lista span{
   position: absolute;
   right: 10px;
   top: 50%;
   margin-top: -10px;
}
.div-imagem {
    float: right;
    display: inline-block;
    width: 234px;
}
.div-imagem img{
    width: 100%;
}
.objetivo-btn {
    text-transform: capitalize;
}
.categoria-btn {
    text-transform: capitalize;
}
.subcategorias {
    border: 2px solid #5089cf;
    padding: 5px;
    position: absolute;
    left: 261px;
    top: -7px;
    background: white;
    opacity: 0;
    display: none;
    z-index: 50;
    width: 510px;
    height: 500px;
    animation-name: fadeOut;
    animation-duration: .3s;
}
.subcategorias li {
    color: #5089cf;
    cursor: pointer;
}
.subcategorias li:hover {
    color: #3c6698;
}
.categoria-objetivo-visible {
    opacity: 1;
    display: block;
    animation-name: fadeIn;
    animation-duration: .3s;
}
.subcategorias-items {
    text-transform: capitalize;
    height: 100%;
    width: 262px;
    float: left;
    display: inline-block;
    position: relative;
}
.imagem-objetivo {
    display: none;
    float: right;
}
.imagem-categoria {
    display: none;
    float: right;
}
.imagem-subcategoria {
    display: none;
    float: right;
}
.imagem-categoria-sub {
    display: none;
    float: right;
}
.image-visible {
    display: block;
}
.overlay-filtro {
    display: none;
    z-index: 10;
    position: fixed;
    float: inherit;
    background: rgba(0,0,0,0.7);
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    overflow: hidden;
    transition: 0.5s;
}
.filtro-div {
    z-index: 15;
}

.dropdown-active {
    background-color: rgb(39, 74, 117)!important;
}

.dropdown-content a:hover {
    color: #23527c;
}

.marca-image-filtro {
    float: right;
    display: none;
}

.marca-image-visible {
    display: block;
}

.imagens-marcas-div {
    float: right;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 234px;
    height: 100%;
    border-left: 1px solid rgba(0, 0, 0, .3);
}

.div-lista ul {
    padding: 0;
    margin-left: -5px;
}

.div-lista svg {
    height: 13px;
}

.fechar-overlay-filtro {
    display: none;
    position: fixed;
    color: white;
    font-size: 20px;
    top: 25px;
    z-index: 12;
    cursor: pointer;
}
.dropdown-escolhido {
    background: #5c799c;
}

.dropdown-escolhido:hover {
    background-color: #4a617d;
}
.li-escolhido {
    background: #5c799c;
}

.li-escolhido:hover {
    background-color: #4a617d;
}

.li-escolhido a {
    color: white;
}
.li-escolhido path {
    fill: white;
}
#filtro-mobile-btn{
    display: none;
}
.cobertura-filtro{
    background-color: rgba(51,51,51,0.9);
    position: fixed;
    top: 0;
    left: 0;
    width: 0%;
    height: 100%;
}
.voltar-filtro{
    display: none;
}
.categoria-clicked {
    background: #5089cf;
}
.categoria-clicked a {
    color: white;
}
.categoria-clicked path {
    fill: white;
}
@media all and (min-width: 993px) {
    .lista-marcas {
        column-count: 2;
    }
}
@media all and (max-width: 992px){
    #filtro-mobile-btn{
        display: block;
        width: 70px;
        height: 70px;
        background-color: #5089cf;
        position: fixed;
        bottom: 65px;
        right: 15px;
        border-radius: 50%;
        color: white;
        text-align: center;
        padding-top: 12px;
        font-size: 12px;
        cursor: pointer;
        z-index: 5;
    }
    #filtro-mobile-btn .fa{
        font-size: 25px;
    }
    .total-filtro{
        position: fixed;
        top: 0;
        left: -600px;
        box-shadow: 0px 0px 5px 1px #545454;
        border-radius: 3px;
        width: 270px;
        background-color: rgba(80, 137, 207, 1);
        display: inline-block;
    }
    .div-imagem {
        display: none!important;
    }

    .dropdown-content, .categorias-objetivo  {
        position: absolute;
        background-color: #fff;
        top: 0;
        left: 0px;
        padding: 5px;
        width: 270px;
        height: 580px;
        z-index: 15;
        border: 2px solid #5089cf;
        overflow-y: scroll;
    }
    .subcategorias{
        border: 2px solid #5089cf;
        padding: 5px;
        position: absolute;
        left: -7px;
        top: -7px;
        background: white;
        opacity: 0;
        display: none;
        z-index: 60;
        width: 510px;
        height: 580px;
        animation-name: fadeOut;
        animation-duration: .3s;
    }
    .voltar-filtro{
        display: block;
    }
    .fechar-overlay-filtro-btn{
        padding: 10px;
        background-color: red;
        line-height: 1.1;
        font-size: 12px;
        position: fixed;
        left: 270px;
        top: 240px;
        width: 50px;
        text-align: center;
        height: 50px;
    }
}
.subcategorias-visible {
    opacity: 1;
    display: block;
    animation-name: fadeIn;
    animation-duration: .3s;
}
@keyframes fadeIn {
    from {opacity: 0;}
    to {opacity: 1;}
}
</style>

<?php 
    function tirarAcentos($string){
        return preg_replace(array("/(á|à|ã|â|ä)/","/(ç)/","/(é|è|ê|ë)/","/(í|ì|î|ï)/","/(ó|ò|õ|ô|ö)/","/(ú|ù|û|ü)/","/(ñ)/"),explode(" ","a c e i o u n"),$string);
    }
?>
<div id="filtro-mobile-btn">
    <i class="fa fa-search"></i><br>
    <span>Produtos</span>
</div>

<div class="fechar-overlay-filtro">
    <div class="fechar-overlay-filtro-btn">
        <span><i class="fa fa-close"></i>fechar</span>
    </div>
</div>

<div class="total-filtro">
    <?php if($query != null || $marca != null || $tipo_filtro != null || $filtro != null) { ?>
    <?php $marca_name = str_replace('-', ' ', $marca); ?>
    <?php $filtro_name = str_replace('-', ' ', $filtro); ?>
    <?php $objetivo_da_categoria ? $objetivo_da_categoria = strtolower($objetivo_da_categoria).' <i class="fa fa-caret-right"></i> ' : '' ?>
    <?php $categoria_da_subcategoria ? $categoria_da_subcategoria = strtolower($categoria_da_subcategoria).' <i class="fa fa-caret-right"></i> ' : '' ?>
    <div class="refinar-busca-mobile desktop-filtro-hidden" style="padding: 5px; min-height: 40px; background-color: #5089cf; color: white; font-weight: 700;">
        <div class="" style="height: 25px">
            <span style="display: inline-block;">Ordenar: </span>
            <div class="" style="display: inline-block; float: right; width: 190px;">
                <?= $this->Form->input('ordenar_por', [
                    'options' => [
                        'randomico' => 'Aleatório',
                        'promocoes' => 'Promoções',
                        'maior-valor' => 'Maior valor',
                        'menor-valor' => 'Menor valor'
                    ],
                    'value'   => $ordenacao,
                    'class'   => 'form-control ordenar-select',
                    'label'   => false,
                    'style'   => 'padding: 0 8px; height: 24px;'
                ]) ?>
            </div>
        </div>
        <div class="" style="width: 100%"> 
            <?php if($query == null && $marca == null && $filtro == null) { ?>
                <button class="btn btn-escolhido btn-danger">Todos</button>
            <?php } ?>
            <?= $query != null ? '<button class="btn btn-escolhido btn-danger query_selected"><p><i class="fa fa-close"></i> '.$query.'</p></button>' : '' ?>
                <?= $marca != null ? '<button class="btn btn-escolhido btn-danger marca_selected"><p><i class="fa fa-close"></i> '.$marca_name.' </p></button>' : '' ?>
                <?= $filtro != null ? '<button class="btn btn-escolhido btn-danger filtro_selected"><p><i class="fa fa-close"></i> '.$objetivo_da_categoria.$categoria_da_subcategoria.$filtro_name.'</p></button>' : '' ?>
        </div>
        <div class="" style="margin-top: 2px;">
            <span>Total de <?= $count_produtos ?> Produtos</span>
        </div>
    </div>
    <?php } ?>
    <div class="input-group">
        <input type="text" class="form-control buscar-query" value="<?= $query ?>" placeholder="Ex: Whey">
        <span class="input-group-btn">
            <button class="btn btn-default buscar-btn" type="button"><i class="fa fa-search"></i></button>
        </span>
    </div><!-- /input-group -->
    <hr>
    <div class="dropdown dropdown-promocoes">
        <button class="dropbtn btn-comprar-overlay">Montar Combo</button>   
    </div> 
    <hr>
    <div class="dropdown">
        <button class="dropbtn marcas-link">Marcas <span><i class="fa fa-caret-right" aria-hidden="true"></i></span></button>
    </div> 
    <div class="dropdown-content dropdown-marcas text-left">
        <div class="div-lista div-lista-marcas">
            <ul class="lista-marcas">
                <li class="voltar-marca mobile-only">
                    <a><i class="fa fa-caret-left" aria-hidden="true"></i> Voltar</a>
                </li>
                <?php foreach($produtos_marcas_filtro as $marca) { ?>
                    <li class="<?= $marca_escolhida == $marca->id ? 'li-escolhido' : '' ?>">
                        <a class="marcas-btn" data-id="<?= $marca->slug ?>" m-id="<?= $marca->id ?>"><?= strtolower($marca->name) ?> (<?= $marca->quantidade ?>)</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <div class="div-imagem text-right imagens-marcas-div">
            <?php foreach($produtos_marcas_filtro as $marca) { ?>
                <?= $this->Html->image('marcas/'.$marca->image, ['class' => 'marca-image-filtro marca-image-'.$marca->slug, 'alt' => $marca->name, 'style' => 'width: 80%; height: auto']) ?>
            <?php } ?>
        </div>
    </div>
    <?php foreach ($objetivos as $objetivo) { ?>
        <div>
            <hr>
            <div class="dropdown">
                <button class="dropbtn objetivo-link" data-id="<?= $objetivo->id ?>"><?= $objetivo->name ?> <span><i class="fa fa-caret-right" aria-hidden="true"></i></span></button>
            </div> 
        </div>
        <div class="categorias-objetivo categorias-objetivo-<?= $objetivo->id ?>">
            <div class="div-lista">
                <ul>
                    <?php 
                        $ver_todos_objetivo = 0;

                        if($objetivo_escolhida == $objetivo->id && $categoria_escolhida <= 0) {
                            $ver_todos_objetivo = 1;
                        }
                    ?>
                    <li class="voltar-categoria mobile-only">
                        <a><i class="fa fa-caret-left" aria-hidden="true"></i> Voltar</a>
                    </li>
                    <li class="<?= $ver_todos_objetivo ? 'li-escolhido' : '' ?>">
                        <?php foreach($qtd_produtos_objetivos as $qtd_produto_objetivo) { ?>
                            <?php if($qtd_produto_objetivo->id == $objetivo->id) { ?>
                                <a class="objetivo-btn" image-id="<?= $objetivo->id ?>" data-id="<?= $objetivo->slug ?>"><?= $objetivo->image ?> Ver Todos <?= strtolower($objetivo->name) ?> (<?= $qtd_produto_objetivo->quantidade ?>)</a>
                            <?php } ?>
                        <?php } ?>
                    </li>
                    <?php foreach ($produtos_categorias_filtro as $categoria) { ?>
                        <?php if($categoria->objetivo_id == $objetivo->id) { ?>
                            <?php $tem_subcategoria = 0; ?>

                            <?php foreach($subcategorias as $subcategoria) { ?>
                                <?php if($subcategoria->categoria_id == $categoria->id) {
                                    $tem_subcategoria = 1;
                                } ?>
                            <?php } ?>

                            <?php if($tem_subcategoria) { ?>
                                <li class="categoria-link <?= $categoria_escolhida == $categoria->id ? 'li-escolhido' : '' ?>" data-id="<?= $categoria->id ?>">
                                    <a><?= $objetivo->image ?> <?= $categoria->name ?> <span><i class="fa fa-caret-right" aria-hidden="true"></i></span></a>
                                </li>
                                <div class="subcategorias subcategorias-<?= $categoria->id ?>">
                                    <div class="subcategorias-items">
                                        <ul>
                                            <?php 
                                                $ver_todos_categoria = 0;

                                                if($categoria_escolhida == $categoria->id && $subcategoria_escolhida <= 0) {
                                                    $ver_todos_categoria = 1;
                                                }
                                            ?>
                                            <li class="voltar-subcategoria mobile-only">
                                                <a><i class="fa fa-caret-left" aria-hidden="true"></i> Voltar</a>
                                            </li>
                                            <li class="categoria-btn categoria-ver-todos <?= $ver_todos_categoria ? 'li-escolhido' : '' ?>" image-id="<?= $categoria->id ?>" data-id="<?= $categoria->slug ?>">
                                                <a><?= $objetivo->image ?> Ver Todos <?= strtolower($categoria->name) ?> (<?= $categoria->quantidade ?>)</a>
                                            </li>
                                            <?php foreach($produtos_subcategorias_filtro as $subcategoria) { ?>
                                                <?php if($subcategoria->categoria_id == $categoria->id) { ?>
                                                    <li class="subcategoria-btn <?= $subcategoria_escolhida == $subcategoria->id ? 'li-escolhido' : '' ?>" image-id="<?= $subcategoria->id ?>" data-id="<?= $subcategoria->slug ?>">
                                                        <a><?= $objetivo->image ?> <?= strtolower($subcategoria->name) ?> (<?= $subcategoria->quantidade ?>)</a>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="div-imagem text-right">
                                        <?= $this->Html->image('categorias-filtro/'.$categoria->id.'.jpg', ['class' => 'imagem-categoria-sub image-visible imagem-categoria-sub-'.$categoria->id, 'alt' => $categoria->name]) ?>

                                        <?php foreach($produtos_subcategorias_filtro as $subcategoria) { ?>
                                            <?php if($subcategoria->categoria_id == $categoria->id) { ?>
                                                <?= $this->Html->image('subcategorias-filtro/'.$subcategoria->id.'.jpg', ['class' => 'imagem-subcategoria imagem-subcategoria-'.$subcategoria->id, 'alt' => $subcategoria->name]) ?>
                                            <?php } ?>
                                        <?php } ?>

                                        <?php foreach($categorias as $categoria) { ?>
                                            <?php if($categoria->objetivo_id == $objetivo->id) { ?>
                                                <?= $this->Html->image('categorias-filtro/'.$categoria->id.'.jpg', ['class' => 'imagem-categoria imagem-categoria-'.$categoria->id, 'alt' => $categoria->name]) ?>
                                            <?php } ?>
                                        <?php } ?>

                                        <?= $this->Html->image('objetivos-filtro/'.$objetivo->id.'.jpg', ['class' => 'imagem-objetivo imagem-objetivo-'.$objetivo->id, 'alt' => $objetivo->name]) ?>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <li class="categoria-btn <?= $categoria_escolhida == $categoria->id ? 'li-escolhido' : '' ?>" image-id="<?= $categoria->id ?>" data-id="<?= $categoria->slug ?>">
                                    <a><?= $objetivo->image ?> <?= strtolower($categoria->name) ?> (<?= $categoria->quantidade ?>)</a>
                                </li>
                            <?php } ?>

                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
            <div class="div-imagem text-right">
                <?= $this->Html->image('objetivos-filtro/'.$objetivo->id.'.jpg', ['class' => 'imagem-objetivo image-visible imagem-objetivo-'.$objetivo->id, 'alt' => $objetivo->name]) ?>

                <?php foreach($categorias as $categoria) { ?>
                    <?php if($categoria->objetivo_id == $objetivo->id) { ?>
                        <?= $this->Html->image('categorias-filtro/'.$categoria->id.'.jpg', ['class' => 'imagem-categoria imagem-categoria-'.$categoria->id, 'alt' => $categoria->name]) ?>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <hr>
    <div class="dropdown">
           <button class="dropbtn btn-mais-vendidos">Mais vendidos</button>
    </div> 
    <hr>
    <div class="dropdown dropdown-promocoes">
           <button class="dropbtn btn-promocoes">Promoções</button>
    </div> 
</div>

<script>
    // abrir filtro mobile e tablet
    $('#filtro-mobile-btn, .filtro-navbar-btn').click(function(){
        $('#filtro-mobile-btn').animate({'right' : '-80px'});
        $('.overlay-filtro').fadeIn();
        setTimeout(function() {
            $('.total-filtro').animate({'left' : '0'}, 300);
            $('.fechar-overlay-filtro').fadeIn();
        }, 300);
    });

    $(document).on('click', '.button-view-filtro', function() {
        $('.overlay-filtro').fadeIn();
        $(this).addClass('filtro-aberto');
        setTimeout(function() {
            $('.total-filtro').animate({'left' : '0'}, 300);
            $('.fechar-overlay-filtro').fadeIn();
        }, 300);
    });
    //  ao clicar no filtro, não fechar overlay
    // $('.total-filtro').click(function(e){
    //     e.stopPropagation();
    // });
    // fechar filtro mobile e tablet
    $('.overlay-filtro').click(function(){
        $('.total-filtro').animate({'left' : '-600px'}, 300);
        $('#filtro-mobile-btn').animate({'right' : '10px'});
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {

        var imagens = $('.imagem-objetivo, .imagem-categoria, .imagem-subcategoria');

        $('.overlay-filtro').click(function() {
            $(this).fadeOut();
            $('body').removeClass('total-block');
            $('.fechar-overlay-filtro').fadeOut();

            $('.categorias-objetivo').removeClass('categoria-objetivo-visible');
            $('.dropdown-marcas').fadeOut();
            $('.dropbtn').removeClass('dropdown-active');
        });

        if(($(document).width() < 993) || (view_produto = 1) ) {
            $('.fechar-overlay-filtro').click(function() {
                $(this).fadeOut();
                $('.overlay-filtro').fadeOut();
                $('body').removeClass('total-block');
                $('.categorias-objetivo').removeClass('categoria-objetivo-visible');
                $('.dropdown-marcas').fadeOut();
                $('.dropbtn').removeClass('dropdown-active');
                $('.total-filtro').animate({'left' : '-600px'}, 300);
                $('#filtro-mobile-btn').animate({'right' : '10px'});
            });
        } else {
            $('.fechar-overlay-filtro').click(function() {
                $(this).fadeOut();
                $('.overlay-filtro').fadeOut();
                $('body').removeClass('total-block');
                $('.categorias-objetivo').removeClass('categoria-objetivo-visible');
                $('.dropdown-marcas').fadeOut();
                $('.dropbtn').removeClass('dropdown-active');
            });
        }

        $('.marcas-btn, .objetivo-btn, .categoria-btn , .subcategoria-btn').click(function() {
            $('.overlay-filtro').fadeOut();
            $('.fechar-overlay-filtro').fadeOut();
            $('body').removeClass('total-block');
        })

        $(document).on('click', '.voltar-categoria, .voltar-marca', function() {
            $('.dropdown-active').click();
        });

        $(document).on('click', '.voltar-subcategoria', function() {
            $('.subcategorias').removeClass('subcategorias-visible');
        });

        if($(document).width() > 992) {
            $('.objetivo-link').click(function() {
                var objetivo_id = $(this).attr('data-id');
                var categorias_div = $('.categorias-objetivo-'+objetivo_id);

                $('.dropdown-marcas').fadeOut();
                $('.subcategorias').removeClass('subcategorias-visible');
                
                imagens.removeClass('image-visible');

                $('.imagem-objetivo-'+objetivo_id).addClass('image-visible');

                if($(this).hasClass('dropdown-active')) {
                    $(this).removeClass('dropdown-active');
                } else {
                    $('.dropbtn').not(this).removeClass('dropdown-active');
                    $(this).addClass('dropdown-active');
                }

                if(categorias_div.hasClass('categoria-objetivo-visible')) {
                    $('.overlay-filtro').fadeOut();
                    $('.fechar-overlay-filtro').fadeOut();
                    $('body').removeClass('total-block');
                    
                    categorias_div.removeClass('categoria-objetivo-visible');
                } else {
                    $('.overlay-filtro').fadeIn();
                    $('.fechar-overlay-filtro').fadeIn();
                    $('body').addClass('total-block');

                    $('.categorias-objetivo').removeClass('categoria-objetivo-visible');
                    categorias_div.addClass('categoria-objetivo-visible');
                }
            });
        } else {
            $('.objetivo-link').click(function() {
                var objetivo_id = $(this).attr('data-id');
                var categorias_div = $('.categorias-objetivo-'+objetivo_id);

                $('.subcategorias').removeClass('subcategorias-visible');
                
                if($(this).hasClass('dropdown-active')) {
                    $(this).removeClass('dropdown-active');
                } else {
                    $('.dropbtn').not(this).removeClass('dropdown-active');
                    $(this).addClass('dropdown-active');
                }

                if(categorias_div.hasClass('categoria-objetivo-visible')) {
                    categorias_div.removeClass('categoria-objetivo-visible');
                } else {
                    $('.categorias-objetivo').removeClass('categoria-objetivo-visible');
                    categorias_div.addClass('categoria-objetivo-visible');
                }
            });
        }

        if($(document).width() > 992) {
            $('.marcas-link').click(function() {
                $('.subcategorias').removeClass('subcategorias-visible');
                $('.categorias-objetivo').removeClass('categoria-objetivo-visible');

                $('.marca-image-filtro').removeClass('marca-image-visible');
                $('.marca-image-filtro:first-of-type').addClass('marca-image-visible');

                if($(this).hasClass('dropdown-active')) {
                    $(this).removeClass('dropdown-active');
                } else {
                    $('.dropbtn').not(this).removeClass('dropdown-active');
                    $(this).addClass('dropdown-active');
                }

                if($('.dropdown-marcas').is(':visible')) {
                    $('.overlay-filtro').fadeOut();
                    $('.fechar-overlay-filtro').fadeOut();
                    $('body').removeClass('total-block');
                } else {
                    $('.overlay-filtro').fadeIn();
                    $('.fechar-overlay-filtro').fadeIn();
                    $('body').addClass('total-block');
                }

                $('.dropdown-marcas').fadeToggle();
            });
        } else {
            $('.marcas-link').click(function() {
                $('.subcategorias').removeClass('subcategorias-visible');
                $('.categorias-objetivo').removeClass('categoria-objetivo-visible');

                if($(this).hasClass('dropdown-active')) {
                    $(this).removeClass('dropdown-active');
                } else {
                    $('.dropbtn').not(this).removeClass('dropdown-active');
                    $(this).addClass('dropdown-active');
                }

                $('.dropdown-marcas').fadeToggle();
            });
        }


        $('.marcas-btn').hover(function() {
            var slug = $(this).attr('data-id');

            $('.marca-image-filtro').not('.marca-image-'+slug).removeClass('marca-image-visible');
            $('.marca-image-'+slug).addClass('marca-image-visible');
        });


        $('.categoria-link').click(function() {
            var categoria_id = $(this).attr('data-id');
            var subcategoria_div = $('.subcategorias-'+categoria_id);

            imagens.removeClass('image-visible');

            if($(this).hasClass('categoria-clicked')) {
                $(this).removeClass('categoria-clicked');
            } else {
                $('.categoria-link').not(this).removeClass('categoria-clicked');
                $(this).addClass('categoria-clicked');
            }

            $('.imagem-categoria-'+categoria_id).addClass('image-visible');
            //$('.imagem-categoria-sub-'+categoria_id).addClass('image-visible');

            if(subcategoria_div.hasClass('subcategorias-visible')) {
                subcategoria_div.removeClass('subcategorias-visible');
            } else {
                $('.subcategorias').removeClass('subcategorias-visible');
                subcategoria_div.addClass('subcategorias-visible');
            }
        });


        $('.objetivo-btn').hover(function() {
            var objetivo_id = $(this).attr('image-id');
            var objetivo_imagem = $('.imagem-objetivo-'+objetivo_id);

            $('.imagem-categoria, .imagem-objetivo').removeClass('image-visible');

            objetivo_imagem.addClass('image-visible');
        });


        $('.categoria-link').hover(function() {
            var categoria_id = $(this).attr('data-id');
            var categoria_imagem = $('.imagem-categoria-'+categoria_id);

            $('.imagem-categoria, .imagem-objetivo, .imagem-subcategoria, .imagem-categoria-sub').removeClass('image-visible');

            categoria_imagem.addClass('image-visible');
        });


        $('.categoria-btn').hover(function() {
            var categoria_id = $(this).attr('image-id');
            var categoria_imagem = $('.imagem-categoria-'+categoria_id);

            $('.imagem-categoria, .imagem-objetivo, .imagem-subcategoria, .imagem-categoria-sub').removeClass('image-visible');

            categoria_imagem.addClass('image-visible');
        });


        $('.categoria-ver-todos').hover(function() {
            var categoria_id = $(this).attr('image-id');
            var categoria_imagem = $('.imagem-categoria-sub-'+categoria_id);

            imagens.removeClass('image-visible');

            categoria_imagem.addClass('image-visible');
        });


        $('.subcategoria-btn').hover(function() {
            var subcategoria_id = $(this).attr('image-id');
            var subcategoria_imagem = $('.imagem-subcategoria-'+subcategoria_id);

            imagens.removeClass('image-visible');
            $('.imagem-categoria-sub').removeClass('image-visible');

            subcategoria_imagem.addClass('image-visible');
        });
    });
</script>