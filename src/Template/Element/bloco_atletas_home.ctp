<div>
    <div class="col-md-3 col-xs-3 text-center informacoes-home atleta">
        <?= $this->Html->link(
            $this->Html->image('atletas.png', ['alt' => 'Atletas']),
            '/atletas',
            ['escape' => false]
        )?>
        <p><span class="titulo">ATLETAS</span></p>
        <p class="info">
                <span>
                    Oficiais e não oficias
                </span>
        </p>
    </div>
</div>