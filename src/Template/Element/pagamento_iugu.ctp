<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 total-pagamentos">
    <ul class="nav nav-tabs">
        <li class="active">
            <a data-toggle="tab" href="#credit_card">
                <i class="fa fa-credit-card fa-2x"></i>
                <br class="clear">
                Cartão de
                <br class="clear">
                crédito
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#boleto_card">
                <i class="fa fa-barcode fa-2x"></i>
                <br class="clear">
                Boleto
                <br class="clear">
                &nbsp
            </a>
        </li>

        <?php if($RetiradaLoja == 601) { ?>
            <li>
                <a data-toggle="tab" href="#money_card">
                    <i class="fa fa-money fa-2x"></i>
                    <br class="clear">
                    Pagar na
                    <br class="clear">
                    loja
                </a>
            </li>
        <?php } ?>
    </ul>
</div>
<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 total-tabs">
    <div class="tab-content">
        <div id="credit_card" class="tab-pane fade in active">
            <?= $this->Form->create(null, ['class' => 'credit-card-form'])?>
            <div class="row">
                <div class="col-xs-12 bandeiras">
                    <ul>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="visa"> <?= $this->Html->image('bandeiras/visa.png');  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="mastercard"> <?= $this->Html->image('bandeiras/mastercard.png')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="elo"> <?= $this->Html->image('bandeiras/elo.png')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="dinners"> <?= $this->Html->image('bandeiras/dinners.png')  ?>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="amex"> <?= $this->Html->image('bandeiras/american_express.jpg')  ?>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10 col-sm-4 col-md-4 form-group">
                    <?= $this->Form->input(null, [
                        'div'           => false,
                        'label'         => 'Número do cartão',
                        'class'         => 'form-control credit-card-obrigatorio credit-card-numero card-number-mask',
                        'placeholder'   => 'Ex: 0000 0000 0000 0000',
                    ])?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-3 col-md-2 form-group">
                    <label>Validade</label>
                    <select class="form-control credit-card-obrigatorio credit-card-validade-mes" autocomplete="off">
                        <option></option>
                        <?php for($m = 1; $m <= 12; $m++) { ?>
                            <option value="<?= str_pad($m, 2, 0, STR_PAD_LEFT) ?>"><?= str_pad($m, 2, 0, STR_PAD_LEFT) ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-2 form-group">
                    <label for="">&nbsp</label>
                    <select class="form-control credit-card-obrigatorio credit-card-validade-ano" autocomplete="off">
                        <option></option>
                        <?php for($m = 0; $m < 30; $m++) { ?>
                            <option value="<?= $m + date('Y') ?>"><?= $m + date('Y') ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-xs-10 col-sm-3 col-md-3 form-group">
                    <?= $this->Form->input(null, [
                        'div'           => false,
                        'label'         => 'Código de Segurança',
                        'class'         => 'form-control credit-card-obrigatorio cvv-mask credit-card-cvv',
                        'placeholder'   => '000',
                        'maxlength'     =>  '4',
                    ])?>
                </div>
                <div class="col-xs-2 form-group div-card">
                    <label for="">&nbsp</label>
                    <span style=" margin-left: 5px;"><i class="fa fa-credit-card fa-2x"></i></span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 form-group">
                    <?= $this->Form->input(null, [
                        'div'           => false,
                        'label'         => 'Nome impresso no cartão',
                        'class'         => 'form-control credit-card-obrigatorio credit-card-titular',
                        'placeholder'   => 'Nome Sobrenome',
                    ])?>
                </div>
                <div class="col-xs-12 col-sm-6 form-group">
                    <?= $this->Form->input(null, [
                        'div'           => false,
                        'label'         => 'CPF do titular',
                        'class'         => 'form-control credit-card-obrigatorio cpf-mask credit-card-cpf',
                        'placeholder'   => '000.000.000-00',
                    ])?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 form-group">
                    <?= $this->Form->input(null, [
                        'div'           => false,
                        'label'         => 'Nascimento',
                        'class'         => 'form-control credit-card-obrigatorio data-mask credit-card-nascimento',
                        'placeholder'   => 'dd/mm/aa',
                    ])?>
                </div>
                <div class="col-xs-12 col-sm-6 form-group">
                    <?= $this->Form->input(null, [
                        'div'           => false,
                        'label'         => 'Telefone',
                        'class'         => 'form-control credit-card-obrigatorio phone-mask credit-card-telefone',
                        'placeholder'   => '(00) 00000-0000'
                    ])?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 form-group">
                    <label>Número de parcelas</label>
                    <select class="form-control credit-card-obrigatorio credit-card-parcelas">
                        <option class="option-vista" data-id="<?= number_format($PedidoCliente->valor, 2, '', '') ?>" value="1">1x de R$<?= number_format($PedidoCliente->valor, 2, ',', '.') ?> (À Vista)</option>
                        <?php 
                            $valor_juros = $PedidoCliente->valor * 0.089; 
                            $valor_juros = ($PedidoCliente->valor + $valor_juros); 
                        ?>
                        <option class="option-parcelado" data-id="<?= number_format($valor_juros, 2, '', '') ?>" value="2">2x de R$<?= number_format($valor_juros/2, 2, ',', '.').' = R$'.number_format($valor_juros, 2, ',', '.') ?></option>
                        <?php if($PedidoCliente->valor > 100.00) { ?>
                            <option class="option-parcelado" data-id="<?= number_format($valor_juros, 2, '', '') ?>" value="3">3x de R$<?= number_format($valor_juros/3, 2, ',', '.').' = R$'.number_format($valor_juros, 2, ',', '.') ?></option>
                        <?php } if($PedidoCliente->valor > 200.00) { ?>
                            <option class="option-parcelado" data-id="<?= number_format($valor_juros, 2, '', '') ?>" value="4">4x de R$<?= number_format($valor_juros/4, 2, ',', '.').' = R$'.number_format($valor_juros, 2, ',', '.') ?></option>
                            <option class="option-parcelado" data-id="<?= number_format($valor_juros, 2, '', '') ?>" value="5">5x de R$<?= number_format($valor_juros/5, 2, ',', '.').' = R$'.number_format($valor_juros, 2, ',', '.') ?></option>
                        <?php } if($PedidoCliente->valor > 300.00) { ?>
                            <option class="option-parcelado" data-id="<?= number_format($valor_juros, 2, '', '') ?>" value="6">6x de R$<?= number_format($valor_juros/6, 2, ',', '.').' = R$'.number_format($valor_juros, 2, ',', '.') ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6 form-group">
                    <p><strong>Resumo de compra</strong></p>
                    <?php if($cupom_invalido) { ?>
                        <p style="color: red">Cupom inválido. Já utilizado por esse CPF</p>
                    <?php } else if($cupom_valido) { ?>
                        <p style="color: green">Cupom de desconto aplicado! =)</p>
                    <?php } ?>
                    <?php if ($DescontoCombo != null) { ?>
                        <p>Desconto combo: <span class="pull-right">R$ <?= number_format(($PedidoCliente->valor / 0.95) * 0.05, 2, ',', '.') ?></span></p>
                    <?php } ?>
                    <p>Total dos produtos: <span class="pull-right">R$ <?= number_format($PedidoCliente->valor, 2, ',', '.') ?></span></p>
                    <p>Frete grátis: <span class="pull-right">R$ 0,00</span></p>

                    <?php if($RetiradaLoja == 601) { ?>
                        <p class="show-endereco">Entrega: <span class="pull-right"><?= $loja_retirada->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                        <p class="detalhe-endereco"><?= $loja_retirada->address." - ".$loja_retirada->number." - ".$loja_retirada->area ?><br>
                            <?=  $loja_retirada->city->name." - ".$loja_retirada->city->uf  ?><br>
                            <?= $loja_retirada->phone ?>
                        </p>
                    <?php } elseif ($RetiradaLoja == 10) { ?>
                        <p class="show-endereco">Entrega: <span class="pull-right"><?= $PedidoCliente->academia->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                        <p class="detalhe-endereco"><?= $PedidoCliente->academia->address." - ".$PedidoCliente->academia->number." - ".$PedidoCliente->academia->area ?><br>
                            <?=  $PedidoCliente->academia->city->name." - ".$PedidoCliente->academia->city->uf  ?><br>
                            <?= $PedidoCliente->academia->phone ?>
                        </p>
                        
                    <?php } elseif ($RetiradaLoja == 20) { ?>
                        <p class="show-endereco">Entrega: <span class="pull-right"><?= $PedidoCliente->cliente->name ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                        <p class="detalhe-endereco"><?= $PedidoCliente->cliente->address." - ".$PedidoCliente->cliente->number." - ".$PedidoCliente->cliente->area ?><br>
                            <?=  $PedidoCliente->cliente->city->name." - ".$PedidoCliente->cliente->city->uf  ?><br>
                            <?= $PedidoCliente->cliente->mobile ?>
                        </p>
                    <?php } ?>
                    
                    <?= json_encode($RetiradaLoja) ?>

                    <p>Membro do time: <span class="pull-right"><?= $PedidoCliente->professore->name ?></span></p>
                </div>
            </div>
            <input type="hidden" class="credit-card-pedido" value="<?= $PedidoCliente->id ?>">
            <input type="hidden" class="credit-card-aluno" value="<?= $PedidoCliente->cliente->name ?>">
            <input type="hidden" class="credit-card-token">
            <div class="row"> 
                <div class="col-xs-12 form-group text-center">
                    <button type="button" class="btn btn-verde credit-card-btn">
                        <i class="fa fa-credit-card-alt"></i> pagar
                    </button>
                    <br>
                </div>
            </div>
            <?= $this->Form->end()?>
        </div>

            <div id="boleto_card" class="tab-pane fade text-left">
                <div class="row">
                    <?= $this->Form->create(null, ['class' => 'boleto-form']) ?>
                        <div class="col-xs-12 col-md-6 text-center form-group">
                            <br>
                            <br>
                            <button type="submit" class="btn btn-verde boleto-btn">
                                <i class="fa fa-barcode"></i>
                                Gerar Boleto
                            </button>
                        </div>
                        <input type="hidden" class="boleto-pedido" value="<?= $PedidoCliente->id ?>">
                        <input type="hidden" class="boleto-valor" value="<?= number_format($PedidoCliente->valor, 2, '', '') ?>">
                    <?= $this->Form->end() ?>
                    <div class="col-xs-12 col-md-6 form-group">
                        <p><strong>Resumo de compra</strong></p>
                        <?php if($cupom_invalido) { ?>
                            <p style="color: red">Cupom inválido. Já utilizado por esse CPF</p>
                        <?php } else if($cupom_valido) { ?>
                            <p style="color: green">Cupom de desconto aplicado! =)</p>
                        <?php } ?>
                        <?php if ($DescontoCombo != null) { ?>
                        <p>Desconto combo: <span class="pull-right">R$ <?= number_format(($PedidoCliente->valor / 0.95) * 0.05, 2, ',', '.') ?></span></p>
                        <?php } ?>
                        <p>Total dos produtos: <span class="pull-right">R$ <?= number_format($PedidoCliente->valor, 2, ',', '.') ?></span></p>
                        <p>Frete grátis: <span class="pull-right">R$ 0,00</span></p>

                    <?php if($RetiradaLoja == 601) { ?>
                        <p class="show-endereco">Entrega: <span class="pull-right"><?= $loja_retirada->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                        <p class="detalhe-endereco"><?= $loja_retirada->address." - ".$loja_retirada->number." - ".$loja_retirada->area ?><br>
                            <?=  $loja_retirada->city->name." - ".$loja_retirada->city->uf  ?><br>
                            <?= $loja_retirada->phone ?>
                        </p>
                    <?php } elseif ($RetiradaLoja == 10) { ?>
                        <p class="show-endereco">Entrega: <span class="pull-right"><?= $PedidoCliente->academia->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                        <p class="detalhe-endereco"><?= $PedidoCliente->academia->address." - ".$PedidoCliente->academia->number." - ".$PedidoCliente->academia->area ?><br>
                            <?=  $PedidoCliente->academia->city->name." - ".$PedidoCliente->academia->city->uf  ?><br>
                            <?= $PedidoCliente->academia->phone ?>
                        </p>
                        
                    <?php } elseif ($RetiradaLoja == 20) { ?>
                        <p class="show-endereco">Entrega: <span class="pull-right"><?= $PedidoCliente->cliente->name ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                        <p class="detalhe-endereco"><?= $PedidoCliente->cliente->address." - ".$PedidoCliente->cliente->number." - ".$PedidoCliente->cliente->area ?><br>
                            <?=  $PedidoCliente->cliente->city->name." - ".$PedidoCliente->cliente->city->uf  ?><br>
                            <?= $PedidoCliente->cliente->mobile ?>
                        </p>
                    <?php } ?>

                        <p>Membro do time: <span class="pull-right"><?= $PedidoCliente->professore->name ?></span></p>
                    </div>
                </div>
                <p><strong>Data de vencimento:</strong> verifique a data de vencimento do boleto que é de 3(três) dias após ser gerado. Caso não seja pago até a data informada o pedido será automaticamente cancelado.</p>
                <p><strong>Prazo de entrega:</strong> é contado a partir da confirmação de pagamento pelo banco. A confirmação pode levar até 2 dias úteis.</p>
                <p><strong>Pagamento:</strong> pode ser feito pela internet e aplicativo do seu banco utilizando o código de barras ou diretamente em bancos, lotéricas e correios com a apresentação o boleto impresso.</p>
                <h4>ATENÇÃO</h4>
                <ul>
                    <li>Não será enviada uma cópia impressa do boleto para seu endereço.</li>
                    <li>Desabilite o recurso anti pop-up caso você use.</li>
                </ul>
                <br>
                <br>
            </div>
        <?php if($RetiradaLoja == 601) { ?>
            <div id="money_card" class="tab-pane fade text-left">
                <div class="row">
                    <?= $this->Form->create(null, ['url' => ['controller' => 'produtos', 'action' => 'pagar_dinheiro'], 'class' => 'pagar-loja-form']) ?>
                        <div class="col-xs-12 col-md-6 text-center form-group">
                            <h5>Clique no botão para separarmos seu pedido do estoque e você retirá-lo com a gente.</h5>
                            <br>
                            <button type="submit" class="btn btn-verde">
                                <i class="fa fa-money"></i>
                                Pagar na loja
                            </button>
                        </div>
                        <input type="hidden" name="pedido_id" value="<?= $PedidoCliente->id ?>">
                    <?= $this->Form->end() ?>
                    <div class="col-xs-12 col-md-6 form-group">
                        <p><strong>Resumo de compra</strong></p>
                        <?php if($cupom_invalido) { ?>
                            <p style="color: red">Cupom inválido. Já utilizado por esse CPF</p>
                        <?php } else if($cupom_valido) { ?>
                            <p style="color: green">Cupom de desconto aplicado! =)</p>
                        <?php } ?>
                        <?php if ($DescontoCombo != null) { ?>
                        <p>Desconto combo: <span class="pull-right">R$ <?= number_format(($PedidoCliente->valor / 0.95) * 0.05, 2, ',', '.') ?></span></p>
                        <?php } ?>
                        <p>Total dos produtos: <span class="pull-right">R$ <?= number_format($PedidoCliente->valor, 2, ',', '.') ?></span></p>
                        <p>Frete grátis: <span class="pull-right">R$ 0,00</span></p>
                        <?php if($RetiradaLoja) { ?>
                            <p class="show-endereco">Entrega: <span class="pull-right"><?= $loja_retirada->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                            <p class="detalhe-endereco"><?= $loja_retirada->address." - ".$loja_retirada->number." - ".$loja_retirada->area ?><br>
                                <?=  $loja_retirada->city->name." - ".$loja_retirada->city->uf  ?><br>
                                <?= $loja_retirada->phone ?>
                            </p>
                        <?php } else { ?>
                            <p class="show-endereco">Entrega: <span class="pull-right"><?= $PedidoCliente->academia->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                            <p class="detalhe-endereco"><?= $PedidoCliente->academia->address." - ".$PedidoCliente->academia->number." - ".$PedidoCliente->academia->area ?><br>
                                <?=  $PedidoCliente->academia->city->name." - ".$PedidoCliente->academia->city->uf  ?><br>
                                <?= $PedidoCliente->academia->phone ?>
                            </p>
                        <?php } ?>
                        <p>Membro do time: <span class="pull-right"><?= $PedidoCliente->professore->name ?></span></p>
                    </div>
                </div>
                <br>
                <br>
            </div>
        <?php } ?>
    </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(document).on('click', '.credit-card-btn', function() {
            var btn = $(this);
            var campos_obrigatorios = $('.credit-card-obrigatorio');
            var bandeira = $('input[name=credit-card-radio-bandeira]:checked').val();

            var numero_cartao = $('.credit-card-numero').val();
                numero_cartao = numero_cartao.replace(/\s/g, '');
            var validade_mes = $('.credit-card-validade-mes option:selected').val();
            var validade_ano = $('.credit-card-validade-ano option:selected').val();
            var validade = validade_mes+'/'+validade_ano;
            var codigo_cvv = $('.credit-card-cvv').val();
            var titular_cartao = $('.credit-card-titular').val();
            var cpf_cartao = $('.credit-card-cpf').val();
            var nascimento_cartao = $('.credit-card-nascimento').val();
            var telefone_cartao = $('.credit-card-telefone').val();
            var parcelas = $('.credit-card-parcelas option:selected').val();
            var valor_total = $('.credit-card-parcelas option:selected').attr('data-id');
            var pedido_id = $('.credit-card-pedido').val();
            var comprador = $('.credit-card-aluno').val();

            loading.fadeIn();
            
            var campos_nulos = 0;

            $('.alert-error').remove();

            campos_obrigatorios.each(function() {
                if($(this).val() == '' || $(this).val() == null) {
                    campos_nulos = 1;
                }
            });

            if(bandeira == '' || bandeira == null) {
                campos_nulos = 1;
            }

            if(campos_nulos == 1) {
                btn.before('<p class="alert-error" style="color: red">Preencha todos os dados!</p>');
                loading.fadeOut();
            }

            if(campos_nulos != 1) {
                
                var cartao_valido = 1;

                if(!Iugu.utils.validateCreditCardNumber(numero_cartao)) {
                    cartao_valido = 0;

                    btn.before('<p class="alert-error" style="color: red">Número do cartão inválido!</p>');
                    loading.fadeOut();
                }

                if(!Iugu.utils.validateCVV(codigo_cvv, bandeira)) {
                    cartao_valido = 0;

                    btn.before('<p class="alert-error" style="color: red">CVV inválido!</p>');
                    loading.fadeOut();
                }

                if(!Iugu.utils.validateExpiration(validade_mes, validade_ano)) {
                    cartao_valido = 0;
                    
                    btn.before('<p class="alert-error" style="color: red">Data de validade inválida!</p>');
                    loading.fadeOut();
                }

                if(Iugu.utils.getBrandByCreditCardNumber(numero_cartao) != bandeira) {
                    cartao_valido = 0;
                    btn.before('<p class="alert-error" style="color: red">Número do cartão ou bandeira inválido!</p>');
                    loading.fadeOut();
                }

                if(cartao_valido) {
                    var card_token = $(".credit-card-token").val();

                    Iugu.setAccountID('6F4F21663CD44586AA13CC4C648CDB51');
                    
                    Iugu.setTestMode(true);

                    var tokenResponseHandler = function(data) {
                        if (data.errors) {
                            console.log(data.errors);
                            alert("Erro salvando cartão: " + JSON.stringify(data.errors));
                        } else {
                            $(".credit-card-token").val(data.id);
                            var card_token = $(".credit-card-token").val();
                        }

                        $.ajax({
                            type: "POST",
                            url: WEBROOT_URL + 'pagar/iugu/credito',
                            data: {
                                card_titular: titular_cartao,
                                card_bandeira: bandeira,
                                card_token: card_token,
                                card_cpf: cpf_cartao,
                                card_nascimento: nascimento_cartao,
                                card_telefone: telefone_cartao,
                                parcelas: parcelas,
                                valor_total: valor_total,
                                pedido_id: pedido_id,
                                comprador: comprador
                            }
                        })
                            .done(function (data) {
                                
                                loading.fadeOut();
                                
                                if(data == 0) {
                                    btn.before('<p class="alert-error" style="color: red">Pagamento não autorizado.</p><br><p class="alert-error" style="color: red">Confira os dados, tente novamente e se o erro persistir, contate a emissora do seu cartão.</p>');

                                    campos_obrigatorios.each(function() {
                                        $(this).val('');
                                    });
                                }

                                if(data == 1) {
                                    location.href = WEBROOT_URL + 'mochila/finalizado/' + pedido_id;
                                }
                            });
                    }

                    titular_cartao;
                    nomes = [];

                    nomes.push( titular_cartao.split(' ')[0] );
                    nomes.push( titular_cartao.substring(nomes[0].length + 1) );

                    cc = Iugu.CreditCard(numero_cartao, validade_mes, validade_ano, nomes[0], nomes[1], codigo_cvv);
                     
                    Iugu.createPaymentToken(cc, tokenResponseHandler);
                }
               
            }

            return false;
        });

        $(document).on('click', '.boleto-btn', function() {
            var btn = $(this);

            var pedido_id = $('.boleto-pedido').val();
            var valor_total = $('.boleto-valor').val();

            loading.fadeIn();

            $('.alert-error').remove();

            $.ajax({
                type: "POST",
                url: WEBROOT_URL + 'pagar/iugu/boleto',
                data: {
                    valor_total: valor_total,
                    pedido_id: pedido_id
                }
            })
                .done(function (data) {
                    
                    loading.fadeOut();
                    
                    if(data == 0) {
                        btn.before('<p class="alert-error" style="color: red">Falha ao gerar boleto. Tente novamente!</p>');
                    }

                    if(data == 1) {
                        location.href = WEBROOT_URL + 'mochila/finalizado/' + pedido_id;
                    }
                });

            return false;
        });
    });
</script>