<style>
    .filtros-ativos{
        height: auto;
        margin-bottom: 15px;
        padding-bottom: 15px;
    }
    .filtros-ativos span{
        min-width: 100px;
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        background-color: rgba(255,255,255,1.0);
        border-radius: 5px;
        padding: 5px 10px;
        margin: 5px;
        display: inline-block;
        height: 30px;
    }
    .filtros-ativos .fa{
        color: rgb(255, 0, 0);
    }
    .filtros-ativos .fa:hover{
        color: rgb(244, 219, 27);
        cursor: pointer;
    }
    .filtros-ativos i{
        float: right;
        margin-top: 2px;
    }
    .subfiltros{
        margin-bottom: 15px;
        padding: 5px 0;
    }
    .subfiltros select{
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        background-color: rgba(255,255,255,1.0);
        border-radius: 5px;
        padding: 5px 10px;
        margin: 5px 3px;
        border: none;
        display: inline-block;
    }
</style>

<div class="row subfiltro-div">
    <p style="margin-left: 25px;"><strong>você está filtrando por:</strong></p>
    <div class="col-xs-6 filtros-ativos">
        <div style="padding: 5px">
            <span>ativo 1 <i class="fa fa-close"></i></span> <span>ativo 2 <i class="fa fa-close"></i></span><span>ativo 1 <i class="fa fa-close"></i></span> <span>ativo 2 <i class="fa fa-close"></i></span>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="col-xs-12 text-right subfiltros">
            <select name="sabor" id="">
                <option value="">sabor</option>
                <option value="">morango</option>
                <option value="">chocolate</option>
                <option value="">uva</option>
                <option value="">limão</option>
                <option value="">laranja</option>
            </select>
            <select name="ordenar" id="">
                <option value="">ordenar por</option>
                <option value="">menor valor</option>
                <option value="">maior valor</option>
                <option value="">promoções</option>
                <option value="">novidades</option>
                <option value="">estou com sorte</option>
            </select>
            <select name="valor" id="">
                <option value="">valor</option>
                <option value="">R$ 0,00 à R$50,00</option>
                <option value="">R$ 51,00 à R$100,00</option>
                <option value="">R$ 101,00 à R$150,00</option>
                <option value="">R$ mais de R$150,00</option>
            </select>
        </div>
    </div>
</div>