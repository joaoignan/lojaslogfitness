<div class="total-banner">	
	<div class="owl-carousel owl-carousel-banner">
		<?php foreach($banners_inicial as $banner_inicial) { ?>
			<?php if ($banner_inicial->target == 1) { ?>
				<div class="item">
	            	<?= $this->Html->link('<div class="bannerModelo1" style="background-image: url(img/banners-loja/'.$banner_inicial->image.')"></div>', $SSlug.$banner_inicial->link,['escape' => false, 'target' => '_blank']) ?>
	        	</div>
			<?php } else { ?>
				<div class="item">
					<div class="bannerModelo1" style="background-image: url(img/banners-loja/<?= $banner_inicial->image ?>)"></div>
				</div>
			<?php } ?>
		<?php } ?>
	</div>
</div>

<style>
	.total-banner{
		position: absolute;
		left: 0;
		right: 0;
		top: 90px;
	}
	.owl-carousel-banner .owl-wrapper-outer{
		padding: 0!important;
	}
	.bannerModelo1{
		width: 100%;
		height: 700px;
		background-position: center top;
		margin-bottom: 15px;
	}
</style>