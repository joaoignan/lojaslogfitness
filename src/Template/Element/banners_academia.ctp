<style type="text/css">
    #banners_academia_pc_config,
    #banners_academia_mobile_config {
      display: none;
    }
</style>

<div id="banners_academia_pc_config">
<?php
$count_banner = count($banners);
$banner_divider = (int)($count_banner / 2);

if(count($banners_academia) >= 1) {
    $banners_a = $banners_academia;
    ?>
    <div class="col-xs-6">
        <div id="banners_a" style="padding:0;" class="carousel slide col-md-12 col-xs-12 img2-home banners_home" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php
                $i = 0;
                foreach ($banners_a as $banner):
                    $i++;
                    $i == 1 ? $active = 'active' : $active = '';
                    echo '<li data-target="#banners_a" data-slide-to="'.$i.'" class="'.$active.'"></li>';
                endforeach;
                ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php
                $i = 0;
                foreach ($banners_a as $banner):
                    $i++;
                    $i == 1 ? $active = 'active' : $active = '';
                    !empty($banner->link) ? $href = 'href="'.$banner->link.'" target="_blank"' : $href = '';
                    echo '<div class="item '.$active.'">';
                    echo '<a '.$href.' >';
                    echo $this->Html->image('banners/'.$banner->image, [
                        'alt' => $banner->name,
                        'class' => 'col-md-12 col-xs-12 img2-home'
                    ]);
                    echo '</a>';
                    echo '</div>';
                endforeach;
                ?>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#banners_a" role="button" data-slide="prev">
                <span class="fa fa-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="right carousel-control" href="#banners_a" role="button" data-slide="next">
                <span class="fa fa-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Próximo</span>
            </a>
        </div>
    </div>
    <?php
}

if($count_banner >= 1) {
    $banners_b = $banners;
    //if(isset($banners_cliente)){ $banners = $banners_cliente; }
    ?>
    <div class="col-xs-6">
        <div id="banners_b" style="padding:0;" class="carousel slide col-md-12 col-xs-12 img2-home banners_home" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php
                $k = 0;
                foreach ($banners_b as $banner):
                    $i++;
                    $k++;
                    $k == 1 ? $active = 'active' : $active = '';
                    echo '<li data-target="#banners_a" data-slide-to="'.$i.'" class="'.$active.'"></li>';
                endforeach;
                ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php
                $i = 0;
                foreach ($banners_b as $banner):

                    $i++;
                    $i == 1 ? $active = 'active' : $active = '';
                    !empty($banner->link) ? $href = 'href="'.$banner->link.'" target="_blank"' : $href = '';
                    echo '<div class="item '.$active.'">';
                    echo '<a '.$href.' >';
                    echo $this->Html->image('banners/'.$banner->image, [
                        'alt' => $banner->name,
                        'class' => 'col-md-12 col-xs-12 img2-home'
                    ]);
                    echo '</a>';
                    echo '</div>';
                endforeach;
                ?>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#banners_b" role="button" data-slide="prev">
                <span class="fa fa-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="right carousel-control" href="#banners_b" role="button" data-slide="next">
                <span class="fa fa-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Próximo</span>
            </a>
        </div>
    </div>
    <?php
}
?>
</div>





<div id="banners_academia_mobile_config">
<?php
$count_banner = count($banners);

if(count($banners_academia) >= 1) {
    $banners_a = $banners_academia;
    $banners_b = $banners;
    ?>
    <div class="col-xs-12">
        <div id="banners_a" style="padding:0;" class="carousel slide col-md-12 col-xs-12 img2-home banners_home" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php
                $i = 0;
                foreach ($banners_a as $banner):
                    $i++;
                    $i == 1 ? $active = 'active' : $active = '';
                    echo '<li data-target="#banners_a" data-slide-to="'.$i.'" class="'.$active.'"></li>';
                endforeach;
                if($count_banner >= 1) {
                    foreach ($banners_b as $banner):
                        $i++;
                        $i == 1 ? $active = 'active' : $active = '';
                        echo '<li data-target="#banners_a" data-slide-to="'.$i.'" class="'.$active.'"></li>';
                    endforeach;
                }
                ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php
                $i = 0;
                foreach ($banners_a as $banner):
                    $i++;
                    $i == 1 ? $active = 'active' : $active = '';
                    !empty($banner->link) ? $href = 'href="'.$banner->link.'" target="_blank"' : $href = '';
                    echo '<div class="item '.$active.'">';
                    echo '<a '.$href.' >';
                    echo $this->Html->image('banners/'.$banner->image, [
                        'alt' => $banner->name,
                        'class' => 'col-md-12 col-xs-12 img2-home'
                    ]);
                    echo '</a>';
                    echo '</div>';
                endforeach;
                if($count_banner >= 1) {
                    foreach ($banners_b as $banner):
                        $i++;
                        $i == 1 ? $active = 'active' : $active = '';
                        !empty($banner->link) ? $href = 'href="'.$banner->link.'" target="_blank"' : $href = '';
                        echo '<div class="item '.$active.'">';
                        echo '<a '.$href.' >';
                        echo $this->Html->image('banners/'.$banner->image, [
                            'alt' => $banner->name,
                            'class' => 'col-md-12 col-xs-12 img2-home'
                        ]);
                        echo '</a>';
                        echo '</div>';
                    endforeach;
                }
                ?>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#banners_a" role="button" data-slide="prev">
                <span class="fa fa-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="right carousel-control" href="#banners_a" role="button" data-slide="next">
                <span class="fa fa-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Próximo</span>
            </a>
        </div>
    </div>
    <?php
} ?>
</div>


<script type="text/javascript">
  $(document).ready(function() {
    var medida_tela = $(window).width();

    if(medida_tela <= 550) {
        $('#banners_academia_pc_config').remove();
        $('#banners_academia_mobile_config').show();
    } else {
        $('#banners_academia_pc_config').show();
        $('#banners_academia_mobile_config').remove();
    }
  });
</script>