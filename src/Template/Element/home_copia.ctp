<?php
    /*function reduzir_tamanho_img($src, $quality) {
        $img = new Imagick();
        $img->readImage($src);
        $img = $img->flattenImages();
        $img->setImageCompression(Imagick::COMPRESSION_JPEG);
        $img->setImageCompressionQuality($quality);
        $img->stripImage();
        $img->writeImage($src); 
    }

    foreach($produtos as $prod_img) {
        $foto_prod = unserialize($prod_img->fotos);
        reduzir_tamanho_img('img/produtos/sm-'.$foto_prod[0], 40);
    }

    foreach($banners as $banners_img) {
        reduzir_tamanho_img('img/banners/'.$banners_img->image, 70);
    }

    foreach($banners_academia as $banners_acad_img) {
        reduzir_tamanho_img('img/banners/'.$banners_acad_img->image, 70);
    }

    foreach($banners_cliente as $banners_clientes_img) {
        reduzir_tamanho_img('img/banners/'.$banners_clientes_img->image, 70);
    }*/
?>

<style>
    /*@media all and (min-width: 1440px){
        .total-produto-box{
            display: inline-block;
            margin: 0 15px;
            width: 20%;
        }
    }
    @media all and (max-width: 1024px){
        .total-produto-box{
            display: inline-block;
            margin: 0 10px;
            width: 25%;
        }
    }
    @media all and (max-width: 768px){
        .total-produto-box{
            display: inline-block;
            margin: 0 10px;
            width: 33%;
        }
    }
    @media all and (max-width: 450px){
        .total-produto-box{
            display: inline-block;
            margin: 0 10px;
            width: 100%;
        }
    }*/
    body{
        font-family: 'Lato', sans-serif!important;
        background-color: #eee!important;
    }
    .btn-compre{
        background: #8dc73f;
    }
    .btn-compre svg{
        fill: white;
        width: 25px;
        height: auto;
    }

    .btn-indique, .produto-acabou-btn{
        background-color: #5089cf;
    }
    .produto-acabou-btn .fa{
        margin-top: -3px;
        color: #fff;
    }
    .btn-box{
        width: 49%;
        padding:5px;
    }
    .btn-box span{
        color: #fff;
        font-size: 12px;
        margin-top: 3px;
    }
    .btn-box img{
        width: 18px;
        margin-right: 5px;
        margin-top: -5px;
    }
    .box-botoes{
        display: inline-block;
        width: 100%;
        padding-top: 5px;
        float: right;
    }
    .div-sabor{
        height: 23px;
        padding: 0;
    }
    .prdt{
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        margin-bottom: 30px;
        padding: 5px 10px;
        min-height: 380px;
    }
    .link-prdt{
        height: 72px;
    }
    #pagar-celular{
        display: none;
    }
    .promocao-index, #grade-produtos {
        padding-right: 230px;
        padding-left: 80px;
        margin-left: 0;
        margin-right: 0;
        width: 100%;
    }
    .product {
        font-size: 14px;
    }
    .produto-item .bg-white-produto {
        min-height: 314px;
        border: 2px solid rgb(234, 234, 234);
        box-shadow: 0px 1px 7px 0px rgba(200,200,200,0.75);
    }
    .bg-gray-produto {
        bottom: 0;
        position: absolute;
        min-height: 92px;
        max-width: 185px;
    }
    .description {
        width: 100%;
        height: 40px;
        overflow: hidden;
        margin: 0 0 5px 0;
        font-size: 14px;
        text-transform: uppercase;
        font-weight: 700;
    }
    @media all and (max-width: 768px) {
        .produto-item .bg-white-produto {
            min-height: 350px;
        }
    }
    @media all and (min-width: 769px) {
        .box-close-filter { 
            width: 0; 
        }
        .filtro { 
            width: 230px; 
            opacity: 1; 
        }
        .box-filtro { 
            left: 0; 
        }
    }
    .produto-grade {
        max-width: 145px;
        max-height: 150px;
        margin-top: 0;
        display: flex;
        align-items: center;
    }
    .box-imagem-produto {
        width: 100%;
        height: 150px;
        margin-top: -15px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .box-info-produto {
        width: 100%;
        height: 200px;
    }
    .box-fundo-produto {
        width: 100%;
        padding: 0;
    }
    .box-preco {
        height: 70px;
        width: 100%;
        float: left;
        display: flex;
        flex-flow: column;
        justify-content: center;
    }
    .box-mochila-add {
        height: 45px;
        width: 45px;
        float: right;
        display: flex;
        align-items: flex-end;
        margin-top: 10px;
    }
    .box-mochila-add svg {
        width: 100%;
        height: 100%;
        cursor: pointer;
        background: #8dc73f;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
        fill: white;
        margin-right: 3px;
    }
    .box-mochila-avise {
        height: 45px;
        width: 45px;
        float: right;
        margin-top: 10px;
        background-color: #5089cf;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
    }
    .box-mochila-avise > .fa{
        color: #FFF;
        font-size: 2.5em;
        margin-bottom: 2px;
        margin-top: 3px;
    }
    .categoria{
        height: 30px;
    }
    .categoria svg{
        position: absolute;
        right: 10px;
        top: 0px;
        width: 20px;
        height: 30px;
    }
    .preco {
        margin: 0;
        font-size: 16px;
    }
    .preco-desc {
        font-size: 16px;
    }
    .preco-normal {
        font-weight: 700;
        font-size: 20px;
        color: #5089cf;
    }
    .bg-white-produto a {
        min-height: 0;
    }
    .box-promocao {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: black;
        background: #f3d012;
    }
    .box-promocao:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-novidade {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: white;
        background: #5089cf;
    }
    .box-novidade:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-ultimo {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: white;
        background: red;
    }
    .box-ultimo:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-mochila-add svg:hover, .box-mochila-add svg:focus, .box-mochila-add svg:link:hover, .box-mochila-add svg:link:focus, .box-mochila-add svg:visited:hover, .box-mochila-add svg:visited:focus, .box-mochila-avise:hover, .btn-box:hover {
      -webkit-transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      -webkit-animation: diff 0.5s 1;
      animation: diff 0.5s 1;
    }

    @-webkit-keyframes diff {
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
    }
    @keyframes diff {
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
    }

    .icone-objetivo {
        position: absolute;
        left: 5px;
        bottom: 120px;
        width: 30px;
        height: 30px;
        color: #5089cf;
    }

    .box-imagem-produto svg {
        position: absolute;
        left: 5px;
        bottom: 130px;
        width: 20px;
        height: 30px;
    }

    .sabor-info {
        font-size: 12px;
        color: #333;
        text-align: center;
        overflow: hidden;
    }

    @media all and (min-width: 1650px) {
        .produto-item {
            width: 16.66666667%;
        }
    }
    @media all and (max-width: 1240px) {
        .produto-item {
            width: 33.3333333334%;
        }
    }
    @media all and (max-width: 1200px) and (min-width: 780px) {
        .form-busca {
            width: 80% !important;
        }
    }
    @media all and (max-width: 1050px) {
        .produto-item {
            width: 50%;
        }
    }
    @media all and (max-width: 850px) {
        .produto-item {
            width: 100%;
        }
    }
    @media all and (max-width: 768px) {
        #pagar-celular{
            display: block;
            margin-bottom: 7px;
        }
        #pagar-celular .btn-login-nav{
            font-size: 18px;
            padding: 6px;
        }
        .promocao-index, #grade-produtos {
            padding-left: 0;
            padding-right: 0;
        }
        .promocao-index {
            padding-top: 90px;
        }
        .produto-item {
            width: 50%;
        }
    }
    @media all and (max-width: 400px) {
        .produto-grade {
            max-width: 120px;
            max-height: 120px;
        }
        .produto-item {
            padding-right: 0px;
            padding-left: 0px;
        }
    }
</style>

<style>
    .loader {
        position: fixed;
        left: 0;
        top: 0%;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: white;
    }
    .content-loader{
        position: fixed;
        border-radius: 25px;
        left: 20%;
        top: 25%;
        width: 60%;
        height: 50%;
        z-index: 999999;
        background-color: white;
        text-align: center;
    }
    .content-loader img{
        width: 500px;
        margin-top: 45px;
    }
    .content-loader i{
        color: #f4d637;
    }
    .content-loader p{
        color: #5087c7;
        font-family: nexa_boldregular;
        font-size: 34px;
    }
    @media all and (max-width: 430px) {
        .content-loader{
            display: none;
        }
    }
    @media all and (max-width: 768px) {
        .loader {
            display: none;
        }
    }
</style>

<?php
if(isset($CLIENTE)){ 
    $client_name = explode(' ', $CLIENTE->name); ?>
    <script>
        var username = '<?= $client_name[0] ?>';
    </script>
<?php } else { ?>
    <script>var username = 0;</script>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.close-filter').dblclick();

        /*if(!window.Notification) {
            console.log('Este browser não suporta Web Notifications!');
            return;
        }

        if (Notification.permission == 'default') {
            Notification.requestPermission(function() {
            });
        } else if (Notification.permission == 'granted') {
            if(username != 0) {
                var notification = new Notification('LOG 2.0', {
                dir: 'auto',
                icon: 'https://instagram.fsjp1-1.fna.fbcdn.net/t51.2885-19/s150x150/14156706_208450462904779_404983613_a.jpg',
                body: 'Logfitness sincera? Vem dar uma olhada!',
                tag : 'log2.0',
                });
            } else {
                var notification = new Notification('LOG 2.0', {
                dir: 'auto',
                icon: 'https://instagram.fsjp1-1.fna.fbcdn.net/t51.2885-19/s150x150/14156706_208450462904779_404983613_a.jpg',
                body: 'Logfitness sincera? Vem dar uma olhada!',
                tag : 'log2.0',
                });
            }

            notification.onshow = function(e) {
            },
            notification.onclick = function(e) {
                $('.ts-modal-trigger').click();
            },
            notification.onclose = function(e) {
            }
            
        } else if (Notification.permission == 'denied') {
        }*/
    });
    /*$(window).load(function() {
        var medida_tela = $(window).width();
        if(medida_tela > 768) {
            $('.close-mochila').click();
        }
    });*/
</script>

<script type="text/javascript">
    $(window).load(function() {
        function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
        }
    });
</script>

<!-- <script>
    $(window).load(function() {
        $('.loader').fadeOut(1500);
    });
</script> -->



<!-- <div class="loader">
    <div class="content-loader">
        <?= $this->Html->image('logfitness.png', ['alt' => 'logfitness'])?><br/><br/>
        <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i><br/><br/>
        <p><strong>Vivemos desafiando os limites, e vc?</strong></p>
    </div>
</div> -->

<!-- "BANNER" DE PROMOÇÕES - IMAGEM PRINCIPAL -->
<!-- <div class="container promocao-index"> -->
    <?php
//    if(count($banners) >= 1 && count($banners_academia) <= 0 && count($banners_cliente) <= 0) {
//        echo $this->Element('banners');
//        //echo $this->Element('banners');
//    }elseif(count($banners) >= 1 && count($banners_academia) >= 1 && count($banners_cliente) <= 0){
//        echo $this->Element('banners_academia');
//        //echo $this->Element('banners_academia');
//    }elseif((count($banners) >= 1 || count($banners_academia) >= 1) && count($banners_cliente) >= 1){
//        echo $this->Element('banners_cliente');
//        //echo $this->Element('banners_cliente');
//    }
    ?>
<!-- </div> -->

<input type="hidden" class="ordenar-hidden" value="&ord=<?= $_GET['ord'] ?>">
<input type="hidden" class="valor-hidden" value="&valor_min=<?= $_GET['valor_min'] ?>&valor_max=<?= $_GET['valor_max'] ?>">
<input type="hidden" class="marca-hidden" value="&m=<?= $_GET['m'] ?>">
<input type="hidden" class="objcat-hidden" value="&oid=<?= $_GET['oid'] ?>&cid=<?= $_GET['cid'] ?>">
<input type="hidden" class="busca-hidden" value="&busca=<?= $_GET['busca'] ?>">

<!-- <script>
    document.ready(function{
        $('#btn-marcas').click()
    })
    
</script> -->

<style>
    .dropdown a{
        color: #fff;
        font-weight: 400;
    }
    .nav .open>a, .nav .open>a:hover, .nav .open>a:focus, .nav>li>a:focus{
        background-color: rgb(66, 118, 181);
        border-color: rgb(66, 118, 181);
    }
    #dropdown-filtro ul{
        padding: 0;
    }
    #dropdown-filtro li{
        padding: 0;
    }
</style>
   
<div class="container container-banners" style="margin-bottom: 30px;">
    
    <div class="owl-carousel-banner">
        <div class="item">
            <?= $this->Html->image('banners/banner_marcas.png') ?>
        </div>
        <div class="item">
            <?= $this->Html->image('banners/banner_marcas.png') ?>
        </div>
        <div class="item">
            <?= $this->Html->image('banners/banner_marcas.png') ?>
        </div>
    </div>
</div>

<style>
    .filtros-ativos{
        height: auto;
        margin-bottom: 15px;
        padding-bottom: 15px;
    }
    .filtros-ativos span{
        min-width: 100px;
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        background-color: rgba(255,255,255,1.0);
        border-radius: 5px;
        padding: 5px 10px;
        margin: 5px;
        display: inline-block;
        height: 30px;
    }
    .filtros-ativos .fa{
        color: rgb(255, 0, 0);
    }
    .filtros-ativos .fa:hover{
        color: rgb(244, 219, 27);
        cursor: pointer;
    }
    .filtros-ativos i{
        float: right;
        margin-top: 2px;
    }
    .subfiltros{
        margin-bottom: 15px;
        padding: 5px 0;
    }
    .subfiltros select{
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        background-color: rgba(255,255,255,1.0);
        border-radius: 5px;
        padding: 5px 10px;
        margin: 5px 3px;
        border: none;
        display: inline-block;
    }
</style>

<div class="container container-grade" data-section="home" style="background-color: rgba(250, 250, 250, 1); padding: 25px 0">
    <div class="col-sm-4 col-md-2 filtro-div mobile-not">
        <?= $this->Element('filtro'); ?>
    </div>
    <div class="col-sm-8 col-md-10 produtos-div">
        <div class="row subfiltro-div">
            <p style="margin-left: 25px;"><strong>você está filtrando por:</strong></p>
            <div class="col-xs-6 filtros-ativos">
                <div style="padding: 5px">
                    <span>ativo 1 <i class="fa fa-close"></i></span> <span>ativo 2 <i class="fa fa-close"></i></span><span>ativo 1 <i class="fa fa-close"></i></span> <span>ativo 2 <i class="fa fa-close"></i></span>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="col-xs-12 text-right subfiltros">
                    <select name="sabor" id="">
                        <option value="">sabor</option>
                        <option value="">morango</option>
                        <option value="">chocolate</option>
                        <option value="">uva</option>
                        <option value="">limão</option>
                        <option value="">laranja</option>
                    </select>
                    <select name="ordenar" id="">
                        <option value="">ordenar por</option>
                        <option value="">menor valor</option>
                        <option value="">maior valor</option>
                        <option value="">promoções</option>
                        <option value="">novidades</option>
                        <option value="">estou com sorte</option>
                    </select>
                    <select name="valor" id="">
                        <option value="">valor</option>
                        <option value="">R$ 0,00 à R$50,00</option>
                        <option value="">R$ 51,00 à R$100,00</option>
                        <option value="">R$ 101,00 à R$150,00</option>
                        <option value="">R$ mais de R$150,00</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="produtos-grade">
            <?php
            $i = 1;
            foreach($produtos as $produto):
                $fotos = unserialize($produto->fotos);
                ?>
            <div class="total-produto-box col-xs-12 col-sm-6 col-md-3" id="produto-<?= $i ?>">
                <div class="col-md-12 col-xs-12 text-center bg-white-produto prdt" itemtype="http://schema.org/Product">
                    <div class="categoria">
                    <?php $icone_ativado = 0; ?>
                    <?php foreach($prod_objetivos as $p_objetivos): 
                        if($p_objetivos->produto_base_id == $produto->produto_base->id && $icone_ativado == 0){          
                            if($p_objetivos->objetivo_id == 1) { ?>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 50.463 50.463" style="enable-background:new 0 0 50.463 50.463;" xml:space="preserve" width="20px" height="20px"><g><g><path d="M47.923,29.694c0.021-0.601-0.516-1.063-0.901-1.515c-0.676-2.733-2.016-5.864-3.961-8.971    C39.942,14.23,31.688,6.204,28.553,4.966c-0.158-0.062-0.299-0.097-0.429-0.126c-0.313-1.013-0.479-1.708-1.698-2.521    c-3.354-2.236-7.099-2.866-9.578-1.843c-2.481,1.023-3.859,6.687-1.19,8.625c2.546,1.857,7.583-1.888,9.195,0.509    c1.609,2.396,3.386,10.374,6.338,15.473c-0.746-0.102-1.514-0.156-2.307-0.156c-3.406,0-6.467,0.998-8.63,2.593    c-1.85-2.887-5.08-4.806-8.764-4.806c-3.82,0-7.141,2.064-8.95,5.13v22.619h4.879l1.042-1.849    c3.354-1.287,7.32-4.607,10.076-8.147C29.551,44.789,47.676,36.789,47.923,29.694z" fill="#5089cf"/></g></g></svg>
                                <?php $icone_ativado = 1; ?>
                                <?php } else if($p_objetivos->objetivo_id == 2) { ?>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="20px" height="20px" viewBox="0 0 164.882 164.883" style="enable-background:new 0 0 164.882 164.883;" xml:space="preserve"><g><path d="M161.77,36.535h-34.86c5.286-11.953,11.972-19.461,12.045-19.543c1.157-1.272,1.065-3.249-0.207-4.402   c-1.267-1.16-3.239-1.065-4.396,0.201c-0.347,0.38-8.403,9.31-14.236,23.744H60.471c-1.72,0-3.118,1.395-3.118,3.118v39.948   c0,1.72,1.397,3.118,3.118,3.118h55.966c2.058,8.284,5.56,16.425,10.576,24.22c1.103,1.699,2.394,3.77,3.733,5.985   c-15.145,6.102-32.2,9.384-49.545,9.384c-0.006,0-0.012,0-0.018,0c-18,0-35.591-3.532-51.152-10.072   c1.178-1.941,2.338-3.763,3.31-5.297c32.15-49.922-6.933-93.712-7.338-94.147c-1.16-1.273-3.126-1.355-4.402-0.207   c-1.272,1.16-1.367,3.13-0.207,4.408c1.492,1.641,36.222,40.743,6.704,86.573c-6.043,9.377-17.284,26.84-15.396,46.722   c0.149,1.613,1.51,2.819,3.1,2.819c0.101,0,0.201,0,0.298-0.013c1.717-0.158,2.98-1.687,2.807-3.397   c-1.136-11.947,3.255-23.176,7.968-31.943c16.492,7.039,35.204,10.783,54.297,10.783c0.006,0,0.012,0,0.019,0   c18.44,0,36.586-3.525,52.649-10.113c4.561,8.67,8.708,19.625,7.6,31.268c-0.158,1.711,1.096,3.239,2.801,3.397   c0.109,0.013,0.201,0.013,0.305,0.013c1.595,0,2.953-1.206,3.093-2.82c1.906-19.881-9.353-37.344-15.393-46.728   c-4.348-6.747-7.441-13.743-9.39-20.846h38.909c1.718,0,3.118-1.397,3.118-3.118V39.638   C164.888,37.918,163.494,36.535,161.77,36.535z M158.653,76.478h-6.51V63.732c0-1.72-1.4-3.117-3.117-3.117   c-1.724,0-3.117,1.397-3.117,3.117v12.745h-8.708V63.732c0-1.72-1.395-3.117-3.117-3.117c-1.718,0-3.118,1.397-3.118,3.117v12.745   h-8.701V63.732c0-1.72-1.406-3.117-3.118-3.117c-1.723,0-3.117,1.397-3.117,3.117v12.745h-8.714V63.732   c0-1.72-1.395-3.117-3.117-3.117c-1.718,0-3.118,1.397-3.118,3.117v12.745h-8.688V63.732c0-1.72-1.404-3.117-3.118-3.117   c-1.723,0-3.118,1.397-3.118,3.117v12.745h-8.705V63.732c0-1.72-1.397-3.117-3.117-3.117c-1.717,0-3.118,1.397-3.118,3.117v12.745   h-7.639V42.765h95.07v33.713H158.653z M80.176,108.765c-3.635,0-6.585-2.953-6.585-6.582c0-3.635,2.95-6.582,6.585-6.582   s6.576,2.947,6.576,6.582C86.746,105.812,83.811,108.765,80.176,108.765z M10.564,68.022v-6.658H0V44.808h10.564v-6.658   l14.94,14.939L10.564,68.022z" fill="#5089cf"/></g></svg>
                                <?php $icone_ativado = 1; ?>
                                <?php }
                            else if($p_objetivos->objetivo_id == 3) { ?>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="20px" height="20px"><g><g><path d="M400.268,175.599c-1.399-3.004-4.412-4.932-7.731-4.932h-101.12l99.797-157.568c1.664-2.628,1.766-5.956,0.265-8.678    C389.977,1.69,387.109,0,384.003,0H247.47c-3.234,0-6.187,1.826-7.637,4.719l-128,256c-1.323,2.637-1.178,5.777,0.375,8.294    c1.562,2.517,4.301,4.053,7.262,4.053h87.748l-95.616,227.089c-1.63,3.883-0.179,8.388,3.413,10.59    c1.382,0.845,2.918,1.254,4.446,1.254c2.449,0,4.864-1.05,6.537-3.029l273.067-324.267    C401.206,182.161,401.667,178.611,400.268,175.599z" fill="#5089cf"/></g></g></svg>
                                <?php $icone_ativado = 1; ?>
                                <?php }
                            else if($p_objetivos->objetivo_id == 4) { ?>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 511.998 511.998" style="enable-background:new 0 0 511.998 511.998;" xml:space="preserve" width="20px" height="20px"><g><g><path d="M396.8,236.586h-93.175l-22.673-68.053c-1.775-5.325-6.929-9.523-12.424-8.747c-5.623,0.128-10.496,3.874-12.023,9.276    l-37.35,130.697l-40.252-181.146c-1.229-5.478-5.871-9.523-11.477-9.975c-5.572-0.375-10.829,2.773-12.902,7.996l-48,119.953H12.8    c-7.074,0-12.8,5.726-12.8,12.8c0,7.074,5.726,12.8,12.8,12.8h102.4c5.222,0.008,9.95-3.174,11.878-8.047l35.823-89.523    l42.197,189.952c1.271,5.726,6.272,9.847,12.126,10.027c0.128,0,0.247,0,0.375,0c5.7,0,10.726-3.772,12.297-9.276l39.851-139.426    l12.501,37.547c1.749,5.222,6.647,8.747,12.151,8.747h102.4c7.074,0,12.8-5.726,12.8-12.8    C409.6,242.311,403.874,236.586,396.8,236.586z" fill="#5089cf"/></g></g><g><g><path d="M467.012,64.374C437.018,34.38,397.705,19.387,358.4,19.387c-36.779,0-73.259,13.662-102.4,39.919    c-29.15-26.257-65.621-39.919-102.4-39.919c-39.313,0-78.618,14.993-108.612,44.988C5.214,104.157-7.595,160.187,5.385,211.011    h26.624c-13.653-43.972-3.678-93.773,31.078-128.529c24.175-24.175,56.32-37.487,90.513-37.487    c31.206,0,60.399,11.563,83.695,31.889L256,94.369l18.714-17.493c23.296-20.318,52.489-31.889,83.686-31.889    c34.193,0,66.33,13.312,90.513,37.487c49.911,49.903,49.903,131.115,0,181.018L256,456.404L87.407,287.811H51.2l204.8,204.8    l211.012-211.012C526.993,221.61,526.993,124.364,467.012,64.374z" fill="#5089cf"/></g></g></svg>
                                <?php $icone_ativado = 1; ?>
                            <?php }       
                        }
                    endforeach; ?>
                    </div>
                    <!-- <div class="bg-white-produto prdt" itemscope="" itemtype="http://schema.org/Product"> -->
                    <div class="box-imagem-produto">
                        <?php if($produto->estoque == 1) { ?>
                            <div class="box-ultimo">    
                                <span>ÚLTIMO</span>
                            </div> 
                        <?php }?>
                        <?php if($produto->estoque == 1 && $produto->created > $max_date_novidade) { ?>
                            <div class="box-novidade" style="top: 19px;">    
                                <span>NOVIDADE</span>
                            </div>
                        <?php } else if($produto->created > $max_date_novidade) { ?>
                            <div class="box-novidade">    
                                <span>NOVIDADE</span>
                            </div>
                        <?php } ?>
                        <?php if(!($produto->estoque == 1 && $produto->created > $max_date_novidade) && $produto->preco_promo) { ?>
                            <?php if($produto->estoque == 1 && $produto->preco_promo || $produto->created > $max_date_novidade && $produto->preco_promo) { ?>
                                <div class="box-promocao" style="top: 19px;">    
                                    <span>PROMOÇÃO</span>
                                </div>
                            <?php } elseif ($produto->preco_promo) { ?>
                                <div class="box-promocao">    
                                    <span>PROMOÇÃO</span>
                                </div>
                            <?php } ?>
                        <?php } ?>
                        <?php if($produto->visivel == 1 && $produto->status_id == 1) { ?>
                            <?= $this->Html->link(
                                $this->Html->image(
                                    'produtos/sm-'.$fotos[0],
                                    ['class' => 'produto-grade produto-grade-drag', 'val' => $produto->slug, 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]),
                                $SSlug.'/produto/'.$produto->slug,
                                ['escape' => false]
                            ); ?>
                        <?php } else if($produto->visivel == 1 && $produto->status_id == 2) { ?>
                            <?= $this->Html->link(
                                $this->Html->image(
                                    'produtos/sm-'.$fotos[0],
                                    ['class' => 'produto-grade', 'val' => $produto->slug, 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]),
                                $SSlug.'/produto/'.$produto->slug,
                                ['escape' => false]
                            ); ?>
                        <?php } ?>
                    </div>
                            <div class="box-info-produto">
                                <script>
                                    $('.btn-indique').click(function () {
                                            var id_produto = $(this).attr('data-id');
                                            $('#overlay-indicacao-'+id_produto).height('100%');
                                            $("html,body").css({"overflow":"hidden"});
                                    });
                                    function closeIndica() {
                                        $('.overlay-indicacao').height('0%');
                                        $("html,body").css({"overflow":"auto"});
                                    }
                                </script>
                                <a href="<?= $SSlug ?>/produto/<?= $produto->slug ?>" class="link-prdt">
                                    <div class="description">
                                        <span class="product" itemprop="name"><?= $produto->produto_base->name; ?></span>
                                    </div>
                                    <?php if($produto->produto_base->propriedade_id && !empty($produto->propriedade)) { ?>
                                        <div class="col-xs-12 div-sabor">
                                        <?= ($produto->produto_base->propriedade_id && !empty($produto->propriedade)) ?
                                        '<p class="sabor-info text-uppercase">'.$produto->propriedade.'</p>' : ''; ?>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-xs-12 div-sabor">
                                            <p class="sabor-info text-uppercase">&nbsp;</p>
                                        </div>
                                    <?php } ?>
                                </a>
                                <!-- Trustvox Estrelinhas -->
                                <!-- <div data-trustvox-product-code="<?= $produto->id ?>"></div> -->
                                <div class="col-xs-12 box-fundo-produto">
                                    <div class="box-preco text-center">
                                        <?php if($produto->preco_promo) { ?>
                                            <p class="preco preco-desc">
                                                <?php $porcentagem_nova = $produto->preco * 0.089 ?>
                                                R$ <span style="text-decoration: line-through"><?= number_format($preco = $produto->preco + $porcentagem_nova, 2, ',','.'); ?></span>
                                            </p>
                                        <?php } ?> 
                                            <p class="preco">R$ <span class="preco-normal"><?= $produto->preco_promo ?
                                                number_format($preco = $produto->preco_promo, 2, ',','.') :
                                                number_format($preco = $produto->preco, 2, ',','.'); ?></span> ou</p>
                                                <?php $produto->preco_promo ? $porcentagem_nova = $produto->preco_promo * 0.089 : $porcentagem_nova = $produto->preco * 0.089 ?>
                                            <?php if($produto->preco_promo != null) {
                                                if($produto->preco_promo < 100.00) { ?>
                                                <?php $parcel = $produto->preco_promo * 0.089; $parcel = ($produto->preco_promo + $parcel)/2?>
                                                <p class="preco">2x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                            <?php } else if($produto->preco_promo > 100.00 && $produto->preco_promo < 200.00) { ?>
                                                <?php $parcel = $produto->preco_promo * 0.089; $parcel = ($produto->preco_promo + $parcel)/3?>
                                                <p class="preco">3x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                            <?php } else if($produto->preco_promo > 200.00 && $produto->preco_promo < 300.00) { ?>
                                                <?php $parcel = $produto->preco_promo * 0.089; $parcel = ($produto->preco_promo + $parcel)/5?>
                                                <p class="preco">5x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                            <?php } else if($produto->preco_promo > 300.00) { ?>
                                                <?php $parcel = $produto->preco_promo * 0.089; $parcel = ($produto->preco_promo + $parcel)/6?>
                                                <p class="preco">6x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                            <?php }
                                        } else {
                                            if($produto->preco < 100.00) { ?>
                                                <?php $parcel = $produto->preco * 0.089; $parcel = ($produto->preco + $parcel)/2?>
                                                <p class="preco">2x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                            <?php } else if($produto->preco > 100.00 && $produto->preco < 200.00) { ?>
                                                <?php $parcel = $produto->preco * 0.089; $parcel = ($produto->preco + $parcel)/3?>
                                                <p class="preco">3x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                            <?php } else if($produto->preco > 200.00 && $produto->preco < 300.00) { ?>
                                                <?php $parcel = $produto->preco * 0.089; $parcel = ($produto->preco + $parcel)/5?>
                                                <p class="preco">5x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                            <?php } else if($produto->preco > 300.00) { ?>
                                                <?php $parcel = $produto->preco * 0.089; $parcel = ($produto->preco + $parcel)/6?>
                                                <p class="preco">6x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                            <?php }
                                        } ?>
                                    </div>
                                    <div class="box-botoes text-right">
                                        <button class="btn btn-box btn-indique" type="button" data-id="<?= $produto->id ?>"><?= $this->Html->image('icnon-indicar.png',['alt'=>'indique']) ?><span>indique</span></button>
                                        <script type="text/javascript">
                                            $('.send-indicacao-home-btn').click(function(e) {
                                                e.preventDefault();
                                                var botao = $(this);
                                            
                                                botao.html('Enviando...');
                                                botao.attr('disabled', 'disabled');

                                                var id = $(this).attr('data-id');

                                                var valid = $('.indicar_prod_home_'+id).validationEngine("validate");
                                                if (valid == true) {
                                                    $.ajax({
                                                        type: "POST",
                                                        url: WEBROOT_URL + 'produtos/overlay-indicar-produtos-home/',
                                                        data: {
                                                            name: $('.name-'+id).val(),
                                                            email: $('.email-'+id).val(), 
                                                            produto_id: $('.id_produto-'+id).val()
                                                        }
                                                    })
                                                    .done(function (data) {
                                                        if(data == 1) {
                                                            botao.html('Enviado!');
                                                            setTimeout(function () {
                                                                botao.html('Enviar');
                                                                botao.removeAttr('disabled');
                                                            }, 2200);
                                                        } else {
                                                            botao.html('Falha ao enviar... Tente novamente...');
                                                            setTimeout(function () {
                                                                botao.html('Enviar');
                                                                botao.removeAttr('disabled');
                                                            }, 2200);
                                                        }
                                                    });
                                                }else{
                                                    $('.indicar_prod_home_'+id).validationEngine({
                                                        updatePromptsPosition: true,
                                                        promptPosition: 'inline',
                                                        scroll: false
                                                    });
                                                }
                                            });
                                        </script>
                                    <!-- overlay de indicação -->
                                    <div id="overlay-indicacao-<?= $produto->id ?>" class="overlay overlay-indicacao">
                                        <!-- Overlay content -->
                                        <div class="overlay-content text-center">
                                            <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                                            <h4>Lembrou de alguém quando viu isso?</h4>
                                            <h4>Indique este produto!</h4>
                                            <br>
                                            <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$produto->id])?>
                                            <?= $this->Form->input('name', [
                                                'div'           => false,
                                                'label'         => false,
                                                'class'         => 'form-control validate[optional] form-indicar name-'.$produto->id,
                                                'placeholder'   => 'Quem indicou?',
                                            ])?>
                                            <br>
                                            <?= $this->Form->input('email', [
                                                'div'           => false,
                                                'label'         => false,
                                                'class'         => 'form-control validate[required] form-indicar email-'.$produto->id,
                                                'placeholder'   => 'E-mail do destinatário',
                                            ])?>
                                            <?= $this->Form->input('id_produto', [
                                                'div'           => false,
                                                'class'         => 'id_produto-'.$produto->id,
                                                'value'         => $produto->id,
                                                'label'         => false,
                                                'type'          => 'hidden'
                                            ])?>
                                            <br>
                                            <?= $this->Form->button('Enviar', [
                                                'type'          => 'button',
                                                'data-id'       => $produto->id,
                                                'class'         => 'btn btn-success send-indicacao-home-btn'
                                            ])?>
                                            <br>
                                            <br>
                                            <?= $this->Form->end()?>
                                        </div>
                                    </div>
                                    <?php if($produto->visivel == 1 && $produto->status_id == 1) { ?>
                                        <button class="btn btn-box btn-compre incluir-mochila" val="<?= $produto->slug ?>" data-id="<?= $produto->id ?>"><?= $this->Html->image('mochila_log_branca.png',['alt'=>'indique']) ?><span>compre</span></button>
                                    <?php } else if($produto->visivel == 1 && $produto->status_id == 2) { ?>  
                                        <button class="btn btn-box produto-acabou-btn" data-id="<?= $produto->id ?> "><i class="fa fa-envelope fa-2x"></i><span>avise</span></button>

                                        <div id="aviseMe-<?= $produto->id ?>" class="overlay overlay-avise">
                                            <div class="overlay-avise-content overlay-avise-<?= $produto->id ?> text-center">
                                                <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                                                <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                                <br />
                                                <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$produto->id])?>
                                                <?= $this->Form->input('produto_id', [
                                                    'div'           => false,
                                                    'label'         => false,
                                                    'type'          => 'hidden',
                                                    'val'           => $produto->id,
                                                    'id'            => 'produto_id_acabou-'.$produto->id
                                                ])?>
                                                <?= $this->Form->input('name', [
                                                    'div'           => false,
                                                    'label'         => false,
                                                    'id'            => 'name_acabou-'.$produto->id,
                                                    'class'         => 'form-control validate[optional]',
                                                    'placeholder'   => 'Qual o seu nome?',
                                                ])?>
                                                <br/>
                                                <?= $this->Form->input('email', [
                                                    'div'           => false,
                                                    'label'         => false,
                                                    'id'            => 'email_acabou-'.$produto->id,
                                                    'class'         => 'form-control validate[required, custom[email]]',
                                                    'placeholder'   => 'Qual o seu email?',
                                                ])?>
                                                <br/>
                                                <?= $this->Form->button('Enviar', [
                                                    'data-id'       => $produto->id,
                                                    'type'          => 'button',
                                                    'class'         => 'btn btn-success send-acabou-btn'
                                                ])?>
                                                <br />
                                                <br />
                                                <?= $this->Form->end()?>
                                            </div>
                                        </div>                               
                                    <?php } ?>
                                    <button class="btn"><i class="fa fa-balance-scale"></i> compare</button>
                                    </div>
                                </div>
                            </div>
                        <!-- </div> -->
                    </div>
                </div>
            <?php
            $i++;
            endforeach;
            ?>  
        </div>
    </div>
    
    
    
    
</div><!--FIM CONTAINER GRADE DE PRODUTOS-->

<div class="row" id="veja-mais">
    <div class="col-lg-12 text-center">
        <button class="btn btn-cupom" id="new-produtos-load-more"> Ver mais produtos <i class="fa fa-chevron-circle-down"></i></button>
    </div>
</div>

<script>
    $('.owl-carousel-banner').owlCarousel({
        loop: true,
        autoplay:true,
        autoplayTimeout:1000,
        autoplayHoverPause:true,
        margin: 10,
        itemsMobile: [600, 1],
        itemsDesktopSmall: [1024, 1],
        itemsDesktop: [6000, 1],
      });

    $(document).ready(function() {

        var posicao_container_antiga = 0;

        $(window).scroll(function() {
            var posicao_pagina = $(document).scrollTop();
            var container = $('.container-grade');
            var posicao_container = (container.offset().top - 50);

            if(posicao_pagina >= posicao_container) {
                $('.filtro-div').addClass('fixed_filtro');
                $('.subfiltro-div').addClass('fixed_subfiltro');
                $('.produtos-div').addClass('col-md-offset-2');
                $('.produtos-grade').css('margin-top', '100px');
            } else if(posicao_pagina < posicao_container) {
                $('.filtro-div').removeClass('fixed_filtro');
                $('.subfiltro-div').removeClass('fixed_subfiltro');
                $('.produtos-div').removeClass('col-md-offset-2');
                $('.produtos-grade').css('margin-top', 0);
            }
        });
    });
</script>

<style type="text/css">
    .separador-trava {
        width: 100%;
        position: absolute;
    }
    .fixed_filtro {
        position: fixed;
        width: 195px;
        top: 75px;
        position: fixed;
        z-index: 10;
    }
    .fixed_subfiltro {
        position: fixed;
        top: 55px;
        padding-top: 20px;
        width: 72.2%;
        background-color: #fafafa;
        z-index: 5;
    }
</style>