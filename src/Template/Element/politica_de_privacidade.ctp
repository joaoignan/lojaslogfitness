<div id="politica-de-privacidade" style="<?= isset($hide) ? 'display: none;' : '' ?>">
    Política de Privacidade<br><br>

    Nossa política de privacidade tem como objetivo deixar claro nosso compromisso em garantir a proteção dos dados solicitados ou coletados.<br>
    Não fornecemos suas informações pessoais para terceiros sem a sua autorização e não divulgamos informações que possam identificar os usuários.
    Somente dados genéricos e agregados, referentes à quantidade de pessoas que acessam áreas dos nossos sites, podem ser divulgados para anunciantes e parceiros comerciais.<br><br>

    Seus Dados<br><br>

    Todos os dados pessoais informados ao nosso site são armazenados em um banco de dados reservado e com acesso restrito a alguns funcionários habilitados,
    que são obrigados, por contrato, a manter a confidencialidade das informações e não utilizá-las inadequadamente.<br><br>

    Como os dados são utilizados<br><br>

    Usamos os dados coletados e fornecidos por nossos clientes para 1) atender a solicitações de produtos, serviços ou informações, 2) prestação de serviços ao
    cliente, 3) administrar sorteios ou promoções, 4) oferecer novos produtos e serviços, 5) medir e melhorar a eficácia do nosso site em campanhas de marketing
    ou dos nossos serviços e ofertas, 6) adaptar as nossas ofertas de acordo com as suas preferências, 7) administrar pesquisas, 8) o envio de comunicações de marketing.<br><br>

    Cookies e outras tecnologias<br><br>

    Nós usamos cookies para criar uma experiência de compra mais personalizada para os visitantes do nosso site. Um cookie é um pequeno arquivo de dados
    que um site pode enviar para seu navegador e que pode ser armazenado em seu disco rígido. Nenhuma informação pessoal identificável é armazenada nesses cookie.<br>
    Nosso servidor Web recolhe automaticamente o endereço do site que você veio antes de visitar o nosso site, qual navegador que você usou para ver o nosso site e todos os termos de busca que você utilizou em nosso site.
    A aceitação dos cookies pode ser livremente alterada na configuração de seu navegador.<br><br>

    Serviço de terceiros<br><br>

    Nós podemos utilizar prestadores de serviços para veicular anúncios, enviar e-mails e analisar o tráfego de site. Essas empresas podem usar tecnologias
    semelhantes aos descritos acima para medir a eficácia dos anúncios e e-mails. Eles também podem usar informações sobre suas visitas a este site para que
    possamos fornecer anúncios sobre bens e serviços que possam ser de seu interesse. Para este efeito, também podemos fornecer a esses prestadores de serviços
    informações anônimas sobre a navegação no site e as compras.<br><br>
    Caso você tenha outras dúvidas ou perguntas sobre esta Política de Privacidade, entre em contato conosco através do nosso SAC.<br><br>
</div>