<?= $this->Html->css('comparador') ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<div class="total-comparador">
	<div class="comparador-main">
		<div class="comparador-content">
			<?php foreach($produtos_comparador as $produto_comparador) { ?>
				<?php $fotos = unserialize($produto_comparador->fotos) ?>

				<?php $valor = $produto_comparador->preco_promo ? $produto_comparador->preco_promo * $desconto_geral : $produto_comparador->preco * $desconto_geral ?>
				<div class="comparador-item">
					<a class="btn-exclui-comparador" data-id="<?= $produto_comparador->id ?>"><i class="fa fa-close"></i></a>
					<?= $this->Html->link(
					'<div class="comparador-item-foto text-center">'.$this->Html->image($fotos[0]).'</div>'
					, $SSlug.'/produto/'.$produto_comparador->slug,['escape' => false])  ?>
					<div class="comparador-item-dados text-center">
						<p class="nome"><?= $produto_comparador->produto_base->name ?></p>
						<p><strong><?= $produto_comparador->produto_base->propriedade->name ?>:</strong> <?= $produto_comparador->propriedade ?></p>
						<p><strong>Peso:</strong> <?= strtolower($produto_comparador->tamanho.' '.$produto_comparador->unidade_medida) ?></p>
						<p><strong>Preço:</strong> R$<?= number_format($valor, 2, ',','.') ?></p>
						<?php if ($produto_comparador->doses) { ?>
							<p><strong>Treinos:</strong> <?= $produto_comparador->doses ?></p>
						<?php } else { ?>
							<p>&nbsp;</p>
						<?php } ?>
						<?php if ($produto_comparador->doses) { ?>
							<p><strong>Valor dose</strong> R$<?= number_format(($valor / $produto_comparador->doses), 2, ',','.') ?></p>
						<?php } else { ?>
							<p>&nbsp;</p>
						<?php } ?>
						<div class="compra-indica">
							<?php if($produto_comparador->status_id == 1) { ?>
								<button class="btn comprar-comparador incluir-mochila-comparador-topo" val="<?= $produto_comparador->slug ?>" data-id="<?= $produto_comparador->id ?>">Comprar</button>
								<button class="btn indica-btn btn-indique-comparador" data-id="<?= $produto_comparador->id ?>">Indicar</button>
							<?php } else { ?>
								<?= $this->Html->link('<button class="btn indica-btn">Similares</button>', $SSlug.'/similares/'.$produto_comparador->slug,['escape' => false])  ?>
								<button class="btn indica-btn produto-acabou-btn" data-id="<?= $produto_comparador->id ?>">Avise-me</button>
							<?php	} ?>
						</div>
					</div>
				</div>
			<?php } ?>
			<?php foreach($produtos_comparador as $produto_comparador) { ?>
				<div id="overlay-comparacao-<?= $produto_comparador->id ?>" class="overlay overlay-indicacao">
	                <div class="overlay-content text-center">
	                    <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
						<h4>Lembrou de alguém quando viu isso?</h4>
                        <h4>Indique este produto!</h4>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 share-whastapp share-buttons">
                                <a href="whatsapp://send?text=Isso é a sua cara! <?= WEBROOT_URL.$SSlug.'/produto/'.$produto_comparador->slug ?>">
                                    <button class="btn button-blue"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</button>
                                </a>
                            </div>
                            <div class="col-xs-12 share-whastapp share-buttons">
                                <button class="btn button-blue" id="share-comparador-<?= $produto_comparador->id ?>"><i class="fa fa-comment-o" aria-hidden="true"></i> Messenger</button>
                                <script>
                                    $(document).ready(function() {
                                        $('#share-comparador-<?= $produto_comparador->id ?>').click(function(){
                                            var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$produto_comparador->slug ?>";
                                            var app_id = '1321574044525013';
                                            window.open('fb-messenger://share?link=' + link + '&app_id=' + app_id);
                                        });
                                    });
                                </script>
                            </div>
                            <div class="tablet-hidden col-xs-12 col-md-3 share-buttons">
                                <button data-id="whatsweb-<?= $produto_comparador->id ?>" class="btn button-blue abrir-whats-comparador"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp Web</button>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                <a href="javascript: void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u=<?= WEBROOT_URL.$SSlug.'/produto/'.$produto_comparador->slug ?>','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');">
                                    <button class="btn button-blue fb-share-button"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</button>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                <button class="btn button-blue" id="copiar-clipboard-<?= $produto_comparador->id ?>"><i class="fa fa-clone" aria-hidden="true"></i> Copiar link</button>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                            	<button data-id="<?=$produto_comparador->id ?>" class="btn button-blue abrir-email-comparador"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail</button>
                            </div>
                        </div>
                        <div class="row">
                        	<div class="tablet-hidden enviar-whats-comparador">
                        		<div class="col-xs-4 text-center form-group">
                                    <?= $this->Form->input('whats-web-form', [
                                        'div'           => false,
                                        'id'            => 'nro-whatsweb-'.$produto_comparador->id,
                                        'label'         => false,
                                        'class'         => 'form-control form-indicar',
                                        'placeholder'   => "Celular*: (00)00000-0000"
                                    ])?>
                        		</div>
                                <div class="col-xs-4 text-left obs-overlay">
                                    <p>*Para enviar pra mais de um contato deixe este campo vazio.</p>
                                </div>
                        		<div class="col-xs-4 text-center form-group">
									<?= $this->Form->button('Enviar', [
                                    'type'          => 'button',
                                    'data-id'       => $produto_comparador->id,
                                    'class'         => 'btn button-blue',
                                    'id'            => 'enviar-whatsweb-'.$produto_comparador->id
                                	])?>
                        		</div>
                                <script>
                                    $(document).ready(function() {
                                        $('#enviar-whatsweb-<?=$produto_comparador->id ?>').click(function(){

                                            var numero;
                                            var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$produto_comparador->slug ?>";

                                            if($('#nro-whatsweb-<?= $produto_comparador->id ?>').val() != '') {
                                                numero = '55'+$('#nro-whatsweb-<?= $produto_comparador->id ?>').val();
                                            }
                                            
                                            window.open('https://api.whatsapp.com/send?phone=' + numero + '&text=Isso é a sua cara! ' + link);
                                        });
                                    });
                                </script>
                        	</div>
                            <div class="col-xs-12 enviar-email-comparador" id="collapse-comparador-<?=$produto_comparador->id ?>">
                                <?= $this->Form->create(null, ['class' => 'indicar_prod_comparador_'.$produto_comparador->id])?>
                                <?= $this->Form->input('name', [
                                    'div'           => false,
                                    'label'         => false,
                                    'class'         => 'form-control validate[optional] form-indicar name-comparador-'.$produto_comparador->id,
                                    'placeholder'   => 'Quem indicou?',
                                ])?>
                                <br>
                                <?= $this->Form->input('email', [
                                    'div'           => false,
                                    'label'         => false,
                                    'class'         => 'form-control validate[required] form-indicar email-comparador-'.$produto_comparador->id,
                                    'placeholder'   => 'E-mail do destinatário',
                                ])?>
                                <?= $this->Form->input('id_produto', [
                                    'div'           => false,
                                    'class'         => 'id_produto-'.$produto_comparador->id,
                                    'value'         => $produto_comparador->id,
                                    'label'         => false,
                                    'type'          => 'hidden'
                                ])?>
                                <br/>
                                <?= $this->Form->button('Enviar', [
                                    'type'          => 'button',
                                    'data-id'       => $produto_comparador->id,
                                    'class'         => 'btn button-blue send-indicacao-comparador-btn'
                                ])?>
                                <br />
                                <?= $this->Form->end()?>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-xs-12 clipboard">   
                                <?= $this->Form->input('url', [
                                    'div'           => false,
                                    'id'            => 'url-clipboard-'.$produto_comparador->id,
                                    'label'         => false,
                                    'value'         => WEBROOT_URL.$SSlug.'/produto/'.$produto_comparador->slug,
                                    'readonly'      => true,
                                    'class'         => 'form-control form-indicar'
                                ])?>
                            </div>  
                            <p><span class="copy-alert hide" id="copy-alert-<?=$produto_comparador->id ?>">Copiado!</span></p>
                            <script>
                                $(document).ready(function() {
                                    // Copy to clipboard 
                                    document.querySelector("#copiar-clipboard-<?=$produto_comparador->id ?>").onclick = () => {
                                      // Select the content
                                        document.querySelector("#url-clipboard-<?=$produto_comparador->id ?>").select();
                                        // Copy to the clipboard
                                        document.execCommand('copy');
                                        // Exibe span de sucesso
                                        $('#copy-alert-<?=$produto_comparador->id ?>').removeClass("hide");
                                        // Oculta span de sucesso
                                        setTimeout(function() {
                                            $('#copy-alert-<?=$produto_comparador->id ?>').addClass("hide");
                                        }, 1500);
                                    };
                                });
                            </script>
                        </div>
                    </div>
                </div> 
			<?php } ?>
		</div>
		<div class="col-xs-12 text-center">
			<?= $this->Html->link('<button class="btn btn-comparar-completo">Comparação completa</button>',$SSlug.'/comparador/completo',['escape' => false]); ?>
			<button class="btn btn-danger btn-fechar-comparador">Mais produtos</button>
		</div>	
	</div>
</div>

<script>
	$(document).ready(function(){
		//adicionar produto ao comparador
		$(document).on('click', '.btn-comparar-action', function(e) {
	        e.preventDefault();
	        $(".loading").css('display', 'block');

	        var produto_id = $(this).attr('data-id');
	        $.get(WEBROOT_URL + 'adicionar-produto-comparador/' + produto_id,
	            function (data) {
	                $('.comparador-content').html(data);

	                $(".loading").css('display', 'none');


	                $('.total-comparador').animate({'bottom' : '0'}, 500);
					setTimeout(function() {
						$('.comparador-main').css('display', 'block');
						$('.comparador-main').animate({'opacity' : '1'}, 500);
					}, 700);
	            });
	    });

		//remover produto do comparador
		$('.btn-exclui-comparador').on('click', function(e) {
	        e.preventDefault();
	        loading.show();

	        var produto_id = $(this).attr('data-id');
	        $.get(WEBROOT_URL + 'remover-produto-comparador/' + produto_id,
	            function (data) {
	                $('.comparador-content').html(data);
	                loading.hide();
	            });
	    });

		// abrir comparador
		$('.abrir-comparador').click(function(){
			$('.total-comparador').animate({'bottom' : '0'}, 500);
			$("body,html").addClass('total-block');
			setTimeout(function() {
				$('.comparador-main').css('display', 'block');
				$('.comparador-main').animate({'opacity' : '1'}, 500);
			}, 700);
		});
		// fechar comparador
		$('.total-comparador, .btn-fechar-comparador').click(function(){
			$("body,html").removeClass('total-block');
			$('.comparador-main').animate({'opacity' : '0'}, 500);
			setTimeout(function() {
				$('.comparador-main').css('display', 'none');
				$('.total-comparador').animate({'bottom' : '2500px'}, 500);
			}, 700);
		});
		// não fechar quando clicar na area branca
		$('.comparador-main').click(function(e){
			e.stopPropagation();
		});

		$('.produto-acabou-btn').click(function(e) {
        e.preventDefault();
        openAviseMe($(this).attr('data-id'));
	    });

	    $('.overlay-avise, .overlay-avise-content').click(function(e) {
	        e.preventDefault();
	    });

	    $('.overlay-avise-content').click(function(e) {
	        e.stopPropagation();
	    });
	});
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.btn-indique-comparador').click(function () {
                var id_produto = $(this).attr('data-id');
                $('#overlay-comparacao-'+id_produto).height('100%');
        });
        function closeIndica() {
            $('.overlay-indicacao').height('0%');
        }
        $(document).on('click', '.dropbtn', function() {
            if($('.button-view-filtro').hasClass('filtro-aberto') && !$(this).hasClass('dropdown-active')) {
                $('.button-view-filtro').removeClass('filtro-aberto');
                
                $('.total-filtro').animate({'left' : '-600px'}, 300);
                $('#filtro-mobile-btn').animate({'right' : '10px'});
            }
        });

        $('.send-indicacao-comparador-btn').click(function(e) {
            e.preventDefault();
            var botao = $(this);
        
            botao.html('Enviando...');
            botao.attr('disabled', 'disabled');

            var id = $(this).attr('data-id');

            var valid = $('.indicar_prod_comparador_'+id).validationEngine("validate");
            if (valid == true) {
                $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + 'produtos/overlay-indicar-produtos-home/',
                    data: {
                        name: $('.name-comparador-'+id).val(),
                        email: $('.email-comparador-'+id).val(), 
                        produto_id: $('.id_produto-'+id).val()
                    }
                })
                .done(function (data) {
                    if(data == 1) {
                        botao.html('Enviado!');
                        setTimeout(function () {
                            botao.html('Enviar');
                            botao.removeAttr('disabled');
                        }, 2200);
                    } else {
                        botao.html('Falha ao enviar... Tente novamente...');
                        setTimeout(function () {
                            botao.html('Enviar');
                            botao.removeAttr('disabled');
                        }, 2200);
                    }
                });
            }else{
                $('.indicar_prod_comparador_'+id).validationEngine({
                    updatePromptsPosition: true,
                    promptPosition: 'inline',
                    scroll: false
                });
            }
        });

        $('.incluir-mochila-comparador-topo').click(function(e) {
	        e.preventDefault();

	        var botao = $(this);
	        var id = $(this).attr('data-id');
	        var url_comprar = $(this).attr('val');
	        botao.html('Aguarde...');
	        $.get(WEBROOT_URL + 'produtos/comprar/' + url_comprar,
	            function (data) {
	                $('.mochila-nova').html(data);
	                botao.html('Adicionado');
	                setTimeout(function() {
	                	 botao.html('Comprar');
	                }, 1400);
	            });
	    });
	    $('.abrir-email-comparador').click(function(e){
            e.preventDefault();
            var id = $(this).attr('data-id');
            $('.enviar-whats-comparador').slideUp("slow");
            $('.enviar-email-comparador').slideToggle("slow");
        });
        $('.abrir-whats-comparador').click(function(e){
            var id = $(this).attr('data-id');
            $('.enviar-email-comparador').slideUp("slow");
            $('.enviar-whats-comparador').slideToggle("slow");
        });
    });
</script>
