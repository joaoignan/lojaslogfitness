<div class="avatar-total" style="background-attachment: fixed">
    <div class="container">
        <div class="avatar-content text-center">
            <div class="avatar text-center">
                <?php if($academia->image != null){
                    echo $this->Html->image('academias/'.$academia->image, ['alt' => 'Log Academia '.$academia->shortname]);
                    } else{
                       echo '&nbsp';
                    } ?>                           
            </div> <!-- avatar -->
        </div> <!-- col-sm-6 col-md-2 avatar-content -->
        <div class="avaliacao col-xs-12 col-sm-5 col-md-6 col-lg-6">
            <div class="row">
                <div class="col-xs-12">
                    <h1><?= $academia->shortname ?></h1>
                </div>
            </div> <!-- row -->
            <div class="row">
                <div class="col-xs-12">
                    <?php for($i = 1; $i <= 5; $i++) {
                        if($i <= $total_estrelas) {
                            echo '<i class="fa fa-star fa-2x rating-dourado"></i>';
                        } else {
                            echo '<i class="fa fa-star fa-2x rating-azul"></i>';
                        }
                    } ?>
                </div>
            </div> <!-- row --> 
            <div class="row botao-loja">
                <div class="col-xs-12">
                    <?= $this->Html->link('<button class="btn" id="btn-loja">loja</button>',WEBROOT_URL.$academia->slug,['escape' => false]) ?>
                    <?= $this->Html->link('<button class="btn" id="btn-perfil">perfil</button>',WEBROOT_URL.'perfil/'.$academia->slug,['escape' => false]) ?>
                    <?= $this->Html->link('<button class="btn btn-success-new" id="btn-mensalidade">pagar mensalidade</button>',WEBROOT_URL.'mensalidade/'.$academia->slug,['escape' => false]) ?>
                    <?= $this->Html->link('<button class="btn btn-success-new" id="btn-mensalidade-voltar">mensalidade</button>',WEBROOT_URL.'mensalidade/'.$academia->slug,['escape' => false]) ?>
                    <!-- <button class="btn" id="btn-balanca"><i class="fa fa-balance-scale" aria-hidden="true"></i> <span>3</span></button>    
                    <button class="btn" id="abrir-mochila"><i class="fa fa-shopping-bag" aria-hidden="true"></i> <span>2</span></button> --> 
                </div>
            </div>
        </div> <!-- avaliacao -->
        <div class="botoes-loja">
        </div><!-- botoes-loja -->


        <!-- <div id="selo_seguranca">
            <?= $this->Html->image('certificado_ssl.png')?>
        </div> -->
    </div> <!-- container -->
</div>