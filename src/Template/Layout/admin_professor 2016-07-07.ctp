<?php
$page_title = 'ADM Professor LogFitness';
?>
<!DOCTYPE html>
<html  lang="pt-br">
<head>

    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $page_title ?>
    </title>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>

    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>
        <?= $page_title ?>:
        <?= __(str_replace('Admin/', '', $this->fetch('title'))) ?>
    </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= $this->request->webroot; ?>assets/images/icons/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= $this->request->webroot; ?>assets/images/icons/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= $this->request->webroot; ?>assets/images/icons/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?= $this->request->webroot; ?>assets/images/icons/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?= $this->request->webroot; ?>assets/images/icons/favicon.png">

    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/iconic/iconic.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/elusive/elusive.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/meteocons/meteocons.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/spinnericon/spinnericon.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/typicons/typicons.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets-minified/admin-all-demo.css">

    <!-- JS Core -->
    <script type="text/javascript" src="<?= $this->request->webroot; ?>assets-minified/js-core.js"></script>

    <script type="text/javascript">
        var Controller  = "<?= $ControllerName; ?>";
        var Action      = "<?= $ActionName; ?>";
        var WEBROOT_URL = "<?= WEBROOT_URL; ?>";
    </script>

    <?= $this->Html->css('validationEngine.jquery.custom') ?>
    <?php //= $this->Html->css('simple-line-icons/css/simple-line-icons') ?>
    <?= $this->Html->css('jquery.fancybox') ?>
    <?= $this->Html->css('admin') ?>

    <?= $this->Html->script('jquery.validationEngine-br') ?>
    <?= $this->Html->script('jquery.validationEngine.custom') ?>
    <?= $this->Html->script('jquery.fancybox') ?>
    <?= $this->Html->script('jquery.mask') ?>
    <?= $this->Html->script('form') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>


<body>
<div id="sb-site">
    <div id="loadingg">
        <div class="spinnner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-blue-cacique font-inverse">
            <div id="mobile-navigation">
                <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                <a href="index.html" class="logo-content-small" title="Max Adm"></a>
            </div>
            <div id="header-logo" class="bg-white">
                <br>
                <span class="helper"></span>
                <?= $this->Html->image('logfitness.png'); ?>
                <a id="close-sidebar" href="#" title="Esconder menu">
                    <i class="glyph-icon icon-angle-left"></i>
                </a>
            </div>
            <div id="header-nav-left">
                <div class="user-account-btn dropdown">
                    <a href="#" title="Minha Conta" class="user-profile clearfix" data-toggle="dropdown">
                        <?= $this->Html->image('professores/default.png', ['alt' => __('Profile image'), 'style' => 'max-width:28px; max-height:28px;']) ?>
                        <span style="width: auto;"><?= $adm_professor->name ?></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-left">
                        <div class="box-sm">
                            <div class="login-box clearfix">
                                <div class="user-img">
                                    <?= $this->Html->image('professores/default.png', ['alt' => __('Profile image'), 'style' => 'max-width:128px; max-height:128px;'])
                                    ?>
                                </div>
                                <div class="user-info">
                                    <span>
                                        <?= $adm_professor->name ?>
                                        <i>Professor</i>
                                    </span>
                                </div>
                            </div>
                            <div class="divider"></div>

                            <ul class="reset-ul mrg5B">
                                <li>
                                    <?= $this->Html->link(
                                        __('Change Password').'<i class="glyph-icon float-right icon-linecons-key"></i>',
                                        '/professores/admin/passwd',
                                        ['escape' => false]) ?>
                                </li>
                            </ul>
                            <div class="pad5A button-pane button-pane-alt text-center">
                                <?= $this->Html->link('<i class="glyph-icon icon-power-off"></i>&nbsp;&nbsp;Sair do Sistema',
                                    '/professores/logout',
                                    ['class' => 'btn display-block font-normal btn-danger', 'escape' => false]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="header-nav-right">
                <a href="#" class="hdr-btn" id="fullscreen-btn" title="Fullscreen">
                    <i class="glyph-icon icon-arrows-alt"></i>
                </a>
            </div>

        </div>
        <div id="page-sidebar" class="bg-black font-inverse color-orange-cacique">
            <div class="scroll-sidebar">

                <ul id="sidebar-menu">
                    <!-- INICIO -->
                    <li class="header"><span>Admin</span></li>
                    <li>
                        <?=$this->Html->link('<i class="glyph-icon icon-home"></i><span>'.__('Dashboard').'</span>',
                            '/professores/admin/dashboard',
                            ['title' => 'Admin Dashboard', 'escape' => false]);?>
                    </li>
                    <li class="divider"></li>

                    <!-- CONFIGURAÇÃO -->
                    <li class="header"><span><?=__('Configuração');?></span></li>

                    <!-- ALUNOS -->
                    <li>
                        <a href="#" title="<?=__('Alunos');?>">
                            <div class="glyph-icon icon-cube"></div>
                            <span><?=__('Alunos');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <li>
                                    <?=$this->Html->link(__('Listar Alunos'),
                                        '/professores/admin/alunos');?>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <!-- ACADEMIA -->
                    <li>
                        <a href="#" title="<?=__('Academia');?>">
                            <div class="glyph-icon icon-cube"></div>
                            <span><?=__('Academia');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <li>
                                    <?=$this->Html->link(__('Indicar academia'),
                                        '/professores/admin/indicar-academia');?>
                                </li>
                            </ul>

                            <ul>
                                <li>
                                    <?=$this->Html->link(__('Minhas academias'),
                                        '/professores/admin/listar-minhas-academias');?>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <!-- PEDIDOS -->
                    <li>
                        <a href="#" title="<?=__('Pedidos');?>">
                            <div class="glyph-icon icon-cube"></div>
                            <span><?=__('Pedidos');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <li>
                                    <?=$this->Html->link(__('Listar Pedidos'),
                                        '/professores/admin/pedidos');?>
                                </li>

                            </ul>
                        </div>
                    </li>

                    <!-- PROFESSOR -->
                    <li>
                        <a href="#" title="<?=__('Professor');?>">
                            <div class="glyph-icon icon-cube"></div>
                            <span><?=__('Professor');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <li>
                                    <?=$this->Html->link(__('Meus dados'),
                                        '/professores/admin/meus-dados');?>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div id="page-content-wrapper">
            <div id="page-content">

                <!-- jQueryUI Spinner -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/spinner/spinner.js"></script>
                <script type="text/javascript">
                    /* jQuery UI Spinner */

                    $(function() { "use strict";
                        $(".spinner-input").spinner();
                    });
                </script>

                <!-- jQueryUI Autocomplete -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/autocomplete/autocomplete.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/autocomplete/menu.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/autocomplete/autocomplete-demo.js"></script>

                <!-- Touchspin -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/touchspin/touchspin.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/touchspin/touchspin-demo.js"></script>

                <!-- Input switch -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/input-switch/inputswitch.js"></script>
                <script type="text/javascript">
                    /* Input switch */

                    $(function() { "use strict";
                        $('.input-switch').bootstrapSwitch();
                    });
                </script>

                <!-- Textarea -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/textarea/textarea.js"></script>
                <script type="text/javascript">
                    /* Textarea autoresize */

                    $(function() { "use strict";
                        $('.textarea-autosize').autosize();
                    });
                </script>

                <!-- Multi select -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/multi-select/multiselect.js"></script>
                <script type="text/javascript">
                    /* Multiselect inputs */

                    $(function() { "use strict";
                        $(".multi-select").multiSelect();
                        $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
                    });
                </script>

                <!-- Uniform -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/uniform/uniform.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/uniform/uniform-demo.js"></script>

                <!-- Chosen -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/chosen/chosen.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/chosen/chosen-demo.js"></script>

                <!-- Nice scroll -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/nicescroll/nicescroll.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/nicescroll/nicescroll-demo.js"></script>

                <!-- include summernote css/js-->
                <link href="<?= JS_URL; ?>summernote/dist/summernote.css" rel="stylesheet" property="">
                <script src="<?= JS_URL; ?>summernote/dist/summernote.js"></script>
                <script src="<?= JS_URL; ?>summernote/lang/summernote-pt-BR.js"></script>

                <div class="row">
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>
                </div>
            </div>
        </div>
    </div>

    <!-- JS Demo -->
    <script type="text/javascript" src="<?= $this->request->webroot; ?>assets-minified/admin-all-demo.js"></script>

    <!-- Colorpicker -->
    <link rel="stylesheet" href="<?= $this->request->webroot; ?>assets/widgets/jquery-minicolors-master/jquery.minicolors.css">
    <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/jquery-minicolors-master/jquery.minicolors.min.js"></script>
    <script type="text/javascript" src="<?= $this->request->webroot; ?>js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>

    <!-- DateTime Picker -->
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>css/bootstrap-datetimepicker.min.css">
    <script type="text/javascript" src="<?= $this->request->webroot; ?>js/moment-with-locales.js"></script>
    <script type="text/javascript" src="<?= $this->request->webroot; ?>js/bootstrap-datetimepicker.min.js"></script>

</div>
</body>
</html>
