<?php

define('WEBROOT_URL',
    //$_SERVER['REQUEST_SCHEME'].'://'.
    'https://'.
    $_SERVER['HTTP_HOST'].
    str_replace('webroot/index.php', '', $_SERVER['PHP_SELF']));

define('IMG_URL',                   WEBROOT_URL.'img'.DS);
define('JS_URL',                    WEBROOT_URL.'js'.DS);
define('CSS_URL',                   WEBROOT_URL.'css'.DS);
define('FONT_URL',                  WEBROOT_URL.'fonts'.DS);

$link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?>

<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->

<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>LOGFITNESS - Ooops</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0;">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= WEBROOT_URL?>img/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= WEBROOT_URL?>img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <style>
        <?= file_get_contents(CSS_URL.DS.'bootstrap.min.css') ?>
        <?= file_get_contents(CSS_URL.DS.'bootstrap-theme.min.css') ?>
    </style>
    <?= $this->Html->css('main.min.css') ?>
    <?= $this->Html->css('media_queries.min.css') ?>

    <script>
        <?= file_get_contents(JS_URL.DS.'vendor/jquery-1.11.2.min.js') ?>
        <?= file_get_contents(JS_URL.DS.'vendor/modernizr-2.8.3-respond-1.4.2.min.js') ?>
    </script>


    
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script>
        var WEBROOT_URL  = "<?= WEBROOT_URL ?>";
    </script>

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">
    Você está utilizando um navegador obsoleto. Vitiste <a href="http://browsehappy.com/"> e atualize seu browser
    para uma melhor experiência web.
</p>
<![endif]-->

<?php header('Access-Control-Allow-Origin: *'); ?>

<style>
    .conteudo{
        text-align: center;
    }
    .botao{
        text-align: center;
    }
    .conteudo img{
        margin-top: 50px;
        margin-bottom: 20px;
    }
    .btn-inicio{
        border: 2px solid;
        border-color: #5087c7;
        background-color: white;
        color: #5087c7;
        text-transform: uppercase;
        padding: 3px 19px;
        font-family: nexa_boldregular;
    }
    @media screen and (max-width: 425px) {
        .conteudo img{
            width: 80%;
        }
    }
    @media (min-width: 768px) {
        .conteudo img{
            width: 55%;
            margin-top: 10px;
            
        }
    }
    @media (min-width: 1024px) {
        .conteudo img{
            width: 32%;
            margin-top: 10px;
            
        }
    }
</style>
<!-- PAGINA PADRÃO -->
<div class="conteudo text-center">
    <?= $this->Html->image('seloerro.png', ['alt' => 'LogFitness']); ?>
</div>
<div class="botao text-center">
    <?= $this->Html->link('Início',
    '/',
    ['class' => 'btn-inicio', 'escape' => false,]) ?>
</div>


<div class="col-sm-12" >
    <?= $this->Form->input('link', [
        'value'         => $link,
        'type'          => 'hidden'
    ])?>
</div>

<!-- Google Analytics -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = '//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-64522723-3', 'auto');
    ga('send', 'pageview');
</script>



</body>



</html>


