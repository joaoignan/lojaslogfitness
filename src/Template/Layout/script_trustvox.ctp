/*$pedidos_enviar = TableRegistry::get('Pedidos')
            ->find('all')
            ->where(['Pedidos.pedido_status_id' => 6])
            ->order(['Pedidos.id' => 'asc'])
            ->all();

        foreach ($pedidos_enviar as $pe) {
            $pedido_trust = $this->Pedidos->get($pe->id, [
                'contain' => ['Clientes', 'Academias', 'Transportadoras', 'PedidoStatus', 'PedidoItens',
                    'PedidoItens.Produtos',
                    'PedidoItens.Produtos.ProdutoBase', 'PedidoPagamentos']
            ]);

            $ch = curl_init();

            $header = [
                "Content-Type: application/json", 
                "Accept: application/vnd.trustvox.com; version=1",
                "Authorization: token tdBtMXo_byqscCJ2z2dd"
            ];

            $first_name =  explode(" ", $pedido_trust->cliente->name);

            $cliente_data = [
                "first_name" => $first_name[0],
                "last_name" => $first_name[count($first_name)-1],
                "email" => $pedido_trust->cliente->email
            ];

            $fotos_data = [0];
            $items_data = [0];

            foreach ($pedido_trust->pedido_itens as $pi) {
                $pi->produto->preco_promo ? $valor = $pi->produto->preco_promo : $valor = $pi->produto->preco;

                $fotos = unserialize($pi->produto->fotos);

                foreach($fotos as $foto):
                    $fotos_data[] = WEBROOT_URL.'img/produtos/md-'.$foto;
                endforeach;

                $items_data[] = [
                    "name" => $pi->produto->produto_base->name,
                    "id" => $pi->produto->id,
                    "url" => WEBROOT_URL.'produto/'.$pi->produto->slug,
                    "price" => $valor,
                    "prothos_urls" => $fotos_data
                ];
            }

            $data = [
                "order_id" => $pedido_trust->id,
                "delivery_date" => $pedido_trust->entrega,
                "client" => $cliente_data,
                "items" => $items_data
            ];

            $data = json_encode($data);

            $options = array(CURLOPT_URL => "http://trustvox.com.br/api/stores/71702/orders",
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HEADER => FALSE,
                CURLOPT_POST => TRUE,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => $header
            );

            curl_setopt_array($ch, $options);

            $response = curl_exec($ch);
            curl_close($ch);
        }*/