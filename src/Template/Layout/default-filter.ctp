<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="pt_BR"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="pt_BR"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="pt_BR"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt_BR"> <!--<![endif]-->

<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>LOGFITNESS <?php /*= $this->fetch('title')*/ ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0;">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= WEBROOT_URL?>img/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= WEBROOT_URL?>img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <?= $this->Html->css('academia_new_animate.min.css')?>
    <?= $this->Html->css('academia_new_bootstrap.min.css')?>
    <?= $this->Html->css('ninja-slider.css')?>
    <?= $this->Html->css('bootstrap.min.css')?>
    <?= $this->Html->css('bootstrap-theme.min.css')?>
    <?= $this->Html->css('ion.rangeSlider.css')?>
    <?= $this->Html->css('ion.rangeSlider.skinHTML5.css')?>
    <?= $this->Html->css('font-awesome.min.css')?>

    <?= $this->Html->script('ninja-slider.js')?>
    <?= $this->Html->script('vendor/modernizr-2.8.3-respond-1.4.2.min.js')?>
    <?= $this->Html->script('ion.rangeSlider.min.js')?>
        
    <?= $this->Html->css('main.css') ?>
    <?= $this->Html->css('media_queries.min.css') ?>
    <?php /*= $this->Html->script('vendor/modernizr-2.8.3-respond-1.4.2.min.js')*/ ?>

    <?php /*= $this->Html->meta('favicon')*/ ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script>
        // var HTTP_REFERER = "<?php //= $_SERVER['HTTP_REFERER'] ?>";
        var WEBROOT_URL  = "<?= WEBROOT_URL?>";
    </script>

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">
    Você está utilizando um navegador obsoleto. Vitiste <a href="http://browsehappy.com/"> e atualize seu browser
    para uma melhor experiência web.
</p>
<![endif]-->

<?php header('Access-Control-Allow-Origin: *'); ?>


<div id="fb-root"></div>


<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1321574044525013',
            xfbml      : true,
            version    : 'v2.7'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<style>
    .laterais {
        background-color: #5089cf; 
        width: 230px; 
        height: 100%; 
        position: fixed;
        z-index: 100;
    }
    .filtro {
        left: 0;
    }
</style>

<!-- Chamando o Navbar -->
<?= $this->request->is('mobile') ? $this->Element('navbar_mobile') : $this->Element('navbar', ['SSlug' => $SSlug]); ?>
<!-- Fim navbar -->
<div class="laterais filtro">
    <p> Filtro aqui</p>
</div>

<div class="loading text-center">
    <span class="helper"></span>
    <i class="fa fa-refresh fa-spin fa-3x fa-fw margin-bottom"></i>
    <span class="">Atualizando conteúdo...</span>
</div>

<?= $this->fetch('content') ?>

<?= !$this->request->isMobile() ? $this->Element('instagram') : ''; ?>

<?= $this->Element('footer'); ?>


<?= $this->Form->create(null)?>
<?= isset($_GET['redir']) ?
    $this->Form->input('redirect', [
        'id'            => 'redirect',
        'type'          => 'hidden',
        'value'         => $_GET['redir'],
    ]):
    $this->Form->input('redirect', [
        'id'            => 'redirect',
        'type'          => 'hidden',
        'value'         => 'home',
    ]);
?>
<?= $this->Form->end()?>

<?= $this->Html->css('star-rating.min.css') ?>
<?= $this->Html->css('validationEngine.jquery.custom.min.css') ?>
<?= $this->Html->css('bootstrap-multiselect.css') ?>
<?= $this->Html->css('jquery.fancybox.min.css') ?>
<?= $this->Html->css('nanoscroller.css') ?>
<?= $this->Html->css(JS_URL.'owl-carousel/owl.carousel.css') ?>
<?= $this->Html->css(JS_URL.'owl-carousel/owl.theme.css') ?>
<?= $this->Html->css('m4w-instagram') ?>

<script>

    <?= file_get_contents(JS_URL.'vendor/bootstrap.min.js') ?>
    <?= file_get_contents(JS_URL.'plugins.js') ?>
    <?= file_get_contents(JS_URL.'star-rating.min.js') ?>
    <?= file_get_contents(JS_URL.'star-rating_locale_pt-br.min.js') ?>
    <?= file_get_contents(JS_URL.'jquery.validationEngine.min.js') ?>
    <?= file_get_contents(JS_URL.'jquery.validationEngine-br.min.js') ?>
    <?= file_get_contents(JS_URL.'jquery.mask.min.js') ?>
</script>

<?= $this->Html->script('jquery.form.min.js') ?>
<?= $this->Html->script('jquery.countdown.min.js') ?>
<?= $this->Html->script('jquery-ui-1.11.4.custom/jquery-ui.min.js') ?>
<?= $this->Html->script('jquery.fancybox.min.js') ?>
<?= $this->Html->script('bootstrap-multiselect.js') ?>
<?= $this->Html->script('chosen.jquery.js') ?>
<?= $this->Html->script('jquery.nanoscroller.min.js') ?>
<?= $this->Html->script('owl-carousel/owl.carousel.js') ?>
<?= $this->Html->script('m4w-instagram') ?>

<?= $this->Html->script('main.js') ?>

<?= $this->fetch('css') ?>
<?= $this->fetch('script') ?>


<?php if(isset($_GET['oid']) && $_GET['oid'] > 0 && !isset($_GET['cid'])){ ?>
    <script>
        var loading = $('.loading');
        var grade_produtos  = $('#grade-produtos');
        loading.show(1);
        function get_produtos_objetivo(objetivo_id){
            $.get(WEBROOT_URL + 'produtos/filtro-objetivo/' + objetivo_id,
                function (data) {
                    $('html, body').animate({
                        scrollTop: grade_produtos.offset().top - 125
                    }, 700);

                    grade_produtos.html(data);
                    loading.hide(1);
                });
        }
        get_produtos_objetivo(<?= $_GET['oid']; ?>);
    </script>
<?php }?>


<?php if(isset($_GET['cid']) && $_GET['cid'] > 0){ ?>
    <script>
        var loading = $('.loading');
        var grade_produtos  = $('#grade-produtos');
        loading.show(1);
        function get_produtos_objetivo(objetivo, categoria){
            $.get(WEBROOT_URL + 'produtos/filtro-objetivo-categoria/' + objetivo + '/' + categoria,
                function (data) {
                    $('html, body').animate({
                        scrollTop: grade_produtos.offset().top - 125
                    }, 700);

                    grade_produtos.html(data);
                    loading.hide(1);
                });
        }
        get_produtos_objetivo(<?= $_GET['oid']; ?>, <?= $_GET['cid']; ?>);
    </script>
<?php }?>

<?php if(isset($_GET['m']) && $_GET['m'] > 0){ ?>
    <script>
        var loading = $('.loading');
        var grade_produtos  = $('#grade-produtos');
        loading.show(1);

        function get_produtos_marca(marca_id){
            $.get(WEBROOT_URL + '/produtos/filtro-marca/' + marca_id,
                function (data) {
                    $('html, body').animate({
                        scrollTop: grade_produtos.offset().top - 125
                    }, 700);
                    grade_produtos.html(data);
                    loading.hide(1);
                });
        }
        get_produtos_marca(<?= $_GET['m']; ?>);
    </script>
<?php }?>

<?php if(isset($_GET['busca']) && $_GET['busca'] != ''){ ?>
    <script>
        var loading = $('.loading');
        var grade_produtos  = $('#grade-produtos');
        loading.show(1);

        function search() {
            loading.show(1);

            $.ajax({
                type: "POST",
                url: WEBROOT_URL + '/produtos/busca/',
                data: {query: "<?= $_GET['busca']?>"}
            })
                .done(function (data) {
                    $('html, body').animate({
                        scrollTop: grade_produtos.offset().top - 125
                    }, 700);
                    grade_produtos.html(data);
                    loading.hide(1);
                });
        }
        search();
    </script>
<?php }?>

<!-- Google Analytics -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = '//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-64522723-3', 'auto');
    ga('send', 'pageview');
</script>

<?php if(isset($academias_map)): ?>
    <script>

        $(document).ready(function () {
            var map;
            var elevator;
            var myOptions = {
                zoomControl: true,
                scaleControl: false,
                scrollwheel: false,
                disableDoubleClickZoom: false,
                zoom: 4,
                center: new google.maps.LatLng(-15.7217175,-48.0783242),
                mapTypeId: 'terrain'
            };
            map = new google.maps.Map($('#map_canvas')[0], myOptions);

            <?php
            $academias_address  = '';
            $academias_info     = '';

            foreach($academias_map as $academia):
            $academias_address =
                "'"
                .$academia->address.','
                .$academia->number.','
                .$academia->area.','
                .str_replace("'", '´', $academia->city->name).','
                .$academia->city->uf
                ."'";

            $academias_info =

                str_replace("'", '´', $academia->shortname).' :: '.str_replace("'", '´', $academia->city->name).'-'.$academia->city->uf
            ;

            $pass = 0;

            if(!empty($academia->latitude) && !empty($academia->longitude)){
            ?>
            var latlng = new google.maps.LatLng(<?= $academia->latitude; ?>, <?= $academia->longitude; ?>);
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: "<?= $academias_info; ?>",
                icon:       WEBROOT_URL + 'img/map-marker.png'
                //shadow:     WEBROOT_URL + 'img/map-marker.png'
            });

            google.maps.event.addListener(marker, 'click', function () {
                //infowindow.open(map,marker);
                select_academia(<?= $academia->id; ?>);
            });

            <?php
            }else{
            //$pass = $pass + 100;
            /*if($pass >= 10) {
                //sleep(1);
                $pass = 0;
            }*/
            ?>
            //setTimeout(function () {
            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + <?= $academias_address; ?> +'&sensor=false',
                null,
                function (data) {
                    //console.log(data);
                    var p = [];
                    if(data.results[0].geometry.location){
                        p = data.results[0].geometry.location;
                    }else{
                        p = data.results[0]['geometry']['location'];
                    }

                    var latlng = new google.maps.LatLng(p.lat, p.lng);
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: "<?= $academias_info; ?>",
                        icon:       WEBROOT_URL + 'img/map-marker.png'
                        //shadow:     WEBROOT_URL + 'img/map-marker.png'
                    });

                    google.maps.event.addListener(marker, 'click', function () {
                        //infowindow.open(map,marker);
                        select_academia(<?= $academia->id; ?>);
                    });

                    $.ajax({
                        type: "POST",
                        url: WEBROOT_URL + "academia-cordinates-add/" + <?= $academia->id; ?> + '/' + p.lat + '/' + p.lng,
                        data: {}
                    }).done(function (data_academia) {
                        //console.log(data_academia);
                    });
                });
            //}, <?= $pass; ?>);
            <?php
            }
            endforeach;
            ?>
        });
    </script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyCba7qYdWhCMKwndRe8DC6TgHYq14jcRy4"></script>
<?php endif; ?>
</body>
</html>