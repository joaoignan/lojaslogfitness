<?php
$page_title = 'ADM Professor LogFitness';
?>
<!DOCTYPE html>
<html  lang="pt-br">
<head>

    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $page_title ?>
    </title>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>

    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>
        <?= $page_title ?>:
        <?= __(str_replace('Admin/', '', $this->fetch('title'))) ?>
    </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>assets/images/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>assets/images/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>assets/images/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>assets/images/icons/favicon-16x16.png">


    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/iconic/iconic.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/elusive/elusive.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/meteocons/meteocons.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/spinnericon/spinnericon.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/typicons/typicons.css">

    <!-- Google Analitcs do administrativo (Logfitness ADM) -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-64522723-4', 'auto');
      ga('send', 'pageview');
    </script>

    <!-- Smart Look -->
    <script type="text/javascript">
        window.smartlook||(function(d) {
        var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
        var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
        c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
        })(document);
        smartlook('init', '65dd1b31ade411a0f1f09a9524c2adf7c3ca5eb5');
    </script>
    <!-- End Smart Look -->

    <!-- JS Core -->
    <script type="text/javascript" src="<?= $this->request->webroot; ?>assets-minified/js-core.js"></script>

    <script type="text/javascript">
        var Controller  = "<?= $ControllerName; ?>";
        var Action      = "<?= $ActionName; ?>";
        var WEBROOT_URL = "<?= WEBROOT_URL; ?>";
    </script>

    <?= $this->Html->css('validationEngine.jquery.custom') ?>
    <?php //= $this->Html->css('simple-line-icons/css/simple-line-icons') ?>
    <?= $this->Html->css('jquery.fancybox') ?>
    <?= $this->Html->css('main') ?>
    <?= $this->Html->css('admin') ?>
    <?= $this->Html->css('admin-all-demo')?>
    <?= $this->Html->css('AdminLTE')?>
    <?= $this->Html->css('font-awesome.min') ?>

    <?= $this->Html->script('jquery.validationEngine-br') ?>
    <?= $this->Html->script('jquery.validationEngine.custom') ?>
    <?= $this->Html->script('jquery.fancybox') ?>
    <?= $this->Html->script('jquery.mask') ?>
    <?= $this->Html->script('form') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>


<body>

<?php if($adm_professor->aceite_termos == 0) { ?>

<style type="text/css">
    body{
      overflow: hidden;
    }
    .bloqueio {
      display: block;
      position: fixed;
      left: 0;
      top: 0%;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background-color: rgba(255,255,255,.7);

    }
    .content-loader{
      position: fixed;
      border-radius: 25px;
      left: 10%;
      top: 5%;
      width: 80%;
      height: 90%;
      z-index: 999999;
      background-color: white;
      text-align: center;
      border: 3px solid black;
    }
    .aviso{
      font-family: nexa;
    }
    .termos {
      width: 90%;
      height: 60%;
      margin-left: 5%;
      border: 1px solid;
      overflow-y: auto;
      padding: 5px;
    }
    .termos p{
      color: black;
      font-size: 16px;
    }
    .content-loader img{
      width: 500px;
      margin-top: 45px;
    }
    .content-loader i{
      color: #f4d637;
    }
    .content-loader h1{
      color: #5087c7;
      font-family: nexa;
      font-size: 40px;
    }
    .aviso p{
      color: #5087c7;
      font-family: nexa;
      font-size: 20px;
    }
    .aviso h1{
      font-family: nexa_boldregular;
    }
    .download{
      width: 90%;
      margin-left: 5%;
    }
    @media all and (max-width: 450px){
      .termos{
        height: 45%;
      }
      .pdf{
        width: 200px;
      }
    }
</style>

<div class="bloqueio">
    <div class="content-loader">
          <div class="aviso text-center">
            <h1>Atenção</h1>
            <p>Aceite online nossos termos de uso.</p>
          </div>
          <div class="termos text-left">
            <h4><b>CONTRATO DE COMERCIALIZAÇÃO, DISPONIBILIZAÇÃO E
            DISTRIBUIÇÃO DE PRODUTOS EM AMBIENTE VIRTUAL</b></h4>
            <br/>
            <br/>
            <p><b>1ª CONTRATANTE:</b></p>
            <br/>
            <p>CACIQUE  DIGITAL  REPRESENTAÇÕES  LTDA. ME,  pessoa  jurídica  de  direito  privado, inscrita no Cadastro Nacional de Pessoas Jurídicas sob o número 22.921.802/0001-77, com sede na Rua Antonio Munia, 141, Sala 01, Bairro Jardim Nazareth, CEP 15054-160, na cidade de São Jose do Rio Preto/SP, neste ato representada pelo senhor FELIPE  VALVERDE SCHUMAHER, brasileiro, solteiro, maior, empresário, portador do RG 45.184.620-5 SSP/SP e do CPF/MF 388.793.678-70, residente e domiciliado na Rua XV de Novembro, 3333, 9º andar,   Centro,   CEP 15010-110, na cidade de São José do Rio Preto/SP, doravante denominada de 1ª CONTRATANTE;</p>
            <br/>
            <br/>
            <p><b>2ª CONTRATANTE</b></p>
            <br/>
            <p>_______________________, pessoa jurídica de
                direito privado, inscrita no Cadastro Nacional de Pessoas Jurídicas sob o número _______
                __________, com sede na _________________________
                ________________, neste ato representada pelo
                senhor ___________, brasileiro, empresário,
                portador do RG ___________ e do CPF/MF ________,
                residente e domiciliado na __________,
                doravante denominada de 2ª CONTRATANTE;</p>
            <br/>
            <p><b>CONSIDERANDO QUE</b></p>
            <br/>
            <p>1. A 1ª CONTRATANTE é empresa especializada na criação, desenvolvimento e gestão
                de sistemas de comercialização de bens e serviços em ambiente virtual, ou seja, em
                ambiente eletrônico na rede mundial de computadores “Internet”, sendo
                responsável pela gestão, administração e operação do website de comércio
                eletrônico www.logfitness.com.br;
                </p>
                <p>2. A 1ª CONTRATANTE, através do site www.logfitness.com.br comercializa
                suplementos alimentares.
                </p>
                <p>A 1ª CONTRATANTE, através do site www.logfitness.com.br tem a intenção de
                comercializar produtos para os frequentadores de academias, centros de saúde e
                estabelecimentos correlatos;</p>
                <p>4. A 2ª CONTRATANTE é uma pessoa jurídica voltada para atividades desenvolvidas por
                academias, centros de saúde e estabelecimentos correlatos;</p>
                <p>5. A 1ª CONTRATANTE tem a intenção de entregar seus produtos apenas por intermédio
                da 2ª CONTRATANTE;</p>
                <p>6. A 1ª CONTRATANTE venderá, disponibilizará e comercializará seus produtos a
                pessoas vinculadas a academia, centros de saúde ou empresas de atividades
                correlatas, sendo certo que sempre seu cliente deverá indicar uma academia a qual
                o pediu ficará vinculado, sendo esta o ponto de entrega do mesmo;
                </p>
                <p>7. As partes desejam estabelecer as cláusulas e condições pelas quais serão regidas a
                presente parceria;</p>
                <p>8. A 2ª CONTRATANTE adere ao presente instrumento quando da assinatura (física ou
                eletrônica) do mesmo.</p>
                <br/>
                <p><b>Tem as partes, justo e contratado o presente Contrato de Comercialização,
                Disponibilização e Distribuição de Produtos em Ambiente Virtual, o qual se regerá pelas
                cláusulas e condições abaixo elencadas.
                </b></p>
                <br/>
                <br/>
                <p><b>DOS TERMOS UTILIZADOS NO CONTRATO:</b></p>
                <br/>
                <p>Cláusula 1ª – Termos utilizados no presente contrato:</p>
                <ul>
                  <li>Partes – quando utilizado fará menção a ambos os contratantes;</li>
                  <li>1ª CONTRATANTE – CACIQUE DIGITAL REPRESENTAÇÕES LTDA. ME. Empresa
                que disponibiliza e administra o site www.logfitness.com.br;</li>
                  <li>2ª CONTRATANTE – empresa atuante no seguimento de academias, centros de saúde
                ou estabelecimentos de atividade correlatas que divulgarão para seus usuários o site
                da 1ª CONTRATANTE para que seus usuários adquiram produtos junto ao referido site;</li>
                  <li>Personal Trainer – profissional atuante em academias, centros de saúde,
                estabelecimentos de atividades correlatas ou que de maneira autônoma atuam junto
                a outras pessoas visando a saúde e o bem estar de referidas pessoas com a
                realização de exercícios físicos.</li>
                  <li>Produtos – são os suplementos disponibilizados para a venda pela 1ª
                CONTRATANTE;</li>
                  <li>Local de entrega – local onde a 1ª CONTRATANTE disponibilizará os produtos para a </li>
                  <li>Contrato – significa este contrato de comercialização, disponibilização e
                distribuição de produtos em ambiente virtual;</li>
                  <li>Adesão – significa a concordância e anuência do 2ª CONTRATANTE aos termos deste
                contrato, bem como de outros instrumentos contratuais vinculados a este
                instrumento, que se dará pela assinatura do respectivo contrato ou termo de adesão;</li>
                  <li>Clique – É o ato materializador da vontade de um internauta adquirir um
                determinado produto ou serviço ofertado no site, cujo ato é executado ao pressionar
                o botão esquerdo do mouse de um PC ou seu equivalente em outro tipo de
                computador ou smartphone;
                </li>
                  <li>Formas de Pagamento – são as formas de pagamento disponibilizadas pela 1ª
                CONTRATANTE junto ao site www.logfitness.com.br e que poderão ser utilizadas
                pelos usuários e clientes para pagamento dos produtos ofertados pela 1ª
                CONTRATANTE;</li>
                  <li>Pagamentos – são os montantes pagos pelos clientes decorrentes da compra de
                produtos;
                </li>
                  <li>MOIP – é um serviço de captura, processamento, roteamento, liquidação e gestão
                de pagamentos.
                </li>
                  <li>Internet – significa a rede mundial aberta de computadores interligados entre si;
                </li>
                  <li>Internauta – significa aquela pessoa que navega pela internet;</li>
                  <li>Cliente – é a pessoal que vai adquirir um produto disponibilizado pela 1ª
                CONTRATANTE;
                </li>
                  <li>Marketplace – significa o ambiente virtual criado, desenvolvido e gerido pela 1ª
                CONTRATANTE para disponibilização dos recursos e ferramentas, incluindo o site
                www.logfitness.com.br destinado à comercialização de produtos pela 1ª
                CONTRATANTE;</li>
                  <li>Comissão – é o montante a ser pago pela 1ª CONTRATANTE à 2ª CONTRATANTE pela
                indicação do site aos clientes a ela vinculados, bem como pelo recebimento e
                disponibilização do produto ao cliente;
                  <li>Comissão – é o montante a ser pago pela 1ª CONTRATANTE à 2ª CONTRATANTE pela
                indicação do site aos clientes a ela vinculados, bem como pelo recebimento e
                disponibilização do produto ao cliente;</li>
                  <li>Site – é o endereço eletrônico desenvolvido e gerenciado pela 1ª CONTRATANTE e
                utilizado para disponibilização dos produtos aos clientes vinculados à 2ª
                CONTRATANTE;</li>
                  <li>Termo de Adesão – significa o instrumento contratual específico celebrado, em
                meio físico ou eletrônico, entre as partes para regular as condições comerciais
                negociadas, mediante qual a 2ª CONTRATANTE adere integralmente aos termos deste
                contrato ou de qualquer outro documento vinculado a ele;</li>
                </ul>
                <br/>
                <p><b>DO OBJETO DO CONTRATO</b></p>
                <br/>
                <p>Cláusula 2ª – O presente instrumento particular de contrato tem por objeto estabelecer os
                termos e condições mediante os quais será formada a parceria entre as partes para a
                comercialização, disponibilização e distribuição dos produtos vendidos pela 1ª
                CONTRATANTE junto ao site www.logfitness.com.br.
                </p>
                <br>
                <p>Parágrafo Primeiro – A comercialização, disponibilização e distribuição dos produtos
                vendidos pela 1ª CONTRATANTE junto ao site www.logfitness.com.br somente ocorrerão em
                território brasileiro, sendo certo que não haverá entrega direta do produto aos clientes,
                apenas sendo os produtos disponibilizados aos clientes através do endereço físico da 2ª
                CONTRATANTE</p>
                <br>
                <p>Parágrafo Segundo – O presente contrato não tem por objeto a distribuição ou
                representação comercial pela 1ª CONTRATANTE de produtos ou serviços vendidos,
                fornecidos ou distribuídos pela 2ª CONTRATANTE mas tão somente a venda, disponibilização
                e distribuição de produtos de complemento alimentar, vendidos, fornecidos e
                disponibilizados pela própria 1ª CONTRATANTE junto a seu site.</p>
                <br>
                <p>Parágrafo Terceiro – Serão vendidos, disponibilizados e distribuídos pela 1ª CONTRATANTE
                os produtos constantes no site www.logfitness.com.br, nos valores e condições previstos
                no próprio site, não podendo a 2ª CONTRATANTE oferecer outros produtos aos clientes em
                condições diferentes das previstas no site.
                </p>
                <br>
                <p><b>DAS DISPOSIÇÕES CONTIDAS NO SITE</b></p>
                <br>
                <p>Cláusula 3ª – Os preços, condições de pagamento, prazo de entrega, forma de entrega,
                descrição do produto e outras informações sobre os produtos vendidos constarão do site
                www.logfitness.com.br, devendo o cliente ou usuário acessar o site e verificar todas as
                condições de venda do produto antes de clicar para adquirir o mesmo.
                </p>
                <br>
                <p>Parágrafo Primeiro – Todas as disposições contidas no site www.logfitness.com.br com
                relação ao produto, prazos, formas de pagamento, formas de entrega ou outros vincularão
                as partes, clientes, usuários ou consumidores, não podendo nenhuma das partes alegar
                desconhecimento sobre tudo o quanto está contido no site.</p>
                <br>
                <p>Parágrafo Segundo – As partes declaram ter ciência das informações contidas no site
                www.logfitness.com.br.</p>
                <br>
                <p><b>DA ADESÃO DA 2ª CONTRATANTE:</b></p>
                <br>
                <p>Cláusula 4ª – A 2ª CONTRATANTE, por meio da celebração do presente contrato ou do termo
                de adesão a ele vinculado, adere, de maneira geral e irrestrita, às condições estabelecidas
                neste contrato, obrigando em todos os seus termos.
                </p>
                <br>
                <p>Parágrafo Único – A adesão ocorrerá por meio da assinatura física ou eletrônica do
                presente contrato ou do termo de adesão a ele vinculado.</p>
                <br>
                <br>
                <p>Cláusula 5ª – Após a assinatura do termo de adesão ou do presente contrato de forma
                física ou eletrônica, a 2ª CONTRATANTE deverá encaminhar à 1ª CONTRATANTE seu contrato
                social, comprovante do endereço de sua sede, informação através de declaração expressa
                do representante legal que representará a 2ª CONTRATANTE junto a este contrato e,
                consequentemente, junto a 1ª CONTRATANTE, bem como qualquer outro documento que seja
                requerido pela 1ª CONTRATANTE após análise prévia dos documentos já solicitados.
                </p>
                <br>
                <p>Parágrafo Primeiro – Os documentos solicitados pela 1ª CONTRATANTE deverão ser
                encaminhados à mesma em até 05 (cinco) dias úteis contados da solicitação efetivada</p>
                <br>
                <p>Parágrafo Segundo – Caso a 2ª CONTRATANTE não encaminhe os documentos requeridos
                pela 1ª CONTRATANTE, a 1ª CONTRATANTE poderá rescindir o presente contrato de pleno
                direito, excluindo a 2ª CONTRATANTE de sua base de dados junto ao site
                www.logfitness.com.br.</p>
                <br>
                <br>
                <p>Cláusula 6ª – A efetivação do presente contrato está condicionada a aceitação do cadastro
                da 2ª CONTRATANTE, sendo que a análise e aprovação do respectivo cadastro será realizado
                pela 1ª CONTRATANTE, que poderá, a seu exclusivo critério, aceitar ou não a adesão proposta
                pela 2ª CONTRATANTE.</p>
                <br>
                <br>
                <p><b>DOS DIREITOS E OBRIGAÇÕES DAS PARTES:</b></p>
                <br>
                <p>Cláusula 7ª – As partes se obrigam a respeitar as cláusulas e condições do presente
                contrato e de todos os documentos que venham a ser firmados entre as partes em razão da
                relação contratual que aqui se estabelece</p>
                <br>
                <p>Cláusula 8ª – A 2ª CONTRATANTE se obriga a:</p>
                  <ul>
                    <li>Manter seus dados cadastrais devidamente atualizados;</li>
                    <li>Responsabilizar-se civil e criminalmente por todas as informações, inclusive
                cadastrais fornecidas à 1ª CONTRATANTE, bem como aos usuários e clientes,
                comprometendo-se a atualizá-las sempre que houver alguma alteração;</li>
                    <li>Disponibilizar ponto de recebimento dos produtos enviados pela 1ª CONTRATANTE
                com funcionário apto a receber e armazenar os produtos enviados pela 1ª
                CONTRATANTE;</li>
                    <li>Disponibilizar funcionário capacitado para receber os produtos enviados pela 1ª
                CONTRATANTE;</li>
                    <li>Disponibilizar funcionário capacitado para realizar a entrega dos produtos enviados
                pela 1ª CONTRATANTE para seu destinatário final;</li>
                    <li>Formalizar com o destinatário final do produto (cliente ou usuário) termo de
                entrega e aceite do produto, constando neste o produto que foi entregue o dia e
                horário da retirada do produto pelo destinatário final (cliente);</li>
                    <li>Caso exista algum vício ou defeito no produto, providenciar o recolhimento para
                remeter o produto defeituoso ou viciado para a 1ª CONTRATANTE;</li>
                    <li>Divulgar o site da 1ª CONTRATANTE para seus frequentadores, clientes ou outros,
                proporcionando o crescimento do negócio;</li>
                  </ul>
                <br>
                <p>Cláusula 9ª – A 1ª CONTRATANTE se obriga a:</p>
                  <ul>
                    <li>Disponibilizar junto ao site www.logfitness.com.br vasta gama de produtos de
                complemento ou suplemento alimentar;</li>
                    <li>Fornecer junto ao site www.logfitness.com.br todas as informações necessárias
                para a venda dos produtos, incluindo informações sobre o produto, preço,
                condições de pagamento, forma de pagamento, prazo de entrega, forma de entrega
                ou outro;</li>
                    <li>Não colocar a venda produtos de natureza ilícita que atente ou possa atentar contra
                a dignidade humana, os bons costumes, as práticas normais de comércio, o direito a
                livre concorrência, atentem ou possam atentar contra direitos de terceiros, sejam
                considerados falsos ou falsificados, não possuam as devidas autorizações, sejam
                defeituosos ou impróprios para consumo, violem as normas aplicáveis ou de
                qualquer maneira ou forma estejam impedidos de serem comercializados por lei,
                estatuto ou norma de ordem pública ou privada;</li>
                    <li>Definir as condições comerciais a serem observadas na comercialização dos
                produtos;</li>
                    <li>Processar o pedido do usuário ou cliente no prazo máximo de 48 (quarenta e oito)
                horas úteis devendo ainda observar os prazos e condições de entrega divulgado no
                site e previamente definido.</li>
                    <li>Enviar os produtos à 2ª CONTRATANTE no prazo estabelecido no site, podendo
                referido envio ser realizado através dos correios ou de 
                Página 7 de 14
                transportadora, observando-se os prazos inerentes ao serviço;
                </li>
                    <li>Abster-se de cancelar injustificadamente o pedido realizado pelo usuário e, caso o
                cancelamento seja inevitável, por motivo de caso fortuito ou força maior, informar
                à 2ª CONTRATANTE, bem como ao usuário ou cliente, procedendo-se ao
                cancelamento do pagamento.</li>
                    <li>Embalar e empacotar o produto de maneira adequada e compatível para envio à 2ª
                CONTRATANTE, de forma a manter a sua integridade durante o transporte.
                </li>
                    <li>Enviar uma via física da Nota Fiscal Eletrônica junto com o produto;</li>
                    <li>Cumprir eventual política de troca e devoluções de mercadorias constantes junto ao
                site;</li>
                    <li>Pagar a comissão à 2ª CONTRATANTE no prazo de 30 dias da entrega do produto;</li>
                  </ul>
                <br>
                <p>Cláusula 10ª – São direitos da 2ª CONTRATANTE:</p>
                  <ul>
                    <li>Receber a comissão estabelecida no presente contrato;</li>
                    <li>Requerer relatório de vendas de produtos vinculados apenas a 2ª CONTRATANTE</li>
                  </ul>
                <br>
                <p>Cláusula 11ª – São direitos da 1ª CONTRATANTE:</p>
                  <ul>
                    <li>Receber os valores integrais pela venda dos produtos disponibilizados no site.</li>
                    <li>Rescindir o presente contrato caso se torne excessivamente oneroso, difícil ou
                impossível o envio e recebimento dos produtos à 2ª CONTRATANTE;</li>
                    <li>Sem prejuízo das medidas legais cabíveis, a qualquer tempo, advertir, suspender ou
                cancelar, temporária ou definitivamente, a eficácia deste contrato caso a 2ª
                CONTRATANTE infrinja as suas disposições ou as disposições do termo de adesão ou
                outro.</li>
                    <li>Caberá à 1ª CONTRATANTE, única e exclusivamente, o processamento de todos os
                pagamentos, repasses, retenções, encargos, estornos e ajustes em razão das
                operações concretizadas através de seu site.</li>
                  </ul>
                <br>
                <br>
                <p><b>DA GESTÃO DE PAGAMENTOS:</b></p>
                <br>
                <br>
                <p>Cláusula 12ª – A 1ª CONTRATANTE se compromete a providenciar as ferramentas
                necessárias para disponibilização das formas de pagamento aos usuários ou clientes,
                respondendo pela contratação de quaisquer serviços ou mecanismos para controle e
                validação dos pagamentos.</p>
                <br>
                <p>Cláusula 13ª – A 1ª CONTRATANTE não está obrigada a aceitar forma de pagamento distinta
                das disponibilizadas no site www.logfitness.com.br</p>
                <br>
                <p>Cláusula 14ª – As transações realizadas através do site disponibilizado pela
                1ª CONTRATANTE – www.logfitness.com.br - serão processadas pelo MOIP.</p>
                <br>
                <p>Parágrafo Primeiro – O MOIP é um serviço de captura, processamento, roteamento,
                liquidação e gestão de pagamentos.</p>
                <br>
                <p>Parágrafo Segundo – Antes do cliente ou usuário realizar compras junto ao site
                www.logfitness.com.br, o cliente ou usuário deverá acessar o site do MOIP
                (www.moip.com.br) e consultar o Contrato de Uso dos Serviços MOIP (“CONTRATO
                MOIP”).</p>
                <br>
                <p>Parágrafo Terceiro – Ao usar o site www.logfitness.com.br, o cliente ou usuário estará
                automaticamente aderindo ao CONTRATO MOIP, com os direitos e deveres definidos
                pelo MOIP em seu contrato público; para todos os efeitos, a assinatura deste contrato
                equipara o cliente ou usuário a um cliente MOIP;</p>
                <br>
                <p>Parágrafo Quarto – A responsabilidade pela realização da transação será exclusiva do
                MOIP de acordo com as regras e limitações previstas no CONTRATO MOIP;
                </p>
                <br>
                <p>Parágrafo Quinto – Os valores das tarifas devidas ao MOIP estão descritos em seu site
                (www.moip.com.br) e poderão ser alterados na forma e condições do CONTRATO MOIP.</p>
                <br>
                <p>Parágrafo Sexto – Para garantir a integridade das transações, os dados fornecidos ao site
                www.logfitness.com.br serão compartilhados com o MOIP.</p>
                <br>
                <p>Parágrafo Sétimo – O MOIP se responsabiliza pela segurança e sigilo dos dados do cliente
                ou usuário a que tiver acesso</p>
                <br>
                <p>Parágrafo Oitavo – O MOIP não dará tratamento aos dados do cliente ou usuário para fins
                diferentes daqueles estabelecidos neste contrato e no CONTRATO MOIP.</p>
                <br>
                <p>Parágrafo Nono – É vedado ao cliente ou usuário usar, oferecer, divulgar e, de qualquer
                forma, favorecer outros serviços de pagamento que não o MOIP.</p>
                <br>
                <br>
                <p><b>DA COMISSÃO DA 2ª CONTRATANTE</b></p>
                <br>
                <p>Cláusula 15ª – A 1ª CONTRATANTE, repassara mensalmente à 2ª CONTRATANTE valores a
                titulo de comissão para a que ofereçam e instruam aos seus clientes os melhores produtos
                de acordo com a necessidade de cada um, indicando o site da 1ª CONTRATANTE para a
                aquisição de referidos produtos.</p>
                <br>
                <p>Cláusula 16ª – Para cada venda realizada pela 1ª CONTRATANTE para usuários ou clientes
                vinculados à 2ª CONTRATANTE através das ferramentas específicas descritas e
                disponibilizadas no site www.logfitness.com.br e indicados por esta para aquisição de
                produtos junto ao site da 1ª CONTRATANTE, a 1ª CONTRATANTE pagará à 2ª CONTRATANTE
                comissão no valor correspondente a 20% (vinte por cento) do valor da venda realizada</p>
                <br>
                <p>Parágrafo Primeiro – O pagamento da comissão informada no caput da cláusula 16ª será
                realizado em até 30 dias contados da entrega do produto ao destinatário final.</p>
                <br>
                <p>Parágrafo Segundo – Quando concorrerem para a realização da venda a 2ª CONTRATANTE e
                um personal trainer a ela vinculado (professor), a comissão descrita no caput de 20% será
                dividida entre 2ª CONTRATANTE e o personal trainer, sendo pago pela 1ª CONTRATANTE 10%
                do valor da venda para cada um.</p>
                <br>
                <p>Parágrafo Terceiro – A 2ª CONTRATANTE deverá fornecer por meio da área administrativa
                disponibilizada pela 1ª CONTRATANTE junto ao site www.logfitness.com.br seus dados
                bancários para que a 1ª CONTRATANTE realize o depósito do valor da comissão, fornecendolhe
                planilha das vendas concretizadas.</p>
                <br>
                <p>Parágrafo Quarto – Também deverão ser fornecidos os dados do personal trainer
                (professor) que concorreu para a venda, sendo certo que a 1ª CONTRATANTE fará o
                pagamento separadamente para cada um, nos termos das disposições contidas nesta
                cláusula</p>
                <br>
                <p>Parágrafo Quinto – A 2ª CONTRATANTE compromete-se a manter os dados bancários
                sempre atualizados para o recebimento de suas comissões.</p>
                <br>
                <p>Parágrafo Sexto – Caso o produto adquirido seja devolvido nos termos da legislação
                vigente, a comissão poderá ser estornada ou cancelada ou, caso a mesma já tenha sido
                paga, deverá ser devolvida pela 2ª CONTRATANTE à 1ª CONTRATANTE em até 05 (cinco) dias
                úteis da devolução do produto.</p>
                <br>
                <p>Parágrafo Sétimo – A 2ª Contratante concorda que as comissões serão calculadas com
                base nas operações concretizadas, descontados eventuais remunerações, encargos ou
                impostos devidos.</p>
                <br>
                <br>
                <p><b>DO PONTO DE ENTREGA</b></p>
                <br>
                <p>Cláusula 17ª – A 2ª CONTRATANTE, a partir da assinatura do presente contrato, será
                considerada como ponto de recepção e retirada dos produtos aqui vendidos pela 1ª
                CONTRATANTE.</p>
                <br>
                <p>Parágrafo Único – O referido serviço será remunerado através da comissão recebida e
                prevista na cláusula 16ª, não lhe sendo devido qualquer outro tipo de valor pelo mesmo.
                </p>
                <br>
                <p>Cláusula 18ª – Nenhum produto vendido pela 1ª CONTRATANTE será entregue diretamente
                ao usuário ou cliente, também não sendo enviado ao endereço do cliente ou usuário, sendo
                certo que os produtos apenas poderão ser retirados junto ao endereço da 2ª CONTRATANTE e
                mediante a assinatura de termo de recebimento fornecido por esta.
                </p>
                <br>
                <p>Cláusula 19ª – Deste modo, fica desde já acordado que o único modo de entrega do
                produto é através da retirada junto ao endereço da 2ª CONTRATANTE.</p>
                <br>
                <p>Cláusula 20ª – As obrigações com relação a entrega do produto ao cliente ou usuário estão
                descrita nas obrigações gerais da 2ª CONTRATANTE, devendo esta zelar para a perfeita
                entrega dos produtos aos clientes e usuários finais.</p>
                <br>
                <p><b>DO FATURAMENTO E ENVIO DE NOTA FISCAL</b></p>
                <br>
                <p>Cláusula 21ª – A 1ª CONTRATANTE faturará diretamente para o usuário ou cliente o produto
                adquirido, não devendo realizar faturamento em nome da 2ª CONTRATANTE, a não ser que
                esta adquira em nome próprio os produtos.</p>
                <br>
                <p>Cláusula 22ª –O faturamento e cobrança dos valores ora transacionados será feita
                diretamente entre os clientes e a 1ª CONTRATANTE.</p>
                <br>
                <p><b>DA DURAÇÃO DO PRESENTE CONTRATO:</b></p>
                <br>
                <p>Cláusula 23ª – O presente instrumento é firmado a prazo indeterminado, ficando desde já
                consignado que qualquer uma das partes pode exercer o direito de desistência ou renúncia
                do presente instrumento, desde que o faça de forma expressa com antecedência mínima de
                30 (trinta) dias.</p>
                <br>
                <p>Parágrafo Único – Durante o prazo de aviso prévio e após a extinção do contrato, as partes
                responderão pelos pedidos já concretizados na forma das disposições constantes no
                presente contrato.</p>
                <br>
                <p><b>DAS DISPOSIÇÕES FINAIS.</b></p>
                <br>
                <p>Cláusula 24ª – A 1ª CONTRATANTE poderá alterar, a qualquer tempo e de maneira unilateral
                as condições previstas neste contrato, no termo de uso do site ou nas condições comerciais
                gerais vigentes e previstas no site.</p>
                <br>
                <p>Cláusula 25ª – A alteração de qualquer condição, inclusive de natureza comercial, será
                precedida de comunicação escrita nos termos deste contrato, com antecedência mínima de
                15 (quinze) dias, desde que não constem no site www.logfitness.com.br, ocasião em que
                serão consideradas como conhecidas pelas partes.
                </p>
                <br>
                <p>Cláusula 26ª – Este contrato poderá ser rescindido no caso de pedido ou decretação de
                falência ou recuperação judicial de uma das partes.</p>
                <br>
                  <p>Cláusula 27ª – Também poderá ser rescindido o contrato caso uma das partes viole
                quaisquer das cláusulas aqui constantes.</p>
                <br>
                <p>Cláusula 28ª – O profissional atuante na área de atuação da 2ª CONTRATANTE (personal
                trainer) deverá estar vinculado a uma pessoa jurídica contratante, sendo certo que quando
                ele estiver vinculado a pessoa jurídica contratante e concorrer para a venda dos produtos da
                1ª Contratante, a ele será devida comissão, nos termos da cláusula 16ª.
                </p>
                <br>
                <p>Cláusula 29ª – Caso a 2ª CONTRATANTE não realize a entrega dos produtos ao destinatário
                final, responderá junto a 1ª CONTRATANTE pelo valor integral dos produtos, frete, além de
                multa de 25% (vinte e cinco por cento) calculada sobre o valor dos produtos enviados no
                mês à mesma, sem prejuízo de eventual perdas e danos imputados à 1ª CONTRATANTE.</p>
                <br>
                <p>Cláusula 30ª – As partes se comprometem, mutuamente, a zelar pela manutenção do sigilo
                de todos os segredos comerciais, conhecimentos técnicos e outras informações que venham
                a tomar conhecimento uma da outra em razão do presente contrato, não podendo usar
                quaisquer dessas informações confidenciais, a não ser quando expressamente autorizadas
                para tanto por seu titular. Nesse sentido, cada parte deverá envidas seus melhores esforços
                a fim de que seus sócios, empresas afiliadas, administradores, prepostos, empregados e/ou
                quaisquer outras pessoas sob sua responsabilidade (direta ou indireta) mantenham em
                sigilo todos os termos e condições do presente instrumento contratual.</p>
                <br>
                <p>Parágrafo Primeiro – As partes concordam que descrições de produtos, preços, condições
                de pagamento, formas de entrega e outras informações relacionadas aos produtos não são
                informações confidenciais.</p>
                <br>
                <p>Parágrafo Segundo – As partes concordam que as informações do presente contrato são
                estritamente confidenciais e que não serão divulgadas a quaisquer indivíduos, sociedades 
                Página 12 de 14
                ou instituições, a não ser que seja necessária para ajuizamento de processo judicial ou
                defesa dos interesses das partes.</p>
                <br> 
                <p>Cláusula 31ª – A 1ª CONTRATANTE, sem prejuízo de outras disposições contidas no presente
                contrato, está autorizada a utilizar a título gratuito e sem qualquer tipo de contraprestação
                os nomes comerciais e marcas registradas ou não da 2ª Contratante, disponibilizando
                referidas marcas no site e demais ferramentas de propriedade da 1ª CONTRATANTE na rede
                mundial de computadores.</p>
                <br>
                <p>Parágrafo Primeiro – A 2ª CONTRATANTE somente poderá utilizar as marcas e nomes
                comerciais da 1ª CONTRATANTE, mediante autorização escrita e expressa desta.</p>
                <br>
                <p>Parágrafo Primeiro – Cada parte reconhece que a utilização que venha a fazer das marcas
                da outra parte não criará para si, nem representará que esta tenha qualquer direito,
                titularidade ou participação sobre essas marcas, ou com relação a elas</p>
                <br>
                <p>Cláusula 32ª – A 2ª CONTRATANTE reconhece que toda a propriedade intelectual no que
                tange o desenvolvimento técnico do sistema de vendas da 1ª CONTRATANTE, incluindo todas
                as ferramentas ali disponibilizadas ou que venham a ser criadas e disponibilizadas são de
                propriedade exclusiva da 1ª CONTRATANTE.</p>
                <br>
                <p>Cláusula 33ª – Este contrato não estabelece nenhum caráter de exclusividade entre as
                partes.</p>
                <br>
                <p>Cláusula 34ª – As comunicações serão realizadas nos seguintes endereços de e-mail:</p>
                <p>Pela 1ª CONTRATANTE: __________.</p>
                <p>Pela 2ª CONTRATANTE: __________.</p>
                <br>
                <p>Cláusula 35ª – As partes garantem que possuem capacidade jurídica para celebrar o
                presente contrato.</p>
                <br>
                <p>Cláusula 36ª – As partes declaram que leram e estão cientes e de pleno acordo com todos
                os termos e condições deste contrato, com as condições especificadas no site
                www.logfitness.com.br, no termo de adesão e em qualquer outro documento que venha a
                ser firmado.</p>
                <br>
                <p>Cláusula 37ª – É facultada a 1ª Contratante, a qualquer tempo, a alteração, adequação ou 
                Página 13 de 14
                remodelação do site www.logfitness.com.br, inclusive layout, funcionalidades, ferramentas
                ou outros.</p>
                <br>
                <p>Cláusula 38ª – A 1ª CONTRATANTE poderá, conforme seu exclusivo critério, restringir,
                limitar ou impedir por qualquer meio ou forma, o acesso de um ou mais usuários ou
                clientes ao site.</p>
                <br>
                <p>Cláusula 39ª – A 1ª CONTRATANTE poderá impor limites para que um mesmo usuário ou
                cliente adquira um volume muito grande de um mesmo produto.</p>
                <br>
                <p>Cláusula 40ª – A 1ª CONTRATANTE poderá efetuar retenção da comissão caso inicie uma
                auditoria e encontre alguma inconformidade quanto a veracidade de informações enviadas
                pela 2ª CONTRATANTE.</p>
                <br>
                <p>Cláusula 41ª – A 1ª CONTRATANTE realizará a cobrança dos pagamentos na forma prevista
                no momento da concretização da operação pelo usuário ou cliente, não se obrigando a
                aceitar formas de pagamento diversas daquelas disponibilizadas e aceitas no momento da
                concretização da compra, podendo ainda se recusar a aceitar descontos, cupons, vales, etc.</p>
                <br>
                <p>Cláusula 42ª – O não exercício por qualquer das partes de quaisquer direitos ou faculdades
                que lhes sejam conferidos por este contrato ou pela lei, bem como eventual tolerância
                contra infrações contratuais cometidas pela outra parte, não importará em renúncia a
                qualquer dos seus direitos contratuais ou legais, novação ou alteração de cláusulas deste
                contrato, podendo a parte, a seu exclusivo critério, exercê-los a qualquer momento.</p>
                <br>
                <p>Cláusula 43ª – Se em decorrência de qualquer decisão judicial irrecorrível, qualquer
                disposição ou termo deste contrato for sentenciado nulo ou anulável, tal nulidade ou
                anulabilidade não afetará as demais cláusulas deste contrato, as quais permanecerão em
                pleno vigor, obrigando as partes.</p>
                <br>
                <p>Cláusula 44ª – Este contrato não gera entre as partes nenhuma obrigação de natureza
                trabalhista, sendo certo que os funcionários das partes ficarão vinculados apenas e
                exclusivamente a seus empregadores.</p>
                <br>
                <p>Cláusula 45ª – Cada parte se responsabiliza pelo pagamento de todo e qualquer tributo que 
                Página 14 de 14
                incida ou venha a incidir na consecução do objeto do presente contrato, tendo em vista a
                atividade realizada por cada um.</p>
                <br>
                <p>Cláusula 46ª – Caso qualquer das partes, para a conservação de seus direitos, venha a
                recorrer a via judicial, poderá exigir da outra, além dos valores pecuniários que lhe forem
                devidos nos termos deste contrato, todas as despesas judiciais a que tenha incorrido e,
                ainda, honorários advocatícios.</p>
                <br>
                <p>Cláusula 47ª – Fica definido que em caso de questões decorrentes do presente
                instrumento, o foro desta comarca de São Jose do Rio Preto/SP, para dirimir se necessário
                for.</p>
                <br>
                <p>As partes assinam o presente termo conferindo ao mesmo toda a força de veracidade e
                legitimidade do conteúdo avençado, firmando ao final o presente termo, para que produza
                os devidos efeitos legais de praxe.
                </p>
        <br>
        <p class="text-right">São José do Rio Preto/SP, 07 de janeiro de 2017.</p>

      </div>
      <div class="download text-right">
        <?= $this->Html->link('Baixe os Termos',
            '/files'.DS.'Termos-de-Aceite.pdf',
            ['target' => '_blank'])
          ;?> 
      </div>
      <?= $this->Form->create($professor, ['id' => 'form-aceito-termos', 'type' => 'file']); ?>
      <div class="aceito">
        <?= $this->Form->input('aceite_termos', [
            'type'      => 'checkbox',
            'class'     => 'checkbox-termos',
            'checked'   => true,
            'label'     => "Li e aceito os novos termos de uso",
            'div'       => false,
        ])?>
      </div>
      <div class="botao-aceito">
        <?= $this->Form->button('Enviar', [
            'id'            => 'btn-aceito-termos',
            'type'          => 'submit',
            'class'         => 'btn btn-success'
        ])?>
      </div>
      <?= $this->Form->end() ?>
    </div>
</div>
<?php } ?>
<div id="sb-site">
    <div id="loadingg">
        <div class="spinnner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-blue-cacique font-inverse">
            <div id="mobile-navigation">
                <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                <a href="index.html" class="logo-content-small" title="Max Adm"></a>
            </div>
            <div id="header-logo" class="bg-white">
                <br>
                <span class="helper"></span>
                <?= $this->Html->image('logfitness.png'); ?>
                <a id="close-sidebar" href="#" title="Esconder menu">
                    <i class="glyph-icon icon-angle-left"></i>
                </a>
            </div>
            <div id="header-nav-left">
                <div class="user-account-btn dropdown">
                    <a href="#" title="Minha Conta" class="user-profile clearfix" data-toggle="dropdown">
                        <?php if(isset($adm_professor->image)) {
                            echo $this->Html->image('professores/'.$adm_professor->image, ['alt' => __('Profile image'), 'style' => 'max-width:28px; max-height:28px;']);
                        } else {
                            echo $this->Html->image('professores/default.png', ['alt' => __('Profile image'), 'style' => 'max-width:28px; max-height:28px;']);
                        } ?>
                        <span style="width: auto;"><?= $adm_professor->name ?></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-left">
                        <div class="box-sm">
                            <div class="login-box clearfix">
                                <div class="user-img">
                                    <?php if(isset($adm_professor->image)) {
                                        echo $this->Html->image('professores/'.$adm_professor->image, ['alt' => __('Profile image'), 'style' => 'max-width:128px; max-height:128px;']);
                                    } else {
                                        echo $this->Html->image('professores/default.png', ['alt' => __('Profile image'), 'style' => 'max-width:128px; max-height:128px;']);
                                    } ?>
                                </div>
                                <div class="user-info">
                                    <span>
                                        <?= $adm_professor->name ?>
                                        <i>Professor</i>
                                    </span>
                                </div>
                            </div>
                            <div class="divider"></div>

                            <ul class="reset-ul mrg5B">
                                <li>
                                    <?= $this->Html->link(
                                        __('Change Password').'<i class="glyph-icon float-right icon-linecons-key"></i>',
                                        '/professores/admin/passwd',
                                        ['escape' => false]) ?>
                                </li>
                            </ul>
                            <div class="pad5A button-pane button-pane-alt text-center">
                                <?= $this->Html->link('<i class="glyph-icon icon-power-off"></i>&nbsp;&nbsp;Sair do Sistema',
                                    '/professores/logout',
                                    ['class' => 'btn display-block font-normal btn-danger', 'escape' => false]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="header-nav-right">
                <a href="#" class="hdr-btn" id="fullscreen-btn" title="Fullscreen">
                    <i class="glyph-icon icon-arrows-alt"></i>
                </a>
            </div>

        </div>
        <div id="page-sidebar" class="bg-white color-orange-cacique">
            <div class="scroll-sidebar">

                <ul id="sidebar-menu">
                    <!-- INICIO -->
                    <li class="header"><span>Admin</span></li>
                    <li>
                        <?=$this->Html->link('<i class="glyph-icon icon-home"></i><span>'.__('Dashboard').'</span>',
                            '/professores/admin/dashboard',
                            ['title' => 'Admin Dashboard', 'escape' => false]);?>
                    </li>
                    <li class="divider"></li>

                    <!-- CONFIGURAÇÃO -->
                    <li class="header"><span><?=__('Configuração');?></span></li>

                    <!-- ALUNOS -->
                    <li>
                        <a href="#" title="<?=__('Aluno');?>">
                            <div class="glyph-icon icon-user"></div>
                            <span><?=__('Aluno');?></span>
                            <?=
                            (isset($badges['AdminProfessor']['pre_orders']) && $badges['AdminProfessor']['pre_orders'] > 0)
                                ? '<span class="bs-badge badge-info">!</span>'
                                : ''
                            ?>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <li>
                                    <?=$this->Html->link(__('Listar alunos'),
                                        '/professores/admin/alunos');?>
                                </li>
                                <li>
                                    <?php
                                    (isset($badges['AdminProfessor']['pre_orders']) && $badges['AdminProfessor']['pre_orders'] > 0)
                                        ? $badge =
                                        '<span class="bs-badge badge-absdolute float-right badge-black">'
                                        .$badges['AdminProfessor']['pre_orders']
                                        .'</span>'
                                        : $badge = '';
                                    echo $this->Html->link(
                                        __('Indicações de Produtos').
                                        $badge,
                                        '/professores/admin/aluno/indicacoes-de-produtos',
                                        ['escape' => false]
                                    );?>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <!-- ACADEMIA -->
                    <li>
                        <a href="#" title="<?=__('Academia');?>">
                            <div class="glyph-icon icon-building"></div>
                            <span><?=__('Academia');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <li>
                                    <?=$this->Html->link(__('Indicar academia'),
                                        '/professores/admin/indicar-academia');?>
                                </li>
                            </ul>

                            <ul>
                                <li>
                                    <?=$this->Html->link(__('Minhas academias'),
                                        '/professores/admin/listar-minhas-academias');?>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <!-- PEDIDOS -->
                    <li>
                        <a href="#" title="<?=__('Pedido');?>">
                            <div class="glyph-icon icon-suitcase"></div>
                            <span><?=__('Pedido');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <li>
                                    <?=$this->Html->link(__('Listar pedidos'),
                                        '/professores/admin/pedidos');?>
                                </li>

                            </ul>
                        </div>
                    </li>

                    <!-- PAGAMENTOS -->
                    <li>
                        <a href="#" title="<?=__('Pagamentos');?>">
                            <div class="glyph-icon icon-money"></div>
                            <span><?=__('Pagamentos');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <li>
                                    <?=$this->Html->link(__('Comissão'),
                                        '/professores/admin/comissoes');?>
                                </li>
                            </ul>
                        </div>
                    </li>


                    <!-- PROFESSOR -->
                    <li>
                        <a href="#" title="<?=__('Professor');?>">
                            <div class="glyph-icon icon-mortar-board"></div>
                            <span><?=__('Professor');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <li>
                                    <?=$this->Html->link(__('Meus dados'),
                                        '/professores/admin/meus-dados');?>
                                </li>
                                <li>
                                    <?=$this->Html->link(__('Dados bancários'),
                                        '/professores/admin/dados-bancarios');?>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <!-- ACT -->
                    <li>
                        <a href="#" title="<?=__('ACT');?>">
                            <div class="glyph-icon icon-file-text-o"></div>
                            <span><?=__('ACT');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <li>
                                    <?=$this->Html->link(__('Documentos'),
                                        '/professores/admin/act');?>
                                </li>
                            </ul>
                        </div>
                    </li>

                </ul>
            </div>
        </div>

        <div id="page-content-wrapper">
            <div id="page-content">
                <div class="loading text-center">
                    <span class="helper"></span>
                    <i class="fa fa-refresh fa-spin fa-3x fa-fw margin-bottom"></i>
                    <span class="">Atualizando conteúdo...</span>
                </div>
                <!-- jQueryUI Spinner -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/spinner/spinner.js"></script>
                <script type="text/javascript">
                    /* jQuery UI Spinner */

                    $(function() { "use strict";
                        $(".spinner-input").spinner();
                    });
                </script>

                <!-- jQueryUI Autocomplete -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/autocomplete/autocomplete.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/autocomplete/menu.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/autocomplete/autocomplete-demo.js"></script>

                <!-- Touchspin -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/touchspin/touchspin.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/touchspin/touchspin-demo.js"></script>

                <!-- Input switch -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/input-switch/inputswitch.js"></script>
                <script type="text/javascript">
                    /* Input switch */

                    $(function() { "use strict";
                        $('.input-switch').bootstrapSwitch();
                    });
                </script>

                <!-- Textarea -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/textarea/textarea.js"></script>
                <script type="text/javascript">
                    /* Textarea autoresize */

                    $(function() { "use strict";
                        $('.textarea-autosize').autosize();
                    });
                </script>

                <!-- Multi select -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/multi-select/multiselect.js"></script>
                <script type="text/javascript">
                    /* Multiselect inputs */

                    $(function() { "use strict";
                        $(".multi-select").multiSelect();
                        $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
                    });
                </script>

                <!-- Uniform -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/uniform/uniform.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/uniform/uniform-demo.js"></script>

                <!-- Chosen -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/chosen/chosen.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/chosen/chosen-demo.js"></script>

                <!-- Nice scroll -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/nicescroll/nicescroll.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/nicescroll/nicescroll-demo.js"></script>

                <!-- include summernote css/js-->
                <link href="<?= JS_URL; ?>summernote/dist/summernote.css" rel="stylesheet" property="">
                <script src="<?= JS_URL; ?>summernote/dist/summernote.js"></script>
                <script src="<?= JS_URL; ?>summernote/lang/summernote-pt-BR.js"></script>

                <div class="row">
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>
                </div>
            </div>
        </div>
    </div>

    <!-- JS Demo -->
    <script type="text/javascript" src="<?= $this->request->webroot; ?>assets-minified/admin-all-demo.js"></script>

    <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/modal/modal.js"></script>

    <!-- Colorpicker -->
    <link rel="stylesheet" href="<?= $this->request->webroot; ?>assets/widgets/jquery-minicolors-master/jquery.minicolors.css">
    <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/jquery-minicolors-master/jquery.minicolors.min.js"></script>
    <script type="text/javascript" src="<?= $this->request->webroot; ?>js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>

    <!-- DateTime Picker -->
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>css/bootstrap-datetimepicker.min.css">
    <script type="text/javascript" src="<?= $this->request->webroot; ?>js/moment-with-locales.js"></script>
    <script type="text/javascript" src="<?= $this->request->webroot; ?>js/bootstrap-datetimepicker.min.js"></script>

</div>

    <!-- Smartsupp Live Chat script -->
    <script type="text/javascript">
    var _smartsupp = _smartsupp || {};
    _smartsupp.key = '472d82fddbe4326ca5661de701a829a7553ce341';
    window.smartsupp||(function(d) {
        var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
        s=d.getElementsByTagName('script')[0];c=d.createElement('script');
        c.type='text/javascript';c.charset='utf-8';c.async=true;
        c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
    })(document);
    </script>
    
</body>
</html>
