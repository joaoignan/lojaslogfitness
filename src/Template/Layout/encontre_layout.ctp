<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="pt_BR"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="pt_BR"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="pt_BR"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt_BR" xmlns='http://www.w3.org/1999/xhtml' xmlns:og='http://ogp.me/ns#'> <!--<![endif]-->
    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="google-site-verification" content="X0LpIu6h8FhuP9butoCMnmCB5UVGkhSzocPpM4NfFzA" />
        <title>LOGFITNESS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">
        <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?= WEBROOT_URL?>img/favicon/manifest.json">

        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?= WEBROOT_URL?>img/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

        <meta name="keywords" content="academia, academias, plataforma, logfitness, suplementos, atleta, loja virtual, frete free, frete grátis, log, startup, whey, massa, musculação, netshoes">
        <meta name="robots" content="index, follow">

        <meta name="google-site-verification" content="HKMEC3k_UJlkbbtA2qYXhh_kyo55m9jvUgFQ4jDJzUY" />
        <link rel="alternate" hreflang="pt-br" href="https://www.logfitness.com.br">

        <meta property='og:title' content='LOGFITNESS'/>
        <?php if($produto) { ?>
            <?php $fotos = unserialize($produto->fotos); ?>
            <meta property='og:description' content='O aluno da academia, realiza a compra do suplemento, efetua o pagamento online e recebe o produto na própria academia, com frete grátis.'/>
            <meta property='og:image' content='https://www.logfitness.com.br/img/produtos/md-<?= $fotos[0] ?>'/>
        <?php } else { ?>
            <meta property='og:description' content='Conheça nossa loja de suplementos! Compre no site e retire aqui na academia.'/>
            <meta property='og:image' content='https://www.logfitness.com.br/img/logo_face.jpg'/>
        <?php } ?>
        <meta property='og:url' content='<?= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>'/>
        <meta property='og:type' content='website'/>
        <meta property='og:site_name' content='LOGFITNESS'/>

        <script type="text/javascript" src="https://js.iugu.com/v2"></script>

        <?= $this->Html->script('jquery.min') ?>
        <?= $this->Html->css('font-awesome.min')?>
        <?= $this->Html->css('bootstrap.min')?>
        <?= $this->Html->css('owl.carousel.css')?>
        <?= $this->Html->css('owl.theme.css')?>
        <?= $this->Html->css('jquery.fancybox') ?>
        <?= $this->Html->css('navbar') ?>
        <?= $this->Html->css('encontre')?>
         <?= $this->Html->css('overlay') ?>
        <?= $this->Html->script('bootstrap.min')?>
        <?= $this->Html->script('jquery.fancybox') ?>
        <?= $this->Html->script('owl-carousel/owl.carousel') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>

        <style type="text/css">
            .loading{
                display: none;
                background: rgba(255,255,255,0.95);
                z-index: 9999;
                position: fixed;
                top: 20%;
                left: 50%;
                margin-left: -150px;
                width: 300px;
                height: 100%;
                max-height: 100px;
                border-radius: 5px;
                border: 1px solid #FFF;
                box-shadow: -2px 2px 10px #BBB;
                padding: 25px;
            }
        </style>


        <script>
        var WEBROOT_URL = "<?= WEBROOT_URL ?>";
        </script>

        <div class="loading text-center">
            <span class="helper"></span>
            <i class="fa fa-refresh fa-spin fa-3x fa-fw margin-bottom"></i>
            <span class="">Atualizando conteúdo...</span>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- Google Analytics -->
        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                    function () {
                        (b[l].q = b[l].q || []).push(arguments)
                    });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-64522723-3', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- End Google Analytics -->
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
             fbq('init', '1675974925977278'); 
            fbq('track', 'PageView');
        </script>
        <!-- <noscript>
             <img height="1" width="1" 
            src="https://www.facebook.com/tr?id=1675974925977278&ev=PageView
            &noscript=1"/>
        </noscript> -->
        <!-- End Facebook Pixel Code -->
        <!-- Smart Look -->
        <script type="text/javascript">
            window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', '65dd1b31ade411a0f1f09a9524c2adf7c3ca5eb5');
        </script>
        <!-- End Smart Look -->
        <script>
        function openBaixoNavbar() {
            document.getElementById("navbar-content").style.height = "100%";
            $("html,body").css({"overflow":"hidden"});
        }
        function closeBaixoNavbar() {
            document.getElementById("navbar-content").style.height = "0%";
            $("html,body").css({"overflow":"auto"});
        }
        </script>

        <script type="application/ld+json">
        {
          "@context":"http://schema.org",
          "@type":"ItemList",
          "itemListElement":[
            {
              "@type":"ListItem",
              "position":1,
              "url":"https://www.logfitness.com.br/produto/Massa-Nitro-No2-3kg-Pote-Pote-Baunilha-Probiotica-a"
            },
            {
              "@type":"ListItem",
              "position":2,
              "url":"https://www.logfitness.com.br/produto/whey-pro-f-isoflaris-900g-body-action-morango"
            },
            {
              "@type":"ListItem",
              "position":3,
              "url":"https://www.logfitness.com.br/produto/pure-series-proplex-morango-1-020-kg-atlhetica"
            }
          ]
        }
        </script>

        <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "Organization",
          "url": "https://www.logfitness.com.br",
          "logo": "https://www.logfitness.com.br/webroot/logo.png",
          "contactPoint": [{
            "@type": "ContactPoint",
            "telephone": "+55-17-3364-3100",
            "contactType": "customer service"
          }]
        }
        </script>
</head>
<body>
    <?= $this->Element('navbar-default'); ?>
	<?= $this->fetch('content') ?>
    <div class="col-xs-12 baixonavbar text-center">
        <div class="btn-menu">
            <span onclick="openBaixoNavbar()"><i class="fa fa-chevron-circle-up fa-2x"></i></span>
            <p>Menu</p>
        </div>
    </div>

    <?= $this->Flash->render() ?>
    
</body>

<?= $this->Html->script('jquery.mask.min.js')?>
<script type="text/javascript">$("#telephone").mask("(00) 00000-0009");</script>