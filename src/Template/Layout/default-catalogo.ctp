<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="pt_BR"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="pt_BR"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="pt_BR"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt_BR" xmlns='http://www.w3.org/1999/xhtml' xmlns:og='http://ogp.me/ns#'> <!--<![endif]-->

<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="google-site-verification" content="X0LpIu6h8FhuP9butoCMnmCB5UVGkhSzocPpM4NfFzA" />
    <META HTTP-EQUIV="EXPIRES" CONTENT="0"> <!-- teste de cache -->
    <META HTTP-EQUIV="pragma" CONTENT="nocache"> <!-- teste de cache -->
    <?php $marca ? $produtoMarca = ' - '.$marca : $produtoMarca = null  ?>
    <?php $produto->produto_base->name ? $produtoName = ' - '.$produto->produto_base->name : $produtoName = null  ?>
    <?php $title = $academia->shortname.$produtoMarca.$produtoName.' - LOGFITNESS' ?>
    <title><?= $title  ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= WEBROOT_URL?>img/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= WEBROOT_URL?>img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="A LogFitness é uma plataforma omnichannel de vendas de suplementos, que facilita a compra e a entrega desses produtos através do perfil da sua academia.">

    <!-- <meta name="keywords" content="academia, academias, plataforma, logfitness, suplementos, atleta, loja virtual, frete free, frete grátis, log, startup, whey, massa, musculação, netshoes"> -->
    <meta name="keywords" content="suplementos importados no brasil, suplementos no boleto, suplementos importados atacado,suplementos importados online, suplementos online, suplementos online confiavel, suplementos online baratos, onde suplementos baratos, suplementos baratos no brasil">
    <meta name="robots" content="index, follow">

    <meta name="google-site-verification" content="HKMEC3k_UJlkbbtA2qYXhh_kyo55m9jvUgFQ4jDJzUY" />
    <link rel="alternate" hreflang="pt-br" href="https://www.logfitness.com.br">

    <meta property='og:title' content='LOGFITNESS'/>
    <?php if($produto) { ?>
        <?php $fotos = unserialize($produto->fotos); ?>
        <!-- <meta property='og:description' content='O aluno da academia, realiza a compra do suplemento, efetua o pagamento online e recebe o produto na própria academia, com frete grátis.'/> -->
        <?php $produto->preco_promo ? $valor = $produto->preco_promo : $valor = $produto->preco ?>
        <?php $produto->preco_promo ? $valor_dose = $produto->preco_promo / $produto->doses : $valor_dose = $produto->preco / $produto->doses ?>
        <meta property='og:description' content='<?= $produto->produto_base->name.' '.$produto->marca.' - R$'.number_format($valor, 2, ',', '.').' - '.$produto->doses.' Treinos - R$'.number_format($valor_dose, 2, ',', '.').' por dose' ?>'/>
        <meta property='og:image' content='https://www.logfitness.com.br/img/produtos/md-<?= $fotos[0] ?>'/>
    <?php } else { ?>
        <meta property='og:description' content="A LogFitness é uma plataforma omnichannel de vendas de suplementos, que facilita a compra e a entrega desses produtos através do perfil da sua academia."/>
        <meta property='og:image' content='https://www.logfitness.com.br/img/logo_face.jpg'/>
    <?php } ?>
    <meta property='og:url' content='<?= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>'/>
    <meta property='og:type' content='website'/>
    <meta property='og:site_name' content='LOGFITNESS'/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <?= $this->Html->script('jquery-2.2.3.min') ?>

    <script type="text/javascript" src="https://js.iugu.com/v2"></script>

    <style>
        <?= file_get_contents(CSS_URL.'academia_new_bootstrap.min.css') ?>
        <?= file_get_contents(CSS_URL.'font-awesome.min.css') ?>
    </style>

    <script>
        <?= file_get_contents(JS_URL.'vendor/modernizr-2.8.3-respond-1.4.2.min.js') ?>   
    </script>
        
        <?= $this->Html->css('catalogo') ?>
        <?= $this->Html->css('overlay') ?>
        <?= $this->Html->css('main') ?>
        <?= $this->Html->css('main_complementar') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script>
        var WEBROOT_URL = "<?= WEBROOT_URL ?>";
        var SSLUG = "<?= $SSlug ?>";
        var ACADEMIA_SLUG = "<?= $academia->slug ?>";
    </script>

    <!-- Google Analytics -->
    <script>
        (function (b, o, i, l, e, r) {
            b.GoogleAnalyticsObject = l;
            b[l] || (b[l] =
                function () {
                    (b[l].q = b[l].q || []).push(arguments)
                });
            b[l].l = +new Date;
            e = o.createElement(i);
            r = o.getElementsByTagName(i)[0];
            e.src = '//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e, r)
        }(window, document, 'script', 'ga'));
        ga('create', 'UA-64522723-3', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->

    <script type="application/ld+json">
    {
      "@context":"http://schema.org",
      "@type":"ItemList",
      "itemListElement":[
        {
          "@type":"ListItem",
          "position":1,
          "url":"https://www.logfitness.com.br/produto/Massa-Nitro-No2-3kg-Pote-Pote-Baunilha-Probiotica-a"
        },
        {
          "@type":"ListItem",
          "position":2,
          "url":"https://www.logfitness.com.br/produto/whey-pro-f-isoflaris-900g-body-action-morango"
        },
        {
          "@type":"ListItem",
          "position":3,
          "url":"https://www.logfitness.com.br/produto/pure-series-proplex-morango-1-020-kg-atlhetica"
        }
      ]
    }
    </script>

    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "url": "https://www.logfitness.com.br",
      "logo": "https://www.logfitness.com.br/webroot/logo.png",
      "contactPoint": [{
        "@type": "ContactPoint",
        "telephone": "+55-17-3364-3100",
        "contactType": "customer service"
      }]
    }
    </script>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">
    Você está utilizando um navegador obsoleto. Vitiste <a href="http://browsehappy.com/"> e atualize seu browser
    para uma melhor experiência web.
</p>
<![endif]-->

<?php header('Access-Control-Allow-Origin: *'); ?>

<!-- BLOQUEIO PARA ERROS -->
<!-- <div class="bloqueio">
    <div class="content-loader">
        <?= $this->Html->image('logfitness.png', ['alt' => 'logfitness'])?><br/><br/>
        <p><strong>Estamos em atualização para melhorar sua experiência na Logfitness</strong></p>
    </div>
</div> -->

<!-- <div class="box-close-filter">
    <span class="fa fa-sliders fa-2x close-filter close-filter-icon mobile-not"></span>
</div> -->

<?= $this->Html->css('novo-main') ?>

<?= $this->Flash->render() ?>

<div class="content-master">
    <?= $this->fetch('content') ?>
</div>

<!-- Google Tag Manager -->
<!-- <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KNV3354');</script> -->
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
     fbq('init', '1675974925977278'); 
    fbq('track', 'PageView');
</script>
<noscript>
     <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=1675974925977278&ev=PageView
    &noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

<!-- Google Tag Manager (noscript) -->
<!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KNV3354"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
<!-- End Google Tag Manager (noscript) -->

<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1321574044525013',
            xfbml      : true,
            version    : 'v2.7'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<?= $this->Html->css('media_queries.min.css') ?>
<?= $this->Html->css('star-rating.min.css') ?>
<?= $this->Html->css('validationEngine.jquery.custom.min.css') ?>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

<script>
    <?php //file_get_contents(JS_URL.'vendor/bootstrap.min.js') ?>
    <?= file_get_contents(JS_URL.'plugins.js') ?>
    <?= file_get_contents(JS_URL.'star-rating.min.js') ?>
    <?= file_get_contents(JS_URL.'star-rating_locale_pt-br.min.js') ?>
    <?= file_get_contents(JS_URL.'jquery.validationEngine.min.js') ?>
    <?= file_get_contents(JS_URL.'jquery.validationEngine-br.min.js') ?>
    <?= file_get_contents(JS_URL.'jquery.mask.min.js') ?>
</script>

<?= $this->Html->script('jquery.form.min') ?>
<?= $this->Html->script('jquery.countdown.min') ?>
<?= $this->Html->script('jquery-ui-1.11.4.custom/jquery-ui.min') ?>
<?= $this->Html->script('chosen.jquery.min') ?>
<?= $this->Html->script('jquery.nanoscroller.min') ?>

<?= $this->fetch('css') ?>
<?= $this->fetch('script') ?>

<style type="text/css">
    #chat-application {
        z-index: 2!important;
    }
</style>
</body>
</html>