<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="pt_BR"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="pt_BR"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="pt_BR"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt_BR" xmlns='http://www.w3.org/1999/xhtml' xmlns:og='http://ogp.me/ns#'> <!--<![endif]-->
<head>
	<meta name="google-site-verification" content="X0LpIu6h8FhuP9butoCMnmCB5UVGkhSzocPpM4NfFzA" />
	<?php $marca ? $produtoMarca = ' - '.$marca : $produtoMarca = null  ?>
    <?php $produto->produto_base->name ? $produtoName = ' - '.$produto->produto_base->name : $produtoName = null  ?>
    <?php $title = $academia->shortname.$produtoMarca.$produtoName.' - LOGFITNESS' ?>
    <title><?= $title  ?></title>
	<meta name="viewport" content="width = 1050, user-scalable = no" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= WEBROOT_URL?>img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= WEBROOT_URL?>img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="Conheça nossa loja de suplementos! Compre no site e retire aqui na academia.">

    <!-- <meta name="keywords" content="academia, academias, plataforma, logfitness, suplementos, atleta, loja virtual, frete free, frete grátis, log, startup, whey, massa, musculação, netshoes"> -->
    <meta name="keywords" content="suplementos importados no brasil, suplementos no boleto, suplementos importados atacado,suplementos importados online, suplementos online, suplementos online confiavel, suplementos online baratos, onde suplementos baratos, suplementos baratos no brasil">
    <meta name="robots" content="index, follow">
    <meta name="google-site-verification" content="HKMEC3k_UJlkbbtA2qYXhh_kyo55m9jvUgFQ4jDJzUY" />
    <link rel="alternate" hreflang="pt-br" href="https://www.logfitness.com.br">
	
	<meta property='og:url' content='<?= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>'/>
    <meta property='og:type' content='website'/>
    <meta property='og:site_name' content='LOGFITNESS'/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <?= $this->Html->script('turnjs/jquery.min.1.7.js') ?>
    <?= $this->Html->script('turnjs/modernizr.2.5.3.min.js') ?>
    <?= $this->Html->script('turnjs/hash.js') ?>
    <?= $this->Html->css('catalogo') ?>

    <!-- Google Analytics -->
    <script>
        (function (b, o, i, l, e, r) {
            b.GoogleAnalyticsObject = l;
            b[l] || (b[l] =
                function () {
                    (b[l].q = b[l].q || []).push(arguments)
                });
            b[l].l = +new Date;
            e = o.createElement(i);
            r = o.getElementsByTagName(i)[0];
            e.src = '//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e, r)
        }(window, document, 'script', 'ga'));
        ga('create', 'UA-64522723-3', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->

    <!-- Smart Look -->
	<script type="text/javascript">
	    window.smartlook||(function(d) {
	    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
	    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
	    c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
	    })(document);
	    smartlook('init', '65dd1b31ade411a0f1f09a9524c2adf7c3ca5eb5');
	</script>
	<!-- End Smart Look -->

</head>
<body>
	<?= $this->fetch('content') ?>
</body>
</html>