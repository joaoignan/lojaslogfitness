<?php
$page_title = 'Admin - ';
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> <?= $page_title ?> <?= $adm_professor->name ?>
  </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>assets/images/icons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>assets/images/icons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>assets/images/icons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>assets/images/icons/favicon-16x16.png">

  <!-- jQuery 2.2.3 -->
  <?= $this->Html->script('jquery-2.2.3.min') ?>

  <?= $this->Html->css('jquery.Jcrop.min') ?>
  <?= $this->Html->script('jquery.Jcrop.min') ?>

  <script type="text/javascript" src="<?= $this->request->webroot; ?>assets-minified/js-core.js"></script>

  <?= $this->Html->css('jquery.fancybox') ?>
  <!-- Bootstrap 3.3.6 -->
  <?= $this->Html->css('bootstrap.min')?>
  <!-- Font Awesome -->
  <?= $this->Html->css('font-awesome.min')?>
  <?= $this->Html->css('Jcrop.min')?>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <?= $this->Html->css('AdminLTE')?>
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <?= $this->Html->css('progress')?>
  <?= $this->Html->css('skin-yellow')?>
  <?= $this->Html->css('overlay')?>

  <?= $this->Html->script('jquery.fancybox') ?>
 <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-64522723-6"></script>
  <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-64522723-6');
  </script>
  <!-- End Google Analytics -->

  <script type="text/javascript">
      var Controller  = "<?= $ControllerName; ?>";
      var Action      = "<?= $ActionName; ?>";
      var WEBROOT_URL = "<?= WEBROOT_URL; ?>";
  </script>
<style>
.logo-log{
  max-width: 50px;
}
.image-menu{
  width: auto!important;
}
.navbar-itens{
  background: none;
  border: none;
}
.navbar-itens hover{
  background-color: #e6e6e6;
}
.valor-mobile{
  display: none;
}

.alert, .alert-icon, .bootstrap-timepicker-widget table td input, .bsdatepicker, .bsdatepicker td, .bsdatepicker td span, .bsdatepicker th, .btn, .button-pane, .chosen-choices, .chosen-choices li.search-choice, .chosen-container, .chosen-container-single .chosen-search input, .chosen-results li.highlighted, .chosen-single, .content-box, .content-box .ui-widget-overlay.loader, .content-box-closed, .content-box-closed .content-box-header, .dashboard-box, .daterangepicker, .daterangepicker .calendar-date, .daterangepicker .ranges li, .daterangepicker td, .daterangepicker th, .dropdown-menu, .dropdown-submenu.float-left > .dropdown-menu, .dropzone, .form-control, .form-input-icon .glyph-icon, .hero-btn, .icon-boxed, .image-box, .info-box, .info-box-alt, .input, .input-group-addon.addon-inside, .jGrowl-notification, .main-header .main-nav.nav-alt li.sf-mega-menu .sf-mega a, .main-header .main-nav.nav-alt li > ul li a, .main-nav ul li ul li a, .minicolors-panel, .ms-list li, .nav-list, .nav-list-horizontal-alt li a, .nav-tabs.nav-justified > li > a, .nav > li > a, .notifications-box, .pagination, .panel-box, .panel-content, .panel-group .panel, .panel-layout, .popover, .progressbar, .progressbar-value, .sb-slidebar .chat-box li a, .sb-slidebar .popover-title, .selector, .table-rounded, .tile-box, .tile-button, .todo-box li, .tooltip-inner, .ui-accordion, .ui-accordion-header, .ui-button, .ui-corner-all, .ui-datepicker, .ui-datepicker td a, .ui-datepicker td span, .ui-datepicker-buttonpane button, .ui-datepicker-next, .ui-datepicker-prev, .ui-dialog, .ui-dialog .ui-dialog-titlebar-close, .ui-dialog-buttonset button, .ui-dialog-titlebar-close, .ui-editRangeSlider-inputValue, .ui-menu li a, .ui-menu-item, .ui-rangeSlider-bar, .ui-rangeSlider-label, .ui-rangeSlider-noArrow .ui-rangeSlider-container, .ui-rangeSlider-withArrows .ui-rangeSlider-container, .ui-tabs, .ui-toolbar input, .ui-toolbar select, div.dataTables_filter input, div[id^=uniform-] span {
    border-radius: 4px;
}
.bg-default, .bg-white.dashboard-box .button-pane, .bg-white.tile-box .tile-footer, .border-default, .bordered-row .form-group, .btn-default, .button-pane, .chosen-container, .chosen-container .chosen-drop, .chosen-container-active.chosen-with-drop .chosen-single div, .chosen-container-multi .chosen-choices li.search-choice, .chosen-container-single .chosen-single div, .content-box, .content-box-header.bg-default, .content-box-header.bg-gray, .content-box-header.bg-white, .dashboard-buttons .btn, .daterangepicker .calendar-date, .dropdown-menu, .email-body, .fc-state-default, .fc-widget-content, .fc-widget-header, .img-thumbnail, .jvectormap-label, .jvectormap-zoomin, .jvectormap-zoomout, .list-group-item, .mail-toolbar, .mailbox-wrapper .nav-list li a, .minicolors-panel, .ms-container .ms-list, .ms-container .ms-selectable li.ms-elem-selectable, .ms-container .ms-selection li.ms-elem-selection, .nav .open > a, .nav .open > a:focus, .nav .open > a:hover, .nav-tabs, .nav-tabs > li > a:focus, .nav-tabs > li > a:hover, .pagination > li > a, .pagination > li > span, .panel, .panel-box.bg-default, .panel-box.bg-gray, .panel-box.bg-white, .panel-content.bg-default, .panel-content.bg-gray, .panel-content.bg-white, .panel-footer, .panel-group .panel-footer + .panel-collapse .panel-body, .panel-group .panel-heading + .panel-collapse .panel-body, .panel-heading, .popover, .popover-title, .posts-list li, .selector i, .table-bordered, .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th, .tabs-navigation > ul, .tabs-navigation > ul li.ui-state-hover > a, .tabs-navigation > ul li > a, .thumb-pane, .thumbnail, .timeline-box .tl-item .popover, .timeline-box:before, .ui-accordion .ui-accordion-header, .ui-datepicker, .ui-datepicker .ui-datepicker-buttonpane button, .ui-datepicker-buttonpane, .ui-dialog, .ui-dialog .ui-dialog-titlebar, .ui-dialog-buttonpane, .ui-menu, .ui-spinner .ui-spinner-button, .ui-tabs-nav, div.selector, div[id^=uniform-] span {
    border-color: #dfe8f1;
}
.btn-link, .chosen-disabled .chosen-single, .content-box-header.bg-default > .ui-tabs-nav li > a, .content-box-header.bg-gray > .ui-tabs-nav li > a, .content-box-header.bg-white > .ui-tabs-nav li > a, .content-box-header > .ui-tabs-nav li.ui-tabs-active > a, .pagination > li > a, .pagination > li > span, .table, a, body .content-box-header > .ui-tabs-nav li.ui-tabs-active > a:hover, div.selector {
    color: #8da0aa;
}
.btn, a, button, div[id^=uniform-] span {
    -webkit-transition: all .1s ease-in-out;
    -moz-transition: all .1s ease-in-out;
    -ms-transition: all .1s ease-in-out;
    -o-transition: all .1s ease-in-out;
}
:active, :focus, :visited, a, a:active, a:focus, a:visited {
    outline: 0;
}
a {
    text-decoration: none;
}
.chosen-container {
    position: relative;
    display: inline-block;
    zoom: 1;
    width: 100% !important;
    vertical-align: middle;
    border-width: 1px;
    border-style: solid;
}
.chosen-container-single .chosen-single {
    line-height: 38px;
    position: relative;
    display: block;
    overflow: hidden;
    height: 38px;
    margin: 0;
    padding: 0 10px !important;
    cursor: pointer;
    white-space: nowrap;
    text-decoration: none;
}
.chosen-container-single .chosen-search {
    position: relative;
    z-index: 1010;
    margin: 0 0 10px;
    white-space: nowrap;
}
.chosen-container-single .chosen-search input {
    width: 100%;
    padding: 0 38px 0 5px;
}
.bootstrap-timepicker-widget table td input, .chosen-container-multi, .chosen-container-single .chosen-search input, .dataTables_length select, .form-control, .ui-toolbar input, .ui-toolbar select, div.dataTables_filter input {
    font-size: 13px;
    display: block;
    float: none;
    background: #fff;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    color: #2b2f33;
    border: 1px solid #dfe8f1;
    -webkit-box-shadow: inset 1px 1px 3px #f6f6f6;
    -moz-box-shadow: inset 1px 1px 3px #f6f6f6;
    box-shadow: inset 1px 1px 3px #f6f6f6;
}
.chosen-container-active.chosen-with-drop .chosen-single div {
    border-bottom: 1px solid transparent;
    border-bottom-right-radius: 0;
}
.chosen-container-single .chosen-search i, .chosen-container-single .chosen-single div, .fc-corner-right, .input-append-right .input-append, .nav-tabs-right > li > a, .pagination > li:last-child > a, .pagination > li:last-child > span, .panel-box[class*=' col-xs'] + .panel-box[class*=' col-xs'], .selector i {
    border-radius: 0;
    border-top-right-radius: 3px !important;
    border-bottom-right-radius: 3px !important;
}
.bsdatepicker td span, .bsdatepicker td.day:hover, .bsdatepicker thead tr:first-child th.switch:hover, .chosen-container-multi .chosen-choices li.search-choice, .chosen-container-single .chosen-single div, .daterangepicker .ranges li:hover, .daterangepicker td.available:hover, .nav > li > a:focus, .nav > li > a:hover, .pager li > a:focus, .pager li > a:hover, .pagination > li > a:focus, .pagination > li > a:hover, .pagination > li > span:focus, .pagination > li > span:hover, .selector i, .ui-datepicker-title, a.list-group-item:focus, a.list-group-item:hover {
    color: #2b2f33;
    background: #eff4f6;
}
.chosen-container-single .chosen-search i, .chosen-container-single .chosen-single div {
    line-height: 38px;
    position: absolute;
    z-index: 4;
    top: 50%;
    right: 0;
    display: block;
    width: 38px;
    height: 38px;
    margin-top: -19px;
    text-align: center;
    border-left: 1px solid transparent;
}
.glyph-icon {
    text-align: center;
}
.bsdatepicker td span, .bsdatepicker td.day:hover, .bsdatepicker thead tr:first-child th.switch:hover, .chosen-container-multi .chosen-choices li.search-choice, .chosen-container-single .chosen-single div, .daterangepicker .ranges li:hover, .daterangepicker td.available:hover, .nav > li > a:focus, .nav > li > a:hover, .pager li > a:focus, .pager li > a:hover, .pagination > li > a:focus, .pagination > li > a:hover, .pagination > li > span:focus, .pagination > li > span:hover, .selector i, .ui-datepicker-title, a.list-group-item:focus, a.list-group-item:hover {
    color: #2b2f33;
    background: #eff4f6;
}
.chosen-container-single .chosen-search i, .chosen-container-single .chosen-single div {
    line-height: 38px;
    position: absolute;
    z-index: 4;
    top: 50%;
    right: 0;
    display: block;
    width: 38px;
    height: 38px;
    margin-top: -19px;
    text-align: center;
    border-left: 1px solid transparent;
}
.chosen-container-single .chosen-single {
    line-height: 38px;
    position: relative;
    display: block;
    overflow: hidden;
    height: 38px;
    margin: 0;
    padding: 0 10px !important;
    cursor: pointer;
    white-space: nowrap;
    text-decoration: none;
}
.chosen-container .chosen-results {
    position: relative;
    overflow-x: hidden;
    overflow-y: auto;
    max-height: 240px;
    -webkit-overflow-scrolling: touch;
}
.chosen-results, .form-wizard > ul, .nav-list ul, .nav-list-horizontal ul, .parsley-errors-list, .reset-ul, .tabs-navigation > ul, ul.messages-box, ul.notifications-box, ul.progress-box {
    margin: 0;
    padding: 0;
    list-style: none;
}
.chosen-container .chosen-results li.active-result {
    display: list-item;
    cursor: pointer;
}
.chosen-container .chosen-results li {
    line-height: 20px;
    margin: 5px 0;
    padding: 3px 10px;
    list-style: none;
}
.chosen-container.chosen-with-drop .chosen-drop {
    right: -1px;
    left: -1px;
    width: auto;
    padding: 10px;
}
.chosen-container .chosen-drop {
    position: absolute;
    z-index: 1010;
    top: 38px;
    left: -9999px;
    width: 100%;
    border-width: 1px;
    border-style: solid;
    border-top: 0;
    background: #fff;
}
.icon-caret-down:before, .ui-accordion-header-icon.ui-icon-triangle-1-s:before {
    content: "\f0d7";
}
#page-sidebar li a.sf-with-ul:after, #page-sidebar li ul li a:before, .dataTables_paginate a i:before, .fc-icon, .glyph-icon:before, .search-choice-close:before, .ui-dialog-titlebar-close:before, .ui-icon:before {
    font-family: FontAwesome;
    font-weight: 400;
    font-style: normal;
    display: inline-block;
    text-align: center;
    text-decoration: none;
    background: 0 0;
    speak: none;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
.icon-search:before {
    content: "\f002";
}
#page-sidebar li a.sf-with-ul:after, #page-sidebar li ul li a:before, .dataTables_paginate a i:before, .fc-icon, .glyph-icon:before, .search-choice-close:before, .ui-dialog-titlebar-close:before, .ui-icon:before {
    font-family: FontAwesome;
    font-weight: 400;
    font-style: normal;
    display: inline-block;
    text-align: center;
    text-decoration: none;
    background: 0 0;
    speak: none;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}

@media all and (max-width: 768px){
  .txt-navbar-itens{
    display: none;
  }
}
</style>

<body class="hold-transition skin-yellow sidebar-mini">
  
  
<div class="wrapper">


  <!-- Main Header -->
  <header class="main-header">

    <?= $this->Html->link(
      '<span class="logo-mini">'.
        $this->Html->image('logo_master.png',
            ['class' => 'logo-log', 'alt' => 'logfitness logo']
        ).
      '</span>'.
      '<span class="logo-lg">'.
            $this->Html->image('logo_master.png',
                ['class' => 'logo-log', 'alt' => 'logfitness logo']
            ).
      '</span>',
        ['controller' => 'Academias', 'action' => 'admin_index'],
        ['class' => 'logo', 'escape' => false]
    ) ?>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li>
            <?= $this->Html->link('<i class="fa fa-building-o"></i><spam class="txt-navbar-itens"> Academias:'.count($professores_navbar).'</spam><span class="valor-mobile label label-success">'.count($professores_navbar).'</span></button>',
                ['controller' => 'Professores', 'action' => 'listar_minhas_academias'],
                ['type' => 'button', 'class' => 'navbar-itens', 'escape' => false]
            ) ?>
          </li>
          <li>
            <?= $this->Html->link('<i class="fa fa fa-users"></i><spam class="txt-navbar-itens"> Alunos:'.count($clientes_navbar).'</spam><span class="valor-mobile label label-success">'.count($clientes_navbar).'</span></button>',
                ['controller' => 'Professores', 'action' => 'admin_alunos'],
                ['type' => 'button', 'class' => 'navbar-itens', 'escape' => false]
            ) ?>
          </li>
          <li>
            <?= $this->Html->link('<i class="fa fa fa-list"></i><spam class="txt-navbar-itens"> Pedidos:'.count($pedidos_navbar).'</spam><span class="valor-mobile label label-success">'.count($pedidos_navbar).'</span></button>',
                ['controller' => 'Professores', 'action' => 'admin_pedidos'],
                ['type' => 'button', 'class' => 'navbar-itens', 'escape' => false]
            ) ?>
          </li>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span class="fa fa-user"></span>
              <span class="hidden-xs"><?= $adm_professor->name ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header" style="height: auto">
                <p style="color: black;">
                  <?= $adm_professor->name ?>
                  <small>Super membro desde <?= $adm_professor->created ?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-right">
                  <?= $this->Html->link('<span class="fa fa-sign-out"></span> Sair',
                      ['controller' => 'Professores', 'action' => 'logout'],
                      ['class' => 'btn bg-red', 'escape' => false]
                  ) ?>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <!-- <div class="user-panel">
        <div class="pull-left image">
          <?= $this->Html->image('academias/'.$adm_academia->image, ['alt' => __('Profile image'),
          'class' => 'img-circle', 'alt' => 'Logo Academia']) ?>
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          Status
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div> -->

      <!-- search form (Optional) -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->

      <style type="text/css">
        .texto-sidebar-closed {
          display: none;
          font-size: 13px;
          position: absolute;
        }
        .sidebar-collapse .texto-sidebar-closed {
          display: block;
        }
        .treeview.last {
          padding-bottom: 10px;
        }
      </style>

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header"><span>Admin</span></li>
        <li class="active">
          <?=$this->Html->link('<i class="fa fa-home"></i><span>'.__('Dashboard').'</span><p class="texto-sidebar-closed" style="left: 6px">Início</p>',
          ['controller' => 'Professores', 'action' => 'admin_index'],
          ['title' => 'Admin Dashboard', 'escape' => false,]);?>
        </li>
        <li class="treeview">
          <?=$this->Html->link('<i class="fa fa-users"><p class="texto-sidebar-closed" style="left: 4px">Alunos</p></i><span>'.__('Alunos').'</span>',
              ['controller' => 'Professores', 'action' => 'admin_alunos'],
              ['escape' => false]);?>
        </li>
        <li class="treeview">
          <?=$this->Html->link('<i class="fa fa-list"><p class="texto-sidebar-closed" style="left: -1px">Pedidos</p></i><span>'.__('Pedidos').'</span>',
              ['controller' => 'Professores', 'action' => 'admin_pedidos'],
              ['escape' => false]);?>
        </li>
        <li class="treeview">
          <?=$this->Html->link('<i class="fa fa-building-o"><p class="texto-sidebar-closed" style="left: 7px">Acads</p></i><span>'.__('Academias').'</span>',
              ['controller' => 'Professores', 'action' => 'listar_minhas_academias'],
              ['escape' => false]);?>
        </li>
        <li class="treeview">
          <?=$this->Html->link('<i class="fa fa-money"><p class="texto-sidebar-closed" style="left: 4px">Comis</p></i><span>'.__('Comissões').'</span>',
              ['controller' => 'Professores', 'action' => 'admin_comissoes_index'],
              ['escape' => false]);?>
        </li>
        <li class="treeview">
          <?=$this->Html->link('<i class="fa fa-list"><p class="texto-sidebar-closed" style="left: 4px">Dados</p></i><span>'.__('Dados cadastrais').'</span>',
              ['controller' => 'Professores', 'action' => 'admin_dados'],
              ['escape' => false]);?>
        </li>
        <li class="treeview">
          <?=$this->Html->link('<i class="fa fa-list"><p class="texto-sidebar-closed" style="left: 4px">Senha</p></i><span>'.__('Alterar senha').'</span>',
              ['controller' => 'Professores', 'action' => 'passwd'],
              ['escape' => false]);?>
        </li>
      </ul>
    </section>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Your Page Content Here -->

      <div id="page-content-wrapper">
            <div id="page-content">

                <!-- Touchspin -->

                <!--<link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/widgets/touchspin/touchspin.css">-->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/touchspin/touchspin.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/touchspin/touchspin-demo.js"></script>

                <!-- Input switch -->

                <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/input-switch/inputswitch.css">-->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/input-switch/inputswitch.js"></script>
                <script type="text/javascript">
                    /* Input switch */

                    $(function() { "use strict";
                        $('.input-switch').bootstrapSwitch();
                    });
                </script>

                <!-- Textarea -->

                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/textarea/textarea.js"></script>
                <script type="text/javascript">
                    /* Textarea autoresize */

                    $(function() { "use strict";
                        $('.textarea-autosize').autosize();
                    });
                </script>

                <!-- Multi select -->

                <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/multi-select/multiselect.css">-->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/multi-select/multiselect.js"></script>
                <script type="text/javascript">
                    /* Multiselect inputs */

                    $(function() { "use strict";
                        $(".multi-select").multiSelect();
                        $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
                    });
                </script>

                <!-- Uniform -->

                <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/uniform/uniform.css">-->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/uniform/uniform.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/uniform/uniform-demo.js"></script>

                <!-- Chosen -->
                <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/chosen/chosen.css">-->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/chosen/chosen.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/chosen/chosen-demo.js"></script>

                <!-- Nice scroll -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/nicescroll/nicescroll.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/nicescroll/nicescroll-demo.js"></script>

                <!-- include summernote css/js-->
                <link href="<?= JS_URL; ?>summernote/dist/summernote.css" rel="stylesheet" property="">
                <script src="<?= JS_URL; ?>summernote/dist/summernote.js"></script>
                <script src="<?= JS_URL; ?>summernote/lang/summernote-pt-BR.js"></script>

                <!--
                <div id="page-title">
                    <h2><?= __(str_replace('Admin/', '', $this->fetch('title'))) ?></h2>
                </div>
                -->

                <div class="row">
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
        <!-- <div class="pull-right hidden-xs">
          Anything you want
        </div> -->
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <?= $this->Html->link('Logfitness','https://www.logfitness.com.br',['escape' => false, 'target' => '_blank']) ?>.</strong> Todos os direitos reservados.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                  <span class="label label-danger pull-right">70%</span>
                </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<!-- Bootstrap 3.3.6 -->
<?= $this->Html->script('bootstrap.min') ?>
<!-- AdminLTE App -->
<?= $this->Html->script('app.min') ?>
<?= $this->Html->script('Jcrop.min') ?>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
