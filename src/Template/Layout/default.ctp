<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="pt_BR"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="pt_BR"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="pt_BR"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt_BR" xmlns='http://www.w3.org/1999/xhtml' xmlns:og='http://ogp.me/ns#'> <!--<![endif]-->

<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="google-site-verification" content="X0LpIu6h8FhuP9butoCMnmCB5UVGkhSzocPpM4NfFzA" />
    <META HTTP-EQUIV="EXPIRES" CONTENT="0"> <!-- teste de cache -->
    <META HTTP-EQUIV="pragma" CONTENT="nocache"> <!-- teste de cache -->
    <?php $marca ? $produtoMarca = ' - '.$marca : $produtoMarca = null  ?>
    <?php $produto->produto_base->name ? $produtoName = ' - '.$produto->produto_base->name : $produtoName = null  ?>
    <?php $title = $academia->shortname.$produtoMarca.$produtoName.' - LOGFITNESS' ?>
    <title><?= $title  ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= WEBROOT_URL?>img/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= WEBROOT_URL?>img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="Conheça nossa loja de suplementos! Compre no site e retire aqui na academia.">

    <!-- <meta name="keywords" content="academia, academias, plataforma, logfitness, suplementos, atleta, loja virtual, frete free, frete grátis, log, startup, whey, massa, musculação, netshoes"> -->
    <meta name="keywords" content="suplementos importados no brasil, suplementos no boleto, suplementos importados atacado,suplementos importados online, suplementos online, suplementos online confiavel, suplementos online baratos, onde suplementos baratos, suplementos baratos no brasil">
    <meta name="robots" content="index, follow">

    <meta name="google-site-verification" content="HKMEC3k_UJlkbbtA2qYXhh_kyo55m9jvUgFQ4jDJzUY" />
    <link rel="alternate" hreflang="pt-br" href="https://www.logfitness.com.br">

    <meta property='og:title' content='LOGFITNESS'/>
    <?php if($produto) { ?>
        <?php $fotos = unserialize($produto->fotos); ?>
        <!-- <meta property='og:description' content='O aluno da academia, realiza a compra do suplemento, efetua o pagamento online e recebe o produto na própria academia, com frete grátis.'/> -->
        <?php $produto->preco_promo ? $valor = $produto->preco_promo * $desconto_geral : $valor = $produto->preco * $desconto_geral ?>
        <?php $produto->preco_promo ? $valor_dose = $produto->preco_promo / $produto->doses : $valor_dose = $produto->preco * $desconto_geral / $produto->doses ?>
        <meta property='og:description' content='<?= $produto->produto_base->name.' '.$produto->marca.' - R$'.number_format($valor, 2, ',', '.').' - '.$produto->doses.' Treinos - R$'.number_format($valor_dose, 2, ',', '.').' por dose' ?>'/>
        <meta property='og:image' content='https://www.logfitness.com.br/img/produtos/md-<?= $fotos[0] ?>'/>
    <?php } else { ?>
        <meta property='og:description' content="Conheça nossa loja de suplementos! Compre no site e retire aqui na academia."/>
        <meta property='og:image' content='https://www.logfitness.com.br/img/logo_face.jpg'/>
    <?php } ?>
    <meta property='og:url' content='<?= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>'/>
    <meta property='og:type' content='website'/>
    <meta property='og:site_name' content='LOGFITNESS'/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <?= $this->Html->script('jquery-2.2.3.min') ?>

    <script type="text/javascript" src="https://js.iugu.com/v2"></script>

   
    <?= $this->Html->css('academia_new_animate.min.css')?>
    <?= $this->Html->css('academia_new_bootstrap.min.css')?>
    <?= $this->Html->css('ninja-slider.css')?>
    <?= $this->Html->css('bootstrap.min.css')?>
    <?= $this->Html->css('bootstrap-theme.min.css')?>
    <?= $this->Html->css('ion.rangeSlider.css')?>
    <?= $this->Html->css('ion.rangeSlider.skinHTML5.css')?>
    <?= $this->Html->css('font-awesome.min.css')?>


    <?= $this->Html->script('ninja-slider.js')?>
    <?= $this->Html->script('vendor/modernizr-2.8.3-respond-1.4.2.min.js')?>
    <?= $this->Html->script('ion.rangeSlider.min.js')?>
    
    <?php define("Version", "3001") ?>

    <?= $this->Html->css('owl.carousel.css')?>
    <?= $this->Html->css('overlay') ?>
    <?= $this->Html->css('main.css?'.Version) ?>
    <?= $this->Html->css('main_complementar') ?>
    <?= $this->Html->script('owl-carousel/owl.carousel') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script>
        var WEBROOT_URL = "<?= WEBROOT_URL ?>";
        var SSLUG = "<?= $SSlug ?>";
        var ACADEMIA_SLUG = "<?= $academia->slug ?>";
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-64522723-6"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-64522723-6');
    </script>
    <!-- End Google Analytics -->

    <script type="application/ld+json">
    {
      "@context":"http://schema.org",
      "@type":"ItemList",
      "itemListElement":[
        {
          "@type":"ListItem",
          "position":1,
          "url":"https://www.logfitness.com.br/produto/Massa-Nitro-No2-3kg-Pote-Pote-Baunilha-Probiotica-a"
        },
        {
          "@type":"ListItem",
          "position":2,
          "url":"https://www.logfitness.com.br/produto/whey-pro-f-isoflaris-900g-body-action-morango"
        },
        {
          "@type":"ListItem",
          "position":3,
          "url":"https://www.logfitness.com.br/produto/pure-series-proplex-morango-1-020-kg-atlhetica"
        }
      ]
    }
    </script>

    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "url": "https://www.logfitness.com.br",
      "logo": "https://www.logfitness.com.br/webroot/logo.png",
      "contactPoint": [{
        "@type": "ContactPoint",
        "telephone": "+55-17-3364-3100",
        "contactType": "customer service"
      }]
    }
    </script>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">
    Você está utilizando um navegador obsoleto. Vitiste <a href="http://browsehappy.com/"> e atualize seu browser
    para uma melhor experiência web.
</p>
<![endif]-->

<?php header('Access-Control-Allow-Origin: *'); ?>

<script type="text/javascript">
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
</script>

<div id="fb-root"></div>

<style>
    #btn-loja{
        display: none!important;
    }
    #logexpress-btn{
      display: block!important;
    }
    .overlay-avise {
        height: 0%;
        width: 100%;
        position: fixed;
        z-index: 999999999;
        top: 0;
        left: 0;
        overflow-y: hidden;
        transition: 0.5s;
    }
    .overlay-avise-content {
        position: relative;
        top: 15%;
        width: 40%;
        text-align: center;
        margin-top: 30px;
        left: 30%;
        background-color: white;
        border-radius: 20px;
        padding: 15px;
    }
    .overlay-avise-content h4{
        color: #5089cf;
        margin-top: 40px;
    }
    .overlay-avise a {
        padding: 8px;
        text-decoration: none;
        font-size: 36px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }
    .overlay-avise a:hover, .-aviseoverlay a:focus {
        color: #f1f1f1;
    }
    @media screen and (max-height: 450px) {
        .overlay-avise {overflow-y: auto;}
        .overlay-avise a {font-size: 20px}
        .overlay-avise .closebtn {
            font-size: 40px;
            top: 15px;
            right: 35px;
      }
    }
    .selos{
        background-color: white!important;
    }
    .bloqueio {
        position: fixed;
        left: 0;
        top: 0%;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: white;
    }
    .alerta-trust {
        position: fixed;
        left: 0;
        top: 110px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: white;
    }
    .laterais {
        width: 230px; 
        height: 100%; 
        position: fixed;
        z-index: 100;
        background-color: white;
    }
    .fundo-titulo{
        border: 2px solid #5089cf;
        height: 42px;
    }
    .laterais .titulos{
        text-align: left;
        font-size: 22px;
        padding-left: 60px;
        margin: 0 0 10px;
        color: #5089cf;
        font-family: nexa_boldregular;
        display: flex;
        align-items: center;
        justify-content: center;
        height: 40px;
    }
    .mochila {
        right: 0;
        box-shadow: 0px 0px 1px 1px rgba(80, 137, 207, 0.4);
        opacity: 1;
        width: 200px;
    }
    .filtro {
        left: 0;
        box-shadow: 1px 1px 5px 0px rgba(0,0,0,0.75);
        opacity: 0;
        width: 0;
    }
    .btn-fecha-mochila{
        width: 150px;
        position: fixed;
        bottom: 10px;
        border-radius: 6px;
        height: 40px;
        right: 39px;
        border: 1px solid #8DC73F;
        background-color: #8DC73F;
        font-size: 1.4em;
        color: #FFF;
    }
    .btn-limpar-mochila{
       width: 100%;
        height: 40px;
        border: none;
        background-color: #5087C7;
        font-size: 1.4em;
        color: #FFF;
        border-radius: 10px;
    }
    .btn-fecha-mochila a{
        padding: 0;
        font-size: 1.2em;
    }
    .total-txt{
        font-size: 1.4em;
        color: #5089cf;
        text-align: center;
    }
    .frete-txt{
        font-size: 1.1em;
        color: #5089cf;
        text-align: center;
        margin: 0;
    }
    .total-compra{
        position: fixed;
        bottom: 50px;
        right: 0;
        z-index: 20;
        width: 230px;
    }
    .produtos-mochila{
        width: 228px;
        opacity: 1;
        position: fixed;
        top: 110px;
        bottom: 175px;
        right: 1px;
        overflow-y: auto!important;
        overflow-x: hidden;
    }
    .cupom-desconto p{
        text-align: center;
    }
    .limpar-mochila{
        padding-bottom: 10px;
        display: none;
    }
    .cupom-desconto{
        padding: 10px 0;
        position: fixed;
        bottom: 120px;
        right: 0;
        z-index: 20;
        width: 230px;
    }
    .btn-cupom{
        border: 2px solid;
        padding-top: 2px;
        border-color: #5087c7;
        background-color: white;
        font-family: nexa_boldregular;
        color: #5087c7;
        height: 26px;
    }
    .btn-cupon: hover{
        color: #5087c7!important;
    }
    .seta-mochila{
        margin: 5px 0 10px;
    }
    .fundo-carrinho {
        background: #ffffff;
        padding: 0 5px;
        height: 70px;
    }
    .fundo-filtro {
        padding: 1px 15px;
    }
    .box-filtro {
        padding: 15px 15px;
        overflow: auto;
        overflow-x: hidden;
        left: -1px;
        top: 153px;
        width: 230px;
        bottom: 0;
    }
    .subtitle {
        color: #5089cf;
        font-weight: 700;
        font-size: 20px;
        padding-top: 15px;
        text-transform: uppercase;
        text-align: center;
    }
    li {
        list-style: none;
        padding-left: 10px;
    }
    a {
        text-decoration: none!important;
    }
    #nav-marcas {
        color: #337ab7!important;
        background-color: transparent!important;
    }
    #nav-marcas:hover,
    #nav-marcas:focus,
    #nav-marcas:active {
        color: #23527c!important;
        text-decoration: none!important;
    }
    .imagem-produto-carrinho {
        float: left;
        padding: 0px 0 0 0;
        margin-right: 4px;
        text-align: center;
        width: 60px;
    }
    .dimensao-produto-carrinho {
        max-width: 60px;
        max-height: 60px;
    }
    .qtd-produtos{
        float: left;
        margin-top: 6px;
        padding-left: 7px;
    }
    .valor-produto{
        float: left;
        display: none;
        margin-top: 10px;
        padding-left: 50px;
    }
    .detalhes{
        font-size: 13px;
        float: left;
        margin-right: 15px;
        margin-top: 5px;
    }
    input[type^='button'] {
        background: white;
        border: none;
        font-size: 16px;
        line-height: 15px;
        color: #5089cf;
        font-weight: 700;
        padding: 0;
    }
    .sub-menu-lateral {
        margin-left: 40px;
        width: 150px;
    }
    .div-objetivos {
        display: none;
    }

    #filtro-ordenar {
        border: 2px solid;
        border-color: #5089cf;
    }
    .valor-box {
        text-align: center;
        padding-left: 15px;
        padding-right: 15px;
    }
    .irs-max, .irs-min {
        display: none;
    }
    input[type="button"]:focus {
        box-shadow: 0 0 0 0;
        border: 0 none;
        outline: 0;
    } 
    .ui-draggable-dragging {
        z-index: 300;
    }
    body {
        overflow-x: hidden;
    }
    .nav-objetivos svg {
        width: 20px;
        height: 20px;
    }
    #aplicar-valor {
        display: none;
    }
    .mobile-not {
        display: block;
    }
    .owl-carousel-produtos .fa-chevron-left, .owl-carousel-produtos .fa-chevron-right {
        color: #5089cf;
    }
    @media all and (max-width: 768px) {
        .mobile-not {
            display: none;
        }
        .ts-modal-trigger{
            margin-bottom: 50px;
        }
    }
</style>

<script> 
    $(document).ready(function() {
        $('.owl-carousel-produtos').owlCarousel({
            loop: true,
            autoPlay:true,
            autoPlay:true,
            autoplayTimeout:600,
            autoplayHoverPause:true,
            pagination: false,
            margin: 10,
            navigation: true,
            navigationText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
            itemsMobile: [600, 1],
            itemsDesktopSmall: [1024, 2],
            itemsDesktop: [6000, 3]
        });

        $('.owl-carousel-produtos-view').owlCarousel({
            loop: true,
            autoPlay:true,
            autoPlay:true,
            autoplayTimeout:600,
            autoplayHoverPause:true,
            pagination: false,
            margin: 10,
            navigation: true,
            navigationText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
            singleItem: true,
        });

        $('.owl-carousel-marcas').owlCarousel({
            loop: true,
            pagination: false,
            navigation: true,
            navigationText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
            itemsMobile: [600, 1],
            itemsDesktopSmall: [1024, 2],
            itemsDesktop: [6000, 4]
        });
        $('.owl-carousel-banner').owlCarousel({
            loop: true,
            autoPlay:true,
            autoplayTimeout:100,
            autoplayHoverPause:true,
            navigation: false,
            margin: 10,
            pagination: false,
            singleItem: true
        });
    });
</script>

<!-- BLOQUEIO PARA ERROS -->
<!-- <div class="bloqueio">
    <div class="content-loader">
        <?= $this->Html->image('logfitness.png', ['alt' => 'logfitness'])?><br/><br/>
        <p><strong>Estamos em atualização para melhorar sua experiência na Logfitness</strong></p>
    </div>
</div> -->

<!-- <div class="box-close-filter">
    <span class="fa fa-sliders fa-2x close-filter close-filter-icon mobile-not"></span>
</div> -->

<?= $this->Html->css('navbar.css') ?>
<?= $this->Html->css('novo-main.css?'.Version) ?>
<!-- Chamando o Navbar -->
<?= $this->Element('navbar-ultimate'); ?>
<!-- Fim navbar -->

<!-- Chamando a mochila -->
<?= $this->Element('mochila'); ?>
<!-- Fim mochila -->

<?= $this->Flash->render() ?>

<style>
    #div-marcas ul{
        padding:0;
    }
    #div-marcas ul li{
        margin:0;
    }
    @keyframes seta-mochila {
        from {color: white;transform: rotate(0deg)}
        to   {color: #f3d012;transform: rotate(540deg)}
    }

    @keyframes seta-mochila-fechar {
        from {color: #f3d012;transform: rotate(540deg)}
        to   {color: white;transform: rotate(0deg)}
    }

    .seta-animate-abrir {
        -webkit-animation: seta-mochila 2s linear;
        -moz-animation: seta-mochila 2s linear;
        -o-animation: seta-mochila 2s linear;
        animation: seta-mochila 2s linear;
        transform: rotate(180deg)!important;
        color: #f3d012!important;
    }

    .seta-animate-fechar {
        -webkit-animation: seta-mochila-fechar 2s linear;
        -moz-animation: seta-mochila-fechar 2s linear;
        -o-animation: seta-mochila-fechar 2s linear;
        animation: seta-mochila-fechar 2s linear;
        transform: rotate(0deg)!important;
        color: white!important;
    }

    .info-prod {
        display: none;
        margin-left: 85px;
    }
    .info-direita-mochila {
        float: left; 
        width: 50px;
    }
    #expandir-mochila {
        cursor: pointer;
        float: left;
        margin-top: 7px;
        transform: rotate(0deg);
    }
    #expandir-filter {
        cursor: pointer;
        margin-top: 7px;
        transform: rotate(0deg);
    }
    .academia-logo {
        margin-top: 0;
    }
    #logo-esquerdo {
        position: relative;
        top: -15px;
        left: -35px;
        display: flex;
        align-items: center;
        justify-content: center;
        height: 85px;
        padding: 5px 2px;
        background: white;
        box-shadow: 0px 0px 5px 2px rgb(80, 137, 207);
        width: auto;
        max-width: 220px;
    }
    #menu-principal {
        display: block;
    }
    .logo-margin {
        margin-left: 0!important;
    }
    .logo-topo {
        margin-top: 0;
    }
    .mochila-titulo {
        float: left;
        padding-left: 30px;
    }
    .close-mochila-icon {
        cursor: pointer;
        position: fixed;
        right: 10px;
        z-index: 200;
        top: 56px;
        fill: #5089cf;
        width: 40px;
    }
    .close-filter-icon {
        cursor: pointer;
        position: fixed;
        left: 16px;
        z-index: 200;
        top: 111px;
        fill: white;
        width: 40px;
    }
    .box-close-mochila {
        width: 0;
        height: 42px;
        position: fixed;
        top: 54px;
        z-index: 150;
        right: 0px;
    }
    .box-close-filter {
        width: 0;
        height: 42px;
        position: fixed;
        top: 54px;
        z-index: 150;
        left: 0px;
        background-color: #5089cf;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
    }
    .box-close-filter:hover {
        background-color: #457abb;
    }
    .close-mochila {
        right: 10px;
    }
    .box-close-mochila {
        width: 60px;
    }
    .close-filter {
        left: 16px;
        font-size: 38px;
    }
    .close-filter-mobile {
        left: 16px;
        font-size: 38px;
    }
    .box-close-filter {
        width: 60px;
    }
    .box-filtro {
        left: -250px;
    }

    .close-filter,
    .close-mochila {
        color: #5089cf;
        fill: #5089cf;
    }

    .close-filter-mobile,
    .close-mochila-mobile {
        color: white;
        fill: white;
    }

    .fundo-branco {
        display: none;
    }

    .fechar-filtro,
    .fechar-mochila-x {
        display: none;
    }
    .contador-itens {
        position: absolute;
        top: 0;
        right: 0;
        width: 20px;
        height: 20px;
        border: 1px solid transparent;
        border-radius: 50px;
        color: #5089cf;
        font-weight: 700;
    }
    .error-qtd {
        position: absolute;
        top: -11px;
        right: 72px;
        color: red;
    }
    @media all and (max-width: 768px) {
        .menu-principal, .reparo-nav {
            height: 50px;
        }
        .box-close-filter,
        .box-close-mochila {
            background-color: transparent;
            top: inherit;
            box-shadow: none;
        }
        .close-filter,
        .close-mochila {
            top: inherit;
            bottom: 4px;
        }
        .close-filter-mobile,
        .close-mochila-mobile {
            top: inherit;
            bottom: 4px;
        }
        .produtos-mochila,
        .box-filtro {
            top: 90px;
        }
        #expandir-mochila {
            display: none;
        }
        .laterais {
            top: 50px;
        }
        .cupom-desconto input {
            border: 1px solid #aaa;
            height: 26px;
            border-radius: 0;
        }
        #aplicar-valor {
            display: block;
        }
        .fundo-branco {
            display: block;
            width: 100%;
            height: 45px;
            position: fixed;
            bottom: 0;
            background-color: #5089cf;
            z-index: 40;
        }
        .mochila-titulo {
            float: none;
            padding-left: 0;
        }
        .fechar-filtro,
        .fechar-mochila-x {
            cursor:pointer;
            color: white;
            position:absolute;
            display: block;      
            top: 9px;
            z-index: 40;   
            font-size: 23px;   
        }
        .fechar-mochila-x {
            left: 10px;
        }
        .fechar-filtro {
            right: 10px;
        }
        .btn-fecha-mochila {
            font-size: 25px;
        }
        .info-direita-mochila {
            width: 140px;
        }
        .imagem-produto-carrinho {
            width: 50px;
        }
        .dimensao-produto-carrinho {
            max-width: 50px;
            max-height: 50px;
        }
    }
    .overlay-total-cpf{
        overflow-y: hidden;
        background-color: rgba(51,51,51,0.9);
        z-index: 150;
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        /*bottom: 2000px;*/
    }
    .overlay-cpf{
        width: 250px;
        height: auto;
        position: absolute;
        left: 50%;
        margin-left: -125px;
        top: 50%;
        margin-top: -200px;
        background-color: rgb(255,255,255);
        border-radius: 5px;
        padding: 10px;
    }
    .overlay-cpf h3{
        color: #5089cf;
        font-weight: 700;
    }
    .overlay-cpf input{
        text-align: center;
    }
    .overlay-cpf .invalido{
        color: rgb(255,0,0);
    }
    .overlay-cpf .valido{
        color: rgb(92,184,92);
    }
    .overlay-cpf button {
        width: 100%;
    }
    .overlay-cpf a{
        color: rgb(255,255,255);
    }
    .alerta-cpf {
        display: none;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        $('.fechar-filtro').click(function() {
            $('.close-filter').click();
        });

        $('.fechar-mochila-x').click(function() {
            $('.close-mochila').click();
        });
        $('.btn-fecha-mochila').click(function() {
            location.href = SSLUG+'/mochila/fechar-o-ziper';
        });

        var contador = 0;

        $('.produto-qtd').each(function() {
            contador = contador + parseInt($(this).val());
        });

        $('.contador-itens').html(contador);
    });
</script>



<?php foreach ($s_produtos as $s_produto){ $contador = $contador + 1;  } ?>

<!-- <div class="box-close-mochila mobile-not">
<span class="contador-itens"></span>
<svg class="close-mochila close-mochila-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"><g><polygon points="8.669,48.706 5,62.65 6.565,64.773 10.008,54.727  "/><polygon points="93.422,64.79 95,62.65 91.331,48.706 89.991,54.727  "/><polygon points="41.311,14.947 39.661,15.937 37.462,30.863 37.071,40.502 39.622,40.182 41.47,14.947  "/><polygon points="62.928,40.51 62.538,30.863 60.338,15.937 58.689,14.947 58.529,14.947 60.378,40.188  "/><polygon points="29.01,42.523 21.586,43.458 9.53,47.966 11.045,54.785 5.5,70.967 5.5,88.654 27.14,88.654  "/><polygon points="36.48,55.079 35.119,88.654 64.879,88.654 63.519,55.079  "/><polygon points="52.475,40.195 47.463,40.199 37.03,41.513 36.716,49.244 63.282,49.244 62.969,41.516  "/><polygon points="88.954,54.785 90.47,47.966 78.413,43.458 70.989,42.524 72.859,88.654 94.5,88.654 94.5,70.967  "/><path d="M28.12,89.154h5.978l1.422-35.075h28.96L65.9,89.154h5.979l-2.368-58.421L65.62,15.752l-7.219-4.906h-7.104h-2.595h-7.105   l-7.218,4.906l-3.891,14.981L28.12,89.154z M51.578,13.947h7.388l2.256,1.354l2.312,15.433l0.79,19.511H35.675l0.792-19.511   l2.311-15.433l2.257-1.354h7.386H51.578z"/></g></svg>
</div> -->

<div class="overlay loading">
    <div class="loading-box text-center">
        <?= $this->Html->image('new-loading.gif', ['class' => 'image-loading']); ?>
        <span>Atualizando...</span>
    </div>
</div>

<div class="overlay-filtro"></div>

<input type="hidden" class="academiaslug" value="<?= $ACADEMIA->slug ?>">

<?php if($cadastro_incompleto) { ?>
    <div class="overlay-total-cpf">
        <div class="overlay-cpf text-center">
            <h3>Quase lá!</h3>
            <div class="pt-1">
                <?= $this->Form->create(null, ['id' => 'completar_cadastro_form']); ?>
                <p><strong>Precisamos que você complete seu cadastro para continuar =)</strong></p>
                <br>
                <p>
                    <?= $this->Form->input('telephone', [
                        'div' => false,
                        'label' => false,
                        'templates' => [
                            'inputContainer' => '{{content}}'
                        ],
                        'type' => 'phone',
                        'value' => $CLIENTE->telephone,
                        'class' => 'form-control phone-mask phone-completar-cadastro',
                        'placeholder' => 'Seu celular'
                    ]) ?>
                </p>
                <br>
                <p class="text-left">
                    <?= $this->Form->input('cpf', [
                        'div' => false,
                        'label' => false,
                        'templates' => [
                            'inputContainer' => '{{content}}'
                        ],
                        'id' => 'cpf_padrao',
                        'value' => $CLIENTE->cpf,
                        'class' => 'form-control cpf cpf-check-padrao',
                        'placeholder' => 'Seu CPF'
                    ]) ?>
                </p>
                <span class="alerta-cpf invalido"><strong>CPF inválido</strong></span>
                <span class="alerta-cpf valido"><strong>CPF válido</strong></span>
                <br>
                <br>
                <p>
                    <?= $this->Form->button('Completar', [
                        'type' => 'button',
                        'class' => 'btn btn-success completar_cadastro_btn'
                    ]) ?>
                </p>
                <?= $this->Form->end(); ?>
                <p><strong>ou</strong></p>
                <?= $this->Html->link('<button class="btn btn-danger deslogar">Deslogar</button>',
                    '/logout',
                    ['escape' => false]
                ) ?>
            </div>
            <div class="pt-2" style="display: none;">
                <p>Opa! Parece que este CPF já está vinculado ao e-mail</p>
                <p class="email-com-cpf-salvo"><strong>***email@logfitness.com</strong></p>
                <p>Se você desconhece ou não se lembra deste e-mail</p>
                <p>
                    <?= $this->Html->link('<button class="btn btn-info">Clique aqui</button>',
                        $SSlug.'/solucionar_problemas',
                        ['escape' => false]
                    ); ?>
                </p>
                <p><strong>ou</strong></p>
                <button class="btn btn-danger completar-cadastro-voltar-btn">Voltar</button>
            </div>
            <div class="pt-3" style="display: none;">
                <p><strong>Obrigado</strong></p>
                <p>Manter seus dados atualizados é importante para nos comunicarmos melhor.</p>
                <p><button class="btn btn-success completar-cadastro-finalizar-btn">Fechar</button></p>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function apenasNumeros(string) 
        {
            var numsStr = string.replace(/[^0-9]/g,'');
            return numsStr;
        }

        function ValidaCPF(cpf) {
            var Soma;
            var Resto;
            var cpf = apenasNumeros(cpf);
            Soma = 0;
            if (cpf == "00000000000") return false;

            for (i=1; i<=9; i++) Soma = Soma + parseInt(cpf.substring(i-1, i)) * (11 - i);
            Resto = (Soma * 10) % 11;

            if ((Resto == 10) || (Resto == 11))  Resto = 0;
            if (Resto != parseInt(cpf.substring(9, 10)) ) return false;

            Soma = 0;
            for (i = 1; i <= 10; i++) Soma = Soma + parseInt(cpf.substring(i-1, i)) * (12 - i);
            Resto = (Soma * 10) % 11;

            if ((Resto == 10) || (Resto == 11))  Resto = 0;
            if (Resto != parseInt(cpf.substring(10, 11) ) ) return false;
            return true;
        }

        $(document).ready(function() {
            $(document).on('click', '.completar_cadastro_btn', function() {
                var form = $('#completar_cadastro_form');
                var cpf = $('.cpf-check-padrao').val();
                var telephone = $('.phone-completar-cadastro').val();

                if(ValidaCPF(cpf)) {
                    loading.show();

                    $.ajax({
                        type: "POST",
                        url: WEBROOT_URL + 'completar-cadastro',
                        data: {cpf: cpf, telephone: telephone}
                    })
                        .done(function (data) {
                            var data = JSON.parse(data);

                            $('.pt-1').fadeOut();

                            if(data['retorno'] == 'sucesso') {
                                setTimeout(function() {
                                    $('.pt-3').fadeIn();
                                }, 400);
                            } else {
                                setTimeout(function() {
                                    $('.email-com-cpf-salvo').html(data['cliente_email']);
                                    $('.pt-2').fadeIn();
                                }, 400);
                            }
                            
                            loading.hide();
                        });
                }

                return false;
            });

            $(document).on('click', '.completar-cadastro-voltar-btn', function() {
                $('.alerta-cpf').fadeOut();

                $('.pt-2').fadeOut();

                setTimeout(function() {
                    $('.pt-1').fadeIn();
                }, 400);
            });

            $(document).on('click', '.completar-cadastro-finalizar-btn', function() {
                location.reload();
            });
        });
    </script>
<?php } ?>

<?= $this->Element('overlay-combo'); ?>
<div class="content-master">
    <?= $this->fetch('content') ?>
</div>

<?= $this->Element('footer-default'); ?>

<script type="text/javascript">
  var _trustvox_certificate = _trustvox_certificate || [];
  _trustvox_certificate.push(['_certificateId', 'logfitness']);
  (function() {
    var tv = document.createElement('script'); tv.type = 'text/javascript'; tv.async = true;
    tv.src = '//s3-sa-east-1.amazonaws.com/trustvox-certificate-modal-js/widget.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tv, s);
  })();
</script>

<!-- Estrelinhas Trustvox -->
<script type="text/javascript">
 var _trustvox_shelf_rate = _trustvox_shelf_rate || [];
 _trustvox_shelf_rate.push(['_storeId', '71702']);
 _trustvox_shelf_rate.push(['_productContainer', '#grade-produtos']);
</script>
<script type="text/javascript" async="true" src="//s3-sa-east-1.amazonaws.com/trustvox-rate-widget-js/widget.js"></script>
<!-- EndEstrelinhas Trustvox -->

<!-- Google Tag Manager -->
<!-- <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KNV3354');</script> -->
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
     fbq('init', '1675974925977278'); 
    fbq('track', 'PageView');
</script>
<noscript>
     <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=1675974925977278&ev=PageView
    &noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

<!-- Google Tag Manager (noscript) -->
<!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KNV3354"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
<!-- End Google Tag Manager (noscript) -->

<style>
    .form-indicar{
        width: 100%;
    }
</style>

<!-- Overlay-->
<script>
    function openNav() {
        document.getElementById("myNav").style.height = "100%";
        $("html,body").css({"overflow":"hidden"});
    }

    function closeNav() {
        document.getElementById("myNav").style.height = "0%";
        $("html,body").css({"overflow":"auto"});
    }
    
    function openAviseMe(id) {
        document.getElementById("aviseMe-"+id).style.height = "100%";
    }

    function closeAviseMe() {
        $(".overlay-avise").height("0%");
    }
    function closeOverlay() {
        $(".overlay").height("0%");
    }
</script>

<script type="text/javascript">    

    $(document).on('click', '.produto-acabou-btn', function(e) {
        e.preventDefault();
        openAviseMe($(this).attr('data-id'));
    });

    $('.overlay-avise, .overlay-avise-content').click(function(e) {
        e.preventDefault();
    });

    $('.overlay-avise-content').click(function(e) {
        e.stopPropagation();
    });
</script>

<script type="text/javascript">    
    $(document).on('click', '.send-acabou-btn', function(e) {
        e.preventDefault();
        loading.show();
        var id = $(this).attr('data-id');
        var form = $('#produto_acabou-'+id);

        var valid = form.validationEngine("validate");
        if (valid == true) {
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + 'produtos/produto-acabou/',
                    data: {
                        name: $('#name_acabou-'+id).val(),
                        email: $('#email_acabou-'+id).val(), 
                        produto_id: $('#produto_id_acabou-'+id).val()   
                    }
                })
                    .done(function (data) {
                        if(data >= 1) {
                            $('.send-acabou-btn').before('<p class="alerta-avise-me" style="color: green">Sucesso!</p>');
                        } else {
                            $('.send-acabou-btn').before('<p class="alerta-avise-me" style="color: red">Erro!</p>');
                        }
                        setTimeout(function() {
                            $('.alerta-avise-me').remove();
                        }, 1800);
                        loading.hide();
                    });
        }else{
            form.validationEngine({
                updatePromptsPosition: true,
                promptPosition: 'inline',
                scroll: false
            });
        }
    });
</script>

<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1321574044525013',
            xfbml      : true,
            version    : 'v2.7'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<?= $this->Form->create(null)?>
<?= isset($_GET['redir']) ?
    $this->Form->input('redirect', [
        'id'            => 'redirect',
        'type'          => 'hidden',
        'value'         => $_GET['redir'],
    ]):
    $this->Form->input('redirect', [
        'id'            => 'redirect',
        'type'          => 'hidden',
        'value'         => 'home',
    ]);
?>
<?= $this->Form->end()?>

<?= $this->Html->css('media_queries.min.css') ?>
<?= $this->Html->css('star-rating.min.css') ?>
<?= $this->Html->css('validationEngine.jquery.custom.min.css') ?>
<?= $this->Html->css('bootstrap-multiselect.css') ?>
<?= $this->Html->css('jquery.fancybox.min.css') ?>
<?= $this->Html->css('nanoscroller.min.css') ?>
<?= $this->Html->css('owl.carousel.css') ?>
<?= $this->Html->css('owl.theme.css') ?>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

<script>
    <?php //file_get_contents(JS_URL.'vendor/bootstrap.min.js') ?>
</script>

<?= $this->Html->script('plugins') ?>
<?= $this->Html->script('filtro') ?>
<?= $this->Html->script('bootstrap.min') ?>
<?= $this->Html->script('jquery.form.min') ?>
<?= $this->Html->script('jquery.countdown.min') ?>
<?= $this->Html->script('jquery-ui-1.11.4.custom/jquery-ui.min') ?>
<?= $this->Html->script('jquery.fancybox.min') ?>
<?= $this->Html->script('bootstrap-multiselect.min') ?>
<?= $this->Html->script('chosen.jquery.min') ?>
<?= $this->Html->script('jquery.nanoscroller.min') ?>
<?= $this->Html->script('owl.carousel.min') ?>
<?= $this->Html->script('jquery.mask.min') ?>
<?= $this->Html->script('jquery.validationEngine.min') ?>
<?= $this->Html->script('jquery.validationEngine-br.min') ?>

<?= $this->Html->script('main.js?'.Version) ?>

<?= $this->fetch('css') ?>
<?= $this->fetch('script') ?>

<?php if(isset($_GET['ind_id']) && $_GET['ind_id'] > 0){ ?>
    <input type="hidden" id="indic_id" name="indic_id" value="<?= $_GET['ind_id'] ?>">
<?php }?>

<?php if(isset($_GET['prods_id']) && $_GET['prods_id'] > 0 && isset($_GET['qtd_prods']) && $_GET['qtd_prods'] > 0){ ?>
    <?php 
        $produtos = $_GET['prods_id'];
        $prods_ids = explode('-', $produtos);
    ?>

    <?php for($i = 0; $i < $_GET['qtd_prods']; $i++) { ?>
        <script type="text/javascript">
            $(window).load(function() {
                $.get(WEBROOT_URL + 'produtos/comprar_ind/' + <?= $prods_ids[$i] ?>,
                    function (data) {
                        $('.laterais.mochila').html(data);

                        $('.produtos-mochila').animate({ 'width' : '228px', 'opacity' : 1 }, function() {
                            $('.info-direita-mochila').fadeIn(200);
                        });
                    });
            });
        </script>
    <?php } ?>
<?php } ?>

<?php if(isset($_GET['busca'])){ ?>
    <script>
        $('.loading').show();
        var busca = '<?= $_GET['busca'] ?>';

        <?php if($_GET['valor_min'] != '' && $_GET['valor_max'] != ''){ ?>
            var valor_min_id = <?= $_GET['valor_min'] ?>;
            var valor_max_id = <?= $_GET['valor_max'] ?>;
        <?php } else { ?>
            var valor_min_id = 1;
            var valor_max_id = 2000;
        <?php } ?>

        <?php if($_GET['ord'] != ''){ ?>
            var ordenar_id = <?= $_GET['ord'] ?>;
        <?php } else { ?>
            var ordenar_id = 0;
        <?php } ?>

        <?php if($_GET['m'] != ''){ ?>
            var marca_id = <?= $_GET['m'] ?>;
        <?php } else { ?>
            var marca_id = 0;
        <?php } ?>

        <?php if($_GET['oid'] != '' && $_GET['cid'] != ''){ ?>
            var objetivo_id  = <?= $_GET['oid'] ?>;
            var categoria_id = <?= $_GET['cid'] ?>;
        <?php } else { ?>
            var objetivo_id  = 0;
            var categoria_id = 0;
        <?php } ?>

        var grade_produtos  = $('#grade-produtos');

        function get_produtos_completo(objetivo, categoria, marca, ordenar, valor_min, valor_max){
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + '/produtos/busca-completa/',
                    data: {query: busca, objetivo: objetivo, categoria: categoria, marca: marca, ordenar: ordenar, valor_min: valor_min, valor_max: valor_max}
                })
                    .done(function (data) {
                        $('html, body').animate({
                            scrollTop: grade_produtos.offset().top - 125
                        }, 700);
                        grade_produtos.html(data);
                        loading.hide();
                    });
        }
        get_produtos_completo(objetivo_id, categoria_id, marca_id, ordenar_id, valor_min_id, valor_max_id);
    </script>
<?php } ?>

<style type="text/css">
    #chat-application {
        z-index: 2!important;
    }
</style>
</body>
</html>