<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt_BR" xmlns='http://www.w3.org/1999/xhtml' xmlns:og='http://ogp.me/ns#'> <!--<![endif]-->

    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="google-site-verification" content="X0LpIu6h8FhuP9butoCMnmCB5UVGkhSzocPpM4NfFzA" />
        <title>LOGFITNESS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0;">
        <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?= WEBROOT_URL?>img/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?= WEBROOT_URL?>img/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <meta name="robots" content="index, follow">
        <meta name="google-site-verification" content="HKMEC3k_UJlkbbtA2qYXhh_kyo55m9jvUgFQ4jDJzUY" />
        <link rel="alternate" hreflang="pt-br" href="https://www.logfitness.com.br">
        <meta name="description" content="Conheça nossa loja de suplementos! Compre no site e retire aqui na academia.">
        <meta name="keywords" content="logfitness, logfitness suplemento, logfitness confiavel, logfitness reclame aqui, logfitness é confiavel, site logfitness, comprar suplementos, comprar suplemento, frete gratis, suplementos baratos, suplementos importados">
        <meta property='og:title' content='LOGFITNESS'/>
        <meta property='og:description' content='Conheça nossa loja de suplementos! Compre no site e retire aqui na academia.'/>
        <meta property='og:image' content='https://www.logfitness.com.br/img/logo_face.jpg'/>
        <meta property='og:url' content='<?= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>'/>
        <meta property='og:type' content='website'/>
        <meta property='og:site_name' content='LOGFITNESS'/>
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-64522723-6"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-64522723-6');
        </script>
        <!-- End Google Analytics -->

        <style>
            .loader {
                position: fixed;
                left: 0;
                top: 0%;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: white;
            }
            .content-loader{
                position: fixed;
                border-radius: 25px;
                left: 20%;
                top: 25%;
                width: 60%;
                height: 50%;
                z-index: 999999;
                background-color: white;
                text-align: center;
            }
            .content-loader img{
                width: 500px;
                margin-top: 45px;
            }
            .content-loader i{
                color: #f4d637;
            }
            .content-loader p{
                color: #5087c7;
                font-family: nexa_boldregular;
                font-size: 34px;
            }
            @media all and (max-width: 430px) {
                .loader{
                    display: none;
                }
               
            }
            @media all and (max-width: 768px) and (min-width: 431px) {
                .content-loader{
                    position: fixed;
                    border-radius: 25px;
                    left: 10%;
                    top: 20%;
                    width: 80%;
                    height: 60%;
                    z-index: 999999;
                    background-color: white;
                    text-align: center;
                }
                .content-loader img{
                    width: 500px;
                    margin-top: 45px;
                }
                .content-loader p{
                    font-size: 28px;
                }
            }
        </style>

        <?= $this->Html->css('font-awesome.min')?>
        <?= $this->Html->css('media_queries.min')?>
        <?= $this->Html->css('freelancer')?>
        <?= $this->Html->css('bootstrap.min')?>
        <?= $this->Html->css('validationEngine.jquery.custom.min')?>
        <?= $this->Html->css('main_mapa') ?>
        <?= $this->Html->css('navbar') ?>
        <?= $this->Html->css('overlay') ?>

        <?= $this->Html->script('freelancer.min')?>
        <?= $this->Html->script('vendor/jquery-1.8.2.min')?>
        <?= $this->Html->script('vendor/jquery-1.11.2.min')?>
        <?= $this->Html->script('vendor/modernizr-2.8.3-respond-1.4.2.min')?>
        <?= $this->Html->script('jquery.validationEngine.min')?>
        <?= $this->Html->script('jquery.validationEngine-br.min')?>
        <?= $this->Html->script('jquery.mask.min.js')?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
        
        <!-- FAZ APARECER OS ICONES NO MAPA -->
        <script>
            // var HTTP_REFERER = "<?php //= $_SERVER['HTTP_REFERER'] ?>";
            var WEBROOT_URL  = "<?= WEBROOT_URL ?>";
        </script>

        <script type="application/ld+json">
        {
          "@context":"http://schema.org",
          "@type":"ItemList",
          "itemListElement":[
            {
              "@type":"ListItem",
              "position":1,
              "url":"https://www.logfitness.com.br/produto/Massa-Nitro-No2-3kg-Pote-Pote-Baunilha-Probiotica-a"
            },
            {
              "@type":"ListItem",
              "position":2,
              "url":"https://www.logfitness.com.br/produto/whey-pro-f-isoflaris-900g-body-action-morango"
            },
            {
              "@type":"ListItem",
              "position":3,
              "url":"https://www.logfitness.com.br/produto/pure-series-proplex-morango-1-020-kg-atlhetica"
            }
          ]
        }
        </script>

        <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "Organization",
          "url": "https://www.logfitness.com.br",
          "logo": "https://www.logfitness.com.br/webroot/logo.png",
          "contactPoint": [{
            "@type": "ContactPoint",
            "telephone": "+55-17-3364-3100",
            "contactType": "customer service"
          }]
        }
        </script>
    </head>
    <body>
<!--[if lt IE 8]>
<p class="browserupgrade">
    Você está utilizando um navegador obsoleto. Vitiste <a href="https://browsehappy.com/"> e atualize seu browser
    para uma melhor experiência web.
</p>
<![endif]-->

<!-- <div class="loader">
    <div class="content-loader">
        <?= $this->Html->image('logfitness.png', ['alt' => 'logfitness'])?><br/><br/>
        <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i><br/><br/>
        <p><strong>Vivemos desafiando os limites, e vc?</strong></p>
    </div>
</div> -->

<!-- Google Tag Manager (noscript) -->
<!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KNV3354"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
<!-- End Google Tag Manager (noscript) -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5&appId=148334755373650";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>


<!-- <div class="loading text-center">
    <span class="helper"></span>
    <i class="fa fa-refresh fa-spin fa-3x fa-fw margin-bottom"></i>
    <span class="">Enviando mensagem...</span>
</div>
 -->

<?php header('Access-Control-Allow-Origin: *'); ?>

    <?= $this->Flash->render() ?>

    <?= $this->fetch('content') ?>
    

<?= $this->Html->css('star-rating.min.css') ?>
<?= $this->Html->css('validationEngine.jquery.custom.min.css') ?>
<?= $this->Html->css('bootstrap-multiselect.css') ?>
<?= $this->Html->css('jquery.fancybox.min.css') ?>

<script>
    <?= file_get_contents(JS_URL.'plugins.js') ?>
    <?= file_get_contents(JS_URL.'jquery.validationEngine.min.js') ?>
    <?= file_get_contents(JS_URL.'jquery.validationEngine-br.min.js') ?>
    <?= file_get_contents(JS_URL.'jquery.mask.min.js') ?>
</script>

<?= $this->Html->script('jquery.form.min.js') ?>
<?= $this->Html->script('jquery.countdown.min.js') ?>
<?= $this->Html->script('jquery-ui-1.11.4.custom/jquery-ui.min.js') ?>
<?= $this->Html->script('jquery.fancybox.min.js') ?>
<?= $this->Html->script('bootstrap-multiselect.js') ?>
<?= $this->Html->script('chosen.jquery.js') ?>
<?= $this->Html->script('enscroll-0.6.2.min.js') ?>

<?= $this->Html->script('main.js') ?>

<?= $this->fetch('css') ?>
<?= $this->fetch('script') ?>

<?php 
    if($_GET['ind_url']) {
        setcookie('indicacao', $_GET['ind_url'], (time() + (2 * 24 * 3600)));
    } 
?>


    <!-- Google Analytics -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = '//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-64522723-3', 'auto');
    ga('send', 'pageview');
</script>

<?php if(isset($academias_map)): ?>
    <script>

        <?php $map = false; ?>

        $(document).ready(function () {

            var map;
            var elevator;
            var myOptions = {
                zoomControl: true,
                scaleControl: false,
                scrollwheel: false,
                disableDoubleClickZoom: false,
                zoom: 4,
                center: new google.maps.LatLng(-15.7217175, -48.0783242),
                mapTypeId: 'terrain'
            };
            map = new google.maps.Map($('#map_canvas')[0], myOptions);

            var mapa_location = 0;

            $(window).scroll(function () {
                if($(this).scrollTop() >= 1800) {
                    if(mapa_location == 0) {
                        navigator.geolocation.getCurrentPosition(function(posicao) {
                            var latlng = posicao.coords.latitude + "," +posicao.coords.longitude; 
                            var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlng + "&sensor=true"; 
                            $.getJSON(url, function (data) { 
                                var endereco = data.results[2].formatted_address.split(", "); 

                                cidade = endereco[0];
                                estado = endereco[1];

                                var uf = '';

                                switch (estado){
                                    case 'AC' : uf = 1; break;
                                    case 'AL' : uf = 2; break;
                                    case 'AM' : uf = 3; break;
                                    case 'AP' : uf = 4; break;
                                    case 'BA' : uf = 5; break;
                                    case 'CE' : uf = 6; break;
                                    case 'DF' : uf = 7; break;
                                    case 'ES' : uf = 8; break;
                                    case 'GO' : uf = 9; break;
                                    case 'MA' : uf = 10; break;
                                    case 'MG' : uf = 11; break;
                                    case 'MS' : uf = 12; break;
                                    case 'MT' : uf = 13; break;
                                    case 'PA' : uf = 14; break;
                                    case 'PB' : uf = 15; break;
                                    case 'PE' : uf = 16; break;
                                    case 'PI' : uf = 17; break;
                                    case 'PR' : uf = 18; break;
                                    case 'RJ' : uf = 19; break;
                                    case 'RN' : uf = 20; break;
                                    case 'RO' : uf = 21; break;
                                    case 'RR' : uf = 22; break;
                                    case 'RS' : uf = 23; break;
                                    case 'SC' : uf = 24; break;
                                    case 'SE' : uf = 25; break;
                                    case 'SP' : uf = 26; break;
                                    case 'TO' : uf = 27; break;
                                    default : uf = '';
                                }

                                $('#states_academias option[value='+uf+']').attr('selected','selected');

                                $.get(WEBROOT_URL + '/cidades/' + uf + '/1',
                                function(data){
                                    $('#city').html(data);
                                    
                                    $('#city').children().each(function() {
                                        if($(this).text() == cidade) {
                                            cidade_val = $(this).val();

                                            $('#city option[value='+cidade_val+']').attr('selected','selected');

                                            $.get(WEBROOT_URL + 'academias/busca-cidade/' + uf + '/' + cidade_val,
                                            function(data){
                                                $('#select-academia').html(data);
                                            });

                                            var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + cidade + estado  + "&sensor=true"; 
                                            $.getJSON(url, function (data) { 
                                                setTimeout(function() {
                                                    var location = new google.maps.LatLng(data.results[0].geometry.location.lat, data.results[0].geometry.location.lng);
                                                    map.setCenter(location);
                                                    map.setZoom(13);
                                                }, 1500);
                                            });
                                            return false;
                                        }
                                    });
                                });
                            });
                        });
                        mapa_location = 1;
                    }
                }
            });
            

            <?php
            $academias_address = '';
            $academias_info = '';

            foreach($academias_map as $academia):
            $academias_address =
                "'"
                . $academia->address . ','
                . $academia->number . ','
                . $academia->area . ','
                . str_replace("'", '´', $academia->city->name) . ','
                . $academia->city->uf
                . "'";

            $academias_info =

                str_replace("'", '´', $academia->shortname) . ' :: ' . str_replace("'", '´', $academia->city->name) . '-' . $academia->city->uf;

            $pass = 0;

            if(!empty($academia->latitude) && !empty($academia->longitude)){
            ?>
            var latlng = new google.maps.LatLng(<?= $academia->latitude; ?>, <?= $academia->longitude; ?>);
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: "<?= $academias_info; ?>",
                icon: WEBROOT_URL + 'img/map-marker.png'
                //shadow:     WEBROOT_URL + 'img/map-marker.png'
            });

            <?php if($academia->city_id == 5179) { ?>
                function attachSecretMessage(marker, secretMessage) {
                  var infowindow = new google.maps.InfoWindow({
                    content: secretMessage
                  });

                  marker.addListener('click', function() {
                    infowindow.open(marker.get('map'), marker);
                  });
                }

                attachSecretMessage(marker, '<center>Já chegamos até em Fernando de Noronha!<br>Pela sua curiosidade, utilize o cupom: "Noronha10" e ganhe 10% de desconto!</center>');
            <?php } ?>

            google.maps.event.addListener(marker, 'click', function () {
                select_academia("<?= $academia->city->uf ?>", "<?= $academia->city->name ?>", <?= $academia->id; ?>);
            });

            <?php
            }else{
            //$pass = $pass + 100;
            /*if($pass >= 10) {
                //sleep(1);
                $pass = 0;
            }*/
            ?>
            //setTimeout(function () {
            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + <?= $academias_address; ?> +'&sensor=false',
                null,
                function (data) {
                    //console.log(data);
                    var p = [];
                    if (data.results[0].geometry.location) {
                        p = data.results[0].geometry.location;
                    } else {
                        p = data.results[0]['geometry']['location'];
                    }

                    var latlng = new google.maps.LatLng(p.lat, p.lng);
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: "<?= $academias_info; ?>",
                        icon: WEBROOT_URL + 'img/map-marker.png'
                        //shadow:     WEBROOT_URL + 'img/map-marker.png'
                    });

                    google.maps.event.addListener(marker, 'click', function () {
                        select_academia("<?= $academia->city->uf ?>", "<?= $academia->city->name ?>", <?= $academia->id; ?>);
                    });

                    $.ajax({
                        type: "POST",
                        url: WEBROOT_URL + "academia-cordinates-add/" + <?= $academia->id; ?> +'/' + p.lat + '/' + p.lng,
                        data: {}
                    }).done(function (data_academia) {
                        //console.log(data_academia);
                    });
                });
            //}, <?= $pass; ?>);
            <?php
            }
            endforeach;
            ?>

        });

    </script>

    <!--
    <script>
        $(document).ready(function () {
            var map;
            var elevator;
            var myOptions = {
                zoom: 4,
                center: new google.maps.LatLng(-15.7217175,-48.0783242),
                mapTypeId: 'terrain'
            };
            map = new google.maps.Map($('#map_canvas')[0], myOptions);

            <?php
    $academias_address  = '';
    $academias_info     = '';

    foreach($academias_map as $academia){
        $academias_address .=
            "'"
            .$academia->address.','
            .$academia->number.','
            .$academia->area.','
            .str_replace("'", '´', $academia->city->name).','
            .$academia->city->uf
            ."',";

        $academias_info .=
            "['"
            .$academia->id
            ."','"
            .str_replace("'", '´', $academia->name).' :: '.str_replace("'", '´', $academia->city->name).'-'.$academia->city->uf
            ."'],"
        ;
    }?>

            var addresses = [
                <?= $academias_address; ?>
            ];

            var infos = [
                <?= $academias_info; ?>
            ];

            var academia_name   = '';
            var academia_id     = 0;
            for(var x = 0; x < addresses.length; x++) {
                academia_name   = infos[x][1];
                academia_id     = infos[x][0];

                console.log(infos[x][1]);

                $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+addresses[x]+'&sensor=false', null, function (data) {

                }).done(function(data){
                    var p = data.results[0].geometry.location;
                    var latlng = new google.maps.LatLng(p.lat, p.lng);
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: academia_name
                    });

                    google.maps.event.addListener(marker, 'click', function() {
                        //infowindow.open(map,marker);
                        select_academia(academia_id);
                    });
                });
            }
        });
    </script> -->
    <!--<script src="https://maps.google.com/maps/api/js?sensor=false&.js"></script>-->
        <script src="https://maps.google.com/maps/api/js?key=AIzaSyCba7qYdWhCMKwndRe8DC6TgHYq14jcRy4"></script>

        <?php endif; ?>



    </body>
</hmtl> 