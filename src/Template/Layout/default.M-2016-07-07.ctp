<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->

<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>LogFitness <?php /*= $this->fetch('title')*/ ?></title>
    <meta name="description" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0;">

    <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= WEBROOT_URL?>img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= WEBROOT_URL?>img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <style>
        <?= file_get_contents(CSS_URL.'bootstrap.min.css') ?>
        <?= file_get_contents(CSS_URL.'bootstrap-theme.min.css') ?>
    </style>
    <?php /*= $this->Html->css('bootstrap.min.css')*/ ?>
    <?php /*= $this->Html->css('bootstrap-theme.min.css')*/ ?>
    <?= $this->Html->css('main.min.css') ?>
    <?= $this->Html->css('media_queries.min.css') ?>

    <?= $this->Html->script('vendor/modernizr-2.8.3-respond-1.4.2.min.js') ?>

    <?php /*= $this->Html->meta('favicon')*/ ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script>
        // var HTTP_REFERER = "<?php //= $_SERVER['HTTP_REFERER'] ?>";
        var WEBROOT_URL  = "<?= WEBROOT_URL ?>";
    </script>

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">
    Você está utilizando um navegador obsoleto. Vitiste <a href="http://browsehappy.com/"> e atualize seu browser
    para uma melhor experiência web.
</p>
<![endif]-->

<?php header('Access-Control-Allow-Origin: *'); ?>

<!--
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1321574044525013',
            xfbml      : true,
            version    : 'v2.6'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<div
    class="fb-like"
    data-share="true"
    data-width="450"
    data-show-faces="true">
</div>
-->

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5&appId=148334755373650";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>


<div class="fundo-degrade">

    <?= $this->request->is('mobile') ? $this->Element('navbar_mobile') : $this->Element('navbar'); ?>

    <div class="loading text-center">
        <span class="helper"></span>
        <i class="fa fa-refresh fa-spin fa-3x fa-fw margin-bottom"></i>
        <span class="">Atualizando conteúdo...</span>
    </div>

    <?= $this->fetch('content') ?>

    <?= !$this->request->isMobile() ? $this->Element('instagram') : ''; ?>

    <?= $this->Element('footer'); ?>

</div>

<?php /*if(!isset($CLIENTE) && !isset($disable_modal_login)):  ?>
    <div id="login" style="display: block;">
        <?= $this->Element('cliente_login', ['academia_slug' => $academia_slug]); ?>
    </div>
<?php endif;*/ ?>

<?= $this->Form->create(null)?>
<?= isset($_GET['redir']) ?
    $this->Form->input('redirect', [
        'id'            => 'redirect',
        'type'          => 'hidden',
        'value'         => $_GET['redir'],
    ]):
    $this->Form->input('redirect', [
        'id'            => 'redirect',
        'type'          => 'hidden',
        'value'         => 'home',
    ]);
?>
<?= $this->Form->end()?>

<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>window.jQuery || document.write('<script src="<?= FULL_URL; ?>js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<?= $this->Html->css('star-rating.min.css') ?>
<?= $this->Html->css('validationEngine.jquery.custom.min.css') ?>
<?= $this->Html->css('bootstrap-multiselect.css') ?>
<?= $this->Html->css('jquery.fancybox.min.css') ?>
<?= $this->Html->css('nanoscroller.css') ?>

<?= $this->Html->script('vendor/bootstrap.min.js') ?>
<?= $this->Html->script('plugins.js') ?>
<?= $this->Html->script('star-rating.min.js') ?>
<?= $this->Html->script('star-rating_locale_pt-br.min.js') ?>
<?= $this->Html->script('jquery.validationEngine.min.js') ?>
<?= $this->Html->script('jquery.validationEngine-br.min.js') ?>
<?= $this->Html->script('jquery.mask.min.js') ?>
<?= $this->Html->script('jquery.form.min.js') ?>
<?= $this->Html->script('jquery.countdown.min.js') ?>
<?= $this->Html->script('jquery-ui-1.11.4.custom/jquery-ui.min.js') ?>
<?= $this->Html->script('jquery.fancybox.min.js') ?>
<?= $this->Html->script('bootstrap-multiselect.js') ?>
<?= $this->Html->script('chosen.jquery.js') ?>
<?php /*= $this->Html->script('enscroll-0.6.2.min.js')*/ ?>
<?= $this->Html->script('jquery.nanoscroller.min.js') ?>

<?= $this->Html->script('main.js') ?>

<?= $this->fetch('css') ?>
<?= $this->fetch('script') ?>

<!-- Google Analytics -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = '//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-64522723-3', 'auto');
    ga('send', 'pageview');
</script>

<?php if(isset($academias_map)): ?>
    <script>

        $(document).ready(function () {
            var map;
            var elevator;
            var myOptions = {
                zoomControl: true,
                scaleControl: false,
                scrollwheel: false,
                disableDoubleClickZoom: false,
                zoom: 4,
                center: new google.maps.LatLng(-15.7217175,-48.0783242),
                mapTypeId: 'terrain'
            };
            map = new google.maps.Map($('#map_canvas')[0], myOptions);

            <?php
            $academias_address  = '';
            $academias_info     = '';

            foreach($academias_map as $academia):
            $academias_address =
                "'"
                .$academia->address.','
                .$academia->number.','
                .$academia->area.','
                .str_replace("'", '´', $academia->city->name).','
                .$academia->city->uf
                ."'";

            $academias_info =

                str_replace("'", '´', $academia->shortname).' :: '.str_replace("'", '´', $academia->city->name).'-'.$academia->city->uf
                ;

            $pass = 0;

            if(!empty($academia->latitude) && !empty($academia->longitude)){
            ?>
            var latlng = new google.maps.LatLng(<?= $academia->latitude; ?>, <?= $academia->longitude; ?>);
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: "<?= $academias_info; ?>",
                icon:       WEBROOT_URL + 'img/map-marker.png'
                //shadow:     WEBROOT_URL + 'img/map-marker.png'
            });

            google.maps.event.addListener(marker, 'click', function () {
                //infowindow.open(map,marker);
                select_academia(<?= $academia->id; ?>);
            });

            <?php
            }else{
            //$pass = $pass + 100;
            /*if($pass >= 10) {
                //sleep(1);
                $pass = 0;
            }*/
            ?>
            //setTimeout(function () {
                $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address=' + <?= $academias_address; ?> +'&sensor=false',
                    null,
                    function (data) {
                        //console.log(data);
                        var p = [];
                        if(data.results[0].geometry.location){
                            p = data.results[0].geometry.location;
                        }else{
                            p = data.results[0]['geometry']['location'];
                        }

                        var latlng = new google.maps.LatLng(p.lat, p.lng);
                        var marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            title: "<?= $academias_info; ?>",
                            icon:       WEBROOT_URL + 'img/map-marker.png'
                            //shadow:     WEBROOT_URL + 'img/map-marker.png'
                        });

                        google.maps.event.addListener(marker, 'click', function () {
                            //infowindow.open(map,marker);
                            select_academia(<?= $academia->id; ?>);
                        });

                        $.ajax({
                            type: "POST",
                            url: WEBROOT_URL + "academia-cordinates-add/" + <?= $academia->id; ?> + '/' + p.lat + '/' + p.lng,
                            data: {}
                        }).done(function (data_academia) {
                            //console.log(data_academia);
                        });
                    });
            //}, <?= $pass; ?>);
            <?php
            }
            endforeach;
            ?>
        });
    </script>

    <!--
    <script>
        $(document).ready(function () {
            var map;
            var elevator;
            var myOptions = {
                zoom: 4,
                center: new google.maps.LatLng(-15.7217175,-48.0783242),
                mapTypeId: 'terrain'
            };
            map = new google.maps.Map($('#map_canvas')[0], myOptions);

            <?php
    $academias_address  = '';
    $academias_info     = '';

    foreach($academias_map as $academia){
        $academias_address .=
            "'"
            .$academia->address.','
            .$academia->number.','
            .$academia->area.','
            .str_replace("'", '´', $academia->city->name).','
            .$academia->city->uf
            ."',";

        $academias_info .=
            "['"
            .$academia->id
            ."','"
            .str_replace("'", '´', $academia->name).' :: '.str_replace("'", '´', $academia->city->name).'-'.$academia->city->uf
            ."'],"
        ;
    }?>

            var addresses = [
                <?= $academias_address; ?>
            ];

            var infos = [
                <?= $academias_info; ?>
            ];

            var academia_name   = '';
            var academia_id     = 0;
            for(var x = 0; x < addresses.length; x++) {
                academia_name   = infos[x][1];
                academia_id     = infos[x][0];

                console.log(infos[x][1]);

                $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address='+addresses[x]+'&sensor=false', null, function (data) {

                }).done(function(data){
                    var p = data.results[0].geometry.location;
                    var latlng = new google.maps.LatLng(p.lat, p.lng);
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: academia_name
                    });

                    google.maps.event.addListener(marker, 'click', function() {
                        //infowindow.open(map,marker);
                        select_academia(academia_id);
                    });
                });
            }
        });
    </script> -->
    <script src="http://maps.google.com/maps/api/js?sensor=false&.js"></script>
<?php endif; ?>
</body>
</html>