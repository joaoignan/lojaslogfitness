<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="pt_BR"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="pt_BR"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="pt_BR"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt_BR" xmlns='http://www.w3.org/1999/xhtml' xmlns:og='http://ogp.me/ns#'> <!--<![endif]-->
    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="google-site-verification" content="X0LpIu6h8FhuP9butoCMnmCB5UVGkhSzocPpM4NfFzA" />
        <title>LOGFITNESS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">
        <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?= WEBROOT_URL?>img/favicon/manifest.json">

        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?= WEBROOT_URL?>img/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <meta name="keywords" content="academia, academias, plataforma, logfitness, suplementos, atleta, loja virtual, frete free, frete grátis, log, startup, whey, massa, musculação, netshoes">
        <meta name="robots" content="index, follow">

        <meta property='og:title' content='LOGFITNESS'/>
        <?php if($produto) { ?>
            <?php $fotos = unserialize($produto->fotos); ?>
            <meta property='og:description' content='O aluno da academia, realiza a compra do suplemento, efetua o pagamento online e recebe o produto na própria academia, com frete grátis.'/>
            <meta property='og:image' content='https://www.logfitness.com.br/img/produtos/md-<?= $fotos[0] ?>'/>
        <?php } else { ?>
            <meta property='og:description' content='Conheça nossa loja de suplementos! Compre no site e retire aqui na academia.'/>
            <meta property='og:image' content='https://www.logfitness.com.br/img/logo_face.jpg'/>
        <?php } ?>
        <meta property='og:url' content='<?= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>'/>
        <meta property='og:type' content='website'/>
        <meta property='og:site_name' content='LOGFITNESS'/>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

        <script type="text/javascript" src="https://js.iugu.com/v2"></script>

        <?= $this->Html->css('topo_default')?>
        <?= $this->Html->css('profiles')?>
        <?= $this->Html->css('font-awesome.min')?>
        <?= $this->Html->css('bootstrap.min')?>
        <?= $this->Html->css('owl.carousel.css')?>
        <?= $this->Html->css('owl.theme.css')?>
        <?= $this->Html->css('jquery.fancybox') ?>
        <?= $this->Html->css('navbar') ?>
        <?= $this->Html->css('overlay') ?>
        <?= $this->Html->script('bootstrap.min')?>
        <?= $this->Html->script('jquery.min') ?>
        <?= $this->Html->script('jquery.fancybox') ?>
        <?= $this->Html->script('owl-carousel/owl.carousel') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>

        <?= $this->Flash->render() ?>

        <!-- Google Analytics -->
        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                    function () {
                        (b[l].q = b[l].q || []).push(arguments)
                    });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-64522723-3', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- End Google Analytics -->

        <!-- Smart Look -->
        <script type="text/javascript">
            window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', '65dd1b31ade411a0f1f09a9524c2adf7c3ca5eb5');
        </script>
        <!-- End Smart Look -->

        <!-- OVERLAY AVALIAÇÂO -->
        <script>
            function openNav() {
                document.getElementById("myNav").style.height = "100%";
            }

            function closeNav() {
                document.getElementById("myNav").style.height = "0%";
            }
        </script>
        <!-- FIM OVERLAY AVALIAÇÂO -->
        <!-- OVERLAY REPORT -->
        <script>
            function openReport() {
                document.getElementById("Report").style.height = "100%";
            }

            function closeReport() {
                document.getElementById("Report").style.height = "0%";
            }
        </script>
        <!-- FIM OVERLAY REPORT -->


        <!-- MASCARA DE TELEFONE -->
        <script>
            $(document).ready(function(){
               var maskBehavior = function (val) {
                 return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
                },
                options = {onKeyPress: function(val, e, field, options) {
                 field.mask(maskBehavior.apply({}, arguments), options);
                 }
                };
                $('.phone').mask(maskBehavior, options);
            });
            $(document).ready(function(){
              $('.time').mask('00:00');
            });
        </script>

        <!-- MASCARA DE HORARIO -->
        <script type="text/javascript">
            $(document).ready(function(){
                $(".horario").mask("99:99 - 99:99");
            });
        </script>
        
        <!-- SMOTH SCROLL -->
        <script>
            $(document).on('click', 'a[href^="#"]', function(event){
                event.preventDefault();
            $('html, body').animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top
                }, 500);
            });
        </script>
        <script>
            var WEBROOT_URL  = "<?= WEBROOT_URL ?>";
        </script>

    <style>
        /* Always set the map height explicitly to define the size of the div
        * element that contains the map. */
        #map {
            height: 100%;
        }
        .rating-azul {
            color: #73a1d9!important;
        }
    </style>

    </head>
    <body>
        <?= $this->Element('navbar-default'); ?>
        <?= $this->Element('topo_default'); ?>
        <div class="container">
            <?= $this->fetch('content') ?>
        </div>
    <?= $this->Element('footer-default'); ?>
            
    <script>
        <?= file_get_contents(JS_URL.'vendor/bootstrap.min.js') ?>
        <?= file_get_contents(JS_URL.'plugins.js') ?>
        <?= file_get_contents(JS_URL.'star-rating.min.js') ?>
        <?= file_get_contents(JS_URL.'star-rating_locale_pt-br.min.js') ?>
        <?= file_get_contents(JS_URL.'jquery.validationEngine.min.js') ?>
        <?= file_get_contents(JS_URL.'jquery.validationEngine-br.min.js') ?>
        <?= file_get_contents(JS_URL.'jquery.mask.min.js') ?>
    </script>

</body>
</html>