<!doctype html>

<html class="no-js" lang="pt_BR" xmlns='http://www.w3.org/1999/xhtml' xmlns:og='http://ogp.me/ns#'>

<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="google-site-verification" content="X0LpIu6h8FhuP9butoCMnmCB5UVGkhSzocPpM4NfFzA" />
    <title>LOGFITNESS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= WEBROOT_URL?>img/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= WEBROOT_URL?>img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta name="keywords" content="academia, academias, plataforma, logfitness, suplementos, atleta, loja virtual, frete free, frete grátis, log, startup, whey, massa, musculação, netshoes">
    <meta name="robots" content="index, follow">

    <meta name="google-site-verification" content="HKMEC3k_UJlkbbtA2qYXhh_kyo55m9jvUgFQ4jDJzUY" />
    <link rel="alternate" hreflang="pt-br" href="https://www.logfitness.com.br">

    <meta property='og:title' content='LOGFITNESS'/>
    <?php if($produto) { ?>
        <?php $fotos = unserialize($produto->fotos); ?>
        <!-- <meta property='og:description' content='O aluno da academia, realiza a compra do suplemento, efetua o pagamento online e recebe o produto na própria academia, com frete grátis.'/> -->
        <?php $produto->preco_promo ? $valor = $produto->preco_promo : $valor = $produto->preco ?>
        <?php $produto->preco_promo ? $valor_dose = $produto->preco_promo / $produto->doses : $valor_dose = $produto->preco / $produto->doses ?>
        <meta property='og:description' content='<?= $produto->produto_base->name.' '.$produto->marca.' - R$'.number_format($valor, 2, ',', '.').' - '.$produto->doses.' Treinos - R$'.number_format($valor_dose, 2, ',', '.').' por dose' ?>'/>
        <meta property='og:image' content='https://www.logfitness.com.br/img/produtos/md-<?= $fotos[0] ?>'/>
    <?php } else { ?>
        <meta property='og:description' content='Conheça nossa loja de suplementos! Compre no site e retire aqui na academia.'/>
        <meta property='og:image' content='https://www.logfitness.com.br/img/logo_face.jpg'/>
    <?php } ?>
    <meta property='og:url' content='<?= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>'/>
    <meta property='og:type' content='website'/>
    <meta property='og:site_name' content='LOGFITNESS'/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	
	<!-- CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-4" style="border: 2px solid blue">
				<p>produtos + codigo</p>
				<p>nome</p></p>
				<p>marca-desk</p>
			</div>
			<div class="col-xs-12 col-md-8" style="border: 2px solid red">
				<div class="row">
					<div class="col-6 order-1">
						<img src="http://lorempixel.com/320/320/" alt="" style="width: 100%;">
					</div>
					<div class="col-6 order-3 order-md-2 align-self-start">
						<p>dropdown tamanho</p>
						<p>dropdown sabor</p>
					</div>
					<div class="col-6 order-2 order-md-3 align-self-start">
						<p>dados e preço</p>
						<p>dados e preço</p>
						<p>dados e preço</p>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-4" style="border: 2px solid purple">
				<p>Sugerir</p>
				<p>comparar</p>
			</div>
		</div>
	</div>
	
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>
</html>