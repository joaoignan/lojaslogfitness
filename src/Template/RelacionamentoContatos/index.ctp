<div class="container informacoes text-center">
    <div class="col-lg-6 central-relacionamento central-relacionamento2">
        <?= $this->Html->image('central-de-relacionamento.png', ['alt' => 'Central de Relacionamento'])?>
        <br class="clear">
        <div class="absolut-center-100">
            <span class="titulo">CENTRAL DE RELACIONAMENTO</span>
        </div>
    </div>
    <div class="col-lg-6 central-relacionamento central-relacionamento2">
        <p>
            Nos envie sua sugestão, dúvida ou crítica!
        </p>
        <div class="absolut-center-100">
            <a id="form-toggle-open">CONTATO</a>
        </div>
    </div>
</div>

<div class="container info-central-box form-toggle" id="form-toggle">

    <!--<div class="col-lg-4 form-toggle-info">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam beatae culpa cum dolorum eos error incidunt inventore, natus odio, optio porro praesentium quae quis velit. Iusto magnam sint velit.
        <br>
        <br>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam beatae culpa cum dolorum eos error incidunt inventore, natus odio, optio porro praesentium quae quis velit. Iusto magnam sint velit.
    </div>-->

    <?= $this->Form->create(null, [
        'id'        => 'form-central-relacionamento',
        'role'      => 'form',
        'default'   => false,
        'type'      => 'file'
    ]) ?>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('name', [
                'div'           => false,
                'label'         => 'Nome',
                'placeholder'   => 'Nome Completo',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('phone', [
                'div'           => false,
                'label'         => 'Telefone',
                'placeholder'   => 'Telefone',
                'class'         => 'validate[required] form-control phone-mask',
            ]) ?>
        </div>

        <div class="col-lg-5">
            <?= $this->Form->input('email', [
                'div'           => false,
                'label'         => 'Email',
                'placeholder'   => 'Email',
                'class'         => 'validate[required,custom[email]] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('description', [
                'div'           => false,
                'label'         => 'Assunto',
                'placeholder'   => 'Assunto / Descrição',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('message', [
                'type'          => 'textarea',
                'div'           => false,
                'label'         => 'Mensagem',
                'placeholder'   => 'Mensagem',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">

        <div class="col-lg-2">
            <?= $this->Form->button('Enviar Cadastro', [
                'id'            => 'submit-cadastrar-central-relacionamento',
                'type'          => 'button',
                'div'           => false,
                'class'         => 'btn btn-default float-right',
            ]) ?>

        </div>

        <div class="col-lg-5" id="form-toggle-message"></div>
    </div>

    <?= $this->Form->end() ?>
</div>

<!-- INFORMAÇÕES  -->
<div class="container">
    <?= $this->Element('bloco_seja_um_parceiro')?>

    <?= $this->Element('bloco_academias')?>

    <?= $this->Element('bloco_atletas')?>

</div>