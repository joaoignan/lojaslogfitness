<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->layout = 'error';

if (Configure::read('debug')):
    $this->layout = 'dev_error';

    $this->assign('title', $message);
    $this->assign('templateName', 'error400.ctp');

    $this->start('file');
?>
<?php if (!empty($error->queryString)) : ?>
    <p class="notice">
        <strong>SQL Query: </strong>
        <?= h($error->queryString) ?>
    </p>
<?php endif; ?>
<?php if (!empty($error->params)) : ?>
        <strong>SQL Query Params: </strong>
        <?= Debugger::dump($error->params) ?>
<?php endif; ?>
<?= $this->element('auto_table_warning') ?>
<?php
    if (extension_loaded('xdebug')):
        xdebug_print_function_stack();
    endif;

    $this->end();
endif;
?>

<style>
    .conteudo{
        text-align: center;
    }
    .botao{
        text-align: center;
    }
    .conteudo img{
        margin-top: 50px;
        margin-bottom: 20px;
    }
    .btn-inicio{
        border: 2px solid;
        border-color: #5087c7;
        background-color: white;
        color: #5087c7;
        text-transform: uppercase;
        padding: 3px 19px;
        font-family: nexa_boldregular;
    }
    @media screen and (max-width: 425px) {
        .conteudo img{
            width: 80%;
        }
    }
    @media (min-width: 768px) {
        .conteudo img{
            width: 55%;
            margin-top: 10px;
            
        }
    }
    @media (min-width: 1024px) {
        .conteudo img{
            width: 32%;
            margin-top: 10px;
            
        }
    }
</style>

<div class="conteudo text-center">
    <?= $this->Html->image('seloerro400.png', ['alt' => 'LogFitness']); ?>
</div>
<div class="botao text-center">
    <?= $this->Html->link('Início',
    '/',
    ['class' => 'btn-inicio', 'escape' => false,]) ?>
</div>

<!-- Google Analytics -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = '//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-64522723-3', 'auto');
    ga('send', 'pageview');
</script>

</body>
</html>
