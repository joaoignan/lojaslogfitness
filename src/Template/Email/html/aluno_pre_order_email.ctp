<style>
    body{
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        text-align: center;
    }
    a{
      color: #5087C7;
    }
    a:hover{
      color: #2e4e73;
    }
    a:visited{
      color: #68acfb;
    }
    .table-total{
      width: 60%;
      background-color: white;
      border: 1px solid rgba(0, 0, 0, 0.10);
      text-align: center;
      position: relative;
      left: 20%;
    }
    .amarelo-log{
      background-color: #f4d637;
    }
    .azul-log{
      background-color: #5087C7;
    }
    .assunto{
      height: 100px;
      margin-bottom: 20px;
    }
    .assunto h1{
      color: white;
      margin-top: 19px;
    }
    .separador-midias{
      position: relative;
      width: 90%;
      border: 1px solid rgba(0, 0, 0, 0.10);
    }
    .separador-footer{
      position: relative;
      width: 5%;
      border: 1px solid rgba(0, 0, 0, 0.10);
    }
    .logo-log{
      height: 20px;
    }
    .logo-acad{
      height: 60px;
    }
    .logos{
      height: 70px
    }
    .nossa-logo{
      text-align: right;
      width: 50%;
      padding-right: 25px;
    }
    .logo-deles{
      text-align: left;
      width: 50%;
      padding-left: 25px;
    }
    .midias{
      width: 30px;
      height: 30px;
      border-radius: 25px;
      background: #5087C7;
      display: inline-block;
      margin: 0 15px;
    }
    .img-midias{
      margin-top: 5px;
    }
    .img-midias2{
      margin-top: 7px;
    }
</style>
<table class="table-total">
  <tr class="logos">
    <td class="logo-deles">
      <?= $logo_academia ?
        $this->Html->image(WEBROOT_URL.'/img/academias/'.$logo_academia,
          ['class' => 'logo-acad', 'alt' => 'logfitness logo'])
        :
        '<p class="logo-log" alt="'.$academia.'">'.$academia.'</p>';
      ?>
    </td>
    <td class="nossa-logo">
      <?= $this->Html->image(WEBROOT_URL.'/img/logfitness.png',
          ['class' => 'logo-log', 'alt' => 'logfitness logo'])
      ?>
    </td>
  </tr>
  <tr class="azul-log assunto">
    <td colspan="2">
      <h1>Mochila personalizada</h2>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <br />
      <h2>Olá, <?= $name; ?>!</h2>
      <br>
      <p>O professor <strong><?= $professor; ?></strong> lhe convidou para fazer parte da LogFitness e junto lhe enviou uma indicação de produto(s).
          <br>
          <br>
          <br>
          <p>Faça login usando seu <strong>email</strong> e a senha <strong><?= $senha ?></strong></p>
          <br>
          <br>
          <br>
          Clique no link para <strong><?= $this->Html->link(
              'visualizar a indicação',
              WEBROOT_URL.'minha-conta/indicacao-de-produtos/'
          )?>.</strong>
          <br>
          <br>
          <br>
          Ou se desejar <?= $this->Html->link(
              'faça login',
              WEBROOT_URL
          )?> na sua conta <strong>LogFitness</strong> e
          <br>
          acesse "<strong>Indicações</strong>" no menu para visualizar todas as indicações recebidas.
      </p>
      <br>
      <br>
      <hr class='separador-midias' />
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <p>Em caso de qualquer dúvida, fique à vontade para responder esse email ou nos contatar no <a href="mailto:sos@logfitness.com.br">sos@logfitness.com.br</a>.</p>
      <br />
    </td>
  </tr>
  <tr>
    <td colspan="2">
        <?= $this->Html->link(
          $this->Html->image(
          WEBROOT_URL.'/img/spotify-email.png',
          ['class' => 'img-midias',
          'alt'   => 'Spotity',
          'title' => 'Ouça nossas playlists',
          'escape' => false]),
        'https://open.spotify.com/user/logfitness',
        ['escape' => false, 'class' => 'midias'])?>

        <?= $this->Html->link(
          $this->Html->image(
          WEBROOT_URL.'/img/facebook-email.png',
          ['class' => 'img-midias',
          'alt'   => 'Facebook',
          'title' => 'Curta nossa página',
          'escape' => false]),
        'https://www.facebook.com/logfitness.com.br',
        ['escape' => false, 'class' => 'midias'])?>

        <?= $this->Html->link(
          $this->Html->image(
          WEBROOT_URL.'/img/insta-email.png',
          ['class' => 'img-midias',
          'alt'   => 'Instagram',
          'title' => 'Acompanhe nossos momentos',
          'escape' => false]),
        'https://www.instagram.com/logfitness.com.br/',
        ['escape' => false, 'class' => 'midias'])?>

        <?= $this->Html->link(
          $this->Html->image(
          WEBROOT_URL.'/img/twitter-email.png',
          ['class' => 'img-midias',
          'alt'   => 'Twitter',
          'title' => 'Siga nosso Twitter',
          'escape' => false]),
        'https://twitter.com/LOGFITNESS',
        ['escape' => false, 'class' => 'midias'])
        ?>
      <hr class='separador-footer' />
      <br />
      <p>LogFitness &copy; <?= date('Y') ?></p>
    </td>
  </tr>
</table>
