<table cellspacing="0" cellpadding="0" border="1" width="600" bordercolor="#eeeeee">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="25" border="0" width="100%">
          <tr>
            <th width="40%" align="center" height="90">
              <?php if ($academia_id != 601 ) { ?>
                <?= $logo_academia ?
                  $this->Html->image(WEBROOT_URL.'/img/academias/'.$logo_academia,
                    ['style' => 'height: 60px', 'alt' => 'academia logo'])
                  :
                  '<p alt="'.$academia.'">'.$academia.'</p>';
                  ?>  
               <?php }?>
            </th>
            <th width="20%" align="center" height="90"></th>
            <th width="40%" align="center" height="90">
              <?= $this->Html->image(WEBROOT_URL.'/img/logo_master.png',
                    ['style' => 'height: 60px', 'alt' => 'Empório Natural'])
                  ?>
            </th>
          </tr>
      </table>
      <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tbody>
          <tr>
            <td colspan="6" width="100%" align="center" height="100" >
              <?= $this->Html->image(WEBROOT_URL.'/img/mochila_fechada_emporio.jpg',
                    ['alt' => 'mochila fechada'])
                  ?>
            </td>
          </tr>
          <tr>
            <td colspan="6" width="100%" align="center">
              <br />
                <p style="font-family: Helvetica; font-size: 23px; color: #009933"><strong>Olá <?= $name ?> =)</strong></p>
                <br>
                <p style="font-family: Helvetica;">Você finalizou sua mochila do pedido <strong><?= $id ?></strong>. Falta pouco para mandarmos o seu pedido. Entre no <?= $this->Html->link('link', $url) ?> e realize o pagamento.</p>
                <br>
                <br>
                <hr style="width: 80%" />
                <p style="font-family: Helvetica;">Em caso de qualquer dúvida, fique à vontade para responder esse email ou nos contatar no <a href="mailto:<?= $email_contato  ?>"><?= $email_contato ?></a>.</p>
                  <br />
            </td>
          </tr>
          <tr>
            <td colspan="6" height="15px"></td>
          </tr>
          <tr>
            <td colspan="6" align="center">
              <br />
              <hr style="width: 5%" />
              <br />
              <p style="font-family: Helvetica;">Powered by logfitness &copy; <?= date('Y') ?></p>
              <br />
            </td>
          </tr>
          <tr>
            <td colspan="6" height="15px"></td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
</table>