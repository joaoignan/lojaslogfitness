<table cellspacing="0" cellpadding="0" border="1" width="600" bordercolor="#eeeeee">
  <?php $fotos = unserialize($produto->fotos); ?>
  <tr>
    <td>
      <table cellspacing="0" cellpadding="25" border="0" width="100%">
          <tr>
            <th width="40%" align="center" height="90">
              <?php if ($academia_id != 601) { ?>
                <?= $logo_academia ?
                  $this->Html->image(WEBROOT_URL.'/img/academias/'.$logo_academia,
                    ['style' => 'height: 80px', 'alt' => 'academia logo'])
                  :
                  '<p alt="'.$academia.'">'.$academia.'</p>';
                ?>  
              <?php } ?>
            </th>
            <th width="20%" align="center" height="90"></th>
            <th width="40%" align="center" height="90">
              <?= $this->Html->image(WEBROOT_URL.'/img/logo_master.png',
                    ['style' => 'max-height: 80px'])
                  ?>
            </th>
          </tr>
      </table>
      <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tbody>
          <tr>
            <td colspan="6" width="100%" align="center" height="100" >
              <?= $this->Html->image(WEBROOT_URL.'/img/produto_indicado.jpg',
                    ['alt' => 'produto indicado'])
                  ?>
            </td>
          </tr>
          <tr>
            <td colspan="6" width="100%" align="center">
              <br />
                <p style="font-family: Helvetica; font-size: 23px">Olá =)</p></span>
                <br>
                <?= $quem_indicou ?
                  '<p style="font-family: Helvetica;">'.$quem_indicou.' viu este incrível produto e lembrou de você!</p>'
                  :
                  '<p style="font-family: Helvetica;">Alguém viu este incrível produto e lembrou de você</p>';
                  ?>
                <br>
                <p style="font-family: Helvetica;">Confira aqui o produto que lhe foi indicado</p>
                <br>
                <br>
            </td>
          <tr>
            <td colspan="6" width="100%" align="center">
               <p><?= $produto->produto_base->name ?></p>
               <?= $this->Html->link(
                $this->Html->image($fotos[0], 
                ['id'    => 'foto-produto',
                'val'   => $produto->slug,
                'style' => 'height: 150px',
                'alt'   => $produto->produto_base->name.' '.$produto->propriedade,
                'escape' => false]),
                $url,
                ['escape' => false])
                ?>
               <br />
            </td>
          </tr>
          <tr>
            <td colspan="6" width="100%" align="center">
                <hr style="width: 80%" />
                <p style="font-family: Helvetica;">Em caso de qualquer dúvida, fique à vontade para responder esse email ou nos contatar no <a href="mailto:<?= $email_contato  ?>"><?= $email_contato ?></a>.</p>
            </td>
          </tr>
          <tr>
            <td colspan="6" height="15px"></td>
          </tr>
          <tr>
            <td colspan="6" align="center">
              <br />
              <hr style="width: 5%" />
              <br />
              <p style="font-family: Helvetica;">Powered by logfitness &copy; <?= date('Y') ?></p>
              <br />
            </td>
          </tr>
          <tr>
            <td colspan="6" height="15px"></td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
</table>