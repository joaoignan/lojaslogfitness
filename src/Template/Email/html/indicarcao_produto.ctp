<table cellspacing="0" cellpadding="0" border="1" width="650" bordercolor="lightgray">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="5" border="0" width="100%">
        <thead>
          <tr>
            <th width="50%" align="left" height="70">
              <?= $logo_academia ?
                  $this->Html->image(WEBROOT_URL.'img/academias/'.$logo_academia,
                    ['style' => 'height: 50px', 'alt' => 'logfitness logo'])
                  :
                  '<p alt="'.$academia.'">'.$academia.'</p>';
                  ?>  
                </th>
            <th width="50%" align="right" height="70">
              <?= $this->Html->image(WEBROOT_URL.'img/logfitness-email.png',
                    ['alt' => 'logfitness logo'])
                  ?>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="2" width="100%" align="center" bgcolor="#5087C7" height="100" >
              <p style="font-size: 37px; color: white; font-family: Helvetica">Agora você faz parte...</p>
            </td>
          </tr>
          <tr>
            <td colspan="2" width="100%" align="center">
              <br />
              <p style="font-family: Helvetica; font-size: 27px">Olá!</p>
              <br>
              <p style="font-family: Helvetica;">Você recebeu uma indicação de produto.</p>
              <br>
              <br>
              <p style="font-family: Helvetica;">Veja
                <?= $this->Html->link(
                'aqui',
                WEBROOT_URL.'minha-conta/indicacao-de-produtos/'
                )?>
                  
              </p>
              <br>
              <br>
              <hr style="width: 80%" />
              <p style="font-family: Helvetica;">Em caso de qualquer dúvida, fique à vontade para responder esse email ou nos contatar no <a href="mailto:sos@logfitness.com.br">sos@logfitness.com.br</a>.</p>
              <br />
              <br />
            </td>
          </tr>
          <tr>
            <td colspan="2" width="100%" align="center">
              <?= $this->Html->link(
                $this->Html->image(
                WEBROOT_URL.'img/new-spotify-email.png',
                ['alt'   => 'Spotity',
                'title' => 'Ouça nossas playlists',
                'escape' => false]),
                'https://open.spotify.com/user/logfitness',
                ['escape' => false])?>

              <?= $this->Html->link(
                $this->Html->image(
                  WEBROOT_URL.'img/new-facebook-email.png',
                  ['alt'   => 'Facebook',
                  'title' => 'Curta nossa página',
                  'escape' => false]),
                  'https://www.facebook.com/logfitness.com.br',
                  ['escape' => false])?>

                <?= $this->Html->link(
                    $this->Html->image(
                    WEBROOT_URL.'img/new-insta-email.png',
                    ['alt'   => 'Instagram',
                    'title' => 'Acompanhe nossos momentos',
                    'escape' => false]),
                    'https://www.instagram.com/logfitness.com.br/',
                    ['escape' => false])?>

                <?= $this->Html->link(
                    $this->Html->image(
                    WEBROOT_URL.'img/new-new-twitter-email.png',
                    ['alt'   => 'Twitter',
                    'title' => 'Siga nosso Twitter',
                    'escape' => false]),
                    'https://twitter.com/LOGFITNESS',
                    ['escape' => false])?>
                  <hr style="width: 5%" />
                  <br />
                  <p style="font-family: Helvetica;">LogFitness &copy; <?= date('Y') ?></p>
                  <br />
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
</table>