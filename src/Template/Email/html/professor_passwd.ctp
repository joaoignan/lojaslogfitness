<style>
    body{
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
</style>

<h2>Olá <?= $name; ?>!</h2>
<br>
<p>
    Você esqueceu a senha de acesso e solicitou a recuperação da mesma.
    <br>
    <br>
    <br>
    Clique no link a seguir para redefinir: <?= $this->Html->link(
        WEBROOT_URL.'time/recover_password/'.$email.'/'.$recover_key,
        WEBROOT_URL.'time/recover_password/'.$email.'/'.$recover_key
    )?>
    <br>
    <br>
    <br>
    O processo é simples e só vai levar um minuto.
</p>

<br>
<br>
<p>
    Empório Natural - &copy; <?= date('Y') ?>
</p>