<table cellspacing="0" cellpadding="0" border="1" width="600" bordercolor="#eeeeee">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="25" border="0" width="100%">
          <tr>
            <th width="40%" align="center" height="90">
              <?php if ($academia_master != 1 ) { ?>
                <?= $logo_academia ?
                  $this->Html->image(WEBROOT_URL.'/img/academias/'.$logo_academia,
                    ['style' => 'height: 60px', 'alt' => 'academia logo'])
                  :
                  '<p alt="'.$academia.'">'.$academia.'</p>';
                  ?>  
               <?php }?>
            </th>
            <th width="20%" align="center" height="90"></th>
            <th width="40%" align="center" height="90">
              <?= $this->Html->image(WEBROOT_URL.'/img/emporio_natural.png',
                    ['style' => 'height: 60px', 'alt' => 'Empório Natural'])
                  ?>
            </th>
          </tr>
      </table>
      <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tbody>
          <tr>
            <td colspan="6" width="100%" align="center" height="100" >
              <?= $this->Html->image(WEBROOT_URL.'/img/titulo-indicar-professor-emporio.jpg',
                    ['alt' => 'agora você faz parte...'])
                  ?>
            </td>
          </tr>
          <tr>
            <td colspan="6" width="100%" align="center">
              <br />
                <p style="font-family: Helvetica; font-size: 23px">Olá, <span><?= $name; ?>!</p></span>
                <br>
                <p style="font-family: Helvetica;">Agora você está no time da <?= $academia ?>, parabéns!</p>
                <br>
                <p style="font-family: Helvetica;">Acesse agora mesmo para conhecer a <?= $this->Html->link(
                    'sua loja',
                    WEBROOT_URL.$url_academia)?>.</p>
                <p style="font-family: Helvetica;">Você também pode acessar o seu <?= $this->Html->link(
                  'painel administrativo',
                   WEBROOT_URL.'/time/acesso'
                )?>
                </p>
                <br>
                <hr style="width: 80%" />
                <p style="font-family: Helvetica; padding-left: 20px; padding-right: 20px;">Em caso de qualquer dúvida, fique à vontade para responder esse email ou nos contatar no <a href="mailto:contato@emporionaturalriopreto.com.br">contato@emporionaturalriopreto.com.br</a>.</p>
                  <br />
            </td>
          </tr>
          <tr>
            <td colspan="6" height="15px"></td>
          </tr>
          <tr>
            <td align="center" width="20%">
            <td align="center" width="15%">
              
            </td>
            <td align="center" width="15%">
              <?= $this->Html->link(
                $this->Html->image(
                  WEBROOT_URL.'img/facebook-email.png',
                  ['alt'   => 'Facebook',
                  'title' => 'Curta nossa página',
                  'escape' => false]),
                  'https://www.facebook.com/empnatural/',
                  ['escape' => false])?>
            </td>
            <td align="center" width="15%">
                <?= $this->Html->link(
                    $this->Html->image(
                    WEBROOT_URL.'img/instagram-email.png',
                    ['alt'   => 'Instagram',
                    'title' => 'Acompanhe nossos momentos',
                    'escape' => false]),
                    'https://www.instagram.com/emporionatural/',
                    ['escape' => false])?>
            </td>
            <td align="center" width="15%">
                
            </td>
            <td align="center" width="20%">
          </tr>
          <tr>
            <td colspan="6" align="center">
              <br />
              <hr style="width: 5%" />
              <br />
              <p style="font-family: Helvetica;">Empório Natural &copy; <?= date('Y') ?></p>
              <br />
            </td>
          </tr>
          <tr>
            <td colspan="6" height="15px"></td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
</table>