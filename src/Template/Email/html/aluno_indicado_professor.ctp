<table cellspacing="0" cellpadding="0" border="1" width="600" bordercolor="#eeeeee">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="25" border="0" width="100%">
          <tr>
            <th width="40%" align="center" height="90">
              
            </th>
            <th width="20%" align="center" height="90"></th>
            <th width="40%" align="center" height="90">
              <?= $this->Html->image(WEBROOT_URL.'/img/new-logfitness-email.jpg',
                    ['style' => 'height: 30px', 'alt' => 'logfitness logo'])
                  ?>
            </th>
          </tr>
      </table>
      <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tbody>
          <tr>
            <td colspan="6" width="100%" align="center" height="100" >
              <?= $this->Html->image(WEBROOT_URL.'/img/titulo-indicar-professor.jpg',
                    ['alt' => 'produto indicado'])
                  ?>
            </td>
          </tr>
          <tr>
            <td colspan="6" width="100%" align="center">
              <br />
                <p style="font-family: Helvetica; font-size: 23px">Olá <?= $name ?></p></span>
                <br>
                <p style="font-family: Helvetica;">Pensando em você, o(a) professor(a) <?= $professor ?> está te convidando para participar da <strong>Logfitness</strong></p>
                <p style="font-family: Helvetica;">Na Log você encontra suplementos alimentares que irão te auxiliar a chegar no seu objetivo e ter os melhores resultados em seus treinos.</p>
                <br />
                <p style="font-family: Helvetica;">Acesse agora a <?= $this->Html->link('Logfitness', WEBROOT_URL) ?>, cadastre-se em sua academia e experimente uma experiência que você nunca sentiu!</p>
                <br />
                <br>
                <hr style="width: 80%" />
                <p style="font-family: Helvetica;">Em caso de qualquer dúvida, fique à vontade para responder esse email ou nos contatar no <a href="mailto:sos@logfitness.com.br">sos@logfitness.com.br</a>.</p>
                  <br />
            </td>
          </tr>
          <tr>
            <td colspan="6" height="15px"></td>
          </tr>
          <tr>
            <td align="center" width="20%">
            <td align="center" width="15%">
              <?= $this->Html->link(
                $this->Html->image(
                WEBROOT_URL.'img/new-spotify-email.png',
                ['alt'   => 'Spotity',
                'title' => 'Ouça nossas playlists',
                'escape' => false]),
                'https://open.spotify.com/user/logfitness',
                ['escape' => false])?>
            </td>
            <td align="center" width="15%">
              <?= $this->Html->link(
                $this->Html->image(
                  WEBROOT_URL.'img/new-facebook-email.png',
                  ['alt'   => 'Facebook',
                  'title' => 'Curta nossa página',
                  'escape' => false]),
                  'https://www.facebook.com/logfitness.com.br',
                  ['escape' => false])?>
            </td>
            <td align="center" width="15%">
                <?= $this->Html->link(
                    $this->Html->image(
                    WEBROOT_URL.'img/new-insta-email.png',
                    ['alt'   => 'Instagram',
                    'title' => 'Acompanhe nossos momentos',
                    'escape' => false]),
                    'https://www.instagram.com/logfitness.com.br/',
                    ['escape' => false])?>
            </td>
            <td align="center" width="15%">
                <?= $this->Html->link(
                    $this->Html->image(
                    WEBROOT_URL.'img/new-twitter-email.png',
                    ['alt'   => 'Twitter',
                    'title' => 'Siga nosso Twitter',
                    'escape' => false]),
                    'https://twitter.com/LOGFITNESS',
                    ['escape' => false])?>
            </td>
            <td align="center" width="20%">
          </tr>
          <tr>
            <td colspan="6" align="center">
              <br />
              <hr style="width: 5%" />
              <br />
              <p style="font-family: Helvetica;">LogFitness &copy; <?= date('Y') ?></p>
              <br />
            </td>
          </tr>
          <tr>
            <td colspan="6" height="15px"></td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
</table>