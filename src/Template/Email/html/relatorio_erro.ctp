<style>
    body{
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
</style>

<h2>Olá!</h2>
<br>
<p>
    Aqui está um relatório de erros.
    <br>
    <br>
    <br>
    O link que gerou o erro é: <?= $link ?>
    <br>
    <br>
    <br>
</p>

<br>
<br>
<p>
    LogFitness - &copy; <?= date('Y') ?>
</p>