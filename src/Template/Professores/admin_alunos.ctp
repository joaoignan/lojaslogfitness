<style>
  @media all and (max-width: 450px){
    .no-mobile{
      display: none;
    }
    .botao{
      margin: 5px 0 5px 0;
    }
  }
  @media all and (max-width: 1023px){
    .no-small{
      display: none;
    }
  }
</style>

<section class="content">
    <div class="col-xs-12">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title"><?= __('Alunos') ?></h3>
        </div>
                          
        <div class="box-body">
          <table class="table table-hover">
            <tr>
              <th class="no-small">ID</th>
              <th>Nome</th>
              <th class="no-mobile">Telefone</th>
              <th class="no-small">E-mail</th>
              <th></th>
            </tr>
            <?php foreach ($alunos as $cliente): ?>
            <tr>
                <td class="no-small"><?= $this->Number->format($cliente->id) ?></td>
                <td><?= h($cliente->name) ?></td>
                <td class="no-mobile"><?= h($cliente->telephone) ?></td>
                <td class="no-small"><?= h($cliente->email) ?></td>
                <td class="actions text-center">
                  <?= $this->Html->link('<i class="fa fa-search" aria-hidden="true"></i> ',
                      ['controller' => 'Professores', 'action' => 'admin_aluno_detalhes', $cliente->id],
                      ['escape' => false, 'class' => 'btn btn-info btn-sm']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
          </table>
        
        <!-- /.box-body -->
          <div class="paginator" style="margin-left: 15px;">
          <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
          </ul>
          <p style="margin-left: 15px;"><?= $this->Paginator->counter() ?></p>
          </div>
        </div>
      <!-- /.box -->
      </div>

    
</section>
