<style>
    @media all and (max-width: 450px){
    .no-mobile{
      display: none;
    }
    .font-header{
        font-size: 12px
    }
    .font-body{
        font-size: 11px
    }
  }
  @media all and (max-width: 1023px){
    .no-small{
      display: none;
    }
  }
</style>
<section class="content">
    <div class="col-xs-12">
        <div class="box box-info">
            <h3 class="box-header">Minhas comissões</h3>
            <div class="box-body">
                <div class="professorComissoes index table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th class="text-center font-header"><?= $this->Paginator->sort('id') ?></th>
                            <th class="text-center font-header"><?= $this->Paginator->sort('ano') ?></th>
                            <th class="text-center font-header"><?= $this->Paginator->sort('mes') ?></th>
                            <th class="text-left font-header"><?= $this->Paginator->sort('vendas') ?></th>
                            <th class="text-left font-header"><?= $this->Paginator->sort('comissao') ?></th>
                            <th class="text-left no-small font-header"><?= $this->Paginator->sort('meta') ?></th>
                            <th class="text-center font-header"><?= $this->Paginator->sort('aceita') ?></th>
                            <th class="text-center font-header"><?= $this->Paginator->sort('paga') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($professorComissoes as $professorComisso): ?>
                            <tr>
                                <td class="text-center font-body"><?= $this->Number->format($professorComisso->id) ?></td>
                                <td class="text-center font-body"><?= $professorComisso->ano ?></td>
                                <td class="text-center font-body"><?= $professorComisso->mes ?></td>
                                <td class="text-left font-body">R$ <?= number_format($professorComisso->vendas, 2, ',', '.') ?></td>
                                <td class="text-left font-body">R$ <?= number_format($professorComisso->comissao, 2, ',', '.') ?></td>
                                <td class="no-small text-left font-body">R$ <?= number_format($professorComisso->meta, 2, ',', '.') ?></td>
                                
                                <?php if($professorComisso->aceita == 2) { ?>
                                    <?php if($professor->conta != null) { ?>
                                        <td class="text-center font-body">
                                            <?= $this->Html->link(
                                                '<i class="fa fa-calendar-o"></i> '.__('Aceitar'),
                                                '/professores/admin/comissao/'.$professorComisso->id,
                                                ['class' => 'btn btn-success', 'escape' => false]) ?>
                                        </td>
                                    <?php } else { ?>
                                        <td class="text-center font-body">
                                            <?= $this->Html->link(
                                                '<i class="fa fa-calendar-o"></i> '.__('Aceitar'),
                                                '/professores/admin/dados-bancarios/1',
                                                ['class' => 'btn btn-success', 'escape' => false]) ?>
                                        </td>
                                    <?php } ?>
                                <?php } else { ?>
                                    <td class="text-center font-body">Sim</td>
                                <?php } ?>

                                <td class="text-center font-body"><?= $professorComisso->paga == 1 ? 'Sim' : 'Não' ?></td>
                            </tr>

                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                        </ul>
                        <p><?= $this->Paginator->counter() ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>