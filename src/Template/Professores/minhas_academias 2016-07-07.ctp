<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);

?>

<div class="professores form">
    <fieldset>
        <legend><?= __('Minhas Academias') ?></legend>
        <div class="content-box">
            <h3 class="content-box-header bg-default">
                <?= __('Actions') ?>
                <div class="header-buttons-separator">
                    <a class="icon-separator toggle-button" href="#">
                        <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
                    </a>
                </div>
            </h3>
            <div class="content-box-wrapper hidee">
                <?= $this->Html->link(__('Listar minhas academias'), ['action' => 'listar_minhas_academias'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>
        <?= $this->Form->create($professor_academias, ['class' => 'form-horizontal '])?>
        <div class="form-group ">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('Nome da Academia') ?>
            </div>
            <div class=" col-sm-6">
                <?= $this->Form->input('academia_id', [
                    'div'           => false,
                    'label'         => false,
                    'options'       => $academias,
                    'class'         => 'form-control chosen-select',
                    'empty'         => 'Selecione sua academia'
                ])?>
            </div>
            <div class="form-group ">
                <?= $this->Form->button('<span>'.__('ADICIONAR ACADEMIA').'</span>'.
                    '<i class="glyph-icon icon-arrow-right"></i>',
                    ['class' => 'btn btn-alt btn-hover btn-default',
                        'escape' => false]); ?>
            </div>
        </div>
        <?= $this->Form->end()?>
    </fieldset>
</div>

