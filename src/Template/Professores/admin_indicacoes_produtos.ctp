<style>
/*dropdown pedidos */
.lista-mobile{
    display: none;
}
.pedidos-single {
    display: flex;
    align-items: center;
    margin-top: 10px;
    margin-bottom: 10px;
}
.pedidos-grade-indicacoes {
    max-height: 100px;
    max-width: 100px;
}
.pedidos-linhas {
    margin: 5px 0px;
    background: #fff;
    border: 2px solid #fff;
    cursor: pointer;
    display: flex;
    align-items: center;
    color: black;
}
.pedidos-linhas:hover {
    background-color: #eee;
    border-color: #eee;
}
.icones-pedidos {
    position: relative;
    width: 22%;
    display: flex;
    height: 70px;
    align-items: center;
    justify-content: center;
    float: left;
    flex-direction: column;
}
.seta-pedidos {
    width: 4%;
    float: left;
}
.icon-pedidos-box {
    height: 40px;
    width: 100%;
}
.icones-box {
    float: right;
    height: 70px;
    margin-top: 10px;
    margin-bottom: 10px;
    color: #5089cf;
    fill: #5089cf;
  }
 .icones-tamanho {
    height: 100%;
    font-size: 2.6em;
}
.borda-detalhes {
    padding-top: 10px;
}
.pedidos-titulo {
    display: none;
    float: left;
    width: 100%;
    padding-left: 15px;
    padding-right: 15px;
}
.separador {
    width: 100%;
    border: 1px solid #5087c7;
    margin-top: 10px;
    margin-bottom: 10px;
}
/* The Overlay (background) */
.overlay {
    height: 0%;
    width: 100%;
    position: fixed;
    z-index: 99999999;
    top: 0;
    left: 0;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0, 0.9);
    overflow-y: hidden;
    transition: 0.5s;
}

.overlay-content {
  position: relative;
  top: 5%;
  left: 10%;
  width: 80%;
  height: 90%;
  text-align: center;
  background-color: white;
  border-radius: 15px;
  padding: 30px;
  overflow: auto;
}

.overlay-content h2{
  font-family: nexa_boldregular;
  color: #5089cf;
}

.overlay-content h3{
  font-family: nexa_boldregular;
  color: #f5d734;
}

.overlay a {
    padding: 8px;
    text-decoration: none;
    font-size: 36px;
    color: #818181;
    display: block;
    transition: 0.3s;
}

.overlay a:hover, .overlay a:focus {
    color: #f1f1f1;
}

.overlay .closebtn {
    position: absolute;
    top: 20px;
    right: 45px;
    font-size: 60px;
    z-index: 9999
}

@media all and (max-width: 450px){
    .no-mobile{
      display: none;
    }
    .icon-pedidos-box {
      height: 40px;
      width: 100%;
      margin-top: 25px;
    }
  }
@media all and (max-width: 768px){
  .lista-desk-tablet{
    display: none;
  }
  .lista-mobile{
    display: block;
  }
  .fotos-mobile {
    height: 45px;
  }
  .icones-box {
    width: 100%;
  }
  .title-mobile{
    font-size: 15px;
    font-weight: 700;
  }
  .valores-mobile{
      font-size: 10px;
  }
  .codigo-mobile{
      padding-top: 10px;
  }
  .pedidos-titulo-mobile{
      display: none;
  }
  .separador-produto {
      width: 100%;
      border: 1px solid #f3d012;
      margin-top: 10px;
      margin-bottom: 10px;
  }
  .total-pedidos-mobile {
      float: right;
      font-size: 16px;
      font-weight: 700;
      color: #5089cf;
  }
  .icon-pedidos-box {
    height: 40px;
    width: 100%;
  }
  .seta-pedidos {
    width: 5%;
    float: left;
    margin-top: 20px;
  }
  .check-mobile{
    position: absolute;
    top: 0;
    right: 8px;
  }
}


@media all and (max-width: 1023px){
    .no-small{
      display: none;
    }
  }

@media all and (max-width: 1279px) {
  .pedidos-topo {
      font-size: 12px;
  }
  .info-pedidos{
      font-size: 11px;
  }
  .icones-pedidos{
    float: left;
    width: 20%;
  }
  .icones-pedidos p{
      font-size: 8px;
  }
}

@media screen and (max-height: 450px) {
  .overlay {overflow-y: auto;}
  .overlay a {font-size: 20px}
  .overlay .closebtn {
    font-size: 40px;
    top: 15px;
    right: 35px;
  }
  .btn-status-mobile{
    width: 100%;
  }
  .check-mobile{
    position: absolute;
    top: 0;
    right: 0px;
  }
}
</style>
<script type="text/javascript">
    $(document).ready(function() {
        var detalhes;
        var detalhesmobile;
        var medida_tela = $(window).width();

        if(medida_tela >= 768) {
            $('[href^="#detalhes"]').click(function(e) {
                e.preventDefault();
                detalhes = $(this).attr('data-target');
                $('.pedidos-titulo').not(detalhes).slideUp();
                $('.pedidos-linhas').not($(this).parent().parent()).removeClass('borda-pedidos');
                $(detalhes).slideToggle();

                if($(this).parent().parent().hasClass('borda-pedidos')) {
                    $(this).parent().parent().removeClass('borda-pedidos');
                } else {
                    $(this).parent().parent().addClass('borda-pedidos');
                }
            });
        } else {
            $('[href^="#detalhesmobile"]').click(function(e) {
                e.preventDefault();
                detalhesmobile = $(this).attr('data-target');
                $('.pedidos-titulo-mobile').not(detalhesmobile).slideUp();
                $(detalhesmobile).slideToggle();
            });
        }
    });
</script>


<section class="content">
  <div class="col-xs-12 text-left">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Mochilas Personalizadas</h3>
            </div>
            <div class="col-xs-6 col-sm-3 botao" style="margin-bottom: 10px">
              <?= $this->Html->link('<i class="glyph-icon icon-plus"></i>  '.__('Nova Indicação'),
                  '/professores/admin/aluno/indicar-produtos?indicar=1',
                  [
                    'class' => 'btn btn-success btn-sm',
                    'escape' => false
                  ])
              ?>
            </div>
            <?php if(!empty($produtos_sessao)) { ?>
            <div class="col-xs-6 col-sm-3 botao" style="margin-bottom: 10px">
              <?= $this->Html->link('<i class="glyph-icon icon-undo"></i>  '.__('Re-Indicação'),
                  '/professores/admin/aluno/indicar-produtos/',
                  [
                    'class' => 'btn btn-success btn-sm',
                    'escape' => false
                  ])
              ?>
            </div>
            <?php } ?>
            <div class="box-body">
              <div class="pedidos index">
                <div class="col-sm-12 lista-desk-tablet">
                  <div class="col-sm-12 pedidos-topo">
                    <div class="col-md-1 text-left"><?= $this->Paginator->sort('id') ?></div>
                    <div class="col-md-2 text-left"><?= $this->Paginator->sort('academia_id') ?></div>
                    <div class="col-md-2 text-left"><?= $this->Paginator->sort('cliente_id', 'Aluno') ?></div>
                    <div class="col-md-1 text-left"><?= $this->Paginator->sort('pedido_id') ?></div>
                    <div class="col-md-2 text-left"><?= $this->Paginator->sort('created', 'Enviada') ?></div>
                    <div class="col-md-2 text-left"><?= $this->Paginator->sort('modified', 'Aceita') ?></div>
                    <div class="col-md-2"></div>
                  </div>
                  <?php foreach ($pre_orders as $pre_order): ?>
                    <div class="col-sm-12 pedidos-linhas">
                      <div class="col-md-1 text-left"><?= $this->Number->format($pre_order->id) ?></div>
                      <div class="col-md-2 text-left"><?= h($pre_order->academia->shortname) ?></div>
                      <div class="col-md-2 text-left"><?= h($pre_order->cliente->name) ?></div>
                      <div class="col-md-1 text-left"><?= $pre_order->pedido_id >= 1 ? '<a href="http://teste.logfitness.com.br/professores/admin/pedidos">'.$pre_order->pedido_id.'</a>' : '-' ?>
                        </div>
                      <div class="col-md-2 text-left"><?= h($pre_order->created) ?></div>
                      <div class="col-md-2 text-left"><?php
                        $badge = '';
                        if(
                            $pre_order->pedido_id   >= 1 &&
                            $pre_order->modified    != $pre_order->created &&
                            date_format($pre_order->modified, 'Y-m-d') >= date('Y-m-d', strtotime('- 7 days'))
                        ){
                            $badge = '<span class="bs-badge badge-blue-alt">NOVO</span>';
                        }
                        echo $pre_order->modified != $pre_order->created ? h($pre_order->modified).' '.$badge : '-'?>
                        </div>
                      <div class="col-md-2 text-center"><?= $this->Html->link(
                        '<button type="button" class="btn bg-blue">Detalhes <i class="fa fa-search"></i></button>',
                        '#detalhes',
                        [
                            'escape'        => false,
                            'style'         => 'cursor: pointer;',
                            'data-pid'      => $pre_order->id,
                            'title'         => "Clique para visualizar detalhes desta mochila",
                            'data-target'   => "#pid-".$pre_order->id
                        ]
                        )?></div>
                    </div>
                    <div id="pid-<?= $pre_order->id ?>" role="dialog" class="pedidos-titulo borda-detalhes">
                      <div class="row bg-info col-xs-12">
                        <div class="col-xs-1 font-bold text-center">Cód.</div>
                        <div class="col-xs-2 font-bold text-center">Produto</div>
                        <div class="col-xs-7 font-bold text-left"></div>
                        <div class="col-xs-2 font-bold text-center">Quantidade</div>
                      </div>
                      <?php foreach ($pre_order->pre_order_items as $item): ?>
                      <?php $fotos = unserialize($item->produto->fotos); ?>
                        <div class="row pedidos-single col-xs-12">
                          <div class="col-xs-1 font-bold text-center"><?= $item->produto_id ?></div>
                          <div class="col-xs-2 font-bold text-center"><?= $this->Html->image(
                                'produtos/'.$fotos[0],
                                ['class' => 'pedidos-grade-indicacoes', 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]) ?></div>
                          <div class="col-xs-7 font-bold text-left"><?= $item->produto->produto_base->name ?></div>
                          <div class="col-xs-2 font-bold text-center"><?= $item->quantidade ?></div>
                        </div>
                      <?php endforeach; ?>
                      <hr class="separador">
                    </div>
                  <?php endforeach; ?>
                </div>
                <div class="lista-mobile">
                  <div class="pedidos-mobile">
                    <?php foreach ($pre_orders as $pre_order): ?>
                      <hr class="separador">
                      <div class="pedidos-linhas-mobile">
                        <div class="col-xs-6 text-center info-pedidos">
                          <p class="title-info-mobile"><strong>Código</strong></p>
                          <p><?= $this->Number->format($pre_order->id) ?></p>
                        </div>
                        <div class="col-xs-6 text-center info-pedidos">
                          <p class="title-info-mobile"><strong>Aluno</strong></p>
                          <p><?= h($pre_order->cliente->name) ?></p>
                        </div>
                        <div class="col-xs-6 text-center info-pedidos">
                          <p class="title-info-mobile"><strong>Enviada</strong></p>
                          <p><?= h($pre_order->created) ?></p>
                        </div>
                        <div class="col-xs-6 text-center info-pedidos">
                          <p class="title-info-mobile"><strong>Pedido</strong></p>
                          <p><?= $pre_order->pedido_id >= 1 ? $pre_order->pedido_id : '-' ?></p>
                        </div>
                        <div class="col-xs-12 text-center info-pedidos" style="float: left; margin-bottom: 10px;">
                          <?= $this->Html->link(
                                ' <button type="button" class="btn bg-blue">Detalhes <i class="fa fa-search"></i></button>',
                                '#detalhesmobile',
                                [
                                    'escape'        => false,
                                    'style'         => 'cursor: pointer;',
                                    'data-pid'      => $pre_order->id,
                                    'title'         => "Clique para visualizar detalhes deste pedido",
                                    'data-target'   => "#pidm-".$pre_order->id
                                ]
                            )
                            ?>
                        </div>
                      </div>
                      <div id="pidm-<?= $pre_order->id ?>" role="dialog" class="pedidos-titulo-mobile">
                        <?php foreach ($pre_order->pre_order_items as $item): ?>
                        <?php $fotos = unserialize($item->produto->fotos); ?>
                          <div class="col-xs-12" style="margin-bottom: 5px;">
                            <div class="col-xs-8 text-left">
                              <p class="title-mobile codigo-mobile">Cód.: <?= $item->produto_id ?></p>
                            </div>
                            <div class="col-xs-4 text-right">
                              <?= $this->Html->image(
                                        'produtos/'.$fotos[0],
                                        ['class' => 'fotos-mobile', 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]) ?>
                            </div>
                            <div class="col-xs-8 text-left">
                              <p class="title-mobile">Produto</p>
                              <p class="valores-mobile"><?= $this->Html->link($item->produto->produto_base->name.' '
                                                .$item->produto->produto_base->embalagem_conteudo.' '
                                                .$item->produto->propriedade,
                                                    '/produto/'.$item->produto->slug,
                                                    ['target' => '_blank', 'escape' => false]
                                            ); ?></p>
                            </div>
                            <div class="col-xs-4 text-left">
                              <p class="title-mobile">Qtd</p>
                              <p class="valores-mobile text-center"><?= $item->quantidade ?></p>
                            </div>
                            <hr class="separador-produto">
                          </div>
                        <?php endforeach; ?>
                          <div class="col-xs-6 text-center">
                            <p class="title-mobile">Academia</p>
                            <p class="valores-mobile"><?= h($pre_order->academia->shortname) ?></p>
                          </div>
                          <div class="col-xs-6 text-center">
                            <p class="title-mobile">Aceita</p>
                            <p class="valores-mobile"><?php
                              $badge = '';
                              if(
                                  $pre_order->pedido_id   >= 1 &&
                                  $pre_order->modified    != $pre_order->created &&
                                  date_format($pre_order->modified, 'Y-m-d') >= date('Y-m-d', strtotime('- 7 days'))
                              ){
                                  $badge = '<span class="bs-badge badge-blue-alt">NOVO</span>';
                              }
                              echo $pre_order->modified != $pre_order->created ? h($pre_order->modified).' '.$badge : '-'?></p>
                          </div>
                      </div>
                    <?php endforeach; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</section>