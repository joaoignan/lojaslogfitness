<?= $this->Html->css('charts/examples') ?>
<?= $this->Html->script('charts/jquery.flot') ?>
<?= $this->Html->script('charts/jquery.flot.categories') ?>


<style>
    .btn-filtro{
        margin-top: 15px;
    }
</style>


<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);

$anos = [];
for($i = 2016; date('Y') >= $i; $i++){
    $anos[$i] = $i;
}

$meses = [];
for($i = 1; $i <= 12; $i++){
    $meses[str_pad($i, 2, '0', STR_PAD_LEFT)] = str_pad($i, 2, '0', STR_PAD_LEFT);
}
?>

<section class="content">
    <div id="content" class="col-sm-12 col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-bar-chart-o"></i>
                <h3 class="box-title">Gerar Relatório</h3>
            </div>
            <div class="box-body">
                <div class="col-xs-12">
                    <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row']); ?>

                <div class="col-sm-6 col-md-2 control-label">
                    <?= $this->Form->label('mes', 'Mês') ?>
                </div>
                <div class="col-sm-6 col-md-4">
                    <?= $this->Form->input('mes', [
                        'options' => $meses,
                        'value' => $mes > 0 ? $mes : '',
                        'empty' => 'Mês',
                        'label' => false,
                        'class' => 'form-control chosen-select validate[required]'
                    ]) ?>
                </div>

                <div class="col-sm-6 col-md-2 control-label">
                    <?= $this->Form->label('ano') ?>
                </div>
                <div class="col-sm-6 col-md-4">
                    <?= $this->Form->input('ano', [
                        'options' => $anos,
                        'value' => $ano > 0 ? $ano : '',
                        'empty' => 'Ano',
                        'label' => false,
                        'class' => 'form-control chosen-select validate[required]'
                    ]) ?>
                </div>

                 <div class="col-sm-12 text-center btn-filtro">
                    <?= $this->Form->button('<i class="glyph-icon icon-search"></i> <span>Filtrar</span>',
                        ['class' => 'btn btn-alt bg-blue',
                            'escape' => false
                        ]); ?>
                </div>
                <?= $this->Form->end(); ?>
                </div>
            </div>  
        </div>
    </div>
    <div id="content" class="col-sm-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-bar-chart-o"></i>
                <h3 class="box-title">Gráfico de Vendas Academia x Professor</h3>
            </div>
            <div class="box-body">
                <div class="col-xs-12 no-padding">
                    <div class="demo-container">
                        <div id="placeholder" class="demo-placeholder"></div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</section>

<!-- If + script da academia -->
<div class="col-sm-12" style="padding-top: 30px">
    <?php if(!$select_date) {


        if(count($professor_acad_relatorio) != 0) {
            $qtd_acad_relatorio = count($academias_professor_id);
            $id_acad[] = '';
            $valor_acad[] = '';

            foreach ($professor_acad_relatorio as $apr) {
                for($i = 0; $i < $qtd_acad_relatorio; $i++) {
                    if($apr->academia_id == $academias_professor_id[$i]) {
                        $id_acad[$i] = $apr->academia_id;
                        $valor_acad[$i] = $valor_acad[$i] + $apr->valor;
                    } else {
                        $id_acad[$i] = $academias_professor_id[$i];
                        $valor_acad[$i] = $valor_acad[$i] + 0;
                    }
                }
            } 

            for($i = 0; $i < $qtd_acad_relatorio; $i++) {
                foreach($acad_relatorio_name as $arn) {
                    if($id_acad[$i] == $arn->id) {
                        if($valor_acad[$i] >= 1) {
                            $name = explode(' ', $arn->name);
                            $acad_vendeu[] = [
                                $name[0],
                                number_format($valor_acad[$i], 2, '.', '.')
                            ]; 
                        } else {
                            $name = explode(' ', $arn->name);
                            $acad_vendeu[] = [
                                $name[0],
                                number_format(0, 2, '.', '.')
                            ]; 
                        }
                    }
                }
            }
        } else {
            foreach($acad_relatorio_name as $arn) {
                $name = explode(' ', $arn->name);
                $acad_vendeu[] = [
                    $name[0],
                    number_format(0, 2, '.', '.')
                ]; 
            }
        }

    } ?>

    <script type="text/javascript">
        $(function() {
            var data = [ <?php foreach ($acad_vendeu as $av) { ?>["<?= $av[0] ?>", <?= $av[1] ?>], <?php } ?> ];
            $.plot("#placeholder", [ data ], {
                series: {
                    bars: {
                        show: true,
                        barWidth: 0.5,
                        align: "centered",
                        lineWidth: 0,
                        fill: true,
                        fillColor: "#5087C7"
                    }
                },
                xaxis: {
                    mode: "categories",
                    tickLength: 0
                },
                yaxis: {
                    min: 0,
                },
                grid: {
                    hoverable: true
                }
            });

            $("<div id='tooltip'></div>").css({
                position: "absolute",
                display: "none",
                border: "1px solid #fdd",
                padding: "2px",
                "background-color": "#fee",
                opacity: 0.80
            }).appendTo("body");

            $("#placeholder").bind("plothover", function (event, pos, item) {
                if (item) {
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                    $("#tooltip").html('R$'+y.replace('.', ','))
                        .css({top: item.pageY+5, left: item.pageX+5})
                        .fadeIn(200);
                } else {
                    $("#tooltip").hide();
                }
            });

            $("#footer").prepend("Flot " + $.plot.version + " &ndash; ");
        });
    </script>
</div>
  <!-- fim do if +script da academia -->