<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);

?>
<?= $this->Html->script('jquery.mask.min.js')?>
<script type="text/javascript">$("#telephone").mask("(00) 90000-0000");</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.states_academias').on('change', function(){
            loading.show(1);
            var state_id = $(this).val();
            $.get(WEBROOT_URL + '/cidades/' + state_id + '/1',
                function(data){
                    $('.city').html(data);
                    loading.hide(1);
                });
        });

        $('.cidade-academia').change(function(){
            loading.show(1);
            $.get(WEBROOT_URL + 'academias/busca-cidade/0/' + $(this).val(),
                function(data){
                    $('#select-academia').html(data);
                    loading.hide(1);
                });
        });
    });
</script>

<section class="content">
    <fieldset>
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Adicionar academia</h3>
                <span class="pull-right"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
                        '/professores/admin/listar-minhas-academias',
                        ['escape' =>  false,
                           'class'  =>  'pull-right']) ?></span>
            </div>
            <div class="box-body">
                <?= $this->Form->create($professor_academias, ['class' => 'form-horizontal '])?>

                <div class="form-group"  >
                    <div class="col-sm-4 col-md-2 control-label">
                        <?= $this->Form->label('Estado') ?>
                    </div>
                    <div class="col-sm-6"  >
                        <?= $this->Form->input('uf_id', [
                            'id'            => 'states_academias',
                            'div'           => false,
                            'label'         => false,
                            'options'       => $states,
                            'empty'         => 'Estado...',
                            'class'         => 'validate[required] states_academias form-control input-cadastro input-cadastro-width'
                        ]) ?>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-sm-4 col-md-2 control-label" >
                        <?= $this->Form->label('Cidade') ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $this->Form->input('city_id', [
                            'id'            => 'city',
                            'div'           => false,
                            'options'       => [],
                            'label'         => false,
                            'empty'         => 'Selecione uma cidade',
                            'class'         => 'validate[required] city form-control input-cadastro input-cadastro-width cidade-academia'
                        ]) ?>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-sm-4 col-md-2 control-label">
                        <?= $this->Form->label('Nome da Academia') ?>
                    </div>
                    <div class=" col-sm-6">
                        <?= $this->Form->input('academia_id', [
                            'id'            => 'select-academia',
                            'options'       => [],
                            'div'           => false,
                            'empty'         => 'Selecione uma academia...',
                            'label'         => false,
                            'class'         => 'form-control input-cadastro validate[required] input-cadastro-width'
                        ])?>
                    </div>
                </div>
                <div class="form-group text-center">
                    <?= $this->Form->button( '<i class="fa fa-check-square-o"></i> '.
                       '<span>'.__('ADICIONAR ACADEMIA ').'</span>',
                        ['class' => 'btn btn-alt btn-hoverr btn-success',
                            'escape' => false]); ?>
                </div>

                <?= $this->Form->end()?>
            </div>
        </div>
    </fieldset>

    
        
    <?= $this->Form->create(null, [
        'id'        => 'form-indicar-academia',
        'role'      => 'form',
        'default'   => false,
        'type'      => 'file',
        'class' => 'form-horizontal bordered-row'
    ]) ?>

    <fieldset>
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Convidar academia</h3>
                <p class="help-block">Não encontrou sua academia? Convide ela para fazer parte da Log!</p>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-4 col-md-2 control-label">
                        <?= $this->Form->label('Nome') ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $this->Form->input('name', [
                            'label' => false,
                            'div'   => false,
                            'placeholder' => 'Nome completo da academia',
                            'class' => 'form-control validate[required]'
                        ]) ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-4 col-md-2 control-label">
                        <?= $this->Form->label('Telefone') ?>
                    </div>
                    <div class="col-sm-4 col-md-2">
                        <?= $this->Form->input('telephone', [
                            'label' => false,
                            'div'   => false,
                            'placeholder' => '(00) 00000-0000',
                            'class' => 'validate[required] form-control phone'
                        ]) ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-4 col-md-2 control-label">
                        <?= $this->Form->label('Responsável') ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $this->Form->input('responsavel', [
                            'div'   => false,
                            'label' => false,
                            'placeholder' => 'Nome do responsável da academia',
                            'class' => 'validate[required] form-control'
                        ]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4 col-md-2 control-label">
                        <?= $this->Form->label('Estado') ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $this->Form->input('uf_id', [
                            'id'            => 'states',
                            'div'           => false,
                            'label'         => false,
                            'options'       => $states,
                            'empty'         => 'Selecione um estado',
                            'placeholder'   => 'Estado',
                            'class'         => 'validate[required] states_academias form-control margin-bottom-30'
                        ]) ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-4 col-md-2 control-label">
                        <?= $this->Form->label('Cidade') ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $this->Form->input('city_id', [
                            'id'            => 'city_convite',
                            'div'           => false,
                            'label'         => false,
                            'empty'         => 'Selecione uma cidade',
                            'placeholder'   => 'Cidade',
                            'class'         => 'validate[required] city form-control margin-bottom-30'
                        ]) ?>
                    </div>
                </div>
                <div class="form-group text-center">
                    <?= $this->Form->button('<span>'.__('ENVIAR CONVITE PARA ACADEMIA ').'</span><i class="glyph-icon icon-arrow-right"></i>',
                        ['class' => 'btn btn-alt btn-hoverrr btn-success',
                            'escape' => false]); ?>
                </div>
            </div>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</section>

