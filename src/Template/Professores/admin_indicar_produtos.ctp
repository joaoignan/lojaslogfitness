<style>
    .btn:hover{
        color: #376092;
    }
    .checkout h2{
      color: #5089cf;
      font-weight: 700;
    }
    .filtros{
      height: 150px;
    }
    .btn-email{
        margin-bottom: 15px;
    }
    .campos-indicar-email{
        height: 110px;
    }
    .div-fechar{
        height: 35px;
    }
    .mochila{
        display: none;
    }
    .titulo-pagamento{
        background-color:#5089cf ;
    }
    .pedidos-topo {
        background-color: #5089cf;
        color: white;
        text-transform: uppercase;
        font-weight: 700;
    }
    .dados-total{
        float: right;
        width: 300px;
    }
    .lista-produtos{
        overflow: auto;
        min-height: 115px;
        max-height:5px;
        width: 100%;
        border: solid 1px;
        border-color: #5089cf;
    }
    .overlay-pagamento {
        overflow: auto;
        display: none;
        position: fixed;
        top: 0!important;
        left: 0;
        width: 100%;
        height: 100%;
        padding: 50px 0;
        background: rgba(0, 0, 0, .9);
        color: #333;
        font-size: 19px;
        line-height: 30px;
        z-index: 9999;
    }
    .checkout {
      position: relative;
      height: 100%;
      width: 30%;
      background: white;
      border-radius: 15px;
      box-shadow: 0 10px 40px rgba(0, 0, 0, 0.1);
      left: 35%;
      overflow: auto;
    }
    #email-indicacao {
        display: none;
    }
    .fechar-overlay {
        cursor:pointer;
        color: #5087c7;
        position:absolute;
        top: 15px;
        right: 20px;
    }
    .text-amarelo{
        color: #f3d117!important;
    }
    .btn-indicacoes{
        height: 90px;
    }
    .box-close-mochila{
        display: none;
    }
    #page-content {
        padding-right: 0px;
    }
    .titulo-pagamento{
        background-color:#5089cf ;
    }
    .mochila-indicacao{
        margin-left: 230px;
    }
    .mochila-indicacao h4 {
        text-align: left;
        margin: 0 0 10px;
        color: white;
        font-family: Nexa;
        display: flex;
        align-items: center;
        justify-content: center;
        font-weight: 900;
        height: 45px;
    }
    .formas-indicacao{

    }
    .forma-indicacao{
        text-align: center;
        display: inline-flex;
    }
    .font-14 {
        font-size: 14px;
    }
    .dimensao-produto-carrinho {
        max-width: 60px;
        max-height: 60px;
    }
    .imagem-produto-carrinho {
        float: left;
        padding: 0px 0 0 0;
        margin-right: 4px;
        text-align: center;
    }
    .pedidos-topo {
        background-color: #5089cf;
        color: white;
        text-transform: uppercase;
        font-weight: 700;
        height: 20px;
    }
    .titulo-produtos-informacoes{
        display: none;
    }
    .lista-produtos{
            overflow: auto;
            min-height: 90px;
            max-height: 160px;
            width: 100%;
            border: solid 1px;
            border-color: #5089cf;
        }
    .linha-produto{
        margin-top: 4px;
    }
    .btn-cupom {
        border: 2px solid;
        padding-top: 2px;
        border-color: #5087c7;
        background-color: white;
        font-family: Nexa;
        color: #5087c7;
    }
    @media all and (max-width: 1024px){
        .filtros {
            height: 120px;
        }
    }
    @media all and (max-width: 768px) {
        .lista-produtos{
            overflow: auto;
            min-height: 130px;
            max-height: 225px;
            width: 100%;
            border: solid 1px;
            border-color: #5089cf;
            box-shadow: inset 0px 0px 6px 0px rgba(0,0,0,0.75);
        }
        .mochila-indicacao{
            margin-left: 0px;
        }
        .titulo-pagamento{
            margin-top: 50px;
        }
        .checkout {
            margin-left: 5%;
            position: relative;
            width: 90%;
            left: 0;
            background: white;
            border-radius: 15px;
            box-shadow: 0 10px 40px rgba(0, 0, 0, 0.1);
            overflow: auto;
        }
        .filtros {
            height: 110px;
        }
    }
    @media all and (max-width: 430px) {
        .pedidos-topo{
            display: none;
        }
        .codigo-mobile{
            display: none;
        }
        .fabricante-mobile{
            display: none;
        }
        .titulo-produtos-informacoes{
            display: block;
        }
        .forma-indicacao button {
            font-size: .9em;
        }
    }
    @media all and (max-width: 1100px) {
        .forma-indicacao button {
            font-size: 1.1em;
            white-space: normal;
        }
    }
    @media all and (max-width: 550px) {
        .forma-indicacao button {
            width: 100%;
        }
        .btn-indicacoes{
            width: 100%;
        }
        .txt-produto{
            height: 70px;
        }
        .separador-produto {
            width: 100%;
            border: 1px solid #f3d012;
            margin-top: 10px;
            margin-bottom: 1px;
        }
        .lista-produtos{
            height: 170px;
        }
        .imagem-produto-carrinho{
            padding-top: 10px;
        }
        .multiselect {
            width: 100% !important;
        }
        .btn-group{
            margin-left: -55px;
        }
    }
    @media all and (min-width: 1400px){
        .checkout {
          height: 65%;
          top: 12%
        }
    }

</style>

<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<?= $this->Flash->render() ?>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#pag_boleto, #pag_cc').click(function() {
                    $('.overlay-pagamento').fadeIn();
                    $('body').css('overflow', 'hidden');
                });

                $('.fechar-overlay').click(function() {
                    $('.overlay-pagamento').fadeOut();
                    $('body').css('overflow', 'auto');
                    $('#email-indicacao, #aluno-indicacao').fadeOut();
                });

                $('#pag_cc').click(function() {
                    $('#aluno-indicacao').fadeOut();
                    $('#email-indicacao').fadeIn();
                });

                $('#pag_boleto').click(function() {
                    $('#email-indicacao').fadeOut();
                    $('#aluno-indicacao').fadeIn();
                });
            });
        </script>

        <div class="overlay-pagamento">
            <div class="checkout col-xs-12" id="email-indicacao">
                <div class="col-sm-12 div-fechar text-right">
                    <i class="fa fa-close fechar-overlay"></i>
                </div>
                <div class="col-xs-12">
                    <?= $this->Form->create(null, ['class' => 'form-horizontal '])?>
                    <div class="col-xs-12 campos-indicar-email">
                        <?= $this->Form->input('name', [
                                'id'            => 'indicado_name',
                                'div'           => false,
                                'label'         => 'Nome',
                                'placeholder'   => 'Nome do aluno',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                    </div>
                    <div class="col-xs-12 campos-indicar-email">
                        <?= $this->Form->input('email', [
                            'id'            => 'indicado_email',
                            'div'           => false,
                            'label'         => 'E-mail',
                            'placeholder'   => 'E-mail do aluno',
                            'class'         => 'validate[required] form-control',
                        ]) ?>
                    </div>
                    <div class="col-xs-12 campos-indicar-email">
                        <?= $this->Form->input('telephone', [
                            'id'            => 'indicado_fone',
                            'div'           => false,
                            'label'         => 'Telefone',
                            'placeholder'   => 'Telefone do aluno',
                            'class'         => 'validate[] form-control phone-mask',
                        ]) ?>
                    </div>
                    <div class="col-xs-12 campos-indicar-email">
                        <?= $this->Form->input('academia_id', [
                            'id'            => 'academias-professor',
                            'data-pid'      => $Professor->id,
                            'options'       => $academias_list,
                            'div'           => false,
                            'empty'         => 'Clique aqui',
                            'label'         => 'Selecione a academia',
                            'class'         => 'form-control validate[required]'
                        ])?>
                    </div>
                    <div class="col-xs-12 text-center btn-email">
                    <?= $this->Form->button('<span> '.__('ENVIAR INDICAÇÃO').'</span> <i class="glyph-icon icon-arrow-right"></i>'.
                        '',
                        [
                        'class'     => 'btn btn-success',
                        'escape'    => false
                        ]);
                    ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>


            <div class="checkout" id="aluno-indicacao">
                <div class="col-sm-12 div-fechar text-right">
                    <i class="fa fa-close fechar-overlay"></i>
                </div>
                <div class="col-xs-12 col-sm-offset-1 col-sm-10 text-center">
                  <h2>Selecione um aluno</h2>
                  <br />
                  <?= $this->Form->create(null, ['class' => 'form-horizontal '])?>
                  <div class="col-xs-12 filtros">
                      <?= $this->Form->input('academia_id', [
                          'id'            => 'academias-professor',
                          'data-pid'      => $Professor->id,
                          'options'       => $academias_list,
                          'div'           => false,
                          'empty'         => 'Filtrar alunos por academia...',
                          'label'         => 'Escolha a Academia',
                          'class'         => 'form-control validate[required]'
                      ])?>
                  </div>
                  <div class="col-xs-12 filtros">
                    <?= $this->Form->input('cliente_id', [
                        'id'            => 'alunos-professor',
                        'value'         => $aluno_id,
                        'options'       => $alunos,
                        'div'           => false,
                        'empty'         => 'Selecione um aluno...',
                        'label'         => 'Nome do Aluno',
                        'class'         => 'form-control validate[required]'
                    ])?>
                  </div>
                </div>
                <div class="text-center">
                    <?= $this->Form->button('<span> '.__('ENVIAR INDICAÇÃO').'</span> <i class="glyph-icon icon-arrow-right"></i>'.
                        '',
                        [
                        'class'     => 'btn btn-success',
                        'escape'    => false
                        ]);
                    ?>
                </div>
                <?= $this->Form->end()?>
            </div>
        </div>

        <!--MOCHILA-->
        <div class="mochila-indicacao">

            <div class="titulo-pagamento">
                <h4 class="mochila-indicacao">INDICAÇÃO FECHADA</h4>

            </div>
            <div class="formas-pagamento">
                <div class="col-xs-12">
                    <div class="col-xs-6 forma-pagamento">
                        <button class="btn btn-cupom col-xs-6 text-center btn-indicacoes" id="pag_boleto" style=" height: 90px;">
                            <i class="fa fa-user"></i>
                            <br class="clear">
                            Aluno
                        </button>
                    </div>

                    <div class="col-xs-6 forma-pagamento">
                        <button class="btn btn-cupom col-xs-6 btn-indicacoes" id="pag_cc" style=" height: 90px;">
                            <i class="fa fa-envelope-o"></i>
                            <br class="clear">
                            E-mail
                        </button>
                    </div>
                </div>
            </div>

    <br/>
        <!-- LISTA DE PRODUTOS -->
        <div class="lista-produtos">
                <div class="col-xs-12 pedidos-topo">
                    <div class="col-xs-6 text-center">
                        <span>PRODUTO</span>
                    </div>
                    <div class="col-sm-2 text-center">
                        <span>QUANTIDADE</span>
                    </div>
                    <div class="col-sm-2 text-center">
                        <span>VALOR UNITÁRIO</span>
                    </div>
                    <div class="col-sm-2 text-right">
                        <span>VALOR TOTAL</span>
                    </div>
                </div>
            <?php
            $subtotal = 0;
            foreach ($produtos_ind as $item):
                $fotos = unserialize($item->fotos);
                ?>
                <div class="col-xs-12 linha-produto">
                    <div class=" col-xs-4 col-sm-1">
                        <?= $this->Html->link(
                            $this->Html->image('produtos/' . $fotos[0],
                                ['class' => 'dimensao-produto-carrinho', 'alt' => $item->produto_base->name . ' ' . $item->propriedade]
                            ),
                            '/produto/' . $item->slug,
                            ['escape' => false]
                        ) ?>
                    </div>
                    <div class="col-xs-8 col-sm-5 text-center txt-produto">
                        <?= $this->Html->link(
                            '<h1 class="font-14 h1-carrinho"><span>'
                            . $item->produto_base->name . ' ' . $item->produto_base->embalagem_conteudo . ' ' . $item->propriedade .
                            '</span></h1>',
                            '/produto/' . $item->slug,
                            ['escape' => false]
                        ) ?>
                        <p class="font-10 codigo-mobile"><span><?= strtoupper($item->cod); ?></span></p>
                        <p class="font-10 fabricante-mobile"><span>Fabricante: <?= $item->produto_base->marca->name; ?></span></p>
                    </div>
                    <div class="col-xs-2 col-sm-2 margin-20 text-right">
                        <p class="titulo-produtos-informacoes"><b>Qtd.</b></p>
                        <p><?= $produtos_sessao[$item->id]; ?>
                    </div>
                    <div class="col-xs-5 col-sm-2 margin-20 text-right">
                        <p class="titulo-produtos-informacoes"><b>Val. unit.</b></p>
                        <?php if($item->preco_promo) { ?>
                            <p class="produtos-informacoes">R$ <?= number_format($item->preco_promo, 2, ',', '.'); ?></p>
                        <?php } else { ?>
                            <p class="produtos-informacoes">R$ <?= number_format($item->preco, 2, ',', '.'); ?></p>
                        <?php } ?>
                    </div>
                    <div class="col-xs-5 col-sm-2 margin-20 text-right">
                        <p class="titulo-produtos-informacoes"><b>Total</b></p>
                        <?php if($item->preco_promo) { ?>
                            <p class="produtos-informacoes" id="total-produto-<?= $item->id ?>" class="total-produto">R$ <?= number_format($total_produto = ($item->preco_promo * $produtos_sessao[$item->id]), 2, ',', '.'); ?>
                        <?php } else { ?>
                            <p class="produtos-informacoes" id="total-produto-<?= $item->id ?>" class="total-produto">R$ <?= number_format($total_produto = ($item->preco * $produtos_sessao[$item->id]), 2, ',', '.'); ?>
                        <?php } ?>

                        <?php $subtotal += $total_produto; ?>
                        </p>
                    </div>
                    <hr class="separador-produto">
                </div>
                <?php
            endforeach;
            ?>
            </div>
            <!-- FIM LISTA DE PRODUTOS -->
        <br/>

</div>
