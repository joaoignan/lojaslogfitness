<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>
<style>
.bootstrap-switch.bootstrap-switch-large{
    min-width:200px !important;
}
    @media all and (max-width: 450px){
    .no-mobile{
      display: none;
    }
  }
  @media all and (max-width: 1023px){
    .no-small{
      display: none;
    }
  }
</style>

<?php if(date('m') == 01) {
    $data_mes = 12;
    $data_ano = date('Y') - 1;
} else {
    $data_mes = (date('m') - 1);
    $data_ano = date('Y');
} ?>


<section class="content">
    <div class="col-xs-12">
        <div class="box box-info">
            <h3 class="box-header"><?= __('Relatório de Comissões até '.$data_mes.'/'.$data_ano) ?></h3>
            <h4 class="box-header">Confira se todas informações neste relatórios estão corretas. Em caso afirmativo, dê o aceite do mesmo, caso contrário envie uma contestação.</h4>
            <h4 class="box-header">Obs.: Somente após o aceite as comissões podem ser pagas.</h4>
            <hr>

            <div class="pedidos index">
                <div class="no-padding">
                    <?= $this->Form->create(null)?>
                    <h3 class="box-header">Aceite do Relatório</h3>
                    <br class="clear">
                    
                    <p>Tipo de conta: <?= $professor->tipo_conta ?></p>
                    <p>Banco: <?= $professor->banco ?></p>
                    <p>Agência: <?= $professor->agencia ?><?= $professor->agencia_dv != null ? '-'.$professor->agencia_dv : '' ?></p>
                    <p>Conta: <?= $professor->conta ?><?= $professor->conta_dv != null ? '-'.$professor->conta_dv : '' ?></p>
                    <?php if($professor->is_cpf == 0) { ?>
                        <p>CNPJ: <?= $professor->cnpj_bancario ?></p>
                        <p>Razão Social: <?= $professor->razao_bancario ?></p>
                    <?php } else { ?>
                        <p>Nome Titular Conta: <?= $professor->favorecido ?></p>
                        <p>CPF Titular Conta: <?= $professor->cpf_favorecido ?></p>
                    <?php } ?>
                    
                    <br class="clear">
                    <div class="form-group">
                        <div class="col-sm-4 col-md-2 control-label">
                            <?= $this->Form->label('aceite') ?>
                        </div>
                        <div class="col-sm-7">

                            <?= $this->Form->input('aceite', [
                                'label'     => false,
                                'div'       => false,
                                'class'     => 'form-control chosen-select validate[required]',
                                'options'   => [
                                    1 => 'Aceitar o Relatório',
                                    0 => 'Contestar o Relatório'
                                ],
                                'empty'     => 'Selecione uma opção...'

                            ]) ?>


                            <?php /*= $this->Form->checkbox('aceite', [
                                'label'     => false,
                                'div'       => false,
                                'checked'   => true,
                                'class'     => 'form-control validate[optional] input-switch',
                                'data-on-color' => 'info',
                                'data-on-text'  => 'Aceitar',
                                'data-off-text' => 'Contestar',
                                'data-off-color' => 'warning',
                                'data-size'     => 'large',
                            ])*/ ?>
                        </div>
                        <!--<p class="col-md-6">Confirme se este relatório está em conformidade com as vendas relacionadas a sua academia...</p>-->
                    </div>
                    <br class="clear">
                    <br class="clear">
                    <?= $this->Form->input('valor', [
                        'type'          => 'hidden',
                        'value'         => (float)$total,
                    ]) ?>

                    <?= $this->Form->input('meta', [
                        'type'          => 'hidden',
                        'value'         => (float)$meta,
                    ]) ?>

                    <?= $this->Form->input('comissao', [
                        'type'          => 'hidden',
                        'value'         => (float)$comissao,
                    ]) ?>

                    <div class="form-group">
                        <div class="col-sm-4 col-md-2 control-label">
                            <?= $this->Form->label('Observações') ?>
                        </div>
                        <div class="col-sm-7">
                            <div>
                                <?= $this->Form->input('obs', [
                                    'type'          => 'textarea',
                                    'placeholder'   => 'Coloque aqui informações sobre o ACEITE ou CONTESTAÇÃO deste relatório',
                                    'div'           => false,
                                    'label'         => false,
                                    'class'         => 'form-control validate[optional]',
                                ]) ?>
                            </div>
                        </div>
                    </div>

                    <br class="clear">
                    <br class="clear">

                    <div class="form-group text-center">
                        <div class="col-sm-12" style="margin: 15px 0;">
                            <div class="col-sm-12 col-md-8">
                                <?= $this->Form->button('<span>'.__('Enviar Relatório ').'</span>'.
                                    '<i class="glyph-icon icon-arrow-right"></i>',
                                    ['class' => 'btn btn-success', 'escape' => false]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive no-padding" style="width: 100%;">
                    <h3 class="box-header">Pedidos</h3>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center"><?= 'Data' ?></th>
                                <th class="text-center"><?= 'Status' ?></th>
                                <th class="text-center"><?= 'Valor' ?></th>
                                <th class="text-center"><?= 'Comissão' ?></th>
                                <th><?= 'Aluno' ?></th>
                                <th class="text-center"><?= 'Cód.' ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total      = 0;
                            $comissao   = 0;
                            foreach ($pedidos as $pedido):
                                $valor              = $pedido->valor;
                                $comissao_pedido    = getPercentCalc($valor, $config['academias_comissao']);
                                $comissao           += $comissao_pedido;
                                ?>
                                <tr class="pedido-linha"
                                    data-pid="<?= $pedido->id ?>"
                                    title=""
                                    data-toggle="modal"
                                    data-target="#pid-<?= $pedido->id ?>">
                                    <td class="text-center"><?= h($pedido->created->format('d/m/Y')) ?></td>
                                    <td class="text-center"><?= $pedido->pedido_status->name ?></td>
                                    <td class="text-center">R$ <?= number_format($valor, 2, ',', '.') ?></td>
                                    <td class="text-center">R$ <?= number_format($comissao_pedido, 2, ',','.') ?></td>
                                    <td><?= $pedido->cliente->name ?></td>
                                    <td class="text-center"><?= $this->Number->format($pedido->id) ?></td>
                                </tr>
                            <?php
                            $total += $valor;
                            endforeach;
                            ?>

                            <tr class="font-italic font-bold2">
                                <td class="text-center">
                                    <strong>TOTAL</strong>
                                </td>
                                <td>
                                    
                                </td>
                                <td class="text-center" >
                                    R$ <?= number_format($total, 2, ',', '.') ?>
                                </td>
                                <td  class="text-center">
                                    R$ <?= number_format($comissao, 2, ',','.') ?>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form">
                    <fieldset>
                        <!-- <p class="font-bold box-header">Meta: R$ <?= number_format($meta = $adm_academia->meta, 2, ',', '.') ?></p> -->
                        <p class="font-bold box-header">Vendas: R$ <?= number_format($total, 2, ',', '.') ?></p>
                        <p class="font-bold box-header">Comissão do Mês: R$ <?= $meta < $total ? number_format($comissao, 2, ',','.') : '0,00' ?></p>
                        <br>
                    </fieldset>
                    <?= $this->Form->end()?>
                </div>
            </div>
        </div>
    </div>
</section>

