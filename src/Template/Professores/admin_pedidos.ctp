<style>
@media screen and (max-height: 450px) {
  .btn-status-mobile{
    width: 100%;
  }
  .check-mobile{
    position: absolute;
    top: 0;
    right: 0px;
  }
}
/*dropdown pedidos */
.lista-mobile{
    display: none;
}
.pedidos-single {
    display: flex;
    align-items: center;
    margin-top: 10px;
    margin-bottom: 10px;
}
.pedidos-grade-indicacoes {
    max-height: 100px;
    max-width: 100px;
}
.pedidos-linhas {
    margin: 5px 0px;
    background: #fff;
    border: 2px solid #fff;
    cursor: pointer;
    display: flex;
    align-items: center;
    color: black;
}
.pedidos-linhas:hover {
    background-color: #eee;
    border-color: #eee;
}
.icones-pedidos {
    position: relative;
    width: 22%;
    display: flex;
    height: 70px;
    align-items: center;
    justify-content: center;
    float: left;
    flex-direction: column;
}
.seta-pedidos {
    width: 4%;
    float: left;
}
.icon-pedidos-box {
    height: 40px;
    width: 100%;
}
.icones-box {
    float: right;
    height: 80px;
    margin-top: 10px;
    margin-bottom: 10px;
    color: #5089cf;
    fill: #5089cf;
  }
 .icones-tamanho {
    height: 100%;
    font-size: 2.6em;
}
.borda-detalhes {
    padding-top: 10px;
}
.pedidos-titulo {
    display: none;
    float: left;
    width: 100%;
    padding-left: 15px;
    padding-right: 15px;
}
.separador {
    width: 100%;
    border: 1px solid #5087c7;
    margin-top: 10px;
    margin-bottom: 10px;
}
@media all and (max-width: 450px){
    .no-mobile{
      display: none;
    }
    .icon-pedidos-box {
      height: 40px;
      width: 100%;
      margin-top: 45px;
    }
  }
@media all and (max-width: 768px){
  .lista-desk-tablet{
    display: none;
  }
  .lista-mobile{
    display: block;
  }
  .fotos-mobile {
    height: 45px;
  }
  .icones-box {
    width: 100%;
  }
  .title-mobile{
    font-size: 15px;
    font-weight: 700;
  }
  .valores-mobile{
      font-size: 10px;
  }
  .codigo-mobile{
      padding-top: 10px;
  }
  .pedidos-titulo-mobile{
      display: none;
  }
  .separador-produto {
      width: 100%;
      border: 1px solid #f3d012;
      margin-top: 10px;
      margin-bottom: 10px;
  }
  .total-pedidos-mobile {
      float: right;
      font-size: 16px;
      font-weight: 700;
      color: #5089cf;
  }
  .icon-pedidos-box {
    height: 40px;
    width: 100%;
  }
  .seta-pedidos {
    width: 5%;
    float: left;
    margin-top: 20px;
  }
  .check-mobile{
    position: absolute;
    top: 0;
    right: 8px;
  }
}


@media all and (max-width: 1023px){
    .no-small{
      display: none;
    }
  }

@media all and (max-width: 1279px) {
  .pedidos-topo {
      font-size: 12px;
  }
  .info-pedidos{
      font-size: 11px;
  }
  .icones-pedidos{
    float: left;
    width: 20%;
  }
  .icones-pedidos p{
      font-size: 8px;
  }
}

@media screen and (max-height: 450px) {
  .btn-status-mobile{
    width: 100%;
  }
  .check-mobile{
    position: absolute;
    top: 0;
    right: 0px;
  }
}
</style>

<script>
  /* Open */
  function openStatus() {
    document.getElementById("entendaStatus").style.height = "100%";
  }
  /* Close */
  function closeStatus() {
    document.getElementById("entendaStatus").style.height = "0%";
  }
</script>
<script>
  /* Open */
  function openRetirada() {
    document.getElementById("retirada").style.height = "100%";
  }
  /* Close */
  function closeRetirada() {
    document.getElementById("retirada").style.height = "0%";
  }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var detalhes;
        var detalhesmobile;
        var medida_tela = $(window).width();

        if(medida_tela >= 768) {
            $('[href^="#detalhes"]').click(function(e) {
                e.preventDefault();
                detalhes = $(this).attr('data-target');
                $('.pedidos-titulo').not(detalhes).slideUp();
                $('.pedidos-linhas').not($(this).parent().parent()).removeClass('borda-pedidos');
                $(detalhes).slideToggle();

                if($(this).parent().parent().hasClass('borda-pedidos')) {
                    $(this).parent().parent().removeClass('borda-pedidos');
                } else {
                    $(this).parent().parent().addClass('borda-pedidos');
                }
            });
        } else {
            $('[href^="#detalhesmobile"]').click(function(e) {
                e.preventDefault();
                detalhesmobile = $(this).attr('data-target');
                $('.pedidos-titulo-mobile').not(detalhesmobile).slideUp();
                $(detalhesmobile).slideToggle();
            });
        }

        // desktop
        $('.close-desk-transportadora-btn').click(function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $("#desk-transportadora-"+id).height('0%');
        });
        $('.open-desk-transportadora-btn').click(function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $("#desk-transportadora-"+id).height('100%');
        });


        // mobile
        $('.close-transportadora-btn').click(function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $("#transportadora-"+id).height('0%');
        });
        $('.open-transportadora-btn').click(function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $("#transportadora-"+id).height('100%');
        });
    });
</script>



<section class="content">
    <!-- The overlay -->
    <div id="entendaStatus" class="overlay">
        <!-- Overlay content -->
        <div class="overlay-content">
        <!-- Button to close the overlay navigation -->
        <a href="javascript:void(0)" class="closebtn" onclick="closeStatus()">&times;</a>
            <h2>Entenda cada status dos seus pedidos!</h2>

            <br />
            <table class="table text-left">
                <tr>
                    <td>
                        <strong>Pendente</strong>
                    </td>
                    <td>
                        - O aluno só fechou a mochila mas não tentou pagar com cartão e nem emitiu boleto.
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Cancelado</strong>
                    </td>
                    <td>
                        - Pagamento Cartão. Ocorreu algum erro na transação do cartão ou a operadora não liberou. Verificar com o banco do aluno.<br />
                        - Pagamento boleto. Boleto é gerado e o aluno tem 3 dias para pagar. Passado esses dias o pedido é cancelado. Basta refazer o pedido e gerar um novo boleto =)
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Aguardando confirmação</strong>
                    </td>
                    <td>
                        - Pagamento Cartão. Se o aluno passou cartão é só aguardar a liberação da operadora<br />
                                  - Pagamento boleto. Esperamos o pagamento e a liberação do banco.
                    </td>
                </tr>
                <tr>
                  <td>
                    <strong>Pago</strong>
                  </td>
                  <td>
                    - Já foi aprovado.
                  </td>
                </tr>
                <tr>
                  <td>
                    <strong>Em separação</strong>
                  </td>
                  <td>
                    - Nossa equipe está no estoque preparando tudo para enviar o pedido.
                  </td>
                </tr>
                <tr>
                  <td>
                    <strong>Em transporte</strong>
                  </td>
                  <td>
                    - Já saiu do nosso estoque com destino à academia.
                  </td>
                </tr>
                <tr>
                  <td>
                    <strong>Aguardando retirada </strong>
                  </td>
                  <td>
                    - Seu pedido está em uma agência dos correios aguardando o responsável da academia realizar a retirada.
                  </td>
                </tr>
                <tr>
                  <td>
                    <strong>Entregue </strong>
                  </td>
                  <td>
                    - Já chegou na academia!
                  </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="col-xs-12 text-left">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Pedidos</h3>
            </div>
            <div class="col-md-offset-5 text-center">
                <span onclick="openStatus()"><button class="btn btn-xs btn-info btn-status-mobile">Entenda os Status</button></span>
            </div>
    <div class="box-body">
        <div class="pedidos index">
          <div class="col-sm-12 lista-desk-tablet">
            <div class="col-sm-12 pedidos-topo">
              <div class="col-sm-1 text-left"><?= $this->Paginator->sort('id') ?></div>
              <div class="col-sm-3 text-left"><?= $this->Paginator->sort('cliente_id', 'Aluno') ?></div>
              <div class="col-sm-2 text-left"><?= $this->Paginator->sort('created', 'Data') ?></div>
              <div class="col-sm-2 text-left no-small">Valor</div>
              <div class="col-sm-2 text-left">Status</div>
              <div class="col-sm-2 text-center"></div>
            </div>
            <?php foreach ($pedidos as $pedido): ?>
            <div class="col-sm-12 pedidos-linhas">
              <div class="col-sm-1 text-left info-pedidos"><?= $this->Number->format($pedido->id) ?></div>
              <div class="col-sm-3 text-left info-pedidos"><?= $pedido->cliente->name ?></div>
              <div class="col-sm-2 text-left info-pedidos"><?= h($pedido->created) ?></div>
              <div class="col-sm-2 text-left info-pedidos no-small">R$ <?= number_format($pedido->valor, 2, ',', '.'); ?>
              </div>
              <div class="col-sm-2 text-left info-pedidos"><?= $pedido->pedido_status->name ?></div>
              <div class="col-sm-2 text-right info-pedidos">
                <?= $this->Html->link(
                    ' <button type="button" class="btn bg-blue">Detalhes <i class="fa fa-search"></i></button>',
                    '#detalhes',
                    [
                        'escape'        => false,
                        'style'         => 'cursor: pointer;',
                        'data-pid'      => $pedido->id,
                        'title'         => "Clique para visualizar detalhes deste pedido",
                        'data-target'   => "#pid-".$pedido->id
                    ]
                )
                ?>
              </div>
            </div>
              <div id="pid-<?= $pedido->id ?>" role="dialog" class="pedidos-titulo borda-detalhes">
                <?php if($pedido->pedido_status_id != 7 && $pedido->pedido_status_id != 8) { ?>
                <div class="col-sm-8 col-xs-12 icones-box">
                  <?php if($pedido->pedido_status_id >=1) { ?>
                  <div class="icones-pedidos text-center padding-0" style="color: #8dc73f">
                    <i class="fa fa-check-circle-o" style="position: absolute; top: 0; right: 22px"></i>
                    <div class="icon-pedidos-box" style="color: #8dc73f">
                      <svg class="icones-tamanho" fill="#8dc73f" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"><g><polygon points="8.669,48.706 5,62.65 6.565,64.773 10.008,54.727  "/><polygon points="93.422,64.79 95,62.65 91.331,48.706 89.991,54.727  "/><polygon points="41.311,14.947 39.661,15.937 37.462,30.863 37.071,40.502 39.622,40.182 41.47,14.947  "/><polygon points="62.928,40.51 62.538,30.863 60.338,15.937 58.689,14.947 58.529,14.947 60.378,40.188  "/><polygon points="29.01,42.523 21.586,43.458 9.53,47.966 11.045,54.785 5.5,70.967 5.5,88.654 27.14,88.654  "/><polygon points="36.48,55.079 35.119,88.654 64.879,88.654 63.519,55.079  "/><polygon points="52.475,40.195 47.463,40.199 37.03,41.513 36.716,49.244 63.282,49.244 62.969,41.516  "/><polygon points="88.954,54.785 90.47,47.966 78.413,43.458 70.989,42.524 72.859,88.654 94.5,88.654 94.5,70.967  "/><path d="M28.12,89.154h5.978l1.422-35.075h28.96L65.9,89.154h5.979l-2.368-58.421L65.62,15.752l-7.219-4.906h-7.104h-2.595h-7.105   l-7.218,4.906l-3.891,14.981L28.12,89.154z M51.578,13.947h7.388l2.256,1.354l2.312,15.433l0.79,19.511H35.675l0.792-19.511   l2.311-15.433l2.257-1.354h7.386H51.578z"/></g></svg>
                    </div>
                    <p style="color: #8dc73f">PEDIDO</p>
                    <p><?= h($pedido->created) ?></p>
                  </div>
                  <div class="seta-pedidos text-center padding-0">
                    <span class="fa fa-arrow-right" style="color: #8dc73f"></span>
                  </div>
                  <?php } else { ?>
                  <div class="icones-pedidos text-center padding-0">
                    <div class="icon-pedidos-box">
                      <svg class="icones-tamanho" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"><g><polygon points="8.669,48.706 5,62.65 6.565,64.773 10.008,54.727  "/><polygon points="93.422,64.79 95,62.65 91.331,48.706 89.991,54.727  "/><polygon points="41.311,14.947 39.661,15.937 37.462,30.863 37.071,40.502 39.622,40.182 41.47,14.947  "/><polygon points="62.928,40.51 62.538,30.863 60.338,15.937 58.689,14.947 58.529,14.947 60.378,40.188  "/><polygon points="29.01,42.523 21.586,43.458 9.53,47.966 11.045,54.785 5.5,70.967 5.5,88.654 27.14,88.654  "/><polygon points="36.48,55.079 35.119,88.654 64.879,88.654 63.519,55.079  "/><polygon points="52.475,40.195 47.463,40.199 37.03,41.513 36.716,49.244 63.282,49.244 62.969,41.516  "/><polygon points="88.954,54.785 90.47,47.966 78.413,43.458 70.989,42.524 72.859,88.654 94.5,88.654 94.5,70.967  "/><path d="M28.12,89.154h5.978l1.422-35.075h28.96L65.9,89.154h5.979l-2.368-58.421L65.62,15.752l-7.219-4.906h-7.104h-2.595h-7.105   l-7.218,4.906l-3.891,14.981L28.12,89.154z M51.578,13.947h7.388l2.256,1.354l2.312,15.433l0.79,19.511H35.675l0.792-19.511   l2.311-15.433l2.257-1.354h7.386H51.578z"/></g></svg>
                    </div>
                    <p>PEDIDO</p>
                  </div>
                  <div class="seta-pedidos text-center padding-0">
                    <span class="fa fa-arrow-right"></span>
                  </div>
                  <?php } ?>

                  <?php if($pedido->pedido_status_id >=3) { ?>
                  <div class="icones-pedidos text-center padding-0" style="color: #8dc73f">
                    <i class="fa fa-check-circle-o" style="position: absolute; top: 0; right: 15px"></i>
                    <div class="icon-pedidos-box">
                        <?= $this->Html->image(
                            WEBROOT_URL.'webroot/images/icone-pedidos-2-verde.png',
                            ['class' => 'icones-tamanho']) ?>
                    </div>
                    <p style="color: #8dc73f">PAGAMENTO</p>
                    <p><?= h($pedido->data_pagamento) ?></p>
                  </div>
                  <div class="seta-pedidos text-center padding-0">
                    <span class="fa fa-arrow-right" style="color: #8dc73f"></span>
                  </div>
                    <?php } else { ?>
                  <div class="icones-pedidos text-center padding-0">
                    <div class="icon-pedidos-box">
                        <?= $this->Html->image(
                            WEBROOT_URL.'webroot/images/icone-pedidos-2.png',
                            ['class' => 'icones-tamanho']) ?>
                    </div>
                      <p>PAGAMENTO</p>
                  </div>
                  <div class="seta-pedidos text-center padding-0">
                    <span class="fa fa-arrow-right"></span>
                  </div>
                  <?php } ?>
                  <?php if($pedido->pedido_status_id == 5 || $pedido->pedido_status_id == 6 || $pedido->pedido_status_id == 9 ) { ?>
                      <div id="desk-transportadora-<?= $pedido->id ?>" class="overlay">
                      <!-- Overlay content -->
                      <!-- Button to close the overlay navigation -->
                          <div class="overlay-content text-center">
                              <a class="closebtn close-desk-transportadora-btn" data-id="<?= $pedido->id ?>">&times;</a>
                              <h3>Acompanhe seu pedido</h3>
                              <br />
                              <div class="col-xs-12 text-left">
                                  <div class="row">
                                      <div class="col-md-6">
                                          <p><strong>Transportadora:</strong> <?= $pedido->transportadora->name; ?></p>
                                          <p><strong>Mensagem da transportadora:</strong> <?= $pedido->transportadora->mensagem; ?></p>
                                      </div>
                                      <div class="col-md-6">
                                          <?php if($pedido->rastreamento) { ?>
                                              <p><strong>Código:</strong> <?= $pedido->rastreamento; ?></p>
                                          <?php } else { ?>
                                              <p><strong>Código:</strong> Em breve</p>
                                          <?php } ?>
                                          <p><strong>Site:</strong> <?= $this->Html->link($pedido->transportadora->site, 'http'.$pedido->transportadora->site, ['target' => '_blank']); ?></p>
                                      </div>
                                      <?php if($pedido->observacao) { ?>
                                      <div class="col-md-12">
                                          <p><span style="color: red"><strong>Observações:</strong></span> <?= $pedido->observacao ?></h4>
                                      </div>
                                      <?php } ?>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="icones-pedidos text-center padding-0 open-desk-transportadora-btn" data-id="<?= $pedido->id ?>">
                        <div class="icon-pedidos-box">
                          <i class="fa fa-check-circle-o" style="position: absolute; top: 0; right: 15px; color: #8dc73f"></i>
                          <i class="fa fa-2x fa-truck icones-tamanho" style="color: #8dc73f; cursor: pointer;"></i>
                        </div>
                        <p style="color: #8dc73f">TRANSPORTE</p>
                        <p style="color: #8dc73f"><?= h($pedido->data_transporte) ?></p>
                    </div>
                      <div class="seta-pedidos text-center padding-0">
                          <span class="fa fa-arrow-right" style="color: #8dc73f"></span>
                      </div>
                      <?php } else { ?>
                      <div class="icones-pedidos text-center padding-0">
                          <div class="icon-pedidos-box">
                            <i class="fa fa-2x fa-truck icones-tamanho" ></i>
                          </div>
                          <p>TRANSPORTE</p>
                      </div>
                      <div class="seta-pedidos text-center padding-0"><span class="fa fa-arrow-right"></span></div>
                  <?php } ?>
                  <?php if($pedido->pedido_status_id >=6 && $pedido->pedido_status_id != 9 && $pedido->pedido_status_id != 10) { ?>
                  <div class="icones-pedidos text-center padding-0" style="color: #8dc73f">
                    <i class="fa fa-check-circle-o" style="position: absolute; top: 0; right: 22px"></i>
                    <div class="icon-pedidos-box">
                      <?= $this->Html->image(
                              WEBROOT_URL.'webroot/images/icone-pedidos-4-verde.png',
                              ['class' => 'icones-tamanho']) ?>
                    </div>
                      <p style="color: #8dc73f">ENTREGUE NA ACADEMIA</p>
                      <p><?= h($pedido->entrega) ?></p>
                  </div>
                  <?php } else { ?>
                  <div class="icones-pedidos text-center padding-0">
                    <div class="icon-pedidos-box">
                      <?= $this->Html->image(
                              WEBROOT_URL.'webroot/images/icone-pedidos-4.png',
                              ['class' => 'icones-tamanho']) ?>
                    </div>
                    <p>ENTREGUE NA ACADEMIA</p>
                  </div>
                  <?php } ?>
                </div>
                <?php } ?>
                <div class="row bg-info col-xs-12">
                    <div class="col-xs-1 font-bold text-center">Cód.</div>
                    <div class="col-xs-2 font-bold text-center">Produto</div>
                    <div class="col-xs-4 font-bold text-left"></div>
                    <div class="col-xs-1 font-bold text-center">Qtd.</div>
                    <div class="col-xs-2 font-bold text-center">Valor</div>
                    <div class="col-xs-2 font-bold text-center">Total</div>
                </div>
                <?php $total = 0; ?>
                <?php foreach($pedido->pedido_itens as $item): ?>
                <?php $fotos = unserialize($item->produto->fotos); ?>
                <div class="row pedidos-single col-xs-12">
                  <div class="col-xs-1 text-center"><?= $item->produto_id ?></div>
                    <div class="col-xs-2 text-center"><?= $this->Html->image(
                                $fotos[0],
                                ['class' => 'pedidos-grade-indicacoes', 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]) ?>
                    </div>
                  <div class="col-xs-4 text-left">
                    <?php if($item->produto->status_id == 1) { ?>
                        <?= $this->Html->link($item->produto->produto_base->name.' '
                            .$item->produto->produto_base->embalagem_conteudo.' '
                            .$item->produto->propriedade,
                                '/produto/'.$item->produto->slug,
                                ['target' => '_blank', 'escape' => false]
                        ); ?>
                  <?php } else { ?>
                      <?= $item->produto->produto_base->name.' '
                          .$item->produto->produto_base->embalagem_conteudo.' '
                          .$item->produto->propriedade ?>
                  <?php } ?>
                  </div>
                  <div class="col-xs-1 text-center"><?= $item->quantidade ?></div>
                  <div class="col-xs-2 text-right">R$ <?= number_format($item->preco, 2, ',', '.') ?></div>
                  <div class="col-xs-2 text-right">R$ <?= number_format(($item->quantidade * $item->preco), 2, ',', '.') ?></div>
                </div>
                <?php $total = $total + ($item->quantidade * $item->preco) ?>
                <?php endforeach; ?>
                  <div class="row col-xs-12 text-right">
                    <div class="col-xs-10 total-pedidos">
                      <p><strong>Total do Pedido:</strong></p>
                    </div>
                    <div class="col-xs-2">R$ <?= number_format($pedido->valor, 2, ',', '.'); ?></div>
                    <div class="col-sm-10">
                      <strong>Parcelas:</strong>
                    </div>
                    <div class="col-xs-2">
                      <?php if($pedido->parcelas > 1) { ?>
                        <div class="text-right" style="clear: both;">
                            <?= $pedido->parcelas ?>x de R$ <?= number_format($pedido->valor_parcelado/$pedido->parcelas, 2, ',', '.') ?>
                        </div>
                      <?php } else { ?>
                          <div class="text-right" style="clear: both;">
                            <?= "1x de ".number_format($pedido->valor, 2, ',', '.'); ?>
                          </div>
                      <?php } ?>
                    </div> 
                    <?php if($pedido->cupom_desconto_id != null) { ?>
                        <?php foreach ($cupom_desconto as $cd) {
                            if($pedido->cupom_desconto_id == $cd->id) { ?>
                                <div class="col-sm-12 text-right">
                                    <p class="id-cupom"><strong>Cupom de desconto: <?= $cd->codigo ?></strong></p>
                                </div>
                            <?php }
                        } ?>
                    <?php } ?>
                  </div>
                  <br />
                  <hr class="separador">
                </div>
                <?php endforeach; ?>          
                <div class="paginator" style="margin-left: 15px;">
                  <ul class="pagination">
                      <?= $this->Paginator->prev('< ' . __('previous')) ?>
                      <?= $this->Paginator->numbers() ?>
                      <?= $this->Paginator->next(__('next') . ' >') ?>
                  </ul>
                  <p style="margin-left: 15px"><?= $this->Paginator->counter() ?></p>
                </div>
          </div>

          <div class="lista-mobile">
            <div class="pedidos-mobile">
              <?php
                foreach($pedidos as $pedido):
                    $transaction = unserialize($pedido->transaction_data);

                    if(!empty($pedido->transaction_data)) {
                        $xml[$pedido->id] = \simplexml_load_string(unserialize($pedido->transaction_data));
                        $xml[$pedido->id] = json_encode($xml[$pedido->id]);
                        $xml[$pedido->id] = json_decode($xml[$pedido->id], true);
                    }
                    ?>
                    <hr class="separador">
                    <div class="pedidos-linhas-mobile">
                        <div class="col-xs-6 text-center info-pedidos">
                          <p class="title-info-mobile"><strong>Pedido</strong></p>
                          <p><?= $pedido->id ?></p></div>
                        <div class="col-xs-6 text-center info-pedidos">
                          <p class="title-info-mobile"><strong>Data</strong></p>
                          <p><?= h($pedido->created) ?></p></div>
                        <div class="col-xs-6 text-center info-pedidos">
                          <p class="title-info-mobile"><strong>Aluno</strong></p>
                          <p><?= $pedido->cliente->name ?></p></div>
                        <div class="col-xs-6 text-center info-pedidos">
                          <p class="title-info-mobile"><strong>Status</strong></p>
                          <p><?= $pedido->pedido_status->name ?></p></div>
                        <div class="col-xs-12 text-center info-pedidos" style="float: left; margin-bottom: 10px;">

                            <?php $p_inativos = 0; ?>

                            <?php foreach($pedido->pedido_itens as $item): ?>
                                <?php if($item->produto->status_id == 2) {
                                    $p_inativos = 1;
                                } ?>
                            <?php endforeach; ?>

                            <?= $this->Html->link(
                                ' <button type="button" class="btn bg-blue">Detalhes <i class="fa fa-search"></i></button>',
                                '#detalhesmobile',
                                [
                                    'escape'        => false,
                                    'style'         => 'cursor: pointer;',
                                    'data-pid'      => $pedido->id,
                                    'title'         => "Clique para visualizar detalhes deste pedido",
                                    'data-target'   => "#pidm-".$pedido->id
                                ]
                            )
                            ?>
                        </div>
                        <div id="pidm-<?= $pedido->id ?>" role="dialog" class="pedidos-titulo-mobile">

                            <?php if($pedido->pedido_status_id != 7) { ?>
                            <div class="icones-box">
                                <?php if($pedido->pedido_status_id >=1) { ?>
                                <div class="icones-pedidos text-center padding-0" style="color: #8dc73f">
                                    <i class="fa fa-check-circle-o check-mobile"></i>
                                    <div class="icon-pedidos-box" style="color: #8dc73f">
                                        <svg class="icones-tamanho" fill="#8dc73f" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"><g><polygon points="8.669,48.706 5,62.65 6.565,64.773 10.008,54.727  "/><polygon points="93.422,64.79 95,62.65 91.331,48.706 89.991,54.727  "/><polygon points="41.311,14.947 39.661,15.937 37.462,30.863 37.071,40.502 39.622,40.182 41.47,14.947  "/><polygon points="62.928,40.51 62.538,30.863 60.338,15.937 58.689,14.947 58.529,14.947 60.378,40.188  "/><polygon points="29.01,42.523 21.586,43.458 9.53,47.966 11.045,54.785 5.5,70.967 5.5,88.654 27.14,88.654  "/><polygon points="36.48,55.079 35.119,88.654 64.879,88.654 63.519,55.079  "/><polygon points="52.475,40.195 47.463,40.199 37.03,41.513 36.716,49.244 63.282,49.244 62.969,41.516  "/><polygon points="88.954,54.785 90.47,47.966 78.413,43.458 70.989,42.524 72.859,88.654 94.5,88.654 94.5,70.967  "/><path d="M28.12,89.154h5.978l1.422-35.075h28.96L65.9,89.154h5.979l-2.368-58.421L65.62,15.752l-7.219-4.906h-7.104h-2.595h-7.105   l-7.218,4.906l-3.891,14.981L28.12,89.154z M51.578,13.947h7.388l2.256,1.354l2.312,15.433l0.79,19.511H35.675l0.792-19.511   l2.311-15.433l2.257-1.354h7.386H51.578z"/></g></svg>
                                    </div>
                                    <p style="color: #8dc73f">PEDIDO</p>
                                    <p><?= h($pedido->created) ?></p>
                                </div>

                                <div class="seta-pedidos text-center padding-0"><span class="fa fa-arrow-right" style="color: #8dc73f"></span></div>
                                <?php } else { ?>
                                <div class="icones-pedidos text-center padding-0">
                                    <div class="icon-pedidos-box">
                                        <svg class="icones-tamanho" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"><g><polygon points="8.669,48.706 5,62.65 6.565,64.773 10.008,54.727  "/><polygon points="93.422,64.79 95,62.65 91.331,48.706 89.991,54.727  "/><polygon points="41.311,14.947 39.661,15.937 37.462,30.863 37.071,40.502 39.622,40.182 41.47,14.947  "/><polygon points="62.928,40.51 62.538,30.863 60.338,15.937 58.689,14.947 58.529,14.947 60.378,40.188  "/><polygon points="29.01,42.523 21.586,43.458 9.53,47.966 11.045,54.785 5.5,70.967 5.5,88.654 27.14,88.654  "/><polygon points="36.48,55.079 35.119,88.654 64.879,88.654 63.519,55.079  "/><polygon points="52.475,40.195 47.463,40.199 37.03,41.513 36.716,49.244 63.282,49.244 62.969,41.516  "/><polygon points="88.954,54.785 90.47,47.966 78.413,43.458 70.989,42.524 72.859,88.654 94.5,88.654 94.5,70.967  "/><path d="M28.12,89.154h5.978l1.422-35.075h28.96L65.9,89.154h5.979l-2.368-58.421L65.62,15.752l-7.219-4.906h-7.104h-2.595h-7.105   l-7.218,4.906l-3.891,14.981L28.12,89.154z M51.578,13.947h7.388l2.256,1.354l2.312,15.433l0.79,19.511H35.675l0.792-19.511   l2.311-15.433l2.257-1.354h7.386H51.578z"/></g></svg>
                                    </div>

                                    <p>PEDIDO</p>
                                </div>

                                <div class="seta-pedidos text-center padding-0"><span class="fa fa-arrow-right"></span></div>
                                <?php } ?>

                                <?php if($pedido->pedido_status_id >=3) { ?>
                                <div class="icones-pedidos text-center padding-0" style="color: #8dc73f">
                                    <i class="fa fa-check-circle-o check-mobile"></i>
                                    <div class="icon-pedidos-box">
                                        <?= $this->Html->image(
                                            WEBROOT_URL.'webroot/images/icone-pedidos-2-verde.png',
                                            ['class' => 'icones-tamanho']) ?>
                                    </div>

                                    <p style="color: #8dc73f">PAGAMENTO</p>
                                    <p><?= h($pedido->data_pagamento) ?></p>
                                </div>

                                <div class="seta-pedidos text-center padding-0"><span class="fa fa-arrow-right" style="color: #8dc73f"></span></div>
                                <?php } else { ?>
                                <div class="icones-pedidos text-center padding-0">
                                    <div class="icon-pedidos-box">
                                        <?= $this->Html->image(
                                            WEBROOT_URL.'webroot/images/icone-pedidos-2.png',
                                            ['class' => 'icones-tamanho']) ?>
                                    </div>

                                    <p>PAGAMENTO</p>
                                </div>

                                <div class="seta-pedidos text-center padding-0"><span class="fa fa-arrow-right"></span></div>
                                <?php } ?>
                                <?php if($pedido->pedido_status_id == 5 || $pedido->pedido_status_id == 6 || $pedido->pedido_status_id == 9 ) { ?>
                                    <!-- Overlay mostrando observação - $pedido->observacao -->
                                <div id="transportadora-<?= $pedido->id ?>" class="overlay">
                                    <!-- Button to close the overlay navigation -->
                                    <!-- Overlay content -->
                                    <div class="overlay-content text-center">
                                        <a class="closebtn close-transportadora-btn" data-id="<?= $pedido->id ?>">&times;</a>
                                        <h3>Acompanhe seu pedido</h3>
                                        <br />
                                        <div class="col-xs-12 text-left">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p><strong>Transportadora:</strong> <?= $pedido->transportadora->name; ?></p>
                                                    <p><strong>Mensagem da transportadora:</strong> <?= $pedido->transportadora->mensagem; ?></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php if($pedido->rastreamento) { ?>
                                                    <p><strong>Código:</strong> <?= $pedido->rastreamento; ?></p>
                                                    <?php } else { ?>
                                                    <p><strong>Código:</strong> Em breve</p>
                                                    <?php } ?>
                                                    <p><strong>Site:</strong> <?= $this->Html->link($pedido->transportadora->site, 'http'.$pedido->transportadora->site, ['target' => '_blank']); ?></p>
                                                </div>
                                                <?php if($pedido->observacao) { ?>
                                                <div class="col-md-12">
                                                    <p><span style="color: red"><strong>Observações:</strong></span> <?= $pedido->observacao ?></h4>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="icones-pedidos text-center padding-0 open-transportadora-btn" data-id="<?= $pedido->id ?>">
                                    <i class="fa fa-check-circle-o" style="position: absolute; top: 0; right: 21px; color: #8dc73f"></i>
                                    <div class="icon-pedidos-box">
                                        <i class="fa fa-2x fa-truck icones-tamanho" style="color: #8dc73f;"></i>
                                    </div>
                                <p style="color: #8dc73f">TRANSPORTE</p>
                                <p style="color: #8dc73f"><?= h($pedido->data_transporte) ?></p>
                                </div>
                                <div class="seta-pedidos text-center padding-0"><span class="fa fa-arrow-right" style="color: #8dc73f"></span></div>
                                <?php } else { ?>
                                <div class="icones-pedidos text-center padding-0">
                                    <div class="icon-pedidos-box">
                                        <i class="fa fa-2x fa-truck icones-tamanho"></i>
                                    </div>
                                    <p>TRANSPORTE</p>
                                </div>
                                <div class="seta-pedidos text-center padding-0"><span class="fa fa-arrow-right"></span></div>
                                <?php } ?>
                                <?php if($pedido->pedido_status_id >=6 && $pedido->pedido_status_id != 9 && $pedido->pedido_status_id != 10) { ?>
                                <div class="icones-pedidos text-center padding-0" style="color: #8dc73f">
                                    <i class="fa fa-check-circle-o check-mobile"></i>
                                    <div class="icon-pedidos-box">
                                        <?= $this->Html->image(
                                            WEBROOT_URL.'webroot/images/icone-pedidos-4-verde.png',
                                            ['class' => 'icones-tamanho']) ?>
                                    </div>
                                    <p style="color: #8dc73f">ENTREGUE NA ACADEMIA</p>
                                    <p><?= h($pedido->entrega) ?></p>
                                </div>
                                <?php } else { ?>
                                <div class="icones-pedidos text-center padding-0">
                                    <div class="icon-pedidos-box">
                                        <?= $this->Html->image(
                                            WEBROOT_URL.'webroot/images/icone-pedidos-4.png',
                                            ['class' => 'icones-tamanho']) ?>
                                    </div>

                                    <p>ENTREGUE NA ACADEMIA</p>
                                </div>
                                <?php } ?>

                            </div>
                            <?php } ?>

                            <?php $total = 0 ?>

                            <?php foreach($pedido->pedido_itens as $item): ?>
                                <?php $fotos = unserialize($item->produto->fotos); ?>
                                <div class="col-xs-12" style="margin-bottom: 5px;">
                                    <div class="col-xs-8 text-left"><p class="title-mobile codigo-mobile">Cód.: <?= $item->produto_id ?></p></div>
                                    <div class="col-xs-4 text-right"><?= $this->Html->image(
                                        $fotos[0],
                                        ['class' => 'fotos-mobile', 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]) ?></div>
                                    <div class="col-xs-12 text-left"><p class="title-mobile">Produto</p><p class="valores-mobile">
                                        <?php if($item->produto->status_id == 1) { ?>
                                            <?= $this->Html->link($item->produto->produto_base->name.' '
                                                .$item->produto->produto_base->embalagem_conteudo.' '
                                                .$item->produto->propriedade,
                                                    '/produto/'.$item->produto->slug,
                                                    ['target' => '_blank', 'escape' => false]
                                            ); ?>
                                        <?php } else { ?>
                                            <?= $item->produto->produto_base->name.' '
                                                .$item->produto->produto_base->embalagem_conteudo.' '
                                                .$item->produto->propriedade ?>
                                        <?php } ?>
                                    </div>
                                    <div class="col-xs-2 text-center"><p class="title-mobile">Qtd</p><p class="valores-mobile"><?= $item->quantidade ?></p></div>
                                    <div class="col-xs-5 text-right"><p class="title-mobile">Valor</p><p class="valores-mobile">R$ <?= number_format($item->preco, 2, ',', '.') ?></p></div>
                                    <div class="col-xs-5 text-right"><p class="title-mobile">Total</p><p class="valores-mobile">R$ <?= number_format(($item->quantidade * $item->preco), 2, ',', '.') ?></p></div>
                                    <hr class="separador-produto">

                                    <?php $total = $total + ($item->quantidade * $item->preco) ?>
                                </div>
                            <?php endforeach; ?>

                                <div class="col-xs-12">
                                    <div class="col-xs-5 text-left valores-mobile no-padding">
                                        <?= '<strong>Parcelas: </strong>'.$pedido->parcelas ?>
                                    </div>
                                    <div class="col-xs-7 valores-mobile  no-padding">
                                      <?php if($pedido->parcelas > 1) { ?>
                                        <div class="text-right valores-mobile" style="clear: both;">
                                            <?= $pedido->parcelas ?>x de R$ <?= number_format($pedido->valor_parcelado/$pedido->parcelas, 2, ',', '.') ?>
                                        </div>
                                    <?php }
                                      else{ ?>
                                        <div class="text-right valores-mobile  no-padding" style="clear: both;">
                                          <?= "1x de ".number_format($pedido->valor, 2, ',', '.'); ?>
                                        </div>
                                     <?php }
                                      ?>
                                    </div>
                                    <div class="col-xs-6 text-left no-padding">
                                      <p><strong>Total pedido:</strong></p>
                                    </div>
                                    <div class="col-xs-6 text-right total-pedidos-mobile no-padding">
                                        <?php if($pedido->parcelas > 1){
                                          echo "R$ ".number_format($pedido->valor_parcelado, 2, ',', '.');
                                          } 
                                          else{
                                            echo "R$ ".number_format($pedido->valor, 2, ',', '.');
                                          }
                                          ?>
                                        
                                    </div>
                                    

                                    <?php if($total != $pedido->valor) { ?>

                                    <div class="col-sm-12 text-right">
                                        <p class="id-cupom"><strong>Cupom de desconto utilizado =)</strong></p>
                                    </div>

                                    <?php } ?>

                                </div>

                            <br class="clear">
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="paginator">
                  <ul class="pagination">
                      <?= $this->Paginator->prev('< ' . __('previous')) ?>
                      <?= $this->Paginator->numbers() ?>
                      <?= $this->Paginator->next(__('next') . ' >') ?>
                  </ul>
                  <p style="text-align: center;"><?= $this->Paginator->counter() ?></p>
                </div>
            </div>
          </div>



        </div>
      </div>
    </div>
  </div>
</section>
