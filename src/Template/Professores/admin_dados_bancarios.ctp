<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<?= $this->Html->script('jquery.mask.min.js')?>
<script>
    $(document).ready(function(){
        $(".cpf").mask("999.999.999-99");
        $(".cpf-mask").mask("999.999.999-99");
        $(".cep").mask("99.999-999");
        $('.cnpj-mask').mask('00.000.000/0000-00', {reverse: false});
    });
</script>

<style>
    .loading{
        display: none;
        background: rgba(255,255,255,0.95);
        padding: 20px;
        z-index: 9999;
        position: fixed;
        left: 50%;
        margin-left: -145px;
        float: inherit;
        width: 290px;
        height: 100px;
        border-radius: 15px;
        border: 2px solid #222d32;
        box-shadow: -2px 2px 10px #BBB;
    }
    .btn-meus-dados{
        margin-top: 15px;
        width: 140px;
    }
    @media all and (max-width: 991px){
        .label-cartao{
            text-align: left!important;
        }
    }
    .inputs-cpf-cnpj input{
      display: none;
    }
    .inputs-cpf-cnpj input:checked + label .label-checkbox {
      border-color: #5089cf;
    }
    .inputs-cpf-cnpj input:checked + label{
      color: #5089cf;
    }
    .label-checkbox{
      padding: 15px;
      border: 2px solid #ddd;
      border-radius: 7px;
      width: 100px;
      display: inline-block;
      cursor: pointer;
      margin: 0 10px;
    }
    .label-checkbox label{
      cursor: pointer;
    }
    .cpf-div {
        display: none;
    }
</style>

<section class="content">
    <div class="col-xs-12 text-center">
        <div class="loading text-center">
            <span class="helper"></span>
            <i class="fa fa-refresh fa-spin fa-3x fa-fw margin-bottom"></i>
            <span class="">Atualizando conteúdo...</span>
        </div><!-- loading -->
    </div> <!-- xs-12 -->
    <div class="col-xs-12 col-lg-10">
        <div class="box box-info">
            <div class="box-body">
                <div class="users form">
                    <?php if($professor->card_status == 0) { ?>
                        <?= $this->Form->create($professor, ['class' => 'form-horizontal bordered-row', 'url' => ['controller' => 'Professores', 'action' => 'salvar_dados_bancarios_cnpj', $comissao]]); ?>
                        <fieldset>
                            <h3 class="box-header">Dados bancários <span class="pull-right">
                                <?= $this->Html->link('<button class="btn btn-success" type="button">Minhas Comissões</button>',
                                    ['controller' => 'Professores', 'action' => 'admin_comissoes_index'], ['escape' => false]) ?>
                                <a href="http://blog.logfitness.com.br/conheca-o-logcard" target="_blank"><button class="btn bg-blue" type="button">Conheça o Logcard</button></a>
                            </span> </h3>
                            <div class="row">
                              <div class="col-xs-12 text-center total-cpf-cnpj">
                                  <div class="inputs-cpf-cnpj">
                                    <input type="radio" id="check-1" name="check-cpf-cnpj" class="check-cpf-cnpj check-cnpj" value="cnpj">
                                    <label for="check-1">
                                        <div class="label-checkbox">
                                            CNPJ
                                        </div>
                                    </label>
                                    <input type="radio" id="check-2" name="check-cpf-cnpj" class="check-cpf-cnpj check-cpf" value="cpf"> 
                                    <label for="check-2">
                                        <div class="label-checkbox">
                                            CPF
                                        </div>
                                    </label>
                                  </div>
                              </div>
                            </div>

                            <script type="text/javascript">
                                $(document).ready(function() {
                                    $('.check-cpf-cnpj').click(function() {
                                        var value = $(this).val();

                                        if(value == 'cpf') {
                                            $('.cnpj-div').fadeOut();
                                            setTimeout(function() {
                                                $('.cpf-div').fadeIn();
                                            }, 250);
                                        }

                                        if(value == 'cnpj') {
                                            $('.cpf-div').fadeOut();
                                            setTimeout(function() {
                                                $('.cnpj-div').fadeIn();
                                            }, 250);
                                        }
                                    });
                                });
                            </script>

                            <?php if($professor->is_cpf == 0) { ?>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $('.check-cnpj').click();
                                    });
                                </script>
                            <?php } else { ?>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $('.check-cpf').click();
                                    });
                                </script>
                            <?php } ?>

                            <div class="col-md-12 cnpj-div">
                                <div class="form-group col-md-6">
                                    <label for="cnpj" class="control-label">CNPJ</label>
                                    <?= $this->Form->input('cnpj_bancario', [
                                      'class'       => 'form-control cnpj-mask',
                                      'placeholder' => 'CNPJ',
                                      'label'       => false
                                    ]) ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="razao_social" class="control-label">Razão Social</label>
                                    <?= $this->Form->input('razao_bancario', [
                                      'class'       => 'form-control',
                                      'placeholder' => 'Razão Social',
                                      'label'       => false
                                    ]) ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="tipo_conta" class="control-label">Tipo de conta</label>
                                    <?= $this->Form->input('tipo_conta', [
                                      'options' => [
                                        'Selecione'      => 'Selecione',
                                        'Conta Corrente' => 'Conta Corrente',
                                        'Conta Poupança' => 'Conta Poupança'
                                      ],
                                      'class'   => 'form-control',
                                      'label'   => false
                                    ]) ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="banco" class="control-label">Banco</label>
                                    <?= $this->Form->input('banco', [
                                      'options' => [
                                        'Banco do Brasil'   => 'Banco do Brasil',
                                        'Bradesco'          => 'Bradesco',
                                        'Caixa'             => 'Caixa Econ. Federal',
                                        'Itaú'              => 'Itaú',
                                        'Santander'         => 'Santander'
                                      ],
                                      'class'   => 'form-control banco-input',
                                      'label'   => false
                                    ]) ?>
                                    <p><strong>Não tem nenhum desses bancos? Nos chame no chat para saber sobre o Logcard!</strong></p>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="col-md-12 no-padding">
                                        <label for="agencia" class="control-label">Agência</label>
                                    </div>
                                    <div class="col-xs-8 no-padding">
                                        <?= $this->Form->input('agencia', [
                                          'class'       => 'form-control agencia-input',
                                          'onkeypress'  => 'return event.charCode >= 48 && event.charCode <= 57',
                                          'placeholder' => 'Agência',
                                          'label'       => false
                                        ]) ?>
                                    </div>
                                    <div class="col-xs-4">
                                        <?= $this->Form->input('agencia_dv', [
                                          'class'       => 'form-control agencia-dv-input',
                                          'placeholder' => 'Digito',
                                          'label'       => false,
                                          'maxlength'   => 1
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="col-md-12 no-padding">
                                        <label for="conta" class="control-label col-sm-2">Conta</label>
                                    </div>
                                    <div class="col-xs-8 no-padding">
                                        <?= $this->Form->input('conta', [
                                          'class'       => 'form-control conta-input',
                                          'onkeypress'  => 'return event.charCode >= 48 && event.charCode <= 57',
                                          'placeholder' => 'Conta',
                                          'label'       => false
                                        ]) ?>
                                    </div>
                                    <div class="col-xs-4">
                                        <?= $this->Form->input('conta_dv', [
                                          'class'       => 'form-control conta-dv-input',
                                          'placeholder' => 'Digito',
                                          'label'       => false,
                                          'maxlength'   => 1
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="banco_obs" class="control-label">Instruções de pagamento</label>
                                    <?= $this->Form->input('banco_obs', [
                                        'type'     => 'textarea',
                                        'class'    => 'form-control validate[optional]',
                                        'label'    => false,
                                        'placeholder'   =>
                                                'Caso haja alguma regra ou observação a ser feita em relação a depósitos/transferências na conta, descreva aqui...',
                                    ]) ?>
                                </div>
                                <div class="form-group col-md-12">
                                    <h5 class="text-center">Obs.: Informe todos os dados de forma correta. Não nos responsabilizamos por transtornos causados pelo preenchimento incorreto dos dados</h5>
                                    <div class="divider"></div>
                                </div>
                                <div class="text-center col-xs-12">
                                    <div class="col-sm-12 col-md-12">
                                        <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                                            ['class' => 'btn btn-alt btn-hoverrr btn-success form-bancarios-cnpj', 'style' =>  'width: 9.5em;',
                                                'escape' => false]); ?>
                                    </div>
                                </div>

                                <script type="text/javascript">
                                  $(document).ready(function() {
                                    $('.form-bancarios-cnpj').click(function() {

                                      $('.error-bancario').remove();
                                      var error = 0;

                                      if($('.cnpj-div .agencia-input').val().length < 4) {
                                        $('.cnpj-div .agencia-input').parent().append('<p class="error-bancario" style="color: red">o número da agência deve ter no mínimo 4 dígitos. Exemplo: 9999</p>');
                                        $('.cnpj-div .agencia-input').focus();
                                        error = 1;
                                      }
                                      if($('.cnpj-div .conta-dv-input').val().length <= 0) {
                                        $('.cnpj-div .conta-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da conta!</p>');
                                        $('.cnpj-div .conta-dv-input').focus();
                                        error = 1;
                                      }

                                      if($('.cnpj-div .banco-input').val() == 'Banco do Brasil') {
                                        if($('.cnpj-div .agencia-dv-input').val().length <= 0) {
                                          $('.cnpj-div .agencia-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da agencia!</p>');
                                          $('.cnpj-div .agencia-dv-input').focus();
                                          error = 1;
                                        }
                                        if($('.cnpj-div .conta-input').val().length < 8) {
                                          $('.cnpj-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                          $('.cnpj-div .conta-input').focus();
                                          error = 1;
                                        }
                                      } else if($('.cnpj-div .banco-input').val() == 'Bradesco') {
                                        if($('.cnpj-div .agencia-dv-input').val().length <= 0) {
                                          $('.cnpj-div .agencia-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da agencia!</p>');
                                          $('.cnpj-div .agencia-dv-input').focus();
                                          error = 1;
                                        }
                                        if($('.cnpj-div .conta-input').val().length < 8) {
                                          $('.cnpj-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                          $('.cnpj-div .conta-input').focus();
                                          error = 1;
                                        }
                                      } else if($('.cnpj-div .banco-input').val() == 'Caixa') {
                                        if($('.cnpj-div .conta-input').val().length < 11) {
                                          $('.cnpj-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 11 dígitos. Exemplo: XXX99999999 (X: Operação)</p>');
                                          $('.cnpj-div .conta-input').focus();
                                          error = 1;
                                        }
                                      } else if($('.cnpj-div .banco-input').val() == 'Itaú') {
                                        if($('.cnpj-div .conta-input').val().length < 5) {
                                          $('.cnpj-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 5 dígitos. Exemplo: 99999</p>');
                                          $('.cnpj-div .conta-input').focus();
                                          error = 1;
                                        }
                                      } else if($('.cnpj-div .banco-input').val() == 'Santander') {
                                        if($('.cnpj-div .conta-input').val().length < 8) {
                                          $('.cnpj-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                          $('.cnpj-div .conta-input').focus();
                                          error = 1;
                                        }
                                      }

                                      if(error == 1) {
                                        return false;
                                      } else {
                                        $(this).get(0).submit();
                                      }
                                    });
                                  });
                                </script>
                            </div>
                            <?= $this->Form->end(); ?>
                        </fieldset>
                        <fieldset>
                            <?= $this->Form->create($professor, ['class' => 'form-horizontal bordered-row', 'url' => ['controller' => 'Professores', 'action' => 'salvar_dados_bancarios_cpf', $comissao]]); ?>
                            <div class="col-md-12 cpf-div">
                                <div class="form-group col-md-6">
                                    <label for="favorecido" class="control-label">Nome Titular Conta</label>
                                    <?= $this->Form->input('favorecido', [
                                        'class'    => 'form-control validate[required]',
                                        'label'    => false
                                    ]) ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="cpf_favorecido" class="control-label">CPF Titular Conta</label>
                                    <?= $this->Form->input('cpf_favorecido', [
                                        'class'    => 'form-control validate[required] cpf-mask',
                                        'label'    => false
                                    ]) ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="tipo_conta" class="control-label">Tipo de conta</label>
                                    <?= $this->Form->input('tipo_conta', [
                                      'options' => [
                                        'Selecione'      => 'Selecione',
                                        'Conta Corrente' => 'Conta Corrente',
                                        'Conta Poupança' => 'Conta Poupança'
                                      ],
                                      'class'   => 'form-control',
                                      'label'   => false
                                    ]) ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="banco" class="control-label">Banco</label>
                                    <?= $this->Form->input('banco', [
                                      'options' => [
                                        'Banco do Brasil'   => 'Banco do Brasil',
                                        'Bradesco'          => 'Bradesco',
                                        'Caixa'             => 'Caixa Econ. Federal',
                                        'Itaú'              => 'Itaú',
                                        'Santander'         => 'Santander'
                                      ],
                                      'class'   => 'form-control banco-input',
                                      'label'   => false
                                    ]) ?>
                                    <p><strong>Não tem nenhum desses bancos? Nos chame no chat para saber sobre o Logcard!</strong></p>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="col-md-12 no-padding">
                                        <label for="agencia" class="control-label">Agência</label>
                                    </div>
                                    <div class="col-xs-8 no-padding">
                                        <?= $this->Form->input('agencia', [
                                          'class'       => 'form-control agencia-input',
                                          'onkeypress'  => 'return event.charCode >= 48 && event.charCode <= 57',
                                          'placeholder' => 'Agência',
                                          'label'       => false
                                        ]) ?>
                                    </div>
                                    <div class="col-xs-4">
                                        <?= $this->Form->input('agencia_dv', [
                                          'class'       => 'form-control agencia-dv-input',
                                          'placeholder' => 'Digito',
                                          'label'       => false,
                                          'maxlength'   => 1
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="col-md-12 no-padding">
                                        <label for="conta" class="control-label col-sm-2">Conta</label>
                                    </div>
                                    <div class="col-xs-8 no-padding">
                                        <?= $this->Form->input('conta', [
                                          'class'       => 'form-control conta-input',
                                          'onkeypress'  => 'return event.charCode >= 48 && event.charCode <= 57',
                                          'placeholder' => 'Conta',
                                          'label'       => false
                                        ]) ?>
                                    </div>
                                    <div class="col-xs-4">
                                        <?= $this->Form->input('conta_dv', [
                                          'class'       => 'form-control conta-dv-input',
                                          'placeholder' => 'Digito',
                                          'label'       => false,
                                          'maxlength'   => 1
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="banco_obs" class="control-label">Instruções de pagamento</label>
                                    <?= $this->Form->input('banco_obs', [
                                        'type'     => 'textarea',
                                        'class'    => 'form-control validate[optional]',
                                        'label'    => false,
                                        'placeholder'   =>
                                                'Caso haja alguma regra ou observação a ser feita em relação a depósitos/transferências na conta, descreva aqui...',
                                    ]) ?>
                                </div>
                                <div class="form-group col-md-12">
                                    <h5 class="text-center">Obs.: Informe todos os dados de forma correta. Não nos responsabilizamos por transtornos causados pelo preenchimento incorreto dos dados</h5>
                                    <div class="divider"></div>
                                </div>
                                <div class="text-center col-xs-12">
                                    <div class="col-sm-12 col-md-12">
                                        <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                                            ['class' => 'btn btn-alt btn-hoverrr btn-success form-bancarios-cpf', 'style' =>  'width: 9.5em;',
                                                'escape' => false]); ?>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                  $(document).ready(function() {
                                    $('.form-bancarios-cpf').click(function() {

                                      $('.error-bancario').remove();
                                      var error = 0;

                                      if($('.cpf-div .agencia-input').val().length < 4) {
                                        $('.cpf-div .agencia-input').parent().append('<p class="error-bancario" style="color: red">o número da agência deve ter no mínimo 4 dígitos. Exemplo: 9999</p>');
                                        $('.cpf-div .agencia-input').focus();
                                        error = 1;
                                      }
                                      if($('.cpf-div .conta-dv-input').val().length <= 0) {
                                        $('.cpf-div .conta-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da conta!</p>');
                                        $('.cpf-div .conta-dv-input').focus();
                                        error = 1;
                                      }

                                      if($('.cpf-div .banco-input').val() == 'Banco do Brasil') {
                                        if($('.cpf-div .agencia-dv-input').val().length <= 0) {
                                          $('.cpf-div .agencia-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da agencia!</p>');
                                          $('.cpf-div .agencia-dv-input').focus();
                                          error = 1;
                                        }
                                        if($('.cpf-div .conta-input').val().length < 8) {
                                          $('.cpf-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                          $('.cpf-div .conta-input').focus();
                                          error = 1;
                                        }
                                      } else if($('.cpf-div .banco-input').val() == 'Bradesco') {
                                        if($('.cpf-div .agencia-dv-input').val().length <= 0) {
                                          $('.cpf-div .agencia-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da agencia!</p>');
                                          $('.cpf-div .agencia-dv-input').focus();
                                          error = 1;
                                        }
                                        if($('.cpf-div .conta-input').val().length < 8) {
                                          $('.cpf-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                          $('.cpf-div .conta-input').focus();
                                          error = 1;
                                        }
                                      } else if($('.cpf-div .banco-input').val() == 'Caixa') {
                                        if($('.cpf-div .conta-input').val().length < 11) {
                                          $('.cpf-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 11 dígitos. Exemplo: XXX99999999 (X: Operação)</p>');
                                          $('.cpf-div .conta-input').focus();
                                          error = 1;
                                        }
                                      } else if($('.cpf-div .banco-input').val() == 'Itaú') {
                                        if($('.cpf-div .conta-input').val().length < 5) {
                                          $('.cpf-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 5 dígitos. Exemplo: 99999</p>');
                                          $('.cpf-div .conta-input').focus();
                                          error = 1;
                                        }
                                      } else if($('.cpf-div .banco-input').val() == 'Santander') {
                                        if($('.cpf-div .conta-input').val().length < 8) {
                                          $('.cpf-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                          $('.cpf-div .conta-input').focus();
                                          error = 1;
                                        }
                                      }

                                      if(error == 1) {
                                        return false;
                                      } else {
                                        $(this).get(0).submit();
                                      }
                                    });
                                  });
                                </script>
                            </div>
                            <?= $this->Form->end() ?>
                        </fieldset>
                    <?php } else if($professor->card_status == 1) { ?>
                        <?= $this->Form->create($professor, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
                        <fieldset>
                            <h3 class="box-header">Dados do cartão <span class="pull-right"><a href="http://blog.logfitness.com.br/conheca-o-logcard" target="_blank"><button class="btn bg-blue" type="button">Conheça o Logcard</button></a></span></h3>

                            <?php if($professor->name != null) { ?>
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('name', 'Nome Completo') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('name', [
                                        'label'         => false,
                                        'placeholder'   => 'Nome completo',
                                        'class' => 'form-control validate[required]',
                                        'disabled' => true
                                    ]) ?>
                                </div>
                            </div>
                            <?php } else { ?>
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('name', 'Nome Completo') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('name', [
                                        'label'         => false,
                                        'placeholder'   => 'Nome completo',
                                        'class' => 'form-control validate[required]'
                                    ]) ?>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if($professor->cpf != null) { ?>
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('cpf', 'CPF') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('cpf', [
                                        'label'         => false,
                                        'placeholder'   => 'CPF',
                                        'class' => 'form-control validate[required] cpf',
                                        'disabled' => true
                                    ]) ?>
                                </div>
                            </div>
                            <?php } else { ?>
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('cpf', 'CPF') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('cpf', [
                                        'label'         => false,
                                        'placeholder'   => 'CPF',
                                        'class' => 'form-control validate[required] cpf'
                                    ]) ?>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if($professor->card_birth != null) { ?>
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('card_birth', 'Data de nascimento') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('card_birth', [
                                        'type'      => 'text',
                                        'label'         => false,
                                        'placeholder'   => 'Data de nascimento',
                                        'class' => 'form-control validate[required, custom[brDate]]',
                                        'disabled' => true
                                    ]) ?>
                                </div>
                            </div>
                            <?php } else { ?>
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('card_birth', 'Data de nascimento') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('card_birth', [
                                        'type'      => 'text',
                                        'label'         => false,
                                        'placeholder'   => 'Data de nascimento',
                                        'class' => 'form-control validate[required, custom[brDate]]'
                                    ]) ?>
                                </div>
                            </div>
                            <?php } ?>

                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('card_gender', 'Gênero') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('card_gender', [
                                        'label'         => false,
                                        'placeholder'   => 'Gênero',
                                        'options'       => ['Feminino' => 'Feminino', 'Masculino' => 'Masculino'],
                                        'class' => 'form-control validate[required]'
                                    ]) ?>
                                </div>
                            </div>

                            <?php if($professor->card_mother != null) { ?>
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('card_mother', 'Nome da mãe') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('card_mother', [
                                        'label'         => false,
                                        'placeholder'   => 'Nome da mãe',
                                        'class' => 'form-control validate[required]',
                                        'disabled' => true
                                    ]) ?>
                                </div>
                            </div>
                            <?php } else { ?>
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('card_mother', 'Nome da mãe') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('card_mother', [
                                        'label'         => false,
                                        'placeholder'   => 'Nome da mãe',
                                        'class' => 'form-control validate[required]'
                                    ]) ?>
                                </div>
                            </div>
                            <?php } ?>

                            
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('cep') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('cep', [
                                        'label'         => false,
                                        'placeholder'   => 'CEP',
                                        'class' => 'form-control validate[required] cep'
                                    ]) ?>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('address') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('address', [
                                        'label'         => false,
                                        'placeholder'   => 'Endereço',
                                        'class' => 'form-control validate[required]'
                                    ]) ?>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('number') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('number', [
                                        'label'         => false,
                                        'placeholder'   => 'Número',
                                        'class' => 'form-control validate[required]'
                                    ]) ?>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('complement') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('complement', [
                                        'label'         => false,
                                        'placeholder'   => 'Complemento',
                                        'class' => 'form-control validate[opitional]'
                                    ]) ?>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('area') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('area', [
                                        'label'         => false,
                                        'placeholder'   => 'Bairro',
                                        'class' => 'form-control validate[required]'
                                    ]) ?>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('state_id') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('state_id', [
                                        'options' => $states,
                                        'value'   => $professor->city->state->id,
                                        'id'      => 'states_academias',
                                        'empty'   => 'Selecione um estado',
                                        'label'   => false,
                                        'class'   => 'form-control validate[required]'
                                    ]) ?>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('city_id') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('city_id', [
                                        'id'      => 'city',
                                        'options' => $cities,
                                        'value'   => $professor->city->id,
                                        'empty'   => 'Selecione uma cidade',
                                        'label'   => false,
                                        'class'   => 'form-control validate[required]'
                                    ]) ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('card_id', 'ID do Cartão') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('card_id', [
                                        'label'         => false,
                                        'type'  => 'text',
                                        'class' => 'form-control validate[required]',
                                        'readonly' => true
                                    ]) ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12 col-md-3 control-label label-cartao">
                                    <?= $this->Form->label('card_number', 'Número do Cartão') ?>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <?= $this->Form->input('card_number', [
                                        'label'         => false,
                                        'class' => 'form-control validate[required]',
                                        'readonly' => true
                                    ]) ?>
                                </div>
                            </div>

                            <div class="divider"></div>
                            <h5 class="box-header text-center">Obs.: Informe todos os dados de forma correta. Não nos responsabilizamos por transtornos causados pelo preenchimento incorreto dos dados. <br>
                            Caso algum dado já preenchido esteja incorreto, entre em contato pelo nosso chat para que atualizemos seu cadastro.</h5>
                            <div class="divider"></div>

                            <div class="text-center col-xs-12">
                              <div class="col-sm-12 col-md-12">
                                <?= $this->Form->button('<i class="fa fa-check-square" aria-hidden="true"></i> <span> '.__('Submit').'</span>',
                                    ['class' => 'btn btn-alt btn-hoverrr btn-success btn-meus-dados',
                                        'escape' => false]); ?>
                                <?= $this->Html->link('<i class="fa fa-credit-card" aria-hidden="true"></i> Ativar cartão','https://meu.brasilprepagos.com.br/strategybox/login/activate_card',[
                                        'escape'    =>  false,
                                        'class'     =>  'btn btn-default btn-meus-dados',
                                        'target'    =>  '_blank',
                                    ]); ?>
                                <?= $this->Html->link('<i class="fa fa-usd" aria-hidden="true"></i> Consultar saldo','https://meu.brasilprepagos.com.br/strategybox/login',[
                                        'escape'    =>  false,
                                        'class'     =>  'btn btn-default btn-meus-dados',
                                        'target'    =>  '_blank',
                                    ]); ?>
                              </div>
                            </div>
                        </fieldset>
                        <?= $this->Form->end() ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/spinner/spinner.js"></script>
<script type="text/javascript">
    /* jQuery UI Spinner */

    $(function() { "use strict";
        $(".spinner-input").spinner();
    });
</script>


<script type="text/javascript">
    /**
     * CARREGAR CIDADES QUE POSSUAM ACADEMIA COM BASE NO ESTADO SELECIONADO
     */
    $('#states_academias').on('change', function(){
        $('.loading').show(1);
        var state_id = $(this).val();
        $.get(WEBROOT_URL + '/cidades/' + state_id + '/1',
            function(data){
                $('#city').html(data);
                $('.loading').hide(1);
            });
    });

     /**
     * CEP - VIACEP
     */
    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#address").val("");
        $("#area").val("");
        $("#city").val("");
        $("#states_academias").val("");
        //$("#ibge").val("");
    }

    //Quando o campo cep perde o foco.
    $("#cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');
        console.log(cep);

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#address").val("...");
                $("#area").val("...");
                $("#city").val("...");
                $("#states_academias").val("...");
                $("#ibge").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        console.log(dados);
                        //Atualiza os campos com os valores da consulta.
                        $("#address").val(dados.logradouro);
                        $("#area").val(dados.bairro);

                        var uf = '';

                        switch (dados.uf){
                            case 'AC' : uf = 1; break;
                            case 'AL' : uf = 2; break;
                            case 'AM' : uf = 3; break;
                            case 'AP' : uf = 4; break;
                            case 'BA' : uf = 5; break;
                            case 'CE' : uf = 6; break;
                            case 'DF' : uf = 7; break;
                            case 'ES' : uf = 8; break;
                            case 'GO' : uf = 9; break;
                            case 'MA' : uf = 10; break;
                            case 'MG' : uf = 11; break;
                            case 'MS' : uf = 12; break;
                            case 'MT' : uf = 13; break;
                            case 'PA' : uf = 14; break;
                            case 'PB' : uf = 15; break;
                            case 'PE' : uf = 16; break;
                            case 'PI' : uf = 17; break;
                            case 'PR' : uf = 18; break;
                            case 'RJ' : uf = 19; break;
                            case 'RN' : uf = 20; break;
                            case 'RO' : uf = 21; break;
                            case 'RR' : uf = 22; break;
                            case 'RS' : uf = 23; break;
                            case 'SC' : uf = 24; break;
                            case 'SE' : uf = 25; break;
                            case 'SP' : uf = 26; break;
                            case 'TO' : uf = 27; break;
                            default : uf = '';
                        }

                        $("#states_academias").val(uf);

                        var state_id = $('#states_academias').val();
                        $.get(WEBROOT_URL + '/cidades/' + state_id,
                            function(data_options){
                                $('#city').html(data_options);
                            });

                        setTimeout(function(){
                            var $dd = $('#city');
                            var $options = $('option', $dd);
                            $options.each(function() {
                                if ($(this).text() == dados.localidade) {
                                    $(this).attr('selected', true);
                                }
                            });
                        }, 700);

                        //$("#ibge").val(dados.ibge);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });

</script>

