<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>


<div class="categorias form">
    <?= $this->Form->create(null, [
        'id'        => 'form-indicar-academia',
        'role'      => 'form',
        'default'   => false,
        'type'      => 'file',
        'class' => 'form-horizontal bordered-row'
    ]) ?>

    <fieldset>
        <legend><?= __('Indicação de Academia') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('Nome') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'div'   => false,
                    'placeholder' => 'Nome completo da academia',
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('Telefone') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('telephone', [
                    'label' => false,
                    'div'   => false,
                    'placeholder' => '(00) 00000-0000',
                    'class' => 'validate[required] form-control phone'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('Responsável') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('responsavel', [
                    'div'   => false,
                    'label' => false,
                    'placeholder' => 'Nome do responsável da academia',
                    'class' => 'validate[required] form-control'
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('Estado') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('uf_id', [
                    'id'            => 'states',
                    'div'           => false,
                    'label'         => false,
                    'options'       => $states,
                    'empty'         => 'Selecione um estado',
                    'placeholder'   => 'Estado',
                    'class'         => 'validate[required] form-control margin-bottom-30'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('Cidade') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('city_id', [
                    'id'            => 'city',
                    'div'           => false,
                    'label'         => false,
                    'empty'         => 'Selecione uma cidade',
                    'placeholder'   => 'Cidade',
                    'class'         => 'validate[required] form-control margin-bottom-30'
                ]) ?>
            </div>
        </div>

        <legend></legend>
        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('ENVIAR INDICAÇÃO DE ACADEMIA').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


