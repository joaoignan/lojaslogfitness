<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<style>
    @media all and (max-width: 450px){
        .foto-mobile{
            text-align: center;
        }
    }
</style>


<section class="content">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-header">Dados cadastrais</h3>
            </div>
            <div class="box-body">
                <div class="professores form">
                    <?= $this->Form->create($professor, ['id' => 'form-edit-prof', 'class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
                    <fieldset>

                        <h4 class="font-bold font-italic"><span class="fa fa-camera" aria-hidden="true"></span> Foto</h4>

                        <script type="text/javascript">
                            $(window).load(function() {
                                function readURL(input) {
                                    if (input.files && input.files[0]) {
                                        var reader = new FileReader();
                                        
                                        reader.onload = function (e) {
                                            $('.image_preview').attr('src', e.target.result);

                                            $('.image_preview').Jcrop({
                                                boxHeight: 500,
                                                boxWidth: 500,
                                                setSelect: [0, 0, 0, 0],
                                                allowSelect: false,
                                                minSize: [20, 85]
                                            }, function () {

                                                var jcrop_api = this;

                                                $(".jcrop-box").attr('type', 'button');

                                                $('.image_preview').Jcrop('animateTo',[0,0,4000,4000]);

                                                jcrop_api.ui.selection.element.on('cropmove',function(e,s,c){
                                                    $('#cropx').val(c.x);
                                                    $('#cropy').val(c.y);
                                                    $('#cropw').val(c.w);
                                                    $('#croph').val(c.h);
                                                    $('#cropwidth').val($('.jcrop-box').width());
                                                    $('#cropheight').val($('.jcrop-box').height());
                                                });
                                            });
                                        }
                                        
                                        reader.readAsDataURL(input.files[0]);
                                    }
                                }
                                
                                $("#image_upload").change(function(){
                                    readURL(this);
                                });
                            });
                        </script>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('imagem', 'Imagem de perfil') ?>
                            </div>
                            <div class="col-sm-7">
                            <?= $this->Form->input('imagem', [
                                'id'    => 'image_upload',
                                'type'  => 'file',
                                'label' => false,
                                'class' => 'form-control validate[optional]'
                            ]) ?>
                            <p><strong>Selecione um arquivo de imagem entre 85x85 e 4000x4000 pixels</strong></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('image_preview', 'Prévia do Logotipo') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Html->image('professores/'.$professor->image, [
                                    'class'    => 'image_preview'
                                ])?>
                            </div>
                        </div>

                        <input type="hidden" name="cropx" id="cropx" value="0" />
                        <input type="hidden" name="cropy" id="cropy" value="0" />
                        <input type="hidden" name="cropw" id="cropw" value="0" />
                        <input type="hidden" name="croph" id="croph" value="0" />
                        <input type="hidden" name="cropwidth" id="cropwidth" value="0" />
                        <input type="hidden" name="cropheight" id="cropheight" value="0" />

                        <div class="form-group text-center">
                            <?= $this->Form->button('<i class="fa fa-check-square-o" aria-hidden="true"></i><span> '.__('Atualizar Dados Cadastrais ').'</span>',
                            ['class' => 'btn btn-alt btn-hoverrr btn-success',
                                'escape' => false]); ?>
                        </div>
                        
                        <h4 class="font-bold font-italic"><span class="fa fa-user"></span> Informações Pessoais</h4>
                        <br class="clear">
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('name') ?>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <?= $this->Form->input('name', [
                                    'label' => false,
                                    'class' => 'form-control validate[required]'
                                ]) ?>
                                <?= $this->Form->input('img_name', [
                                    'label' => $professor->shortname,
                                    'type'  => 'hidden',
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('cpf') ?>
                            </div>
                            <?php if($professor->cpf != null) { ?>
                                <div class="col-sm-6 col-md-4">
                                    <?= $this->Form->input('cpf', [
                                        'label' => false,
                                        'disabled'  => true,
                                        'readonly'  => true,
                                        'class' => 'form-control validate[required,custom[cpf]] cpf'
                                    ]) ?>
                                </div>
                            <?php } else { ?>
                                <div class="col-sm-6 col-md-4">
                                    <?= $this->Form->input('cpf', [
                                        'label' => false,
                                        'disabled'  => false,
                                        'readonly'  => false,
                                        'class' => 'form-control validate[required,custom[cpf]] cpf'
                                    ]) ?>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('rg') ?>
                            </div>
                            <?php if($professor->rg != null) { ?>
                                <div class="col-sm-6 col-md-4">
                                    <?= $this->Form->input('rg', [
                                        'label' => false,
                                        'disabled'  => true,
                                        'readonly'  => true,
                                        'class' => 'form-control validate[required]'
                                    ]) ?>
                                </div>
                            <?php } else { ?>
                                <div class="col-sm-6 col-md-4">
                                    <?= $this->Form->input('rg', [
                                        'label' => false,
                                        'disabled'  => false,
                                        'readonly'  => false,
                                        'class' => 'form-control validate[required]'
                                    ]) ?>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('CREF / CRM') ?>
                            </div>
                            <?php if($professor->cref != null) { ?>
                                <div class="col-sm-6 col-md-4">
                                    <?= $this->Form->input('cref', [
                                        'label' => false,
                                        'disabled'  => true,
                                        'readonly'  => true,
                                        'class' => 'form-control validate[required]'
                                    ]) ?>
                                </div>
                            <?php } else { ?>
                                <div class="col-sm-6 col-md-4">
                                    <?= $this->Form->input('cref', [
                                        'label' => false,
                                        'disabled'  => false,
                                        'readonly'  => false,
                                        'class' => 'form-control validate[required]'
                                    ]) ?>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('facebook_url', 'Link do Facebook') ?>
                            </div>
                            <div class="col-sm-7">
                                <div class="input-group">
                                <span class="input-group-addon">facebook.com.br/</span>
                                <?= $this->Form->input('facebook_url', [
                                    'id'                => 'facebook_url',
                                    'placeholder'       => 'Complemento da URL facebook.com.br/',
                                    'label'             => false,
                                    'class'             => 'form-control validate[optional]'
                                ]) ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('função') ?>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <?= $this->Form->input('funcao', [
                                    'label'   => false,
                                    'options' => [
                                        'Personal Trainer' => 'Personal Trainer',
                                        'Nutricionista'    => 'Nutricionista',
                                        'Recepcionista'    => 'Recepcionista',
                                        'Instrutor'        => 'Instrutor',
                                        'Atendente'        => 'Atendente'
                                    ],
                                    'class'   => 'form-control validate[required]'
                                ]) ?>
                            </div>
                        </div>

                        <div class="divider"></div>
                        <h4 class="font-bold font-italic"><span class="fa fa-phone"></span> Contato</h4>
                        <br class="clear">

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('phone') ?>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <?= $this->Form->input('phone', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[required] phone'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('mobile', 'Celular/WhatsApp') ?>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <?= $this->Form->input('mobile', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[optional] phone'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('email') ?>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <?= $this->Form->input('email', [
                                    'label' => false,
                                    'class' => 'form-control validate[required, custom[email]]'
                                ]) ?>
                            </div>
                        </div>

                        <div class="divider"></div>
                        <h4 class="font-bold font-italic"><span class="fa fa-map-marker"></span> Endereço</h4>
                        <br class="clear">

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('cep') ?>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <?= $this->Form->input('cep', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[required] cep'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('address') ?>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <?= $this->Form->input('address', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[required]'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('number') ?>
                            </div>
                            <div class="col-sm-2 col-md-2">
                                <?= $this->Form->input('number', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[optional]'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('complement') ?>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <?= $this->Form->input('complement', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[optional]'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('area') ?>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <?= $this->Form->input('area', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[required]'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('state_id') ?>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <?= $this->Form->input('state_id', [
                                    'options'       => $states,
                                    'id'            => 'states',
                                    'value'         => $professor->city->state->id,
                                    'empty' => 'Selecione um estado',
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'label' => false,
                                    'class' => 'form-control chosen-select validate[required]'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('city_id') ?>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <?= $this->Form->input('city_id', [
                                    'id'            => 'city',
                                    'value'         => $professor->city->id,
                                    'options' => $cities,
                                    'empty' => 'Selecione uma cidade',
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'label' => false,
                                    'class' => 'form-control chosen-select validate[required]'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group text-center">
                           <?= $this->Form->button('<i class="fa fa-check-square-o" aria-hidden="true"></i><span> '.__('Atualizar Dados Cadastrais ').'</span>',
                            ['class' => 'btn btn-alt btn-hoverrr btn-success',
                                'escape' => false]); ?>
                        </div>
                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

