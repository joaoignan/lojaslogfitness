<section class="content">
	<div class="row">
        <div class="col-xs-12">
          <!-- Profile Image -->
			<div class="box box-primary">
				<div class="box-body box-profile">
					<div class="row"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
					 					['controller' => 'Professores', 'action' => 'admin_alunos'],
					 					['escape'	=>	false,
					  					 'class'	=>	'btn pull-right']) ?>
					</div>
					<?= $this->Html->image('clientes/'.$cliente->image, ['alt' => $cliente->name, 'class' => 'profile-user-img img-responsive img-circle']) ?>
					<h3 class="profile-username text-center"><?= h($cliente->name) ?></h3>
					<p class="text-muted text-center">Código: <?= $this->Number->format($cliente->id) ?></p>
					<p class="text-muted text-center">Usuário Log desde: <?= h($cliente->created) ?></p>
					<div class="col-md-6">
						<p class="text-center"><i class="fa fa-user" aria-hidden="true"></i> Pessoal</p>
						<ul class="list-group list-group-unbordered">
							<li class="list-group-item">
								<b>Gênero: <?= h($cliente->genre) ?></b> <a class="pull-right"></a>
							</li>
							<li class="list-group-item">
								<b>CPF: <?= h($cliente->cpf) ?></b> <a class="pull-right"></a>
							</li>
							<li class="list-group-item">
								<b>RG: <?= h($cliente->rg) ?></b> <a class="pull-right"></a>
							</li>
							<li class="list-group-item">
								<b>Telefone: <?= h($cliente->telephone) ?></b> <a class="pull-right"></a>
							</li>
							<li class="list-group-item">
								<b>Celular: <?= h($cliente->mobile) ?></b> <a class="pull-right"></a>
							</li>
							<li class="list-group-item">
								<b>Email: <?= h($cliente->email) ?></b> <a class="pull-right"></a>
							</li>
							<li class="list-group-item">
								<b>Aniversário: <?= h($cliente->birth) ?></b> <a class="pull-right"></a>
							</li>
						</ul>
					</div>
					<div class="col-md-6">
						<p class="text-center"><i class="fa fa-map-marker" aria-hidden="true"></i> Moradia</p>
						<ul class="list-group list-group-unbordered">
							<li class="list-group-item">
								<b>CEP: <?= h($cliente->cep) ?></b> <a class="pull-right"></a>
							</li>
							<li class="list-group-item">
								<b>Endereço: <?= h($cliente->address) ?></b> <a class="pull-right"></a>
							</li>
							<li class="list-group-item">
								<b>Número: <?= h($cliente->number) ?></b> <a class="pull-right"></a>
							</li>
							<li class="list-group-item">
								<b>Complemento: <?= h($cliente->complement) ?></b> <a class="pull-right"></a>
							</li>
							<li class="list-group-item">
								<b>Bairro: <?= h($cliente->area) ?></b> <a class="pull-right"></a>
							</li>
							<li class="list-group-item">
								<b>Cidade: <?= h($cliente->city->name) ?></b> <a class="pull-right"></a>
							</li>
						</ul>
					</div>
				</div>
			<!-- /.box-body -->
			</div>
		</div>
	</div>
</section>