<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<section class="content">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-body">
                <div class="users form">
                    <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
                    <fieldset>
                        <h3 class="box-header">Alterar senha</h3>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('old_password') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('old_password', [
                                    'type'  => 'password',
                                    'label' => false,
                                    'class' => 'form-control validate[required]'
                                ]) ?>
                            </div>
                        </div>

                        <div class="divider"></div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('password', __('New Password')) ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('password', [
                                    'label' => false,
                                    'value' => '',
                                    'class' => 'form-control validate[required,minSize[7]]'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('confirm_new_password') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('confirm_new_password', [
                                    'type'  => 'password',
                                    'label' => false,
                                    'class' => 'form-control validate[required,minSize[7],equals[password]]'
                                ]) ?>
                            </div>
                        </div>


                        <div class="divider"></div>

                        <div class="text-center col-xs-12">
                            <div class="col-xs-12 col-md-8">
                            <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                                '<i class="glyph-icon icon-arrow-right"></i>',
                                ['class' => 'btn btn-alt btn-hover btn-success',
                                    'escape' => false]); ?>
                            </div>
                        </div>
                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>

        </div>
    </div>
</section>