<section class="content">
    <div class="row">
        <div class="col-xs-12">
          <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <div class="row"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
                                        ['controller' => 'Professores', 'action' => 'listar_minhas_academias'],
                                        ['escape'   =>  false,
                                         'class'    =>  'btn pull-right']) ?>
                    </div>
                    <div class="col-md-6">
                        <p class="text-center"><i class="fa fa-user" aria-hidden="true"></i> Pessoal</p>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Nome: <?= h($prof_academia->academia->name) ?></b> <a class="pull-right"></a>
                            </li>
                            <li class="list-group-item">
                                <b>Nome Curto: <?= h($prof_academia->academia->shortname) ?></b> <a class="pull-right"></a>
                            </li>
                            <li class="list-group-item">
                                <b>CNPJ: <?= h($prof_academia->academia->cnpj) ?></b> <a class="pull-right"></a>
                            </li>
                            <li class="list-group-item">
                                <b>Inscrição estadual: <?= h($prof_academia->academia->ie) ?></b> <a class="pull-right"></a>
                            </li>
                            <li class="list-group-item">
                                <b>Contato: <?= h($prof_academia->academia->contact) ?></b> <a class="pull-right"></a>
                            </li>
                            <li class="list-group-item">
                                <b>Telefone: <?= h($prof_academia->academia->phone) ?></b> <a class="pull-right"></a>
                            </li>
                            <li class="list-group-item">
                                <b>Celular/Whatsapp: <?= h($prof_academia->academia->mobile) ?></b> <a class="pull-right"></a>
                            </li>
                            <li class="list-group-item">
                                <b>E-mail: <?= h($prof_academia->academia->email) ?></b> <a class="pull-right"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <p class="text-center"><i class="fa fa-map-marker" aria-hidden="true"></i> Localização</p>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Endereço: <?= h($prof_academia->academia->address_acad) ?></b> <a class="pull-right"></a>
                            </li>
                            <li class="list-group-item">
                                <b>Complemento: <?= h($prof_academia->academia->complement_acad) ?></b> <a class="pull-right"></a>
                            </li>
                            <li class="list-group-item">
                                <b>Bairro: <?= h($prof_academia->academia->area_acad) ?></b> <a class="pull-right"></a>
                            </li>
                            <li class="list-group-item">
                                <b>Cidade: <?= h($cidade_acad) ?> - <?= h($estado_acad) ?></b> <a class="pull-right"></a>
                            </li>
                            <li class="list-group-item">
                                <b>CEP: <?= h($prof_academia->academia->cep_acad) ?></b> <a class="pull-right"></a>
                            </li>
                            <li class="list-group-item">
                                <b><?= $this->Html->link('Ver loja',
                                    '/'.$prof_academia->academia->slug,
                                ['class' => 'btn btn-success', 'escape' =>false]); ?></b>
                            </li>
                        </ul>
                    </div>     
                </div>    
            </div>
        </div>
    </div>
</section>

<?php /*
<div class="row">
    <div class="column large-12">
        <h4 class="font-bold"><?= __('Related Clientes') ?></h4>
        <?php if (!empty($academia->clientes)): ?>
            <table class="table table-hover">
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Name') ?></th>
                    <th><?= __('Genre') ?></th>
                    <th><?= __('Cpf') ?></th>
                    <th><?= __('Rg') ?></th>
                    <th><?= __('Telephone') ?></th>
                    <th><?= __('Mobile') ?></th>
                    <th><?= __('Birth') ?></th>
                    <th><?= __('Email') ?></th>
                    <th><?= __('Cep') ?></th>
                    <th><?= __('Address') ?></th>
                    <th><?= __('Number') ?></th>
                    <th><?= __('Complement') ?></th>
                    <th><?= __('Area') ?></th>
                    <th><?= __('City Id') ?></th>
                    <th><?= __('Password') ?></th>
                    <th><?= __('Academia Id') ?></th>
                    <th><?= __('Status Id') ?></th>
                    <th><?= __('Created') ?></th>
                    <th><?= __('Modified') ?></th>
                    <th class="actions"></th>
                </tr>
                <?php foreach ($academia->clientes as $clientes): ?>
                    <tr>
                        <td><?= h($clientes->id) ?></td>
                        <td><?= h($clientes->name) ?></td>
                        <td><?= h($clientes->genre) ?></td>
                        <td><?= h($clientes->cpf) ?></td>
                        <td><?= h($clientes->rg) ?></td>
                        <td><?= h($clientes->telephone) ?></td>
                        <td><?= h($clientes->mobile) ?></td>
                        <td><?= h($clientes->birth) ?></td>
                        <td><?= h($clientes->email) ?></td>
                        <td><?= h($clientes->cep) ?></td>
                        <td><?= h($clientes->address) ?></td>
                        <td><?= h($clientes->number) ?></td>
                        <td><?= h($clientes->complement) ?></td>
                        <td><?= h($clientes->area) ?></td>
                        <td><?= h($clientes->city_id) ?></td>
                        <td><?= h($clientes->password) ?></td>
                        <td><?= h($clientes->academia_id) ?></td>
                        <td><?= h($clientes->status_id) ?></td>
                        <td><?= h($clientes->created) ?></td>
                        <td><?= h($clientes->modified) ?></td>

                        <td class="actions">
                            <div class="btn-group">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                                    <i class="glyph-icon icon-navicon"></i>
                                    <span class="sr-only"><?= __('Actions'); ?></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu" style="left: -110px;">
                                    <li>
                                        <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                            ['Clientes', 'action' => 'view', $clientes->id],
                                            ['escape' => false]) ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                            ['controller' => 'Clientes', 'action' => 'edit', $clientes->id],
                                            ['escape' => false]) ?>
                                    </li>
                                    <li>
                                        <?= $this->Form->postLink('<i class="glyph-icon icon-trash-o"></i>  '.__('Delete'),
                                            ['controller' => 'Clientes', 'action' => 'delete', $clientes->id],
                                            ['confirm' => __('Are you sure you want to delete # {0}?', $clientes->id), 'escape' => false]) ?>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>

                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>
 */ ?>