<style>
    @media all and (max-width: 450px){
    .no-mobile{
      display: none;
    }
    .font-header{
        font-size: 12px
    }
    .font-body{
        font-size: 11px
    }
  }
  @media all and (max-width: 1023px){
    .no-small{
      display: none;
    }
  }
</style>

<section class="content">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Minhas academias</h3>
            </div>
            <div class="box-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><?= $this->Paginator->sort('name') ?></th>
                        <th class="no-small"><?= $this->Paginator->sort('cidade') ?></th>
                        <th class="no-mobile"><?= $this->Paginator->sort('telefone') ?></th>
                        <th class="no-mobile"><?= $this->Paginator->sort('mobile') ?></th>
                        <th class="no-small"><?= $this->Paginator->sort('E-mail') ?></th>
                        <th class="no-small">Ver loja</th>
                        <th class="actions"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($prof_academia as $prof_academias): ?>
                        <tr>
                            <td><?= h(strtoupper($prof_academias->academia->shortname)) ?></td>
                            <td class="no-small"><?= h($prof_academias->academia->city->name.'/'.$prof_academias->academia->city->uf) ?></td>
                            <td class="no-mobile"><?= h($prof_academias->academia->phone) ?></td>
                            <td class="no-mobile"><?= h($prof_academias->academia->mobile) ?></td>
                            <td class="no-small"><?= h($prof_academias->academia->email) ?></td>
                            <td class="no-small">
                                <?= $this->Html->link('Ver loja',
                                    '/'.$prof_academias->academia->slug,
                                ['class' => 'btn btn-success', 'escape' =>false]); ?>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <?php
                                    $aceite = '';
                                    if($prof_academias->status_id == 3 ){
                                        $aceite = 'style="background-color: #ea9143"';?>
                                    <?php }?>
                                    <button class="btn btn-info dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false" <?= $aceite ?>>
                                        <i class="fa fa-bars"></i>
                                        <span class="sr-only"><?=__('Actions');?></span>
                                    </button>
                                    <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                                        <li>
                                            <?= $this->Form->postLink('<i class="fa fa-power-off"></i>  '.__('Desligar'),
                                                ['controller' => 'Professores', 'action' => 'admin_edit_professores', $prof_academias->academia->id],
                                                ['confirm' => __('Tem certeza que deseja desligar-se da {0}?', $prof_academias->academia->shortname), 'escape' => false]) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-search"></i>  '.__('View'),
                                                ['controller' => 'Professores', 'action' => 'admin_view_academias', $prof_academias->academia->id],
                                                ['escape' => false]) ?>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>


