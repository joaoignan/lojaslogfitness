<style type="text/css">
    .profile-user-img {
        margin: 0 auto;
        width: 100px;
        height: 100px;
        padding: 3px;
        border: 3px solid #d2d6de;
    }
    .footer-total {
        display: none;
    }
    .title-page {
        float: left;
        color: #5089cf;
        margin-top: 20px;    }
    .voltar-page {
        float: right;
        margin-top: 30px;
        margin-right: 15px;
        font-size: 18px;
    }
    .btn-att-dados{
        border: 2px solid;
        border-color: #5087c7;
        background-color: white;
        font-family: nexa_boldregular;
        color: #5087c7;
    }
    @media all and (max-width: 1279px) {
        .minha-conta-container {
            padding-right: 50px;
            padding-left: 35px;
            padding-top: 50px;
        }
    }
    @media all and (max-width: 430px) {
        .voltar-page {
            display: none;
            float: right;
            margin-top: 30px;
            margin-right: 15px;
            font-size: 18px;
        }
        .btn-cupom{
            width: 100%;
        }
        .complemento{
            text-align: center;
        }
        .title-page span{
            display: none;
        }
    }
</style>

<div class="container minha-conta-container">
    <div class="col-xs-12 col-sm-12">
        <h4 class="title-page">&nbsp;<span class="fa fa-money fa-2x" style="color: #5089cf;"></span> Dados faturamento</h4>
    </div>
    <br class="clear">

    <div class="minha-conta">

        <div class="col-sm-12">
            <?= $this->Form->create($cliente, [
                'id'        => 'form-completar-cadastro-cliente',
                'class'     => 'verificar-dados-incompletos',
                'role'      => 'form',
                'default'   => false,
                'type'      => 'file'
            ]) ?>

            <script>
                var xyz = <?= $cliente->id; ?>;
            </script>
            <br class="clear">
            <hr>
            <p class="complemento">Manter seus dados atualizados é vantajoso para nós e para você!</p>
            <hr>
            <?= $this->Flash->render(); ?>

            <h5 class="font-bold font-italic"><i class="fa fa-user"></i> Dados pessoais</h5>
            
            <div class="form-group row">
                <div class="col-sm-2 text-center">
                    <?= $this->Html->image('clientes/'.$cliente->image, [
                        'id'    => 'image_preview',
                    ])?>
                </div>

                <div class="col-sm-8" style="padding-top: 20px;">
                    <label for="csv">Selecionar foto</label>
                    <?= $this->Form->input('imagem', [
                        'id'    => 'image_upload',
                        'type'  => 'file',
                        'label' => false,
                        'class' => 'form-control validate[optional]'
                    ]) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-5">
                    <?= $this->Form->input('name', [
                        'div'           => false,
                        'label'         => 'Nome Completo*',
                        'placeholder'   => 'Nome Completo',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
                
                <div class="col-sm-5">
                    <?= $this->Form->input('email', [
                        'div'           => false,
                        'label'         => 'E-mail*',
                        'placeholder'   => 'E-mail',
                        'class'         => 'validate[required,custom[email]] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-2">
                    <?= $this->Form->input('genre', [
                        'div'           => false,
                        'label'         => 'Gênero*',
                        'options'       => ['F' => 'Feminino', 'M' => 'Masculino', 'O' => 'Outros'],
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
                <div class="col-sm-3">
                    <?= $this->Form->input('cpf', [
                        'div'           => false,
                        'readonly'      => true,
                        'disabled'      => true,
                        'label'         => 'CPF*',
                        'placeholder'   => 'CPF',
                        'class'         => 'validate[required,custom[cpf]] form-control cpf ck-c-cpf',
                    ]) ?>
                </div>

                <div class="col-sm-2">
                    <?= $this->Form->input('rg', [
                        'div'           => false,
                        'readonly'      => true,
                        'disabled'      => true,
                        'label'         => 'RG*',
                        'placeholder'   => 'RG',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
                <div class="col-sm-3">
                    <?= $this->Form->input('birth', [
                        'value'         => !empty($cliente->birth) ? date_format($cliente->birth, 'd/m/Y') : '',
                        'type'          => 'text',
                        'div'           => false,
                        'label'         => 'Nascimento*',
                        'placeholder'   => 'Nascimento',
                        'class'         => 'validate[required, custom[brDate]] form-control br_date',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-3">
                    <?= $this->Form->input('telephone', [
                        'div'           => false,
                        'label'         => 'Telefone 1*',
                        'placeholder'   => 'Telefone 1',
                        'class'         => 'validate[required] form-control phone-mask',
                    ]) ?>
                </div>

                <div class="col-sm-3">
                    <?= $this->Form->input('mobile', [
                        'div'           => false,
                        'label'         => 'Telefone 2',
                        'placeholder'   => 'Telefone 2',
                        'class'         => 'validate[optional] form-control  phone-mask',
                    ]) ?>
                </div>       
            </div>

            <!-- <div class="form-group row">
                <div class="col-sm-11">
                    
                    <?= $this->Form->checkbox('email_notificaca0', [
                            'id'            => 'emailnotificacao',
                            'checked'       => $emailnotificacao,
                            'class'         => 'custom-checkbox',
                        ]);
                    
                    ?>

                    <p> Desejo receber e-mails com atualizações, pedidos e novidades!!</p> 

                </div>
            </div> -->



            <hr>
            <h5 class="font-bold font-italic"><i class="fa fa-wpforms"></i> Endereço de faturamento</h5>

            <div class="form-group row">
                <div class="col-sm-2">
                    <?= $this->Form->input('cep', [
                        'id'            => 'cep',
                        'div'           => false,
                        'label'         => 'CEP*',
                        'placeholder'   => 'CEP',
                        'class'         => 'validate[required] form-control cep',
                    ]) ?>
                </div>

                <div class="col-sm-6">
                    <?= $this->Form->input('address', [
                        'id'            => 'address',
                        'div'           => false,
                        'label'         => 'Endereço*',
                        'placeholder'   => 'Endereço',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>

                <div class="col-sm-2">
                    <?= $this->Form->input('number', [
                        'div'           => false,
                        'label'         => 'Número*',
                        'placeholder'   => 'Número',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-3">
                    <?= $this->Form->input('uf_id', [
                        'value'         => $cliente->city_id >= 1 ? $cliente_dados->Cities['state_id'] : '',
                        'id'            => 'states',
                        'div'           => false,
                        'label'         => 'Estado*',
                        'options'       => $states,
                        'empty'         => 'Selecione um estado',
                        'placeholder'   => 'Estado',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>

                <div class="col-sm-5">
                    <?= $this->Form->input('city_id', [
                        'value'         => $cliente->city_id >= 1 ? $cliente->city_id : '',
                        'id'            => 'city',
                        'div'           => false,
                        'options'       => $cities,
                        'label'         => 'Cidade*',
                        'empty'         => 'Selecione uma cidade',
                        'placeholder'   => 'Cidade',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-3">
                    <?= $this->Form->input('complement', [
                        'div'           => false,
                        'label'         => 'Complemento',
                        'placeholder'   => 'Complemento',
                        'class'         => 'validate[optional] form-control',
                    ]) ?>
                </div>
                <div class="col-sm-5">
                    <?= $this->Form->input('area', [
                        'id'            => 'area',
                        'div'           => false,
                        'label'         => 'Bairro*',
                        'placeholder'   => 'Bairro',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12">
                    <?= $this->Form->input('email_notificacao', [
                        'type'      => 'checkbox',
                        'class'     => 'checkbox-login',
                        'checked'   => $cliente->email_notificacao ? true : false,
                        'label'     => " Quero receber novidades no meu email!",
                        'div'       => false,
                    ])?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-2">
                    <?= $this->Form->button('Atualizar Dados', [
                        'type'          => 'submit',
                        'div'           => false,
                        'class'         => 'btn btn-cupom float-right',
                    ]) ?>
                </div>

                <div class="col-sm-5" id="form-toggle-message">
                    <?= isset($message) ? $message : ''; ?>
                </div>
            </div>

            <hr>
            <h5 class="font-bold font-italic"><i class="fa fa-truck"></i> Endereço da academia</h5>
            <div class="col-sm-12">
            <p class="complemento">*Para alterar o Endereço da academia você deve alterar sua<?= $this->Html->link('academia.',$SSlug.'/minha-academia',['class' => '', 'escape' =>false]); ?></p>
            </div>

            <?php //if(count($academia) >= 1){ ?>

            <div class="form-group row">
                <div class="col-sm-3">
                    <?= $this->Form->input('academia_uf', [
                        'value'         => isset($academia->city->state->id) ? $academia->city->state->id : '',
                        'options'       => $states,
                        'id'            => 'states_academia',
                        'div'           => false,
                        'disabled'      => true,
                        'label'         => 'Estado',
                        'class'         => 'form-control',
                    ]) ?>
                </div>

                <div class="col-sm-5">
                    <?= $this->Form->input('academia_city', [
                        'value'         => isset($academia->city->id) ? $academia->city->id : '',
                        'options'       => $academia_cities,
                        'id'            => 'city_academia',
                        'div'           => false,
                        'disabled'      => true,
                        'label'         => 'Cidade',
                        'class'         => 'form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-8">
                    <?= $this->Form->input('academia_id', [
                        'id'            => 'academia-name-select',
                        'value'         => isset($academia->id) ? $academia->id : '',
                        'options'       => $academias,
                        'div'           => false,
                        'disabled'      => true,
                        'readonly'      => false,
                        'label'         => 'Academia',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-2">
                    <?= $this->Form->input('academia_cep', [
                        'value'         => isset($academia->cep) ? $academia->cep : '',
                        'div'           => false,
                        'label'         => 'CEP',
                        'disabled'      => true,
                        'readonly'      => true,
                        'class'         => 'validate[required] form-control cep',
                    ]) ?>
                </div>

                <div class="col-sm-6">
                    <?= $this->Form->input('academia_address', [
                        'value'         => isset($academia->address) ? $academia->address : '',
                        'div'           => false,
                        'label'         => 'Endereço',
                        'disabled'      => true,
                        'readonly'      => true,
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>

                <div class="col-sm-2">
                    <?= $this->Form->input('academia_number', [
                        'value'         => isset($academia->number) ? $academia->number : '',
                        'div'           => false,
                        'label'         => 'Número',
                        'disabled'      => true,
                        'readonly'      => true,
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-3">
                    <?= $this->Form->input('academia_complement', [
                        'value'         => isset($academia->complement) ? $academia->complement : '',
                        'div'           => false,
                        'label'         => 'Complemento',
                        'disabled'      => true,
                        'readonly'      => true,
                        'class'         => 'validate[optional] form-control',
                    ]) ?>
                </div>
                <div class="col-sm-5">
                    <?= $this->Form->input('academia_area', [
                        'value'         => isset($academia->area) ? $academia->area : '',
                        'div'           => false,
                        'label'         => 'Bairro',
                        'disabled'      => true,
                        'readonly'      => true,
                        'class'         => 'form-control',
                    ]) ?>
                </div>
            </div>
            
            

            <?php /*}else{ ?>
                <h4>
                    Você precisa indicar em qual academia realiza seus treinos para que possamos enviar seu produto e você
                    fará a retirada na mesma
                </h4>
                <p>
                    Acesse a área <?= $this->Html->link('"Meus dados"',
                        '/minha-academia',
                        ['class' => '']
                    ); ?> e procure por sua academia.
                </p>
            <?php }*/ ?>

            <?= $this->Form->end() ?>
        </div>
    </div>

</div>


<div class="container">

</div>
