<div class="container">
    <h2>Recuperar acesso :: Definir nova senha</h2>

    <br class="clear">

    <div class="row minha-conta">

        <?php if($sucesso){ ?>
            <br class="clear">
            <h4>Acesso recuperado com sucesso! </h4>
            <br class="clear">
        <?php }else{ ?>
            <div class="col-lg-9  bg-white">

                <?= $this->Form->create(null, [
                    'role'      => 'form',
                    'default'   => false,
                    'type'      => 'file'
                ]) ?>

                <h4>Crie uma senha de fácil memorização e que seja difícil de descobrir</h4>

                <div class="form-group row">
                    <div class="col-lg-4">
                        <?= $this->Form->input('password', [
                            'div'           => false,
                            'value'         => '',
                            'label'         => 'Nova Senha*',
                            'placeholder'   => 'Nova Senha*',
                            'class'         => 'validate[required,minSize[6]] form-control',
                        ]) ?>
                    </div>

                    <div class="col-lg-4">
                        <?= $this->Form->input('confirm_new_password', [
                            'type'  => 'password',
                            'label'         => 'Confirme a Nova Senha*',
                            'placeholder'   => 'Confirme a Nova Senha*',
                            'class' => 'form-control validate[required,minSize[6],equals[password]]'
                        ]) ?>
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-lg-2">
                        <?= $this->Form->button('Alterar a Senha', [
                            'type'          => 'submit',
                            'div'           => false,
                            'class'         => 'btn btn-default float-right',
                        ]) ?>
                    </div>

                    <div class="col-lg-5" id="form-toggle-message">

                        <?= $this->Flash->render() ?>
                        <?= isset($message) ? $message : ''; ?>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        <?php } ?>
    </div>
</div>