<style type="text/css">
    .footer-total {
        display: none;
    }
    .title-page {
        float: left;
        color: #5089cf;
        margin-top: 20px;    
    }
    .voltar-page {
        float: right;
        margin-top: 30px;
        margin-right: 15px;
        font-size: 18px;
    }
    .btn-att-dados{
        border: 2px solid;
        border-color: #5087c7;
        background-color: white;
        font-family: nexa_boldregular;
        color: #5087c7;
    }
    @media all and (max-width: 1279px) {
        .minha-conta-container {
            padding-right: 50px;
            padding-left: 35px;
            padding-top: 50px;
        }
    }
    @media all and (max-width: 430px) {
        .voltar-page {
            display: none;
            float: right;
            margin-top: 30px;
            margin-right: 15px;
            font-size: 18px;
        }
        .btn-cupom{
            width: 100%;
        }
        .complemento{
            text-align: center;
        }
        .title-page span{
            display: none;
        }
    }
</style>

<div class="container minha-conta-container">
    <div class="col-xs-12 col-sm-12">
        <h4 class="title-page">&nbsp;<span class="fa fa-line-chart fa-2x" style="color: #5089cf;"></span> Meus Objetivos</h4>
    </div>
    
    <br class="clear">

    <div class="minha-conta">

        <div class="col-sm-12">
            <?= $this->Form->create($cliente, [
                //'id'        => 'form-completar-cadastro-cliente',
                //'class'     => 'verificar-dados-incompletos',
                'role'      => 'form',
                //'default'   => false,
            ]) ?>

            <script>
                var xyz = <?= $cliente->id; ?>;
            </script>

            <br class="clear">
            <hr>
            <p class="complemento">Manter seus dados atualizados é vantajoso para nós e para você!</p>
            <hr>
            <?= $this->Flash->render(); ?>

            <div class="form-group row">
                <div class="col-sm-12">
                    <h5 class="font-bold"><i class="fa fa-line-chart"></i> Conte-nos quais são seus objetivos:</h5>
                </div>
                <div class="col-lg-12">

                    <?php
                    foreach($objetivos_list as $ko => $objetivo):
                        in_array($ko, $objetivos_select) ? $checked = true :  $checked = false;
                        echo '<div class="col-lg-4 font-bold"><label for="objetivo_'.$ko.'">';
                        echo $this->Form->checkbox('objetivos.'.$ko, [
                            'id'            => 'objetivo_'.$ko,
                            'hiddenField'   => 'N',
                            'value'         => 'Y',
                            'checked'       => $checked,
                            'label'         => false,
                            'class'         => 'custom-checkbox'
                        ]);
                        echo ' '.$objetivo;
                        echo '</<label></div>';

                    endforeach;
                    ?>
                </div>

                <div class="col-lg-12">
                    <hr>
                    <h5 class="font-bold"><i class="fa fa-soccer-ball-o"></i> Informe quais esportes costuma praticar:</h5>
                </div>
                <div class="col-lg-12">
                    <?php
                    foreach($esportes_list as $ke => $esporte):
                        in_array($ke, $esportes_select) ? $c = true :  $c = false;

                        echo '<div class="col-lg-4 font-bold"><label for="esporte_'.$ke.'">';
                        echo $this->Form->checkbox('esportes.'.$ke, [
                            'id'            => 'esporte_'.$ke,
                            'hiddenField'   => 'N',
                            'value'         => 'Y',
                            'checked'       => $c,
                            'label'         => false,
                            'class'         => 'custom-checkbox'
                        ]);

                        echo ' '.$esporte;
                        echo '</<label></div>';

                    endforeach;
                    ?>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-sm-12">
                    <hr>
                    <h5 class="font-bold"><i class="fa fa-bicycle"></i> Academia em que treina:</h5>
                </div>
                <div class="col-sm-11">
                    <?= $this->Form->input('academia_id', [
                        'value'         => $cliente->academia_id,
                        'options'       => $academias_list,
                        'id'            => 'academia',
                        'div'           => false,
                        'label'         => false,
                        'disabled'      => true,
                        'readonly'      => true,
                        'empty'         => 'Selecione uma academia...',
                        'class'         => 'validate[optional] form-control chosen',
                    ]) ?>
                </div>
                <div class="col-sm-12">
                <br/>
                <p>Você pode alterar sua academia<?= $this->Html->link('clicando aqui!',$SSlug.'/minha-academia',['class' => '', 'escape' =>false]); ?></p>
                </div>

            </div>

<!--            <div class="form-group row">-->
<!---->
<!--                <div class="col-lg-12">-->
<!--                    <hr>-->
<!--                    <h4 class="font-bold"><i class="fa fa-male"></i> Professor da academia em que treina</h4>-->
<!--                </div>-->
<!--                <div class="col-lg-8">-->
<!--                    --><?php //$this->Form->input('professor_id', [
//                        'value'         => $cliente->professor_id,
//                        'options'       => $professores_list,
//                        'id'            => 'professor',
//                        'div'           => false,
//                        'label'         => false,
//                        'disabled'      => false,
//                        'readonly'      => false,
//                        'empty'         => 'Selecione um professor...',
//                        'class'         => 'validate[optional] form-control chosen',
//                    ]) ?>
<!--                </div>-->
<!--            </div>-->

            <div class="form-group row">
                <hr>
                <div class="col-lg-2">
                    <?= $this->Form->button('Atualizar Dados', [
                        'type'          => 'submit',
                        'div'           => false,
                        'class'         => 'btn btn-cupom float-right',
                    ]) ?>
                </div>

                <div class="col-lg-5" id="form-toggle-message">
                    <?= isset($message) ? $message : ''; ?>
                </div>
            </div>

            <?= $this->Form->end() ?>
        </div>
    </div>

</div>


<div class="container">

</div>
