<style type="text/css">
    .footer-total {
        display: none;
    }
    .title-page {
        float: left;
        color: #5089cf;
        margin-top: 20px;    
    }
    .voltar-page {
        float: right;
        margin-top: 30px;
        margin-right: 15px;
        font-size: 18px;
    }
    .btn-att-dados{
        border: 2px solid;
        border-color: #5087c7;
        background-color: white;
        font-family: nexa_boldregular;
        color: #5087c7;
    }
    @media all and (max-width: 1279px) {
        .minha-conta-container {
            padding-right: 50px;
            padding-left: 35px;
            padding-top: 50px;
            margin-bottom: 50px;
        }
        .title-page{
            margin-left: 10px;
        }
    }
    @media all and (max-width: 430px) {
        .voltar-page {
            display: none;
            float: right;
            margin-top: 30px;
            margin-right: 15px;
            font-size: 18px;
        }
        .btn-cupom{
            width: 100%;
        }
        .complemento{
            text-align: center;
        }
        .title-page span{
            display: none;
        }
    }
</style>

<div class="container minha-conta-container">
    <div class="col-xs-12 col-sm-12">
        <h4 class="title-page"><span class="fa fa-building-o fa-2x"></span> Minha Academia</h4>
    </div>
    <br class="clear">
    <br class="clear">

        <?php

        function calcula_frete($servico,$cep_origem,$cep_destino,$peso,$mao_propria,$valor_declarado,$aviso_recebimento){         
            $mao_propria = (strtolower($mao_propria) == 's')?'s':'n';
            $aviso_recebimento = (strtolower($aviso_recebimento) == 's')?'s':'n';
             
            $url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem='. $cep_origem .'&sCepDestino='. $cep_destino .'&nVlPeso='. $peso .'&nCdFormato=1&nVlComprimento=20&nVlAltura=5&nVlLargura=15&sCdMaoPropria='. $mao_propria .'&nVlValorDeclarado='. $valor_declarado .'&sCdAvisoRecebimento='. $aviso_recebimento .'&nCdServico='. $servico .'&nVlDiametro=0&StrRetorno=xml';
             
            $frete_calcula = simplexml_load_string(file_get_contents($url));
            $frete = $frete_calcula->cServico;
             
            if($frete->Erro == '0'){
             
                switch($frete->Codigo){
                    case 41106: $servico = 'PAC'; break;
                    case 40045: $servico = 'SEDEX a Cobrar'; break;
                    case 40215: $servico = 'SEDEX 10'; break;
                    case 40290: $servico = 'SEDEX Hoje'; break;
                    default: $servico = 'SEDEX'; 
                }
             
                //$retorno = 'Serviço: '.$servico.'<br>';
                //$retorno .= 'Valor: '.$frete->Valor.'<br>';
                $retorno .= 'Prazo de entrega na sua academia: '.$frete->PrazoEntrega.' dia(s)';
             
            }elseif($frete->Erro == '7'){
                 
                $retorno = 'Serviço temporariamente indisponível, tente novamente mais tarde.';
                 
            }else{
                 
                $retorno = 'Erro no cálculo do frete, código de erro: '.$frete->Erro;
                 
            }
             
            return $retorno;
             
        }
        ?>


        <div class="col-sm-12">
            <?= $this->Form->create($cliente, [
                'id'        => 'form-completar-cadastro-cliente',
                'class'     => 'verificar-dados-incompletos',
                'role'      => 'form',
                'default'   => false,
                'type'      => 'file'
            ]) ?>

            <script>
                var xyz = <?= $cliente->id; ?>;
            </script>

            <hr>
            <p class="complemento">Manter seus dados atualizados é vantajoso para nós e para você!</p>
            <hr>

            <?php //if(count($academia) >= 1){ ?>

            <div class="form-group row">
            <div class="col-sm-12">
                <h4 class="font-bold"><i class="fa fa-building-o"></i> Dados da Academia</h4>
            </div>
                <div class="col-sm-4">
                    <?= $this->Form->input('academia_uf', [
                        'value'         => isset($academia->city->state->id) ? $academia->city->state->id : '',
                        'options'       => $states,
                        'id'            => 'states_academia',
                        'div'           => false,
                        'label'         => 'Estado',
                        'class'         => 'form-control',
                    ]) ?>
                </div>

                <div class="col-sm-6">
                    <?= $this->Form->input('academia_city', [
                        'value'         => isset($academia->city->id) ? $academia->city->id : '',
                        'options'       => $academia_cities,
                        'id'            => 'city_academia',
                        'div'           => false,
                        'label'         => 'Cidade',
                        'class'         => 'form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-10">
                    <?= $this->Form->input('academia_id', [
                        'id'            => 'academia-name-select',
                        'value'         => isset($academia->id) ? $academia->id : '',
                        'options'       => $academias,
                        'div'           => false,
                        'disabled'      => false,
                        'readonly'      => false,
                        'label'         => 'Academia',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-3">
                    <?= $this->Form->input('academia_cep', [
                        'value'         => isset($academia->cep_acad) ? $academia->cep_acad : '',
                        'div'           => false,
                        'label'         => 'CEP',
                        'disabled'      => true,
                        'readonly'      => true,
                        'class'         => 'validate[required] form-control cep',
                    ]) ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('academia_address', [
                        'value'         => isset($academia->address_acad) ? $academia->address_acad : '',
                        'div'           => false,
                        'label'         => 'Endereço',
                        'disabled'      => true,
                        'readonly'      => true,
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>

                <div class="col-sm-3">
                    <?= $this->Form->input('academia_number', [
                        'value'         => isset($academia->number_acad) ? $academia->number_acad : '',
                        'div'           => false,
                        'label'         => 'Número',
                        'disabled'      => true,
                        'readonly'      => true,
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-3">
                    <?= $this->Form->input('academia_complement', [
                        'value'         => isset($academia->complement_acad) ? $academia->complement_acad : '',
                        'div'           => false,
                        'label'         => 'Complemento',
                        'disabled'      => true,
                        'readonly'      => true,
                        'class'         => 'validate[optional] form-control',
                    ]) ?>
                </div>
                <div class="col-sm-5">
                    <?= $this->Form->input('academia_area', [
                        'value'         => isset($academia->area_acad) ? $academia->area_acad : '',
                        'div'           => false,
                        'label'         => 'Bairro',
                        'disabled'      => true,
                        'readonly'      => true,
                        'class'         => 'form-control',
                    ]) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <hr>
                    <h4 class="font-bold"><i class="fa fa-male"></i> Dados do Professor</h4>
                </div>
                <div class="col-sm-10">
                    <?= $this->Form->input('professor_id', [
                        'value'         => $cliente->professor_id,
                        'options'       => $professores_list,
                        'id'            => 'professor',
                        'div'           => false,
                        'label'         => false,
                        'disabled'      => false,
                        'readonly'      => false,
                        'empty'         => 'Selecione um professor...',
                        'class'         => 'form-control chosen',
                        'required'      => true
                    ]) ?>
                </div>
            </div>

            <?php /*}else{ ?>
                <h4>
                    Você precisa indicar em qual academia realiza seus treinos para que possamos enviar seu produto e você
                    fará a retirada na mesma
                </h4>
                <p>
                    Acesse a área <?= $this->Html->link('"Meus dados"',
                        '/minha-conta/meus-dados',
                        ['class' => '']
                    ); ?> e procure por sua academia.
                </p>
            <?php }*/ ?>

            <div class="form-group row">

                <hr>
                <div class="col-sm-2 ">
                    <?= $this->Form->button('Atualizar Dados', [
                        'type'          => 'submit',
                        'div'           => false,
                        'class'         => 'btn btn-cupom',
                    ]) ?>
                </div>

                <div class="col-sm-10" id="form-toggle-message">
                    <?= isset($message) ? $message : ''; ?>
                </div>
            </div>

            <?= $this->Form->end() ?>
        </div>
    </div>

</div>