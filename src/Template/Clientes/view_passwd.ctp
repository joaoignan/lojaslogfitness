<style type="text/css">
    .footer-total {
        display: none;
    }
    .title-page {
        float: left;
        color: #5089cf;
        margin-top: 20px;    
    }
    .voltar-page {
        float: right;
        margin-top: 30px;
        margin-right: 15px;
        font-size: 18px;
    }
    .btn-att-dados{
        border: 2px solid;
        border-color: #5087c7;
        background-color: white;
        font-family: nexa_boldregular;
        color: #5087c7;
    }
    @media all and (max-width: 1279px) {
        .minha-conta-container {
            padding-right: 50px;
            padding-left: 35px;
            padding-top: 100px;
            margin-bottom: 15px;
        }
    }
    @media all and (max-width: 430px) {
        .voltar-page {
            display: none;
            float: right;
            margin-top: 30px;
            margin-right: 15px;
            font-size: 18px;
        }
        .btn-cupom{
            width: 100%;
        }
        .complemento{
            text-align: center;
        }
        .title-page span{
            display: none;
        }
    }
</style>

<div class="container minha-conta-container">
    <div class="col-xs-12 col-sm-12">
        <h4 class="title-page">&nbsp;<span class="fa fa-key fa-2x" style="color: #5089cf;"></span> Alterar Senha</h4>
    </div>

    <br class="clear">

    <div class="minha-conta">

        <div class="col-sm-12">
            <?= $this->Form->create($cliente, [
                'id'        => 'form-completar-cadastro-cliente',
                'role'      => 'form',
                'default'   => false,
                'type'      => 'file'
            ]) ?>

            <script>
                var xyz = <?= $cliente->id; ?>;
            </script>

            <h4>Crie uma senha de fácil memorização e que seja difícil de descobrir:</h4>
            <br class="clear">
            <?php if($fbid == null){ ?>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <?= $this->Form->input('old_password', [
                            'type'          => 'password',
                            'div'           => false,
                            'label'         => 'Senha atual*',
                            'placeholder'   => 'Senha atual',
                            'class'         => 'validate[required] form-control',
                        ]) ?>
                    </div>
                </div>
            <?php }else{ ?>
                <h5>Obs.: Ao usar o Facebook para acessar sua conta, não será necessário digitar esta senha, mas em casos específicos
                    você pode precisar dela...</h5>
            <?php } ?>

            <div class="form-group row">
                <div class="col-sm-4">
                    <?= $this->Form->input('password', [
                        'div'           => false,
                        'value'         => '',
                        'label'         => 'Nova Senha*',
                        'placeholder'   => 'Nova Senha*',
                        'class'         => 'validate[required,minSize[6]] form-control',
                    ]) ?>
                </div>

                <div class="col-sm-4">
                    <?= $this->Form->input('confirm_new_password', [
                        'type'  => 'password',
                        'label'         => 'Confirme a Nova Senha*',
                        'placeholder'   => 'Confirme a Nova Senha*',
                        'class' => 'form-control validate[required,minSize[6],equals[password]]'
                    ]) ?>
                </div>

            </div>

            <div class="form-group row">
                <br class="clear">
                <div class="col-sm-2">
                    <?= $this->Form->button('Alterar Senha', [
                        'type'          => 'submit',
                        'div'           => false,
                        'class'         => 'btn btn-cupom float-right font-bold',
                    ]) ?>
                </div>
                <br class="clear">
                <br class="clear">
                <div class="col-sm-5" id="form-toggle-message">

                    <?= $this->Flash->render() ?>
                    <?= isset($message) ? $message : ''; ?>
                </div>
            </div>
            <?= $this->Form->end() ?>
            <br class="clear">
        </div>
    </div>

</div>


<div class="container">

</div>
