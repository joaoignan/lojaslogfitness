<style type="text/css">
    .footer-total {
        display: none;
    }
    .minha-conta-container {
        padding-right: 230px;
        padding-left: 230px;
        margin-left: 0;
        margin-right: 0;
        width: 100%;
        margin-bottom: 15px;
    }
    .title-page {
        float: left;
        color: #5089cf;
        margin-top: 20px;    
    }
    .voltar-page {
        float: right;
        margin-top: 30px;
        margin-right: 15px;
        font-size: 18px;
    }
    .btn-att-dados{
        border: 2px solid;
        border-color: #5087c7;
        background-color: white;
        font-family: nexa_boldregular;
        color: #5087c7;
    }
    .box-close-filter{
        display: none;
    }
    .box-close-mochila{
        display: none;
    }
    @media all and (max-width: 1279px) {
        .minha-conta-container {
            padding-right: 50px;
            padding-left: 50px;
            padding-top: 70px;
            margin-bottom: 15px;
        }
        .title-page{
            margin-left: 10px;
        }
    }
    @media all and (max-width: 430px) {
        .voltar-page {
            display: none;
            float: right;
            margin-top: 30px;
            margin-right: 15px;
            font-size: 18px;
        }
        .btn-cupom{
            width: 100%;
            font-size: 11px;
            padding-top: 6px;
            margin-bottom: 15px;
        }
        .complemento{
            text-align: center;
        }
        .title-page span{
            display: none;
        }
    }
</style>
<div class="minha-conta-container">
    <div class="col-xs-12 col-sm-12">
        <h4 class="title-page"><span class="fa fa-unlock-alt fa-2x"></span> Esqueci a senha</h4>
    </div>
    <br class="clear">

    <div class="minha-conta">

        <?php if($solicitacao == 'solicitacao-enviada'){ ?>
            <br class="clear">
            <h5>
                <strong>Solicitação enviada com sucesso!</strong>
                <br>
                <br>
                Siga os passos enviados no email <strong><?= $email; ?></strong>
            </h5>
            <br class="clear">
        <?php }elseif($solicitacao == 'falha'){ ?>
            <br class="clear">
            <h5>Falha ao enviar solicitação. <br> Dados não encontrados...</h5>
            <br class="clear">
        <?php }elseif($solicitacao == 'facebook'){ ?>
            <br class="clear">
            <h5>Seu e-mail está vinculado ao facebook, clique no botão abaixo para se conectar.</h5>
            <div class="row">
                <div class="col-sm-12 col-sm-3">
                    <?= $this->Html->link('<i class="fa fa-facebook-official"></i> Acessar com Facebook ',
                    '/fb-login/',
                    ['class' => 'btn btn-primary', 'escape' => false, 'style' => 'margin-top: 5px; width:100%;']) ?>
                </div>
            </div>
            <br class="clear">
        <?php }else{ ?>
            <div class="col-sm-12" style="margin-bottom: 15px;">
                <?= $this->Form->create(null, [
                    'id'        => 'form-completar-cadastro-cliente',
                    'role'      => 'form',
                    'default'   => false,
                    'type'      => 'file'
                ]) ?>

                <h5>Informe o email utilizado para o cadastro...</h5>
                <br class="clear">
                <div class="form-group row">
                    <div class="col-xs-12 col-sm-6">
                        <?= $this->Form->input('email', [
                            'type'          => 'text',
                            'div'           => false,
                            'label'         => 'Email ou CPF de acesso*',
                            'placeholder'   => 'Informe o email ou CPF cadastrado',
                            'class'         => 'validate[required] form-control',
                        ]) ?>
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-xs-12 col-sm-6">
                        <?= $this->Form->button('Solicitar Redefinição de Senha', [
                            'type'          => 'submit',
                            'div'           => false,
                            'class'         => 'btn btn-cupom',
                        ]) ?>
                    </div>

                    <div class="col-sm-5" id="form-toggle-message">

                        <?= $this->Flash->render() ?>
                        <?= isset($message) ? $message : ''; ?>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        <?php } ?>
    </div>
</div>
