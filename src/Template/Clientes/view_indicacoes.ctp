<style type="text/css">
    .footer-total {
        display: none;
    }
    .title-page {
        float: left;
        color: #5089cf;
    }
    .voltar-page {
        float: right;
        margin-top: 50px;
        margin-right: 15px;
        font-size: 18px;
    }
    .indicacoes {
        margin-top: 30px;
        margin-bottom: 10px;
    }
    .indicacoes-topo {
        background-color: #5089cf;
        color: white;
        text-transform: uppercase;
        font-weight: 700;
    }
    .indicacoes-linhas {
        margin: 5px 0px;
        background: #fff;
        border: 2px solid #fff;
        cursor: pointer;
        display: flex;
        align-items: center;
        color: black;
    }
    .indicacoes-linhas:hover {
        background-color: #eee;
        border-color: #eee;
    }
    .modal-backdrop {
        display: none;
    }
    .indicacoes-titulo {
        display: none; 
        float: left; 
        width: 100%; 
        padding-left: 15px; 
        padding-right: 15px;
    }
    .produto-grade-indicacoes {
        max-height: 100px;
        max-width: 100px;
    }
    .indicacao-single {
        display: flex;
        align-items: center;
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .separador {
        width: 100%;
        border: 1px solid #5087c7;
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .modal-open {
        overflow: auto;
    }
    .total-indicacoes {
        float: right;
        margin-right: 5px;
        font-size: 20px;
        font-weight: 700;
        color: #5089cf;
    }
    .bg-info {
        color: #000;
        background-color: #f3d117;
    }
    .indicacoes-titulo-mobile{
        display: none;
    }
    .btn-detalhes-mobile{
            display: none;
        }
    @media all and (max-width: 1279px) {
        .minha-conta-container {
            padding-right: 50px;
            padding-left: 50px;
            padding-top: 50px;
        }
    }
    @media all and (max-width: 430px) {
        .minha-conta-container {
            padding-right: 5px;
            padding-left: 5px;
            padding-top: 70px;
        }
        .voltar-page {
            display: none;
        }
        .btn-cupom{
            width: 100%;
        }
        .complemento{
            text-align: center;
        }
        .title-page span{
            display: none;
        }
        .btn-detalhes{
            display: none;
        }
        .btn-detalhes-mobile{
            display: block;
        }
        .indicacoes-linhas{
            font-size: 11px;
        }
        .indicacoes-topo{
            font-size: 11px;
        }
        .btn-cupom{
            padding-left: 8px;
            padding-right: 8px;
        }
        .bg-info{
            display: none;
        }
        .indicacoes-titulo-mobile{
            font-size: 13px!important;
            font-weight: 700!important;
            display: block;                
        }
        .indicacao-single {
            display: block;
            margin-top: 0px;
            margin-bottom: 0px;
            align-items: center;
        }
        .produto-grade-indicacoes {
            max-height: 70px;
            max-width: 70px;
        }
        .separador-produto {
            width: 100%;
            border: 1px solid #f3d012;
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .minha-conta a {
            padding: 0;
        }

    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        var detalhes;
        $('[href^="#detalhes"]').click(function(e) {
            e.preventDefault();
            detalhes = $(this).attr('data-target');
            $('.indicacoes-titulo').not(detalhes).slideUp();
            $(detalhes).slideToggle();
        });
    }); 
</script>

<div class="container minha-conta-container">
    <div class="col-xs-12 col-sm-12">
        <h4 class="title-page">&nbsp;<span class="fa fa-comment-o fa-2x"></span> Indicações</h4>
    </div>

    <br class="clear">

    <div class="minha-conta col-sm-12">
        <div class="indicacoes">
            <div class="col-xs-12 indicacoes-topo">
                <div class="col-xs-3 text-left">Data</div>
                <div class="col-xs-4 text-left">Professor</div>
                <div class="col-xs-2 text-left">Pedido</div>
                <div class="col-xs-3 text-center"></div>
            </div>
            <div>
            <?php
            $i = count($pre_orders);
            foreach($pre_orders as $pre_order):
                ?>
                <div class="col-xs-12 indicacoes-linhas">
                    <div class="col-xs-3 text-left"><?= h($pre_order->created) ?></div>
                    <div class="col-xs-4 text-left"><?= $pre_order->professore->name ?></div>
                    <div class="col-xs-2 text-left"><?= $pre_order->pedido_id >= 1 ? $pre_order->pedido_id : '-' ?></div>
                    <div class="col-xs-3 text-center">
                        <?= $this->Html->link(
                            ' <button type="button" class="btn btn-cupom">Detalhes <i class="fa fa-search"></i></button>',
                            '#detalhes',
                            [
                                'escape'        => false,
                                'style'         => 'cursor: pointer;',
                                'data-pid'      => $pre_order->id,
                                'title'         => "Clique para visualizar detalhes desta indicação",
                                'data-target'   => "#pid-".$pre_order->id,
                                'class'         => 'btn-detalhes'
                            ]
                        )
                        ?>
                        <?= $this->Html->link(
                            ' <button type="button" class="btn btn-cupom"><i class="fa fa-bars"></i> </button>',
                            '#detalhes',
                            [
                                'escape'        => false,
                                'style'         => 'cursor: pointer;',
                                'data-pid'      => $pre_order->id,
                                'title'         => "Clique para visualizar detalhes desta indicação",
                                'data-target'   => "#pid-".$pre_order->id,
                                'class'         => 'btn-detalhes-mobile'
                            ]
                        )
                        ?>
                    </div>
                </div>
                <div id="pid-<?= $pre_order->id ?>" role="dialog" class="indicacoes-titulo">
                    <div class="row bg-info">
                        <div class="col-sm-1 font-bold text-center">Cód.</div>
                        <div class="col-sm-6 font-bold text-center">Produto</div>
                        <div class="col-sm-1 font-bold text-center">Qtd.</div>
                        <div class="col-sm-2 font-bold text-center">Valor</div>
                        <div class="col-sm-2 font-bold text-center">Total</div>
                    </div>

                    <?php foreach($pre_order->pre_order_items as $item): ?>
                        <?php $fotos = unserialize($item->produto->fotos); ?>
                        <div class="row indicacao-single">
                            <div class="col-xs-6 col-sm-1 text-center"><p class="indicacoes-titulo-mobile">Código: </p><p><?= $item->produto_id ?></div>
                            <div class="col-xs-6 col-sm-2 text-center"><?= $this->Html->image(
                                'produtos/'.$fotos[0],
                                ['class' => 'produto-grade-indicacoes', 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]) ?></div>
                            <div class="col-xs-12 col-sm-4 text-left"><p class="indicacoes-titulo-mobile">Produto:</p>
                                <p><?= $item->produto->produto_base->name.' '
                                .$item->produto->produto_base->embalagem_conteudo.' '
                                .$item->produto->propriedade ?></p>
                            </div>
                            <div class="col-xs-4 col-sm-1 text-center"><p class="indicacoes-titulo-mobile">Qtd.</p><p><?= $item->quantidade ?></p></div>

                            <?php if($item->produto->preco_promo) { ?>
                                <div class="col-xs-4 col-sm-2 text-center"><p class="indicacoes-titulo-mobile">Valor</p><p>R$ <?= number_format($preco = $item->produto->preco_promo, 2, ',', '.'); ?></p></div>
                            <?php } else { ?>
                                <div class="col-xs-4 col-sm-2 text-center"><p class="indicacoes-titulo-mobile">Valor</p><p>R$ <?= number_format($preco = $item->produto->preco, 2, ',', '.'); ?></p></div>
                            <?php } ?>

                            <?php if($item->produto->preco_promo) { ?>
                                <div class="col-xs-4 col-sm-2 text-center"><p class="indicacoes-titulo-mobile">Total</p><p>R$ <?= number_format($total = $item->quantidade * $item->produto->preco_promo, 2, ',', '.'); ?></p></div>
                            <?php } else { ?>
                                <div class="col-xs-4 col-sm-2 text-center"><p class="indicacoes-titulo-mobile">Total</p><p>R$ <?= number_format($total = $item->quantidade * $item->produto->preco, 2, ',', '.'); ?></p></div>
                            <?php } ?>

                            


                            <hr class="separador-produto">
                        </div>

                        <?php $s_total = $s_total + $total; ?>
                    <?php endforeach; ?>

                    <div class="total-indicacoes">R$ <?= number_format($s_total, 2, ',', '.'); ?></div>

                    <br class="clear">

                    <?= $pre_order->pedido_id < 1 ?
                        $this->Html->link(
                            '<button type="button" data-id="'.$pre_order->id.'" class="btn btn-cupom font-bold btn-recolocar">
                        <i class="fa fa-arrow-circle-down"></i> Colocar na mochila</button>',
                            '/mochila/incluir-indicacao/'.$pre_order->id,
                            [
                                'title'     => 'Clique para adicionar os itens indicados na mochila',
                                'escape'    => false
                            ]
                        ) : 
                        $this->Html->link(
                            '<button type="button" data-id="'.$pre_order->id.'" class="btn btn-cupom font-bold btn-recolocar">
                        <i class="fa fa-arrow-circle-down"></i> Recolocar na mochila</button>',
                            '/mochila/incluir-indicacao/'.$pre_order->id,
                            [
                                'title'     => 'Clique para adicionar os itens indicados na mochila',
                                'escape'    => false
                            ]
                        ) ?>

                    

                    <?php $s_total = 0; ?>

                    <h6 class="font-italicc text-left">
                        <?= $pre_order->pedido_id < 1 ?
                            'Obs.: Ao clicar em "Colocar na mochila" os itens adicionados anteriormente
                             serão removidos ficando somente os da indicação.'
                            :
                            'Parabéns! Você aceitou esta indicação e adquiriu os produtos recomendados pelo 
                             professor.'
                        ?>
                    </h6>
                    <hr class="separador">
                </div>
                <?php
                $i--;
            endforeach;
            ?>
            </div>
        </div>
    </div>
</div>