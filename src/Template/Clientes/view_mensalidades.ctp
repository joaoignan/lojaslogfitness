<style type="text/css">
    #legenda{
        display: none;
    }
	.historico, .ativas{
		margin: 15px;
	}
	.historico table, .ativas table{
		border: 2px solid rgba(80, 137, 207, 1.0);
	}
	.historico table th, .ativas table th{
		padding: 10px;
		border-bottom: 2px solid rgba(80, 137, 207, 1.0);
		background-color: rgba(80, 137, 207, 0.2);
		border-collapse: collapse;
	}
	.historico table td, .ativas table td{
		padding: 10px;
		border-bottom: 2px solid rgba(80, 137, 207, 1.0);
		border-collapse: collapse;
	}
    .footer-total {
        display: none;
    }
    .minha-conta-container h5{
    	font-weight: 700;
    }
    .title-page {
        float: left;
        color: #5089cf;
        margin-top: 20px;    
    }
    .voltar-page {
        float: right;
        margin-top: 30px;
        margin-right: 15px;
        font-size: 18px;
    }
    .btn-att-dados{
        border: 2px solid;
        border-color: #5087c7;
        background-color: white;
        font-family: nexa_boldregular;
        color: #5087c7;
    }
    .overlay {
      height: 0%;
      width: 100%;
      position: fixed;
      z-index: 1;
      top: 0;
      left: 0;
      background-color: rgba(80, 137, 207, 0.6)!important;
      overflow-y: hidden;
      transition: 0.5s;
    }
    .overlay-content {
      position: relative;
      top: 20%;
      height: 50%;
      left: 25%;
      width: 50%;
      background: white;
      border-radius: 10px;
      text-align: center;
      padding: 15px;
    }
    .overlay-content h3{
      font-family: nexa_boldregular;
      color: #5089cf;
    }
    .overlay-content h4{
      font-family: nexa_boldregular;
      color: #000;
    }
    .overlay-content .fa{
      color: rgba(80, 137, 207, 0.8);
      margin: 0 2px;
    }
    .overlay-logado .fa:hover{
      color: #f3d012
    }
    .overlay a {
      padding: 8px;
      text-decoration: none;
      font-size: 36px;
      color: #fff!important;
      display: block;
      transition: 0.3s;
    }
    .overlay a:hover, .overlay a:focus {
      color: #f3d012!important;
    }
    .overlay .closebtn {
      position: absolute;
      top: 20px;
      right: 45px;
      font-size: 60px;
    }
    @media all and (max-width: 1279px) {
        .minha-conta-container {
            padding-right: 50px;
            padding-left: 35px;
            padding-top: 50px;
            margin-bottom: 50px;
        }
        .title-page{
            margin-left: 10px;
        }
    }
    @media all and (max-width: 500px) {
        #legenda{
            display: block;
            font-family: nexa;
            color: gray;
            text-align: center;
        }
        .voltar-page {
            display: none;
            float: right;
            margin-top: 30px;
            margin-right: 15px;
            font-size: 18px;
        }
        .btn-cupom{
            width: 100%;
        }
        .complemento{
            text-align: center;
        }
        .title-page span{
            display: none;
        }
        .ativas, .historico{
            overflow: scroll;
        }
        .overlay-content{
            height: 70%;
            left: 5%;
            width: 90%;
          }
    }
        @media screen and (max-height: 450px) {
          .overlay {overflow-y: auto;}
          .overlay a {font-size: 20px}
          .overlay .closebtn {
            font-size: 40px;
            top: 15px;
            right: 35px;
          }
        }
</style>

<script>
    function openCancela() {
        document.getElementById("cancela").style.height = "100%";
        $("html,body").css({"overflow":"hidden"});
    }
</script>

<?= $this->Flash->render() ?>

<div class="container minha-conta-container">
    <div class="col-xs-12">
    	<h4 class="title-page"><span class="fa fa-calendar fa-2x"></span> Mensalidades</h4>
    </div>
    <div class="col-xs-12 ativas">
    	<h5 class="text-center">Ativas</h5>
    	<table width="800" align="center">
            <tr>
                <th style="width: 215px">Nome</th>
                <th>Tipo</th>
                <th>Valor</th>
                <th>Compra</th>
                <th>Vencimento</th>
                <th>Academia</th>
                <th class="text-right">Cancelável em</th>
            </tr>
            <?php foreach ($planos_ativos as $plano_ativo) { ?>
                <?php foreach($pedidos_ativos as $pedido_ativo) { ?>
                    <?php if($plano_ativo->id == $pedido_ativo->plano_cliente_id) { ?>
                    <tr>
            			<td style="width: 215px"><?= $plano_ativo->academia_mensalidades_plano->name ?></td>
                        <td><?= $plano_ativo->type ?></td>
            			<td>R$ <?= number_format($pedido_ativo->valor, 2, ',', '') ?></td>
            			<td><?= $plano_ativo->created->format('d/m/Y') ?></td>
            			<td><?= $plano_ativo->data_vencimento->format('d/m/Y') ?></td>
                        <td><?= $this->Html->link($pedido_ativo->academia->shortname, 'https://logfitness.com.br/perfil/'.$pedido_ativo->academia->slug,['target' => '_blank']) ?></td>
                        <?php $tempo_minimo = $plano_ativo->created->addMonth($plano_ativo->academia_mensalidades_plano->time_months) ?>
                        <?php if($plano_ativo->type == 'Assinatura') { ?>
                            <?php if($now >= $tempo_minimo) { ?>
                		        <td class="text-right"><a data-id="<?= $plano_ativo->id ?>" class="cancelar-btn">Cancelar Plano</a></td>
                		    <?php } else { ?>
                                <td class="text-right"><?= $tempo_minimo->format('d/m/Y') ?></td>
                            <?php } ?>
                        <?php } else { ?>
                            <td class="text-center"> - </td>
                        <?php } ?>
                    </tr>
                    <div id="cancela-<?= $plano_ativo->id ?>" class="overlay">
                        <a href="javascript:void(0)" class="closebtn close-overlay-btn">&times;</a>
                        <div class="overlay-content">
                            <h3>Tem certeza que quer cancelar o plano?</h3>
                            <h4>Ao cancelar o plano não serão estornados mensalidades só o cancelamento de cobranças futuras.</h4>
                            <div class="col-xs-12 col-md-6">
                                <?= $this->Html->link('<button class="btn btn-success">Sim, cancelar</button>', '/cancelar_plano/'.$plano_ativo->id,['escape' => false]) ?>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <a href="#"><button class="btn btn-danger close-overlay-btn">Não, voltar</button></a>
                            </div>
                            
                            
                        </div>
                    </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
    	</table>
    </div>

    <script>
        $(document).ready(function() {
            $('.cancelar-btn').click(function() {
                var id = $(this).attr('data-id');
                $('#cancela-'+id).height('100%');
            });

            $('.close-overlay-btn').click(function() {
                $('.overlay').height(0);
            });
        });
    </script>

    <p id="legenda">Deslize para os lados e confira todas as informações.</p>

    <div class="col-xs-12 historico">
    	<h5 class="text-center">Histórico</h5>
    	<table width="800" align="center">
    		<tr>
    			<th style="width: 215px">Nome</th>
                <th>Tipo</th>
    			<th>Valor</th>
    			<th>Compra</th>
    			<th>Vencimento</th>
                <th>Academia</th>
    			<th class="text-right">Boleto</th>
    		</tr>
    		<?php foreach ($planos_inativos as $plano_inativo) { ?>
                <?php foreach($pedidos_inativos as $pedido_inativo) { ?>
                    <?php if($plano_inativo->id == $pedido_inativo->plano_cliente_id) { ?>
                    <tr>
                        <td style="width: 215px"><?= $plano_inativo->academia_mensalidades_plano->name ?></td>
                        <td><?= $plano_inativo->type ?></td>
                        <td>R$ <?= number_format($pedido_inativo->valor, 2, ',', '') ?></td>
                        <td><?= $plano_inativo->created->format('d/m/Y') ?></td>
                        <td style="<?= $plano_inativo->data_vencimento <= $now ? 'color: red' : ''; ?>"><?= $plano_inativo->data_vencimento->format('d/m/Y') ?></td>
                        <td><?= $this->Html->link($pedido_inativo->academia->shortname, $pedido_inativo->academia->slug) ?></td>
                        <td class="text-right"><?= $pedido_inativo->pedido_status_id == 2 && $pedido_inativo->payment_method == 'BoletoBancario' ? '<a target="_blank" href="'.$pedido_inativo->iugu_payment_pdf.'">Ver boleto</a>' : '' ?></td>
                    </tr>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
    	</table>
    </div>
</div>