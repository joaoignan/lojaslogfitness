<?php $this->assign('title', 'Indicações de Produtos'); ?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
      <?= link_button_listAcademias(__('Listar Academias'), ['controller' => 'Academias', 'action' => 'index']); ?>
        <?= link_button_listClientes(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']); ?>
        <?= link_button_listPedidos(__('Listar Pedidos'), ['controller' => 'Pedidos', 'action' => 'index']); ?>    
    </div>
</div>

<div class="preOrders index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('professor_id') ?></th>
            <th><?= $this->Paginator->sort('academia_id') ?></th>
            <th><?= $this->Paginator->sort('cliente_id') ?></th>
            <th><?= $this->Paginator->sort('pedido_id', 'Nº Pedido') ?></th>
            <th><?= $this->Paginator->sort('created', 'Enviada') ?></th>
            <th><?= $this->Paginator->sort('modified', 'Aceita') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($preOrders as $preOrder): ?>
            <tr>
                <td><?= $this->Number->format($preOrder->id) ?></td>
                <td>
                    <?= $preOrder->has('professore') ? $preOrder->professore->name : '' ?>
                </td>
                <td>
                    <?= $preOrder->has('academia') ? $preOrder->academia->shortname : '' ?>
                </td>
                <td>
                    <?= $preOrder->has('cliente') ? $preOrder->cliente->name : '' ?>
                </td>
                <td>
                    <?= $preOrder->has('pedido') ? $preOrder->pedido->id : '' ?>
                </td>
                <td><?= h($preOrder->created) ?></td>
                <td>
                    <?php
                    $badge = '';
                    if(
                        $preOrder->pedido_id   >= 1 &&
                        $preOrder->modified    != $preOrder->created &&
                        date_format($preOrder->modified, 'Y-m-d') >= date('Y-m-d', strtotime('- 7 days'))
                    ){
                        $badge = '<span class="bs-badge badge-blue-alt">NOVO</span>';
                    }
                    echo $preOrder->modified != $preOrder->created ? h($preOrder->modified).' '.$badge : '-'
                    ?>
                </td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-search"></i>  '.__('Ver detalhes'),
                                    '#',
                                    [
                                        'data-target'=> '#dados-'.$preOrder->id,
                                        'data-toggle'=> 'modal',
                                        'escape' => false
                                    ])
                                ?>
                            </li>
                        </ul>
                    </div>

                    <div class="modal fade"
                         id="dados-<?= $preOrder->id ?>"
                         tabindex="-1" role="dialog"
                         aria-labelledby="dados-<?= $preOrder->id ?>Label"
                         aria-hidden="true">

                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">
                                        Detalhes da Indicação <strong>#<?= $preOrder->id ?></strong>
                                        <br>
                                        Aluno: <strong><?= $preOrder->cliente->name ?></strong>
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th class="text-center">Item</th>
                                            <th class="text-center">Cód.</th>
                                            <th>Descrição</th>
                                            <th class="text-right">Quantidade</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($preOrder->pre_order_items as $item):
                                            ?>
                                            <tr>
                                                <td class="text-center"><?= $i ?></td>
                                                <td class="text-center"><?= $item->produto_id ?></td>
                                                <td><?= $item->produto->produto_base->name ?></td>
                                                <td class="text-right"><?= $item->quantidade ?></td>
                                            </tr>
                                            <?php
                                            $i++;
                                        endforeach;
                                        ?>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="col-md-12">
                                    <h5>
                                        <?= $preOrder->pedido_id >= 1
                                            ? 'Indicação Aceita :: <strong>Pedido #'.$preOrder->pedido_id.'</strong>'
                                            : 'Indicação ainda não aceita'
                                        ?>
                                    </h5>
                                </div>


                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


