
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= $this->Html->link(__('Nova Transportadora'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>

        <?= $this->Html->link(__('Cities'),
            ['controller' => 'Cities', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
    </div>

</div>

<div class="transportadoras index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('site') ?></th>
            <th><?= $this->Paginator->sort('city') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('modified') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($transportadoras as $transportadora): ?>
            <tr>
                <td><?= $this->Number->format($transportadora->id) ?></td>
                <td><?= h($transportadora->name) ?></td>
                <td><?= h($transportadora->site) ?></td>
                <td><?= h($transportadora->city->name) ?></td>
                <td><?= h($transportadora->created) ?></td>
                <td><?= h($transportadora->modified) ?></td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                    ['action' => 'view', $transportadora->id],
                                    ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                    ['action' => 'edit', $transportadora->id],
                                    ['escape' => false]) ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


