<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?= $this->Html->link(__('Listar Transportadoras'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('Cities'),
            ['controller' => 'Cities', 'action' => 'index'],
            ['class' => 'btn btn-default']) ?>
    </div>
</div>

<div class="transportadoras form">
    <?= $this->Form->create($transportadora, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Adicionar Transportadora') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('site') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('site', [
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('mensagem') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('mensagem', [
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('state_id') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('state_id', [
                    'options'       => $states,
                    'id'            => 'states',
                    'empty' => 'Selecione um estado',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('city_id') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('city_id', [
                    'id'            => 'city',
                    'options' => [],
                    'empty' => 'Selecione uma cidade',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('status_id') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('status_id', [
                    'options' => $status,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


