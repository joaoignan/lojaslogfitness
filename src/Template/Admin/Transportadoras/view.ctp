
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?= $this->Html->link(__('Editar Transportadora'), ['action' => 'edit', $transportadora->id], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('Listar Transportadoras'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
   </div>

</div>

<div class="transportadoras view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($transportadora->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($transportadora->name) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Cnpj') ?></td>
                    <td><?= h($transportadora->cnpj) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Ie') ?></td>
                    <td><?= h($transportadora->ie) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Phone') ?></td>
                    <td><?= h($transportadora->phone) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Email') ?></td>
                    <td><?= h($transportadora->email) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Cep') ?></td>
                    <td><?= h($transportadora->cep) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Address') ?></td>
                    <td><?= h($transportadora->address) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Complement') ?></td>
                    <td><?= h($transportadora->complement) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Area') ?></td>
                    <td><?= h($transportadora->area) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('City') ?></td>
                    <td><?= $transportadora->has('city') ? $this->Html->link($transportadora->city->name, ['controller' => 'Cities', 'action' => 'view', $transportadora->city->id]) : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Image') ?></td>
                    <td><?= h($transportadora->image) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status') ?></td>
                    <td><?= $transportadora->has('status') ? $this->Html->link($transportadora->status->name, ['controller' => 'Status', 'action' => 'view', $transportadora->status->id]) : '' ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($transportadora->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Number') ?></td>
                    <td><?= $this->Number->format($transportadora->number) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($transportadora->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($transportadora->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>