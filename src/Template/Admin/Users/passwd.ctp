<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="users form">
    <?= $this->Form->create($user, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
    <fieldset>
        <legend><?= __('Change Password').' :: '.$user_name ?></legend>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('old_password') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('old_password', [
                    'type'  => 'password',
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="divider"></div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('password', __('New Password')) ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('password', [
                    'label' => false,
                    'value' => '',
                    'class' => 'form-control validate[required,minSize[7]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('confirm_new_password') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('confirm_new_password', [
                    'type'  => 'password',
                    'label' => false,
                    'class' => 'form-control validate[required,minSize[7],equals[password]]'
                ]) ?>
            </div>
        </div>

        <div class="divider"></div>

        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>