<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
            <?= link_button_list(__('Listar Fornecedores'), ['action' => 'index']); ?>
    </div>
</div>

<div class="fornecedores form">
    <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Adicionar Fornecedor') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[required]',
                    'required' => true
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('username') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('username', [
                    'label' => false,
                    'class' => 'form-control validate[required]',
                    'required' => true
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('email') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('email', [
                    'label' => false,
                    'class' => 'form-control validate[required, custom[email]]',
                    'required' => true
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('cnpj') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('cnpj', [
                    'label' => false,
                    'class' => 'form-control validate[required, custom[cnpj]] cnpj',
                    'required' => true
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('ie') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('ie', [
                    'label' => false,
                    'class' => 'form-control validate[optional]',
                    'required' => true
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('phone') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('phone', [
                    'label' => false,
                    'class' => 'form-control validate[required] phone',
                    'required' => true
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('marca_id') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('marca_id', [
                    'options' => $marcas,
                    'empty' => 'Selecione uma marca',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]',
                    'required' => true
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('cep') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('cep', [
                    'label' => false,
                    'class' => 'form-control validate[required] cep',
                    'required' => true
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('address') ?>
            </div>
            <div class="col-sm-5">
                <?= $this->Form->input('address', [
                    'label' => false,
                    'class' => 'form-control validate[required]',
                    'required' => true
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('number') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('number', [
                    'label' => false,
                    'class' => 'form-control validate[required]',
                    'required' => true
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('complement') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('complement', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('area') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('area', [
                    'label' => false,
                    'class' => 'form-control validate[required]',
                    'required' => true
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('state_id') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('state_id', [
                    'options'       => $states,
                    'id'            => 'states',
                    'empty' => 'Selecione um estado',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]',
                    'required' => true
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('city_id') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('city_id', [
                    'id'            => 'city',
                    'options' => [],
                    'empty' => 'Selecione uma cidade',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]',
                    'required' => true
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('status_id') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('status_id', [
                    'options' => [
                      2 => 'Inativo',
                      1 => 'Ativo'
                    ],
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]',
                    'required' => true
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
          <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrrr btn-default', 'style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


