<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
              <?= link_button_add(__('Novo Fornecedor'), ['action' => 'add']); ?>
    </div>

</div>

<div class="fornecedores index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('username') ?></th>
            <th><?= $this->Paginator->sort('cnpj') ?></th>
            <th><?= $this->Paginator->sort('phone') ?></th>
            <th><?= $this->Paginator->sort('email') ?></th>
            <th><?= $this->Paginator->sort('marca_id') ?></th>
            <th><?= $this->Paginator->sort('city_id') ?></th>
            <th><?= $this->Paginator->sort('status_id') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($fornecedores as $fornecedore): ?>
            <tr>
                <td><?= $this->Number->format($fornecedore->id) ?></td>
                <td><?= h($fornecedore->user->name) ?></td>
                <td><?= h($fornecedore->user->username) ?></td>
                <td><?= h($fornecedore->cnpj) ?></td>
                <td><?= h($fornecedore->phone) ?></td>
                <td><?= h($fornecedore->user->email) ?></td>
                <td><?= h($fornecedore->marca->name) ?></td>
                <td><?= h($fornecedore->city->name.'-'.$fornecedore->city->uf) ?></td>
                <td><?= h($fornecedore->user->status->name) ?></td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                    ['action' => 'view', $fornecedore->id],
                                    ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                    ['action' => 'edit', $fornecedore->id],
                                    ['escape' => false]) ?>
                            </li>
                            
                        </ul>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


