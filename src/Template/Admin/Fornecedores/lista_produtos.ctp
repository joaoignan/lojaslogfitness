<?php
$this->assign('title', 'Lista de Produtos');
?>

<div class="row">
    <?= $this->Form->create(null)?>
    <div class="row">
        <div class="form-group col-md-5">
            <?= $this->Form->input('search', [
                'div'           => false,
                'label'         => false,
                'class'         => 'form-control',
                'placeholder'   => 'Busca'
            ])?>
        </div>
        <div class="form-group text-left">
            <?= $this->Form->button('<i class="glyph-icon icon-search"></i><span> '.__('Buscar Produto').'</span>',
                [
                    'class'  => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 15em;',
                    'type'   => 'submit',
                    'escape' => false
                ])
            ?>
        </div>
    </div>
    <?= $this->Form->end()?>
    <table class="table table-hover">
        <thead>
            <tr>
                <th class="text-center"><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('ProdutoBase.name', 'Produto') ?></th>
                <th class="text-right"><?= $this->Paginator->sort('estoque', 'Estoque') ?></th>
                <th class="text-right"><?= $this->Paginator->sort('custo', 'Compra R$') ?></th>
            </tr>
        </thead>
        <tbody>
        	<?php foreach ($produtos as $produto): ?>
	            <tr>
	                <td class="text-center"><?= $this->Number->format($produto->id) ?></td>
	                <td><?= h($produto->produto_base->name) ?><?= $produto->propriedade ? ' - '.$produto->propriedade : '' ?></td>
	                <td class="text-center"><?= h($produto->estoque) ?></td>
	                <td class="text-right"> R$ <?= number_format($produto->custo, 2, ',', '.') ?></td>
	            </tr>
	        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>