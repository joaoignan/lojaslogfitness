
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideeee">
               <?= link_button_edit(__('Editar Fornecedor'), ['action' => 'edit', $fornecedore->id]); ?>
        <?= link_button_list(__('Listar Fornecedores'), ['action' => 'index']); ?>
    
    </div>

</div>

<div class="fornecedores view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($fornecedore->user->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($fornecedore->user->name) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Username') ?></td>
                    <td><?= h($fornecedore->user->username) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Cnpj') ?></td>
                    <td><?= h($fornecedore->cnpj) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Marca') ?></td>
                    <td><?= h($fornecedore->marca->name) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Ie') ?></td>
                    <td><?= h($fornecedore->ie) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Phone') ?></td>
                    <td><?= h($fornecedore->phone) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Email') ?></td>
                    <td><?= h($fornecedore->user->email) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Cep') ?></td>
                    <td><?= h($fornecedore->cep) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Address') ?></td>
                    <td><?= h($fornecedore->address) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Complement') ?></td>
                    <td><?= h($fornecedore->complement) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Area') ?></td>
                    <td><?= h($fornecedore->area) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('City') ?></td>
                    <td><?= $fornecedore->has('city') ? $this->Html->link($fornecedore->city->name, ['controller' => 'Cities', 'action' => 'view', $fornecedore->city->id]) : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status') ?></td>
                    <td><?= $this->Html->link($fornecedore->user->status->name, ['controller' => 'Status', 'action' => 'view', $fornecedore->user->status->id]) ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($fornecedore->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Number') ?></td>
                    <td><?= $this->Number->format($fornecedore->number) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($fornecedore->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($fornecedore->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>


