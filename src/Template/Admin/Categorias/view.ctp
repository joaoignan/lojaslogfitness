
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideee">
        <?= link_button_edit(__('Editar Categoria'), ['action' => 'edit', $categoria->id]); ?>
        <?= link_button_list(__('List Categorias'), ['action' => 'index']); ?>
        <?= link_button_add(__('New Categoria'), ['action' => 'add']); ?>
            </div>

</div>

<div class="categorias view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($categoria->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
                            <table class="table table-hover">
                                                                        <tr>
                            <td class="font-bold"><?= __('Name') ?></td>
                            <td><?= h($categoria->name) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Slug') ?></td>
                            <td><?= h($categoria->slug) ?></td>
                            </tr>

                            <tr>
                            <td class="font-bold"><?= __('Objetivo') ?></td>
                            <td><?= h($categoria->objetivo->name) ?></td>
                            </tr>

                            <tr>
                            <td class="font-bold"><?= __('Descrição') ?></td>
                            <td><?= h($categoria->descricao) ?></td>
                            </tr>
                                                                </table>
                        </div>

                            <div class="col-md-3">
                    <table class="table table-hover">
                                            <tr>
                            <td class="font-bold"><?= __('Id') ?></td>
                            <td><?= $this->Number->format($categoria->id) ?></td>
                        </tr>
                                            <tr>
                            <td class="font-bold"><?= __('Status Id') ?></td>
                            <td><?= $this->Number->format($categoria->status_id) ?></td>
                        </tr>
                                                                                <tr>
                        <td class="font-bold"><?= __('Created') ?></td>
                        <td><?= h($categoria->created) ?></td>
                    </tr>
                                    <tr>
                        <td class="font-bold"><?= __('Modified') ?></td>
                        <td><?= h($categoria->modified) ?></td>
                    </tr>
                                                                 </table>
            </div>
        </div>
    </div>
