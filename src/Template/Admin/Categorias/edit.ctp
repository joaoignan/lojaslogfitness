<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideee">
        <?= link_button_list(__('List Categorias'), ['action' => 'index']); ?>
    </div>
</div>

<div class="categorias form">
    <?= $this->Form->create($categoria, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Edit Categoria') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('slug') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('slug', [
                    'label' => false,
                    'class' => 'form-control validate[required, custom[slug]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('descricao', 'Descrição') ?>
            </div>
            <div class="col-sm-10" id="summernote">
                <div id="summernote-text" class="summernote">
                    <?= $categoria->descricao; ?>
                </div>

                <?= $this->Form->input('descricao', [
                    'value' => $categoria->descricao,
                    'id'    => 'summernote-input',
                    'type'  => 'hidden',
                    'label' => false,
                    'class' => 'form-control validate[optional] input-block-level'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('objetivo_id') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('objetivo_id', [
                    'options' => $objetivos,
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('status_id') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('status_id', [
                    'options' => $status,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 9.5em;', 'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


