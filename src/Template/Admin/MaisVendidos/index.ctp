<style>
	.total-lista{
		width: 100%;
		padding: 15px;
		background-color: #fff;
	}
	.lista{
		padding: 5px;
	}
	.cabecalho-lista div, .linha-lista div{
		display: inline-block;
		padding: 0 10px;
		min-width: 90px;
		margin-bottom: 5px;
	}
	.nome_produto{
		width: 300px;
	}
	.lista-add-produto{
		display: none;
		padding: 0 15px;
	}
	.qtd_div .glyph-icon{
		color: #c0392b
	}
</style>

<div class="row">
	<div class="col-xs-12 text-center">
		<h3>Estes são os produtos apresentados na página de <?= $this->Html->link('mais vendidos', '/emporio/mais-vendidos', ['escape' => false, 'target' => '_blank'])  ?></h3>
	</div>
</div>
<div class="row">
	<div class="total-lista text-left">
		<div class="row">
			<div class="col-xs-12 text-left">
				<button type="button" class="btn btn-success btn-sm text-center" data-toggle="modal" data-target="#novoItem">
					<i class="glyph-icon icon-plus" aria-hidden="true"></i> Novo
				</button>
			</div>
		</div>
		<div class="lista">
			<div class="row cabecalho-lista">
				<div class="text-center">
					<p><strong>Posição</strong></p>
				</div>
				<div class="text-center">
					<p><strong>Código</strong></p>
				</div>
				<div class="nome_produto">
					<p><strong>Nome do produto</strong></p>
				</div>
				<div class="text-center">
					<p><strong>Disponivel</strong></p>
				</div>
				<div class="text-center">
					<p><strong>Ações</strong></p>
				</div>
			</div>	
			<?php foreach($mais_vendidos as $mais_vendido) { ?>
				<div class="row linha-lista">
					<div class="text-center">
						<p><?= $mais_vendido->posicao ?></p>
					</div>
					<div class="text-center">
						<p><?= $mais_vendido->produto->id ?></p>
					</div>
					<div class="nome_produto">
						<p><?= $mais_vendido->produto->produto_base->name ?></p>
					</div>
					<div class="text-center qtd_div">
						<p>
							<?php 
								if ($mais_vendido->produto->estoque >= 1) {
									echo $mais_vendido->produto->estoque;
								} else { ?>
									0 <i class="glyph-icon icon-warning" aria-hidden="true" title="ACABOU O PRODUTO"></i>
								<?php }
							?>
						</p>
					</div>
					<div class="text-center">
						<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm-<?= $mais_vendido->id ?>">
							<i class="glyph-icon icon-trash" aria-hidden="true"></i>
						</button>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="novoItem" tabindex="-1" role="dialog" aria-labelledby="novoItemLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="novoItemLabel">Adicionar produto</h4>
      </div>
      <div class="modal-body">
        <form action="">
        	<div class="row">
        		<div class="col-xs-12">
        			<div class="form-group">
				    <label for="exampleInputEmail2">Buscar</label>
				    <input type="text" class="form-control busca-input" id="exampleInputEmail2" placeholder="Digite o nome ou o código">
				  </div>
				  <button type="button" id="btn-pesquisa-add" class="btn btn-default btn-pesquisa-add">Pesquisar</button>
        		</div>
        	</div>
        	<div class="row">
        		<div class="lista-add-produto">
        			<!-- PRA CARA PRODUTO ENCONTRADO RETORNA UM DESSES -->
        			<div class="options-produtos">
					</div>
					<!-- FIM DO RETORNO -->
					<select class="form-control select-posicao">
						<option>Selecione a posição na lista</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
					</select>
        		</div>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success salvar-mais-vendido">Aplicar</button>
      </div>
    </div>
  </div>
</div>

<?php foreach($mais_vendidos as $mais_vendido) { ?>
	<!-- Modal excluir -->
	<div class="modal fade bs-example-modal-sm-<?= $mais_vendido->id ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	    	<div class="modal-header">
	    		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	    		<h4 class="modal-title" id="novoItemLabel">Remover produto</h4>
	    	</div>
	    	<div class="modal-body">
	      		<p>Deseja realmente excluir o produto <strong><?= $mais_vendido->produto->produto_base->name ?></strong> da posição <strong><?= $mais_vendido->posicao ?></strong>?</p>
	      		<br>
	      		<?= $this->Html->link('Excluir', '/admin/mais_vendidos/excluir_mais_vendido/'.$mais_vendido->id, ['escape' => false, 'class' => 'btn btn-danger'])  ?>
	    	</div>
	    </div>
	  </div>
	</div>
<?php } ?>


<script>
	$(document).ready(function(){
		$('.btn-pesquisa-add').click(function(){
			var query = $('.busca-input').val();
			$('.lista-add-produto').slideUp('fast');

			if(query != null) {
				$.ajax({
	                type: "POST",
	                url: WEBROOT_URL + 'buscar-produtos-list/',
	                data: {
	                    query: query
	                }
	            })
	                .done(function (data) {
	                    $('.options-produtos').html(data);
	                    $('.lista-add-produto').slideDown('fast');
	                });
			}
		});

		$('.salvar-mais-vendido').click(function() {
			var produto_id = $('.option-produto:checked').val();
			var posicao = $('.select-posicao option:selected').val();
			var btn = $(this);

			$('.error-save').remove();

			$.ajax({
	                type: "POST",
	                url: WEBROOT_URL + 'salvar-mais-vendidos/',
	                data: {
	                    produto_id: produto_id,
	                    posicao: posicao
	                }
	            })
	                .done(function (data) {
	                    if(data == 1) {
	                    	location.reload();
	                    } else {
	                    	btn.append('<p class="error-save" style="color: red">Erro ao salvar produto!</p>');
	                    }
	                });
		});
	})
</script>