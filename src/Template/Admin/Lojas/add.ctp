<div class="row">
	<div class="col-xs-12">
		<h4 class="font-bold font-italic"><span class="glyph-icon icon-list"></span> Dados</h4>
		<br>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="form-group">
			<label for="shortname">Nome da loja</label>
			<?= $this->Form->input('shortname', [
	        'label' => false,
	        'class' => 'form-control validate[]',
	        'required' => true,
	        'placeholder' => 'Digite o nome fantasia'
	        ]) ?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="form-group">
			<label for="name">Razão Social</label>
			<?= $this->Form->input('name', [
	        'label' => false,
	        'class' => 'form-control validate[]',
	        'required' => true,
	        'placeholder' => 'Digite a razão social'
	        ]) ?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="form-group">
			<label for="cnpj">CNPJ</label>
			<?= $this->Form->input('cnpj', [
	        'label' => false,
	        'class' => 'form-control validate[]',
	        'required' => true,
	        'placeholder' => 'Digite o CNPJ'
	        ]) ?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="form-group">
			<label for="ie">Inscrição Estadual</label>
			<?= $this->Form->input('ie', [
	        'label' => false,
	        'class' => 'form-control validate[]',
	        'required' => true,
	        'placeholder' => 'Digite a inscrição Estadual'
	        ]) ?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="form-group">
			<label for="phone">Telefone</label>
			<?= $this->Form->input('phone', [
                'label' => false,
                'class' => 'form-control validate[required] phone',
                'placeholder' => '(99) 99999-9999'
            ]) ?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
		<div class="form-group">
			<label for="contact">Qtd de alunos</label>
			<?= $this->Form->input('alunos', [
                'placeholder' => 'Ex: 600',
                'label' => false,
                'class' => 'form-control validate[optional, onlyNumber]'
            ]) ?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
		<div class="form-group">
			<label for="status_id">Status</label>
			<?= $this->Form->input('status_id', [
                'options' => $status,
                'empty' => 'Selecione',
                'label' => false,
                'class' => 'form-control chosen-select validate[required]'
            ]) ?>
		</div>
	</div>
</div>
<div class="divider"></div>
<div class="row">
	<div class="col-xs-12">
		<h4 class="font-bold font-italic"><span class="glyph-icon icon-building"></span> Endereço</h4>
		<br>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
		<div class="form-group">
			<label for="cep">CEP</label>
			<?= $this->Form->input('cep', [
                'label' => false,
                'class' => 'form-control validate[required] cep',
                'placeholder' => '99999-999'
            ]) ?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="form-group">
			<label for="address">Endereço</label>
			<?= $this->Form->input('address', [
                'label' => false,
                'class' => 'form-control validate[required] address-map'
            ]) ?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-2 ">
		<div class="form-group">
			<label for="number">Número</label>
			<?= $this->Form->input('number', [
                'label' => false,
                'class' => 'form-control validate[optional] number-map'
            ]) ?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="form-group">
			<label for="complement">Complemento</label>
			<?= $this->Form->input('complement', [
                'label' => false,
                'class' => 'form-control validate[optional]'
            ]) ?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="form-group">
			<label for="area">Bairro</label>
			<?= $this->Form->input('area', [
                'label' => false,
                'class' => 'form-control validate[required] area-map'
            ]) ?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="form-group">
			<label for="state_id">Estado</label>
			<?= $this->Form->input('state_id', [
                'options'       => $states,
                'id'            => 'states',
                'value'         => $academia->city->state_id,
                'empty' => 'Selecione um estado',
                'label' => false,
                'class' => 'form-control chosen-select validate[required] uf-map'
            ]) ?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="form-group">
			<label for="city_id">Cidade</label>
			<?= $this->Form->input('city_id', [
                'id'            => 'city',
                'options' => $cities,
                'empty' => 'Selecione uma cidade',
                'label' => false,
                'class' => 'form-control chosen-select validate[required] city-map'
            ]) ?>
		</div>
	</div>
</div>
<div class="divider"></div>
<div class="row">
	<div class="col-xs-12">
		 <h4 class="font-bold font-italic"><span class="glyph-icon icon-phone-square"></span> Contato</h4>
		<br>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="form-group">
			<label for="contact">Nome do responsável</label>
			<?= $this->Form->input('contact', [
                'label' => false,
                'class' => 'form-control validate[optional]',
                'placeholder' => 'Digite o nome'
            ]) ?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="form-group">
			<label for="email">E-mail</label>
			<?= $this->Form->input('email', [
                'label' => false,
                'class' => 'form-control validate[required, custom[email]]',
                'placeholder' => 'Digite o email'
            ]) ?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="form-group">
			<label for="contact">Celular/Whatsapp</label>
			<?= $this->Form->input('contact', [
                'label' => false,
                'class' => 'form-control validate[optional] phone',
                'placeholder' => '(99) 99999-9999'
            ]) ?>
		</div>
	</div>
</div>
<div class="divider"></div>
<div class="row">
	<div class="col-xs-12 text-center">
		<div class="form-group">
			<button class="btn btn-alt btn-default"><i class="glyph-icon icon-check-square-o"></i> Salvar</button>
		</div>
	</div>
</div>

