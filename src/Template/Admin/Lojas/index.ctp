<div class="row">
	<div class="col-xs-12">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-left"><strong>Nome da loja</strong></th>
					<th class="text-left"><strong>Endereço</strong></th>
					<th class="text-center"><strong>Status</strong></th>
					<th class="text-center"><strong>Ações</strong></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($lojas as $loja) { ?>
					<tr>
						<td><?= $loja->shortname ?></td>
						<td class="text-left">
							<?= $loja->address.', '.$loja->number.' - '.$loja->area ?> <br>
								<small><?= $loja->city->name.' - '.$loja->city->uf ?> </small>
							</td>
						<td class="text-center"><?= $loja->status->name ?></td>
						<td class="text-center"><button class="btn btn-info">Detalhes</button></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>