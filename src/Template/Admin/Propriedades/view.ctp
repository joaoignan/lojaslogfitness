<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Propriedades', 'edit', $group_id)){ ?>
            <?= $this->Html->link(__('Edit Propriedade'), ['action' => 'edit', $propriedade->id], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Propriedades', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Propriedades'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Propriedades', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Propriedade'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Status', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Status'),
                ['controller' => 'Status', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Status', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Status'),
                ['controller' => 'Status', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produtos'),
                ['controller' => 'Produtos', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto'),
                ['controller' => 'Produtos', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>

<div class="propriedades view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($propriedade->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($propriedade->name) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status') ?></td>
                    <td><?= $propriedade->has('status') ? $this->Html->link($propriedade->status->name, ['controller' => 'Status', 'action' => 'view', $propriedade->status->id]) : '' ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($propriedade->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($propriedade->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($propriedade->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="column large-12">
        <h4 class="font-bold"><?= __('Related Produtos') ?></h4>
        <?php if (!empty($propriedade->produtos)): ?>
            <table class="table table-hover">
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Produto Base Id') ?></th>
                    <th><?= __('Preco') ?></th>
                    <th><?= __('Preco Promo') ?></th>
                    <th><?= __('Propriedade Id') ?></th>
                    <th><?= __('Propriedade') ?></th>
                    <th><?= __('Fotos') ?></th>
                    <th><?= __('  Avaliacao') ?></th>
                    <th><?= __('Estoque') ?></th>
                    <th><?= __('Estoque Min') ?></th>
                    <th><?= __('Visivel') ?></th>
                    <th><?= __('Status Id') ?></th>
                    <th><?= __('Created') ?></th>
                    <th><?= __('Modified') ?></th>
                    <th class="actions"></th>
                </tr>
                <?php foreach ($propriedade->produtos as $produtos): ?>
                    <tr>
                        <td><?= h($produtos->id) ?></td>
                        <td><?= h($produtos->produto_base_id) ?></td>
                        <td><?= h($produtos->preco) ?></td>
                        <td><?= h($produtos->preco_promo) ?></td>
                        <td><?= h($produtos->propriedade_id) ?></td>
                        <td><?= h($produtos->propriedade) ?></td>
                        <td><?= h($produtos->fotos) ?></td>
                        <td><?= h($produtos->__avaliacao) ?></td>
                        <td><?= h($produtos->estoque) ?></td>
                        <td><?= h($produtos->estoque_min) ?></td>
                        <td><?= h($produtos->visivel) ?></td>
                        <td><?= h($produtos->status_id) ?></td>
                        <td><?= h($produtos->created) ?></td>
                        <td><?= h($produtos->modified) ?></td>
                        <td class="actions">
                            <div class="btn-group">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                                    <i class="glyph-icon icon-navicon"></i>
                                    <span class="sr-only"><?= __('Actions'); ?></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu" style="left: -110px;">
                                    <li>
                                        <?php if($cca->cca('admin', 'Produtos', 'view', $group_id)){ ?>
                                            <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                                ['Produtos', 'action' => 'view', $produtos->id],
                                                ['escape' => false]) ?>
                                        <?php } ?>
                                    </li>
                                    <li>
                                        <?php if($cca->cca('admin', 'Produtos', 'edit', $group_id)){ ?>
                                            <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                                ['controller' => 'Produtos', 'action' => 'edit', $produtos->id],
                                                ['escape' => false]) ?>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>