<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'Propriedades', 'index', $group_id)){ ?>
            <?= link_button_list(__('Listar Propriedades'), ['action' => 'index']); ?>
        <?php } ?>
    </div>
</div>

<div class="propriedades form">
    <?= $this->Form->create($propriedade, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Editar Propriedade') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[required,maxSize[15]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('status_id') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('status_id', [
                    'options' => $status,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

         <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrrr btn-default',  'style' =>  'width: 9.5em;', 'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>