
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
     <div class="content-box-wrapper hideee">
        <?= link_button_edit(__('Editar Cliente'), ['action' => 'edit', $cliente->id]); ?>
        <?= link_button_listClientes(__('Listar Clientes'), ['action' => 'index']); ?>
    </div>
</div>

<div class="clientes view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($cliente->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
                            <table class="table table-hover">
                                                                        <tr>
                            <td class="font-bold"><?= __('Name') ?></td>
                            <td><?= h($cliente->name) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Genre') ?></td>
                            <td><?= h($cliente->genre) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Cpf') ?></td>
                            <td><?= h($cliente->cpf) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Rg') ?></td>
                            <td><?= h($cliente->rg) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Telephone') ?></td>
                            <td><?= h($cliente->telephone) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Mobile') ?></td>
                            <td><?= h($cliente->mobile) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Email') ?></td>
                            <td><?= h($cliente->email) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Cep') ?></td>
                            <td><?= h($cliente->cep) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Address') ?></td>
                            <td><?= h($cliente->address) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Number') ?></td>
                            <td><?= h($cliente->number) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Complement') ?></td>
                            <td><?= h($cliente->complement) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Area') ?></td>
                            <td><?= h($cliente->area) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('City') ?></td>
                            <td><?= $cliente->has('city') ? $this->Html->link($cliente->city->name, ['controller' => 'Cities', 'action' => 'view', $cliente->city->id]) : '' ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Password') ?></td>
                            <td><?= h($cliente->password) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Academia') ?></td>
                            <td><?= $cliente->has('academia') ? $this->Html->link($cliente->academia->name, ['controller' => 'Academias', 'action' => 'view', $cliente->academia->id]) : '' ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Status') ?></td>
                            <td><?= $cliente->has('status') ? $this->Html->link($cliente->status->name, ['controller' => 'Status', 'action' => 'view', $cliente->status->id]) : '' ?></td>
                            </tr>
                                                                </table>
                        </div>

                            <div class="col-md-3">
                    <table class="table table-hover">
                                            <tr>
                            <td class="font-bold"><?= __('Id') ?></td>
                            <td><?= $this->Number->format($cliente->id) ?></td>
                        </tr>
                                                                                <tr>
                        <td class="font-bold"><?= __('Birth') ?></td>
                        <td><?= h($cliente->birth) ?></td>
                    </tr>
                                    <tr>
                        <td class="font-bold"><?= __('Created') ?></td>
                        <td><?= h($cliente->created) ?></td>
                    </tr>
                                    <tr>
                        <td class="font-bold"><?= __('Modified') ?></td>
                        <td><?= h($cliente->modified) ?></td>
                    </tr>
                                                                 </table>
            </div>
        </div>
    </div>
