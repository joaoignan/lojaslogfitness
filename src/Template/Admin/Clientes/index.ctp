
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
   
    <div class="content-box-wrapper hidee">
        <?= link_button_listAcademias(__('Listar Academias'), ['controller' => 'Academias', 'action' => 'index']); ?>
        <?php if($group_id != 7) { ?>
            <?= link_button_listPedidos(__('Listar Pedidos'), ['controller' => 'Pedidos', 'action' => 'index']); ?>
        <?php } ?>
    </div>

</div>

<div class="clientes index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('genre') ?></th>
            <?php if($group_id != 7) { ?>
                <th><?= $this->Paginator->sort('cpf') ?></th>
                <th><?= $this->Paginator->sort('rg') ?></th>
            <?php } else { ?>
                <th><?= $this->Paginator->sort('academia') ?></th>
            <?php } ?>
            <th><?= $this->Paginator->sort('telephone') ?></th>
            <th><?= $this->Paginator->sort('mobile') ?></th>
            <?php if($group_id != 7) { ?>
                <th class="actions"></th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($clientes as $cliente): ?>
            <tr>
                <td><?= $this->Number->format($cliente->id) ?></td>
                <td><?= h($cliente->name) ?></td>
                <td><?= h($cliente->genre) ?></td>
                <?php if($group_id != 7) { ?>
                    <td><?= h($cliente->cpf) ?></td>
                    <td><?= h($cliente->rg) ?></td>
                <?php } else { ?>
                    <td><?= h($cliente->academia->shortname) ?></td>
                <?php } ?>
                <td><?= h($cliente->telephone) ?></td>
                <td><?= h($cliente->mobile) ?></td>
                <?php if($group_id != 7) { ?>
                    <td class="actions">
                        <div class="btn-group">
                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                                <i class="glyph-icon icon-navicon"></i>
                                <span class="sr-only"><?= __('Actions'); ?></span>
                            </button>
                            <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                                <li>
                                    <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                        ['action' => 'view', $cliente->id],
                                        ['escape' => false]) ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                        ['action' => 'edit', $cliente->id],
                                        ['escape' => false]) ?>
                                </li>                            
                            </ul>
                        </div>
                    </td>
                <?php } ?>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


