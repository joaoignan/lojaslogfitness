<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
                <?= $this->Html->link(__('List Clientes'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
                    <?= $this->Html->link(__('List Cities'),
        ['controller' => 'Cities', 'action' => 'index'],
        ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New City'),
        ['controller' => 'Cities', 'action' => 'add'],
        ['class' => 'btn btn-default']) ?>
                <?= $this->Html->link(__('List Academias'),
        ['controller' => 'Academias', 'action' => 'index'],
        ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Academia'),
        ['controller' => 'Academias', 'action' => 'add'],
        ['class' => 'btn btn-default']) ?>
                <?= $this->Html->link(__('List Status'),
        ['controller' => 'Status', 'action' => 'index'],
        ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Status'),
        ['controller' => 'Status', 'action' => 'add'],
        ['class' => 'btn btn-default']) ?>
                <?= $this->Html->link(__('List Interesses'),
        ['controller' => 'Interesses', 'action' => 'index'],
        ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Interess'),
        ['controller' => 'Interesses', 'action' => 'add'],
        ['class' => 'btn btn-default']) ?>
                <?= $this->Html->link(__('List Pedidos'),
        ['controller' => 'Pedidos', 'action' => 'index'],
        ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Pedido'),
        ['controller' => 'Pedidos', 'action' => 'add'],
        ['class' => 'btn btn-default']) ?>
                <?= $this->Html->link(__('List Produto Stars'),
        ['controller' => 'ProdutoStars', 'action' => 'index'],
        ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Produto Star'),
        ['controller' => 'ProdutoStars', 'action' => 'add'],
        ['class' => 'btn btn-default']) ?>
                </div>
        </div>

        <div class="clientes form">
        <?= $this->Form->create($cliente, ['class' => 'form-horizontal bordered-row']); ?>
        <fieldset>
            <legend><?= __('Add Cliente') ?></legend>
                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('name') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('genre') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('genre', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('cpf') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('cpf', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('rg') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('rg', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('telephone') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('telephone', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('mobile') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('mobile', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('birth') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('birth', array(
                    'empty' => true,
                    'default' => '',
                    'label' => false,
                    'class' => 'form-control validate[]'
                    )) ?>
                </div>
            </div>
                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('email') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('email', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('cep') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('cep', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('address') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('address', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('number') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('number', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('complement') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('complement', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('area') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('area', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('city_id') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('city_id', [
                    'options' => $cities,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('password') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('password', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('academia_id') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('academia_id', [
                    'options' => $academias,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('status_id') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('status_id', [
                    'options' => $status,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group text-center">
                <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                'escape' => false]); ?>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>


