<script type="text/javascript">
    $(document).ready(function() {
            $('.data-mask').mask('00-00-0000');
    });
</script>

<div class="col-xs-12" style="height: 150px">
    <div class="col-xs-2">
        <?= $this->Html->link('Limpar Lojamodelo',
            '/admin/navegacoes/apagar_lojamodelo',
            ['class' => 'btn btn-default']); ?>
    </div>
    <div class="col-xs-10" style="padding-bottom: 20px">
        <div class="col-xs-2">
            Facebook: <?= $count_facebook ?>
        </div>
        <div class="col-xs-2">
            Whatsapp: <?= $count_whatsapp ?>
        </div>
        <div class="col-xs-2">
            Home: <?= $count_home ?>
        </div>
        <div class="col-xs-2">
            Admin Acad: <?= $count_admin_acad ?>
        </div>
        <div class="col-xs-2">
            Admin Time: <?= $count_admin_time ?>
        </div>
        <div class="col-xs-2">
            Mais vendidos: <?= $count_mais_vendidos ?>
        </div>
        <div class="col-xs-2">
            Promoções: <?= $count_promocoes ?>
        </div>
        <div class="col-xs-2">
            Catalogo: <?= $count_catalogo ?>
        </div>
        <div class="col-xs-2">
            Montar combo: <?= $count_combo ?>
        </div>
    </div>

    <div class="col-xs-12" style="padding-bottom: 20px">
        <div class="col-xs-2">
            Adaptogen: <?= $count_adaptogen ?>
        </div>
        <div class="col-xs-2">
            Atlhetica: <?= $count_atlhetica ?>
        </div>
        <div class="col-xs-2">
            Black Skull: <?= $count_black_skull ?>
        </div>
        <div class="col-xs-2">
            Body Action: <?= $count_body_action ?>
        </div>
        <div class="col-xs-2">
            Bodybuilders: <?= $count_bodybuilders ?>
        </div>
        <div class="col-xs-2">
            Gt Nutrition: <?= $count_gt_nutrition ?>
        </div>
        <div class="col-xs-2">
            Integral Medica: <?= $count_integral_medica ?>
        </div>
        <div class="col-xs-2">
            Iridium Labs: <?= $count_iridium_labs ?>
        </div>
        <div class="col-xs-2">
            Labrada: <?= $count_labrada ?>
        </div>
        <div class="col-xs-2">
            Logfitness: <?= $count_logfitness ?>
        </div>
        <div class="col-xs-2">
            Max Titanium: <?= $count_max_titanium ?>
        </div>
        <div class="col-xs-2">
            Midway: <?= $count_midway ?>
        </div>
        <div class="col-xs-2">
            New Millen: <?= $count_new_millen ?>
        </div>
        <div class="col-xs-2">
            Optimum Nutrition: <?= $count_optimum_nutrition ?>
        </div>
        <div class="col-xs-2">
            Power Suplements: <?= $count_power_suplements ?>
        </div>
        <div class="col-xs-2">
            Probiotica: <?= $count_probiotica ?>
        </div>
        <div class="col-xs-2">
            Universal: <?= $count_universal ?>
        </div>
        <div class="col-xs-2">
            Usplabs: <?= $count_usplabs ?>
        </div>
        <div class="col-xs-2">
            Vitaminlife: <?= $count_vitaminlife ?>
        </div>
        <div class="col-xs-2">
            Vpx: <?= $count_vpx ?>
        </div>
    </div>
</div>

<div class="col-xs-12" style="height: 80px;">
    <div class="col-xs-2">
        <?= $this->Form->input(null, [
            'options' => [
                0 => 'Todos',
                'mozilla'  => 'Usuário',
                'facebook' => 'Facebook',
                'whatsapp' => 'Whatsapp'
            ],
            'value' => $user_agent,
            'label' => 'User Agent',
            'class' => 'form-control chosen-select filter_user_agent',
        ]) ?>
    </div>

    <div class="col-xs-2">
        <?= $this->Form->input(null, [
            'options' => $tipos_navegacao_list,
            'value' => $tipo,
            'label' => 'Tipo',
            'class' => 'form-control chosen-select filter_tipo',
        ]) ?>
    </div>

    <div class="col-xs-2">
        <?= $this->Form->input(null, [
            'options' => $academias_navegacao_list,
            'value' => $academia,
            'label' => 'Academia',
            'class' => 'form-control chosen-select filter_academia',
        ]) ?>
    </div>

    <div class="col-xs-2">
        <?= $this->Form->input(null, [
            'value' => $data_inicio,
            'placeholder' => 'Ex: 31-10-2017',
            'label' => 'Data Inicial',
            'class' => 'form-control filter_data_inicio data-mask',
        ]) ?>
    </div>

    <div class="col-xs-2">
        <?= $this->Form->input(null, [
            'value' => $data_fim,
            'placeholder' => 'Ex: 31-10-2017',
            'label' => 'Data Final',
            'class' => 'form-control filter_data_fim data-mask',
        ]) ?>
    </div>

    <div class="col-xs-2" style="margin-top: 25px;">
        <?= $this->Form->button('<i class="glyph-icon icon-search"></i> <span>Filtrar</span>',
        ['class' => 'btn btn-alt btn-default filter_button',
            'escape' => false
        ]); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.filter_button').click(function() {
            var user_agent = $('.filter_user_agent').val();
            var tipo = $('.filter_tipo').val();
            var academia = $('.filter_academia').val();
            var data_inicio = $('.filter_data_inicio').val();
            var data_fim = $('.filter_data_fim').val();

            location.href = WEBROOT_URL + 'admin/navegacoes/index/' + user_agent + '/' + tipo + '/' + academia + '/' + data_inicio + '/' + data_fim;
        });
    });
</script>

<div class="fornecedores index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('user_agent') ?></th>
            <th><?= $this->Paginator->sort('tipo') ?></th>
            <th><?= $this->Paginator->sort('usuario') ?></th>
            <th><?= $this->Paginator->sort('url_origem') ?></th>
            <th><?= $this->Paginator->sort('url_destino') ?></th>
            <th><?= $this->Paginator->sort('academia') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($navegacoes as $navegacao): ?>
            <tr>
                <td><?= $this->Number->format($navegacao->id) ?></td>
                <td>
                    <?php if(strpos($navegacao->user_agent,"facebook") !== false) { ?>
                        Facebook
                    <?php } else if(strpos($navegacao->user_agent,"WhatsApp") !== false) { ?>
                        Whatsapp
                    <?php } else { ?>
                        <?= strpos($navegacao->user_agent,"whatsapp") ?>
                        Usuário
                    <?php } ?>
                </td>
                <td><?= h($navegacao->tipo) ?></td>
                <td><?= h($navegacao->usuario) ?></td>
                <td><?= h($navegacao->url_origem) ?></td>
                <td><?= h($navegacao->url_destino) ?></td>
                <td><?= h($navegacao->academia) ?></td>
                <td><?= h($navegacao->created) ?></td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


