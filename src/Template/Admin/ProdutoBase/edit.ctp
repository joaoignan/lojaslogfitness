<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'ProdutoBase', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produto Base'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Embalagens', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Embalagens'),
                ['controller' => 'Embalagens', 'action' => 'index'],
                ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Embalagens', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Embalagen'),
                ['controller' => 'Embalagens', 'action' => 'add'],
                ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Status', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Status'),
                ['controller' => 'Status', 'action' => 'index'],
                ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Status', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Status'),
                ['controller' => 'Status', 'action' => 'add'],
                ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'ProdutoObjetivos', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produto Objetivos'),
                ['controller' => 'ProdutoObjetivos', 'action' => 'index'],
                ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'ProdutoObjetivos', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto Objetivo'),
                ['controller' => 'ProdutoObjetivos', 'action' => 'add'],
                ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produtos'),
                ['controller' => 'Produtos', 'action' => 'index'],
                ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto'),
                ['controller' => 'Produtos', 'action' => 'add'],
                ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
    </div>

        <div class="produtoBase form">
        <?= $this->Form->create($produtoBase, ['class' => 'form-horizontal bordered-row']); ?>
        <fieldset>
            <legend><?= __('Edit Produto Base') ?></legend>
                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('name') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[3], maxSize[255]]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('atributos') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('atributos', [
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[3], maxSize[500]]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('dicas') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('dicas', [
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[3], maxSize[500]]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('embalagem_id') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('embalagem_id', [
                    'options' => $embalagens,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('peso') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('peso', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('largura') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('largura', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('altura') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('altura', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('profundidade') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('profundidade', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('status_id') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('status_id', [
                    'options' => $status,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group text-center">
                <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                'escape' => false]); ?>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>