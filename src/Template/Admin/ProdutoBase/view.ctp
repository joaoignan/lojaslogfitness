<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'ProdutoBase', 'edit', $group_id)){ ?>
            <?= $this->Html->link(__('Edit Produto Base'), ['action' => 'edit', $produtoBase->id], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'ProdutoBase', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produto Base'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'ProdutoBase', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto Base'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Embalagens', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Embalagens'),
                ['controller' => 'Embalagens', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Embalagens', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Embalagen'),
                ['controller' => 'Embalagens', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Status', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Status'),
                ['controller' => 'Status', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Status', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Status'),
                ['controller' => 'Status', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'ProdutoObjetivos', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produto Objetivos'),
                ['controller' => 'ProdutoObjetivos', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'ProdutoObjetivos', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto Objetivo'),
                ['controller' => 'ProdutoObjetivos', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produtos'),
                ['controller' => 'Produtos', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto'),
                ['controller' => 'Produtos', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>

<div class="produtoBase view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($produtoBase->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($produtoBase->name) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Embalagen') ?></td>
                    <td><?= $produtoBase->has('embalagen') ? $this->Html->link($produtoBase->embalagen->name, ['controller' => 'Embalagens', 'action' => 'view', $produtoBase->embalagen->id]) : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status') ?></td>
                    <td><?= $produtoBase->has('status') ? $this->Html->link($produtoBase->status->name, ['controller' => 'Status', 'action' => 'view', $produtoBase->status->id]) : '' ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($produtoBase->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Peso') ?></td>
                    <td><?= $this->Number->format($produtoBase->peso) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Largura') ?></td>
                    <td><?= $this->Number->format($produtoBase->largura) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Altura') ?></td>
                    <td><?= $this->Number->format($produtoBase->altura) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Profundidade') ?></td>
                    <td><?= $this->Number->format($produtoBase->profundidade) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($produtoBase->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($produtoBase->modified) ?></td>
                </tr>

                <tr>
                    <td class="font-bold"><?= __('Atributos') ?></td>
                    <?= $this->Text->autoParagraph(h($produtoBase->atributos)); ?>
                </tr>

                <tr>
                    <td class="font-bold"><?= __('Dicas') ?></td>
                    <?= $this->Text->autoParagraph(h($produtoBase->dicas)); ?>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="column large-12">
        <h4 class="font-bold"><?= __('Related ProdutoObjetivos') ?></h4>
        <?php if (!empty($produtoBase->produto_objetivos)): ?>
            <table class="table table-hover">
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Produto Base Id') ?></th>
                    <th><?= __('Objetivo Id') ?></th>
                    <th><?= __('Created') ?></th>
                    <th class="actions"></th>
                </tr>
                <?php foreach ($produtoBase->produto_objetivos as $produtoObjetivos): ?>
                    <tr>
                        <td><?= h($produtoObjetivos->id) ?></td>
                        <td><?= h($produtoObjetivos->produto_base_id) ?></td>
                        <td><?= h($produtoObjetivos->objetivo_id) ?></td>
                        <td><?= h($produtoObjetivos->created) ?></td>
                        <td class="actions">
                            <div class="btn-group">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                                    <i class="glyph-icon icon-navicon"></i>
                                    <span class="sr-only"><?= __('Actions'); ?></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu" style="left: -110px;">
                                    <li>
                                        <?php if($cca->cca('admin', 'ProdutoObjetivos', 'view', $group_id)){ ?>
                                            <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                                ['ProdutoObjetivos', 'action' => 'view', $produtoObjetivos->id],
                                                ['escape' => false]) ?>
                                        <?php } ?>
                                    </li>
                                    <li>
                                        <?php if($cca->cca('admin', 'ProdutoObjetivos', 'edit', $group_id)){ ?>
                                            <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                                ['controller' => 'ProdutoObjetivos', 'action' => 'edit', $produtoObjetivos->id],
                                                ['escape' => false]) ?>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="column large-12">
        <h4 class="font-bold"><?= __('Related Produtos') ?></h4>
        <?php if (!empty($produtoBase->produtos)): ?>
            <table class="table table-hover">
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Produto Base Id') ?></th>
                    <th><?= __('Preco') ?></th>
                    <th><?= __('Preco Promo') ?></th>
                    <th><?= __('Sabor') ?></th>
                    <th><?= __('Fotos') ?></th>
                    <th><?= __('  Avaliacao') ?></th>
                    <th><?= __('Estoque') ?></th>
                    <th><?= __('Estoque Min') ?></th>
                    <th><?= __('Visivel') ?></th>
                    <th><?= __('Status Id') ?></th>
                    <th><?= __('Created') ?></th>
                    <th><?= __('Modified') ?></th>
                    <th class="actions"></th>
                </tr>
                <?php foreach ($produtoBase->produtos as $produtos): ?>
                    <tr>
                        <td><?= h($produtos->id) ?></td>
                        <td><?= h($produtos->produto_base_id) ?></td>
                        <td><?= h($produtos->preco) ?></td>
                        <td><?= h($produtos->preco_promo) ?></td>
                        <td><?= h($produtos->sabor) ?></td>
                        <td><?= h($produtos->fotos) ?></td>
                        <td><?= h($produtos->__avaliacao) ?></td>
                        <td><?= h($produtos->estoque) ?></td>
                        <td><?= h($produtos->estoque_min) ?></td>
                        <td><?= h($produtos->visivel) ?></td>
                        <td><?= h($produtos->status_id) ?></td>
                        <td><?= h($produtos->created) ?></td>
                        <td><?= h($produtos->modified) ?></td>
                        <td class="actions">
                            <div class="btn-group">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                                    <i class="glyph-icon icon-navicon"></i>
                                    <span class="sr-only"><?= __('Actions'); ?></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu" style="left: -110px;">
                                    <li>
                                        <?php if($cca->cca('admin', 'Produtos', 'view', $group_id)){ ?>
                                            <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                                ['Produtos', 'action' => 'view', $produtos->id],
                                                ['escape' => false]) ?>
                                        <?php } ?>
                                    </li>
                                    <li>
                                        <?php if($cca->cca('admin', 'Produtos', 'edit', $group_id)){ ?>
                                            <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                                ['controller' => 'Produtos', 'action' => 'edit', $produtos->id],
                                                ['escape' => false]) ?>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>