<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'ProdutoBase', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto Base'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Embalagens', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Embalagens'),
                ['controller' => 'Embalagens', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Embalagens', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Embalagen'),
                ['controller' => 'Embalagens', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Status', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Status'),
                ['controller' => 'Status', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Status', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Status'),
                ['controller' => 'Status', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'ProdutoObjetivos', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produto Objetivos'),
                ['controller' => 'ProdutoObjetivos', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'ProdutoObjetivos', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto Objetivo'),
                ['controller' => 'ProdutoObjetivos', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produtos'),
                ['controller' => 'Produtos', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto'),
                ['controller' => 'Produtos', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>

<div class="produtoBase index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('embalagem_id') ?></th>
            <th><?= $this->Paginator->sort('peso') ?></th>
            <th><?= $this->Paginator->sort('largura') ?></th>
            <th><?= $this->Paginator->sort('altura') ?></th>
            <th><?= $this->Paginator->sort('profundidade') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($produtoBase as $produtoBase): ?>
            <tr>
                <td><?= $this->Number->format($produtoBase->id) ?></td>
                <td><?= h($produtoBase->name) ?></td>
                <td>
                    <?= $produtoBase->has('embalagen') ? $this->Html->link($produtoBase->embalagen->name, ['controller' => 'Embalagens', 'action' => 'view', $produtoBase->embalagen->id]) : '' ?>
                </td>
                <td><?= $this->Number->format($produtoBase->peso) ?></td>
                <td><?= $this->Number->format($produtoBase->largura) ?></td>
                <td><?= $this->Number->format($produtoBase->altura) ?></td>
                <td><?= $this->Number->format($produtoBase->profundidade) ?></td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?php if($cca->cca('admin', 'ProdutoBase', 'view', $group_id)){ ?>
                                    <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                        ['action' => 'view', $produtoBase->id],
                                        ['escape' => false]) ?>
                                <?php } ?>
                            </li>
                            <li>
                                <?php if($cca->cca('admin', 'ProdutoBase', 'edit', $group_id)){ ?>
                                    <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                        ['action' => 'edit', $produtoBase->id],
                                        ['escape' => false]) ?>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>