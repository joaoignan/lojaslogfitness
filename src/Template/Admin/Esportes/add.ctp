<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideee">
               <?= link_button_list(__('Listar Esportes'), ['action' => 'index']); ?>
                </div>
        </div>

        <div class="esportes form">
        <?= $this->Form->create($esporte, ['class' => 'form-horizontal bordered-row']); ?>
        <fieldset>
            <legend><?= __('Adicionar Esporte') ?></legend>
                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('name') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group text-center">
                 <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrrrr btn-default', 'style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>


