
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_edit(__('Editar Esporte'), ['action' => 'edit', $esporte->id]); ?>
        <?= link_button_list(__('Listar Esportes'), ['action' => 'index']); ?>
        <?= link_button_add(__('Novo Esporte'), ['action' => 'add']); ?>
    </div>

</div>

<div class="esportes view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($esporte->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($esporte->name) ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($esporte->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($esporte->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($esporte->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>