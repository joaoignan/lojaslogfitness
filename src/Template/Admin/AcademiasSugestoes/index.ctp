<div class="academiasSugestoes index">
    <table class="table table-hover">
        <thead>
        <tr>

            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('responsavel') ?></th>
            <th><?= $this->Paginator->sort('telephone') ?></th>
            <th><?= $this->Paginator->sort('city_id') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('Indicação') ?></th>
            <!--<th class="actions"></th>-->

        </thead>
        <tbody>

        <?php foreach ($academiasSugestoes as $academiasSugesto): ;?>
            <tr>

                <td><?= h(strtoupper($academiasSugesto->name)) ?></td>
                <td><?= h($academiasSugesto->responsavel) ?></td>
                <td><?= h($academiasSugesto->telephone) ?></td>
                <td><?= $academiasSugesto->has('city') ? $academiasSugesto->city->name.'-'.$academiasSugesto->city->uf : '' ?> </td>
                <td><?= h($academiasSugesto->created) ?></td>
                <td><?= $academiasSugesto->professore != null ? h($academiasSugesto->professore->name) : '' ?>
                    <?= $academiasSugesto->email_indicador ?></td>

                <!--
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                    ['action' => 'view', $academiasSugesto->id],
                                    ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Form->postLink('<i class="glyph-icon icon-trash-o"></i>  '.__('Delete'),
                                    ['action' => 'readed', $academiasSugesto->id],
                                    ['confirm' => __('Are you sure you want to delete # {0}?', $academiasSugesto->id), 'escape' => false]) ?>
                            </li>
                        </ul>
                    </div>
                </td>
                -->
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


