<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
                <?= $this->Html->link(__('List Academias Sugestoes'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
                    <?= $this->Html->link(__('List Cities'),
        ['controller' => 'Cities', 'action' => 'index'],
        ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New City'),
        ['controller' => 'Cities', 'action' => 'add'],
        ['class' => 'btn btn-default']) ?>
                </div>
        </div>

        <div class="academiasSugestoes form">
        <?= $this->Form->create($academiasSugesto, ['class' => 'form-horizontal bordered-row']); ?>
        <fieldset>
            <legend><?= __('Add Academias Sugesto') ?></legend>
                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('name') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('responsavel') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('responsavel', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('telephone') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('telephone', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('city_id') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('city_id', [
                    'options' => $cities,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group text-center">
                <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                'escape' => false]); ?>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>


