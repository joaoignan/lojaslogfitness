
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?= $this->Html->link(__('Edit Academias Sugesto'), ['action' => 'edit', $academiasSugesto->id], ['class' => 'btn btn-default']) ?>
        <?= $this->Form->postLink(__('Delete Academias Sugesto'), ['action' => 'delete', $academiasSugesto->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $academiasSugesto->id), 'class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('List Academias Sugestoes'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Academias Sugesto'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
                    <?= $this->Html->link(__('List Cities'),
        ['controller' => 'Cities', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New City'),
        ['controller' => 'Cities', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
            </div>

</div>

<div class="academiasSugestoes view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($academiasSugesto->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
                            <table class="table table-hover">
                                                                        <tr>
                            <td class="font-bold"><?= __('Name') ?></td>
                            <td><?= h($academiasSugesto->name) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Responsavel') ?></td>
                            <td><?= h($academiasSugesto->responsavel) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Telephone') ?></td>
                            <td><?= h($academiasSugesto->telephone) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('City') ?></td>
                            <td><?= $academiasSugesto->has('city') ? $this->Html->link($academiasSugesto->city->name, ['controller' => 'Cities', 'action' => 'view', $academiasSugesto->city->id]) : '' ?></td>
                            </tr>
                                                                </table>
                        </div>

                            <div class="col-md-3">
                    <table class="table table-hover">
                                            <tr>
                            <td class="font-bold"><?= __('Id') ?></td>
                            <td><?= $this->Number->format($academiasSugesto->id) ?></td>
                        </tr>
                                                                                <tr>
                        <td class="font-bold"><?= __('Created') ?></td>
                        <td><?= h($academiasSugesto->created) ?></td>
                    </tr>
                                                                 </table>
            </div>
        </div>
    </div>


