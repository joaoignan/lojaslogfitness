<style type="text/css">
    #page-title {
        display: none;
    }
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>

<div class="row">
    <?php if (!$this->request->is('mobile')) { ?>
        <div class="total-chart" style="width: 750px;">
            <div class="content-box">
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="text-center">Total de vendas online nos últimos 30 dias: R$ <?= number_format($vendas_total, 2, ',', '.') ?></h3>
                        <canvas id="myChart" height="90px"></canvas>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if ($this->request->is('mobile')) { ?>
        <div class="content-box">
            <div class="row">
                <div class="col-xs-12">
                    <canvas id="myChart" height="200px"></canvas>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<script>
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [<?php foreach($vendas_30_dias as $venda_30_dias) { ?> '<?= $venda_30_dias['data']; ?>',<?php } ?>],
        datasets: [
            {
                label: 'Vendas nos últimos 30 dias',
                data: [<?php foreach($vendas_30_dias as $venda_30_dias) { echo $venda_30_dias['vendas'].', '; } ?>],
                backgroundColor: [
                    'rgba(153, 204, 0, 0.2)',
                ],
                borderColor: [
                    'rgba(153, 204, 0, 1)',
                ],
                borderWidth: 3
            }
        ]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    callback: function(label, index, labels) {
                        return 'R$' + label.toString();
                    }
                }
            }]
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, chartData) {
                    return 'R$'+tooltipItem.yLabel.toString();
                }
            }
        }
    }
});
</script>

<div class="row">
    <?php if (!$this->request->is('mobile')) { ?>
        <!-- <div class="total-chart" style="width: 750px;">
            <div class="content-box">
                <div class="row">
                    <div class="col-xs-12">
                        <iframe width="750px" height="371px" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vT1m_-Yz1AXpjQqFGwHvlZP_1aiEwUunWZJDsD6j3k645PLrwvR3aSOZt8g1z-c2gEkGZRQBdM3IE2B/pubchart?oid=1964148570&amp;format=interactive"></iframe>
                    </div>
                </div>
            </div>
        </div> -->
    <?php } ?>
</div>
