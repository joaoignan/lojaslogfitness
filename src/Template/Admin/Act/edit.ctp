<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideee">
        <?= link_button_list(__('Listar Documentos'), ['action' => 'index']); ?>
    </div>
</div>

<div class="act form">
    <?= $this->Form->create($act, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Editar Documento') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('title') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('title', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('Arquivo') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('arquivo', [
                    'readonly' => 'true',
                    'label' => false,
                    'class' => 'form-control validate[required, custom[validateMIME[image/jpeg|image/png|application/pdf|application/doc|application/ppt|text/plain|]]]',
//                    'type'  => 'file'
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('Disponível para') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('is_visible', [
                    'options'   => [ 'Todos','Academias','Professores'],
                    'empty' => false,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('status_id') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('status_id', [
                    'options' => $status,
                    'empty' => false,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>


        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrrr btn-default','style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


