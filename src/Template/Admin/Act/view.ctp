
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideee">
        <?php if($cca->cca('admin', 'Act', 'edit', $group_id)){ ?>
            <?= link_button_edit(__('Editar Documentos'), ['action' => 'edit', $act->id]); ?>
        <?php } ?>
        <?php if($cca->cca('admin', 'Act', 'index', $group_id)){ ?>
            <?= link_button_list(__('Listar Documentos'), ['action' => 'index']); ?>
        <?php } if($cca->cca('admin', 'Act', 'add', $group_id)){ ?>
            <?= link_button_add(__('Novo Documento'), ['action' => 'add']); ?>

        <?php } ?>
    </div>
</div>

<div class="act view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($act->title) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Title') ?></td>
                    <td><?= h($act->title) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Arquivo') ?></td>
                    <td><?= h($act->arquivo) ? $this->Html->link($act->arquivo,'/files'.DS.$act->arquivo,['target' => '_blank']) :'' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Visível para') ?></td>
                    <td><?php if($act->is_visible >= 0)
                    {
                        if($act->is_visible == 0)
                        {
                            echo 'Todos';
                        }
                        elseif ($act->is_visible == 1)
                        {
                            echo 'Academias';
                        }
                        elseif ($act->is_visible == 2)
                        {
                            echo 'Professores';
                        }
                        elseif($act->is_visible == 3)
                        {
                            echo 'Alunos';
                        }
                    }; ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($act->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status Id') ?></td>
                    <td><?= $this->Number->format($act->status_id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($act->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($act->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
</div>


