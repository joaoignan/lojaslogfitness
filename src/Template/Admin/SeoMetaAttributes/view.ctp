<div class="seoMetaAttributes view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($seoMetaAttribute->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($seoMetaAttribute->name) ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($seoMetaAttribute->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($seoMetaAttribute->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($seoMetaAttribute->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="column large-12">
        <h4 class="font-bold"><?= __('Related SeoMetaTags') ?></h4>
        <?php if (!empty($seoMetaAttribute->seo_meta_tags)): ?>
            <table class="table table-hover">
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Seo Meta Attribute Id') ?></th>
                    <th><?= __('Name') ?></th>
                    <th><?= __('Content') ?></th>
                    <th><?= __('Controller') ?></th>
                    <th><?= __('Action') ?></th>
                    <th><?= __('Created') ?></th>
                    <th><?= __('Modified') ?></th>
                    <th class="actions"></th>
                </tr>
                <?php foreach ($seoMetaAttribute->seo_meta_tags as $seoMetaTags): ?>
                    <tr>
                        <td><?= h($seoMetaTags->id) ?></td>
                        <td><?= h($seoMetaTags->seo_meta_attribute_id) ?></td>
                        <td><?= h($seoMetaTags->name) ?></td>
                        <td><?= h($seoMetaTags->content) ?></td>
                        <td><?= h($seoMetaTags->controller) ?></td>
                        <td><?= h($seoMetaTags->action) ?></td>
                        <td><?= h($seoMetaTags->created) ?></td>
                        <td><?= h($seoMetaTags->modified) ?></td>
                        <td class="actions">
                            <div class="btn-group">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                                    <i class="glyph-icon icon-navicon"></i>
                                    <span class="sr-only"><?= __('Actions'); ?></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu" style="left: -110px;">
                                    <li>
                                        <?php if($cca->cca('admin', 'SeoMetaTags', 'view', $group_id)){ ?>
                                            <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                                ['SeoMetaTags', 'action' => 'view', $seoMetaTags->id],
                                                ['escape' => false]) ?>
                                        <?php } ?>
                                    </li>
                                    <li>
                                        <?php if($cca->cca('admin', 'SeoMetaTags', 'edit', $group_id)){ ?>
                                            <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                                ['controller' => 'SeoMetaTags', 'action' => 'edit', $seoMetaTags->id],
                                                ['escape' => false]) ?>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'SeoMetaAttribute', 'edit', $group_id)){ ?>
            <?= $this->Html->link(__('Edit Seo Meta Attribute'), ['action' => 'edit', $seoMetaAttribute->id], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'SeoMetaAttributes', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Seo Meta Attributes'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'SeoMetaAttributes', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Seo Meta Attribute'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'SeoMetaTags', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Seo Meta Tags'),
                ['controller' => 'SeoMetaTags', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'SeoMetaTags', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Seo Meta Tag'),
                ['controller' => 'SeoMetaTags', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>