
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'ProdutoPromos', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto Promo'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produtos'),
                ['controller' => 'Produtos', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto'),
                ['controller' => 'Produtos', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Status', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Status'),
                ['controller' => 'Status', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Status', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Status'),
                ['controller' => 'Status', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>

<div class="produtoPromos index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('produto_id') ?></th>
            <th><?= $this->Paginator->sort('image') ?></th>
            <th><?= $this->Paginator->sort('action') ?></th>
            <th><?= $this->Paginator->sort('status_id') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('modified') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($produtoPromos as $produtoPromo): ?>
            <tr>
                <td><?= $this->Number->format($produtoPromo->id) ?></td>
                <td>
                    <?= $produtoPromo->has('produto') ? $this->Html->link($produtoPromo->produto->id, ['controller' => 'Produtos', 'action' => 'view', $produtoPromo->produto->id]) : '' ?>
                </td>
                <td><?= h($produtoPromo->image) ?></td>
                <td><?= h($produtoPromo->action) ?></td>
                <td>
                    <?= $produtoPromo->has('status') ? $this->Html->link($produtoPromo->status->name, ['controller' => 'Status', 'action' => 'view', $produtoPromo->status->id]) : '' ?>
                </td>
                <td><?= h($produtoPromo->created) ?></td>
                <td><?= h($produtoPromo->modified) ?></td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?php if($cca->cca('admin', 'ProdutoPromos', 'view', $group_id)){ ?>
                                    <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                        ['action' => 'view', $produtoPromo->id],
                                        ['escape' => false]) ?>
                                <?php } ?>
                            </li>
                            <li>
                                <?php if($cca->cca('admin', 'ProdutoPromos', 'edit', $group_id)){ ?>
                                    <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                        ['action' => 'edit', $produtoPromo->id],
                                        ['escape' => false]) ?>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>