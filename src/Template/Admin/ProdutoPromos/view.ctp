<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'ProdutoPromos', 'edit', $group_id)){ ?>
            <?= $this->Html->link(__('Edit Produto Promo'), ['action' => 'edit', $produtoPromo->id], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'ProdutoPromos', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produto Promos'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'ProdutoPromos', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto Promo'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produtos'),
                ['controller' => 'Produtos', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto'),
                ['controller' => 'Produtos', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Status', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Status'),
                ['controller' => 'Status', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Status', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Status'),
                ['controller' => 'Status', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>

<div class="produtoPromos view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($produtoPromo->id) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Produto') ?></td>
                    <td><?= $produtoPromo->has('produto') ? $this->Html->link($produtoPromo->produto->id, ['controller' => 'Produtos', 'action' => 'view', $produtoPromo->produto->id]) : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Image') ?></td>
                    <td><?= h($produtoPromo->image) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Action') ?></td>
                    <td><?= h($produtoPromo->action) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status') ?></td>
                    <td><?= $produtoPromo->has('status') ? $this->Html->link($produtoPromo->status->name, ['controller' => 'Status', 'action' => 'view', $produtoPromo->status->id]) : '' ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($produtoPromo->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($produtoPromo->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($produtoPromo->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>