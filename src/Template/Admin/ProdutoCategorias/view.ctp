<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Categorias', 'edit', $group_id)){ ?>
            <?= $this->Html->link(__('Edit Produto Categoria'), ['action' => 'edit', $produtoCategoria->id], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Categorias', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produto Categorias'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Categorias', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto Categoria'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>

<div class="categorias view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($produtoCategoria->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($produtoCategoria->name) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Slug') ?></td>
                    <td><?= h($produtoCategoria->slug) ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($produtoCategoria->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status Id') ?></td>
                    <td><?= $this->Number->format($produtoCategoria->status_id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($produtoCategoria->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($produtoCategoria->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>