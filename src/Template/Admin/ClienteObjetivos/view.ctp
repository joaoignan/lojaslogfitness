
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?= $this->Html->link(__('Edit Cliente Objetivo'), ['action' => 'edit', $clienteObjetivo->id], ['class' => 'btn btn-default']) ?>
        <?= $this->Form->postLink(__('Delete Cliente Objetivo'), ['action' => 'delete', $clienteObjetivo->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $clienteObjetivo->id), 'class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('List Cliente Objetivos'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Cliente Objetivo'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
                    <?= $this->Html->link(__('List Clientes'),
        ['controller' => 'Clientes', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Cliente'),
        ['controller' => 'Clientes', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
                <?= $this->Html->link(__('List Objetivos'),
        ['controller' => 'Objetivos', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Objetivo'),
        ['controller' => 'Objetivos', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
            </div>

</div>

<div class="clienteObjetivos view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($clienteObjetivo->id) ?></h2>
    <div class="row">
        <div class="col-sm-9">
                            <table class="table table-hover">
                                                                        <tr>
                            <td class="font-bold"><?= __('Cliente') ?></td>
                            <td><?= $clienteObjetivo->has('cliente') ? $this->Html->link($clienteObjetivo->cliente->name, ['controller' => 'Clientes', 'action' => 'view', $clienteObjetivo->cliente->id]) : '' ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Objetivo') ?></td>
                            <td><?= $clienteObjetivo->has('objetivo') ? $this->Html->link($clienteObjetivo->objetivo->name, ['controller' => 'Objetivos', 'action' => 'view', $clienteObjetivo->objetivo->id]) : '' ?></td>
                            </tr>
                                                                </table>
                        </div>

                            <div class="col-md-3">
                    <table class="table table-hover">
                                            <tr>
                            <td class="font-bold"><?= __('Id') ?></td>
                            <td><?= $this->Number->format($clienteObjetivo->id) ?></td>
                        </tr>
                                                                                <tr>
                        <td class="font-bold"><?= __('Created') ?></td>
                        <td><?= h($clienteObjetivo->created) ?></td>
                    </tr>
                                                                 </table>
            </div>
        </div>
    </div>


