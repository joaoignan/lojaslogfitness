<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);

$config = unserialize($config->configuration);
?>

<style type="text/css">
    .layout-banner .radio{
        display: inline-block;
        margin-right: 15px;
        margin-left: 15px;
    }
    .layout-banner img{
        width: 350px;
    }
    .layout-banner .radio + .radio{
        margin-top: 10px;
    }
    .jcrop-holder #preview-pane {
        display: block;
        position: absolute;
        z-index: 2000;
        top: 10px;
        right: -280px;
        padding: 6px;
        border: 1px rgba(0,0,0,.4) solid;
        background-color: white;

        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;

        -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
        box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
    }

    #preview-pane .preview-container {
        width: 160px;
        height: 95px;
        overflow: hidden;
    }
    </style>

    <script type="text/javascript">
    $(window).load(function() {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.image_preview').attr('src', e.target.result);
                    console.log(e.target.result);
                    $('.image_preview').Jcrop({
                        boxHeight: 500,
                        boxWidth: 500,
                        setSelect: [0, 0, 0, 0],
                        allowSelect: false,
                        minSize: [20, 85]
                    }, function () {

                        var jcrop_api = this;

                        $(".jcrop-box").attr('type', 'button');

                        $('.image_preview').Jcrop('animateTo',[0,0,4000,4000]);

                        jcrop_api.ui.selection.element.on('cropmove',function(e,s,c){
                            $('#cropx').val(c.x);
                            $('#cropy').val(c.y);
                            $('#cropw').val(c.w);
                            $('#croph').val(c.h);
                            $('#cropwidth').val($('.jcrop-box').width());
                            $('#cropheight').val($('.jcrop-box').height());
                        });
                    });
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }
        
        $("#image_upload").change(function(){
            readURL(this);
        });
    });
    </script>

<div class="configurations form">
    <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row', 'url' => ['controller' => 'Configurations', 'action' => 'salvar_variaveis_globais']]); ?>
    <fieldset>
        <legend><?= __('Configurações Gerais') ?></legend>
        <div class="row">
            <div class="col-xs-12 text-center layout-banner">
                <label class="text-left">Tipo de Banner das lojas</label>
                <div class="radio">
                  <label>
                    <input type="radio" name="tipo_banner" id="tipo_banner1" value="0" <?= $tipo_banner == 0 ? 'checked' : '' ?>>
                    Banner 1920px x 850px <br>
                    <?= $this->Html->image('banner_modelo_1.jpg')  ?>
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" name="tipo_banner" id="tipo_banner2" value="1" <?= $tipo_banner == 1 ? 'checked' : '' ?>>
                    Banner 1920px x 350px <br>
                    <?= $this->Html->image('banner_modelo_2.jpg')  ?>
                  </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="variavel_desconto"><strong>Desconto na loja inteira</strong></label>
                    <?= $this->Form->input('variavel_desconto', [
                        'value'         => $variavel_desconto->valor,
                        'type'          => 'number',
                        'maxlength'     => '2',
                        'label'         => false,
                        'placeholder'   => 'Ex.: 10',
                        'class' => 'form-control validate[optional, maxSize[2]'
                    ]) ?>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="variavel_desconto"><strong>Desconto nos combos</strong></label>
                    <?= $this->Form->input('variavel_desconto_combo', [
                        'value'         => $variavel_desconto_combo->valor,
                        'type'          => 'number',
                        'maxlength'     => '2',
                        'label'         => false,
                        'placeholder'   => 'Ex.: 5%',
                        'class' => 'form-control validate[optional, maxSize[2]'
                    ]) ?>
                </div>
            </div>
        </div>
    
        <?php if ($tipo_gateway->valor == 2){ ?>  
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="form-group">
                        <label for="parcelas_promo"><strong>Parcelas grátis na promoção (valor igual ao definido na promoção com valor de 2 quando sem promoção.)</strong></label>
                        <?= $this->Form->input('parcelas_promo', [
                        'name'  => 'parcelas_promo',
                        'value' => $parcelas_promo,
                        'label' => false,
                        'type'  => 'number',
                        'min'   => 2,
                        'class' => 'form-control validate[optional]'
                        ]) ?>
                    </div>
                </div>
            </div>
        <?php } ?>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="entregas"><strong>Entrega na residência do cliente</strong></label>
                    <?= $this->Form->input('entregas', [
                        'options'  => [
                            0 => 'Não entrego',
                            1 => 'Vendas sem perfil',
                            2 => 'Vendas com perfil',
                            3 => 'Todas as vendas'
                          ],
                        'name'  => 'entregas',
                        'value' => $entregas,
                        'label' => false,
                        'class' => 'form-control'
                    ]) ?>
                </div>
            </div>
        </div>

        <?php //echo $entregas ?>

        <br class="clear">
        <br class="clear">
        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i> <span>'.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-default', 'escape' => false, 'style' => 'width: 9.5em;']);
            ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>