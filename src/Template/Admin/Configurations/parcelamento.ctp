<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);

$config = unserialize($config->configuration);
$faixas = 0;
?>

<div class="configurations form">
    <?= $this->Form->create($configuration, ['class' => 'form-horizontal bordered-row', 'id' => 'form-config-parcelamento']); ?>
    <fieldset>
        <legend><?= __('Sistema :: Configurações > Parcelamento') ?></legend>

        <h4><span class="glyph-icon icon-list"></span> Faixas de Parcelamento</h4>
        <br class="clear">
        <h6 class="font-italicc">
            Obs.: As faixas devem ser adicionadas na sequência e em ordem crescente conforme exemplo abaixo:
            <br>
            <br>
            Faixa 1 => Valor Inicial: 0,01 | Valor Final: 100,00 | Máximo de Parcelas : 2 <br>
            Faixa 2 => Valor Inicial: 100,01 | Valor Final: 500,00 | Máximo de Parcelas : 3 <br>
            Faixa 3 => Valor Inicial: 500,01 | Valor Final: 1000,00 | Máximo de Parcelas : 6 <br>
            Faixa 4 => Valor Inicial: 1000,01 | Valor Final: 9999999,99 | Máximo de Parcelas : 10 <br>
            <br>
            Só é possível realizar a exclusão da última faixa pois é necessário manter a ordem e a sequência das regras aplicadas.
            <br>
            <br>
        </h6>

        <?php
        if(isset($config['parcelas'])):
            foreach ($config['parcelas'] as $p => $parcela):
                $faixas++;
                ?>
                <div class="form-group" id="faixa-<?= $faixas ?>">
                    <div class="col-sm-2 control-label">
                        <?= $this->Form->label('parcelamento_inicial['.$faixas.']', 'Valor Inicial') ?>
                    </div>
                    <div class="col-sm-2">
                        <?= $this->Form->input('parcelamento_inicial['.$faixas.']', [
                            'value'         => $config['parcelamento_inicial'][$faixas],
                            'type'          => 'number',
                            'min'           => 1,
                            'step'          => 1,
                            'maxlength'     => '11',
                            'label'         => false,
                            'placeholder'   => 'Ex.: 12',
                            'class' => 'form-control validate[required, min[1]] text-right parcelamento-inicial'
                        ]) ?>
                    </div>

                    <div class="col-sm-2 control-label">
                        <?= $this->Form->label('parcelamento_final['.$faixas.']', 'Valor Final') ?>
                    </div>
                    <div class="col-sm-2">
                        <?= $this->Form->input('parcelamento_final['.$faixas.']', [
                            'value'         => $config['parcelamento_final'][$faixas],
                            'type'          => 'number',
                            'min'           => 1,
                            'step'          => 1,
                            'maxlength'     => '11',
                            'label'         => false,
                            'placeholder'   => 'Ex.: 12',
                            'class' => 'form-control validate[required, min[1]] text-right parcelamento-final'
                        ]) ?>
                    </div>

                    <div class="col-sm-2 control-label">
                        <?= $this->Form->label('parcelas['.$faixas.']', 'Máximo de Parcelas') ?>
                    </div>
                    <div class="col-sm-1">
                        <?= $this->Form->input('parcelas['.$faixas.']', [
                            'value'         => $config['parcelas'][$faixas],
                            'type'          => 'number',
                            'min'           => 1,
                            'max'           => 15,
                            'step'          => 1,
                            'maxlength'     => '2',
                            'label'         => false,
                            'placeholder'   => 'Ex.: 12',
                            'class' => 'form-control validate[required, min[1]] text-right'
                        ]) ?>
                    </div>
                </div>
                <?php
            endforeach;
        endif;
        ?>

        <div id="faixas-parcelamento"></div>

        <div class="text-right">
            <?= $this->Form->button('<span></span>'.
                '<i class="glyph-icon icon-trash"></i>',
                [
                    'data-faixa'=> $faixas,
                    'id'        => 'delete-faixa-parcelamento',
                    'class'     => 'btn btn-alt btn-danger',
                    'escape'    => false,
                    'type'      => 'button',
                    'title'     => 'Remover última faixa',
                    'style'     => 'top: -50px; right: 40px;'
                ]);
            ?>
        </div>

        <div class="form-group text-center">
               <?= $this->Form->button('<i class="glyph-icon icon-plus-square"></i><span> Adicionar Faixa</span>',
                [
                    'data-faixa'=> $faixas,
                    'id'        => 'add-faixa-parcelamento',
                    'class'     => 'btn btn-alt btn-hoverr btn-primary',
                    'escape'    => false,
                    'type'      => 'button',
                    'style'     => 'top: -20px;'
                ]);
            ?>
        </div>

        <hr>

        <h4><span class="glyph-icon icon-dollar"></span> Taxa de Parcelamento</h4>
        <br class="clear">
        <h6 class="font-italicc">
            Indique se a taxa sobre vendas parceladas devem ser ou não cobradas do cliente (embutida nas parcelas):
            <br>
            <br>
        </h6>

        <div class="col-sm-2 control-label">
            <?= $this->Form->label('transferir_taxa_parcelamento', 'Cobrar do Cliente?') ?>
        </div>
        <div class="col-sm-1">
            <?= $this->Form->checkbox('transferir_taxa_parcelamento', [
                'label'     => false,
                'checked'   => isset($config['transferir_taxa_parcelamento']) ? $config['transferir_taxa_parcelamento'] : true,
                'class'     => 'form-control validate[optional] input-switch',
                'data-on-color' => 'info',
                'data-on-text'  => 'Sim',
                'data-off-text' => 'Não',
                'data-off-color' => 'warning',
                'data-size'     => 'medium',
            ]) ?>
        </div>

        <br class="clear">
        <br class="clear">
        <br class="clear">

        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',           
                ['class' => 'btn btn-alt btn-hoverrr btn-default', 'escape' => false, 'style'  => 'top: 0px; width: 9.5em;']);
            ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>

<div class="hide" id="faixa-parcelamento">
    <div class="form-group" id="faixa-9999">
        <div class="col-sm-2 control-label">
            <?= $this->Form->label('parcelamento_inicial[9999]', 'Valor Inicial') ?>
        </div>
        <div class="col-sm-2">
            <?= $this->Form->input('parcelamento_inicial[9999]', [
                'type'          => 'number',
                'min'           => 1,
                'step'          => 1,
                'maxlength'     => '11',
                'label'         => false,
                'placeholder'   => 'Ex.: 12',
                'class' => 'form-control validate[required, min[1]] text-right parcelamento-inicial'
            ]) ?>
        </div>

        <div class="col-sm-2 control-label">
            <?= $this->Form->label('parcelamento_final[9999]', 'Valor Final') ?>
        </div>
        <div class="col-sm-2">
            <?= $this->Form->input('parcelamento_final[9999]', [
                'type'          => 'number',
                'min'           => 1,
                'step'          => 1,
                'maxlength'     => '11',
                'label'         => false,
                'placeholder'   => 'Ex.: 12',
                'class'         => 'form-control validate[required, min[1]] text-right parcelamento-final'
            ]) ?>
        </div>

        <div class="col-sm-2 control-label">
            <?= $this->Form->label('parcelas[9999]', 'Máximo de Parcelas') ?>
        </div>
        <div class="col-sm-1">
            <?= $this->Form->input('parcelas[9999]', [
                'type'          => 'number',
                'min'           => 1,
                'max'           => 15,
                'step'          => 1,
                'maxlength'     => '2',
                'label'         => false,
                'placeholder'   => 'Ex.: 12',
                'class' => 'form-control validate[required, min[1]] text-right'
            ]) ?>
        </div>
    </div>
</div>