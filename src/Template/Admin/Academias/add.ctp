<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= $this->Html->link(__('Listar Academias'), ['action' => 'index', 'todas'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('Listar Clientes'),
            ['controller' => 'Clientes', 'action' => 'index'],
            ['class' => 'btn btn-default']) ?>
    </div>
</div>

<div class="academias form">
    <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
    <fieldset>
        <legend><?= __('Adicionar Academia') ?></legend>

        <h4 class="font-bold font-italic"><span class="glyph-icon icon-key"></span> Dados Acesso</h4>
        <br class="clear">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('email') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('email', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('cpf_acesso') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('cpf_acesso', [
                    'label' => false,
                    'class' => 'form-control cpf validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('password', 'Senha') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('password', [
                    'id'    => 'nome',
                    'label' => false,
                    'type' => 'password',
                    'readonly' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="divider"></div>
        <h4 class="font-bold font-italic"><span class="glyph-icon icon-list"></span> Dados Loja</h4>
        <br class="clear">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('CNPJ/CPF', 'CNPJ/CPF') ?>
            </div>
            <div class="col-sm-3">
                <input type="radio" class="cnpj_cpf" name="cnpj_cpf" value="cnpj" checked> CNPJ
                <input type="radio" class="cnpj_cpf" name="cnpj_cpf" value="cpf"> CPF
            </div>
        </div>

        <script type="text/javascript">
            $('.cnpj_cpf').click(function() {
                if($(this).val() == 'cnpj') {
                    $('.cnpj-form').removeClass('hidden');
                    $('.cpf-form').addClass('hidden');
                } else {
                    $('.cpf-form').removeClass('hidden');
                    $('.cnpj-form').addClass('hidden');
                }
            });
        </script>

        <div class="form-group cnpj-form">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('cnpj') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('cnpj', [
                    'label' => false,
                    'class' => 'form-control validate[optional,custom[cnpj]] cnpj'
                ]) ?>
            </div>
        </div>

        <div class="form-group cpf-form hidden">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('cpf') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('cpf', [
                    'label' => false,
                    'class' => 'form-control validate[optional,custom[cpf]] cpf'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'id'    => 'nome',
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('shortname') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('shortname', [
                    'id'        => 'name',
                    'maxlenght' => 22,
                    'label'     => false,
                    'class'     => 'form-control validate[required,maxSize[22]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('taxa_comissao', 'Taxa de comissão') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('taxa_comissao', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="divider"></div>
        <h4 class="font-bold font-italic"><span class="glyph-icon icon-phone-square"></span> Contato</h4>
        <br class="clear">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('contact', 'Responsável') ?>
            </div>
            <div class="col-sm-5">
                <?= $this->Form->input('contact', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('phone') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('phone', [
                    'label' => false,
                    'class' => 'form-control validate[required] phone'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('mobile', 'Celular/WhatsApp') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('mobile', [
                    'label' => false,
                    'class' => 'form-control validate[optional] phone'
                ]) ?>
            </div>
        </div>

        <div class="divider"></div>
        <h4 class="font-bold font-italic"><span class="glyph-icon icon-building"></span> Endereço</h4>
        <br class="clear">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('cep') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('cep', [
                    'label' => false,
                    'class' => 'form-control validate[required] cep'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('address') ?>
            </div>
            <div class="col-sm-5">
                <?= $this->Form->input('address', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('number') ?>
            </div>
            <div class="col-sm-1">
                <?= $this->Form->input('number', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('complement') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('complement', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('area') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('area', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('state_id') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('state_id', [
                    'options'       => $states,
                    'id'            => 'states',
                    'empty' => 'Selecione um estado',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('city_id') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('city_id', [
                    'id'            => 'city',
                    'options' => $cities,
                    'empty' => 'Selecione uma cidade',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <?= $this->Form->input('latitude', [
            'id'    => 'latitude-att',
            'type'  => 'hidden',
            'label' => false
        ]) ?>
        <?= $this->Form->input('longitude', [
            'id'    => 'longitude-att',
            'type'  => 'hidden',
            'label' => false
        ]) ?>
        
        <div class="divider"></div>
        <h4 class="font-bold font-italic"><span class="glyph-icon icon-photo"></span> Outros</h4>
        <br class="clear">

        <style type="text/css">
            .jcrop-holder #preview-pane {
                display: block;
                position: absolute;
                z-index: 2000;
                top: 10px;
                right: -280px;
                padding: 6px;
                border: 1px rgba(0,0,0,.4) solid;
                background-color: white;

                -webkit-border-radius: 6px;
                -moz-border-radius: 6px;
                border-radius: 6px;

                -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
                -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
                box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
            }

            #preview-pane .preview-container {
                width: 160px;
                height: 95px;
                overflow: hidden;
            }

            
        </style>

        <script type="text/javascript">
            $(window).load(function() {
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        
                        reader.onload = function (e) {
                            $('.image_preview').attr('src', e.target.result);

                            $('.image_preview').Jcrop({
                                boxHeight: 500,
                                boxWidth: 500,
                                setSelect: [0, 0, 0, 0],
                                allowSelect: false,
                                minSize: [20, 85]
                            }, function () {

                                var jcrop_api = this;

                                $(".jcrop-box").attr('type', 'button');

                                $('.image_preview').Jcrop('animateTo',[0,0,4000,4000]);

                                jcrop_api.ui.selection.element.on('cropmove',function(e,s,c){
                                    $('#cropx').val(c.x);
                                    $('#cropy').val(c.y);
                                    $('#cropw').val(c.w);
                                    $('#croph').val(c.h);
                                    $('#cropwidth').val($('.jcrop-box').width());
                                    $('#cropheight').val($('.jcrop-box').height());
                                });
                            });
                        }
                        
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                
                $("#image_upload").change(function(){
                    readURL(this);
                });
            });
        </script>

        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('imagem', 'Logotipo') ?>
            </div>
            <div class="col-sm-7">
            <?= $this->Form->input('imagem', [
                'id'    => 'image_upload',
                'type'  => 'file',
                'label' => false,
                'class' => 'form-control validate[optional]'
            ]) ?>
            <p><strong>Selecione um arquivo de imagem entre 85x85 e 4000x4000 pixels</strong></p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('image_preview', 'Prévia do Logotipo') ?>
            </div>
            <div class="col-sm-7">
                <?= $this->Html->image('academias/'.$academia->image, [
                    'class'    => 'image_preview'
                ])?>
            </div>
        </div>

        <input type="hidden" name="cropx" id="cropx" value="0" />
        <input type="hidden" name="cropy" id="cropy" value="0" />
        <input type="hidden" name="cropw" id="cropw" value="0" />
        <input type="hidden" name="croph" id="croph" value="0" />
        <input type="hidden" name="cropwidth" id="cropwidth" value="0" />
        <input type="hidden" name="cropheight" id="cropheight" value="0" />

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('alunos', 'Quantidade de Alunos') ?>
            </div>
            <div class="col-sm-1">
                <?= $this->Form->input('alunos', [
                    'placeholder' => '',
                    'label' => false,
                    'class' => 'form-control validate[optional, onlyNumber]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('observacao_acad', 'Observações') ?>
            </div>
            <div class="col-sm-10" id="summernote-observacao-acad">
                <div id="summernote-observacao-acad-text" class="summernote">
                </div>

                <?= $this->Form->input('observacao_acad', [
                    'id'    => 'summernote-observacao-acad-input',
                    'type'  => 'hidden',
                    'label' => false,
                    'class' => 'form-control validate[optional, minSize[3]]'
                ]) ?>
            </div>
        </div>

        <div class="divider"></div>
        <h4 class="box-header"><span class="glyph-icon icon-money" aria-hidden="true"></span> Dados Bancários</h4>
        <br class="clear">
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('favorecido') ?>
            </div>
            <div class="col-sm-7">
                <?= $this->Form->input('favorecido', [
                    'id'            => 'favorecido',
                    'value'         => $academia->favorecido,
                    'disabled'  => false,
                    'readonly'  => false,
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('cpf_favorecido') ?>
            </div>
            <div class="col-sm-7">
                <?= $this->Form->input('cpf_favorecido', [
                    'id'            => 'cpf_favorecido',
                    'value'         => $academia->cpf_favorecido,
                    'disabled'  => false,
                    'readonly'  => false,
                    'label' => false,
                    'class' => 'form-control cpfmask'
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('tipo_conta') ?>
            </div>
            <div class="col-sm-7">
                <?= $this->Form->input('tipo_conta', [
                  'options' => [
                    'Selecione'      => 'Selecione',
                    'Conta Corrente' => 'Conta Corrente',
                    'Conta Poupança' => 'Conta Poupança'
                  ],
                  'class'   => 'form-control',
                  'label'   => false
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('banco') ?>
            </div>
            <div class="col-sm-7">
                <?= $this->Form->input('banco', [
                  'options' => [
                    'Selecione'      => 'Selecione',
                    'Banco do Brasil'   => 'Banco do Brasil',
                    'Bradesco'          => 'Bradesco',
                    'Caixa'             => 'Caixa Econ. Federal',
                    'Itaú'              => 'Itaú',
                    'Santander'         => 'Santander'
                  ],
                  'class'   => 'form-control banco-input',
                  'label'   => false
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('Agência') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('agencia', [
                    'id'            => 'agencia',
                    'value'         => $academia->agencia,
                    'disabled'  => false,
                    'readonly'  => false,
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('Agência digito') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('agencia_dv', [
                    'id'            => 'agencia_dv',
                    'value'         => $academia->agencia_dv,
                    'disabled'  => false,
                    'readonly'  => false,
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('conta') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('conta', [
                    'id'            => 'conta',
                    'value'         => $academia->conta,
                    'disabled'  => false,
                    'readonly'  => false,
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('Conta digito') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('conta_dv', [
                    'id'            => 'conta_dv',
                    'value'         => $academia->conta_dv,
                    'disabled'  => false,
                    'readonly'  => false,
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('Observações') ?>
            </div>
            <div class="col-sm-7">
                <?= $this->Form->textarea('banco_obs', [
                    'id'            => 'banco_obs',
                    'value'         => $academia->banco_obs,
                    'disabled'  => false,
                    'readonly'  => false,
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                [
                    'class' => 'btn btn-alt btn-hover btn-default',
                    'escape' => false
                ]
            ); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


