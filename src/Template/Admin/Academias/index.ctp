<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);

?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_listClientes(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']); ?>
        <?= link_button_add(__('Nova Academia'), ['action' => 'add']); ?>
    </div>
</div>

<div class="academias index">
    <?= $this->Form->create(null)?>
    <div class="row">
        <div class="form-group col-md-6">
            <?= $this->Form->input('search', [
                'div'           => false,
                'label'         => false,
                'class'         => 'form-control',
                'placeholder'   => 'Busca'
            ])?>
        </div>
        <div class="form-group text-left">
            <?= $this->Form->button('<i class="glyph-icon icon-search"></i><span> '.__('Buscar Academia').'</span>',
                [
                    'class' => 'btn btn-alt btn-hoverr btn-default', 'style' =>  'width: 15em;',
                    'escape' => false
                ]) ?>
        </div>
    </div>
    <?= $this->Form->end()?>

    <?= $this->Form->create(null, ['url' => ['controller' => 'academias', 'action' => 'salvar_status_index']])?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('image') ?></th>
            <th><?= $this->Paginator->sort('shortname') ?></th>
            <th><?= $this->Paginator->sort('CNPJ/CPF') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('status_id') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($academias as $academia): ?>
            <tr>
                <td><?= $this->Html->image('academias/'.$academia->image, ['escape' => false, 'style' => 'max-width:40px']); ?></td>
                <td>
                    <?= h($academia->shortname) ?>
                    <br>
                    <small>
                        <?= h($academia->phone).' | '.h($academia->email) ?>
                    </small>
                </td>
                <td><?= $academia->cpf ? $academia->cpf : $academia->cnpj ?></td>
                <td class="text-center">
                    <?= $this->Form->checkbox('status_id['.$academia->id.']', [
                        'label'     => false,
                        'checked'   => $academia->status_id == 1,
                        'class'     => 'form-control validate[optional] input-switch',
                        'data-on-color' => 'info',
                        'data-on-text'  => 'Sim',
                        'data-off-text' => 'Não',
                        'data-off-color' => 'danger',
                        'data-size'     => 'medium',
                    ]) ?>
                </td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                    ['action' => 'view', $academia->id],
                                    ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                    ['action' => 'edit', $academia->id],
                                    ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-external-link"></i> Visitar',
                                    WEBROOT_URL.$academia->slug,
                                    ['escape' => false, 'target' => '_blank']) ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="form-group text-center">
        <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                      ['class' => 'btn btn-alt btn-hoverrr btn-default',  'style' =>  'width: 9.5em;',
                          'escape' => false]);
        ?>
    </div>
    <?= $this->Form->end()?>

    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


