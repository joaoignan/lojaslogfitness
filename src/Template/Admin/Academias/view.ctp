<?php
    $url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=15084130&sCepDestino='. $academia->cep .'&nVlPeso=2&nCdFormato=1&nVlComprimento=20&nVlAltura=5&nVlLargura=15&sCdMaoPropria=s&nVlValorDeclarado=700&sCdAvisoRecebimento=n&nCdServico=41106&nVlDiametro=0&StrRetorno=xml';
     
    $frete_calcula = simplexml_load_string(file_get_contents($url));
    $frete = $frete_calcula->cServico;
     
    if($frete->Erro == '0'){
        $valor = $frete->Valor;
        $prazo = $frete->PrazoEntrega.' dia(s)';
    }elseif($frete->Erro == '7'){
        $retorno = 'Serviço temporariamente indisponível, tente novamente mais tarde.';
    }else{
        $retorno = 'Erro no cálculo do frete, código de erro: '.$frete->Erro;
    }  
?>

<?= $this->Html->css('charts/examples') ?>
<?= $this->Html->script('charts/jquery.flot') ?>
<?= $this->Html->script('charts/jquery.flot.categories') ?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_edit(__('Editar Academia'), ['action' => 'edit', $academia->id]); ?>
        <?= link_button_listAcademias(__('Listar Academias'), ['action' => 'index', 'todas']); ?>
    
    </div>

</div>

<div class="academias view" xmlns="http://www.w3.org/1999/html">
    <h2>#<?= h($academia->id) ?> :: <?= h($academia->name) ?></h2>
    <div class="row">
        <div class="col-sm-6">
            <table class="table table-hover">
                <?php if(isset($academia->image)) { ?>
                    <tr>
                        <td><?= $this->Html->image('academias/'.$academia->image, ['escape' => false, 'style' => 'max-width:200px']); ?></td>
                        <td></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($academia->name) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Shortname') ?></td>
                    <td><?= h($academia->shortname) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('URL Personalizada') ?></td>
                    <td><?= $this->Html->link(WEBROOT_URL.$academia->slug,WEBROOT_URL.$academia->slug,['target' => '_blank']) ?></td>
                </tr>
                <?php if($academia->cpf) { ?>
                    <tr>
                        <td class="font-bold"><?= __('Cpf') ?></td>
                        <td><?= h($academia->cpf) ?></td>
                    </tr>
                <?php } else { ?>
                    <tr>
                        <td class="font-bold"><?= __('Cnpj') ?></td>
                        <td><?= h($academia->cnpj) ?></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td class="font-bold"><?= __('Email') ?></td>
                    <td><?= h($academia->email) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Responsável') ?></td>
                    <td><?= h($academia->contact) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Phone') ?></td>
                    <td><?= h($academia->phone) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Celular/WhatsApp') ?></td>
                    <td><?= h($academia->mobile) ?></td>
                </tr>
            </table>
        </div>

        <div class="col-sm-5">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Address') ?></td>
                    <td><?= h($academia->address) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Number') ?></td>
                    <td><?= h($academia->number) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Complement') ?></td>
                    <td><?= h($academia->complement) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Area') ?></td>
                    <td><?= h($academia->area) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('City') ?></td>
                    <td><?= $academia->city->name.' - '.$academia->city->uf ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Cep') ?></td>
                    <td><?= h($academia->cep) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status') ?></td>
                    <td><?= $academia->has('status') ? $this->Html->link($academia->status->name, ['controller' => 'Status', 'action' => 'view', $academia->status->id]) : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Quantidade de Membros no Time') ?></td>
                    <td><?= h($qtd_time) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Quantidade de Alunos') ?></td>
                    <td><?= h($academia->alunos) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Observações') ?></td>
                    <td><?= h($academia->observacao_acad) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($academia->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($academia->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>