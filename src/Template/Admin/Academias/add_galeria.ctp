<style>
 @media all and (max-width: 450px){
    .no-mobile{
      display: none;
    }
  }
  @media all and (max-width: 1023px){
    .no-small{
      display: none;
    }
  }
</style>

<?php
$myTemplates = [
    'inputContainer' => '{{content}}',
];
$this->Form->templates($myTemplates);
?>
<section class="content">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-body">
                <div class="banners form">
                <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
                <fieldset>
                    <h3 class="box-header">
                    Adicionar Galeria
                    </h3>
                    <div class="form-group">
                        <div class="col-sm-3 control-label">
                            <?= $this->Form->label('title') ?>
                        </div>
                        <div class="col-sm-8">
                            <?= $this->Form->input('title', [
                                            'label' => false,
                                            'class' => 'form-control validate[required, minSize[3], maxSize[255]]']) ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3 control-label">
                            <?= $this->Form->label('imagem', 'Logotipo') ?>
                        </div>
                        <div class="col-sm-8">
                            <?= $this->Form->input('imagem', [
                                'id'    => 'image_upload',
                                'type'  => 'file',
                                'label' => false,
                                'class' => 'form-control validate[required, custom[validateMIME[image/jpeg|image/png]]]'
                            ]) ?>
                            <p><strong>Selecione um arquivo de imagem entre 400x400 e 4000x4000 pixels</strong></p>
                        </div>
                    </div>

                    <div class="text-center">
                        <div class="col-xs-6 col-sm-offset-3 col-sm-3">
                            <?= $this->Form->button('Salvar',
                            ['class' => 'btn btn-success', 'escape' => false]); ?>
                        </div>    
                    <?= $this->Form->end() ?>
                        <div class="col-xs-6 col-sm-3">
                            <?= $this->Html->link('Voltar','/admin/academias/edit/'.$academia->id,
                            ['class' => 'btn bg-red', 'escape' => false]) ?>
                        </div>
                    </div>
                </fieldset>  
                </div>
            </div>
        </div>
    </div>
</section>

