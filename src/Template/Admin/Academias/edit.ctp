
<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<script type="text/javascript">
    $(document).ready(function() {
        $('.name-input').focus();
    });
</script>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
            <?= link_button_listAcademias(__('Listar Academias'), ['action' => 'index', 'todas']); ?>
        <?= link_button_listClientes(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']); ?>
   
    </div>
</div>

<div class="academias form">
    <?= $this->Form->create($academia, ['class' => 'form-horizontal bordered-row form-editar-dados', 'type' => 'file']); ?>
    <fieldset>
        <legend><?= __('Editar Academia') ?></legend>

        <h4 class="font-bold font-italic"><span class="glyph-icon icon-list"></span> Dados Loja</h4>
        <br class="clear">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('CNPJ/CPF', 'CNPJ/CPF') ?>
            </div>
            <div class="col-sm-3">
                <input type="radio" class="cnpj_cpf" name="cnpj_cpf" value="cnpj" <?= $academia->cpf == null ? 'checked' : '' ?>> CNPJ
                <input type="radio" class="cnpj_cpf" name="cnpj_cpf" value="cpf" <?= $academia->cpf != null ? 'checked' : '' ?>> CPF
            </div>
        </div>

        <script type="text/javascript">
            $('.cnpj_cpf').click(function() {
                if($(this).val() == 'cnpj') {
                    $('.cnpj-form').removeClass('hidden');
                    $('.cpf-form').addClass('hidden');
                } else {
                    $('.cpf-form').removeClass('hidden');
                    $('.cnpj-form').addClass('hidden');
                }
            });
        </script>

        <div class="form-group cnpj-form <?= $academia->cpf == null ? '' : 'hidden' ?>">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('cnpj') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('cnpj', [
                    'label' => false,
                    'class' => 'form-control validate[optional,custom[cnpj]] cnpj'
                ]) ?>
            </div>
        </div>

        <div class="form-group cpf-form <?= $academia->cpf != null ? '' : 'hidden' ?>">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('cpf') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('cpf', [
                    'label' => false,
                    'class' => 'form-control validate[optional,custom[cpf]] cpf'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'id'    => 'nome',
                    'label' => false,
                    'class' => 'form-control name-input validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('shortname') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('shortname', [
                    'id'        => 'name',
                    'maxlenght' => 22,
                    'label'     => false,
                    'class'     => 'form-control validate[required,maxSize[22]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('URL personalizada') ?>
            </div>
            <div class="col-sm-6">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">emporionatural.com.br/</span>
                    <?= $this->Form->input('slug', [
                        'id'        => 'slug',
                        'maxlenght' => 22,
                        'label'     => false,
                        'class'     => 'form-control validate[required,maxSize[22]]'
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('taxa_comissao', 'Taxa de comissão') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('taxa_comissao', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('email', 'E-mail') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('email', [
                    'label' => false,
                    'value' => $academia->email,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="divider"></div>
        <h4 class="font-bold font-italic"><span class="glyph-icon icon-phone-square"></span> Contato</h4>
        <br class="clear">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('contact', 'Responsável') ?>
            </div>
            <div class="col-sm-5">
                <?= $this->Form->input('contact', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('phone') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('phone', [
                    'label' => false,
                    'class' => 'form-control validate[required] phone'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('mobile', 'Celular/WhatsApp') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('mobile', [
                    'label' => false,
                    'class' => 'form-control validate[optional] phone'
                ]) ?>
            </div>
        </div>

        <div class="divider"></div>
        <h4 class="font-bold font-italic"><span class="glyph-icon icon-building"></span> Endereço</h4>
        <br class="clear">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('cep') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('cep', [
                    'label' => false,
                    'class' => 'form-control validate[required] cep'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('address') ?>
            </div>
            <div class="col-sm-5">
                <?= $this->Form->input('address', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('number') ?>
            </div>
            <div class="col-sm-1">
                <?= $this->Form->input('number', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('complement') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('complement', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('area') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('area', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('state_id') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('state_id', [
                    'options'       => $states,
                    'id'            => 'states',
                    'empty' => 'Selecione um estado',
                    'value' => $academia->city->state_id,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('city_id') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('city_id', [
                    'id'            => 'city',
                    'options' => $cities,
                    'empty' => 'Selecione uma cidade',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <?= $this->Form->input('latitude', [
            'id'    => 'latitude-att',
            'type'  => 'hidden',
            'label' => false
        ]) ?>
        <?= $this->Form->input('longitude', [
            'id'    => 'longitude-att',
            'type'  => 'hidden',
            'label' => false
        ]) ?>
        
        <div class="divider"></div>
        <h4 class="font-bold font-italic"><span class="glyph-icon icon-photo"></span> Outros</h4>
        <br class="clear">

        <style type="text/css">
            .jcrop-holder #preview-pane {
                display: block;
                position: absolute;
                z-index: 2000;
                top: 10px;
                right: -280px;
                padding: 6px;
                border: 1px rgba(0,0,0,.4) solid;
                background-color: white;

                -webkit-border-radius: 6px;
                -moz-border-radius: 6px;
                border-radius: 6px;

                -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
                -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
                box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
            }

            #preview-pane .preview-container {
                width: 160px;
                height: 95px;
                overflow: hidden;
            }

            
        </style>

        <script type="text/javascript">
            $(window).load(function() {
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        
                        reader.onload = function (e) {
                            $('.image_preview').attr('src', e.target.result);

                            $('.image_preview').Jcrop({
                                boxHeight: 500,
                                boxWidth: 500,
                                setSelect: [0, 0, 0, 0],
                                allowSelect: false,
                                minSize: [20, 85]
                            }, function () {

                                var jcrop_api = this;

                                $(".jcrop-box").attr('type', 'button');

                                $('.image_preview').Jcrop('animateTo',[0,0,4000,4000]);

                                jcrop_api.ui.selection.element.on('cropmove',function(e,s,c){
                                    $('#cropx').val(c.x);
                                    $('#cropy').val(c.y);
                                    $('#cropw').val(c.w);
                                    $('#croph').val(c.h);
                                    $('#cropwidth').val($('.jcrop-box').width());
                                    $('#cropheight').val($('.jcrop-box').height());
                                });
                            });
                        }
                        
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                
                $("#image_upload").change(function(){
                    readURL(this);
                });
            });
        </script>

        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('imagem', 'Logotipo') ?>
            </div>
            <div class="col-sm-7">
            <?= $this->Form->input('imagem', [
                'id'    => 'image_upload',
                'type'  => 'file',
                'label' => false,
                'class' => 'form-control validate[optional]'
            ]) ?>
            <p><strong>Selecione um arquivo de imagem entre 85x85 e 4000x4000 pixels</strong></p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('image_preview', 'Prévia do Logotipo') ?>
            </div>
            <div class="col-sm-7">
                <?= $this->Html->image('academias/'.$academia->image, [
                    'class'    => 'image_preview'
                ])?>
            </div>
        </div>

        <input type="hidden" name="cropx" id="cropx" value="0" />
        <input type="hidden" name="cropy" id="cropy" value="0" />
        <input type="hidden" name="cropw" id="cropw" value="0" />
        <input type="hidden" name="croph" id="croph" value="0" />
        <input type="hidden" name="cropwidth" id="cropwidth" value="0" />
        <input type="hidden" name="cropheight" id="cropheight" value="0" />

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('alunos', 'Quantidade de Alunos') ?>
            </div>
            <div class="col-sm-1">
                <?= $this->Form->input('alunos', [
                    'placeholder' => '',
                    'label' => false,
                    'class' => 'form-control validate[optional, onlyNumber]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('observacao_acad', 'Observações') ?>
            </div>
            <div class="col-sm-10">
                <?= $this->Form->input('observacao_acad', [
                    'value' => $academia->observacao_acad,
                    'label' => false,
                    'class' => 'form-control validate[optional, minSize[3]]'
                ]) ?>
            </div>
        </div>
        <div class="divider"></div>
        <h4 class="box-header"><span class="glyph-icon icon-money" aria-hidden="true"></span> Dados Bancários</h4>
        <br class="clear">
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('favorecido') ?>
            </div>
            <div class="col-sm-7">
                <?= $this->Form->input('favorecido', [
                    'id'            => 'favorecido',
                    'value'         => $academia->favorecido,
                    'disabled'  => false,
                    'readonly'  => false,
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('cpf_favorecido') ?>
            </div>
            <div class="col-sm-7">
                <?= $this->Form->input('cpf_favorecido', [
                    'id'            => 'cpf_favorecido',
                    'value'         => $academia->cpf_favorecido,
                    'disabled'  => false,
                    'readonly'  => false,
                    'label' => false,
                    'class' => 'form-control cpfmask'
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('tipo_conta') ?>
            </div>
            <div class="col-sm-7">
                <?= $this->Form->input('tipo_conta', [
                  'options' => [
                    'Selecione'      => 'Selecione',
                    'Conta Corrente' => 'Conta Corrente',
                    'Conta Poupança' => 'Conta Poupança'
                  ],
                  'class'   => 'form-control',
                  'label'   => false
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('banco') ?>
            </div>
            <div class="col-sm-7">
                <?= $this->Form->input('banco', [
                  'options' => [
                    'Selecione'      => 'Selecione',
                    'Banco do Brasil'   => 'Banco do Brasil',
                    'Bradesco'          => 'Bradesco',
                    'Caixa'             => 'Caixa Econ. Federal',
                    'Itaú'              => 'Itaú',
                    'Santander'         => 'Santander'
                  ],
                  'class'   => 'form-control banco-input',
                  'label'   => false
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('Agência') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('agencia', [
                    'id'            => 'agencia',
                    'value'         => $academia->agencia,
                    'disabled'  => false,
                    'readonly'  => false,
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('Agência digito') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('agencia_dv', [
                    'id'            => 'agencia_dv',
                    'value'         => $academia->agencia_dv,
                    'disabled'  => false,
                    'readonly'  => false,
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('conta') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('conta', [
                    'id'            => 'conta',
                    'value'         => $academia->conta,
                    'disabled'  => false,
                    'readonly'  => false,
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('Conta digito') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('conta_dv', [
                    'id'            => 'conta_dv',
                    'value'         => $academia->conta_dv,
                    'disabled'  => false,
                    'readonly'  => false,
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-md-2 control-label">
                <?= $this->Form->label('Observações') ?>
            </div>
            <div class="col-sm-7">
                <?= $this->Form->textarea('banco_obs', [
                    'id'            => 'banco_obs',
                    'value'         => $academia->banco_obs,
                    'disabled'  => false,
                    'readonly'  => false,
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
        </div>
        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                [
                    'class' => 'btn btn-alt btn-hover btn-default',
                    'escape' => false
                ]
            ); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.btn-editar-dados').click(function(e) {

            $('.error-bancario').remove();
            var error = 0;

            if($('.agencia-input').val().length < 4) {
                $('.agencia-input').parent().append('<p class="error-bancario" style="color: red">o número da agência deve ter no mínimo 4 dígitos. Exemplo: 9999</p>');
                $('.agencia-input').focus();
                error = 1;
            }
            if($('.conta-dv-input').val().length <= 0) {
                $('.conta-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da conta!</p>');
                $('.conta-dv-input').focus();
                error = 1;
            }

            if($('.banco-input').val() == 'Banco do Brasil') {
                if($('.agencia-dv-input').val().length <= 0) {
                    $('.agencia-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da agencia!</p>');
                    $('.agencia-dv-input').focus();
                    error = 1;
                }
                if($('.conta-input').val().length < 8) {
                    $('.conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                    $('.conta-input').focus();
                    error = 1;
                }
            } else if($('.banco-input').val() == 'Bradesco') {
                if($('.agencia-dv-input').val().length <= 0) {
                    $('.agencia-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da agencia!</p>');
                    $('.agencia-dv-input').focus();
                    error = 1;
                }
                if($('.conta-input').val().length < 8) {
                    $('.conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                    $('.conta-input').focus();
                    error = 1;
                }
            } else if($('.banco-input').val() == 'Caixa') {
                if($('.conta-input').val().length < 11) {
                    $('.conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 11 dígitos. Exemplo: XXX99999999 (X: Operação)</p>');
                    $('.conta-input').focus();
                    error = 1;
                }
            } else if($('.banco-input').val() == 'Itaú') {
                if($('.conta-input').val().length < 5) {
                    $('.conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 5 dígitos. Exemplo: 99999</p>');
                    $('.conta-input').focus();
                    error = 1;
                }
            } else if($('.banco-input').val() == 'Santander') {
                if($('.conta-input').val().length < 8) {
                    $('.conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                    $('.conta-input').focus();
                    error = 1;
                }
            }

            if(error == 1) {
            return false;
            }

            var form = $('.form-editar-dados');
            var input_latitude  = $('#latitude-att');
            var input_longitude = $('#longitude-att');

            var uf = '';
            var state_id = parseInt($('.uf-map').val());
            var cidade_text = $('.city-map option[value='+$('.city-map').val()+']').text();

            switch (state_id){
                case 1  : uf = 'AC';  break;
                case 2  : uf = 'AL';  break;
                case 3  : uf = 'AM';  break;
                case 4  : uf = 'AP';  break;
                case 5  : uf = 'BA';  break;
                case 6  : uf = 'CE';  break;
                case 7  : uf = 'DF';  break;
                case 8  : uf = 'ES';  break;
                case 9  : uf = 'GO';  break;
                case 10 : uf = 'MA';  break;
                case 11 : uf = 'MG';  break;
                case 12 : uf = 'MS';  break;
                case 13 : uf = 'MT';  break;
                case 14 : uf = 'PA';  break;
                case 15 : uf = 'PB';  break;
                case 16 : uf = 'PE';  break;
                case 17 : uf = 'PI';  break;
                case 18 : uf = 'PR';  break;
                case 19 : uf = 'RJ';  break;
                case 20 : uf = 'RN';  break;
                case 21 : uf = 'RO';  break;
                case 22 : uf = 'RR';  break;
                case 23 : uf = 'RS';  break;
                case 24 : uf = 'SC';  break;
                case 25 : uf = 'SE';  break;
                case 26 : uf = 'SP';  break;
                case 27 : uf = 'TO';  break;
                default : uf = '';
            }

            var academia_address = $('.address-map').val() + ','
                + $('.number-map').val() + ','
                + $('.area-map').val() + ','
                + $('.city-map option[value='+$('.city-map').val()+']').text() + ','
                + uf + ', Brasil';

            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + academia_address + '&sensor=false',
                null,
                function (data) {
                    var p = [];
                    if (data.results[0].geometry.location) {
                        p = data.results[0].geometry.location;
                    } else {
                        p = data.results[0]['geometry']['location'];
                    }
                    
                    input_latitude.val(p.lat);
                    input_longitude.val(p.lng);
                        
                    form.get(0).submit();
                });
            return false;
        });
    });
</script>