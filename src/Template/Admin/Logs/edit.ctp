<?php
$myTemplates = [
    'inputContainer' => '{{content}}',
];
$this->Form->templates($myTemplates);
?>

            <div class="logs form">
            <?= $this->Form->create($log, ['class' => 'form-horizontal bordered-row']); ?>
            <fieldset>
                <legend><?= __('Edit Log') ?></legend>
                <?php
                                echo '<div class="form-group">
                        <div class="col-sm-2 control-label">
                            '.$this->Form->label('controller').'
                        </div>
                        <div class="col-sm-6">'.
                $this->Form->input('controller', [
                'label' => false,
                'class' => 'form-control'
                ]).
                '</div>
                </div>';

                                echo '<div class="form-group">
                        <div class="col-sm-2 control-label">
                            '.$this->Form->label('action').'
                        </div>
                        <div class="col-sm-6">'.
                $this->Form->input('action', [
                'label' => false,
                'class' => 'form-control'
                ]).
                '</div>
                </div>';

                                echo '<div class="form-group">
                        <div class="col-sm-2 control-label">
                            '.$this->Form->label('registro').'
                        </div>
                        <div class="col-sm-6">'.
                $this->Form->input('registro', [
                'label' => false,
                'class' => 'form-control'
                ]).
                '</div>
                </div>';

                                echo '<div class="form-group">
                        <div class="col-sm-2 control-label">
                            '.$this->Form->label('detalhes').'
                        </div>
                        <div class="col-sm-6">'.
                $this->Form->input('detalhes', [
                'label' => false,
                'class' => 'form-control'
                ]).
                '</div>
                </div>';

                                echo '<div class="form-group">
                        <div class="col-sm-2 control-label">
                            '.$this->Form->label('user_id').'
                        </div>
                        <div class="col-sm-6">'.
                $this->Form->input('user_id', [
                'options' => $users,
                'empty' => true,
                'label' => false,
                'class' => 'form-control chosen-select'
                ]).
                '</div>
                </div>';

                                ?>
                <div class="form-group text-center">
                    <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                    '<i class="glyph-icon icon-arrow-right"></i>',
                    ['class' => 'btn btn-alt btn-hover btn-default',
                    'escape' => false]); ?>
                </div>
            </fieldset>
    <?= $this->Form->end() ?>
</div>


<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
                <?= $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $log->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $log->id),
        'class' => 'btn btn-default']
        )
        ?>
                    <?= $this->Html->link(__('List Logs'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
                    <?= $this->Html->link(__('List Users'),
            ['controller' => 'Users', 'action' => 'index'],
            ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New User'),
            ['controller' => 'Users', 'action' => 'add'],
            ['class' => 'btn btn-default']) ?>
            </div>
</div>