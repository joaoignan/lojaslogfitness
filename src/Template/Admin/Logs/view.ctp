
<div class="logs view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($log->id) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Controller') ?></td>
                    <td><?= h($log->controller) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Action') ?></td>
                    <td><?= h($log->action) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Detalhes') ?></td>
                    <td><?= h($log->detalhes) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('User') ?></td>
                    <td><?= $log->has('user') ? $this->Html->link($log->user->name, ['controller' => 'Users', 'action' => 'view', $log->user->id]) : '' ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($log->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Registro') ?></td>
                    <td><?= $this->Number->format($log->registro) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($log->created) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Logs', 'edit', $group_id)){ ?>
            <?= $this->Html->link(__('Edit Log'), ['action' => 'edit', $log->id], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Logs', 'delete', $group_id)){ ?>
            <?= $this->Form->postLink(__('Delete Log'), ['action' => 'delete', $log->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $log->id), 'class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Logs', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Logs'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Logs', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Log'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Users', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Users'),
                ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Users', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New User'),
                ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>