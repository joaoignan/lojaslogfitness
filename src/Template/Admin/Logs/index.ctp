
<div class="logs index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('controller') ?></th>
            <th><?= $this->Paginator->sort('action') ?></th>
            <th><?= $this->Paginator->sort('registro') ?></th>
            <th><?= $this->Paginator->sort('detalhes') ?></th>
            <th><?= $this->Paginator->sort('user_id') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($logs as $log): ?>
            <tr>
                <td><?= $this->Number->format($log->id) ?></td>
                <td><?= h($log->created) ?></td>
                <td><?= h($log->controller) ?></td>
                <td><?= h($log->action) ?></td>
                <td><?= $this->Number->format($log->registro) ?></td>
                <td><?= h($log->detalhes) ?></td>
                <td>
                    <?= $log->has('user') ? $this->Html->link($log->user->name, ['controller' => 'Users', 'action' => 'view', $log->user->id]) : '' ?>
                </td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?php if($cca->cca('admin', 'Logs', 'view', $group_id)){ ?>
                                    <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                        ['action' => 'view', $log->id],
                                        ['escape' => false]) ?>
                                <?php } ?>
                            </li>
                            <li>
                                <?php if($cca->cca('admin', 'Logs', 'edit', $group_id)){ ?>
                                    <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                        ['action' => 'edit', $log->id],
                                        ['escape' => false]) ?>
                                <?php } ?>
                            </li>
                            <li>
                                <?php if($cca->cca('admin', 'Logs', 'delete', $group_id)){ ?>
                                    <?= $this->Form->postLink('<i class="glyph-icon icon-trash-o"></i>  '.__('Delete'),
                                        ['action' => 'delete', $log->id],
                                        ['confirm' => __('Are you sure you want to delete # {0}?', $log->id), 'escape' => false]) ?>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Log', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Log'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Users', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Users'),
                ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Users', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New User'),
                ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>