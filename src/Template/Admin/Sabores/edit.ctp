<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'Sabores', 'index', $group_id)){ ?>
            <?= link_button_list(__('Listar Sabores'), ['action' => 'index']); ?>
        <?php } if($cca->cca('admin', 'Sabores', 'add', $group_id)){ ?>
            <?= link_button_add(__('Novo Sabor'), ['action' => 'add']); ?>
        <?php } ?>
    </div>
</div>

<div class="objetivos form">
    <?= $this->Form->create($sabor, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Editar Objetivo') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'id'    => 'name',
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[3], maxSize[500]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>