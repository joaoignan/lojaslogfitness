<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">

        <?= link_button_listContatos(__('Listar Contatos Recebidos'), ['action' => 'index']); ?>

    </div>

</div>

<div class="relacionamentoContatos view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($relacionamentoContato->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($relacionamentoContato->name) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Phone') ?></td>
                    <td><?= h($relacionamentoContato->phone) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Email') ?></td>
                    <td><?= h($relacionamentoContato->email) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Description') ?></td>
                    <td><?= h($relacionamentoContato->description) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Mensagem') ?></td>
                    <td><?= $this->Text->autoParagraph(h($relacionamentoContato->message)); ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($relacionamentoContato->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($relacionamentoContato->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($relacionamentoContato->modified) ?></td>
                </tr>


            </table>
        </div>
    </div>
</div>


