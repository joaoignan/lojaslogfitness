<div class="relacionamentoContatos index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('Mensagem') ?></th>
            <!--<th><?= $this->Paginator->sort('status_id') ?></th>-->
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($relacionamentoContatos as $relacionamentoContato): ?>
            <tr>
                <td><?= $this->Number->format($relacionamentoContato->id) ?></td>
                <td><?= strtoupper(h($relacionamentoContato->name)) ?>
                    <br>
                    <small class="font-italic"><?= h($relacionamentoContato->phone) ?></small>
                    <br>
                    <small class="font-italic"><?= h($relacionamentoContato->email) ?></small>
                </td>
                <td>
                    <small><?= strtoupper(h($relacionamentoContato->description)) ?></small>
                    <br>
                    <small class="font-italic" style="font-size: 10px"><?= h($relacionamentoContato->created) ?></small>
                    <br>
                    <?= h($relacionamentoContato->message) ?>
                </td>
                <!--<td>
                    <?= $relacionamentoContato->has('status') ? $this->Html->link($relacionamentoContato->status->name, ['controller' => 'Status', 'action' => 'view', $relacionamentoContato->status->id]) : '' ?>
                </td>-->
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                    ['action' => 'view', $relacionamentoContato->id],
                                    ['escape' => false]) ?>
                            </li>
                            <!--<li>
                                <?= $this->Form->postLink('<i class="glyph-icon icon-trash-o"></i>  '.__('Delete'),
                                ['action' => 'delete', $relacionamentoContato->id],
                                ['confirm' => __('Are you sure you want to delete # {0}?', $relacionamentoContato->id), 'escape' => false]) ?>
                            </li>-->
                        </ul>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


