<div class="slides view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($slide->title) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Title') ?></td>
                    <td><?= h($slide->title) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Image') ?></td>
                    <td><?= $this->Html->image('slides/'.$slide->image, ['style' => 'max-width: 600px; max-height: 200px'])  ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Link') ?></td>
                    <td><?= h($slide->link) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status') ?></td>
                    <td><?= $slide->has('status') ? $this->Html->link($slide->status->name, ['controller' => 'Status', 'action' => 'view', $slide->status->id]) : '' ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($slide->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($slide->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($slide->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Slides', 'edit', $group_id)){ ?>
            <?= $this->Html->link(__('Edit Slide'), ['action' => 'edit', $slide->id], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Slides', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Slides'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Slides', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Slide'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>