<div class="slides index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('title') ?></th>
            <th><?= $this->Paginator->sort('image') ?></th>
            <th><?= $this->Paginator->sort('link') ?></th>
            <th><?= $this->Paginator->sort('status_id') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('modified') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($slides as $slide): ?>
            <tr>
                <td><?= $this->Number->format($slide->id) ?></td>
                <td><?= h($slide->title) ?></td>
                <td><?= $this->Html->image('slides/'.$slide->image, ['style' => 'max-width: 150px; max-height: 50px'])  ?></td>
                <td><?= h($slide->link) ?></td>
                <td>
                    <?= $slide->has('status') ? $this->Html->link($slide->status->name, ['controller' => 'Status', 'action' => 'view', $slide->status->id]) : '' ?>
                </td>
                <td><?= h($slide->created) ?></td>
                <td><?= h($slide->modified) ?></td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <?php if($cca->cca('admin', 'Slides', 'view', $group_id)){ ?>
                                <li>
                                    <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                        ['action' => 'view', $slide->id],
                                        ['escape' => false]) ?>
                                </li>
                            <?php } if($cca->cca('admin', 'Slides', 'edit', $group_id)){ ?>
                                <li>
                                    <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                        ['action' => 'edit', $slide->id],
                                        ['escape' => false]) ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Slides', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Slide'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>