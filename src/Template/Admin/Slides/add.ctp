<?php
$myTemplates = [
    'inputContainer' => '{{content}}',
];
$this->Form->templates($myTemplates);
?>

<div class="slides form">
    <?= $this->Form->create($slide, ['type' => 'file', 'class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Add Slide') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('title') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('title', [
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[3], maxSize[20]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('imagem') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('imagem', [
                    'type'  => 'file',
                    'label' => false,
                    'class' => 'form-control validate[required, custom[validateMIME[image/jpeg|image/png]]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('background') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('background', [
                    'id' => 'colorpicker-br',
                    'label' => false,
                    'class' => 'form-control minicolors validate[required, minSize[3], maxSize[255]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('link') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('link', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('status_id') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('status_id', [
                    'options' => $status,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Slides', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Slides'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>