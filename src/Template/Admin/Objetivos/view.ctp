<div class="objetivos view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($objetivo->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($objetivo->name) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status') ?></td>
                    <td><?= $objetivo->has('status') ? $this->Html->link($objetivo->status->name, ['controller' => 'Status', 'action' => 'view', $objetivo->status->id]) : '' ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($objetivo->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($objetivo->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($objetivo->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="column large-12">
        <h4 class="font-bold"><?= __('Related ProdutoObjetivos') ?></h4>
        <?php if (!empty($objetivo->produto_objetivos)): ?>
            <table class="table table-hover">
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Produto Base Id') ?></th>
                    <th><?= __('Objetivo Id') ?></th>
                    <th><?= __('Created') ?></th>
                    <th class="actions"></th>
                </tr>
                <?php foreach ($objetivo->produto_objetivos as $produtoObjetivos): ?>
                    <tr>
                        <td><?= h($produtoObjetivos->id) ?></td>
                        <td><?= h($produtoObjetivos->produto_base_id) ?></td>
                        <td><?= h($produtoObjetivos->objetivo_id) ?></td>
                        <td><?= h($produtoObjetivos->created) ?></td>
                        <td class="actions">
                            <div class="btn-group">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                                    <i class="glyph-icon icon-navicon"></i>
                                    <span class="sr-only"><?= __('Actions'); ?></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu" style="left: -110px;">
                                    <li>
                                        <?php if($cca->cca('admin', 'ProdutoObjetivos', 'view', $group_id)){ ?>
                                            <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                                ['ProdutoObjetivos', 'action' => 'view', $produtoObjetivos->id],
                                                ['escape' => false]) ?>
                                        <?php } ?>
                                    </li>
                                    <li>
                                        <?php if($cca->cca('admin', 'ProdutoObjetivos', 'edit', $group_id)){ ?>
                                            <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                                ['controller' => 'ProdutoObjetivos', 'action' => 'edit', $produtoObjetivos->id],
                                                ['escape' => false]) ?>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Objetivos', 'edit', $group_id)){ ?>
            <?= $this->Html->link(__('Edit Objetivo'), ['action' => 'edit', $objetivo->id], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Objetivos', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Objetivos'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Objetivos', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Objetivo'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Status', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Status'),
                ['controller' => 'Status', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Status', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Status'),
                ['controller' => 'Status', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'ProdutoObjetivos', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produto Objetivos'),
                ['controller' => 'ProdutoObjetivos', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'ProdutoObjetivos', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto Objetivo'),
                ['controller' => 'ProdutoObjetivos', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>