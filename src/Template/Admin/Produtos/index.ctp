<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
       <?php if($cca->cca('admin', 'Produtos', 'add', $group_id)){ ?>
            <?= link_button_importCSV(__('Importar CSV'), ['action' => 'import_csv']); ?>
        <?php } if($cca->cca('admin', 'Produtos', 'import_csv', $group_id)){ ?>
            <?= link_button_add(__('Novo Produto'), ['action' => 'add']); ?>
        <?php } ?>
    </div>
</div>

<div class="produtos index">

    <?= $this->Form->create(null)?>
    <div class="row">
        <div class="form-group col-md-6">
            <?= $this->Form->input('search', [
                'div'           => false,
                'label'         => false,
                'class'         => 'form-control',
                'placeholder'   => 'Busca'
            ])?>
        </div>
        <div class="form-group text-left">
            <?= $this->Form->button('<i class="glyph-icon icon-search"></i><span> '.__('Buscar Produto').'</span>',
                [
                    'class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 15em;',
                    'escape' => false
                ])
            ?>
        </div>
    </div>
    <?= $this->Form->end()?>

    <table class="table table-hover">
        <thead>
        <tr>
            <th class="text-center"><?= $this->Paginator->sort('id') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('cod_interno', 'Cod ERP') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('produto_base_id') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('alterado') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('sabor_id') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('propriedade') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('marca') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('embalagem') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('tamanho') ?></th>
            <th class="text-right"><?= $this->Paginator->sort('preco', 'Preço') ?></th>
            <th class="text-right"><?= $this->Paginator->sort('status_id') ?></th>
            <th class="text-right"><?= $this->Paginator->sort('visivel') ?></th>
            <th class="text-right"><?= $this->Paginator->sort('forma_consumo') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('quando_tomar') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('fotos', 'extensao') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('views', 'visualizações') ?></th>
            <th class="actions text-center"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($produtos as $produto): ?>
            <?php $fotos = unserialize($produto->fotos); ?>
            <?php $foto_principal = explode('.', $fotos[0]); ?>
            <?php $extensao = $foto_principal[1]; ?>
            <tr>
                <td class="text-center"><?= $this->Number->format($produto->id) ?></td>
                <td class="text-center"><?= $this->Number->format($produto->cod_interno) ?></td>
                <td class="text-center"><?= $produto->produto_base->name ?></td> 
                <td class="text-center"><?= $produto->alterado ? '<span style="color: green">Sim</span>' : '<span style="color: red">Não</span>' ?></td>
                <?php if($produto->sabor_id != null) { ?>
                    <?php foreach ($sabores as $sabor) { ?>
                        <?php if($produto->sabor_id == $sabor->id) { ?>
                            <td class="text-center"><?= $sabor->name ?></td>
                        <?php } ?>
                    <?php } ?>
                <?php } else { ?>
                    <td class="text-center"></td>
                <?php } ?>
                <td class="text-center"><?= $produto->propriedade ?></td>
                <td class="text-center"><?= $produto->produto_base->marca->name ?></td>
                <td class="text-center"><?= $produto->embalagem ?></td>
                <td class="text-center"><?= $produto->tamanho ?> <?= $produto->unidade_medida ?></td>
                <td class="text-right">R$ <?= number_format($produto->preco, 2, ',', '.') ?></td>
                <td class="text-center"><?= h($produto->status->name) ?></td>
                <td class="text-center"><?= h($produto->visivel ? 'Sim' : 'Não') ?></td>
                <td class="text-center"><?= h($produto->produto_base->forma_consumo) ?></td>
                <td class="text-center"><?= h($produto->produto_base->quando_tomar) ?></td>
                <td class="text-center"><?= $extensao ?></td>
                <td class="text-center"><?= $produto->views ?></td>
                <td class="actions text-center">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?php if($cca->cca('admin', 'Produtos', 'view', $group_id)){ ?>
                                    <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                        ['action' => 'view', $produto->id],
                                        ['escape' => false]) ?>
                                <?php } ?>
                            </li>
                            <li>
                                <?php if($cca->cca('admin', 'Produtos', 'edit', $group_id)){ ?>
                                    <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                        ['action' => 'edit', $produto->id],
                                        ['escape' => false]) ?>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>