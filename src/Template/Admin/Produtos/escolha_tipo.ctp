<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>
<style>
	.selecionaTipo img{
		width: 200px;
		height: 250px;
	    -webkit-filter: grayscale(100%);
	    transition: all 0.5s;
	    cursor: pointer;
	    border: 2px solid #0d7548;
	}
	.selecionaTipo img:hover{
		-webkit-filter: grayscale(0%);
	}
</style>
<div class="produtos form">
	<div class="row">
		<p>código aqui:</p>
		<?= $cod_barras ?>
	</div>
    <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset class="text-center">
        <legend><?= __('Tipo de produto') ?></legend>
        <br class="clear">
        <div class="row">
            <div class="text-center col-xs-12 col-sm-6 col-md-4 col-md-offset-2 col-lg-3 col-lg-offset-3">
				<span class="selecionaTipo">
					<?= $this->Html->link($this->Html->image('tipo_suplemento.jpg'),['controller' => 'Produtos', 'action' => 'add', $cod_barras],['escape' => false]) ?>
				</span>
            	<h3>Suplemento</h3>
            </div>
            <div class="text-center col-xs-12 col-sm-6 col-md-4 col-lg-3">
				<span class="selecionaTipo">
					<?= $this->Html->link($this->Html->image('tipo_acessorios.jpg'),['controller' => 'Produtos', 'action' => 'add_acessorio', $cod_barras],['escape' => false]) ?>
				</span>
            	<h3>Acessório</h3>
            </div>
        </div> 
    </fieldset>
    <?= $this->Form->end() ?>
</div>