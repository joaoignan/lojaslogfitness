<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>
<script>
    $(document).ready(function(){
        $("#checkSubcategoriasMae").click(function(){
            $(".checkSubcategoriasFilha").prop('checked', $(this).prop('checked'));
        });
        $(".checkSubcategoriasFilha").change(function(){ 
            if (!$(this).prop("checked")) {
                $("#checkSubcategoriasMae").prop("checked",false);
            } 
        });
    });
</script>

<script>
    var produto_foto_default = "<?= IMG_URL; ?>produtos/default.jpg";
</script>

<style>
    .check-cadastro{
        display: inline-block;
        min-width: 160px;
    }
</style>
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'Produtos', 'gerenciador', $group_id)){ ?>
            <?= link_button_list(__('Listar Produtos'), ['action' => 'gerenciador']); ?>
        <?php } ?>
    </div>
</div>

<div class="produtos form">
    <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset class="text-center">
        <legend><?= __('Insira o Código de Barras') ?></legend>
        <br class="clear">

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-offset-4 col-md-4 text-center">
                <div class="form-group">
                    <label for="cod_barras">Código de Barras</label>
                    <input type="text" class="form-control" placeholder="Ex:1234567891234" name="cod_barras" id="cod_barras">
                </div>
            </div>
        </div> 
        <div class="col-xs-12">
            <div class="form-group text-center">
                <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Enviar').'</span>',
                    ['class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 9.5em;',
                        'escape' => false]); ?>
            </div>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>