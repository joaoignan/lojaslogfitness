<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="netGroupCities form">
    <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
    <fieldset>
        <legend><?= __('Importar Produtos do arquivo CSV') ?></legend>
        

        Baixe uma cópia do modelo <?= $this->Html->link('aqui',
            WEBROOT_URL.'files/LogFitness_Produtos.csv',
            ['class' => '']
        )?>
        <br class="clear">
        <br class="clear">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('csv', 'Arquivo CSV') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('csv', [
                    'type' => 'file',
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
                Selecione um arquivo CSV com padrão de dados separados por vírgulas ","
                
            </div>
        </div>

        <div class="form-group text-center">
              <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrr btn-default',  'style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
        </div>

    </fieldset>
    <?= $this->Form->end() ?>
</div>