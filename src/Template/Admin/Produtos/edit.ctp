<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'Produtos', 'gerenciador', $group_id)){ ?>
            <?= link_button_list(__('Listar Produtos'), ['action' => 'gerenciador']); ?>
        <?php } ?>
    </div>
</div>

<div class="produtos form">
    <?= $this->Form->create($produto, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
    <fieldset>
        <legend><?= __('Editar Produto') ?></legend>
        <h5 class="font-italic"><?= __('Informações Básicas') ?></h5>
        <h6 class="font-italic">
            Obs.: Ao alterar estas informações básicas, todos os produtos dependentes da mesma serão atualizados.
        </h6>
        <br class="clear">

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
                </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'value' => $produto_base->name,
                    'readonly' => false,
                    'disabled' => false,
                    'label' => false,
                    'placeholder' => 'Nome sem propriedades como sabor, cor, marca, etc. Ex.: "Whey Protein"',
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('marca_id') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('marca_id', [
                    'value'         => $produto_base->marca_id,
                    'options'       => $marcas,
                    'label'         => false,
                    'class'         => 'form-control validate[required] chosen-select'
                ]) ?>
            </div>
        </div>
                    
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('atributos', 'Descrição') ?>
            </div>
            <div class="col-sm-10" id="summernote">
                <div id="summernote-text" class="summernote">
                    <?= $produto_base->atributos; ?>
                </div>

                <?= $this->Form->input('atributos', [
                    'value' => $produto_base->atributos,
                    'id'    => 'summernote-input',
                    'type'  => 'hidden',
                        'label' => false,
                    'class' => 'form-control validate[optional] input-block-level'
                    ]) ?>
                </div>
            </div>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('beneficios', 'Benefícios') ?>
            </div>
            <div class="col-sm-10" id="summernote-beneficios">
                <div id="summernote-beneficios-text" class="summernote">
                    <?= strlen(trim($produto_base->beneficios)) >= 3 ? $produto_base->beneficios : $categorias_descricoes; ?>
                </div>

                <?= $this->Form->input('beneficios', [
                    'value' => strlen(trim($produto_base->beneficios)) >= 3 ? $produto_base->beneficios : $categorias_descricoes,
                    'id'    => 'summernote-beneficios-input',
                    'type'  => 'hidden',
                        'label' => false,
                    'class' => 'form-control validate[optional, minSize[3]]'
                    ]) ?>
                </div>
            </div>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('dicas', 'Recomendações') ?>
            </div>
            <div class="col-sm-10" id="summernote-dicas">
                <div id="summernote-dicas-text" class="summernote">
                    <?= $produto_base->dicas; ?>
                </div>

                <?= $this->Form->input('dicas', [
                    'value' => $produto_base->dicas,
                    'id'    => 'summernote-dicas-input',
                    'type'  => 'hidden',
                    'label' => false,
                    'class' => 'form-control validate[optional, minSize[3]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('ingredientes') ?>
            </div>
            <div class="col-sm-10">
                <?= $this->Form->textarea('ingredientes', [
                    'label' => false,
                    'class' => 'form-control validate[optional, minSize[3]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('como_tomar', 'Como tomar') ?>
            </div>
            <div class="col-sm-10" id="summernote-como_tomar">
                <div id="summernote-como_tomar-text" class="summernote">
                    <?= $produto->como_tomar; ?>
                </div>

                <?= $this->Form->input('como_tomar', [
                    'id'    => 'summernote-como_tomar-input',
                    'type'  => 'hidden',
                    'label' => false,
                    'class' => 'form-control validate[optional, minSize[3]]'
                ]) ?>
            </div>
        </div>

        <?= $this->Form->input('embalagem_conteudo', [
            'value' => 'UN',
            'type'  => 'hidden',
            'label' => false
        ]) ?>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('embalagem', 'Embalagem/Conteúdo') ?>
            </div>
            <div class="col-sm-2">
                    <?= $this->Form->input('embalagem', [
                        'options' => [
                            'saco' => 'saco',
                            'pote' => 'pote',
                            'frasco' => 'frasco',
                            'caixa' => 'caixa'
                        ],
                        'label' => false,
                    'class' => 'form-control embalagem-text validate[optional]'
                    ]) ?>
                </div>
            </div>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tamanho') ?>
            </div>
            <div class="col-sm-2">
                    <?= $this->Form->input('tamanho', [
                    'value' => $produto_base->tamanho,
                    'placeholder' => 'Ex.: "20.2"',
                        'type'  => 'number',
                        'label' => false,
                    'class' => 'form-control tamanho-doses validate[optional]'
                    ]) ?>
                </div>
            </div>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('porcao') ?>
                </div>
            <div class="col-sm-2">
                    <?= $this->Form->input('porcao', [
                    'value' => $porcao_base,
                        'placeholder' => 'Ex.: "2.2"',
                    'type'  => 'number',
                        'label' => false,
                    'class' => 'form-control porcao-doses validate[optional]'
                    ]) ?>  
                </div>
            </div>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('doses') ?>
            </div>
            <div class="col-sm-2">
                    <?= $this->Form->input('doses', [
                    'value' => $doses_base,
                        'placeholder' => 'Ex.: "10"',
                    'type'  => 'number',
                        'label' => false,
                    'class' => 'form-control resultado-doses validate[optional]'
                    ]) ?>
                </div>
            </div>

            <script type="text/javascript">
            $(document).ready(function() {
                $('.tamanho-doses, .porcao-doses').blur(function() {
                    if($('.tamanho-doses').val() != '' && $('.porcao-doses').val() != '') {
                        var tamanho = $('.tamanho-doses').val();
                        var porcao = $('.porcao-doses').val();

                        var doses = tamanho / porcao;
                        $('.resultado-doses').val(parseInt(doses));
                    }
                });
            });
            </script>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('unidade_medida') ?>
                </div>
            <div class="col-sm-2">
                <?= $this->Form->input('unidade_medida', [
                    'value' => $produto_base->unidade_medida,
                    'placeholder' => 'Ex.: "g"',
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('categorias_ids', 'Categorias') ?>
            </div>
            <div class="col-sm-10">
                <?= $this->Form->input('categorias_ids', [
                    'options'   => $categorias_list,
                    'value'     => $categorias_selected,
                    'placeholder' => 'Escolha a(s) categoria(s)',
                    'label'     => false,
                    'multiple'  => true,
                    'class'     => 'form-control chosen-select validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('combina_list', 'Combina com') ?>
            </div>
            <div class="col-sm-10">
                <?= $this->Form->input('combina_list', [
                    'options'   => $categorias_list,
                    'value'     => $categorias_combina_selected,
                    'placeholder' => 'Escolha a(s) categoria(s)',
                    'label'     => false,
                    'multiple'  => true,
                    'class'     => 'form-control chosen-select validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('propriedade_id', 'Segmentação') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('propriedade_id', [
                    'value'     => $produto_base->propriedade_id,
                    'id'        => 'propriedade_select',
                    'options'   => $propriedades,
                    'empty'     => ':: Nenhuma ::',
                    'label'     => false,
                    'class'     => 'form-control chosen-select validate[optional]'
                ]) ?>
            </div>
            <div class="col-sm-6">
                <h6 class="font-italic pad10T">Escolha por qual tipo de propriedade o produto será segmentado</h6>
            </div>
        </div>

        <div class="divider"></div>
        <h5 class="font-italic"><?= __('Informações Complementares') ?></h5>
        <h6 class="font-italic">
            Estas informações não afetam outros produtos
        </h6>
        <br class="clear">

        <div class="form-group hidden" id="propriedade">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('propriedade') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('propriedade', [
                    'id'            => 'propriedade_text',
                    'type'          => 'text',
                    'placeholder'   => 'Ex.: "Morango" ou "Vermelho"',
                    'label'         => false,
                    'class'         => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('sabor_id', 'Sabores') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('sabor_id', [
                    'options' => $sabores_list,
                    'empty' => 'Selecione um sabor...',
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('forma_consumo', 'Forma de consumo') ?>
            </div>
            <div class="col-sm-2">
                    <?= $this->Form->input('forma_consumo', [
                        'options' => [
                            'cápsula' => 'cápsula',
                            'cápsula mastigável' => 'cápsula mastigável',
                            'líquido' => 'líquido',
                            'mastigável' => 'mastigável',
                            'em pó' => 'em pó'
                        ],
                    'value' => $produto_base->forma_consumo,
                        'label' => false,
                    'class' => 'form-control validate[optional]'
                    ]) ?>
                </div>
            </div>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('quando_tomar', 'Quando tomar') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('quando_tomar', [
                    'options' => [
                        'pré-treino' => 'pré-treino',
                        'durante o treino' => 'durante o treino',
                        'pós-treino' => 'pós-treino',
                        'diário' => 'diário'
                    ],
                    'value' => $produto_base->quando_tomar,
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('Código ERP') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('cod_interno', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('custo') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('custo', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('preco', 'Preço') ?>
            </div>
            <div class="col-sm-6">
                    <?= $this->Form->input('preco', [
                        'label' => false,
                    'class' => 'form-control validate[optional]'
                    ]) ?>
                </div>
            </div>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('preco_promo', 'Preço Promocional') ?>
            </div>
            <div class="col-sm-6">
                    <?= $this->Form->input('preco_promo', [
                        'label' => false,
                    'class' => 'form-control validate[optional]'
                    ]) ?>
                </div>
            </div>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('slug') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('slug', [
                    'id'    => 'slug',
                        'label' => false,
                    'class' => 'form-control validate[required, custom[slug]]'
                    ]) ?>  
                </div>
            </div>

            <?php 
                $fotos = unserialize($produto->fotos);

        for($i = 0; $i < 7; $i++){
            if(isset($fotos[$i])){
                $foto[$i] = $fotos[$i];
                echo $this->Form->input('photo[]', [
                    'type'  => 'hidden',
                    'value' => $fotos[$i]
                ]);
            }else{
                $foto[$i] = 'default.jpg';
                echo $this->Form->input('photo[]', [
                    'type'  => 'hidden',
                    'value' => ''
                ]);
            }
        }
            ?>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('foto', 'Foto 1') ?>
                    </div>
            <div class="col-sm-6">
                <?= $this->Form->input('foto[]', [
                            'data-preview' => '#foto1-preview',
                            'id'    => 'foto1',
                            'type'  => 'file',
                            'label' => false,
                    'value' => '/teste',
                            'class' => 'form-control foto_upload validate[optional, custom[validateMIME[image/jpeg|image/png]]]'
                        ]) ?>
                    </div>
                    <div class="col-sm-1 text-center">
                <a href="<?= IMG_URL; ?>produtos/<?= $foto[0] ?>" class="fancybox" id="foto1-preview-fancybox">
                    <?= $this->Html->image('produtos/xs-'.$foto[0], [
                                'class' => 'foto-produto-preview',
                                'id'    => 'foto1-preview'
                            ])?>
                        </a>
                    </div>
                    <div class="col-sm-1 text-left">
                        <a class="btn btn-danger clear-foto" data-id="foto1">
                            <i class="glyph-icon icon-trash"></i>
                        </a>
                    </div>
                </div>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('foto', 'Foto 2') ?>
                    </div>
            <div class="col-sm-6">
                <?= $this->Form->input('foto[]', [
                            'data-preview' => '#foto2-preview',
                            'id'    => 'foto2',
                            'type'  => 'file',
                            'label' => false,
                            'class' => 'form-control foto_upload validate[optional, custom[validateMIME[image/jpeg|image/png]]]'
                        ]) ?>
                    </div>
                    <div class="col-sm-1 text-center">
                <a href="<?= IMG_URL; ?>produtos/<?= $foto[1] ?>" class="fancybox" id="foto2-preview-fancybox">
                    <?= $this->Html->image('produtos/xs-'.$foto[1], [
                                'class' => 'foto-produto-preview',
                                'id'    => 'foto2-preview'
                            ])?>
                        </a>
                    </div>
                    <div class="col-sm-1 text-left">
                        <a class="btn btn-danger clear-foto" data-id="foto2">
                            <i class="glyph-icon icon-trash"></i>
                        </a>
                    </div>
                </div>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('foto', 'Foto 3') ?>
                    </div>
            <div class="col-sm-6">
                <?= $this->Form->input('foto[]', [
                            'data-preview' => '#foto3-preview',
                            'id'    => 'foto3',
                            'type'  => 'file',
                            'label' => false,
                            'class' => 'form-control foto_upload validate[optional, custom[validateMIME[image/jpeg|image/png]]]'
                        ]) ?>
                    </div>
                    <div class="col-sm-1 text-center">
                <a href="<?= IMG_URL; ?>produtos/<?= $foto[2] ?>" class="fancybox" id="foto3-preview-fancybox">
                    <?= $this->Html->image('produtos/xs-'.$foto[2], [
                                'class' => 'foto-produto-preview',
                                'id'    => 'foto3-preview'
                            ])?>
                        </a>
                    </div>
                    <div class="col-sm-1 text-left">
                        <a class="btn btn-danger clear-foto" data-id="foto3">
                            <i class="glyph-icon icon-trash"></i>
                        </a>
                    </div>
                </div>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('visivel') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('visivel', [
                    'options'   => [
                        0   => 'Não',
                        1   => 'Sim',
                    ],
                        'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                    ]) ?>
                </div>
            </div>

                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('status_id') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('status_id', [
                    'options'   => [
                        2   => 'Inativo',
                        1   => 'Ativo',
                    ],
                    'empty' => false,
                        'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                    ]) ?>
                </div>
            </div>

            <div class="divider"></div>
        <h5 class="font-italic"><?= __('Substâncias') ?></h5>
        <h6 class="font-italic">
                Estas informações não afetam outros produtos
        </h6>
            <div class="form-group">
                <div class="col-sm-6">
                <?= $this->Form->input('produtos_list', [
                        'options'   => $produtos_list,
                        'empty' => 'Selecione um produto...',
                        'label' => false,
                        'id'    => 'produto-busca-tabela',
                        'class' => 'form-control chosen-select validate[optional]'
                    ]) ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->button('<span> '.__('Buscar tabela nutricional').'</span>',
                    ['class' => 'btn btn-alt btn-hoverrr btn-default busca-tabela', 'style' =>  'width: 17.5em;',
                            'escape' => false, 'type' => 'button']); ?>
                </div>
            </div>
            <br class="clear">

        <script type="text/javascript">
            $('.busca-tabela').on('click', function() {
                var tabela_produto = $('#produto-busca-tabela').val();

                while($('.substancia-div').length >= 1) {
                    $('.remover-substancia').click();
                }

                $.get('<?= WEBROOT_URL ?>' + 'admin/produtos/tabela_nutricional/' + tabela_produto,
                    function (data) {
                        data = JSON.parse(data);
                        var value_atual = 0;

                        $.each(data, function(index, item) {
                            if(value_atual != 0) {
                                $('.adicionar-substancia').click();
                            }
                            $("#substancia-"+value_atual).val(item.substancia);
                            $("#valor-substancia-"+value_atual).val(item.valor);
                            $("#unidade-medida-"+value_atual).html(item.unidade_medida);

                            value_atual++;
                        });
                    });
            });
        </script>

            <div class="substancias-group">
                <div class="form-group substancia-div-1">
                    <div class="col-sm-2 control-label">
                        <?= $this->Form->label('Substância') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $this->Form->input('substancia.0', [
                            'options'   => $substancias_list,
                            'id'    => 'substancia-0',
                            'empty' => false,
                            'label' => false,
                        'class' => 'form-control substancia validate[optional]'
                        ]) ?>
                    </div>
                    <div class="col-sm-2">
                        <?= $this->Form->input('valor_substancia.0', [
                            'label' => false,
                            'placeholder' => 'valor',
                        'class' => 'form-control validate[optional]'
                        ]) ?>
                    </div>
                <div class="col-sm-2 text-left">
                    <p id="unidade-medida-0">g</p>
                    </div>
                </div>
            </div>

            <div class="form-group text-center">
                <?= $this->Form->button('<i class="glyph-icon icon-plus"></i><span> '.__('Adicionar substância').'</span>',
                ['class' => 'btn btn-alt btn-hoverrr btn-default adicionar-substancia', 'type' => 'button', 'style' =>  'width: 15.5em;',
                        'escape' => false]); ?>

                <?= $this->Form->button('<i class="glyph-icon icon-minus"></i><span> '.__('Remover substância').'</span>',
                ['class' => 'btn btn-alt btn-hoverrr btn-danger remover-substancia', 'type' => 'button', 'style' =>  'width: 15.5em;',
                        'escape' => false]); ?>
            </div>

            <div class="form-group text-center">
                <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>

<script type="text/javascript">
        $(document).on('change', '.substancia', function(){
            var substancia_id = $(this).val();
            var value = $(this).attr('id').split('-');
            value = value[1];

            <?php foreach ($substancias as $substancia) { ?>
                if(substancia_id == <?= $substancia->id ?>) {
                    $('#unidade-medida-'+value).html('<?= $substancia->unidade_medida ?>');
                }
            <?php } ?>
        });

        $('.adicionar-substancia').click(function() {
            var ultima_substancia = $('.substancia').last();
            var value_ultima_substancia = ultima_substancia.attr('id').split('-');
            value_ultima_substancia = parseInt(value_ultima_substancia[1]);
            var new_value = value_ultima_substancia+1;

            var data =  "<div class='form-group substancia-div'>"+
                            "<div class='col-sm-2 control-label'>"+
                                "<label for='substancia'>Substância</label>"+
                            "</div>"+
                            "<div class='col-sm-4'>"+
                            "<select name='substancia["+new_value+"]' id='substancia-"+new_value+"' class='form-control substancia validate[optional]'>"+
                                <?php foreach ($substancias as $substancia) { ?>
                                    "<option value='<?= $substancia->id ?>'><?= $substancia->name ?></option>"+
                                <?php } ?>
                                "</select>"+
                            "</div>"+
                            "<div class='col-sm-2'>"+
                            "<input type='text' name='valor_substancia["+new_value+"]' placeholder='valor' class='form-control validate[optional]' id='valor-substancia-"+new_value+"'>"+
                            "</div>"+
                        "<div class='col-sm-2 text-left'>"+
                            "<p id='unidade-medida-"+new_value+"'>g</p>"+
                            "</div>"+
                        "</div>";

            $('.substancias-group').append(data);
        });

        $('.remover-substancia').click(function() {
            $('.substancia-div').last().remove();
        });

    value_atual = 0;
    <?php foreach($produto_substancias_list as $psl) { ?>
        <?= $psl->substancia ?>
        if(value_atual != 0) {
            $('.adicionar-substancia').click();
    }
        $("#substancia-"+value_atual).val("<?= $psl['substancia'] ?>");
        $("#valor-substancia-"+value_atual).val("<?= $psl['valor'] ?>");
        $("#unidade-medida-"+value_atual).html("<?= $psl['unidade_medida'] ?>");

        value_atual++;
    <?php } ?>
</script>