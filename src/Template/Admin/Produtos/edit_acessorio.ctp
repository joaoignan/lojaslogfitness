<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>
<script>
    $(document).ready(function(){
        $("#checkSubcategoriasMae").click(function(){
            $(".checkSubcategoriasFilha").prop('checked', $(this).prop('checked'));
        });
        $(".checkSubcategoriasFilha").change(function(){ 
            if (!$(this).prop("checked")) {
                $("#checkSubcategoriasMae").prop("checked",false);
            } 
        });
    });
</script>

<script>
    var produto_foto_default = "<?= IMG_URL; ?>produtos/default-acessorio.jpg";
</script>

<style>
    .check-cadastro{
        display: inline-block;
        min-width: 160px;
    }
</style>
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'Produtos', 'gerenciador', $group_id)){ ?>
            <?= link_button_list(__('Listar Produtos'), ['action' => 'gerenciador']); ?>
        <?php } ?>
    </div>
</div>

<div class="produtos form">
    <?= $this->Form->create($produto, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
    <fieldset>
        <legend><?= __('Editar Produto') ?></legend>
        <h3 class="font-italic"><?= __('Confira o código de barras do produto antes de fazer qualquer alteração') ?></h3>
        <br class="clear">

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="cod_interno">Cód Barras</label>
                    <h2><?= $produto->cod_barras ?></h2>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-8">
                <div class="form-group">
                    <label for="cod_interno">Produto</label>
                    <h3><?= $produto->produto_base->marca->name.' - '.$produto->produto_base->name.' '.$produto->tamanho.' - '.$produto->cor ?></h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="cod_interno">Cód ERP</label>
                     <?= $this->Form->input('cod_interno', [
                        'label' => false,
                        'placeholder' => 'Código ERP',
                        'class' => 'form-control validate[optional]'
                    ]) ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="tamanho">Tamanho</label>
                    <?= $this->Form->input('tamanho', [
                        'placeholder' => 'Ex: G, 3m, 45cm, 500ml',
                        'label' => false,
                        'class' => 'form-control validate[optional]'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="form-group">
                        <label for="cor">Cor</label>
                         <?= $this->Form->input('cor', [
                            'label' => false,
                            'value' => $produto->cor,
                            'placeholder' => 'Ex: Vermelho',
                            'class' => 'form-control validate[optional]'
                        ]) ?>
                    </div>
                </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="preco">Preço de venda</label>
                    <?= $this->Form->input('preco', [
                        'label' => false,
                        'type'  => 'text',
                        'value' => number_format($produto->preco, 2, ',', '.'),
                        'class' => 'form-control price validate[required]'
                    ]) ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="preco_promo">Preço promoção</label>
                    <?= $this->Form->input('preco_promo', [
                        'label' => false,
                        'type'  => 'text',
                        'value' => number_format($produto->preco_promo, 2, ',', '.'),
                        'class' => 'form-control price validate[optional]'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div> 
        <div class="row">
            <div class="col-xs-12">
                <div class="divider"></div>
                <h5 class="font-italic"><?= __('Informações Complementares') ?></h5>
                <br class="clear">
            </div>

            <?php 
                $fotos = unserialize($produto->fotos);
            ?>

            <div class="col-xs-12">
                <div class="form-group">
                    <div class="col-sm-1 control-label">
                        <?= $this->Form->label('fotos', 'Foto 1') ?>
                    </div>
                    <div class="col-sm-5">
                        <?= $this->Form->input('fotos[]', [
                            'data-preview' => '#foto1-preview',
                            'id'    => 'foto1',
                            'type'  => 'file',
                            'label' => false,
                            'class' => 'form-control foto_upload validate[optional, custom[validateMIME[image/jpeg|image/png]]]'
                        ]) ?>
                    </div>
                    <div class="col-sm-1 text-center">
                        <a href="<?= IMG_URL; ?>produtos/default-acessorio.jpg" class="fancybox" id="foto1-preview-fancybox">
                            <?= $this->Html->image('produtos/default-acessorio.jpg', [
                                'class' => 'foto-produto-preview',
                                'id'    => 'foto1-preview'
                            ])?>
                        </a>
                    </div>
                    <div class="col-sm-1 text-left">
                        <a class="btn btn-danger clear-foto" data-id="foto1">
                            <i class="glyph-icon icon-trash"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <div class="col-sm-1 control-label">
                        <?= $this->Form->label('fotos', 'Foto 2') ?>
                    </div>
                    <div class="col-sm-5">
                        <?= $this->Form->input('fotos[]', [
                            'data-preview' => '#foto2-preview',
                            'id'    => 'foto2',
                            'type'  => 'file',
                            'label' => false,
                            'class' => 'form-control foto_upload validate[optional, custom[validateMIME[image/jpeg|image/png]]]'
                        ]) ?>
                    </div>
                    <div class="col-sm-1 text-center">
                        <a href="<?= IMG_URL; ?>produtos/default-acessorio.jpg" class="fancybox" id="foto2-preview-fancybox">
                            <?= $this->Html->image('produtos/default-acessorio.jpg', [
                                'class' => 'foto-produto-preview',
                                'id'    => 'foto2-preview'
                            ])?>
                        </a>
                    </div>
                    <div class="col-sm-1 text-left">
                        <a class="btn btn-danger clear-foto" data-id="foto2">
                            <i class="glyph-icon icon-trash"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <div class="col-sm-1 control-label">
                        <?= $this->Form->label('fotos', 'Foto 3') ?>
                    </div>
                    <div class="col-sm-5">
                        <?= $this->Form->input('fotos[]', [
                            'data-preview' => '#foto3-preview',
                            'id'    => 'foto3',
                            'type'  => 'file',
                            'label' => false,
                            'class' => 'form-control foto_upload validate[optional, custom[validateMIME[image/jpeg|image/png]]]'
                        ]) ?>
                    </div>
                    <div class="col-sm-1 text-center">
                        <a href="<?= IMG_URL; ?>produtos/default-acessorio.jpg" class="fancybox" id="foto3-preview-fancybox">
                            <?= $this->Html->image('produtos/default-acessorio.jpg', [
                                'class' => 'foto-produto-preview',
                                'id'    => 'foto3-preview'
                            ])?>
                        </a>
                    </div>
                    <div class="col-sm-1 text-left">
                        <a class="btn btn-danger clear-foto" data-id="foto3">
                            <i class="glyph-icon icon-trash"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="form-group">
                    <label for="atributos">Descrição</label>
                    <?= $this->Form->input('atributos', [
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => false,
                        'id'    => 'atributos',
                        'value' => $produto->produto_base->atributos,
                        'class' => 'form-control validate[optional, minSize[3]] input-block-level'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-md-4">
                <div class="form-group">     
                    <label for="summernote-beneficios">Benefícios</label>
                    <?= $this->Form->input('beneficios', [
                        'id'    => 'summernote-beneficios-input',
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => false,
                        'id'    => 'beneficios',
                        'value' => $produto->produto_base->beneficios,
                        'class' => 'form-control validate[optional, minSize[3]] input-block-level'
                    ]) ?>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="form-group">
                    <label for="summernote-dicas">Dicas</label>
                    <?= $this->Form->input('dicas', [
                        'id'    => 'summernote-dicas-input',
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => false,
                        'id'    => 'dicas',
                        'value' => $produto->produto_base->dicas,
                        'class' => 'form-control validate[optional, minSize[3]]'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-xs-12 col-md-9">
            <div class="form-group text-center">
                <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                    ['class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 9.5em;',
                        'escape' => false]); ?>
            </div>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>

<script type="text/javascript">
    $('form').on('focus', 'input[type=number]', function (e) {
        $(this).on('mousewheel.disableScroll', function (e) {
            e.preventDefault()
        })
    })
    $('form').on('blur', 'input[type=number]', function (e) {
        $(this).off('mousewheel.disableScroll')
    })
    $('.money').mask('000.000.000.000.000,00', {reverse: true});
</script>