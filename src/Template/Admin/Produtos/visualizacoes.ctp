<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="produtos index">

    <table class="table table-hover">
        <thead>
        <tr>
            <th class="text-center"><?= $this->Paginator->sort('id') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('cod_interno', 'Cod ERP') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('produto_base_id', 'Produto') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('views', 'visualizações') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($produtos as $produto): ?>
            <tr>
                <td class="text-center"><?= $this->Number->format($produto->id) ?></td>
                <td class="text-center"><?= $this->Number->format($produto->cod_interno) ?></td>
                <td class="text-center"><?= $produto->produto_base->name.' - '.$produto->propriedade.' - '.$produto->tamanho.' '.$produto->unidade_medida.' - '.$produto->produto_base->marca->name ?></td> 
                <td class="text-center"><?= $produto->views ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>