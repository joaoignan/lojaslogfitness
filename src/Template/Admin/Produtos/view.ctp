<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'Produtos', 'edit', $group_id)){ ?>
            <?= link_button_edit(__('Editar Produto'),  ['action' => 'edit', $produto->id]); ?>
        <?php } if($cca->cca('admin', 'Produtos', 'index', $group_id)){ ?>
            <?= link_button_list(__('Listar Produtos'), ['action' => 'index']); ?>
        <?php } if($cca->cca('admin', 'Produtos', 'add', $group_id)){ ?>
            <?= link_button_add(__('Novo Produto'), ['action' => 'add']); ?>
        <?php } ?>
    </div>
</div>

<div class="produtos view" xmlns="http://www.w3.org/1999/html">
    <h2>Produto #<?= h($produto->id) ?> :: <?= $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo.' '.$produto->propriedade ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Status') ?></td>
                    <td><?= $produto->has('status') ? $this->Html->link($produto->status->name, ['controller' => 'Status', 'action' => 'view', $produto->status->id]) : '' ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($produto->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Código ERP') ?></td>
                    <td><?= $this->Number->format($produto->cod_interno) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Custo') ?></td>
                    <td>R$ <?= number_format($produto->custo, 2, ',', '.') ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Preco') ?></td>
                    <td>R$ <?= number_format($produto->preco, 2, ',', '.') ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Porcentagem') ?></td>
                    <td><?= number_format($produto->porcentagem, 2, ',', '.') ?> %</td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Estoque') ?></td>
                    <td><?= $produto->estoque ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Estoque mínimo') ?></td>
                    <td><?= $produto->estoque_min ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Visivel') ?></td>
                    <td><?= $produto->visivel == 1 ? 'Sim' : 'Não' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Visualizações') ?></td>
                    <td><?= $produto->views ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($produto->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($produto->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>