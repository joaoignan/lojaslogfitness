<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>
<script>
    $(document).ready(function(){
        $("#checkSubcategoriasMae").click(function(){
            $(".checkSubcategoriasFilha").prop('checked', $(this).prop('checked'));
        });
        $(".checkSubcategoriasFilha").change(function(){ 
            if (!$(this).prop("checked")) {
                $("#checkSubcategoriasMae").prop("checked",false);
            } 
        });
    });
</script>

<script>
    var produto_foto_default = "<?= IMG_URL; ?>produtos/default.jpg";
</script>

<style>
    .check-cadastro{
        display: inline-block;
        min-width: 160px;
    }
</style>
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'Produtos', 'gerenciador', $group_id)){ ?>
            <?= link_button_list(__('Listar Produtos'), ['action' => 'gerenciador']); ?>
        <?php } ?>
    </div>
</div>

<div class="produtos form">
    <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
    <fieldset>
        <legend><?= __('Adicionar Produto') ?></legend>
        <h5 class="font-italic"><?= __('Informações Mínimas') ?></h5>
        <br class="clear">

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="form-group">
                    <label for="cod_barras">Código de Barras</label>
                    <p><?= $cod_barras ?></p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-5">
                <div class="form-group">
                    <label for="produto_base_id">Produto Base (víncule produtos iguais, com sabores ou tamanhos diferentes)</label>
                    <?= $this->Form->input('produto_base_id', [
                        'id'      => 'produto_base_id',
                        'options' => $produtos_base_list,
                        'empty' => 'Nenhum',
                        'label' => false,
                        'class' => 'form-control chosen-select validate[optional]'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="name">Nome do Produto</label>
                     <?= $this->Form->input('name', [
                        'label' => false,
                        'placeholder' => 'Nome sem propriedades Ex.: "Whey Protein"',
                        'class' => 'form-control validate[required, minSize[3], maxSize[255]]'
                    ]) ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="marca_id">Marca</label>
                    <?= $this->Form->input('marca_id', [
                    'id'            => 'marcas_select',
                    'options'       => $marcas_list,
                    'empty'         => 'Selecione uma marca',
                    'label'         => false,
                    'class'         => 'form-control validate[required] chosen-select'
                ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="cod_interno">Cód ERP</label>
                     <?= $this->Form->input('cod_interno', [
                        'label' => false,
                        'placeholder' => 'Código ERP',
                        'class' => 'form-control validate[optional]'
                    ]) ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="embalagem">Embalagem/conteúdo</label>
                    <?= $this->Form->input('embalagem', [
                        'options' => [
                            'saco' => 'saco',
                            'pote' => 'pote',
                            'frasco' => 'frasco',
                            'caixa' => 'caixa'
                        ],
                        'label' => false,
                        'empty' => 'Selecione uma embalagem',
                        'class' => 'form-control embalagem-text validate[required]'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="tamanho">Tamanho</label>
                    <?= $this->Form->input('tamanho', [
                        'placeholder' => 'Ex.: "1800"',
                        'type'  => 'number',
                        'label' => false,
                        'class' => 'form-control tamanho-doses validate[required]'
                    ]) ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="unidade_medida">Unidade de medida</label>
                    <?= $this->Form->input('unidade_medida', [
                        'options' => [
                            'tabletes' => 'Tabletes',
                            'cápsulas' => 'Cápsulas',
                            'barras' => 'Barras',
                            'saches' => 'Saches',
                            'sticks' => 'Sticks',
                            'packs' => 'Packs',
                            'unidades' => 'Unidades',
                            'gr' => 'Gramas (gr)',
                            'ml' => 'Mililitros (ml)',
                        ],
                        'empty' => 'Selecione uma unidade de medida',
                        'label' => false,
                        'class' => 'form-control validate[required]'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="">Porção (por dose)</label>
                    <?= $this->Form->input('porcao', [
                        'placeholder' => 'Ex.: "2.2"',
                        'type'  => 'text',
                        'label' => false,
                        'class' => 'form-control porcao-doses validate[required]'
                    ]) ?>  
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="doses">Total de doses (Cálculo de tamanho/porção)</label>
                    <?= $this->Form->input('doses', [
                        'placeholder' => 'Ex.: "10"',
                        'type'  => 'text',
                        'label' => false,
                        'class' => 'form-control resultado-doses validate[optional]',
                        'readonly' => true
                    ]) ?>
                </div>
            </div>
            <script type="text/javascript">
                $('.porcao-doses').mask('##00.00', {reverse: true});
                $('.tamanho-doses, .porcao-doses').blur(function() {
                    if($('.tamanho-doses').val() != '' && $('.porcao-doses').val() != '') {
                        var tamanho = $('.tamanho-doses').val();
                        var porcao = $('.porcao-doses').val();

                        var doses = tamanho / porcao;
                        $('.resultado-doses').val(parseInt(doses));
                    }
                });
            </script>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="propriedade">Sabor</label>
                    <input type="text" name="propriedade" placeholder="Ex: Morango" class="form-control">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="forma_consumo">Forma de consumo</label>
                    <?= $this->Form->input('forma_consumo', [
                        'options' => [
                            'cápsula' => 'cápsula',
                            'cápsula mastigável' => 'cápsula mastigável',
                            'líquido' => 'líquido',
                            'mastigável' => 'mastigável',
                            'em pó' => 'em pó'
                        ],
                        'label' => false,
                        'empty' => 'Selecione uma forma de consumo',
                        'id'    => 'forma_consumo',
                        'class' => 'form-control validate[required]'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="preco">Preço de venda</label>
                    <?= $this->Form->input('preco', [
                        'label' => false,
                        'type'  => 'text',
                        'class' => 'form-control price validate[required]'
                    ]) ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="preco_promo">Preço promoção</label>
                    <?= $this->Form->input('preco_promo', [
                        'label' => false,
                        'type'  => 'text',
                        'class' => 'form-control price validate[optional]'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="quando_tomar">Quando Tomar</label>
                    <?= $this->Form->input('quando_tomar', [
                        'options' => [
                            'pré-treino' => 'pré-treino',
                            'durante o treino' => 'durante o treino',
                            'pós-treino' => 'pós-treino',
                            'diário' => 'diário'
                        ],
                        'label' => false,
                        'empty' => 'Selecione quando tomar',
                        'id'    => 'quando_tomar',
                        'class' => 'form-control validate[required]'
                    ]) ?>  
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-8">
                        <h4>Objetivos/Categorias/Subcategorias</h4>
                        <div class="checkbox subCategoria">
                        <?php foreach($objetivos as $objetivo) { ?>
                            <div class="col-xs-12" style="margin-top: 15px;">
                                <div class="col-xs-12">
                                    <label>
                                        <input type="checkbox" name="objetivos[]" class="objetivo-checkbox" value="<?= $objetivo->id ?>"> <?= $objetivo->name ?>
                                    </label>
                                </div>
                                <?php foreach($objetivo->categorias as $categoria) { ?>
                                    <div class="col-xs-12" style="display: none">
                                        <label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" name="categorias[]" class="categoria-checkbox" o-id="<?= $objetivo->id ?>" value="<?= $categoria->id ?>"> <?= $categoria->name ?>
                                        </label>
                                    </div>
                                    <?php foreach ($categoria->subcategorias as $subcategoria) { ?>
                                        <div class="col-xs-12" style="display: none">
                                            <label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="subcategorias[]" class="subcategoria-checkbox" o-id="<?= $objetivo->id ?>" c-id="<?= $categoria->id ?>" value="<?= $subcategoria->id ?>"> <?= $subcategoria->name ?>
                                            </label>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                $(document).on('click', '.objetivo-checkbox', function() {
                    var objetivo_id = $(this).val();
                    var is_checked = $(this).is(':checked');

                    if(is_checked) {
                        $('.categoria-checkbox').each(function() {
                            if($(this).attr('o-id') == objetivo_id) {
                                $(this).parent().parent().show();
                            }
                        });
                    } else {
                        $('.categoria-checkbox').each(function() {
                            if($(this).attr('o-id') == objetivo_id) {
                                $(this).prop('checked', false);
                                $(this).parent().parent().hide();
                            }
                        });

                        $('.subcategoria-checkbox').each(function() {
                            if($(this).attr('o-id') == objetivo_id) {
                                $(this).parent().parent().hide();
                                $(this).prop('checked', false);
                            }
                        });
                    }
                });

                $(document).on('click', '.categoria-checkbox', function() {
                    var categoria_id = $(this).val();
                    var is_checked = $(this).is(':checked');

                    if(is_checked) {
                        $('.subcategoria-checkbox').each(function() {
                            if($(this).attr('c-id') == categoria_id) {
                                $(this).parent().parent().show();
                            }
                        });
                    } else {
                        $('.subcategoria-checkbox').each(function() {
                            if($(this).attr('c-id') == categoria_id) {
                                $(this).prop('checked', false);
                                $(this).parent().parent().hide();
                            }
                        });
                    }
                });
            </script>
        </div> 
        <div class="row">
            <div class="col-xs-12">
                <div class="divider"></div>
                <h5 class="font-italic"><?= __('Informações Complementares') ?></h5>
                <br class="clear">
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <div class="col-sm-1 control-label">
                        <?= $this->Form->label('fotos', 'Foto 1') ?>
                    </div>
                    <div class="col-sm-5">
                        <?= $this->Form->input('fotos[]', [
                            'data-preview' => '#foto1-preview',
                            'id'    => 'foto1',
                            'type'  => 'file',
                            'label' => false,
                            'class' => 'form-control foto_upload validate[required, custom[validateMIME[image/jpeg|image/png]]]'
                        ]) ?>
                    </div>
                    <div class="col-sm-1 text-center">
                        <a href="<?= IMG_URL; ?>produtos/default.jpg" class="fancybox" id="foto1-preview-fancybox">
                            <?= $this->Html->image('produtos/default.jpg', [
                                'class' => 'foto-produto-preview',
                                'id'    => 'foto1-preview'
                            ])?>
                        </a>
                    </div>
                    <div class="col-sm-1 text-left">
                        <a class="btn btn-danger clear-foto" data-id="foto1">
                            <i class="glyph-icon icon-trash"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <div class="col-sm-1 control-label">
                        <?= $this->Form->label('fotos', 'Foto 2') ?>
                    </div>
                    <div class="col-sm-5">
                        <?= $this->Form->input('fotos[]', [
                            'data-preview' => '#foto2-preview',
                            'id'    => 'foto2',
                            'type'  => 'file',
                            'label' => false,
                            'class' => 'form-control foto_upload validate[optional, custom[validateMIME[image/jpeg|image/png]]]'
                        ]) ?>
                    </div>
                    <div class="col-sm-1 text-center">
                        <a href="<?= IMG_URL; ?>produtos/default.jpg" class="fancybox" id="foto2-preview-fancybox">
                            <?= $this->Html->image('produtos/default.jpg', [
                                'class' => 'foto-produto-preview',
                                'id'    => 'foto2-preview'
                            ])?>
                        </a>
                    </div>
                    <div class="col-sm-1 text-left">
                        <a class="btn btn-danger clear-foto" data-id="foto2">
                            <i class="glyph-icon icon-trash"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <div class="col-sm-1 control-label">
                        <?= $this->Form->label('fotos', 'Foto 3') ?>
                    </div>
                    <div class="col-sm-5">
                        <?= $this->Form->input('fotos[]', [
                            'data-preview' => '#foto3-preview',
                            'id'    => 'foto3',
                            'type'  => 'file',
                            'label' => false,
                            'class' => 'form-control foto_upload validate[optional, custom[validateMIME[image/jpeg|image/png]]]'
                        ]) ?>
                    </div>
                    <div class="col-sm-1 text-center">
                        <a href="<?= IMG_URL; ?>produtos/default.jpg" class="fancybox" id="foto3-preview-fancybox">
                            <?= $this->Html->image('produtos/default.jpg', [
                                'class' => 'foto-produto-preview',
                                'id'    => 'foto3-preview'
                            ])?>
                        </a>
                    </div>
                    <div class="col-sm-1 text-left">
                        <a class="btn btn-danger clear-foto" data-id="foto3">
                            <i class="glyph-icon icon-trash"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="form-group">
                    <label for="atributos">Descrição</label>
                    <?= $this->Form->input('atributos', [
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => false,
                        'id'    => 'atributos',
                        'class' => 'form-control validate[optional, minSize[3]] input-block-level'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-md-4">
                <div class="form-group">     
                    <label for="summernote-beneficios">Benefícios</label>
                    <?= $this->Form->input('beneficios', [
                        'id'    => 'summernote-beneficios-input',
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => false,
                        'id'    => 'beneficios',
                        'class' => 'form-control validate[optional, minSize[3]] input-block-level'
                    ]) ?>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="form-group">
                    <label for="summernote-dicas">Dicas</label>
                    <?= $this->Form->input('dicas', [
                        'id'    => 'summernote-dicas-input',
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => false,
                        'id'    => 'dicas',
                        'class' => 'form-control validate[optional, minSize[3]]'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-md-4">
                <div class="form-group">
                    <label for="ingredientes">Ingredientes</label>
                    <?= $this->Form->textarea('ingredientes', [
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => false,
                        'class' => 'form-control validate[optional, minSize[3]]'
                    ]) ?>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="form-group">
                    <label for="como_tomar">Como Tomar</label>
                    <?= $this->Form->input('como_tomar', [
                        'id'    => 'summernote-como_tomar-input',
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => false,
                        'class' => 'form-control validate[optional, minSize[3]]'
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="divider"></div>
            <h5 class="font-italic"><?= __('Tabela Nutricional') ?></h5>
            <h6 class="font-italic">
                Estas informações não afetam outros produtos
            </h6>
            <div class="form-group">
                <div class="col-sm-6">
                    <?= $this->Form->input('produtos_list', [
                            'options'   => $produtos_list,
                            'empty' => 'Selecione um produto...',
                            'label' => false,
                            'id'    => 'produto-busca-tabela',
                            'class' => 'form-control chosen-select validate[optional]'
                        ]) ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->button('<span> '.__('Buscar tabela nutricional').'</span>',
                        ['class' => 'btn btn-alt btn-hoverrr btn-default busca-tabela', 'style' =>  'width: 17.5em;',
                            'escape' => false, 'type' => 'button']); ?>
                </div>
            </div>
            <br class="clear">

            <script type="text/javascript">
                $('.busca-tabela').on('click', function() {
                    var tabela_produto = $('#produto-busca-tabela').val();
                    console.log(tabela_produto);

                    while($('.substancia-div').length >= 1) {
                        $('.remover-substancia').click();
                    }

                    $.get('<?= WEBROOT_URL ?>' + 'admin/produtos/tabela_nutricional/' + tabela_produto,
                        function (data) {
                            data = JSON.parse(data);
                            var value_atual = 0;

                            $.each(data, function(index, item) {
                                if(value_atual != 0) {
                                    $('.adicionar-substancia').click();
                                }
                                $("#substancia-"+value_atual).val(item.substancia);
                                $("#valor-substancia-"+value_atual).val(item.valor);
                                $("#unidade-medida-"+value_atual).html(item.unidade_medida);

                                value_atual++;
                            });
                        });
                });
            </script>

            <div class="substancias-group">
                <div class="form-group substancia-div-1">
                    <div class="col-sm-2 control-label">
                        <?= $this->Form->label('Substância') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $this->Form->input('substancia.0', [
                            'options'   => $substancias_list,
                            'id'    => 'substancia-0',
                            'name'  => 'substancias[]',
                            'empty' => false,
                            'label' => false,
                            'class' => 'form-control substancia validate[optional]'
                        ]) ?>
                    </div>
                    <div class="col-sm-2">
                        <?= $this->Form->input('valor_substancia.0', [
                            'label' => false,
                            'placeholder' => 'valor',
                            'name'  => 'substancias_valor[]',
                            'class' => 'form-control substancia-valor validate[optional]'
                        ]) ?>
                    </div>
                    <div class="col-sm-1 text-left">
                        <p id="unidade-medida-0" style="float: left;">g</p>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-5 control-label">
                    <?= $this->Html->link('Não encontrou a substância? Cadastre aqui',
                        ['controller' => 'Substancias', 'action' => 'index'],
                        ['class' => 'font-normal frase-vermelha', 'escape' => false]) ?>
                </div>
            </div>

            <style type="text/css">
                .remover-substancia-linha {
                    color: #e74c3c;
                    font-size: 30px;
                    margin-left: 15px;
                    margin-top: -8px;
                    position: relative;
                    float: left;
                    cursor: pointer;
                }
                .remover-substancia-linha:hover {
                    color: #eb6759;
                }
                .frase-vermelha {
                    color: #e74c3c;
                    font-weight: 700;
                }

                .frase-vermelha:hover {
                    color: #ff1800;
                }
            </style>

            <div class="form-group text-center">
                <?= $this->Form->button('<i class="glyph-icon icon-plus"></i><span> '.__('Adicionar substância').'</span>',
                    ['class' => 'btn btn-alt btn-hoverrr btn-default adicionar-substancia', 'type' => 'button', 'style' =>  'width: 15.5em;',
                        'escape' => false]); ?>

                <?= $this->Form->button('<i class="glyph-icon icon-minus"></i><span> '.__('Remover substância').'</span>',
                    ['class' => 'btn btn-alt btn-hoverrr btn-danger remover-substancia', 'type' => 'button', 'style' =>  'width: 15.5em;',
                        'escape' => false]); ?>
            </div>
        </div>
        <div class="col-xs-12 col-md-9">
            <div class="form-group text-center">
                <button type="button" class="btn btn-alt btn-hoverrr btn-default" style="width: 9.5em" data-toggle="modal" data-target="#modalConfirma"><i class="glyph-icon icon-check-square-o"></i><span> Confirmar</button>
                
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="modalConfirma" role="dialog">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Confirmar produto novo</h4>
                </div>
                <div class="modal-body">
                  <p>Deseja salvar o novo produto?</p>
                </div>
                <div class="modal-footer">
                    <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                    ['class' => 'btn btn-alt btn-hoverrr btn-default', 'escape' => false]); ?>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
              </div>
            </div>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>

<script type="text/javascript">
    $(document).on('change', '.substancia', function(){
        var substancia_id = $(this).val();
        var value = $(this).attr('id').split('-');
        value = value[1];

        <?php foreach ($substancias as $substancia) { ?>
            if(substancia_id == <?= $substancia->id ?>) {
                $('#unidade-medida-'+value).html('<?= $substancia->unidade_medida ?>');
            }
        <?php } ?>
    });

    $('.adicionar-substancia').click(function() {
        var ultima_substancia = $('.substancia').last();
        var value_ultima_substancia = ultima_substancia.attr('id').split('-');
        value_ultima_substancia = parseInt(value_ultima_substancia[1]);
        var new_value = value_ultima_substancia+1;

        var data =  "<div class='form-group substancia-div'>"+
                        "<div class='col-sm-2 control-label'>"+
                            "<label for='substancia'>Substância</label>"+
                        "</div>"+
                        "<div class='col-sm-4'>"+
                            "<select name='substancias[]' id='substancia-"+new_value+"' class='form-control substancia validate[optional]'>"+
                            <?php foreach ($substancias as $substancia) { ?>
                                "<option value='<?= $substancia->id ?>'><?= $substancia->name ?></option>"+
                            <?php } ?>
                            "</select>"+
                        "</div>"+
                        "<div class='col-sm-2'>"+
                            "<input type='text' name='substancias_valor[]' placeholder='valor' class='form-control substancia-valor validate[optional]' id='valor-substancia-"+new_value+"'>"+
                        "</div>"+
                        "<div class='col-sm-1 text-left'>"+
                            "<p id='unidade-medida-"+new_value+"' style='float:left'>g</p>"+
                            "<i class='glyph-icon icon-trash remover-substancia-linha'></i>"+
                        "</div>"+
                    "</div>";

        $('.substancias-group').append(data);
    });

    $('.remover-substancia').click(function() {
        $('.substancia-div').last().remove();
    });

    $(document).on('click', '.remover-substancia-linha', function() {
        $(this).parent().parent().remove();
    });
</script>

<script type="text/javascript">
    $('form').on('focus', 'input[type=number]', function (e) {
        $(this).on('mousewheel.disableScroll', function (e) {
            e.preventDefault()
        })
    })
    $('form').on('blur', 'input[type=number]', function (e) {
        $(this).off('mousewheel.disableScroll')
    })
    $('input').keypress(function (e) {
        var code = null;
        code = (e.keyCode ? e.keyCode : e.which);                
        return (code == 13) ? false : true;
    });
</script>