<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>
<script>
    $(document).ready(function(){
        $("#checkSubcategoriasMae").click(function(){
            $(".checkSubcategoriasFilha").prop('checked', $(this).prop('checked'));
        });
        $(".checkSubcategoriasFilha").change(function(){ 
            if (!$(this).prop("checked")) {
                $("#checkSubcategoriasMae").prop("checked",false);
            } 
        });
    });
</script>

<script>
    var produto_foto_default = "<?= IMG_URL; ?>produtos/default-acessorio.jpg";
</script>

<style>
    .check-cadastro{
        display: inline-block;
        min-width: 160px;
    }
</style>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'Produtos', 'gerenciador', $group_id)){ ?>
            <?= link_button_list(__('Listar Produtos'), ['action' => 'gerenciador']); ?>
        <?php } ?>
    </div>
</div>

<div class="produtos form">
    <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
    <fieldset>
        <legend><?= __('Adicionar Produto') ?></legend>
        <h5 class="font-italic"><?= __('Informações Mínimas') ?></h5>
        <br class="clear">

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="form-group">
                    <label for="cod_barras">Código de Barras</label>
                    <p><?= $cod_barras ?></p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-5">
                <div class="form-group">
                    <label for="produto_base_id">Produto Base (víncule produtos iguais, com sabores ou tamanhos diferentes)</label>
                    <?= $this->Form->input('produto_base_id', [
                        'id'      => 'produto_base_id',
                        'options' => $produtos_base_list,
                        'empty' => 'Nenhum',
                        'label' => false,
                        'class' => 'form-control chosen-select validate[optional]'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="name">Nome do Produto</label>
                     <?= $this->Form->input('name', [
                        'label' => false,
                        'placeholder' => 'Nome sem propriedades Ex.: "Whey Protein"',
                        'class' => 'form-control validate[required, minSize[3], maxSize[255]]'
                    ]) ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="marca_id">Marca</label>
                    <?= $this->Form->input('marca_id', [
                    'id'            => 'marcas_select',
                    'options'       => $marcas_list,
                    'empty'         => 'Selecione uma marca',
                    'label'         => false,
                    'class'         => 'form-control validate[required] chosen-select'
                ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="cod_interno">Cód ERP</label>
                     <?= $this->Form->input('cod_interno', [
                        'label' => false,
                        'placeholder' => 'Código ERP',
                        'class' => 'form-control validate[optional]'
                    ]) ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="tamanho">Tamanho</label>
                    <?= $this->Form->input('tamanho', [
                        'placeholder' => 'Ex: G, 3m, 45cm, 500ml',
                        'label' => false,
                        'class' => 'form-control validate[optional]'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="cor">Cor</label>
                     <?= $this->Form->input('cor', [
                        'label' => false,
                        'placeholder' => 'Ex: Vermelho',
                        'class' => 'form-control validate[optional]'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="preco">Preço de venda</label>
                    <?= $this->Form->input('preco', [
                        'label' => false,
                        'type'  => 'text',
                        'class' => 'form-control price validate[required]'
                    ]) ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                    <label for="preco_promo">Preço promoção</label>
                    <?= $this->Form->input('preco_promo', [
                        'label' => false,
                        'type'  => 'text',
                        'class' => 'form-control price validate[optional]'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-8">
                        <h4>Objetivos/Categorias/Subcategorias</h4>
                        <div class="checkbox subCategoria">
                        <?php foreach($objetivos as $objetivo) { ?>
                            <div class="col-xs-12" style="margin-top: 15px;">
                                <div class="col-xs-12">
                                    <label>
                                        <input type="checkbox" name="objetivos[]" class="objetivo-checkbox" value="<?= $objetivo->id ?>"> <?= $objetivo->name ?>
                                    </label>
                                </div>
                                <?php foreach($objetivo->categorias as $categoria) { ?>
                                    <div class="col-xs-12" style="display: none">
                                        <label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" name="categorias[]" class="categoria-checkbox" o-id="<?= $objetivo->id ?>" value="<?= $categoria->id ?>"> <?= $categoria->name ?>
                                        </label>
                                    </div>
                                    <?php foreach ($categoria->subcategorias as $subcategoria) { ?>
                                        <div class="col-xs-12" style="display: none">
                                            <label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="subcategorias[]" class="subcategoria-checkbox" o-id="<?= $objetivo->id ?>" c-id="<?= $categoria->id ?>" value="<?= $subcategoria->id ?>"> <?= $subcategoria->name ?>
                                            </label>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                $(document).on('click', '.objetivo-checkbox', function() {
                    var objetivo_id = $(this).val();
                    var is_checked = $(this).is(':checked');

                    if(is_checked) {
                        $('.categoria-checkbox').each(function() {
                            if($(this).attr('o-id') == objetivo_id) {
                                $(this).parent().parent().show();
                            }
                        });
                    } else {
                        $('.categoria-checkbox').each(function() {
                            if($(this).attr('o-id') == objetivo_id) {
                                $(this).prop('checked', false);
                                $(this).parent().parent().hide();
                            }
                        });

                        $('.subcategoria-checkbox').each(function() {
                            if($(this).attr('o-id') == objetivo_id) {
                                $(this).parent().parent().hide();
                                $(this).prop('checked', false);
                            }
                        });
                    }
                });

                $(document).on('click', '.categoria-checkbox', function() {
                    var categoria_id = $(this).val();
                    var is_checked = $(this).is(':checked');

                    if(is_checked) {
                        $('.subcategoria-checkbox').each(function() {
                            if($(this).attr('c-id') == categoria_id) {
                                $(this).parent().parent().show();
                            }
                        });
                    } else {
                        $('.subcategoria-checkbox').each(function() {
                            if($(this).attr('c-id') == categoria_id) {
                                $(this).prop('checked', false);
                                $(this).parent().parent().hide();
                            }
                        });
                    }
                });
            </script>
        </div> 
        <div class="row">
            <div class="col-xs-12">
                <div class="divider"></div>
                <h5 class="font-italic"><?= __('Informações Complementares') ?></h5>
                <br class="clear">
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <div class="col-sm-1 control-label">
                        <?= $this->Form->label('fotos', 'Foto 1') ?>
                    </div>
                    <div class="col-sm-5">
                        <?= $this->Form->input('fotos[]', [
                            'data-preview' => '#foto1-preview',
                            'id'    => 'foto1',
                            'type'  => 'file',
                            'label' => false,
                            'class' => 'form-control foto_upload validate[required, custom[validateMIME[image/jpeg|image/png]]]'
                        ]) ?>
                    </div>
                    <div class="col-sm-1 text-center">
                        <a href="<?= IMG_URL; ?>produtos/default-acessorio.jpg" class="fancybox" id="foto1-preview-fancybox">
                            <?= $this->Html->image('produtos/default-acessorio.jpg', [
                                'class' => 'foto-produto-preview',
                                'id'    => 'foto1-preview'
                            ])?>
                        </a>
                    </div>
                    <div class="col-sm-1 text-left">
                        <a class="btn btn-danger clear-foto" data-id="foto1">
                            <i class="glyph-icon icon-trash"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <div class="col-sm-1 control-label">
                        <?= $this->Form->label('fotos', 'Foto 2') ?>
                    </div>
                    <div class="col-sm-5">
                        <?= $this->Form->input('fotos[]', [
                            'data-preview' => '#foto2-preview',
                            'id'    => 'foto2',
                            'type'  => 'file',
                            'label' => false,
                            'class' => 'form-control foto_upload validate[optional, custom[validateMIME[image/jpeg|image/png]]]'
                        ]) ?>
                    </div>
                    <div class="col-sm-1 text-center">
                        <a href="<?= IMG_URL; ?>produtos/default-acessorio.jpg" class="fancybox" id="foto2-preview-fancybox">
                            <?= $this->Html->image('produtos/default-acessorio.jpg', [
                                'class' => 'foto-produto-preview',
                                'id'    => 'foto2-preview'
                            ])?>
                        </a>
                    </div>
                    <div class="col-sm-1 text-left">
                        <a class="btn btn-danger clear-foto" data-id="foto2">
                            <i class="glyph-icon icon-trash"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <div class="col-sm-1 control-label">
                        <?= $this->Form->label('fotos', 'Foto 3') ?>
                    </div>
                    <div class="col-sm-5">
                        <?= $this->Form->input('fotos[]', [
                            'data-preview' => '#foto3-preview',
                            'id'    => 'foto3',
                            'type'  => 'file',
                            'label' => false,
                            'class' => 'form-control foto_upload validate[optional, custom[validateMIME[image/jpeg|image/png]]]'
                        ]) ?>
                    </div>
                    <div class="col-sm-1 text-center">
                        <a href="<?= IMG_URL; ?>produtos/default-acessorio.jpg" class="fancybox" id="foto3-preview-fancybox">
                            <?= $this->Html->image('produtos/default-acessorio.jpg', [
                                'class' => 'foto-produto-preview',
                                'id'    => 'foto3-preview'
                            ])?>
                        </a>
                    </div>
                    <div class="col-sm-1 text-left">
                        <a class="btn btn-danger clear-foto" data-id="foto3">
                            <i class="glyph-icon icon-trash"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="form-group">
                    <label for="atributos">Descrição</label>
                    <?= $this->Form->input('atributos', [
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => false,
                        'id'    => 'atributos',
                        'class' => 'form-control validate[optional, minSize[3]] input-block-level'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-md-4">
                <div class="form-group">     
                    <label for="summernote-beneficios">Benefícios</label>
                    <?= $this->Form->input('beneficios', [
                        'id'    => 'summernote-beneficios-input',
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => false,
                        'id'    => 'beneficios',
                        'class' => 'form-control validate[optional, minSize[3]] input-block-level'
                    ]) ?>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="form-group">
                    <label for="summernote-dicas">Dicas</label>
                    <?= $this->Form->input('dicas', [
                        'id'    => 'summernote-dicas-input',
                        'type'  => 'textarea',
                        'rows'  => 4,
                        'label' => false,
                        'id'    => 'dicas',
                        'class' => 'form-control validate[optional, minSize[3]]'
                    ]) ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-xs-12 col-md-9">
            <div class="form-group text-center">
                <button type="button" class="btn btn-alt btn-hoverrr btn-default" style="width: 9.5em" data-toggle="modal" data-target="#modalConfirma"><i class="glyph-icon icon-check-square-o"></i><span> Salvar </button>
                
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="modalConfirma" role="dialog">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Confirmar edição</h4>
                </div>
                <div class="modal-body">
                  <p>Deseja salvar as alterações no produto?</p>
                </div>
                <div class="modal-footer">
                    <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                    ['class' => 'btn btn-alt btn-hoverrr btn-default', 'escape' => false]); ?>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
              </div>
            </div>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>

<script type="text/javascript">
    $('form').on('focus', 'input[type=number]', function (e) {
        $(this).on('mousewheel.disableScroll', function (e) {
            e.preventDefault()
        })
    })
    $('form').on('blur', 'input[type=number]', function (e) {
        $(this).off('mousewheel.disableScroll')
    })
</script>