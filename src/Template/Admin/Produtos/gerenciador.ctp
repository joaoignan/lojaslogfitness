<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'Produtos', 'buscar_cod_barras', $group_id)){ ?>
            <?= link_button_add(__('Novo Produto'), ['controller' => 'Produtos', 'action' => 'buscar_cod_barras']); ?>
        <?php } ?>
    </div>
</div>

<div class="produtos index">
    <?= $this->Form->create(null)?>
    <div class="row">
        <div class="form-group col-md-6">
            <?= $this->Form->input('search', [
                'div'           => false,
                'label'         => false,
                'value'         => $search_query,
                'class'         => 'form-control',
                'placeholder'   => 'Busca'
            ])?>
        </div>
        <div class="form-group text-left">
            <?= $this->Form->button('<i class="glyph-icon icon-search"></i><span> '.__('Buscar Produto').'</span>',
                [
                    'class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 15em;',
                    'escape' => false
                ])
            ?>
        </div>
    </div>
    <?= $this->Form->end()?>

    <?php if(count($produtos) >= 1) { ?>
        <?= $this->Form->create($produtos)?>
        <table class="table table-hover">
            <thead>
            <tr>
                <th class="text-center"></th>
                <th><?= $this->Paginator->sort('Marcas.name', 'Produto') ?></th>
                <th><?= $this->Paginator->sort('Produtos.cod_barras', 'Cód Barras') ?></th>
                <th class="text-right"><?= $this->Paginator->sort('Produtos.preco', 'Preço R$') ?></th>
                <th class="text-right"><?= $this->Paginator->sort('Produtos.preco_promo', 'Promoção R$') ?></th>
                <th class="text-right"><?= $this->Paginator->sort('Produtos.estoque', 'Estoque') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('Produtos.visivel', 'Visivel') ?></th>
                <th class="actions text-center"></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($produtos as $produto): ;?>
                <?php 
                    $fotos = unserialize($produto->fotos);

                    $primeira_foto = explode('produtos/', $fotos[0]);
                    $foto = $primeira_foto[0].'produtos/sm-'.$primeira_foto[1];
                ?>
                <tr>
                    <td class="text-center">
                        <?= $this->Html->image($foto, ['style' => 'max-height: 40px; max-width: 40px;'])?>
                    </td>
                    <td>
                        <?php if ($produto->produto_base->tipo_produto_id == 1) { ?>
                            <?= $this->Html->link($produto->produto_base->marca->name.' - '.$produto->produto_base->name.' '.$produto->propriedade.' - '.$produto->tamanho.$produto->unidade_medida, ['controller' => 'Produtos', 'action' => 'edit', $produto->id]) ?>
                        <?php } else if ($produto->produto_base->tipo_produto_id == 2) { ?>
                            <?= $this->Html->link($produto->produto_base->marca->name.' - '.$produto->produto_base->name.' - '.$produto->tamanho.' '.$produto->cor, ['controller' => 'Produtos', 'action' => 'edit_acessorio', $produto->id]) ?>
                        <?php } ?>
                    </td>
                    <td>
                        <?= $produto->cod_barras ?>
                    </td>
                    <td class="text-right">
                        <?= $this->Form->input('preco['.$produto->id.']', [
                            'value'   => number_format($produto->preco, 2),
                            'id'      => 'preco-'.$produto->id,
                            'data-id' => $produto->id,
                            'type'    => 'text',
                            'div'     => false,
                            'label'   => false,
                            'class'   => 'form-control input-table-number price preco-porcentagem validate[optional]'
                        ]) ?>
                    </td>
                    <td class="text-right">
                        <?= $this->Form->input('preco_promo['.$produto->id.']', [
                            'value'   => number_format($produto->preco_promo, 2),
                            'id'      => 'preco_promo-'.$produto->id,
                            'data-id' => $produto->id,
                            'type'    => 'text',
                            'div'     => false,
                            'label'   => false,
                            'class'   => 'form-control input-table-number price preco-porcentagem validate[optional]'
                        ]) ?>
                    </td>
                    <td class="text-right">
                        <?= $this->Form->input('estoque['.$produto->id.']', [
                            'value'   => $produto->estoque,
                            'id'      => 'estoque-'.$produto->id,
                            'data-id' => $produto->id,
                            'type'    => 'number',
                            'div'     => false,
                            'label'   => false,
                            'class'   => 'form-control input-table-number validate[optional]'
                        ]) ?>
                    </td>
                    <td class="text-center">
                        <?= $this->Form->checkbox('visivel['.$produto->id.']', [
                            'label'     => false,
                            'checked'   => $produto->visivel,
                            'class'     => 'form-control validate[optional] input-switch',
                            'data-on-color' => 'info',
                            'data-on-text'  => 'Sim',
                            'data-off-text' => 'Não',
                            'data-off-color' => 'danger',
                            'data-size'     => 'medium',
                        ]) ?>
                    </td>
                    <td class="text-center">
                        <?php if ($produto->produto_base->tipo_produto_id == 1) { ?>
                           <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                            ['action' => 'edit', $produto->id],
                            ['escape' => false]) ?>
                        <?php } else if ($produto->produto_base->tipo_produto_id == 2) { ?>
                           <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                            ['action' => 'edit_acessorio', $produto->id],
                            ['escape' => false]) ?>
                        <?php } ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                          ['class' => 'btn btn-alt btn-hoverrr btn-default',  'style' =>  'width: 9.5em;',
                              'escape' => false]);
            ?>
        </div>
        <?= $this->Form->end()?>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    <?php } else { ?>
        <h3>Não encontramos produtos com essa busca!</h3>
    <?php } ?>
</div>