<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>
<div class="cca form">
    <?= $this->Form->create('Cca', ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?php echo __('Gerenciar Permissões por Grupo'); ?></legend>

        <div class="form-group">
            <div class="control-label col-sm-2">
                <?php echo $this->Form->label('group_id', 'Grupo'); ?>
            </div>
            <div class="col-sm-4">
                <?php echo $this->Form->input('group_id', [
                    'label' => false,
                    'class' => 'form-control validate[required]',
                    'empty' => 'Selecione...',
                    'values' => $groups
                ]); ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Gerenciar').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?php echo $this->Form->end(); ?>
</div>