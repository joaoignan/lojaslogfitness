<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<?php
foreach($messages as $message){
    echo $message;
}
?>
<div class="groups form">
    <?php echo $this->Form->create('Group', array('class' => 'form-validate')); ?>
    <fieldset>
        <legend>
            <?php echo __('Gerenciar Permissões do Grupo: <strong>'.$cca_group['name'].'</strong>'); ?>
        </legend>
        <table class="table">
            <thead>
            <tr>
                <th colspan="3" class="text-left">Categoria / Página</th>
                <th class="text-left">Controlador / Ação</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $category = '';
            foreach($cca_actions as $key => $controller){
                if($controller['controller'] != $category){
                    if(!in_array($controller['controller'], array('Pages', 'Status'))) {
                        ?>
                        <tr>
                            <td style="width: 8%;">
                                <?php
                                echo $this->Form->checkbox('Controller.controller.' . $controller['id'], array(
                                        'label' => false,
                                        'div' => false,
                                        'checked' => false,
                                        'disabled' => false,
                                        'id' => $controller['controller'],
                                        'class' => 'grupo'
                                    )
                                ); ?>
                            </td>
                            <td class="font-bold" colspan="3"><?php echo __($controller['controller']); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-right" style="width: 10%;">
                                <?php
                                if($cca->cca('admin', $controller['controller'], $controller['action'], $cca_group['id'], false)){
                                    $checked = true;
                                }else{
                                    $checked = false;
                                }
                                echo $this->Form->checkbox('CcaAction.'. $controller['id'], array(
                                        'label' => false,
                                        'div' => false,
                                        'checked' => $checked,
                                        'disabled' => false,
                                        'class' => $controller['controller']
                                    )
                                );
                                ?>
                            </td>
                            <td>
                                <?php echo __($controller['controller']) . ' / ' . __($controller['action']); ?>
                            </td>
                            <td>
                            <span class="font-italic font-gray-dark">
                                /<?php echo strtolower($controller['controller']) . '/' . $controller['action']; ?>
                            </span>
                            </td>
                        </tr>
                    <?php
                    }
                }else {
                    if (!in_array($controller['action'], array())){?>
                        <tr>
                            <td colspan="2" class="text-right" style="width: 10%;">
                                <?php
                                if($cca->cca('admin', $controller['controller'], $controller['action'], $cca_group['id'], false)){
                                    $checked = true;
                                }else{
                                    $checked = false;
                                }
                                echo $this->Form->checkbox('CcaAction.'. $controller['id'], array(
                                        'label' => false,
                                        'div' => false,
                                        'checked' => $checked,
                                        'disabled' => false,
                                        'class' => $controller['controller']
                                    )
                                );
                                ?>
                            </td>
                            <td>
                                <?php echo __($controller['controller']) . ' / ' . __($controller['action']); ?>
                            </td>
                            <td>
                            <span class="font-italic font-gray-dark">
                                /<?php echo strtolower($controller['controller']) . '/' . $controller['action']; ?>
                            </span>
                            </td>
                        </tr>
                    <?php
                    }
                }
                $category = $controller['controller'];
            }
            ?>
            </tbody>
        </table>

        <br class="clear" /><br class="clear" />

        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Salvar Permissões').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default', 'escape' => false]); ?>
        </div>

    </fieldset>
    <?php echo $this->Form->end(); ?>
</div>


<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Groups', 'index', $cca_group['id'])){ ?>
            <?= $this->Html->link('Listar Grupos',
                ['action' => 'index'],
                ['class' => 'btn btn-default', 'escape' => false]);
            ?>
        <?php } if($cca->cca('admin', 'Groups', 'add', $cca_group['id'])){ ?>
            <?= $this->Html->link('Cadastrar Grupo',
                ['action' => 'add'],
                ['class' => 'btn btn-default', 'escape' => false]);
            ?>
        <?php } if($cca->cca('admin', 'Users', 'index', $cca_group['id'])){ ?>
            <?= $this->Html->link('Listar Usuários',
                ['controller' => 'users', 'action' => 'index'],
                ['class' => 'btn btn-default', 'escape' => false]);
            ?>
        <?php } if($cca->cca('admin', 'Users', 'add', $cca_group['id'])){ ?>
            <?= $this->Html->link('Cadastrar Usuário',
                ['controller' => 'users', 'action' => 'add'],
                ['class' => 'btn btn-default', 'escape' => false]);
            ?>
        <?php }  ?>
    </div>
</div>

<br class="clear" />
<div class="related">
    <h3><?php echo __('Usuários Relacionados'); ?></h3>
    <?php if (!empty($cca_group['User'])): ?>
        <table class="table" cellpadding = "0" cellspacing = "0">
            <tr>
                <th><?php echo __('Id'); ?></th>
                <th><?php echo __('Name'); ?></th>
                <th><?php echo __('Image'); ?></th>
                <th><?php echo __('Username'); ?></th>
                <th><?php echo __('Group Id'); ?></th>
                <th><?php echo __('Status Id'); ?></th>
                <th><?php echo __('Created'); ?></th>
                <th><?php echo __('Modified'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
            <?php foreach ($cca_group['User'] as $user): ?>
                <tr>
                    <td><?php echo $user['id']; ?></td>
                    <td><?php echo $user['name']; ?></td>
                    <td><?php echo $user['image']; ?></td>
                    <td><?php echo $user['username']; ?></td>
                    <td><?php echo $cca_group['Group']['name']; ?></td>
                    <td><?php echo $user['status_id']; ?></td>
                    <td><?php echo $user['created']; ?></td>
                    <td><?php echo $user['modified']; ?></td>
                    <td class="actions">
                        <?php if($cca->cca('admin', 'Users', 'view', $cca_group['id'])){ ?>
                            <?php echo $this->Html->link('<i class="glyph-icon icon-file-text-o"></i>',
                                array('controller' => 'users', 'action' => 'view', $user['id']),
                                array('escape' => false, 'class' => 'btn small bg-green tooltip-button'));
                            ?>
                        <?php } if($cca->cca('admin', 'Users', 'edit', $cca_group['id'])){ ?>
                            <?php echo $this->Html->link('<i class="glyph-icon icon-edit"></i>',
                                array('controller' => 'users', 'action' => 'edit', $user['id']),
                                array('escape' => false, 'class' => 'btn small bg-blue-alt tooltip-button'));
                            ?>
                        <?php } if($cca->cca('admin', 'Users', 'delete', $cca_group['id'])){ ?>
                            <?php echo $this->Form->postLink('<i class="glyph-icon icon-remove"></i>',
                                array('controller' => 'users', 'action' => 'delete', $user['id']),
                                array('escape' => false, 'class' => 'btn small bg-red tooltip-button'),
                                __('Are you sure you want to delete # %s?', $user['id'])); ?>
                        <?php } ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>

    <div class="actions">
        <ul>
            <?php if($cca->cca('admin', 'Users', 'add', $cca_group['id'])){ ?>
                <?php echo $this->Html->link('Novo Usuário',
                    ['controller' => 'users', 'action' => 'add'],
                    ['class' => 'btn btn-default', 'escape' => false]);
                ?>
            <?php } ?>
        </ul>
    </div>
</div>