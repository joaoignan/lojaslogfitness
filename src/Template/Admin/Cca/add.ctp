<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="cca form">
    <?= $this->Form->create($cca, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Add Cca') ?></legend>
        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Cca', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Cca'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
        <?php if($cca->cca('admin', 'CcaActions', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Cca Actions'),
                ['controller' => 'CcaActions', 'action' => 'index'],
                ['class' => 'btn btn-default']) ?>
        <?php } ?>
        <?php if($cca->cca('admin', 'CcaActions', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Cca Action'),
                ['controller' => 'CcaActions', 'action' => 'add'],
                ['class' => 'btn btn-default']) ?>
        <?php } ?>
        <?php if($cca->cca('admin', 'Groups', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Groups'),
                ['controller' => 'Groups', 'action' => 'index'],
                ['class' => 'btn btn-default']) ?>
        <?php } ?>
        <?php if($cca->cca('admin', 'Groups', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Group'),
                ['controller' => 'Groups', 'action' => 'add'],
                ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>