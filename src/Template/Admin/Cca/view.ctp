
<div class="cca view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($cca->cca_action_id) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Cca Action') ?></td>
                    <td><?= $cca->has('cca_action') ? $this->Html->link($cca->cca_action->id, ['controller' => 'CcaActions', 'action' => 'view', $cca->cca_action->id]) : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Group') ?></td>
                    <td><?= $cca->has('group') ? $this->Html->link($cca->group->name, ['controller' => 'Groups', 'action' => 'view', $cca->group->id]) : '' ?></td>
                </tr>
            </table>
        </div>

        </table>
    </div>
</div>
</div>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Cca', 'edit', $group_id)){ ?>
            <?= $this->Html->link(__('Edit Cca'), ['action' => 'edit', $cca->cca_action_id], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Cca', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Cca'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Cca', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Cca'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'CcaActions', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Cca Actions'),
                ['controller' => 'CcaActions', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'CcaActions', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Cca Action'),
                ['controller' => 'CcaActions', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Groups', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Groups'),
                ['controller' => 'Groups', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Groups', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Group'),
                ['controller' => 'Groups', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>