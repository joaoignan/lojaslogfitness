<div class="cca index">
    <div class="content-box-wrapper">
        <h3><?= __('Actions') ?></h3>
        <?php if($cca->cca('admin', 'Cca', 'cca_sync', $group_id)){ ?>
            <?= $this->Html->link(__('Sync Controllers / Actions'),
                ['controller' => 'Cca', 'action' => 'cca_sync'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
        <br class="clear" />
        <?php if($cca->cca('admin', 'Cca', 'cca_roles_step_1', $group_id)){ ?>
            <?= $this->Html->link(__('Roles by Group'),
                ['controller' => 'Cca', 'action' => 'cca_roles_step_1'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>

    <br class="clear" />
    <div class="content-box-wrapper">
        <h3><?= __('Groups') ?></h3>
        <?php if($cca->cca('admin', 'Groups', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Groups'),
                ['controller' => 'Groups', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
        <br class="clear" />
        <?php if($cca->cca('admin', 'Groups', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Group'),
                ['controller' => 'Groups', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>

    <br class="clear" />
    <div class="content-box-wrapper">
        <h3><?= __('Users') ?></h3>
        <?php if($cca->cca('admin', 'Users', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Users'),
                ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
        <br class="clear" />
        <?php if($cca->cca('admin', 'Users', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New User'),
                ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>