<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
$filtro_status = $pedido_status->toArray();
$filtro_status = array_reverse($filtro_status);
$filtro_status[] = 'Todos';
$filtro_status = array_reverse($filtro_status);
?>

<style type="text/css">
    .vip-letter {
        font-weight: 900;
        color: #daa520;
        font-size: 18px;
    }

    .btn-default{
        cursor: pointer;
    }

</style>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_listAcademias(__('Listar Academias'), ['controller' => 'Academias', 'action' => 'index']); ?>
        <?= link_button_listClientes(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']); ?>

        <div class="col-md-8 float-right">
            <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row']); ?>

            <div class="col-sm-5">
                <?= $this->Form->input('filtro_search', [
                    'id'    => 'filtro_search',
                    'value' => $search_val,
                    'label' => false,
                    'placeholder' => 'Busca',
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>

            <div class="col-sm-5">
                <?= $this->Form->input('filtro_status', [
                    'options' => $filtro_status,
                    'value' => $status,
                    'id'    => 'filtro_status' ,
                    'empty' => 'Selecione ...',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>

            <div class="col-sm-1 text-center">
                <?= $this->Form->button('<i class="glyph-icon icon-search"></i> <span>Filtrar</span>',
                    ['class' => 'btn btn-alt btn-default',
                        'escape' => false
                    ]); ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>

</div>

<div class="pedidos index">
    <?= $this->Form->create(null)?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th class="text-center"><?= $this->Paginator->sort('Pedidos.id', 'Cod') ?></th>
            <th><?= $this->Paginator->sort('Clientes.name', 'Cliente') ?></th>
            <th><?= $this->Paginator->sort('Academias.name', 'Academia') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('Pedidos.valor', 'Valor') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('Pedidos.created', 'Data Pedido') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('Pedidos.data_pagamento', 'Data Pagamento') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('Pedidos.pedido_status_id', 'Status') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($pedidos as $pedido): ?>
            <?php if ($pedido->pedido_status_id != 8) { ?>
                <tr class="pedido-linha"
                    data-pid="<?= $pedido->id ?>"
                    title=""
                    data-toggle="modal"
                    data-target="#pid-<?= $pedido->id ?>">
                    <td class="text-center"><?= $this->Number->format($pedido->id) ?></td>
                    <td><?= $pedido->cliente->name ?></td>
                    <td>
                        <?= '#'.$pedido->academia_id.' - '.$pedido->academia->name ?>
                        <br>
                        <small><?= $pedido->academia->shortname ?></small>
                        <br>
                        <small><?= $pedido->academia->city->name.'/'.$pedido->academia->city->uf ?></small>
                        <br>
                        <small><?= $pedido->professore->name ? 'Professor: '.$pedido->professore->name : '' ?></small>
                    </td>
                    <td class="text-center">R$ <?= number_format($pedido->valor, 2, ',', '.') ?>
                        <?php 
                            if($pedido->cupom_desconto_id != null) {
                                foreach ($cupom_desconto as $cd) {
                                    if($pedido->cupom_desconto_id == $cd->id) { ?>
                                        <span class="glyph-icon icon-ticket" title="Cupom utilizado: <?= $cd->codigo ?>" style="color: green"></span>
                                    <?php };
                                } 
                            } else if($pedido->type == 'combo') { ?>
                                <p class="vip-letter" title="Combo!" style="cursor: default">C</p>
                            <?php };

                           if ($pedido->retirada_loja == 601) { ?>
                            <p><a class="btn-loja btn btn-alt btn-hoverrr btn-default" title="Dados de Entrega" data-toggle="popover" data-trigger="click" data-content="<?=$pedido->address ?>, <?=$pedido->number ?> <br> <?=$pedido->area ?> <br> <span>CEP:</span> <?=$pedido->cep ?> <br>  <?= $pedido->city_name?> - <?=$pedido->uf ?>"> <i class="glyph-icon icon-truck"></i> <span style="font-size: 12px;">Na loja</span></a></p>
                           <?php } 
                           
                           elseif ($pedido->retirada_loja == 10) { ?>
                            <p><a class="btn-academia btn btn-alt btn-hoverrr btn-default" title="Dados de Entrega" data-toggle="popover" data-trigger="click" data-content="<?=$pedido->address ?>, <?=$pedido->academia->number ?> <br> <?=$pedido->area ?> <br> <span>CEP:</span> <?=$pedido->cep ?> <br>  <?= $pedido->city_name?> - <?=$pedido->uf ?>"> <i class="glyph-icon icon-truck"></i> <span style="font-size: 12px;">Na Academia</span></a></p>
                           <?php }
                           
                           elseif ($pedido->retirada_loja == 20) { ?>
                            <p><a class="btn-personalizado btn btn-alt btn-hoverrr btn-default" title="Dados de Entrega" data-toggle="popover" data-trigger="click" data-content="<?=$pedido->address ?>, <?=$pedido->number ?> <br> <?=$pedido->area ?> <br> <span>CEP:</span> <?=$pedido->cep ?> <br>  <?= $pedido->city_name?> - <?=$pedido->uf ?>"> <i class="glyph-icon icon-truck"></i> <span style="font-size: 12px;">Personalizado</span></a></p>
                           <?php }; ?>
                            
                            

                    </td>
                    <td class="text-center"><?= h($pedido->created) ?></td>
                    <td class="text-center"><?= ($pedido->data_pagamento ? $pedido->data_pagamento : "Não pago") ?></td>
                    <td class="text-centerrrr">
                        <?php if($group_id != 7) { ?>
                            <?= $this->Form->input('pedido_status_id['.$pedido->id.']', [
                                'label'     => false,
                                'div'       => false,
                                'options'   => $pedido_status,
                                'value'     => $pedido->pedido_status_id,
                                'class'   => 'form-control chosen-select validate[required]'
                            ]) ?>
                        <?php } else { ?>
                            <?= $this->Form->input('pedido_status_id['.$pedido->id.']', [
                                'label'     => false,
                                'div'       => false,
                                'disabled'  => true,
                                'options'   => $pedido_status,
                                'value'     => $pedido->pedido_status_id,
                                'class'     => 'form-control chosen-select validate[required]'
                            ]) ?>
                        <?php } ?>
                    </td>
                    <td class="actions text-center">
                        <div class="btn-group">
                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                                <i class="glyph-icon icon-navicon"></i>
                                <span class="sr-only"><?= __('Actions'); ?></span>
                            </button>
                            <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                                <li>
                                    <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                        ['action' => 'view', $pedido->id],
                                        ['escape' => false, 'target' => '_blank']) ?>
                                </li>
                                <?php if($group_id != 7) { ?>
                                    <li>
                                        <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                            ['action' => 'edit', $pedido->id],
                                            ['escape' => false]) ?>
                                    </li>
                                <?php } ?>
                                <?php if ($pedido->pedido_status_id == 7) { ?>
                                    <li>
                                        <?= $this->Html->link('<i class="glyph-icon icon-trash"></i> Excluir',
                                            ['controller' => 'pedidos', 'action' => 'excluir_pedido', $pedido->id],
                                            ['escape' => false]) 
                                        ?>
                                    </li>                                
                                <?php } ?>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        <?php endforeach; ?>
        </tbody>
    </table>

    <br class="clear">
    <?php if($group_id != 7) { ?>
        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Salvar Alterações').'</span>',
                ['class' => 'btn btn-alt btn-hoverrr btn-default', 'escape' => false]);
            ?>
        </div>
        <br class="clear">
    <?php } ?>
    <?= $this->Form->create(null)?>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
     <!--    PEDIDOS STATUS -->
    <br class="clear">
    <br class="clear">
    <div class="text-center col-md-offset-3" style="width: 400px">
        <table class="table table-hover">
            <thead>
            <tr>
                <th colspan="3">Pedidos por Status</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-right" style="width: 200px">Realizados</td>
                <td class="text-left" style="width: 200px"><?= $pedido_realizado ?></td>
            </tr>
            <tr>
                <td class="text-right" style="width: 200px">Aguardando Pagamento</td>
                <td class="text-left" style="width: 200px"><?= $pedido_aguardando ?></td>
            </tr>
            <tr>
                <td class="text-right" style="width: 200px">Pagos</td>
                <td class="text-left" style="width: 200px"><?= $pedido_pago ?></td>
            </tr>
            <tr>
                <td class="text-right" style="width: 200px">Preparando para Entrega</td>
                <td class="text-left" style="width: 200px"><?= $pedido_preparando ?></td>
            </tr>
            <tr>
                <td class="text-right" style="width: 200px">Em Faturamento</td>
                <td class="text-left" style="width: 200px"><?= $pedido_em_faturamento ?></td>
            </tr>
            <tr>
                <td class="text-right" style="width: 200px">Em Transporte</td>
                <td class="text-left" style="width: 200px"><?= $pedido_transporte ?></td>
            </tr>
            <tr>
                <td class="text-right" style="width: 200px">Aguardando Retirada</td>
                <td class="text-left" style="width: 200px"><?= $pedido_aguardando_retirada ?></td>
            </tr>
            <tr>
                <td class="text-right" style="width: 200px">Entregues</td>
                <td class="text-left" style="width: 200px"><?= $pedido_entregue ?></td>
            </tr>
            <tr>
                <td class="text-right" style="width: 200px">Cancelados</td>
                <td class="text-left" style="width: 200px"><?= $pedido_cancelado ?></td>
            </tr>
            <tr>
                <td class="text-right" style="width: 200px">Excluidos</td>
                <td class="text-left" style="width: 200px"><?= $pedido_excluido ?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
$(document).ready(function(){
    // $('.btn-academia').popover({title: "Dados de entrega", content: "<?=$pedido->academia->address ?>, <?=$pedido->academia->number ?> <br> <?=$pedido->academia->area ?> <br> <span>CEP:</span> <?=$pedido->academia->cep ?> <br>  <?= $pedido->academia->city->name?> - <?=$pedido->academia->city->uf ?>", trigger: "click", html: true}); 

    // $('.btn-loja').popover({title: "Dados de entrega", content: "<?=$pedido->cliente->address ?>, <?=$pedido->cliente->number ?> <br> <?=$pedido->cliente->area ?> <br> <span>CEP:</span> <?=$pedido->cliente->cep ?> <br>  <?= $pedido->academia->city->name?> - <?=$pedido->academia->city->uf ?>", trigger: "click", html: true}); 

    // $('.btn-personalizado').popover({title: "Dados de entrega", content: "<?=$pedido->cliente->address ?>, <?=$pedido->cliente->number ?> <br> <?=$pedido->cliente->area ?> <br> <span>CEP:</span> <?=$pedido->cliente->cep ?> <br>  <?= $pedido->academia->city->name?> - <?=$pedido->academia->city->uf ?>", trigger: "click", html: true}); 

    $('[data-toggle="popover"]').popover({html: true}); 

    $('.btn').on('click', function (e) {
        $('.btn').not(this).popover('hide');
});

});
</script>