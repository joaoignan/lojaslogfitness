<?php
$this->assign('title', 'Carteira LOG');

$exibir = 0;
?>

<?php foreach ($pedidos_total as $pedido_total) {
    if($pedido_total->academia_id != null) {
        $exibir = 1;
    }
} ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#pagar-btn').click(function(e) {
            e.preventDefault();
            if (confirm("tem certeza?")) {
                if(confirm("pensa bem em..")) {
                    if(confirm("não vai fazer besteira!")) {
                        if(confirm("depois não diga que não avisei!")) {
                            location.href = WEBROOT_URL + 'admin/pedidos/carteira_log/'+ $(this).attr('data-id');
                        }
                    }
                }
            }
        });
    });
</script>

<?php if($exibir == 1) { ?>
    <div class="row" style="margin-bottom: 100px">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Academia</th>
                    <th class="text-right">Valor total</th>
                    <th class="actions text-center"></th>
                </tr>
            </thead>
            <tbody>
            	<?php foreach ($pedidos_total as $pedido_total): ?>
    	            <tr>
    	                <td><?= h($pedido_total->academia_name) ?></td>
                        <td class="text-right"> R$ <?= number_format($pedido_total->total, 2, ',', '.') ?></td>
                        <td class="actions text-center">
                            <div class="btn-group">
                                <?= $this->Html->link('<i class="fa fa-pencil"></i> Pago',
                                              '#',
                                              [
                                                'escape'  => false, 
                                                'class'   => 'btn bg-blue', 
                                                'data-id' => $pedido_total->academia_id,
                                                'id'      => 'pagar-btn'
                                            ]) ?>
                            </div>
                        </td>
    	            </tr>
    	        <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php } ?>

<div class="row">
    <table class="table table-hover">
        <thead>
            <tr>
                <th class="text-center"><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('Academia.shortname', 'Academia') ?></th>
                <th class="text-right"><?= $this->Paginator->sort('valor', 'Valor') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('carteira_status', 'Status') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pedidos as $pedido): ?>
                <tr>
                    <td class="text-center"><?= $this->Number->format($pedido->id) ?></td>
                    <td><?= h($pedido->academia->shortname) ?></td>
                    <td class="text-right"> R$ <?= number_format($pedido->valor, 2, ',', '.') ?></td>
                    <td class="text-center">
                        <?= $pedido->carteira_status == 1 ? 
                            '<span style="color:green">Pago</span>' : 
                            '<span style="color:red">Não Pago</span>' ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>