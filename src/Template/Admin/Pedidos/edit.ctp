<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
         <?= $this->Html->link('<i class="glyph-icon icon-suitcase"></i> '.__('Listar Pedidos'),
            ['action' => 'index'], ['class' => 'btn btn-default', 'escape' => false]) ?>
        <?= $this->Html->link('<i class="glyph-icon icon-building"></i> '.__('Listar Academias'),
            ['controller' => 'Academias', 'action' => 'index'], ['class' => 'btn btn-default', 'escape' => false]) ?>
        <?= $this->Html->link('<i class="glyph-icon icon-users"></i> '.__('Listar Clientes'),
            ['controller' => 'Clientes', 'action' => 'index'], ['class' => 'btn btn-default', 'escape' => false]) ?>
    
    </div>
</div>

<div class="pedidos form">
    <?= $this->Form->create($pedido, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Editar Pedido') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('Cod') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('pedido_id', [
                    'type' => 'text',
                    'value' => $pedido->id,
                    'readonly'  => true,
                    'disabled'  => true,
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('cliente_id') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('cliente_id', [
                    'type' => 'text',
                    'value' => $pedido->cliente->name,
                    'readonly'  => true,
                    'disabled'  => true,
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('academia_id') ?>
            </div>
            <div class="col-sm-5">
                <?= $this->Form->input('academia_id', [
                    'type'      => 'text',
                    'value'     => $pedido->academia->name,
                    'readonly'  => true,
                    'disabled'  => true,
                    'label' => false,
                    'class' => 'form-control'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('valor') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('valor', [
                    'label' => false,
                    'readonly'  => true,
                    'disabled'  => true,
                    'class' => 'form-control validate[]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('payment_method') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('payment_method', [
                    'label' => false,
                    'readonly'  => true,
                    'disabled'  => true,
                    'class' => 'form-control validate[]'
                ]) ?>
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tid') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('tid', [
                    'label' => false,
                    'readonly'  => true,
                    'disabled'  => true,
                    'class' => 'form-control validate[]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('pedido_status_id') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('pedido_status_id', [
                    'options' => $pedidoStatus,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('Quem recebeu?') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('quem_recebeu', [
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control validate[]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('entrega', 'Entrega Realizada em:') ?>
            </div>
            <div class='col-sm-3'>
                <div class='input-group date datetimepicker'>
                    <?= $this->Form->input('entrega', [
                        'value' => !empty($pedido->entrega) ? date_format($pedido->entrega, 'd/m/Y') : '',
                        'type'  => 'text',
                        'label' => false,
                        'class' => 'form-control validate[]'
                    ]) ?>
                    <span class="input-group-addon">
                        <span class="glyph-icon icon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('Observações') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('observacao', [
                    'empty' => true,
                    'label' => false,
                    'type'  => 'textarea',
                    'class' => 'form-control validate[]'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
        <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrrr btn-default', 'style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


