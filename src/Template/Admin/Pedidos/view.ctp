<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
       <?= $this->Html->link('<i class="glyph-icon icon-suitcase"></i> '.__('Listar Pedidos'),
            ['action' => 'index'], ['class' => 'btn btn-default', 'escape' => false]) ?>
        <?= $this->Html->link('<i class="glyph-icon icon-building"></i> '.__('Listar Academias'),
            ['controller' => 'Academias', 'action' => 'index'], ['class' => 'btn btn-default', 'escape' => false]) ?>
        <?= $this->Html->link('<i class="glyph-icon icon-users"></i> '.__('Listar Clientes'),
            ['controller' => 'Clientes', 'action' => 'index'], ['class' => 'btn btn-default', 'escape' => false]) ?>    
    </div>

</div>

<div class="pedidos view" xmlns="http://www.w3.org/1999/html">
    <h2>Pedido #<?= h($pedido->id) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Cliente') ?></td>
                    <td><?= $pedido->has('cliente') ? $this->Html->link($pedido->cliente->name, ['controller' => 'Clientes', 'action' => 'view', $pedido->cliente->id]) : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Academia') ?></td>
                    <td><?= $pedido->has('academia') ? $this->Html->link($pedido->academia->name, ['controller' => 'Academias', 'action' => 'view', $pedido->academia->id]) : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status') ?></td>
                    <td><?= $pedido->has('pedido_status') ? $this->Html->link($pedido->pedido_status->name, '#') : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Payment Method') ?></td>
                    <td><?= h($pedido->payment_method) ?></td>
                </tr>

            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($pedido->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Valor') ?></td>
                    <td><?= $this->Number->format($pedido->valor) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Tid') ?></td>
                    <td><?= $this->Number->format($pedido->tid) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Entrega') ?></td>
                    <td><?= h($pedido->entrega) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($pedido->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($pedido->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="column large-12">
        <h4 class="font-bold"><?= __('Itens do Pedido') ?></h4>
        <?php if (!empty($pedido->pedido_itens)): ?>
            <table class="table table-hover">
                <tr>
                    <th><?= __('Cód') ?></th>
                    <th><?= __('Produto') ?></th>
                    <th><?= __('Quantidade') ?></th>
                    <th><?= __('Preco') ?></th>
                    <th><?= __('Total') ?></th>
                </tr>
                <?php foreach ($pedido->pedido_itens as $pedidoItens): ?>
                    <tr>
                        <td><?= h($pedidoItens->produto_id) ?></td>
                        <td><a href="/admin/produtos/edit/<?= $pedidoItens->produto_id ?>" target="_blank"><?= h($pedidoItens->produto->produto_base->name.' '
                                .$pedidoItens->produto->produto_base->embalagem_conteudo.' '
                                .$pedidoItens->produto->propriedade) ?></a></td>
                        <td><?= h($pedidoItens->quantidade) ?></td>
                        <td>R$ <?= h(number_format($pedidoItens->preco, 2, ',', '.')) ?></td>
                        <td>R$ <?= h(number_format($pedidoItens->quantidade * $pedidoItens->preco, 2, ',', '.')) ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>