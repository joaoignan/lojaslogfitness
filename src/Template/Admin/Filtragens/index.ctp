<div class="fornecedores index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('palavra_chave') ?></th>
            <th><?= $this->Paginator->sort('qtd') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($filtragens as $filtragem): ?>
            <tr>
                <td><?= $this->Number->format($filtragem->id) ?></td>
                <td><?= h($filtragem->palavra_chave) ?></td>
                <td><?= h($filtragem->qtd) ?></td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


