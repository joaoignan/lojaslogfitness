<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="cities form">
    <?= $this->Form->create($city, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Add City') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('state_id') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('state_id', [
                    'options' => $states,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('uf') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('uf', [
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[2], maxSize[2]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[3], maxSize[50]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Cities', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Cities'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
        <?php if($cca->cca('admin', 'States', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List States'),
                ['controller' => 'States', 'action' => 'index'],
                ['class' => 'btn btn-default']) ?>
        <?php } ?>
        <?php if($cca->cca('admin', 'States', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New State'),
                ['controller' => 'States', 'action' => 'add'],
                ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>