<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);

$dias[12] = 'Meio dia';

for ($i = 1; $i <= 30; $i++) {
    $dias[$i*24] = $i;
}
?>

<div class="cities form">
    <?= $this->Form->create($city, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Edit City') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('state_id') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('state_id', [
                    'options' => $states,
                'empty' => true,
                'label' => false,
                'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('uf') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('uf', [
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[2], maxSize[2]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[3], maxSize[50]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('prazo_inicial') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('prazo_inicial', [
                    'label' => false,
                    'options' => $dias,
                    'empty' => 'Selecione um prazo...',
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('transportadora_id') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('transportadora_id', [
                    'label' => false,
                    'options' => $transportadoras_list,
                    'empty' => 'Selecione uma transportadora...',
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('valor_transportadora') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('valor_transportadora', [
                    'label' => false,
                    'type' => 'text',
                    'class' => 'form-control validate[optional] price'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Submit').'</span>'.
            '<i class="glyph-icon icon-arrow-right"></i>',
            ['class' => 'btn btn-alt btn-hover btn-default',
            'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>