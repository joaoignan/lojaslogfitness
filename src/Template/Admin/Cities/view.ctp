
<div class="cities view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($city->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('State') ?></td>
                    <td><?= $city->has('state') ? $this->Html->link($city->state->name, ['controller' => 'States', 'action' => 'view', $city->state->id]) : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Uf') ?></td>
                    <td><?= h($city->uf) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($city->name) ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($city->id) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Cities', 'edit', $group_id)){ ?>
            <?= $this->Html->link(__('Edit City'), ['action' => 'edit', $city->id], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Cities', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Cities'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Cities', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New City'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'States', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List States'),
                ['controller' => 'States', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'States', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New State'),
                ['controller' => 'States', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>

</div>
