<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= $this->Form->input(null, [
            'options' => $states,
            'value' => $state_id,
            'label' => false,
            'class' => 'form-control chosen-select filter_states'
        ]) ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.filter_states').change(function() {
            var value = $(this).val();

            location.href = WEBROOT_URL + 'admin/cities/index/' + value;
        });
    });
</script>

<div class="cities index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('state_id') ?></th>
            <th><?= $this->Paginator->sort('uf') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('prazo_inicial') ?></th>
            <th><?= $this->Paginator->sort('transportadora_id') ?></th>
            <th><?= $this->Paginator->sort('valor_transportadora') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($cities as $city): ?>
            <tr>
                <td><?= $this->Number->format($city->id) ?></td>
                <td>
                    <?= $city->has('state') ? $this->Html->link($city->state->name, ['controller' => 'States', 'action' => 'view', $city->state->id]) : '' ?>
                </td>
                <td><?= h($city->uf) ?></td>
                <td><?= h($city->name) ?></td>
                <td><?= $city->prazo_inicial > 0 ? $city->prazo_inicial.' dias' : '' ?></td>
                <td><?= $city->has('transportadora') ? $city->transportadora->name : '' ?></td>
                <td><?= $city->valor_transportadora ? 'R$'.number_format($city->valor_transportadora, 2, ',', '.') : '' ?></td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?/*= __('Actions'); */?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <!-- <li>
                                <?/*= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                    ['action' => 'view', $city->id],
                                    ['escape' => false]) */?>
                            </li> -->
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                    ['action' => 'edit', $city->id],
                                    ['escape' => false]) ?>
                            </li>
                            <!-- <li>
                                <?/*= $this->Form->postLink('<i class="glyph-icon icon-trash-o"></i>  '.__('Delete'),
                                    ['action' => 'delete', $city->id],
                                    ['confirm' => __('Are you sure you want to delete # {0}?', $city->id), 'escape' => false]) */?>
                            </li> -->
                        </ul>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
</div>