<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<style>
	.form-cupom{
		margin: 10px 0;
	}

	.btn-cupom{
		margin: 10px 0;
	}
</style>

<div class="content">
	<div class="row form-cupom">
		<?= $this->Form->create(null, ['id' => 'form-cupom', 'role' => 'form', 'default' => false,
		'class' => '' ])?>
		<div class="col-sm-2">
			<?= $this->Form->input('descricao', [
		        'div'           => false,
		        'label'         => 'Descrição',
		        'class'         => 'input-cadastro form-control validate[required]',
		        'placeholder'   => 'Descreva o cupom',
	        ])?>
        </div>
		<div class="col-sm-2">
			<?= $this->Form->input('codigo', [
		        'div'           => false,
		        'label'         => 'Tag de desconto',
		        'class'         => 'input-cadastro form-control validate[required]',
		        'placeholder'   => 'Ex: log20',
	        ])?>
	    </div>
	    <div class="col-sm-2">
			<?= $this->Form->input('quantidade', [
		        'div'           => false,
		        'type'			=> 'number',
		        'label'         => 'Qtd. de cupons',
		        'class'         => 'input-cadastro form-control validate[required]',
		        'placeholder'   => 'Ex: 15',
	        ])?>
	    </div>
	    <div class="col-sm-2">
	    	<label>Desconto em</label>
	    	<select name="tipo" class="form-control">
	    		<option value="1">Porcentagem</option>
	    		<option value="2">Valor</option>
	    	</select>
		</div>

	    <div class="col-sm-2">
	    	<label>Repetir cupom/cpf?</label>
			<?= $this->Form->input('repetir_cupom', [
                        'options'  => [
                            0 => 'Não',
                            1 => 'Sim',
                          ],
                        'name'  => 'repetir_cupom',
                        'value' => $cupom_desconto->repetir_cupom,
                        'label' => false,
                        'class' => 'form-control'
             ]) ?>
		</div>
		
		<div class="col-sm-2">
			<?= $this->Form->input('valor', [
		        'div'           => false,
		        'label'         => 'Valor do cupom',
		        'class'         => 'input-cadastro form-control validate[required]',
		        'placeholder'   => 'Ex: 15',
		    ])?>
		</div>
		<div class='col-sm-2'>
			<div>
                <?= $this->Form->label('Data vencimento') ?>
            </div>
            <div class='input-group date datetimepicker'>
                <?= $this->Form->input('limite', [
                    'value' => !empty($cupom->limite) ? date_format($cupom->limite, 'd/m/Y') : '',
                    'type'  => 'text',
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
                <span class="input-group-addon">
                    <span class="glyph-icon icon-calendar"></span>
                </span>
            </div>
        </div>
	</div>
	<div class="row btn-cupom">
		<div class="col-xs-12 text-center">
			<?= $this->Form->button('<i class="glyph-icon icon-ticket"></i><span> '.__('Submit').'</span>',
                 ['class' => 'btn btn-alt btn-hoverrr btn-default',  'style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
		</div>
		<?= $this->Form->end()?>
	</div>
</div>