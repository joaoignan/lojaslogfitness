<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
         <?= $this->Html->link('<i class="glyph-icon icon-ticket"></i> '.__('Listar Cupons'),
            ['action' => 'index'], ['class' => 'btn btn-default', 'escape' => false]) ?>
    </div>
</div>

<div class="content">
	<div class="row">
		<?= $this->Form->create($cupom, ['role' => 'form', 'default' => false,
		'class' => '' ])?>
		<div class="col-sm-2">
			<?= $this->Form->input('descricao', [
		        'div'           => false,
		        'label'         => 'Descrição',
		        'value'         => $cupom->descricao,
		        'class'         => 'input-cadastro form-control validate[required]',
		        'placeholder'   => 'Descreva o cupom',
	        ])?>
        </div>
		<div class="col-sm-2">
			<?= $this->Form->input('codigo', [
		        'div'           => false,
		        'label'         => 'Tag de desconto',
		        'value'         => $cupom->codigo,
		        'class'         => 'input-cadastro form-control validate[required]',
		        'placeholder'   => 'Ex: log20',
	        ])?>
	    </div>
	    <div class="col-sm-2">
	    	<label>Desconto em</label>
	    	<select name="tipo" class="form-control">
	    		<option value="1" <?= $cupom->tipo == 1 ? 'selected' : '' ?>>Porcentagem</option>
	    		<option value="2" <?= $cupom->tipo == 2 ? 'selected' : '' ?>>Valor</option>
	    	</select>
		</div>

	    <div class="col-sm-2">
	    	<label>Repetir cupom/cpf?</label>
			<?= $this->Form->input('repetir_cupom', [
                        'options'  => [
                            0 => 'Não',
                            1 => 'Sim',
                          ],
                        'name'  => 'repetir_cupom',
                        'value' => $cupom_desconto->repetir_cupom,
                        'label' => false,
                        'class' => 'form-control'
             ]) ?>
		</div>

		<div class="col-sm-2">
			<?= $this->Form->input('valor', [
		        'div'           => false,
		        'label'         => 'Valor do cupom',
		        'value'         => $cupom->valor,
		        'class'         => 'input-cadastro form-control validate[required]',
		        'placeholder'   => 'Ex: 15',
		    ])?>
		</div>
		<div class="col-sm-2">
			<?= $this->Form->input('quantidade', [
		        'div'           => false,
		        'type'			=> 'number',
		        'label'         => 'Qtd. de cupons',
		        'value'			=> $cupom->quantidade,
		        'class'         => 'input-cadastro form-control validate[required]',
		        'placeholder'   => 'Ex: 15',
	        ])?>
	    </div>
		<div class='col-sm-2'>
            <div class='input-group date datetimepicker'>
                <?= $this->Form->input('limite', [
                    'value' => !empty($cupom->limite) ? date_format($cupom->limite, 'd/m/Y') : '',
                    'type'  => 'text',
                    'label' => 'Data vencimento',
                    'class' => 'form-control validate[required]'
                ]) ?>
                <span class="input-group-addon">
                    <span class="glyph-icon icon-calendar"></span>
                </span>
            </div>
        </div>
	</div>
		<div class="col-xs-12 text-center">
			<?= $this->Form->button('<i class="glyph-icon icon-ticket"></i><span> '.__('Submit').'</span>',
                 ['class' => 'btn btn-alt btn-hoverrr btn-default',  'style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
		</div>
		<?= $this->Form->end()?>
</div>