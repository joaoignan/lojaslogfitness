<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="clientes index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('descricao') ?></th>
            <th><?= $this->Paginator->sort('codigo') ?></th>
            <th><?= $this->Paginator->sort('valor') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('limite') ?></th>
            <th><?= $this->Paginator->sort('status') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($cupons as $cupom): ?>
        	<?php $cupom->status == 1 ? $status = 1 : $status = 2 ?>
        	<?php if($cupom->limite < $data_atual) {
        		$status = 2;
        	} ?>
            <tr>
                <td><?= $this->Number->format($cupom->id) ?></td>
                <td><?= h($cupom->descricao) ?></td>
                <td><?= h($cupom->codigo) ?></td>
                <td><?= h($cupom->valor) ?></td>
                <td><?= h($cupom->created) ?></td>
                <td><?= h($cupom->limite) ?></td>
                <td><?= h($status) == 1 ? 'Ativo' : 'Inativo'  ?></td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                    ['action' => 'edit', $cupom->id],
                                    ['escape' => false]) ?>
                            </li>                           
                        </ul>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


