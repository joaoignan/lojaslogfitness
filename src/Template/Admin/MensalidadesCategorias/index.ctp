
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_add(__('Nova Categoria'), ['action' => 'add']); ?>
    </div>

</div>

<div class="categorias index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('taxa boleto') ?></th>
            <th><?= $this->Paginator->sort('taxa cartao1') ?></th>
            <th><?= $this->Paginator->sort('taxa cartao2') ?></th>
            <th><?= $this->Paginator->sort('taxa cartao3') ?></th>
            <th><?= $this->Paginator->sort('taxa cartao4') ?></th>
            <th><?= $this->Paginator->sort('taxa antecipacao') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('modified') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($categorias as $categoria): ?>
            <tr>
                <td><?= $this->Number->format($categoria->id) ?></td>
                <td><?= h($categoria->name) ?></td>
                <td>R$ <?= number_format($categoria->tax_boleto, 2, ',', '.') ?></td>
                <td>R$ <?= number_format($categoria->tax_card_valor1, 2, ',', '.') ?> - <?= number_format($categoria->tax_card_porcent1, 2, ',', '.') ?>%</td>
                <td>R$ <?= number_format($categoria->tax_card_valor2, 2, ',', '.') ?> - <?= number_format($categoria->tax_card_porcent2, 2, ',', '.') ?>%</td>
                <td>R$ <?= number_format($categoria->tax_card_valor3, 2, ',', '.') ?> - <?= number_format($categoria->tax_card_porcent3, 2, ',', '.') ?>%</td>
                <td>R$ <?= number_format($categoria->tax_card_valor4, 2, ',', '.') ?> - <?= number_format($categoria->tax_card_porcent4, 2, ',', '.') ?>%</td>
                <td><?= h($categoria->tax_antecipacao) ?>%</td>
                <td><?= h($categoria->created) ?></td>
                <td><?= h($categoria->modified) ?></td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                    ['action' => 'edit', $categoria->id],
                                    ['escape' => false]) ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


