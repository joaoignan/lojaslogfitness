<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<script type="text/javascript">
    $(document).ready(function(){
      $('.taxa-fixa').mask('00.00', {reverse: true});
      $('.taxa-porcentagem').mask('00.00%', {reverse: true});
    });
</script>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideee">
        <?= link_button_list(__('List Categorias'), ['action' => 'index']); ?>
    </div>
</div>

<div class="categorias form">
    <?= $this->Form->create($categoria, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Edit Categoria') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_boleto') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_boleto', [
                    'label' => false,
                    'class' => 'form-control taxa-fixa validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_valor1') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_valor1', [
                    'label' => false,
                    'class' => 'form-control taxa-fixa validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_percent1') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_percent1', [
                    'label' => false,
                    'class' => 'form-control taxa-porcentagem validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_valor2') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_valor2', [
                    'label' => false,
                    'class' => 'form-control taxa-fixa validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_percent2') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_percent2', [
                    'label' => false,
                    'class' => 'form-control taxa-porcentagem validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_valor3') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_valor3', [
                    'label' => false,
                    'class' => 'form-control taxa-fixa validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_percent3') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_percent3', [
                    'label' => false,
                    'class' => 'form-control taxa-porcentagem validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_valor4') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_valor4', [
                    'label' => false,
                    'class' => 'form-control taxa-fixa validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_percent4') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_percent4', [
                    'label' => false,
                    'class' => 'form-control taxa-porcentagem validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_antecipacao') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_antecipacao', [
                    'label' => false,
                    'class' => 'form-control taxa-porcentagem validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 9.5em;', 'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


