<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<script type="text/javascript">
    $(document).ready(function(){
      $('.taxa-fixa').mask('00.00', {reverse: true});
      $('.taxa-porcentagem').mask('00.00%', {reverse: true});
    });
</script>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?= $this->Html->link(__('List Categorias'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
    </div>
</div>

<div class="categorias form">
    <?= $this->Form->create($categoria, ['type' => 'file','class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Adicionar Diferencial') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_boleto') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_boleto', [
                    'label' => false,
                    'type'  => 'text',
                    'class' => 'form-control taxa-fixa validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_valor1') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_valor1', [
                    'label' => false,
                    'type'  => 'text',
                    'class' => 'form-control taxa-fixa validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_porcent1') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_porcent1', [
                    'label' => false,
                    'type'  => 'text',
                    'class' => 'form-control taxa-porcentagem validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_valor2') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_valor2', [
                    'label' => false,
                    'type'  => 'text',
                    'class' => 'form-control taxa-fixa validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_porcent2') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_porcent2', [
                    'label' => false,
                    'type'  => 'text',
                    'class' => 'form-control taxa-porcentagem validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_valor3') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_valor3', [
                    'label' => false,
                    'type'  => 'text',
                    'class' => 'form-control taxa-fixa validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_porcent3') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_porcent3', [
                    'label' => false,
                    'type'  => 'text',
                    'class' => 'form-control taxa-porcentagem validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_valor4') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_valor4', [
                    'label' => false,
                    'type'  => 'text',
                    'class' => 'form-control taxa-fixa validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_card_porcent4') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_card_porcent4', [
                    'label' => false,
                    'type'  => 'text',
                    'class' => 'form-control taxa-porcentagem validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('tax_antecipacao') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('tax_antecipacao', [
                    'label' => false,
                    'type'  => 'text',
                    'class' => 'form-control taxa-porcentagem validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


