<?php
$page_title = 'Painel Administrativo - Logfitness';
$cca = \Cake\ORM\TableRegistry::get('Cca');
?>
<!DOCTYPE html>
<html  lang="en">
<head>

    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $page_title ?>
    </title>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
   
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>assets/images/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>assets/images/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>assets/images/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>assets/images/icons/favicon-16x16.png">



    <!--<link href="<?/*= CSS_URL; */?>external/google-code-prettify/prettify.css" rel="stylesheet">-->
    <!--<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">-->

    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/iconic/iconic.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/elusive/elusive.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/meteocons/meteocons.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/spinnericon/spinnericon.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/icons/typicons/typicons.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets-minified/admin-all-demo.css">

    <!-- JS Core -->
    <?= $this->Html->script('jquery-2.2.3.min') ?>
    <script type="text/javascript" src="<?= $this->request->webroot; ?>assets-minified/js-core.js"></script>

    <script type="text/javascript">
        var Controller  = "<?= $ControllerName; ?>";
        var Action      = "<?= $ActionName; ?>";
        var WEBROOT_URL = "<?= WEBROOT_URL; ?>";
    </script>

    <?= $this->Html->css('validationEngine.jquery.custom') ?>
    <?php //= $this->Html->css('simple-line-icons/css/simple-line-icons') ?>
    <?= $this->Html->css('jquery.fancybox') ?>
    <?= $this->Html->css('admin') ?>

    <?= $this->Html->css('jquery.Jcrop.min') ?>
    <?= $this->Html->script('jquery.Jcrop.min') ?>

    <?= $this->Html->script('jquery.validationEngine-br') ?>
    <?= $this->Html->script('jquery.validationEngine.custom') ?>
    <?= $this->Html->script('jquery.fancybox') ?>
    <?= $this->Html->script('jquery.mask') ?>
    <?= $this->Html->css('Jcrop.min')?>
    <?= $this->Html->script('form') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-64522723-6"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-64522723-6');
</script>
<!-- End Google Analytics -->

</head>


<body>
<div id="sb-site">
    <div id="loadingg">
        <div class="spinnner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-blue-cacique font-inverse">
            <div id="mobile-navigation">
                <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                <a href="index.html" class="logo-content-small" title="Max Adm"></a>
            </div>
            <div id="header-logo" class="bg-white">
                <span class="helper"></span>
                <?= $this->Html->image('logo_master.png'); ?>
                <a id="close-sidebar" href="#" title="Esconder menu">
                    <i class="glyph-icon icon-angle-left"></i>
                </a>
            </div>
            <div id="header-nav-left">
                <div class="user-account-btn dropdown">
                    <a href="#" title="Minha Conta" class="user-profile clearfix" data-toggle="dropdown">
                        <?= $this->Html->image('users/'.$user_image, ['alt' => __('Profile image'), 'style' => 'max-width:28px; max-height:28px;']) ?>
                        <span style="width: auto;"><?= $user_name ?></span>
                        <i class="glyph-icon icon-angle-down"></i>
                    </a>
                    <div class="dropdown-menu float-left">
                        <div class="box-sm">
                            <div class="login-box clearfix">
                                <div class="user-img">
                                    <!--<a href="#" title="" class="change-img">Change photo</a>-->
                                    <?php //= $this->Html->image(WI_RESIZE.'128/128/users*'.$auth->user('image')) ?>
                                    <?= $this->Html->image('users/'.$user_image, ['alt' => __('Profile image'), 'style' => 'max-width:128px; max-height:128px;']) ?>
                                </div>
                                <div class="user-info">
                                    <span>
                                        <?= $user_name ?>
                                        <i><?= $group_name ?></i>
                                    </span>
                                    <!--
                                    <a href="#" title="Edit profile">Edit profile</a>
                                    <a href="#" title="View notifications">View notifications</a>
                                    -->
                                </div>
                            </div>
                            <div class="divider"></div>

                            <ul class="reset-ul mrg5B">
                                <li>
                                    <?php if($cca->cca('admin', 'Users', 'passwd', $group_id)){ ?>
                                        <?= $this->Html->link(
                                            __('Change Password').'<i class="glyph-icon float-right icon-linecons-key"></i>',
                                            ['controller' => 'Users', 'action' => 'passwd'],
                                            ['escape' => false]) ?>
                                    <?php } ?>
                                </li>
                            </ul>
                            <div class="pad5A button-pane button-pane-alt text-center">
                                <?php if($cca->cca('admin', 'Users', 'logout', $group_id)){ ?>
                                    <?= $this->Html->link('<i class="glyph-icon icon-power-off"></i>&nbsp;&nbsp;Sair do Sistema',
                                        ['controller' => 'Users', 'action' => 'logout'],
                                        ['class' => 'btn display-block font-normal btn-danger', 'escape' => false]) ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="header-nav-right">

                <a href="#" class="hdr-btn" id="fullscreen-btn" title="Fullscreen">
                    <i class="glyph-icon icon-arrows-alt"></i>
                </a>
            </div>

        </div>


        <!-- MENU PRINCIPAL -->
        <div id="page-sidebar" class="bg-white color-orange-cacique">
            <div class="scroll-sidebar">

                <ul id="sidebar-menu">
                    <!-- INICIO -->
                    <li class="header"><span>Admin</span></li>
                    <li>
                        <?php if($cca->cca('admin', 'Pages', 'index', $group_id)){ ?>
                            <?=$this->Html->link('<i class="glyph-icon icon-home"></i><span>'.__('Dashboard').'</span>',
                                ['controller' => 'Pages','action' => 'index', 'prefix' => 'admin'],
                                ['title' => 'Admin Dashboard', 'escape' => false]);?>
                        <?php } ?>
                    </li>
                    <li class="divider"></li>

                    <!-- CONFIGURAÇÃO -->
                    <li class="header"><span><?=__('Configuração');?></span></li>

                    <!-- GERAL -->
                    <?php if($group_id != 6) { ?>
                    <?php if($group_id != 7) { ?>
                       <li>
                        <a href="#" title="<?=__('Geral');?>">
                            <i class="glyph-icon icon-th-large"></i>
                            <span><?=__('Geral');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <?php if($cca->cca('admin', 'Banners', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Banners'),
                                            ['controller' => 'Banners', 'action' => 'index', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if($cca->cca('admin', 'Categorias', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Categorias'),
                                            ['controller' => 'Categorias', 'action' => 'index', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if($cca->cca('admin', 'Marcas', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Marcas'),
                                            ['controller' => 'Marcas', 'action' => 'index', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if($cca->cca('admin', 'Objetivos', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Objetivos'),
                                            ['controller' => 'Objetivos', 'action' => 'index', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if($cca->cca('admin', 'Configurations', 'parcelamento', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Parcelamento'),
                                            ['controller' => 'Configurations', 'action' => 'parcelamento', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if($cca->cca('admin', 'Subcategorias', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Subcategorias'),
                                            ['controller' => 'Subcategorias', 'action' => 'index', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if($cca->cca('admin', 'Substancias', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Substâncias'),
                                            ['controller' => 'Substancias', 'action' => 'index', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div><!-- .sidebar-submenu -->
                    </li>

                    <!-- <li>
                        <a href="" title="<?=__('Lojas') ?>">
                            <div class="glyph-icon icon-home"></div>
                            <span><?=__('Minhas Lojas');?></span>
                        </a>
                         <div class="sidebar-submenu">
                            <ul>
                                <?php if($cca->cca('admin', 'Lojas', 'add', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Listar lojas'),
                                        ['controller' => 'Lojas', 'action' => 'index', 'prefix' => 'admin']);?>     
                                    </li>
                                <?php } if($cca->cca('admin', 'MinhasLojas', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Adicionar Lojas'),
                                        ['controller' => 'Lojas', 'action' => 'add', 'prefix' => 'admin']);?>    
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </li> -->

                    <!-- CUPOMDESCONTO -->
                    <li>
                        <a href="#" title="<?=__('Cupons');?>">
                            <div class="glyph-icon icon-ticket"></div>
                            <span><?=__('Cupons');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <?php if($cca->cca('admin', 'CupomDesconto', 'add', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Novo Cupom'),
                                        ['controller' => 'CupomDesconto', 'action' => 'add', 'prefix' => 'admin']);?>     
                                    </li>
                                <?php } if($cca->cca('admin', 'CupomDesconto', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Listar Cupons'),
                                        ['controller' => 'CupomDesconto', 'action' => 'index', 'prefix' => 'admin']);?>    
                                    </li>
                                <?php } ?>
                            </ul>
                        </div><!-- .sidebar-submenu -->
                    </li>


                    <!-- PRODUTOS -->
                    <?php if($cca->cca('admin', 'Produtos', 'gerenciador', $group_id)){ ?>
                        <li>
                            <?= $this->Html->link('<i class="glyph-icon icon-tags"></i>  Produtos',
                            ['controller' => 'Produtos', 'action' => 'gerenciador', 'prefix' => 'admin'], ['escape' => false]);?>
                        </li>
                    <?php } ?>
                    <?php } ?>
                    <!-- CLIENTES -->
                    <?php if($cca->cca('admin', 'Clientes', 'index', $group_id)){ ?>
                        <li>
                            <?=$this->Html->link('<i class="glyph-icon icon-group"></i> Clientes',
                                ['controller' => 'Clientes', 'action' => 'index', 'prefix' => 'admin'], ['escape' => false]);?>
                        </li>
                    <?php } ?>
                    <!-- PEDIDOS -->
                    <?php if($cca->cca('admin', 'Pedidos', 'index', $group_id)){ ?>
                        <li>
                            <?=$this->Html->link('<i class="glyph-icon icon-book"></i>  Pedidos',
                                ['controller' => 'Pedidos', 'action' => 'index', 'prefix' => 'admin'],['escape' => false]);?>
                        </li>
                    <?php } ?>
                    <!-- ACADEMIAS -->
                    <li>
                        <a href="#" title="<?=__('Perfis');?>">
                            <div class="glyph-icon icon-building"></div>
                            <span><?=__('Perfis');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <?php if($cca->cca('admin', 'Academias', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Todos'),
                                            ['controller' => 'Academias', 'action' => 'index', 'todas', 'prefix' => 'admin']);?>
                                    </li>

                                <?php } if($cca->cca('admin', 'AcademiaComissoes', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Comissões'),
                                            ['controller' => 'AcademiaComissoes', 'action' => 'index', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if($cca->cca('admin', 'Professores', 'listar_professores', $group_id)){ ?>
                                    <!-- <li>
                                        <?=$this->Html->link(__('Professores'),
                                            ['controller' => 'Professores', 'action' => 'listar_professores', 'prefix' => 'admin']);?>
                                    </li> -->
                                <?php } ?>
                            </ul>
                        </div>
                    </li>
                    <?php if($group_id != 7) { ?>
                    <!-- PROFESSORES -->
                    <!-- <li>
                        <a href="#" title="<?=__('Professores');?>">
                            <div class="glyph-icon icon-user"></div>
                            <span><?=__('Professores');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <?php if($cca->cca('admin', 'Professores', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Todos'),
                                            ['controller' => 'Professores', 'action' => 'index', 'todos', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if($cca->cca('admin', 'Professores', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Ativos'),
                                            ['controller' => 'Professores', 'action' => 'index/ativos', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if($cca->cca('admin', 'Professores', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Novos / Inativos'),
                                            ['controller' => 'Professores', 'action' => 'index/inativos', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if($cca->cca('admin', 'ProfessorComissoes', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Comissões'),
                                            ['controller' => 'ProfessorComissoes', 'action' => 'index', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </li> -->

                    <!-- SYSTEM -->
                    <li>
                        <a href="#" title="<?=__('System');?>">
                            <i class="glyph-icon icon-cogs"></i>
                            <span><?=__('System');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <?php if($cca->cca('admin', 'Configurations', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Configurations'),
                                            ['controller' => 'Configurations', 'action' => 'index', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if($cca->cca('admin', 'Groups', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Groups'),
                                            ['controller' => 'Groups', 'action' => 'index', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if($cca->cca('admin', 'Cca', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Permissions'),
                                            ['controller' => 'Cca', 'action' => 'index', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if($cca->cca('admin', 'Users', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Users'),
                                            ['controller' => 'Users', 'action' => 'index', 'prefix' => 'admin']);?>
                                    </li>

                                <?php } ?>
                            </ul>
                        </div><!-- .sidebar-submenu -->
                    </li>
                    <?php } ?>
                    <?php } ?>
                    <?php if($group_id != 6 && $group_id != 7) { ?>
                    <!-- FRANQUEADOS -->

                    <!-- <li>
                        <a href="#" title="<?=__('Representantes');?>">
                            <div class="glyph-icon icon-clipboard"></div>
                            <span><?=__('Representantes');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <?php if($cca->cca('admin', 'Franqueados', 'index', $group_id)){ ?>
                                    <li>
                                        <?=$this->Html->link(__('Lista de Representantes'),
                                            ['controller' => 'Franqueados', 'action' => 'index', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if ($cca->cca('admin', 'Franqueados', 'comissoes', $group_id)) { ?>
                                    <li>
                                        <?=$this->Html->link(__('Comissões'),
                                            ['controller' => 'Franqueados', 'action' => 'comissoes', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if ($cca->cca('admin', 'Franqueados', 'comissoes_academias', $group_id)) { ?>
                                    <li>
                                        <?=$this->Html->link(__('Comissões x Academias'),
                                            ['controller' => 'Franqueados', 'action' => 'comissoes_academias', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </li> --><!-- .sidebar-submenu -->
                    
                    <?php } ?>
                    <?php if($group_id == 7) { ?>
                    <li>
                        <a href="#" title="<?=__('Financeiro');?>">
                            <i class="glyph-icon icon-money"></i>
                            <span><?=__('Financeiro');?></span>
                        </a>
                        <div class="sidebar-submenu">
                            <ul>
                                <?php if ($cca->cca('admin', 'Franqueados', 'comissoes_franqueado', $group_id)) { ?>
                                    <li>
                                        <?=$this->Html->link(__('Comissões'),
                                            ['controller' => 'Franqueados', 'action' => 'comissoes_franqueado', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } if ($cca->cca('admin', 'Franqueados', 'comissoes_academias_franqueado', $group_id)) { ?>
                                    <li>
                                        <?=$this->Html->link(__('Comissões x Academias'),
                                            ['controller' => 'Franqueados', 'action' => 'comissoes_academias_franqueado', 'prefix' => 'admin']);?>
                                    </li>
                                <?php } ?>
                                
                            </ul>
                        </div>
                    </li>
                    <?php } ?>
                </ul><!-- #sidebar-menu -->

            </div>
        </div>
        <div id="page-content-wrapper">
            <div id="page-content"  class="admin-content">


                <!-- jQueryUI Spinner -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/spinner/spinner.js"></script>
                <script type="text/javascript">
                    /* jQuery UI Spinner */

                    $(function() { "use strict";
                        $(".spinner-input").spinner();
                    });
                </script>

                <!-- jQueryUI Autocomplete -->

                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/autocomplete/autocomplete.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/autocomplete/menu.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/autocomplete/autocomplete-demo.js"></script>

                <!-- Touchspin -->

                <!--<link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>assets/widgets/touchspin/touchspin.css">-->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/touchspin/touchspin.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/touchspin/touchspin-demo.js"></script>

                <!-- Input switch -->

                <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/input-switch/inputswitch.css">-->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/input-switch/inputswitch.js"></script>
                <script type="text/javascript">
                    /* Input switch */

                    $(function() { "use strict";
                        $('.input-switch').bootstrapSwitch();
                    });
                </script>

                <!-- Textarea -->

                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/textarea/textarea.js"></script>
                <script type="text/javascript">
                    /* Textarea autoresize */

                    $(function() { "use strict";
                        $('.textarea-autosize').autosize();
                    });
                </script>

                <!-- Multi select -->

                <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/multi-select/multiselect.css">-->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/multi-select/multiselect.js"></script>
                <script type="text/javascript">
                    /* Multiselect inputs */

                    $(function() { "use strict";
                        $(".multi-select").multiSelect();
                        $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
                    });
                </script>

                <!-- Uniform -->

                <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/uniform/uniform.css">-->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/uniform/uniform.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/uniform/uniform-demo.js"></script>

                <!-- Chosen -->
                <!--<link rel="stylesheet" type="text/css" href="../../assets/widgets/chosen/chosen.css">-->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/chosen/chosen.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/chosen/chosen-demo.js"></script>

                <!-- Nice scroll -->
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/nicescroll/nicescroll.js"></script>
                <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/nicescroll/nicescroll-demo.js"></script>

                <!-- include summernote css/js-->
                <link href="<?= JS_URL; ?>summernote/dist/summernote.css" rel="stylesheet" property="">
                <script src="<?= JS_URL; ?>summernote/dist/summernote.js"></script>
                <script src="<?= JS_URL; ?>summernote/lang/summernote-pt-BR.js"></script>

                <?php if(!empty(trim(__(str_replace(array('Admin/','Admin\\'), '', $this->fetch('title')))))){?>
                    <div id="page-title">
                        <h2><?= __(str_replace(array('Admin/','Admin\\'), '', $this->fetch('title'))) ?></h2>
                        <p></p>
                    </div>
                <?php }?>

                <div class="row">
                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content') ?>
                </div>
            </div>
        </div>
    </div>

    <!-- JS Demo -->
    <script type="text/javascript" src="<?= $this->request->webroot; ?>assets-minified/admin-all-demo.js"></script>

    <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/modal/modal.js"></script>

    <!-- Colorpicker -->
    <link rel="stylesheet" href="<?= $this->request->webroot; ?>assets/widgets/jquery-minicolors-master/jquery.minicolors.css">
    <script type="text/javascript" src="<?= $this->request->webroot; ?>assets/widgets/jquery-minicolors-master/jquery.minicolors.min.js"></script>
    <script type="text/javascript" src="<?= $this->request->webroot; ?>js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>

    <!-- DateTime Picker -->
    <link rel="stylesheet" type="text/css" href="<?= $this->request->webroot; ?>css/bootstrap-datetimepicker.min.css">
    <script type="text/javascript" src="<?= $this->request->webroot; ?>js/moment-with-locales.js"></script>
    <script type="text/javascript" src="<?= $this->request->webroot; ?>js/bootstrap-datetimepicker.min.js"></script>
    
    <?= $this->Html->script('Jcrop.min') ?>

</div>
</body>
</html>
