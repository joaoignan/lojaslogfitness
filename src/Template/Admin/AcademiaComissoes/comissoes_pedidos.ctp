<?php
$this->assign('title', 'Comissões x Pedidos de Academias');

$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);

$anos = [];
for($i = 2016; date('Y') >= $i; $i++){
    $anos[$i] = $i;
}

$meses = [];
for($i = 1; $i <= 12; $i++){
    $meses[str_pad($i, 2, '0', STR_PAD_LEFT)] = str_pad($i, 2, '0', STR_PAD_LEFT);
}
?>
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
                <?= link_button_listAcademias(__('Listar Academias'), ['controller' => 'Academias', 'action' => 'index']); ?>


        <div class="col-md-6 float-right">
            <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row']); ?>

            <div class="col-sm-1 control-label">
                <?= $this->Form->label('ano') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('ano', [
                    'options' => $anos,
                    'value' => $ano > 0 ? $ano : '',
                    'empty' => 'Ano',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>

            <div class="col-sm-1 control-label">
                <?= $this->Form->label('mes', 'Mês') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('mes', [
                    'options' => $meses,
                    'value' => $mes > 0 ? $mes : '',
                    'empty' => 'Mês',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>

            <div class="col-sm-1 text-center">
                <?= $this->Form->button('<i class="glyph-icon icon-search"></i> <span>Filtrar</span>',
                    ['class' => 'btn btn-alt btn-default',
                        'escape' => false
                    ]); ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>

<div class="academiaComissoes index">

    <?php if(isset($select_date)){ ?>
        <h4>
            Selecione acima um período para consulta...
        </h4>
    <?php }else{ ?>

        <table class="table table-hover">
            <thead>
            <tr>
                <th>Academia</th>
                <th class="text-left">Contato</th>
                <th class="text-right">Comissão</th>
                <th class="text-right">Pagamento</th>
                <th class="text-center">Aceite</th>
                <th class="text-right">Venda</th>
                <th class="text-right"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            //debug($pedidos_comissoes);
            foreach ($pedidos_comissoes as $key => $pedidos_comissao):
                ?>
                <tr>
                    <td class="text-left">
                        <?= $this->Html->link(ucwords(strtolower($pedidos_comissao->academia->name)),
                            WEBROOT_URL.'/admin/academias/view/'.$pedidos_comissao->academia->id, ['escape' => false, 'target' => '_blank']
                        ); ?>
                        <br>
                        <small class="font-italic"><?= ucwords(strtolower($pedidos_comissao->academia->contact)) ?></small>
                    </td>
                    <td class="text-left">
                        <?= $pedidos_comissao->academia->phone ?>
                        <br>
                        <small><?= $pedidos_comissao->academia->email ?></small>
                    </td>
                    <td class="text-right">
                        R$ <?= number_format($comissoes['comissao'][$pedidos_comissao->academia_id], 2, ',','.') ?>
                    </td>
                    <td class="text-right">
                        <?= $pedidos_comissao['AcademiaComissoes']['comissao'] > 0
                            ? 'R$ '.number_format($pedidos_comissao['AcademiaComissoes']['comissao'], 2, ',', '.')
                            : '-' ?>
                        <br>
                        <?php /*if($pedidos_comissao['AcademiaComissoes']['paga'] == 1){
                            echo '<small class="color-blue">Pago</small>';
                        }elseif($pedidos_comissao['AcademiaComissoes']['paga'] == null){
                            echo '';
                        }else{
                            echo '<small class="color-red">Não Pago</small>';
                        } */?>

                        <?php if($comissoes['paga'][$pedidos_comissao->academia_id] == 1){
                            echo '<small class="color-blue">Pago</small>';
                        }elseif($comissoes['paga'][$pedidos_comissao->academia_id] == null){
                            echo '';
                        }else{
                            echo '<small class="color-red">Não Pago</small>';
                        } ?>
                    </td>
                    <td class="text-center">
                        <?php /*if($pedidos_comissao['AcademiaComissoes']['aceita'] == 1){
                            echo 'Sim';
                        }elseif($pedidos_comissao['AcademiaComissoes']['aceita'] == null){
                            echo '-';
                        }else{
                            echo 'Não';
                        } */?>

                        <?php if($comissoes['aceita'][$pedidos_comissao->academia_id] == 1){
                            echo 'Sim';
                        }elseif($comissoes['aceita'][$pedidos_comissao->academia_id] == null){
                            echo '-';
                        }else{
                            echo 'Não';
                        } ?>
                    </td>
                    <td class="text-right">
                        R$ <?= number_format($pedidos_comissao->total, 2, ',', '.') ?>
                        <br>
                        <small class="font-italic">R$ <?= number_format($pedidos_comissao['AcademiaComissoes']['meta'], 2, ',', '.') ?></small>
                    </td>
                    <td>
                        <div class="col-sm-1 text-center">
                            <?= $this->Form->button('<i class="glyph-icon icon-usd"></i> <span></span>',
                                [
                                    'data-target'=> '#dados-'.$pedidos_comissao->academia->id,
                                    'data-toggle'=> 'modal',
                                    'title'     => 'Visualizar dados bancários',
                                    'class'     => 'btn btn-alt btn-default',
                                    'escape'    => false
                                ]);
                            ?>
                        </div>

                        <div class="modal fade"
                             id="dados-<?= $pedidos_comissao->academia->id ?>"
                             tabindex="-1" role="dialog"
                             aria-labelledby="dados-<?= $pedidos_comissao->academia->id ?>Label"
                             aria-hidden="true">

                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Dados Bancários :: <?= $pedidos_comissao->academia->shortname ?></h4>
                                    </div>
                                    <div class="modal-body">
                                        <h4><?= $pedidos_comissao->academia->name ?></h4>
                                        <br>
                                        <?php if($pedidos_comissao->academia->card_status == 0) { ?>
                                            <p>
                                            <strong>Tipo de conta: </strong><?= $pedidos_comissao->academia->tipo_conta ?>
                                            <br>

                                            <strong>Banco: </strong><?= $pedidos_comissao->academia->banco ?>
                                            <br>

                                            <strong>Agência: </strong>
                                            <?= $pedidos_comissao->academia->agencia ?><?= $pedidos_comissao->academia->agencia_dv != null ? ' - '.$pedidos_comissao->academia->agencia_dv : '' ?>
                                            <br>

                                            <strong>Conta: </strong>
                                            <?= $pedidos_comissao->academia->conta ?><?= $pedidos_comissao->academia->conta_dv != null ? ' - '.$pedidos_comissao->academia->conta_dv : '' ?>
                                            <br>

                                            <?php if($pedidos_comissao->academia->is_cpf == 0) { ?>
                                                <strong>CNPJ: </strong>
                                                <?= $pedidos_comissao->academia->cnpj ?>
                                                <br>

                                                <strong>Razão Social: </strong>
                                                <?= $pedidos_comissao->academia->name ?>
                                                <br>
                                            <?php } else { ?>
                                                <strong>Nome Titular Conta: </strong>
                                                <?= $pedidos_comissao->academia->favorecido ?>
                                                <br>

                                                <strong>CPF Titular Conta: </strong>
                                                <?= $pedidos_comissao->academia->cpf_favorecido ?>
                                                <br>
                                            <?php } ?>

                                            <strong>Observações: </strong><?= $pedidos_comissao->academia->banco_obs ?>
                                            <br>
                                        </p>
                                        <?php } else { ?>
                                            <p>
                                                <strong>Nome: </strong>
                                                <?= $pedidos_comissao->academia->card_name ?>
                                                <br>

                                                <strong>CPF: </strong>
                                                <?= $pedidos_comissao->academia->card_cpf ?>
                                                <br>

                                                <strong>Data de nascimento: </strong>
                                                <?= $pedidos_comissao->academia->card_birth ?>
                                                <br>

                                                <strong>Nome da mãe: </strong>
                                                <?= $pedidos_comissao->academia->card_mother ?>
                                                <br>

                                                <strong>Gênero: </strong>
                                                <?= $pedidos_comissao->academia->card_gender ?>
                                                <br>

                                                <strong>ID do cartão: </strong>
                                                <?= $pedidos_comissao->academia->card_id ?>
                                                <br>

                                                <strong>Número do cartão: </strong>
                                                <?= $pedidos_comissao->academia->card_number ?>
                                                <br>
                                            </p>
                                        <?php } ?>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>

            <?php endforeach; ?>
            </tbody>
        </table>
    <?php } ?>
</div>

