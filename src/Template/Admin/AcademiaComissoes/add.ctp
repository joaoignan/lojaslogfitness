<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
                <?= $this->Html->link(__('List Academia Comissoes'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
                    <?= $this->Html->link(__('List Academias'),
        ['controller' => 'Academias', 'action' => 'index'],
        ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Academia'),
        ['controller' => 'Academias', 'action' => 'add'],
        ['class' => 'btn btn-default']) ?>
                </div>
        </div>

        <div class="academiaComissoes form">
        <?= $this->Form->create($academiaComisso, ['class' => 'form-horizontal bordered-row']); ?>
        <fieldset>
            <legend><?= __('Add Academia Comisso') ?></legend>
                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('academia_id') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('academia_id', [
                    'options' => $academias,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('meta') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('meta', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('vendas') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('vendas', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('comissao') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('comissao', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('aceita') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('aceita', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('paga') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('paga', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group text-center">
                <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                'escape' => false]); ?>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>


