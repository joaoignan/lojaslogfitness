<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_listComissoes(__('Listar Comissoões'), ['action' => 'index']); ?>
        <?= link_button_listAcademias(__('Listar Academias'), ['controller' => 'Academias', 'action' => 'index']); ?>   
     </div>
</div>

<div class="academiaComissoes view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($academiaComisso->id) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Academia') ?></td>
                    <td><?= $academiaComisso->has('academia') ? $this->Html->link($academiaComisso->academia->name, ['controller' => 'Academias', 'action' => 'view', $academiaComisso->academia->id]) : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Nome Titular/Razão Social') ?></td>
                    <td><?= $academiaComisso->nome_titular ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('CPF/CNPJ') ?></td>
                    <td><?= $academiaComisso->cpf_titular ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Tipo de Conta') ?></td>
                    <td><?= $academiaComisso->tipo_conta ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Banco') ?></td>
                    <td><?= $academiaComisso->banco ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Agência') ?></td>
                    <td><?= $academiaComisso->agencia ?><?= $academiaComisso->agencia_dv != null ? '-'.$academiaComisso->agencia_dv : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Conta') ?></td>
                    <td><?= $academiaComisso->conta ?><?= $academiaComisso->conta_dv != null ? '-'.$academiaComisso->conta_dv : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Data de aceite') ?></td>
                    <td><?= $academiaComisso->data_aceite ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Observação da Academia') ?></td>
                    <td><?= $academiaComisso->obs ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($academiaComisso->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Meta') ?></td>
                    <td>R$ <?= $this->Number->format($academiaComisso->meta) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Vendas') ?></td>
                    <td>R$ <?= $this->Number->format($academiaComisso->vendas) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Comissao') ?></td>
                    <td>R$ <?= $this->Number->format($academiaComisso->comissao) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Aceita') ?></td>
                    <td>
                        <?php if($academiaComisso->aceita == 0) { ?>
                            Contestada
                        <?php } else if($academiaComisso->aceita == 1) { ?>
                            Aceita
                        <?php } else { ?>
                            Aguardando aceite
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Paga') ?></td>
                    <td><?= $academiaComisso->paga == 1 ? 'Sim' : 'Não' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($academiaComisso->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($academiaComisso->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>