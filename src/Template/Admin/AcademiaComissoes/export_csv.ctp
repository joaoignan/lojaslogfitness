<?php
/* Tabela Academia */
$cells_acad[] = '';
$cells_acad[] = 'ID';
$cells_acad[] = 'Numero Cartao';
$cells_acad[] = 'CPF';
$cells_acad[] = 'Nome';
$cells_acad[] = 'Nome Meio';
$cells_acad[] = 'Ultimo Nome';
$cells_acad[] = 'Genero';
$cells_acad[] = 'Nascimento';
$cells_acad[] = 'Mae';
$cells_acad[] = 'Email';
$cells_acad[] = 'Fone';
$cells_acad[] = 'Endereço';
$cells_acad[] = 'Numero';
$cells_acad[] = 'Complemento';
$cells_acad[] = 'CEP';
$cells_acad[] = 'Cidade';
$cells_acad[] = 'UF';
$cells_acad[] = 'Pais';
$cells_acad[] = 'SMS';
$cells_acad[] = 'Email';
$cells_acad[] = 'Carga Inicial';
$cells_acad[] = '';

foreach ($academiaComissoes as $academiaComissao) {
	$card_name = explode(' ', $academiaComissao['academia']['card_name']);

	$middle_name = '';
	$first_name = array_shift($card_name);
	$last_name = array_pop($card_name);

	if(!empty($card_name)) {
		for($var = 0; $var < count($card_name); $var++) {
			$middle_name = $middle_name.$card_name[$var].' ';
		}
	}

	$cells_acad[] = $academiaComissao['academia_id'];
	$cells_acad[] = $academiaComissao['academia']['card_id'];
	$cells_acad[] = $academiaComissao['academia']['card_number'];
	$cells_acad[] = $academiaComissao['academia']['card_cpf'];
	$cells_acad[] = $first_name;
	$cells_acad[] = $middle_name;
	$cells_acad[] = $last_name;
	$cells_acad[] = $academiaComissao['academia']['card_gender'];
	$cells_acad[] = date_format($academiaComissao['academia']['card_birth'], 'd/m/Y');
	$cells_acad[] = $academiaComissao['academia']['card_mother'];
	$cells_acad[] = $academiaComissao['academia']['email'];
	$cells_acad[] = $academiaComissao['academia']['phone'];
	$cells_acad[] = $academiaComissao['academia']['address'];
	$cells_acad[] = $academiaComissao['academia']['number'];
	$cells_acad[] = $academiaComissao['academia']['complement'];
	$cells_acad[] = $academiaComissao['academia']['cep'];
	$cells_acad[] = $academiaComissao['academia']['city']['name'];
	$cells_acad[] = $academiaComissao['academia']['city']['uf'];
	$cells_acad[] = 'brasil';
	$cells_acad[] = $academiaComissao['academia']['phone'];
	$cells_acad[] = '';
	$cells_acad[] = 'R$ '.$academiaComissao['comissao'];
	$cells_acad[] = $academiaComissao['id'];
}

$var = 0;
foreach ($cells_acad as $cell_acad) {
	if($var != 22) {
		echo $cell_acad = '"'.$cell_acad.'",';
	} else {
		echo '"'.$cell_acad.'"'."\r\n";
		$var = -1;
	}
	$var++;
}

echo "\r\n";
echo "\r\n";
echo "\r\n";
echo "\r\n";
echo "\r\n";

/* Tabela Professor */
$cells_prof[] = '';
$cells_prof[] = 'ID';
$cells_prof[] = 'Numero Cartao';
$cells_prof[] = 'CPF';
$cells_prof[] = 'Nome';
$cells_prof[] = 'Nome Meio';
$cells_prof[] = 'Ultimo Nome';
$cells_prof[] = 'Genero';
$cells_prof[] = 'Nascimento';
$cells_prof[] = 'Mae';
$cells_prof[] = 'Email';
$cells_prof[] = 'Fone';
$cells_prof[] = 'Endereço';
$cells_prof[] = 'Numero';
$cells_prof[] = 'Complemento';
$cells_prof[] = 'CEP';
$cells_prof[] = 'Cidade';
$cells_prof[] = 'UF';
$cells_prof[] = 'Pais';
$cells_prof[] = 'SMS';
$cells_prof[] = 'Email';
$cells_prof[] = 'Carga Inicial';
$cells_prof[] = '';

foreach ($professorComissoes as $professorComissao) {
	$name = explode(' ', $professorComissao['professore']['name']);

	$middle_name = '';
	$first_name = array_shift($name);
	$last_name = array_pop($name);

	if(!empty($name)) {
		for($var = 0; $var < count($name); $var++) {
			$middle_name = $middle_name.$name[$var].' ';
		}
	}

	$cells_prof[] = $professorComissao['professor_id'];
	$cells_prof[] = $professorComissao['professore']['card_id'];
	$cells_prof[] = $professorComissao['professore']['card_number'];
	$cells_prof[] = $professorComissao['professore']['cpf'];
	$cells_prof[] = $first_name;
	$cells_prof[] = $middle_name;
	$cells_prof[] = $last_name;
	$cells_prof[] = $professorComissao['professore']['card_gender'];
	$cells_prof[] = date_format($professorComissao['professore']['card_birth'], 'd/m/Y');
	$cells_prof[] = $professorComissao['professore']['card_mother'];
	$cells_prof[] = $professorComissao['professore']['email'];
	$cells_prof[] = $professorComissao['professore']['phone'];
	$cells_prof[] = $professorComissao['professore']['address'];
	$cells_prof[] = $professorComissao['professore']['number'];
	$cells_prof[] = $professorComissao['professore']['complement'];
	$cells_prof[] = $professorComissao['professore']['cep'];
	$cells_prof[] = $professorComissao['professore']['city']['name'];
	$cells_prof[] = $professorComissao['professore']['city']['uf'];
	$cells_prof[] = 'brasil';
	$cells_prof[] = $professorComissao['professore']['phone'];
	$cells_prof[] = '';
	$cells_prof[] = 'R$ '.$professorComissao['comissao'];
	$cells_prof[] = $professorComissao['id'];
}

$var = 0;
foreach ($cells_prof as $cell_prof) {
	if($var != 22) {
		echo $cell_prof = '"'.$cell_prof.'",';
	} else {
		echo '"'.$cell_prof.'"'."\r\n";
		$var = -1;
	}
	$var++;
}
?>