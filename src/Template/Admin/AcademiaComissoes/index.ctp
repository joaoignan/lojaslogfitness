<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);

$anos = [];
for($i = 2017; date('Y') >= $i; $i++){
    $anos[$i] = $i;
}

$meses = [];
for($i = 1; $i <= 12; $i++){
    $meses[str_pad($i, 2, '0', STR_PAD_LEFT)] = str_pad($i, 2, '0', STR_PAD_LEFT);
}
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_listAcademias(__('Listar Academias'), ['controller' => 'Academias', 'action' => 'index']); ?>

        <div class="col-md-10 float-right">
            <?= $this->Form->create(null, ['url' => ['controller' => 'academiaComissoes', 'action' => 'filtrar_index'], 'class' => 'form-horizontal bordered-row']); ?>

            <div class="col-sm-1 control-label">
                <?= $this->Form->label('ano') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('ano', [
                    'options' => $anos,
                    'value' => $ano,
                    'empty' => 'Ano',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>

            <div class="col-sm-1 control-label">
                <?= $this->Form->label('mes', 'Mês') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('mes', [
                    'options' => $meses,
                    'value' => $mes,
                    'empty' => 'Mês',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>

            <div class="col-sm-1 control-label">
                <?= $this->Form->label('filter', 'Filtro') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('filter', [
                    'options' => [
                        0 => 'Todas',
                        1 => 'A Pagar',
                        2 => 'Pagas'
                    ],
                    'value' => $filter,
                    'label' => false,
                    'class' => 'form-control chosen-select filter_comissoes'
                ]) ?>
            </div>

            <div class="col-sm-1 text-center">
                <?= $this->Form->button('<i class="glyph-icon icon-search"></i> <span>Filtrar</span>',
                    ['class' => 'btn btn-alt btn-default',
                        'escape' => false
                    ]); ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.filter_comissoes').change(function() {
            var value = $(this).val();

            location.href = WEBROOT_URL + 'admin/academia_comissoes/index/' + value;
        });
    });
</script>

<?php if($mes == $mes_atual && $ano == $ano_atual) { ?>
    <div class="academiaComissoes index">
        <table class="table table-hover">
            <thead>
            <tr>
                <th class="text-center"><?= $this->Paginator->sort('Academia') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('Vendas mês anterior') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('Vendas desse mês') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('Comissões desse mês') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('Dados Bancários') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($informacoes as $informacao): ?>
                <tr>
                    <td class="text-center">
                        <?= $this->Html->link('#'.$informacao['academia']['id'].' - '.ucwords(strtolower($informacao['academia']['shortname'])),
                            WEBROOT_URL.'/admin/academias/view/'.$informacao['academia']['id'], ['escape' => false, 'target' => '_blank']
                        ); ?>
                    <td class="text-center">R$ <?= number_format($informacao['vendas_mes_anterior'], 2, ',', '.') ?></td>
                    <td class="text-center">R$ <?= number_format($informacao['vendas_mes_atual'], 2, ',', '.') ?></td>
                    <td class="text-center">R$ <?= number_format($informacao['comissoes_mes_atual'], 2, ',', '.') ?></td>
                    <td class="actions text-center">
                        <?= $this->Form->button('<i class="glyph-icon icon-usd"></i> <span>Visualizar dados</span>',
                            [
                                'data-target'=> '#dados-'.$informacao['academia']['id'],
                                'data-toggle'=> 'modal',
                                'title'     => 'Visualizar dados bancários',
                                'escape'    => false,
                                'class'     => 'btn btn-alt btn-hoverrr btn-default',
                                'type'      => 'button'
                            ]);
                        ?>
                        <div class="modal fade"
                             id="dados-<?= $informacao['academia']['id'] ?>"
                             tabindex="-1" role="dialog"
                             aria-labelledby="dados-<?= $informacao['academia']['id'] ?>Label"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Dados Bancários :: <?= $informacao['academia']['shortname'] ?></h4>
                                    </div>
                                    <div class="modal-body">
                                        <h4><?= $informacao['academia']['name'] ?></h4>
                                        <br>
                                        <p>
                                            <strong>Tipo de conta: </strong><?= $informacao['academia']['tipo_conta'] ?>
                                            <br>

                                            <strong>Banco: </strong><?= $informacao['academia']['banco'] ?>
                                            <br>

                                            <strong>Agência: </strong>
                                            <?= $informacao['academia']['agencia'] ?><?= $informacao['academia']['agencia_dv'] != null ? ' - '.$informacao['academia']['agencia_dv'] : '' ?>
                                            <br>

                                            <strong>Conta: </strong>
                                            <?= $informacao['academia']['conta'] ?><?= $informacao['academia']['conta_dv'] != null ? ' - '.$informacao['academia']['conta_dv'] : '' ?>
                                            <br>

                                            <strong>Nome Titular Conta: </strong>
                                            <?= $informacao['academia']['favorecido'] ?>
                                            <br>

                                            <strong>CPF Titular Conta: </strong>
                                            <?= $informacao['academia']['cpf_favorecido'] ?>
                                            <br>

                                            <strong>Observações: </strong><?= $informacao['academia']['banco_obs'] ?>
                                            <br>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>

            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    </div>
<?php } else { ?>
    <div class="academiaComissoes index">
        <?= $this->Form->create(null)?>
        <table class="table table-hover">
            <thead>
            <tr>
                <th class="text-center"><?= $this->Paginator->sort('Cod') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('Academia') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('Vendas mês anterior') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('Vendas desse mês') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('Comissões desse mês') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('Dados Bancários') ?></th>
                <th class="text-center"><?= $this->Paginator->sort('Pago/Não Pago') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($academiaComissoes as $academiaComisso): ?>
                <tr>
                    <td class="text-center"><?= $this->Number->format($academiaComisso->id) ?></td>
                    <td class="text-center">
                        <?= $this->Html->link('#'.$academiaComisso->academia_id.' - '.ucwords(strtolower($academiaComisso->academia->shortname)),
                            WEBROOT_URL.'/admin/academias/view/'.$academiaComisso->academia_id, ['escape' => false, 'target' => '_blank']
                        ); ?>
                    <td class="text-center">R$ <?= number_format($vendas_mes_anterior[$academiaComisso->academia_id], 2, ',', '.') ?></td>
                    <td class="text-center">R$ <?= number_format($academiaComisso->vendas, 2, ',', '.') ?></td>
                    <td class="text-center">R$ <?= number_format($academiaComisso->comissao, 2, ',', '.') ?></td>
                    <td class="actions text-center">
                        <?= $this->Form->button('<i class="glyph-icon icon-usd"></i> <span>Visualizar dados</span>',
                            [
                                'data-target'=> '#dados-'.$academiaComisso->id,
                                'data-toggle'=> 'modal',
                                'title'     => 'Visualizar dados bancários',
                                'escape'    => false,
                                'class'     => 'btn btn-alt btn-hoverrr btn-default',
                                'type'      => 'button'
                            ]);
                        ?>
                        <div class="modal fade"
                             id="dados-<?= $academiaComisso->id ?>"
                             tabindex="-1" role="dialog"
                             aria-labelledby="dados-<?= $academiaComisso->id ?>Label"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Dados Bancários :: <?= $academiaComisso->academia->shortname ?></h4>
                                    </div>
                                    <div class="modal-body">
                                        <h4><?= $academiaComisso->academia->name ?></h4>
                                        <br>
                                        <p>
                                            <strong>Tipo de conta: </strong><?= $academiaComisso->academia->tipo_conta ?>
                                            <br>

                                            <strong>Banco: </strong><?= $academiaComisso->academia->banco ?>
                                            <br>

                                            <strong>Agência: </strong>
                                            <?= $academiaComisso->academia->agencia ?><?= $academiaComisso->academia->agencia_dv != null ? ' - '.$academiaComisso->academia->agencia_dv : '' ?>
                                            <br>

                                            <strong>Conta: </strong>
                                            <?= $academiaComisso->academia->conta ?><?= $academiaComisso->academia->conta_dv != null ? ' - '.$academiaComisso->academia->conta_dv : '' ?>
                                            <br>

                                            <?php if(!$academiaComisso->academia->cpf) { ?>
                                                <strong>CNPJ: </strong>
                                                <?= $academiaComisso->academia->cnpj ?>
                                                <br>

                                                <strong>Razão Social: </strong>
                                                <?= $academiaComisso->academia->name ?>
                                                <br>
                                            <?php } else { ?>
                                                <strong>Nome Titular Conta: </strong>
                                                <?= $academiaComisso->academia->favorecido ?>
                                                <br>

                                                <strong>CPF Titular Conta: </strong>
                                                <?= $academiaComisso->academia->cpf ?>
                                                <br>
                                            <?php } ?>

                                            <strong>Observações: </strong><?= $academiaComisso->academia->banco_obs ?>
                                            <br>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="text-center">
                        <?= $this->Form->checkbox('paga['.$academiaComisso->id.']', [
                            'label'     => false,
                            'checked'   => $academiaComisso->paga == 1 ? true : false,
                            'class'     => 'form-control validate[optional] input-switch',
                            'data-on-color' => 'info',
                            'data-on-text'  => 'Sim',
                            'data-off-text' => 'Não',
                            'data-off-color' => 'warning',
                            'data-size'     => 'medium',
                        ]) ?>
                    </td>
                </tr>

            <?php endforeach; ?>
            </tbody>
        </table>

        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Salvar Informações').'</span>',
                ['class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 17em;', 'escape' => false]);
            ?>
        </div>

        <?= $this->Form->end()?>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    </div>
<?php } ?>

<style type="text/css">
    .visualizar-dados-btn {
        background: transparent;
        border: 0;
        color: #888;
        margin-left: 10px;
    }
    .visualizar-dados-btn:hover {
        color: black;
    }
</style>