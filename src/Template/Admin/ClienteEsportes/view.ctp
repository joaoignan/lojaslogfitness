
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?= $this->Html->link(__('Edit Cliente Esporte'), ['action' => 'edit', $clienteEsporte->id], ['class' => 'btn btn-default']) ?>
        <?= $this->Form->postLink(__('Delete Cliente Esporte'), ['action' => 'delete', $clienteEsporte->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $clienteEsporte->id), 'class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('List Cliente Esportes'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Cliente Esporte'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
                    <?= $this->Html->link(__('List Clientes'),
        ['controller' => 'Clientes', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Cliente'),
        ['controller' => 'Clientes', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
                <?= $this->Html->link(__('List Esportes'),
        ['controller' => 'Esportes', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Esporte'),
        ['controller' => 'Esportes', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
            </div>

</div>

<div class="clienteEsportes view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($clienteEsporte->id) ?></h2>
    <div class="row">
        <div class="col-sm-9">
                            <table class="table table-hover">
                                                                        <tr>
                            <td class="font-bold"><?= __('Cliente') ?></td>
                            <td><?= $clienteEsporte->has('cliente') ? $this->Html->link($clienteEsporte->cliente->name, ['controller' => 'Clientes', 'action' => 'view', $clienteEsporte->cliente->id]) : '' ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Esporte') ?></td>
                            <td><?= $clienteEsporte->has('esporte') ? $this->Html->link($clienteEsporte->esporte->name, ['controller' => 'Esportes', 'action' => 'view', $clienteEsporte->esporte->id]) : '' ?></td>
                            </tr>
                                                                </table>
                        </div>

                            <div class="col-md-3">
                    <table class="table table-hover">
                                            <tr>
                            <td class="font-bold"><?= __('Id') ?></td>
                            <td><?= $this->Number->format($clienteEsporte->id) ?></td>
                        </tr>
                                                                                <tr>
                        <td class="font-bold"><?= __('Created') ?></td>
                        <td><?= h($clienteEsporte->created) ?></td>
                    </tr>
                                                                 </table>
            </div>
        </div>
    </div>


