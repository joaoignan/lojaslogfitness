
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= $this->Html->link(__('New Cliente Esporte'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>

                <?= $this->Html->link(__('List Clientes'),
            ['controller' => 'Clientes', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Cliente'),
        ['controller' => 'Clientes', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
                <?= $this->Html->link(__('List Esportes'),
            ['controller' => 'Esportes', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Esporte'),
        ['controller' => 'Esportes', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
            </div>

</div>

<div class="clienteEsportes index">
    <table class="table table-hover">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('cliente_id') ?></th>
            <th><?= $this->Paginator->sort('esporte_id') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th class="actions"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($clienteEsportes as $clienteEsporte): ?>
        <tr>
            <td><?= $this->Number->format($clienteEsporte->id) ?></td>
            <td>
                <?= $clienteEsporte->has('cliente') ? $this->Html->link($clienteEsporte->cliente->name, ['controller' => 'Clientes', 'action' => 'view', $clienteEsporte->cliente->id]) : '' ?>
            </td>
            <td>
                <?= $clienteEsporte->has('esporte') ? $this->Html->link($clienteEsporte->esporte->name, ['controller' => 'Esportes', 'action' => 'view', $clienteEsporte->esporte->id]) : '' ?>
            </td>
            <td><?= h($clienteEsporte->created) ?></td>
            <td class="actions">
                <div class="btn-group">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                        <i class="glyph-icon icon-navicon"></i>
                        <span class="sr-only"><?= __('Actions'); ?></span>
                    </button>
                    <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                        <li>
                            <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                            ['action' => 'view', $clienteEsporte->id],
                            ['escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                            ['action' => 'edit', $clienteEsporte->id],
                            ['escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Form->postLink('<i class="glyph-icon icon-trash-o"></i>  '.__('Delete'),
                            ['action' => 'delete', $clienteEsporte->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $clienteEsporte->id), 'escape' => false]) ?>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


