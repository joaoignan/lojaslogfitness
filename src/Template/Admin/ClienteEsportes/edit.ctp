<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
                <?= $this->Form->postLink(
            __('Delete'),
            ['action' => 'delete', $clienteEsporte->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $clienteEsporte->id),
        'class' => 'btn btn-default']
        )
        ?>
                    <?= $this->Html->link(__('List Cliente Esportes'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
                    <?= $this->Html->link(__('List Clientes'),
        ['controller' => 'Clientes', 'action' => 'index'],
        ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Cliente'),
        ['controller' => 'Clientes', 'action' => 'add'],
        ['class' => 'btn btn-default']) ?>
                <?= $this->Html->link(__('List Esportes'),
        ['controller' => 'Esportes', 'action' => 'index'],
        ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Esporte'),
        ['controller' => 'Esportes', 'action' => 'add'],
        ['class' => 'btn btn-default']) ?>
                </div>
        </div>

        <div class="clienteEsportes form">
        <?= $this->Form->create($clienteEsporte, ['class' => 'form-horizontal bordered-row']); ?>
        <fieldset>
            <legend><?= __('Edit Cliente Esporte') ?></legend>
                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('cliente_id') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('cliente_id', [
                    'options' => $clientes,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('esporte_id') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('esporte_id', [
                    'options' => $esportes,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group text-center">
                <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                'escape' => false]); ?>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>


