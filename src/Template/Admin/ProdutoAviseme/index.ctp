<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= $this->Form->create(null, [
            'role'      => 'form',
            'default'   => false
        ]) ?>
        <?= $this->Form->button('Enviar emails', [
            'type'          => 'submit',
            'div'           => false,
            'class'         => 'btn btn-default',
        ]) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<div class="banners index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('email') ?></th>
            <th><?= $this->Paginator->sort('produto_id') ?></th>
            <th><?= $this->Paginator->sort('status', 'Status do produto') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($produto_aviseme as $p_aviseme): ?>
            <?php if($p_aviseme->status == 0) { ?>
                <tr>
                    <td><?= $this->Number->format($p_aviseme->id) ?></td>
                    <td><?= h($p_aviseme->name) ?></td>
                    <td><?= h($p_aviseme->email) ?></td>
                    <td><a target="_blank" href="/admin/produtos/view/<?= $p_aviseme->produto_id ?>"><?= h($p_aviseme->produto_id) ?></a></td>
                    <td><?= h($p_aviseme->produto->status_id) == 1 ? 'Ativo' : 'Inativo' ?></td>
                    <td><?= h($p_aviseme->created) ?></td>
                </tr>
            <?php } ?>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


