<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="status form">
    <?= $this->Form->create($status, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Edit Status') ?></legend>
                <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[3], maxSize[255]]'
                ]) ?>
            </div>
        </div>

                <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Submit').'</span>'.
            '<i class="glyph-icon icon-arrow-right"></i>',
            ['class' => 'btn btn-alt btn-hover btn-default',
            'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Status', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Status'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
</div>
</div>