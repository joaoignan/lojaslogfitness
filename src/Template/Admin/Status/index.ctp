<div class="status index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('modified') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($status as $status): ?>
            <tr>
                <td><?= $this->Number->format($status->id) ?></td>
                <td><?= h($status->name) ?></td>
                <td><?= h($status->created) ?></td>
                <td><?= h($status->modified) ?></td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?php if($cca->cca('admin', 'Status', 'view', $group_id)){ ?>
                                    <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                        ['action' => 'view', $status->id],
                                        ['escape' => false]) ?>
                                <?php } ?>
                            </li>
                            <li>
                                <?php if($cca->cca('admin', 'Status', 'edit', $group_id)){ ?>
                                    <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                        ['action' => 'edit', $status->id],
                                        ['escape' => false]) ?>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Status', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Status'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>