<?= $this->Form->create(); ?>
<div class="relacionamentoContatos index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('Contatado') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('email') ?></th>
            <th><?= $this->Paginator->sort('phone') ?></th>
            <th><?= $this->Paginator->sort('type', 'Tipo') ?></th>
            <th><?= $this->Paginator->sort('caminho') ?></th>
            <th><?= $this->Paginator->sort('cidade') ?></th>
            <th><?= $this->Paginator->sort('interesse') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($leads as $l): ?>
            <tr>
                <td><?= $this->Number->format($l->id) ?></td>
                <td>
                    <?= $this->Form->checkbox('paga['.$l->id.']', [
                        'label'     => false,
                        'checked'   => $l->status == 1 ? true : false,
                        'class'     => 'form-control validate[optional] input-switch',
                        'data-on-color' => 'info',
                        'data-on-text'  => 'Sim',
                        'data-off-text' => 'Não',
                        'data-off-color' => 'warning',
                        'data-size'     => 'medium',
                    ]) ?>
                </td>
                <td><?= strtoupper(h($l->name)) ?></td>
                <td><?= strtoupper(h($l->email)) ?></td>
                <td><?= strtoupper(h($l->phone)) ?></td>
                <?php if($l->type == 1) { ?>
                    <td>Aluno</td>
                <?php } else if($l->type == 2) { ?>
                    <td>Professor</td>
                <?php } else if($l->type == 3) { ?>
                    <td>Academia</td>
                <?php } else if($l->type == 4) { ?>
                    <td>Franquia 1</td>
                <?php } else if($l->type == 5) { ?>
                    <td>Franquia 2</td>
                <?php } else if($l->type == 6) { ?>
                    <td>Franquia 3</td>
                <?php } ?>
                <td><?= strtoupper(h($l->caminho)) ?></td>
                <td><?= strtoupper(h($l->city->name)) ?></td>
                <td><?= strtoupper(h($l->grau_interesse)) ?></td>
                <td><?= strtoupper(h($l->created)) ?></td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>

    <div class="form-group text-center">
        <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Salvar Informações').'</span>',
            ['class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 17em;', 'escape' => false]);
        ?>
    </div>

    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
<?= $this->Form->end(); ?>