<?php
$this->assign('title', 'Comissões');

$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_listAcademias(__('Listar Academias'), ['controller' => 'Academias', 'action' => 'index']); ?>

        <?= $this->Html->link('Fechar comissões', 
            '/admin/franqueados/fechar_comissoes',
            ['div' => false, 'class' => 'btn btn-default']); ?>
    </div>
</div>
<?= $this->Form->create(); ?>
<div class="academiaComissoes index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th class="text-center">Franqueado</th>
            <th class="text-center">Data</th>
            <th class="text-right">Venda</th>
            <th class="text-right">Comissão</th>
            <th class="text-center">Paga</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($comissoes as $comissao) { ?>
            <tr>
                <td class="text-center">
                    <?= $comissao->user->name ?>
                </td>
                <td class="text-center">
                    <?= $comissao->mes ?>/<?= $comissao->ano ?>
                </td>
                <td class="text-right">
                    R$ <?= number_format($comissao->vendas, 2, ',', '.') ?>
                </td>
                <td class="text-right">
                    R$ <?= number_format($comissao->comissao, 2, ',', '.') ?>
                </td>
                <td class="text-center">
                    <?= $this->Form->checkbox('paga['.$comissao->id.']', [
                        'label'     => false,
                        'checked'   => $comissao->paga == 1 ? true : false,
                        'class'     => 'form-control validate[optional] input-switch',
                        'data-on-color' => 'info',
                        'data-on-text'  => 'Sim',
                        'data-off-text' => 'Não',
                        'data-off-color' => 'warning',
                        'data-size'     => 'medium',
                    ]) ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <div class="form-group text-center">
        <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Salvar Informações').'</span>',
            ['class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 17em;', 'escape' => false]);
        ?>
    </div>
</div>
<?= $this->Form->end() ?>

