<?php
$this->assign('title', 'Comissões');

$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_listAcademias(__('Listar Academias'), ['controller' => 'Academias', 'action' => 'index']); ?>
    </div>
</div>

<div class="academiaComissoes index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th class="text-center">Data</th>
            <th class="text-right">Venda</th>
            <th class="text-right">Comissão</th>
            <th class="text-right">Pago</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($comissoes as $comissao) { ?>
            <tr>
                <td class="text-center">
                    <?= $comissao->mes ?>/<?= $comissao->ano ?>
                </td>
                <td class="text-right">
                    R$ <?= number_format($comissao->vendas, 2, ',', '.') ?>
                </td>
                <td class="text-right">
                    R$ <?= number_format($comissao->comissao, 2, ',', '.') ?>
                </td>
                <td class="text-right">
                    <?= $comissao->status == 1 ? '<span style="color: green">Sim</span>' : '<span style="color: red">Não</span>' ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

