<?php
$this->assign('title', 'Comissões x Academias');

$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);

$anos = [];
for($i = 2016; date('Y') >= $i; $i++){
    $anos[$i] = $i;
}

$meses = [];
for($i = 1; $i <= 12; $i++){
    $meses[str_pad($i, 2, '0', STR_PAD_LEFT)] = str_pad($i, 2, '0', STR_PAD_LEFT);
}
?>
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_listAcademias(__('Listar Academias'), ['controller' => 'Academias', 'action' => 'index']); ?>


        <div class="col-md-6 float-right">
            <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row']); ?>

            <div class="col-sm-1 control-label">
                <?= $this->Form->label('ano') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('ano', [
                    'options' => $anos,
                    'value' => $ano > 0 ? $ano : '',
                    'empty' => 'Ano',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>

            <div class="col-sm-1 control-label">
                <?= $this->Form->label('mes', 'Mês') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('mes', [
                    'options' => $meses,
                    'value' => $mes > 0 ? $mes : '',
                    'empty' => 'Mês',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>

            <div class="col-sm-1 text-center">
                <?= $this->Form->button('<i class="glyph-icon icon-search"></i> <span>Filtrar</span>',
                    ['class' => 'btn btn-alt btn-default',
                        'escape' => false
                    ]); ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>

<div class="academiaComissoes index">

    <?php if (isset($select_date)) { ?>
        <h4>
            Selecione acima um período para consulta...
        </h4>
    <?php } else { ?>

        <table class="table table-hover">
            <thead>
            <tr>
                <th class="text-left">Código</th>
                <th class="text-left">Franqueado</th>
                <th class="text-right">Venda do mês anterior</th>
                <th class="text-right">Venda desse mês</th>
                <th class="text-right">Comissão desse mês</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($pedidos_comissoes as $pedidos_comissao) { ?>
                <tr>
                    <td class="text-left">
                        <?= $pedidos_comissao->user_id ?>
                    </td>
                    <td class="text-left">
                        <?= $pedidos_comissao->user->name ?>
                    </td>
                    <td class="text-right">
                        <?php foreach ($vendas_mes_anterior as $venda_mes_anterior) {
                            if($venda_mes_anterior->academia_id == $pedidos_comissao->academia_id) {
                                echo 'R$ '.number_format($venda_mes_anterior->venda, 2, ',', '.');
                            }
                        } ?>
                    </td>
                    <td class="text-right">
                        R$ <?= number_format($pedidos_comissao->venda, 2, ',', '.') ?>
                    </td>
                    <td class="text-right">
                        R$ <?= number_format($pedidos_comissao->venda * 0.05, 2, ',', '.') ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>
</div>