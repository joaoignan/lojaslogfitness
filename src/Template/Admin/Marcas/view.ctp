<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
      <?php if($cca->cca('admin', 'Marcas', 'edit', $group_id)){ ?>
            <?= link_button_edit(__('Editar Marca'), ['action' => 'edit', $marca->id]); ?>
        <?php } if($cca->cca('admin', 'Marcas', 'index', $group_id)){ ?>
            <?= link_button_list(__('Listar Marcas'), ['action' => 'index']); ?>
        <?php } if($cca->cca('admin', 'Marcas', 'add', $group_id)){ ?>
            <?= link_button_add(__('Nova Marca'), ['action' => 'add']); ?>
        <?php } ?>
    </div>
</div>

<div class="marcas view" xmlns="http://www.w3.org/1999/html">
    <h2><?= $this->Html->image('marcas/'.$marca->image)?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($marca->name) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status') ?></td>
                    <td><?= $marca->has('status') ? $this->Html->link($marca->status->name, ['controller' => 'Status', 'action' => 'view', $marca->status->id]) : '' ?></td>
                </tr>
            </table>
        </div>
        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($marca->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($marca->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($marca->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>