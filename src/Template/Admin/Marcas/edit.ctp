<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'Marcas', 'index', $group_id)){ ?>
            <?= link_button_list(__('Listar Marcas'), ['action' => 'index']); ?>
        <?php } ?>
    </div>
</div>

<div class="marcas form">
    <?= $this->Form->create($marca, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
    <fieldset>
        <legend><?= __('Editar Marca') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'id'    => 'name',
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[3], maxSize[50]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('image') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('image', [
                    'id'    => 'image_upload',
                    'type'  => 'file',
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
            <div class="col-sm-2">
                <a href=""></a>
            </div>
        </div>

        <?= $this->Form->input('image_action', [
            'id'    => 'image_action',
            'type'  => 'hidden',
            'value' => 'new',
        ]) ?>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('image_preview', 'Imagem Atual') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Html->image($marca->image, [
                    'id'    => 'image_preview',
                    'alt'   => 'Image Upload',
                ])?>
            </div>
        </div>

        <?php if ($tipo_banner == 0) { ?>
            <script type="text/javascript">
                $(window).load(function() {
                    function readURL(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $('.banner_fundo_preview').attr('src', e.target.result);
                                console.log(e.target.result);
                                $('.banner_fundo_preview').Jcrop({
                                    aspectRatio: 192 / 85,
                                    boxHeight: 960,
                                    boxWidth: 425,
                                    setSelect: [0, 0, 0, 0],
                                    allowSelect: false,
                                    minSize: [20, 85]
                                }, function () {

                                    var jcrop_api = this;

                                    $(".jcrop-box").attr('type', 'button');

                                    $('.banner_fundo_preview').Jcrop('animateTo',[0,0,4000,4000]);

                                    jcrop_api.ui.selection.element.on('cropmove',function(e,s,c){
                                        $('#banner_fundo_cropx').val(c.x);
                                        $('#banner_fundo_cropy').val(c.y);
                                        $('#banner_fundo_cropw').val(c.w);
                                        $('#banner_fundo_croph').val(c.h);
                                        $('#banner_fundo_cropwidth').val($('.jcrop-box').width());
                                        $('#banner_fundo_cropheight').val($('.jcrop-box').height());
                                    });
                                });
                            }
                            
                            reader.readAsDataURL(input.files[0]);
                        }
                    }
                    
                    $("#banner_fundo_upload").change(function(){
                        readURL(this);
                    });
                });
            </script>
       <?php } else if ($tipo_banner == 1) { ?>
            <script type="text/javascript">
                $(window).load(function() {
                    function readURL(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $('.banner_fundo_preview').attr('src', e.target.result);
                                console.log(e.target.result);
                                $('.banner_fundo_preview').Jcrop({
                                    aspectRatio: 192 / 35,
                                    boxHeight: 1920,
                                    boxWidth: 350,
                                    setSelect: [0,0,0,0],
                                    allowSelect: false,
                                    minSize: [20, 85]
                                }, function () {

                                    var jcrop_api = this;

                                    $(".jcrop-box").attr('type', 'button');

                                    $('.banner_fundo_preview').Jcrop('animateTo',[0,0,4000,4000]);

                                    jcrop_api.ui.selection.element.on('cropmove',function(e,s,c){
                                        $('#banner_fundo_cropx').val(c.x);
                                        $('#banner_fundo_cropy').val(c.y);
                                        $('#banner_fundo_cropw').val(c.w);
                                        $('#banner_fundo_croph').val(c.h);
                                        $('#banner_fundo_cropwidth').val($('.jcrop-box').width());
                                        $('#banner_fundo_cropheight').val($('.jcrop-box').height());
                                    });
                                });
                            }
                            
                            reader.readAsDataURL(input.files[0]);
                        }
                    }
                    
                    $("#banner_fundo_upload").change(function(){
                        readURL(this);
                    });
                });
            </script>
       <?php  } ?>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('banner_fundo') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('banner_fundo', [
                    'id'    => 'banner_fundo_upload',
                    'type'  => 'file',
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <input type="hidden" name="valor-tipo-banner" value="<?= $tipo_banner ?>">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('banner_fundo_preview', 'Prévia do Banner de fundo') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Html->image($marca->banner_fundo, [
                    'id'    => 'banner_fundo_preview',
                    'class' => 'banner_fundo_preview',
                    'alt'   => 'Image Upload',
                    'style' => 'max-width: 500px;'
                ])?>
            </div>
        </div>

        <input type="hidden" name="banner_fundo_cropx" id="banner_fundo_cropx" value="0" />
        <input type="hidden" name="banner_fundo_cropy" id="banner_fundo_cropy" value="0" />
        <input type="hidden" name="banner_fundo_cropw" id="banner_fundo_cropw" value="0" />
        <input type="hidden" name="banner_fundo_croph" id="banner_fundo_croph" value="0" />
        <input type="hidden" name="banner_fundo_cropwidth" id="banner_fundo_cropwidth" value="0" />
        <input type="hidden" name="banner_fundo_cropheight" id="banner_fundo_cropheight" value="0" />

        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


