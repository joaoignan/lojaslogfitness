<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="clientes index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('academia') ?></th>
            <th><?= $this->Paginator->sort('aluno') ?></th>
            <th><?= $this->Paginator->sort('valor') ?></th>
            <th><?= $this->Paginator->sort('pagamento') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($assinaturas as $assinatura): ?>
            <tr>
                <td><?= $this->Number->format($assinatura->id) ?></td>
                <td><?= h($assinatura->academia->shortname) ?></td>
                <td><?= h($assinatura->cliente->name) ?></td>
                <td>R$ <?= h(number_format($assinatura->valor, 2, ',', '.')) ?></td>
                <td><?= h($assinatura->payment_method) ?></td>
                <td><?= h($assinatura->created) ?></td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


