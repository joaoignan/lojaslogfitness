
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_add(__('Nova Categoria'), ['action' => 'add']); ?>
    </div>

</div>

<div class="categorias index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('iugu_name') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('modified') ?></th>
            <th><?= $this->Paginator->sort('account_verify', 'Status') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($mensalidades as $mensalidade): ?>
            <tr>
                <td><?= $this->Number->format($mensalidade->id) ?></td>
                <td><?= h($mensalidade->iugu_name) ?></td>
                <td><?= h($mensalidade->created) ?></td>
                <td><?= h($mensalidade->modified) ?></td>
                <?php if ($mensalidade->account_verify == 0) { ?>
                    <td class="actions">
                        <?= $this->Html->link(__('Editar academia'),
                            ['controller' => 'Academias', 'action' => 'edit', $mensalidade->academia_id],
                            ['escape' => false, 'target' => '_blank']) ?>
                    </td>
                <?php } else if ($mensalidade->account_verify == 1 || $mensalidade->account_verify == 3) { ?>
                    <td style="color: green">Conta verificada</td>
                <?php } else if ($mensalidade->account_verify == 2) { ?>
                    <td style="color: #5089CF">Conta sendo verificada</td>
                <?php } else if ($mensalidade->account_verify == 4) { ?>
                    <td style="color: red">Conta reprovada</td>
                <?php } ?>            
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


