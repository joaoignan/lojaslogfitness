<?= $this->Html->script('jquery.mask.min.js')?>
<script type="text/javascript">$("#valor_transferencia").mask('000.000.000.000.000,00', {reverse: true});</script>
<style>
    .saldo-transferencia{
        font-weight: 700;
        color: green;
    }
    .colta-atual h4{
        font-weight: 700;
    }
    .transferencia{
        width: 80%;
        margin-left: 10%;
    }
    #valor_transferencia::-webkit-input-placeholder{
        text-align:center;
    }
    #email::-moz-placeholder{
        text-align:center;
    }
    .solicitar{
        margin: 15px 0;
    }
    .transfer-table{
        width: 80%;
        margin-bottom: 35px;
    }
    .transfer-table td{
        height: 35px;
        vertical-align: middle;
        padding: 0 5px;
    }
    @media all and (max-width: 1024px){
        .transfer-table{
            width: 100%;
        }
    }
    .alert-informacao {
        opacity: 1!important;
        height: inherit!important;
    }
</style>


<section class="content">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Realizar Transferência</h3>
            </div>
            <div class="box-body">
                <?= $this->Form->create() ?>
                <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 text-center">
                    <div class="row">
                        <div class="col-xs-12" style="margin-top: 15px">
                            <h4>Informe a academia que irá transferir</h4>
                            <?= $this->Form->input('academia_transferir', [
                                'options' => $list_academia_mensalidades_tranferir,
                                'empty'       => 'selecione uma LogMensalidade...',
                                'label'       => false,
                                'placeholder' => 'R$150,00',
                                'class'       => 'form-control transferencia'
                            ]) ?>
                        </div>

                        <div class="col-xs-12" style="margin-top: 15px">
                            <h4>Informe a academia que irá receber</h4>
                            <?= $this->Form->input('academia_receber', [
                                'options' => $list_academia_mensalidades_receber,
                                'empty'       => 'selecione uma LogMensalidade...',
                                'label'       => false,
                                'placeholder' => 'R$150,00',
                                'class'       => 'form-control transferencia'
                            ]) ?>
                        </div>

                        <div class="col-xs-12" style="margin-top: 15px">
                            <h4>Informe o valor para a transferência</h4>
                            <p class="help-block">Valor mínimo de R$ 5,00</p>
                            <?= $this->Form->input('valor_transferencia', [
                                'label'       => false,
                                'id'          => 'valor_transferencia',
                                'placeholder' => 'R$150,00',
                                'class'       => 'form-control transferencia'
                            ]) ?>
                        </div>
                    </div>

                    <?= $this->Form->button('Transferir', ['escape' => false, 'class' => 'btn btn-success btn-lg', 'style' => 'margin-top:15px']) ?>

                    <?= $this->Form->end(); ?>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</section>