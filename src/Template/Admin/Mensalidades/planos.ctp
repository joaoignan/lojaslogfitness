<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="clientes index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('academia') ?></th>
            <th><?= $this->Paginator->sort('aluno') ?></th>
            <th><?= $this->Paginator->sort('valor') ?></th>
            <th><?= $this->Paginator->sort('pagamento') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($planos as $plano): ?>
            <tr>
                <td><?= $this->Number->format($plano->id) ?></td>
                <td><?= h($plano->academia->shortname) ?></td>
                <td><?= h($plano->cliente->name) ?></td>
                <td>R$ <?= h(number_format($plano->valor, 2, ',', '.')) ?></td>
                <td><?= h($plano->payment_method) ?></td>
                <td><?= h($plano->created) ?></td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


