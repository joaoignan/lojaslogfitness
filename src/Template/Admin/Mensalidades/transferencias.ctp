<?php
$this->assign('title', 'Transferências');

$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);

$anos = [];
for($i = 2016; date('Y') >= $i; $i++){
    $anos[$i] = $i;
}

$meses = [];
for($i = 1; $i <= 12; $i++){
    $meses[str_pad($i, 2, '0', STR_PAD_LEFT)] = str_pad($i, 2, '0', STR_PAD_LEFT);
}
?>
<div class="content-box">
    <div class="content-box-wrapper hidee">
        <?= link_button_listAcademias(__('Listar LogMensalidades'), ['action' => 'index']); ?>

        <div class="col-md-6 float-right">
            <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row']); ?>

            <div class="col-sm-1 control-label">
                <?= $this->Form->label('ano') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('ano', [
                    'options' => $anos,
                    'value' => $ano > 0 ? $ano : '',
                    'empty' => 'Ano',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>

            <div class="col-sm-1 control-label">
                <?= $this->Form->label('mes', 'Mês') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('mes', [
                    'options' => $meses,
                    'value' => $mes > 0 ? $mes : '',
                    'empty' => 'Mês',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>

            <div class="col-sm-1 text-center">
                <?= $this->Form->button('<i class="glyph-icon icon-search"></i> <span>Filtrar</span>',
                    ['class' => 'btn btn-alt btn-default',
                        'escape' => false
                    ]); ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>

<div class="academiaComissoes index">

    <?php if(isset($select_date)){ ?>
        <h4>
            Selecione acima um período para consulta...
        </h4>
    <?php }else{ ?>

        <table class="table table-hover">
            <thead>
            <tr>
                <th class="text-left">Academia</th>
                <th class="text-right">Valor</th>
                <th class="text-right">Criado</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($transferencias as $transferencia):
                ?>
                <tr>
                    <td class="text-left">
                        <p><?= $transferencia->academia->shortname ?></p>
                    </td>
                    <td class="text-right">
                        <p><?= number_format($transferencia->valor, 2, ',', '.') ?></p>
                    </td>
                    <td class="text-right">
                        <p><?= $transferencia->created ?></p>
                    </td>
                </tr>
                <?php 
                    $total += $transferencia->valor;
                ?>
            <?php endforeach; ?>
                <tr>
                    <td class="text-left">
                        <p><strong>Total: </strong></p>
                    </td>
                    <td class="text-right">
                        <p><strong><?= number_format($total, 2, ',', '.') ?></strong></p>
                    </td>
                    <td class="text-right">
                    </td>
                </tr>
            </tbody>
        </table>
    <?php } ?>
</div>