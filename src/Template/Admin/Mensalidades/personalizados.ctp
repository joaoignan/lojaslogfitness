<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="clientes index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('academia') ?></th>
            <th><?= $this->Paginator->sort('aluno') ?></th>
            <th><?= $this->Paginator->sort('valor') ?></th>
            <th><?= $this->Paginator->sort('pagamento') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($personalizados as $personalizado): ?>
            <tr>
                <td><?= $this->Number->format($personalizado->id) ?></td>
                <td><?= h($personalizado->academia->shortname) ?></td>
                <td><?= h($personalizado->cliente->name) ?></td>
                <td>R$ <?= h(number_format($personalizado->valor, 2, ',', '.')) ?></td>
                <td><?= h($personalizado->payment_method) ?></td>
                <td><?= h($personalizado->created) ?></td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


