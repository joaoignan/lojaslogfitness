<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'BannersMarcas', 'add', $group_id)){ ?>
            <?= link_button_add(__('Novo Banner de Marca'), ['action' => 'add']); ?>

        <?php } ?>
    </div>

</div>

<div class="banners index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('marca_id') ?></th>
            <th><?= $this->Paginator->sort('title') ?></th>
            <th><?= $this->Paginator->sort('image') ?></th>
            <th><?= $this->Paginator->sort('status_id') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('modified') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($bannersMarcas as $banner): ?>
            <tr>
                <td><?= $this->Number->format($banner->id) ?></td>
                <td><?= $banner->marca->name ?></td>
                <td><?= h($banner->title) ?></td>
                <td>
                    <a href="<?= IMG_URL; ?>banners/<?= $banner->image ?>" class="fancybox">
                        <?= $this->Html->image('banners/'.$banner->image, ['style' => 'max-width: 50px;']) ?>
                    </a>
                </td>
                <td>
                    <?= $banner->has('status') ? $this->Html->link($banner->status->name, ['controller' => 'Status', 'action' => 'view', $banner->status->id]) : '' ?>
                </td>
                <td><?= h($banner->created) ?></td>
                <td><?= h($banner->modified) ?></td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <?php if($cca->cca('admin', 'BannersMarcas', 'view', $group_id)){ ?>
                                <li>
                                    <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                        ['action' => 'view', $banner->id],
                                        ['escape' => false]) ?>
                                </li>
                            <?php } if($cca->cca('admin', 'BannersMarcas', 'edit', $group_id)){ ?>
                                <li>
                                    <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                        ['action' => 'edit', $banner->id],
                                        ['escape' => false]) ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


