<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'BannersMarcas', 'index', $group_id)){ ?>
            <?= link_button_list(__('Listar Banners de Marca'), ['action' => 'index']); ?>
        <?php } ?>
    </div>
</div>

<div class="banners form">
    <?= $this->Form->create($bannersMarca, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
    <fieldset>
        <legend><?= __('Editar Banner') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('marca_id') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('marca_id', [
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('title') ?>
            </div>
            <div class="col-sm-8">
                <?= $this->Form->input('title', [
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[3], maxSize[255]]',
                    'placeholder' => 'Nome para referência'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('link') ?>
            </div>
            <div class="col-sm-8">
                <?= $this->Form->input('link', [
                    'label' => false,
                    'class' => 'form-control validate[optional,custom[url]]',
                    'placeholder' => 'Link de ação para o banner (opcional)'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('imagem') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('imagem', [
                    'id'    => 'image_upload',
                    'label' => false,
                    'class' => 'form-control validate[optional, custom[validateMIME[image/jpeg|image/png]]]',
                    'type'  => 'file'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('imagem_atual') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Html->image('banners/'.$bannersMarca->image) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('status_id') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('status_id', [
                    'options' => $status,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrr btn-default',  'style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>

