<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideee">
        <?php if($cca->cca('admin', 'BannersMarcas', 'edit', $group_id)){ ?>
            <?= link_button_edit(__('Editar Banner de Marca'), ['action' => 'edit', $bannersMarca->id]); ?>
        <?php } ?>
        <?php if($cca->cca('admin', 'BannersMarcas', 'index', $group_id)){ ?>
            <?= link_button_list(__('Listar Banners de Marca'), ['action' => 'index']); ?>

        <?php } ?>
    </div>
</div>

<div class="banners view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($bannersMarca->title) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Title') ?></td>
                    <td><?= h($bannersMarca->title) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Image') ?></td>
                    <td><?= $this->Html->image('banners/'.$bannersMarca->image) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status') ?></td>
                    <td><?= $bannersMarca->status->name ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($bannersMarca->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($bannersMarca->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($bannersMarca->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>


