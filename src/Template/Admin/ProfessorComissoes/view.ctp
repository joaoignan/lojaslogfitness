
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?= $this->Html->link(__('Edit Professor Comisso'), ['action' => 'edit', $professorComisso->id], ['class' => 'btn btn-default']) ?>
        <?= $this->Form->postLink(__('Delete Professor Comisso'), ['action' => 'delete', $professorComisso->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $professorComisso->id), 'class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('List Professor Comissoes'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Professor Comisso'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
                    <?= $this->Html->link(__('List Professores'),
        ['controller' => 'Professores', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Professore'),
        ['controller' => 'Professores', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
                <?= $this->Html->link(__('List Pedidos'),
        ['controller' => 'Pedidos', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Pedido'),
        ['controller' => 'Pedidos', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
            </div>

</div>

<div class="professorComissoes view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($professorComisso->id) ?></h2>
    <div class="row">
        <div class="col-sm-9">
                            <table class="table table-hover">
                                                                        <tr>
                            <td class="font-bold"><?= __('Professore') ?></td>
                            <td><?= $professorComisso->has('professore') ? $this->Html->link($professorComisso->professore->name, ['controller' => 'Professores', 'action' => 'view', $professorComisso->professore->id]) : '' ?></td>
                            </tr>
                                                                </table>
                        </div>

                            <div class="col-md-3">
                    <table class="table table-hover">
                                            <tr>
                            <td class="font-bold"><?= __('Id') ?></td>
                            <td><?= $this->Number->format($professorComisso->id) ?></td>
                        </tr>
                                            <tr>
                            <td class="font-bold"><?= __('Ano') ?></td>
                            <td><?= $this->Number->format($professorComisso->ano) ?></td>
                        </tr>
                                            <tr>
                            <td class="font-bold"><?= __('Mes') ?></td>
                            <td><?= $this->Number->format($professorComisso->mes) ?></td>
                        </tr>
                                            <tr>
                            <td class="font-bold"><?= __('Meta') ?></td>
                            <td><?= $this->Number->format($professorComisso->meta) ?></td>
                        </tr>
                                            <tr>
                            <td class="font-bold"><?= __('Vendas') ?></td>
                            <td><?= $this->Number->format($professorComisso->vendas) ?></td>
                        </tr>
                                            <tr>
                            <td class="font-bold"><?= __('Comissao') ?></td>
                            <td><?= $this->Number->format($professorComisso->comissao) ?></td>
                        </tr>
                                            <tr>
                            <td class="font-bold"><?= __('Aceita') ?></td>
                            <td><?= $this->Number->format($professorComisso->aceita) ?></td>
                        </tr>
                                            <tr>
                            <td class="font-bold"><?= __('Paga') ?></td>
                            <td><?= $this->Number->format($professorComisso->paga) ?></td>
                        </tr>
                                                                                <tr>
                        <td class="font-bold"><?= __('Created') ?></td>
                        <td><?= h($professorComisso->created) ?></td>
                    </tr>
                                    <tr>
                        <td class="font-bold"><?= __('Modified') ?></td>
                        <td><?= h($professorComisso->modified) ?></td>
                    </tr>
                                                                
                        <tr>
                        <td class="font-bold"><?= __('Obs') ?></td>
                        <?= $this->Text->autoParagraph(h($professorComisso->obs)); ?>
                        </tr>
                                     </table>
            </div>
        </div>
    </div>
<div class="row">
    <div class="column large-12">
    <h4 class="font-bold"><?= __('Related Pedidos') ?></h4>
    <?php if (!empty($professorComisso->pedidos)): ?>
    <table class="table table-hover">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Cliente Id') ?></th>
            <th><?= __('Entrega') ?></th>
            <th><?= __('Peso') ?></th>
            <th><?= __('Volume') ?></th>
            <th><?= __('Frete') ?></th>
            <th><?= __('Academia Id') ?></th>
            <th><?= __('Professor Id') ?></th>
            <th><?= __('Transportadora Id') ?></th>
            <th><?= __('Rastreado Por') ?></th>
            <th><?= __('Rastreamento') ?></th>
            <th><?= __('Pedido Status Id') ?></th>
            <th><?= __('Valor') ?></th>
            <th><?= __('Payment Method') ?></th>
            <th><?= __('Tid') ?></th>
            <th><?= __('Transaction Data') ?></th>
            <th><?= __('Created') ?></th>
            <th><?= __('Modified') ?></th>
            <th class="actions"></th>
        </tr>
        <?php foreach ($professorComisso->pedidos as $pedidos): ?>
        <tr>
            <td><?= h($pedidos->id) ?></td>
            <td><?= h($pedidos->cliente_id) ?></td>
            <td><?= h($pedidos->entrega) ?></td>
            <td><?= h($pedidos->peso) ?></td>
            <td><?= h($pedidos->volume) ?></td>
            <td><?= h($pedidos->frete) ?></td>
            <td><?= h($pedidos->academia_id) ?></td>
            <td><?= h($pedidos->professor_id) ?></td>
            <td><?= h($pedidos->transportadora_id) ?></td>
            <td><?= h($pedidos->rastreado_por) ?></td>
            <td><?= h($pedidos->rastreamento) ?></td>
            <td><?= h($pedidos->pedido_status_id) ?></td>
            <td><?= h($pedidos->valor) ?></td>
            <td><?= h($pedidos->payment_method) ?></td>
            <td><?= h($pedidos->tid) ?></td>
            <td><?= h($pedidos->transaction_data) ?></td>
            <td><?= h($pedidos->created) ?></td>
            <td><?= h($pedidos->modified) ?></td>

            <td class="actions">
                <div class="btn-group">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                        <i class="glyph-icon icon-navicon"></i>
                        <span class="sr-only"><?= __('Actions'); ?></span>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu" style="left: -110px;">
                        <li>
                            <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                            ['Pedidos', 'action' => 'view', $pedidos->id],
                            ['escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                            ['controller' => 'Pedidos', 'action' => 'edit', $pedidos->id],
                            ['escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Form->postLink('<i class="glyph-icon icon-trash-o"></i>  '.__('Delete'),
                            ['controller' => 'Pedidos', 'action' => 'delete', $pedidos->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $pedidos->id), 'escape' => false]) ?>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>


