<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
                <?= $this->Html->link(__('List Professor Comissoes'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
                    <?= $this->Html->link(__('List Professores'),
        ['controller' => 'Professores', 'action' => 'index'],
        ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Professore'),
        ['controller' => 'Professores', 'action' => 'add'],
        ['class' => 'btn btn-default']) ?>
                <?= $this->Html->link(__('List Pedidos'),
        ['controller' => 'Pedidos', 'action' => 'index'],
        ['class' => 'btn btn-default']) ?>
        <?= $this->Html->link(__('New Pedido'),
        ['controller' => 'Pedidos', 'action' => 'add'],
        ['class' => 'btn btn-default']) ?>
                </div>
        </div>

        <div class="professorComissoes form">
        <?= $this->Form->create($professorComisso, ['class' => 'form-horizontal bordered-row']); ?>
        <fieldset>
            <legend><?= __('Add Professor Comisso') ?></legend>
                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('ano') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('ano', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('mes') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('mes', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('professor_id') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('professor_id', [
                    'options' => $professores,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('meta') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('meta', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('vendas') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('vendas', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('comissao') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('comissao', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('aceita') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('aceita', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('paga') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('paga', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('obs') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('obs', [
                    'label' => false,
                    'class' => 'form-control validate[]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('pedidos._ids') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('pedidos._ids', [
                    'options' => $pedidos,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[]'
                    ]) ?>
                </div>
            </div>
                        <div class="form-group text-center">
                <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                'escape' => false]); ?>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>


