<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);

$anos = [];
for($i = 2017; date('Y') >= $i; $i++){
    $anos[$i] = $i;
}

$meses = [];
for($i = 1; $i <= 12; $i++){
    $meses[str_pad($i, 2, '0', STR_PAD_LEFT)] = str_pad($i, 2, '0', STR_PAD_LEFT);
}
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_listAcademias(__('Listar Professores'), ['controller' => 'Professores', 'action' => 'index']); ?>

        <div class="col-md-10 float-right">
            <?= $this->Form->create(null, ['url' => ['controller' => 'professorComissoes', 'action' => 'filtrar_index'], 'class' => 'form-horizontal bordered-row']); ?>

            <div class="col-sm-1 control-label">
                <?= $this->Form->label('ano') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('ano', [
                    'options' => $anos,
                    'value' => $ano,
                    'empty' => 'Ano',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>

            <div class="col-sm-1 control-label">
                <?= $this->Form->label('mes', 'Mês') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('mes', [
                    'options' => $meses,
                    'value' => $mes,
                    'empty' => 'Mês',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>

            <div class="col-sm-1 control-label">
                <?= $this->Form->label('ano') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('filter', [
                    'options' => [
                        0 => 'Todas',
                        1 => 'A Pagar',
                        2 => 'Pagas'
                    ],
                    'value' => $filter,
                    'label' => false,
                    'class' => 'form-control chosen-select filter_comissoes'
                ]) ?>
            </div>

            <div class="col-sm-1 text-center">
                <?= $this->Form->button('<i class="glyph-icon icon-search"></i> <span>Filtrar</span>',
                    ['class' => 'btn btn-alt btn-default',
                        'escape' => false
                    ]); ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.filter_comissoes').change(function() {
            var value = $(this).val();

            location.href = WEBROOT_URL + 'admin/academia_comissoes/index/' + value;
        });
    });
</script>

<div class="professorComissoes index">
    <?= $this->Form->create(null)?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th class="text-center"><?= $this->Paginator->sort('Cod') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('Professor') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('Vendas mês anterior') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('Vendas desse mês') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('Comissões desse mês') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('Dados Bancários') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('Pago/Não Pago') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($professorComissoes as $professorComisso): ?>
            <tr>
                <td class="text-center"><?= $this->Number->format($professorComisso->id) ?></td>
                <td class="text-center">
                    <?= $this->Html->link('#'.$professorComisso->professor_id.' - '.ucwords(strtolower($professorComisso->professore->name)),
                        WEBROOT_URL.'/admin/professores/view/'.$professorComisso->professor_id, ['escape' => false, 'target' => '_blank']
                    ); ?>
                <td class="text-center">R$ <?= number_format($vendas_mes_anterior[$professorComisso->professor_id], 2, ',', '.') ?></td>
                <td class="text-center">R$ <?= number_format($professorComisso->vendas, 2, ',', '.') ?></td>
                <td class="text-center">R$ <?= number_format($professorComisso->comissao, 2, ',', '.') ?></td>
                <td class="actions text-center">
                    <?= $this->Form->button('<i class="glyph-icon icon-usd"></i> <span>Visualizar dados</span>',
                        [
                            'data-target'=> '#dados-'.$professorComisso->id,
                            'data-toggle'=> 'modal',
                            'title'     => 'Visualizar dados bancários',
                            'escape'    => false,
                            'class'     => 'btn btn-alt btn-hoverrr btn-default',
                            'type'      => 'button'
                        ]);
                    ?>
                    <div class="modal fade"
                         id="dados-<?= $professorComisso->id ?>"
                         tabindex="-1" role="dialog"
                         aria-labelledby="dados-<?= $professorComisso->id ?>Label"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Dados Bancários :: <?= $professorComisso->professore->name ?></h4>
                                </div>
                                <div class="modal-body">
                                    <h4><?= $professorComisso->professore->name ?></h4>
                                    <br>
                                    <p>
                                        <strong>Tipo de conta: </strong><?= $professorComisso->professore->tipo_conta ?>
                                        <br>

                                        <strong>Banco: </strong><?= $professorComisso->professore->banco ?>
                                        <br>

                                        <strong>Agência: </strong>
                                        <?= $professorComisso->professore->agencia ?><?= $professorComisso->professore->agencia_dv != null ? ' - '.$professorComisso->professore->agencia_dv : '' ?>
                                        <br>

                                        <strong>Conta: </strong>
                                        <?= $professorComisso->professore->conta ?><?= $professorComisso->professore->conta_dv != null ? ' - '.$professorComisso->professore->conta_dv : '' ?>
                                        <br>

                                        <strong>CPF Titular Conta: </strong>
                                        <?= $professorComisso->professore->cpf ?>
                                        <br>

                                        <strong>Observações: </strong><?= $professorComisso->professore->banco_obs ?>
                                        <br>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <td class="text-center">
                    <?= $this->Form->checkbox('paga['.$professorComisso->id.']', [
                        'label'     => false,
                        'checked'   => $professorComisso->paga == 1 ? true : false,
                        'class'     => 'form-control validate[optional] input-switch',
                        'data-on-color' => 'info',
                        'data-on-text'  => 'Sim',
                        'data-off-text' => 'Não',
                        'data-off-color' => 'warning',
                        'data-size'     => 'medium',
                    ]) ?>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>

    <div class="form-group text-center">
        <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Salvar Informações').'</span>',
            ['class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 17em;', 'escape' => false]);
        ?>
    </div>

    <?= $this->Form->end()?>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>

<style type="text/css">
    .visualizar-dados-btn {
        background: transparent;
        border: 0;
        color: #888;
        margin-left: 10px;
    }
    .visualizar-dados-btn:hover {
        color: black;
    }
</style>