
<div class="groups view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($group->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($group->name) ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($group->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status Id') ?></td>
                    <td><?= $this->Number->format($group->status_id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($group->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($group->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="column large-12">
        <h4 class="font-bold"><?= __('Related Users') ?></h4>
        <?php if (!empty($group->users)): ?>
            <table class="table table-hover">
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __(' ') ?></th>
                    <th><?= __('Name') ?></th>
                    <th><?= __('Username') ?></th>
                    <th><?= __('Status Id') ?></th>
                    <th><?= __('Group Id') ?></th>
                    <th><?= __('Created') ?></th>
                    <th><?= __('Modified') ?></th>
                    <th class="actions"></th>
                </tr>
                <?php foreach ($group->users as $users): ?>
                    <tr>
                        <td><?= h($users->id) ?></td>
                        <td><?= $this->Html->image(WI_RESIZE.'32/32/users*'.$users->image) ?></td>
                        <td><?= h($users->name) ?></td>
                        <td><?= h($users->username) ?></td>
                        <td><?= h($users->status_id) ?></td>
                        <td><?= h($users->group_id) ?></td>
                        <td><?= h($users->created) ?></td>
                        <td><?= h($users->modified) ?></td>

                        <td class="actions">
                            <div class="btn-group">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                                    <i class="glyph-icon icon-navicon"></i>
                                    <span class="sr-only"><?= __('Actions'); ?></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu" style="left: -110px;">
                                    <?php if($cca->cca('admin', 'Users', 'view', $group_id)){ ?>
                                        <li>
                                            <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                                ['Users', 'action' => 'view', $users->id],
                                                ['escape' => false]) ?>
                                        </li>
                                    <?php } if($cca->cca('admin', 'Users', 'edit', $group_id)){ ?>
                                        <li>
                                            <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                                ['controller' => 'Users', 'action' => 'edit', $users->id],
                                                ['escape' => false]) ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'Groups', 'edit', $group_id)){ ?>
            <?= $this->Html->link(__('Edit Group'), ['action' => 'edit', $group->id], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Groups', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Groups'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Groups', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Group'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Users', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Users'),
                ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Users', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New User'),
                ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>