<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideee">
        <?= link_button_list(__('List Diferenciais'), ['action' => 'index']); ?>
    </div>
</div>

<div class="categorias form">
    <?= $this->Form->create($diferencial, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Edit Categoria') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('imagem') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('imagem', [
                    'label' => false,
                    'class' => 'form-control validate[required, custom[validateMIME[image/jpeg|image/png]]]',
                    'type'  => 'file'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('status') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('status', [
                    'options' => [
                        'Inativo',
                        'Ativo'
                    ],
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 9.5em;', 'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


