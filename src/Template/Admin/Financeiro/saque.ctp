<?= $this->Html->script('jquery.mask.min.js')?>
<script type="text/javascript">$("#valor_transferencia").mask('000.000.000.000.000,00', {reverse: true});</script>
<style>
	.saldo-transferencia{
		font-weight: 700;
		color: green;
	}
	.colta-atual h4{
		font-weight: 700;
	}
	.transferencia{
		width: 80%;
		margin-left: 10%;
	}
	#valor_transferencia::-webkit-input-placeholder{
	    text-align:center;
	}
	#email::-moz-placeholder{
	    text-align:center;
	}
	.solicitar{
		margin: 15px 0;
	}
	.transfer-table{
		width: 80%;
		margin-bottom: 35px;
	}
	.transfer-table td{
		height: 35px;
		vertical-align: middle;
		padding: 0 5px;
	}
	@media all and (max-width: 1024px){
		.transfer-table{
			width: 100%;
		}
	}
</style>


<section class="content">
	<div class="col-xs-12">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Solicitar Transferência</h3>
				<span class="pull-right"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
                  '/academia/admin/dashboard',
                  ['escape' =>  false,
                     'class'  =>  'pull-right']) ?></span>
            </div>
			<div class="box-body">
				<?= $this->Form->create() ?>
				<div class="col-xs-12 text-center">
					<?php 
						$valor = explode(' ', $resp_iugu_conta_info->balance_available_for_withdraw);
	                    if($valor[1] == 'BRL') {
	                        $valor = str_replace(',', '.', $valor[0]);
	                    } else {
	                        $valor = str_replace(',', '.', $valor[1]);
	                    }
					?>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 text-center">
					<table align="center" class="transfer-table" style="background-color: #eeeeee">
						<tr>
							<td style="text-align: left;">Saldo Disponível para retirada</td>
							<td style="text-align: right;"> R$ <?= number_format($valor, 2, ',', '.') ?></td>
						</tr>
					</table>

					<h4>Informe o valor para a transferência</h4>
					<p class="help-block">Valor mínimo de R$ 5,00</p>
					<?= $this->Form->input('valor_transferencia', [
						'label'		  => false,
						'id'		  => 'valor_transferencia',
						'placeholder' => 'R$150,00',
						'class'       => 'form-control transferencia'
					]) ?>

					<p class="help-block">Confira os dados da sua conta antes de realizar a transferência.</p>

					<?= $this->Form->button('Solicitar', ['escape' => false, 'class' => 'btn btn-success btn-lg']) ?>

					<?= $this->Form->end(); ?>
					<hr>
				</div>
			</div>
		</div>
	</div>
</section>