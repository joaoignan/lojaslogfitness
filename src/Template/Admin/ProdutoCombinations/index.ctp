<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'Produtos', 'index', $group_id)){ ?>
            <?= link_button_list(__('Listar Produtos'), ['controller' => 'Produtos', 'action' => 'index']); ?>
        <?php } if($cca->cca('admin', 'Produtos', 'add', $group_id)){ ?>
            <?= link_button_add(__('Novo Produto'), ['controller' => 'Produtos', 'action' => 'add']); ?>
        <?php } ?>
    </div>
</div>

<div class="produtoCombinations index">
    <?= $this->Form->create(null)?>
    <div class="row">
        <div class="form-group col-md-6">
            <?= $this->Form->input('search', [
                'div'           => false,
                'label'         => false,
                'class'         => 'form-control',
                'placeholder'   => 'Busca'
            ])?>
        </div>
        <div class="form-group text-left">
            <?= $this->Form->button('<i class="glyph-icon icon-search"></i><span> '.__('Buscar Produto').'</span>',
                [
                    'class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 15em;',
                    'escape' => false
                ])
            ?>
        </div>
    </div>
    <?= $this->Form->end()?>

    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('status_id') ?></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($produtoCombinations as $produtoCombination):  ?>
            <tr>
                <td><?= $this->Number->format($produtoCombination->id) ?></td>

                <td>
                    <?= $produtoCombination->produto_base->name?>


                </td>
                <td>
                    <?= $produtoCombination->has('status') ? $produtoCombination->status->name : '' ?>
                </td>
                <td>
                    <?php if($cca->cca('admin', 'ProdutoCombinations', 'add', $group_id)){ ?>
                        <?= $this->Html->link('<i class="glyph-icon icon-th-large"></i><span> Combinações</span>',
                            ['controller' => 'ProdutoCombinations', 'action' => 'add', $produtoCombination->id],
                            ['class' => 'btn btn-alt btn-hoverrr btn-default', 'escape' => false]
                        )?>
                    <?php } ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>