<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'ProdutoCombinations', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produto Combinations'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'index', $group_id)){ ?>
             <?= $this->Html->link(__('List Produtos'),
                ['controller' => 'Produtos', 'action' => 'index'],
                ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'add', $group_id)){ ?>
             <?= $this->Html->link(__('New Produto'),
                ['controller' => 'Produtos', 'action' => 'add'],
                ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
    </div>

        <div class="produtoCombinations form">
        <?= $this->Form->create($produtoCombination, ['class' => 'form-horizontal bordered-row']); ?>
        <fieldset>
            <legend><?= __('Edit Produto Combination') ?></legend>
                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('produto1_id') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('produto1_id', [
                    'options' => $produtos,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group">
                <div class="col-sm-2 control-label">
                    <?= $this->Form->label('produto2_id') ?>
                </div>
                <div class="col-sm-6">
                    <?= $this->Form->input('produto2_id', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                    ]) ?>
                </div>
            </div>

                        <div class="form-group text-center">
                <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                'escape' => false]); ?>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>


