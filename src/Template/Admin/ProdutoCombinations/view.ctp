<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'ProdutoCombinations', 'edit', $group_id)){ ?>
            <?= $this->Html->link(__('Edit Produto Combination'), ['action' => 'edit', $produtoCombination->id], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'ProdutoCombinations', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produto Combinations'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'ProdutoCombinations', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto Combination'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Produtos'),
                ['controller' => 'Produtos', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'Produtos', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Produto'),
                ['controller' => 'Produtos', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>

<div class="produtoCombinations view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($produtoCombination->id) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Produto') ?></td>
                    <td><?= $produtoCombination->has('produto') ? $this->Html->link($produtoCombination->produto->id, ['controller' => 'Produtos', 'action' => 'view', $produtoCombination->produto->id]) : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Produto2 Id') ?></td>
                    <td><?= h($produtoCombination->produto2_id) ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($produtoCombination->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($produtoCombination->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($produtoCombination->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>