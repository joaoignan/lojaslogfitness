<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
         <?php if($cca->cca('admin', 'ProdutoCombinations', 'index', $group_id)){ ?>
            <?= $this->Html->link('<i class="glyph-icon icon-arrow-circle-left"></i> '.__('Voltar'),
                ['action' => 'index'], ['class' => 'btn btn-default',  'style' =>  'width: 8em;',
                    'escape' => false]); ?>
        <?php } ?>
    </div>
</div>

<div class="produtoCombinations form">
    <?= $this->Form->create($produtoCombination, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Editar Combinações de Produtos') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('produto_base1_id', 'Produto') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('produto_base1_id', [
                    'type'      => 'text',
                    'value'     => $produto->name,
                    'readonly'  => true,
                    'label'     => false,
                    'class'     => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('produto_base2_id', 'Combina Com') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('produto_base2_id', [
                    'options'   => $produtos,
                    'multiple'  => true,
                    'label'     => false,
                    'class'     => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrrr btn-default', 'style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>