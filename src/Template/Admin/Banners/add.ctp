<?php
$myTemplates = [
    'inputContainer' => '{{content}}',
];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'Banners', 'index', $group_id)){ ?>
            <?= link_button_list(__('Listar Banners'), ['action' => 'index']); ?>
        <?php } ?>
    </div>
</div>

<div class="banners form">
    <?= $this->Form->create($banner, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
    <fieldset>
        <legend><?= __('Adicionar Banner') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name', 'Nome') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[3], maxSize[255]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('link') ?>
            </div>
            <div class="col-sm-8">
                <?= $this->Form->input('link', [
                    'label' => false,
                    'class' => 'form-control validate[optional,custom[url]]',
                    'placeholder' => 'Link de ação para o banner (opcional)'
                ]) ?>
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('imagem') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('imagem', [
                    'id'    => 'image_upload',
                    'label' => false,
                    'class' => 'form-control validate[required, custom[validateMIME[image/jpeg|image/png]]]',
                    'type'  => 'file'
                ]) ?>
            </div>
            <div class="col-sm-2">
                <a href=""></a>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('image_preview', 'Imagem Atual') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Html->image('default.png', [
                    'id'    => 'image_preview',
                    'alt'   => 'Image Upload',
                ])?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('visivel', 'Visível') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('visivel', [
                    'options'  => [
                        0 => 'Não',
                        1 => 'Sim',
                        ],
                    'empty' => false,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('type', 'Tipo') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('type', [
                    'options'  => [
                        'inicial'   => 'Página Inicial',
                        'busca'     => 'Busca',
                        'promocoes' => 'Promoções',
                        ],
                    'empty' => false,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
           <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrrr btn-default', 'style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>
