<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?php if($cca->cca('admin', 'Banners', 'add', $group_id)){ ?>
            <?= link_button_add(__('Novo Banner'), ['action' => 'add']); ?>
        <?php } ?>
    </div>

</div>

<div class="banners index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('image') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($banners as $banner): ?>
            <tr>
                <td><?= $this->Number->format($banner->id) ?></td>
                <td><?= h($banner->name) ?></td>
                <td>
                <?= $this->Html->image('banners-loja/'.$banner->image, ['style' => 'max-height: 80px; max-width: 80px;'])?>
                </td>
                <td><?= h($banner->created) ?></td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <?php if($cca->cca('admin', 'Banners', 'view', $group_id)){ ?>
                                <li>
                                    <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                        ['action' => 'view', $banner->id],
                                        ['escape' => false]) ?>
                                </li>
                            <?php } if($cca->cca('admin', 'Banners', 'edit', $group_id)){ ?>
                                <li>
                                    <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                        ['action' => 'edit', $banner->id],
                                        ['escape' => false]) ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


