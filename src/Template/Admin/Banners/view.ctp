
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideee">
        <?php if($cca->cca('admin', 'Banners', 'edit', $group_id)){ ?>
            <?= link_button_edit(__('Editar Banner'),  ['action' => 'edit', $banner->id]); ?>
        <?php } ?>
        <?php if($cca->cca('admin', 'Banners', 'index', $group_id)){ ?>
            <?= link_button_list(__('Listar Banners'), ['action' => 'index']); ?>
        <?php } if($cca->cca('admin', 'Banners', 'add', $group_id)){ ?>
            <?= link_button_add(__('Novo Banner'), ['action' => 'add']); ?>
        <?php } ?>
    </div>
</div>

<div class="banners view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($banner->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Title') ?></td>
                    <td><?= h($banner->name) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Image') ?></td>
                    <td><?= $this->Html->image('banners-loja/'.$banner->image, [
                        'id'    => 'image_preview'
                    ]) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Visivel') ?></td>
                    <td><?= $banner->visivel ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($banner->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($banner->created) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
