<?php $this->assign('title', 'Sugestões de alunos'); ?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
</div>

<div class="preOrders index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('email') ?></th>
            <th><?= $this->Paginator->sort('telephone') ?></th>
            <th><?= $this->Paginator->sort('quem indicou?') ?></th>
            <th><?= $this->Paginator->sort('academia/professor') ?></th>
            <th><?= $this->Paginator->sort('city') ?></th>
            <th><?= $this->Paginator->sort('state') ?></th>
            <th><?= $this->Paginator->sort('aceitou') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($cliente_sugestos as $cs): ?>

            <tr>
                <td><?= $this->Number->format($cs->id) ?></td>
                <td>
                    <?= $cs->name ?>
                </td>
                <td>
                    <?= $cs->email ?>
                </td>
                <td>
                    <?= $cs->has('telephone') ? $cs->telephone : '' ?>
                </td>
                <td>
                    <?= $cs->has('academia_id') ? 'Academia' : 'Professor' ?>
                </td>
                <td>
                    <?= $cs->has('academia_id') ? $cs->academia->name : $cs->professore->name ?>
                </td>
                <td>
                    <?= $cs->city->name ?>
                </td>
                <td>
                    <?= $cs->city->uf ?>
                </td>
                <td>
                    <?= $cs->aceitou == 0 ? 'Não' : 'Sim' ?>
                </td>
                <td><?= h($cs->created) ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>

<br>
<br>

<?= 'Quantidade de indicações por academia: '.$qtd_ind_acad.'<br><br>' ?>
<?= 'Quantidade de indicações por professor: '.$qtd_ind_prof.'<br><br>' ?>
<?= 'Quantidade de indicações aceitas: '.$qtd_ind_aceitas.'<br><br>' ?>
<?= 'Quantidade de indicações total: '.$qtd_ind.'<br><br>' ?>