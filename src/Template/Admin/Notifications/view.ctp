
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideee">
       <?= link_button_edit(__('Editar Notificação'), ['action' => 'edit', $notification->id]); ?>
        <?= link_button_list(__('Listar Notificações'), ['action' => 'index']); ?>
        <?= link_button_add(__('Nova Notificação'), ['action' => 'add']); ?>
            </div>

</div>

<div class="notifications view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($notification->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
                            <table class="table table-hover">
                                                                        <tr>
                            <td class="font-bold"><?= __('Name') ?></td>
                            <td><?= h($notification->name) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Description') ?></td>
                            <td><?= h($notification->description) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Link') ?></td>
                            <td><?= h($notification->link) ?></td>
                            </tr>
                                                                </table>
                        </div>

                            <div class="col-md-3">
                    <table class="table table-hover">
                                            <tr>
                            <td class="font-bold"><?= __('Id') ?></td>
                            <td><?= $this->Number->format($notification->id) ?></td>
                        </tr>
                                            <tr>
                            <td class="font-bold"><?= __('Global') ?></td>
                    <td><?= $notification->global == 1 ? 'Sim' : 'Não' ?></td>
                        </tr>
                                            <tr>
                            <td class="font-bold"><?= __('Status Id') ?></td>
                            <td><?= $this->Number->format($notification->status_id) ?></td>
                        </tr>
                                                                                <tr>
                        <td class="font-bold"><?= __('Created') ?></td>
                        <td><?= h($notification->created) ?></td>
                    </tr>
                                    <tr>
                        <td class="font-bold"><?= __('Modified') ?></td>
                        <td><?= h($notification->modified) ?></td>
                    </tr>
                                                                 </table>
            </div>
        </div>
    </div>
