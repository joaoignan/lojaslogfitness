
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_add(__('Nova Notificação'), ['action' => 'add']); ?>
    </div>

</div>

<div class="notifications index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th class="text-center"><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('description') ?></th>
            <th><?= $this->Paginator->sort('link') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('global') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('status_id') ?></th>
            <th class="text-center"><?= $this->Paginator->sort('created') ?></th>
            <th class="actions text-center"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($notifications as $notification): ?>
            <tr>
                <td class="text-center"><?= $this->Number->format($notification->id) ?></td>
                <td><?= h($notification->name) ?></td>
                <td><?= h($notification->description) ?></td>
                <td><?= h($notification->link) ?></td>
                <td class="text-center"><?= $notification->global == 1 ? 'Sim' : 'Não' ?></td>
                <td class="text-center"><?= $notification->status->name ?></td>
                <td class="text-center"><?= h($notification->created) ?></td>
                <td class="actions text-center">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                    ['action' => 'view', $notification->id],
                                    ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                    ['action' => 'edit', $notification->id],
                                    ['escape' => false]) ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


