<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_list(__('Listar Notificaçãoes'), ['action' => 'index']); ?>
    </div>
</div>

<div class="notifications form">
    <?= $this->Form->create($notification, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Editar Notificação') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[required,maxSize[50]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('description') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('description', [
                    'type'  => 'textarea',
                    'label' => false,
                    'class' => 'form-control validate[required,maxSize[255]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('link') ?>
            </div>
            <div class="col-sm-8">
                <?= $this->Form->input('link', [
                    'label' => false,
                    'class' => 'form-control validate[optional,custom[url],maxSize[255]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('global') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('global', [
                    'id'        => 'notification-global',
                    'options'   => [
                        0 => 'Não',
                        1 => 'Sim'
                    ],
                    'label'     => false,
                    'class'     => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group" id="notification-objetivos">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('objetivos') ?>
            </div>
            <div class="col-sm-8">
                <?= $this->Form->input('objetivos', [
                    'options'   => $objetivos,
                    'value'     => $selected_objetivos,
                    'multiple' => true,
                    'label' => false,
                    'class' => 'form-control validate[optional] chosen-select',
                ]) ?>
            </div>
        </div>

        <div class="form-group" id="notification-esportes">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('esportes') ?>
            </div>
            <div class="col-sm-8">
                <?= $this->Form->input('esportes', [
                    'options'   => $esportes,
                    'value'     => $selected_esportes,
                    'multiple'  => true,
                    'label'     => false,
                    'class'     => 'form-control validate[optional] chosen-select',
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('status_id') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('status_id', [
                    'options' => $status,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrrr btn-default',  'style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


