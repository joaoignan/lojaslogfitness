<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideee">
        <?= link_button_list(__('Listar Professores(as)'), ['action' => 'index/todos']); ?>
    </div>
</div>

<div class="academias form">
    <?= $this->Form->create($professor, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
    <fieldset>
        <legend><?= __('Editar Academia') ?></legend>

        <h4 class="font-bold font-italic"><span class="glyph-icon icon-list"></span> Dados</h4>
        <br class="clear">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'id'    => 'nome',
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('cpf') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('cpf', [
                    'label' => false,
                    'class' => 'form-control validate[opitional,custom[cpf]] cpf'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('rg') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('rg', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('cref') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('cref', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('facebook_url', 'Link do Facebook') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('facebook_url', [
                    'id'                => 'facebook_url',
                    'placeholder'       => 'Complemento da URL facebook.com.br/',
                    'label'     => false,
                    'class'     => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="divider"></div>
        <h4 class="font-bold font-italic"><span class="glyph-icon icon-phone-square"></span> Contato</h4>
        <br class="clear">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('email') ?>
            </div>
            <div class="col-sm-5">
                <?= $this->Form->input('email', [
                    'label' => false,
                    'class' => 'form-control validate[opitional, custom[email]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('phone') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('phone', [
                    'label' => false,
                    'class' => 'form-control validate[opitional] phone'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('mobile', 'Celular/WhatsApp') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('mobile', [
                    'label' => false,
                    'class' => 'form-control validate[optional] phone'
                ]) ?>
            </div>
        </div>

        <div class="divider"></div>
        <h4 class="font-bold font-italic"><span class="glyph-icon icon-building"></span> Endereço</h4>
        <br class="clear">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('cep') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('cep', [
                    'label' => false,
                    'class' => 'form-control validate[opitional] cep'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('address') ?>
            </div>
            <div class="col-sm-5">
                <?= $this->Form->input('address', [
                    'label' => false,
                    'class' => 'form-control validate[opitional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('number') ?>
            </div>
            <div class="col-sm-1">
                <?= $this->Form->input('number', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('complement') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('complement', [
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('area') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('area', [
                    'label' => false,
                    'class' => 'form-control validate[opitional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('state_id') ?>
            </div>
            <div class="col-sm-3">
                <?= $this->Form->input('state_id', [
                    'options'       => $states,
                    'id'            => 'states',
                    'value'         => $professor->city->state_id,
                    'empty' => 'Selecione um estado',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[opitional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('city_id') ?>
            </div>
            <div class="col-sm-4">
                <?= $this->Form->input('city_id', [
                    'id'            => 'city',
                    'options' => $cities,
                    'empty' => 'Selecione uma cidade',
                    'label' => false,
                    'class' => 'form-control chosen-select validate[opitional]'
                ]) ?>
            </div>
        </div>

        <div class="divider"></div>
        <h4 class="font-bold font-italic"><span class="glyph-icon icon-photo"></span> Apresentação</h4>
        <br class="clear">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('imagem', 'Logotipo') ?>
            </div>
            <div class="col-sm-5">
                <?= $this->Form->input('imagem', [
                    'id'    => 'image_upload',
                    'type'  => 'file',
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
            <div class="col-sm-2">
                <a href=""></a>
            </div>
        </div>

        <?= $this->Form->input('image_action', [
            'id'    => 'image_action',
            'type'  => 'hidden',
            'value' => 'new',
        ]) ?>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('image_preview', 'Prévia do Logotipo') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Html->image('professores/'.$professor->image, [
                    'id'    => 'image_preview',
                    'alt'   => 'Image Upload',
                ])?>
            </div>
        </div>

        <div class="divider"></div>
        <h4 class="font-bold font-italic"><span class="glyph-icon icon-key"></span> Acesso</h4>
        <br class="clear">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('status_id') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('status_id', [
                    'options' => $status,
                    'empty' => false,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[opitional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('taxa_comissao', 'Taxa de comissão') ?>
            </div>
            <div class="col-sm-2">
                <?= $this->Form->input('taxa_comissao', [
                    'label' => false,
                    'value' => $professor->taxa_comissao,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
           <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                [
                    'class' => 'btn btn-alt btn-hoverr btn-default', 'style' =>  'width: 9.5em;',
                    'escape' => false
                ]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


