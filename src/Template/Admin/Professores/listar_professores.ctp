<?= $this->assign('title',' ');?>
<legend><?= __('Professores') ?></legend>
<div class="clientes index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('academy', 'Nome Fantasia') ?></th>
            <th><?= $this->Paginator->sort('academy', 'Razão Social ') ?></th>
            <th><?= $this->Paginator->sort('city', 'Cidade / UF') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($prof_academia  as $professores): ?>
            <tr>
                <td><?= substr_replace($professores->professore->name,(strlen($professores->professore->name) > 27 ? '...' : ''), 27); ?><br>
                </td>
                <td><?= h($professores->academia->shortname)?></td>
                <td><?= substr_replace($professores->academia->name,(strlen($professores->academia->name) > 27 ? '...' : ''), 27); ?></td>
                <td><?= h($professores->professore->city->name.'/'.$professores->professore->city->uf) ?></td>
                <td>
                    <div class="btn-group">
                        <?php
                        $aceite = '';
                        if($professores->status_id == 3 ){
                            $aceite = 'style="background-color: #ea9143"';?>
                        <?php }?>
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false" <?= $aceite ?>>
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?=__('Actions');?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                    ['action' => 'view', $professores->professore->id],
                                    ['escape' => false]) ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


