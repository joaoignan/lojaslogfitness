<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_listClientes(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']); ?>
        <?= link_button_listClientes(__('Listar Academias'), ['controller' => 'Academias', 'action' => 'index']); ?>
    </div>
</div>

<div class="professores index">
    <?= $this->Form->create(null)?>
    <div class="row">
        <div class="form-group col-md-6">
            <?= $this->Form->input('search', [
                'div'           => false,
                'label'         => false,
                'class'         => 'form-control',
                'placeholder'   => 'Busca'
            ])?>
        </div>
        <div class="form-group text-left">
            <?= $this->Form->button('<i class="glyph-icon icon-search"></i><span> '.__('Buscar Professor').'</span>',
                [
                    'class' => 'btn btn-alt btn-hoverr btn-default', 'style' =>  'width: 15em;',
                    'escape' => false
                ]) ?>
        </div>
    </div>
    <?= $this->Form->end()?>

    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('cpf') ?></th>
            <th><?= $this->Paginator->sort('city_id') ?></th>
            <th class="actions"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($professores as $professor): ?>
            <tr>
                <td><?= $this->Number->format($professor->id) ?></td>
                <td>
                    <?= h($professor->name) ?>
                    <br>
                    <small>
                        <?= h($professor->phone ? $professor->phone : $professor->mobile).' | '.h($professor->email) ?>
                    </small>
                </td>
                <td><?= h($professor->cpf) ?></td>
                <td><?= h($professor->city->name.'-'.$professor->city->uf) ?></td>
                <td class="actions">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                    ['action' => 'view', $professor->id],
                                    ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-pencil-square-o"></i>  '.__('Edit'),
                                    ['action' => 'edit', $professor->id],
                                    ['escape' => false]) ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


