
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideee">
        <?= link_button_list(__('Listar Professores(as)'), ['action' => 'listar_professores']); ?>
    </div>

</div>

<div class="clientes view" xmlns="http://www.w3.org/1999/html">

    <h2><?= h($professore->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($professore->name) ?></td>
                </tr>
                <!--                <tr>
                    <td class="font-bold"><?= __('Genre') ?></td>
                    <td><?= h($professore->genre) ?></td>
                </tr>-->
                <!--                <tr>
                    <td class="font-bold"><?= __('Birth') ?></td>
                    <td><?= h($professore->birth) ?></td>
                </tr>-->
                <tr>
                    <td class="font-bold"><?= __('Cpf') ?></td>
                    <td><?= h($professore->cpf) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Rg') ?></td>
                    <td><?= h($professore->rg) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Cref') ?></td>
                    <td><?= h($professore->cref) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Telephone') ?></td>
                    <td><?= h($professore->phone) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Mobile') ?></td>
                    <td><?= h($professore->mobile) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Email') ?></td>
                    <td><?= h($professore->email) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Address') ?></td>
                    <td><?= h($professore->address) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Number') ?></td>
                    <td><?= h($professore->number) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Complement') ?></td>
                    <td><?= h($professore->complement) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Area') ?></td>
                    <td><?= h($professore->area) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('City') ?></td>
                    <td><?= $professore->city->name ?>-<?= $professore->city->uf ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Cep') ?></td>
                    <td><?= h($professore->cep) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Academia') ?></td>
                    <td><?php foreach($professore->professor_academias as $academias ){
                            if($academias->status_id == 1 OR $academias->status_id == 3){
                                echo h($academias->academia->shortname).'<br>';
                            }
                        };?>
                    </td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status') ?></td>
                    <td><?= $professore->status->name ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($professore->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($professore->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($professore->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>