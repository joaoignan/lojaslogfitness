<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="seoMetaTags form">
    <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row', 'id' => 'form-seo-tags']); ?>
    <fieldset>
        <h5>Tags exibidas por padrão:</h5>
        <h6>name="language" content="pt-br"</h6>
        <h6>name="author" content="Agência M4Web - http://m4web.com.br"</h6>
        <h6>name="designer" content="Agência M4Web - http://m4web.com.br"</h6>
        <h6>name="developer" content="Agência M4Web - http://m4web.com.br"</h6>
        <h6>name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"</h6>
        <div class="divider"></div>
        <legend><?php echo __('Admin Edit').' '.__('Seo Meta Tag'); ?></legend>
        <br class="clear" />

        <?php foreach($tags as $key => $tag){ ?>
            <div class="meta-tags tag-<?php echo $key; ?>">

                <div class="form-group">
                    <div class="col-sm-2 control-label">
                        <?php
                        echo $this->Form->label('seo_meta_attribute_id'); ?>
                    </div>
                    <div class="col-sm-2">
                        <?php
                        echo $this->Form->input('seo_meta_attribute_id.', array(
                            'options' => $seoMetaAttributes,
                            'div'   => false,
                            'label' => false,
                            'value' => $tag['seo_meta_attribute_id'],
                            'class' => 'form-control chosen-select validate[required]'
                        )); ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2 control-label">
                        <?php
                        echo $this->Form->label('name'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $this->Form->input('name.', array(
                            //'id'   => 'seo-meta-tag-name',
                            'div'   => false,
                            'label' => false,
                            'value' => $tag['name'],
                            'class' => 'form-control seo-meta-tag-name validate[required, minSize[3], maxSize[255]]'
                        )); ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2 control-label">
                        <?php
                        echo $this->Form->label('content'); ?>
                    </div>
                    <div class="col-sm-8">
                        <?php
                        echo $this->Form->input('content.', array(
                            'label' => false,
                            'value' => $tag['content'],
                            'class' => 'form-control validate[required, minSize[3], maxSize[255]]'
                        )); ?>
                    </div>

                    <button type="button" class="btn btn-danger delete-tag" title="Remover Meta Tag" id="tag-<?php echo $key; ?>">
                        <i class="glyph-icon icon-minus-square"></i>
                    </button>
                </div>

                <?php
                echo $this->Form->input('cod.', [
                    'type'  => 'hidden',
                    'value' => $tag['id']
                ]);

                echo $this->Form->input('controller.', [
                    'type'  => 'hidden',
                    'value' => $controller
                ]);

                echo $this->Form->input('action.', [
                    'type'  => 'hidden',
                    'value' => $action
                ]); ?>

                <br class="clear">
                <div class="divider col-md-12"></div>
            </div>
        <?php } ?>

        <div id="fields_clone"></div>

        <br class="clear" />
        <br class="clear" />

        <button type="button" class="btn btn-info" title="Adicionar Meta Tag" id="add-tag">
            <i class="glyph-icon icon-plus-square"></i>
        </button>

        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Save').'</span>'.
                '<i class="glyph-icon icon-save"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                    'escape' => false]); ?>
        </div>
    </fieldset>

    <div class="fields_group meta-tags" style="display: none;">
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?php
                echo $this->Form->label('seo_meta_attribute_id'); ?>
            </div>
            <div class="col-sm-2">
                <?php
                echo $this->Form->input('seo_meta_attribute_id.', array(
                    'options' => $seoMetaAttributes,
                    'div'   => false,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[required]'
                )); ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?php
                echo $this->Form->label('name'); ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $this->Form->input('name.', array(
                    //'id'   => 'seo-meta-tag-name',
                    'div'   => false,
                    'label' => false,
                    'class' => 'form-control seo-meta-tag-name validate[required, minSize[3], maxSize[255]]'
                )); ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?php
                echo $this->Form->label('content'); ?>
            </div>
            <div class="col-sm-8">
                <?php
                echo $this->Form->input('content.', array(
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[3], maxSize[255]]'
                )); ?>
            </div>

            <button type="button" class="btn btn-danger delete-tag" title="Remover Meta Tag" id="tag-<?php echo $key; ?>">
                <i class="glyph-icon icon-minus-square"></i>
            </button>
        </div>

        <?php
        echo $this->Form->input('cod.', [
            'type'  => 'hidden',
            'value' => ''
        ]);

        echo $this->Form->input('controller.', [
            'type'  => 'hidden',
            'value' => $controller
        ]);

        echo $this->Form->input('action.', [
            'type'  => 'hidden',
            'value' => $action
        ]); ?>

        <div class="divider col-md-12"></div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

<script>

    $(".seo-meta-tag-name").autocomplete({
        source: availableTags,
        autoFocus: false
        //appendTo: "#seo-meta-tag-name"
    });
</script>