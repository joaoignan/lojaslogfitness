<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="seoMetaTags index">
    <?= $this->Form->create(null, ['class' => 'form-validate']); ?>
    <fieldset>
        <legend><?= h('Visualizar/Editar Meta Tags por Página'); ?></legend>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('controller_action', 'Controlador / Ação'); ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('controller_action', [
                    'type' => 'select',
                    'class' => 'chosen-select validate[required]',
                    'empty' => 'Selecione...',
                    'options' => $actions,
                    'label' => false
                ]); ?>
            </div>
        </div>

        <br class="clear" /><br class="clear" />

        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Next').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                [   'class' => 'btn btn-alt btn-hover btn-default',
                    'escape' => false
                ]); ?>
        </div>

    </fieldset>
    <?= $this->Form->end(); ?>
</div>