<div class="seoMetaTags view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($seoMetaTag->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Seo Meta Attribute') ?></td>
                    <td><?= $seoMetaTag->has('seo_meta_attribute') ? $this->Html->link($seoMetaTag->seo_meta_attribute->name, ['controller' => 'SeoMetaAttributes', 'action' => 'view', $seoMetaTag->seo_meta_attribute->id]) : '' ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Name') ?></td>
                    <td><?= h($seoMetaTag->name) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Content') ?></td>
                    <td><?= h($seoMetaTag->content) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Controller') ?></td>
                    <td><?= h($seoMetaTag->controller) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Action') ?></td>
                    <td><?= h($seoMetaTag->action) ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($seoMetaTag->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($seoMetaTag->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($seoMetaTag->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'SeoMetaTags', 'edit', $group_id)){ ?>
            <?= $this->Html->link(__('Edit Seo Meta Tag'), ['action' => 'edit', $seoMetaTag->id], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'SeoMetaTags', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Seo Meta Tags'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'SeoMetaTags', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Seo Meta Tag'), ['action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'SeoMetaAttributes', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Seo Meta Attributes'),
                ['controller' => 'SeoMetaAttributes', 'action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'SeoMetaAttributes', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Seo Meta Attribute'),
                ['controller' => 'SeoMetaAttributes', 'action' => 'add'], ['class' => 'btn btn-default']) ?>
        <?php } ?>
    </div>
</div>