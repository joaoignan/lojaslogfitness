<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="seoMetaTags form">
    <?= $this->Form->create($seoMetaTag, ['class' => 'form-horizontal bordered-row']); ?>
    <fieldset>
        <legend><?= __('Add Seo Meta Tag') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('seo_meta_attribute_id') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('seo_meta_attribute_id', [
                    'options' => $seoMetaAttributes,
                    'empty' => true,
                    'label' => false,
                    'class' => 'form-control chosen-select validate[]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[3], maxSize[255]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('content') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('content', [
                    'label' => false,
                    'class' => 'form-control validate[required, minSize[3], maxSize[255]]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('controller') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('controller', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('action') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('action', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group text-center">
            <?= $this->Form->button('<span>'.__('Submit').'</span>'.
                '<i class="glyph-icon icon-arrow-right"></i>',
                ['class' => 'btn btn-alt btn-hover btn-default',
                    'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>


<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hide">
        <?php if($cca->cca('admin', 'SeoMetaTags', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Seo Meta Tags'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'SeoMetaAttributes', 'index', $group_id)){ ?>
            <?= $this->Html->link(__('List Seo Meta Attributes'),
                ['controller' => 'SeoMetaAttributes', 'action' => 'index'],
                ['class' => 'btn btn-default']) ?>
        <?php } if($cca->cca('admin', 'SeoMetaAttributes', 'add', $group_id)){ ?>
            <?= $this->Html->link(__('New Seo Meta Attribute'),
                ['controller' => 'SeoMetaAttributes', 'action' => 'add'],
                ['class' => 'btn btn-default']) ?>
        <?php }?>
    </div>
</div>