
<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideee">
        <?= link_button_edit(__('Editar Subcategoria'), ['action' => 'edit', $subcategoria->id]); ?>
        <?= link_button_list(__('List Subcategorias'), ['action' => 'index']); ?>
        <?= link_button_add(__('New Subcategoria'), ['action' => 'add']); ?>
            </div>

</div>

<div class="categorias view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($subcategoria->name) ?></h2>
    <div class="row">
        <div class="col-sm-9">
                            <table class="table table-hover">
                                                                        <tr>
                            <td class="font-bold"><?= __('Name') ?></td>
                            <td><?= h($subcategoria->name) ?></td>
                            </tr>
                                                                                                <tr>
                            <td class="font-bold"><?= __('Slug') ?></td>
                            <td><?= h($subcategoria->slug) ?></td>
                            </tr>

                            <tr>
                            <td class="font-bold"><?= __('Categoria') ?></td>
                            <td><?= h($subcategoria->categoria->objetivo->name.' - '.$subcategoria->categoria->name) ?></td>
                            </tr>

                            <tr>
                            <td class="font-bold"><?= __('Descrição') ?></td>
                            <td><?= h($subcategoria->descricao) ?></td>
                            </tr>
                                                                </table>
                        </div>

                            <div class="col-md-3">
                    <table class="table table-hover">
                                            <tr>
                            <td class="font-bold"><?= __('Id') ?></td>
                            <td><?= $this->Number->format($subcategoria->id) ?></td>
                        </tr>
                                            <tr>
                            <td class="font-bold"><?= __('Status Id') ?></td>
                            <td><?= $this->Number->format($subcategoria->status_id) ?></td>
                        </tr>
                                                                                <tr>
                        <td class="font-bold"><?= __('Created') ?></td>
                        <td><?= h($subcategoria->created) ?></td>
                    </tr>
                                                                 </table>
            </div>
        </div>
    </div>
