<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hideee">
        <?= link_button_list(__('List Categorias'), ['action' => 'index']); ?>
    </div>
</div>

<div class="categorias form">
    <?= $this->Form->create($subcategoria, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
    <fieldset>
        <legend><?= __('Edit Categoria') ?></legend>
        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('name') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('name', [
                    'label' => false,
                    'class' => 'form-control validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('objetivos') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('objetivos', [
                    'options' => $objetivos,
                    'value'   => $subcategoria->categoria->objetivo_id,
                    'empty'   => 'Selecione um objetivo...',
                    'label'   => false,
                    'class'   => 'form-control objetivos-busca validate[required]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('categoria_id') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('categoria_id', [
                    'options' => $categorias,
                    'label' => false,
                    'class' => 'form-control objetivos-resultado validate[required]'
                ]) ?>
            </div>
        </div>

        <script type="text/javascript">
            $(window).load(function() {
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('.banner_lateral_preview').attr('src', e.target.result);
                            console.log(e.target.result);
                            $('.banner_lateral_preview').Jcrop({
                                aspectRatio: .48,
                                boxHeight: 260,
                                boxWidth: 540,
                                setSelect: [0, 0, 0, 0],
                                allowSelect: false,
                                minSize: [20, 85]
                            }, function () {

                                var jcrop_api = this;

                                $(".jcrop-box").attr('type', 'button');

                                $('.banner_lateral_preview').Jcrop('animateTo',[0,0,4000,4000]);

                                jcrop_api.ui.selection.element.on('cropmove',function(e,s,c){
                                    $('#banner_lateral_cropx').val(c.x);
                                    $('#banner_lateral_cropy').val(c.y);
                                    $('#banner_lateral_cropw').val(c.w);
                                    $('#banner_lateral_croph').val(c.h);
                                    $('#banner_lateral_cropwidth').val($('.jcrop-box').width());
                                    $('#banner_lateral_cropheight').val($('.jcrop-box').height());
                                });
                            });
                        }
                        
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                
                $("#banner_lateral_upload").change(function(){
                    readURL(this);
                });
            });
        </script>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('banner_latera') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('banner_latera', [
                    'id'    => 'banner_lateral_upload',
                    'type'  => 'file',
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('banner_lateral_preview', 'Prévia do Banner Lateral') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Html->image($subcategoria->banner_lateral, [
                    'id'    => 'banner_lateral_preview',
                    'class' => 'banner_lateral_preview',
                    'alt'   => 'Image Upload',
                    'style' => 'max-width: 500px;'
                ])?>
            </div>
        </div>

        <input type="hidden" name="banner_lateral_cropx" id="banner_lateral_cropx" value="0" />
        <input type="hidden" name="banner_lateral_cropy" id="banner_lateral_cropy" value="0" />
        <input type="hidden" name="banner_lateral_cropw" id="banner_lateral_cropw" value="0" />
        <input type="hidden" name="banner_lateral_croph" id="banner_lateral_croph" value="0" />
        <input type="hidden" name="banner_lateral_cropwidth" id="banner_lateral_cropwidth" value="0" />
        <input type="hidden" name="banner_lateral_cropheight" id="banner_lateral_cropheight" value="0" />

        <script type="text/javascript">
            $(window).load(function() {
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('.banner_fundo_preview').attr('src', e.target.result);
                            console.log(e.target.result);
                            $('.banner_fundo_preview').Jcrop({
                                aspectRatio: 2.25,
                                boxHeight: 960,
                                boxWidth: 425,
                                setSelect: [0, 0, 0, 0],
                                allowSelect: false,
                                minSize: [20, 85]
                            }, function () {

                                var jcrop_api = this;

                                $(".jcrop-box").attr('type', 'button');

                                $('.banner_fundo_preview').Jcrop('animateTo',[0,0,4000,4000]);

                                jcrop_api.ui.selection.element.on('cropmove',function(e,s,c){
                                    $('#banner_fundo_cropx').val(c.x);
                                    $('#banner_fundo_cropy').val(c.y);
                                    $('#banner_fundo_cropw').val(c.w);
                                    $('#banner_fundo_croph').val(c.h);
                                    $('#banner_fundo_cropwidth').val($('.jcrop-box').width());
                                    $('#banner_fundo_cropheight').val($('.jcrop-box').height());
                                });
                            });
                        }
                        
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                
                $("#banner_fundo_upload").change(function(){
                    readURL(this);
                });
            });
        </script>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('banner_fund') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->input('banner_fund', [
                    'id'    => 'banner_fundo_upload',
                    'type'  => 'file',
                    'label' => false,
                    'class' => 'form-control validate[optional]'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('banner_fundo_preview', 'Prévia do Banner de fundo') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Html->image($subcategoria->banner_fundo, [
                    'id'    => 'banner_fundo_preview',
                    'class' => 'banner_fundo_preview',
                    'alt'   => 'Image Upload',
                    'style' => 'max-width: 500px;'
                ])?>
            </div>
        </div>

        <input type="hidden" name="banner_fundo_cropx" id="banner_fundo_cropx" value="0" />
        <input type="hidden" name="banner_fundo_cropy" id="banner_fundo_cropy" value="0" />
        <input type="hidden" name="banner_fundo_cropw" id="banner_fundo_cropw" value="0" />
        <input type="hidden" name="banner_fundo_croph" id="banner_fundo_croph" value="0" />
        <input type="hidden" name="banner_fundo_cropwidth" id="banner_fundo_cropwidth" value="0" />
        <input type="hidden" name="banner_fundo_cropheight" id="banner_fundo_cropheight" value="0" />

        <div class="form-group text-center">
            <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrr btn-default', 'style' =>  'width: 9.5em;', 'escape' => false]); ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.objetivos-busca').on('change', function(){
            $('.loading').show(1);
            var objetivo_id = $(this).val();
            $.get(WEBROOT_URL + '/admin/subcategorias/busca_categorias/' + objetivo_id,
                function(data){
                    $('.objetivos-resultado').html(data);
                    $('.loading').hide(1);

                    return false;
                });
        });
    });
</script>
