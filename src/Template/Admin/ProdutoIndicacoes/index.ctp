<?php $this->assign('title', 'Novo Indicações de Produtos'); ?>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
</div>

<div class="preOrders index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('email') ?></th>
            <th><?= $this->Paginator->sort('url') ?></th>
            <th><?= $this->Paginator->sort('aceitou') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($indic_prod as $ip): ?>
            <tr>
                <td><?= $this->Number->format($ip->id) ?></td>
                <td>
                    <?= $ip->has('name') ? $ip->name : '' ?>
                </td>
                <td>
                    <?= $ip->has('email') ? $ip->email : '' ?>
                </td>
                <td>
                    <?= $ip->has('url') ? $ip->url : '' ?>
                </td>
                <td>
                    <?= $ip->has('aceitou') ? $ip->aceitou : '' ?>
                </td>
                <td><?= h($ip->created) ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>

<br>
<br>

<?= 'Quantidade de indicações: '.$qtd_prod.'<br><br>' ?>
<?= 'Quantidade de indicações aceitas: '.$qtd_aceitos ?>