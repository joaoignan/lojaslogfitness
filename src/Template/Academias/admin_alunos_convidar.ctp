<?= $this->Html->script('jquery.mask.min.js')?>
<script type="text/javascript">$(".telephone-mask").mask("(00) 00000-0009");$(".cpf-mask").mask("000.000.000-00");</script>

<section class="content">
  <legend><?= __('Alunos') ?></legend>
	<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Convidar aluno com CPF</h3>
      <span class="pull-right"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
                  '/academia/admin/alunos',
                  ['escape' =>  false,
                     'class'  =>  'pull-right']) ?></span>
      <h5>*Alunos que ainda não tem cadastro na LOG ganham um cupom de 10% de desconto para a primeira compra!</h5>
    </div>
    <div class="box-body">
      <?= $this->Form->create(null, ['url' => ['controller' => 'Academias', 'action' => 'admin_alunos_convidar_cpf'], 'class' => 'form-horizontal form-convite-cpf'])?>
  			<div class="form-group">
					<label for="name" class="col-sm-4 col-md-2 control-label">Nome</label>
					<div class="col-sm-6">
						<?= $this->Form->input('name', [
                'div'           => false,
                'label'         => false,
                'class'         => 'form-control validate[required]',
                'placeholder'   => 'Nome Completo',
                'id'			=> 'nome',
                'required'		=> true
            ])?>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-sm-4 col-md-2 control-label">Email</label>
					<div class="col-sm-6">
						<?= $this->Form->input('email', [
                'div'           => false,
                'label'         => false,
                'class'         => 'form-control validate[required]',
                'placeholder'   => 'E-mail',
                'id'			=> 'nome',
                'required'		=> true
            ])?>
					</div>
				</div>
				<div class="form-group">
					<label for="telefone1" class="col-sm-4 col-md-2 control-label">Telefone</label>
					<div class="col-sm-6">
						<?= $this->Form->input('telephone', [
                'div'           => false,
                'label'         => false,
                'class'         => 'form-control validate[optional] telephone-mask',
                'placeholder'   => 'Telefone',
            ])?>
					</div>
				</div>
        <div class="form-group">
          <label for="cpf" class="col-sm-4 col-md-2 control-label">CPF</label>
          <div class="col-sm-6">
            <?= $this->Form->input('cpf', [
                'div'           => false,
                'label'         => false,
                'class'         => 'form-control validate[required] cpf-mask cpf-verify',
                'placeholder'   => 'CPF',
            ])?>
          </div>
        </div>
  			<div class="box-footer">
  				<?= $this->Form->button('<span> '.__('Convidar').'</span> <i class="glyph-icon icon-arrow-right"></i>'.
              '',
              [
              'class'     => 'btn btn-success',
              'escape'    => false
              ]);
          ?>
  			</div>
      <?= $this->Form->end() ?>
    </div>
  </div>

  <script type="text/javascript">
    $(document).on('submit', '.form-convite-cpf', function() {
      var cpf = $('.cpf-verify').val();

      function apenasNumeros(string) 
      {
          var numsStr = string.replace(/[^0-9]/g,'');
          return numsStr;
      }

      function TestaCPF(cpf) {
          var Soma;
          var Resto;
          var cpf = apenasNumeros(cpf);
          Soma = 0;
          if (cpf == "00000000000") return false;

          for (i=1; i<=9; i++) Soma = Soma + parseInt(cpf.substring(i-1, i)) * (11 - i);
          Resto = (Soma * 10) % 11;

          if ((Resto == 10) || (Resto == 11))  Resto = 0;
          if (Resto != parseInt(cpf.substring(9, 10)) ) return false;

          Soma = 0;
          for (i = 1; i <= 10; i++) Soma = Soma + parseInt(cpf.substring(i-1, i)) * (12 - i);
          Resto = (Soma * 10) % 11;

          if ((Resto == 10) || (Resto == 11))  Resto = 0;
          if (Resto != parseInt(cpf.substring(10, 11) ) ) return false;
          return true;
      }

      if(!TestaCPF(cpf)) {
        return false;
      }
    });
  </script>

  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Convidar aluno sem CPF</h3>
      <span class="pull-right"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
                  '/academia/admin/alunos',
                  ['escape' =>  false,
                     'class'  =>  'pull-right']) ?></span>
    </div>
    <div class="box-body">
      <?= $this->Form->create(null, ['class' => 'form-horizontal '])?>
        <div class="form-group">
          <label for="name" class="col-sm-4 col-md-2 control-label">Nome</label>
          <div class="col-sm-6">
            <?= $this->Form->input('name', [
                'div'           => false,
                'label'         => false,
                'class'         => 'form-control validate[required]',
                'placeholder'   => 'Nome Completo',
                'id'      => 'nome',
                'required'    => true
            ])?>
          </div>
        </div>
        <div class="form-group">
          <label for="email" class="col-sm-4 col-md-2 control-label">Email</label>
          <div class="col-sm-6">
            <?= $this->Form->input('email', [
                'div'           => false,
                'label'         => false,
                'class'         => 'form-control validate[required]',
                'placeholder'   => 'E-mail',
                'id'      => 'nome',
                'required'    => true
            ])?>
          </div>
        </div>
        <div class="form-group">
          <label for="telefone1" class="col-sm-4 col-md-2 control-label">Telefone</label>
          <div class="col-sm-6">
            <?= $this->Form->input('telephone', [
                'div'           => false,
                'label'         => false,
                'class'         => 'form-control validate[optional] telephone-mask',
                'placeholder'   => 'Telefone',
            ])?>
          </div>
        </div>
        <div class="box-footer">
          <?= $this->Form->button('<span> '.__('Convidar').'</span> <i class="glyph-icon icon-arrow-right"></i>'.
              '',
              [
              'class'     => 'btn btn-success',
              'escape'    => false
              ]);
          ?>
        </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
	
	<div class="box box-info">
    <div class="box-header with-border" style="padding-bottom: 0px;">
      <h3 class="box-title">Importar lista de alunos (CSV)</h3>
      <p class="help-block">Com essa ferramenta você consegue convidar vários alunos de uma única vez. Caso tenha dúvida entre em contato pelo nosso chat!</p>
    </div>
    <?= $this->Form->create(null, ['class' => 'form', 'type' => 'file'])?>
      <div class="box-body">
        <div class="form-group">
          <label for="csv">Inserir arquivo</label>
          <?= $this->Form->input('csv', [
                'type' => 'file',
                'label' => false,
                'required' => true
            ]) ?>
          <p class="help-block">Selecione um arquivo CSV com padrão de dados separados por vírgulas ",".</p>
          <p><strong>Baixe aqui uma cópia do modelo. Não retire os nomes das colunas e coloque apenas os números (com DDD) no telefone.</strong></p>
          <p><?= $this->Html->link('<i class="fa fa-download" aria-hidden="true"></i> Download',
                                WEBROOT_URL.'files/LogFitness_Exemplo_Aluno.csv',
                                ['class' => 'btn btn-info btn-sm', 'escape' => false])?></p>
        </div>   
      </div>

      <div class="box-footer">
        <?= $this->Form->button('<span> '.__('Enviar').'</span> <i class="glyph-icon icon-arrow-right"></i>'.
          '',
          [
          'class'     => 'btn btn-success',
          'escape'    => false
          ]);
        ?>
      </div>
    <?= $this->Form->end() ?>
  </div>
</section>