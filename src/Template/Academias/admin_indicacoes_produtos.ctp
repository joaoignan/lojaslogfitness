<section class="content">
<legend><?= __('Indicações de Produtos') ?></legend>

<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_list(__('Listar Alunos'),'/academia/admin/alunos'); ?>
    </div>
</div>

<div class="clientes index">
    <table class="table table-hover">

        <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('professor_id') ?></th>
            <th><?= $this->Paginator->sort('cliente_id', 'Aluno') ?></th>
            <th><?= $this->Paginator->sort('pedido_id') ?></th>
            <th><?= $this->Paginator->sort('created', 'Enviada') ?></th>
            <th><?= $this->Paginator->sort('modified', 'Aceita') ?></th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($pre_orders as $pre_order): ?>
            <tr>
                <td><?= $this->Number->format($pre_order->id) ?></td>
                <td><?= h($pre_order->professore->name) ?></td>
                <td><?= h($pre_order->cliente->name) ?></td>
                <td><?= $pre_order->pedido_id >= 1 ? $pre_order->pedido_id : '-' ?></td>
                <td><?= h($pre_order->created) ?></td>
                <td>
                    <?php
                    $badge = '';
                    if(
                        $pre_order->pedido_id   >= 1 &&
                        $pre_order->modified    != $pre_order->created &&
                        date_format($pre_order->modified, 'Y-m-d') >= date('Y-m-d', strtotime('- 7 days'))
                    ){
                        $badge = '<span class="bs-badge badge-blue-alt">NOVO</span>';
                    }
                    echo $pre_order->modified != $pre_order->created ? h($pre_order->modified).' '.$badge : '-'?>
                </td>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                            <i class="glyph-icon icon-navicon"></i>
                            <span class="sr-only"><?= __('Actions'); ?></span>
                        </button>
                        <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                            <li>
                                <?= $this->Html->link('<i class="glyph-icon icon-search"></i>  '.__('Ver detalhes'),
                                    '#',
                                    [
                                        'data-target'=> '#dados-'.$pre_order->id,
                                        'data-toggle'=> 'modal',
                                        'escape' => false
                                    ])
                                ?>
                            </li>
                        </ul>
                    </div>

                    <div class="modal fade"
                         id="dados-<?= $pre_order->id ?>"
                         tabindex="-1" role="dialog"
                         aria-labelledby="dados-<?= $pre_order->id ?>Label"
                         aria-hidden="true">

                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">
                                        Detalhes da Indicação <strong>#<?= $pre_order->id ?></strong>
                                        <br>
                                        Aluno: <strong><?= $pre_order->cliente->name ?></strong>
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th class="text-center">Item</th>
                                            <th class="text-center">Cód.</th>
                                            <th>Descrição</th>
                                            <th class="text-right">Quantidade</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($pre_order->pre_order_items as $item):
                                            ?>
                                            <tr>
                                                <td class="text-center"><?= $i ?></td>
                                                <td class="text-center"><?= $item->produto_id ?></td>
                                                <td><?= $item->produto->produto_base->name ?></td>
                                                <td class="text-right"><?= $item->quantidade ?></td>
                                            </tr>
                                            <?php
                                            $i++;
                                        endforeach;
                                        ?>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="col-md-12">
                                    <h5>
                                        <?= $pre_order->pedido_id >= 1
                                            ? 'Indicação Aceita :: <strong>Pedido #'.$pre_order->pedido_id.'</strong>'
                                            : 'Indicação ainda não aceita'
                                        ?>
                                    </h5>
                                </div>


                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
</section>

