<style>
    @media all and (max-width: 450px){
    .no-mobile{
      display: none;
    }
    .font-header{
        font-size: 12px
    }
    .font-body{
        font-size: 11px
    }
  }
  @media all and (max-width: 1023px){
    .no-small{
      display: none;
    }
  }
</style>
<section class="content">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Time</h3>
            </div>
            <div class="clientes index">
                <div class="">
                    <div class="col-sm-6">
                        <?= $this->Html->link('<i class="fa fa-user"></i>  '.('Novo membro'),
                          ['controller' => 'Academias', 'action' => 'admin_professores_convidar'],
                          [
                            'class' => 'btn btn-success',
                            'escape' => false
                          ])
                          ?>
                    </div>
                </div>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="font-header"><?= $this->Paginator->sort('name') ?></th>
                            <th class="no-small"><?= $this->Paginator->sort('city') ?></th>
                            <th class="font-header"><?= $this->Paginator->sort('telephone') ?></th>
                            <th class="no-small"><?= $this->Paginator->sort('mobile') ?></th>
                            <th class="no-small"><?= $this->Paginator->sort('email') ?></th>
                            <th class="actions"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($prof_academia  as $professores): ;?>
                        <tr>
                            <td class="font-body"><?= h($professores->professore->name) ?></td>
                            <td class="no-small"><?= h($professores->professore->city->name.'/'.$professores->professore->city->uf) ?></td>
                            <td class="font-body"><?= h($professores->professore->phone) ?></td>
                            <td class="no-small"><?= h($professores->professore->mobile) ?></td>
                            <td class="no-small"><?= h($professores->professore->email) ?></td>
                            <td>

                                <div class="btn-group" >
                                    <?php
                                    $aceite = '';
                                    if($professores->status_id == 3 ){
                                        $aceite = 'style="background-color: #ea9143"';?>
                                    <?php }?>
                                    <button class="btn btn-info dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false" <?= $aceite?>>
                                        <i class="fa fa-bars"></i>
                                        <span class="sr-only"><?=__('Actions');?></span>
                                    </button>

                                    <ul class="dropdown-menu  pull-right" role="menu" style="left: -110px;">
                                        <li>
                                            <?php if($professores->status_id == 3){ ?>
                                                <?= $this->Form->postLink('<i class="glyph-icon icon-check-square-o"></i>  '.__('Aceitar'),
                                                    ['controller' => 'Academias', 'action' => 'admin_add_professores', $professores->professore->id],
                                                    ['confirm' => __('Tem certeza que deseja aceitar o(a) Professor(a) {0}?', $professores->professore->name), 'escape' => false])?>
                                            <?php }?>
                                        </li>
                                        <li>
                                            <?= $this->Form->postLink('<i class="glyph-icon icon-power-off"></i>  '.__('Desligar'),
                                                ['controller' => 'Academias', 'action' => 'admin_edit_professores', $professores->professore->id],
                                                ['confirm' => __('Tem certeza que deseja desligar o(a) Professor(a) {0}?', $professores->professore->name), 'escape' => false]) ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="glyph-icon icon-search-plus"></i>  '.__('View'),
                                                ['controller' => 'Academias', 'action' => 'admin_professores_view', $professores->professore->id],
                                                ['escape' => false]) ?>
                                        </li>
                                    </ul>
                                </div>

                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="paginator"  style="margin-left: 15px;">
                    <ul class="pagination">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                    <p style="margin-left: 15px;"><?= $this->Paginator->counter() ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
