<?= $this->Html->css('charts/examples') ?>
<?= $this->Html->script('charts/jquery.flot') ?>
<?= $this->Html->script('charts/jquery.flot.categories') ?>

<style>
    .btn-filtro{
        margin-top: 15px;
    }
</style>


<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);

$anos = [];
for($i = 2016; date('Y') >= $i; $i++){
    $anos[$i] = $i;
}

$meses = [];
for($i = 1; $i <= 12; $i++){
    $meses[str_pad($i, 2, '0', STR_PAD_LEFT)] = str_pad($i, 2, '0', STR_PAD_LEFT);
}
?>

<section class="content">
    <div id="content" class="col-sm-12 col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-bar-chart-o"></i>
                <h3 class="box-title">Gerar Relatório</h3>
            </div>
            <div class="box-body">
                <div class="col-xs-12">
                    <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row']); ?>

                <div class="col-sm-6 col-md-2 control-label">
                    <?= $this->Form->label('mes', 'Mês') ?>
                </div>
                <div class="col-sm-6 col-md-4">
                    <?= $this->Form->input('mes', [
                        'options' => $meses,
                        'value' => $mes > 0 ? $mes : '',
                        'empty' => 'Mês',
                        'label' => false,
                        'class' => 'form-control chosen-select validate[required]'
                    ]) ?>
                </div>

                <div class="col-sm-6 col-md-2 control-label">
                    <?= $this->Form->label('ano') ?>
                </div>
                <div class="col-sm-6 col-md-4">
                    <?= $this->Form->input('ano', [
                        'options' => $anos,
                        'value' => $ano > 0 ? $ano : '',
                        'empty' => 'Ano',
                        'label' => false,
                        'class' => 'form-control chosen-select validate[required]'
                    ]) ?>
                </div>

                <div class="col-sm-12 text-center btn-filtro">
                    <?= $this->Form->button('<i class="fa fa-line-chart"></i> <span>Gerar</span>',
                        ['class' => 'btn btn-alt bg-blue',
                            'escape' => false
                        ]); ?>
                </div>
                <?= $this->Form->end(); ?>
                </div>
            </div>  
        </div>
    </div>

    <div id="content" class="col-sm-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-bar-chart-o"></i>
                <h3 class="box-title">Gráfico Valor x Professor</h3>
            </div>
            <div class="box-body">
                <div class="col-xs-12 no-padding">
                    <div class="demo-container">
                        <div id="placeholder" class="demo-placeholder"></div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</section>

<div class="col-sm-12" style="padding-top: 30px">
    <?php if(!$select_date) {

        if(count($prof_academia_relatorio) != 0) {
            $qtd_prof_relatorio = count($professores_academia_id);
            $id_prof[] = '';
            $valor_prof[] = '';

            foreach ($prof_academia_relatorio as $par) {
                for($i = 0; $i < $qtd_prof_relatorio; $i++) {
                    if($par->professor_id == $professores_academia_id[$i]) {
                        $id_prof[$i] = $par->professor_id;
                        $valor_prof[$i] = $valor_prof[$i] + $par->valor;
                    } else {
                        $id_prof[$i] = $professores_academia_id[$i];
                        $valor_prof[$i] = $valor_prof[$i] + 0;
                    }
                }
            } 

            for($i = 0; $i < $qtd_prof_relatorio; $i++) {
                foreach($prof_relatorio_name as $prn) {
                    if($id_prof[$i] == $prn->id) {
                        if($valor_prof[$i] >= 1) {
                            $name = explode(' ', $prn->name);
                            $prof_vendeu[] = [
                                $name[0],
                                number_format($valor_prof[$i], 2, '.', '.')
                            ]; 
                        } else {
                            $name = explode(' ', $prn->name);
                            $prof_vendeu[] = [
                                $name[0],
                                number_format(0, 2, '.', '.')
                            ];
                        }
                    }
                }
            }
        } else {
            foreach($prof_relatorio_name as $prn) {
                $name = explode(' ', $prn->name);
                $prof_vendeu[] = [
                    $name[0],
                    number_format(0, 2, '.', '.')
                ];
            }
        }

    } ?>

    <script type="text/javascript">
        $(function() {
            var data = [ <?php foreach ($prof_vendeu as $pv) { ?>["<?= $pv[0] ?>", <?= $pv[1] ?>], <?php } ?> ];
            $.plot("#placeholder", [ data ], {
                series: {
                    bars: {
                        show: true,
                        barWidth: 0.7,
                        align: "centered",
                        lineWidth: 0,
                        fill: true,
                        fillColor: "#5087C7"
                    }
                },
                xaxis: {
                    mode: "categories",
                    tickLength: 0
                },
                yaxis: {
                    min: 0,
                },
                grid: {
                    hoverable: true
                }
            });

            $("<div id='tooltip'></div>").css({
                position: "absolute",
                display: "none",
                border: "1px solid #fdd",
                padding: "2px",
                "background-color": "#fee",
                opacity: 0.80
            }).appendTo("body");

            $("#placeholder").bind("plothover", function (event, pos, item) {
                if (item) {
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                    $("#tooltip").html('R$'+y.replace('.', ','))
                        .css({top: item.pageY+5, left: item.pageX+5})
                        .fadeIn(200);
                } else {
                    $("#tooltip").hide();
                }
            });

            $("#footer").prepend("Flot " + $.plot.version + " &ndash; ");
        });
    </script>
</div>