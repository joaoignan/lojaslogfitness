<style>
  .table-personalizada table{
    border: 2px solid rgba(80, 137, 207, 1.0);
  }
  .table-personalizada table th{
    padding: 10px;
    border-bottom: 2px solid rgba(80, 137, 207, 1.0);
    background-color: rgba(80, 137, 207, 0.2);
    border-collapse: collapse;
  }
  .table-personalizada table td{
    padding: 10px;
    border-bottom: 2px solid rgba(80, 137, 207, 1.0);
    border-collapse: collapse;
  }
</style>


<section class="content">
  <div class="col-xs-12">
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Extrato Bancário</h3>
        <div class="box-tools">
        </div>
        <hr>
      </div>
      <div class="box-body">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#extrato" data-toggle="tab">Extrato</a></li>
            <li><a href="#saques" data-toggle="tab">Saques</a></li>
            <li><a href="#boletos" data-toggle="tab">Boletos</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active table-personalizada" id="extrato">
              <div class="tabela" style="width: 100%; overflow-x: auto;">
                <table width="600" align="center">
                  <tr>
                    <th>Aluno</th>
                    <th>Plano</th>
                    <th>Valor</th>
                    <th>Status</th>
                    <th>Data de compra</th>
                  </tr>
                  <?php foreach($pedidos_extrato as $pedido_extrato) { ?>
                  <?php $is_plan = 0; ?>
                    <tr>
                      <td><?= $pedido_extrato->cliente->name ?></td>
                      <?php foreach ($planos_clientes as $plano_cliente) { ?>
                        <?php foreach ($academia_mensalidades_planos as $amp) { ?>
                          <?php if($plano_cliente->id == $pedido_extrato->plano_cliente_id) { ?>
                            <?php $is_plan = 1; ?>
                            <?php if($plano_cliente->academia_mensalidades_planos_id == $amp->id) { ?>
                              <td><?= $amp->name ?></td>
                            <?php } ?>
                          <?php } ?>
                        <?php } ?>
                      <?php } ?>
                      <?php if($is_plan == 0) { ?>
                        <td>Plano Personalizado</td>
                      <?php } ?>
                      <td>R$ <?= number_format($pedido_extrato->valor, 2, ',', '.') ?></td>
                      <?php if($pedido_extrato->pedido_status_id >= 3 && $pedido_extrato->pedido_status_id < 7) { ?>
                        <td>Pago</td>
                      <?php } else { ?>
                        <td><?=  $pedido_extrato->pedido_status->name ?></td>
                      <?php } ?>
                      <td><?= $pedido_extrato->data_pagamento ? $pedido_extrato->data_pagamento->format('d/m/Y') : '' ?></td>
                    </tr>
                  <?php } ?>
                </table>
              </div>
            </div>
            <div class="tab-pane table-personalizada" id="saques">
              <div class="tabela" style="width: 100%; overflow-x: auto;">
                <table width="300" align="center">
                  <tr>
                    <th>Valor</th>
                    <th class="text-center">Data</th>
                  </tr>
                  <?php foreach($saques as $saque) { ?>
                    <tr>
                      <td>R$ <?= number_format($saque->valor, 2, ',', '.') ?></td>
                      <td class="text-center"><?= $saque->created->format('d/m/Y') ?></td>
                    </tr>
                  <?php } ?>
                </table>
              </div>
            </div>
            <div class="tab-pane table-personalizada" id="boletos">
              <div class="tabela" style="width: 100%; overflow-x: auto;">
                <table width="600" align="center">
                  <tr>
                    <th>Aluno</th>
                    <th>Valor</th>
                    <th>Data de compra</th>
                    <th>Boleto</th>
                    <th>Reenviar por email</th>
                  </tr>
                  <?php foreach($pedidos_aguardando as $pedido_aguardando) { ?>
                    <tr>
                      <td><?= $pedido_aguardando->cliente->name ?></td>
                      <td>R$ <?= number_format($pedido_aguardando->valor, 2, ',', '.') ?></td>
                      <td><?= $pedido_aguardando->modified->format('d/m/Y') ?></td>
                      <td>
                        <?= $this->Html->link('ver boleto', 
                        $pedido_aguardando->iugu_payment_pdf,
                        ['class' => 'btn bg-blue btn-sm', 'target' => '_blank']); ?>
                      </td>
                      <td>
                        <?= $this->Html->link('enviar email', 
                        '/academia/admin/admin_mensalidades/planos/enviar-boleto/'.$pedido_aguardando->id,
                        ['class' => 'btn bg-blue btn-sm']); ?>
                      </td>
                    </tr>
                  <?php } ?>
                </table>
              </div>
            </div>
          </div>
        </div>  
      </div>
    </div>
  </div>
</section>