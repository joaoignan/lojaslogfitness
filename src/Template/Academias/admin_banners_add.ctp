<style>
 @media all and (max-width: 450px){
    .no-mobile{
      display: none;
    }
  }
  @media all and (max-width: 1023px){
    .no-small{
      display: none;
    }
  }
</style>

<?php
$myTemplates = [
    'inputContainer' => '{{content}}',
];
$this->Form->templates($myTemplates);
?>
<section class="content">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-body">
                <div class="banners form">
                <?= $this->Form->create($banner, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
                <fieldset>
                    <h3 class="box-header">
                    Adicionar Banner
                    </h3>
                    <div class="form-group">
                        <div class="col-sm-3 control-label">
                            <?= $this->Form->label('title') ?>
                        </div>
                        <div class="col-sm-8">
                            <?= $this->Form->input('title', [
                                            'label' => false,
                                            'class' => 'form-control validate[required, minSize[3], maxSize[255]]']) ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3 control-label">
                            <?= $this->Form->label('link') ?>
                        </div>
                        <div class="col-sm-8">
                            <?= $this->Form->input('link', [
                                            'label' => false,
                                            'class' => 'form-control validate[optional,custom[url]]',
                                            'placeholder' => 'Link de ação para o banner (opcional)']) ?>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $(window).load(function() {
                            function readURL(input) {
                                if (input.files && input.files[0]) {
                                    var reader = new FileReader();
                                    
                                    reader.onload = function (e) {
                                        $('.image_preview').attr('src', e.target.result);

                                        $('.image_preview').Jcrop({
                                            boxHeight: 1000,
                                            boxWidth: 1000,
                                            setSelect: [0, 0, 0, 0],
                                            allowSelect: false,
                                            minSize: [400, 400],
                                            aspectRatio: 1
                                        }, function () {

                                            var jcrop_api = this;

                                            $(".jcrop-box").attr('type', 'button');

                                            $('.image_preview').Jcrop('animateTo',[0,0,4000,4000]);

                                            jcrop_api.ui.selection.element.on('cropmove',function(e,s,c){
                                                $('#cropx').val(c.x);
                                                $('#cropy').val(c.y);
                                                $('#cropw').val(c.w);
                                                $('#croph').val(c.h);
                                                $('#cropwidth').val($('.jcrop-box').width());
                                                $('#cropheight').val($('.jcrop-box').height());
                                            });
                                        });
                                    }
                                    
                                    reader.readAsDataURL(input.files[0]);
                                }
                            }
                            
                            $("#image_upload").change(function(){
                                readURL(this);
                            });
                        });
                    </script>

                    <div class="form-group">
                        <div class="col-sm-3 control-label">
                            <?= $this->Form->label('imagem', 'Logotipo') ?>
                        </div>
                        <div class="col-sm-8">
                            <?= $this->Form->input('imagem', [
                                'id'    => 'image_upload',
                                'type'  => 'file',
                                'label' => false,
                                'class' => 'form-control validate[required, custom[validateMIME[image/jpeg|image/png]]]'
                            ]) ?>
                            <p><strong>Selecione um arquivo de imagem entre 400x400 e 4000x4000 pixels</strong></p>
                        </div>
                    </div>
                
                    <div class="form-group">
                        <div class="col-sm-4 col-md-3 control-label">
                            <?= $this->Form->label('image_preview', 'Prévia do Banner') ?>
                        </div>
                        <div class="col-sm-12 text-center">
                            <?= $this->Html->image('default.png', [
                                'class'    => 'image_preview',
                                'alt'   => 'Image Upload'
                            ])?>
                        </div>
                    </div>

                    <input type="hidden" name="cropx" id="cropx" value="0" />
                    <input type="hidden" name="cropy" id="cropy" value="0" />
                    <input type="hidden" name="cropw" id="cropw" value="0" />
                    <input type="hidden" name="croph" id="croph" value="0" />
                    <input type="hidden" name="cropwidth" id="cropwidth" value="0" />
                    <input type="hidden" name="cropheight" id="cropheight" value="0" />

                    <div class="text-center">
                        <div class="col-xs-6 col-sm-offset-3 col-sm-3">
                            <?= $this->Form->button('Salvar',
                            ['class' => 'btn btn-success', 'escape' => false]); ?>
                        </div>    
                    <?= $this->Form->end() ?>
                        <div class="col-xs-6 col-sm-3">
                            <?= $this->Html->link('Voltar','/academia/admin/banners/',
                            ['class' => 'btn bg-red', 'escape' => false]) ?>
                        </div>
                    </div>
                </fieldset>  
                </div>
            </div>
        </div>
    </div>
</section>

