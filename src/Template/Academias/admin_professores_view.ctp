<section class="content">
    <div class="row">
        <div class="col-xs-12">
          <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <div class="row"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
                                        ['controller' => 'Academias', 'action' => 'admin_professores'],
                                        ['escape'   =>  false,
                                         'class'    =>  'btn pull-right']) ?>
                    </div>
                    <?= $this->Html->image('professores/'.$prof_academia->professore->image, ['alt' => $prof_academia->professore->name, 'class' => 'profile-user-img img-responsive img-circle']) ?>
                    <h3 class="profile-username text-center"><?= h($prof_academia->professore->name) ?></h3>
                    <p class="text-muted text-center">Código: <?= $this->Number->format($prof_academia->professore->id) ?></p>
                    <p class="text-muted text-center">Último acesso: 
                    <?php 
                        if (h($prof_academia->professore->last_access) == null){
                            echo 'Não acessou ainda';
                        }
                        else{
                            echo h($prof_academia->professore->last_access);
                        }

                    ?>

                     </p>
                    <div class="col-md-6">
                        <p class="text-center"><i class="fa fa-user" aria-hidden="true"></i> Pessoal</p>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Nome: </b><?= h($prof_academia->professore->name) ?>
                            </li>
                            <li class="list-group-item">
                                <b>E-mail: </b><?= h($prof_academia->professore->email) ?>
                            </li>
                            <li class="list-group-item">
                                <b>Celular: </b><?= h($prof_academia->professore->mobile) ?>
                            </li>
                            <li class="list-group-item">
                                <b>Telefone: </b><?= h($prof_academia->professore->phone) ?>
                            </li>
                            <li class="list-group-item">
                                <b>RG: </b><?= h($prof_academia->professore->rg) ?>
                            </li>
                            <li class="list-group-item">
                                <b>CPF: </b><?= h($prof_academia->professore->cpf) ?>
                            </li>
                            <li class="list-group-item">
                                <b>CREF: </b><?= h($prof_academia->professore->cref) ?>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <p class="text-center"><i class="fa fa-map-marker" aria-hidden="true"></i> Moradia</p>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>CEP: </b><?= h($prof_academia->professore->cep) ?>
                            </li>
                            <li class="list-group-item">
                                <b>Endereço: </b><?= h($prof_academia->professore->address) ?>
                            </li>
                            <li class="list-group-item">
                                <b>Número: </b><?= h($prof_academia->professore->number) ?>
                            </li>
                            <li class="list-group-item">
                                <b>Complemento: </b><?= h($prof_academia->professore->complement) ?>
                            </li>
                            <li class="list-group-item">
                                <b>Bairro: </b><?= h($prof_academia->professore->area) ?>
                            </li>
                            <li class="list-group-item">
                                <b>Cidade: </b><?= $prof_academia->professore->city->name ?>
                            </li>
                            <li class="list-group-item">
                                <b>Estado: </b><?= $prof_academia->professore->city->uf ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
