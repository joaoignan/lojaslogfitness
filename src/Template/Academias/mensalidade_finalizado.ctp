<style type="text/css">
    #btn-mensalidade{
        display: none;
    }
    .footer-total{
        display: none;
    }
</style>

<div class="container text-center boletos">
    <p>Boletos gerados! Clique no botão para vizualizar e/ou imprimir</p>
    <?php $contador = 1; ?>
    <?php foreach ($pedidos as $pedido) {
        echo '<div class="col-xs-6 col-sm-4 col-md-2 text-center boleto">';
        echo $this->Html->link('<button class="btn btn-md"><i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;&nbsp;'.$contador.'º boleto</button>',
            $pedido->iugu_payment_url,
            ['target' => '_blank', 'escape' => false, 'class' => 'btn btn-cupom font-bold']
        );
        echo '</div>';
        $contador++;
    } ?>
</div>

