<?php
    /*function reduzir_tamanho_img($src, $quality) {
        $img = new Imagick();
        $img->readImage($src);
        $img = $img->flattenImages();
        $img->setImageCompression(Imagick::COMPRESSION_JPEG);
        $img->setImageCompressionQuality($quality);
        $img->stripImage();
        $img->writeImage($src); 
    }

    foreach($produtos as $prod_img) {
        $foto_prod = unserialize($prod_img->fotos);
        reduzir_tamanho_img('img/produtos/sm-'.$foto_prod[0], 40);
    }

    foreach($banners as $banners_img) {
        reduzir_tamanho_img('img/banners/'.$banners_img->image, 70);
    }

    foreach($banners_academia as $banners_acad_img) {
        reduzir_tamanho_img('img/banners/'.$banners_acad_img->image, 70);
    }

    foreach($banners_cliente as $banners_clientes_img) {
        reduzir_tamanho_img('img/banners/'.$banners_clientes_img->image, 70);
    }*/
?>

<style>
    .overlay-avise {
        height: 0%;
        width: 100%;
        position: fixed;
        z-index: 999999999;
        top: 0;
        left: 0;
        background-color: rgb(0,0,0);
        background-color: rgba(0,0,0, 0.9);
        overflow-y: hidden;
        transition: 0.5s;
    }

    .overlay-avise-content {
        position: relative;
        top: 15%;
        width: 40%;
        text-align: center;
        margin-top: 30px;
        left: 30%;
        background-color: white;
        border-radius: 20px;
        padding: 15px;
    }
    .overlay-avise-content h4{
        color: #5089cf;
        font-family: nexa_boldregular
    }
    .overlay-avise a {
        padding: 8px;
        text-decoration: none;
        font-size: 36px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }

    .overlay-avise a:hover, .-aviseoverlay a:focus {
        color: #f1f1f1;
    }

    .overlay-avise .closebtn {
        position: absolute;
        top: 20px;
        right: 45px;
        font-size: 60px;
    }
    @media screen and (max-height: 450px) {
        .overlay-avise {overflow-y: auto;}
        .overlay-avise a {font-size: 20px}
        .overlay-avise .closebtn {
            font-size: 40px;
            top: 15px;
            right: 35px;
      }
    }
    .valor-box {
        text-align: center;
        padding-left: 15px;
        padding-right: 15px;
    }

    .promocao-index, #grade-produtos {
        padding-right: 230px;
        padding-left: 230px;
        margin-left: 0;
        margin-right: 0;
        width: 100%;
    }
    .product {
        font-size: 14px;
    }
    .produto-item .bg-white-produto {
        min-height: 314px;
        border: 2px solid rgb(234, 234, 234);
        box-shadow: 0px 1px 7px 0px rgba(200,200,200,0.75);
    }
    .bg-gray-produto {
        bottom: 0;
        position: absolute;
        min-height: 92px;
        max-width: 185px;
    }
    .description {
        height: 40px;
        width: 100%;
        overflow: hidden;
        text-overflow: ellipsis;
        max-height: 60px;
        margin: 0;
        font-size: 14px;
        text-transform: uppercase;
        font-weight: 700;
    }
    @media all and (max-width: 768px) {
        .produto-item .bg-white-produto {
            min-height: 310px;
        }
    }
    @media all and (min-width: 769px) {
        .box-close-filter { 
            width: 0; 
        }
        .filtro { 
            width: 230px; 
            opacity: 1; 
        }
        .box-filtro { 
            left: 0; 
        }
    }
    .produto-grade {
        max-width: 145px;
        max-height: 150px;
        margin-top: 0;
        display: flex;
        align-items: center;
    }
    .box-imagem-produto {
        width: 100%;
        height: 150px;
        margin-top: 10px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .box-info-produto {
        width: 100%;
        height: 130px;
        margin-bottom: 10px;
    }
    .box-fundo-produto {
        width: 100%;
        height: 40px;
    }
    .box-preco {
        height: 60px;
        width: 70%;
        float: left;
        display: flex;
        flex-flow: column;
        align-items: baseline;
        justify-content: center;
        padding-left: 4px;
    }
    .box-mochila-add {
        height: 45px;
        width: 45px;
        float: right;
        display: flex;
        align-items: flex-end;
        margin-top: 10px;
    }
    .box-mochila-add svg {
        width: 100%;
        height: 100%;
        cursor: pointer;
        background: #8dc73f;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
        fill: white;
        margin-right: 3px;
    }
    .box-mochila-avise {
        height: 45px;
        width: 45px;
        float: right;
        margin-top: 10px;
        background-color: #5089cf;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
    }
    .box-mochila-avise > .fa{
        color: #FFF;
        font-size: 2.5em;
        margin-bottom: 2px;
        margin-top: 3px;
    }
    .categoria{
        height: 30px;
    }
    .categoria svg{
        position: absolute;
        left: 5px;
        width: 20px;
        height: 30px;
    }
    .preco {
        margin: 0;
    }
    .preco-desc {
        font-size: 13px;
    }
    .preco-normal {
        font-weight: 700;
        font-size: 18px;
        color: #5089cf;
    }
    .bg-white-produto a {
        min-height: 0;
    }
    .box-promocao {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-size: 13px;
        color: black;
        background: #f3d012;
    }
    .box-promocao:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-novidade {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-size: 13px;
        color: white;
        background: #5089cf;
    }
    .box-novidade:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-ultimo {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-size: 13px;
        color: white;
        background: red;
    }
    .box-ultimo:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-mochila-add svg:hover, .box-mochila-add svg:focus, .box-mochila-add svg:link:hover, .box-mochila-add svg:link:focus, .box-mochila-add svg:visited:hover, .box-mochila-add svg:visited:focus, .box-mochila-avise:hover {
      -webkit-transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      -webkit-animation: diff 0.5s 1;
      animation: diff 0.5s 1;
    }

    @-webkit-keyframes diff {
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
    }
    @keyframes diff {
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
    }

    .icone-objetivo {
        position: absolute;
        left: 5px;
        bottom: 120px;
        width: 30px;
        height: 30px;
        color: #5089cf;
    }

    .box-imagem-produto svg {
        position: absolute;
        left: 5px;
        bottom: 130px;
        width: 20px;
        height: 30px;
    }

    .sabor-info {
        font-weight: bold;
        font-size: 13px;
        color: #333;
        text-align: center;
        font-family: nexa_lightregular,"Droid Sans",serif;
        overflow: hidden;
        height: 20px;
    }

    @media all and (min-width: 1650px) {
        .produto-item {
            width: 16.66666667%;
        }
    }
    @media all and (max-width: 1240px) {
        .produto-item {
            width: 33.3333333334%;
        }
    }
    @media all and (max-width: 1200px) and (min-width: 780px) {
        .form-busca {
            width: 80% !important;
        }
    }
    @media all and (max-width: 1050px) {
        .produto-item {
            width: 50%;
        }
    }
    @media all and (max-width: 850px) {
        .produto-item {
            width: 100%;
        }
    }
    @media all and (max-width: 768px) {
        .promocao-index, #grade-produtos {
            padding-left: 0;
            padding-right: 0;
        }
        .promocao-index {
            padding-top: 90px;
        }
        .produto-item {
            width: 50%;
        }
    }
    @media all and (max-width: 400px) {
        .produto-grade {
            max-width: 120px;
            max-height: 120px;
        }
        .produto-item {
            padding-right: 0px;
            padding-left: 0px;
        }
    }
</style>

<style>
    .loader {
        position: fixed;
        left: 0;
        top: 0%;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: white;
    }
    .content-loader{
        position: fixed;
        border-radius: 25px;
        left: 20%;
        top: 25%;
        width: 60%;
        height: 50%;
        z-index: 999999;
        background-color: white;
        text-align: center;
    }
    .content-loader img{
        width: 500px;
        margin-top: 45px;
    }
    .content-loader i{
        color: #f4d637;
    }
    .content-loader p{
        color: #5087c7;
        font-family: nexa_boldregular;
        font-size: 34px;
    }
    @media all and (max-width: 430px) {
        .content-loader{
            display: none;
        }
    }
    @media all and (max-width: 768px) {
        .loader {
            display: none;
        }
    }
</style>

<?php
if(isset($CLIENTE)){ 
    $client_name = explode(' ', $CLIENTE->name); ?>
    <script>
        var username = '<?= $client_name[0] ?>';
    </script>
<?php } else { ?>
    <script>var username = 0;</script>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.close-filter').dblclick();

        if(!window.Notification) {
            console.log('Este browser não suporta Web Notifications!');
            return;
        }

        if (Notification.permission == 'default') {
            Notification.requestPermission(function() {
            });
        } else if (Notification.permission == 'granted') {
            if(username != 0) {
                var notification = new Notification('LOG 2.0', {
                dir: 'auto',
                icon: 'https://instagram.fsjp1-1.fna.fbcdn.net/t51.2885-19/s150x150/14156706_208450462904779_404983613_a.jpg',
                body: 'Logfitness sincera? Vem dar uma olhada!',
                tag : 'log2.0',
                });
            } else {
                var notification = new Notification('LOG 2.0', {
                dir: 'auto',
                icon: 'https://instagram.fsjp1-1.fna.fbcdn.net/t51.2885-19/s150x150/14156706_208450462904779_404983613_a.jpg',
                body: 'Logfitness sincera? Vem dar uma olhada!',
                tag : 'log2.0',
                });
            }

            notification.onshow = function(e) {
            },
            notification.onclick = function(e) {
                $('.ts-modal-trigger').click();
            },
            notification.onclose = function(e) {
            }
            
        } else if (Notification.permission == 'denied') {
        }
    });
    $(window).load(function() {
        var medida_tela = $(window).width();
        if(medida_tela > 768) {
            $('.close-mochila').click();
        }
    });
</script>

<?= $this->Flash->render() ?>

<!-- <script>
    $(window).load(function() {
        $('.loader').fadeOut(1500);
    });
</script> -->



<!-- <div class="loader">
    <div class="content-loader">
        <?= $this->Html->image('logfitness.png', ['alt' => 'logfitness'])?><br/><br/>
        <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i><br/><br/>
        <p><strong>Vivemos desafiando os limites, e vc?</strong></p>
    </div>
</div> -->

<?php if($pedido_id) { ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $.get(WEBROOT_URL + 'produtos/comprar_express/',
                function (data) {
                    $('.laterais.mochila').html(data);
                    if($('.laterais.mochila').width() < 200) {
                        $('.close-mochila').click();
                    }
                    $('.produtos-mochila').animate({ 'width' : '228px', 'opacity' : 1 }, function() {
                        $('.info-direita-mochila').fadeIn(200);
                    });
                    loading.hide(1);
                });
        });
    </script>
<?php } ?>

<input type="hidden" class="ordenar-hidden" value="&ord=<?= $_GET['ord'] ?>">
<input type="hidden" class="valor-hidden" value="&valor_min=<?= $_GET['valor_min'] ?>&valor_max=<?= $_GET['valor_max'] ?>">
<input type="hidden" class="marca-hidden" value="&m=<?= $_GET['m'] ?>">
<input type="hidden" class="objcat-hidden" value="&oid=<?= $_GET['oid'] ?>&cid=<?= $_GET['cid'] ?>">
<input type="hidden" class="busca-hidden" value="&busca=<?= $_GET['busca'] ?>">

<!-- GRADE DE PRODUTOS - 16 PRODUTOS (4 X 4) -->
<div class="container" id="grade-produtos" data-section="home" style="margin-top: 10px">

    <?php if($_GET['cid'] || $_GET['m'] || $_GET['busca']) { ?>
    <div class="col-xs-12 voltar-link">
        <div class="col-xs-8" style="float: left; text-align: left">
            <p class="filtragem">
                <?php if($_GET['m']) { ?>
                    <a data-id="<?= $marca_selected->id ?>" class="btn btn-cupom marca_selected-express"><?= $marca_selected->name; ?> <i class="fa fa-close"></i> </a>
                <?php } ?>
                <?php if($_GET['cid']) { ?>
                    <a data-id="<?= $categoria_selected->id ?>" obj-id="<?= $objetivo_selected->id ?>" class="btn btn-cupom categoria_selected-express"><i style="width: 31px; float: left;"><?= $objetivo_selected->image; ?></i> <?= $categoria_selected->name; ?> <i class="fa fa-close"></i> </a>
                <?php } ?>
                <?php if($_GET['busca']) { ?>
                    <a class="btn btn-cupom query_selected-express"><?= $_GET['busca'] ?> <i class="fa fa-close"></i> </a>
                <?php } ?>
            </p>
        </div>
        <a id="limpar-filtro-express" class="btn btn-cupom"><span class="fa fa-close"></span> Limpar filtro</a>
    </div>
    <?php } ?>

    <?php
    $i = 1;
    foreach($produtos as $produto):
        $fotos = unserialize($produto->fotos);
        ?>
        <div class="col-xs-3 produto-item" id="produto-<?= $i ?>">
            <div class="col-md-12 col-xs-12 text-center">
                <div class="bg-white-produto col-xs-12" itemscope="" itemtype="http://schema.org/Product">
                    <div class="box-imagem-produto">

                        <?php if($produto->estoque == 1) { ?>
                            <div class="box-ultimo">    
                                <span>ÚLTIMO</span>
                            </div> 
                        <?php }?>   

                        <?php if($produto->estoque == 1 && $produto->created > $max_date_novidade) { ?>
                            <div class="box-novidade" style="top: 19px;">    
                                <span>NOVIDADE</span>
                            </div>
                        <?php } else if($produto->created > $max_date_novidade) { ?>
                            <div class="box-novidade">    
                                <span>NOVIDADE</span>
                            </div>
                        <?php } ?>

                        <?php if(!($produto->estoque == 1 && $produto->created > $max_date_novidade) && $produto->preco_promo) { ?>
                            <?php if($produto->estoque == 1 && $produto->preco_promo || $produto->created > $max_date_novidade && $produto->preco_promo) { ?>
                                <div class="box-promocao" style="top: 19px;">    
                                    <span>PROMOÇÃO</span>
                                </div>
                            <?php } elseif ($produto->preco_promo) { ?>
                                <div class="box-promocao">    
                                    <span>PROMOÇÃO</span>
                                </div>
                            <?php } ?>
                        <?php } ?>

                        <?= $this->Html->link(
                            $this->Html->image(
                                'produtos/sm-'.$fotos[0],
                                ['class' => 'produto-grade', 'val' => $produto->slug, 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]),
                            $SSlug.'/produto_express/'.$produto->slug,
                            ['escape' => false]
                        ); ?>
                    </div>
                    <div class="categoria">
                         <?php $icone_ativado = 0; ?>
                            <?php foreach($prod_objetivos as $p_objetivos): 
                                if($p_objetivos->produto_base_id == $produto->produto_base->id && $icone_ativado == 0) {
                                
                                    if($p_objetivos->objetivo_id == 1) { ?>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 50.463 50.463" style="enable-background:new 0 0 50.463 50.463;" xml:space="preserve" width="20px" height="20px"><g><g><path d="M47.923,29.694c0.021-0.601-0.516-1.063-0.901-1.515c-0.676-2.733-2.016-5.864-3.961-8.971    C39.942,14.23,31.688,6.204,28.553,4.966c-0.158-0.062-0.299-0.097-0.429-0.126c-0.313-1.013-0.479-1.708-1.698-2.521    c-3.354-2.236-7.099-2.866-9.578-1.843c-2.481,1.023-3.859,6.687-1.19,8.625c2.546,1.857,7.583-1.888,9.195,0.509    c1.609,2.396,3.386,10.374,6.338,15.473c-0.746-0.102-1.514-0.156-2.307-0.156c-3.406,0-6.467,0.998-8.63,2.593    c-1.85-2.887-5.08-4.806-8.764-4.806c-3.82,0-7.141,2.064-8.95,5.13v22.619h4.879l1.042-1.849    c3.354-1.287,7.32-4.607,10.076-8.147C29.551,44.789,47.676,36.789,47.923,29.694z" fill="#5089cf"/></g></g></svg>
                                        <?php $icone_ativado = 1; ?>
                                    <?php } else if($p_objetivos->objetivo_id == 2) { ?>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="20px" height="20px" viewBox="0 0 164.882 164.883" style="enable-background:new 0 0 164.882 164.883;" xml:space="preserve"><g><path d="M161.77,36.535h-34.86c5.286-11.953,11.972-19.461,12.045-19.543c1.157-1.272,1.065-3.249-0.207-4.402   c-1.267-1.16-3.239-1.065-4.396,0.201c-0.347,0.38-8.403,9.31-14.236,23.744H60.471c-1.72,0-3.118,1.395-3.118,3.118v39.948   c0,1.72,1.397,3.118,3.118,3.118h55.966c2.058,8.284,5.56,16.425,10.576,24.22c1.103,1.699,2.394,3.77,3.733,5.985   c-15.145,6.102-32.2,9.384-49.545,9.384c-0.006,0-0.012,0-0.018,0c-18,0-35.591-3.532-51.152-10.072   c1.178-1.941,2.338-3.763,3.31-5.297c32.15-49.922-6.933-93.712-7.338-94.147c-1.16-1.273-3.126-1.355-4.402-0.207   c-1.272,1.16-1.367,3.13-0.207,4.408c1.492,1.641,36.222,40.743,6.704,86.573c-6.043,9.377-17.284,26.84-15.396,46.722   c0.149,1.613,1.51,2.819,3.1,2.819c0.101,0,0.201,0,0.298-0.013c1.717-0.158,2.98-1.687,2.807-3.397   c-1.136-11.947,3.255-23.176,7.968-31.943c16.492,7.039,35.204,10.783,54.297,10.783c0.006,0,0.012,0,0.019,0   c18.44,0,36.586-3.525,52.649-10.113c4.561,8.67,8.708,19.625,7.6,31.268c-0.158,1.711,1.096,3.239,2.801,3.397   c0.109,0.013,0.201,0.013,0.305,0.013c1.595,0,2.953-1.206,3.093-2.82c1.906-19.881-9.353-37.344-15.393-46.728   c-4.348-6.747-7.441-13.743-9.39-20.846h38.909c1.718,0,3.118-1.397,3.118-3.118V39.638   C164.888,37.918,163.494,36.535,161.77,36.535z M158.653,76.478h-6.51V63.732c0-1.72-1.4-3.117-3.117-3.117   c-1.724,0-3.117,1.397-3.117,3.117v12.745h-8.708V63.732c0-1.72-1.395-3.117-3.117-3.117c-1.718,0-3.118,1.397-3.118,3.117v12.745   h-8.701V63.732c0-1.72-1.406-3.117-3.118-3.117c-1.723,0-3.117,1.397-3.117,3.117v12.745h-8.714V63.732   c0-1.72-1.395-3.117-3.117-3.117c-1.718,0-3.118,1.397-3.118,3.117v12.745h-8.688V63.732c0-1.72-1.404-3.117-3.118-3.117   c-1.723,0-3.118,1.397-3.118,3.117v12.745h-8.705V63.732c0-1.72-1.397-3.117-3.117-3.117c-1.717,0-3.118,1.397-3.118,3.117v12.745   h-7.639V42.765h95.07v33.713H158.653z M80.176,108.765c-3.635,0-6.585-2.953-6.585-6.582c0-3.635,2.95-6.582,6.585-6.582   s6.576,2.947,6.576,6.582C86.746,105.812,83.811,108.765,80.176,108.765z M10.564,68.022v-6.658H0V44.808h10.564v-6.658   l14.94,14.939L10.564,68.022z" fill="#5089cf"/></g></svg>
                                        <?php $icone_ativado = 1; ?>
                                    <?php } else if($p_objetivos->objetivo_id == 3) { ?>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="20px" height="20px"><g><g><path d="M400.268,175.599c-1.399-3.004-4.412-4.932-7.731-4.932h-101.12l99.797-157.568c1.664-2.628,1.766-5.956,0.265-8.678    C389.977,1.69,387.109,0,384.003,0H247.47c-3.234,0-6.187,1.826-7.637,4.719l-128,256c-1.323,2.637-1.178,5.777,0.375,8.294    c1.562,2.517,4.301,4.053,7.262,4.053h87.748l-95.616,227.089c-1.63,3.883-0.179,8.388,3.413,10.59    c1.382,0.845,2.918,1.254,4.446,1.254c2.449,0,4.864-1.05,6.537-3.029l273.067-324.267    C401.206,182.161,401.667,178.611,400.268,175.599z" fill="#5089cf"/>
                                            </g></g></svg>
                                        <?php $icone_ativado = 1; ?>
                                    <?php } else if($p_objetivos->objetivo_id == 4) { ?>
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 511.998 511.998" style="enable-background:new 0 0 511.998 511.998;" xml:space="preserve" width="20px" height="20px"><g><g><path d="M396.8,236.586h-93.175l-22.673-68.053c-1.775-5.325-6.929-9.523-12.424-8.747c-5.623,0.128-10.496,3.874-12.023,9.276    l-37.35,130.697l-40.252-181.146c-1.229-5.478-5.871-9.523-11.477-9.975c-5.572-0.375-10.829,2.773-12.902,7.996l-48,119.953H12.8    c-7.074,0-12.8,5.726-12.8,12.8c0,7.074,5.726,12.8,12.8,12.8h102.4c5.222,0.008,9.95-3.174,11.878-8.047l35.823-89.523    l42.197,189.952c1.271,5.726,6.272,9.847,12.126,10.027c0.128,0,0.247,0,0.375,0c5.7,0,10.726-3.772,12.297-9.276l39.851-139.426    l12.501,37.547c1.749,5.222,6.647,8.747,12.151,8.747h102.4c7.074,0,12.8-5.726,12.8-12.8    C409.6,242.311,403.874,236.586,396.8,236.586z" fill="#5089cf"/></g></g><g><g><path d="M467.012,64.374C437.018,34.38,397.705,19.387,358.4,19.387c-36.779,0-73.259,13.662-102.4,39.919    c-29.15-26.257-65.621-39.919-102.4-39.919c-39.313,0-78.618,14.993-108.612,44.988C5.214,104.157-7.595,160.187,5.385,211.011    h26.624c-13.653-43.972-3.678-93.773,31.078-128.529c24.175-24.175,56.32-37.487,90.513-37.487    c31.206,0,60.399,11.563,83.695,31.889L256,94.369l18.714-17.493c23.296-20.318,52.489-31.889,83.686-31.889    c34.193,0,66.33,13.312,90.513,37.487c49.911,49.903,49.903,131.115,0,181.018L256,456.404L87.407,287.811H51.2l204.8,204.8    l211.012-211.012C526.993,221.61,526.993,124.364,467.012,64.374z" fill="#5089cf"/></g></g></svg>
                                        <?php $icone_ativado = 1; ?>
                                    <?php }
                                
                                }
                            endforeach; ?>
                    </div>

                    <div class="box-info-produto">
                        <p class="description">
                            <span class="product" itemprop="name"><?= $produto->produto_base->name; ?></span>
                        </p>

                        <?php if($produto->produto_base->propriedade_id && !empty($produto->propriedade)) { ?>
                        <div class="col-xs-12" style="height: 27px">
                            <?= ($produto->produto_base->propriedade_id && !empty($produto->propriedade)) ?
                            '<p class="sabor-info text-uppercase">'.$produto->propriedade.'</p>' : ''; ?>
                        </div>
                        <?php } else { ?>
                            <p class="sabor-info text-uppercase">&nbsp;</p>
                        <?php } ?>

                        <!-- Trustvox Estrelinhas -->
                        <!-- <div data-trustvox-product-code="<?= $produto->id ?>"></div> -->

                        <div class="col-xs-12 box-fundo-produto">
                            <div class="box-preco">                  
                                <p class="preco preco-desc">
                                    <?php $porcentagem_nova = $produto->preco * 0.089 ?>
                                    R$ <span style="text-decoration: line-through"><?= number_format($preco = $produto->preco + $porcentagem_nova, 2, ',','.'); ?></span>
                                </p>

                                <p class="preco preco-normal">
                                    <?php $produto->preco_promo ? $porcentagem_nova = $produto->preco_promo * $desconto_geral * 0.089 : $porcentagem_nova = $produto->preco * $desconto_geral * 0.089 ?>
                                    R$ <span><?= $produto->preco_promo ?
                                        number_format($preco = ($produto->preco_promo * $desconto_geral + $porcentagem_nova) * 0.75, 2, ',','.') :
                                        number_format($preco = ($produto->preco * $desconto_geral + $porcentagem_nova) * 0.75, 2, ',','.'); ?></span>
                                </p>
                            </div>
                            <?php if($produto->visivel == 1 && $produto->status_id == 1) { ?>
                            <div class="box-mochila-add">
                                <svg val="<?= $produto->slug ?>" data-id="<?= $produto->id ?>" class="incluir-mochila-express" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"><g><polygon points="8.669,48.706 5,62.65 6.565,64.773 10.008,54.727  "/><polygon points="93.422,64.79 95,62.65 91.331,48.706 89.991,54.727  "/><polygon points="41.311,14.947 39.661,15.937 37.462,30.863 37.071,40.502 39.622,40.182 41.47,14.947  "/><polygon points="62.928,40.51 62.538,30.863 60.338,15.937 58.689,14.947 58.529,14.947 60.378,40.188  "/><polygon points="29.01,42.523 21.586,43.458 9.53,47.966 11.045,54.785 5.5,70.967 5.5,88.654 27.14,88.654  "/><polygon points="36.48,55.079 35.119,88.654 64.879,88.654 63.519,55.079  "/><polygon points="52.475,40.195 47.463,40.199 37.03,41.513 36.716,49.244 63.282,49.244 62.969,41.516  "/><polygon points="88.954,54.785 90.47,47.966 78.413,43.458 70.989,42.524 72.859,88.654 94.5,88.654 94.5,70.967  "/><path d="M28.12,89.154h5.978l1.422-35.075h28.96L65.9,89.154h5.979l-2.368-58.421L65.62,15.752l-7.219-4.906h-7.104h-2.595h-7.105   l-7.218,4.906l-3.891,14.981L28.12,89.154z M51.578,13.947h7.388l2.256,1.354l2.312,15.433l0.79,19.511H35.675l0.792-19.511   l2.311-15.433l2.257-1.354h7.386H51.578z"/></g></svg>
                            </div>
                            <?php } else if($produto->visivel == 1 && $produto->status_id == 2) { ?>
                                <div class="box-mochila-avise text-center">
                                    <i class="fa fa-envelope fa-3x produto-acabou-btn" data-id="<?= $produto->id ?>"></i>
                                </div> 
                                <div id="aviseMe-<?= $produto->id ?>" class="overlay-avise" onclick="closeAviseMe()">
                                  <div class="overlay-avise-content overlay-avise-<?= $produto->id ?> text-center">
                                    <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                    <br />
                                    <br />
                                    <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$produto->id])?>
                                    <?= $this->Form->input('produto_id', [
                                        'div'           => false,
                                        'label'         => false,
                                        'type'          => 'hidden',
                                        'val'           => $produto->id,
                                        'id'            => 'produto_id_acabou-'.$produto->id
                                    ])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'name_acabou-'.$produto->id,
                                        'class'         => 'form-control validate[optional]',
                                        'placeholder'   => 'Qual o seu nome?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'email_acabou-'.$produto->id,
                                        'class'         => 'form-control validate[required, custom[email]]',
                                        'placeholder'   => 'Qual o seu email?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->button('Enviar', [
                                        'data-id'       => $produto->id,
                                        'type'          => 'button',
                                        'class'         => 'btn btn-success send-acabou-btn'
                                    ])?>
                                    <br />
                                    <br />
                                    <?= $this->Form->end()?>
                                  </div>
                                </div>                               
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $i++;
    endforeach;
    ?>
    <!--FIM CONTAINER GRADE DE PRODUTOS-->
</div>

<div class="row" id="veja-mais">
    <div class="col-lg-12 text-center">
        <button class="btn btn-cupom" id="new-produtos-load-more-express"> Ver mais produtos <i class="fa fa-chevron-circle-down"></i></button>
    </div>
</div>