<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>
<section class="content">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="content-box">
                <div class="content-box-wrapper hidee">
                    
                </div>
                <div class="banners form">
                    <?= $this->Form->create($banner, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
                    <fieldset>
                        <h3 class="box-header">
                            Editar banner    
                        </h3>
                        <div class="form-group">
                            <div class="col-sm-3 control-label">
                                <?= $this->Form->label('title') ?>
                            </div>
                            <div class="col-sm-8">
                                <?= $this->Form->input('title', [
                                    'label' => false,
                                    'class' => 'form-control validate[required, minSize[3], maxSize[255]]',
                                    'placeholder' => 'Nome para referência'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3 control-label">
                                <?= $this->Form->label('link') ?>
                            </div>
                            <div class="col-sm-8">
                                <?= $this->Form->input('link', [
                                    'label' => false,
                                    'class' => 'form-control validate[optional,custom[url]]',
                                    'placeholder' => 'Link de ação para o banner (opcional)'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3 control-label">
                                <?= $this->Form->label('status_id') ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $this->Form->input('status_id', [
                                    'options'  => [
                                      2 => 'Inativo',
                                      1 => 'Ativo'
                                    ],
                                    'empty' => false,
                                    'label' => false,
                                    'class' => 'form-control validate[required]'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3 control-label">
                                <?= $this->Form->label('imagem') ?>
                            </div>
                            <div class="col-sm-8">
                                <?= $this->Form->input('imagem', [
                                    'id'    => 'image_upload',
                                    'label' => false,
                                    'class' => 'form-control validate[optional, custom[validateMIME[image/jpeg|image/png]]]',
                                    'type'  => 'file'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3 control-label">
                                <?= $this->Form->label('imagem_atual') ?>
                            </div>
                            <div class="col-sm-8">
                                <?= $this->Html->image('banners/'.$banner->image,
                                ['style' => 'width:100%'
                                ]) ?>
                            </div>
                        </div>

                        <div class="form-group text-center">
                            <div class="col-sm-offset-3 col-sm-3">
                                <?= $this->Form->button('Salvar',
                                    ['class' => 'btn btn-success', 'escape' => false]); ?>
                            </div>
                        <?= $this->Form->end() ?>
                            <div class="col-sm-3">
                                <?= $this->Html->link('Voltar','/academia/admin/banners/',
                                    ['class' => 'btn bg-red', 'escape' => false]); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</section>
