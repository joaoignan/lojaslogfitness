<?= $this->Html->script('jquery.mask.min.js')?>
<script>
	$(document).ready(function() {
		$('.money').mask('000000000000000.00', {reverse: true});	
	});
</script>

<style>
	.div-de-formulario{
		min-height: 80px;
	}
</style>

<section class="content">
	<div class="box box-info">
	    <div class="box-header with-border">
			<h3 class="box-title">Criar plano/assinatura</h3>
			<span class="pull-right"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
				'/academia/admin/admin_mensalidades/planos',
				['escape' =>  false,
				 'class'  =>  'pull-right']) ?></span>
	    </div> <!-- box-header -->
    	<div class="box-body">
			<div class="nav-tabs-custom">
		    	<ul class="nav nav-tabs">
		            <li class="active"><a href="#plano" data-toggle="tab">Novo Plano</a></li>
		            <li><a href="#assinatura" data-toggle="tab">Nova Assinatura</a></li>
		        </ul>
	        	<div class="tab-content">
					<div class="tab-pane active" id="plano">
						<div class="row">
							<div class="col-sm-12 col-md-6 text-center">
				    			<h4>Detalhes sobre criação</h4>
				    			<h5>Plano</h5>
				    			<p class="text-left">"Plano": o aluno irá pagar em um boleto só ou passar o cartão de crédito uma única vez, podendo dividir em quantas parcelas você deixar.</p>
				    			<br>
				    			<h5>Vendas limitadas</h5>
				    			<p class="text-left">É muito legal para promoções ou aulas que tem uma quantidade máxima de participantes. Quando atingir o numero estabelecido o Plano não é mais comercializado mas é possível criar um novo plano com as mesmas condições com uma quantidade nova.</p>
				    		</div>
				    		<div class="col-sm-12 col-md-6 text-center">
				    			<h4>Criar novo plano</h4>
				    			<div class="row text-left">
						    		<?= $this->Form->create(null, ['id' => 'form-plano-add']) ?>
						    			<div class="col-xs-12 div-de-formulario">
								    		<label for="name" class="label-control">Nome do plano</label>
								    		<input type="text" class="form-control validate[required]" name="name" placeholder="Escreva o nome do plano">
								    		<br>
							    		</div>
							    		<div class="">
						    			<div class="col-sm-12 col-md-6 div-de-formulario">
						    				<label for="valor_total">Valor total (em R$)</label>	
						    				<input name="valor_total" id="valor_total" class="valor_total form-control money validate[required]">
						    				<br>
						    			</div>
										<div class="col-sm-12 col-md-6 div-de-formulario">
						    				<label for="time_months">Duração mínima (em meses)</label>
						    				<input type="number" id="time_months" value="1" name="time_months" class="time_months form-control validate[required]" min="1" max="99">
						    				<br>
						    			</div>
						    			<div class="col-sm-12 col-md-6 div-de-formulario">
											<label for="">Valor mensal (em R$)</label>
											<p class="calculo_valor_mensal">calcula fazendo valor/duração</p>
						    				<br>
						    			</div>
						    			<div class="col-sm-12 col-md-6 div-de-formulario">
						    				<div class="col-xs-12 no-padding">
						    					<label>Limite de venda</label>
					    					</div>
						    				<div class="col-xs-6 col-sm-2 col-md-6 no-padding">
						    					<input type="checkbox" name="opt_limite_vendas" value="1"> Sim
						    				</div>
						    				<div class="col-xs-6 col-sm-2 col-md-4 no-padding">
						    					<input type="number" name="limite_vendas" placeholder="Qtd" class="form-control validate[optional]">
						    				</div>
						    			</div>
						    			<div class="col-sm-12 col-md-6 div-de-formulario">
						    				<label for="parcelas_cartao">Parcelamento no cartão</label>
						    				<?= $this->Form->input('parcelas_cartao', [
						    					'class'   => 'form-control validate[required]',
						    					'options' => [
						    						0 => 'Não aceitar cartão',
						    						1 => 'Em até 1 parcela',
						    						2 => 'Em até 2 parcelas',
						    						3 => 'Em até 3 parcelas',
						    						4 => 'Em até 4 parcelas',
						    						5 => 'Em até 5 parcelas',
						    						6 => 'Em até 6 parcelas',
						    						7 => 'Em até 7 parcelas',
						    						8 => 'Em até 8 parcelas',
						    						9 => 'Em até 9 parcelas',
						    						10 => 'Em até 10 parcelas',
						    						11 => 'Em até 11 parcelas',
						    						12 => 'Em até 12 parcelas'
						    					],
						    					'label'   => false
						    				]) ?>
						    			</div>
						    			<div class="col-sm-12 col-md-6 div-de-formulario">
						    				<label for="parcelas_boleto">Parcelamento no Boleto</label>
						    				<?= $this->Form->input('parcelas_boleto', [
						    					'class'   => 'form-control validate[required]',
						    					'options' => [
						    						0 => 'Não aceitar boleto',
						    						1 => 'Em até 1 parcela',
						    						2 => 'Em até 2 parcelas',
						    						3 => 'Em até 3 parcelas',
						    						4 => 'Em até 4 parcelas',
						    						5 => 'Em até 5 parcelas',
						    						6 => 'Em até 6 parcelas',
						    						7 => 'Em até 7 parcelas',
						    						8 => 'Em até 8 parcelas',
						    						9 => 'Em até 9 parcelas',
						    						10 => 'Em até 10 parcelas',
						    						11 => 'Em até 11 parcelas',
						    						12 => 'Em até 12 parcelas'
						    					],
						    					'label'   => false
						    				]) ?>
						    			</div>
						    			<input type="hidden" name="type" value="1">
						    			<div class="col-xs-12">
						    				<label for="descricao">Descrição</label>
						    				<textarea maxlength="200" name="descricao" id="descricao_plano" rows="4" class="form-control validate[optional]" placeholder="Ex: Horários válidos: Segunda-Sexta das 10:00 às 17:00"></textarea>
						    				<br>
						    			</div>
						    			<div class="col-xs-12 text-center">
						    				<br>
						    				<button class="btn btn-success">Criar plano</button>
						    			</div>
						    		<?= $this->Form->end() ?>
					    		</div>
				    		</div>
						</div>
					</div>
				</div>
            	<div class="tab-pane" id="assinatura">
                	<div class="row">
						<div class="col-sm-12 col-md-6 text-center">
			    			<h4>Detalhes sobre criação</h4>
			    			<h5>Assinatura</h5>
							<p class="text-left">"Assinatura": você seleciona o tempo mínimo de assinatura e o sistema gera automaticamente a cobrança recorrente e mensal. Não compromete o limite do cartão de crédito. Você pode escolher também que quando atinge o tempo mínimo o sistema para de cobrar ou só para de cobrar quando o aluno pedir o cancelamento para a academia. O cancelamento da cobrança no segundo caso é por conta da academia e pode ser acompanhado <?= $this->Html->link( 'aqui', '/academia/admin/admin_mensalidades/assinaturas',['target' => '_blank']) ?></p>
							<br>
							<h5>Vendas limitadas</h5>
							<p class="text-left">É muito legal para promoções ou aulas que tem uma quantidade máxima de participantes. Quando atingir o numero estabelecido o Plano não é mais comercializado mas é possível criar um novo plano com as mesmas condições com uma quantidade nova.</p>
			    		</div>
			    		<div class="col-sm-12 col-md-6 text-center">
			    			<h4>Criar nova Assinatura</h4>
			    			<div class="row text-left">
					    		<?= $this->Form->create(null, ['id' => 'form-assinatura-add']) ?>
				    			<div class="col-xs-12 div-de-formulario">
						    		<label for="name" class="label-control">Nome da assinatura</label>
						    		<input type="text" class="form-control validate[required]" name="name" placeholder="Escreva o nome da assinatura">
						    		<br>
					    		</div>
				    			<div class="col-sm-12 col-md-6 div-de-formulario">
				    				<label for="valor_total">Valor total (em R$)</label>	
				    				<input name="valor_total" id="valor_total" class="valor_total form-control money validate[required]">
				    				<br>
				    			</div>
				    			<script type="text/javascript">
				    				$(document).ready(function() {
				    					$('.valor_total').blur(function() {
				    						if($(this).val() != '' && $('.time_months').val() != '') {
				    							var qtd_mes = $('.time_months').val();
				    							var valor = $(this).val();
				    							valor = parseFloat(valor);
				    							var valor_mensal =  valor.toFixed(2) / qtd_mes;
				    							$('.calculo_valor_mensal').html(valor_mensal.toFixed(2));
				    						}
				    					});
				    					$('.time_months').blur(function () {
				    						$('.valor_total').blur();
				    					});
				    				});
				    			</script>
								<div class="col-sm-12 col-md-6 div-de-formulario">
				    				<label for="time_months">Duração mínima (em meses)</label>
				    				<input type="number" id="time_months" value="1" name="time_months" class="time_months form-control validate[required]" min="1" max="99">
				    				<br>
				    			</div>
				    			<div class="col-sm-12 col-md-6 div-de-formulario">
									<label for="">Valor mensal (em R$)</label>
									<p class="calculo_valor_mensal">calcula fazendo valor/duração</p>
				    				<br>
				    			</div>
				    			<div class="col-sm-12 col-md-6 div-de-formulario">
				    				<div class="col-xs-12 no-padding">
				    					<label>Limite de venda</label>
			    					</div>
				    				<div class="col-xs-6 col-sm-2 col-md-6 no-padding">
				    					<input type="checkbox" name="opt_limite_vendas" value="1"> Sim
				    				</div>
				    				<div class="col-xs-6 col-sm-2 col-md-4 no-padding">
				    					<input type="number" name="limite_vendas" placeholder="Qtd" class="form-control validate[optional]">
				    				</div>
				    			</div>
				    			<div class="col-xs-12">
				    				<label for="descricao">Descrição</label>
				    				<textarea maxlength="200" name="descricao" id="descricao_plano" rows="4" class="form-control validate[optional]" placeholder="Ex: Horários válidos: Segunda-Sexta das 10:00 às 17:00"></textarea>
				    				<br>
				    			</div>
				    			<input type="hidden" name="type" value="2">
				    			<div class="col-xs-12 text-center">
				    				<br>
				    				<button class="btn btn-success">Criar assinatura</button>
				    			</div>
					    		<?= $this->Form->end() ?>
				    		</div><!-- row text-left -->
			    		</div><!-- col-sm-12 col-md-6 text-center -->
	            	</div>
	        	</div>
	    	</div>
    	</div><!-- box-body -->
    </div> <!-- box box-info -->
</section>