<style>

.logcoin {
    height: 0%;
    width: 100%;
    position: fixed;
    z-index: 9999;
    top: 0;
    left: 0;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0, 0.9);
    overflow-y: hidden;
    transition: 0.5s;
}

.logcoin-content {
    position: relative;
    top: 10%;
    height: 80%;
    left: 20%;
    width: 60%;
    border-radius: 15px;
    background: #ecb92a;
    text-align: center;
    margin-top: 10px;
    overflow-x: auto;
}

.logcoin-content::-webkit-scrollbar {
    width: 12px;
}
 
/* Track */
.logcoin-content::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); 
    -webkit-border-radius: 10px;
    border-radius: 10px;
}
 
/* Handle */
.logcoin-content::-webkit-scrollbar-thumb {
    -webkit-border-radius: 10px;
    border-radius: 10px;
    background: rgba(255,255,255,0.8); 
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
}
.logcoin-content::-webkit-scrollbar-thumb:window-inactive {
  background: rgba(80,137,207,0.4); 
}


.logcoin a {
    padding: 8px;
    text-decoration: none;
    font-size: 36px;
    color: #818181;
    display: block;
    transition: 0.3s;
}

.logcoin img{
    width: 100%
    }

.logcoin a:hover, .logcoin a:focus {
    color: #f1f1f1;
}

.logcoin .closebtn {
    position: absolute;
    top: 20px;
    right: 45px;
    font-size: 60px;
}

@media screen and (max-width: 1024px) {
    .logcoin-content {
    position: relative;
    top: 3%;
    height: auto;
    left: 10%;
    width: 80%;
    border-radius: 25px;
    background: #4f88cf;
    text-align: center;
    margin-top: 10px;
    }
    .logcoin img{
    width: 75%
    }
}

@media screen and (max-width: 450px) {
  .logcoin {overflow-y: auto;}
  .logcoin a {font-size: 20px}
  .logcoin .closebtn {
    font-size: 40px;
    top: 15px;
    right: 35px;
  }
  .logcoin-content {
    position: relative;
    top: 20%;
    height: auto;
    left: 5%;
    width: 90%;
    border-radius: 25px;
    background: #4f88cf;
    text-align: center;
    margin-top: 10px;
    }

    .logcoin img{
    width: 85%
    }
}
</style>
</head>
<body>

<div id="NavLogCoin" class="logcoin">
  <div class="logcoin-content">
    <?= $this->Html->image('entenda_logcoins.jpg',
     [  'escape'    =>  'false',
        'alt'       =>  'entenda_logcoins',
        'title'     =>  'Entenda as logcoins'
     ]) ?>
    <a href="javascript:void(0)" onclick="closeLogCoin()"><button class ="btn btn-success" >Beleza! Entendi</button></a>    
  </div>
</div>




<span style="font-size:30px;cursor:pointer" onclick="openLogCoin()">&#9776; open</span>

<script>
function openLogCoin() {
    document.getElementById("NavLogCoin").style.height = "100%";
}

function closeLogCoin() {
    document.getElementById("NavLogCoin").style.height = "0%";
}
</script>
     
</body>