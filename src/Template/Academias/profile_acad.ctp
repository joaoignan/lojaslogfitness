<style>
    #btn-perfil{
        display: none;
    }
</style>
<?= $this->Flash->render() ?>

<div class="row profile-menu">
            <ul>
                <li><a href="#pacotes"><i class="fa fa-trophy" aria-hidden="true"></i> Planos</a></li>
                <li><a href="#sobre"><i class="fa fa-info" aria-hidden="true"></i> Sobre</a></li>
                <li><a href="#produtos"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Produtos</a></li>
                <li><a href="#modalidades"><i class="fa fa-futbol-o" aria-hidden="true"></i> Modalidades</a></li>
                <li><a href="#calendario"><i class="fa fa-calendar" aria-hidden="true"></i> Calendário</a></li>
                <li><a href="#galeria"><i class="fa fa-camera" aria-hidden="true"></i> Galeria</a></li>
                <li><a href="#avaliacoes"><i class="fa fa-star-half-o" aria-hidden="true"></i> Avaliações</a></li>
                <li><a href="#mapa"><i class="fa fa-map-marker" aria-hidden="true"></i> Mapa</a></li>
            </ul>
        </div> <!-- row -->
        <?php if(count($planos) >= 1) { ?>
            <div id="pacotes" class="row box-conteudo text-center">
                <h3>Pacotes</h3>
                <hr>
                <div class="owl-carousel-4">
                    <?php foreach($planos as $plano) { ?>
                        <?php $qtd_planos_cliente = 0; ?>
                        <?php foreach ($planos_clientes as $plano_cliente) {
                            if($plano_cliente->academia_mensalidades_planos_id == $plano->id) {
                                $qtd_planos_cliente++;
                            }
                        } ?>
                        <?php if($qtd_planos_cliente < $plano->limite_vendas) { ?>
                            <div class="item">
                                <div class="col-xs-12">
                                    <div class="pacote-content">
                                        <?php if($plano->type == 2){?>
                                            <div class="faixa-assinatura text-center">
                                                <span>assinatura</span>
                                            </div>
                                        <?php } ?>
                                        <div class="col-xs-12">
                                            <h4><?= $plano->name ?></h4>
                                        </div>
                                        <p>Tipo: <?= $plano->type == 1 ? 'Plano' : 'Assinatura' ?></p>
                                        <p>Duração: <?= $plano->time_months == 1 ? $plano->time_months.' mês' : $plano->time_months.' meses' ?></p>
                                        <p>R$ <?= number_format($valor_mes = $plano->valor_total/$plano->time_months, 2, ',', ' '); ?> / mês</p>
                                        <label style="height: 126px">
                                            <?php if($plano->descricao != null){
                                                echo $plano->descricao;
                                            } else {
                                                echo "&nbsp";
                                                } ?></label>
                                        <div class="metodos">
                                            <p><strong>métodos de pagamento</strong></p>
                                            <p>
                                                <?php if($plano->type == 2) { ?>
                                                    <div class="col-xs-12 text-center">
                                                        <i class="fa fa-credit-card fa-2x" aria-hidden="true"></i>
                                                        <p>crédito</p>
                                                    </div>
                                                <?php } else { ?>
                                                    <?php if($plano->parcelas_cartao >= 1 && $plano->parcelas_boleto >= 1) { ?>
                                                        <div class="col-xs-6 text-center">
                                                            <i class="fa fa-credit-card fa-2x" aria-hidden="true"></i>
                                                            <p>crédito</p>
                                                        </div>
                                                        <div class="col-xs-6 text-center">
                                                            <i class="fa fa-barcode fa-2x" aria-hidden="true"></i>
                                                            <p>boleto</p>
                                                        </div>
                                                    <?php } else { ?>
                                                        <?php if($plano->parcelas_cartao >= 1) { ?>
                                                            <div class="col-xs-12 text-center">
                                                                <i class="fa fa-credit-card fa-2x" aria-hidden="true"></i>
                                                                <p>crédito</p>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if($plano->parcelas_boleto >= 1) { ?>
                                                            <div class="col-xs-12 text-center">
                                                                <i class="fa fa-barcode fa-2x" aria-hidden="true"></i>
                                                                <p>boleto</p>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </p>
                                        </div>
                                        <?= $this->Html->link('Ver planos',
                                            'mensalidade/'.$slug,[
                                            'class' => 'btn btn-success btn-sm',
                                        ])?>
                                    </div>
                                </div> 
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        <?php } else { ?>
            <!-- <div id="pacotes" class="row box-conteudo text-center">
                <div class="col-xs-12 text-center academia-ticoliro" style="background-color: #ececec;">
                    <div class="academia-ticoliro-content">
                        <h1>Essa academia ainda não possui LOGMensalidades</h1>
                    </div>
                </div>
            </div> -->
        <?php } ?>
        <div id="sobre" class="row box-conteudo text-center">
            <h3>Sobre</h3>
            <hr>
            <div class="col-xs-12 academia-sobre text-left">
                <p class="info-sobre"><?= $academia->sobre ?></p>
                <br>
                <h4>Contato</h4>
                <p><i class="fa fa-phone" aria-hidden="true"></i> <?= $academia->phone ?></p>
                <?php $telefone = preg_replace("/[^0-9]/", "", $academia->mobile); ?>
                <?php if($academia->mobile != null) { ?>
                    <a href="intent://send/<?= $telefone ?>#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?= $academia->mobile ?> <br>
                    <i class="fa fa-android"></i> Clique para batermos um papo!</a>
                <?php } ?>
                <p><i class="fa fa-map-marker" aria-hidden="true"></i> <?= $academia->address_acad ?>, <?= $academia->number_acad ?> -
                <?php if ($academia->complement != null ) {?>  <?= $academia->complement ?> - <?php } ?> <?= $academia->area_acad ?> - <?= $cidade_acad ?> - <?= $estado_acad ?></p>
                <p><?= $academia->horario_semana != '00:00 - 00:00' ? '<i class="fa fa-clock-o" aria-hidden="true"></i>Segunda à sexta: '.$academia->horario_semana : '' ?><?= $academia->intervalo_semana != '00:00 - 00:00' ? ' - Fechado: '.$academia->intervalo_semana : '' ?></p>
                <p><?= $academia->horario_sabado != '00:00 - 00:00' ? '<i class="fa fa-clock-o" aria-hidden="true"></i>Sábado: '.$academia->horario_sabado : ''  ?><?= $academia->intervalo_sabado != '00:00 - 00:00' ? ' - Fechado: '.$academia->intervalo_sabado : '' ?></p>
                <p></i><?= $academia->horario_domingo != '00:00 - 00:00' ? '<i class="fa fa-clock-o" aria-hidden="true"></i>Domingo: '.$academia->horario_domingo : ''   ?><?= $academia->intervalo_domingo != '00:00 - 00:00' ? ' - Fechado: '.$academia->intervalo_domingo : '' ?></p>
                <hr>
            </div> <!-- academia-sobre -->
            <div class="col-xs-12 diferenciais text-left">
                <h4>Diferenciais</h4>
                <?= $this->Html->Image('log_diferencial.png',['title' => 'Super Academia']); ?>
                <?php if($acad_mensalidade->account_verify == 1) {
                        echo $this->Html->Image('logmensalidades.png',['title' => 'LOGMensalidades']);
                    }?>
                <?php foreach ($acad_diferenciais as $a_dif) {
                    foreach ($diferenciais as $dif) {
                        if($a_dif->diferencial_id == $dif->id) {
                            echo $this->Html->image('diferenciais/'.$dif->image,['title' => $dif->name, 'alt' => 'foto-galeria', 'class' => 'text-center']);
                        }
                    }
                } ?>
            </div> <!-- diferenciais -->
        </div> <!-- row box-conteudo text-center -->
        <div id="produtos" class="row box-conteudo text-center">
            <h3>Produtos <span class="pull-right"><?= $this->Html->link('<button class="btn btn-success">loja</button>','https://www.logfitness.com.br/'.$academia->slug,['escape' => false]) ?></span></h3>
            <p>Acesse a loja da sua academia. Compre produtos para potencializar seus treinos e receba em sua academia com frete grátis!</p>
            <hr>
            <div class="owl-carousel-2">
                <?php foreach($produtos as $produto) { ?>
                <?php $fotos = unserialize($produto->fotos); ?>
                <div class="item">
                    <div class="galeria-content">
                        <?= $this->Html->link(
                            $this->Html->image(
                                'produtos/md-'.$fotos[0],
                                ['class' => 'produto-grade', 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]),
                            WEBROOT_URL.$academia->slug.'/produto/'.$produto->slug,
                            ['escape' => false]
                        ); ?>
                        <p><strong><?= $produto->produto_base->name; ?></strong></p>
                    </div>
                </div>

                <?php } ?>
            </div>
        </div>
        <?php if(count($modalidades) >= 1) { ?>
            <div id="modalidades" class="row box-conteudo text-center">
                <h3>Modalidades</h3>
                <hr>
                <div class="modalidades-content text-left">
                    <ul>
                        <?php foreach ($modalidades as $modalidade) { ?>
                            <div class="modalidade-single">
                                <i class="fa fa-check" aria-hidden="true"></i> <?= $modalidade->esporte->name ?>
                            </div>
                        <?php } ?>
                    </ul>
                </div> <!-- modalidades-content -->
            </div> <!-- row box-conteudo text-center -->
        <?php } ?>

        <?php if(count($lista_calendario) >= 1) { ?>
            <div id="calendario" class="row box-conteudo text-center">
                <h3>Calendário Semanal</h3>
                <hr>
                <div class="calendario-content text-center col-xs-12">
                    <div class="table-calendario">
                        <table style="width: 100%" class="text-center" id="horarios-table">
                            <tr>
                                <th><strong>Segunda</strong></th>
                                <th><strong>Terça</strong></th>
                                <th><strong>Quarta</strong></th>
                                <th><strong>Quinta</strong></th>
                                <th><strong>Sexta</strong></th>
                                <th><strong>Sábado</strong></th>
                                <th><strong>Domingo</strong></th>
                            </tr>
                            <?php foreach ($lista_calendario as $key => $lcalendario) {
                                if($key % 7 == 0) {
                                    echo '</tr>';
                                }
                                if($key % 7 == 0 || $key == 0) {
                                    echo '<tr>';
                                }
                                echo '<td>';
                                if($lcalendario == 'vazio') {
                                    echo '<p>-</p>';
                                } else {
                                    echo '<p style="text-transform: capitalize; font-size: 18px">'.ucwords(strtolower($lcalendario->aula)).'</p>';
                                }
                                if($lcalendario == 'vazio') {
                                    echo '<p>-</p>';   
                                } else {
                                    echo '<p style="font-size: 16px">'.$lcalendario->horario_inicio->format('H:i').' - '.$lcalendario->horario_termino->format('H:i').'</p>';   
                                }
                                echo '</td>';
                            } ?>
                        </table>
                    </div>
                </div>
            </div>
        <?php } ?>

        <?php if(count($galeria) >= 1) { ?>
            <div id="galeria" class="row box-conteudo text-center">
                <h3>Galeria</h3>
                <hr>
                <div class="owl-carousel-2">
                    <?php foreach($galeria as $imagem) { ?>
                    <div class="item">
                        <div class="galeria-content">
                            <?php if($imagem[1] == 'galeria/') {
                                echo $this->Html->link(
                                    $this->Html->image($imagem[1].$imagem[0]->image,['alt' => $imagem[0]->title, 'class' => 'text-center']),'/img/galeria/lg-'.$imagem[0]->image,
                                      ['class' => 'fancybox', 'escape' => false]);
                            } else {
                                echo $this->Html->link(
                                    $this->Html->image($imagem[1].$imagem[0]->image,['alt' => $imagem[0]->title, 'class' => 'text-center']),'/img/'.$imagem[1].$imagem[0]->image,
                                      ['class' => 'fancybox', 'escape' => false]);
                            } ?>
                        </div>
                    </div>

                    <?php } ?>
                </div>
            </div>
        <?php } ?>

        <?php if(count($instrutores) >= 1) { ?>
            <div id="instrutores" class="row box-conteudo text-center">
                <h3>Nossa Equipe</h3>
                <hr>
                <div class="owl-carousel-1">
                    <?php foreach ($instrutores as $instrutor) { ?>
                        <div class="item">
                            <div class="instrutores-content">
                                <div class="col-xs-12 topo-professor text-center">
                                </div> <!-- topo-professor -->
                                <div class="col-xs-12 dados-professor text-center">
                                    <?= $this->Html->image('professores/'.$instrutor->professore->image,['alt' => 'foto professor '.$instrutor->professore->name, 'class' => 'text-center']); ?>
                                    <?php $name = explode(' ', $instrutor->professore->name); $name = $name[0].' '.end($name) ?>
                                    <p><?= $name ?></p>
                                    <p><?=  $instrutor->professore->funcao?></p>
                                </div> <!-- dados-professor -->
                            </div> <!-- instrutores-content -->
                        </div>
                    <?php } ?>
                </div>
            </div> <!-- row box-conteudo text-center -->
        <?php } ?>

        <?php if(count($avaliacoes) >= 1) { ?>
        <div id="avaliacoes" class="row box-conteudo text-center">
            <h3>Avaliações <span class="pull-right" onclick="openNav()"><button type="button" class="btn btn-success btn-sm"><i class="fa fa-star" aria-hidden="true"></i> Avaliar</button> </span></h3>
            <hr>
            <div class="avaliacoes">
                <div class="owl-carousel-3">
                    <?php foreach($avaliacoes as $avaliacao) { ?>
                        <?php $nome = explode(' ', $avaliacao->cliente->name); $first_name = $nome[0]; $last_name = end($nome); ?>
                        <div class="item">
                            <div class="avaliacao-content">
                                <div class="avatar-avaliador">
                                    <?= $this->Html->image('clientes/'.$avaliacao->cliente->image,['alt' => 'foto '.$avaliacao->cliente->name, 'class' => 'text-center']); ?>
                                </div>
                                <div class="dados-avaliador text-left">
                                    <p><?= $first_name ?> <?= $last_name ?></p>
                                    <p>
                                        <?php for($i = 1; $i <= 5; $i++) {
                                            if($i <= $avaliacao->rating) {
                                                echo '<i class="fa fa-star rating-dourado"></i>';
                                            } else {
                                                echo '<i class="fa fa-star"></i>';
                                            }
                                        } ?>
                                    </p>
                                </div>
                                <div class="comentario-avaliador">
                                    <p><?= $avaliacao->avaliacao ?></p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>

        <?php if($academia->latitude && $academia->longitude) { ?>
            <div id="mapa" class="row box-conteudo text-center">
                <h3>Localização</h3>
                <hr>
                <div class="mapa-content">
                    <div id="map"></div>
                </div>
            </div> <!-- row box-conteudo text-center -->
        <?php } ?>

        <div id="myNav" class="overlay">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <?php if(!$Cliente) { ?>
                <div class="overlay-content overlay-deslogado">
                    <?= $this->Form->create(null, ['url' => ['controller' => 'Clientes', 'action' => 'login', 'return-page']])?>
                        <h3>Faça login para avaliar a</h3>
                        <h4><?= $academia->shortname ?></h4>
                        <div class="col-xs-12 text-center">
                            <?= $this->Form->input('email', [
                                'type'          => 'text',
                                'div'           => false,
                                'label'         => false,
                                'class'         => 'form-control validate[required]',
                                'placeholder'   => 'E-MAIL ou CPF'
                            ])?>
                            <?= $this->Form->input('password', [
                                'id'            => 'login-password',
                                'div'           => false,
                                'label'         => false,
                                'class'         => 'form-control validate[required]',
                                'placeholder'   => 'SENHA',
                            ])?>
                            <div class="col-xs-12 col-md-6 forgot-password">
                                <?= $this->Html->link('Esqueci a senha',
                                    $SSlug.'/login/esqueci-a-senha',[
                                    'class' => 'white forgot-new',
                                    'style'         => 'font-size: 12px;',
                                ])?>
                            </div>
                            <div class="col-xs-12 col-md-6 btn-logins">
                                <p>
                                    <?= $this->Form->button('Entrar', [
                                        'type'      => 'submit',
                                        'class'     => 'btn btn-success btn-entrar',
                                        'escape'    => false
                                    ]) ?>
                                    <strong>ou</strong>
                                    <?= $this->Html->link('<button class="btn btn-info"><i class="fa fa-facebook"></i> Login</button>',
                                        '/fb-login/',
                                        ['escape' => false, 'style' => 'display: inline; font-size: 14px;']) ?>
                                </p>
                            </div>
                            <p></p>
                        </div>
                    <?= $this->Form->end() ?>
                </div>
            <?php } else if($Cliente->academia_id != $academia->id) { ?>
                <div class="overlay-content overlay-logado">
                    <h3>Você apenas pode avaliar a academia da qual você faz parte.</h3>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <h4>Visitar o perfil da minha academia</h4>
                            <?= $this->Html->link('<button class="btn btn-success">Ir agora</button>',WEBROOT_URL.'perfil/'.$cliente_acad->academia->slug,['escape' => false]) ?>
                        </div>
                        <div class="col-sm-6">
                            <h4>Alterar a minha academia</h4>
                            <?= $this->Html->link('<button class="btn btn-info">Alterar</button>',WEBROOT_URL.'minha-academia',['escape' => false]) ?>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="overlay-content overlay-logado">
                    <?= $this->Form->create(null, ['url' => ['controller' => 'Clientes', 'action' => 'avaliar_academia']]) ?>
                        <h3>Queremos sua opinião sobre a</h3>
                        <h4><?= $academia->shortname ?></h4>
                        <p><i class="fa fa-star fa-3x rating-icon rating1"></i><i class="fa fa-star fa-3x rating-icon rating2"></i><i class="fa fa-star fa-3x rating-icon rating3"></i><i class="fa fa-star fa-3x rating-icon rating4"></i><i class="fa fa-star fa-3x rating-icon rating5"></i></p>
                        <?= $this->Form->input('rating', [
                            'type'  => 'hidden',
                            'id'    => 'rating-hidden',
                            'value' => 0
                        ]) ?>
                        <?= $this->Form->input('academia_id', [
                            'type'  => 'hidden',
                            'value' => $academia->id
                        ]) ?>
                        <?= $this->Form->textarea('avaliacao', [
                            'class'       => 'form-control',
                            'rows'        => '3',
                            'placeholder' => 'Deixe seu comentário de até 100 caracteres',
                            'maxlength'   => '100'
                        ]) ?>
                        <div class="col-xs-12">
                            <button class="btn btn-success">Enviar</button>
                        </div>
                    <?= $this->Form->end() ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.rating1').click(function() {
                $('#rating-hidden').val('1');
                $('.rating-icon').removeClass('rating-dourado');
                $(this).addClass('rating-dourado');
            });

            $('.rating2').click(function() {
                $('#rating-hidden').val('2');
                $('.rating-icon').removeClass('rating-dourado');
                $('.rating1').addClass('rating-dourado');
                $(this).addClass('rating-dourado');
            });

            $('.rating3').click(function() {
                $('#rating-hidden').val('3');
                $('.rating-icon').removeClass('rating-dourado');
                $('.rating1').addClass('rating-dourado');
                $('.rating2').addClass('rating-dourado');
                $(this).addClass('rating-dourado');
            });

            $('.rating4').click(function() {
                $('#rating-hidden').val('4');
                $('.rating-icon').removeClass('rating-dourado');
                $('.rating1').addClass('rating-dourado');
                $('.rating2').addClass('rating-dourado');
                $('.rating3').addClass('rating-dourado');
                $(this).addClass('rating-dourado');
            });

            $('.rating5').click(function() {
                $('#rating-hidden').val('5');
                $('.rating-icon').addClass('rating-dourado');
            });
        });
    </script>

    <style type="text/css">
        .rating-dourado {
            color: #f3d012!important;
        }
        .dados-avaliador .fa {
            color: #73a1d9;
        }
    </style>

    <?php if($Avaliacoes_anchor) { ?>
        <script type="text/javascript">
            $(window).load(function() {
                $('html, body').animate({
                    scrollTop: $('#avaliacoes').offset().top
                }, 500);
                $('#myNav').height('100%');
            });
        </script>
    <?php } ?>

    <?php if($academia->latitude && $academia->longitude) { ?>
        <script>
            var map;
            function initMap() {
                var meulocal = {lat: <?= $academia->latitude ?>, lng: <?= $academia->longitude ?>};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 18,
                    center: meulocal,
                    scrollwheel: false,
                });
                var marker = new google.maps.Marker({
                    position: meulocal,
                    map: map,
                    title: "<?= $academia->shortname ?> :: <?= $academia->city->name ?> - <?= $academia->city->uf ?>",
                    icon: WEBROOT_URL + 'img/map-marker.png'
                });
            }
        </script>
    <?php } ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJCTAVhfiDaRSNBflCc-Mn2m6tsZo5oF4&callback=initMap"
async defer></script>

    <!-- OWL CAROUSEL -->
    <script> 
        $(document).ready(function() {
          $('.owl-carousel-1').owlCarousel({
            loop: true,
            margin: 10,
            itemsMobile: [600, 1],
            itemsDesktopSmall: [1024, 2],
            itemsDesktop: [6000, 5]
          });
          $('.owl-carousel-2').owlCarousel({
            loop: true,
            margin: 10,
            itemsMobile: [600, 1],
            itemsDesktopSmall: [1024, 2],
            itemsDesktop: [6000, 4],
          });
          $('.owl-carousel-3').owlCarousel({
            loop: true,
            margin: 10,
            itemsMobile: [600, 1],
            itemsDesktopSmall: [1024, 3],
            itemsDesktop: [6000, 4]
          });
          $('.owl-carousel-4').owlCarousel({
            loop: true,
            margin: 10,
            itemsMobile: [600, 1],
            itemsDesktopSmall: [1024, 2],
            itemsDesktop: [6000, 3]
          })
        })
    </script>
    <!-- FIM OWL CAROUSEL -->
    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox();
        });
    </script>