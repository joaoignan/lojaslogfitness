<?= $this->Html->script('jquery.mask.min.js')?>
<script type="text/javascript">
	$(document).ready(function() {
		$('.money').mask('000000000000000,00', {reverse: true});
		$("#form-plano-add").validationEngine();
	});
</script>

<style>
	.div-de-formulario{
		min-height: 80px;
	}
</style>

<section class="content">
	<div class="box box-info">
	    <div class="box-header with-border">
			<h3 class="box-title">Editar assinatura</h3>
			<span class="pull-right"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
				'/academia/admin/admin_mensalidades/planos',
				['escape' =>  false,
				 'class'  =>  'pull-right']) ?></span>
	    </div> <!-- box-header -->
    	<div class="box-body">
    		<div class="col-sm-12 col-md-6 text-center">
    			<h4>Detalhes sobre criação de planos/assinaturas</h4>
    			<h5>Assinatura</h5>
    			<p class="text-left">"Assinatura": você seleciona o tempo mínimo de assinatura e o sistema gera automaticamente a cobrança recorrente e mensal. Não compromete o limite do cartão de crédito. Você pode escolher também que quando atinge o tempo mínimo o sistema para de cobrar ou só para de cobrar quando o aluno pedir o cancelamento para a academia. O cancelamento da cobrança no segundo caso é por conta da academia e pode ser acompanhado aqui(link para assinaturas cancelamentos)</p>
    			<br>
    			<h5>Vendas limitadas</h5>
    			<p class="text-left">É muito legal para promoções ou aulas que tem uma quantidade máxima de participantes. Quando atingir o numero estabelecido o Plano não é mais comercializado mas é possível criar um novo plano com as mesmas condições com uma quantidade nova.</p>
    		</div>
    		<div class="col-sm-12 col-md-6 text-center">
    			<h4>Editar assinatura</h4>
    			<div class="row text-left">
		    		<?= $this->Form->create($academia_mensalidades_planos, ['id' => 'form-plano-add']) ?>
		    			<div class="col-xs-12 div-de-formulario">
				    		<label for="name" class="label-control">Nome da assinatura</label>
				    		<?= $this->Form->input('name', [
		    					'class'   	  => 'form-control validate[required]',
		    					'placeholder' => 'Escreva o nome do plano',
				    			'label'       => false,
		    					'div'         => false,
		    					'disabled'	  => true
		    				]) ?>
				    		<br>
			    		</div>
			    		<!-- <div class="col-sm-12 col-md-6 div-de-formulario">
		    				<label for="type">Tipo</label>
		    				<?= $this->Form->input('type', [
		    					'class'   => 'form-control validate[required]',
		    					'options' => [
		    						1 => 'Plano',
		    						2 => 'Assinatura'
		    					],
		    					'value'   => $academia_mensalidades_planos->type,
		    					'label'   => false,
		    					'disabled' => true
		    				]) ?>
		    				<br>
		    			</div> -->
		    			<div class="col-sm-12 col-md-6 div-de-formulario">
		    				<label for="valor_total">Valor total (em R$)</label>	
		    				<?= $this->Form->input('valor_total', [
		    					'type'  => 'text',
		    					'class' => 'form-control money validate[required]',
		    					'id'    => 'valor_total',
		    					'value' => number_format($academia_mensalidades_planos->valor_total, 2, '','.'),
		    					'label' => false,
		    					'div'   => false
		    				]) ?>
		    				<br>
		    			</div>
		    			<div class="col-sm-12 col-md-6 div-de-formulario">
		    				<label for="time_months">Duração mínima (em meses)</label>
		    				<?= $this->Form->input('time_months', [
		    					'type'  => 'number',
		    					'id'    => 'time_months',
		    					'class' => 'form-control validate[required]',
		    					'min'   => 1,
		    					'max'   => 99,
		    					'label' => false,
		    					'div'   => false,
		    					'disabled' => true
		    				]) ?>
		    				<br>
		    			</div>
		    			<script type="text/javascript">
		    				$(document).ready(function() {
		    					$('#valor_total').blur(function() {
		    						if($(this).val() != '' && $('#time_months').val() != '') {
		    							var qtd_mes = $('#time_months').val();
		    							var valor = $(this).val();
		    							valor = parseFloat(valor);
		    							var valor_mensal =  valor.toFixed(2) / qtd_mes;
		    							$('.calculo_valor_mensal').html(valor_mensal.toFixed(2));
		    						}
		    					});
		    				});
		    			</script>
		    			<div class="col-sm-12 col-md-6 div-de-formulario">
							<label for="">Valor mensal (em R$)</label>
							<p class="calculo_valor_mensal">
								<?= number_format($valor_mes = $academia_mensalidades_planos->valor_total/$academia_mensalidades_planos->time_months, 2, ',', ' '); ?>
							</p>
		    				<br>
		    			</div>
		    			<div class="col-sm-12 col-md-6 div-de-formulario">
		    				<div class="col-xs-12 no-padding">
		    					<label>Limite de venda</label>
	    					</div>
		    				<div class="col-xs-6 col-sm-2 col-md-6 no-padding">

		    					<input type="checkbox" name="opt_limite_vendas" <?= $academia_mensalidades_planos->limite_vendas >= 1 && $academia_mensalidades_planos->limite_vendas <= 9000 ? 'checked' : '' ?> value="1"> Sim
		    				</div>
		    				<div class="col-xs-6 col-sm-2 col-md-4 no-padding">
		    					<?= $this->Form->input('limite_vendas', [
		    						'type'  	  => 'number',
			    					'class' 	  => 'form-control validate[optional]',
			    					'placeholder' => 'Qtd',
			    					'label' 	  => false,
			    					'div'   	  => false
			    				]) ?>
		    				</div>
		    			</div>
		    			<div class="col-xs-12">
		    				<label for="descricao">Descrição</label>
		    				<?= $this->Form->textarea('descricao', [
		    					'class' 	  => 'form-control validate[optional]',
		    					'id'    	  => 'descricao_plano',
		    					'value'       => $academia_mensalidades_planos->descricao,
		    					'placeholder' => 'Ex: Horários válidos: Segunda-Sexta das 10:00 às 17:00',
		    					'rows'  	  => 4,
		    					'label' 	  => false,
		    					'div'   	  => false,
		    					'maxLength'	  => 200
		    				]) ?>
		    				<br>
		    			</div>
		    			<!-- <div class="col-sm-12 col-md-6 div-de-formulario">
		    				<label for="method_payment">Método de pagamento</label> <br>
		    				<?= $this->Form->input('method_payment', [
		    					'class'   => 'form-control validate[required]',
		    					'id'      => 'modo_pagamento',
		    					'options' => [
		    						1 => 'Boleto/Cartão',
		    						2 => 'Boleto',
		    						3 => 'Cartão'
		    					],
		    					'value'   => $academia_mensalidades_planos->method_payment,
		    					'label'   => false
		    				]) ?>
		    				<br>
		    			</div> -->
		    			<div class="col-xs-12 text-center">
		    				<br>
		    				<button class="btn btn-success">Salvar</button>
		    			</div>
		    		<?= $this->Form->end() ?>
	    		</div><!-- row text-left -->
    		</div><!-- col-sm-12 col-md-6 text-center -->
    	</div><!-- box-body -->
    </div> <!-- box box-info -->
</section>