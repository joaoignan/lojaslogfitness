<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>
<?= $this->Html->css('datepicker3') ?>

<?= $this->Html->script('jquery.mask.min.js')?>
<script>
    $(document).ready(function(){
        $(".cpf").mask("999.999.999-99");
        $(".cpf-mask").mask("999.999.999-99");
        $(".card_aniversario").mask('99/99/9999');
        $('#datepicker').datepicker({
          autoclose: true
        });
    });
</script>

<style>
    .btn-meus-dados{
        margin-top: 15px;
        width: 140px;
    }
    .inputs-cpf-cnpj input{
      display: none;
    }
    .inputs-cpf-cnpj input:checked + label .label-checkbox {
      border-color: #5089cf;
    }
    .inputs-cpf-cnpj input:checked + label{
      color: #5089cf;
    }
    .label-checkbox{
      padding: 15px;
      border: 2px solid #ddd;
      border-radius: 7px;
      width: 100px;
      display: inline-block;
      cursor: pointer;
      margin: 0 10px;
    }
    .label-checkbox label{
      cursor: pointer;
    }
    .cpf-div {
        display: none;
    }
    @media all and (max-width: 991px){
        .label-cartao{
            text-align: left!important;
        }
    }
</style>

<section class="content">
  <div class="col-xs-12 col-lg-10">
    <div class="box box-info">
      <div class="box-body">
        <div class="users form">
            <?php if($academia->card_status == 0) { ?>
                <?= $this->Form->create($academia, ['class' => 'form-horizontal bordered-row', 'url' => ['controller' => 'Academias', 'action' => 'salvar_dados_bancarios_cnpj', $comissao]]); ?>
                <fieldset>
                    <h3 class="box-header">Dados bancários <span class="pull-right">
                        <?= $this->Html->link('<button class="btn btn-success" type="button">Minhas Comissões</button>',
                            ['controller' => 'Academias', 'action' => 'admin_comissoes_index'], ['escape' => false]) ?>
                        <a href="http://blog.logfitness.com.br/conheca-o-logcard" target="_blank"><button class="btn bg-blue" type="button">Conheça o Logcard</button></a>
                    </span> </h3>
                    <div class="row">
                      <div class="col-xs-12 text-center total-cpf-cnpj">
                          <div class="inputs-cpf-cnpj">
                            <input type="radio" id="check-1" name="check-cpf-cnpj" class="check-cpf-cnpj check-cnpj" value="cnpj">
                            <label for="check-1">
                                <div class="label-checkbox">
                                    CNPJ
                                </div>
                            </label>
                            <input type="radio" id="check-2" name="check-cpf-cnpj" class="check-cpf-cnpj check-cpf" value="cpf"> 
                            <label for="check-2">
                                <div class="label-checkbox">
                                    CPF
                                </div>
                            </label>
                          </div>
                      </div>
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('.check-cpf-cnpj').click(function() {
                                var value = $(this).val();

                                if(value == 'cpf') {
                                    $('.cnpj-div').fadeOut();
                                    setTimeout(function() {
                                        $('.cpf-div').fadeIn();
                                    }, 250);
                                }

                                if(value == 'cnpj') {
                                    $('.cpf-div').fadeOut();
                                    setTimeout(function() {
                                        $('.cnpj-div').fadeIn();
                                    }, 250);
                                }
                            });
                        });
                    </script>

                    <?php if($academia->is_cpf == 0) { ?>
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('.check-cnpj').click();
                            });
                        </script>
                    <?php } else { ?>
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('.check-cpf').click();
                            });
                        </script>
                    <?php } ?>

                    <div class="col-md-12 cnpj-div">
                        <div class="form-group col-md-6">
                            <label for="cnpj" class="control-label">CNPJ</label>
                            <?= $this->Form->input('cnpj', [
                              'class'       => 'form-control',
                              'placeholder' => 'CNPJ',
                              'label'       => false,
                              'disabled'    => true
                            ]) ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="razao_social" class="control-label">Razão Social</label>
                            <?= $this->Form->input('name', [
                              'class'       => 'form-control',
                              'placeholder' => 'Razão Social da Academia',
                              'label'       => false,
                              'disabled'    => true
                            ]) ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tipo_conta" class="control-label">Tipo de conta</label>
                            <?= $this->Form->input('tipo_conta', [
                              'options' => [
                                'Selecione'      => 'Selecione',
                                'Conta Corrente' => 'Conta Corrente',
                                'Conta Poupança' => 'Conta Poupança'
                              ],
                              'class'   => 'form-control',
                              'label'   => false
                            ]) ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="banco" class="control-label">Banco</label>
                            <?= $this->Form->input('banco', [
                              'options' => [
                                'Banco do Brasil'   => 'Banco do Brasil',
                                'Bradesco'          => 'Bradesco',
                                'Caixa'             => 'Caixa Econ. Federal',
                                'Itaú'              => 'Itaú',
                                'Santander'         => 'Santander'
                              ],
                              'class'   => 'form-control banco-input',
                              'label'   => false
                            ]) ?>
                            <p><strong>Não tem nenhum desses bancos? Nos chame no chat para saber sobre o Logcard!</strong></p>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="col-md-12 no-padding">
                                <label for="agencia" class="control-label">Agência</label>
                            </div>
                            <div class="col-xs-8 no-padding">
                                <?= $this->Form->input('agencia', [
                                  'class'       => 'form-control agencia-input',
                                  'onkeypress'  => 'return event.charCode >= 48 && event.charCode <= 57',
                                  'placeholder' => 'Agência',
                                  'label'       => false
                                ]) ?>
                            </div>
                            <div class="col-xs-4">
                                <?= $this->Form->input('agencia_dv', [
                                  'class'       => 'form-control agencia-dv-input',
                                  'placeholder' => 'Digito',
                                  'label'       => false,
                                  'maxlength'   => 1
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="col-md-12 no-padding">
                                <label for="conta" class="control-label col-sm-2">Conta</label>
                            </div>
                            <div class="col-xs-8 no-padding">
                                <?= $this->Form->input('conta', [
                                  'class'       => 'form-control conta-input',
                                  'onkeypress'  => 'return event.charCode >= 48 && event.charCode <= 57',
                                  'placeholder' => 'Conta',
                                  'label'       => false
                                ]) ?>
                            </div>
                            <div class="col-xs-4">
                                <?= $this->Form->input('conta_dv', [
                                  'class'       => 'form-control conta-dv-input',
                                  'placeholder' => 'Digito',
                                  'label'       => false,
                                  'maxlength'   => 1
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="banco_obs" class="control-label">Instruções de pagamento</label>
                            <?= $this->Form->input('banco_obs', [
                                'type'     => 'textarea',
                                'class'    => 'form-control validate[optional]',
                                'label'    => false,
                                'placeholder'   =>
                                        'Caso haja alguma regra ou observação a ser feita em relação a depósitos/transferências na conta da academia, descreva aqui...',
                            ]) ?>
                        </div>
                        <div class="form-group col-md-12">
                            <h5 class="text-center">Obs.: Informe todos os dados de forma correta. Não nos responsabilizamos por transtornos causados pelo preenchimento incorreto dos dados</h5>
                            <div class="divider"></div>
                        </div>
                        <div class="text-center col-xs-12">
                            <div class="col-sm-12 col-md-12">
                                <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                                    ['class' => 'btn btn-alt btn-hoverrr btn-success form-bancarios-cnpj', 'style' =>  'width: 9.5em;',
                                        'escape' => false]); ?>
                            </div>
                        </div>

                        <script type="text/javascript">
                          $(document).ready(function() {
                            $('.form-bancarios-cnpj').click(function() {

                              $('.error-bancario').remove();
                              var error = 0;

                              if($('.cnpj-div .agencia-input').val().length < 4) {
                                $('.cnpj-div .agencia-input').parent().append('<p class="error-bancario" style="color: red">o número da agência deve ter no mínimo 4 dígitos. Exemplo: 9999</p>');
                                $('.cnpj-div .agencia-input').focus();
                                error = 1;
                              }
                              if($('.cnpj-div .conta-dv-input').val().length <= 0) {
                                $('.cnpj-div .conta-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da conta!</p>');
                                $('.cnpj-div .conta-dv-input').focus();
                                error = 1;
                              }

                              if($('.cnpj-div .banco-input').val() == 'Banco do Brasil') {
                                if($('.cnpj-div .agencia-dv-input').val().length <= 0) {
                                  $('.cnpj-div .agencia-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da agencia!</p>');
                                  $('.cnpj-div .agencia-dv-input').focus();
                                  error = 1;
                                }
                                if($('.cnpj-div .conta-input').val().length < 8) {
                                  $('.cnpj-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                  $('.cnpj-div .conta-input').focus();
                                  error = 1;
                                }
                              } else if($('.cnpj-div .banco-input').val() == 'Bradesco') {
                                if($('.cnpj-div .agencia-dv-input').val().length <= 0) {
                                  $('.cnpj-div .agencia-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da agencia!</p>');
                                  $('.cnpj-div .agencia-dv-input').focus();
                                  error = 1;
                                }
                                if($('.cnpj-div .conta-input').val().length < 8) {
                                  $('.cnpj-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                  $('.cnpj-div .conta-input').focus();
                                  error = 1;
                                }
                              } else if($('.cnpj-div .banco-input').val() == 'Caixa') {
                                if($('.cnpj-div .conta-input').val().length < 11) {
                                  $('.cnpj-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 11 dígitos. Exemplo: XXX99999999 (X: Operação)</p>');
                                  $('.cnpj-div .conta-input').focus();
                                  error = 1;
                                }
                              } else if($('.cnpj-div .banco-input').val() == 'Itaú') {
                                if($('.cnpj-div .conta-input').val().length < 5) {
                                  $('.cnpj-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 5 dígitos. Exemplo: 99999</p>');
                                  $('.cnpj-div .conta-input').focus();
                                  error = 1;
                                }
                              } else if($('.cnpj-div .banco-input').val() == 'Santander') {
                                if($('.cnpj-div .conta-input').val().length < 8) {
                                  $('.cnpj-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                  $('.cnpj-div .conta-input').focus();
                                  error = 1;
                                }
                              }

                              if(error == 1) {
                                return false;
                              } else {
                                $(this).get(0).submit();
                              }
                            });
                          });
                        </script>
                    </div>
                    <?= $this->Form->end(); ?>
                </fieldset>
                <fieldset>
                    <?= $this->Form->create($academia, ['class' => 'form-horizontal bordered-row', 'url' => ['controller' => 'Academias', 'action' => 'salvar_dados_bancarios_cpf', $comissao]]); ?>
                    <div class="col-md-12 cpf-div">
                        <div class="form-group col-md-6">
                            <label for="favorecido" class="control-label">Nome Titular Conta</label>
                            <?= $this->Form->input('favorecido', [
                                'class'    => 'form-control validate[required]',
                                'label'    => false
                            ]) ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="cpf_favorecido" class="control-label">CPF Titular Conta</label>
                            <?= $this->Form->input('cpf_favorecido', [
                                'class'    => 'form-control validate[required] cpf-mask',
                                'label'    => false
                            ]) ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tipo_conta" class="control-label">Tipo de conta</label>
                            <?= $this->Form->input('tipo_conta', [
                              'options' => [
                                'Selecione'      => 'Selecione',
                                'Conta Corrente' => 'Conta Corrente',
                                'Conta Poupança' => 'Conta Poupança'
                              ],
                              'class'   => 'form-control',
                              'label'   => false
                            ]) ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="banco" class="control-label">Banco</label>
                            <?= $this->Form->input('banco', [
                              'options' => [
                                'Banco do Brasil'   => 'Banco do Brasil',
                                'Bradesco'          => 'Bradesco',
                                'Caixa'             => 'Caixa Econ. Federal',
                                'Itaú'              => 'Itaú',
                                'Santander'         => 'Santander'
                              ],
                              'class'   => 'form-control banco-input',
                              'label'   => false
                            ]) ?>
                            <p><strong>Não tem nenhum desses bancos? Nos chame no chat para saber sobre o Logcard!</strong></p>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="col-md-12 no-padding">
                                <label for="agencia" class="control-label">Agência</label>
                            </div>
                            <div class="col-xs-8 no-padding">
                                <?= $this->Form->input('agencia', [
                                  'class'       => 'form-control agencia-input',
                                  'onkeypress'  => 'return event.charCode >= 48 && event.charCode <= 57',
                                  'placeholder' => 'Agência',
                                  'label'       => false
                                ]) ?>
                            </div>
                            <div class="col-xs-4">
                                <?= $this->Form->input('agencia_dv', [
                                  'class'       => 'form-control agencia-dv-input',
                                  'placeholder' => 'Digito',
                                  'label'       => false,
                                  'maxlength'   => 1
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="col-md-12 no-padding">
                                <label for="conta" class="control-label col-sm-2">Conta</label>
                            </div>
                            <div class="col-xs-8 no-padding">
                                <?= $this->Form->input('conta', [
                                  'class'       => 'form-control conta-input',
                                  'onkeypress'  => 'return event.charCode >= 48 && event.charCode <= 57',
                                  'placeholder' => 'Conta',
                                  'label'       => false
                                ]) ?>
                            </div>
                            <div class="col-xs-4">
                                <?= $this->Form->input('conta_dv', [
                                  'class'       => 'form-control conta-dv-input',
                                  'placeholder' => 'Digito',
                                  'label'       => false,
                                  'maxlength'   => 1
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="banco_obs" class="control-label">Instruções de pagamento</label>
                            <?= $this->Form->input('banco_obs', [
                                'type'     => 'textarea',
                                'class'    => 'form-control validate[optional]',
                                'label'    => false,
                                'placeholder'   =>
                                        'Caso haja alguma regra ou observação a ser feita em relação a depósitos/transferências na conta da academia, descreva aqui...',
                            ]) ?>
                        </div>
                        <div class="form-group col-md-12">
                            <h5 class="text-center">Obs.: Informe todos os dados de forma correta. Não nos responsabilizamos por transtornos causados pelo preenchimento incorreto dos dados</h5>
                            <div class="divider"></div>
                        </div>
                        <div class="text-center col-xs-12">
                            <div class="col-sm-12 col-md-12">
                                <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                                    ['class' => 'btn btn-alt btn-hoverrr btn-success form-bancarios-cpf', 'style' =>  'width: 9.5em;',
                                        'escape' => false]); ?>
                            </div>
                        </div>
                        <script type="text/javascript">
                          $(document).ready(function() {
                            $('.form-bancarios-cpf').click(function() {

                              $('.error-bancario').remove();
                              var error = 0;

                              if($('.cpf-div .agencia-input').val().length < 4) {
                                $('.cpf-div .agencia-input').parent().append('<p class="error-bancario" style="color: red">o número da agência deve ter no mínimo 4 dígitos. Exemplo: 9999</p>');
                                $('.cpf-div .agencia-input').focus();
                                error = 1;
                              }
                              if($('.cpf-div .conta-dv-input').val().length <= 0) {
                                $('.cpf-div .conta-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da conta!</p>');
                                $('.cpf-div .conta-dv-input').focus();
                                error = 1;
                              }

                              if($('.cpf-div .banco-input').val() == 'Banco do Brasil') {
                                if($('.cpf-div .agencia-dv-input').val().length <= 0) {
                                  $('.cpf-div .agencia-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da agencia!</p>');
                                  $('.cpf-div .agencia-dv-input').focus();
                                  error = 1;
                                }
                                if($('.cpf-div .conta-input').val().length < 8) {
                                  $('.cpf-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                  $('.cpf-div .conta-input').focus();
                                  error = 1;
                                }
                              } else if($('.cpf-div .banco-input').val() == 'Bradesco') {
                                if($('.cpf-div .agencia-dv-input').val().length <= 0) {
                                  $('.cpf-div .agencia-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da agencia!</p>');
                                  $('.cpf-div .agencia-dv-input').focus();
                                  error = 1;
                                }
                                if($('.cpf-div .conta-input').val().length < 8) {
                                  $('.cpf-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                  $('.cpf-div .conta-input').focus();
                                  error = 1;
                                }
                              } else if($('.cpf-div .banco-input').val() == 'Caixa') {
                                if($('.cpf-div .conta-input').val().length < 11) {
                                  $('.cpf-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 11 dígitos. Exemplo: XXX99999999 (X: Operação)</p>');
                                  $('.cpf-div .conta-input').focus();
                                  error = 1;
                                }
                              } else if($('.cpf-div .banco-input').val() == 'Itaú') {
                                if($('.cpf-div .conta-input').val().length < 5) {
                                  $('.cpf-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 5 dígitos. Exemplo: 99999</p>');
                                  $('.cpf-div .conta-input').focus();
                                  error = 1;
                                }
                              } else if($('.cpf-div .banco-input').val() == 'Santander') {
                                if($('.cpf-div .conta-input').val().length < 8) {
                                  $('.cpf-div .conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                  $('.cpf-div .conta-input').focus();
                                  error = 1;
                                }
                              }

                              if(error == 1) {
                                return false;
                              } else {
                                $(this).get(0).submit();
                              }
                            });
                          });
                        </script>
                    </div>
                    <?= $this->Form->end() ?>
                </fieldset>
            <?php } else if($academia->card_status == 1) { ?>
                <?= $this->Form->create($academia, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
                <fieldset>
                    <h3 class="box-header">Dados do cartão <span class="pull-right"><a href="http://blog.logfitness.com.br/conheca-o-logcard" target="_blank"><button class="btn bg-blue" type="button">Conheça o Logcard</button></a></span></h3>

                    <?php if($academia->card_name != null) { ?>
                    <div class="form-group">
                        <div class="col-xs-12 col-md-3 control-label label-cartao">
                            <?= $this->Form->label('card_name', 'Nome Completo') ?>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <?= $this->Form->input('card_name', [
                                'label'         => false,
                                'placeholder'   => 'Nome completo',
                                'class' => 'form-control validate[required]',
                                'disabled' => true
                            ]) ?>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="form-group">
                        <div class="col-xs-12 col-md-3 control-label label-cartao">
                            <?= $this->Form->label('card_name', 'Nome Completo') ?>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <?= $this->Form->input('card_name', [
                                'label'         => false,
                                'placeholder'   => 'Nome completo',
                                'class' => 'form-control validate[required]'
                            ]) ?>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if($academia->card_cpf != null) { ?>
                    <div class="form-group">
                        <div class="col-xs-12 col-md-3 control-label label-cartao">
                            <?= $this->Form->label('card_cpf', 'CPF') ?>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <?= $this->Form->input('card_cpf', [
                                'label'         => false,
                                'placeholder'   => 'CPF',
                                'class' => 'form-control validate[required] cpf',
                                'disabled' => true
                            ]) ?>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="form-group">
                        <div class="col-xs-12 col-md-3 control-label label-cartao">
                            <?= $this->Form->label('card_cpf', 'CPF') ?>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <?= $this->Form->input('card_cpf', [
                                'label'         => false,
                                'placeholder'   => 'CPF',
                                'class' => 'form-control validate[required] cpf'
                            ]) ?>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if($academia->card_birth != null) { ?>
                    <div class="form-group">
                        <div class="col-xs-12 col-md-3 control-label label-cartao">
                            <?= $this->Form->label('card_birth', 'Data de nascimento') ?>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <?= $this->Form->input('card_birth', [
                                'type'      => 'text',
                                'value'     => $academia->card_birth->format('d/m/Y'),
                                'label'         => false,
                                'placeholder'   => 'Data de nascimento',
                                'class' => 'form-control validate[required, custom[brDate]] card_aniversario',
                                'disabled' => true
                            ]) ?>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="form-group">
                        <div class="col-xs-12 col-md-3 control-label label-cartao">
                            <?= $this->Form->label('card_birth', 'Data de nascimento') ?>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <?= $this->Form->input('card_birth', [
                                'type'      => 'text',
                                'label'         => false,
                                'placeholder'   => 'Data de nascimento',
                                'class' => 'form-control validate[required, custom[brDate]] card_aniversario'
                            ]) ?>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if($academia->card_gender != null) { ?>
                    <div class="form-group">
                        <div class="col-xs-12 col-md-3 control-label label-cartao">
                            <?= $this->Form->label('card_gender', 'Gênero') ?>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <?= $this->Form->input('card_gender', [
                                'label'         => false,
                                'placeholder'   => 'Gênero',
                                'class' => 'form-control validate[required]',
                                'disabled' => true
                            ]) ?>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="form-group">
                        <div class="col-xs-12 col-md-3 control-label label-cartao">
                            <?= $this->Form->label('card_gender', 'Gênero') ?>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <?= $this->Form->input('card_gender', [
                                'label'         => false,
                                'placeholder'   => 'Gênero',
                                'options'       => ['Feminino' => 'Feminino', 'Masculino' => 'Masculino'],
                                'class' => 'form-control validate[required]'
                            ]) ?>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if($academia->card_mother != null) { ?>
                    <div class="form-group">
                        <div class="col-xs-12 col-md-3 control-label label-cartao">
                            <?= $this->Form->label('card_mother', 'Nome da mãe') ?>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <?= $this->Form->input('card_mother', [
                                'label'         => false,
                                'placeholder'   => 'Nome da mãe',
                                'class' => 'form-control validate[required]',
                                'disabled' => true
                            ]) ?>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="form-group">
                        <div class="col-xs-12 col-md-3 control-label label-cartao">
                            <?= $this->Form->label('card_mother', 'Nome da mãe') ?>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <?= $this->Form->input('card_mother', [
                                'label'         => false,
                                'placeholder'   => 'Nome da mãe',
                                'class' => 'form-control validate[required]'
                            ]) ?>
                        </div>
                    </div>
                    <?php } ?>

                    <div class="form-group">
                        <div class="col-xs-12 col-md-3 control-label label-cartao">
                            <?= $this->Form->label('card_id', 'ID do Cartão') ?>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <?= $this->Form->input('card_id', [
                                'label'         => false,
                                'type'  => 'text',
                                'class' => 'form-control validate[required]',
                                'readonly' => true
                            ]) ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12 col-md-3 control-label label-cartao">
                            <?= $this->Form->label('card_number', 'Número do Cartão') ?>
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <?= $this->Form->input('card_number', [
                                'label'         => false,
                                'class' => 'form-control validate[required]',
                                'readonly' => true
                            ]) ?>
                        </div>
                    </div>

                    <div class="divider"></div>
                    <h5 class="box-header text-center">Obs.: Informe todos os dados de forma correta. Não nos responsabilizamos por transtornos causados pelo preenchimento incorreto dos dados. <br>
                    Caso algum dado já preenchido esteja incorreto, entre em contato pelo nosso chat para que atualizemos seu cadastro.</h5>
                    <div class="divider"></div>

                    <div class="text-center col-xs-12">
                      <div class="col-sm-12 col-md-12">
                        <?= $this->Form->button('<i class="fa fa-check-square" aria-hidden="true"></i> <span> '.__('Submit').'</span>',
                            ['class' => 'btn btn-alt btn-hoverrr btn-success btn-meus-dados',
                                'escape' => false]); ?>
                        <?= $this->Html->link('<i class="fa fa-credit-card" aria-hidden="true"></i> Ativar cartão','https://meu.brasilprepagos.com.br/strategybox/login/activate_card',[
                                'escape'    =>  false,
                                'class'     =>  'btn btn-default btn-meus-dados',
                                'target'    =>  '_blank',
                            ]); ?>
                        <?= $this->Html->link('<i class="fa fa-usd" aria-hidden="true"></i> Consultar saldo','https://meu.brasilprepagos.com.br/strategybox/login',[
                                'escape'    =>  false,
                                'class'     =>  'btn btn-default btn-meus-dados',
                                'target'    =>  '_blank',
                            ]); ?>
                      </div>
                    </div>
                </fieldset>
                <?= $this->Form->end() ?>
            <?php } ?>
        </div>
      </div>
    </div>
  </div>
<?= $this->Html->script('bootstrap-datepicker')?>
</section>
