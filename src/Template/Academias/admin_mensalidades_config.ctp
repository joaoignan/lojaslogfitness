<style>
.tarifa{
    font-size: 32px;
}
.minus{
    font-size: 11px;
}
.parcelamento ul{
    list-style-type: none;
    padding: 0;
    margin-bottom: 0;
}
.parcelamento span{
    color: #5089cf;
}
.antecipacao{
    font-size: 32px;
    color: #5089cf;
    padding: 10px;
    border: 2px solid #5089cf;
}
hr{
  border-top: 2px solid rgba(243, 208, 18, 0.40);
  width: 95%;
}   
.hidden-box {
  display: none;
}
</style>
<style>
  *, *:before, *:after {
  box-sizing: border-box;
}

h1 {
  font-size: 2em;
  font-weight: normal;
  line-height: 1.3em;
  margin: 0 0 25px 0;
  text-align: center;
}

.section {
  text-align: center;
  padding: 10px;
}
@media all and (min-width: 700px) {
  .section {
    width: 50%;
    padding: 20px 10px;
    float: left;
  }
}
@media all and (min-width: 1200px) {
  .section {
    width: 25%;
  }
}

.section h2 {
  font-size: 1.3em;
  font-weight: normal;
  padding-bottom: 15px;
}
.section p {
  height: 35px;
}


/***  Generic toggle  ***/

input.toggle {
  position: absolute;
  left: -99999px;
}
input.toggle + label {
  position: relative;
  display: inline-block;
  width: 74px;
  height: 30px;
  text-indent: -99999px;
  cursor: pointer;
  color: transparent;
  background-color: #fff;
  border: 2px solid #ddd;
  border-radius: 20px;
  outline: none;
  -webkit-tap-highlight-color: transparent;
  -webkit-user-select: none;
  -moz-user-select: none;
  user-select: none;
  transition-duration: .2s;
  transition-property: background-color, border;
}
input.toggle + label:before {
  content: 'Não';
  position: absolute;
  top: 0px;
  right: 11px;
  text-indent: 0;
  font-size: 13px;
  line-height: 26px;
  color: #999;
  z-index: 3;
  text-transform: uppercase;
}
input.toggle + label:after {
  content: '';
  position: absolute;
  top: -1px;
  left: -1px;
  z-index: 2;
  width: 28px;
  height: 28px;
  background-color: #fff;
  border: 1px solid #ddd;
  border-radius: 100px;
  transform: translate3d(0, 0, 0);
  transition-duration: .2s;
  transition-property: transform, border;
}

input.toggle:checked + label {
  background-color: #5cb85c;
  border: 2px solid #5cb85c;
}
input.toggle:checked + label:before {
  content: 'Sim';
  right: auto;
  left: 15px;
  color: #fff;
}
input.toggle:checked + label:after {
  border-color: #5cb85c;
  transform: translate3d(44px, 0, 0);
}
</style>

<script type="text/javascript">
  $(document).ready(function() {
    $('#email_notificacoes_toggle').click(function() {
      if($(this).is(':checked')) {
        $('.email-notificacoes-box').slideDown();
      } else {
        $('.email-notificacoes-box').slideUp();
      }
    });
    $('#active_auto_antecipate').click(function() {
      if($(this).is(':checked')) {
        $('.antecipacao-box').slideDown();
      } else {
        $('.antecipacao-box').slideUp();
      }
    });

    $("#form-config-geral").validationEngine();

    $('.cpf-mask').mask('000.000.000-00');
  });
</script>

<?= $this->Html->script('jquery.mask.min.js')?>
<script type="text/javascript">$("#cnpj").mask("00.000.000/0000-00");</script>

<section class="content">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">LOGMensalidade Configurações</h3>
      </div>
    </div>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#config" data-toggle="tab">Config Geral</a></li>
            <li><a href="#banco" data-toggle="tab">Banco</a></li>
            <li><a href="#boleto" data-toggle="tab">Boleto</a></li>
            <li><a href="#cartao" data-toggle="tab">Cartão</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="banco">
                <div class="row">
                    <?= $this->Form->create($academia, ['type' => 'file']); ?>
                        <div class="form-group col-md-6">
                            <label for="mens_tipo_conta" class="control-label">Tipo de conta</label>
                            <?= $this->Form->input('mens_tipo_conta', [
                              'options' => [
                                'Selecione'      => 'Selecione',
                                'Conta Corrente' => 'Conta Corrente',
                                'Conta Poupança' => 'Conta Poupança'
                              ],
                              'class'   => 'form-control',
                              'label'   => false
                            ]) ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="mens_banco" class="control-label">Banco</label>
                            <?= $this->Form->input('mens_banco', [
                              'options' => [
                                'Banco do Brasil'   => 'Banco do Brasil',
                                'Bradesco'          => 'Bradesco',
                                'Caixa'             => 'Caixa Econ. Federal',
                                'Itaú'              => 'Itaú',
                                'Santander'         => 'Santander'
                              ],
                              'class'   => 'form-control banco-input',
                              'label'   => false
                            ]) ?>
                        </div>

                        <script type="text/javascript">
                          $(document).ready(function() {
                            $('#form-bancarios-btn').click(function() {

                              $('.error-bancario').remove();
                              var error = 0;

                              if($('.agencia-input').val().length < 4) {
                                $('.agencia-input').parent().append('<p class="error-bancario" style="color: red">o número da agência deve ter no mínimo 4 dígitos. Exemplo: 9999</p>');
                                $('.agencia-input').focus();
                                error = 1;
                              }
                              if($('.conta-dv-input').val().length <= 0) {
                                $('.conta-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da conta!</p>');
                                $('.conta-dv-input').focus();
                                error = 1;
                              }

                              if($('.banco-input').val() == 'Banco do Brasil') {
                                if($('.agencia-dv-input').val().length <= 0) {
                                  $('.agencia-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da agencia!</p>');
                                  $('.agencia-dv-input').focus();
                                  error = 1;
                                }
                                if($('.conta-input').val().length < 8) {
                                  $('.conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                  $('.conta-input').focus();
                                  error = 1;
                                }
                              } else if($('.banco-input').val() == 'Bradesco') {
                                if($('.agencia-dv-input').val().length <= 0) {
                                  $('.agencia-dv-input').parent().append('<p class="error-bancario" style="color: red">você esqueceu o dígito da agencia!</p>');
                                  $('.agencia-dv-input').focus();
                                  error = 1;
                                }
                                if($('.conta-input').val().length < 8) {
                                  $('.conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                  $('.conta-input').focus();
                                  error = 1;
                                }
                              } else if($('.banco-input').val() == 'Caixa') {
                                if($('.conta-input').val().length < 11) {
                                  $('.conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 11 dígitos. Exemplo: XXX99999999 (X: Operação)</p>');
                                  $('.conta-input').focus();
                                  error = 1;
                                }
                              } else if($('.banco-input').val() == 'Itaú') {
                                if($('.conta-input').val().length < 5) {
                                  $('.conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 5 dígitos. Exemplo: 99999</p>');
                                  $('.conta-input').focus();
                                  error = 1;
                                }
                              } else if($('.banco-input').val() == 'Santander') {
                                if($('.conta-input').val().length < 8) {
                                  $('.conta-input').parent().append('<p class="error-bancario" style="color: red">o número da conta deve ter no mínimo 8 dígitos. Exemplo: 99999999</p>');
                                  $('.conta-input').focus();
                                  error = 1;
                                }
                              }

                              if(error == 1) {
                                return false;
                              } else {
                                $(this).get(0).submit();
                              }
                            });
                          });
                        </script>

                        <div class="form-group col-md-6">
                            <div class="col-md-12 no-padding">
                                <label for="mens_agencia" class="control-label">Agência</label>
                            </div>
                            <div class="col-xs-8 no-padding">
                                <?= $this->Form->input('mens_agencia', [
                                  'class'       => 'form-control agencia-input',
                                  'onkeypress'  => 'return event.charCode >= 48 && event.charCode <= 57',
                                  'placeholder' => 'Agência',
                                  'label'       => false
                                ]) ?>
                            </div>
                            <div class="col-xs-4">
                                <?= $this->Form->input('mens_agencia_dv', [
                                  'class'       => 'form-control agencia-dv-input',
                                  'placeholder' => 'Digito',
                                  'label'       => false,
                                  'maxlength'   => 1
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="col-md-12 no-padding">
                                <label for="mens_conta" class="control-label col-sm-2">Conta</label>
                            </div>
                            <div class="col-xs-8 no-padding">
                                <?= $this->Form->input('mens_conta', [
                                  'class'       => 'form-control conta-input',
                                  'onkeypress'  => 'return event.charCode >= 48 && event.charCode <= 57',
                                  'placeholder' => 'Conta',
                                  'label'       => false
                                ]) ?>
                            </div>
                            <div class="col-xs-4">
                                <?= $this->Form->input('mens_conta_dv', [
                                  'class'       => 'form-control conta-dv-input',
                                  'placeholder' => 'Digito',
                                  'label'       => false,
                                  'maxlength'   => 1
                                ]) ?>
                            </div>
                        </div>
                        <?php if($academiamensalidade->opt_conta_cpf == 0) { ?>
                          <div class="form-group col-md-6">
                              <label for="razao_social" class="control-label">Razão Social</label>
                              <?= $this->Form->input('name', [
                                'class'       => 'form-control',
                                'placeholder' => 'Razão Social da Academia',
                                'label'       => false,
                                'disabled'    => true
                              ]) ?>
                          </div>
                          <div class="form-group col-md-6">
                              <label for="cnpj" class="control-label">CNPJ</label>
                              <?= $this->Form->input('cnpj', [
                                'class'       => 'form-control',
                                'placeholder' => 'CNPJ',
                                'label'       => false,
                                'disabled'    => true
                              ]) ?>
                          </div>
                        <?php } else { ?>
                          <div class="form-group col-md-6">
                            <label for="mens_conta" class="control-label">Nome do responsável</label>
                            <?php if($academia->mens_name) { ?>
                              <?= $this->Form->input('mens_name', [
                                'class'    => 'form-control validate[required]',
                                'value'    => $academia->mens_name,
                                'label'    => false,
                                'disabled' => true
                              ]) ?>
                            <?php } else { ?>
                              <?= $this->Form->input('mens_name', [
                                'class'    => 'form-control validate[required]',
                                'value'    => $academia->mens_name,
                                'label'    => false
                              ]) ?>
                            <?php } ?>
                          </div>
                          <div class="form-group col-md-6">
                            <label for="nome_conta" class="control-label">CPF do responsável</label>
                            <?php if($academia->mens_cpf) { ?>
                              <?= $this->Form->input('mens_cpf', [
                                'class'    => 'form-control validate[required] cpf-mask',
                                'value'    => $academia->mens_cpf,
                                'label'    => false,
                                'disabled' => true
                              ]) ?>
                            <?php } else { ?>
                              <?= $this->Form->input('mens_cpf', [
                                'class'    => 'form-control validate[required] cpf-mask',
                                'value'    => $academia->mens_cpf,
                                'label'    => false
                              ]) ?>
                            <?php } ?>
                          </div>
                        <?php } ?>

                        <div class="form-group col-md-12">
                            <label for="comprovante" class="control-label">Comprovante</label>
                            <?= $this->Form->input('imagem_comprovante', [
                              'type'  => 'file',
                              'label' => false,
                              'class' => 'form-control validate[optional, custom[validateMIME[image/jpeg|image/png]]]'
                            ]) ?>
                            <p class="help-block">Adicione um comprovante de domicílio bancário.</p>
                            <br>
                            <?php if($academia->image_comprovante != null) { ?>
                              <?= $this->Html->image('comprovantes/'.$academia->image_comprovante,['alt' => 'comprovante '.$academia->shortname, 'class' => 'text-center']); ?>
                            <?php } ?>
                        </div>
                        <div class="form-group text-left">
                            <div class="col-xs-12 col-md-6 col-md-offset-6">
                                <?= $this->Form->button('Salvar', [
                                  'class' => 'btn btn-success',
                                  'id'    => 'form-bancarios-btn'
                                ]) ?>
                            </div>
                        </div>
                    <?= $this->Form->end() ?>
                </div>
            </div> <!-- banco -->
            <div class="tab-pane active" id="config">
              <div class="row">
                <?= $this->Form->create(null, ['id' => 'form-config-geral', 'novalidate', 'url' => ['controller' => 'Academias', 'action' => 'salvar_mensalidades_config_geral']]) ?>
                  <div class="form-group col-xs-12">
                    <div class="row">
                      <div class="col-xs-6">
                        <label for="nome_conta" class="control-label">Nome da Academia</label>
                        <?= $this->Form->input(null, [
                          'class'    => 'form-control',
                          'id'       => 'nome_conta',
                          'value'    => $academia->name,
                          'disabled' => true
                        ]) ?>
                      </div>
                      <div class="col-xs-6">
                        <label for="cnpj_conta" class="control-label">CNPJ da Academia</label>
                        <?= $this->Form->input(null, [
                          'class'    => 'form-control',
                          'id'       => 'cnpj_conta',
                          'value'    => $academia->cnpj,
                          'disabled' => true
                        ]) ?>
                      </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                      <div class="col-xs-6">
                        <label for="mens_conta" class="control-label">Nome do responsável</label>
                        <?php if($academia->mens_name) { ?>
                          <?= $this->Form->input('mens_name', [
                            'class'    => 'form-control validate[required]',
                            'value'    => $academia->mens_name,
                            'label'    => false,
                            'disabled' => true
                          ]) ?>
                        <?php } else { ?>
                          <?= $this->Form->input('mens_name', [
                            'class'    => 'form-control validate[required]',
                            'value'    => $academia->mens_name,
                            'label'    => false
                          ]) ?>
                        <?php } ?>
                      </div>
                      <div class="col-xs-6">
                        <label for="nome_conta" class="control-label">CPF do responsável</label>
                        <?php if($academia->mens_cpf) { ?>
                          <?= $this->Form->input('mens_cpf', [
                            'class'    => 'form-control validate[required] cpf-mask',
                            'value'    => $academia->mens_cpf,
                            'label'    => false,
                            'disabled' => true
                          ]) ?>
                        <?php } else { ?>
                          <?= $this->Form->input('mens_cpf', [
                            'class'    => 'form-control validate[required] cpf-mask',
                            'value'    => $academia->mens_cpf,
                            'label'    => false
                          ]) ?>
                        <?php } ?>
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-xs-12 col-md-6">
                          <h4>Receber notificações de pagamentos</h4>
                          <p class="help-block">Habilita ou Desabilita o envio de emails notificando novos pagamentos.</p>
                      </div>
                      <div class="col-xs-12 col-md-6 text-rigth">
                        <input id="email_notificacoes_toggle" type="checkbox" class="toggle" <?= $academiamensalidade->email_notificacoes != null ? 'checked="true"' : '' ?> name="email_notificacoes_toggle"s></input>
                        <label for="email_notificacoes_toggle"></label>
                      </div>
                    </div>
                    <div class="row email-notificacoes-box <?= $academiamensalidade->email_notificacoes == null ? 'hidden-box' : '' ?>">
                      <div class="col-xs-12 col-md-6">
                        <label for="email_notificacoes" class="control-label">Enviar para:</label>
                        <input type="email" id="email_notificacoes" value="<?= $academiamensalidade->email_notificacoes ?>" name="email_notificacoes" class="form-control">
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-xs-12 col-md-6">
                          <h4>Transferência Automática</h4>
                          <p class="help-block">Habilita ou Desabilita a transferência automática para um de seus bancos. Obs: R$2,00 por transferência</p>
                      </div>
                      <div class="col-xs-12 col-md-6 text-rigth">
                        <input id="active_auto_transf" type="checkbox" class="toggle" <?= $academiamensalidade->active_auto_transf ? 'checked="true"' : '' ?> name="active_auto_transf"></input>
                        <label for="active_auto_transf"></label>
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-xs-12">
                        <h4>Antecipação de Recebíveis</h4>
                        <p class="help-block">Esta função permite configurar a antecipação automática do seu saldo. Por padrão, o repasse do saldo de transações realizadas com cartão de crédito, acontecem a cada 30 dias por parcela (30, 60, 90, etc.). Através desta função, você poderá determinar uma data para adiantar o recebimento do seu saldo.</p>
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-xs-12 col-md-6">
                          <h4>Antecipação Automática</h4>
                          <p class="help-block">Habilita ou Desabilita a função de antecipação automática.</p>
                      </div>
                      <div class="col-xs-12 col-md-6 text-rigth">
                        <input id="active_auto_antecipate" type="checkbox" class="toggle" <?= $academiamensalidade->opt_antecipate != 0 ? 'checked="true"' : '' ?> name="active_auto_antecipate"></input>
                        <label for="active_auto_antecipate"></label>
                      </div>
                    </div>
                    <div class="antecipacao-box <?= $academiamensalidade->opt_antecipate == 0 ? 'hidden-box' : '' ?>">
                      <div class="row">
                        <div class="col-xs-12 col-md-8">
                          <h4>Opções de Antecipação Automática</h4>
                          <p class="help-block">Escolha a recorrência da sua antecipação. Pode ser semanal, mensal ou um determinado número de dias após a transação. A tarifa de juros de antecipação será cobrada referente ao número de dias antecipados.</p>
                        </div>
                        <div class="col-xs-12 col-md-3">
                          <select name="opt_antecipate" id="opt_antecipate" class="form-control">
                            <option <?= $academiamensalidade->opt_antecipate == 1 ? 'selected="selected"' : '' ?> value="1">Diariamente</option>
                            <option <?= $academiamensalidade->opt_antecipate == 2 ? 'selected="selected"' : '' ?> value="2">Semanalmente</option>
                            <option <?= $academiamensalidade->opt_antecipate == 3 ? 'selected="selected"' : '' ?> value="3">Mensalmente</option>
                            <option <?= $academiamensalidade->opt_antecipate == 4 ? 'selected="selected"' : '' ?> value="4">Dia após pagamento</option>
                          </select>
                        </div>
                      </div>

                      <script type="text/javascript">
                        $(document).ready(function() {
                          $('#opt_antecipate').change(function() {
                            if($(this).val() == 1) {
                              $('.antecipacao-config').slideUp();
                            } else if($(this).val() == 2) {
                              $('.antecipacao-config').slideUp();
                              $('.antecipacao-semanal-box').slideDown();
                            } else if($(this).val() == 3) {
                              $('.antecipacao-config').slideUp();
                              $('.antecipacao-mensal-box').slideDown();
                            } else if($(this).val() == 4) {
                              $('.antecipacao-config').slideUp();
                              $('.antecipacao-dias-box').slideDown();
                            }
                          });
                        });
                      </script>

                      <div class="row antecipacao-config antecipacao-semanal-box <?= $academiamensalidade->opt_antecipate != 2 ? 'hidden-box' : '' ?>">
                        <div class="col-xs-12 col-md-8">
                          <h4>Antecipação Semanal</h4>
                          <p class="help-block">Escolha qual dia da semana deseja antecipar seus recebíveis e tê-los disponíveis para saque. Exemplo: Uma transação ocorreu na quinta-feira e a antecipação está configurada para acontecer às segundas-feiras (4 dias após), para esta transação serão cobradas 26 dias de antecipação.</p>
                        </div>
                        <div class="col-xs-12 col-md-3">
                          <select name="days_semanal_antecipate" id="opt_antecipate" class="form-control">
                            <option <?= $academiamensalidade->days_antecipate == 0 ? 'selected="selected"' : '' ?> value="0">Domingo</option>
                            <option <?= $academiamensalidade->days_antecipate == 1 ? 'selected="selected"' : '' ?> value="1">Segunda</option>
                            <option <?= $academiamensalidade->days_antecipate == 2 ? 'selected="selected"' : '' ?> value="2">Terça</option>
                            <option <?= $academiamensalidade->days_antecipate == 3 ? 'selected="selected"' : '' ?> value="3">Quarta</option>
                            <option <?= $academiamensalidade->days_antecipate == 4 ? 'selected="selected"' : '' ?> value="4">Quinta</option>
                            <option <?= $academiamensalidade->days_antecipate == 5 ? 'selected="selected"' : '' ?> value="5">Sexta</option>
                            <option <?= $academiamensalidade->days_antecipate == 6 ? 'selected="selected"' : '' ?> value="6">Sábado</option>
                          </select>
                        </div>
                      </div>
                      <div class="row antecipacao-config antecipacao-mensal-box <?= $academiamensalidade->opt_antecipate != 3 ? 'hidden-box' : '' ?>">
                        <div class="col-xs-12 col-md-8">
                          <h4>Antecipação Mensal</h4>
                          <p class="help-block">Escolha qual dia do mês deseja antecipar seus recebíveis e tê-los disponíveis para saque. Exemplo: A transação ocorreu no dia 10 e a antecipação está configurada para acontecer todo dia 28, para esta transação serão cobrados 12 dias de antecipação.</p>
                        </div>
                        <div class="col-xs-12 col-md-3">
                          <input name="days_mensal_antecipate" placeholder="Escolha o dia" value="<?= $academiamensalidade->days_antecipate ?>" type="number" min="1" max="31" class="form-control validate[optional]">
                        </div>
                      </div>
                      <div class="row antecipacao-config antecipacao-dias-box <?= $academiamensalidade->opt_antecipate != 4 ? 'hidden-box' : '' ?>">
                        <div class="col-xs-12 col-md-8">
                          <h4>Dias Após Pagamento</h4>
                          <p class="help-block">Escolha quantos dias após o pagamento deseja antecipar seus recebíveis e tê-los disponíveis para saque. Exemplo: A antecipação foi configurada para acontecer 14 dias após a transação. Neste caso, serão cobradas 16 dias de antecipação.</p>
                        </div>
                        <div class="col-xs-12 col-md-3s">
                          <input name="days_dias_antecipate" placeholder="Informe quantos dias" value="<?= $academiamensalidade->days_antecipate ?>" type="number" min="1" max="31" class="form-control validate[optional]">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-12 col-md-6 col-md-offset-6">
                        <?= $this->Form->button('Salvar', [
                          'class'       => 'btn btn-success',
                        ]) ?>
                      </div>
                    </div>
                  </div>
                <?= $this->Form->end() ?>
              </div>
            </div> <!-- config -->
            <div class="tab-pane" id="boleto">
              <div class="row">
                <?= $this->Form->create(null, ['url' => ['controller' => 'Academias', 'action' => 'salvar_mensalidades_config_boleto']]) ?>
                  <div class="form-group col-xs-12">
                    <div class="row">
                      <div class="col-xs-6">
                          <h4>Ativo</h4>
                          <p class="help-block">Habilita ou desabilita esta forma de pagamento</p>
                      </div>
                      <div class="col-xs-6 text-rigth">
                        <input id="active_boleto" type="checkbox" class="toggle" <?= $academiamensalidade->opt_boleto == 1 ? 'checked="true"' : '' ?> name="opt_boleto"></input>
                        <label for="active_boleto"></label>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <p>Ao habilitar o recebimento de faturas com boleto, seus alunos poderão realizar pagamentos através da emissão de boletos bancários. A gestão destes pagamentos é feita de forma automática.</p>
                        </div>
                    </div>
                    <div class="row text-left">
                      <div class="col-xs-6">
                        <h4>Customizar vencimento</h4>
                        <p class="help-block">Informe quantos dias após o vencimento, o boleto ainda poderá ser pago.</p>
                      </div>
                      <div class="col-xs-6 col-md-2 text-rigth">
                        <input type="number" name="venc_boleto" min="1" max="31" class=" form-control">
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-xs-12 col-md-6 col-md-offset-6">
                      <button class="btn btn-success">Salvar</button>
                  </div>
                <?= $this->Form->end() ?>
                <hr>
                <div class="col-xs-12 text-center">
                  <div class="row">
                    <h3>Taxas e Tarifas</h3>
                    <div class="col-xs-12 parcelamento">
                      <ul>
                        <li><p>Tarifa por transação <span>R$<?= number_format($mensalidade_categoria->tax_boleto, 2, ',','.') ?></span></p></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div> <!-- boleto -->
            <div class="tab-pane" id="cartao">
              <div class="row">
                <?= $this->Form->create(null, ['novalidate', 'url' => ['controller' => 'Academias', 'action' => 'salvar_mensalidades_config_card']]) ?>
                  <div class="form-group col-xs-12">
                    <div class="row">
                      <div class="col-xs-6">
                          <h4>Ativo</h4>
                          <p class="help-block">Habilita ou desabilita esta forma de pagamento</p>
                      </div>
                      <div class="col-xs-6 text-rigth">
                        <input id="active_cartao" type="checkbox" class="toggle" <?= $academiamensalidade->opt_card == 1 ? 'checked="true"' : '' ?> name="opt_card"></input>
                        <label for="active_cartao"></label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                      <div class="row">
                          <div class="col-xs-6">
                              <h4>Parcelas</h4>
                              <p class="help-block">Habilita ou desabilita parcelas</p>
                          </div>
                          <div class="col-xs-6 text-rigth">
                            <input id="active_parcelas_cartao" type="checkbox" class="toggle" <?= $academiamensalidade->parcelas_card != 0 ? 'checked="true"' : '' ?> name="active_parcelas_cartao"></input>
                            <label for="active_parcelas_cartao"></label>
                          </div>
                      </div>
                  </div>
                  <!-- <div class="form-group col-xs-12">
                      <div class="row">
                          <div class="col-xs-6">
                              <h4>Repasse de juros</h4>
                              <p class="help-block">Habilita ou desabilita o repasse de juros para o aluno</p>
                          </div>
                          <div class="col-xs-6 text-rigth">
                            <input id="opt_repasse" type="checkbox" class="toggle" <?= $academiamensalidade->opt_repasse == 1 ? 'checked="true"' : '' ?> name="opt_repasse"></input>
                            <label for="opt_repasse"></label>
                          </div>
                      </div>
                  </div> -->

                  <script type="text/javascript">
                    $(document).ready(function() {
                      $('#active_parcelas_cartao').click(function() {
                        if($(this).is(':checked')) {
                          $('.limite-parcelas-box').slideDown();
                        } else {
                          $('.limite-parcelas-box').slideUp();
                        }
                      });
                    });
                  </script>

                  <div class="form-group col-xs-12 limite-parcelas-box <?= $academiamensalidade->parcelas_card == 0 ? 'hidden-box' : '' ?>">
                      <div class="row">
                          <div class="col-xs-6">
                              <h4>Máximo de Parcelas</h4>
                              <p class="help-block">Limite máximo de parcelas (entre 1 e 12)</p>
                          </div>
                          <div class="col-xs-6 col-md-2 text-rigth">
                              <input type="number" name="parcelas_card" value="<?= $academiamensalidade->parcelas_card ?>" min="1" max="12" class=" form-control">
                          </div>
                      </div>
                  </div>
                  <div class="form-group col-xs-12 col-md-6 col-md-offset-6">
                      <button class="btn btn-success">Salvar</button>
                  </div>
                <?= $this->Form->end() ?>
                <hr>
                <div class="col-xs-12 text-center">
                    <div class="row">
                        <h3>Tarifas de transação</h3>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="tarifa"><strong>R$<?= number_format($mensalidade_categoria->tax_card_valor1, 2, ',','.') ?></strong> <i class="fa fa-plus"></i> <strong><?= number_format($mensalidade_categoria->tax_card_porcent1, 2, ',','.') ?>%</strong> <i class="fa fa-arrow-right" aria-hidden="true"></i> <strong>Receba em 30 dias*</strong></p>
                            <p class="help-block minus">* Transferências só acontecem de segunda a sexta, exceto feriados vigentes no município de São Paulo - Brasil</p>
                        </div>
                    </div>
                    <div class="row">
                        <h4>Tarifas com parcelamento</h4>
                        <div class="col-xs-12 parcelamento">
                            <ul>
                                <li><p>2x a 3x = <span>R$<?= number_format($mensalidade_categoria->tax_card_valor2, 2, ',','.') ?> + <?= number_format($mensalidade_categoria->tax_card_porcent2, 2, ',','.') ?>%</span></p></li>
                                <li><p>4x a 6x = <span>R$<?= number_format($mensalidade_categoria->tax_card_valor3, 2, ',','.') ?> + <?= number_format($mensalidade_categoria->tax_card_porcent3, 2, ',','.') ?>%</span></p></li>
                                <li><p>7x a 12x = <span>R$<?= number_format($mensalidade_categoria->tax_card_valor4, 2, ',','.') ?> + <?= number_format($mensalidade_categoria->tax_card_porcent4, 2, ',','.') ?>%</span></p></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <h4>Taxa de antecipação</h4>
                        <div class="col-xs-12 text-left">
                            <p>Você tem a opção de antecipar o recebimento de suas vendas realizadas no crédito à vista ou parcelado sempre que precisar.</p>
                            <p>A taxa de juros é cobrada com base no números de dias antecipados.</p>
                        </div>
                        <div class="col-xs-12 col-md-8 col-md-offset-2">
                            <p class="antecipacao"><?= number_format($mensalidade_categoria->tax_antecipacao, 2, ',','.') ?>% de juros ao mês</p>
                        </div>
                    </div>
                </div>
              </div>
            </div> <!-- cartao -->
        </div> <!-- tab-content -->
    </div><!-- nav-tabs-custom -->
</section>

<script>
  function animateToggle(targetElem) {
  targetElem.classList.add('changing');
  setTimeout(function() {
    targetElem.classList.remove('changing');
  }, 300)
}

var toggles = document.querySelectorAll('.android-5 .toggle');
for (var i = 0; i < toggles.length; i++) {
  toggles[i].addEventListener('change', function(e) {
    animateToggle(e.target);
  });
}
</script>
