<div class="content-box">
    <h3 class="content-box-header bg-default">
        <?= __('Actions') ?>
        <div class="header-buttons-separator">
            <a class="icon-separator toggle-button" href="#">
                <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
            </a>
        </div>
    </h3>
    <div class="content-box-wrapper hidee">
        <?= link_button_list(__('Listar Banners'), '/academia/admin/banners/'); ?>
    </div>
</div>
<div class="banners view" xmlns="http://www.w3.org/1999/html">
    <h2><?= h($banner->title) ?></h2>
    <div class="row">
        <div class="col-sm-9">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Title') ?></td>
                    <td><?= h($banner->title) ?></td>
                </tr>
                <tr>
                  <a href="<?= IMG_URL; ?>banners/<?= $banner->image ?>" class="fancybox">
                    <td class="font-bold"><?= __('Image') ?></td>
                    <td><?= $this->Html->image('banners/'.$banner->image) ?></td>
                  </a>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Status') ?></td>
                    <td><?= $banner->status->name ?></td>
                </tr>
            </table>
        </div>

        <div class="col-md-3">
            <table class="table table-hover">
                <tr>
                    <td class="font-bold"><?= __('Id') ?></td>
                    <td><?= $this->Number->format($banner->id) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Created') ?></td>
                    <td><?= h($banner->created) ?></td>
                </tr>
                <tr>
                    <td class="font-bold"><?= __('Modified') ?></td>
                    <td><?= h($banner->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
