<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>

<?= $this->Html->script('jquery.mask.min.js')?>
<script type="text/javascript">$(".phonemask").mask("(00) 00000-0009");</script>
<script type="text/javascript">$(".postalcode").mask("00000-000");</script>
<script type="text/javascript">$(".cpfmask").mask("000.000.000-00");</script>
<script type="text/javascript">
    $(document).ready(function() {
        //Quando o campo cep perde o foco.
        $("#cep").blur(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');
            console.log(cep);

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#address").val("...");
                    $("#area").val("...");
                    //$("#city").val("...");
                    //$("#states").val("...");
                    $("#ibge").val("...");

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            console.log(dados);
                            //Atualiza os campos com os valores da consulta.
                            $("#address").val(dados.logradouro);
                            $("#area").val(dados.bairro);

                            var uf = '';

                            switch (dados.uf){
                                case 'AC' : uf = 1; break;
                                case 'AL' : uf = 2; break;
                                case 'AM' : uf = 3; break;
                                case 'AP' : uf = 4; break;
                                case 'BA' : uf = 5; break;
                                case 'CE' : uf = 6; break;
                                case 'DF' : uf = 7; break;
                                case 'ES' : uf = 8; break;
                                case 'GO' : uf = 9; break;
                                case 'MA' : uf = 10; break;
                                case 'MG' : uf = 11; break;
                                case 'MS' : uf = 12; break;
                                case 'MT' : uf = 13; break;
                                case 'PA' : uf = 14; break;
                                case 'PB' : uf = 15; break;
                                case 'PE' : uf = 16; break;
                                case 'PI' : uf = 17; break;
                                case 'PR' : uf = 18; break;
                                case 'RJ' : uf = 19; break;
                                case 'RN' : uf = 20; break;
                                case 'RO' : uf = 21; break;
                                case 'RR' : uf = 22; break;
                                case 'RS' : uf = 23; break;
                                case 'SC' : uf = 24; break;
                                case 'SE' : uf = 25; break;
                                case 'SP' : uf = 26; break;
                                case 'TO' : uf = 27; break;
                                default : uf = '';
                            }

                            //$("#states").val(uf);

                            /*var state_id = $('#states').val();
                            $.get(WEBROOT_URL + '/cidades/' + state_id,
                                function(data_options){
                                    $('#city').html(data_options);
                                });

                            setTimeout(function(){
                                var $dd = $('#city');
                                var $options = $('option', $dd);
                                $options.each(function() {
                                    if ($(this).text() == dados.localidade) {
                                        $(this).attr('selected', true);
                                    }
                                });
                            }, 700);*/

                            //$("#ibge").val(dados.ibge);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulário_cep();
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep();
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulário_cep();
            }
        });

        //Quando o campo cep perde o foco.
        $("#cep-acad").blur(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');
            console.log(cep);

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#address-acad").val("...");
                    $("#area-acad").val("...");
                    //$("#city").val("...");
                    //$("#states").val("...");
                    $("#ibge").val("...");

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            console.log(dados);
                            //Atualiza os campos com os valores da consulta.
                            $("#address-acad").val(dados.logradouro);
                            $("#area-acad").val(dados.bairro);

                            var uf = '';

                            switch (dados.uf){
                                case 'AC' : uf = 1; break;
                                case 'AL' : uf = 2; break;
                                case 'AM' : uf = 3; break;
                                case 'AP' : uf = 4; break;
                                case 'BA' : uf = 5; break;
                                case 'CE' : uf = 6; break;
                                case 'DF' : uf = 7; break;
                                case 'ES' : uf = 8; break;
                                case 'GO' : uf = 9; break;
                                case 'MA' : uf = 10; break;
                                case 'MG' : uf = 11; break;
                                case 'MS' : uf = 12; break;
                                case 'MT' : uf = 13; break;
                                case 'PA' : uf = 14; break;
                                case 'PB' : uf = 15; break;
                                case 'PE' : uf = 16; break;
                                case 'PI' : uf = 17; break;
                                case 'PR' : uf = 18; break;
                                case 'RJ' : uf = 19; break;
                                case 'RN' : uf = 20; break;
                                case 'RO' : uf = 21; break;
                                case 'RR' : uf = 22; break;
                                case 'RS' : uf = 23; break;
                                case 'SC' : uf = 24; break;
                                case 'SE' : uf = 25; break;
                                case 'SP' : uf = 26; break;
                                case 'TO' : uf = 27; break;
                                default : uf = '';
                            }

                            //$("#states").val(uf);

                            /*var state_id = $('#states').val();
                            $.get(WEBROOT_URL + '/cidades/' + state_id,
                                function(data_options){
                                    $('#city').html(data_options);
                                });

                            setTimeout(function(){
                                var $dd = $('#city');
                                var $options = $('option', $dd);
                                $options.each(function() {
                                    if ($(this).text() == dados.localidade) {
                                        $(this).attr('selected', true);
                                    }
                                });
                            }, 700);*/

                            //$("#ibge").val(dados.ibge);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulário_cep_acad();
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep_acad();
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulário_cep_acad();
            }
        });
    })
</script>

<style type="text/css">
    .jcrop-holder #preview-pane {
        display: block;
        position: absolute;
        z-index: 2000;
        top: 10px;
        right: -280px;
        padding: 6px;
        border: 1px rgba(0,0,0,.4) solid;
        background-color: white;

        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;

        -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
        box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
    }

    #preview-pane .preview-container {
        width: 160px;
        height: 95px;
        overflow: hidden;
    }

    
</style>

<section class="content">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-body">
                <div class="academias form">
                    <?= $this->Form->create($academia, ['class' => 'form-horizontal bordered-row form-editar-dados', 'type' => 'file']); ?>
                    <fieldset>
                        <h3 class="box-header">Dados cadastrais</h3>
                        <h4 class="font-bold font-italic box-header"><span class="fa fa-picture-o" aria-hidden="true"></span> Logo</h4>
                        <br class="clear">

                        <script type="text/javascript">
                            $(window).load(function() {
                                function readURL(input) {
                                    if (input.files && input.files[0]) {
                                        var reader = new FileReader();
                                        
                                        reader.onload = function (e) {
                                            $('.image_preview').attr('src', e.target.result);

                                            $('.image_preview').Jcrop({
                                                boxHeight: 500,
                                                boxWidth: 500,
                                                setSelect: [0, 0, 0, 0],
                                                allowSelect: false,
                                                minSize: [20, 85]
                                            }, function () {

                                                var jcrop_api = this;

                                                $(".jcrop-box").attr('type', 'button');

                                                $('.image_preview').Jcrop('animateTo',[0,0,4000,4000]);

                                                jcrop_api.ui.selection.element.on('cropmove',function(e,s,c){
                                                    $('#cropx').val(c.x);
                                                    $('#cropy').val(c.y);
                                                    $('#cropw').val(c.w);
                                                    $('#croph').val(c.h);
                                                    $('#cropwidth').val($('.jcrop-box').width());
                                                    $('#cropheight').val($('.jcrop-box').height());
                                                });
                                            });
                                        }
                                        
                                        reader.readAsDataURL(input.files[0]);
                                    }
                                }
                                
                                $("#image_upload").change(function(){
                                    readURL(this);
                                });
                            });
                        </script>

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('imagem', 'Logotipo') ?>
                            </div>
                            <div class="col-sm-7">
                            <?= $this->Form->input('imagem', [
                                'id'    => 'image_upload',
                                'type'  => 'file',
                                'label' => false,
                                'class' => 'form-control validate[optional]'
                            ]) ?>
                            <p><strong>Selecione um arquivo de imagem entre 85x85 e 4000x4000 pixels</strong></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('image_preview', 'Prévia do Logotipo') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Html->image('academias/'.$academia->image, ['class' => 'image_preview'])?>
                            </div>
                        </div>

                        <input type="hidden" name="cropx" id="cropx" value="0" />
                        <input type="hidden" name="cropy" id="cropy" value="0" />
                        <input type="hidden" name="cropw" id="cropw" value="0" />
                        <input type="hidden" name="croph" id="croph" value="0" />
                        <input type="hidden" name="cropwidth" id="cropwidth" value="0" />
                        <input type="hidden" name="cropheight" id="cropheight" value="0" />

                        <div class="form-group text-center col-xs-12">
                            <div class="col-sm-12">
                                <?= $this->Form->button('<i class="fa fa-check-square-o" aria-hidden="true"></i><span> '.__('Cortar/Salvar imagem').'</span>', ['class' => 'btn btn-alt btn-hoverrr btn-success', 'escape' => false]); ?>
                            </div>
                        </div>
                        <br>
                        <br>
                        <h4 class="box-header"><span class="fa fa-list" aria-hidden="true"></span> Dados</h4>
                        <br class="clear">

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('name') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('name', [
                                    'label' => false,
                                    'disabled'  => true,
                                    'readonly'  => true,
                                    'class' => 'form-control validate[required]'
                                ]) ?>
                                <?= $this->Form->input('img_name', [
                                    'label' => $academia->shortname,
                                    'type'  => 'hidden',
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('shortname') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('shortname', [
                                    'maxlenght' => 22,
                                    'label'     => false,
                                    'class'     => 'form-control validate[required,maxSize[22]]'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('slug', 'URL Personalizada') ?>
                            </div>
                            <div class="col-sm-7">
                                <div class="input-group">
                                <span class="input-group-addon">logfitness.com.br/</span>
                                <?= $this->Form->input('slug', [
                                    'id'                => 'slug',
                                    'placeholder'       => 'Complemento da URL logfitness.com.br/',
                                    'label'             => false,
                                    'class'             => 'form-control validate[required,custom[slug]]'
                                ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('facebook_url', 'Link do Facebook') ?>
                            </div>
                            <div class="col-sm-7">
                                <div class="input-group">
                                <span class="input-group-addon">facebook.com.br/</span>
                                <?= $this->Form->input('facebook_url', [
                                    'id'                => 'facebook_url',
                                    'placeholder'       => 'Complemento da URL facebook.com.br/',
                                    'label'             => false,
                                    'class'             => 'form-control validate[optional]'
                                ]) ?>
                                </div>
                            </div>
                        </div>
                        <?php if($academia->cpf) { ?>
                            <div class="form-group">
                                <div class="col-sm-4 col-md-2 control-label">
                                    <?= $this->Form->label('cpf') ?>
                                </div>
                                <div class="col-sm-7">
                                    <?= $this->Form->input('cpf', [
                                        'label' => false,
                                        'disabled'  => true,
                                        'readonly'  => true,
                                        'class' => 'form-control validate[required,custom[cpf]] cpf'
                                    ]) ?>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="form-group">
                                <div class="col-sm-4 col-md-2 control-label">
                                    <?= $this->Form->label('cnpj') ?>
                                </div>
                                <div class="col-sm-7">
                                    <?= $this->Form->input('cnpj', [
                                        'label' => false,
                                        'disabled'  => true,
                                        'readonly'  => true,
                                        'class' => 'form-control validate[required,custom[cnpj]] cnpj'
                                    ]) ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('ie') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('ie', [
                                    'label' => false,
                                    'disabled'  => true,
                                    'readonly'  => true,
                                    'class' => 'form-control validate[required]'
                                ]) ?>
                            </div>
                        </div>  
                        <div class="divider"></div>
                        <h4 class="box-header"><span class="fa fa-phone-square" aria-hidden="true"></span> Contato</h4>
                        <br class="clear">

                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('email') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('email', [
                                    'label' => false,
                                    'disabled'  => true,
                                    'readonly'  => true,
                                    'class' => 'form-control validate[required, custom[email]]'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('contact', 'Responsável') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('contact', [
                                    'label' => false,
                                    'class' => 'form-control validate[optional]'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('phone') ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $this->Form->input('phone', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[required] phone phonemask'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('mobile', 'Celular/WhatsApp') ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $this->Form->input('mobile', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[optional] phone phonemask'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('contact_partner', 'Segundo Responsável') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('contact_partner', [
                                    'label' => false,
                                    'class' => 'form-control validate[optional]'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('phone_partner', 'Telefone') ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $this->Form->input('phone_partner', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[optional] phone phonemask'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('mobile_partner', 'Celular/WhatsApp') ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $this->Form->input('mobile_partner', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[optional] phone phonemask'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('alunos', 'Quantidade de Alunos') ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $this->Form->input('alunos', [
                                    'placeholder' => '',
                                    'label' => false,
                                    'class' => 'form-control validate[optional, onlyNumber]'
                                ]) ?>
                            </div>
                        </div>
                        <div class="divider"></div>
                        <h4 class="box-header"><span class="fa fa-building" aria-hidden="true"></span> Endereço</h4>
                        <br class="clear">
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('cep') ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $this->Form->input('cep', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[required] cep postalcode'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('address') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('address', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[required] address-map'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('number') ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $this->Form->input('number', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[optional] number-map'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('complement') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('complement', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[optional]'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('area') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('area', [
                                    'label' => false,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'class' => 'form-control validate[required] area-map'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('state_id') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('state_id', [
                                    'options'       => $states,
                                    'id'            => 'states',
                                    'value'         => $academia->city->state->id,
                                    'empty' => 'Selecione um estado',
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'label' => false,
                                    'class' => 'form-control chosen-select validate[required] uf-map'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('city_id') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('city_id', [
                                    'id'            => 'city',
                                    'value'         => $academia->city->id,
                                    'options' => $cities,
                                    'empty' => 'Selecione uma cidade',
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'label' => false,
                                    'class' => 'form-control validate[required] city-map'
                                ]) ?>
                            </div>
                        </div>
                        <?= $this->Form->input('latitude', [
                            'id'    => 'latitude-att',
                            'type'  => 'hidden',
                            'value' => $academia->latitude,
                            'label' => false
                        ]) ?>
                        <?= $this->Form->input('longitude', [
                            'id'    => 'longitude-att',
                            'type'  => 'hidden',
                            'value' => $academia->longitude,
                            'label' => false
                        ]) ?>
                        <br>
                        <div class="divider"></div>
                        <h4 class="box-header"><span class="fa fa-money" aria-hidden="true"></span> Dados Bancários</h4>
                        <br class="clear">
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('favorecido') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('favorecido', [
                                    'id'            => 'favorecido',
                                    'value'         => $academia->favorecido,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'label' => false,
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('cpf_favorecido') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('cpf_favorecido', [
                                    'id'            => 'cpf_favorecido',
                                    'value'         => $academia->cpf_favorecido,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'label' => false,
                                    'class' => 'form-control cpfmask'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('tipo_conta') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('tipo_conta', [
                                  'options' => [
                                    'Selecione'      => 'Selecione',
                                    'Conta Corrente' => 'Conta Corrente',
                                    'Conta Poupança' => 'Conta Poupança'
                                  ],
                                  'class'   => 'form-control',
                                  'label'   => false
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('banco') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->input('banco', [
                                  'options' => [
                                    'Selecione'      => 'Selecione',
                                    'Banco do Brasil'   => 'Banco do Brasil',
                                    'Bradesco'          => 'Bradesco',
                                    'Caixa'             => 'Caixa Econ. Federal',
                                    'Itaú'              => 'Itaú',
                                    'Santander'         => 'Santander'
                                  ],
                                  'class'   => 'form-control banco-input',
                                  'label'   => false
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('Agência') ?>
                            </div>
                            <div class="col-sm-3">
                                <?= $this->Form->input('agencia', [
                                    'id'            => 'agencia',
                                    'value'         => $academia->agencia,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'label' => false,
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('Agência digito') ?>
                            </div>
                            <div class="col-sm-2">
                                <?= $this->Form->input('agencia_dv', [
                                    'id'            => 'agencia_dv',
                                    'value'         => $academia->agencia_dv,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'label' => false,
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('conta') ?>
                            </div>
                            <div class="col-sm-3">
                                <?= $this->Form->input('conta', [
                                    'id'            => 'conta',
                                    'value'         => $academia->conta,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'label' => false,
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('Conta digito') ?>
                            </div>
                            <div class="col-sm-2">
                                <?= $this->Form->input('conta_dv', [
                                    'id'            => 'conta_dv',
                                    'value'         => $academia->conta_dv,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'label' => false,
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-md-2 control-label">
                                <?= $this->Form->label('Observações') ?>
                            </div>
                            <div class="col-sm-7">
                                <?= $this->Form->textarea('banco_obs', [
                                    'id'            => 'banco_obs',
                                    'value'         => $academia->banco_obs,
                                    'disabled'  => false,
                                    'readonly'  => false,
                                    'label' => false,
                                    'class' => 'form-control'
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group text-center col-xs-12">
                            <div class="col-sm-12">
                            <?= $this->Form->button('<i class="fa fa-check-square-o" aria-hidden="true"></i><span> '.__('Atualizar Dados Cadastrais ').'</span>',
                                ['class'    => 'btn btn-alt btn-hoverrr btn-success btn-editar-dados',
                                 'type'     => 'button',
                                 'escape'   => false,]); ?>
                            </div>
                        </div>
                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        $('.btn-editar-dados').click(function(e) {
            var form = $('.form-editar-dados');
            var input_latitude  = $('#latitude-att');
            var input_longitude = $('#longitude-att');

            var uf = '';
            var state_id = parseInt($('.uf-map').val());
            var cidade_text = $('.city-map option[value='+$('.city-map').val()+']').text();

            switch (state_id){
                case 1  : uf = 'AC';  break;
                case 2  : uf = 'AL';  break;
                case 3  : uf = 'AM';  break;
                case 4  : uf = 'AP';  break;
                case 5  : uf = 'BA';  break;
                case 6  : uf = 'CE';  break;
                case 7  : uf = 'DF';  break;
                case 8  : uf = 'ES';  break;
                case 9  : uf = 'GO';  break;
                case 10 : uf = 'MA';  break;
                case 11 : uf = 'MG';  break;
                case 12 : uf = 'MS';  break;
                case 13 : uf = 'MT';  break;
                case 14 : uf = 'PA';  break;
                case 15 : uf = 'PB';  break;
                case 16 : uf = 'PE';  break;
                case 17 : uf = 'PI';  break;
                case 18 : uf = 'PR';  break;
                case 19 : uf = 'RJ';  break;
                case 20 : uf = 'RN';  break;
                case 21 : uf = 'RO';  break;
                case 22 : uf = 'RR';  break;
                case 23 : uf = 'RS';  break;
                case 24 : uf = 'SC';  break;
                case 25 : uf = 'SE';  break;
                case 26 : uf = 'SP';  break;
                case 27 : uf = 'TO';  break;
                default : uf = '';
            }

            var academia_address = $('.address-map').val() + ','
                + $('.number-map').val() + ','
                + $('.area-map').val() + ','
                + $('.city-map option[value='+$('.city-map').val()+']').text() + ','
                + uf + ', Brasil';

            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + academia_address + '&sensor=false',
                null,
                function (data) {
                    var p = [];
                    if (data.results[0].geometry.location) {
                        p = data.results[0].geometry.location;
                    } else {
                        p = data.results[0]['geometry']['location'];
                    }
                    
                    input_latitude.val(p.lat);
                    input_longitude.val(p.lng);
                        
                    form.get(0).submit();
                });
            return false;
        });
    });
</script>

<script>
    $('#states').on('change', function(){
        var state_id = $(this).val();
        $.get(WEBROOT_URL + '/cidades/' + state_id,
            function(data){
                $('#city').html(data);
            });
    });

    $('#states_acad').on('change', function(){
        var state_id = $(this).val();
        $.get(WEBROOT_URL + '/cidades/' + state_id,
            function(data){
                $('#city_acad').html(data);
            });
    });
</script>