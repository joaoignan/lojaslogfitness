<?= $this->Html->css('charts/examples') ?>
<?= $this->Html->script('charts/jquery.flot') ?>
<?= $this->Html->script('charts/jquery.flot.categories') ?>

<style>
	.info-conta{
		margin-bottom: 15px;
	}
	.tabela-info-conta{
		overflow-x: auto;
	}
	.info-conta table{
		border: 2px solid rgba(80, 137, 207, 1.0);
	}
	.info-conta table th{
		padding: 10px;
		border-bottom: 2px solid rgba(80, 137, 207, 1.0);
		background-color: rgba(80, 137, 207, 0.2);
		border-collapse: collapse;
	}
	.info-conta table td{
		padding: 10px;
		border-bottom: 2px solid rgba(80, 137, 207, 1.0);
		border-collapse: collapse;
	}
	.botoes{
		margin-bottom: 15px;
	}
	.botoes button{
		width: 90%
	}
	@media all and (max-width: 768px){
		.tabela-info-conta{
			overflow-x: scroll;
		}
	}
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>

<section class="content">
	<div class="col-xs-12">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Administrativo LOGMensalidades</h3>
            </div>
			<div class="box-body">
				<div class="row graficos text-center">
					<div class="col-xs-12 col-sm-12 col-md-3 col-md-offset-1 grafico-single text-center">
						<h4>Mensalidades</h4>
						<canvas class="pie-chart"></canvas>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-1 grafico-single text-single">
						<h4>Venda de planos</h4>
						<canvas class="line-chart"></canvas>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 text-center info-conta">
						<h3>Saldo e Informações da conta</h3>
						<div class="tabela-info-conta">
							<table width="600" align="center">
								<tr>
									<th class="text-center">À Receber</th>
									<th class="text-center">Saldo</th>
									<th class="text-center">Em trânsito</th>
								</tr>
								<tr>
									<td class="text-center">R$ <?= number_format($valor_total, 2, ',', '.') ?></td>
									<td class="text-center"><?= $resp_iugu_subconta_info->balance_available_for_withdraw ?></td>
									<td class="text-center">R$ <?= number_format($valor_transito, 2, ',', '.') ?></td>
								</tr>
							</table>
						</div><!-- tabela-info-conta -->
					</div> <!-- info-conta -->
					<div class="col-xs-12 col-sm-12 col-md-6 text-center botoes">
						<?= $this->Html->link('<button class="btn btn-success">Solicitar Transferência</button>','/academia/admin/admin_mensalidades/transferencia',['escape' => false]) ?>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 text-center botoes">
						<?= $this->Html->link('<button class="btn btn-success antecipacao-btn">Antecipação de Recebíveis</button>','/academia/admin/admin_mensalidades/recebiveis',['escape' => false]) ?>
					</div>
					<!-- <div class="col-xs-12 text-center info-conta">
						<div class="tabela-info-conta">
							<table width="600" align="center">
								<tr>
									<th class="text-center">Mês anterior</th>
									<th class="text-center">Este mês</th>
								</tr>
								<tr>
									<td class="text-center"><?= $resp_iugu_subconta_info->volume_last_month ?></td>
									<td class="text-center"><?= $resp_iugu_subconta_info->volume_this_month ?></td>
								</tr>
							</table>
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	var ctx = document.getElementsByClassName("pie-chart");
	var ctxx = document.getElementsByClassName("line-chart");


	var myPieChart = new Chart(ctx,{
    	type: 'pie',
    	data: {
    		labels:[
    			"Ativas",
    			"A Vencer",
    			"Vencidas"
    		],
    		datasets:[{
    			data:[<?= $qtd_alunos_ativos ?>, <?= $qtd_alunos_vencer ?>,<?= $qtd_alunos_vencidos ?>],
    			backgroundColor:[
    				"#4f88cf",
    				"#f3d012",
    				"#ff0000"
    			]
    		}]
		},
		options: {
			cutoutPercentage:  0,
		},
    	
	});

	<?php for($i = 1; $i <= 31; $i++) { 
		$planosvendidos_graph[$i] = 0;
	} ?>

	<?php for($i = 1; $i <= 31; $i++) {
		foreach ($planos_vendidos as $plano_vendido) {
			if ($plano_vendido->created->format('d') == $i) { 
				$planosvendidos_graph[$i]++;
			}
		}
	} ?>

	var myLineChart = new Chart(ctxx, {
    	type: 'line',
    	data: {
    		labels: [
    			<?php for($i = 1; $i <= 31; $i++) { 
					echo '"'.$i.'",';
				} ?>
    		],
    		datasets:[{
    			label:[
    				"Planos vendidos"
    			],
    			data:[
    				<?php for($i = 1; $i <= 31; $i++) { 
						echo $planosvendidos_graph[$i].',';
					} ?>
    				2,4,1,8,2,6,5
    			],
    			fill: false,
    			borderColor: "#4f88cf",
    			lineTension: 0
    		}]

    	},
    	options: {
    		scales: {
        		yAxes: [{
            		ticks: {
                		beginAtZero: true
           			}
        		}]
    		}
		}
	});
	
</script>