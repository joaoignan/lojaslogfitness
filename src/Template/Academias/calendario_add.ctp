<h3>Calendário Semanal</h3>
<hr>
<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-1 add-aula">
    <h4>Adicione uma aula</h4>
    <?= $this->Form->create(null, ['id' => 'form-calendario']) ?>
    <div class="col-xs-12">
        <?= $this->Form->input('aula', [
            'class' => 'form-control horario-semana'
        ]) ?>
    </div>
    <div class="col-xs-6">
        <?= $this->Form->input('horario_inicio', [
            'class' => 'form-control horario-semana horario time'
        ]) ?>
    </div>
    <div class="col-xs-6">
        <?= $this->Form->input('horario_termino', [
            'class' => 'form-control horario-semana horario time'
        ]) ?>
    </div>
    <div class="col-xs-12">
        <?= $this->Form->input('dia', [
            'options' => [
                'Segunda',
                'Terça',
                'Quarta',
                'Quinta',
                'Sexta',
                'Sábado',
                'Domingo'
            ],
            'div'   => false,
            'class' => 'form-control horario-semana'
        ]) ?>
    </div>
    <div class="col-xs-12">
        <?= $this->Form->button('Salvar', [
            'type'  => 'button',
            'id'    => 'form-calendario-btn',
            'class' => 'btn btn-success'
        ]) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-1 att-aula"> 
    <h4>Atualize seus horários</h4>
    <p>Seus alunos e futuros alunos irão se orientar pelos horários que você incluir no calendário. Manter seus horários atualizados irá impactar positivamente para atrair futuros alunos! </p>
</div>


<script type="text/javascript">
    $('#form-calendario-btn').click(function() {
        var form = $('#form-calendario');

        var form_data = form.serialize();
        $.ajax({
            type: "POST",
            url: WEBROOT_URL + "academia/admin/perfil/calendario-add",
            data: form_data
        })
            .done(function (data) {
                $('#calendario').html(data);
            });
    });
</script>

<div class="calendario-content text-center col-xs-12">
    <div class="table-calendario">
        <table style="width: 100%" class="text-center" id="horarios-table">
            <tr>
                <th><strong>Segunda</strong></th>
                <th><strong>Terça</strong></th>
                <th><strong>Quarta</strong></th>
                <th><strong>Quinta</strong></th>
                <th><strong>Sexta</strong></th>
                <th><strong>Sábado</strong></th>
                <th><strong>Domingo</strong></th>
            </tr>
            <?php foreach ($lista_calendario as $key => $lcalendario) {
                if($key % 7 == 0) {
                    echo '</tr>';
                }
                if($key % 7 == 0 || $key == 0) {
                    echo '<tr>';
                }
                echo '<td>';
                if($lcalendario == 'vazio') {
                    echo '<p>-</p>';
                } else {
                    echo '<p style="text-transform: capitalize">'.ucwords(strtolower($lcalendario->aula)).'</p>';
                }
                if($lcalendario == 'vazio') {
                    echo '<p>-</p>';   
                } else {
                    echo '<p>'.$lcalendario->horario_inicio->format('H:i').' - '.$lcalendario->horario_termino->format('H:i').'</p>';   
                }
                if($lcalendario != 'vazio') {
                    echo $this->Html->link('<i class="fa fa-trash"></i> Excluir', 
                        '/academia/admin/perfil/calendario-remove/'.$lcalendario->id, 
                        ['class' => 'btn btn-danger remove', 'escape' => false]);
                }
                echo '</td>';
            } ?>
        </table>
    </div>
</div>