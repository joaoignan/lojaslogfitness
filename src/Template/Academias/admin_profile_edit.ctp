    <style type="text/css">
        .galeria-content img {
            height: 180px;
            max-width: 240px;
        }
        .inactive{
            color: white!important;
        }
        .inactive:hover{
            color: #f3d012!important;
        }
    </style>
    <div class="altera-logo text-center">
        <?= $this->Html->link('Alterar logo', 'https://www.logfitness.com.br/academia/admin/meus-dados',['target' => '_blank']) ?>
    </div>
    
    <?= $this->Flash->render() ?>

    <div class="row box-conteudo text-center">
        <h3>Perfil Logfitness, onde novos alunos encontram sua academia!</h3>
        <p>Esse é o seu perfil Logfitness. Aqui você colocará as informações sobre sua academia como horário de funcionamento, modalidades de treinos disponíveis, os diferenciais da sua academia, sua equipe cadastrada na LOG e muito mais coisas que ainda estamos desenvolvendo!</p>
        <p>Este perfil será visível para todos, por isso é importante que você sempre o mantenha atualizado.</p>
        <?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',WEBROOT_URL.'academia/admin/dashboard',['escape' => false, 'class' => 'btn btn-danger']) ?>
    </div> <!-- row box-conteudo text-center -->
    
    <div class="row profile-menu">
        <ul>
            <li><a href="#pacotes"><i class="fa fa-trophy" aria-hidden="true"></i> Planos</a></li>
            <li><a href="#sobre"><i class="fa fa-info" aria-hidden="true"></i> Sobre</a></li>
            <li><a href="#modalidades"><i class="fa fa-futbol-o" aria-hidden="true"></i> Modalidades</a></li>
            <li><a href="#calendario"><i class="fa fa-calendar" aria-hidden="true"></i> Calendário</a></li>
            <li><a href="#galeria"><i class="fa fa-camera" aria-hidden="true"></i> Galeria</a></li>
            <li><a href="#avaliacoes"><i class="fa fa-star-half-o" aria-hidden="true"></i> Avaliações</a></li>
            <li><a href="#mapa"><i class="fa fa-map-marker" aria-hidden="true"></i> Mapa</a></li>
        </ul>
    </div> <!-- row -->
    <div id="pacotes" class="row box-conteudo text-center">
        <h3>Planos</h3>
        <!-- <hr> -->
        <div class="em-breve">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            <p>Adicione planos mensais, bimestrais, anuais, recorrentes ou com a duração que você quiser! Escolha os métodos de pagamentos de cada plano , gerencie as compras de seus alunos e envie e-mails para lembrá-los de renovar as suas mensalidades. Tudo isso em uma única ferramenta!</p>
            <?php if($academia_mensalidade) { ?>
                <h1><?= $this->Html->link('<button class="btn btn-success">Novo plano/assinatura</button></h1> ','/academia/admin/admin_mensalidades/planos_add',['escape' => false]);  ?></h1>
            <?php } else{ ?>
                <h1><?= $this->Html->link('<button class="btn btn-success">Novo plano/assinatura</button></h1> ','/academia/admin/mensalidade/',['escape' => false]);  ?></h1>
            <?php } ?>
            </div>
        </div>
        <!-- <div class="owl-carousel-4">
            <div class="item">
                <div class="pacote-content">
                    <div class="col-xs-12">
                        <h4>Nome do pacote</h4>
                    </div> 
                    <p>Duração</p>
                    <p>R$ 59,90 / mês</p>
                    <p>Seg-Ter-Qua-Qui-Sex-Sáb-Dom</p>
                    <p>Pequena descrição (50 caracteres)</p>
                    <button class="btn btn-success btn-sm">Adiquirir</button>
                </div> 
            </div> 
            <div class="item">
                <div class="pacote-content">
                    <div class="col-xs-12">
                        <h4>Nome do pacote</h4>
                    </div>
                    <p>Duração</p>
                    <p>R$ 59,90 / mês</p>
                    <p>Seg-Ter-Qua-Qui-Sex-Sáb-Dom</p>
                    <p>Pequena descrição (50 caracteres)</p>
                    <button class="btn btn-success btn-sm">Adiquirir</button>
                </div> 
            </div> 
            <div class="item">
                <div class="pacote-content">
                    <div class="col-xs-12">
                        <h4>Nome do pacote</h4>
                    </div> 
                    <p>Duração</p>
                    <p>R$ 59,90 / mês</p>
                    <p>Seg-Ter-Qua-Qui-Sex-Sáb-Dom</p>
                    <p>Pequena descrição (50 caracteres)</p>
                    <button class="btn btn-success btn-sm">Adiquirir</button>
                </div> 
            </div> 
            <div class="item">
                <div class="pacote-content">
                    <div class="col-xs-12">
                        <h4>Nome do pacote</h4>
                    </div> 
                    <p>Duração</p>
                    <p>R$ 59,90 / mês</p>
                    <p>Seg-Ter-Qua-Qui-Sex-Sáb-Dom</p>
                    <p>Pequena descrição (50 caracteres)</p>
                    <button class="btn btn-success btn-sm">Adiquirir</button>
                </div> 
            </div> 
            <div class="item">
                <div class="pacote-content">
                    <div class="col-xs-12">
                        <h4>Nome do pacote</h4>
                    </div> 
                    <p>Duração</p>
                    <p>R$ 59,90 / mês</p>
                    <p>Seg-Ter-Qua-Qui-Sex-Sáb-Dom</p>
                    <p>Pequena descrição (50 caracteres)</p>
                    <button class="btn btn-success btn-sm">Adiquirir</button>
                </div> 
            </div> 
        </div> --> 
    </div> <!-- row box-conteudo text-center -->
    <div id="calendario" class="row box-conteudo text-center">
        <h3>Calendário Semanal</h3>
        <hr>
        <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-1 add-aula">
            <h4>Adicione uma aula</h4>
            <?= $this->Form->create(null, ['id' => 'form-calendario']) ?>
            <div class="col-xs-12">
                <?= $this->Form->input('aula', [
                    'class' => 'form-control horario-semana'
                ]) ?>
            </div>
            <div class="col-xs-6">
                <?= $this->Form->input('horario_inicio', [
                    'class' => 'form-control horario-semana horario time'
                ]) ?>
            </div>
            <div class="col-xs-6">
                <?= $this->Form->input('horario_termino', [
                    'class' => 'form-control horario-semana horario time'
                ]) ?>
            </div>
            <div class="col-xs-12">
                <?= $this->Form->input('dia', [
                    'options' => [
                        'Segunda',
                        'Terça',
                        'Quarta',
                        'Quinta',
                        'Sexta',
                        'Sábado',
                        'Domingo'
                    ],
                    'div'   => false,
                    'class' => 'form-control horario-semana'
                ]) ?>
            </div>
            <div class="col-xs-12">
                <?= $this->Form->button('Salvar', [
                    'type'  => 'button',
                    'id'    => 'form-calendario-btn',
                    'class' => 'btn btn-success'
                ]) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-1 att-aula"> 
            <h4>Atualize seus horários</h4>
            <p>Seus alunos e futuros alunos irão se orientar pelos horários que você incluir no calendário. Manter seus horários atualizados irá impactar positivamente para atrair futuros alunos! </p>
        </div>
       

        <script type="text/javascript">
            $('#form-calendario-btn').click(function() {
                var form = $('#form-calendario');

                var form_data = form.serialize();
                $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + "academia/admin/perfil/calendario-add",
                    data: form_data
                })
                    .done(function (data) {
                        $('#calendario').html(data);
                    });
            });
        </script>

        <div class="calendario-content text-center col-xs-12">
            <div class="table-calendario">
                <table style="width: 100%" class="text-center" id="horarios-table">
                    <tr>
                        <th><strong>Segunda</strong></th>
                        <th><strong>Terça</strong></th>
                        <th><strong>Quarta</strong></th>
                        <th><strong>Quinta</strong></th>
                        <th><strong>Sexta</strong></th>
                        <th><strong>Sábado</strong></th>
                        <th><strong>Domingo</strong></th>
                    </tr>
                    <?php foreach ($lista_calendario as $key => $lcalendario) {
                        if($key % 7 == 0) {
                            echo '</tr>';
                        }
                        if($key % 7 == 0 || $key == 0) {
                            echo '<tr>';
                        }
                        echo '<td>';
                        if($lcalendario == 'vazio') {
                            echo '<p>-</p>';
                        } else {
                            echo '<p style="text-transform: capitalize">'.ucwords(strtolower($lcalendario->aula)).'</p>';
                        }
                        if($lcalendario == 'vazio') {
                            echo '<p>-</p>';   
                        } else {
                            echo '<p>'.$lcalendario->horario_inicio->format('H:i').' - '.$lcalendario->horario_termino->format('H:i').'</p>';   
                        }
                        if($lcalendario != 'vazio') {
                            echo $this->Html->link('<i class="fa fa-trash"></i> Excluir', 
                                '/academia/admin/perfil/calendario-remove/'.$lcalendario->id, 
                                ['class' => 'btn btn-danger remove', 'escape' => false]);
                        }
                        echo '</td>';
                    } ?>
                </table>
            </div>
        </div>
    </div>
    <?= $this->Form->create($academia, ['type' => 'file', 'id' => 'form-perfil']); ?>
    <div id="modalidades" class="row box-conteudo text-center">
        <h3>Modalidades</h3>
        <hr>
        <div class="modalidades-content text-left">

            <?php
            foreach($modalidades_list as $ke => $modalidade):
                in_array($modalidade->id, $modalidades) ? $checked = true : $checked = false;
                ?>
                <div class="checkbox modalidade-single">
                    <label>
                        <div class="checker" id="uniform-inlineCheckbox110">
                            <span>
                                <?= $this->Form->input('modalidade.'.$modalidade->id, [
                                    'type'      => 'checkbox',
                                    'class'     => 'custom-checkbox',
                                    'id'        => 'inlineCheckbox110',
                                    'checked'   => $checked,
                                    'label'     => false,
                                    'div'       => false,
                                ])?>
                            </span>
                        </div>
                        <?= $modalidade->name ?>
                    </label>
                </div>
                <?php
            endforeach;
            ?>
        </div> <!-- modalidades-content -->
        <div class="col-xs-12 editar text-right">
            <?= $this->Form->button('<i class="fa fa-check" aria-hidden="true"></i><span> Salvar</span>',
                    ['class' => 'btn btn-success btn-sm',
                        'escape' => false]); ?>
        </div>
    </div> <!-- row box-conteudo text-center -->
    
    <div id="sobre" class="row box-conteudo text-center">
        <h3>Sobre</h3>
        <hr>
        <div class="col-xs-12 academia-sobre text-left">
            <p>
                <?= $this->Form->textarea('sobre', [
                    'class'       => 'form-control',
                    'rows'        => '3',
                    'maxlength'   => '500',
                    'placeholder' => 'Fale um pouco sobre sua academia (até 500 caracteres)',
                    'label'       => false,
                    'div'         => false
                ])?>
            </p>
            <br>
            <h4>Contato</h4>
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-2 dados-contato">
                    <i class="fa fa-phone" aria-hidden="true"></i> Telefone
                </div>
                <div class="col-xs-12 col-sm-9 col-md-6">
                    <?= $this->Form->input('phone', [
                        'class'       => 'form-control phone',
                        'placeholder' => '(99) 99999-9999',
                        'label'       => false,
                        'div'         => false
                    ])?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-sm-3 dados-contato"><i class="fa fa-map-marker" aria-hidden="true"></i> Endereço</div>
                <div class="col-sm-9 col-md-6">
                    <?= $this->Form->input('address', [
                        'class'       => 'form-control address',
                        'placeholder' => '(99) 99999-9999',
                        'title'       => 'Você deve alterar seu endereço na página "Meus Dados"',
                        'disabled'    => true,
                        'label'       => false,
                        'div'         => false
                    ])?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-md-2 dados-contato"><i class="fa fa-clock-o" aria-hidden="true"></i> Segunda/Sexta</div>
                <div class="col-xs-12 col-sm-9 col-md-3 text-left">
                    <?php $hsemana = explode(' - ', $academia->horario_semana); ?>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('.horario-semana').on('keyup change', function() {
                                $('#horario_semana').val($('#horario_semana1').val()+' - '+$('#horario_semana2').val());
                            });
                        });
                    </script>
                    <input type="text" id="horario_semana1" val="<?= $hsemana[0] ?>" class="form-control horario-semana horario time" placeholder="<?= $hsemana[0] ?>"> às <input type="text" id="horario_semana2" val="<?= $hsemana[1] ?>" class="form-control horario-semana horario time" placeholder="<?= $hsemana[1] ?>"> 

                    <?= $this->Form->input('horario_semana', [
                        'type'  => 'hidden',
                        'id'    => 'horario_semana',
                        'label' => false,
                        'div'   => false
                    ])?>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 almoco">
                    <p><input type="checkbox" id="fechada-entre-semana" name="fechada-entre-semana"> Fechada entre</p>
                </div>
                <div class="col-sm-9 col-md-3 text-left"> 
                    <?php $fsemana = explode(' - ', $academia->intervalo_semana); ?>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('.fechada-semana').on('keyup change', function() {
                                $('#fechada_semana').val($('#fechada_semana1').val()+' - '+$('#fechada_semana2').val());
                            });
                        });
                    </script>
                    <input type="text" id="fechada_semana1" val="<?= $fsemana[0] ?>" class="form-control fechada-semana horario time" placeholder="<?= $fsemana[0] ?>"> às <input type="text" id="fechada_semana2" val="<?= $fsemana[1] ?>" class="form-control fechada-semana horario time" placeholder="<?= $fsemana[1] ?>"> 

                    <?= $this->Form->input('intervalo_semana', [
                        'type'  => 'hidden',
                        'id'    => 'fechada_semana',
                        'label' => false,
                        'div'   => false
                    ])?>
                </div>
            </div>
            <hr class="separador-cinza">
            <div class="row">
                <div class="col-sm-3 col-md-2 dados-contato"><i class="fa fa-clock-o" aria-hidden="true"></i> Sábado</div>
                <div class="col-xs-12 col-sm-9 col-md-3 text-left">
                    <?php $hsabado = explode(' - ', $academia->horario_sabado); ?>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('.horario-sabado').on('keyup change', function() {
                                $('#horario_sabado').val($('#horario_sabado1').val()+' - '+$('#horario_sabado2').val());
                            });
                        });
                    </script>
                    <input type="text" id="horario_sabado1" val="<?= $hsabado[0] ?>" class="form-control horario-sabado horario time" placeholder="<?= $hsabado[0] ?>"> às <input type="text" id="horario_sabado2" val="<?= $hsabado[1] ?>" class="form-control horario-sabado horario time" placeholder="<?= $hsabado[1] ?>"> 

                    <?= $this->Form->input('horario_sabado', [
                        'type'  => 'hidden',
                        'id'    => 'horario_sabado',
                        'label' => false,
                        'div'   => false
                    ])?>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 almoco">
                    <p><input type="checkbox" id="fechada-entre-sabado" name="fechada-entre-sabado"> Fechada entre</p>
                </div>
                <div class="col-sm-9 col-md-3 text-left"> 
                    <?php $fsabado = explode(' - ', $academia->intervalo_sabado); ?>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('.fechada-sabado').on('keyup change', function() {
                                $('#fechada_sabado').val($('#fechada_sabado1').val()+' - '+$('#fechada_sabado2').val());
                            });
                        });
                    </script>
                    <input type="text" id="fechada_sabado1" val="<?= $fsabado[0] ?>" class="form-control fechada-sabado horario time" placeholder="<?= $fsabado[0] ?>"> às <input type="text" id="fechada_sabado2" val="<?= $fsabado[1] ?>" class="form-control fechada-sabado horario time" placeholder="<?= $fsabado[1] ?>"> 

                    <?= $this->Form->input('intervalo_sabado', [
                        'type'  => 'hidden',
                        'id'    => 'fechada_sabado',
                        'label' => false,
                        'div'   => false
                    ])?>
                </div>
            </div>
            <hr class="separador-cinza">
            <div class="row">
                <div class="col-sm-3 col-md-2 dados-contato"><i class="fa fa-clock-o" aria-hidden="true"></i> Domingo</div>
                <div class="col-xs-12 col-sm-9 col-md-3 text-left">
                    <?php $hdomingo = explode(' - ', $academia->horario_domingo); ?>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('.horario-domingo').on('keyup change', function() {
                                $('#horario_domingo').val($('#horario_domingo1').val()+' - '+$('#horario_domingo2').val());
                            });
                        });
                    </script>
                    <input type="text" id="horario_domingo1" val="<?= $hdomingo[0] ?>" class="form-control horario-domingo horario time" placeholder="<?= $hdomingo[0] ?>"> às <input type="text" id="horario_domingo2" val="<?= $hdomingo[1] ?>" class="form-control horario-domingo horario time" placeholder="<?= $hdomingo[1] ?>"> 

                    <?= $this->Form->input('horario_domingo', [
                        'type'  => 'hidden',
                        'id'    => 'horario_domingo',
                        'label' => false,
                        'div'   => false
                    ])?>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 almoco">
                    <p><input type="checkbox" id="fechada-entre-domingo" name="fechada-entre-domingo"> Fechada entre</p>
                </div>
                <div class="col-sm-9 col-md-3 text-left"> 
                    <?php $fdomingo = explode(' - ', $academia->intervalo_domingo); ?>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('.fechada-domingo').on('keyup change', function() {
                                $('#fechada_domingo').val($('#fechada_domingo1').val()+' - '+$('#fechada_domingo2').val());
                            });
                        });
                    </script>
                    <input type="text" id="fechada_domingo1" val="<?= $fdomingo[0] ?>" class="form-control fechada-domingo horario time" placeholder="<?= $fdomingo[0] ?>"> às <input type="text" id="fechada_domingo2" val="<?= $fdomingo[1] ?>" class="form-control fechada-domingo horario time" placeholder="<?= $fdomingo[1] ?>"> 

                    <?= $this->Form->input('intervalo_domingo', [
                        'type'  => 'hidden',
                        'id'    => 'fechada_domingo',
                        'label' => false,
                        'div'   => false
                    ])?>
                </div>
            </div>
            <hr>
        </div> <!-- academia-sobre -->
        <div class="col-xs-12 diferenciais text-left">
            <h4>Diferenciais</h4>
            <?php
            foreach($diferenciais_list as $ke => $diferencial):
                in_array($diferencial->id, $diferenciais) ? $checked = true : $checked = false;
                ?>
                <div class="checkbox diferenciais-single">
                    <label>
                        <div class="checker" id="uniform-inlineCheckbox110">
                            <span>
                                <?= $this->Form->input('diferencial.'.$diferencial->id, [
                                    'type'      => 'checkbox',
                                    'class'     => 'custom-checkbox',
                                    'id'        => 'inlineCheckbox110',
                                    'checked'   => $checked,
                                    'label'     => false,
                                    'div'       => false,
                                ])?>
                            </span>
                        </div>
                        <?= $diferencial->name ?>
                    </label>
                </div>
                <?php
            endforeach;
            ?>
        </div> <!-- modalidades-content -->
        <div class="col-xs-12 editar salvar-sobre-box text-right">
            <?= $this->Form->button('<i class="fa fa-check" aria-hidden="true"></i><span> Salvar</span>',
                    ['class' => 'btn btn-success salvar-sobre-btn btn-sm',
                        'escape' => false]); ?>
        </div>
    </div> <!-- row box-conteudo text-center -->


    <script type="text/javascript">
        $(document).ready(function() {
            $('.salvar-sobre-btn').click(function() {
                if(($('#horario_semana1').val() != '' && $('#horario_semana2').val() == '') || 
                    ($('#horario_semana1').val() == '' && $('#horario_semana2').val() != '') ||
                      ($('#horario_sabado1').val() != '' && $('#horario_sabado2').val() == '') ||
                        ($('#horario_sabado1').val() == '' && $('#horario_sabado2').val() != '') ||
                          ($('#horario_domingo1').val() != '' && $('#horario_domingo2').val() == '') ||
                            ($('#horario_domingo1').val() == '' && $('#horario_domingo2').val() != '') ||
                              ($('#fechada_semana1').val() != '' && $('#fechada_semana2').val() == '') ||
                                ($('#fechada_semana1').val() == '' && $('#fechada_semana2').val() != '') ||
                                  ($('#fechada_sabado1').val() != '' && $('#fechada_sabado2').val() == '') ||
                                    ($('#fechada_sabado1').val() == '' && $('#fechada_sabado2').val() != '') ||
                                      ($('#fechada_domingo1').val() != '' && $('#fechada_domingo2').val() == '') ||
                                        ($('#fechada_domingo1').val() == '' && $('#fechada_domingo2').val() != '')) {
                    $('.salvar-sobre-box').append('Você não pode preencher um horário e deixar o outro em branco!');
                    return false;
                }
            });
            
            $('.add-galeria').click(function() {
                $('#AddGaleria').height('100%');
            });

            $('.closeGaleria').click(function() {
                $('#AddGaleria').height('0%');
            });
        });
    </script>


    <div id="galeria" class="row box-conteudo text-center">
        <h3>Galeria</h3>
        <hr>
        <div class="owl-carousel-2">

            <?php foreach($galeria as $imagem) { ?>

            <div class="item">
                <div class="galeria-content">
                    <?= $this->Html->link(
                        $this->Html->image('galeria/'.$imagem->image,['alt' => $imagem->title, 'class' => 'text-center']),'img/galeria/lg-'.$imagem->image,
                          ['class' => 'fancybox', 'escape' => false]); ?>
                    <br>
                    <?= $this->Html->link('<i class="fa fa-trash"></i> Remover', 
                        '/academia/admin/perfil/galeria-remove/'.$imagem->id, 
                        ['class' => 'btn btn-danger remove', 'escape' => false]) ?>
                </div>
            </div>

            <?php } ?>

        </div>
        <div class="col-xs-12 text-center">
            <button type="button" class="btn btn-info btn-lg add-galeria"><i class="fa fa-image" aria-hidden="true"></i> Adicionar</button> 
        </div>
    </div>
    <div id="instrutores" class="row box-conteudo text-center">
        <h3>Nossa equipe</h3>
        <hr>
        <div class="owl-carousel-1">
            <?php foreach ($instrutores as $instrutor) { ?>
                <div class="item">
                    <div class="instrutores-content">
                        <div class="col-xs-12 topo-professor text-center">
                        </div> <!-- topo-professor -->
                        <div class="col-xs-12 dados-professor text-center">
                            <?= $this->Html->image('professores/'.$instrutor->professore->image,['alt' => 'foto professor '.$instrutor->professore->name, 'class' => 'text-center']); ?>
                            <?php $name = explode(' ', $instrutor->professore->name); $name = $name[0].' '.end($name) ?>
                            <p style="text-transform: capitalize"><?= ucwords(strtolower($name)) ?></p>
                            <p><?= $instrutor->professore->funcao ?></p>
                        </div> <!-- dados-professor -->
                    </div> <!-- instrutores-content -->
                </div>
            <?php } ?>
        </div> <!-- owl-carousel -->
        <div class="col-xs-12 editar text-right">
            <?= $this->Html->link('<button type="button" class="btn btn-info btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Convidar</button>', 
                '/academia/admin/professores/convidar', 
                ['escape' => false, 'target' => "_blank"]) ?>
        </div>
    </div> <!-- row box-conteudo text-center -->
    <div id="avaliacoes" class="row box-conteudo text-center">
        <h3>Avaliações</h3>
        <hr>
        <div class="avaliacoes">
            <div class="owl-carousel-3">
                <?php foreach($avaliacoes as $avaliacao) { ?>
                    <?php $nome = explode(' ', $avaliacao->cliente->name); $first_name = $nome[0]; $last_name = end($nome); ?>
                    <div class="item">
                        <div class="avaliacao-content">
                            <div class="avatar-avaliador">
                                <?= $this->Html->image('clientes/'.$avaliacao->cliente->image,['alt' => 'foto '.$avaliacao->cliente->name, 'class' => 'text-center']); ?>
                            </div>
                            <div class="dados-avaliador text-left">
                                <p><?= $first_name ?> <?= $last_name ?></p>
                                <p>
                                    <?php for($i = 1; $i <= 5; $i++) {
                                        if($i <= $avaliacao->rating) {
                                            echo '<i class="fa fa-star rating-dourado"></i>';
                                        } else {
                                            echo '<i class="fa fa-star"></i>';
                                        }
                                    } ?>
                                </p>
                            </div>
                            <div class="comentario-avaliador">
                                <?= $avaliacao->avaliacao ?>
                            </div>
                            <span class="reportar-btn" data-id="<?= $avaliacao->id ?>" onclick="openReport()"><i class="fa fa-ban" aria-hidden="true"></i> Reportar</span>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div> <!-- row box-conteudo text-center -->
    <?= $this->Form->end() ?>
    <div id="mapa" class="row box-conteudo text-center">
        <h3>Mapa</h3>
        <p>O google nem sempre acerta! Confira no mapa a localização da sua academia. Caso esteja incorreta entre em contato com a gente para que possamos corrigir. =)</p>
        <hr>
        <div class="mapa-content">
            <div id="map"></div>
        </div>
    </div> <!-- row box-conteudo text-center -->

    <div class="row text-center">
        <?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',WEBROOT_URL.'academia/admin/dashboard',['escape' => false, 'class' => 'btn btn-danger']) ?>
        <br>
        <br>
    </div>

    <div class="row text-center footer-total">
        <hr>
        <ul class="list-inline">
            <li>
                <a href="https://open.spotify.com/user/logfitness" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-2x fa-spotify fa-footer"></i></a>
            </li>
            <li>
                <a href="https://www.facebook.com/logfitness.com.br" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-2x fa-facebook fa-footer"></i></a>
            </li>
            <li>
                <a href="https://www.instagram.com/logfitness.com.br/" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-2x fa-instagram fa-footer"></i></a>
            </li>
            <li>
                <a href="https://twitter.com/LOGFITNESS" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-2x fa-twitter fa-footer"></i></a>
            </li>
        </ul>
        <div class="container" >
            <footer>
                <div class="col-sm-12 rodape font-10 text-center">
                    <p>Copyright ©<?= date("Y") ?> <span>www.logfitness.com.br</span>. TODOS OS DIREITOS RESERVADOS.
                        Todo o conteúdo do site, todas as fotos, imagens, logotipos, marcas, dizeres, som, software, conjunto imagem,
                        layout, trade dress, aqui veiculados são de propriedade exclusiva da LogFitness. É vedada qualquer reprodução,
                        total ou parcial, de qualquer elemento de identidade, sem expressa autorização. A violação de qualquer direito mencionado
                        implicará na responsabilização cível e criminal nos termos da Lei.

                        CACIQUE DIGITAL REPRESENTACOES LTDA - ME - CNPJ: 22.921.802/0001-77 Rua Antonio Munia, 141, Sala 01 - Jardim Nazareth - CEP 15054-160 - São José do Rio Preto - SP   -   
                        <a href="#politica-de-privacidade" class="fancybox" style="color: #555">Política de Privacidade</a>
                    </p>
                </div>
            </footer>
        </div><!-- container -->
    </div><!-- row text-center footer-total -->

    <div id="AddGaleria" class="overlay">
        <div class="overlay-content" id="galeria_inner">
        <a href="javascript:void(0)" class="closebtn closeGaleria">&times;</a>

        <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row', 'type' => 'file', 'url' => ['controller' => 'Academias', 'action' => 'galeria_add']]); ?>
            <fieldset>
                <h3 class="box-header">
                Adicionar imagem a galeria
                </h3>
                <div class="form-group">
                    <div class="col-sm-3 control-label">
                        <?= $this->Form->label('title') ?>
                    </div>
                    <div class="col-sm-8">
                        <?= $this->Form->input('title', [
                                        'label' => false,
                                        'class' => 'form-control validate[required, minSize[3], maxSize[255]]']) ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3 control-label">
                        <?= $this->Form->label('imagem') ?>
                    </div>
                    <div class="col-sm-8">
                        <?= $this->Form->input('imagem', [
                            'id'    => 'image_upload',
                            'type'  => 'file',
                            'label' => false,
                            'class' => 'form-control validate[required, custom[validateMIME[image/jpeg|image/png]]]'
                        ]) ?>
                        <p><strong>Selecione um arquivo de imagem entre 400x400 e 4000x4000 pixels</strong></p>
                    </div>
                </div>

                <div class="text-center">
                    <div class="col-xs-12">
                        <?= $this->Form->button('Salvar',
                        ['class' => 'btn btn-success', 'escape' => false]); ?>
                    </div>    
                <?= $this->Form->end() ?>
                </div>
            </fieldset>
        </div><!-- overlay-content -->
    </div>
    <div id="Report" class="overlay">
        <a href="javascript:void(0)" class="closebtn" onclick="closeReport()">&times;</a>
        <div class="overlay-content">
            <?= $this->Form->create(null, ['url' => ['controller' => 'Academias', 'action' => 'reportar_avaliacao']]); ?>
                <h3>Descreva sua denúncia</h3>
                <p>Avaliações contendo linguagem ofensiva, racismo ou qualquer tipo de discriminação com alunos/equipe da academia são analisadas e desabilitadas. Se este for um caso, por favor denuncie.</p>
                <div class="col-xs-12 text-center">
                    <?= $this->Form->input('avaliacao_id', [
                        'type' => 'hidden',
                        'id'   => 'avaliacao_id_hidden' 
                    ]) ?>
                    <?= $this->Form->textarea('motivo', [
                        'class'       => 'form-control',
                        'rows'        => '3',
                        'placeholder' => 'Descreva o motivo desta denúncia (150 caracteres)',
                        'maxlength'   => '150'
                    ]) ?>
                </div>
                <div class="col-xs-12">
                    <?= $this->Form->button('Enviar', [
                        'class'  => 'btn btn-success',
                        'escape' => false
                    ]) ?>
                </div>
            <?= $this->Form->end() ?>
        </div><!-- overlay-content -->
    </div><!-- overlay -->
</div> <!-- container -->

<script type="text/javascript">
    $(document).ready(function() {
        $('.reportar-btn').click(function() {
            var avaliacao_id = $(this).attr('data-id');
            $('#avaliacao_id_hidden').val(avaliacao_id);
        });
    });
</script>

<style type="text/css">
    .rating-dourado {
        color: #f3d012!important;
    }
    .dados-avaliador .fa {
        color: #73a1d9;
    }
</style>

 <?php if($academia->latitude && $academia->longitude) { ?>
        <script>
            var map;
            function initMap() {
                var meulocal = {lat: <?= $academia->latitude ?>, lng: <?= $academia->longitude ?>};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 18,
                    center: meulocal
                });
                var marker = new google.maps.Marker({
                    position: meulocal,
                    map: map,
                    title: "<?= $academia->shortname ?> :: <?= $academia->city->name ?> - <?= $academia->city->uf ?>",
                    icon: WEBROOT_URL + 'img/map-marker.png'
                });
            }
        </script>
    <?php } ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJCTAVhfiDaRSNBflCc-Mn2m6tsZo5oF4&callback=initMap"
async defer></script>

<!-- OWL CAROUSEL -->
<script> 
    $(document).ready(function() {
      $('.owl-carousel-1').owlCarousel({
        loop: true,
        margin: 10,
        itemsMobile: [600, 1],
        itemsDesktopSmall: [1024, 3],
        itemsDesktop: [6000, 5]
      });
      $('.owl-carousel-2').owlCarousel({
        loop: true,
        margin: 10,
        dots: false,
        itemsMobile: [600, 1],
        itemsDesktopSmall: [1024, 2],
        itemsDesktop: [6000, 4]
      });
      $('.owl-carousel-3').owlCarousel({
        loop: false,
        margin: 10,
        itemsMobile: [600, 1],
        itemsDesktopSmall: [1024, 3],
        itemsDesktop: [6000, 4]
      });
      $('.owl-carousel-4').owlCarousel({
        loop: true,
        margin: 10,
        itemsMobile: [600, 1],
        itemsDesktopSmall: [1024, 2],
        itemsDesktop: [6000, 4]
      })
    })
</script>
<!-- FIM OWL CAROUSEL -->
<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox").fancybox();
    });
</script>
<script language="javascript">
    // Função responsável por inserir linhas na tabela
    function inserirLinhaTabela() {

        // Captura a referência da tabela com id “minhaTabela”
        var table = document.getElementById("horarios-table");
        // Captura a quantidade de linhas já existentes na tabela
        var numOfRows = table.rows.length;
        // Captura a quantidade de colunas da última linha da tabela
        var numOfCols = table.rows[numOfRows-1].cells.length;

        // Insere uma linha no fim da tabela.
        var newRow = table.insertRow(numOfRows);

        // Faz um loop para criar as colunas
        for (var j = 0; j < numOfCols; j++) {
            // Insere uma coluna na nova linha 
            newCell = newRow.insertCell(j);
            // Insere um conteúdo na coluna
            newCell.innerHTML = "<p><input type='text' class='forms-horario' placeholder='Aula'></p>                             <p><input type='text' class='horario forms-horario' placeholder='13:00 - 14:00'></p>";
        }

    } 
</script>