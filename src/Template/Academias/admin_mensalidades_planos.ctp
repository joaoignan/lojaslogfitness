<style>
  .nome-plano{
    width: auto;
    display: inline-block;
    padding-left: 5px;
  }
  .editar-plano a{
    color: black;
  }
  .editar-plano, .ver-mais-vencidas, .ver-mais-vencer, .ver-mais-ativas{ 
    float: right;
    width: 100px;
  }
  .inner-ver-mais-vencidas, .inner-ver-mais-vencer, .inner-ver-mais-ativas {
    height: 53px;
    cursor: pointer;
  }
  .inner-editar-vencidas, .inner-editar-vencer, .inner-editar-ativas {
    padding-top: 7px;
    height: 53px;
    cursor: pointer;
  }
  .ver-mais-vencidas label, .ver-mais-vencer label, .ver-mais-ativas label, .editar-plano label{
    margin-bottom: 1px;
    margin-top: 5px;
  }
  #personalizados table{
    border: 2px solid rgba(80, 137, 207, 1.0);
  }
  #personalizados table th{
    padding: 10px;
    border-bottom: 2px solid rgba(80, 137, 207, 1.0);
    background-color: rgba(80, 137, 207, 0.2);
    border-collapse: collapse;
  }
  #personalizados table td{
    padding: 10px;
    border-bottom: 2px solid rgba(80, 137, 207, 1.0);
    border-collapse: collapse;
  }
  #boletos table{
    border: 2px solid rgba(80, 137, 207, 1.0);
  }
  #boletos table th{
    padding: 10px;
    border-bottom: 2px solid rgba(80, 137, 207, 1.0);
    background-color: rgba(80, 137, 207, 0.2);
    border-collapse: collapse;
  }
  #boletos table td{
    padding: 10px;
    border-bottom: 2px solid rgba(80, 137, 207, 1.0);
    border-collapse: collapse;
  }
  .title-planos{
    height: 40px;
  }
  .edit-data{
    width: 100%!important;
    margin: 10px 0!important
  }
  .vencidas{
    background: rgba(255, 0, 0, 0.2);
    border: 1px solid rgba(255, 0, 0, 0.8)
  }
  .inner-ver-mais-vencidas:hover, .inner-editar-vencidas:hover{
    background: rgba(255, 0, 0, 0.6);
  }
  .a_vencer{
    background: rgba(243, 208, 18, 0.2);
    border: 1px solid rgba(243, 208, 18, 0.8)
  }
  .inner-ver-mais-vencer:hover, .inner-editar-vencer:hover{
    background: rgba(243, 208, 18, 0.6);
  }
  .ativas{
    background: rgba( 80, 137, 207, 0.2);
    border: 1px solid rgba(80, 137, 207, 0.8);
  }
  .inner-ver-mais-ativas:hover, .inner-editar-ativas:hover{
    background: rgba( 80, 137, 207, 0.6);
  }
  .div-plano{
    margin: 5px 0;
    min-height: 55px;
    height: auto;
    border-radius: 5px;
  }
  .titulo-plano{
    color: rgba(86, 86, 86, 1.0);
    font-weight: 700;
    font-family: nexa;
    font-size: 30px;
    margin-top: 5px;
    margin-right: 5px;
  }
  .planos-content{
    display: block;
    border-radius: 5px;
    background-color: #FFF;
    margin: 15px 10px;
    padding: 10px;
  }
  .planos-content a{
    color: #565656;
  }
  .planos-content table tr{
    height: 35px;
  }
  .planos-content table th{
    border-bottom: 1px solid black;
  }
  .planos-content i{
    cursor: pointer;
  }
  .conteudo-plano {
    display: none;
  }
  @media all and (max-width: 450px){
    .div-plano h2{
      font-size: 18px;
    }
    .academia-sobre .phone {
      width: 100%;
      display: inline-block;
    }
    .editar{
      margin-top: 10px;
    }
    .almoco{
    text-align: left!important;
    }
    .tabela-plano{
      overflow-x: scroll;
    }
    .titulo-plano{
      font-size: 15px;
      margin-top: 13px;
    }
    .editar-plano, .ver-mais-vencidas, .ver-mais-vencer, .ver-mais-ativas{ 
      float: right;
      width: 70px;
    }
  }
</style>

<?php 
  function resume_string($var, $limite){  
    if (strlen($var) > $limite) {       
      $var = substr($var, 0, $limite);        
      $var = trim($var) . "...";  
    }

    return $var;
  }
?>

<section class="content">
  <div class="col-xs-12">
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Meus Planos</h3>
        <div class="box-tools">
        </div>
        <hr>
        <div class="col-xs-12 no-padding">
        <?= $this->Html->link('<i class="fa fa-plus"></i> Novo Plano/Assinatura',
        '/academia/admin/admin_mensalidades/planos_add',
        ['class' => 'btn btn-success btn-sm', 'escape' => false])?>
        </div>
      </div>
      <div class="box-body">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#vencidas" data-toggle="tab">Vencidas</a></li>
            <li><a href="#a_vencer" data-toggle="tab">A vencer</a></li>
            <li><a href="#ativas" data-toggle="tab">Ativas</a></li>
            <li><a href="#personalizados" data-toggle="tab">Personalizados</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active " id="vencidas">

              <?php foreach ($academia_mensalidades_planos as $planos) { ?>

                <div class="div-plano vencidas">
                  <div class="vencidas-header" >
                    <div class="title-planos">
                      <div class="">
                        <div class="nome-plano">
                          <label class="titulo-plano"><?= resume_string($planos->name, 40) ?></label>
                        </div>
                        <div class="text-center editar-plano">
                          <?php if($planos->visivel == 1) { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-lock"></i><br> Inativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos->id,
                                ['escape' => false])?>
                          <?php } else { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-unlock"></i><br> Ativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos->id,
                                ['escape' => false])?>
                          <?php } ?>
                        </div>
                        <div class="text-center editar-plano">
                          <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-pencil"></i><br> Editar</div>',
                              '/academia/admin/admin_mensalidades/planos_edit/'.$planos->id,
                              ['escape' => false])?>
                        </div>
                        <div class="text-center ver-mais-vencidas click-header-vencidas" data-id="<?= $planos->id ?>">
                          <div class="col-xs-12 inner-ver-mais-vencidas">
                            <label><i class="fa fa-users"> <?= $qtd_alunos_vencidos[$planos->id] >= 1 ? $qtd_alunos_vencidos[$planos->id] : '0' ?></i></label>
                            <p>alunos</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="conteudo-plano conteudo-plano-vencidas-<?= $planos->id ?>">
                    <div class="planos-content">
                      <div class="row">
                        <div class="col-sm-12 col-md-10">
                          <p><?= $planos->descricao ?></p>
                        </div>
                      </div>
                      <hr>
                        <div class="tabela-plano">
                          <table style="width: 800px;" cellpadding="10">
                            <tr>
                              <th><strong>Aluno</strong></th>
                              <th class="text-center"><strong>Pagamento</strong></th>
                              <th class="text-center"><strong>Vencimento</strong></th>
                              <th class="text-center"><strong>Enviar e-mail</strong></th>
                            </tr>
                            <?php foreach ($planos_clientes_vencidos as $plano_cliente_vencido) { ?>
                              <?php if($plano_cliente_vencido->academia_mensalidades_planos_id == $planos->id) { ?>
                                <tr>
                                  <td><?= $plano_cliente_vencido->cliente->name ?></td>
                                  <td class="text-center"><?= $plano_cliente_vencido->created->format('d/m/Y') ?></td>
                                  <td class="text-center"><?= $plano_cliente_vencido->data_vencimento->format('d/m/Y') ?> <i class="fa fa-pencil open_vencimento_overlay" data-id="<?= $plano_cliente_vencido->id ?>"></i></td>
                                  <td class="text-center">
                                    <?= $this->Html->link('enviar e-mail', 
                                    '/academia/admin/admin_mensalidades/planos/cobrar_vencido/'.$plano_cliente_vencido->id,
                                    ['class' => 'btn bg-blue btn-sm']) ?>
                                  </td>
                                </tr>       
                              <?php } ?>
                            <?php } ?>                             
                          </table>
                        </div>

                        <?php foreach ($planos_clientes_vencidos as $plano_cliente_vencido) { ?>
                          <?php if($plano_cliente_vencido->academia_mensalidades_planos_id == $planos->id) { ?>
                            <div class="overlay vencimento-<?= $plano_cliente_vencido->id ?>">
                              <div class="overlay-content edit-plano">
                                <a href="javascript:void(0)" class="closebtn">&times;</a>
                                  <h3>Editar data de vencimento</h3>
                                  <?= $this->Form->create(null, ['url' => ['controller' => 'Academias', 'action' => 'admin_mensalidades_planos_editar_vencimento']]) ?>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <label for="name" class="form-label">Aluno</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_vencido->cliente->name ?>" disabled="true" class="form-control edit-data">
                                      <input type="hidden" name="plano_id" value="<?= $plano_cliente_vencido->id ?>">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                      <label for="name" class="form-label">Data Atual</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_vencido->data_vencimento->format('d/m/Y') ?>" disabled="true" class="form-control edit-data">
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                       <label for="name" class="form-label">Nova data</label> 
                                      <input type="date" min="2015-01-02" max="2999-12-31" name="data_vencimento" placeholder="30/12/2017" required class="form-control date edit-data">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <button type="submit" class="btn btn-success">Salvar</button>
                                    </div>
                                  </div>
                                  <?= $this->Form->end() ?>
                              </div>
                            </div>
                          <?php } ?>
                        <?php } ?>
                    </div>
                  </div>
                </div>

              <?php } ?>

            </div>
            <div class="tab-pane" id="a_vencer">

              <?php foreach ($academia_mensalidades_planos as $planos) { ?>

                <div class="div-plano a_vencer">
                  <div class="row vencidas-header" data-id="<?= $planos->id ?>">
                    <div class="col-xs-12 title-planos">
                      <div class="">
                        <div class="nome-plano">
                          <label class="titulo-plano"><?= resume_string($planos->name, 40) ?></label>
                        </div>
                        <div class="text-center editar-plano">
                          <?php if($planos->visivel == 1) { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-lock"></i><br> Inativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos->id,
                                ['escape' => false])?>
                          <?php } else { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-unlock"></i><br> Ativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos->id,
                                ['escape' => false])?>
                          <?php } ?>
                        </div>
                        <div class="text-center editar-plano">
                          <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencer"><i class="fa fa-pencil"></i><br> Editar</div>',
                              '/academia/admin/admin_mensalidades/planos_edit/'.$planos->id,
                              ['escape' => false])?>
                        </div>
                         <div class="text-center ver-mais-vencer click-header-vencer" data-id="<?= $planos->id ?>">
                          <div class="col-xs-12 inner-ver-mais-vencer">
                            <label><i class="fa fa-users"> <?= $qtd_alunos_vencer[$planos->id] >= 1 ? $qtd_alunos_vencer[$planos->id] : '0' ?></i></label>
                            <p>alunos</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="conteudo-plano conteudo-plano-vencer-<?= $planos->id ?>">
                    <div class="planos-content">
                      <div class="">
                        <div class="col-sm-12 col-md-10">
                          <p><?= $planos->descricao ?></p>
                        </div>
                      </div>
                      <hr>
                        <div class="tabela-plano">
                          <table style="width: 800px;" cellpadding="10">
                            <tr>
                              <th><strong>Aluno</strong></th>
                              <th class="text-center"><strong>Pagamento</strong></th>
                              <th class="text-center"><strong>Vencimento</strong></th>
                              <th class="text-center"><strong>Enviar e-mail</strong></th>
                              <th class="text-center"><strong>Ver fatura</strong></th>
                            </tr>
                            <?php foreach ($planos_clientes_vencer as $plano_cliente_vencer) { ?>
                              <?php if($plano_cliente_vencer->academia_mensalidades_planos_id == $planos->id) { ?>
                                <tr>
                                  <td><?= $plano_cliente_vencer->cliente->name ?></td>
                                  <td class="text-center"><?= $plano_cliente_vencer->created->format('d/m/Y') ?></td>
                                  <td class="text-center"><?= $plano_cliente_vencer->data_vencimento->format('d/m/Y') ?> <i class="fa fa-pencil open_vencimento_overlay" data-id="<?= $plano_cliente_vencer->id ?>"></i></td>
                                  <td class="text-center">
                                    <?= $this->Html->link('enviar e-mail', 
                                    '/academia/admin/admin_mensalidades/planos/cobrar_avencer/'.$plano_cliente_vencer->id,
                                    ['class' => 'btn bg-blue btn-sm']) ?>
                                  </td>
                                  <td class="text-center">
                                    <?php foreach ($pedidos as $pedido) {
                                      if($plano_cliente_vencer->pedido_id == $pedido->id) {
                                        echo $this->Html->link('ver fatura', 
                                        $pedido->iugu_payment_pdf,
                                        ['class' => 'btn bg-blue btn-sm', 'target' => '_blank']);
                                      }
                                    } ?>
                                  </td>
                                </tr>       
                              <?php } ?>
                            <?php } ?>                     
                          </table>
                        </div>
                        <?php foreach ($planos_clientes_vencer as $plano_cliente_vencer) { ?>
                          <?php if($plano_cliente_vencer->academia_mensalidades_planos_id == $planos->id) { ?>
                            <div class="overlay vencimento-<?= $plano_cliente_vencer->id ?>">
                              <div class="overlay-content edit-plano">
                                <a href="javascript:void(0)" class="closebtn">&times;</a>
                                  <h3>Editar data de vencimento</h3>
                                  <?= $this->Form->create(null, ['url' => ['controller' => 'Academias', 'action' => 'admin_mensalidades_planos_editar_vencimento']]) ?>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <label for="name" class="form-label">Aluno</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_vencer->cliente->name ?>" disabled="true" class="form-control edit-data">
                                      <input type="hidden" name="plano_id" value="<?= $plano_cliente_vencer->id ?>">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                      <label for="name" class="form-label">Data Atual</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_vencer->data_vencimento->format('d/m/Y') ?>" disabled="true" class="form-control edit-data">
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                       <label for="name" class="form-label">Nova data</label> 
                                      <input type="date" min="2015-01-02" max="2999-12-31" name="data_vencimento" placeholder="30/12/2017" required class="form-control date edit-data">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <button type="submit" class="btn btn-success">Salvar</button>
                                    </div>
                                  </div>
                                  <?= $this->Form->end() ?>
                              </div>
                            </div>
                          <?php } ?>
                        <?php } ?>
                      </div>
                  </div>
                </div>

              <?php } ?>

            </div>
            <div class="tab-pane" id="ativas">

              <?php foreach ($academia_mensalidades_planos as $planos) { ?>

                <div class="div-plano ativas">
                  <div class="vencidas-header">
                    <div class="title-planos">
                      <div class="">
                        <div class="nome-plano">
                          <label class="titulo-plano"><?= resume_string($planos->name, 40) ?></label>
                        </div>
                        <div class="text-center editar-plano">
                          <?php if($planos->visivel == 1) { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-lock"></i><br> Inativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos->id,
                                ['escape' => false])?>
                          <?php } else { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-unlock"></i><br> Ativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos->id,
                                ['escape' => false])?>
                          <?php } ?>
                        </div>
                        <div class="text-center editar-plano">
                          <?= $this->Html->link('<div class="col-xs-12 inner-editar-ativas"><i class="fa fa-pencil"></i><br> Editar</div>',
                              '/academia/admin/admin_mensalidades/planos_edit/'.$planos->id,
                              ['escape' => false])?>
                        </div>
                        <div class="text-center ver-mais-ativas click-header-ativas" data-id="<?= $planos->id ?>">
                          <div class="col-xs-12 inner-ver-mais-ativas">
                            <label><i class="fa fa-users"> <?= $qtd_alunos_ativos[$planos->id] >= 1 ? $qtd_alunos_ativos[$planos->id] : 0 ?></i></label>
                            <p>alunos</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="conteudo-plano conteudo-plano-ativas-<?= $planos->id ?>">
                    <div class="planos-content">
                      <div class="">
                        <div class="col-sm-12 col-md-10">
                          <p><?= $planos->descricao ?></p>
                        </div>
                      </div>
                      <hr>
                        <div class="tabela-plano">
                          <table style="width: 800px;" cellpadding="10">
                            <tr>
                              <th><strong>Aluno</strong></th>
                              <th class="text-center"><strong>Pagamento</strong></th>
                              <th class="text-center"><strong>Vencimento</strong></th>
                              <th class="text-center"><strong>Ver fatura</strong></th>
                            </tr>
                            <?php foreach ($planos_clientes_ativos as $plano_cliente_ativo) { ?>
                              <?php if($plano_cliente_ativo->academia_mensalidades_planos_id == $planos->id) { ?>
                              <tr>
                                <td><?= $plano_cliente_ativo->cliente->name ?></td>
                                <td class="text-center"><?= $plano_cliente_ativo->created->format('d/m/Y') ?></td>
                                <td class="text-center"><?= $plano_cliente_ativo->data_vencimento->format('d/m/Y') ?> <i class="fa fa-pencil open_vencimento_overlay" data-id="<?= $plano_cliente_ativo->id ?>"></i></td>
                                <td class="text-center">
                                  <?php foreach ($pedidos as $pedido) {
                                    if($plano_cliente_ativo->pedido_id == $pedido->id) {
                                      echo $this->Html->link('ver fatura', 
                                      $pedido->iugu_payment_pdf,
                                      ['class' => 'btn bg-blue btn-sm', 'target' => '_blank']);
                                    }
                                  } ?>
                                </td>
                              </tr>
                              <?php } ?>
                            <?php } ?>
                          </table>
                        </div>
                        <?php foreach ($planos_clientes_ativos as $plano_cliente_ativo) { ?>
                          <?php if($plano_cliente_ativo->academia_mensalidades_planos_id == $planos->id) { ?>
                            <div class="overlay vencimento-<?= $plano_cliente_ativo->id ?>">
                              <div class="overlay-content edit-plano">
                                <a href="javascript:void(0)" class="closebtn">&times;</a>
                                  <h3>Editar data de vencimento</h3>
                                  <?= $this->Form->create(null, ['url' => ['controller' => 'Academias', 'action' => 'admin_mensalidades_planos_editar_vencimento']]) ?>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <label for="name" class="form-label">Aluno</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_ativo->cliente->name ?>" disabled="true" class="form-control edit-data">
                                      <input type="hidden" name="plano_id" value="<?= $plano_cliente_ativo->id ?>">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                      <label for="name" class="form-label">Data Atual</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_ativo->data_vencimento->format('d/m/Y') ?>" disabled="true" class="form-control edit-data">
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                       <label for="name" class="form-label">Nova data</label> 
                                      <input type="date" min="2015-01-02" max="2999-12-31" name="data_vencimento" placeholder="30/12/2017" required class="form-control date edit-data">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <button type="submit" class="btn btn-success">Salvar</button>
                                    </div>
                                  </div>
                                  <?= $this->Form->end() ?>
                              </div>
                            </div>
                          <?php } ?>
                        <?php } ?>  
                    </div>
                  </div>
                </div>

              <?php } ?>

            </div>
            <div class="tab-pane" id="personalizados">
              <div class="tabela" style="width: 100%; overflow-x: auto;">
                <table width="600" align="center">
                  <tr>
                    <th>Aluno</th>
                    <th>Valor</th>
                    <th>Data de compra</th>
                    <th>Ver fatura</th>
                  </tr>
                  <?php foreach($pedidos_personalizados as $pedido_personalizado) { ?>
                    <tr>
                      <td><?= $pedido_personalizado->cliente->name ?></td>
                      <td>R$ <?= number_format($pedido_personalizado->valor, 2, ',', '.') ?></td>
                      <td><?= $pedido_personalizado->created->format('d/m/Y') ?></td>
                      <td>
                        <?= $this->Html->link('ver fatura', 
                        $pedido_personalizado->iugu_payment_pdf,
                        ['class' => 'btn bg-blue btn-sm', 'target' => '_blank']); ?>
                      </td>
                    </tr>
                  <?php } ?>
                </table>
              </div>
            </div>
          </div>
        </div>  
      </div>
    </div>
  </div>
</section>

<?= $this->Html->script('jquery.mask.min.js')?>
<script type="text/javascript">
  $(document).ready(function() {
    $('.open_vencimento_overlay').click(function() {
      $('.vencimento-'+$(this).attr('data-id')).height('100%');
    });

    $('.closebtn').click(function() {
      $('.overlay').height(0);
    });

    $('.click-header-vencidas').click(function() {
      $('.conteudo-plano-vencidas-'+$(this).attr('data-id')).slideToggle();
    });

    $(".btn-editar").on('click', function(e) {
       e.stopPropagation();
    });

    $('.click-header-vencer').click(function() {
      $('.conteudo-plano-vencer-'+$(this).attr('data-id')).slideToggle();
    });

    $('.click-header-ativas').click(function() {
      $('.conteudo-plano-ativas-'+$(this).attr('data-id')).slideToggle();
    });
  });
</script>