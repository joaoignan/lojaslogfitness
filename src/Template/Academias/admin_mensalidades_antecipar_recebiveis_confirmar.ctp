<style>
	.tabela-recebiveis{
	overflow-x: auto;
	}
	.tabela-recebiveis table{
		border: 2px solid rgba(80, 137, 207, 1.0);
		margin: 10px 0;
	}
	.tabela-recebiveis table th{
		padding: 10px;
		border-bottom: 2px solid rgba(80, 137, 207, 1.0);
		background-color: rgba(80, 137, 207, 0.2);
		border-collapse: collapse;
	}
	.tabela-recebiveis table td{
		padding: 10px;
		border-bottom: 2px solid rgba(80, 137, 207, 1.0);
		border-collapse: collapse;
	}
	.botoes{
		margin: 10px 0;
	}
	@media all and (max-width: 1024px){
		.tabela-recebiveis{
			overflow-x: scroll;
		}
	}
</style>
<section class="content">
	<div class="col-xs-12">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Confirmar Antecipações</h3>
				<span class="pull-right"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
                  '/academia/admin/admin_mensalidades/recebiveis',
                  ['escape' =>  false,
                     'class'  =>  'pull-right']) ?></span>
            </div>
			<div class="box-body">
				<div class="col-xs-12 tabela-recebiveis">
					<table width="1000px" align="center">
						<tr>
							<th>Fatura</th>
							<th>Data</th>
							<th>Parcela</th>
							<th>Nº Parcelas</th>
							<th>Total</th>
							<th>Taxas</th>
							<th>Receber</th>
						</tr>
						<?php foreach ($transactions_list as $transaction_list) { ?>
							<?php $data = array_reverse(explode('-', $transaction_list->scheduled_date)) ?>
							<?php 
								$valor = explode(' ', $transaction_list->total);
								if($valor[1] == 'BRL') {
				                    $valor = str_replace(',', '.', $valor[0]);
				                } else {
				                    $valor = str_replace(',', '.', $valor[1]);
				                }
		                        $valor = (double)$valor;
		                        $qtd_parcelas = (int)$transaction_list->number_of_installments;
		                        $parcela_atual = (int)$transaction_list->installment;
			                    if($qtd_parcelas == 1) {
			                    	$taxa = ($valor * $mensalidade_categoria->tax_card_porcent1) * .01;
			                    	$taxa = $taxa + $mensalidade_categoria->tax_card_valor1;
			                    } else if($qtd_parcelas >= 2 && $qtd_parcelas <= 3) {
			                    	$taxa = ($valor * $mensalidade_categoria->tax_card_porcent2) * .01;
			                    	if($parcela_atual == 1) {
			                            $taxa = $taxa + $mensalidade_categoria->tax_card_valor2;
			                        }
			                    } else if($qtd_parcelas >= 4 && $qtd_parcelas <= 6) {
			                    	$taxa = ($valor * $mensalidade_categoria->tax_card_porcent3) * .01;
			                    	if($parcela_atual == 1) {
			                            $taxa = $taxa + $mensalidade_categoria->tax_card_valor3;
			                        }
			                    } else if($qtd_parcelas >= 7) {
			                    	$taxa = ($valor * $mensalidade_categoria->tax_card_porcent4) * .01;
			                    	if($parcela_atual == 1) {
			                            $taxa = $taxa + $mensalidade_categoria->tax_card_valor4;
			                        }
			                    }
		                        $total = $valor - $taxa;
			                ?>
							<tr>
								<td><?= $transaction_list->invoice_id ?></td>
								<td><?= $data[0].'/'.$data[1].'/'.$data[2] ?></td>
								<td><?= $transaction_list->installment ?></td>
								<td><?= $qtd_parcelas ?></td>
								<td>R$ <?= number_format($total, 2, ',', '.') ?></td>
								<td>R$ <?= number_format($custo_final[$transaction_list->id], 2, ',', '.') ?></td>
								<td>R$ <?= number_format($valor_final[$transaction_list->id], 2, ',', '.') ?></td>
							</tr>

							<?php $somatoria_total += $total; ?>
							<?php $custo_total += $custo_final[$transaction_list->id]; ?>
							<?php $valor_total += $valor_final[$transaction_list->id]; ?>
						<?php } ?>
							<tr>
								<td><b>Total</b></td>
								<td></td>
								<td></td>
								<td></td>
								<td>R$ <?= number_format($somatoria_total, 2, ',', '.') ?></td>
								<td>R$ <?= number_format($custo_total, 2, ',', '.') ?></td>
								<td><b>R$ <?= number_format($valor_total, 2, ',', '.') ?></b></td>
							</tr>
					</table>
				</div> <!-- col-xs-12 -->
				<div class="col-xs-12 col-sm-6 text-center botoes">
					
					<?= $this->Html->link('<button class="btn btn-danger">Voltar</button>',
	                  '/academia/admin/admin_mensalidades/recebiveis',
	                  ['escape' =>  false]
	                ) ?>
				</div>
				<?= $this->Form->create(null) ?>
				<div class="col-xs-12 col-sm-6 text-center botoes">
					<button class="btn btn-success antecipacao-btn">Confirmar Antecipações</button>
				</div>
				<?= $this->Form->end() ?>
			</div>
		</div>
	</div>
</section>