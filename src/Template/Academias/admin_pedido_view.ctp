<section class="content">
    <div class="row">
        <div class="col-xs-12">
          <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <div class="row"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
                                        '/academia/admin/alunos',
                                        ['escape'   =>  false,
                                         'class'    =>  'btn pull-right']) ?>
                    </div>
                    <?= $this->Html->image('clientes/'.$pedido->cliente->image, ['alt' => $pedido->cliente->name, 'class' => 'profile-user-img img-responsive img-circle']) ?>
                    <h3 class="profile-username text-center"><?= h($pedido->cliente->name) ?></h3>
                    <p class="text-muted text-center">Código: <?= $this->Number->format($pedido->cliente->id) ?></p>
                    <div class="col-md-6">
                        <h3>Pedido #<?= h($pedido->id) ?></h3>
                        <div class="row">
                            <div class="col-sm-9">
                                <table class="table table-hover">
                                    <tr>
                                        <td class="font-bold"><?= __('Aluno') ?></td>
                                        <td><?= $pedido->cliente->name ?></td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold"><?= __('Status') ?></td>
                                        <td><?= $pedido->pedido_status->name ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-3">
                                <table class="table table-hover">
                                    <tr>
                                        <td class="font-bold"><?= __('Id') ?></td>
                                        <td><?= $this->Number->format($pedido->id) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold"><?= __('Valor') ?></td>
                                        <td><?= $this->Number->format($pedido->valor) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold"><?= __('Entrega') ?></td>
                                        <td><?= h($pedido->entrega) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold"><?= __('Created') ?></td>
                                        <td><?= h($pedido->created) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold"><?= __('Modified') ?></td>
                                        <td><?= h($pedido->modified) ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
</div>
<div class="">
    <div class="column large-12">
        <h4 class="font-bold"><?= __('Itens do Pedido') ?></h4>
        <?php if (!empty($pedido->pedido_itens)): ?>
            <table class="table table-hover">
                <tr>
                    <th><?= __('Cód') ?></th>
                    <th><?= __('Produto') ?></th>
                    <th><?= __('Quantidade') ?></th>
                    <th><?= __('Preco') ?></th>
                    <th><?= __('Total') ?></th>
                </tr>
                <?php foreach ($pedido->pedido_itens as $pedidoItens): ?>
                    <tr>
                        <td><?= h($pedidoItens->produto_id) ?></td>
                        <td><?= h($pedidoItens->produto->produto_base->name.' '
                                .$pedidoItens->produto->produto_base->embalagem_conteudo.' '
                                .$pedidoItens->produto->propriedade) ?></td>
                        <td><?= h($pedidoItens->quantidade) ?></td>
                        <td>R$ <?= h(number_format($pedidoItens->preco, 2, ',', '.')) ?></td>
                        <td>R$ <?= h(number_format($pedidoItens->quantidade * $pedidoItens->preco, 2, ',', '.')) ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>
</section>