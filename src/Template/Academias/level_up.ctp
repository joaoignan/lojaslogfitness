<style>

.level-up {
    height: 0%;
    width: 100%;
    position: fixed;
    z-index: 9999;
    top: 0;
    left: 0;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0, 0.9);
    overflow-y: hidden;
    transition: 0.5s;
}

.level-up-content {
    position: relative;
    top: 5%;
    height: 85%;
    left: 25%;
    width: 50%;
    border-radius: 25px;
    background: #4f88cf;
    text-align: center;
    margin-top: 10px;
    overflow-x: auto;
}

.level-up a {
    padding: 8px;
    text-decoration: none;
    font-size: 36px;
    color: #818181;
    display: block;
    transition: 0.3s;
}

.level-up img{
    width: 75%
}

.level-up a:hover, .level-up a:focus {
    color: #f1f1f1;
}

.level-up .closebtn {
    position: absolute;
    top: 20px;
    right: 45px;
    font-size: 60px;
}
@media screen and (max-width: 1024px) {
    .level-up-content {
    position: relative;
    top: 3%;
    height: auto;
    left: 10%;
    width: 80%;
    border-radius: 25px;
    background: #4f88cf;
    text-align: center;
    margin-top: 10px;
    }
    .level-up img{
    width: 75%
    }
}

@media screen and (max-width: 450px) {
  .level-up {overflow-y: auto;}
  .level-up a {font-size: 20px}
  .level-up .closebtn {
    font-size: 40px;
    top: 15px;
    right: 35px;
  }
  .level-up-content {
    position: relative;
    top: 20%;
    height: auto;
    left: 5%;
    width: 90%;
    border-radius: 25px;
    background: #4f88cf;
    text-align: center;
    margin-top: 10px;
    }

    .level-up img{
    width: 85%
    }
}
</style>
</head>
<body>

<div id="NavLevelUp" class="level-up">
  
  <div class="level-up-content">
    <?= $this->Html->image('level_2.jpg',
     [  'escape'    =>  'false',
        'alt'       =>  'level_2',
        'title'     =>  'parabéns!'
     ]) ?>
     <a href="javascript:void(0)" onclick="closeLevelUp()"><button class ="btn btn-success" >Beleza!</button></a>
  </div>
</div>


<span style="font-size:30px;cursor:pointer" onclick="openLevelUp()">&#9776; open</span>

<script>
function openLevelUp() {
    document.getElementById("NavLevelUp").style.height = "100%";
}

function closeLevelUp() {
    document.getElementById("NavLevelUp").style.height = "0%";
}
</script>
     
</body>