<style>
	.tabela-recebiveis{
	overflow-x: auto;
	}
	.tabela-recebiveis table{
		border: 2px solid rgba(80, 137, 207, 1.0);
		margin: 10px 0;
	}
	.tabela-recebiveis table th{
		padding: 10px;
		border-bottom: 2px solid rgba(80, 137, 207, 1.0);
		background-color: rgba(80, 137, 207, 0.2);
		border-collapse: collapse;
	}
	.tabela-recebiveis table td{
		padding: 10px;
		border-bottom: 2px solid rgba(80, 137, 207, 1.0);
		border-collapse: collapse;
	}
	@media all and (max-width: 1024px){
		.tabela-recebiveis{
			overflow-x: scroll;
		}
	}
</style>
<section class="content">
	<div class="col-xs-12">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Antecipar recebíveis</h3>
				<span class="pull-right"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
                  '/academia/admin/admin_mensalidades/dashboard',
                  ['escape' =>  false,
                     'class'  =>  'pull-right']) ?></span>
            </div>
            <?= $this->Form->create(null) ?>
				<div class="box-body">
					<div class="col-xs-12 tabela-recebiveis">
						<table width="1000px" align="center">
							<tr>
								<th></th>
								<th>Fatura</th>
								<th>Data</th>
								<th>Parcela</th>
								<th>Nº Parcelas</th>
								<th>Total</th>
								<th>Taxas</th>
								<th>Receber</th>
							</tr>
							<?php foreach ($transactions as $transaction) { ?>
								<?php $data = array_reverse(explode('-', $transaction->scheduled_date)) ?>
								<?php 
									$valor = explode(' ', $transaction->total);
				                    if($valor[1] == 'BRL') {
					                    $valor = str_replace(',', '.', $valor[0]);
					                } else {
					                    $valor = str_replace(',', '.', $valor[1]);
					                }
		                        	$valor = (double)$valor;
				                    $qtd_parcelas = (int)$transaction->number_of_installments;
                    				$parcela_atual = (int)$transaction->installment;
				                    if($qtd_parcelas == 1) {
				                    	$taxa = ($valor * $mensalidade_categoria->tax_card_porcent1) * .01;
				                    	$taxa = $taxa + $mensalidade_categoria->tax_card_valor1;
				                    } else if($qtd_parcelas >= 2 && $qtd_parcelas <= 3) {
				                    	$taxa = ($valor * $mensalidade_categoria->tax_card_porcent2) * .01;
				                    	if($parcela_atual == 1) {
				                            $taxa = $taxa + $mensalidade_categoria->tax_card_valor2;
				                        }
				                    } else if($qtd_parcelas >= 4 && $qtd_parcelas <= 6) {
				                    	$taxa = ($valor * $mensalidade_categoria->tax_card_porcent3) * .01;
				                    	if($parcela_atual == 1) {
				                            $taxa = $taxa + $mensalidade_categoria->tax_card_valor3;
				                        }
				                    } else if($qtd_parcelas >= 7) {
				                    	$taxa = ($valor * $mensalidade_categoria->tax_card_porcent4) * .01;
				                    	if($parcela_atual == 1) {
				                            $taxa = $taxa + $mensalidade_categoria->tax_card_valor4;
				                        }
				                    }
				                    $total = $valor - $taxa;
				                ?>
								<tr>
									<td><input type="checkbox" name="transaction.<?= $transaction->id ?>"></td>
									<td><?= $transaction->invoice_id ?></td>
									<td><?= $data[0].'/'.$data[1].'/'.$data[2] ?></td>
									<td><?= $transaction->installment ?></td>
									<td><?= $qtd_parcelas ?></td>
									<td>R$ <?= number_format($total, 2, ',', '.') ?></td>
									<td>R$ <?= number_format($custo_final[$transaction->id], 2, ',', '.') ?></td>
									<td>R$ <?= number_format($valor_final[$transaction->id], 2, ',', '.') ?></td>
								</tr>
							<?php } ?>
						</table>
					</div> <!-- col-xs-12 -->
					<div class="col-xs-12 text-center">
						<button class="btn btn-success antecipacao-btn">Antecipar Selecionadas</button>
					</div>
				</div>
			<?= $this->Form->end() ?>
		</div>
	</div>
</section>