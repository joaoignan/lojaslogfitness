<?= $this->Html->script('jquery.mask.min.js')?>
<script type="text/javascript">$("#cpf").mask("000.000.000-00");</script>
<script type="text/javascript">$("#telephone").mask("(00) 00000-0009");</script>
<script type="text/javascript">$(".phone-mask").mask("(00) 00000-0009");</script>
<script type="text/javascript">$(".cpf-mask").mask("000.000.000-00");</script>
<script type="text/javascript">$(".birth-mask").mask("00/00/0000");</script>
<script>
    function openBoleto() {
        document.getElementById("Boleto").style.height = "100%";
    }

    function closeBoleto() {
        document.getElementById("Boleto").style.height = "0%";
    }
    function openCartao() {
        document.getElementById("Cartao").style.height = "100%";
    }

    function closeCartao() {
        document.getElementById("Cartao").style.height = "0%";
    }
</script>

<style>
    .pagamento button{
        background-color: white;
    }
    .pagamento button .fa{
        color: #5089cf;
    }
    .mensalidades{
        display: none;
    }
    .ocult{
        display: none;
      }
    hr{
    	border-top: 1px solid rgba(243, 208, 18, 0.8);
	}
	.header-box{
		height: 80px;
	}
  .pacote-content {
    height: 215px;
  }
	.pacote-content .form-control{
		margin-bottom: 10px;
	}
	.metodos i {
		font-size: 75px;
		margin-bottom: 10px;
	}
	.metodos input[type="radio"] {
        visibility: hidden;
	}
	.metodos label i{
		display: block;
		color: gray;
		-webkit-transition: color 1s; /* For Safari 3.1 to 6.0 */
    	transition: color 1s;
	}
	.metodos input[type="radio"]:checked+label i{
   		color: rgba(80, 137, 207, 1.0);
	}
	.overlay {
    height: 0%;
    width: 100%;
    position: fixed;
    z-index: 99999!important;
    top: 0;
    left: 0;
    background-color: rgb(80, 137, 207)!important;
    background-color: rgba(80, 137, 207, 0.6)!important;
    overflow-y: hidden;
    transition: 0.5s;
  }
  .overlay-content {
    position: relative;
    top: 20%;
    height: 50%;
    left: 25%;
    width: 50%;
    background: white;
    border-radius: 10px;
    text-align: center;
    padding: 15px;
  }
  .overlay-content h3{
    font-family: nexa_boldregular;
    color: #5089cf;
  }
  .overlay-content h4{
    font-family: nexa_boldregular;
    color: #000;
  }
  .overlay-content .fa{
    color: rgba(80, 137, 207, 0.8);
    margin: 0 2px;
  }
  .overlay-logado .fa:hover{
    color: #f3d012
  }
  .overlay-content .form-control{
    margin: 10px 10%;
    width: 80%;
  }
  .overlay-content .forgot-password a{
    font-size: 12px;
    color: #444;
    transition: 0.3s;
  }
  .overlay-content .forgot-password a:hover{
    color: #f3d012;
    text-decoration: none;
  }
  .overlay a {
    color: #f3d012!important;
    padding: 8px;
    text-decoration: none;
    font-size: 36px;
    color: #fff;
    display: block;
    transition: 0.3s;
  }
  .overlay a:hover, .overlay a:focus {
    color: #f3d012;
  }
  .overlay .closebtn {
    position: absolute;
    top: 20px;
    right: 45px;
    font-size: 60px;
  }
  .pagamento{
    margin: 10px;
    padding: 22px;
    min-height: 215px;
    box-shadow: 0px 0px 5px 2px rgba(80, 137, 207, 0.4);
  }
  @media all and (max-width: 768px){
  	.header-box{
		  height: auto;
    }
  }
  @media all and (max-width: 450px){
  .overlay-content{
    height: 70%;
    left: 5%;
    width: 90%;
  }
  .academia-sobre .phone {
    width: 100%;
    display: inline-block;
  }
  .editar{
    margin-top: 10px;
  }
  .almoco{
  text-align: left!important;
  }
}
@media screen and (max-height: 450px) {
  .overlay {overflow-y: auto;}
  .overlay a {font-size: 20px}
  .overlay .closebtn {
    font-size: 40px;
    top: 15px;
    right: 35px;
  }
}
</style>

<style>
    .box-close-mochila{
            display: none;
        }
    .mochila{
        display: none;
    }
    .mochila-pagamento h4{
        text-align: left;
        margin: 0 0 10px;
        color: white;
        font-family: nexa_boldregular;
        display: flex;
        align-items: center;
        justify-content: center;
        height: 45px;
    }
    .titulo-pagamento{
        background-color:#5089cf ;
    }
    .pedidos-topo {
        background-color: #5089cf;
        color: white;
        text-transform: uppercase;
        font-weight: 700;
    }
    .dados-total{
        float: right;
    }
    .lista-produtos{
        overflow: auto;
        min-height: 130px;
        max-height: 225px;
        width: 100%;
        border: solid 1px;
        border-color: #5089cf;
    }
    .close-mochila-icon {
        cursor: auto;
    }
    .overlay-pagamento {
        overflow: auto;
        display: none;
        position: fixed;
        top: 0!important;
        left: 0;
        width: 100%;
        height: 100%;
        padding: 50px 0;
        background-color: rgba(80, 137, 207, 0.6)!important;
        color: #333;
        font-size: 19px;
        line-height: 30px;
        z-index: 9999;
    }

    .checkout .form {
        height: 250px;
        margin-top: 20px;
        overflow-x: hidden;
        overflow-y: scroll;
    }

    .checkout .form fieldset {
        border: none;
        padding: 0;
        padding: 10px 0;
        position: relative;
        clear: both;
    }

    .checkout .fieldset-expiration {
        float: left;
        width: 80%;
        margin-left: 10%;
    }

    .checkout .form fieldset .select {
        width: 84px;
        margin-right: 12px;
        float: left;
    }

    .checkout .form .bandeira-cartao {
        width: 80%;
        margin-left: 10%;
        float: left;
    }
    .checkout .form .numero-cartao {
        width: 80%;
        margin-left: 10%;
    }

    .checkout .form .nome-cartao {
        width: 80%;
        margin-left: 10%;
    }

    .checkout .form .fieldset-expiration {
        float: left;
        clear: none;
        width: 55%;
        margin-left: 10%;
    }

    .checkout .form .fieldset-expiration .select {
        width: 43%;
    }

    .checkout .form .fieldset-ccv {
        float: left;
        clear: none;
        width: 25%;
        margin-left: -8px;
    }

    .checkout .form .niver-cartao {
        width: 40%;
        margin-left: 10%;
        float: left;
        clear: none;
    }

    .checkout .form .fieldset-telefone {
        width: 40%;
        float: left;
        clear: none;
    }

    .checkout .form .fieldset-cpf {
        width: 80%;
        margin-left: 10%;
    }

    .checkout .form .fieldset-parcelas {
        width: 80%;
        margin-left: 10%;
    }

    .checkout .form label {
        display: block;
        text-transform: uppercase;
        font-size: 11px;
        color: hsla(0, 0%, 0%, .6);
        margin-bottom: 5px;
        font-weight: bold;
        font-family: Inconsolata;
    }

    .checkout .form input,
    .checkout .form .select {
        width: 96%;
        height: 38px;
        color: hsl(0, 0%, 20%);
        padding: 10px;
        border-radius: 5px;
        font-size: 15px;
        outline: none!important;
        border: 1px solid hsla(0, 0%, 0%, 0.3);
        box-shadow: inset 0 1px 4px hsla(0, 0%, 0%, 0.2);
    } 

    .checkout .form input .input-cart-number,
    .checkout .form .select .input-cart-number {
        width: 82px;
        display: inline-block;
        margin-right: 8px;
    }

    .checkout .form input .input-cart-number:last-child,
    .checkout .form .select .input-cart-number:last-child {
        margin-right: 0;
    }

    .checkout .form .select {
        position: relative;
    }

    .checkout .form .select:after {
        content: '';
        border-top: 8px solid #222;
        border-left: 4px solid transparent;
        border-right: 4px solid transparent;
        position: absolute;
        z-index: 2;
        top: 14px;
        right: 10px;
        pointer-events: none;
    }

    .checkout .form .select select {
        -webkit-appearance: none;
        appearance: none;
        position: absolute;
        padding: 0;
        border: none;
        width: 100%;
        outline: none!important;
        top: 6px;
        left: 6px;
        background: none;
    }

    .checkout {
      margin: 60px auto 0px;
      position: relative;
      width: 500px;
      background: white;
      border-radius: 15px;
      padding: 160px 45px 30px;
      box-shadow: 0 10px 40px hsla(0, 0, 0, .1);
    }

    .credit-card-box {
        perspective: 1000;
        width: 400px;
        height: 280px;
        position: absolute;
        top: -90px;
        left: 50%;
        transform: translateX(-50%);
        font-family: Inconsolata;
    }

    .credit-card-box.hover .flip {
        transform: rotateY(540deg);
    }

    .credit-card-box .front,
    .credit-card-box .back {
        width: 400px;
        height: 250px;
        border-radius: 15px;
        backface-visibility: hidden;
        background: #ccc;
        position: absolute;
        color: #fff;
        font-family: Inconsolata;
        top: 0;
        left: 0;
        text-shadow: 0 1px 1px hsla(0, 0, 0, 0.3);
        box-shadow: 0 1px 6px hsla(0, 0, 0, 0.3);
    }

    .card-color-change {
        position: absolute;
        z-index: -1;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border-radius: 15px;

        opacity: 0;

        background: -webkit-linear-gradient(135deg, #5087c7, #ffd41c);
        background: -moz-linear-gradient(135deg, #5087c7, #ffd41c);
        background: -o-linear-gradient(135deg, #5087c7, #ffd41c);
        background: linear-gradient(135deg, #5087c7, #ffd41c);

        -webkit-transition: opacity .7s linear;
        -moz-transition: opacity .7s linear;
        -o-transition: opacity .7s linear;
        transition: opacity .7s linear;
    }

    .credit-card-box .front::before, 
    .credit-card-box .back::before {
        content: '';
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        background: url('https://www.logfitness.com.br/webroot/images/earth.svg') no-repeat center;
        background-size: cover;
        opacity: .05;
    }

    .credit-card-box .flip {
        transition: 1s;
        transform-style: preserve-3d;
        position: relative;
    }

    .credit-card-box .logo {
        position: absolute;
        top: 9px;
        right: 20px;
        width: 60px;
    }
    
    .credit-card-box .logo svg,
    .credit-card-box .logo img {
        width: 100%;
        height: auto;
        fill: #fff;
        display: none;
    }

    .credit-card-box .front {
        z-index: 2;
        transform: rotateY(0deg);
    }

    .credit-card-box .back {
        transform: rotateY(180deg);
    }
    
    .credit-card-box .back .logo {
        top: 185px;
    }

    .credit-card-box .chip {
        position: absolute;
        width: 60px;
        height: 45px;
        top: 20px;
        left: 20px;
        background: linear-gradient(135deg, hsl(269,54%,87%) 0%,hsl(200,64%,89%) 44%,hsl(18,55%,94%) 100%);;
        border-radius: 8px;
    }

    .credit-card-box .chip::before {
        content: '';
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        border: 4px solid hsla(0, 0%, 50%, .1);
        width: 80%;
        height: 70%;
        border-radius: 5px;
    }
    
    .credit-card-box .strip {
        background: linear-gradient(135deg, hsl(0, 0%, 25%), hsl(0, 0%, 10%));
        position: absolute;
        width: 100%;
        height: 50px;
        top: 30px;
        left: 0;
    }

    .credit-card-box .number {
        position: absolute;
        margin: 0 auto;
        top: 103px;
        left: 19px;
        font-size: 38px;
    }

    .credit-card-box label {
        font-size: 10px;
        letter-spacing: 1px;
        text-shadow: none;
        text-transform: uppercase;
        font-weight: normal;
        opacity: 0.5;
        display: block;
        margin-bottom: 3px;
    }

    .credit-card-box .card-holder,
    .credit-card-box .card-expiration-date {
        position: absolute;
        margin: 0 auto;
        top: 180px;
        left: 19px;
        font-size: 22px;
        text-transform: capitalize;
    }

    .credit-card-box .card-expiration-date {
        text-align: right;
        left: auto;
        right: 20px;
    }

    .credit-card-box .ccv {
        height: 36px;
        background: #fff;
        width: 91%;
        border-radius: 5px;
        top: 110px;
        left: 0;
        right: 0;
        position: absolute;
        margin: 0 auto;
        color: #000;
        text-align: right;
        padding: 10px;
    }

    .credit-card-box .ccv label {
        margin-top: -32px;
    }

    .credit-card-box label {
        margin: -25px 0 14px;
        color: #fff;
    }
    .input-cart-number {
        width: 96%!important;
    }
    .bandeira-cartao select {
        width: 96%;
        height: 38px;
        color: hsl(0, 0%, 20%);
        padding: 10px;
        border-radius: 5px;
        font-size: 15px;
        outline: none!important;
        border: 1px solid hsla(0, 0%, 0%, 0.3);
        box-shadow: inset 0 1px 4px hsla(0, 0%, 0%, 0.2);
    }
    #cartao-pagamento {
        display: none;
    }
    .fieldset-parcelas select {
        width: 96%;
        height: 38px;
        color: hsl(0, 0%, 20%);
        padding: 10px;
        border-radius: 5px;
        font-size: 15px;
        outline: none!important;
        border: 1px solid hsla(0, 0%, 0%, 0.3);
        box-shadow: inset 0 1px 4px hsla(0, 0%, 0%, 0.2);
    }
    .fechar-overlay {
        cursor:pointer;
        color: #5087c7;
        position:absolute;
        top: 15px;
        right: 20px;
    }
    .logo-moip {
        position: absolute;
        right: 8px;
    }
    #pagarCredito {
        margin-left: -10px;
        width: 200px; 
        margin-top: 10px; 
        border-color: #8DC73F; 
        background-color: #8DC73F; 
        color: white;
    }
    .ccv-number {
        margin-top: -18px;
    }
    .baixo-produtos {
        padding-left: 230px;
    }
    .erro-style {
        color: #f90000;
        font-size: 11px;
        font-style: italic;
        margin-left: 10px;
    }
    @media all and (max-width: 768px) {
        .baixo-produtos {
            padding-left: 0;
            padding-bottom: 30px;
        }
    }
    @media all and (max-width: 600px) {
        .credit-card-box {
            top: -50px;
            left: 50%;
            width: 300px;
            height: 187.5px;
        }
        .credit-card-box .front,
        .credit-card-box .back {
            width: 300px;
            height: 187.5px;
        }
        .checkout {
            width: 400px;
            padding: 160px 20px 30px 30px;
        }
        .chip {
            width: 40px!important;
            height: 30px!important;
        }
        .credit-card-box .number {
            font-size: 28px;
            top: 90px;
        }
        .credit-card-box .card-holder,
        .credit-card-box .card-expiration-date {
            top: 130px;
            font-size: 18px;
        }
        .checkout .form fieldset .select {
            margin-right: 9px;
        }
        .credit-card-box .strip {
            height: 30px;
        }
        .credit-card-box .ccv {
            top: 90px;
            height: 26px;
        }
        .ccv-number {
            margin-top: -23px;
        }
        .credit-card-box .back .logo {
            top: 145px;
        }
    }
    @media all and (max-width: 470px) {
        .credit-card-box {
            width: 240px;
            height: 150px;
        }
        .credit-card-box .front,
        .credit-card-box .back {
            width: 240px;
            height: 150px;
        }
        .checkout {
            width: 300px;
            padding: 90px 0px 30px 0px;
        }
        .chip {
            width: 40px!important;
            height: 30px!important;
            top: 15px;
        }
        .credit-card-box .number {
            font-size: 21px;
            top: 60px;
        }
        .credit-card-box .card-holder,
        .credit-card-box .card-expiration-date {
            top: 110px;
            font-size: 16px;
            line-height: 16px;
        }
        .credit-card-box .strip {
            height: 20px;
        }
        .credit-card-box .ccv {
            top: 70px;
            height: 22px;
        }
        .ccv-number {
            margin-top: -26px;
        }
        .credit-card-box .back .logo {
            top: 95px;
        }
        .credit-card-box .front .logo {
            top: 4px;
        }
        .fechar-overlay {
            right: 10px;
        }
        .checkout .form .numero-cartao,
        .checkout .form .bandeira-cartao,
        .checkout .form .nome-cartao,
        .checkout .form .fieldset-cpf,
        .checkout .form .fieldset-parcelas {
            width: 90%;
            margin-left: 6%;
        }
        .checkout .form .fieldset-ccv {
            width: 30%;
            margin-left: -6px;
        }
        .checkout .form .fieldset-expiration {
            width: 60%;
            margin-left: 6%;
        }
        .checkout .form .niver-cartao {
            width: 45%;
            margin-left: 6%;
        }
        .checkout .form .fieldset-telefone {
            width: 44%;
        }
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        $('#pag_boleto, #pag_cc').click(function() {
            $('.overlay-pagamento').fadeIn();
            $('body').css('overflow', 'hidden');
        });

        $('.fechar-overlay').click(function() {
            $('.overlay-pagamento').fadeOut();
            $('body').css('overflow', 'auto');
            $('#cartao-pagamento, #boleto-pagamento').fadeOut();
        });

        $('#pag_cc').click(function() {
            $('#boleto-pagamento').fadeOut();
            $('#cartao-pagamento').fadeIn();
        });

        $('#pag_boleto').click(function() {
            $('#cartao-pagamento').fadeOut();
            $('#boleto-pagamento').fadeIn();
        });

        //Iugu

        var bandeira;

        $('.input-cart-number').blur(function() {
            bandeira = Iugu.utils.getBrandByCreditCardNumber($(this).val());
            var valido = Iugu.utils.validateCreditCardNumber($(this).val());

            if(!valido) {
                $('.erro-card').remove();
                $('.numero-cartao').append('<p class="erro-card erro-style">* Número de cartão inválido!</p>')
            } else {
                $('.erro-card').remove();
            }

            $('.logo-cartao').fadeOut(300);
            $('#iugu_brand').val(bandeira);

            if(bandeira == 'visa') {
                $('.visa').delay(300).fadeIn();
                $('.card-color-change').css('opacity', 1);
            } else if(bandeira == 'mastercard') {
                $('.mastercard').delay(300).fadeIn();
                $('.card-color-change').css('opacity', 1);
            } else if(bandeira == 'amex') {
                $('.amex').delay(300).fadeIn();
                $('.card-color-change').css('opacity', 1);
            } else if(bandeira == 'Diners') {
                $('.diners').delay(300).fadeIn();
                $('.card-color-change').css('opacity', 1);
            } else if(bandeira == false) {
                $('.card-color-change').css('opacity', 0);
            }
        });

        $('#card-ccv').blur(function() { 
            if(bandeira != null) {
                var valido = Iugu.utils.validateCVV($(this).val(), bandeira);

                if(!valido) {
                    $('.erro-cvv').remove();
                    $('.fieldset-ccv').append('<p class="erro-cvv erro-style">* Número de ccv inválido!</p>')
                } else {
                    $('.erro-cvv').remove();
                }
            }
        });

        //End Iugu

        $('.input-cart-number').on('keyup change', function(){

            var card1 = $(this).val().substring(0,4);
            var card2 = $(this).val().substring(4,8);
            var card3 = $(this).val().substring(8,12);
            var card4 = $(this).val().substring(12,16);
          
            $('.credit-card-box .number').html(card1 + ' ' + card2 + ' ' + card3 + ' ' + card4);
        });

        $('#card-holder').on('keyup change', function(){
          $t = $(this);
          $('.credit-card-box .card-holder div').html($t.val());
        });

        $('#card-expiration-month, #card-expiration-year').change(function(){
          m = $('#card-expiration-month option').index($('#card-expiration-month option:selected'));
          m = (m < 10) ? '0' + m : m;
          y = $('#card-expiration-year').val();
          $('.card-expiration-date div').html(m + '/' + y);
          $('#card-expiration-hidden').val(m + '/' + y);

          if(m != '' && y != '') {
            var valido = Iugu.utils.validateExpirationString($('#card-expiration-hidden').val());

            if(!valido) {
                $('.erro-expiration').remove();
                $('.fieldset-expiration').append('<p class="erro-expiration erro-style">* Data de validade inválida!</p>')
            } else {
                $('.erro-expiration').remove();
            }
          }
        })

        $('#card-ccv').on('focus', function(){
          $('.credit-card-box').addClass('hover');
        }).on('blur', function(){
          $('.credit-card-box').removeClass('hover');
        }).on('keyup change', function(){
          $('.ccv div').html($(this).val());
        });

        setTimeout(function(){
          $('#card-ccv').focus().delay(1000).queue(function(){
            $(this).blur().dequeue();
          });
        }, 500);
    });
</script>

<div class="row">
	<div class="col-xs-12 text-center">
		<h2>Adquirir plano</h2>
	</div>
</div>
<hr>
<div class="col-xs-12 col-sm-12 col-md-4 text-center">
	<div class="header-box">
		<h3>Passo 1 - Confira o seu plano</h3>
	</div>
    <div class="col-xs-12">
    	<div class="pacote-content">
            <?php if($plano->type == 2){?>
                <div class="faixa-assinatura text-center">
                    <span>assinatura</span>
                </div>
            <?php } ?>
    	    <div class="col-xs-12 header-paco">
    	        <h4><?= $plano->name ?></h4>
    	    </div> 
            <p>Tipo: <?= $plano->type == 1 ? 'Plano' : 'Assinatura' ?></p>
    	    <p>Duração: <?= $plano->time_months == 1 ? $plano->time_months.' mês' : $plano->time_months.' meses' ?></p>
    	    <p>R$ <?= number_format($valor_mes = $plano->valor_total/$plano->time_months, 2, ',', ' '); ?> / mês</p>
    	    <label style="height: 55px"><?= $plano->descricao ?></label>
    	</div>
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-8 text-center">
	<div class="header-box">
		<h3>Passo 2 - Selecione o método de pagamento</h3>
	</div>
	<div class="col-xs-12 metodos pagamento">
        <?php if($plano->type == 1) { ?>
            <?php if($plano->method_payment != 3) { ?>
                <div class="col-xs-12 col-sm-12 <?= $plano->method_payment == 1 ? 'col-md-6' : 'col-md-4 col-md-offset-4' ?> text-center">
                  <h3>Boleto</h3>
                  <button class="btn btn-cupom col-xs-12 text-center" id="pag_boleto" style=" height: 90px;">
                      <i class="fa fa-barcode"></i>
                  </button>
                </div>
            <?php } ?>
            <?php if($plano->method_payment != 2) { ?>
                <div class="col-xs-12 col-sm-12 <?= $plano->method_payment == 1 ? 'col-md-6' : 'col-md-4 col-md-offset-4' ?> text-center">
                  <h3>Crédito</h3>
                  <button class="btn btn-cupom col-xs-12" id="pag_cc" style=" height: 90px;">
                      <i class="fa fa-credit-card"></i>
                  </button>
            	</div>
            <?php } ?>
        <?php } else { ?>
            <div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4 text-center">
              <h3>Crédito</h3>
              <button class="btn btn-cupom col-xs-12" id="pag_cc" style=" height: 90px;">
                  <i class="fa fa-credit-card"></i>
              </button>
            </div>
        <?php } ?>
    </div>

<div class="overlay-pagamento">
    <div class="checkout" id="cartao-pagamento">
      <div class="credit-card-box">
        <div class="flip">
          <div class="front">
            <span class="card-color-change"></span>
            <div class="chip"></div>
            <div class="logo">
              <svg version="1.1" id="visa" class="logo-cartao visa" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                   width="47.834px" height="47.834px" viewBox="0 0 47.834 47.834" style="enable-background:new 0 0 47.834 47.834;">
                <g>
                  <g>
                    <path d="M44.688,16.814h-3.004c-0.933,0-1.627,0.254-2.037,1.184l-5.773,13.074h4.083c0,0,0.666-1.758,0.817-2.143
                             c0.447,0,4.414,0.006,4.979,0.006c0.116,0.498,0.474,2.137,0.474,2.137h3.607L44.688,16.814z M39.893,26.01
                             c0.32-0.819,1.549-3.987,1.549-3.987c-0.021,0.039,0.317-0.825,0.518-1.362l0.262,1.23c0,0,0.745,3.406,0.901,4.119H39.893z
                             M34.146,26.404c-0.028,2.963-2.684,4.875-6.771,4.875c-1.743-0.018-3.422-0.361-4.332-0.76l0.547-3.193l0.501,0.228
                             c1.277,0.532,2.104,0.747,3.661,0.747c1.117,0,2.313-0.438,2.325-1.393c0.007-0.625-0.501-1.07-2.016-1.77
                             c-1.476-0.683-3.43-1.827-3.405-3.876c0.021-2.773,2.729-4.708,6.571-4.708c1.506,0,2.713,0.31,3.483,0.599l-0.526,3.092
                             l-0.351-0.165c-0.716-0.288-1.638-0.566-2.91-0.546c-1.522,0-2.228,0.634-2.228,1.227c-0.008,0.668,0.824,1.108,2.184,1.77
                             C33.126,23.546,34.163,24.783,34.146,26.404z M0,16.962l0.05-0.286h6.028c0.813,0.031,1.468,0.29,1.694,1.159l1.311,6.304
                             C7.795,20.842,4.691,18.099,0,16.962z M17.581,16.812l-6.123,14.239l-4.114,0.007L3.862,19.161
                             c2.503,1.602,4.635,4.144,5.386,5.914l0.406,1.469l3.808-9.729L17.581,16.812L17.581,16.812z M19.153,16.8h3.89L20.61,31.066
                             h-3.888L19.153,16.8z"/>
                  </g>
                </g>
              </svg>
              <svg version="1.1" id="mastercard" class="logo-cartao mastercard" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 504 504" style="enable-background:new 0 0 504 504;" xml:space="preserve">
                <path style="fill:#FFB600;" d="M504,252c0,83.2-67.2,151.2-151.2,151.2c-83.2,0-151.2-68-151.2-151.2l0,0
                    c0-83.2,67.2-151.2,150.4-151.2C436.8,100.8,504,168.8,504,252L504,252z"/>
                <path style="fill:#F7981D;" d="M352.8,100.8c83.2,0,151.2,68,151.2,151.2l0,0c0,83.2-67.2,151.2-151.2,151.2
                    c-83.2,0-151.2-68-151.2-151.2"/>
                <path style="fill:#FF8500;" d="M352.8,100.8c83.2,0,151.2,68,151.2,151.2l0,0c0,83.2-67.2,151.2-151.2,151.2"/>
                <path style="fill:#FF5050;" d="M149.6,100.8C67.2,101.6,0,168.8,0,252s67.2,151.2,151.2,151.2c39.2,0,74.4-15.2,101.6-39.2l0,0l0,0
                    c5.6-4.8,10.4-10.4,15.2-16h-31.2c-4-4.8-8-10.4-11.2-15.2h53.6c3.2-4.8,6.4-10.4,8.8-16h-71.2c-2.4-4.8-4.8-10.4-6.4-16h83.2
                    c4.8-15.2,8-31.2,8-48c0-11.2-1.6-21.6-3.2-32h-92.8c0.8-5.6,2.4-10.4,4-16h83.2c-1.6-5.6-4-11.2-6.4-16H216
                    c2.4-5.6,5.6-10.4,8.8-16h53.6c-3.2-5.6-7.2-11.2-12-16h-29.6c4.8-5.6,9.6-10.4,15.2-15.2c-26.4-24.8-62.4-39.2-101.6-39.2
                    C150.4,100.8,150.4,100.8,149.6,100.8z"/>
                <path style="fill:#E52836;" d="M0,252c0,83.2,67.2,151.2,151.2,151.2c39.2,0,74.4-15.2,101.6-39.2l0,0l0,0
                    c5.6-4.8,10.4-10.4,15.2-16h-31.2c-4-4.8-8-10.4-11.2-15.2h53.6c3.2-4.8,6.4-10.4,8.8-16h-71.2c-2.4-4.8-4.8-10.4-6.4-16h83.2
                    c4.8-15.2,8-31.2,8-48c0-11.2-1.6-21.6-3.2-32h-92.8c0.8-5.6,2.4-10.4,4-16h83.2c-1.6-5.6-4-11.2-6.4-16H216
                    c2.4-5.6,5.6-10.4,8.8-16h53.6c-3.2-5.6-7.2-11.2-12-16h-29.6c4.8-5.6,9.6-10.4,15.2-15.2c-26.4-24.8-62.4-39.2-101.6-39.2h-0.8"/>
                <path style="fill:#CB2026;" d="M151.2,403.2c39.2,0,74.4-15.2,101.6-39.2l0,0l0,0c5.6-4.8,10.4-10.4,15.2-16h-31.2
                    c-4-4.8-8-10.4-11.2-15.2h53.6c3.2-4.8,6.4-10.4,8.8-16h-71.2c-2.4-4.8-4.8-10.4-6.4-16h83.2c4.8-15.2,8-31.2,8-48
                    c0-11.2-1.6-21.6-3.2-32h-92.8c0.8-5.6,2.4-10.4,4-16h83.2c-1.6-5.6-4-11.2-6.4-16H216c2.4-5.6,5.6-10.4,8.8-16h53.6
                    c-3.2-5.6-7.2-11.2-12-16h-29.6c4.8-5.6,9.6-10.4,15.2-15.2c-26.4-24.8-62.4-39.2-101.6-39.2h-0.8"/>
                <g>
                    <path style="fill:#FFFFFF;" d="M204.8,290.4l2.4-13.6c-0.8,0-2.4,0.8-4,0.8c-5.6,0-6.4-3.2-5.6-4.8l4.8-28h8.8l2.4-15.2h-8l1.6-9.6
                        h-16c0,0-9.6,52.8-9.6,59.2c0,9.6,5.6,13.6,12.8,13.6C199.2,292.8,203.2,291.2,204.8,290.4z"/>
                    <path style="fill:#FFFFFF;" d="M210.4,264.8c0,22.4,15.2,28,28,28c12,0,16.8-2.4,16.8-2.4l3.2-15.2c0,0-8.8,4-16.8,4
                        c-17.6,0-14.4-12.8-14.4-12.8H260c0,0,2.4-10.4,2.4-14.4c0-10.4-5.6-23.2-23.2-23.2C222.4,227.2,210.4,244.8,210.4,264.8z
                         M238.4,241.6c8.8,0,7.2,10.4,7.2,11.2H228C228,252,229.6,241.6,238.4,241.6z"/>
                    <path style="fill:#FFFFFF;" d="M340,290.4l3.2-17.6c0,0-8,4-13.6,4c-11.2,0-16-8.8-16-18.4c0-19.2,9.6-29.6,20.8-29.6
                        c8,0,14.4,4.8,14.4,4.8l2.4-16.8c0,0-9.6-4-18.4-4c-18.4,0-36.8,16-36.8,46.4c0,20,9.6,33.6,28.8,33.6
                        C331.2,292.8,340,290.4,340,290.4z"/>
                    <path style="fill:#FFFFFF;" d="M116.8,227.2c-11.2,0-19.2,3.2-19.2,3.2L95.2,244c0,0,7.2-3.2,17.6-3.2c5.6,0,10.4,0.8,10.4,5.6
                        c0,3.2-0.8,4-0.8,4s-4.8,0-7.2,0c-13.6,0-28.8,5.6-28.8,24c0,14.4,9.6,17.6,15.2,17.6c11.2,0,16-7.2,16.8-7.2l-0.8,6.4H132l6.4-44
                        C138.4,228,122.4,227.2,116.8,227.2z M120,263.2c0,2.4-1.6,15.2-11.2,15.2c-4.8,0-6.4-4-6.4-6.4c0-4,2.4-9.6,14.4-9.6
                        C119.2,263.2,120,263.2,120,263.2z"/>
                    <path style="fill:#FFFFFF;" d="M153.6,292c4,0,24,0.8,24-20.8c0-20-19.2-16-19.2-24c0-4,3.2-5.6,8.8-5.6c2.4,0,11.2,0.8,11.2,0.8
                        l2.4-14.4c0,0-5.6-1.6-15.2-1.6c-12,0-24,4.8-24,20.8c0,18.4,20,16.8,20,24c0,4.8-5.6,5.6-9.6,5.6c-7.2,0-14.4-2.4-14.4-2.4
                        l-2.4,14.4C136,290.4,140,292,153.6,292z"/>
                    <path style="fill:#FFFFFF;" d="M472.8,214.4l-3.2,21.6c0,0-6.4-8-15.2-8c-14.4,0-27.2,17.6-27.2,38.4c0,12.8,6.4,26.4,20,26.4
                        c9.6,0,15.2-6.4,15.2-6.4l-0.8,5.6h16l12-76.8L472.8,214.4z M465.6,256.8c0,8.8-4,20-12.8,20c-5.6,0-8.8-4.8-8.8-12.8
                        c0-12.8,5.6-20.8,12.8-20.8C462.4,243.2,465.6,247.2,465.6,256.8z"/>
                    <path style="fill:#FFFFFF;" d="M29.6,291.2l9.6-57.6l1.6,57.6H52l20.8-57.6L64,291.2h16.8l12.8-76.8H67.2l-16,47.2l-0.8-47.2H27.2
                        l-12.8,76.8H29.6z"/>
                    <path style="fill:#FFFFFF;" d="M277.6,291.2c4.8-26.4,5.6-48,16.8-44c1.6-10.4,4-14.4,5.6-18.4c0,0-0.8,0-3.2,0
                        c-7.2,0-12.8,9.6-12.8,9.6l1.6-8.8h-15.2L260,292h17.6V291.2z"/>
                    <path style="fill:#FFFFFF;" d="M376.8,227.2c-11.2,0-19.2,3.2-19.2,3.2l-2.4,13.6c0,0,7.2-3.2,17.6-3.2c5.6,0,10.4,0.8,10.4,5.6
                        c0,3.2-0.8,4-0.8,4s-4.8,0-7.2,0c-13.6,0-28.8,5.6-28.8,24c0,14.4,9.6,17.6,15.2,17.6c11.2,0,16-7.2,16.8-7.2l-0.8,6.4H392l6.4-44
                        C399.2,228,382.4,227.2,376.8,227.2z M380.8,263.2c0,2.4-1.6,15.2-11.2,15.2c-4.8,0-6.4-4-6.4-6.4c0-4,2.4-9.6,14.4-9.6
                        C380,263.2,380,263.2,380.8,263.2z"/>
                    <path style="fill:#FFFFFF;" d="M412,291.2c4.8-26.4,5.6-48,16.8-44c1.6-10.4,4-14.4,5.6-18.4c0,0-0.8,0-3.2,0
                        c-7.2,0-12.8,9.6-12.8,9.6l1.6-8.8h-15.2L394.4,292H412V291.2z"/>
                </g>
                <g>
                    <path style="fill:#DCE5E5;" d="M180,279.2c0,9.6,5.6,13.6,12.8,13.6c5.6,0,10.4-1.6,12-2.4l2.4-13.6c-0.8,0-2.4,0.8-4,0.8
                        c-5.6,0-6.4-3.2-5.6-4.8l4.8-28h8.8l2.4-15.2h-8l1.6-9.6"/>
                    <path style="fill:#DCE5E5;" d="M218.4,264.8c0,22.4,7.2,28,20,28c12,0,16.8-2.4,16.8-2.4l3.2-15.2c0,0-8.8,4-16.8,4
                        c-17.6,0-14.4-12.8-14.4-12.8H260c0,0,2.4-10.4,2.4-14.4c0-10.4-5.6-23.2-23.2-23.2C222.4,227.2,218.4,244.8,218.4,264.8z
                         M238.4,241.6c8.8,0,10.4,10.4,10.4,11.2H228C228,252,229.6,241.6,238.4,241.6z"/>
                    <path style="fill:#DCE5E5;" d="M340,290.4l3.2-17.6c0,0-8,4-13.6,4c-11.2,0-16-8.8-16-18.4c0-19.2,9.6-29.6,20.8-29.6
                        c8,0,14.4,4.8,14.4,4.8l2.4-16.8c0,0-9.6-4-18.4-4c-18.4,0-28.8,16-28.8,46.4c0,20,1.6,33.6,20.8,33.6
                        C331.2,292.8,340,290.4,340,290.4z"/>
                    <path style="fill:#DCE5E5;" d="M95.2,244.8c0,0,7.2-3.2,17.6-3.2c5.6,0,10.4,0.8,10.4,5.6c0,3.2-0.8,4-0.8,4s-4.8,0-7.2,0
                        c-13.6,0-28.8,5.6-28.8,24c0,14.4,9.6,17.6,15.2,17.6c11.2,0,16-7.2,16.8-7.2l-0.8,6.4H132l6.4-44c0-18.4-16-19.2-22.4-19.2
                         M128,263.2c0,2.4-9.6,15.2-19.2,15.2c-4.8,0-6.4-4-6.4-6.4c0-4,2.4-9.6,14.4-9.6C119.2,263.2,128,263.2,128,263.2z"/>
                    <path style="fill:#DCE5E5;" d="M136,290.4c0,0,4.8,1.6,18.4,1.6c4,0,24,0.8,24-20.8c0-20-19.2-16-19.2-24c0-4,3.2-5.6,8.8-5.6
                        c2.4,0,11.2,0.8,11.2,0.8l2.4-14.4c0,0-5.6-1.6-15.2-1.6c-12,0-16,4.8-16,20.8c0,18.4,12,16.8,12,24c0,4.8-5.6,5.6-9.6,5.6"/>
                    <path style="fill:#DCE5E5;" d="M469.6,236c0,0-6.4-8-15.2-8c-14.4,0-19.2,17.6-19.2,38.4c0,12.8-1.6,26.4,12,26.4
                        c9.6,0,15.2-6.4,15.2-6.4l-0.8,5.6h16l12-76.8 M468.8,256.8c0,8.8-7.2,20-16,20c-5.6,0-8.8-4.8-8.8-12.8c0-12.8,5.6-20.8,12.8-20.8
                        C462.4,243.2,468.8,247.2,468.8,256.8z"/>
                    <path style="fill:#DCE5E5;" d="M29.6,291.2l9.6-57.6l1.6,57.6H52l20.8-57.6L64,291.2h16.8l12.8-76.8h-20l-22.4,47.2l-0.8-47.2h-8.8
                        l-27.2,76.8H29.6z"/>
                    <path style="fill:#DCE5E5;" d="M260.8,291.2h16.8c4.8-26.4,5.6-48,16.8-44c1.6-10.4,4-14.4,5.6-18.4c0,0-0.8,0-3.2,0
                        c-7.2,0-12.8,9.6-12.8,9.6l1.6-8.8"/>
                    <path style="fill:#DCE5E5;" d="M355.2,244.8c0,0,7.2-3.2,17.6-3.2c5.6,0,10.4,0.8,10.4,5.6c0,3.2-0.8,4-0.8,4s-4.8,0-7.2,0
                        c-13.6,0-28.8,5.6-28.8,24c0,14.4,9.6,17.6,15.2,17.6c11.2,0,16-7.2,16.8-7.2l-0.8,6.4H392l6.4-44c0-18.4-16-19.2-22.4-19.2
                         M388,263.2c0,2.4-9.6,15.2-19.2,15.2c-4.8,0-6.4-4-6.4-6.4c0-4,2.4-9.6,14.4-9.6C380,263.2,388,263.2,388,263.2z"/>
                    <path style="fill:#DCE5E5;" d="M395.2,291.2H412c4.8-26.4,5.6-48,16.8-44c1.6-10.4,4-14.4,5.6-18.4c0,0-0.8,0-3.2,0
                        c-7.2,0-12.8,9.6-12.8,9.6l1.6-8.8"/>
                </g>
              </svg>
              <svg version="1.1" id="amex" class="logo-cartao amex" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 291.764 291.764" style="enable-background:new 0 0 291.764 291.764;" xml:space="preserve">
                <g>
                    <path style="fill:#26A6D1;" d="M18.235,41.025h255.294c10.066,0,18.235,8.169,18.235,18.244v173.235
                        c0,10.066-8.169,18.235-18.235,18.235H18.235C8.16,250.74,0,242.57,0,232.505V59.269C0,49.194,8.169,41.025,18.235,41.025z"/>
                    <path style="fill:#FFFFFF;" d="M47.047,113.966l-28.812,63.76h34.492l4.276-10.166h9.774l4.276,10.166h37.966v-7.759l3.383,7.759
                        h19.639l3.383-7.923v7.923h78.959l9.601-9.902l8.99,9.902l40.555,0.082l-28.903-31.784l28.903-32.058h-39.926l-9.346,9.719
                        l-8.707-9.719h-85.897l-7.376,16.457l-7.549-16.457h-34.42v7.495l-3.829-7.495C76.479,113.966,47.047,113.966,47.047,113.966z
                         M53.721,123.02h16.813l19.111,43.236V123.02h18.418l14.761,31l13.604-31h18.326v45.752h-11.151l-0.091-35.851l-16.257,35.851
                        h-9.975l-16.348-35.851v35.851h-22.94l-4.349-10.257H50.147l-4.34,10.248H33.516C33.516,168.763,53.721,123.02,53.721,123.02z
                         M164.956,123.02h45.342L224.166,138l14.315-14.98h13.868l-21.071,22.995l21.071,22.73h-14.497l-13.868-15.154l-14.388,15.154
                        h-44.64L164.956,123.02L164.956,123.02z M61.9,130.761l-7.741,18.272h15.473L61.9,130.761z M176.153,132.493v8.352h24.736v9.309
                        h-24.736v9.118h27.745l12.892-13.43l-12.345-13.357h-28.292L176.153,132.493z"/>
                </g>
              </svg>
              <svg version="1.1" id="diners" class="logo-cartao diners" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="43.96px" height="43.959px" viewBox="0 0 43.96 43.959" style="enable-background:new 0 0 43.96 43.959;"
                     xml:space="preserve">
                <g>
                    <g id="Layer_1_copy_44_">
                        <path d="M23.6,39.674c-0.064-0.15-0.125-0.336-0.187-0.516l-0.933-2.646c-0.017-0.043-0.03-0.088-0.045-0.129
                            c-0.017-0.025-0.043-0.029-0.056-0.029c-0.021,0-0.034,0.008-0.049,0.013c-0.099,0.062-0.302,0.164-0.459,0.217
                            c-0.043,0.202-0.135,0.453-0.201,0.642l-0.811,2.322c-0.074,0.207-0.23,0.312-0.418,0.312h-0.03H20.39v0.174v0.021h0.024
                            c0.181-0.01,0.362-0.021,0.545-0.021c0.202,0,0.409,0.011,0.61,0.021h0.022v-0.174V39.86h-0.077c-0.168,0-0.356-0.03-0.358-0.154
                            c0-0.082,0.061-0.216,0.112-0.389l0.156-0.512h1.115l0.193,0.572c0.057,0.16,0.104,0.299,0.1,0.355
                            c0,0.098-0.172,0.125-0.291,0.125h-0.057h-0.021v0.174v0.023l0.022-0.002c0.26-0.009,0.51-0.021,0.758-0.021
                            c0.24,0,0.471,0.012,0.695,0.021l0.022,0.002v-0.177v-0.021h-0.051C23.751,39.861,23.658,39.805,23.6,39.674z M23.941,40.036
                            c-0.228-0.01-0.453-0.021-0.695-0.021c-0.248,0-0.502,0.011-0.76,0.021v-0.15h0.057c0.117,0,0.312-0.021,0.312-0.147
                            c0-0.065-0.045-0.2-0.101-0.363l-0.198-0.587h-1.147l-0.161,0.527c-0.051,0.17-0.112,0.304-0.112,0.394
                            c0,0.153,0.213,0.177,0.379,0.177h0.057v0.152c-0.204-0.01-0.41-0.02-0.614-0.02c-0.182,0-0.363,0.009-0.545,0.02v-0.152h0.03
                            c0.197,0,0.366-0.117,0.439-0.33l0.81-2.319c0.066-0.188,0.158-0.438,0.188-0.629c0.161-0.056,0.363-0.155,0.458-0.218
                            c0.016-0.004,0.023-0.01,0.041-0.01c0.016,0,0.025,0,0.036,0.016c0.015,0.041,0.03,0.087,0.046,0.125l0.932,2.646
                            c0.061,0.178,0.119,0.363,0.188,0.518c0.061,0.143,0.166,0.201,0.334,0.201h0.027V40.036z M27.273,36.526
                            c0.194,0.002,0.483,0.021,0.725,0.021c0.246,0,0.539-0.02,0.771-0.021v0.107h-0.047c-0.185,0-0.396,0.031-0.396,0.312v2.648
                            c0,0.279,0.213,0.312,0.396,0.312h0.047v0.105c-0.238,0-0.531-0.02-0.775-0.02c-0.242,0-0.527,0.018-0.721,0.02v-0.105h0.047
                            c0.182,0,0.396-0.032,0.396-0.312v-2.648c0-0.28-0.215-0.312-0.396-0.312h-0.047V36.526z M21.975,37.071v0.003l-0.469,1.431h0.945
                            l-0.465-1.434H21.975z M21.535,38.479l0.445-1.354l0.439,1.354H21.535z M26.96,36.373l0.078,0.013
                            c-0.021,0.131-0.039,0.26-0.054,0.391c-0.01,0.132-0.01,0.265-0.01,0.396l-0.111,0.041c-0.008-0.188-0.053-0.478-0.393-0.479
                            h-0.852v2.714c0,0.396,0.197,0.457,0.439,0.458h0.073v0.106c-0.198-0.002-0.536-0.021-0.801-0.021
                            c-0.293,0-0.634,0.019-0.834,0.021v-0.105h0.073c0.279-0.001,0.441-0.041,0.441-0.446v-2.726H24.16
                            c-0.377-0.006-0.422,0.109-0.488,0.461h-0.107c0.019-0.132,0.045-0.268,0.058-0.405c0.021-0.134,0.028-0.271,0.03-0.409h0.082
                            c0.047,0.141,0.177,0.147,0.295,0.146h0.025h2.609C26.794,36.529,26.938,36.52,26.96,36.373z M29.712,34.618
                            c-2.174,0-3.551-1.404-3.551-3.497c0-1.905,1.7-3.272,3.522-3.272c0.752,0,1.479,0.244,2.195,0.494l0.092,1.33h-0.23
                            c-0.178-1.09-0.947-1.525-1.988-1.525c-1.07,0-2.621,0.716-2.621,2.947c0,1.879,1.34,3.227,2.771,3.227
                            c0.918,0,1.681-0.633,1.867-1.607l0.213,0.055l-0.213,1.356C31.378,34.367,30.328,34.618,29.712,34.618z M20.455,36.678v-0.172
                            v-0.022h-0.022c-0.208,0-0.415,0.021-0.622,0.021c-0.236,0-0.469-0.021-0.708-0.021h-0.022v0.174v0.021h0.133
                            c0.147,0.002,0.417,0.054,0.418,0.455v1.86l-2.224-2.504c-0.168-0.007-0.316,0.013-0.464,0.013c-0.191,0-0.388-0.02-0.581-0.02
                            H16.34v0.174v0.021h0.087c0.218,0,0.438,0.164,0.438,0.433v2.13c-0.003,0.404-0.099,0.62-0.432,0.621h-0.07H16.34v0.174v0.021
                            h0.023c0.227,0,0.461-0.021,0.687-0.021c0.217,0,0.433,0.021,0.652,0.021h0.022v-0.174v-0.021h-0.106
                            c-0.382-0.006-0.45-0.129-0.455-0.569v-2.026l2.567,2.861l0.208,0.008h0.025l-0.003-0.023c-0.027-0.156-0.031-0.332-0.031-0.51
                            v-2.301c0.005-0.598,0.22-0.615,0.429-0.621h0.075h0.022V36.678z M19.908,37.299v2.303c0,0.177,0.004,0.353,0.029,0.51h-0.193
                            l-2.604-2.905v2.085c0,0.441,0.086,0.594,0.477,0.594h0.084v0.15c-0.216,0-0.434-0.021-0.651-0.021
                            c-0.229,0-0.461,0.021-0.688,0.021v-0.15h0.07c0.35,0,0.455-0.238,0.455-0.643v-2.132c0-0.283-0.232-0.455-0.46-0.455h-0.064
                            v-0.151c0.19,0,0.39,0.02,0.581,0.02c0.15,0,0.299-0.02,0.45-0.02l2.251,2.534h0.001l0.004-0.007l0.003-0.003v-1.896
                            c0-0.416-0.287-0.477-0.439-0.477h-0.11v-0.15c0.238,0,0.47,0.02,0.708,0.02c0.208,0,0.414-0.02,0.623-0.02v0.15h-0.076
                            C20.146,36.657,19.908,36.697,19.908,37.299z M26.921,36.322l-0.002,0.022c-0.008,0.133-0.108,0.135-0.25,0.138h-2.61h-0.023
                            c-0.131-0.003-0.223-0.005-0.258-0.132l-0.006-0.016h-0.138h-0.021v0.021c0,0.146-0.012,0.287-0.03,0.427
                            c-0.017,0.146-0.041,0.288-0.062,0.431l-0.002,0.025h0.176h0.019l0.004-0.018c0.084-0.375,0.074-0.438,0.449-0.443h0.808
                            l0.002,2.68c-0.009,0.393-0.119,0.396-0.398,0.402h-0.096h-0.023v0.174v0.021h0.023c0.197,0,0.553-0.021,0.854-0.021
                            c0.271,0,0.625,0.021,0.824,0.021h0.019v-0.174V39.86h-0.119c-0.24-0.006-0.394-0.031-0.396-0.413v-2.669h0.809
                            c0.318,0,0.34,0.275,0.353,0.465l0.002,0.029l0.178-0.065l0.015-0.006v-0.015c0-0.135,0-0.272,0.01-0.406
                            c0.016-0.137,0.035-0.271,0.055-0.408l0.006-0.021l-0.145-0.021L26.921,36.322z M27.007,36.778c-0.01,0.138-0.01,0.273-0.01,0.408
                            l-0.151,0.058c-0.011-0.187-0.035-0.485-0.373-0.485h-0.832v2.69c0,0.392,0.178,0.437,0.42,0.437h0.094v0.151
                            c-0.193,0-0.549-0.021-0.822-0.021c-0.303,0-0.658,0.021-0.854,0.021v-0.151h0.096c0.279,0,0.421-0.024,0.421-0.425v-2.702h-0.83
                            c-0.379,0-0.396,0.092-0.472,0.461h-0.151c0.021-0.144,0.047-0.283,0.062-0.433c0.021-0.142,0.03-0.282,0.03-0.43h0.123
                            c0.039,0.151,0.166,0.146,0.302,0.146h2.61c0.138,0,0.263-0.005,0.272-0.156l0.121,0.021
                            C27.042,36.506,27.025,36.639,27.007,36.778z M23.559,39.691c-0.066-0.154-0.127-0.343-0.188-0.521l-0.931-2.646
                            c-0.017-0.039-0.03-0.087-0.045-0.122c-0.002-0.006-0.002-0.006-0.008-0.006H22.38c-0.011,0-0.019,0.005-0.028,0.008
                            c-0.098,0.062-0.302,0.163-0.451,0.203c-0.032,0.191-0.123,0.443-0.189,0.631l-0.809,2.322c-0.077,0.22-0.254,0.346-0.46,0.346
                            h-0.008v0.105c0.173-0.011,0.348-0.021,0.525-0.021c0.196,0,0.395,0.011,0.588,0.021v-0.105h-0.033
                            c-0.164-0.003-0.395-0.021-0.401-0.2c0.001-0.101,0.063-0.231,0.113-0.401l0.166-0.543h1.177l0.205,0.604
                            c0.057,0.163,0.104,0.297,0.104,0.371c-0.006,0.154-0.221,0.167-0.336,0.171H22.51v0.104c0.25-0.01,0.496-0.021,0.736-0.021
                            c0.234,0,0.455,0.011,0.676,0.021v-0.104h-0.008C23.74,39.908,23.621,39.84,23.559,39.691z M21.474,38.525l0.484-1.476h0.016
                            h0.011H22l0.482,1.476H21.474z M14.141,36.686c-0.151,0-0.217,0.017-0.288,0.021v1.508h0.248c0.506,0,0.778-0.191,0.778-0.789
                            C14.878,36.973,14.591,36.686,14.141,36.686z M14.101,38.192h-0.226v-1.467c0.065-0.006,0.129-0.02,0.266-0.02
                            c0.44,0.004,0.715,0.277,0.718,0.719C14.855,38.012,14.601,38.186,14.101,38.192z M13.707,34.475
                            c-0.327,0-0.651-0.018-0.986-0.018c-0.335,0-0.671,0.01-1.003,0.018v-0.238h0.167c0.26,0,0.445,0,0.445-0.311V31.52
                            c0-0.53-0.324-0.793-0.855-0.793c-0.296,0-0.77,0.244-1.079,0.447v2.752c0,0.31,0.207,0.31,0.467,0.31h0.167v0.239
                            c-0.325,0-0.651-0.019-0.986-0.019c-0.334,0-0.669,0.009-1.003,0.019v-0.239h0.167c0.26,0,0.445,0,0.445-0.31v-2.463h0.001
                            c0-0.345-0.104-0.438-0.54-0.613v-0.176c0.4-0.131,0.78-0.252,1.228-0.447c0.026,0,0.054,0.019,0.054,0.094v0.604
                            c0.531-0.381,0.987-0.697,1.61-0.697c0.79,0,1.069,0.576,1.069,1.301v2.398c0,0.31,0.203,0.31,0.464,0.31h0.168V34.475z
                             M15.702,39.594l-0.881-1.318l0.025-0.01c0.395-0.154,0.689-0.426,0.689-0.871c0-0.701-0.546-0.865-1.182-0.868
                            c-0.267,0-0.51,0.021-0.814,0.021c-0.297,0-0.598-0.019-0.763-0.021v0.108h0.069c0.158,0,0.381,0.025,0.381,0.405v2.57
                            c0,0.203-0.189,0.297-0.381,0.297h-0.069v0.104c0.243,0,0.474-0.021,0.711-0.021c0.284,0,0.569,0.021,0.848,0.021v-0.104h-0.068
                            c-0.23,0-0.438-0.028-0.438-0.431v-1.104h0.023h0.346l0.005,0.012c0.331,0.582,0.663,1.129,1.036,1.629
                            c0.154,0,0.315-0.021,0.473-0.021c0.162,0,0.317,0.017,0.475,0.021v-0.109C15.951,39.873,15.84,39.803,15.702,39.594z
                             M14.101,38.236h-0.248H13.83v-1.55l0.021-0.002c0.068-0.006,0.137-0.02,0.29-0.02c0.461,0,0.761,0.301,0.761,0.76
                            C14.9,38.031,14.61,38.236,14.101,38.236z M15.738,39.568l-0.85-1.271c0.39-0.16,0.692-0.445,0.692-0.902
                            c-0.001-0.734-0.588-0.909-1.227-0.911c-0.27,0-0.512,0.021-0.814,0.021c-0.308,0-0.621-0.021-0.784-0.021h-0.022v0.174v0.022
                            h0.112c0.158,0.004,0.333,0.002,0.337,0.36v2.57c-0.001,0.171-0.153,0.25-0.337,0.25h-0.09h-0.022v0.174v0.021h0.022
                            c0.253,0,0.49-0.021,0.732-0.021c0.287,0,0.582,0.021,0.87,0.021h0.023v-0.174v-0.021h-0.113
                            c-0.234-0.007-0.387-0.002-0.393-0.384v-1.061h0.299c0.327,0.578,0.659,1.121,1.047,1.631c0.176,0.008,0.338-0.013,0.492-0.013
                            c0.166,0,0.328,0.021,0.497,0.021h0.021v-0.174v-0.02l-0.018-0.003C15.967,39.83,15.879,39.779,15.738,39.568z M16.21,40.036
                            c-0.166,0-0.329-0.021-0.495-0.021c-0.157,0-0.32,0.021-0.477,0.021c-0.389-0.513-0.724-1.059-1.052-1.643h-0.333v1.084
                            c0,0.392,0.181,0.406,0.415,0.406h0.091v0.151c-0.29,0-0.582-0.021-0.87-0.021c-0.243,0-0.481,0.021-0.733,0.021v-0.151h0.09
                            c0.189,0,0.36-0.086,0.36-0.272v-2.57c0-0.366-0.203-0.384-0.36-0.384h-0.09v-0.15c0.162,0,0.475,0.02,0.784,0.02
                            c0.303,0,0.546-0.02,0.814-0.02c0.637,0,1.205,0.172,1.205,0.891c0,0.453-0.303,0.732-0.704,0.89l0.864,1.294
                            c0.142,0.215,0.243,0.275,0.491,0.305V40.036z M12.366,39.172l-0.005,0.02c-0.152,0.568-0.397,0.598-1.005,0.602
                            c-0.33-0.002-0.596,0-0.596-0.308v-1.159h0.585c0.336,0.004,0.338,0.173,0.373,0.48l0.002,0.024l0.175-0.034l0.019-0.006
                            l-0.001-0.018c-0.009-0.193-0.016-0.385-0.016-0.577c0-0.197,0.007-0.396,0.016-0.591l0.001-0.021h-0.175H11.72l-0.002,0.02
                            c-0.034,0.271-0.086,0.423-0.378,0.426h-0.58v-1.281h0.681c0.522,0.006,0.565,0.242,0.595,0.571l0.002,0.024l0.178-0.045
                            l0.016-0.006v-0.02c-0.011-0.145-0.016-0.334-0.016-0.51c0-0.097,0.002-0.187,0.005-0.263l0.001-0.021h-0.023
                            c-0.344,0-0.868,0.019-1.294,0.019c-0.423,0-0.95-0.019-1.258-0.019H9.625v0.173v0.021h0.093c0.184,0.004,0.35,0.021,0.351,0.27
                            v2.648c-0.001,0.248-0.167,0.265-0.351,0.268H9.647H9.625v0.021v0.151v0.021h0.022c0.288,0,0.89-0.02,1.344-0.02
                            c0.455,0,1.037,0.02,1.361,0.02h0.018l0.004-0.018c0.046-0.268,0.106-0.535,0.171-0.803l0.005-0.021l-0.162-0.041h-0.022V39.172z
                             M12.352,40.036c-0.325,0-0.906-0.021-1.36-0.021c-0.457,0-1.058,0.021-1.346,0.021v-0.15h0.071c0.183,0,0.374-0.024,0.374-0.289
                            v-2.65c0-0.266-0.191-0.289-0.374-0.289H9.646v-0.152c0.308,0,0.835,0.021,1.26,0.021c0.425,0,0.949-0.021,1.293-0.021
                            c-0.008,0.219-0.004,0.553,0.013,0.774l-0.154,0.04c-0.025-0.328-0.085-0.592-0.617-0.592h-0.702v1.326h0.603
                            c0.301,0,0.368-0.172,0.397-0.447h0.152c-0.01,0.199-0.015,0.396-0.015,0.594c0,0.191,0.005,0.384,0.015,0.577l-0.152,0.03
                            c-0.03-0.304-0.044-0.5-0.394-0.5h-0.606v1.176c0,0.33,0.292,0.33,0.617,0.33c0.607,0,0.874-0.041,1.026-0.616l0.142,0.034
                            C12.458,39.5,12.397,39.766,12.352,40.036z M19.811,36.547c0.201,0,0.4-0.02,0.6-0.021v0.105h-0.053
                            c-0.218,0.002-0.472,0.061-0.472,0.666V39.6c0,0.168,0.003,0.334,0.026,0.488h-0.167l-2.627-2.938v2.14
                            c0.002,0.44,0.1,0.614,0.5,0.616h0.062v0.105c-0.209,0-0.418-0.021-0.631-0.021c-0.223,0-0.448,0.021-0.667,0.021v-0.105
                            l0.05,0.001c0.362-0.002,0.477-0.263,0.477-0.667v-2.13c0-0.297-0.243-0.478-0.482-0.479h-0.044v-0.105
                            c0.185,0.002,0.374,0.021,0.561,0.021c0.153,0,0.299-0.021,0.45-0.021l2.251,2.545l0.016-0.018l0.009-0.01l0.007-1.912
                            c0-0.43-0.306-0.498-0.463-0.5h-0.089v-0.105C19.355,36.528,19.579,36.547,19.811,36.547z M19.632,39.029l-0.004-0.006
                            L19.632,39.029L19.632,39.029z M27.998,36.501c-0.246,0-0.551-0.02-0.748-0.02h-0.021v0.174v0.021h0.094
                            c0.183,0.002,0.351,0.021,0.353,0.269v2.649c-0.002,0.248-0.17,0.265-0.353,0.267H27.25h-0.021v0.021v0.152v0.021h0.021
                            c0.197,0,0.498-0.021,0.746-0.021c0.252,0,0.553,0.021,0.797,0.021h0.021v-0.174V39.86h-0.092
                            c-0.185-0.002-0.351-0.018-0.354-0.266v-2.65c0.002-0.247,0.168-0.266,0.354-0.268h0.069h0.021v-0.175v-0.021h-0.021
                            C28.555,36.483,28.25,36.501,27.998,36.501z M28.792,36.657h-0.069c-0.183,0-0.375,0.023-0.375,0.289v2.648
                            c0,0.266,0.192,0.289,0.375,0.289h0.069v0.152c-0.242,0-0.545-0.02-0.797-0.02c-0.248,0-0.548,0.02-0.746,0.02v-0.152h0.071
                            c0.183,0,0.373-0.022,0.373-0.289v-2.648c0-0.266-0.19-0.289-0.373-0.289h-0.07v-0.15c0.197,0,0.5,0.02,0.748,0.02
                            c0.256,0,0.561-0.02,0.795-0.02L28.792,36.657L28.792,36.657z M43.203,39.555c0.08-0.111,0.125-0.248,0.156-0.357h0.104
                            c-0.053,0.273-0.121,0.542-0.18,0.816c-0.438,0-0.875-0.021-1.316-0.021c-0.439,0-0.875,0.021-1.314,0.021v-0.107h0.05
                            c0.182,0,0.401-0.034,0.401-0.359v-2.603c0-0.278-0.223-0.312-0.401-0.312h-0.05v-0.106c0.261,0.002,0.519,0.021,0.779,0.021
                            c0.25,0,0.496-0.02,0.746-0.021v0.106h-0.105c-0.188,0-0.366,0.017-0.366,0.297v2.609c0,0.217,0.16,0.287,0.323,0.305
                            c0.093,0.006,0.187,0.013,0.285,0.013c0.123,0,0.254-0.007,0.39-0.021C42.921,39.807,43.109,39.682,43.203,39.555z M40.07,39.691
                            c-0.064-0.154-0.127-0.343-0.187-0.521l-0.932-2.646c-0.017-0.039-0.029-0.087-0.043-0.12c-0.005-0.008-0.003-0.008-0.009-0.008
                            h-0.008c-0.012,0-0.018,0.005-0.029,0.008c-0.098,0.062-0.301,0.164-0.45,0.203c-0.031,0.191-0.123,0.443-0.188,0.631
                            l-0.812,2.322c-0.076,0.22-0.252,0.346-0.459,0.346h-0.009v0.105c0.175-0.011,0.349-0.021,0.525-0.021
                            c0.195,0,0.396,0.011,0.59,0.021v-0.105h-0.035c-0.164-0.003-0.395-0.021-0.399-0.201c0-0.1,0.063-0.229,0.11-0.4l0.166-0.543
                            h1.181l0.205,0.604c0.055,0.163,0.103,0.297,0.103,0.371c-0.009,0.154-0.222,0.167-0.336,0.171H39.02v0.104h0.002
                            c0.25-0.01,0.496-0.021,0.735-0.021c0.234,0,0.455,0.011,0.677,0.021v-0.104h-0.009C40.253,39.908,40.132,39.84,40.07,39.691z
                             M37.986,38.525l0.484-1.476h0.016h0.012h0.018l0.479,1.476H37.986z M38.488,37.071l-0.002,0.002l-0.469,1.432h0.946l-0.467-1.434
                            H38.488z M38.046,38.479l0.445-1.354l0.438,1.354H38.046z M43.326,39.154l-0.006,0.019c-0.027,0.108-0.072,0.25-0.15,0.356
                            c-0.088,0.121-0.266,0.239-0.473,0.263c-0.129,0.016-0.258,0.021-0.383,0.021c-0.099,0-0.189-0.004-0.279-0.01
                            c-0.158-0.023-0.281-0.072-0.283-0.263v-2.608c0.004-0.256,0.127-0.246,0.322-0.252h0.127h0.021v-0.174v-0.021h-0.021
                            c-0.26,0-0.514,0.02-0.77,0.02c-0.269,0-0.533-0.02-0.802-0.02h-0.021v0.173v0.021h0.092c0.186,0.002,0.357,0.021,0.359,0.267
                            v2.603c-0.002,0.299-0.177,0.312-0.359,0.314h-0.07h-0.021v0.174v0.021h0.021c0.445,0,0.893-0.021,1.336-0.021
                            c0.445,0,0.891,0.021,1.334,0.021h0.018l0.004-0.018c0.062-0.288,0.132-0.568,0.189-0.859l0.004-0.025h-0.174L43.326,39.154
                            L43.326,39.154z M43.3,40.036c-0.442,0-0.889-0.021-1.332-0.021c-0.446,0-0.891,0.021-1.336,0.021v-0.15H40.7
                            c0.184,0,0.383-0.024,0.383-0.341v-2.6c0-0.267-0.199-0.289-0.383-0.289h-0.068v-0.151c0.269,0,0.531,0.02,0.8,0.02
                            c0.258,0,0.51-0.02,0.77-0.02v0.151h-0.129c-0.189,0-0.342,0.005-0.342,0.272v2.609c0,0.203,0.141,0.264,0.303,0.284
                            c0.207,0.015,0.436,0.015,0.668-0.011c0.213-0.025,0.395-0.146,0.484-0.272c0.08-0.11,0.125-0.254,0.155-0.364h0.146
                            C43.434,39.465,43.363,39.747,43.3,40.036z M43.43,36.362c-0.306,0-0.533,0.232-0.533,0.533c0,0.299,0.229,0.529,0.533,0.529
                            c0.303,0,0.53-0.23,0.53-0.529S43.73,36.362,43.43,36.362z M43.43,37.327c-0.24,0-0.425-0.204-0.425-0.434
                            c0-0.229,0.185-0.434,0.425-0.434c0.239,0,0.42,0.204,0.42,0.434S43.666,37.327,43.43,37.327z M43.492,36.911
                            c0.072-0.021,0.125-0.081,0.125-0.161c0-0.097-0.084-0.141-0.18-0.141h-0.271v0.026c0.068-0.004,0.076,0.017,0.076,0.07V37.1
                            c0,0.041-0.014,0.039-0.078,0.05v0.024h0.258V37.15c-0.066-0.011-0.08-0.009-0.08-0.042v-0.18h0.061
                            c0.099,0.143,0.144,0.246,0.191,0.246h0.104v-0.017c-0.029-0.019-0.076-0.069-0.115-0.126L43.492,36.911z M43.406,36.895h-0.064
                            v-0.252h0.061c0.058,0,0.105,0.031,0.105,0.117C43.507,36.846,43.475,36.895,43.406,36.895z M40.111,39.674
                            c-0.064-0.15-0.125-0.336-0.188-0.516l-0.93-2.646c-0.018-0.043-0.031-0.088-0.047-0.129c-0.016-0.025-0.041-0.029-0.055-0.029
                            c-0.021,0-0.037,0.008-0.047,0.013c-0.101,0.062-0.304,0.163-0.462,0.219c-0.045,0.2-0.137,0.451-0.198,0.64l-0.812,2.322
                            c-0.074,0.204-0.231,0.312-0.42,0.312h-0.029h-0.021v0.174v0.021h0.021c0.185-0.01,0.365-0.021,0.546-0.021
                            c0.2,0,0.409,0.011,0.61,0.021h0.021v-0.174V39.86h-0.076c-0.168,0-0.354-0.03-0.354-0.155c-0.002-0.081,0.058-0.214,0.108-0.388
                            l0.156-0.512h1.117l0.191,0.572c0.057,0.161,0.104,0.299,0.102,0.355c0,0.098-0.176,0.125-0.293,0.125H39h-0.023v0.174v0.023
                            L39,40.053c0.256-0.009,0.51-0.021,0.758-0.021c0.242,0,0.469,0.012,0.695,0.021l0.023,0.002v-0.176v-0.021h-0.052
                            C40.264,39.861,40.169,39.805,40.111,39.674z M40.457,40.036c-0.229-0.01-0.457-0.021-0.699-0.021c-0.248,0-0.5,0.011-0.758,0.021
                            v-0.15h0.055c0.115,0,0.314-0.021,0.314-0.147c0-0.065-0.047-0.2-0.104-0.363l-0.197-0.587h-1.146l-0.164,0.527
                            c-0.049,0.17-0.109,0.304-0.109,0.394c0,0.153,0.213,0.177,0.379,0.177h0.058v0.152c-0.203-0.01-0.41-0.02-0.612-0.02
                            c-0.183,0-0.363,0.009-0.546,0.02v-0.152h0.029c0.198,0,0.364-0.117,0.44-0.33l0.81-2.319c0.065-0.188,0.157-0.438,0.187-0.629
                            c0.162-0.056,0.364-0.155,0.461-0.218c0.018-0.004,0.024-0.01,0.041-0.01c0.016,0,0.024,0,0.035,0.016
                            c0.016,0.041,0.032,0.087,0.047,0.125l0.93,2.646c0.061,0.178,0.121,0.363,0.189,0.518c0.061,0.143,0.165,0.201,0.331,0.201h0.031
                            L40.457,40.036L40.457,40.036z M30.811,36.407c-1.062,0.001-1.888,0.825-1.89,1.891l0,0c0.002,1.1,0.832,1.834,1.914,1.838
                            c1.086-0.004,1.935-0.785,1.935-1.961C32.768,37.083,31.896,36.408,30.811,36.407z M30.835,40.113
                            c-1.073,0-1.892-0.729-1.892-1.815c0-1.054,0.812-1.867,1.865-1.867c1.078,0,1.938,0.668,1.938,1.744
                            C32.748,39.34,31.914,40.113,30.835,40.113z M30.811,36.45c-1.041,0-1.847,0.807-1.847,1.848c0.002,1.074,0.812,1.793,1.873,1.793
                            c1.065,0,1.888-0.766,1.892-1.916C32.727,37.111,31.878,36.452,30.811,36.45z M30.886,39.911c-0.84-0.001-1.254-0.882-1.256-1.793
                            c0.002-0.682,0.269-1.486,1.168-1.486c0.861,0,1.261,0.911,1.263,1.654C32.061,39.023,31.887,39.911,30.886,39.911z
                             M30.798,36.651c-0.885,0-1.146,0.791-1.146,1.467c0,0.905,0.414,1.771,1.233,1.771c0.982,0,1.152-0.866,1.152-1.604
                            C32.039,37.547,31.641,36.651,30.798,36.651z M30.886,39.867c-0.801-0.001-1.211-0.849-1.213-1.75
                            c0.004-0.674,0.259-1.442,1.125-1.444c0.824,0.002,1.22,0.879,1.22,1.611C32.016,39.023,31.85,39.867,30.886,39.867z
                             M36.322,36.547c0.201,0,0.402-0.02,0.602-0.021v0.105h-0.055c-0.217,0.002-0.471,0.058-0.471,0.666V39.6
                            c0,0.168,0.004,0.334,0.024,0.488h-0.166L33.63,37.15v2.142c0,0.44,0.101,0.614,0.498,0.616h0.063v0.104
                            c-0.209,0-0.418-0.02-0.629-0.02c-0.225,0-0.449,0.02-0.668,0.02v-0.105l0.051,0.001c0.363-0.002,0.478-0.263,0.478-0.665v-2.132
                            c0-0.297-0.246-0.478-0.481-0.479h-0.045v-0.105c0.186,0.002,0.375,0.021,0.561,0.021c0.152,0,0.301-0.021,0.451-0.021l2.25,2.545
                            l0.018-0.018l0.012-0.01l0.004-1.912c0-0.429-0.307-0.498-0.461-0.5h-0.09v-0.105C35.867,36.528,36.091,36.547,36.322,36.547z
                             M36.968,36.678v-0.172v-0.021h-0.021c-0.211,0-0.418,0.02-0.625,0.02c-0.234,0-0.467-0.02-0.707-0.02h-0.023v0.173v0.021h0.135
                            c0.148,0.004,0.418,0.054,0.42,0.455v1.86l-2.225-2.502c-0.172-0.008-0.316,0.011-0.469,0.011c-0.189,0-0.387-0.019-0.58-0.019
                            H32.85v0.173v0.021h0.088c0.217,0,0.438,0.164,0.438,0.433v2.132c-0.004,0.402-0.101,0.617-0.435,0.619h-0.069H32.85v0.174v0.021
                            h0.022c0.229,0,0.461-0.021,0.688-0.021c0.215,0,0.433,0.021,0.65,0.021h0.021v-0.174v-0.021h-0.107
                            c-0.381-0.006-0.447-0.129-0.453-0.569v-2.026l2.564,2.861l0.209,0.006h0.025l-0.005-0.022c-0.024-0.155-0.028-0.332-0.028-0.509
                            v-2.303c0.004-0.596,0.221-0.615,0.428-0.621h0.076H36.968L36.968,36.678z M36.419,37.299v2.303c0,0.177,0.004,0.353,0.031,0.51
                            h-0.193l-2.604-2.904v2.085c0,0.441,0.086,0.593,0.477,0.593h0.086v0.151c-0.217,0-0.436-0.021-0.65-0.021
                            c-0.229,0-0.461,0.021-0.688,0.021v-0.151h0.069c0.351,0,0.455-0.237,0.455-0.642v-2.132c0-0.283-0.229-0.455-0.459-0.455h-0.065
                            v-0.152c0.19,0,0.39,0.021,0.58,0.021c0.153,0,0.299-0.021,0.45-0.021l2.252,2.535l0.007-0.007l0.004-0.003v-1.896
                            c0-0.416-0.287-0.476-0.44-0.476h-0.107v-0.151c0.236,0,0.469,0.021,0.707,0.021c0.207,0,0.414-0.021,0.622-0.021v0.151h-0.077
                            C36.658,36.657,36.419,36.697,36.419,37.299z M36.145,39.029l-0.004-0.006L36.145,39.029L36.145,39.029z M9.343,36.322
                            L9.34,36.344c-0.008,0.133-0.111,0.135-0.252,0.138H6.48H6.455c-0.129-0.003-0.223-0.005-0.258-0.132l-0.002-0.016h-0.14H6.034
                            v0.021c0,0.146-0.009,0.287-0.031,0.427c-0.015,0.146-0.04,0.289-0.062,0.429L5.94,37.239h0.176h0.018l0.003-0.018
                            c0.083-0.375,0.076-0.438,0.449-0.443h0.808v2.68c-0.006,0.393-0.119,0.396-0.396,0.402H6.901H6.88v0.174v0.021h0.021
                            c0.198,0,0.553-0.021,0.855-0.021c0.271,0,0.625,0.021,0.825,0.021h0.021v-0.174V39.86H8.483
                            c-0.241-0.006-0.393-0.031-0.397-0.413v-2.669h0.807c0.32,0,0.34,0.275,0.354,0.465l0.001,0.029l0.179-0.065l0.014-0.006v-0.015
                            c0-0.137,0-0.272,0.009-0.406c0.015-0.137,0.035-0.271,0.056-0.408l0.003-0.021l-0.143-0.021L9.343,36.322z M9.428,36.778
                            c-0.009,0.138-0.009,0.273-0.009,0.408l-0.153,0.058c-0.011-0.187-0.035-0.485-0.374-0.485H8.063v2.69
                            c0,0.392,0.177,0.437,0.418,0.437h0.098v0.151c-0.197,0-0.552-0.021-0.824-0.021c-0.304,0-0.658,0.021-0.855,0.021v-0.151h0.095
                            c0.279,0,0.42-0.024,0.42-0.425v-2.702h-0.83c-0.379,0-0.394,0.092-0.47,0.461H5.963c0.021-0.144,0.046-0.283,0.061-0.433
                            c0.02-0.142,0.03-0.282,0.03-0.43h0.123c0.041,0.151,0.167,0.146,0.303,0.146h2.61c0.137,0,0.263-0.005,0.274-0.156l0.12,0.021
                            C9.463,36.506,9.444,36.639,9.428,36.778z M5.027,39.029l-0.004-0.006l0.004,0.004V39.029z M18.773,31.744v2.185
                            c0,0.31,0.186,0.31,0.446,0.31h0.52v0.239c-0.511-0.008-0.9-0.019-1.3-0.019c-0.382,0-0.771,0.011-1.106,0.019v-0.24h0.251
                            c0.259,0,0.445,0,0.445-0.311v-2.609c0-0.288-0.343-0.346-0.483-0.419v-0.14c0.679-0.289,1.051-0.529,1.135-0.529
                            c0.055,0,0.083,0.027,0.083,0.119v0.838h0.02c0.232-0.361,0.623-0.957,1.189-0.957c0.232,0,0.531,0.156,0.531,0.492
                            c0,0.25-0.177,0.476-0.437,0.476c-0.289,0-0.289-0.224-0.614-0.224C19.293,30.971,18.773,31.184,18.773,31.744z M22.834,33.659
                            c0-1.1-2.024-0.744-2.024-2.239c0-0.521,0.419-1.19,1.44-1.19c0.297,0,0.697,0.084,1.061,0.271l0.064,0.947H23.16
                            c-0.094-0.585-0.418-0.92-1.016-0.92c-0.371,0-0.724,0.215-0.724,0.612c0,1.09,2.157,0.754,2.157,2.213
                            c0,0.615-0.494,1.267-1.6,1.267c-0.373,0-0.809-0.131-1.134-0.315l-0.103-1.069l0.167-0.048c0.122,0.613,0.492,1.136,1.172,1.136
                            C22.628,34.32,22.834,33.985,22.834,33.659z M0.771,36.501c-0.247,0-0.55-0.02-0.75-0.02H0v0.174v0.021h0.092
                            c0.183,0.002,0.351,0.021,0.352,0.269v2.649c-0.001,0.248-0.169,0.264-0.352,0.267H0.021H0v0.021v0.152v0.021h0.021
                            c0.2,0,0.498-0.021,0.744-0.021c0.253,0,0.555,0.021,0.799,0.021h0.023v-0.174V39.86H1.494c-0.184-0.003-0.353-0.018-0.353-0.266
                            v-2.65c0-0.248,0.169-0.266,0.353-0.268h0.07h0.023v-0.172v-0.023H1.564C1.325,36.482,1.022,36.501,0.771,36.501z M1.563,36.657
                            h-0.07c-0.182,0-0.375,0.023-0.375,0.289v2.648c0,0.266,0.193,0.289,0.375,0.289h0.07v0.152c-0.243,0-0.546-0.02-0.799-0.02
                            c-0.248,0-0.546,0.02-0.744,0.02v-0.152h0.071c0.182,0,0.374-0.022,0.374-0.289v-2.648c0-0.266-0.192-0.289-0.374-0.289H0.02
                            v-0.15c0.198,0,0.501,0.02,0.75,0.02c0.252,0,0.555-0.02,0.793-0.02V36.657z M5.205,36.547c0.201,0,0.401-0.02,0.6-0.021v0.107
                            H5.753c-0.219,0-0.473,0.059-0.473,0.664V39.6c0,0.168,0.004,0.334,0.026,0.488H5.139L2.513,37.15v2.142
                            c0,0.44,0.099,0.613,0.498,0.616h0.063v0.106c-0.209-0.002-0.419-0.021-0.63-0.021c-0.222,0-0.448,0.021-0.666,0.021v-0.108
                            l0.048,0.002c0.364-0.002,0.476-0.263,0.476-0.665v-2.132c0-0.297-0.243-0.478-0.481-0.478H1.777l0.001-0.107
                            c0.184,0.002,0.374,0.021,0.558,0.021c0.154,0,0.301-0.021,0.451-0.021l2.25,2.544l0.017-0.017l0.01-0.01l0.008-1.912
                            c0-0.43-0.308-0.5-0.462-0.5H4.52v-0.106C4.748,36.528,4.973,36.547,5.205,36.547z M0.771,36.547c0.245,0,0.538-0.021,0.771-0.021
                            v0.107H1.493c-0.181,0-0.397,0.031-0.397,0.312v2.648c0,0.281,0.216,0.312,0.397,0.312h0.049v0.107
                            c-0.239-0.002-0.531-0.021-0.778-0.021c-0.239,0-0.525,0.021-0.721,0.021v-0.107h0.049c0.18,0,0.396-0.03,0.396-0.312v-2.648
                            c0-0.28-0.216-0.312-0.396-0.312H0.043v-0.107C0.239,36.528,0.53,36.547,0.771,36.547z M17.024,31.912l0.084-0.058
                            c0.008-0.055,0.008-0.11,0.008-0.168c-0.008-0.984-0.818-1.459-1.571-1.459c-0.65,0-1.878,0.539-1.878,2.361
                            c0,0.594,0.298,2.027,1.776,2.027c0.763,0,1.291-0.484,1.719-1.051l-0.13-0.129c-0.342,0.342-0.743,0.621-1.255,0.621
                            c-0.742,0-1.311-0.724-1.365-1.607c-0.021-0.314-0.021-0.455,0-0.539L17.024,31.912L17.024,31.912z M15.508,30.527
                            c0.521,0,0.81,0.381,0.81,0.854c0,0.11-0.027,0.232-0.26,0.232H14.44C14.552,30.944,14.942,30.527,15.508,30.527z M2.625,34.475
                            c3.291,0,3.829-2.312,3.829-3.233c0-1.646-1.31-3.254-3.718-3.254c-0.688,0-1.218,0.021-1.609,0.021c-0.363,0-0.733,0-1.096-0.021
                            v0.241c0.418,0.01,0.855-0.047,0.855,0.789H0.885v4.512c-0.026,0.632-0.297,0.648-0.854,0.707v0.238
                            c0.418-0.008,0.828-0.018,1.246-0.018C1.713,34.46,2.151,34.475,2.625,34.475z M1.796,28.323c0.168-0.012,0.344-0.037,0.726-0.037
                            c1.794,0,2.91,1.255,2.91,2.993c0,1.386-0.66,2.899-2.668,2.899c-0.455,0-0.967-0.084-0.967-0.799L1.796,28.323L1.796,28.323z
                             M7.32,28.333c0-0.232,0.222-0.447,0.454-0.447c0.242,0,0.457,0.195,0.457,0.447c0,0.25-0.205,0.467-0.457,0.467
                            C7.533,28.799,7.32,28.575,7.32,28.333z M21.503,20.531V8.778c2.362,0.906,4.04,3.194,4.043,5.876
                            C25.542,17.338,23.865,19.622,21.503,20.531z M32.932,33.928V28.75c0-0.605-0.14-0.623-0.494-0.726v-0.148
                            c0.373-0.118,0.762-0.286,0.957-0.399c0.102-0.055,0.176-0.104,0.205-0.104c0.059,0,0.075,0.057,0.075,0.133v6.422
                            c0,0.31,0.205,0.31,0.466,0.31h0.155v0.239c-0.313,0-0.64-0.019-0.976-0.019c-0.334,0-0.668,0.011-1.014,0.019v-0.24h0.178
                            C32.746,34.237,32.932,34.237,32.932,33.928z M7.793,34.46c-0.334,0-0.669,0.009-1.013,0.019v-0.24h0.177
                            c0.259,0,0.447,0,0.447-0.309V31.41c0-0.409-0.141-0.466-0.484-0.649v-0.148c0.437-0.133,0.957-0.307,0.993-0.335
                            c0.066-0.037,0.122-0.046,0.169-0.046c0.046,0,0.065,0.057,0.065,0.131v3.568c0,0.309,0.205,0.309,0.465,0.309H8.77v0.24
                            C8.453,34.475,8.128,34.46,7.793,34.46z M39.382,28.75v3.998c0,0.567-0.047,1.105-0.131,1.656l0.194,0.102l0.279-0.215
                            c0.24,0.104,0.604,0.327,1.291,0.327c1.332,0,2.223-1.219,2.223-2.437c0-1.014-0.649-1.953-1.729-1.953
                            c-0.51,0-1.012,0.354-1.383,0.669v-3.394c0-0.076-0.021-0.132-0.076-0.132c-0.028,0-0.103,0.046-0.204,0.103
                            c-0.195,0.113-0.585,0.281-0.959,0.4v0.149C39.242,28.129,39.382,28.145,39.382,28.75z M40.126,31.169
                            c0.25-0.216,0.539-0.438,0.883-0.438c0.726,0,1.486,0.838,1.486,1.841c0,0.827-0.416,1.805-1.412,1.805
                            c-0.623,0-0.957-0.596-0.957-0.948V31.169z M12.397,39.221l0.1,0.024c-0.062,0.257-0.12,0.513-0.163,0.767
                            c-0.326,0-0.897-0.02-1.344-0.02c-0.444,0-1.028,0.02-1.323,0.02v-0.104h0.049c0.18-0.002,0.396-0.033,0.396-0.312v-2.65
                            c0-0.28-0.216-0.312-0.396-0.312H9.667v-0.106c0.311,0.002,0.821,0.021,1.238,0.021c0.415,0,0.928-0.02,1.271-0.021
                            c-0.004,0.071-0.005,0.153-0.005,0.239c0,0.168,0.005,0.354,0.015,0.496l-0.109,0.027c-0.022-0.318-0.111-0.588-0.637-0.585
                            h-0.724v1.37h0.623c0.309,0.003,0.391-0.184,0.419-0.445h0.109c-0.01,0.191-0.015,0.381-0.015,0.57
                            c0,0.186,0.005,0.373,0.015,0.559l-0.109,0.024c-0.025-0.289-0.062-0.502-0.414-0.498h-0.628v1.2c0,0.352,0.319,0.352,0.638,0.352
                            C11.952,39.84,12.246,39.789,12.397,39.221z M35.6,33.036c0,0.278,0,1.079,0.809,1.079c0.314,0,0.734-0.242,1.125-0.563v-2.502
                            c0-0.188-0.447-0.289-0.781-0.383v-0.168c0.836-0.056,1.357-0.129,1.449-0.129c0.074,0,0.074,0.062,0.074,0.167v3.433
                            c0,0.168,0.104,0.176,0.26,0.176c0.113,0,0.252-0.008,0.375-0.008v0.193c-0.402,0.037-1.164,0.233-1.342,0.289l-0.047-0.027
                            v-0.754c-0.558,0.452-0.984,0.779-1.646,0.779c-0.502,0-1.022-0.327-1.022-1.105v-2.379c0-0.244-0.037-0.477-0.558-0.521v-0.178
                            c0.334-0.008,1.078-0.063,1.199-0.063c0.104,0,0.104,0.063,0.104,0.269V33.036z M9.38,36.373l0.079,0.013
                            c-0.019,0.131-0.038,0.26-0.053,0.391c-0.009,0.133-0.01,0.266-0.01,0.396l-0.108,0.041c-0.012-0.188-0.055-0.478-0.396-0.479
                            H8.04v2.714c0,0.396,0.199,0.458,0.443,0.459h0.074v0.104c-0.201,0-0.538-0.019-0.802-0.019c-0.292,0-0.634,0.019-0.834,0.019
                            v-0.104h0.076c0.277-0.001,0.441-0.042,0.441-0.449v-2.725H6.586c-0.376-0.006-0.421,0.109-0.487,0.461H5.99
                            c0.02-0.135,0.043-0.268,0.057-0.405c0.02-0.134,0.029-0.271,0.031-0.409h0.083c0.045,0.141,0.176,0.145,0.295,0.145H6.48h2.609
                            C9.216,36.528,9.358,36.52,9.38,36.373z M19.227,25.523h5.104c5.931,0.028,11.344-4.837,11.344-10.755
                            c0-6.473-5.414-10.946-11.344-10.943h-5.104C13.226,3.822,8.285,8.297,8.285,14.768C8.285,20.688,13.225,25.553,19.227,25.523z
                             M19.251,4.721c5.485,0.001,9.931,4.448,9.931,9.934c0,5.485-4.445,9.931-9.931,9.934c-5.484-0.003-9.929-4.449-9.93-9.934
                            C9.322,9.168,13.767,4.722,19.251,4.721z M5.205,36.501c-0.236,0-0.47-0.02-0.708-0.02H4.475v0.174v0.021h0.134
                            c0.146,0.004,0.416,0.053,0.418,0.455v1.86L2.804,36.49C2.633,36.482,2.487,36.5,2.336,36.5c-0.189,0-0.387-0.019-0.581-0.019
                            H1.733v0.173v0.021h0.088c0.217,0.002,0.438,0.164,0.438,0.435v2.131c-0.003,0.401-0.099,0.617-0.433,0.619H1.755H1.733v0.174
                            v0.021h0.022c0.229,0,0.462-0.02,0.688-0.02c0.216,0,0.434,0.02,0.652,0.02h0.023v-0.172v-0.021H3.009
                            c-0.381-0.005-0.447-0.13-0.454-0.569v-2.026l2.567,2.86l0.209,0.009h0.025l-0.004-0.027c-0.023-0.152-0.029-0.328-0.029-0.506
                            v-2.301c0.005-0.599,0.219-0.615,0.429-0.621h0.075h0.021v-0.174v-0.021H5.827C5.618,36.483,5.411,36.501,5.205,36.501z
                             M5.828,36.657H5.753c-0.213,0-0.451,0.041-0.451,0.645v2.301c0,0.178,0.004,0.354,0.03,0.511H5.139l-2.605-2.904v2.085
                            c0,0.441,0.086,0.592,0.476,0.592h0.085v0.152c-0.217,0-0.434-0.02-0.651-0.02c-0.227,0-0.46,0.02-0.688,0.02v-0.152h0.07
                            c0.349,0,0.457-0.237,0.457-0.641v-2.132c0-0.283-0.234-0.455-0.461-0.455H1.757v-0.151c0.192,0,0.388,0.021,0.582,0.021
                            c0.151,0,0.298-0.021,0.451-0.021l2.251,2.534l0.009-0.01v-1.896c0-0.416-0.288-0.478-0.439-0.478H4.499v-0.151
                            c0.238,0,0.47,0.021,0.708,0.021c0.208,0,0.415-0.021,0.623-0.021v0.15H5.828z M12.958,14.654
                            c0.005-2.681,1.678-4.967,4.039-5.876v11.75C14.635,19.62,12.962,17.335,12.958,14.654z"/>
                    </g>
                </g>
              </svg>
              <?= $this->Html->image(
                                WEBROOT_URL.'webroot/images/hipercard-cartao.png',
                                ['id' => 'hipercard','class' => 'logo-cartao hipercard']) ?>
              <?= $this->Html->image(
                                WEBROOT_URL.'webroot/images/elo-cartao.png',
                                ['id' => 'elo','class' => 'logo-cartao elo']) ?>
              <?= $this->Html->image(
                                WEBROOT_URL.'webroot/images/hiper-cartao.png',
                                ['id' => 'hiper','class' => 'logo-cartao hiper']) ?>
            </div>
            <div class="number"></div>
            <div class="card-holder">
              <label style="margin-bottom: 0; margin-top: -10px;">Dono do cartão</label>
              <div></div>
            </div>
            <div class="card-expiration-date">
              <label style="margin-bottom: 0; margin-top: -10px;">Validade</label>
              <div></div>
            </div>
          </div>
          <div class="back">
            <span class="card-color-change"></span>
            <div class="strip"></div>
            <div class="logo">
              <svg version="1.1" id="visa" class="logo-cartao visa" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                   width="47.834px" height="47.834px" viewBox="0 0 47.834 47.834" style="enable-background:new 0 0 47.834 47.834;">
                <g>
                  <g>
                    <path d="M44.688,16.814h-3.004c-0.933,0-1.627,0.254-2.037,1.184l-5.773,13.074h4.083c0,0,0.666-1.758,0.817-2.143
                             c0.447,0,4.414,0.006,4.979,0.006c0.116,0.498,0.474,2.137,0.474,2.137h3.607L44.688,16.814z M39.893,26.01
                             c0.32-0.819,1.549-3.987,1.549-3.987c-0.021,0.039,0.317-0.825,0.518-1.362l0.262,1.23c0,0,0.745,3.406,0.901,4.119H39.893z
                             M34.146,26.404c-0.028,2.963-2.684,4.875-6.771,4.875c-1.743-0.018-3.422-0.361-4.332-0.76l0.547-3.193l0.501,0.228
                             c1.277,0.532,2.104,0.747,3.661,0.747c1.117,0,2.313-0.438,2.325-1.393c0.007-0.625-0.501-1.07-2.016-1.77
                             c-1.476-0.683-3.43-1.827-3.405-3.876c0.021-2.773,2.729-4.708,6.571-4.708c1.506,0,2.713,0.31,3.483,0.599l-0.526,3.092
                             l-0.351-0.165c-0.716-0.288-1.638-0.566-2.91-0.546c-1.522,0-2.228,0.634-2.228,1.227c-0.008,0.668,0.824,1.108,2.184,1.77
                             C33.126,23.546,34.163,24.783,34.146,26.404z M0,16.962l0.05-0.286h6.028c0.813,0.031,1.468,0.29,1.694,1.159l1.311,6.304
                             C7.795,20.842,4.691,18.099,0,16.962z M17.581,16.812l-6.123,14.239l-4.114,0.007L3.862,19.161
                             c2.503,1.602,4.635,4.144,5.386,5.914l0.406,1.469l3.808-9.729L17.581,16.812L17.581,16.812z M19.153,16.8h3.89L20.61,31.066
                             h-3.888L19.153,16.8z"/>
                  </g>
                </g>
              </svg>
              <svg version="1.1" id="mastercard" class="logo-cartao mastercard" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 504 504" style="enable-background:new 0 0 504 504;" xml:space="preserve">
                <path style="fill:#FFB600;" d="M504,252c0,83.2-67.2,151.2-151.2,151.2c-83.2,0-151.2-68-151.2-151.2l0,0
                    c0-83.2,67.2-151.2,150.4-151.2C436.8,100.8,504,168.8,504,252L504,252z"/>
                <path style="fill:#F7981D;" d="M352.8,100.8c83.2,0,151.2,68,151.2,151.2l0,0c0,83.2-67.2,151.2-151.2,151.2
                    c-83.2,0-151.2-68-151.2-151.2"/>
                <path style="fill:#FF8500;" d="M352.8,100.8c83.2,0,151.2,68,151.2,151.2l0,0c0,83.2-67.2,151.2-151.2,151.2"/>
                <path style="fill:#FF5050;" d="M149.6,100.8C67.2,101.6,0,168.8,0,252s67.2,151.2,151.2,151.2c39.2,0,74.4-15.2,101.6-39.2l0,0l0,0
                    c5.6-4.8,10.4-10.4,15.2-16h-31.2c-4-4.8-8-10.4-11.2-15.2h53.6c3.2-4.8,6.4-10.4,8.8-16h-71.2c-2.4-4.8-4.8-10.4-6.4-16h83.2
                    c4.8-15.2,8-31.2,8-48c0-11.2-1.6-21.6-3.2-32h-92.8c0.8-5.6,2.4-10.4,4-16h83.2c-1.6-5.6-4-11.2-6.4-16H216
                    c2.4-5.6,5.6-10.4,8.8-16h53.6c-3.2-5.6-7.2-11.2-12-16h-29.6c4.8-5.6,9.6-10.4,15.2-15.2c-26.4-24.8-62.4-39.2-101.6-39.2
                    C150.4,100.8,150.4,100.8,149.6,100.8z"/>
                <path style="fill:#E52836;" d="M0,252c0,83.2,67.2,151.2,151.2,151.2c39.2,0,74.4-15.2,101.6-39.2l0,0l0,0
                    c5.6-4.8,10.4-10.4,15.2-16h-31.2c-4-4.8-8-10.4-11.2-15.2h53.6c3.2-4.8,6.4-10.4,8.8-16h-71.2c-2.4-4.8-4.8-10.4-6.4-16h83.2
                    c4.8-15.2,8-31.2,8-48c0-11.2-1.6-21.6-3.2-32h-92.8c0.8-5.6,2.4-10.4,4-16h83.2c-1.6-5.6-4-11.2-6.4-16H216
                    c2.4-5.6,5.6-10.4,8.8-16h53.6c-3.2-5.6-7.2-11.2-12-16h-29.6c4.8-5.6,9.6-10.4,15.2-15.2c-26.4-24.8-62.4-39.2-101.6-39.2h-0.8"/>
                <path style="fill:#CB2026;" d="M151.2,403.2c39.2,0,74.4-15.2,101.6-39.2l0,0l0,0c5.6-4.8,10.4-10.4,15.2-16h-31.2
                    c-4-4.8-8-10.4-11.2-15.2h53.6c3.2-4.8,6.4-10.4,8.8-16h-71.2c-2.4-4.8-4.8-10.4-6.4-16h83.2c4.8-15.2,8-31.2,8-48
                    c0-11.2-1.6-21.6-3.2-32h-92.8c0.8-5.6,2.4-10.4,4-16h83.2c-1.6-5.6-4-11.2-6.4-16H216c2.4-5.6,5.6-10.4,8.8-16h53.6
                    c-3.2-5.6-7.2-11.2-12-16h-29.6c4.8-5.6,9.6-10.4,15.2-15.2c-26.4-24.8-62.4-39.2-101.6-39.2h-0.8"/>
                <g>
                    <path style="fill:#FFFFFF;" d="M204.8,290.4l2.4-13.6c-0.8,0-2.4,0.8-4,0.8c-5.6,0-6.4-3.2-5.6-4.8l4.8-28h8.8l2.4-15.2h-8l1.6-9.6
                        h-16c0,0-9.6,52.8-9.6,59.2c0,9.6,5.6,13.6,12.8,13.6C199.2,292.8,203.2,291.2,204.8,290.4z"/>
                    <path style="fill:#FFFFFF;" d="M210.4,264.8c0,22.4,15.2,28,28,28c12,0,16.8-2.4,16.8-2.4l3.2-15.2c0,0-8.8,4-16.8,4
                        c-17.6,0-14.4-12.8-14.4-12.8H260c0,0,2.4-10.4,2.4-14.4c0-10.4-5.6-23.2-23.2-23.2C222.4,227.2,210.4,244.8,210.4,264.8z
                         M238.4,241.6c8.8,0,7.2,10.4,7.2,11.2H228C228,252,229.6,241.6,238.4,241.6z"/>
                    <path style="fill:#FFFFFF;" d="M340,290.4l3.2-17.6c0,0-8,4-13.6,4c-11.2,0-16-8.8-16-18.4c0-19.2,9.6-29.6,20.8-29.6
                        c8,0,14.4,4.8,14.4,4.8l2.4-16.8c0,0-9.6-4-18.4-4c-18.4,0-36.8,16-36.8,46.4c0,20,9.6,33.6,28.8,33.6
                        C331.2,292.8,340,290.4,340,290.4z"/>
                    <path style="fill:#FFFFFF;" d="M116.8,227.2c-11.2,0-19.2,3.2-19.2,3.2L95.2,244c0,0,7.2-3.2,17.6-3.2c5.6,0,10.4,0.8,10.4,5.6
                        c0,3.2-0.8,4-0.8,4s-4.8,0-7.2,0c-13.6,0-28.8,5.6-28.8,24c0,14.4,9.6,17.6,15.2,17.6c11.2,0,16-7.2,16.8-7.2l-0.8,6.4H132l6.4-44
                        C138.4,228,122.4,227.2,116.8,227.2z M120,263.2c0,2.4-1.6,15.2-11.2,15.2c-4.8,0-6.4-4-6.4-6.4c0-4,2.4-9.6,14.4-9.6
                        C119.2,263.2,120,263.2,120,263.2z"/>
                    <path style="fill:#FFFFFF;" d="M153.6,292c4,0,24,0.8,24-20.8c0-20-19.2-16-19.2-24c0-4,3.2-5.6,8.8-5.6c2.4,0,11.2,0.8,11.2,0.8
                        l2.4-14.4c0,0-5.6-1.6-15.2-1.6c-12,0-24,4.8-24,20.8c0,18.4,20,16.8,20,24c0,4.8-5.6,5.6-9.6,5.6c-7.2,0-14.4-2.4-14.4-2.4
                        l-2.4,14.4C136,290.4,140,292,153.6,292z"/>
                    <path style="fill:#FFFFFF;" d="M472.8,214.4l-3.2,21.6c0,0-6.4-8-15.2-8c-14.4,0-27.2,17.6-27.2,38.4c0,12.8,6.4,26.4,20,26.4
                        c9.6,0,15.2-6.4,15.2-6.4l-0.8,5.6h16l12-76.8L472.8,214.4z M465.6,256.8c0,8.8-4,20-12.8,20c-5.6,0-8.8-4.8-8.8-12.8
                        c0-12.8,5.6-20.8,12.8-20.8C462.4,243.2,465.6,247.2,465.6,256.8z"/>
                    <path style="fill:#FFFFFF;" d="M29.6,291.2l9.6-57.6l1.6,57.6H52l20.8-57.6L64,291.2h16.8l12.8-76.8H67.2l-16,47.2l-0.8-47.2H27.2
                        l-12.8,76.8H29.6z"/>
                    <path style="fill:#FFFFFF;" d="M277.6,291.2c4.8-26.4,5.6-48,16.8-44c1.6-10.4,4-14.4,5.6-18.4c0,0-0.8,0-3.2,0
                        c-7.2,0-12.8,9.6-12.8,9.6l1.6-8.8h-15.2L260,292h17.6V291.2z"/>
                    <path style="fill:#FFFFFF;" d="M376.8,227.2c-11.2,0-19.2,3.2-19.2,3.2l-2.4,13.6c0,0,7.2-3.2,17.6-3.2c5.6,0,10.4,0.8,10.4,5.6
                        c0,3.2-0.8,4-0.8,4s-4.8,0-7.2,0c-13.6,0-28.8,5.6-28.8,24c0,14.4,9.6,17.6,15.2,17.6c11.2,0,16-7.2,16.8-7.2l-0.8,6.4H392l6.4-44
                        C399.2,228,382.4,227.2,376.8,227.2z M380.8,263.2c0,2.4-1.6,15.2-11.2,15.2c-4.8,0-6.4-4-6.4-6.4c0-4,2.4-9.6,14.4-9.6
                        C380,263.2,380,263.2,380.8,263.2z"/>
                    <path style="fill:#FFFFFF;" d="M412,291.2c4.8-26.4,5.6-48,16.8-44c1.6-10.4,4-14.4,5.6-18.4c0,0-0.8,0-3.2,0
                        c-7.2,0-12.8,9.6-12.8,9.6l1.6-8.8h-15.2L394.4,292H412V291.2z"/>
                </g>
                <g>
                    <path style="fill:#DCE5E5;" d="M180,279.2c0,9.6,5.6,13.6,12.8,13.6c5.6,0,10.4-1.6,12-2.4l2.4-13.6c-0.8,0-2.4,0.8-4,0.8
                        c-5.6,0-6.4-3.2-5.6-4.8l4.8-28h8.8l2.4-15.2h-8l1.6-9.6"/>
                    <path style="fill:#DCE5E5;" d="M218.4,264.8c0,22.4,7.2,28,20,28c12,0,16.8-2.4,16.8-2.4l3.2-15.2c0,0-8.8,4-16.8,4
                        c-17.6,0-14.4-12.8-14.4-12.8H260c0,0,2.4-10.4,2.4-14.4c0-10.4-5.6-23.2-23.2-23.2C222.4,227.2,218.4,244.8,218.4,264.8z
                         M238.4,241.6c8.8,0,10.4,10.4,10.4,11.2H228C228,252,229.6,241.6,238.4,241.6z"/>
                    <path style="fill:#DCE5E5;" d="M340,290.4l3.2-17.6c0,0-8,4-13.6,4c-11.2,0-16-8.8-16-18.4c0-19.2,9.6-29.6,20.8-29.6
                        c8,0,14.4,4.8,14.4,4.8l2.4-16.8c0,0-9.6-4-18.4-4c-18.4,0-28.8,16-28.8,46.4c0,20,1.6,33.6,20.8,33.6
                        C331.2,292.8,340,290.4,340,290.4z"/>
                    <path style="fill:#DCE5E5;" d="M95.2,244.8c0,0,7.2-3.2,17.6-3.2c5.6,0,10.4,0.8,10.4,5.6c0,3.2-0.8,4-0.8,4s-4.8,0-7.2,0
                        c-13.6,0-28.8,5.6-28.8,24c0,14.4,9.6,17.6,15.2,17.6c11.2,0,16-7.2,16.8-7.2l-0.8,6.4H132l6.4-44c0-18.4-16-19.2-22.4-19.2
                         M128,263.2c0,2.4-9.6,15.2-19.2,15.2c-4.8,0-6.4-4-6.4-6.4c0-4,2.4-9.6,14.4-9.6C119.2,263.2,128,263.2,128,263.2z"/>
                    <path style="fill:#DCE5E5;" d="M136,290.4c0,0,4.8,1.6,18.4,1.6c4,0,24,0.8,24-20.8c0-20-19.2-16-19.2-24c0-4,3.2-5.6,8.8-5.6
                        c2.4,0,11.2,0.8,11.2,0.8l2.4-14.4c0,0-5.6-1.6-15.2-1.6c-12,0-16,4.8-16,20.8c0,18.4,12,16.8,12,24c0,4.8-5.6,5.6-9.6,5.6"/>
                    <path style="fill:#DCE5E5;" d="M469.6,236c0,0-6.4-8-15.2-8c-14.4,0-19.2,17.6-19.2,38.4c0,12.8-1.6,26.4,12,26.4
                        c9.6,0,15.2-6.4,15.2-6.4l-0.8,5.6h16l12-76.8 M468.8,256.8c0,8.8-7.2,20-16,20c-5.6,0-8.8-4.8-8.8-12.8c0-12.8,5.6-20.8,12.8-20.8
                        C462.4,243.2,468.8,247.2,468.8,256.8z"/>
                    <path style="fill:#DCE5E5;" d="M29.6,291.2l9.6-57.6l1.6,57.6H52l20.8-57.6L64,291.2h16.8l12.8-76.8h-20l-22.4,47.2l-0.8-47.2h-8.8
                        l-27.2,76.8H29.6z"/>
                    <path style="fill:#DCE5E5;" d="M260.8,291.2h16.8c4.8-26.4,5.6-48,16.8-44c1.6-10.4,4-14.4,5.6-18.4c0,0-0.8,0-3.2,0
                        c-7.2,0-12.8,9.6-12.8,9.6l1.6-8.8"/>
                    <path style="fill:#DCE5E5;" d="M355.2,244.8c0,0,7.2-3.2,17.6-3.2c5.6,0,10.4,0.8,10.4,5.6c0,3.2-0.8,4-0.8,4s-4.8,0-7.2,0
                        c-13.6,0-28.8,5.6-28.8,24c0,14.4,9.6,17.6,15.2,17.6c11.2,0,16-7.2,16.8-7.2l-0.8,6.4H392l6.4-44c0-18.4-16-19.2-22.4-19.2
                         M388,263.2c0,2.4-9.6,15.2-19.2,15.2c-4.8,0-6.4-4-6.4-6.4c0-4,2.4-9.6,14.4-9.6C380,263.2,388,263.2,388,263.2z"/>
                    <path style="fill:#DCE5E5;" d="M395.2,291.2H412c4.8-26.4,5.6-48,16.8-44c1.6-10.4,4-14.4,5.6-18.4c0,0-0.8,0-3.2,0
                        c-7.2,0-12.8,9.6-12.8,9.6l1.6-8.8"/>
                </g>
              </svg>
              <svg version="1.1" id="amex" class="logo-cartao amex" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 291.764 291.764" style="enable-background:new 0 0 291.764 291.764;" xml:space="preserve">
                <g>
                    <path style="fill:#26A6D1;" d="M18.235,41.025h255.294c10.066,0,18.235,8.169,18.235,18.244v173.235
                        c0,10.066-8.169,18.235-18.235,18.235H18.235C8.16,250.74,0,242.57,0,232.505V59.269C0,49.194,8.169,41.025,18.235,41.025z"/>
                    <path style="fill:#FFFFFF;" d="M47.047,113.966l-28.812,63.76h34.492l4.276-10.166h9.774l4.276,10.166h37.966v-7.759l3.383,7.759
                        h19.639l3.383-7.923v7.923h78.959l9.601-9.902l8.99,9.902l40.555,0.082l-28.903-31.784l28.903-32.058h-39.926l-9.346,9.719
                        l-8.707-9.719h-85.897l-7.376,16.457l-7.549-16.457h-34.42v7.495l-3.829-7.495C76.479,113.966,47.047,113.966,47.047,113.966z
                         M53.721,123.02h16.813l19.111,43.236V123.02h18.418l14.761,31l13.604-31h18.326v45.752h-11.151l-0.091-35.851l-16.257,35.851
                        h-9.975l-16.348-35.851v35.851h-22.94l-4.349-10.257H50.147l-4.34,10.248H33.516C33.516,168.763,53.721,123.02,53.721,123.02z
                         M164.956,123.02h45.342L224.166,138l14.315-14.98h13.868l-21.071,22.995l21.071,22.73h-14.497l-13.868-15.154l-14.388,15.154
                        h-44.64L164.956,123.02L164.956,123.02z M61.9,130.761l-7.741,18.272h15.473L61.9,130.761z M176.153,132.493v8.352h24.736v9.309
                        h-24.736v9.118h27.745l12.892-13.43l-12.345-13.357h-28.292L176.153,132.493z"/>
                </g>
              </svg>
              <svg version="1.1" id="diners" class="logo-cartao diners" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="43.96px" height="43.959px" viewBox="0 0 43.96 43.959" style="enable-background:new 0 0 43.96 43.959;"
                     xml:space="preserve">
                <g>
                    <g id="Layer_1_copy_44_">
                        <path d="M23.6,39.674c-0.064-0.15-0.125-0.336-0.187-0.516l-0.933-2.646c-0.017-0.043-0.03-0.088-0.045-0.129
                            c-0.017-0.025-0.043-0.029-0.056-0.029c-0.021,0-0.034,0.008-0.049,0.013c-0.099,0.062-0.302,0.164-0.459,0.217
                            c-0.043,0.202-0.135,0.453-0.201,0.642l-0.811,2.322c-0.074,0.207-0.23,0.312-0.418,0.312h-0.03H20.39v0.174v0.021h0.024
                            c0.181-0.01,0.362-0.021,0.545-0.021c0.202,0,0.409,0.011,0.61,0.021h0.022v-0.174V39.86h-0.077c-0.168,0-0.356-0.03-0.358-0.154
                            c0-0.082,0.061-0.216,0.112-0.389l0.156-0.512h1.115l0.193,0.572c0.057,0.16,0.104,0.299,0.1,0.355
                            c0,0.098-0.172,0.125-0.291,0.125h-0.057h-0.021v0.174v0.023l0.022-0.002c0.26-0.009,0.51-0.021,0.758-0.021
                            c0.24,0,0.471,0.012,0.695,0.021l0.022,0.002v-0.177v-0.021h-0.051C23.751,39.861,23.658,39.805,23.6,39.674z M23.941,40.036
                            c-0.228-0.01-0.453-0.021-0.695-0.021c-0.248,0-0.502,0.011-0.76,0.021v-0.15h0.057c0.117,0,0.312-0.021,0.312-0.147
                            c0-0.065-0.045-0.2-0.101-0.363l-0.198-0.587h-1.147l-0.161,0.527c-0.051,0.17-0.112,0.304-0.112,0.394
                            c0,0.153,0.213,0.177,0.379,0.177h0.057v0.152c-0.204-0.01-0.41-0.02-0.614-0.02c-0.182,0-0.363,0.009-0.545,0.02v-0.152h0.03
                            c0.197,0,0.366-0.117,0.439-0.33l0.81-2.319c0.066-0.188,0.158-0.438,0.188-0.629c0.161-0.056,0.363-0.155,0.458-0.218
                            c0.016-0.004,0.023-0.01,0.041-0.01c0.016,0,0.025,0,0.036,0.016c0.015,0.041,0.03,0.087,0.046,0.125l0.932,2.646
                            c0.061,0.178,0.119,0.363,0.188,0.518c0.061,0.143,0.166,0.201,0.334,0.201h0.027V40.036z M27.273,36.526
                            c0.194,0.002,0.483,0.021,0.725,0.021c0.246,0,0.539-0.02,0.771-0.021v0.107h-0.047c-0.185,0-0.396,0.031-0.396,0.312v2.648
                            c0,0.279,0.213,0.312,0.396,0.312h0.047v0.105c-0.238,0-0.531-0.02-0.775-0.02c-0.242,0-0.527,0.018-0.721,0.02v-0.105h0.047
                            c0.182,0,0.396-0.032,0.396-0.312v-2.648c0-0.28-0.215-0.312-0.396-0.312h-0.047V36.526z M21.975,37.071v0.003l-0.469,1.431h0.945
                            l-0.465-1.434H21.975z M21.535,38.479l0.445-1.354l0.439,1.354H21.535z M26.96,36.373l0.078,0.013
                            c-0.021,0.131-0.039,0.26-0.054,0.391c-0.01,0.132-0.01,0.265-0.01,0.396l-0.111,0.041c-0.008-0.188-0.053-0.478-0.393-0.479
                            h-0.852v2.714c0,0.396,0.197,0.457,0.439,0.458h0.073v0.106c-0.198-0.002-0.536-0.021-0.801-0.021
                            c-0.293,0-0.634,0.019-0.834,0.021v-0.105h0.073c0.279-0.001,0.441-0.041,0.441-0.446v-2.726H24.16
                            c-0.377-0.006-0.422,0.109-0.488,0.461h-0.107c0.019-0.132,0.045-0.268,0.058-0.405c0.021-0.134,0.028-0.271,0.03-0.409h0.082
                            c0.047,0.141,0.177,0.147,0.295,0.146h0.025h2.609C26.794,36.529,26.938,36.52,26.96,36.373z M29.712,34.618
                            c-2.174,0-3.551-1.404-3.551-3.497c0-1.905,1.7-3.272,3.522-3.272c0.752,0,1.479,0.244,2.195,0.494l0.092,1.33h-0.23
                            c-0.178-1.09-0.947-1.525-1.988-1.525c-1.07,0-2.621,0.716-2.621,2.947c0,1.879,1.34,3.227,2.771,3.227
                            c0.918,0,1.681-0.633,1.867-1.607l0.213,0.055l-0.213,1.356C31.378,34.367,30.328,34.618,29.712,34.618z M20.455,36.678v-0.172
                            v-0.022h-0.022c-0.208,0-0.415,0.021-0.622,0.021c-0.236,0-0.469-0.021-0.708-0.021h-0.022v0.174v0.021h0.133
                            c0.147,0.002,0.417,0.054,0.418,0.455v1.86l-2.224-2.504c-0.168-0.007-0.316,0.013-0.464,0.013c-0.191,0-0.388-0.02-0.581-0.02
                            H16.34v0.174v0.021h0.087c0.218,0,0.438,0.164,0.438,0.433v2.13c-0.003,0.404-0.099,0.62-0.432,0.621h-0.07H16.34v0.174v0.021
                            h0.023c0.227,0,0.461-0.021,0.687-0.021c0.217,0,0.433,0.021,0.652,0.021h0.022v-0.174v-0.021h-0.106
                            c-0.382-0.006-0.45-0.129-0.455-0.569v-2.026l2.567,2.861l0.208,0.008h0.025l-0.003-0.023c-0.027-0.156-0.031-0.332-0.031-0.51
                            v-2.301c0.005-0.598,0.22-0.615,0.429-0.621h0.075h0.022V36.678z M19.908,37.299v2.303c0,0.177,0.004,0.353,0.029,0.51h-0.193
                            l-2.604-2.905v2.085c0,0.441,0.086,0.594,0.477,0.594h0.084v0.15c-0.216,0-0.434-0.021-0.651-0.021
                            c-0.229,0-0.461,0.021-0.688,0.021v-0.15h0.07c0.35,0,0.455-0.238,0.455-0.643v-2.132c0-0.283-0.232-0.455-0.46-0.455h-0.064
                            v-0.151c0.19,0,0.39,0.02,0.581,0.02c0.15,0,0.299-0.02,0.45-0.02l2.251,2.534h0.001l0.004-0.007l0.003-0.003v-1.896
                            c0-0.416-0.287-0.477-0.439-0.477h-0.11v-0.15c0.238,0,0.47,0.02,0.708,0.02c0.208,0,0.414-0.02,0.623-0.02v0.15h-0.076
                            C20.146,36.657,19.908,36.697,19.908,37.299z M26.921,36.322l-0.002,0.022c-0.008,0.133-0.108,0.135-0.25,0.138h-2.61h-0.023
                            c-0.131-0.003-0.223-0.005-0.258-0.132l-0.006-0.016h-0.138h-0.021v0.021c0,0.146-0.012,0.287-0.03,0.427
                            c-0.017,0.146-0.041,0.288-0.062,0.431l-0.002,0.025h0.176h0.019l0.004-0.018c0.084-0.375,0.074-0.438,0.449-0.443h0.808
                            l0.002,2.68c-0.009,0.393-0.119,0.396-0.398,0.402h-0.096h-0.023v0.174v0.021h0.023c0.197,0,0.553-0.021,0.854-0.021
                            c0.271,0,0.625,0.021,0.824,0.021h0.019v-0.174V39.86h-0.119c-0.24-0.006-0.394-0.031-0.396-0.413v-2.669h0.809
                            c0.318,0,0.34,0.275,0.353,0.465l0.002,0.029l0.178-0.065l0.015-0.006v-0.015c0-0.135,0-0.272,0.01-0.406
                            c0.016-0.137,0.035-0.271,0.055-0.408l0.006-0.021l-0.145-0.021L26.921,36.322z M27.007,36.778c-0.01,0.138-0.01,0.273-0.01,0.408
                            l-0.151,0.058c-0.011-0.187-0.035-0.485-0.373-0.485h-0.832v2.69c0,0.392,0.178,0.437,0.42,0.437h0.094v0.151
                            c-0.193,0-0.549-0.021-0.822-0.021c-0.303,0-0.658,0.021-0.854,0.021v-0.151h0.096c0.279,0,0.421-0.024,0.421-0.425v-2.702h-0.83
                            c-0.379,0-0.396,0.092-0.472,0.461h-0.151c0.021-0.144,0.047-0.283,0.062-0.433c0.021-0.142,0.03-0.282,0.03-0.43h0.123
                            c0.039,0.151,0.166,0.146,0.302,0.146h2.61c0.138,0,0.263-0.005,0.272-0.156l0.121,0.021
                            C27.042,36.506,27.025,36.639,27.007,36.778z M23.559,39.691c-0.066-0.154-0.127-0.343-0.188-0.521l-0.931-2.646
                            c-0.017-0.039-0.03-0.087-0.045-0.122c-0.002-0.006-0.002-0.006-0.008-0.006H22.38c-0.011,0-0.019,0.005-0.028,0.008
                            c-0.098,0.062-0.302,0.163-0.451,0.203c-0.032,0.191-0.123,0.443-0.189,0.631l-0.809,2.322c-0.077,0.22-0.254,0.346-0.46,0.346
                            h-0.008v0.105c0.173-0.011,0.348-0.021,0.525-0.021c0.196,0,0.395,0.011,0.588,0.021v-0.105h-0.033
                            c-0.164-0.003-0.395-0.021-0.401-0.2c0.001-0.101,0.063-0.231,0.113-0.401l0.166-0.543h1.177l0.205,0.604
                            c0.057,0.163,0.104,0.297,0.104,0.371c-0.006,0.154-0.221,0.167-0.336,0.171H22.51v0.104c0.25-0.01,0.496-0.021,0.736-0.021
                            c0.234,0,0.455,0.011,0.676,0.021v-0.104h-0.008C23.74,39.908,23.621,39.84,23.559,39.691z M21.474,38.525l0.484-1.476h0.016
                            h0.011H22l0.482,1.476H21.474z M14.141,36.686c-0.151,0-0.217,0.017-0.288,0.021v1.508h0.248c0.506,0,0.778-0.191,0.778-0.789
                            C14.878,36.973,14.591,36.686,14.141,36.686z M14.101,38.192h-0.226v-1.467c0.065-0.006,0.129-0.02,0.266-0.02
                            c0.44,0.004,0.715,0.277,0.718,0.719C14.855,38.012,14.601,38.186,14.101,38.192z M13.707,34.475
                            c-0.327,0-0.651-0.018-0.986-0.018c-0.335,0-0.671,0.01-1.003,0.018v-0.238h0.167c0.26,0,0.445,0,0.445-0.311V31.52
                            c0-0.53-0.324-0.793-0.855-0.793c-0.296,0-0.77,0.244-1.079,0.447v2.752c0,0.31,0.207,0.31,0.467,0.31h0.167v0.239
                            c-0.325,0-0.651-0.019-0.986-0.019c-0.334,0-0.669,0.009-1.003,0.019v-0.239h0.167c0.26,0,0.445,0,0.445-0.31v-2.463h0.001
                            c0-0.345-0.104-0.438-0.54-0.613v-0.176c0.4-0.131,0.78-0.252,1.228-0.447c0.026,0,0.054,0.019,0.054,0.094v0.604
                            c0.531-0.381,0.987-0.697,1.61-0.697c0.79,0,1.069,0.576,1.069,1.301v2.398c0,0.31,0.203,0.31,0.464,0.31h0.168V34.475z
                             M15.702,39.594l-0.881-1.318l0.025-0.01c0.395-0.154,0.689-0.426,0.689-0.871c0-0.701-0.546-0.865-1.182-0.868
                            c-0.267,0-0.51,0.021-0.814,0.021c-0.297,0-0.598-0.019-0.763-0.021v0.108h0.069c0.158,0,0.381,0.025,0.381,0.405v2.57
                            c0,0.203-0.189,0.297-0.381,0.297h-0.069v0.104c0.243,0,0.474-0.021,0.711-0.021c0.284,0,0.569,0.021,0.848,0.021v-0.104h-0.068
                            c-0.23,0-0.438-0.028-0.438-0.431v-1.104h0.023h0.346l0.005,0.012c0.331,0.582,0.663,1.129,1.036,1.629
                            c0.154,0,0.315-0.021,0.473-0.021c0.162,0,0.317,0.017,0.475,0.021v-0.109C15.951,39.873,15.84,39.803,15.702,39.594z
                             M14.101,38.236h-0.248H13.83v-1.55l0.021-0.002c0.068-0.006,0.137-0.02,0.29-0.02c0.461,0,0.761,0.301,0.761,0.76
                            C14.9,38.031,14.61,38.236,14.101,38.236z M15.738,39.568l-0.85-1.271c0.39-0.16,0.692-0.445,0.692-0.902
                            c-0.001-0.734-0.588-0.909-1.227-0.911c-0.27,0-0.512,0.021-0.814,0.021c-0.308,0-0.621-0.021-0.784-0.021h-0.022v0.174v0.022
                            h0.112c0.158,0.004,0.333,0.002,0.337,0.36v2.57c-0.001,0.171-0.153,0.25-0.337,0.25h-0.09h-0.022v0.174v0.021h0.022
                            c0.253,0,0.49-0.021,0.732-0.021c0.287,0,0.582,0.021,0.87,0.021h0.023v-0.174v-0.021h-0.113
                            c-0.234-0.007-0.387-0.002-0.393-0.384v-1.061h0.299c0.327,0.578,0.659,1.121,1.047,1.631c0.176,0.008,0.338-0.013,0.492-0.013
                            c0.166,0,0.328,0.021,0.497,0.021h0.021v-0.174v-0.02l-0.018-0.003C15.967,39.83,15.879,39.779,15.738,39.568z M16.21,40.036
                            c-0.166,0-0.329-0.021-0.495-0.021c-0.157,0-0.32,0.021-0.477,0.021c-0.389-0.513-0.724-1.059-1.052-1.643h-0.333v1.084
                            c0,0.392,0.181,0.406,0.415,0.406h0.091v0.151c-0.29,0-0.582-0.021-0.87-0.021c-0.243,0-0.481,0.021-0.733,0.021v-0.151h0.09
                            c0.189,0,0.36-0.086,0.36-0.272v-2.57c0-0.366-0.203-0.384-0.36-0.384h-0.09v-0.15c0.162,0,0.475,0.02,0.784,0.02
                            c0.303,0,0.546-0.02,0.814-0.02c0.637,0,1.205,0.172,1.205,0.891c0,0.453-0.303,0.732-0.704,0.89l0.864,1.294
                            c0.142,0.215,0.243,0.275,0.491,0.305V40.036z M12.366,39.172l-0.005,0.02c-0.152,0.568-0.397,0.598-1.005,0.602
                            c-0.33-0.002-0.596,0-0.596-0.308v-1.159h0.585c0.336,0.004,0.338,0.173,0.373,0.48l0.002,0.024l0.175-0.034l0.019-0.006
                            l-0.001-0.018c-0.009-0.193-0.016-0.385-0.016-0.577c0-0.197,0.007-0.396,0.016-0.591l0.001-0.021h-0.175H11.72l-0.002,0.02
                            c-0.034,0.271-0.086,0.423-0.378,0.426h-0.58v-1.281h0.681c0.522,0.006,0.565,0.242,0.595,0.571l0.002,0.024l0.178-0.045
                            l0.016-0.006v-0.02c-0.011-0.145-0.016-0.334-0.016-0.51c0-0.097,0.002-0.187,0.005-0.263l0.001-0.021h-0.023
                            c-0.344,0-0.868,0.019-1.294,0.019c-0.423,0-0.95-0.019-1.258-0.019H9.625v0.173v0.021h0.093c0.184,0.004,0.35,0.021,0.351,0.27
                            v2.648c-0.001,0.248-0.167,0.265-0.351,0.268H9.647H9.625v0.021v0.151v0.021h0.022c0.288,0,0.89-0.02,1.344-0.02
                            c0.455,0,1.037,0.02,1.361,0.02h0.018l0.004-0.018c0.046-0.268,0.106-0.535,0.171-0.803l0.005-0.021l-0.162-0.041h-0.022V39.172z
                             M12.352,40.036c-0.325,0-0.906-0.021-1.36-0.021c-0.457,0-1.058,0.021-1.346,0.021v-0.15h0.071c0.183,0,0.374-0.024,0.374-0.289
                            v-2.65c0-0.266-0.191-0.289-0.374-0.289H9.646v-0.152c0.308,0,0.835,0.021,1.26,0.021c0.425,0,0.949-0.021,1.293-0.021
                            c-0.008,0.219-0.004,0.553,0.013,0.774l-0.154,0.04c-0.025-0.328-0.085-0.592-0.617-0.592h-0.702v1.326h0.603
                            c0.301,0,0.368-0.172,0.397-0.447h0.152c-0.01,0.199-0.015,0.396-0.015,0.594c0,0.191,0.005,0.384,0.015,0.577l-0.152,0.03
                            c-0.03-0.304-0.044-0.5-0.394-0.5h-0.606v1.176c0,0.33,0.292,0.33,0.617,0.33c0.607,0,0.874-0.041,1.026-0.616l0.142,0.034
                            C12.458,39.5,12.397,39.766,12.352,40.036z M19.811,36.547c0.201,0,0.4-0.02,0.6-0.021v0.105h-0.053
                            c-0.218,0.002-0.472,0.061-0.472,0.666V39.6c0,0.168,0.003,0.334,0.026,0.488h-0.167l-2.627-2.938v2.14
                            c0.002,0.44,0.1,0.614,0.5,0.616h0.062v0.105c-0.209,0-0.418-0.021-0.631-0.021c-0.223,0-0.448,0.021-0.667,0.021v-0.105
                            l0.05,0.001c0.362-0.002,0.477-0.263,0.477-0.667v-2.13c0-0.297-0.243-0.478-0.482-0.479h-0.044v-0.105
                            c0.185,0.002,0.374,0.021,0.561,0.021c0.153,0,0.299-0.021,0.45-0.021l2.251,2.545l0.016-0.018l0.009-0.01l0.007-1.912
                            c0-0.43-0.306-0.498-0.463-0.5h-0.089v-0.105C19.355,36.528,19.579,36.547,19.811,36.547z M19.632,39.029l-0.004-0.006
                            L19.632,39.029L19.632,39.029z M27.998,36.501c-0.246,0-0.551-0.02-0.748-0.02h-0.021v0.174v0.021h0.094
                            c0.183,0.002,0.351,0.021,0.353,0.269v2.649c-0.002,0.248-0.17,0.265-0.353,0.267H27.25h-0.021v0.021v0.152v0.021h0.021
                            c0.197,0,0.498-0.021,0.746-0.021c0.252,0,0.553,0.021,0.797,0.021h0.021v-0.174V39.86h-0.092
                            c-0.185-0.002-0.351-0.018-0.354-0.266v-2.65c0.002-0.247,0.168-0.266,0.354-0.268h0.069h0.021v-0.175v-0.021h-0.021
                            C28.555,36.483,28.25,36.501,27.998,36.501z M28.792,36.657h-0.069c-0.183,0-0.375,0.023-0.375,0.289v2.648
                            c0,0.266,0.192,0.289,0.375,0.289h0.069v0.152c-0.242,0-0.545-0.02-0.797-0.02c-0.248,0-0.548,0.02-0.746,0.02v-0.152h0.071
                            c0.183,0,0.373-0.022,0.373-0.289v-2.648c0-0.266-0.19-0.289-0.373-0.289h-0.07v-0.15c0.197,0,0.5,0.02,0.748,0.02
                            c0.256,0,0.561-0.02,0.795-0.02L28.792,36.657L28.792,36.657z M43.203,39.555c0.08-0.111,0.125-0.248,0.156-0.357h0.104
                            c-0.053,0.273-0.121,0.542-0.18,0.816c-0.438,0-0.875-0.021-1.316-0.021c-0.439,0-0.875,0.021-1.314,0.021v-0.107h0.05
                            c0.182,0,0.401-0.034,0.401-0.359v-2.603c0-0.278-0.223-0.312-0.401-0.312h-0.05v-0.106c0.261,0.002,0.519,0.021,0.779,0.021
                            c0.25,0,0.496-0.02,0.746-0.021v0.106h-0.105c-0.188,0-0.366,0.017-0.366,0.297v2.609c0,0.217,0.16,0.287,0.323,0.305
                            c0.093,0.006,0.187,0.013,0.285,0.013c0.123,0,0.254-0.007,0.39-0.021C42.921,39.807,43.109,39.682,43.203,39.555z M40.07,39.691
                            c-0.064-0.154-0.127-0.343-0.187-0.521l-0.932-2.646c-0.017-0.039-0.029-0.087-0.043-0.12c-0.005-0.008-0.003-0.008-0.009-0.008
                            h-0.008c-0.012,0-0.018,0.005-0.029,0.008c-0.098,0.062-0.301,0.164-0.45,0.203c-0.031,0.191-0.123,0.443-0.188,0.631
                            l-0.812,2.322c-0.076,0.22-0.252,0.346-0.459,0.346h-0.009v0.105c0.175-0.011,0.349-0.021,0.525-0.021
                            c0.195,0,0.396,0.011,0.59,0.021v-0.105h-0.035c-0.164-0.003-0.395-0.021-0.399-0.201c0-0.1,0.063-0.229,0.11-0.4l0.166-0.543
                            h1.181l0.205,0.604c0.055,0.163,0.103,0.297,0.103,0.371c-0.009,0.154-0.222,0.167-0.336,0.171H39.02v0.104h0.002
                            c0.25-0.01,0.496-0.021,0.735-0.021c0.234,0,0.455,0.011,0.677,0.021v-0.104h-0.009C40.253,39.908,40.132,39.84,40.07,39.691z
                             M37.986,38.525l0.484-1.476h0.016h0.012h0.018l0.479,1.476H37.986z M38.488,37.071l-0.002,0.002l-0.469,1.432h0.946l-0.467-1.434
                            H38.488z M38.046,38.479l0.445-1.354l0.438,1.354H38.046z M43.326,39.154l-0.006,0.019c-0.027,0.108-0.072,0.25-0.15,0.356
                            c-0.088,0.121-0.266,0.239-0.473,0.263c-0.129,0.016-0.258,0.021-0.383,0.021c-0.099,0-0.189-0.004-0.279-0.01
                            c-0.158-0.023-0.281-0.072-0.283-0.263v-2.608c0.004-0.256,0.127-0.246,0.322-0.252h0.127h0.021v-0.174v-0.021h-0.021
                            c-0.26,0-0.514,0.02-0.77,0.02c-0.269,0-0.533-0.02-0.802-0.02h-0.021v0.173v0.021h0.092c0.186,0.002,0.357,0.021,0.359,0.267
                            v2.603c-0.002,0.299-0.177,0.312-0.359,0.314h-0.07h-0.021v0.174v0.021h0.021c0.445,0,0.893-0.021,1.336-0.021
                            c0.445,0,0.891,0.021,1.334,0.021h0.018l0.004-0.018c0.062-0.288,0.132-0.568,0.189-0.859l0.004-0.025h-0.174L43.326,39.154
                            L43.326,39.154z M43.3,40.036c-0.442,0-0.889-0.021-1.332-0.021c-0.446,0-0.891,0.021-1.336,0.021v-0.15H40.7
                            c0.184,0,0.383-0.024,0.383-0.341v-2.6c0-0.267-0.199-0.289-0.383-0.289h-0.068v-0.151c0.269,0,0.531,0.02,0.8,0.02
                            c0.258,0,0.51-0.02,0.77-0.02v0.151h-0.129c-0.189,0-0.342,0.005-0.342,0.272v2.609c0,0.203,0.141,0.264,0.303,0.284
                            c0.207,0.015,0.436,0.015,0.668-0.011c0.213-0.025,0.395-0.146,0.484-0.272c0.08-0.11,0.125-0.254,0.155-0.364h0.146
                            C43.434,39.465,43.363,39.747,43.3,40.036z M43.43,36.362c-0.306,0-0.533,0.232-0.533,0.533c0,0.299,0.229,0.529,0.533,0.529
                            c0.303,0,0.53-0.23,0.53-0.529S43.73,36.362,43.43,36.362z M43.43,37.327c-0.24,0-0.425-0.204-0.425-0.434
                            c0-0.229,0.185-0.434,0.425-0.434c0.239,0,0.42,0.204,0.42,0.434S43.666,37.327,43.43,37.327z M43.492,36.911
                            c0.072-0.021,0.125-0.081,0.125-0.161c0-0.097-0.084-0.141-0.18-0.141h-0.271v0.026c0.068-0.004,0.076,0.017,0.076,0.07V37.1
                            c0,0.041-0.014,0.039-0.078,0.05v0.024h0.258V37.15c-0.066-0.011-0.08-0.009-0.08-0.042v-0.18h0.061
                            c0.099,0.143,0.144,0.246,0.191,0.246h0.104v-0.017c-0.029-0.019-0.076-0.069-0.115-0.126L43.492,36.911z M43.406,36.895h-0.064
                            v-0.252h0.061c0.058,0,0.105,0.031,0.105,0.117C43.507,36.846,43.475,36.895,43.406,36.895z M40.111,39.674
                            c-0.064-0.15-0.125-0.336-0.188-0.516l-0.93-2.646c-0.018-0.043-0.031-0.088-0.047-0.129c-0.016-0.025-0.041-0.029-0.055-0.029
                            c-0.021,0-0.037,0.008-0.047,0.013c-0.101,0.062-0.304,0.163-0.462,0.219c-0.045,0.2-0.137,0.451-0.198,0.64l-0.812,2.322
                            c-0.074,0.204-0.231,0.312-0.42,0.312h-0.029h-0.021v0.174v0.021h0.021c0.185-0.01,0.365-0.021,0.546-0.021
                            c0.2,0,0.409,0.011,0.61,0.021h0.021v-0.174V39.86h-0.076c-0.168,0-0.354-0.03-0.354-0.155c-0.002-0.081,0.058-0.214,0.108-0.388
                            l0.156-0.512h1.117l0.191,0.572c0.057,0.161,0.104,0.299,0.102,0.355c0,0.098-0.176,0.125-0.293,0.125H39h-0.023v0.174v0.023
                            L39,40.053c0.256-0.009,0.51-0.021,0.758-0.021c0.242,0,0.469,0.012,0.695,0.021l0.023,0.002v-0.176v-0.021h-0.052
                            C40.264,39.861,40.169,39.805,40.111,39.674z M40.457,40.036c-0.229-0.01-0.457-0.021-0.699-0.021c-0.248,0-0.5,0.011-0.758,0.021
                            v-0.15h0.055c0.115,0,0.314-0.021,0.314-0.147c0-0.065-0.047-0.2-0.104-0.363l-0.197-0.587h-1.146l-0.164,0.527
                            c-0.049,0.17-0.109,0.304-0.109,0.394c0,0.153,0.213,0.177,0.379,0.177h0.058v0.152c-0.203-0.01-0.41-0.02-0.612-0.02
                            c-0.183,0-0.363,0.009-0.546,0.02v-0.152h0.029c0.198,0,0.364-0.117,0.44-0.33l0.81-2.319c0.065-0.188,0.157-0.438,0.187-0.629
                            c0.162-0.056,0.364-0.155,0.461-0.218c0.018-0.004,0.024-0.01,0.041-0.01c0.016,0,0.024,0,0.035,0.016
                            c0.016,0.041,0.032,0.087,0.047,0.125l0.93,2.646c0.061,0.178,0.121,0.363,0.189,0.518c0.061,0.143,0.165,0.201,0.331,0.201h0.031
                            L40.457,40.036L40.457,40.036z M30.811,36.407c-1.062,0.001-1.888,0.825-1.89,1.891l0,0c0.002,1.1,0.832,1.834,1.914,1.838
                            c1.086-0.004,1.935-0.785,1.935-1.961C32.768,37.083,31.896,36.408,30.811,36.407z M30.835,40.113
                            c-1.073,0-1.892-0.729-1.892-1.815c0-1.054,0.812-1.867,1.865-1.867c1.078,0,1.938,0.668,1.938,1.744
                            C32.748,39.34,31.914,40.113,30.835,40.113z M30.811,36.45c-1.041,0-1.847,0.807-1.847,1.848c0.002,1.074,0.812,1.793,1.873,1.793
                            c1.065,0,1.888-0.766,1.892-1.916C32.727,37.111,31.878,36.452,30.811,36.45z M30.886,39.911c-0.84-0.001-1.254-0.882-1.256-1.793
                            c0.002-0.682,0.269-1.486,1.168-1.486c0.861,0,1.261,0.911,1.263,1.654C32.061,39.023,31.887,39.911,30.886,39.911z
                             M30.798,36.651c-0.885,0-1.146,0.791-1.146,1.467c0,0.905,0.414,1.771,1.233,1.771c0.982,0,1.152-0.866,1.152-1.604
                            C32.039,37.547,31.641,36.651,30.798,36.651z M30.886,39.867c-0.801-0.001-1.211-0.849-1.213-1.75
                            c0.004-0.674,0.259-1.442,1.125-1.444c0.824,0.002,1.22,0.879,1.22,1.611C32.016,39.023,31.85,39.867,30.886,39.867z
                             M36.322,36.547c0.201,0,0.402-0.02,0.602-0.021v0.105h-0.055c-0.217,0.002-0.471,0.058-0.471,0.666V39.6
                            c0,0.168,0.004,0.334,0.024,0.488h-0.166L33.63,37.15v2.142c0,0.44,0.101,0.614,0.498,0.616h0.063v0.104
                            c-0.209,0-0.418-0.02-0.629-0.02c-0.225,0-0.449,0.02-0.668,0.02v-0.105l0.051,0.001c0.363-0.002,0.478-0.263,0.478-0.665v-2.132
                            c0-0.297-0.246-0.478-0.481-0.479h-0.045v-0.105c0.186,0.002,0.375,0.021,0.561,0.021c0.152,0,0.301-0.021,0.451-0.021l2.25,2.545
                            l0.018-0.018l0.012-0.01l0.004-1.912c0-0.429-0.307-0.498-0.461-0.5h-0.09v-0.105C35.867,36.528,36.091,36.547,36.322,36.547z
                             M36.968,36.678v-0.172v-0.021h-0.021c-0.211,0-0.418,0.02-0.625,0.02c-0.234,0-0.467-0.02-0.707-0.02h-0.023v0.173v0.021h0.135
                            c0.148,0.004,0.418,0.054,0.42,0.455v1.86l-2.225-2.502c-0.172-0.008-0.316,0.011-0.469,0.011c-0.189,0-0.387-0.019-0.58-0.019
                            H32.85v0.173v0.021h0.088c0.217,0,0.438,0.164,0.438,0.433v2.132c-0.004,0.402-0.101,0.617-0.435,0.619h-0.069H32.85v0.174v0.021
                            h0.022c0.229,0,0.461-0.021,0.688-0.021c0.215,0,0.433,0.021,0.65,0.021h0.021v-0.174v-0.021h-0.107
                            c-0.381-0.006-0.447-0.129-0.453-0.569v-2.026l2.564,2.861l0.209,0.006h0.025l-0.005-0.022c-0.024-0.155-0.028-0.332-0.028-0.509
                            v-2.303c0.004-0.596,0.221-0.615,0.428-0.621h0.076H36.968L36.968,36.678z M36.419,37.299v2.303c0,0.177,0.004,0.353,0.031,0.51
                            h-0.193l-2.604-2.904v2.085c0,0.441,0.086,0.593,0.477,0.593h0.086v0.151c-0.217,0-0.436-0.021-0.65-0.021
                            c-0.229,0-0.461,0.021-0.688,0.021v-0.151h0.069c0.351,0,0.455-0.237,0.455-0.642v-2.132c0-0.283-0.229-0.455-0.459-0.455h-0.065
                            v-0.152c0.19,0,0.39,0.021,0.58,0.021c0.153,0,0.299-0.021,0.45-0.021l2.252,2.535l0.007-0.007l0.004-0.003v-1.896
                            c0-0.416-0.287-0.476-0.44-0.476h-0.107v-0.151c0.236,0,0.469,0.021,0.707,0.021c0.207,0,0.414-0.021,0.622-0.021v0.151h-0.077
                            C36.658,36.657,36.419,36.697,36.419,37.299z M36.145,39.029l-0.004-0.006L36.145,39.029L36.145,39.029z M9.343,36.322
                            L9.34,36.344c-0.008,0.133-0.111,0.135-0.252,0.138H6.48H6.455c-0.129-0.003-0.223-0.005-0.258-0.132l-0.002-0.016h-0.14H6.034
                            v0.021c0,0.146-0.009,0.287-0.031,0.427c-0.015,0.146-0.04,0.289-0.062,0.429L5.94,37.239h0.176h0.018l0.003-0.018
                            c0.083-0.375,0.076-0.438,0.449-0.443h0.808v2.68c-0.006,0.393-0.119,0.396-0.396,0.402H6.901H6.88v0.174v0.021h0.021
                            c0.198,0,0.553-0.021,0.855-0.021c0.271,0,0.625,0.021,0.825,0.021h0.021v-0.174V39.86H8.483
                            c-0.241-0.006-0.393-0.031-0.397-0.413v-2.669h0.807c0.32,0,0.34,0.275,0.354,0.465l0.001,0.029l0.179-0.065l0.014-0.006v-0.015
                            c0-0.137,0-0.272,0.009-0.406c0.015-0.137,0.035-0.271,0.056-0.408l0.003-0.021l-0.143-0.021L9.343,36.322z M9.428,36.778
                            c-0.009,0.138-0.009,0.273-0.009,0.408l-0.153,0.058c-0.011-0.187-0.035-0.485-0.374-0.485H8.063v2.69
                            c0,0.392,0.177,0.437,0.418,0.437h0.098v0.151c-0.197,0-0.552-0.021-0.824-0.021c-0.304,0-0.658,0.021-0.855,0.021v-0.151h0.095
                            c0.279,0,0.42-0.024,0.42-0.425v-2.702h-0.83c-0.379,0-0.394,0.092-0.47,0.461H5.963c0.021-0.144,0.046-0.283,0.061-0.433
                            c0.02-0.142,0.03-0.282,0.03-0.43h0.123c0.041,0.151,0.167,0.146,0.303,0.146h2.61c0.137,0,0.263-0.005,0.274-0.156l0.12,0.021
                            C9.463,36.506,9.444,36.639,9.428,36.778z M5.027,39.029l-0.004-0.006l0.004,0.004V39.029z M18.773,31.744v2.185
                            c0,0.31,0.186,0.31,0.446,0.31h0.52v0.239c-0.511-0.008-0.9-0.019-1.3-0.019c-0.382,0-0.771,0.011-1.106,0.019v-0.24h0.251
                            c0.259,0,0.445,0,0.445-0.311v-2.609c0-0.288-0.343-0.346-0.483-0.419v-0.14c0.679-0.289,1.051-0.529,1.135-0.529
                            c0.055,0,0.083,0.027,0.083,0.119v0.838h0.02c0.232-0.361,0.623-0.957,1.189-0.957c0.232,0,0.531,0.156,0.531,0.492
                            c0,0.25-0.177,0.476-0.437,0.476c-0.289,0-0.289-0.224-0.614-0.224C19.293,30.971,18.773,31.184,18.773,31.744z M22.834,33.659
                            c0-1.1-2.024-0.744-2.024-2.239c0-0.521,0.419-1.19,1.44-1.19c0.297,0,0.697,0.084,1.061,0.271l0.064,0.947H23.16
                            c-0.094-0.585-0.418-0.92-1.016-0.92c-0.371,0-0.724,0.215-0.724,0.612c0,1.09,2.157,0.754,2.157,2.213
                            c0,0.615-0.494,1.267-1.6,1.267c-0.373,0-0.809-0.131-1.134-0.315l-0.103-1.069l0.167-0.048c0.122,0.613,0.492,1.136,1.172,1.136
                            C22.628,34.32,22.834,33.985,22.834,33.659z M0.771,36.501c-0.247,0-0.55-0.02-0.75-0.02H0v0.174v0.021h0.092
                            c0.183,0.002,0.351,0.021,0.352,0.269v2.649c-0.001,0.248-0.169,0.264-0.352,0.267H0.021H0v0.021v0.152v0.021h0.021
                            c0.2,0,0.498-0.021,0.744-0.021c0.253,0,0.555,0.021,0.799,0.021h0.023v-0.174V39.86H1.494c-0.184-0.003-0.353-0.018-0.353-0.266
                            v-2.65c0-0.248,0.169-0.266,0.353-0.268h0.07h0.023v-0.172v-0.023H1.564C1.325,36.482,1.022,36.501,0.771,36.501z M1.563,36.657
                            h-0.07c-0.182,0-0.375,0.023-0.375,0.289v2.648c0,0.266,0.193,0.289,0.375,0.289h0.07v0.152c-0.243,0-0.546-0.02-0.799-0.02
                            c-0.248,0-0.546,0.02-0.744,0.02v-0.152h0.071c0.182,0,0.374-0.022,0.374-0.289v-2.648c0-0.266-0.192-0.289-0.374-0.289H0.02
                            v-0.15c0.198,0,0.501,0.02,0.75,0.02c0.252,0,0.555-0.02,0.793-0.02V36.657z M5.205,36.547c0.201,0,0.401-0.02,0.6-0.021v0.107
                            H5.753c-0.219,0-0.473,0.059-0.473,0.664V39.6c0,0.168,0.004,0.334,0.026,0.488H5.139L2.513,37.15v2.142
                            c0,0.44,0.099,0.613,0.498,0.616h0.063v0.106c-0.209-0.002-0.419-0.021-0.63-0.021c-0.222,0-0.448,0.021-0.666,0.021v-0.108
                            l0.048,0.002c0.364-0.002,0.476-0.263,0.476-0.665v-2.132c0-0.297-0.243-0.478-0.481-0.478H1.777l0.001-0.107
                            c0.184,0.002,0.374,0.021,0.558,0.021c0.154,0,0.301-0.021,0.451-0.021l2.25,2.544l0.017-0.017l0.01-0.01l0.008-1.912
                            c0-0.43-0.308-0.5-0.462-0.5H4.52v-0.106C4.748,36.528,4.973,36.547,5.205,36.547z M0.771,36.547c0.245,0,0.538-0.021,0.771-0.021
                            v0.107H1.493c-0.181,0-0.397,0.031-0.397,0.312v2.648c0,0.281,0.216,0.312,0.397,0.312h0.049v0.107
                            c-0.239-0.002-0.531-0.021-0.778-0.021c-0.239,0-0.525,0.021-0.721,0.021v-0.107h0.049c0.18,0,0.396-0.03,0.396-0.312v-2.648
                            c0-0.28-0.216-0.312-0.396-0.312H0.043v-0.107C0.239,36.528,0.53,36.547,0.771,36.547z M17.024,31.912l0.084-0.058
                            c0.008-0.055,0.008-0.11,0.008-0.168c-0.008-0.984-0.818-1.459-1.571-1.459c-0.65,0-1.878,0.539-1.878,2.361
                            c0,0.594,0.298,2.027,1.776,2.027c0.763,0,1.291-0.484,1.719-1.051l-0.13-0.129c-0.342,0.342-0.743,0.621-1.255,0.621
                            c-0.742,0-1.311-0.724-1.365-1.607c-0.021-0.314-0.021-0.455,0-0.539L17.024,31.912L17.024,31.912z M15.508,30.527
                            c0.521,0,0.81,0.381,0.81,0.854c0,0.11-0.027,0.232-0.26,0.232H14.44C14.552,30.944,14.942,30.527,15.508,30.527z M2.625,34.475
                            c3.291,0,3.829-2.312,3.829-3.233c0-1.646-1.31-3.254-3.718-3.254c-0.688,0-1.218,0.021-1.609,0.021c-0.363,0-0.733,0-1.096-0.021
                            v0.241c0.418,0.01,0.855-0.047,0.855,0.789H0.885v4.512c-0.026,0.632-0.297,0.648-0.854,0.707v0.238
                            c0.418-0.008,0.828-0.018,1.246-0.018C1.713,34.46,2.151,34.475,2.625,34.475z M1.796,28.323c0.168-0.012,0.344-0.037,0.726-0.037
                            c1.794,0,2.91,1.255,2.91,2.993c0,1.386-0.66,2.899-2.668,2.899c-0.455,0-0.967-0.084-0.967-0.799L1.796,28.323L1.796,28.323z
                             M7.32,28.333c0-0.232,0.222-0.447,0.454-0.447c0.242,0,0.457,0.195,0.457,0.447c0,0.25-0.205,0.467-0.457,0.467
                            C7.533,28.799,7.32,28.575,7.32,28.333z M21.503,20.531V8.778c2.362,0.906,4.04,3.194,4.043,5.876
                            C25.542,17.338,23.865,19.622,21.503,20.531z M32.932,33.928V28.75c0-0.605-0.14-0.623-0.494-0.726v-0.148
                            c0.373-0.118,0.762-0.286,0.957-0.399c0.102-0.055,0.176-0.104,0.205-0.104c0.059,0,0.075,0.057,0.075,0.133v6.422
                            c0,0.31,0.205,0.31,0.466,0.31h0.155v0.239c-0.313,0-0.64-0.019-0.976-0.019c-0.334,0-0.668,0.011-1.014,0.019v-0.24h0.178
                            C32.746,34.237,32.932,34.237,32.932,33.928z M7.793,34.46c-0.334,0-0.669,0.009-1.013,0.019v-0.24h0.177
                            c0.259,0,0.447,0,0.447-0.309V31.41c0-0.409-0.141-0.466-0.484-0.649v-0.148c0.437-0.133,0.957-0.307,0.993-0.335
                            c0.066-0.037,0.122-0.046,0.169-0.046c0.046,0,0.065,0.057,0.065,0.131v3.568c0,0.309,0.205,0.309,0.465,0.309H8.77v0.24
                            C8.453,34.475,8.128,34.46,7.793,34.46z M39.382,28.75v3.998c0,0.567-0.047,1.105-0.131,1.656l0.194,0.102l0.279-0.215
                            c0.24,0.104,0.604,0.327,1.291,0.327c1.332,0,2.223-1.219,2.223-2.437c0-1.014-0.649-1.953-1.729-1.953
                            c-0.51,0-1.012,0.354-1.383,0.669v-3.394c0-0.076-0.021-0.132-0.076-0.132c-0.028,0-0.103,0.046-0.204,0.103
                            c-0.195,0.113-0.585,0.281-0.959,0.4v0.149C39.242,28.129,39.382,28.145,39.382,28.75z M40.126,31.169
                            c0.25-0.216,0.539-0.438,0.883-0.438c0.726,0,1.486,0.838,1.486,1.841c0,0.827-0.416,1.805-1.412,1.805
                            c-0.623,0-0.957-0.596-0.957-0.948V31.169z M12.397,39.221l0.1,0.024c-0.062,0.257-0.12,0.513-0.163,0.767
                            c-0.326,0-0.897-0.02-1.344-0.02c-0.444,0-1.028,0.02-1.323,0.02v-0.104h0.049c0.18-0.002,0.396-0.033,0.396-0.312v-2.65
                            c0-0.28-0.216-0.312-0.396-0.312H9.667v-0.106c0.311,0.002,0.821,0.021,1.238,0.021c0.415,0,0.928-0.02,1.271-0.021
                            c-0.004,0.071-0.005,0.153-0.005,0.239c0,0.168,0.005,0.354,0.015,0.496l-0.109,0.027c-0.022-0.318-0.111-0.588-0.637-0.585
                            h-0.724v1.37h0.623c0.309,0.003,0.391-0.184,0.419-0.445h0.109c-0.01,0.191-0.015,0.381-0.015,0.57
                            c0,0.186,0.005,0.373,0.015,0.559l-0.109,0.024c-0.025-0.289-0.062-0.502-0.414-0.498h-0.628v1.2c0,0.352,0.319,0.352,0.638,0.352
                            C11.952,39.84,12.246,39.789,12.397,39.221z M35.6,33.036c0,0.278,0,1.079,0.809,1.079c0.314,0,0.734-0.242,1.125-0.563v-2.502
                            c0-0.188-0.447-0.289-0.781-0.383v-0.168c0.836-0.056,1.357-0.129,1.449-0.129c0.074,0,0.074,0.062,0.074,0.167v3.433
                            c0,0.168,0.104,0.176,0.26,0.176c0.113,0,0.252-0.008,0.375-0.008v0.193c-0.402,0.037-1.164,0.233-1.342,0.289l-0.047-0.027
                            v-0.754c-0.558,0.452-0.984,0.779-1.646,0.779c-0.502,0-1.022-0.327-1.022-1.105v-2.379c0-0.244-0.037-0.477-0.558-0.521v-0.178
                            c0.334-0.008,1.078-0.063,1.199-0.063c0.104,0,0.104,0.063,0.104,0.269V33.036z M9.38,36.373l0.079,0.013
                            c-0.019,0.131-0.038,0.26-0.053,0.391c-0.009,0.133-0.01,0.266-0.01,0.396l-0.108,0.041c-0.012-0.188-0.055-0.478-0.396-0.479
                            H8.04v2.714c0,0.396,0.199,0.458,0.443,0.459h0.074v0.104c-0.201,0-0.538-0.019-0.802-0.019c-0.292,0-0.634,0.019-0.834,0.019
                            v-0.104h0.076c0.277-0.001,0.441-0.042,0.441-0.449v-2.725H6.586c-0.376-0.006-0.421,0.109-0.487,0.461H5.99
                            c0.02-0.135,0.043-0.268,0.057-0.405c0.02-0.134,0.029-0.271,0.031-0.409h0.083c0.045,0.141,0.176,0.145,0.295,0.145H6.48h2.609
                            C9.216,36.528,9.358,36.52,9.38,36.373z M19.227,25.523h5.104c5.931,0.028,11.344-4.837,11.344-10.755
                            c0-6.473-5.414-10.946-11.344-10.943h-5.104C13.226,3.822,8.285,8.297,8.285,14.768C8.285,20.688,13.225,25.553,19.227,25.523z
                             M19.251,4.721c5.485,0.001,9.931,4.448,9.931,9.934c0,5.485-4.445,9.931-9.931,9.934c-5.484-0.003-9.929-4.449-9.93-9.934
                            C9.322,9.168,13.767,4.722,19.251,4.721z M5.205,36.501c-0.236,0-0.47-0.02-0.708-0.02H4.475v0.174v0.021h0.134
                            c0.146,0.004,0.416,0.053,0.418,0.455v1.86L2.804,36.49C2.633,36.482,2.487,36.5,2.336,36.5c-0.189,0-0.387-0.019-0.581-0.019
                            H1.733v0.173v0.021h0.088c0.217,0.002,0.438,0.164,0.438,0.435v2.131c-0.003,0.401-0.099,0.617-0.433,0.619H1.755H1.733v0.174
                            v0.021h0.022c0.229,0,0.462-0.02,0.688-0.02c0.216,0,0.434,0.02,0.652,0.02h0.023v-0.172v-0.021H3.009
                            c-0.381-0.005-0.447-0.13-0.454-0.569v-2.026l2.567,2.86l0.209,0.009h0.025l-0.004-0.027c-0.023-0.152-0.029-0.328-0.029-0.506
                            v-2.301c0.005-0.599,0.219-0.615,0.429-0.621h0.075h0.021v-0.174v-0.021H5.827C5.618,36.483,5.411,36.501,5.205,36.501z
                             M5.828,36.657H5.753c-0.213,0-0.451,0.041-0.451,0.645v2.301c0,0.178,0.004,0.354,0.03,0.511H5.139l-2.605-2.904v2.085
                            c0,0.441,0.086,0.592,0.476,0.592h0.085v0.152c-0.217,0-0.434-0.02-0.651-0.02c-0.227,0-0.46,0.02-0.688,0.02v-0.152h0.07
                            c0.349,0,0.457-0.237,0.457-0.641v-2.132c0-0.283-0.234-0.455-0.461-0.455H1.757v-0.151c0.192,0,0.388,0.021,0.582,0.021
                            c0.151,0,0.298-0.021,0.451-0.021l2.251,2.534l0.009-0.01v-1.896c0-0.416-0.288-0.478-0.439-0.478H4.499v-0.151
                            c0.238,0,0.47,0.021,0.708,0.021c0.208,0,0.415-0.021,0.623-0.021v0.15H5.828z M12.958,14.654
                            c0.005-2.681,1.678-4.967,4.039-5.876v11.75C14.635,19.62,12.962,17.335,12.958,14.654z"/>
                    </g>
                </g>
              </svg>
              <?= $this->Html->image(
                                WEBROOT_URL.'webroot/images/hipercard-cartao.png',
                                ['id' => 'hipercard','class' => 'logo-cartao hipercard']) ?>
              <?= $this->Html->image(
                                WEBROOT_URL.'webroot/images/elo-cartao.png',
                                ['id' => 'elo','class' => 'logo-cartao elo']) ?>
              <?= $this->Html->image(
                                WEBROOT_URL.'webroot/images/hiper-cartao.png',
                                ['id' => 'hiper','class' => 'logo-cartao hiper']) ?>
            </div>
            <div class="ccv">
              <label>CCV</label>
              <div class="ccv-number"></div>
            </div>
          </div>
        </div>
      </div>
      <i class="fa fa-close fechar-overlay"></i>



      <?php if($plano->type == 1) { ?>
          <?= $this->Form->create(null, ['id' => 'payment-form', 'class' => 'form payment-form-plano', 'autocomplete' => 'off', 'novalidate', 'url' => ['controller' => 'Academias', 'action' => 'pagar_plano']]) ?>
            <fieldset class="numero-cartao" data-recording-ignore="mask">
                <label for="card-number">Número do cartão</label>
                <input type="number" data-iugu="number" maxlength="16" class="form-control input-cart-number validate[required]" autocomplete="off" >
                <input type="hidden" name="iugu_brand" id="iugu_brand" autocomplete="off">
            </fieldset>
            <fieldset class="nome-cartao" data-recording-ignore="mask">
              <label for="card-holder">Nome gravado no cartão</label>
              <input name="portador_name" data-iugu="full_name" id="card-holder" type="text" class="validate[required]" autocomplete="off"/>
            </fieldset>
            <fieldset class="fieldset-expiration" data-recording-ignore="mask">
              <label for="card-expiration-month">Validade</label>
              <div class="select">
                <select id="card-expiration-month" autocomplete="off">
                  <option></option>
                  <?php for($m = 1; $m <= 12; $m++): ?>
                    <option value="<?= str_pad($m, 2, 0, STR_PAD_LEFT) ?>"><?= str_pad($m, 2, 0, STR_PAD_LEFT) ?></option>
                  <?php endfor; ?>
                </select>
              </div>
              <div class="select" data-recording-ignore="mask">
                <select id="card-expiration-year" autocomplete="off">
                  <option></option>
                  <?php for($m = 0; $m < 30; $m++): ?>
                    <option value="<?= $m + date('y') ?>"><?= $m + date('Y') ?></option>
                  <?php endfor; ?>
                </select>
              </div>
              <input type="hidden" data-iugu="expiration" id="card-expiration-hidden" class="validate[required]" autocomplete="off">
            </fieldset>
            <fieldset class="fieldset-ccv" data-recording-ignore="mask">
              <label for="card-ccv">CCV</label>
              <input id="card-ccv" data-iugu="verification_value" type="text" id="card-ccv" maxlength="4" autocomplete="off"/>
            </fieldset>
            <fieldset class="niver-cartao">
              <label for="card-nascimento">Data de nascimento</label>
              <input name="portador_birth" type="text" id="card-nascimento" class="br_date birth-mask validate[required, custom[brDate]]" />
            </fieldset>
            <fieldset class="fieldset-telefone">
                <label for="card-telefone">Telefone</label>
                <input name="portador_phone" type="text" id="card-telefone" class="phone-mask validate[required]" />
            </fieldset>
            <fieldset class="fieldset-cpf">
                <label for="card-cpf">CPF</label>
                <input name="portador_cpf" type="text" id="card-cpf" class="cpf-mask validate[required,custom[cpf]]"/>
            </fieldset>
            <fieldset class="fieldset-parcelas">
                <label for="parcelas">Quantidade de Parcelas</label>
                <select id="parcelas" name="parcelas" class="form-control validate[required]">
                    <option value="1" class="parcela1">1x de R$<?= number_format($plano->valor_total, 2, ',', '.') ?> (À Vista)</option>
                    <?php if($academia_mensalidades->parcelas_card > 1) { ?>
                        <?php if($academia_mensalidades->opt_repasse == 1) { ?>
                            <?php if($academia_mensalidades->parcelas_card >= 2) { ?>
                                <?php $parcel = $plano->valor_total * 0.089; $parcel = ($plano->valor_total + $parcel)/2?>
                                <option value="2">2x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*2, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 3) { ?>
                                <?php $parcel = $plano->valor_total * 0.089; $parcel = ($plano->valor_total + $parcel)/3?>
                                <option value="3">3x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*3, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 4) { ?>
                                <?php $parcel = $plano->valor_total * 0.089; $parcel = ($plano->valor_total + $parcel)/4?>
                                <option value="4">4x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*4, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 5) { ?>
                                <?php $parcel = $plano->valor_total * 0.089; $parcel = ($plano->valor_total + $parcel)/5?>
                                <option value="5">5x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*5, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 6) { ?>
                                <?php $parcel = $plano->valor_total * 0.089; $parcel = ($plano->valor_total + $parcel)/6?>
                                <option value="6">6x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*6, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 7) { ?>
                                <?php $parcel = $plano->valor_total * 0.089; $parcel = ($plano->valor_total + $parcel)/7?>
                                <option value="7">7x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*7, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 8) { ?>
                                <?php $parcel = $plano->valor_total * 0.089; $parcel = ($plano->valor_total + $parcel)/8?>
                                <option value="8">8x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*8, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 9) { ?>
                                <?php $parcel = $plano->valor_total * 0.089; $parcel = ($plano->valor_total + $parcel)/9?>
                                <option value="9">9x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*9, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 10) { ?>
                                <?php $parcel = $plano->valor_total * 0.089; $parcel = ($plano->valor_total + $parcel)/10?>
                                <option value="10">10x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*10, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 11) { ?>
                                <?php $parcel = $plano->valor_total * 0.089; $parcel = ($plano->valor_total + $parcel)/11?>
                                <option value="11">11x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*11, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 12) { ?>
                                <?php $parcel = $plano->valor_total * 0.089; $parcel = ($plano->valor_total + $parcel)/12?>
                                <option value="12">12x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*12, 2, ',', '.') ?></option>
                            <?php } ?>
                        <?php } else { ?>
                            <?php if($academia_mensalidades->parcelas_card >= 2) { ?>
                                <?php $parcel = $plano->valor_total * 0; $parcel = ($plano->valor_total + $parcel)/2?>
                                <option value="2">2x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*2, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 3) { ?>
                                <?php $parcel = $plano->valor_total * 0; $parcel = ($plano->valor_total + $parcel)/3?>
                                <option value="3">3x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*3, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 4) { ?>
                                <?php $parcel = $plano->valor_total * 0; $parcel = ($plano->valor_total + $parcel)/4?>
                                <option value="4">4x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*4, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 5) { ?>
                                <?php $parcel = $plano->valor_total * 0; $parcel = ($plano->valor_total + $parcel)/5?>
                                <option value="5">5x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*5, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 6) { ?>
                                <?php $parcel = $plano->valor_total * 0; $parcel = ($plano->valor_total + $parcel)/6?>
                                <option value="6">6x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*6, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 7) { ?>
                                <?php $parcel = $plano->valor_total * 0; $parcel = ($plano->valor_total + $parcel)/7?>
                                <option value="7">7x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*7, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 8) { ?>
                                <?php $parcel = $plano->valor_total * 0; $parcel = ($plano->valor_total + $parcel)/8?>
                                <option value="8">8x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*8, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 9) { ?>
                                <?php $parcel = $plano->valor_total * 0; $parcel = ($plano->valor_total + $parcel)/9?>
                                <option value="9">9x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*9, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 10) { ?>
                                <?php $parcel = $plano->valor_total * 0; $parcel = ($plano->valor_total + $parcel)/10?>
                                <option value="10">10x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*10, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 11) { ?>
                                <?php $parcel = $plano->valor_total * 0; $parcel = ($plano->valor_total + $parcel)/11?>
                                <option value="11">11x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*11, 2, ',', '.') ?></option>
                            <?php } if($academia_mensalidades->parcelas_card >= 12) { ?>
                                <?php $parcel = $plano->valor_total * 0; $parcel = ($plano->valor_total + $parcel)/12?>
                                <option value="12">12x de R$<?= number_format($parcel, 2, ',', '.').' = R$'.number_format($total = $parcel*12, 2, ',', '.') ?></option>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </select>
                <input type="hidden" name="valor_total" value="<?= $total ?>" readonly="true"/>
            </fieldset>
            <input type="hidden" name="token" id="token" value="" readonly="true" size="64" style="text-align:center" />
            <input type="hidden" name="plano_id" id="plano_id" value="<?= $plano->id ?>" readonly="true"/>
            <div style="text-align: center; width: 100%">
                <button type="submit" class="btn btn-cupom botao-pagar-cartao">
                    <i class="fa fa-credit-card-alt"></i> pagar
                </button>
            </div>

            <h5 class="font-bold" id="cc_message"></h5>
          <?= $this->Form->end() ?>
        <?php } else if($plano->type == 2) { ?>
            <!-- assinatura -->
            <?= $this->Form->create(null, ['id' => 'payment-form', 'class' => 'form payment-form-assinatura', 'autocomplete' => 'off', 'novalidate', 'url' => ['controller' => 'Academias', 'action' => 'pagar_assinatura']]) ?>
            <fieldset class="numero-cartao" data-recording-ignore="mask">
                <label for="card-number">Número do cartão</label>
                <input type="number" data-iugu="number" maxlength="16" class="form-control input-cart-number validate[required]" autocomplete="off" >
                <input type="hidden" name="iugu_brand" id="iugu_brand" autocomplete="off">
            </fieldset>
            <fieldset class="nome-cartao" data-recording-ignore="mask">
              <label for="card-holder">Nome gravado no cartão</label>
              <input name="portador_name" data-iugu="full_name" id="card-holder" type="text" class="validate[required]" autocomplete="off"/>
            </fieldset>
            <fieldset class="fieldset-expiration" data-recording-ignore="mask">
              <label for="card-expiration-month">Validade</label>
              <div class="select">
                <select id="card-expiration-month" autocomplete="off">
                  <option></option>
                  <?php for($m = 1; $m <= 12; $m++): ?>
                    <option value="<?= str_pad($m, 2, 0, STR_PAD_LEFT) ?>"><?= str_pad($m, 2, 0, STR_PAD_LEFT) ?></option>
                  <?php endfor; ?>
                </select>
              </div>
              <div class="select" data-recording-ignore="mask">
                <select id="card-expiration-year" autocomplete="off">
                  <option></option>
                  <?php for($m = 0; $m < 30; $m++): ?>
                    <option value="<?= $m + date('y') ?>"><?= $m + date('Y') ?></option>
                  <?php endfor; ?>
                </select>
              </div>
              <input type="hidden" data-iugu="expiration" id="card-expiration-hidden" class="validate[required]" autocomplete="off">
            </fieldset>
            <fieldset class="fieldset-ccv" data-recording-ignore="mask">
              <label for="card-ccv">CCV</label>
              <input id="card-ccv" data-iugu="verification_value" type="text" id="card-ccv" maxlength="4" autocomplete="off"/>
            </fieldset>
            <fieldset class="niver-cartao">
              <label for="card-nascimento">Data de nascimento</label>
              <input name="portador_birth" type="text" id="card-nascimento" class="br_date birth-mask validate[required, custom[brDate]]" />
            </fieldset>
            <fieldset class="fieldset-telefone">
                <label for="card-telefone">Telefone</label>
                <input name="portador_phone" type="text" id="card-telefone" class="phone-mask validate[required]" />
            </fieldset>
            <fieldset class="fieldset-cpf">
                <label for="card-cpf">CPF</label>
                <input name="portador_cpf" type="text" id="card-cpf" class="cpf-mask validate[required,custom[cpf]]"/>
            </fieldset>
            <fieldset class="fieldset-parcelas">
                <label for="parcelas">Quantidade de Parcelas</label>
                <select id="parcelas" name="parcelas" class="form-control validate[required]">
                    <option value="1" class="parcela1">1x de R$<?= number_format($total = $plano->valor_total/$plano->time_months, 2, ',', '.') ?> (À Vista)</option>
                </select>
                <input type="hidden" name="valor_total" value="<?= $total ?>" readonly="true"/>
            </fieldset>
            <input type="hidden" name="token" id="token" value="" readonly="true" size="64" style="text-align:center" />
            <input type="hidden" name="plano_id" id="plano_id" value="<?= $plano->id ?>" readonly="true"/>
            <div style="text-align: center; width: 100%">
                <button type="submit" class="btn btn-cupom botao-pagar-cartao">
                    <i class="fa fa-credit-card-alt"></i> pagar
                </button>
            </div>

            <h5 class="font-bold" id="cc_message"></h5>
          <?= $this->Form->end() ?>
        <?php } ?>
    </div>



    <div class="checkout" id="boleto-pagamento" style="margin: 40px auto 30px; padding: 30px 45px 30px;">
        <i class="fa fa-close fechar-overlay"></i>
        <h4 class="font-bold">Clique no botão abaixo para gerar seu boleto para impressão.</h4>
        <h5 class="font-bold">
            Obs.: Permita que popups deste site sejam sempre abertas para que o boleto seja
            visualizado corretamente.
        </h5>
        <?php if($plano->type == 1) { ?>
            <?= $this->Form->create(null, ['id' => 'payment-boleto', 'url' => ['controller' => 'Academias', 'action' => 'pagar_plano']]) ?>
                <input type="hidden" name="valor_total" value="<?= $plano->valor_total ?>" readonly="true"/>
                <input type="hidden" name="plano_id" id="plano_id" value="<?= $plano->id ?>" readonly="true"/>
                <div style="text-align: center; width: 100%">
                    <button type="submit" class="btn btn-cupom botao-gerar-boleto" style="border-color: #8DC73F; background-color: #8DC73F; color: white;">
                        <i class="fa fa-barcode"></i>
                        Gerar Boleto
                    </button>
                    <a href="" target="_blank" class="btn btn-cupom link-gerar-boleto" style="border-color: #8DC73F; background-color: #8DC73F; color: white; display: none">
                        <i class="fa fa-barcode"></i>
                        Visualizar Boleto
                    </a>
                </div>
            <?= $this->Form->end() ?>
        <?php } else if($plano->type == 2) { ?>
            <!-- assinatura -->
            <?= $this->Form->create(null, ['id' => 'payment-boleto', 'url' => ['controller' => 'Academias', 'action' => 'pagar_assinatura']]) ?>
                <input type="hidden" name="valor_total" value="<?= $plano->valor_total ?>" readonly="true"/>
                <input type="hidden" name="plano_id" id="plano_id" value="<?= $plano->id ?>" readonly="true"/>
                <div style="text-align: center; width: 100%">
                    <button type="submit" class="btn btn-cupom botao-gerar-boleto" style="border-color: #8DC73F; background-color: #8DC73F; color: white;">
                        <i class="fa fa-barcode"></i>
                        Gerar Boleto
                    </button>
                    <a href="" target="_blank" class="btn btn-cupom link-gerar-boleto" style="border-color: #8DC73F; background-color: #8DC73F; color: white; display: none">
                        <i class="fa fa-barcode"></i>
                        Visualizar Boleto
                    </a>
                </div>
            <?= $this->Form->end() ?>
        <?php } ?>
    </div>
</div>

<script type="text/javascript">
    var IUGU_ID = <?= IUGU_ID  ?>;
    Iugu.setAccountID(IUGU_ID);
    //Iugu.setTestMode(true);

    jQuery(function($) {
      $('#payment-boleto').submit(function() {
        var form = $(this);
        $('.botao-gerar-boleto').attr('disabled', 'disabled');
        $('.botao-gerar-boleto').html('Gerando boleto...');

        var form_data = form.serialize();

        $.ajax({
          type: "POST",
          url: WEBROOT_URL + 'perfil/pagar_plano/',
          data: form_data
          
        }).done(function (data) {
          $('.botao-gerar-boleto').fadeOut(400, function() {
            $('.link-gerar-boleto').attr('href', data);
            $('.link-gerar-boleto').fadeIn();
          });
          $('.fechar-overlay').click(function() {
            location.href = WEBROOT_URL + 'perfil/';
          });
        });

        return false;
      });   
      $('.payment-form-plano').submit(function(evt) {
          $('.botao-pagar-cartao').attr('disabled', 'disabled');
          $('.botao-pagar-cartao').html('Efetuando pagamento...');
          var form = $(this);
          var tokenResponseHandler = function(data) {
              
              if (data.errors) {
                  console.log(data.errors);
                  alert("Erro salvando cartão: " + JSON.stringify(data.errors));
                  $('.botao-pagar-cartao').removeAttr('disabled');
                  $('.botao-pagar-cartao').html('<i class="fa fa-credit-card-alt"></i>  pagar');
              } else {
                  $("#token").val(data.id);
                  //form.get(0).submit();

                  $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + 'perfil/pagar_plano/',
                    data: form_data
                  }).done(function (data) {
                    $('#payment-form').html(data);
                  });
              }
          }
          
          Iugu.createPaymentToken(this, tokenResponseHandler);
          return false;
      });

      $('.payment-form-assinatura').submit(function(evt) {
          $('.botao-pagar-cartao').attr('disabled', 'disabled');
          $('.botao-pagar-cartao').html('Efetuando pagamento...');
          var form = $(this);
          var tokenResponseHandler = function(data) {
              
              if (data.errors) {
                  console.log(data.errors);
                  alert("Erro salvando cartão: " + JSON.stringify(data.errors));
                  $('.botao-pagar-cartao').removeAttr('disabled');
                  $('.botao-pagar-cartao').html('<i class="fa fa-credit-card-alt"></i>  pagar');
              } else {
                  $("#token").val(data.id);
                  //form.get(0).submit();

                  $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + 'perfil/pagar_assinatura/',
                    data: form_data
                  }).done(function (data) {
                    $('#payment-form').html(data);
                  });
              }
          }
          
          Iugu.createPaymentToken(this, tokenResponseHandler);
          return false;
      });
    });
</script>