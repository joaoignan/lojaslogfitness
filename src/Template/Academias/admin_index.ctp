<?= $this->Html->css('charts/examples') ?>
<?= $this->Html->script('charts/jquery.flot') ?>
<?= $this->Html->script('charts/jquery.flot.categories') ?>

<style>
    .botoes-5{
        margin-top: 15px;
    }
    .body-correct{
        height: 290px;
    }
    .body-correct2 {
        height: 236px;
    }
    .box-correct3{
        height: 326px
    }
    .btn-uma-linha{
        display: none;
    }
    .nome-prod {
        height: 50px;
        float: left;
        width: 75%;
    }
    .nome-prod a{ 
        
    }
    .ajuste-box{
        padding: 5px 10px;
    }
    .btn-dados-bancarios{
        background-color: #5089cf;
        color: #fff;
    }
    .btn-dados-bancarios:hover{
        background-color: #6d97ca;
        color: #fff;
    }
    .fotos-professores{
        margin-top: 40px;
    }
    .ver-loja{
        cursor: pointer;
    }
    li.promo.active{
        border-top-color: orange!important;
    }
    .desc-prods{
        height: 50px;
        margin-left: 50px;
        display: flex;
    }
    .desc-prods a{
        padding-right: 5px;
    }
    .btn-financeiro{
        font-size: 13px;
        margin-bottom: 8px;
    }
    .desc-pedidos{
        text-align: center;
    }
    .banner-atual{
        margin-bottom: 16px;
        margin-top: 15px;
        display: flex;
        display: -webkit-flex;
        justify-content: center;
        align-items: center;
        -webkit-justify-content: center;
        -webkit-align-items: center;
    }
    .alinhamento-professores{
        padding-top: 86px;
    }
    .info-pedidos-icone{
        text-align: center;
    }
    .icones-tamanho{
        height: 30px
    }
    .info-pedidos{
        vertical-align: middle!important;
    }
    .esquerda{
        text-align: left!important;
    }
    .box-slider{
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .box-produtos{
        height: 100%
    }
    .tabs-produtos{
        padding-bottom: 0;
    }
    .info-box-number{
        font-family: nexa;
    }
    .passo{
        position: absolute;
        background-color: #f3d012;
        border-radius: 50px;
        left: 5px;
        top: 5px;
        color: red;
        width: 30px;
        height: 30px;
        text-align: center;
        font-size: 18px;
        font-family: nexa;
        font-weight: 700;
        border: 3px solid red;
        z-index: 9;
    }
    .box-professores{
        height: 100%
    }
    .box-lista{
        height: 160px;
    }
    .logo-atual{
        margin: 0 0 5px 0;
    }
    .image-menu-grade{
        max-width: 160px;
        max-height: 100px;
        border: 2px solid #dedede;
    }
    .btn-altera-logo{
        font-family: nexa;
        font-size: 16px;
        background-color: #5089cf;
        color: #fff;
    }
    .btn-altera-logo:hover{
        background-color: #6d97ca;
        color: #fff
    }
    .btn-altera-banner{
        font-family: nexa;
        font-size: 16px;
        background-color: #5089cf;
        color: #fff;
    }
    .btn-altera-banner:hover{
        background-color: #6d97ca;
        color: #fff
    }
    .banner1{
        float: left;
        width: 155px;
        margin-left: 7px;
    }
    .banner-mais{
        float: left;
        height: 100px;
        width: 50px;
        background-color: #232121;
    }
    .contador-banners{
        color: white;
        font-family: nexa;
        font-size: 20px;
        font-weight: 700;
        margin-top: 35px;
    }
    ul.tab-prods {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    }

    .botoes-logo-banner {
        display: flex;
        display: -webkit-flex;
        align-items: center;
        -webkit-align-items: center;
        justify-content: center;
        -webkit-justify-content: center;
    }

    .div-btn-banner {
        height: 155px;
        margin-bottom: 16px;
        margin-top: 15px;
    }

    .div-btn-logo {
        height: 80px;
    }

/* Float the list items side by side */
ul.tab-prods li {
    width: 50%
}

/* Style the links inside the list items */
ul.tab-prods li a {
    display: inline-block;
    color: black;
    text-align: center;
    text-decoration: none;
    transition: 0.3s;
    font-size: 17px;
    background-color: #ccc;
}

/* Change background color of links on hover */
ul.tab-prods li a:hover {
    background-color: #ddd;
}

/* Style the tab content */
.tabcontent {
    display: none;
    margin: 0 13px 10px 16px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}

.topright {
 float: right;
 cursor: pointer;
 font-size: 20px;
}
.tabcontent {
    -webkit-animation: fadeEffect 1s;
    animation: fadeEffect 1s; /* Fading effect takes 1 second */
}

@-webkit-keyframes fadeEffect {
    from {opacity: 0;}
    to {opacity: 1;}
}

@keyframes fadeEffect {
    from {opacity: 0;}
    to {opacity: 1;}
}
@media all and (max-width: 1024px){
    .font-pedidos{
        font-size: 12px;
    }
    .nome-prod a{
        font-size: 11px;
    }
    .nome-prod{
        width: 64%;
    }
    .products-list li{
        height: 90px;
    }
    .alinhamento-professores {
    padding-top: 85px;
    }
    .ajuste-alunos{
        margin-top: 40px;
    }
    .alunos-mobile{
        font-size: 12px;
    }
}
@media all and (max-width: 450px){
    .btn-mobile{
        margin: 7px;
    }
    .ajuste-alunos {
    margin-top: 40px;
    }
}

.topright:hover {color: red;}
</style>

<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-6 text-center">
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Pedidos Atualizados</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="box-correct3">
                    <table class="table no-margin text-left font-pedidos">
                      <thead>
                      <tr>
                        <th class="desc-pedidos">Pedido</th>
                        <th class="desc-pedidos">Aluno</th>
                        <th class="desc-pedidos">Status</th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php $ctrl = 0 ?>
                        <?php foreach ($academia_pedidos as $ap) {
                          if($ctrl <= 3) {
                            echo '<tr>';
                            echo '<td class="info-pedidos text-center">'.$this->Html->link($ap->id,
                              '/academia/admin/pedido/'.$ap->id, ['escape' =>false]).'</td>';
                            echo '<td class="info-pedidos text-center">'.$ap->cliente->name.'</td>';

                            if($ap->pedido_status_id == 1 || $ap->pedido_status_id == 2) {
                              echo '<td class="info-pedidos-icone"><svg class="icones-tamanho" fill="#8dc73f" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"><g><polygon points="8.669,48.706 5,62.65 6.565,64.773 10.008,54.727  "/><polygon points="93.422,64.79 95,62.65 91.331,48.706 89.991,54.727  "/><polygon points="41.311,14.947 39.661,15.937 37.462,30.863 37.071,40.502 39.622,40.182 41.47,14.947  "/><polygon points="62.928,40.51 62.538,30.863 60.338,15.937 58.689,14.947 58.529,14.947 60.378,40.188  "/><polygon points="29.01,42.523 21.586,43.458 9.53,47.966 11.045,54.785 5.5,70.967 5.5,88.654 27.14,88.654  "/><polygon points="36.48,55.079 35.119,88.654 64.879,88.654 63.519,55.079  "/><polygon points="52.475,40.195 47.463,40.199 37.03,41.513 36.716,49.244 63.282,49.244 62.969,41.516  "/><polygon points="88.954,54.785 90.47,47.966 78.413,43.458 70.989,42.524 72.859,88.654 94.5,88.654 94.5,70.967  "/><path d="M28.12,89.154h5.978l1.422-35.075h28.96L65.9,89.154h5.979l-2.368-58.421L65.62,15.752l-7.219-4.906h-7.104h-2.595h-7.105   l-7.218,4.906l-3.891,14.981L28.12,89.154z M51.578,13.947h7.388l2.256,1.354l2.312,15.433l0.79,19.511H35.675l0.792-19.511   l2.311-15.433l2.257-1.354h7.386H51.578z"/></g></svg><br/>
                                <span class="small" style="color: #8dc73f">PEDIDO</span>
                              </td>';
                            } else if($ap->pedido_status_id == 3 || $ap->pedido_status_id == 4 || $ap->pedido_status_id == 10) {
                              echo '<td class="info-pedidos-icone">'.$this->Html->image(
                                    WEBROOT_URL.'webroot/images/icone-pedidos-2-verde.png',
                                    ['class' => 'icones-tamanho']).'<br/>
                                        <span class="small" style="color: #8dc73f">PAGO</span>
                                    </td>';
                            } else if($ap->pedido_status_id == 5) {
                              echo $ap->rastreamento ?
                              '<td class="info-pedidos-icone"><a href="http://www.linkcorreios.com.br/'.$ap->rastreamento.'" style="margin: 0; text-align: center">
                                    <i class="fa fa-2x fa-truck icones-tamanho" style="color: #8dc73f"></i>
                                    </a>
                                    <span class="small" style="color: #8dc73f">TRANSPORTE</span>
                                    </td>' :
                                    '<td class="info-pedidos-icone"><i class="fa fa-2x fa-truck icones-tamanho" style="color: #8dc73f"></i><br/>
                                    <span class="small" style="color: #8dc73f">TRANSPORTE</span>
                                    </td>';
                            } else if($ap->pedido_status_id == 6 || $ap->pedido_status_id == 9) {
                              echo '<td class="info-pedidos-icone">'.$this->Html->image(
                                    WEBROOT_URL.'webroot/images/icone-pedidos-4-verde.png',
                                    ['class' => 'icones-tamanho']).'<br/><span class="small" style="color: #8dc73f">ENTREGUE</span>
                              </td>';
                            } else {
                              echo '<td class="info-pedidos-icone"><svg class="icones-tamanho" fill="#dd4b39" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"><g><polygon points="8.669,48.706 5,62.65 6.565,64.773 10.008,54.727  "/><polygon points="93.422,64.79 95,62.65 91.331,48.706 89.991,54.727  "/><polygon points="41.311,14.947 39.661,15.937 37.462,30.863 37.071,40.502 39.622,40.182 41.47,14.947  "/><polygon points="62.928,40.51 62.538,30.863 60.338,15.937 58.689,14.947 58.529,14.947 60.378,40.188  "/><polygon points="29.01,42.523 21.586,43.458 9.53,47.966 11.045,54.785 5.5,70.967 5.5,88.654 27.14,88.654  "/><polygon points="36.48,55.079 35.119,88.654 64.879,88.654 63.519,55.079  "/><polygon points="52.475,40.195 47.463,40.199 37.03,41.513 36.716,49.244 63.282,49.244 62.969,41.516  "/><polygon points="88.954,54.785 90.47,47.966 78.413,43.458 70.989,42.524 72.859,88.654 94.5,88.654 94.5,70.967  "/><path d="M28.12,89.154h5.978l1.422-35.075h28.96L65.9,89.154h5.979l-2.368-58.421L65.62,15.752l-7.219-4.906h-7.104h-2.595h-7.105   l-7.218,4.906l-3.891,14.981L28.12,89.154z M51.578,13.947h7.388l2.256,1.354l2.312,15.433l0.79,19.511H35.675l0.792-19.511   l2.311-15.433l2.257-1.354h7.386H51.578z"/></g></svg><br/>
                                <span class="small" style="color: #dd4b39">CANCELADO</span>
                              </td>';
                            }

                            echo '</tr>';

                            $ctrl++;
                          }
                        } ?>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.table-responsive -->
                </div>
                        <!-- /.box-body -->
                <div class="box-footer clearfix">
                  <a href="javascript:void(0)" class="uppercase">
                  <?= $this->Html->link('Ver todos ('.count($academia_pedidos).')',
                      ['controller' => 'Academias', 'action' => 'admin_pedidos'],
                      ['escape' =>false,
                      'class' => 'btn bg-blue']); ?></a>
                </div>
                        <!-- /.box-footer -->
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-6 text-center">
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Financeiro</h3>
                </div>
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="financeiro_produtos">
                            <div class="box-body ajuste-box">
                                <div class="col-xs-12">
                                    <a href="<?= WEBROOT_URL ?>academia/admin/comissoes/" title="Clique para ir para Comissões">
                                        <div class="info-box">
                                            <span class="info-box-icon bg-blue"><i class="ion ion-social-usd"></i></span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Comissões Abertas</span>
                                                <?php foreach ($academia_comissoes_pendentes as $acp) {
                                                    $valor = 0;

                                                    $percent_comissao = $Academia->taxa_comissao;

                                                    $valor           += $acp->valor;
                                                    $comissao_pedido = getPercentCalc($valor, $percent_comissao);
                                                    $comissao        += $comissao_pedido;
                                                } ?>
                                                <span class="info-box-number">R$ <?= number_format($comissao, 2, ',', '.'); ?></span>
                                            </div><!-- /.info-box-content -->
                                        </div>
                                     </a>
                                    <a href="<?= WEBROOT_URL ?>academia/admin/comissoes/" title="Clique para ir para Comissões">
                                        <div class="info-box">
                                            <span class="info-box-icon bg-yellow"><i class="ion ion-social-usd"></i></span>
                                            <div class="info-box-content">
                                            <?php $ctrl = 0; ?>
                                            <?php foreach ($academia_comissoes_receber as $acr) {
                                                if($acr->aceita == 0) {
                                                    $ctrl = 1;
                                                }
                                            } ?>
                                            <?php if($ctrl == 1) {
                                            echo '<div><i class="fa fa-exclamation-circle text-danger pull-right" aria-hidden="true" title="Há comissões contestadas!"></i></div>';
                                            } ?>
                                            <span class="info-box-text">Comissões a Receber</span>
                                            <span class="info-box-number">R$ <?= number_format($academia_comissoes_receber->sumOf('comissao'), 2, ',', '.'); ?></span>
                                            </div><!-- /.info-box-content -->
                                        </div>
                                    </a>
                                    <a href="<?= WEBROOT_URL ?>academia/admin/comissoes/" title="Clique para ir para Comissões">
                                        <div class="info-box">
                                            <span class="info-box-icon bg-green"><i class="ion ion-social-usd"></i></span>
                                            <div class="info-box-content">
                                            <span class="info-box-text">Comissões Recebidas</span>
                                            <span class="info-box-number">R$ <?= number_format($academia_comissoes_pagas->sumOf('comissao'), 2, ',', '.'); ?></span>
                                            </div><!-- /.info-box-content -->
                                        </div>
                                    </a>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                        <?= $this->Html->link(
                                            '<button class="btn btn-financeiro btn-dados-bancarios botoes-5">
                                                Dados bancários
                                            </button>',
                                            ['controller' => 'Academias', 'action' => 'admin_dados_bancarios'],
                                            ['class' => '', 'escape' =>false]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>                   
                    </div>                        
                </div>                   
            </div>
        </div>
    </div>
</section>