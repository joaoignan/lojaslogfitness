<style>
  .box-body p{
    font-size: 16px;
  }
  .box-body h2{
    color: #5089cf;
  }
  .box-body hr{
    border-top: 2px solid rgba(243, 208, 18, 0.40);
    width: 70%;
  }
  .box-body .fa{
    color: #5089cf;
    margin: 0 0 0 15px;
  }
  .box-vantagens{
    margin: 5px 0;
    border: 2px solid #5089cf;
    border-radius: 5px;
    min-height: 335px;
  }
  .box-vantagens table{
    font-size: 15px;
  }
  .box-vantagens .fa{
    color: #5089cf!important;
    margin: 0px;
  }
  .box-passos{
    padding-top: 20px;
    margin: 5px 0;
    border: 2px solid #5089cf;
    border-radius: 5px;
    min-height: 260px;
  }
  .box-passos .fa{
    color: #5089cf;
  }
  .tabela-antecipe{
    border: 2px solid #5089cf;
    padding: 15px;
  }
  .tabela-antecipe h3, .importante h3{
    color: #5089cf;
  }
  .tabela-antecipe ul{
    list-style-type: none;
    font-size: 16px;
    padding: 0;
    margin-bottom: 0;
  }
  .bandeiras img{
    width: 60%;
    -webkit-filter: grayscale(100%);
    -moz-filter: grayscale(100%);
    -ms-filter: grayscale(100%);
    -o-filter: grayscale(100%);
    filter: grayscale(100%);    
  }
  .funciona h3{
    color: #5089cf;
  }
  .funciona .fa{
    color: #f3d012;
    margin: 0 0 0 15px;
  }
  .passos .fa{
    margin: 0;
  }
  .f1{
    background-color: rgba(80, 137, 207, 0.2);
  }
  .f2{
    background-color: rgba(80, 137, 207, 0.6);
  }
  @media all and (max-width: 768px){
    .bandeiras img{
      width: 93%;
    }
  }
</style>

<section class="content">
  <div class="col-xs-12">
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">LOGMensalidades</h3>
      </div><!-- /.box-header -->
      <div class="box-body text-center">
        <h2>ESQUEÇA A SUA MÁQUINA DE CARTÃO. VOCÊ VAI EVOLUIR</h2>
        <h4>Com a LOGMensalidades sua taxa vai lá pra baixo e sua variedade em formas de pagamento vai pras alturas!</h4>
        <h4><i class="fa fa-check" aria-hidden="true"></i> Sem mensalidades <i class="fa fa-check" aria-hidden="true"></i> Sem contratos <i class="fa fa-check" aria-hidden="true"></i> Só pague pelo que usar <i class="fa fa-check" aria-hidden="true"></i> Quanto mais usa, menos paga</h4>
        <br>
        <div class="row">
          <div class="conteudo">
            <div class="compress col-xs-12 col-sm-6 col-md-4">
              <div class="col-xs-12 box-vantagens">
                <h3>Boleto</h3>
                <p><i class="fa fa-barcode fa-4x" aria-hidden="true"></i></p>
                <table width="100%">
                  <tr class="f1">
                    <td>f 1</td>
                    <td>0 - 100 alunos</td>
                    <td>R$ 2,99</td>
                  </tr>
                  <tr class="f2">
                    <td>f2</td>
                    <td>101 - 200 alunos</td>
                    <td>R$ 2,50</td>
                  </tr>
                  <tr class="f1">
                    <td>f3</td>
                    <td>201 - 500 alunos</td>
                    <td>R$ 2,25</td>
                  </tr>
                  <tr class="f2">
                    <td>f4</td>
                    <td>> 500 alunos</td>
                    <td>R$ 1,99</td>
                  </tr>
                </table>
                <br>
                <p>Receba em:</p>
                <p><strong>2 dias</strong></p>
                <p>Receba uma única vez</p>
              </div>
            </div>
            <div class="compress col-xs-12 col-sm-6 col-md-4">
              <div class="col-xs-12 box-vantagens">
                <h3>Cartão</h3>
                <p><i class="fa fa-credit-card fa-4x" aria-hidden="true"></i></p>
                <table width="100%">
                  <tr class="f1">
                    <td>f 1</td>
                    <td>0 - 100 alunos</td>
                    <td>3,99% + R$0,35</td>
                  </tr>
                  <tr class="f2">
                    <td>f2</td>
                    <td>101 - 200 alunos</td>
                    <td>3,69% + R$0,35</td>
                  </tr>
                  <tr class="f1">
                    <td>f3</td>
                    <td>201 - 500 alunos</td>
                    <td>3,39% + R$0,35</td>
                  </tr>
                  <tr class="f2">
                    <td>f4</td>
                    <td>> 500 alunos</td>
                    <td>2,99% + R$0,35</td>
                  </tr>
                </table>
                <br>
                <p>Receba em:</p>
                <p><strong>30 dias</strong></p>
                <p>Receba uma única vez</p>
              </div>
            </div>
            <div class="compress col-xs-12 col-sm-6 col-md-4">
              <div class="col-xs-12 box-vantagens">
                <h3>Assinatura</h3>
                <p><i class="fa fa-refresh fa-spin fa-4x fa-fw"></i></p>
                <p>O valor da taxa cobrado dos planos de assinatura será referente ao tipo de pagamento selecionado pelo aluno conforme a faixa  da academia</p>
                <p>Renovação automática</p>
              </div>
            </div>  
          </div> <!-- conteudo -->
        </div><!-- row -->
        <hr>
        <div class="row faixas">
          <div class="col-xs-12">
            <h4>Entenda as faixas de taxa</h4>
            <p>Para calcular qual a faixa em que sua academia se enquadra você deve somar os alunos que utilizarão a forma de pagamento boleto + os alunos que utilizarão a forma de pagamento cartão!</p>
            <p>Exemplo: <strong>70 alunos</strong> utilizando boleto <strong>+ 150 alunos</strong> utilizando cartão = <strong>220</strong> alunos (f3)</p>
          </div>
        </div> <!-- faixas -->
        <hr>
        <div class="row text-center funciona">
          <h2>É muito simples</h2>
          <div class="col-xs-12 text-left">
            <div class="row text-center">
              <div class="passos">
                <div class="compress col-xs-12 col-sm-6 col-md-4">
                  <div class="col-xs-12 box-passos">
                    <h3>1º Pagamento</h3>
                    <p><i class="fa fa-usd fa-4x" aria-hidden="true"></i></p>
                    <p>O aluno faz o pagamento direto no perfil da academia ou a academia passa o cartão do aluno aqui no administrativo.</p>
                  </div>
                </div> <!-- compress -->
                <div class="compress col-xs-12 col-sm-6 col-md-4">
                  <div class="col-xs-12 box-passos">
                    <h3>2º Acompanhar</h3>
                    <p><i class="fa fa-calendar-check-o fa-4x" aria-hidden="true"></i></p>
                    <p>No prazo estabelecido o recurso está na sua conta dentro da LOGMensalidades já com taxa do cartão ou boleto descontada.</p>
                  </div>
                </div><!-- compress -->
                <div class="compress col-xs-12 col-sm-6 col-md-4">
                  <div class="col-xs-12 box-passos">
                    <h3>3º Transferência</h3>
                    <p><i class="fa fa-exchange fa-4x" aria-hidden="true"></i></p>
                    <p>Transfira para conta bancária já cadastrada da sua academia pagando só R$2,00 por retirada.</p>
                  </div>
                </div> <!-- compress --> 
              </div> <!-- conteudo -->
            </div><!-- row -->
          </div><!-- col-xs-12 text-left -->
        </div> <!-- row -->
        <hr>
        <div class="row antecipe">
          <div class="col-xs-12">
            <h4>Antecipe seus pagamentos de forma simples! Sem aquelas burocracias de cartão.</h4> 
            <h4>Pague só pelos dias que antecipar. Aqui tudo é transparente e sem letras miúdas.</h4>
            <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 tabela-antecipe">
              <h3>Tabela de antecipação Cartão: 2,50% ao mês</h3>
              <br>
              <div class="col-sm-12 col-md-6 text-left">
                <ul>
                  <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i> 2 vezes: 3,63%</li>
                  <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i> 3 vezes: 4,80%</li>
                  <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i> 4 vezes: 5,95%</li>
                </ul>
              </div>
              <div class="col-sm-12 col-md-6 text-left">
                <ul>
                  <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i> 5 vezes: 7,08%</li>
                  <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i> 6 vezes: 8,20%</li>
                  <li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i> 12 vezes: 14,52%</li>
                </ul>
              </div>
            </div><!-- tabela-antecipe -->
          </div><!-- col-xs-12 -->
        </div><!-- antecipe -->
        <hr>
        <div class="row importante">
          <div class="col-xs-12">
            <h3>Importante</h3>
            <p>Por questões de segurança e legislação nós apenas podemos transferir o dinheiro da sua conta LOGMensalidades para uma conta bancária aberta no mesmo CNPJ ou CPF. Por enquanto o sistema transfere para esses 5 bancos. Caso você não tenha conta aberta em algum desses 5 bancos você não conseguirá utilizar a LOGMensalidades.</p>
          </div>
        </div>
        <div class="row bandeiras">
          <h4><strong>Bancos disponíveis para retirada</strong></h4>
          <?= $this->Html->image('bancos_mensalidade.png',['alt' => 'Bancos aceitos'])?>
        </div><!-- bandeiras -->
        <br>
        <div class="row ativar">
          <?= $this->Form->create(null) ?>
          <div class="col-xs-12" style="margin-top: 20px; margin-bottom: 30px;">
            <div class="col-xs-2 col-xs-offset-4">
              <button type="button" id="btn-opt-cnpj" class="btn btn-opt-cpf btn-opt-cpf-active">CNPJ</button>
            </div>
            <div class="col-xs-2">
              <button type="button" id="btn-opt-cpf" class="btn btn-opt-cpf">CPF</button>
            </div>
          </div>
          <?= $this->Form->input('opt_conta_cpf', [
            'type'  => 'hidden',
            'id'    => 'opt_conta_cpf',
            'value' => 0
          ]) ?>
          <?= $this->Form->button('ATIVAR LOGMENSALIDADES',
            ['class' => 'btn btn-success']) ?>
          <?= $this->Form->end() ?>
        </div>
      </div> <!-- box-body text-center -->
    </div> <!-- box box-info -->
  </div> <!-- col-xs-12 -->
</section>

<div class="seila"></div>

<style type="text/css">
  .btn-opt-cpf {
    font-size: 18px;
    font-weight: 700;
    width: 100%;
    height: 60px;
  }

  .btn-opt-cpf:hover {
    background: #cbcdd0;
  }

  .btn-opt-cpf-active,
  .btn-opt-cpf-active:active,
  .btn-opt-cpf-active:visited,
  .btn-opt-cpf-active:focus {
    color: #fff;
    background-color: #00a65a;
    border-color: #008d4c;
  }

  .btn-opt-cpf-active:hover {
    color: #fff;
    background-color: #008d4c;
    border-color: #398439;
  }
</style>

<script type="text/javascript">
  $(document).ready(function() {
    $('.btn-opt-cpf').click(function() {
      $('.btn-opt-cpf').removeClass('btn-opt-cpf-active');
      $(this).addClass('btn-opt-cpf-active');
      if($(this).attr('id') == 'btn-opt-cnpj') {
        $('#opt_conta_cpf').val(0);
      } else {
        $('#opt_conta_cpf').val(1);
      }
    });
  });
</script>