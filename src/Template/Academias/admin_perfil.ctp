<?= $this->Html->css('categorias')?>

<style>
    .medalha{
        float: left;
        width: 80px;
        padding: 15px;
    }
    .avatar, .quadro{
        height: 400px;
    }
    .avatar a{
        font-size: 11px;
        color: #709ad0;
    }
    .emblema-perfil{
        width: 110px;
    }
    .moldura-um {
        width: 100%;
        height: 300px;
        border: 20px inset #f3d012;
        padding: 10px;
        background: #e1e1e1;
    }
    .painel{
        width:100%;
        height:100%;
        border:4px solid #999;
        background: white
    }
</style>

 <script>
  /* Open */
  function openCategorias() {
    document.getElementById("navCategorias").style.height = "100%";
  }
  /* Close */
  function closeCategorias() {
    document.getElementById("navCategorias").style.height = "0%";
  }
  </script>

<section class="content">

<div id="navCategorias" class="categorias" onclick="closeCategorias()">
  <div class="categorias-content">
    <?= $this->Html->image('entenda_categorias.jpg',
     [  'escape'    =>  'false',
        'alt'       =>  'entenda_categorias',
        'title'     =>  'Entenda as Categorias'
     ]) ?>
    <a href="javascript:void(0)" onclick="closeCategorias()"><button class ="btn btn-success" >Beleza! Entendi</button></a>    
  </div>
</div>
    <div class="row">
        <div class="col-xs-12">
          <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <div class="row"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
                                        '/academia/admin/professores',
                                        ['escape'   =>  false,
                                         'class'    =>  'btn pull-right']) ?>
                    </div>
                    <div class="col-md-6 avatar text-center">
                        <?= $this->Html->image('academias/'.$academia_perfil->image, ['alt' => __('Profile image'), 'alt' => 'Logo Academia', 'class' => 'image-menu']) ?>
                        <p><?= $academia_perfil->name ?></p>
                        <!-- EMBLEMA DO LEVEL DA ACADEMIA -->
                        <?php if($Academia->experiencia < 500) { ?> 
                            <p><?= $this->Html->image('emblema_1.png',['escape' => false, 'title' => 'Patente', 'class' => 'emblema-perfil']) ?></p>
                        <?php } else if($Academia->experiencia >= 500 && $Academia->experiencia < 3000) { ?> 
                            <p><?= $this->Html->image('emblema_2.png',['escape' => false, 'title' => 'Patente', 'class' => 'emblema-perfil']) ?></p>
                        <?php } else if($Academia->experiencia >= 3000 && $Academia->experiencia < 15000) { ?> 
                            <p><?= $this->Html->image('emblema_3.png',['escape' => false, 'title' => 'Patente', 'class' => 'emblema-perfil']) ?></p>
                        <?php } else if($Academia->experiencia >= 15000 && $Academia->experiencia < 60000) { ?> 
                            <p><?= $this->Html->image('emblema_4.png',['escape' => false, 'title' => 'Patente', 'class' => 'emblema-perfil']) ?></p>
                        <?php } ?>




                        <a href="javascript:void(0)" onclick="openCategorias()">Veja a tabela de categorias</a>
                    </div>
                    <div class="col-md-6 quadro text-center">
                         <h3>Quadro de Conquistas</h3>
                         <div class="moldura-um">
                             <div class="painel text-center">
                                 <div class="medalha">
                                     <!-- <?= $this->Html->image('anilhas_xp_16x16.png',['escape' => false, 'title' => 'LogCoins', 'style' => 'vertical-align: sub;']) ?>
                                     <p>nome medalha</p> -->
                                 </div>
                             </div>
                         </div>
                    </div>
                    <div class="col-md-6">
                        <p class="text-center"><i class="fa fa-user" aria-hidden="true"></i> Responsável</p>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <p><b>Nome: </b><?= $academia_perfil->contact ?></p>
                            </li>
                            <li class="list-group-item">
                                <p><b>E-mail: </b><?= $academia_perfil->email ?></p>
                            </li>
                            <li class="list-group-item">
                                <p><b>Telefone: </b><?= $academia_perfil->phone ?></p>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <p class="text-center"><i class="fa fa-map-marker" aria-hidden="true"></i> Localização</p>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <p><b>Endereço: </b><?= $academia_perfil->address ?>, <?= $academia_perfil->number ?></p>
                            </li>
                            <li class="list-group-item">
                                <p><b>Complemento: </b><?= $academia_perfil->complement ?></p>
                            </li>
                            <li class="list-group-item">
                                <p><b>Bairro: </b><?= $academia_perfil->area ?></p>
                            </li>
                            <li class="list-group-item">
                                <p><b>Cidade: </b><?= $academia_perfil->city->name ?></p>
                            </li>
                            <li class="list-group-item">
                                <p><b>Estado: </b><?= $academia_perfil->city->state->name ?></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
