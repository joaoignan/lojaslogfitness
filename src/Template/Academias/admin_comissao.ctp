<?= $this->Html->script('jquery.mask.min.js')?>
<script type="text/javascript">
    $(document).ready(function() {
        $("#cpf").mask("999.999.999-99");
        $('.br_date').mask('00/00/0000');
        $("#form_comissao").validationEngine();
    });
</script>
<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>
<style>
.bootstrap-switch.bootstrap-switch-large{
    min-width:200px !important;
}
    @media all and (max-width: 450px){
    .no-mobile{
      display: none;
    }
  }
  @media all and (max-width: 1023px){
    .no-small{
      display: none;
    }
  }
/* The Overlay (background) */
.overlay {
    height: 0%;
    width: 100%;
    position: fixed;
    z-index: 811;
    top: 0;
    left: 0;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0, 0.9);
    overflow-y: hidden;
    transition: 0.5s;
}

.overlay-content {
    position: relative;
    top: 10%;
    left: 10%;
    width: 80%;
    height: 70%;
    text-align: center;
    background-color: white;
    border-radius: 15px;
    border: 2px solid #5089cf;
    padding: 30px;
    overflow: auto;
}

.overlay-content h2{
  font-family: nexa_boldregular;
  color: #5089cf;
}

.overlay-content h3{
  font-family: nexa_boldregular;
  color: #f5d734;
}

.overlay a {
    padding: 8px;
    text-decoration: none;
    font-size: 36px;
    color: #818181;
    display: block;
    transition: 0.3s;
}

.overlay a:hover, .overlay a:focus {
    color: #f1f1f1;
}

.overlay .closebtn {
    position: absolute;
    top: 20px;
    right: 45px;
    font-size: 60px;
    z-index: 9999
}

@media screen and (max-height: 450px) {
  .overlay {overflow-y: auto;}
  .overlay a {font-size: 20px}
  .overlay .closebtn {
    font-size: 40px;
    top: 15px;
    right: 35px;
  }
  .btn-status-mobile{
    width: 100%;
  }
  .check-mobile{
    position: absolute;
    top: 0;
    right: 0px;
  }
</style>

<script>
  /* Open */
  function openNav() {
    document.getElementById("myNav").style.height = "100%";
  }
  /* Close */
  function closeNav() {
    document.getElementById("myNav").style.height = "0%";
  }
</script>

<?php if(date('m') == 01) {
    $data_mes = 12;
    $data_ano = date('Y') - 1;
} else {
    $data_mes = (date('m') - 1);
    $data_ano = date('Y');
} ?>
<section class="content">
    <div class="col-xs-12">
        <div class="box box-info">
            <h3 class="box-header">Relatório de Comissões até <?= $data_mes.'/'.$data_ano ?></h3>
            <h4 class="box-header">Confira se todas informações neste relatórios estão corretas. Em caso afirmativo, dê o aceite do mesmo, caso contrário envie uma contestação.</h4>
            <h4 class="box-header">Obs.: Somente após o aceite as comissões podem ser pagas.</h4>
            <hr>
            <div class="pedidos index">
                <?= $this->Form->create(null, ['id' => 'form_comissao'])?>
                <div class="no-padding">
                    <h3 class="box-header">Aceite do Relatório</h3>
                    <br class="clear">
                    
                    <p>Tipo de conta: <?= $academia->tipo_conta ?></p>
                    <p>Banco: <?= $academia->banco ?></p>
                    <p>Agência: <?= $academia->agencia ?><?= $academia->agencia_dv != null ? '-'.$academia->agencia_dv : '' ?></p>
                    <p>Conta: <?= $academia->conta ?><?= $academia->conta_dv != null ? '-'.$academia->conta_dv : '' ?></p>
                    <?php if($academia->is_cpf == 0) { ?>
                        <p>CNPJ: <?= $academia->cnpj ?></p>
                        <p>Razão Social: <?= $academia->name ?></p>
                    <?php } else { ?>
                        <p>Nome Titular Conta: <?= $academia->favorecido ?></p>
                        <p>CPF Titular Conta: <?= $academia->cpf_favorecido ?></p>
                    <?php } ?>
                    
                    <br class="clear">
                    <div class="form-group">
                        <div class="col-sm-4 col-md-2 control-label">
                            <?= $this->Form->label('aceite') ?>
                        </div>
                        <div class="col-sm-7">

                            <?= $this->Form->input('aceite', [
                                'label'     => false,
                                'div'       => false,
                                'class'     => 'form-control chosen-select validate[required]',
                                'options'   => [
                                    1 => 'Aceitar o Relatório',
                                    0 => 'Contestar o Relatório'
                                ],
                                'empty'     => 'Selecione uma opção...'

                            ]) ?>


                            <?php /*= $this->Form->checkbox('aceite', [
                                'label'     => false,
                                'div'       => false,
                                'checked'   => true,
                                'class'     => 'form-control validate[optional] input-switch',
                                'data-on-color' => 'info',
                                'data-on-text'  => 'Aceitar',
                                'data-off-text' => 'Contestar',
                                'data-off-color' => 'warning',
                                'data-size'     => 'large',
                            ])*/ ?>
                        </div>
                        <!--<p class="col-md-6">Confirme se este relatório está em conformidade com as vendas relacionadas a sua academia...</p>-->
                    </div>
                    <br class="clear">
                    <br class="clear">
                    <?= $this->Form->input('valor', [
                        'type'          => 'hidden',
                        'value'         => (float)$total,
                    ]) ?>

                    <?= $this->Form->input('meta', [
                        'type'          => 'hidden',
                        'value'         => (float)$meta,
                    ]) ?>

                    <?= $this->Form->input('comissao', [
                        'type'          => 'hidden',
                        'value'         => (float)$comissao,
                    ]) ?>

                    <div class="form-group">
                        <div class="col-sm-4 col-md-2 control-label">
                            <?= $this->Form->label('Observações') ?>
                        </div>
                        <div class="col-sm-7">
                            <div>
                                <?= $this->Form->input('obs', [
                                    'type'          => 'textarea',
                                    'placeholder'   => 'Coloque aqui informações sobre o ACEITE ou CONTESTAÇÃO deste relatório',
                                    'div'           => false,
                                    'label'         => false,
                                    'class'         => 'form-control validate[optional]',
                                ]) ?>
                            </div>
                        </div>
                    </div>

                    <br class="clear">
                    <br class="clear">

                    <div class="form-group text-center">
                        <div class="col-sm-12" style="margin: 15px 0;">
                            <div class="col-sm-12 col-md-8">
                                <?= $this->Form->button('<span>'.__('Enviar Relatório ').'</span>'.
                                    '<i class="glyph-icon icon-arrow-right"></i>',
                                    ['class' => 'btn btn-success', 'escape' => false]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive no-padding" style="width: 100%;">
                    <h3 class="box-header">Pedidos</h3>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center"><?= 'Data' ?></th>
                                <th class="no-small text-center"><?= 'Status' ?></th>
                                <th class="text-center"><?= 'Valor' ?></th>
                                <th class="text-center"><?= 'Comissão' ?></th>
                                <th><?= 'Aluno' ?></th>
                                <th class="text-center"><?= 'Cód.' ?></th>
                                <th class="text-center actions"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total      = 0;
                            $comissao   = 0;
                            foreach ($pedidos as $pedido):
                                $valor              = $pedido->valor;
                                $comissao_pedido    = getPercentCalc($valor, $config['academias_comissao']);
                                $comissao           += $comissao_pedido;
                                ?>
                                <tr class="pedido-linha"
                                    data-pid="<?= $pedido->id ?>"
                                    title=""
                                    data-toggle="modal"
                                    data-target="#pid-<?= $pedido->id ?>">
                                    <td class="text-center"><?= h($pedido->created) ?></td>
                                    <td class="no-small text-center"><?= $pedido->pedido_status->name ?></td>
                                    <td class="text-center">R$ <?= number_format($valor, 2, ',', '.') ?></td>
                                    <td class="text-center">R$ <?= number_format($comissao_pedido, 2, ',','.') ?></td>
                                    <td><?= $pedido->cliente->name ?></td>
                                    <td class="text-center"><?= $this->Number->format($pedido->id) ?></td>
                                    <td class="text-center">
                                        <?= $this->Html->link('<i class="fa fa-search"></i>',
                                                        '/academia/admin/pedido/'.$pedido->id,
                                                        ['escape' => false, 'target' => '_blank', 'class' => 'btn btn-info']) ?>
                                    </td>
                                </tr>
                            <?php
                            $total += $valor;
                            endforeach;
                            ?>

                            <tr class="font-italic font-bold2">
                                <td></td>
                                <td class="text-center">
                                    <strong>TOTAL</strong>
                                </td>
                                <td class="text-center">
                                    R$ <?= number_format($total, 2, ',', '.') ?>
                                </td>
                                <td  class="text-center">
                                    R$ <?= number_format($comissao, 2, ',','.') ?>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="form">
                    <fieldset>
                        <!-- <p class="font-bold box-header">Meta: R$ <?= number_format($meta = $adm_academia->meta, 2, ',', '.') ?></p> -->
                        <p class="font-bold box-header">Vendas: R$ <?= number_format($total, 2, ',', '.') ?></p>
                        <p class="font-bold box-header">Comissão do Mês: R$ <?= $meta < $total ? number_format($comissao, 2, ',','.') : '0,00' ?></p>
                        <br>
                    </fieldset>
                    <?= $this->Form->end()?>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    <?php if($Academia->card_name == null || $Academia->card_cpf == null || $Academia->card_birth == null) { ?>
        $('.enviar-relatorio').click(function() {
            openNav();
            return false;
        });
    <?php } ?>
    $('.closebtn-relatorio').click(function() {
        closeNav();
    });
</script>