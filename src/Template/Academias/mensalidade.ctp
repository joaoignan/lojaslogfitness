<style>
	#btn-mensalidade{
		display: none;
	}
	.trava-baixo{
		position: absolute;
	    width: 93%;
	    bottom: 25px;
	}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$('.plano-personalizado-btn').click(function(e) {
			e.preventDefault();
			var valor = $('.valor-personalizado').val();
			valor = valor.toString().replace('.', '').replace(',', '.');
			var href = $(this).attr('href');
			if(parseFloat(valor) >= 1) {
				location.href = href + valor;
			} else {
				alert('valor deve ser maior que 0');
			}
		});

		$('.money').mask('000000000000000,00', {reverse: true});
	});
</script>

<!-- Smart Look -->
  <script type="text/javascript">
      window.smartlook||(function(d) {
      var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
      var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
      c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
      })(document);
      smartlook('init', '65dd1b31ade411a0f1f09a9524c2adf7c3ca5eb5');
  </script>
  <!-- End Smart Look -->

  <!-- Smartsupp Live Chat script -->
    <script type="text/javascript">
    var _smartsupp = _smartsupp || {};
    _smartsupp.key = '472d82fddbe4326ca5661de701a829a7553ce341';
    window.smartsupp||(function(d) {
        var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
        s=d.getElementsByTagName('script')[0];c=d.createElement('script');
        c.type='text/javascript';c.charset='utf-8';c.async=true;
        c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
    })(document);
    </script>

    <script>
    jQuery(document).ready(function() {
    $(window).scroll(function () {
        set = $(document).scrollTop()+"px";
        jQuery('#float-banner').animate(
            {top:set},
            {duration:1000, queue:false}
        );
    });
    $('#my-link').on('click', function() {
      $('#float-banner').hide();
    })
  });
  </script>

<div class="row">
	<div class="col-xs-12 text-center">
		<h2>Planos ativos para compra</h2>
	</div>
</div>
<div class="row">
	<?php if(($mensalidade->account_verify == 1) || ($mensalidade->account_verify == 3)) { ?>
		<div class="col-xs-12 col-sm-6 col-md-4 text-center">
			<div class="pacote-content">
				<div class="row">
					<div class="col-xs-12">
			        	<h4>Plano Personalizado</h4>
						<p>Tipo: Plano</p>
			    		<p>Informe o valor</p>
			    		<p>R$ <input type="text" placeholder="0,00" class="valor-personalizado money" style="width: 75px;  text-align: right;"></p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
			    		<label>Confirme com sua academia os valores antes de adquirir este plano.</label>
					</div>
				</div>
				<div class="row trava-baixo">
					<div class="col-xs-12">
						<div class="metodos">
						    <p><strong>Métodos de pagamento</strong></p>
						   	<div class="col-xs-6 text-center">
							   	<i class="fa fa-credit-card fa-2x" aria-hidden="true"></i>
							   	<p>crédito</p>
						   	</div>
					   		<div class="col-xs-6 text-center">
						   		<i class="fa fa-barcode fa-2x" aria-hidden="true"></i>
							   	<p>boleto</p>
					   		</div>
					   	</div>
					</div>
					<div class="col-xs-12">
			    		<?= $this->Html->link('Adquirir',
			                $Slug.'/mensalidade/comprar/99522?valor=', 
			                ['class' => 'btn btn-success btn-sm plano-personalizado-btn']
			            )?>
			    	</div>
				</div>
			</div> 
		</div>
		<?php foreach($planos as $plano) { ?>
			<?php $qtd_planos_cliente = 0; ?>
            <?php foreach ($planos_clientes as $plano_cliente) {
                if($plano_cliente->academia_mensalidades_planos_id == $plano->id) {
                    $qtd_planos_cliente++;
                }
            } ?>
            <?php if($qtd_planos_cliente < $plano->limite_vendas) { ?>
				<div class="col-xs-12 col-sm-6 col-md-4 text-center">
					<div class="pacote-content">
						<?php if($plano->type == 2){?>
							<div class="faixa-assinatura text-center">
								<span>assinatura</span>
							</div>
						<?php } ?>
						<div class="row">
						    <div class="col-xs-12">
						        <h4><?= $plano->name ?></h4>
							    <p>Tipo: <?= $plano->type == 1 ? 'Plano' : 'Assinatura' ?></p>
							    <p>Duração: <?= $plano->time_months == 1 ? $plano->time_months.' mês' : $plano->time_months.' meses' ?></p>
							    <p>R$ <?= number_format($valor_mes = $plano->valor_total/$plano->time_months, 2, ',', ' '); ?> / mês</p>
						    </div> 
						</div>
						<div class="row">
							<div class="col-xs-12">
								<label>
							    	<?php if($plano->descricao != null){
			                                echo $plano->descricao;
			                            } else {
			                                echo "&nbsp";
			                                } ?>
		                        </label>
							</div>
						</div>
						<div class="row trava-baixo">
							<div class="col-xs-12">
								<div class="metodos">
							    	<p><strong>Métodos de pagamento</strong></p>
								   	<p>
								   		<?php if($plano->type == 2) { ?>
		                                    <div class="col-xs-12 text-center">
		                                        <i class="fa fa-credit-card fa-2x" aria-hidden="true"></i>
		                                        <p>crédito</p>
		                                    </div>
		                                <?php } else { ?>
		                                    <?php if($plano->parcelas_cartao >= 1 && $plano->parcelas_boleto >= 1) { ?>
		                                        <div class="col-xs-6 text-center">
		                                            <i class="fa fa-credit-card fa-2x" aria-hidden="true"></i>
		                                            <p>crédito</p>
		                                        </div>
		                                        <div class="col-xs-6 text-center">
		                                            <i class="fa fa-barcode fa-2x" aria-hidden="true"></i>
		                                            <p>boleto</p>
		                                        </div>
		                                    <?php } else { ?>
		                                        <?php if($plano->parcelas_cartao >= 1) { ?>
		                                            <div class="col-xs-12 text-center">
		                                                <i class="fa fa-credit-card fa-2x" aria-hidden="true"></i>
		                                                <p>crédito</p>
		                                            </div>
		                                        <?php } ?>
		                                        <?php if($plano->parcelas_boleto >= 1) { ?>
		                                            <div class="col-xs-12 text-center">
		                                                <i class="fa fa-barcode fa-2x" aria-hidden="true"></i>
		                                                <p>boleto</p>
		                                            </div>
		                                        <?php } ?>
		                                    <?php } ?>
		                                <?php } ?>
								   	</p>
							   	</div>
							</div>
							<div class="col-xs-12">
								<?= $this->Html->link('Adquirir',
				                '/mensalidade/comprar/'.$plano->id,[
				                'class' => 'btn btn-success btn-sm',
				            ])?>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		<?php } ?>
	<?php } else { ?>
		<div style="height: 400px; width: 100%;">
			<div class="col-xs-12 text-center academia-ticoliro" style="background-color: #ececec;">
				<div class="academia-ticoliro-content">
					<h1>Sua academia ainda não possui LOGMensalidades</h1>
				</div>
			</div>
		</div>
	<?php } ?>
</div>



