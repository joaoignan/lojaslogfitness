<?php foreach ($act as $aact) {
    $extensao = substr($aact->arquivo, -4);
    if($extensao == '.pdf') {
        $pdf[] = $aact;
    } else {
        $outros[] = $aact;
    }
} ?>

<style type="text/css">
    @media all and (max-width: 450px){
        .oculta {
            display: none;
        }
        table tr{
            height: 120px;
        }
    }
    .imagem-act {
        max-width: 60px;
        max-height: 60px;
    }
    .miniaturas{
        text-align: center;
        vertical-align: middle!important;
    }
    .miniaturas i{
        color: #5089cf;
    }
    .info-arquivo{
        vertical-align: middle!important;
    }
    table th, td{
        text-align: center;
    }
    table tr{
        min-height: 80px;
    }
</style>

<section class="content">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Midias Digitais</h3>
      </div>
    </div>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Imagens</a></li>
            <li><a href="#tab_2" data-toggle="tab">PDFs</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Miniatura</th>
                            <th>Título</th>
                            <th class="oculta">Disponibilizado</th>
                            <th class="actions">Download</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($outros as $images): ?>
                        <?php
                        $extensao = substr($images->arquivo, -4);

                        if($extensao != '.jpg' && $extensao != 'jpeg' && $extensao != '.png') {
                            $status = 1;
                        } else {
                            $status = 0;
                        }
                        ?>

                        <tr>
                            <td class="miniaturas"><?= $status == 1 ?
                                '<i class="fa fa-file-image-o fa-3x" aria-hidden="true"></i>' :
                                $this->Html->link(
                                  $this->Html->image(
                                    '/files'.DS.$images->arquivo,
                                    ['class' => 'imagem-act',
                                    'alt'   => $images->title,
                                    'escape' => false])
                                    ,'files/'.DS.$images->arquivo,
                                  ['class' => 'fancybox', 'escape' => false])
                                ?>

                            </td>
                            <td class="info-arquivo"><?= h($images->title)  ? $this->Html->link($images->title,'/files'.DS.$images->arquivo,['target' => '_blank']) :'' ?></td>
                            <td class="oculta info-arquivo"><?= h($images->created) ?></td>
                            <td class="actions info-arquivo">
                                <?= $this->Html->link('<button class="btn btn-info" type="button" aria-expanded="false">
                                    <i class="fa fa-download" aria-hidden="true"></i></button>',
                                            '/files'.DS.$images->arquivo,
                                            ['target' => '_blank', 'escape' => false])?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="tab_2">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Título</th>
                            <th class="oculta">Disponibilizado</th>
                            <th class="actions">Download</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($pdf as $pdf): ?>
                        <tr>
                            <td class="miniaturas"><i class="fa fa-file-pdf-o fa-3x" aria-hidden="true"></i></td>
                            <td class="info-arquivo"><?= h($pdf->title)  ? $this->Html->link($pdf->title,'/files'.DS.$pdf->arquivo,['target' => '_blank']) : '' ?></td>
                            <td class="oculta info-arquivo"><?= h($pdf->created) ?></td>
                            <td class="actions info-arquivo">
                                <?= $this->Html->link('<button class="btn btn-info" type="button" aria-expanded="false">
                                    <i class="fa fa-download" aria-hidden="true"></i> '.__('Download').'</button>',
                                            '/files'.DS.$pdf->arquivo,
                                            ['target' => '_blank', 'escape' => false])?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    	$(document).ready(function() {
    		$(".fancybox").fancybox();
    	});
    </script>
</section>
