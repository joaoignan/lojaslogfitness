<style>
  .nome-plano{
    width: auto;
    display: inline-block;
    padding-left: 5px;
  }
  .editar-plano a{
    color: black;
  }
  .cancelar-plano .open-overlay-cancelar-todos{
    background: none;
    border: none;
  }
  .cancelar-plano, .editar-plano, .ver-mais-vencidas, .ver-mais-vencer, .ver-mais-ativas{ 
    float: right;
    width: auto;
  }

  .inner-ver-mais-vencidas, .inner-ver-mais-vencer, .inner-ver-mais-ativas {
    height: 53px;
    cursor: pointer;
  }
  .inner-editar-vencidas, .inner-editar-vencer, .inner-editar-ativas {
    padding-top: 7px;
    height: 53px;
    cursor: pointer;
  }
  .ver-mais-vencidas label, .ver-mais-vencer label, .ver-mais-ativas label, .editar-plano label{
    margin-bottom: 1px;
    margin-top: 5px;
  }
  #personalizados table{
    border: 2px solid rgba(80, 137, 207, 1.0);
  }
  #personalizados table th{
    padding: 10px;
    border-bottom: 2px solid rgba(80, 137, 207, 1.0);
    background-color: rgba(80, 137, 207, 0.2);
    border-collapse: collapse;
  }
  #personalizados table td{
    padding: 10px;
    border-bottom: 2px solid rgba(80, 137, 207, 1.0);
    border-collapse: collapse;
  }
  .title-planos{
    min-height: 40px;
  }
  .edit-data{
    width: 100%!important;
    margin: 10px 0!important
  }
  .vencidas{
    background: rgba(255, 0, 0, 0.2);
    border: 1px solid rgba(255, 0, 0, 0.8)
  }
  .inner-ver-mais-vencidas:hover, .inner-editar-vencidas:hover{
    background: rgba(255, 0, 0, 0.6);
  }
  .a_vencer{
    background: rgba(243, 208, 18, 0.2);
    border: 1px solid rgba(243, 208, 18, 0.8)
  }
  .inner-ver-mais-vencer:hover, .inner-editar-vencer:hover{
    background: rgba(243, 208, 18, 0.6);
  }
  .ativas{
    background: rgba( 80, 137, 207, 0.2);
    border: 1px solid rgba(80, 137, 207, 0.8);
  }
  .inner-ver-mais-ativas:hover, .inner-editar-ativas:hover{
    background: rgba( 80, 137, 207, 0.6);
  }
  .div-plano{
    margin: 5px 0;
    min-height: 55px;
    height: auto;
    border-radius: 5px;
  }
  .titulo-plano{
    color: rgba(86, 86, 86, 1.0);
    font-weight: 700;
    font-family: nexa;
    font-size: 30px;
    margin-top: 5px;
    margin-right: 5px;
  }
  .planos-content{
    display: block;
    border-radius: 5px;
    background-color: #FFF;
    margin: 15px 10px;
    padding: 10px;
  }
  .planos-content a{
    color: #565656;
  }
  .planos-content table tr{
    height: 35px;
  }
  .planos-content table th{
    border-bottom: 1px solid black;
  }
  .planos-content i{
    cursor: pointer;
  }
  .conteudo-plano {
    display: none;
  }
  @media all and (max-width: 1024px){
    .tabela-plano{
      overflow-x: scroll;
    }
  }
  @media all and (max-width: 768px){
    .nome-plano{
      width: 100%;
    }
    .editar-plano{
      width: 20%;
    }
    .cancelar-plano{
      width: 35%;
    }
  }
  @media all and (max-width: 450px){
    .div-plano h2{
      font-size: 18px;
    }
    .academia-sobre .phone {
      width: 100%;
      display: inline-block;
    }
    .editar{
      margin-top: 10px;
    }
    .almoco{
    text-align: left!important;
    }
    .titulo-plano{
      font-size: 20px;
      margin-top: 13px;
    }
    .cancelar-plano, .editar-plano, .ver-mais-vencidas, .ver-mais-vencer, .ver-mais-ativas{ 
      float: right;
      height: 70px;
      width: 50%;
    }
  }
</style>

<?php 
  function resume_string($var, $limite){  
    if (strlen($var) > $limite) {       
      $var = substr($var, 0, $limite);        
      $var = trim($var) . "...";  
    }

    return $var;
  }
?>

<section class="content">
  <div class="col-xs-12">
    <div class="box box-info">
      <div class="box-header">
        <h3 class="box-title">Minhas Assinaturas</h3>
        <div class="box-tools">
        </div>
        <hr>
        <div class="col-xs-12 no-padding">
        <?= $this->Html->link('<i class="fa fa-plus"></i> Nova Assinatura/Plano',
        '/academia/admin/admin_mensalidades/planos_add',
        ['class' => 'btn btn-success btn-sm', 'escape' => false])?>
        </div>
      </div>
      <div class="box-body">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#vencidas" data-toggle="tab">Vencidas</a></li>
            <li><a href="#a_vencer" data-toggle="tab">A vencer</a></li>
            <li><a href="#ativas" data-toggle="tab">Ativas</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active " id="vencidas">

              <h2>Ativos</h2>

              <?= count($academia_mensalidades_planos_ativos) <= 0 ? '<h3>Não há assinaturas ativas</h3>' : '' ?>

              <?php foreach ($academia_mensalidades_planos_ativos as $planos_ativos) { ?>

                <div class="div-plano vencidas">
                  <div class="row vencidas-header">
                    <div class="col-xs-12 title-planos">
                      <div class="">
                        <div class="nome-plano">
                          <label class="titulo-plano"><?= resume_string($planos_ativos->name, 40) ?></label>
                        </div>
                        <div class="text-center cancelar-plano">
                          <button class="open-overlay-cancelar-todos" type="button" data-id="<?= $planos_ativos->id ?>"><div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-close"></i><br> cancelar todos</div></button>
                        </div>

                        <script type="text/javascript">
                          $(document).ready(function () {
                            $('.open-overlay-cancelar-todos').click(function () {
                              var id = $(this).attr('data-id');

                              $('.cancelar-todos-'+id).height('100%');
                            });
                          });
                        </script>

                        <div class="overlay cancelar-todos-<?= $planos_ativos->id ?>">
                          <div class="overlay-content cancelar-inner text-center">
                            <a href="javascript:void(0)" class="closebtn">&times;</a>
                            <h2>Cancelar todos</h2>
                            <p>Atenção: Ao realizar esta ação você irá cancelar todas as assinaturas ativas, ou seja, as cobranças futuras desses alunos e iremos remover esta assinatura do seu perfil. Tem certeza que deseja cancelar?</p>
                            <?= $this->Html->link('<button class="btn btn-danger">Sim, cancelar todos</button>',
                              '/academia/admin/admin_mensalidades/assinaturas/cancelar_todas_assinaturas/'.$planos_ativos->id,
                              ['escape' => false, 'class' => 'open-overlay-cancelar-todos', 'style' => 'margin-bottom: 15px;'])?>
                          </div>
                        </div>
                        <div class="text-center editar-plano">
                          <?php if($planos_ativos->visivel == 1) { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-lock"></i><br> inativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos_ativos->id,
                                ['escape' => false])?>
                          <?php } else { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-unlock"></i><br> Ativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos_ativos->id,
                                ['escape' => false])?>
                          <?php } ?>
                        </div>
                        <div class="text-center editar-plano">
                          <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-pencil"></i><br> editar</div>',
                              '/academia/admin/admin_mensalidades/assinaturas_edit/'.$planos_ativos->id,
                              ['escape' => false])?>
                        </div>
                        <div class="text-center ver-mais-vencidas click-header-vencidas" data-id="<?= $planos_ativos->id ?>">
                          <div class="col-xs-12 inner-ver-mais-vencidas">
                            <label><i class="fa fa-users"> <?= $qtd_alunos_vencidos[$planos_ativos->id] >= 1 ? $qtd_alunos_vencidos[$planos_ativos->id] : '0' ?></i></label>
                            <p>alunos</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="conteudo-plano conteudo-plano-vencidas-<?= $planos_ativos->id ?>">
                    <div class="planos-content">
                      <div class="">
                        <div class="col-sm-12 col-md-10">
                          <p><?= $planos_ativos->descricao ?></p>
                        </div>
                      </div>
                      <hr>
                      <div class="tabela-plano">
                        <table style="width: 650px;" cellpadding="10">
                          <tr>
                            <th><strong>Aluno</strong></th>
                            <th class="text-center"><strong>Vencimento</strong></th>
                            <th class="text-center"><strong>Duração mínima</strong></th>
                            <th class="text-center"><strong>Enviar e-mail</strong></th>
                            <th class="text-right"><strong>Cancelar assinatura</strong></th>
                          </tr>
                          <?php foreach ($planos_clientes_vencidos as $plano_cliente_vencido) { ?>
                            <?php if($plano_cliente_vencido->academia_mensalidades_planos_id == $planos_ativos->id) { ?>
                              <?php $tempo_minimo = $plano_cliente_vencido->created->addMonth($planos_ativos->time_months) ?>
                              <tr>
                                <td><?= $plano_cliente_vencido->cliente->name ?></td>
                                <td class="text-center"><?= $plano_cliente_vencido->data_vencimento->format('d/m/Y') ?> <i class="fa fa-pencil open_vencimento_overlay" data-id="<?= $plano_cliente_vencido->id ?>"></i></td>
                                <td class="text-center">
                                  <?= $tempo_minimo->format('d/m/Y') ?>
                                </td>
                                <td class="text-center">
                                  <?= $this->Html->link('enviar e-mail', 
                                  '/academia/admin/admin_mensalidades/planos/cobrar_vencido/'.$plano_cliente_vencido->id,
                                  ['class' => 'btn bg-blue btn-sm']) ?>
                                <td class="text-right"><?= $this->Html->link('Cancelar assinatura', '/academia/admin/admin_mensalidades/planos/cancelar_plano/'.$plano_cliente_vencido->id, ['escape' => false, 'class' => 'btn btn-lg']) ?></td>
                                </td>
                              </tr>       
                            <?php } ?>
                          <?php } ?>                             
                        </table>
                      </div>

                        <?php foreach ($planos_clientes_vencidos as $plano_cliente_vencido) { ?>
                          <?php if($plano_cliente_vencido->academia_mensalidades_planos_id == $planos_ativos->id) { ?>
                            <div class="overlay vencimento-<?= $plano_cliente_vencido->id ?>">
                              <div class="overlay-content edit-plano">
                                <a href="javascript:void(0)" class="closebtn">&times;</a>
                                  <h3>Editar data de vencimento</h3>
                                  <?= $this->Form->create(null, ['url' => ['controller' => 'Academias', 'action' => 'admin_mensalidades_planos_editar_vencimento']]) ?>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <label for="name" class="form-label">Aluno</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_vencido->cliente->name ?>" disabled="true" class="form-control edit-data">
                                      <input type="hidden" name="plano_id" value="<?= $plano_cliente_vencido->id ?>">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                      <label for="name" class="form-label">Data Atual</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_vencido->data_vencimento->format('d/m/Y') ?>" disabled="true" class="form-control edit-data">
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                       <label for="name" class="form-label">Nova data</label> 
                                      <input type="date" min="2015-01-02" max="2999-12-31" name="data_vencimento" placeholder="30/12/2017" required class="form-control date edit-data">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12 text-center">
                                      <button type="submit" class="btn btn-success">Salvar</button>
                                    </div>
                                  </div>
                                  <?= $this->Form->end() ?>
                              </div>
                            </div>
                          <?php } ?>
                        <?php } ?>
                    </div>
                  </div>
                </div>

              <?php } ?>

              <h2>Inativos</h2>

              <?= count($academia_mensalidades_planos_inativos) <= 0 ? '<h3>Não há assinaturas inativas</h3>' : '' ?>
              
              <?php foreach ($academia_mensalidades_planos_inativos as $planos_inativos) { ?>

                <div class="div-plano vencidas">
                  <div class="row vencidas-header">
                    <div class="col-xs-12 title-planos">
                      <div class="">
                        <div class="nome-plano">
                          <label class="titulo-plano"><?= resume_string($planos_inativos->name, 40) ?></label>
                        </div>
                        <div class="text-center cancelar-plano">
                          <button class="open-overlay-cancelar-todos" type="button" data-id="<?= $planos_inativos->id ?>"><div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-close"></i><br> cancelar todos</div></button>
                        </div>

                        <script type="text/javascript">
                          $(document).ready(function () {
                            $('.open-overlay-cancelar-todos').click(function () {
                              var id = $(this).attr('data-id');

                              $('.cancelar-todos-'+id).height('100%');
                            });
                          });
                        </script>

                        <div class="overlay cancelar-todos-<?= $planos_inativos->id ?>">
                          <div class="overlay-content cancelar-inner text-center">
                            <a href="javascript:void(0)" class="closebtn">&times;</a>
                            <h2>Cancelar todos</h2>
                            <p>Atenção: Ao realizar esta ação você irá cancelar todas as assinaturas ativas, ou seja, as cobranças futuras desses alunos e iremos remover esta assinatura do seu perfil. Tem certeza que deseja cancelar?</p>
                            <?= $this->Html->link('<button class="btn btn-danger">Sim, cancelar todos</button>',
                              '/academia/admin/admin_mensalidades/assinaturas/cancelar_todas_assinaturas/'.$planos_inativos->id,
                              ['escape' => false, 'class' => 'open-overlay-cancelar-todos', 'style' => 'margin-bottom: 15px;'])?>
                          </div>
                        </div>
                        <div class="text-center editar-plano">
                          <?php if($planos_inativos->visivel == 1) { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-lock"></i><br> inativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos_inativos->id,
                                ['escape' => false])?>
                          <?php } else { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-unlock"></i><br> Ativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos_inativos->id,
                                ['escape' => false])?>
                          <?php } ?>
                        </div>
                        <div class="text-center editar-plano">
                          <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencidas"><i class="fa fa-pencil"></i><br> editar</div>',
                              '/academia/admin/admin_mensalidades/assinaturas_edit/'.$planos_inativos->id,
                              ['escape' => false])?>
                        </div>
                        <div class="text-center ver-mais-vencidas click-header-vencidas" data-id="<?= $planos_inativos->id ?>">
                          <div class="col-xs-12 inner-ver-mais-vencidas">
                            <label><i class="fa fa-users"> <?= $qtd_alunos_vencidos[$planos_inativos->id] >= 1 ? $qtd_alunos_vencidos[$planos_inativos->id] : '0' ?></i></label>
                            <p>alunos</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="conteudo-plano conteudo-plano-vencidas-<?= $planos_inativos->id ?>">
                    <div class="planos-content">
                      <div class="">
                        <div class="col-sm-12 col-md-10">
                          <p><?= $planos_inativos->descricao ?></p>
                        </div>
                      </div>
                      <hr>
                      <div class="tabela-plano">
                        <table style="width: 650px;" cellpadding="10">
                          <tr>
                            <th><strong>Aluno</strong></th>
                            <th class="text-center"><strong>Vencimento</strong></th>
                            <th class="text-center"><strong>Duração mínima</strong></th>
                            <th class="text-center"><strong>Enviar e-mail</strong></th>
                            <th class="text-right"><strong>Cancelar assinatura</strong></th>
                          </tr>
                          <?php foreach ($planos_clientes_vencidos as $plano_cliente_vencido) { ?>
                            <?php if($plano_cliente_vencido->academia_mensalidades_planos_id == $planos_inativos->id) { ?>
                              <?php $tempo_minimo = $plano_cliente_vencido->created->addMonth($planos_inativos->time_months) ?>
                              <tr>
                                <td><?= $plano_cliente_vencido->cliente->name ?></td>
                                <td class="text-center"><?= $plano_cliente_vencido->data_vencimento->format('d/m/Y') ?> <i class="fa fa-pencil open_vencimento_overlay" data-id="<?= $plano_cliente_vencido->id ?>"></i></td>
                                <td class="text-center">
                                  <?= $tempo_minimo->format('d/m/Y') ?>
                                </td>
                                <td class="text-center">
                                  <?= $this->Html->link('enviar e-mail', 
                                  '/academia/admin/admin_mensalidades/planos/cobrar_vencido/'.$plano_cliente_vencido->id,
                                  ['class' => 'btn bg-blue btn-sm']) ?>
                                <td class="text-right"><?= $this->Html->link('Cancelar assinatura', '/academia/admin/admin_mensalidades/planos/cancelar_plano/'.$plano_cliente_vencido->id, ['escape' => false, 'class' => 'btn btn-lg']) ?></td>
                                </td>
                              </tr>       
                            <?php } ?>
                          <?php } ?>                             
                        </table>
                      </div>

                        <?php foreach ($planos_clientes_vencidos as $plano_cliente_vencido) { ?>
                          <?php if($plano_cliente_vencido->academia_mensalidades_planos_id == $planos_inativos->id) { ?>
                            <div class="overlay vencimento-<?= $plano_cliente_vencido->id ?>">
                              <div class="overlay-content edit-plano">
                                <a href="javascript:void(0)" class="closebtn">&times;</a>
                                  <h3>Editar data de vencimento</h3>
                                  <?= $this->Form->create(null, ['url' => ['controller' => 'Academias', 'action' => 'admin_mensalidades_planos_editar_vencimento']]) ?>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <label for="name" class="form-label">Aluno</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_vencido->cliente->name ?>" disabled="true" class="form-control edit-data">
                                      <input type="hidden" name="plano_id" value="<?= $plano_cliente_vencido->id ?>">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                      <label for="name" class="form-label">Data Atual</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_vencido->data_vencimento->format('d/m/Y') ?>" disabled="true" class="form-control edit-data">
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                       <label for="name" class="form-label">Nova data</label> 
                                      <input type="date" min="2015-01-02" max="2999-12-31" name="data_vencimento" placeholder="30/12/2017" required class="form-control date edit-data">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12 text-center">
                                      <button type="submit" class="btn btn-success">Salvar</button>
                                    </div>
                                  </div>
                                  <?= $this->Form->end() ?>
                              </div>
                            </div>
                          <?php } ?>
                        <?php } ?>
                    </div>
                  </div>
                </div>

              <?php } ?>

            </div>
            <div class="tab-pane" id="a_vencer">

              <h2>Ativos</h2>

              <?= count($academia_mensalidades_planos_ativos) <= 0 ? '<h3>Não há assinaturas ativas</h3>' : '' ?>
              
              <?php foreach ($academia_mensalidades_planos_ativos as $planos_ativos) { ?>

                <div class="div-plano a_vencer">
                  <div class="row vencidas-header">
                    <div class="col-xs-12 title-planos">
                      <div class="">
                        <div class="nome-plano">
                          <label class="titulo-plano"><?= resume_string($planos_ativos->name, 40) ?></label>
                        </div>
                        <div class="text-center cancelar-plano">
                          <button class="open-overlay-cancelar-todos" type="button" data-id="<?= $planos_ativos->id ?>"><div class="col-xs-12 inner-editar-vencer"><i class="fa fa-close"></i><br> cancelar todos</div></button>
                        </div>

                        <script type="text/javascript">
                          $(document).ready(function () {
                            $('.open-overlay-cancelar-todos').click(function () {
                              var id = $(this).attr('data-id');

                              $('.cancelar-todos-'+id).height('100%');
                            });
                          });
                        </script>

                        <!-- MARCOS -->
                        <div class="overlay cancelar-todos-<?= $planos_ativos->id ?>">
                          <div class="overlay-content cancelar-inner text-center">
                            <a href="javascript:void(0)" class="closebtn">&times;</a>
                            <h2>Cancelar todos</h2>
                            <p>Atenção: Ao realizar esta ação você irá cancelar todas as assinaturas ativas, ou seja, as cobranças futuras desses alunos e iremos remover esta assinatura do seu perfil. Tem certeza que deseja cancelar?</p>
                            <?= $this->Html->link('<button class="btn btn-danger">Sim, cancelar todos</button>',
                              '/academia/admin/admin_mensalidades/assinaturas/cancelar_todas_assinaturas/'.$planos_ativos->id,
                              ['escape' => false, 'class' => 'open-overlay-cancelar-todos', 'style' => 'margin-bottom: 15px;'])?>
                          </div>
                        </div>
                        <div class="text-center editar-plano">
                          <?php if($planos_ativos->visivel == 1) { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencer"><i class="fa fa-lock"></i><br> inativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos_ativos->id,
                                ['escape' => false])?>
                          <?php } else { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencer"><i class="fa fa-unlock"></i><br> Ativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos_ativos->id,
                                ['escape' => false])?>
                          <?php } ?>
                        </div>
                        <div class="text-center editar-plano">
                          <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencer"><i class="fa fa-pencil"></i><br> editar</div>',
                              '/academia/admin/admin_mensalidades/assinaturas_edit/'.$planos_ativos->id,
                              ['escape' => false])?>
                        </div>
                        <div class="text-center ver-mais-vencer click-header-vencer" data-id="<?= $planos_ativos->id ?>">
                          <div class="col-xs-12 inner-ver-mais-vencer">
                            <label><i class="fa fa-users"> <?= $qtd_alunos_vencer[$planos_ativos->id] >= 1 ? $qtd_alunos_vencer[$planos_ativos->id] : '0' ?></i></label>
                            <p>alunos</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="conteudo-plano conteudo-plano-vencer-<?= $planos_ativos->id ?>">
                    <div class="planos-content">
                      <div class="">
                        <div class="col-sm-12 col-md-10">
                          <p><?= $planos_ativos->descricao ?></p>
                        </div>
                      </div>
                      <hr>
                      <div class="tabela-plano">
                        <table style="width: 650px;" cellpadding="10">
                          <tr>
                            <th><strong>Aluno</strong></th>
                            <th class="text-center"><strong>Vencimento</strong></th>
                            <th class="text-center"><strong>Duração mínima</strong></th>
                            <th class="text-center"><strong>Enviar e-mail</strong></th>
                            <th class="text-right"><strong>Cancelar plano</strong></th>
                          </tr>
                          <?php foreach ($planos_clientes_vencer as $plano_cliente_vencer) { ?>
                            <?php if($plano_cliente_vencer->academia_mensalidades_planos_id == $planos_ativos->id) { ?>
                              <?php $tempo_minimo = $plano_cliente_vencer->created->addMonth($planos_ativos->time_months) ?>
                              <tr>
                                <td><?= $plano_cliente_vencer->cliente->name ?></td>
                                <td class="text-center"><?= $plano_cliente_vencer->data_vencimento->format('d/m/Y') ?> <i class="fa fa-pencil open_vencimento_overlay" data-id="<?= $plano_cliente_vencer->id ?>"></i></td>
                                <td class="text-center">
                                  <?= $tempo_minimo->format('d/m/Y') ?>
                                </td>
                                <td class="text-center">
                                  <?= $this->Html->link('enviar e-mail', 
                                  '/academia/admin/admin_mensalidades/planos/cobrar_avencer/'.$plano_cliente_vencer->id,
                                  ['class' => 'btn bg-blue btn-sm']) ?>
                                </td>
                                <td class="text-right"><?= $this->Html->link('Cancelar assinatura', '/academia/admin/admin_mensalidades/planos/cancelar_plano/'.$plano_cliente_vencer->id, ['escape' => false, 'class' => 'btn btn-lg']) ?></td>
                              </tr>       
                            <?php } ?>
                          <?php } ?>                     
                        </table>
                      </div>
                        <?php foreach ($planos_clientes_vencer as $plano_cliente_vencer) { ?>
                          <?php if($plano_cliente_vencer->academia_mensalidades_planos_id == $planos_ativos->id) { ?>
                            <div class="overlay vencimento-<?= $plano_cliente_vencer->id ?>">
                              <div class="overlay-content edit-plano">
                                <a href="javascript:void(0)" class="closebtn">&times;</a>
                                  <h3>Editar data de vencimento</h3>
                                  <?= $this->Form->create(null, ['url' => ['controller' => 'Academias', 'action' => 'admin_mensalidades_planos_editar_vencimento']]) ?>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <label for="name" class="form-label">Aluno</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_vencer->cliente->name ?>" disabled="true" class="form-control edit-data">
                                      <input type="hidden" name="plano_id" value="<?= $plano_cliente_vencer->id ?>">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                      <label for="name" class="form-label">Data Atual</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_vencer->data_vencimento->format('d/m/Y') ?>" disabled="true" class="form-control edit-data">
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                       <label for="name" class="form-label">Nova data</label> 
                                      <input type="date" min="2015-01-02" max="2999-12-31" name="data_vencimento" placeholder="30/12/2017" required class="form-control date edit-data">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12 text-center">
                                      <button type="submit" class="btn btn-success">Salvar</button>
                                    </div>
                                  </div>
                                  <?= $this->Form->end() ?>
                              </div>
                            </div>
                          <?php } ?>
                        <?php } ?>
                      </div>
                  </div>
                </div>

              <?php } ?>

              <h2>Inativos</h2>
              
              <?= count($academia_mensalidades_planos_inativos) <= 0 ? '<h3>Não há assinaturas inativas</h3>' : '' ?>
              
              <?php foreach ($academia_mensalidades_planos_inativos as $planos_inativos) { ?>

                <div class="div-plano a_vencer">
                  <div class="row vencidas-header">
                    <div class="col-xs-12 title-planos">
                      <div class="">
                        <div class="nome-plano">
                          <label class="titulo-plano"><?= resume_string($planos_inativos->name, 40) ?></label>
                        </div>
                        <div class="text-center cancelar-plano">
                          <button class="open-overlay-cancelar-todos" type="button" data-id="<?= $planos_inativos->id ?>"><div class="col-xs-12 inner-editar-vencer"><i class="fa fa-close"></i><br> cancelar todos</div></button>
                        </div>

                        <script type="text/javascript">
                          $(document).ready(function () {
                            $('.open-overlay-cancelar-todos').click(function () {
                              var id = $(this).attr('data-id');

                              $('.cancelar-todos-'+id).height('100%');
                            });
                          });
                        </script>

                        <!-- MARCOS -->
                        <div class="overlay cancelar-todos-<?= $planos_inativos->id ?>">
                          <div class="overlay-content cancelar-inner text-center">
                            <a href="javascript:void(0)" class="closebtn">&times;</a>
                            <h2>Cancelar todos</h2>
                            <p>Atenção: Ao realizar esta ação você irá cancelar todas as assinaturas ativas, ou seja, as cobranças futuras desses alunos e iremos remover esta assinatura do seu perfil. Tem certeza que deseja cancelar?</p>
                            <?= $this->Html->link('<button class="btn btn-danger">Sim, cancelar todos</button>',
                              '/academia/admin/admin_mensalidades/assinaturas/cancelar_todas_assinaturas/'.$planos_inativos->id,
                              ['escape' => false, 'class' => 'open-overlay-cancelar-todos', 'style' => 'margin-bottom: 15px;'])?>
                          </div>
                        </div>
                        <div class="text-center editar-plano">
                          <?php if($planos_inativos->visivel == 1) { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencer"><i class="fa fa-lock"></i><br> inativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos_inativos->id,
                                ['escape' => false])?>
                          <?php } else { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencer"><i class="fa fa-unlock"></i><br> Ativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos_inativos->id,
                                ['escape' => false])?>
                          <?php } ?>
                        </div>
                        <div class="text-center editar-plano">
                          <?= $this->Html->link('<div class="col-xs-12 inner-editar-vencer"><i class="fa fa-pencil"></i><br> editar</div>',
                              '/academia/admin/admin_mensalidades/assinaturas_edit/'.$planos_inativos->id,
                              ['escape' => false])?>
                        </div>
                        <div class="text-center ver-mais-vencer click-header-vencer" data-id="<?= $planos_inativos->id ?>">
                          <div class="col-xs-12 inner-ver-mais-vencer">
                            <label><i class="fa fa-users"> <?= $qtd_alunos_vencer[$planos_inativos->id] >= 1 ? $qtd_alunos_vencer[$planos_inativos->id] : '0' ?></i></label>
                            <p>alunos</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="conteudo-plano conteudo-plano-vencer-<?= $planos_inativos->id ?>">
                    <div class="planos-content">
                      <div class="">
                        <div class="col-sm-12 col-md-10">
                          <p><?= $planos_inativos->descricao ?></p>
                        </div>
                      </div>
                      <hr>
                      <div class="tabela-plano">
                        <table style="width: 650px;" cellpadding="10">
                          <tr>
                            <th><strong>Aluno</strong></th>
                            <th class="text-center"><strong>Vencimento</strong></th>
                            <th class="text-center"><strong>Duração mínima</strong></th>
                            <th class="text-center"><strong>Enviar e-mail</strong></th>
                            <th class="text-right"><strong>Cancelar plano</strong></th>
                          </tr>
                          <?php foreach ($planos_clientes_vencer as $plano_cliente_vencer) { ?>
                            <?php if($plano_cliente_vencer->academia_mensalidades_planos_id == $planos_inativos->id) { ?>
                              <?php $tempo_minimo = $plano_cliente_vencer->created->addMonth($planos_inativos->time_months) ?>
                              <tr>
                                <td><?= $plano_cliente_vencer->cliente->name ?></td>
                                <td class="text-center"><?= $plano_cliente_vencer->data_vencimento->format('d/m/Y') ?> <i class="fa fa-pencil open_vencimento_overlay" data-id="<?= $plano_cliente_vencer->id ?>"></i></td>
                                <td class="text-center">
                                  <?= $tempo_minimo->format('d/m/Y') ?>
                                </td>
                                <td class="text-center">
                                  <?= $this->Html->link('enviar e-mail', 
                                  '/academia/admin/admin_mensalidades/planos/cobrar_avencer/'.$plano_cliente_vencer->id,
                                  ['class' => 'btn bg-blue btn-sm']) ?>
                                </td>
                                <td class="text-right"><?= $this->Html->link('Cancelar assinatura', '/academia/admin/admin_mensalidades/planos/cancelar_plano/'.$plano_cliente_vencer->id, ['escape' => false, 'class' => 'btn btn-lg']) ?></td>
                              </tr>       
                            <?php } ?>
                          <?php } ?>                     
                        </table>
                      </div>
                        <?php foreach ($planos_clientes_vencer as $plano_cliente_vencer) { ?>
                          <?php if($plano_cliente_vencer->academia_mensalidades_planos_id == $planos_inativos->id) { ?>
                            <div class="overlay vencimento-<?= $plano_cliente_vencer->id ?>">
                              <div class="overlay-content edit-plano">
                                <a href="javascript:void(0)" class="closebtn">&times;</a>
                                  <h3>Editar data de vencimento</h3>
                                  <?= $this->Form->create(null, ['url' => ['controller' => 'Academias', 'action' => 'admin_mensalidades_planos_editar_vencimento']]) ?>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <label for="name" class="form-label">Aluno</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_vencer->cliente->name ?>" disabled="true" class="form-control edit-data">
                                      <input type="hidden" name="plano_id" value="<?= $plano_cliente_vencer->id ?>">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                      <label for="name" class="form-label">Data Atual</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_vencer->data_vencimento->format('d/m/Y') ?>" disabled="true" class="form-control edit-data">
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                       <label for="name" class="form-label">Nova data</label> 
                                      <input type="date" min="2015-01-02" max="2999-12-31" name="data_vencimento" placeholder="30/12/2017" required class="form-control date edit-data">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12 text-center">
                                      <button type="submit" class="btn btn-success">Salvar</button>
                                    </div>
                                  </div>
                                  <?= $this->Form->end() ?>
                              </div>
                            </div>
                          <?php } ?>
                        <?php } ?>
                      </div>
                  </div>
                </div>

              <?php } ?>

            </div>
            <div class="tab-pane" id="ativas">

              <h2>Ativos</h2>
              
              <?= count($academia_mensalidades_planos_ativos) <= 0 ? '<h3>Não há assinaturas ativas</h3>' : '' ?>
              
              <?php foreach ($academia_mensalidades_planos_ativos as $planos_ativos) { ?>

                <div class="div-plano ativas">
                  <div class="row vencidas-header">
                    <div class="col-xs-12 title-planos">
                      <div class="">
                        <div class="nome-plano">
                          <label class="titulo-plano"><?= resume_string($planos_ativos->name, 40) ?></label>
                        </div>
                        <div class="text-center cancelar-plano">
                          <button class="open-overlay-cancelar-todos" type="button" data-id="<?= $planos_ativos->id ?>"><div class="col-xs-12 inner-editar-ativas"><i class="fa fa-close"></i><br> cancelar todos</div></button>
                        </div>

                        <script type="text/javascript">
                          $(document).ready(function () {
                            $('.open-overlay-cancelar-todos').click(function () {
                              var id = $(this).attr('data-id');

                              $('.cancelar-todos-'+id).height('100%');
                            });
                          });
                        </script>

                        <!-- MARCOS -->
                        <div class="overlay cancelar-todos-<?= $planos_ativos->id ?>">
                          <div class="overlay-content cancelar-inner text-center">
                            <a href="javascript:void(0)" class="closebtn">&times;</a>
                            <h2>Cancelar todos</h2>
                            <p>Atenção: Ao realizar esta ação você irá cancelar todas as assinaturas ativas, ou seja, as cobranças futuras desses alunos e iremos remover esta assinatura do seu perfil. Tem certeza que deseja cancelar?</p>
                            <?= $this->Html->link('<button class="btn btn-danger">Sim, cancelar todos</button>',
                              '/academia/admin/admin_mensalidades/assinaturas/cancelar_todas_assinaturas/'.$planos_ativos->id,
                              ['escape' => false, 'class' => 'open-overlay-cancelar-todos', 'style' => 'margin-bottom: 15px;'])?>
                          </div>
                        </div>

                        <div class="text-center editar-plano">
                          <?php if($planos_ativos->visivel == 1) { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-ativas"><i class="fa fa-lock"></i><br> inativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos_ativos->id,
                                ['escape' => false])?>
                          <?php } else { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-ativas"><i class="fa fa-unlock"></i><br> Ativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos_ativos->id,
                                ['escape' => false])?>
                          <?php } ?>
                        </div>
                        <div class="text-center editar-plano">
                          <?= $this->Html->link('<div class="col-xs-12 inner-editar-ativas"><i class="fa fa-pencil"></i><br> editar</div>',
                              '/academia/admin/admin_mensalidades/assinaturas_edit/'.$planos_ativos->id,
                              ['escape' => false])?>
                        </div>
                        <div class="text-center ver-mais-ativas click-header-ativas" data-id="<?= $planos_ativos->id ?>">
                          <div class="col-xs-12 inner-ver-mais-ativas">
                            <label><i class="fa fa-users"> <?= $qtd_alunos_ativos[$planos_ativos->id] >= 1 ? $qtd_alunos_ativos[$planos_ativos->id] : 0 ?></i></label>
                            <p>alunos</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="conteudo-plano conteudo-plano-ativas-<?= $planos_ativos->id ?>">
                    <div class="planos-content">
                      <div class="">
                        <div class="col-sm-12 col-md-10">
                          <p><?= $planos_ativos->descricao ?></p>
                        </div>
                      </div>
                      <hr>
                      <div class="tabela-plano">
                        <table style="width: 650px;" cellpadding="10">
                          <tr>
                            <th><strong>Aluno</strong></th>
                            <th class="text-center"><strong>Vencimento</strong></th>
                            <th class="text-center"><strong>Duração mínima</strong></th>
                            <th class="text-right"><strong>Cancelar</strong></th>
                          </tr>
                          <?php foreach ($planos_clientes_ativos as $plano_cliente_ativo) { ?>
                            <?php if($plano_cliente_ativo->academia_mensalidades_planos_id == $planos_ativos->id) { ?>
                              <?php $tempo_minimo = $plano_cliente_ativo->created->addMonth($planos_ativos->time_months) ?>
                              <tr>
                                <td><?= $plano_cliente_ativo->cliente->name ?></td>
                                <td class="text-center"><?= $plano_cliente_ativo->data_vencimento->format('d/m/Y') ?> <i class="fa fa-pencil open_vencimento_overlay" data-id="<?= $plano_cliente_ativo->id ?>"></i></td>
                                <td class="text-center">
                                  <?= $tempo_minimo->format('d/m/Y') ?>
                                </td>
                                <td class="text-right"><?= $this->Html->link('Cancelar assinatura', '/academia/admin/admin_mensalidades/planos/cancelar_plano/'.$plano_cliente_ativo->id, ['escape' => false, 'class' => 'btn btn-lg']) ?></td>
                              </tr>
                            <?php } ?>
                          <?php } ?>                            
                        </table>
                      </div>
                        <?php foreach ($planos_clientes_ativos as $plano_cliente_ativo) { ?>
                          <?php if($plano_cliente_ativo->academia_mensalidades_planos_id == $planos_ativos->id) { ?>
                            <div class="overlay vencimento-<?= $plano_cliente_ativo->id ?>">
                              <div class="overlay-content edit-plano">
                                <a href="javascript:void(0)" class="closebtn">&times;</a>
                                  <h3>Editar data de vencimento</h3>
                                  <?= $this->Form->create(null, ['url' => ['controller' => 'Academias', 'action' => 'admin_mensalidades_planos_editar_vencimento']]) ?>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <label for="name" class="form-label">Aluno</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_ativo->cliente->name ?>" disabled="true" class="form-control edit-data">
                                      <input type="hidden" name="plano_id" value="<?= $plano_cliente_ativo->id ?>">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                      <label for="name" class="form-label">Data Atual</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_ativo->data_vencimento->format('d/m/Y') ?>" disabled="true" class="form-control edit-data">
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                       <label for="name" class="form-label">Nova data</label> 
                                      <input type="date" min="2015-01-02" max="2999-12-31" name="data_vencimento" placeholder="30/12/2017" required class="form-control date edit-data">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12 text-center">
                                      <button type="submit" class="btn btn-success">Salvar</button>
                                    </div>
                                  </div>
                                  <?= $this->Form->end() ?>
                              </div>
                            </div>
                          <?php } ?>
                        <?php } ?>  
                    </div>
                  </div>
                </div>

              <?php } ?>

              <h2>Inativos</h2>
              
              <?= count($academia_mensalidades_planos_inativos) <= 0 ? '<h3>Não há assinaturas inativas</h3>' : '' ?>
              
              <?php foreach ($academia_mensalidades_planos_inativos as $planos_inativos) { ?>

                <div class="div-plano ativas">
                  <div class="row vencidas-header">
                    <div class="col-xs-12 title-planos">
                      <div class="">
                        <div class="nome-plano">
                          <label class="titulo-plano"><?= resume_string($planos_inativos->name, 40) ?></label>
                        </div>
                        <div class="text-center cancelar-plano">
                          <button class="open-overlay-cancelar-todos" type="button" data-id="<?= $planos_inativos->id ?>"><div class="col-xs-12 inner-editar-ativas"><i class="fa fa-close"></i><br> cancelar todos</div></button>
                        </div>

                        <script type="text/javascript">
                          $(document).ready(function () {
                            $('.open-overlay-cancelar-todos').click(function () {
                              var id = $(this).attr('data-id');

                              $('.cancelar-todos-'+id).height('100%');
                            });
                          });
                        </script>

                        <!-- MARCOS -->
                        <div class="overlay cancelar-todos-<?= $planos_inativos->id ?>">
                          <div class="overlay-content cancelar-inner text-center">
                            <a href="javascript:void(0)" class="closebtn">&times;</a>
                            <h2>Cancelar todos</h2>
                            <p>Atenção: Ao realizar esta ação você irá cancelar todas as assinaturas ativas, ou seja, as cobranças futuras desses alunos e iremos remover esta assinatura do seu perfil. Tem certeza que deseja cancelar?</p>
                            <?= $this->Html->link('<button class="btn btn-danger">Sim, cancelar todos</button>',
                              '/academia/admin/admin_mensalidades/assinaturas/cancelar_todas_assinaturas/'.$planos_inativos->id,
                              ['escape' => false, 'class' => 'open-overlay-cancelar-todos', 'style' => 'margin-bottom: 15px;'])?>
                          </div>
                        </div>

                        <div class="text-center editar-plano">
                          <?php if($planos_inativos->visivel == 1) { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-ativas"><i class="fa fa-lock"></i><br> inativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos_inativos->id,
                                ['escape' => false])?>
                          <?php } else { ?>
                            <?= $this->Html->link('<div class="col-xs-12 inner-editar-ativas"><i class="fa fa-unlock"></i><br> Ativar</div>',
                                '/academia/admin/admin_mensalidades/planos_delete/'.$planos_inativos->id,
                                ['escape' => false])?>
                          <?php } ?>
                        </div>
                        <div class="text-center editar-plano">
                          <?= $this->Html->link('<div class="col-xs-12 inner-editar-ativas"><i class="fa fa-pencil"></i><br> editar</div>',
                              '/academia/admin/admin_mensalidades/assinaturas_edit/'.$planos_inativos->id,
                              ['escape' => false])?>
                        </div>
                        <div class="text-center ver-mais-ativas click-header-ativas" data-id="<?= $planos_inativos->id ?>">
                          <div class="col-xs-12 inner-ver-mais-ativas">
                            <label><i class="fa fa-users"> <?= $qtd_alunos_ativos[$planos_inativos->id] >= 1 ? $qtd_alunos_ativos[$planos_inativos->id] : 0 ?></i></label>
                            <p>alunos</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="conteudo-plano conteudo-plano-ativas-<?= $planos_inativos->id ?>">
                    <div class="planos-content">
                      <div class="">
                        <div class="col-sm-12 col-md-10">
                          <p><?= $planos_inativos->descricao ?></p>
                        </div>
                      </div>
                      <hr>
                      <div class="tabela-plano">
                        <table style="width: 650px;" cellpadding="10">
                          <tr>
                            <th><strong>Aluno</strong></th>
                            <th class="text-center"><strong>Vencimento</strong></th>
                            <th class="text-center"><strong>Duração mínima</strong></th>
                            <th class="text-right"><strong>Cancelar</strong></th>
                          </tr>
                          <?php foreach ($planos_clientes_ativos as $plano_cliente_ativo) { ?>
                            <?php if($plano_cliente_ativo->academia_mensalidades_planos_id == $planos_inativos->id) { ?>
                              <?php $tempo_minimo = $plano_cliente_ativo->created->addMonth($planos_inativos->time_months) ?>
                              <tr>
                                <td><?= $plano_cliente_ativo->cliente->name ?></td>
                                <td class="text-center"><?= $plano_cliente_ativo->data_vencimento->format('d/m/Y') ?> <i class="fa fa-pencil open_vencimento_overlay" data-id="<?= $plano_cliente_ativo->id ?>"></i></td>
                                <td class="text-center">
                                  <?= $tempo_minimo->format('d/m/Y') ?>
                                </td>
                                <td class="text-right"><?= $this->Html->link('Cancelar assinatura', '/academia/admin/admin_mensalidades/planos/cancelar_plano/'.$plano_cliente_ativo->id, ['escape' => false, 'class' => 'btn btn-lg']) ?></td>
                              </tr>
                            <?php } ?>
                          <?php } ?>                            
                        </table>
                      </div>
                        <?php foreach ($planos_clientes_ativos as $plano_cliente_ativo) { ?>
                          <?php if($plano_cliente_ativo->academia_mensalidades_planos_id == $planos_inativos->id) { ?>
                            <div class="overlay vencimento-<?= $plano_cliente_ativo->id ?>">
                              <div class="overlay-content edit-plano">
                                <a href="javascript:void(0)" class="closebtn">&times;</a>
                                  <h3>Editar data de vencimento</h3>
                                  <?= $this->Form->create(null, ['url' => ['controller' => 'Academias', 'action' => 'admin_mensalidades_planos_editar_vencimento']]) ?>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <label for="name" class="form-label">Aluno</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_ativo->cliente->name ?>" disabled="true" class="form-control edit-data">
                                      <input type="hidden" name="plano_id" value="<?= $plano_cliente_ativo->id ?>">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                      <label for="name" class="form-label">Data Atual</label> 
                                      <input type="text" placeholder="<?= $plano_cliente_ativo->data_vencimento->format('d/m/Y') ?>" disabled="true" class="form-control edit-data">
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                       <label for="name" class="form-label">Nova data</label> 
                                      <input type="date" min="2015-01-02" max="2999-12-31" name="data_vencimento" placeholder="30/12/2017" required class="form-control date edit-data">
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12 text-center">
                                      <button type="submit" class="btn btn-success">Salvar</button>
                                    </div>
                                  </div>
                                  <?= $this->Form->end() ?>
                              </div>
                            </div>
                          <?php } ?>
                        <?php } ?>  
                    </div>
                  </div>
                </div>

              <?php } ?>

            </div>
          </div>
        </div>  
      </div>
    </div>
  </div>
</section>

<?= $this->Html->script('jquery.mask.min.js')?>
<script type="text/javascript">
  $(document).ready(function() {
    $('.open_vencimento_overlay').click(function() {
      $('.vencimento-'+$(this).attr('data-id')).height('100%');
    });

    $('.closebtn').click(function() {
      $('.overlay').height(0);
    });

    $('.click-header-vencidas').click(function() {
      $('.conteudo-plano-vencidas-'+$(this).attr('data-id')).slideToggle();
    });

    $('.click-header-vencer').click(function() {
      $('.conteudo-plano-vencer-'+$(this).attr('data-id')).slideToggle();
    });

    $('.click-header-ativas').click(function() {
      $('.conteudo-plano-ativas-'+$(this).attr('data-id')).slideToggle();
    });
  });
</script>
