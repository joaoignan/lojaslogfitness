
<?= $this->Html->script('jquery.mask.min.js')?>
<script type="text/javascript">
  $("#phone").mask("(00) 00000-0009");
  $(".cpf").mask("999.999.999-99");
</script>
<section class="content">
  <?php
    $myTemplates = [
        'inputContainer' => '{{content}}',
    ];
    $this->Form->templates($myTemplates);
  ?>
<section class="content">
  <div class="col-xs-12">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Convidar membro do time</h3>
        <span class="pull-right"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
          ['controller' => 'Academias', 'action' => 'admin_professores'],
          ['escape' =>  false,
          'class'  =>  'pull-right']) ?></span>
      </div>
      <?= $this->Form->create(null , ['class' => 'form-horizontal bordered-row']); ?>
      <div class="box-body">
        <p>Insira os dados do novo membro do time</p>
        <div class="form-group">
          <label for="cpf" class="col-sm-4 col-md-2 control-label">CPF</label>
          <div class="col-sm-6">
            <?= $this->Form->input('cpf', [
                'label'         => false,
                'div'           => false,
                'placeholder'   => false,
                'class'         => 'validate[required] form-control margin-bottom-10 cpf cpf-valid verifica-prof'
            ]) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="name" class="col-sm-4 col-md-2 control-label">Email</label>
          <div class="col-sm-6">
            <?= $this->Form->input('email', [
                'label'         => false,
                'div'           => false,
                'placeholder'   => false,
                'class'         => 'validate[required] form-control margin-bottom-10 email-response'
            ]) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="name" class="col-sm-4 col-md-2 control-label">Nome</label>
          <div class="col-sm-6">
            <?= $this->Form->input('name', [
                'label'         => false,
                'div'           => false,
                'placeholder'   => false,
                'class'         => 'validate[required] form-control margin-bottom-10 name-response'
            ]) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="name" class="col-sm-4 col-md-2 control-label">Telefone</label>
          <div class="col-sm-6">
            <?= $this->Form->input('phone', [
                'label'         =>  false,
                'div'           =>  false,
                'placeholder'   =>  false,
                'class'         => 'validate[required] phone-mask form-control margin-bottom-10 tel-response'
            ]) ?>
          </div>
        </div>
        <div class="form-group">
          <label for="funcao" class="col-sm-4 col-md-2 control-label">Função</label>
          <div class="col-sm-6">
            <?= $this->Form->input('funcao', [
                'label'   => false,
                'div'     => false,
                'options' => [
                    'Atendente'        => 'Atendente',
                    'Estagiário'       => 'Estagiário',
                    'Gerente'          => 'Gerente',
                    'Instrutor'        => 'Instrutor',
                    'Nutricionista'    => 'Nutricionista',
                    'Personal Trainer' => 'Personal Trainer',
                    'Professor'        => 'Professor',
                    'Proprietário'     => 'Proprietário',
                    'Recepcionista'    => 'Recepcionista'
                ],
                'class'   => 'form-control validate[required] margin-bottom-10'
            ]) ?>
          </div>
        </div>
        <div class="box-footer">
            <?= $this->Form->button('Convidar', [
                'type'          => 'submit',
                'div'           => false,
                'class'         => 'btn btn-success',
            ]) ?>
        </div>
          <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  $('.cpf-valid').blur(function() {
    var cpf_input = $(this);
    $('.erro-cpf-invalido').remove();
    if (cpf_input.val().length > 0) {
      if(!TestaCPF(cpf_input.val())) {
        cpf_input.after('<p class="erro-cpf-invalido" style="color: red">CPF inválido!</p>');
        cpf_input.focus();  
      }
    }
  });

  $('.verifica-prof').blur(function() {
    var cpf_input = $(this);
    var email_input = $('.email-response');
    var name_input = $('.name-response');
    var tel_input = $('.tel-response');
    $('.erro-cpf-invalido').remove();
      if(TestaCPF(cpf_input.val())) {
        $.get(WEBROOT_URL + 'academias/verifica-professor-cpf/'+cpf_input.val(),
          function (data) {    
            if(data != 2) {
              data = JSON.parse(data);
              cpf_input.attr('readonly', true);
              if(data.email != null && data.email.length >= 1) {
                email_input.val(data.email);
                email_input.attr('readonly', true);
              }
              if(data.name != null && data.name.length >= 1) {
                name_input.val(data.name);
              }
              if(data.phone != null && data.phone.length >= 1) {
                tel_input.val(data.phone);
              } else if(data.mobile != null && data.mobile.length >= 1) {
                tel_input.val(data.mobile);
              }
              name_input.blur();  
            }
        });
      } else if (cpf_input.val().length > 0) {
        cpf_input.after('<p class="erro-cpf-invalido" style="color: red">CPF inválido!</p>');
        cpf_input.focus();             
      }
  });

  $('.email-response').blur(function () {
    var email_input = $(this);
    var cpf_input = $('.verifica-prof');
    var name_input = $('.name-response');
    var tel_input = $('.tel-response');
    $.get(WEBROOT_URL + 'academias/verifica-professor-email?email='+email_input.val(),
      function (data) {
        if(data != 2) {
          data = JSON.parse(data);
          email_input.attr('readonly', true);
          if(data.cpf != null && data.cpf.length >= 1) {
            cpf_input.val(data.cpf);
            cpf_input.attr('readonly', true);
          }
          if(data.name != null && data.name.length >= 1) {
            name_input.val(data.name);
          }
          if(data.phone != null && data.phone.length >= 1) {
            tel_input.val(data.phone);
          } else if(data.mobile != null && data.mobile.length >= 1) {
            tel_input.val(data.mobile);
          }
          name_input.blur();  
        }
    });
  });

  function apenasNumeros(string) 
  {
    var numsStr = string.replace(/[^0-9]/g,'');
    return numsStr;
  }

  function TestaCPF(cpf) {
    var Soma;
    var Resto;
    var cpf = apenasNumeros(cpf);
    Soma = 0;
    if (cpf == "00000000000") return false;

    for (i=1; i<=9; i++) Soma = Soma + parseInt(cpf.substring(i-1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(cpf.substring(9, 10)) ) return false;

    Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(cpf.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(cpf.substring(10, 11) ) ) return false;
    return true;
  }
</script>