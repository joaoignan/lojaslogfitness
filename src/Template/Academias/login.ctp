<?php
$this->layout = null;
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>
<!DOCTYPE html>
<html lang ="pt_BR">
<head>
    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>Acesso Perfil - LogFitness</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>assets/images/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>assets/images/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>assets/images/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>assets/images/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>assets/images/icons/favicon-16x16.png">


    <link rel="stylesheet" type="text/css" href="<?= WEBROOT_URL ?>assets-minified/admin-all-demo.css">

    <!-- JS Core -->
    <script type="text/javascript" src="<?= WEBROOT_URL ?>assets-minified/js-core.js"></script>

    <script type="text/javascript">
        $(window).load(function(){
            setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>
</head>
<body>
<div id="loading">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>

<style type="text/css">
    html,body {
        height: 100%;
        background: #fff;
        overflow: hidden;
        background: url("<?= WEBROOT_URL ?>assets/image-resources/blurred-bg/blurred-bg-2.jpg") no-repeat center center;
        background-size: cover;
    }
    .active-sobrenav{
      color: #f3d012!important;
    }
    .sobrenavbar{
      height: 25px;
      padding: 2px 30px;
      background-color: #5089cf;
    }
    .sobrenavbar span{
      font-size: 14px;
      font-family: nexa;
      font-weight: bold;
      color: #FFF;
      margin: 0 15px;
      cursor: pointer;
      text-transform: none;
    }
    .sobrenavbar span:hover{
      color: #f3d012;
    }
    .logo-login {
        max-width: 120px;
        height: auto;
        margin-top: 5px;
        margin-bottom: 5px;
    }
    .identificacao{
        font-family: Nexa;
        color: #698cc3;
        font-size: 20px;
    }
    @media all and (max-width: 768px){
        .sobrenavbar{
            display: none;
        }
    }
    @media all and (max-width: 500px){
        .logo-login {
            width: 80%;
            height: auto;
            margin-top: 5px;
            margin-bottom: 5px;
        }
    }
</style>

<script type="text/javascript" src="<?= WEBROOT_URL ?>assets/widgets/wow/wow.js"></script>
<script type="text/javascript">
    /* WOW animations */
    wow = new WOW({
        animateClass: 'animated',
        offset: 100
    });
    wow.init();
</script>
<div class="center-vertical">
    <div class="center-content">
        <div class="col-xs-12 col-sm-8 col-md-5 center-margin text-center">
            <?= $this->Form->create() ?>
            <div class="content-box wow bounceInDown modal-content">
                <?= $this->Html->image('logo_master.png',['class' => 'logo-login'])  ?>
                <h3 class="content-box-header content-box-header-alt bg-default identificacao">
                        acesso perfil
                </h3>
                <?= $this->Flash->render() ?>
                <div class="content-box-wrapper">
                    <div class="form-group">
                        <div class="input-group">
                            <?= $this->Form->input('email',
                                [
                                    'type'          => 'text',
                                    'label'         => false,
                                    'class'         => 'form-control',
                                    'id'            => 'exampleInputEmail1',
                                    'placeholder'   => __('E-mail ou CNPJ')
                                ]) ?>
                            <span class="input-group-addon bg-blue">
                                    <i class="glyph-icon icon-user"></i>
                                </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <?= $this->Form->input('password',
                                [
                                    'label'         => false,
                                    'class'         => 'form-control',
                                    'id'            => 'exampleInputPassword1',
                                    'placeholder'   => __('Password')
                                ]) ?>
                            <span class="input-group-addon bg-blue">
                                    <i class="glyph-icon icon-unlock-alt"></i>
                                </span>
                        </div>
                    </div>
                    <div class="form-group text-center col-xs-12">
                        <?= $this->Html->link('Esqueci a senha',['controller' => 'Academias', 'action' => 'forgot_passwd'],['title' => 'Recuperar acesso'])?>
                    </div>
                    <!-- <div class="form-group text-center col-sm-6">
                        <?= $this->Html->link('Sou do time',['controller' => 'Professores', 'action' => 'login'],['title' => 'Admin Professor'])?>
                    </div> -->
                    <div class="form-group text-left">
                        <?= $this->Form->button(__('<i class="glyph-icon icon-arrow-right"></i>&nbsp;&nbsp;Entrar'), ['class' => 'btn btn-success btn-block']); ?>
                    </div>

                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<!-- JS Demo -->
<script type="text/javascript" src="<?= WEBROOT_URL ?>assets-minified/admin-all-demo.js"></script>

<!-- trustvox -->
<script type="text/javascript">
    var _trustvox_certificate = _trustvox_certificate || [];
    _trustvox_certificate.push(['_certificateId', 'logfitness']);
    (function() {
      var tv = document.createElement('script'); tv.type = 'text/javascript'; tv.async = true;
      tv.src = '//s3-sa-east-1.amazonaws.com/trustvox-certificate-modal-js/widget.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tv, s);
    })();
</script>

</body>
</html>
