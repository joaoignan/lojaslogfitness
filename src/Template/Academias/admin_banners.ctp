<style>
 @media all and (max-width: 450px){
    .no-mobile{
      display: none;
    }
  }
  @media all and (max-width: 1023px){
    .no-small{
      display: none;
    }
  }
</style>

<section class="content">
   <div class="col-xs-12">
      <div class="box box-info">
        <h3 class="box-header">
            Banners
        </h3>
        <div class="col-xs-12">
            <?= $this->Html->link('<i class="fa fa-picture-o"></i>'.__(' Novo Banner'),
            '/academia/admin/banners/novo',
            [
              'class' => 'btn btn-success',
              'escape' => false
            ])
        ?>
        </div>
        <div class="banners index">
          <table class="table table-hover">
              <thead>
              <tr>
                  <th class="no-small"><?= $this->Paginator->sort('id') ?></th>
                  <th class="no-small"><?= $this->Paginator->sort('title') ?></th>
                  <th><?= $this->Paginator->sort('image') ?></th>
                  <th><?= $this->Paginator->sort('status_id') ?></th>
                  <th class="no-small"><?= $this->Paginator->sort('created') ?></th>
                  <th class="no-mobile"><?= $this->Paginator->sort('modified') ?></th>
                  <th class="actions"></th>
              </tr>
              </thead>
              <tbody>
              <?php foreach ($banners as $banner): ?>
                  <tr>
                      <td class="no-small"><?= $this->Number->format($banner->id) ?></td>
                      <td class="no-small"><?= h($banner->title) ?></td>
                      <td>
                        <a href="<?= IMG_URL; ?>banners/<?= $banner->image ?>" class="fancybox">
                          <?= $this->Html->image('banners/'.$banner->image, ['style' => 'max-width: 50px;']) ?>
                        </a>
                      </td>
                      <td><?= $banner->has('status') ? $this->Html->link($banner->status->name, ['controller' => 'Status', 'action' => 'view', $banner->status->id]) : '' ?>
                      </td>
                      <td class="no-small"><?= h($banner->created) ?></td>
                      <td class="no-mobile"><?= h($banner->modified) ?></td>
                      <td class="actions">
                          <div class="btn-group">
                              <?= $this->Html->link('<i class="fa fa-pencil"></i> Editar',
                                          '/academia/admin/banners/editar/'.$banner->id,
                                          ['escape' => false, 'class' => 'btn bg-blue']) ?>

                              <?= $this->Html->link('<i class="fa fa-close"></i> Excluir',
                                          '/academia/admin/banners/'.$banner->id,
                                          ['escape' => false, 'class' => 'btn bg-red']) ?>
                          </div>
                      </td>
                  </tr>

              <?php endforeach; ?>
              </tbody>
          </table>
        <div class="paginator" style="margin-left: 15px;">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p style="margin-left: 15px;"><?= $this->Paginator->counter() ?></p>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>

</section>
