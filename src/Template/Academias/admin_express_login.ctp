<script>
    $('.laterais, .box-close-filter, .box-close-mochila').hide();
</script>

<style>
    #form-cadastro-main,
    #info-cadastro-main-sem,
    #info-cadastro-main-com,
    #escolher-academia-box,
    #fb-login,
    #form-login-main {
        display: block;
    }
    .title-login {
        font-size: 2em;
        text-align: center;
        margin: 0 0 10px;
        color: #5087c7;
        font-family: nexa_boldregular;
    }
    .button-acessar {
        border: 0;
        font-weight: bold;
        color: white;
        background: #8DC73F;
        float: none;
        width: 100%;
        height: 45px;
        text-shadow: 1px 1px 1px #0F2538;
        margin-right: 0px;
        margin-left: 0px;
    }
    .fundo-branco {
        display: none;
    }
    .button-cadastrar {
        width: 70%;
        background: #8DC73F;
    }
    .button-nao-faco-parte {
        width: 70%;
        margin-left: 0;
        margin-right: 0;
    }
    .separador-cadastrar {
        width: 70%;
        border: 1px solid #5087c7;
        margin-top: 0;
        margin-bottom: 0;
    }
    .botoes-facebook {
        width: 100%;
    }
    .separador-cadastrar-mobile {
        display: none;
        width: 70%;
        border: 1px solid #5087c7;
        margin-top: 0;
        margin-bottom: 0;
    }
    .checkbox-div {
        margin-top: 15px;
        margin-left: 60px;
    }
    @media all and (max-width: 768px) and (min-width: 431px) {
        .input-cadastro{
            width: 70%;
        }
        .logins{
            margin-top: 70px;
        }
        .button-nao-faco-parte{
            width: 83%;
        } 
    }
    @media all and (max-width: 430px) {
        .botoes-facebook {
            width: 100%;
        }
        .logins{
            margin-top: 70px;
        }
        .button-acessar{
            width: 90%;
        }
        .input-cadastro-academia{
            width: 100%;
        }
        .button-cadastrar{
            width: 90%;
        }
        .button-nao-faco-parte{
            width: 90%;
            font-size: 11px;
        }
        .separador-cadastrar-mobile {
            display: block;
        }
        .fb-font{
            font-size: 12px;
        }
    }
    @media all and (max-width: 786px) {
        .fundo-branco {
            display: none;
        }
    }
</style>

<div class="container logins" id="modal-cliente-login" style="width: 950px; height: auto">
    <div class="bg-mapa" style=" ">
        <p class="text-center hidden-xs" style="margin: 15px 0 15px 0;">
            <span class="text-center font-bold">
                <?= isset($fb_login_error)
                    ?
                    '<h5 class="font-bold text-center">
                    Ocorreu um erro ao realizar a conexão com o Facebook.<br> 
                    Verifique se você concedeu permissão de acesso ao aplicativo da LogFitness ou tente novamente dentro 
                    de alguns minutos.
                    </h5>'
                    :
                    '<p id="info-cadastro-main-com" class="text-center"></p>
                     <p id="info-cadastro-main-sem" class="text-center"></p>'
                ?>
            </span>
        </p>

        <div class="col-sm-6 col-sm-offset-3">
            <div class="form-group row ">
                <br>
                <h3 class="title-login">Faça seu login como aluno</h3>
                <p class="text-center">Utilize sua conta de aluno para fazer seu pedido</p>
                <br>
                <?= $this->Form->create(null, ['id' => 'form-login-main'])?>
                <div class="col-sm-12">
                    <div class="form-group row ">
                        <div class="col-sm-12">
                            <?= $this->Form->input('email', [
                                'div'           => false,
                                'label'         => false,
                                'class'         => 'input-cadastro form-control validate[required]',
                                'placeholder'   => 'EMAIL ou CPF',
                            ])?>
                        </div>
                        <div class="col-sm-12">
                            <?= $this->Form->input('password', [
                                'id'            => 'login-password',
                                'div'           => false,
                                'label'         => false,
                                'class'         => 'input-cadastro form-control validate[required]',
                                'placeholder'   => 'SENHA',
                                'autocomplete'  => 'off',
                            ])?>
                        </div>
                        <div class="col-sm-12 text-center">
                            <?= $this->Html->link('Esqueci a senha',
                                $SSlug.'/login/esqueci-a-senha',
                                ['class' => 'btn'])?>
                        </div>
                        <div class="col-sm-12 text-center">
                            <?= $this->Form->button('ACESSAR', [
                                'id'            => 'submit-login-express',
                                'type'          => 'button',
                                'class'         => 'button-acessar',
                            ])?>
                        </div>
                    
                <?= $this->Form->end()?>

                <p class="text-center" style="margin-bottom: 15px"><b>OU</b></p>

                <div class="col-md-12 text-center margin-bottom-30" id="fb-login" >
                <?= $this->Html->link('<i class="fa fa-facebook-square"></i> Acessar com Facebook',
                            '/fb-login/',
                        ['class' => 'btn btn-primary font-bold botoes-facebook fb-font', 'escape' => false]) ?>
                </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>






