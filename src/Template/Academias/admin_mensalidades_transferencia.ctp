<?= $this->Html->script('jquery.mask.min.js')?>
<script type="text/javascript">$("#valor_transferencia").mask('000.000.000.000.000,00', {reverse: true});</script>
<style>
	.saldo-transferencia{
		font-weight: 700;
		color: green;
	}
	.colta-atual h4{
		font-weight: 700;
	}
	.transferencia{
		width: 80%;
		margin-left: 10%;
	}
	#valor_transferencia::-webkit-input-placeholder{
	    text-align:center;
	}
	#email::-moz-placeholder{
	    text-align:center;
	}
	.solicitar{
		margin: 15px 0;
	}
	.transfer-table{
		width: 80%;
		margin-bottom: 35px;
	}
	.transfer-table td{
		height: 35px;
		vertical-align: middle;
		padding: 0 5px;
	}
	@media all and (max-width: 1024px){
		.transfer-table{
			width: 100%;
		}
	}
</style>


<section class="content">
	<div class="col-xs-12">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Solicitar Transferência</h3>
				<span class="pull-right"><?= $this->Html->link('<i class="fa fa-undo" aria-hidden="true"></i> Voltar',
                  '/academia/admin/dashboard',
                  ['escape' =>  false,
                     'class'  =>  'pull-right']) ?></span>
            </div>
			<div class="box-body">
				<?= $this->Form->create(null, ['url' => ['controller' => 'Academias', 'action' => 'realizar_saque']]) ?>
				<div class="col-xs-12 text-center">
					<?php 
						$valor = explode(' ', $resp_iugu_subconta_info->balance_available_for_withdraw);
	                    if($valor[1] == 'BRL') {
	                        $valor = str_replace(".","",$valor[0]);
            				$valor = str_replace(",",".",$valor);
	                    } else {
	                        $valor = str_replace(".","",$valor[1]);
            				$valor = str_replace(",",".",$valor);
	                    }
					?>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 text-center">
					<table align="center" class="transfer-table" style="background-color: #eeeeee">
						<tr>
							<td style="text-align: left;">Saldo Disponível para retirada</td>
							<td style="text-align: right;"> R$ <?= number_format($valor, 2, ',', '.') ?></td>
						</tr>
						<tr>
							<td style="text-align: left;"><strong>(-) Custo retirada</strong></td>
							<td style="text-align: right;">R$ 2,00</td>
						</tr>
						<tr>
							<td style="text-align: left;"><strong>(=) Valor disponível da retirada</strong></td>
							<td style="text-align: right;"> R$ <?= number_format($total = $valor - 2, 2, ',', '.') ?></td>
						</tr>
					</table>

					<h4>Informe o valor para a transferência</h4>
					<p class="help-block">Valor mínimo de R$ 5,00</p>
					<?= $this->Form->input('valor_transferencia', [
						'label'		  => false,
						'id'		  => 'valor_transferencia',
						'placeholder' => 'R$150,00',
						'class'       => 'form-control transferencia'
					]) ?>

					<?php if($total) ?>
					<script type="text/javascript">
						var total = <?= $total ?>;

						$('#valor_transferencia').blur(function() {
							var valor = $(this).val().replace(",", '.');
							valor = parseFloat(valor);
							if(valor <= total) {
								$('.solicitar-btn').removeAttr('disabled');
							} else {	
								$('.solicitar-btn').attr('disabled', true);
							}
						});
					</script>

					<p class="help-block">Confira os dados da sua conta antes de realizar a transferência.</p>
					<p class="help-block">Obs: As transferências são feitas automaticamente no próximo dia útil e estão disponíveis na conta até as 18:00.</p>

					<?php if(!$Academia->mens_conta || !$Academia->mens_agencia || !$Academia->mens_tipo_conta || !$Academia->mens_banco || !$Academia->cnpj || $total < 5) { ?>
						<?= $this->Form->button('Solicitar', ['escape' => false, 'class' => 'btn btn-success btn-lg', 'disabled']) ?>
					<?php } else { ?>
						<?= $this->Form->button('Solicitar', ['escape' => false, 'class' => 'btn btn-success btn-lg solicitar-btn', 'disabled']) ?>
					<?php } ?>

					<?= $this->Form->end(); ?>
					<hr>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 text-center conta-atual">
					<div class="col-xs-12 text-left">
						<div class="col-xs-12 text-right">
							<?= $this->Html->link('<i class="fa fa-pencil"></i> alterar','/academia/admin/admin_mensalidades/config',['escape' => false])?>
						</div>
						<p><strong>Razão Social</strong></p>
						<p><?= $Academia->name ?></p>
						<p><strong>CNPJ</strong></p>
						<p><?= $Academia->cnpj ?></p>
						<p><strong>Tipo da conta</strong></p>
						<p><?= $Academia->mens_tipo_conta ?></p>
						<p><strong>Banco</strong></p>
						<p><?= $Academia->mens_banco ?></p>
						<p><strong>Agência</strong></p>
						<p><?= $Academia->mens_agencia ?> - <?= $Academia->mens_agencia_dv ?></p>
						<p><strong>Conta</strong></p>
						<p><?= $Academia->mens_conta ?> - <?= $Academia->mens_conta_dv ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
