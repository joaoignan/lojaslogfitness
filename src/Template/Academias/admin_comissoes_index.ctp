<style>
    @media all and (max-width: 450px){
    .no-mobile{
      display: none;
    }
    .font-header{
        font-size: 12px
    }
    .font-body{
        font-size: 11px
    }
  }
  @media all and (max-width: 1023px){
    .no-small{
      display: none;
    }
  }
</style>
<section class="content">
    <div class="col-xs-12">
        <div class="box box-info">
            <h3 class="box-header">Minhas comissões</h3>
            <div class="box-body">
                <div class="academiaComissoes index table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center font-header"><?= $this->Paginator->sort('id') ?></th>
                                <th class="text-center font-header"><?= $this->Paginator->sort('ano') ?></th>
                                <th class="text-center font-header"><?= $this->Paginator->sort('mes') ?></th>
                                <th class="text-left font-header"><?= $this->Paginator->sort('vendas') ?></th>
                                <th class="text-left font-header"><?= $this->Paginator->sort('comissao') ?></th>
                                <th class="text-left no-small font-header"><?= $this->Paginator->sort('meta') ?></th>
                                <th class="text-center font-header"><?= $this->Paginator->sort('aceita') ?></th>
                                <th class="text-center font-header"><?= $this->Paginator->sort('paga') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($academiaComissoes as $academiaComisso): ?>
                            <tr>
                                <td class="text-center font-body"><?= $this->Number->format($academiaComisso->id) ?></td>
                                <td class="text-center font-body"><?= $academiaComisso->ano ?></td>
                                <td class="text-center font-body"><?= $academiaComisso->mes ?></td>
                                <td class="text-left font-body">R$ <?= number_format($academiaComisso->vendas, 2, ',', '.') ?></td>
                                <td class="text-left font-body">R$ <?= number_format($academiaComisso->comissao, 2, ',', '.') ?></td>
                                <td class="no-small text-left font-body">R$ <?= number_format($academiaComisso->meta, 2, ',', '.') ?></td>

                                <?php if($academiaComisso->aceita == 2) { ?>
                                    <?php if($academia->conta != null) { ?>
                                        <td class="text-center font-body">
                                            <?= $this->Html->link(
                                                '<i class="fa fa-calendar-o"></i> '.__('Aceitar'),
                                                '/academia/admin/comissao/'.$academiaComisso->id,
                                                ['class' => 'btn btn-success', 'escape' => false]) ?>
                                        </td>
                                    <?php } else { ?>
                                        <td class="text-center font-body">
                                            <?= $this->Html->link(
                                                '<i class="fa fa-calendar-o"></i> '.__('Aceitar'),
                                                '/academia/admin/dados-bancarios/1',
                                                ['class' => 'btn btn-success', 'escape' => false]) ?>
                                        </td>
                                    <?php } ?>
                                <?php } else { ?>
                                    <td class="text-center font-body">Sim</td>
                                <?php } ?>

                                <td class="text-center font-body"><?= $academiaComisso->paga == 1 ? 'Sim' : 'Não' ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="paginator" style="margin-left: 15px;">
                    <ul class="pagination">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                    <p style="margin-left: 15px;"><?= $this->Paginator->counter() ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
