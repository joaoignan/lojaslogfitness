<?= $this->Flash->render() ?>
<div class="container informacoes text-center">
    <div class="col-lg-6 academias academias2">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/Nr7UNxpzO18" frameborder="0" allowfullscreen></iframe>
        <br class="clear">
        <div class="absolut-center-100">

        </div>
    </div>
    <div class="col-lg-6 academias academias2">
        <span class="titulo">ACADEMIAS</span>
        <p>
            É muito simples!
        </p>
        <div class="absolut-center-100">
            <a id="form-toggle-open">CADASTRE AQUI</a>
        </div>
    </div>
</div>

<div class="container info-central-box form-toggle" id="form-toggle">

    <?= $this->Form->create(null, [
        'id'        => 'form-cadastrar-academia',
        'role'      => 'form',
        'default'   => false,
        'type'      => 'file'
    ]) ?>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('name', [
                'div'           => false,
                'label'         => 'Razão Social*',
                'placeholder'   => 'Razão Social',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('shortname', [
                'div'           => false,
                'maxlength'     => 22,
                'label'         => 'Nome Curto*',
                'placeholder'   => 'Nome Fantasia / Nome de Fachada / Marca Empresarial / Nome popular da empresa',
                'class'         => 'validate[required,maxSize[22]] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('slug', [
                'value'         => 'logfitness.com.br/',
                'div'           => false,
                'disabled'      => true,
                'readonly'      => true,
                'label'         => 'URL Personalizada*',
                'placeholder'   => 'nome-personalizado-da-sua-academia',
                'class'         => 'slug-academia-texto form-control text-right',
            ]) ?>
        </div>
        <div class="col-lg-5">
            <?= $this->Form->input('slug', [
                'div'           => false,
                'label'         => '.',
                'placeholder'   => 'nome-personalizado-da-sua-academia',
                'class'         => 'validate[required,custom[slug]]] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-4">
            <?= $this->Form->input('cnpj', [
                'div'           => false,
                'label'         => 'CNPJ*',
                'placeholder'   => 'CNPJ',
                'class'         => 'validate[required,custom[cnpj]] form-control cnpj',
            ]) ?>
        </div>

        <div class="col-lg-4">
            <?= $this->Form->input('ie', [
                'div'           => false,
                'label'         => 'Inscrição Estadual*',
                'placeholder'   => 'Inscrição Estadual',
                'class'         => 'validate[optional] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('contact', [
                'div'           => false,
                'label'         => 'Responsável*',
                'placeholder'   => 'Nome do Responsável',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('phone', [
                'div'           => false,
                'label'         => 'Telefone*',
                'placeholder'   => 'Telefone',
                'class'         => 'validate[required] form-control phone-mask',
            ]) ?>
        </div>

        <div class="col-lg-3">
            <?= $this->Form->input('mobile', [
                'div'           => false,
                'label'         => 'Celular/WhatsApp',
                'placeholder'   => 'Celular/WhatsApp',
                'class'         => 'validate[optional] form-control phone-mask',
            ]) ?>
        </div>

        <div class="col-lg-2">
            <?= $this->Form->input('alunos', [
                'div'           => false,
                'type'          => 'number',
                'label'         => 'Quantidade de alunos*',
                'placeholder'   => 'Quantidade de alunos',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <hr>
    <div class="form-group row">
        <div class="col-lg-12">

            <h4 class="font-bold"><i class="fa fa-soccer-ball-o"></i> Informe quais modalidades a academia oferece</h4>
        </div>
        <div class="col-lg-12">
            <?php
            foreach($esportes_list as $ke => $esporte):

                echo '<div class="col-lg-4 font-bold"><label for="esportes['.$ke.']">';
                echo $this->Form->checkbox('esportes.'.$ke, [
                    'hiddenField'   => 'N',
                    'value'         => 'Y',
                    'label'         => false,
                    'class'         => 'custom-checkbox'
                ]);
                echo ' '.$esporte;
                echo '</<label></div>';

            endforeach;
            ?>
        </div>
    </div>

    <hr>
    <div class="form-group row">
        <div class="col-lg-5">
            <?= $this->Form->input('email', [
                'div'           => false,
                'label'         => 'Email*',
                'placeholder'   => 'Email',
                'class'         => 'validate[required,custom[email]] form-control',
            ]) ?>
        </div>

        <div class="col-lg-3">
            <?= $this->Form->input('password', [
                'div'           => false,
                'label'         => 'Senha*',
                'placeholder'   => 'Senha para acesso',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <hr>
    <div class="form-group row">
        <div class="col-lg-2">
            <?= $this->Form->input('cep', [
                'div'           => false,
                'label'         => 'CEP*',
                'placeholder'   => 'CEP',
                'class'         => 'validate[required] form-control cep',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <?= $this->Form->input('address', [
                'div'           => false,
                'label'         => 'Endereço*',
                'placeholder'   => 'Endereço',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>

        <div class="col-lg-2">
            <?= $this->Form->input('number', [
                'div'           => false,
                'label'         => 'Número*',
                'placeholder'   => 'Número',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('complement', [
                'div'           => false,
                'label'         => 'Complemento',
                'placeholder'   => 'Complemento',
                'class'         => 'validate[optional] form-control',
            ]) ?>
        </div>
        <div class="col-lg-5">
            <?= $this->Form->input('area', [
                'div'           => false,
                'label'         => 'Bairro*',
                'placeholder'   => 'Bairro',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('uf_id', [
                'id'            => 'states',
                'div'           => false,
                'label'         => 'Estado*',
                'options'       => $states,
                'empty'         => 'Selecione um estado',
                'placeholder'   => 'Estado',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>

        <div class="col-lg-5">
            <?= $this->Form->input('city_id', [
                'id'            => 'city',
                'div'           => false,
                'label'         => 'Cidade*',
                'empty'         => 'Selecione uma cidade',
                'placeholder'   => 'Cidade',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <hr>
    <div class="form-group row">
        <div class="col-lg-5">
            <?= $this->Form->input('imagem', [
                'id'            => 'image_upload',
                'type'          => 'file',
                'div'           => false,
                'label'         => 'Logotipo',
                'placeholder'   => 'Logotipo',
                'class'         => 'validate[custom[validateMIME[image/jpeg|image/png]]] form-control',
            ]) ?>
        </div>

        <div class="col-lg-3">
            <?= $this->Html->image('default.png', [
                'id'    => 'image_preview',
                'alt'   => 'Image Upload',
            ])?>
        </div>
    </div>

    <hr>
    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Html->link('Leia os Termos',
                '/files'.DS.'Termos-de-Aceite.pdf')
            ;?>
            <?= $this->Form->input('aceite_termos', [
                'type' => 'checkbox',
                'label' => 'Li e aceito os termos de aceite'
            ])?>

        </div>
    </div>
    <div class="form-group row">

        <div class="col-lg-2">
            <?= $this->Form->button('Enviar Cadastro', [
                //'id'            => 'submit-cadastrar-academia',
                'type'          => 'submit',
                'div'           => false,
                'class'         => 'btn btn-default float-right',
            ]) ?>

        </div>

        <div class="col-lg-5" id="form-toggle-message"></div>
    </div>

    <?= $this->Form->end() ?>
</div>


<div class="container info-central-box">
    <p class="font-12">
            <span>
                Você terá a sua página personalizada (www.logfitness.com.br/SuaAcademia) com seu logo e uma variedade
                gigante de produtos nacionais e importados, que ficam no nosso estoque. Você não vai comprar nem vender
                nada, só divulgar o seu logfitness. Preencha seu cadastro que iremos entrar em contato e te explicar TUDO!
            </span>
    </p>
</div>

<!-- INFORMAÇÕES  -->
<div class="container">

    <?= $this->Element('bloco_seja_um_parceiro')?>

    <?= $this->Element('bloco_atletas')?>

    <?= $this->Element('bloco_central_relacionamento')?>

</div>