<div class="container informacoes text-center">
    <div class="col-lg-6 atleta">
        <?= $this->Html->image('atletas.png', ['alt' => 'Atletas'])?>
        <br class="clear">
        <div class="absolut-center-100">
            <span class="titulo">ATLETAS</span>
        </div>
    </div>
    <div class="col-lg-6 atletas" style="margin-bottom: 60px;">
        <p>
            Você agora recebe seus suplementos e produtos naturais direto no seu treino! Deixe a motivação e esforço só
            para os exercícios. A logfitness é a primeira plataforma no mundo a aliar o preço e variedade do e-commerce
            com a segurança e atendimento personalizado de uma loja física. É muito simples. Faça seu cadastro como cliente
            e selecione a SUA academia para receber os produtos. Você não paga frete nenhum e retira seus pedidos direto no
            seu treino. Se a sua academia não está cadastrada, nos indique para você poder comprar na logfitness.
        </p>
        <div class="absolut-center-100">
            <a id="form-toggle-open">INDIQUE SUA ACADEMIA</a>
        </div>
    </div>
</div>

<div class="container info-central-box form-toggle" id="form-toggle">

    <!-- <div class="col-lg-4 form-toggle-info">
         Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam beatae culpa cum dolorum eos error incidunt inventore, natus odio, optio porro praesentium quae quis velit. Iusto magnam sint velit.
         <br>
         <br>
         Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam beatae culpa cum dolorum eos error incidunt inventore, natus odio, optio porro praesentium quae quis velit. Iusto magnam sint velit.
     </div>-->

    <?= $this->Form->create(null, [
        'id'        => 'form-cadastrar-atleta',
        'role'      => 'form',
        'default'   => false,
        'type'      => 'file'
    ]) ?>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('name', [
                'div'           => false,
                'label'         => 'Nome da Academia*',
                'placeholder'   => 'Nome da Academia',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('telephone', [
                'div'           => false,
                'label'         => 'Telefone*',
                'placeholder'   => 'Telefone',
                'class'         => 'validate[required] form-control phone-mask',
            ]) ?>
        </div>

        <div class="col-lg-5">
            <?= $this->Form->input('responsavel', [
                'div'           => false,
                'label'         => 'Responsável pela Academia',
                'placeholder'   => 'Responsável pela Academia',
                'class'         => 'validate[optional] form-control',
            ]) ?>
        </div>

    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('uf_id', [
                'id'            => 'states',
                'div'           => false,
                'label'         => 'Estado*',
                'options'       => $states,
                'empty'         => 'Selecione um estado',
                'placeholder'   => 'Estado',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>

        <div class="col-lg-5">
            <?= $this->Form->input('city_id', [
                'id'            => 'city',
                'div'           => false,
                'label'         => 'Cidade*',
                'empty'         => 'Selecione uma cidade',
                'placeholder'   => 'Cidade',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">

        <div class="col-lg-2">
            <?= $this->Form->button('Enviar Indicação de Academia', [
                'id'            => 'submit-cadastrar-atleta',
                'type'          => 'submit',
                'div'           => false,
                'class'         => 'btn btn-default float-right',
            ]) ?>

        </div>

        <div class="col-lg-5" id="form-toggle-message"></div>
    </div>

    <?= $this->Form->end() ?>
</div>


<div class="container info-central-box">
    <!-- <p class="font-12">
           <span>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut dolor ducimus esse fuga harum obcaecati reprehenderit
                similique! Ab adipisci, aliquam aliquid eos fugit illo maxime non nostrum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut dolor ducimus esse fuga harum obcaecati reprehenderit similique! Ab adipisci, aliquam aliquid eos fugit illo maxime non nostrum quaerat sed sint?
            </span>

    </p> -->
</div>

<!-- INFORMAÇÕES  -->
<div class="container">
    <?= $this->Element('bloco_seja_um_parceiro')?>

    <?= $this->Element('bloco_academias')?>

    <?= $this->Element('bloco_central_relacionamento')?>
</div>