 <!DOCTYPE html>
 <html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<title>Logfitness - Solução para venda de suplementos</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Oswald|Open+Sans" rel="stylesheet">
		<?= $this->Html->css('font-awesome.min')?>
		<?= $this->Html->css('estilera')?>
	</head>
	<body>
		<div class="container-fluid">
			<nav class="navbar" style="background-color: transparent; position: fixed; top: 0; left: 0; right: 0; z-index: 2">
				<?= $this->Html->link( $this->Html->image('logfitness.png',[ 'class' => 'logo-navbar' ]), WEBROOT_URL, ['escape' => false]) ?>
			</nav>
		</div>
	     <section class="home">
	     	<div class="row no-gutters">
     			<div class="col">
     				<?= $this->Html->image('landing-lojas/revolucione.png',['width' => '100%'])  ?>
     			</div>
	     	</div>
	     </section>
		 <section class="intro">
  		 	 <div class="container">
			 	 <div class="row">
					 <div class="col-12 col-sm-4">
					     <div class="text-center">
					     	<?= $this->Html->image('landing-lojas/casinha.png', ['alt' => 'Seu estoque e logística sendo replicados para seus parceiros', 'class' => 'img-intro'])  ?>
					     	<h4>Seu estoque sendo replicado para seus parceiros</h4>
					     </div>
					 </div>
					 <div class="col-12 col-sm-4">
					     <div class="text-center">
					     	<?= $this->Html->image('landing-lojas/carrinho.png', ['alt' => 'Crie lojas para as academias da sua cidade', 'class' => 'img-intro'])  ?>
					     	<h4>Crie lojas para as academias da sua cidade</h4>
					     </div>
					 </div>
					 <div class="col-12 col-sm-4">
					     <div class="text-center">
					     	<?= $this->Html->image('landing-lojas/pessoa.png', ['alt' => 'Eles vendem seus produtos na loja deles.', 'class' => 'img-intro'])  ?>
						 	<h4>Elas vendem seus produtos na loja deles!</h4>
					     </div>
					 </div>
				 </div>
				 <div class="row justify-content-center explique">
					 <div class="col-12 col-sm-8 text-center">
			 			 <p class="maiorzinho">Suas vendas não são restritas apenas para as academias. Também é possível cadastrar os professores e ter mais pessoas vendendo. Você pode, por exemplo, oferecer uma porcentagem do valor vendido para motivá-los!</p>
			 			 <?= $this->Html->image('landing-lojas/baixocusto.png', ['class' => 'img-fluid mobile-hide'])  ?>	
			 		 </div>
				 </div>
			 </div>
  		 </section>
  		 
		 <section class="descomplica text-center">
			 <div class="container">
			     <div class="row justify-content-center">
				     <div class="col-12 col-sm-8 text-center">
				     	<?= $this->Html->image('landing-lojas/descomplica.gif', ['class' => 'img-fluid'])  ?>
						<br>
						<p>E para utilizar essa ferramenta incrível você não precisa manjar de tecnologia. Nosso painel é de fácil compreensão e simples de administrar. Desde o cadastro das academias e time até o pagamento de comissões. As páginas das academias você também cria de maneira simples e rápida, com apenas um cadastro! Massa, né? Além de todas essas facilidades pra você vender muito, é importante manter um bom relacionamento com as academias da sua cidade, criar incentivo! Assim, elas se sentem motivadas a vender mais e vocês ganham juntos.</p>
				     </div>
			     </div>
			 </div>
 		 </section>
		 
		 <section class="mais">
			 <div class="container">
				 <div class="row">
				     <div class="col-12 col-sm-6 text-justify">
				         <p>Além de todas as ferramentas para facilitar o gerenciamento, nossa equipe se reinventa constantemente. <strong>Buscamos sempre lançar ferramentas que agreguem valor ao site e desperte a curiosidade do aluno que vai comprar suplementos.</strong> Não paramos nunca! Quem não teve dúvida do custo/benefício do produto que estava comprando em relação ao de outras marcas? Pra isso criamos um comparador super completo para os itens da sua loja. E quanto a venda de combos? Nem sempre era aquilo que o consumidor queria, né? Pra isso montamos a máquina de combo. Nela, o próprio consumidor monta seu combo. Ah, e claro! O professor pode fazer indicação de produtos de acordo com o treino do aluno e mandar a lista completa pra ele por e-mail ou Whatsapp. </p>
				         <div class="col-12">
							 <div class="row justify-content-center">
								 <div class="col-12 col-sm-4 text-center">
								 	<?= $this->Html->image('landing-lojas/comparador.png', ['class' => 'img-fluid', 'alt' => 'Comparador de suplementos da Log']) ?>
								 	<h4>Comparador de Suplementos</h4>
								 </div>
								 <div class="col-12 col-sm-4 text-center">
								 	<?= $this->Html->image('landing-lojas/maquinadecombo.png', ['class' => 'img-fluid', 'alt' => 'Maquina de combo da Log']) ?>
								 	<h4>Máquina de Combo</h4>
								 </div>
								 <div class="col-12 col-sm-4 text-center">
								 	<?= $this->Html->image('landing-lojas/indicacao.png', ['class' => 'img-fluid', 'alt' => 'Indicar produtos Log']) ?>
								 	<h4>Indicar produtos</h4>
								 </div>
					         </div>
				         </div>
				     </div>
				     <div class="col-12 col-sm-6 computer">
				     	<?= $this->Html->image('landing-lojas/computer.gif', ['class' => 'img-fluid', 'style' => 'position: relative; top: -30px'])  ?>
			         </div>
				 </div>
			 </div>
		 </section>


		<div class="padding">
		</div>
 		<section class="blue-bg mobile-hide">
		 		<div class="container">
				<div class="row justify-content-center align-items-center" style="height: 400px;">
					<div class="col-12 col-sm-9 text-center">
		 				<h1>Que tal multiplicar suas vendas?</h1>
	 		 			<p class="multiplique">Já parou pra pensar o quanto seu faturamento iria aumentar se você tivesse uma loja em cada academia da sua cidade? Com a log isso é possível!</p>
					</div>
				</div>
		 		</div>
  		 </section>


		<section class="contato container" style="padding-bottom: 35px;">
	 		<div class="row">
	 			<div class="col-12">
	 				<?= $this->Flash->render() ?>
	 			</div>
	 	 		<div class="col-12 col-sm-8">
		 	 		<h1>Quer saber mais?</h1>
		 	 		<p>Solicite uma conversa com nossa equipe de atendimento. <br> É só preencher o formulário abaixo e nós retornaremos o contato!</p>
	 			</div>
	 			<div class="w-100"></div>
	 	 		<div class="col-12 col-sm-8">
 	 			<?= $this->Form->create(null,['url' => ['action' => 'interesse_lojas']]) ?>
			  		<div class="form-group">
						<?= $this->Form->input('name', [
                            'div'           => false,
                            'label'         => false,
                            'class'         => 'form-control',
                            'placeholder'   => 'Nome completo*',
                            'required'      => true
                    	])?>
			  		</div>
		  		</div>
		  		<div class="w-100"></div>
	 	 		<div class="col-12 col-sm-4">
			  		<div class="form-group">
			  			<?= $this->Form->input('email', [
                            'div'           => false,
                            'label'         => false,
                            'class'         => 'form-control',
                            'placeholder'   => 'Seu melhor e-mail*',
                            'type'          => 'email',
                            'required'      => true
                        ])?>
			  		</div>
	 	 		</div>
	 	 		<div class="col-12 col-sm-4">
	 	 			<?= $this->Form->input('phone', [
                        'id'            => 'phone',
                        'div'           => false,
                        'label'         => false,
                        'class'         => 'form-control',
                        'placeholder'   => 'Telefone*',
                        'type'          => 'tel',
                        'required'      => true
                    ])?>
				</div>
				<div class="w-100"></div>
	 	 		<div class="col-12 col-sm-5">
	 	 			<div class="form-group">
		 	 			<?= $this->Form->input('uf_id', [
	                        'label'         => false,
	                        'id'            => 'states-lojas',
	                        'div'           => false,
	                        'options'       => $states,
	                        'empty'         => 'Selecione um estado',
	                        'placeholder'   => 'Estado',
	                        'class'         => 'form-control',
	                        'required'      => false
	                    ]) ?>
	                </div>
	 	 		</div>
	 	 		<div class="col-12 col-sm-3">
	 	 			<div class="form-group">
						<?= $this->Form->input('city_id', [
	                        'label'         => false,
	                        'id'            => 'city-lojas',
	                        'div'           => false,
	                        'empty'         => 'Selecione uma cidade',
	                        'placeholder'   => 'Cidade',
	                        'class'         => 'form-control',
	                        'disabled'       => true,
	                        'required'      => false
	                    ]) ?>
	                </div>
	 	 		</div>
	 	 		<div class="w-100"></div>
	 	 		<div class="col">
	 	 			<div class="form-group">
		 	 			<?= $this->Form->button('Enviar', [
	                        'type'      => 'submit',
	                        'class'     => 'btn btn-outline-info',
	                        'escape'    => false
	                    ]) ?>
                	</div>
				<?= $this->Form->end() ?>
	 	 		</div>
	     	</div>
		</section>
		<script>
            var WEBROOT_URL  = "<?= WEBROOT_URL ?>";
        </script>
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<script>
            $('#states-lojas').on('change', function(){
                var state_id = $(this).val();
                console.log(WEBROOT_URL + '/cidades/' + state_id);
                $.get(WEBROOT_URL + '/cidades/' + state_id,
                    function(data){
                        $('#city-lojas').html(data);
                    });
            });
        </script>
		<script>
            $('select[name="uf_id"]').on('change', function() {
                var estadoId = $(this).val();
                if(estadoId >= 1){
                  setTimeout(function() { 
                    $('#city-lojas').removeAttr('disabled');
                  }, 500);
                }
                else{
                  console.log('não passou no if');
                }
            });
        </script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	 </body>
 </html>