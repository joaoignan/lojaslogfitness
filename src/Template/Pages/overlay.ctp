<?= $this->Html->css('overlay') ?>
<?= $this->Html->css('bootstrap.min') ?>
<?= $this->Html->css('novo-main') ?>
<?= $this->Html->css('font-awesome.min') ?>
<?= $this->Html->script('jquery.min') ?>

<script>

	$(document).ready(function(){
		$('.open_overlay').click(function(){
			// atribui o valor do data-id na variável
			// var id_overlay = $(this).attr('data-id');
			$('.overlay').animate({'height' : '100%'}, 500);
			$('html,body').css({"overflow":"hidden"});
		});
		$('.close_overlay, .overlay').click(function(){
			$('.overlay').animate({'height' : '0'}, 500);
			$('html,body').css({"overflow":"auto"});
		});

		// ANTIGO
		function openPadrao() {
	        document.getElementById("padrao").style.height = "100%";
	        $("html,body").css({"overflow":"hidden"});
	    };
	    function closePadrao() {
	        document.getElementById("padrao").style.height = "0%";
	        $("html,body").css({"overflow":"auto"});
	    };
	});	
</script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

<div class="text-center">
	<?= $this->Element('banners_modelo1_home') ?>
	<div class="container">
		<div class="col-xs-12">
			<button class="open_overlay">Abrir</button>
		</div>	
	</div>
</div>
<div id="padrao" class="overlay">
    <div class="overlay-content">
    	<a href="javascript:void(0)" class="closebtn close_overlay">&times;</a>
			<h1>h1</h1>
			<h2>h2</h2>
			<h3>h3</h3>
			<h4>h4</h4>
			<h5>h5</h5>
			<h6>h6</h6>
			<p>Parágrago</p>
			<a href="#">link</a>
			<p><i class="fa fa-facebook"></i> font awesome</p>
			<hr>
    </div>
</div>