<!DOCTYPE html>
<html lang="pt_br">
	<head>

		<title>LOGFITNESS</title>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="">
		<meta name="description" content="">

    	<?= $this->Html->css('bootstrap.min.css')?>
    	<?= $this->Html->css('academia_new_animate.min.css')?>
    	<?= $this->Html->css('academia_new_bootstrap.min.css')?>
    	<?= $this->Html->css('font-awesome.min.css')?>
    	<?= $this->Html->css('academia_new_templatemo-style.css')?>
    	<?= $this->Html->css('media_queries.min.css')?>
    	<?= $this->Html->css('owl.carousel.css')?>
    	<?= $this->Html->css('owl.theme.css')?>

		<style>
			.fa-index{
				color: #f4d637;
			}
			.logo_navbar{
            	width: 170px;
        	}
		</style>

    	<script>
        <?= file_get_contents(JS_URL.'vendor/jquery-1.8.2.min.js') ?>
        <?= file_get_contents(JS_URL.'vendor/jquery-1.11.2.min.js') ?>
        <?= file_get_contents(JS_URL.'vendor/modernizr-2.8.3-respond-1.4.2.min.js') ?>
        <?= file_get_contents(JS_URL.'owl-carousel/owl.carousel.js') ?>
        </script>

	</head>
	<body>
		<div id="submenu"></div>
		<!-- start navigation -->
        <nav class="navbar navbar-default navbar-fixed-top templatemo-nav" role="navigation">
            <div class="container">
                <div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<a href="#home" class="navbar-brand"><?= $this->Html->image('logfitness.png', ['class' => 'logo_navbar'])?></a>
				</div>
                <div class="menu-navbar">
					<ul class="nav navbar-nav navbar-right text-uppercase">
						<li><a href="#home2" class="menu-home">Início</a></li>
						<li><a href="#divider" class="menu-vantagens">Vantagens</a></li>
						<li><a href="#funciona" class="menu-funciona">Como Funciona</a></li>
						<li><a href="#depoimentos" class="menu-depoimentos">Depoimentos</a></li>
						<li><a href="http://www.logfitness.com.br/professores/acesso">Login</a></li>
						<li><a href="#tenhalog" id="btn_cadastrar_menu" class="menu-cadastrar">Cadastre-se</a></li>
					</ul>
				</div>
			</div>
		</nav>

		<!-- end navigation -->
		<!-- start home2 -->
		<section id="home2">
			<div class="overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?= $this->Html->image('texto_topo.png', ['alt' => 'Prática Interativa & integrada', 'id' => 'txt_topo']); ?>
						</div>
					</div>
				</div>
			</div>
			<?= $this->Html->image('pcs_site.png', ['alt' => 'Homepage index', 'id' => 'computadores']); ?>
		</section>
		<!-- end home2 -->

		<!-- start sobre -->
		<section id="sobre">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
					<br/><p>Conheça a <strong>plataforma online</strong><br/> que está mudando a maneira de<br/> se manter uma vida <strong>mais saudável.</strong></p>
					</div>

					<div class="col-md-12">
                    <p><br/>A <strong>LOGFITNESS</strong> aliou o <strong>preço, variedade e comodidade</strong> do <span style="white-space: nowrap"> e-commerce</span><br/> com o a <strong>segurança</strong> e <strong>atendimento personalizado</strong> de uma loja física.<br/> Proporcionando uma experiência completamente <strong>NOVA</strong>.</p>
                    </div>
				</div>
			</div>
		</section>
		<!-- end sobre -->

		<!-- Inicio Vantagens -->
		<section id="divider">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_vantagens" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_vantagens">Fazer parte do nosso time é vantajoso<br/>
							para você professor e para seus alunos.</p>
						</div>
					</div>
					<div class="col-md-12">
                		<div class="fundo_vantagens5"><p class="eftspacing"><strong>professor & aluno</strong><br/><br/>
                		COM O SEU CONHECIMENTO NA HORA DE<br/>
						ESCOLHER OS PRODUTOS A RELAÇÃO ENTRE<br/>
						PROFESSOR E O ALUNO TEM UM NOVO PATAMAR.</p></div>
            			<div class="fundo_vantagens6"><p class="eftspacing"><strong>mais completo</strong><br/><br/>
            			A PLATAFORMA PERMITE QUE VOCÊ DÊ AINDA MAIS<br/>
						SUPORTE AOS SEUS ALUNOS OFERECENDO UM NOVO<br/>
						JEITO DE SE MANTER UMA VIDA MAIS SAUDÁVEL.</p></div>
					</div>
					<div class="col-md-12">
            			<div class="fundo_vantagens7"><p class="eftspacing"><strong>fonte de recursos</strong><br/><br/>
            			QUANTO MAIS SEUS ALUNOS<br/>
						COMPRAREM, MAIOR SERÁ O SEU GANHO.<br/>
						AUMENTE OS SEUS LUCROS!</p></div>
            			<div class="fundo_vantagens8"><p class="eftspacing"><strong>e muito mais</strong><br/><br/>
            			AS VANTAGENS DE SER LOG NÃO<br/>
						PARAM POR AQUI. EXPLORE NOSSO MUNDO<br/>
						E DESCUBRA O PORQUE FAZER PARTE DESTE TIME.</p></div>
            		</div>
				</div>
			</div>
		</section>
		<!-- end vantagens -->

		<!-- start professor -->
		<section id="professor">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_professor_comissionado" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_professor_comissionado">Quando a venda é feita através do perfil da<br/>
								academia e o aluno seleciona um professor a<br/>
								comissão é dividida ao meio.<br/>
								Metade academia, metade professor.</p>
						</div>
					</div>
					<div class="col-md-6 ">
						<br/>
						<br/>
						<p id="txt_professor" style="margin-top: 30px;">O professor é quem conhece melhor o treino e as necessidades do aluno.</p><br/>
						<p id="txt_professor"><strong>TODOS</strong> ganham. Academia aumentando as vendas dentro do seu perfil dentro da LOG, o professor atendendo melhor seu aluno e o aluno tendo os melhores resultados assistido por quem ele mais confia!</p>
					</div>
					<div class="col-md-6"  id="img_professor">
						<img src="//logfitness.r.worldssl.net/webroot/images/professor.jpg" class="img-responsive" alt="feature img">
					</div>
				</div>
			</div>
		</section>
		<!-- end professor -->

		<!-- start funciona -->
		<section id="funciona">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_como_funciona" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_como_funciona">A LOG permite que você professor se<br/>
							vincule à academias credenciadas e<br/>
							indique os melhores produtos para os<br/>
							seus alunos através daquela academia</p>
						</div>
					</div>
					<div class="col-md-3 wow fadeInUp templatemo-box" data-wow-delay="0.3s">
                    		<span><?= $this->Html->image('icone5.png', ['alt' => 'O professor faz o seu cadastro na plataforma', 'id' => 'img_funciona']); ?></span>
                    		<p align="center" id="txt_funciona"><br/>O PROFESSOR FAZ O SEU CADASTRO NA PLATAFORMA.</p>
					</div>
					<div class="col-md-3 wow fadeInUp templatemo-box" data-wow-delay="0.3s">
                    		<span><?= $this->Html->image('icone2.png', ['alt' => 'Após a aprovação, o professor recebe seu login e senha de acesso', 'id' => 'img_funciona']); ?></span>
                    		<p align="center" id="txt_funciona"><br/>APÓS A APROVAÇÃO, O PROFESSOR RECEBE SEU LOGIN E SENHA DE ACESSO.</p>
					</div>
					<div class="col-md-3 wow fadeInUp templatemo-box" data-wow-delay="0.3s">
                    		<span><?= $this->Html->image('icone6.png', ['alt' => 'Está na hora de se vincular às academias credenciadas que você faz parte', 'id' => 'img_funciona']); ?></span>
                    		<p align="center" id="txt_funciona"><br/>ESTÁ NA HORA DE SE VINCULAR ÀS ACADEMIAS CREDENCIADAS QUE VOCÊ FAZ PARTE</p>
					</div>
					<div class="col-md-3 wow fadeInUp templatemo-box" data-wow-delay="0.3s">
                    		<span><?= $this->Html->image('icone4.png', ['alt' => 'Agora você está pronto para indicar os melhores produtos para os seus alunos!', 'id' => 'img_funciona']); ?></span>
                    		<p align="center" id="txt_funciona"><br/>AGORA VOCÊ ESTÁ PRONTO PARA INDICAR OS MELHORES PRODUTOS PARA OS SEUS ALUNOS!</p>
					</div>
				</div>
			</div>
		</section>
		<!-- end funciona -->

		<!-- start video institucional -->
		<section id="video">
			<div class="container">
				<div class="row">
					<div class="video-container">
						<iframe width="100%" height="315" src="https://www.youtube.com/embed/Nr7UNxpzO18" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</section>
		<!-- end video institucional -->

		<!-- start depoimentos-->
		<section id="depoimentos">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_depoimentos" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_depoimentos">Separamos o depoimento de algumas<br/>
							pessoas que já fazem parte do nosso time.<br/>
							<strong>CONFIRA!</strong></p>
						</div>
						<div id="bg-asset"></div>
						<div id="owl-demo" class="owl-carousel owl-theme">
							<div class="item"><?= $this->Html->image('depoimento1.jpg',['alt' => 'Depoimento 1',]); ?></div>
							<div class="item"><?= $this->Html->image('depoimento4.jpg',['alt' => 'Depoimento 4',]); ?></div>
							<div class="item"><?= $this->Html->image('depoimento2.jpg',['alt' => 'Depoimento 2',]); ?></div>
							<div class="item"><?= $this->Html->image('depoimento3.jpg',['alt' => 'Depoimento 1',]); ?></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end depoimentos -->

		<!-- start tenhalog-->
		<section id="tenhalog">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_tenha_log" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_tenha_log">Preencha as informações abaixo e<br/>
							envie o formulário de solicitação ou<br/>
							tire suas dúvidas com nosso contato.</p>
						</div>
					</div>
					<div class="col-md-4 col-md-offset-2">
						<input type="button" class="botoes" id="btn_cadastrar" value="Cadastrar">
					</div>
					<div class="col-md-4">
						<input type="button" class="botoes" id="btn_duvidas" value="Tirar dúvidas">
					</div>
				</div>
			</div>
		</section>
		<!-- end tenhalog -->

		<!-- start cadastre-se-->
		<section id="cadastre-se">
			<div class="overlay">
				<div class="container">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <?= $this->Form->create(null, [
                            'id'        => 'form-cadastrar-professor',
                            'role'      => 'form',
                            'default'   => false,
                            'type'      => 'file'
                        ]) ?>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	                            <?= $this->Form->input('name', [
                                    'div'           => false,
                                    'label'         => 'Nome*',
                                    'placeholder'   => 'Nome Completo',
                                    'class'         => 'validate[required] form-control',
                                ]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	                            <?= $this->Form->input('valid_name', [
                                    'div'           => false,
                                ]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                <?= $this->Form->input('cpf', [
                                    'div'           => false,
                                    'label'         => 'CPF*',
                                    'placeholder'   => 'CPF',
                                    'class'         => 'validate[required,custom[cpf]] form-control cpf',
                                ]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                <?= $this->Form->input('rg', [
                                    'div'           => false,
                                    'label'         => 'RG*',
                                    'placeholder'   => 'RG',
                                    'class'         => 'validate[optional] form-control',
                                ]) ?>
                            </div>
                        </div>
						<div class="form-group row">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <?= $this->Form->input('cref', [
                                    'div'           => false,
                                    'label'         => 'CREF*',
                                    'placeholder'   => 'CREF',
                                    'class'         => 'validate[optional] form-control',
                                ]) ?>
                            </div>                           
                            <div class="col-xs-12 col-sm-6 col-md-offset-2 col-md-3 col-lg-offset-2 col-lg-3">
                                <?= $this->Form->input('phone', [
                                    'div'           => false,
                                    'label'         => 'Telefone*',
                                    'placeholder'   => 'Telefone',
                                    'class'         => 'validate[required] form-control phone-mask',
                                ]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                <?= $this->Form->input('mobile', [
                                    'div'           => false,
                                    'label'         => 'Celular/WhatsApp',
                                    'placeholder'   => 'Celular/WhatsApp',
                                    'class'         => 'validate[optional] form-control phone-mask',
                                ]) ?>
                            </div>
                        </div>
    <hr> <!--Divisória-->
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <?= $this->Form->input('email', [
                                    'div'           => false,
                                    'label'         => 'Email*',
                                    'placeholder'   => 'Email',
                                    'class'         => 'validate[required,custom[email]] form-control',
                                ]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <?= $this->Form->input('password', [
                                    'div'           => false,
                                    'label'         => 'Senha*',
                                    'placeholder'   => 'Senha para acesso',
                                    'class'         => 'validate[required] form-control',
                                ]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
									<?= $this->Form->input('password_confirm', [
										'div'           => false,
										'label'         => 'Repetir senha*',
										'placeholder'   => 'Confirme sua senha',
										'class'         => 'validate[required] form-control',
										'type'          => 'password',
									]) ?>
									<br/>
								</div>
                        </div>
    <hr> <!--Divisória-->
                        <div class="form-group row">
                            <div class="col-xs-8 col-sm-6 col-md-2 3col-lg-2">
                                <?= $this->Form->input('cep', [
                                    'div'           => false,
                                    'label'         => 'CEP*',
                                    'placeholder'   => 'CEP',
                                    'class'         => 'validate[required] form-control cep',
                                ]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                <?= $this->Form->input('address', [
                                    'div'           => false,
                                    'label'         => 'Endereço*',
                                    'placeholder'   => 'Endereço',
                                    'class'         => 'validate[required] form-control',
                                    ]) ?>
                            </div>
                            <div class="col-xs-5 col-sm-4 col-md-2 col-lg-2">
                                <?= $this->Form->input('number', [
                                    'div'           => false,
                                    'label'         => 'Número*',
                                    'placeholder'   => 'Número',
                                    'class'         => 'validate[required] form-control',
                                ]) ?>
                            </div>       
                            <div class="col-xs-7 col-sm-6 col-md-6 col-lg-5">
                                <?= $this->Form->input('complement', [
                                    'div'           => false,
                                    'label'         => 'Complemento',
                                    'placeholder'   => 'Complemento',
                                    'class'         => 'validate[optional] form-control',
                                ]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-7">
                                <?= $this->Form->input('area', [
                                    'div'           => false,
                                    'label'         => 'Bairro*',
                                    'placeholder'   => 'Bairro',
                                    'class'         => 'validate[required] form-control',
                                ]) ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                                <?= $this->Form->input('uf_id', [
                                    'id'            => 'states',
                                    'div'           => false,
                                    'label'         => 'Estado*',
                                    'options'       => $states,
                                    'empty'         => 'Selecione um estado',
                                    'placeholder'   => 'Estado',
                                    'class'         => 'validate[required] form-control',
                                    ]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-9">
                                <?= $this->Form->input('city_id', [
                                    'id'            => 'city',
                                    'div'           => false,
                                    'label'         => 'Cidade*',
                                    'empty'         => 'Selecione uma cidade',
                                    'placeholder'   => 'Cidade',
                                    'class'         => 'validate[required] form-control',
                                    ]) ?>
                            </div>
                        </div>
                        <div class="form-group row">
                        	<div class="col-md-3">
								<?= $this->Html->link('Leia os Termos',
									'/files'.DS.'Termos-de-Aceite.pdf',
									['target' => '_blank'])
								;?>
							</div>
							<div class="col-md-4">
								<label for="aceite-termos">&nbsp&nbsp&nbsp&nbspLi e aceito os termos de aceite
									<?= $this->Form->input('aceite_termos', [
										'type' => 'checkbox',
										'label' => false,
										'class' => 'text-center checkbox-aval validate[required]',
										'style' => 'margin: -27px 0'
									])?>
								</label>
							</div>
                            <div class="col-md-2">
                                <?= $this->Form->button('Enviar Cadastro', [
                                    'id'            => 'submit-cadastrar-professor',
                                    'type'          => 'submit',
                                    'div'           => false,
                                    'class'         => 'btn btn-default float-right',
                                ]) ?>
                            </div>
                            <div class="col-md-5" id="form-toggle-message"></div>
                        </div>
                        <?= $this->Form->end() ?>
                    </div>
            	</div>
            </div>
		</section>
		<!-- end cadastre-se -->

		<!-- start contact -->
		<section id="duvidas">
			<div class="overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="contact-form">
                                <?= $this->Form->create(null, [
                                    'id'        => 'form-central-relacionamento',
                                    'role'      => 'form',
                                    'default'   => false,
                                    'type'      => 'file'
                                ]) ?>
                                <div class="form-group row">
									<div class="col-md-12">
                                        <?= $this->Form->input('name', [
                                            'id'            => 'contato-name',
                                            'div'           => false,
                                            'label'         => 'Nome',
                                            'placeholder'   => 'Nome Completo',
                                            'class'         => 'validate[required] form-control',
                                        ]) ?>
                                        <br/>
									</div>
                                </div>
                                <div class="form-group row">
									<div class="col-md-6">
                                        <?= $this->Form->input('phone', [
                                            'id'            => 'contato-phone',
                                            'div'           => false,
                                            'label'         => 'Telefone',
                                            'placeholder'   => 'Telefone',
                                            'class'         => 'validate[required] form-control phone-mask',
                                        ]) ?>
									</div>

									<div class="col-md-6">
                                        <?= $this->Form->input('email', [
                                            'id'            => 'contato-email',
                                            'div'           => false,
                                            'label'         => 'E-mail',
                                            'placeholder'   => 'E-mail',
                                            'class'         => 'validate[required,custom[email]] form-control',
                                        ]) ?>
                                        <br/>
									</div>
                                </div>
                                <div class="form-group row">
									<div class="col-md-6">
                                        <?= $this->Form->input('description', [
                                            'id'            => 'contato-description',
                                            'div'           => false,
                                            'label'         => 'Assunto',
                                            'placeholder'   => 'Assunto / Descrição',
                                            'class'         => 'validate[required] form-control',
                                        ]) ?>
                                        <br/>
									</div>
                                </div>
                                <div class="form-group row">
									<div class="col-md-12">
                                        <?= $this->Form->input('message', [
                                            'id'            => 'contato-name',
                                            'type'          => 'textarea',
                                            'div'           => false,
                                            'label'         => 'Mensagem',
                                            'placeholder'   => 'Mensagem',
                                            'class'         => 'validate[required] form-control',
                                        ]) ?>
                                        <br/>
									</div>
                                </div>
									<div class="col-md-8">
                                        <?= $this->Form->button('Enviar Contato', [
                                            'id'            => 'submit-cadastrar-central-relacionamento',
                                            'type'          => 'button',
                                            'div'           => false,
                                            'class'         => 'btn btn-default float-right',
                                        ]) ?>
									</div>
                                <div class="col-lg-5" id="form-toggle-message"></div>
							</div>
                            <?= $this->Form->end() ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		 <!-- end contact -->

		<!-- start quemsou -->
		<section id="quemsou">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-xs-12" id="titulo_quem_sou2" >

					</div>
					<div class="col-md-6 col-xs-12">
						<p class="txt_titulos_quem_sou">Se você é um aluno e está buscando<br/>
							pelos melhores produtos do mercado<br/>
							ou é dono de um centro de treinamento<br/>
							querendo dar um UP em sua renda acesse a <br/>
							página que criamos exclusivamente para você !</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 col-md-offset-2">
						<a href="http://www.logfitness.com.br"><input type="button" class="botoes" id="btn_aluno" value="Aluno"></a>
					</div>
					<div class="col-md-4">
                        <a href="http://www.logfitness.com.br/academia"><input type="button" class="botoes" id="btn_professor" value="Academia">
					</div>
				</div>
			</div>
		</section>
		<!-- end quemsou -->

		<!-- start social media-->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:45px; background:#5089cf; margin-bottom: 25px;">
			<div class="row">
				<div class="col-xs-12" style="text-align: center">
					<div class="social_icon">
						<a href="https://open.spotify.com/user/logfitness/playlist/5osNGX93BsYhhJzI94dASW"  target="_blank"><i class="fa fa-2x fa-fw fa-spotify fa-index" title="Acompanhe nossa playlist"></i></a>
					</div>
					<div class="social_icon">
						<a href="https://www.facebook.com/logfitness.com.br" class="btn-social btn-outline" target="_blank"><i class="fa fa-2x fa-fw fa-facebook fa-index" title="Curta nossa fanpage"></i></a>
					</div>
					<div class="social_icon">
						<a href="https://www.instagram.com/logfitness.com.br/" class="btn-social btn-outline" target="_blank"><i class="fa fa-2x fa-fw fa-instagram fa-index" title="Siga nosso Instagram"></i></a>
					</div>
					<div class="social_icon">
						<a href="https://twitter.com/LOGFITNESS" class="btn-social btn-outline" target="_blank"><i class="fa fa-2x fa-fw fa-twitter fa-index" title="Siga nosso Twitter"></i></a>
					</div>	
				</div>
			</div>
		</div>
		<!-- end social media-->

		<!-- start footer -->
		<footer>
			<div class="container">
				<div class="row">
					<p style="font-size:12px;">Copyright ©2016 <span>www.logfitness.com.br</span>. TODOS OS DIREITOS RESERVADOS.
                Todo o conteúdo do site, todas as fotos,imagens, logotipos, marcas, dizeres, som, software, conjunto imagem,
                layout, trade dress, aqui veiculados são de propriedade exclusiva da LogFitness. É vedada qualquer reprodução,
                total ou parcial, de qualquer elemento de identidade, sem expressa autorização. A violação de qualquer direito mencionado
                implicará na responsabilização cível e criminal nos termos da Lei.

                CACIQUE DIGITAL REPRESENTACOES LTDA - ME - CNPJ: 22.921.802/0001-77 Rua Antonio Munia, 141, Sala 01 - Jardim Nazareth - CEP 15054-160 - São José do Rio Preto - SP</p>
				</div>
			</div>
		</footer>
		  <!--   end footer -->
		<script>
		<?= file_get_contents(JS_URL.'jquery.cadastrar.js') ?>
		<?= file_get_contents(JS_URL.'jquery.js') ?>
		<?= file_get_contents(JS_URL.'bootstrap.min.jsbootstrap.min.js') ?>
		<?= file_get_contents(JS_URL.'wow.min.js') ?>
		<?= file_get_contents(JS_URL.'jquery.singlePageNav.min.js') ?>
		<?= file_get_contents(JS_URL.'custom.js') ?>
		</script>

		<script>
			$(document).ready(function(){
				$("#btn_cadastrar").click(function(){
					$("#duvidas").slideUp();
					$("#cadastre-se").slideToggle();
				});
				$("#btn_duvidas").click(function(){
					$("#cadastre-se").slideUp();
					$("#duvidas").slideToggle();
				});
				
				var medida_tela = $(window).width();
				if(medida_tela < 767) {
					//se clicar em um link do navbar -> menu fecha
					$('.nav li a').click(function () {
						$('.menu-navbar').slideUp(750);
					});
					//se clicar no icon do navbar -> menu toggle(abre e fecha)
					$('.navbar-toggle').click(function () {
						$('.menu-navbar').toggle(750);
					});
				}
				$('.nav a[href^="#"]').on('click', function(e) {
					e.preventDefault();
					var id = $(this).attr('href'),
						targetOffset = $(id).offset().top;

					$('html, body').animate({
						scrollTop: targetOffset - 50
					}, 500);
				});
            });
		</script>

		<script>
			$(document).ready(function() {

				$("#owl-demo").owlCarousel({
					autoPlay : 10000,
					navigation : false, // Show next and prev buttons
					slideSpeed : 300,
					paginationSpeed : 400,
					singleItem:true,
					stopOnHover:true

					// "singleItem:true" is a shortcut for:
					// items : 1,
					// itemsDesktop : false,
					// itemsDesktopSmall : false,
					// itemsTablet: false,
					// itemsMobile : false

				});

			});
		</script>

		<!-- Smartsupp Live Chat script -->
    <script type="text/javascript">
    var _smartsupp = _smartsupp || {};
    _smartsupp.key = '472d82fddbe4326ca5661de701a829a7553ce341';
    window.smartsupp||(function(d) {
        var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
        s=d.getElementsByTagName('script')[0];c=d.createElement('script');
        c.type='text/javascript';c.charset='utf-8';c.async=true;
        c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
    })(document);
    </script>

	</body>
</html>
