<head>
    <?= $this->Html->css('owl.carousel') ?>
    <?= $this->Html->css('owl.theme') ?>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <?= $this->Html->script('owl-carousel/owl.carousel.min')?>
</head>
<script>
    $(document).ready(function(){
        $('#entenda').addClass('active-navbar');
    });
</script>

<style type="text/css">
    .alert {
        color: #3c763d;
        background-color: #dff0d8;
        border-color: #d6e9c6;
        z-index: 9000000;
        margin-top: 0px;
        position: absolute;
        width: 100%;
    }
    .active-sobrenav{
      color: #f3d012!important;
    }
    .sobrenavbar{
      height: 25px;
      padding: 2px 30px;
      background-color: #5089cf;
    }
    .sobrenavbar span{
      font-size: 14px;
      font-family: 'Lato', sans-serif;
      font-weight: bold;
      color: #FFF;
      margin: 0 15px;
      cursor: pointer;
      text-transform: none;
    }
    .sobrenavbar span:hover{
      color: #f3d012;
    }
    @media all and (max-width: 768px){
        .sobrenavbar{
                display: none;
            }
    }
</style>

<script type="text/javascript">
    setTimeout(function() {
        $('.alert').fadeOut(1200);
    }, 2000);

    $(window).load(function() {
        function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
        }
        
    });
</script>

<body id="page-top" class="index">

    <!-- Navigation -->
<?= $this->Element('navbar-default'); ?>
    <!-- Header -->

<script>
    $('#link-modelo').click(function() {
        $('.popup-modelo').fadeIn();
        $('.popup-modelo').css('overflow', 'auto');
        $('body').css('overflow', 'hidden');
    });
</script>



<div id="navebook" class="ebook">
    <!-- Button to close the ebook navigation -->
  <!-- ebook content -->

<script type="text/javascript">
    $(window).ready(function() {
        $('#ebook-btn').click(function(){
            var form = $('#ebook-form');
            var valid = form.validationEngine("validate");
            if (valid == true) {
                var form_data = form.serialize();
                $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + "ebook",
                    data: form_data,
                })
                    .done(function (data) {
                        if(data >= 1) {
                            $('#ebook-btn').parent().hide();
                            $('#ebook-form').hide();
                            $('#ebook-link').fadeIn();
                        } else {
                            $('#ebook-btn').html('Falhou, tente novamente..');
                        }
                    });
            }else{
                form.validationEngine({
                    updatePromptsPosition: true,
                    promptPosition: 'inline',
                    scroll: false
                });
            }
        });
    });
</script>

<div class="popup">
    <div class="cortina">
                <?= $this->Form->create(null, ['id' => 'form-login'])?>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-xs-offset-11 col-xs-1">
                            
                        </div>
                    </div>
                    <div class="row text-center">
                        <p style="font-family: 'Lato', sans-serif; color: #f4d637; font-size: 24px; padding-left: 18px;">Aluno 
                        <span id="popup-btn-close" class="popup-btn-close pull-right"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
                        </p>
                    </div>
                    <div class="row">
                        <div>
                            <?= $this->Html->link('<i class="fa fa-facebook-official"></i> Login com Facebook ',
                            '/fb-login/',
                            ['class' => 'btn btn-primary', 'escape' => false, 'style' => 'margin-top: 5px; width:100%;']) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div style="width:100%; text-align:center">
                            <span class="text-center" style="font-size: 16px; font-weight: bold; color: white;">ou</span>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-0">
                            <?= $this->Form->input('email', [
                                'div'           => false,
                                'label'         => false,
                                'class'         => 'form-control validate[required]',
                                'placeholder'   => 'E-MAIL ou CPF',
                                'style'         => 'width: 90%; margin: 10px auto;',
                            ])?>
                            <h6 id="info-cadastro-2" class="col-md-12 white forgot-new font-bold"></h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-0">
                            <?= $this->Form->input('password', [
                                'id'            => 'login-password',
                                'div'           => false,
                                'label'         => false,
                                'class'         => 'form-control validate[required]',
                                'placeholder'   => 'SENHA',
                                'style'         => 'width: 90%; margin: 10px auto;',
                            ])?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $this->Html->link('Esqueci a senha',
                                $SSlug.'/login/esqueci-a-senha',[
                                'class' => 'white forgot-new',
                                'style'         => 'font-size: 12px;',
                            ])?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $this->Form->button('Entrar', [
                                'type'      => 'button',
                                'id'        => 'submit-login',
                                'class'     => 'btn btn-default btn-entrar',
                                'escape'    => false,
                                'style'     => 'width: 100%; margin: 10px auto;'
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 text-center">
                            <?= $this->Html->link('Sou academia',
                                '/academia/acesso',[
                                'class' => 'white',
                                'style'         => 'font-size: 14px;',
                            ])?>
                        </div>
                        <div class="col-xs-6 text-center">
                            <?= $this->Html->link('Sou professor',
                                '/professor/acesso',[
                                'class' => 'white',
                                'style'         => 'font-size: 14px;',
                            ])?>
                        </div>
                    </div>

                </div>
                <?= $this->Form->end()?>
    </div>
</div>

    <style>
        .logos-falam img{
            -webkit-filter: grayscale(100%);
        }
        .logos-falam img:hover{
            -webkit-filter: grayscale(0%);
        }
        .logos-falam svg{
            -webkit-filter: grayscale(100%);
        }
        .logos-falam svg:hover{
            -webkit-filter: grayscale(0%);
        }
        .alerta-contato{
            color: black;
        }
        .midias p{
            font-family: 'Lato', sans-serif;
            font-size: 25px;
        }
        .aluno p{
            font-family: 'Lato', sans-serif;
            font-size: 110px
        }
        .input-cadastro {
            width: 100%;
        }
        .linha-contador{   
            height: auto;
            z-index: 1;
        }
        .logo_navbar{
            width: 170px;
        }
        .close-modelo {
            color: #5087C7;
        }
        .botao-modelo {
            color: #5087C7;
            border-color: #5087C7;
        }
        .texto-modelo {
            font-family: 'Lato', sans-serif;
            font-size: 2.6em;
            color: #5087C7;
            line-height: 1em;
        }
        .subtexto-modelo {
            font-family: 'Lato', sans-serif;
            font-size: 2em;
            color: #5087C7;
            margin-top: 20px;
            margin-bottom: 30px;
        }
        #header-home {
            background-color: #5087C7;
            z-index: 2;
            text-align: center;
            margin-top: 60px;
        }
        #txt_funciona {
            color: #5087C7;
        }
        .pra-vce-aluno {
            color:#5087c7;
            font-family: 'Lato', sans-serif;
            font-size: 3.4em;
            letter-spacing: -2px;
        }
        .contador {
            color:#5087c7;
            font-family: 'Lato', sans-serif;
            font-size: 3.4em;
            letter-spacing: -3px;
        }
        .header-texto {
            color:#FDD403;
            font-family: 'Lato', sans-serif;
            font-size: 3.6em;
            letter-spacing: -3px;
        }
        .produtos-home {
            margin-top: 120px;
        }
        .owl-pagination {
            display: none!important;
        }
        #owl-produtos {
            margin-top: 35px;
        }
        .bg-white-produto {
            box-shadow: 0px 1px 10px 1px rgba(0,0,0,0.75);
            min-height: 200px;
            margin: 10px;
            padding: 5px;
            max-height: 290px;
        }
        .produto-grade {
            max-height: 150px;
            max-width: 110px;
            margin-top: 25%;
        }
        .description {
            font-size: 16px;
        }
        .produtos-box:first-child {
            margin-left: -5px;
        }
        .popup-modelo {
            display: none;
            position: fixed;
            top: 0!important;
            width: 100%;
            height: 100%;
            padding: 50px 30px;
            background: rgba(0, 0, 0, .9);
            color: #333;
            font-size: 19px;
            line-height: 30px;
            z-index: 9999;
        }
        .cortina-modelo {
            margin-left: 12%;
            margin-top: 50px;
            width: 75%;
            height: auto;
            background: #f4d637;
            padding: 25px;
            border-radius: 10px;
            text-align: center;
        }
        .produtos_m{
            display: none;
        }
        .form-wrap form label[for]:before,
        .form-wrap form label[for]:after {
            content: none;
        }
        .checkbox {
            margin-bottom: 30px!important;
            margin-left: 25px!important;
        }
        @media all and (max-width: 768px){
            .produtos_pc{
                display: none;
            }
            .produtos_m{
                display: block;
                width: 100%;
                margin-top: 40%
            }
            .contador {
                color: #5087c7;
                font-family: 'Lato', sans-serif;
                font-size: 4.1em;
                letter-spacing: -3px;
            }
        }
        @media all and (max-width: 767px) {
            .mapa-content{
                display: none;
            }
        }
        @media all and (max-width: 450px) {
            #header-home {
                margin-top: 50px;
            }
            .bg-white-produto {
                max-height: 200px;
            }
            .texto-modelo {
                font-size: 1.5em;
            }
            .cortina-modelo {
                width: 100%;
                margin-left: 0;
            }
            .contador{
                text-align: center;
                font-size: 3.1em;
            }
            .produtos_m{
                display: block;
                width: 100%;
                margin-top: 0;
            }
        }
        @media all and (max-width: 450px){
            .portfolio-item {
                width: 100%;
            }
            .btn-outline {
                font-size: 15px;
            }
            .aluno p{
            font-size: 40px
            }
        }
        @media all and (max-width: 1024px){
            .produtos_pc{
                width: 100%;
                padding-top: 25%;
            }
        }
        @media all and (min-width: 1200px){
            .produtos_pc{
                width: 100%;
            }
        }
        .intro-header {
          background-color: #777777;
          background: no-repeat center center;
          background-attachment: scroll;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          background-size: cover;
          -o-background-size: cover;
          margin-bottom: 10px;
        }
        .intro-header .site-heading,
        .intro-header .post-heading,
        .intro-header .page-heading {
          padding: 100px 0 50px;
          color: white;
        }
        @media only screen and (min-width: 768px) {
          .intro-header .site-heading,
          .intro-header .post-heading,
          .intro-header .page-heading {
            padding: 150px 0;
          }
        }
        .intro-header .site-heading,
        .intro-header .page-heading {
          text-align: center;
        }
        .intro-header .site-heading h1,
        .intro-header .page-heading h1 {
          margin-top: 0;
          font-size: 50px;
        }
        .intro-header .site-heading .subheading,
        .intro-header .page-heading .subheading {
          font-size: 24px;
          line-height: 1.1;
          display: block;
          font-family: 'Lato', sans-serif;
          font-weight: 300;
          margin: 10px 0 0;
        }
        @media only screen and (min-width: 768px) {
          .intro-header .site-heading h1,
          .intro-header .page-heading h1 {
            font-size: 80px;
          }
        }

        @media only screen and (max-width: 667px) {
          .imgtabs {
            display: none;
          }
        }
        .intro-header .post-heading h1 {
          font-size: 35px;
        }
        .intro-header .post-heading .subheading,
        .intro-header .post-heading .meta {
          line-height: 1.1;
          display: block;
        }
        .intro-header .post-heading .subheading {
          font-family: 'Lato', sans-serif;
          font-size: 24px;
          margin: 10px 0 30px;
          font-weight: 600;
        }
        .intro-header .post-heading .meta {
          font-family: 'Lato', sans-serif;
          font-style: italic;
          font-weight: 300;
          font-size: 20px;
        }
        .intro-header .post-heading .meta a {
          color: white;
        }
        @media only screen and (min-width: 768px) {
          .intro-header .post-heading h1 {
            font-size: 55px;
          }
          .intro-header .post-heading .subheading {
            font-size: 30px;
          }
        }
        @media all and (min-width: 992px){
            .linha-contador{
                border: 1px solid #9f9f9f;
                position: relative;
                margin-bottom: -70px;
                margin-top: -30px;
                background-color: #fff;
                box-shadow: 0 3px 3px 0 rgba(0,0,0,0.14), 0 1px 7px 0 rgba(0,0,0,0.12), 0 3px 1px -1px rgba(0,0,0,0.2);
            }
        } 

        body {
         background-color: white;
          }

         .tabCommon  {
          font-family: 'Lato', sans-serif;
        }

        .tabCommon .btn {
          font-family: 'Lato', sans-serif;
          text-transform: uppercase;
          
        }

        .tabCommon img {
            padding-top: 20px
            }

        a {
          font-family: 'Lato', sans-serif;
        }

        .marcas {
         padding-top: 25px;
         padding-right: 10px;
         max-width: 200px;
         }

        

        .oie {
         max-width: 50px;
            padding-bottom: 5px

         }

         @media only screen and (max-width: 736px) {
          .intro-header {
            background-image: url(img/entenda-mob.jpg);
            background-repeat:no-repeat;
             background-size:100%;
             background-position:center;
            padding-top: 45%;
          }
        }

        @media only screen and (min-width: 736px) {
          .intro-header {
            background-image: url('img/entenda-bg.jpg'); padding-top: 480px;
          }
        }
        @media only screen and (min-width: 1440px) { 
            .intro-header {
                background-image: url('img/entenda-bg.jpg'); padding-top: 580px;
            }
        }
        @media only screen and (max-width: 736px) {
          .navzinhas span {
            display: none;
          }
          .navzinhas a{
           font-size: 7px;
           color: black;
          }
          .navzinhas img {
            width: 30px;
            padding-bottom: 5px
          }

        }
        @media screen and (max-width: 500px){
            footer{
                margin-bottom: 30px;
            }
        }


     </style>

    <!-- Section inicial -->
     <header class="intro-header"></header>
     <!-- Fim Section inicial -->
     
     <div class="container">
         <div class="row">
              <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                 <h1 class="uppercase">O que posso fazer na LOG?</h1>
                 <div class="tabCommon">
                     <ul class="nav nav-tabs text-center">
                         <li class="active navzinhas"><a data-toggle="tab" href="#menu1">
                         <img src="img/aluno.png" class="oie navzinhas"/> <br>ALUNO</a>
                         </li>
                         <li class="navzinhas"><a data-toggle="tab" href="#menu2"><img src="img/stationary-bike.png" class="oie navzinhas" /> <br/>ACADEMIA</a></li>
                         <li class="navzinhas"><a data-toggle="tab" href="#menu3"><img src="img/exercise.png" class="oie navzinhas"/><br/>TIME</a></li>
                         <li class="navzinhas"><a data-toggle="tab" href="#menu4"><img src="img/ideabulb.png" class="oie navzinhas" /><br/>FRANQUIA</a></li>
                     </ul>
                     <div class="tab-bottom" style="padding-top: 25px"></div>
                     <div class="tab-content">
                         <div id="menu1" class="tab-pane fade in active text-center">
                             <div class="tab-contentInner">
                                 <div class="row">
                                     <div class="col-lg-4 text-center">
                                         <?= $this->Html->image('location.png', ['alt' => 'Encontrar academias em todo o Brasil'])?>
                                         <p>Encontrar academias em todo o Brasil</p>
                                     </div>
                                     <div class="col-lg-4 text-center">
                                         <?= $this->Html->image('cashs.png', ['alt' => 'Pagar minha mensalidade no site ou direto na academia'])?>
                                         <p>Pagar minha mensalidade no site ou direto na academia</p>
                                     </div>
                                     <div class="col-lg-4 text-center">
                                         <?= $this->Html->image('proteins.png', ['alt' => 'Comprar suplementos e receber na academia com frete grátis'])?>
                                         <p>Comprar suplementos e receber na academia com frete grátis</p>
                                     </div>
                                     <a href="https://logfitness.com.br/buscar_academia" class="btn btn-lg btn-primary">Busque sua academia</a>
                                 </div>
                             </div>
                         </div>
                         <div id="menu2" class="tab-pane fade">
                             <div class="tab-contentInner text-center">
                                 <div class="row" style="padding-bottom: 15px">
                                     <div class="col-lg-4 text-center">
                                         <?= $this->Html->image('megaphone.png', ['alt' => 'Divulguar e trazer mais alunos'])?>
                                         <p>Divulguar e trazer mais alunos</p>
                                     </div>
                                     <div class="col-lg-4 text-center">
                                         <?= $this->Html->image('cloud-computing.png', ['alt' => 'Receber suas mensalidades sem complicação e com as menores taxas'])?>
                                         <p>Receber suas mensalidades sem complicação e com as menores taxas</p>
                                     </div>
                                     <div class="col-lg-4 text-center">
                                         <?= $this->Html->image('pay-per-click.png', ['alt' => 'Ter sua loja virtual na sua academia'])?>
                                         <p>Ter sua Loja virtual da sua academia sem precisar investir nada</p>
                                     </div>
                                 </div>
                                 <a href="https://www.logfitness.com.br/academia/" class="btn btn-primary btn-lg">Mais informações</a>
                             </div>
                         </div>
                         <div id="menu3" class="tab-pane fade">
                             <div class="tab-contentInner text-center">
                                 <div class="row" style="padding-bottom: 15px">
                                     
                                     <div class="col-lg-4 text-center">
                                         <?= $this->Html->image('profcard.png', ['alt' => 'Recena suas aulas sem complicação e com as menores taxas'])?><br>
                                         <small>EM BREVE</small>
                                         <p>Receba suas aulas sem complicação e com as menores taxas</p>
                                     </div>
                                     <div class="col-lg-4 text-center">
                                         <?= $this->Html->image('megafoneprof.png', ['alt' => 'Sem investimento em estoque, sem preocupação com logística'])?><br>
                                         <small>EM BREVE</small>
                                         <p>Divulgue-se para trazer mais alunos</p>
                                     </div>
                                     <div class="col-lg-4 text-center">
                                         <?= $this->Html->image('proteins.png', ['alt' => 'Ajude seus alunos a se superarem com os melhores suplementos e ainda receba comissão'])?>
                                         <p>Ajude seus alunos a se superarem com os melhores suplementos e ainda receba comissão</p>
                                     </div>
                                      <a href="https://www.logfitness.com.br/professor/" class="btn btn-primary btn-lg">Mais informações</a>
                                 </div>
                             </div>
                         </div>
                         <div id="menu4" class="tab-pane fade">
                             <div class="tab-contentInner text-center">
                                 <div class="row">
                                     <div class="col-lg-6 text-left">
                                     <p>Quer fazer parte do mercado que mais cresce no Brasil? Se você tem muita força de vontade e quer participar do futuro, seja um braço da LOG na sua cidade.</p>
                                     <a href="https://www.logfitness.com.br/franquia/" target="_blank" class="btn btn-primary btn-lg">Mais informações</a>
                                     </div>
                                     <div class="col-lg-6">
                                         <?= $this->Html->image('growth.png', ['alt' => 'Logfranquia'])?>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>

         </div>
     </div>
     <div class="" style="padding: 30px"></div>
     <aside class="" style="background-color: color: #fff;
         background-color: #5087c7;">
         <div class="container text-center">
             <div style="padding: 3em">
                 <h2 class="uppercase" style="color: #fff">Aqui você encontra mais de 15 marcas</h2>
                 <?= $this->Html->image('bodyaction.png', ['class' => 'marcas'])?>
                 <?= $this->Html->image('atlhetica.png', ['class' => 'marcas'])?>
                 <?= $this->Html->image('logo-blackskull.png', ['class' => 'marcas'])?>
                 <?= $this->Html->image('maxnutrition.png', ['class' => 'marcas'])?>
             </div>
         </div>
     </aside>
     <!-- Como Funciona -->
    <!-- Cadastrar Section -->
    <section id="cadastrar" style="padding: 0px;">
        <div class="container-fluid">

            <div class="col-xs-12 col-md-6 col-md-offset-3 linha-contador text-center">
                <p class="contador"><?=$academia_ativas -1 ?> academias com LOG!</p>
            </div>
            <div class="row mapa-content" style="display: block!important;">
                <div id="map_canvas" style="display: block!important; width: 100%; height: 450px;"></div>
            </div>
        </div>
    </section>

    <style type="text/css">
        .midia {
            background: #f4d637;
            color: white;
        }
        #ts-certificate-modal-container #ts-certificate-modal {
            z-index: 10000;
        }
        #ts-certificate-modal-container #ts-certificate-overlay {
            z-index: 9999;
        }
        #trustvox-selo-site-sincero {
            cursor: pointer;
        }
        .exame-logo {
            fill: #c1161c;
            width: 100%;
            height: auto;
        }
        .alinhar-centro {
            margin-top: 20px;
            justify-content: center;
            align-items: center;
            height: 120px;
            display: -webkit-flex;
            -webkit-justify-content: center;
            -webkit-align-items: center;
        }
    </style>

    <!-- Nós na mídia -->
    <section class="midia" id="midia">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center aluno">
                   <p style="font-size: 80px">O que falam de nós</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-2 col-lg-offset-1 text-center alinhar-centro logos-falam">
                    <a target="_blank" href="http://exame.abril.com.br/estilo-de-vida/10-mitos-sobre-o-uso-de-suplementos-de-academia/">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="195px" height="60px" viewBox="0 0 195 60" xml:space="preserve" class="exame-logo">
                        <g id="primary">
                            <path d="M122.107,16.082c-0.143-0.081-0.667,0.081-0.771,0.609c0,0.425,0.246,0.729,0.792,0.89
                                c1.012,0.304,1.315,0.283,1.599,0.446c0.384,0.222,1.316,0.566,1.459,3.969c0.099,4.031-0.041,9.3-0.082,14.079
                                c-0.021,4.539-0.567,4.561-1.437,5.167c-1.116,0.447-1.644,0.75-1.621,1.277c0,0.384,0.284,0.771,1.437,0.771
                                c3.548-0.043,6.525-0.103,9.402-0.103c2.695,0,5.267,0.039,8.123,0.141c1.479,0.182,1.417-0.404,1.417-0.93
                                c-0.019-0.487-0.2-2.817,0.104-4.924c0-1.662-0.427-1.521-0.729-1.541c-0.406-0.059-0.852,0.202-0.852,1.054
                                c-0.02,0.812,0.182,2.917-3.425,3.263c-2.188,0.242-3.464,0.325-4.578,0.283c-1.541,0-1.824-0.02-1.845-0.952
                                c0-2.775,0.081-6.542,0.122-8.205c0.062-1.275,1.196-1.195,2.271-1.257c1.214-0.04,3.686-0.06,4.395,2.513
                                c0.325,1.339,0.588,1.316,1.014,1.257c0.467-0.081,0.668-0.345,0.607-1.337c-0.06-0.952-0.264-5.146,0.061-7.153
                                c0.08-0.971-0.04-1.215-0.466-1.335c-0.445-0.083-0.911,0.184-1.173,1.093c-0.266,0.506-0.935,2.148-3.731,2.289
                                c-2.835,0.121-3.018-0.039-2.977-1.721c-0.019-1.723,0.021-5.531,0.021-5.552c-0.04-0.304-0.04-1.519,1.294-1.419
                                c1.704-0.019,4.701,0.083,5.451,0.244c0.771,0.182,2.815,0.386,3.062,3.485c0.117,0.85,0.564,0.831,0.809,0.85
                                c0.324,0.022,0.708-0.162,0.607-0.992c-0.121-0.811-0.061-3.91,0.082-5.002c0.059-1.155-0.508-1.136-1.237-1.177
                                c-0.466,0-5.957,0.204-9.985,0.306C128.974,16.569,122.107,16.082,122.107,16.082"></path>
                            <path d="M84.568,16.204c-0.588-0.06-0.81,0.224-0.851,0.607c0.042,0.407,0.102,0.529,0.587,0.811
                                c1.479,0.689,2.877,0.81,3.161,4.741v14.284c-0.202,2.066,0.163,3.383-2.593,4.861c-0.892,0.506-1.074,0.748-1.033,1.216
                                c0.102,0.588,0.628,0.588,0.647,0.568c0.771,0.101,2.655-0.185,5.106-0.082c1.601-0.062,3.952,0.142,4.863,0.06
                                c0.546-0.162,0.668-0.282,0.668-0.749c0.042-0.324-0.141-0.689-0.709-0.952c-0.851-0.407-3.403-1.013-3.626-4.902l-0.02-11.367
                                l8.063,17.183c0.081,0.242,0.366,0.849,0.909,0.811c0.446-0.022,1.156-0.689,1.5-1.439l7.578-16.45v13.574
                                c0,0.019,0.142,2.267-1.276,2.531c-0.647,0.202-0.952,0.668-0.912,1.013c0.042,0.405,0.364,0.689,0.791,0.71
                                c0.405,0.019,2.836-0.062,5.327-0.062c2.635,0,5.327,0.101,5.733,0.021c0.628,0,0.829-0.426,0.789-0.729
                                c0.062-0.59-0.484-0.791-0.81-0.933c-1.68-0.709-3.626-1.175-3.687-6.158l0.02-14.608c0.103-1.458,0.405-2.531,2.836-3.019
                                c0.953-0.384,1.176-0.728,1.137-0.992c-0.041-0.405-0.144-0.669-0.609-0.588c-0.385-0.04-1.965,0.103-3.646,0.142
                                c-1.54-0.02-2.96-0.059-3.951-0.142c-1.461,0-1.986,1.339-2.227,1.865l-7.032,15.5l-8.164-16.532c-0.304-0.81-0.75-0.771-1.5-0.833
                                C90.93,16.163,87.566,16.507,84.568,16.204"></path>
                            <path d="M0.797,16.082c-0.141-0.081-0.667,0.081-0.769,0.609c0,0.425,0.263,0.729,0.79,0.89
                                c1.013,0.304,1.316,0.283,1.6,0.446c0.386,0.222,1.317,0.566,1.46,3.969c0.101,4.031-0.042,9.3-0.083,14.079
                                c-0.019,4.539-0.566,4.561-1.417,5.167c-1.134,0.447-1.661,0.75-1.64,1.277c0,0.384,0.285,0.771,1.438,0.771
                                c3.566-0.043,6.523-0.103,9.399-0.103c2.696,0,5.268,0.039,8.125,0.141c1.478,0.182,1.417-0.404,1.417-0.93
                                c-0.019-0.487-0.202-2.817,0.102-4.924c0-1.662-0.426-1.521-0.729-1.541c-0.405-0.059-0.851,0.202-0.851,1.054
                                c-0.021,0.812,0.184,2.917-3.403,3.263c-2.208,0.242-3.483,0.325-4.599,0.283c-1.539,0-1.822-0.02-1.843-0.952
                                c0-2.775,0.082-6.542,0.141-8.205c0.041-1.275,1.175-1.195,2.25-1.257c1.215-0.04,3.688-0.06,4.396,2.513
                                c0.324,1.339,0.587,1.316,1.012,1.257c0.466-0.081,0.669-0.345,0.607-1.337c-0.06-0.952-0.262-5.146,0.062-7.153
                                c0.079-0.971-0.042-1.215-0.467-1.335c-0.446-0.083-0.913,0.184-1.155,1.093c-0.283,0.506-0.952,2.148-3.749,2.289
                                c-2.835,0.121-3.018-0.039-2.958-1.721c-0.019-1.723,0-5.531,0-5.552c-0.04-0.304-0.019-1.519,1.298-1.419
                                c1.702-0.019,4.7,0.083,5.448,0.244c0.77,0.182,2.818,0.386,3.06,3.485c0.122,0.85,0.568,0.831,0.812,0.85
                                c0.323,0.022,0.708-0.162,0.607-0.992c-0.122-0.811-0.061-3.91,0.101-5.002c0.04-1.155-0.526-1.136-1.256-1.177
                                c-0.467,0-5.957,0.204-9.987,0.306C7.665,16.569,0.797,16.082,0.797,16.082"></path>
                            <path d="M81.955,41.811c-0.607-0.261-1.682,0.103-3.12-2.856L68.442,16.386c-0.244-0.527-0.791-0.587-1.013,0
                                l-1.196,2.371L55.841,39.644c0,0-0.688,1.642-2.024,2.107c0,0.02-1.015,0.365-0.994,0.951c0,0.446,0.425,0.649,0.953,0.609
                                c2.998-0.102,4.76-0.143,7.797,0.04c0.568,0.021,0.874-0.263,0.934-0.628c0.061-0.406-0.161-0.912-0.75-1.136
                                c-1.114-0.485-2.167-0.282-1.944-1.579c0.203-1.277,1.013-3.525,1.56-4.397c0.426-0.729,0.708-0.649,1.195-0.649h3.667v0.002h3.545
                                c0.587,0.062,0.77,0.123,1.093,0.688c0.285,0.485,1.136,1.804,1.581,3.424c0.425,1.583-0.385,2.432-1.257,2.757
                                c-0.587,0.221-1.256,0.16-1.256,0.971c0.021,0.345,0.345,0.488,0.771,0.508c1.073-0.062,3.383-0.083,5.775-0.122
                                c2.006,0.039,3.403,0.021,5.288,0.141c0.75-0.019,0.891-0.361,0.911-0.628C82.726,42.196,82.44,41.952,81.955,41.811z
                                 M68.685,32.045c-0.972,0.022-1.722,0.022-2.451,0.022v-0.001c-0.769,0-1.54-0.021-2.572-0.021
                                c-0.386-0.06-0.872,0.083-0.587-0.809l3.141-6.321l0.019,0.102l2.977,5.977C69.435,31.602,69.678,32.026,68.685,32.045z"></path>
                            <path d="M24.967,16.185c0,0-1.194-0.103-1.257,0.485c0,0.364,0.042,0.669,0.69,0.891
                                c1.882,0.912,2.855,1.923,3.281,2.613c1.417,1.56,7.211,10.149,7.211,10.129c0,0.02-7.474,8.224-7.474,8.206
                                c0,0.019-2.188,2.149-4.295,3.16c-0.669,0.364-0.75,0.709-0.669,1.032c0.062,0.426,0.446,0.649,1.215,0.628
                                c1.602,0.021,5.228-0.386,10.251,0c0.709,0,1.032-0.303,1.054-0.688c0.042-0.425-0.284-0.89-0.911-1.032
                                c-1.439-0.244-3.727-1.115-1.741-3.709c1.985-2.573,4.333-5.145,4.333-5.165c0,0.021,2.736,4.053,4.155,6.159
                                c0.87,1.316,0.384,2.552-1.074,2.857c-0.527,0.223-0.647,0.506-0.647,0.932c0,0.403,0.364,0.668,1.174,0.608
                                c1.277,0.06,5.591-0.245,10.959,0c0.872,0,1.156-0.245,1.216-0.548c0.062-0.527-0.283-0.871-0.648-1.054
                                c-0.789-0.466-1.721-0.365-3.93-3.385c-2.532-3.463-7.192-9.986-7.192-10.007c0,0.021,7.76-8.449,7.76-8.449
                                c0.283-0.404,1.62-1.68,3.525-2.065c0.667-0.263,0.81-0.769,0.728-1.054c0.041-0.323-0.364-0.567-0.83-0.546
                                c-1.541,0.161-3.283,0.303-4.863,0.343c-1.62-0.04-4.091-0.202-5.57-0.343c-0.587-0.021-0.891,0.262-0.913,0.626
                                c-0.082,0.387,0.142,0.911,0.73,0.953c1.478,0.244,3.403,0.69,1.883,2.876c-1.579,2.149-4.293,5.024-4.293,5.006
                                c0,0.019-4.052-5.917-4.052-5.936c-0.203-0.364-0.812-1.539,0.81-1.925c0.851-0.223,1.095-0.467,1.013-0.953
                                c0.06-0.525-0.426-0.628-1.054-0.647c-1.234-0.062-3.829,0.364-5.812,0.303C28.29,16.386,24.967,16.185,24.967,16.185"></path>
                            <path d="M146.588,41.036"></path>
                            <path d="M146.588,43.228"></path>
                            <path d="M148.781,43.228"></path>
                            <line x1="147.242" y1="44.031" x2="147.242" y2="41.84"></line>
                            <rect x="146.878" y="41.429" width="2.191" height="2.191"></rect>
                            <path d="M160.585,31.52c-0.714-0.5-1.567-0.75-2.553-0.75c-0.845,0-1.563,0.157-2.163,0.473
                                c-0.598,0.317-1.094,0.741-1.481,1.273c-0.388,0.534-0.674,1.143-0.858,1.826s-0.277,1.395-0.277,2.132
                                c0,0.804,0.093,1.562,0.277,2.279c0.185,0.716,0.471,1.343,0.858,1.876s0.885,0.958,1.491,1.272c0.606,0.315,1.33,0.475,2.17,0.475
                                c0.621,0,1.17-0.104,1.648-0.308c0.481-0.202,0.896-0.485,1.244-0.848c0.35-0.362,0.621-0.793,0.819-1.292
                                c0.198-0.5,0.316-1.041,0.355-1.62h1.875c-0.183,1.777-0.795,3.159-1.835,4.146c-1.039,0.987-2.462,1.48-4.264,1.48
                                c-1.093,0-2.048-0.188-2.861-0.562c-0.817-0.376-1.495-0.892-2.034-1.55c-0.541-0.657-0.944-1.435-1.213-2.33
                                c-0.271-0.896-0.405-1.854-0.405-2.881c0-1.027,0.144-1.99,0.432-2.895c0.291-0.899,0.716-1.686,1.275-2.357
                                c0.559-0.671,1.257-1.2,2.092-1.588s1.793-0.582,2.871-0.582c0.737,0,1.435,0.098,2.094,0.295c0.657,0.198,1.243,0.488,1.757,0.869
                                c0.513,0.381,0.939,0.857,1.284,1.431c0.342,0.572,0.563,1.234,0.67,1.983h-1.874C161.769,32.771,161.305,32.02,160.585,31.52"></path>
                            <path d="M165.757,33.8c0.283-0.889,0.707-1.675,1.273-2.358c0.566-0.685,1.271-1.231,2.114-1.639
                                c0.842-0.407,1.82-0.612,2.939-0.612c1.118,0,2.1,0.205,2.94,0.612c0.844,0.408,1.546,0.955,2.112,1.639
                                c0.566,0.684,0.991,1.47,1.273,2.358c0.281,0.888,0.425,1.812,0.425,2.773c0,0.961-0.144,1.885-0.425,2.772
                                c-0.282,0.889-0.707,1.675-1.273,2.359s-1.269,1.226-2.112,1.627c-0.841,0.403-1.822,0.604-2.94,0.604
                                c-1.119,0-2.098-0.201-2.939-0.604c-0.844-0.401-1.548-0.942-2.114-1.627s-0.99-1.471-1.273-2.359
                                c-0.283-0.888-0.423-1.812-0.423-2.772C165.334,35.611,165.474,34.688,165.757,33.8 M167.485,38.674
                                c0.185,0.692,0.474,1.315,0.868,1.868c0.396,0.551,0.899,0.995,1.521,1.331c0.618,0.334,1.354,0.504,2.21,0.504
                                s1.593-0.17,2.212-0.504c0.617-0.336,1.123-0.78,1.519-1.331c0.394-0.553,0.685-1.176,0.869-1.868
                                c0.184-0.689,0.276-1.391,0.276-2.101s-0.093-1.413-0.276-2.103c-0.185-0.691-0.476-1.312-0.869-1.865
                                c-0.396-0.553-0.901-0.996-1.519-1.334c-0.619-0.333-1.355-0.501-2.212-0.501s-1.592,0.168-2.21,0.501
                                c-0.621,0.338-1.125,0.781-1.521,1.334c-0.395,0.553-0.684,1.174-0.868,1.865c-0.185,0.689-0.277,1.393-0.277,2.103
                                S167.301,37.984,167.485,38.674"></path>
                            <polygon points="183.532,29.526 187.974,41.37 192.435,29.526 195,29.526 195,43.62 193.223,43.62 193.223,31.895 
                                193.186,31.895 188.782,43.62 187.185,43.62 182.782,31.895 182.744,31.895 182.744,43.62 180.966,43.62 180.966,29.526     "></polygon>
                        </g>
                        </svg>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-2 text-center alinhar-centro">
                    <div class="ts-modal-trigger logos-falam" id="trustvox-selo-site-sincero">
                      <?= $this->Html->image('selo-trustvox.png', ['alt' => 'selo trustvox', 'style' => 'width: auto; max-height: 120px;']); ?>
                    </div>
                    <script type="text/javascript">
                        var _trustvox_certificate = _trustvox_certificate || [];
                        _trustvox_certificate.push(['_certificateId', 'logfitness']);
                        (function() {
                          var tv = document.createElement('script'); tv.type = 'text/javascript'; tv.async = true;
                          tv.src = '//s3-sa-east-1.amazonaws.com/trustvox-certificate-modal-js/widget.js';
                          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tv, s);
                        })();
                    </script> 
                </div>
                <div class="col-sm-6 col-lg-2 text-center alinhar-centro logos-falam">
                    <a target="_blank" href="http://revistasuplementacao.com.br/?page=interna&slug=logfitness_chega_ao_mercado_e_espera_faturar_r_1_5_milhao_em_2017">
                        <?= $this->Html->image('logo-suplementacao.png', ['alt' => 'revista suplementacao', 'style' => 'max-width: 165px; height: auto;']); ?>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-2 text-center alinhar-centro logos-falam">
                    <a target="_blank" href="https://www.ecommercebrasil.com.br/noticias/plataforma-e-commerce-suplementos-academias/">
                        <?= $this->Html->image('ecommerce_brasil.png', ['alt' => 'Ecommerce Brasil', 'style' => 'max-width: 165px; height: auto;']); ?>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-2 text-center alinhar-centro logos-falam">
                    <a target="_blank" href="http://boaforma.abril.com.br/fitness/nutricionista-indica-os-suplementos-ideais-para-treinos-aerobicos/">
                        <?= $this->Html->image('boa_forma.png', ['alt' => 'revista suplementacao', 'style' => 'max-width: 165px; height: auto;']); ?>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above" style="background-color: #5087c7">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-xs-12 midias">
                        <p>midias sociais</p>
                        <ul class="list-inline">
                            <li>
                                <a href="http://blog.logfitness.com.br" class="btn-social btn-outline" target="_blank" title="Acesse nosso blog"><i class="fa fa-fw fa-rss"></i></a>
                            </li>
                            <li>
                                <a href="https://open.spotify.com/user/logfitness" class="btn-social btn-outline" target="_blank" title="Escute nossas playlists"><i class="fa fa-fw fa-spotify"></i></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/logfitness.com.br" class="btn-social btn-outline" target="_blank" title="Curta nossa página!"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/logfitness.com.br/" class="btn-social btn-outline" target="_blank" title="Acompanhe nosso Instagram"><i class="fa fa-fw fa-instagram"></i></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/LOGFITNESS" class="btn-social btn-outline" target="_blank" title="Siga a gente no twitter"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container info-central-box form-toggle" id="form-toggle-contato">

            <?= $this->Form->create(null, [
                'id'        => 'form-central-relacionamento',
                'role'      => 'form',
                'default'   => false,
                'type'      => 'file'
            ]) ?>

            <div class="form-group row">
                <div class="col-lg-8">
                    <?= $this->Form->input('name', [
                        'id'            => 'contato-name',
                        'div'           => false,
                        'label'         => 'Nome',
                        'placeholder'   => 'Nome Completo',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-3">
                    <?= $this->Form->input('phone', [
                        'id'            => 'contato-phone',
                        'div'           => false,
                        'label'         => 'Telefone',
                        'placeholder'   => 'Telefone',
                        'class'         => 'validate[required] form-control phone-mask',
                    ]) ?>
                </div>

                <div class="col-lg-5">
                    <?= $this->Form->input('email', [
                        'id'            => 'contato-email',
                        'div'           => false,
                        'label'         => 'E-mail',
                        'placeholder'   => 'E-mail',
                        'class'         => 'validate[required,custom[email]] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-8">
                    <?= $this->Form->input('description', [
                        'id'            => 'contato-description',
                        'div'           => false,
                        'label'         => 'Assunto',
                        'placeholder'   => 'Assunto / Descrição',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-8">
                    <?= $this->Form->input('message', [
                        'id'            => 'contato-name',
                        'type'          => 'textarea',
                        'div'           => false,
                        'label'         => 'Mensagem',
                        'placeholder'   => 'Mensagem',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-md-2">
                    <?= $this->Form->button('Enviar Contato', [
                        'id'            => 'submit-cadastrar-central-relacionamento',
                        'type'          => 'button',
                        'div'           => false,
                        'class'         => 'btn btn-default float-right',
                    ]) ?>

                </div>

                <div class="col-xs-5 alerta-contato" id="form-toggle-message"></div>
            </div>

            <?= $this->Form->end() ?>
        </div>

        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-md-10">
                        Copyright &copy; <span>www.logfitness.com.br</span>. TODOS OS DIREITOS RESERVADOS.
                Todo o conteúdo do site, todas as fotos,imagens, logotipos, marcas, dizeres, som, software, conjunto imagem,
                layout, trade dress, aqui veiculados são de propriedade exclusiva da LogFitness. É vedada qualquer reprodução,
                total ou parcial, de qualquer elemento de identidade, sem expressa autorização. A violação de qualquer direito mencionado
                implicará na responsabilização cível e criminal nos termos da Lei.

                CACIQUE DIGITAL REPRESENTACOES LTDA - ME - CNPJ: 22.921.802/0001-77 Rua Antonio Munia, 141, Sala 01 - Jardim Nazareth - CEP 15054-160 - São José do Rio Preto - SP
                    </div>
                    <div class="col-md-2">
                        <?= $this->Form->button('Contato',
                        ['class' => 'btn-conheca a-contato contato-home',
                            'escape' => false]
                    ) ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

    <script>
        jQuery(document).ready(function($) {
    tab = $('.tabs h3 a');

    tab.on('click', function(event) {
        event.preventDefault();
        tab.removeClass('active');
        $(this).addClass('active');

        tab_content = $(this).attr('href');
        $('div[id$="tab-content"]').removeClass('active');
        $(tab_content).addClass('active');
    });
});
    </script>

    <script>
    // Popup Window
    var scrollTop = '';
    var newHeight = '70';

    $(window).bind('scroll', function() {
      scrollTop = $( window ).scrollTop();
      newHeight = scrollTop + 70;
    });

    $('.popup-trigger').click(function(e) {
      e.stopPropagation();
      $('body').css('overflow', 'hidden');
      if(jQuery(window).width() < 767) {
        $('nav').after( $( ".popup" ) );
        $('.popup').show();
      } else {
        $('.popup').removeClass('popup-mobile').css('top', newHeight).toggle();
      };
    });

    $('html').click(function() {
      $('.popup').hide();
    });

    $('#popup-btn-close').click(function(e){
      $('.popup').hide();
      $('body').css('overflow', 'auto');
    });

    $('.popup').click(function(e){
      e.stopPropagation();
    });

    $("#owl-produtos").owlCarousel({

      autoPlay: 2500,

      items             : 3,
      itemsDesktop      : [1199,3],
      itemsDesktopSmall : [979,2],
      itemsMobile       : [400,1]

    });
    </script>

    <!-- Smartsupp Live Chat script -->
    <script type="text/javascript">
    var _smartsupp = _smartsupp || {};
    _smartsupp.key = '472d82fddbe4326ca5661de701a829a7553ce341';
    window.smartsupp||(function(d) {
        var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
        s=d.getElementsByTagName('script')[0];c=d.createElement('script');
        c.type='text/javascript';c.charset='utf-8';c.async=true;
        c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
    })(document);
    </script>

    <?= $this->Html->script('jquery')?>


    <?= $this->Html->script('bootstrap.min')?>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <?= $this->Html->script('jqBootstrapValidation')?>
    <?= $this->Html->script('contact_me')?>
    <?= $this->Html->script('freelancer.min')?>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>
</body>

