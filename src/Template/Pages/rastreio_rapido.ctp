<style>
	#logexpress-btn{
		display: none!important;
	}
	.container-conteudo {
		padding-top: 120px;
	}
	.codigos-textarea {
		width: 40%;
		height: 150px;
	}
	.title-rastreio {
		color: #5089cf;
	}
	.box-padrao {
	    border: 1px solid #ccc;
	    border-radius: 5px;
	    margin: 10px 0px;
	    padding-bottom: 20px;
	}
	.box-pedido {
		border: 1px solid #ccc;
	    border-radius: 5px;
	    margin: 10px 0px;
	}
</style>

<div class="container container-conteudo text-center">
	<div class="row">
		<?php if(!$pedidos) { ?>
			<div class="col-md-7 col-xs-12 box-padrao">
				<div class="col-xs-12">
					<h2 class="title-rastreio">Acompanhe seus pedidos</h2>
				</div>
				<?= $this->Form->create(null, ['class' => 'rastreio-form']); ?>
				<div class="col-xs-12">
					<h5>Escreva o código dos pedidos separados por ";" (ponto e vírgula) aqui:</h5>
					<?= $this->Form->textarea('codigos_rastreio', [
						'escape' => false,
						'placeholder' => 'Ex: 1453; 1522; 2322; 2517; 3044',
						'class'	=> 'codigos-textarea'
					]) ?>
				</div>
				<div class="col-xs-12">
					<?= $this->Form->button('Rastrear', [
						'type' => 'button',
						'class' => 'btn btn-success btn-rastrear'
					]) ?>
				</div>
				<?= $this->Form->end(); ?>
			</div>
			<div class="col-md-5 text-center mobile-not">
				<?= $this->Html->image('gif5.gif'); ?>
			</div>
		<?php } else { ?>
			<div class="col-xs-12 box-padrao">
				<div class="col-xs-12">
					<h2 class="title-rastreio">Acompanhe seus pedidos</h2>
				</div>
				<?= $this->Form->create(null, ['class' => 'rastreio-form']); ?>
				<div class="col-xs-12">
					<h5>Escreva o código dos pedidos separados por ";" (ponto e vírgula) aqui:</h5>
					<?= $this->Form->textarea('codigos_rastreio', [
						'escape' => false,
						'placeholder' => 'Ex: 1453; 1522; 2322; 2517; 3044',
						'class'	=> 'codigos-textarea'
					]) ?>
				</div>
				<div class="col-xs-12">
					<?= $this->Form->button('Rastrear', [
						'type' => 'button',
						'class' => 'btn btn-success btn-rastrear'
					]) ?>
				</div>
				<?= $this->Form->end(); ?>
			</div>

			<div class="col-xs-12 text-left">
				<div class="row">
					<?php foreach($pedidos as $pedido) { ?>
						<div class="col-xs-12 box-pedido">
							<div class="row">
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-6 text-left">
											<h2>Pedido #<?= $pedido->id ?> <span style="font-size: 18px">- <span style="color: #489f36"><?= $pedido->pedido_status->name ?></span></span></h2>
											<?php if($pedido->pedido_status_id == 1) { ?>
												<?= $this->Html->link('Pagar', 
													$SSlug.'/mochila/fechar-o-ziper/retormar-pedido/'.$pedido->id, 
													['escape' => false, 'class' => 'btn btn-success', 'target' => '_blank']); ?>
											<?php } else if($pedido->pedido_status_id == 2) { ?>
												<?php if($pedido->payment_method == 'BoletoBancario' && $pedido->iugu_payment_pdf) { ?>
													<?= $this->Html->link('Reimprimir Boleto', 
														$pedido->iugu_payment_pdf, 
														['escape' => false, 'class' => 'btn btn-success', 'target' => '_blank']); ?>
													<?= $this->Html->link('Pagar', 
														$SSlug.'/mochila/fechar-o-ziper/retormar-pedido/'.$pedido->id, 
														['escape' => false, 'class' => 'btn btn-success', 'target' => '_blank']); ?>
												<?php } ?>
											<?php } else if($pedido->pedido_status_id == 7) { ?>
												<?= $this->Html->link('Pagar', 
													$SSlug.'/mochila/fechar-o-ziper/retormar-pedido/'.$pedido->id, 
													['escape' => false, 'class' => 'btn btn-success', 'target' => '_blank']); ?>
											<?php } ?>
										</div>
										<div class="col-xs-6 text-right">
											<?php if($pedido->pedido_status_id == 5 && $pedido->data_transporte) { ?>
												<h3>Data de <span style="color: #489f36">envio</span>: <?= $pedido->data_transporte->format('d/m/Y') ?></h3>
											<?php } else if($pedido->pedido_status_id > 5 && $pedido->entrega && $pedido->pedido_status_id != 7 && $pedido->pedido_status_id != 8 && $pedido->pedido_status_id != 10) { ?>
												<h3>Data de <span style="color: #489f36">entrega</span>: <?= $pedido->entrega->format('d/m/Y') ?></h3>
											<?php } ?>
										</div>
									</div>
								</div>
								<div class="col-xs-12">
									<?php if($pedido->pedido_status_id < 5) { ?>

									<?php } else if($pedido->pedido_status_id == 5) { ?>
										<div class="col-xs-6 text-left">
											<h4>Transportadora: <?= $pedido->transportadora->name; ?></h4>
											<h4>Mensagem da transportadora: <?= $pedido->transportadora->mensagem; ?></h4>
										</div>
										<div class="col-xs-6 text-left">
											<?php if($pedido->rastreamento) { ?>
												<h4>Código: <strong><?= $pedido->rastreamento; ?></strong></h4>
											<?php } else { ?>
												<h4>Código: <strong>Em breve</strong></h4>
											<?php } ?>
											<h4>Site: <?= $this->Html->link($pedido->transportadora->site, $pedido->transportadora->site, ['target' => '_blank']); ?></h4>
										</div>
										<?php if($pedido->observacao) { ?>
											<div class="col-xs-12">
												<h4><span style="color: red">Observações</span>: <?= $pedido->observacao ?></h4>
											</div>
										<?php } ?>
									<?php } else if($pedido->pedido_status_id == 6) { ?>
										<div class="col-xs-6 text-left">
											<h4>Transportadora: <?= $pedido->transportadora->name; ?></h4>
										</div>
										<div class="col-xs-6 text-left">
											<?php if($pedido->rastreamento) { ?>
												<h4>Código: <strong><?= $pedido->rastreamento; ?></strong></h4>
											<?php } else { ?>
												<h4>Código: <strong>Em breve</strong></h4>
											<?php } ?>
											<h4>Site: <?= $this->Html->link($pedido->transportadora->site, $pedido->transportadora->site, ['target' => '_blank']); ?></h4>
										</div>
										<?php if($pedido->quem_recebeu) { ?>
											<div class="col-xs-12">
												<h4>Quem recebeu: <?= $pedido->quem_recebeu ?></h4>
											</div>
										<?php } ?>
									<?php } else if($pedido->pedido_status_id == 9) { ?>
										<div class="col-xs-6 text-left">
											<h4>Transportadora: <?= $pedido->transportadora->name; ?></h4>
											<h4>Mensagem da transportadora: <?= $pedido->transportadora->mensagem; ?></h4>
										</div>
										<div class="col-xs-6 text-left">
											<?php if($pedido->rastreamento) { ?>
												<h4>Código: <strong><?= $pedido->rastreamento; ?></strong></h4>
											<?php } else { ?>
												<h4>Código: <strong>Em breve</strong></h4>
											<?php } ?>
											<h4>Site: <?= $this->Html->link($pedido->transportadora->site, $pedido->transportadora->site, ['target' => '_blank']); ?></h4>
										</div>
										<?php if($pedido->quem_recebeu) { ?>
											<div class="col-xs-12">
												<h4>Quem recebeu: <?= $pedido->quem_recebeu ?></h4>
											</div>
										<?php } ?>
									<?php } ?>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {

		$(document).on('click', '.btn-rastrear', function() {
			var text_ids = $('.codigos-textarea').val();
			var id_list = '';
			var ids;
			var qtd = 0;

			if(text_ids != '') {
				ids = text_ids.split(';');

				$.each(ids, function(index, value) {
					if(value != '') {
						if(qtd == 0) {
							id_list += value.trim();
						} else {
							id_list += '&'+value.trim();
						}
					}

					qtd++;
				});

				window.location = WEBROOT_URL + SSLUG + '/rastreio-rapido/' + id_list;
			} else {
				$('.btn-rastrear').before('<p class="alert-error" style="color: red">Digite pelo menos o código de 1 pedido!</p>');
			}

			return false;
		});
	});
</script>