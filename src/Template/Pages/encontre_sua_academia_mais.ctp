<script type="text/javascript">
	$(document).ready(function() {
		var mais_iniciado = 0;
	    $('#exibicao-academias-encontre').scroll(function () {
            if($(this).scrollTop() >= ($('.academias_exibidas_mais').height() - 1400) && mais_iniciado == 0) {
            	mais_iniciado = 1;
            	$.get(WEBROOT_URL + '/academia/mais/',
	            function (data) {
	            	$('.academias_carregar_mais').append(data);
	                mais_iniciado = 0;
	            });
            }
        });
	});
</script>

<?php foreach($academias as $academia) { ?>
	<div class="col-sm-12 col-md-6  total-thumbnail">
    	<div class="thumbnail">
    		<div class="details">
    			<?php if (!$this->request->is('mobile')) { ?>
    			<div id="myCarousel-<?= $academia->id ?>" class="carousel slide" data-ride="carousel">
			    <!-- Wrapper for slides -->
				    <div class="carousel-inner">
				    	<?php $contador = 0; ?>
				    	<?php foreach($galeria as $imagem) { ?>
				    		<?php if($imagem[0]->academia_id == $academia->id) { ?>
						    	<div class="item item-bg <?= $contador == 0 ? 'active' : '' ?>" style="background-image:url('img/<?= $imagem[1] == 'galeria/' ? $imagem[1].'lg-'.$imagem[0]->image : '' ?><?= $imagem[1] == 'banners/' ? $imagem[1].$imagem[0]->image : '' ?>')">
						      	</div>
					      		<?php $contador++; ?>
                        	<?php } ?>
		                <?php } ?>
		                <?php if($contador == 0) { ?>
		                	<?php 
			                	$images_padrao = [
			                		'image-padrao1.jpg',
			                		'image-padrao2.jpg',
			                		'image-padrao3.jpg',
			                		'image-padrao4.jpg'
			                	];

			                	shuffle($images_padrao); 
		                	?>
							<div class="item item-bg active" style="background-image: url('img/academias/<?= $images_padrao[0]?>');">
		      				</div>
		                <?php } ?>
				    </div>
			    	<!-- Left and right controls -->
				    <a class="left carousel-control" href="#myCarousel-<?= $academia->id ?>" data-slide="prev">
				      <span class="glyphicon glyphicon-chevron-left"></span>
				      <span class="sr-only">Previous</span>
				    </a>
				    <a class="right carousel-control" href="#myCarousel-<?= $academia->id ?>" data-slide="next">
				      <span class="glyphicon glyphicon-chevron-right"></span>
				      <span class="sr-only">Next</span>
				    </a>
				</div>
				<?php } ?>
				<?php
					$tamMax = 80;
			        $contador = 0;
			        $estrelas = 0;
			        $total_estrelas = 0;

			        $estrelas_txt = "";

			        foreach ($avaliacoes as $avaliacao) {
			        	if($avaliacao->academia_id == $academia->id) {
				            $estrelas += $avaliacao->rating;
				            $contador++;
			        	}
			        }

			        if($contador >= 1) {
			        	$total_estrelas = $estrelas / $contador;
			        }

			        $estrelas_txt = "";
			        for($i = 1; $i <= $total_estrelas; $i++) {
			        	$estrelas_txt = $estrelas_txt."<i class='fa fa-star dourado'></i>";
			        }

			        for($i = 1; $i <= (5 - $total_estrelas); $i++) {
			        	$estrelas_txt = $estrelas_txt."<i class='fa fa-star'></i>";
			        } 


		        	$modalidades_txt = "";

		        	$contador = 0;

		        	foreach ($modalidades as $modalidade) {
			        	if($modalidade->academia_id == $academia->id) {
			        		if($contador == 0) {
			        			$modalidades_txt = $modalidade->esporte->name;
			        		} else {
			        			$modalidades_txt = $modalidades_txt.", ".$modalidade->esporte->name;
			        		}

			        		$contador++;
			        	}
			        } 

		        	if($contador >= 1) {
		        		$modalidades_txt = $modalidades_txt;
		        	} else {
		        		$modalidades_txt = "<p>&nbsp;</p>";
		        	}

		        	$academia_true = 0;
					$valor_novo    = 0.0;
		        	$valor         = 90000.0;
        			
        			foreach ($mensalidades as $mensalidade) {
		        		if($mensalidade->academia_id == $academia->id) {
		        			$valor      = 90000.0;
		        			$valor_novo = 0.0;
		        			$academia_true = 1;

				        	foreach ($planos as $plano) {
				        		if($plano->academia_mensalidades_id == $mensalidade->id) {
					        		$valor_novo = $plano->valor_total / $plano->time_months;

					        		if($valor_novo < $valor) {
					        			$valor = $valor_novo;
					        		}
				        		}
				        	}
		        		} else if ($academia_true == 0) {
		        			$valor_novo = 0.0;
		        		}
		        	}
		        ?>
				<a href=<?=WEBROOT_URL.$academia->slug ?>>
					<div class="description">
						<div class="description-top">
							<?php if($academia->image != null) { ?>
				    			<div class="logo">
				    				<div class="logo-inner">
				    					<?= $this->Html->image('academias/'.$academia->image,['title' => 'logo academia '.$academia->shortname, 'alt' => 'Logo']) ?>
				    				</div>
				    			</div>
				    		<?php } ?>
			    			<div class="text-name">
			    				<p><strong><?= $academia->shortname ?></strong></p>
								<p><?= $estrelas_txt ?></p>
			    			</div>
		    			</div>
		    			<div class="description-bot">
		    				<p><strong>Endereço:</strong> <?= $academia->address_acad ?>, <?= $academia->number_acad ?> - <?= $academia->area_acad ?> - <?= $cidades_list[$academia->city_id_acad]['cidade'] ?>, <?= $cidades_list[$academia->city_id_acad]['estado'] ?></p>
		    			</div>
		    			<div class="ver-perfil">
		    				<p>ver loja</p>
		    			</div>
	    			</div>
    			</a>
    		</div>
    	</div><!-- thumbnail -->
  	</div>
<?php } ?>
