<!DOCTYPE html>
<html lang="pt_br">
	<head>

		<title>LOGFITNESS</title>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="">
		<meta name="description" content="">

    	<?= $this->Html->css('bootstrap.min.css')?>
    	<?= $this->Html->css('academia_new_animate.min.css')?>
    	<?= $this->Html->css('academia_new_bootstrap.min.css')?>
    	<?= $this->Html->css('font-awesome.min.css')?>
    	<?= $this->Html->css('academia_new_templatemo-style.css')?>
    	<?= $this->Html->css('media_queries.min.css')?>
    	<?= $this->Html->css('owl.carousel.css')?>
    	<?= $this->Html->css('owl.theme.css')?>

		<style>
			.fa-index{
				color: #f4d637;
			}
			.logo_navbar{
            	width: 170px;
        	}
        	.active-sobrenav{
      			color: #f3d012!important;
		    }
		    .sobrenavbar{
		      height: 25px;
		      padding: 2px 30px;
		      background-color: #5089cf;
		    }
		    .sobrenavbar span{
		      font-size: 14px;
		      font-family: nexa;
		      font-weight: bold;
		      color: #FFF;
		      margin: 0 15px;
		      cursor: pointer;
		      text-transform: none;
		    }
		    .sobrenavbar span:hover{
		      color: #f3d012;
		    }
		    .logo-login {
		        width: 50%;
		        height: auto;
		        margin-top: 5px;
		        margin-bottom: 5px;
		    }
		    .identificacao{
		        font-family: Nexa;
		        color: #698cc3;
		        font-size: 20px;
		    }
		    @media all and (max-width: 768px){
		        .sobrenavbar{
		            display: none;
		        }
		    }
		</style>

    	<script>
        <?= file_get_contents(JS_URL.'vendor/jquery-1.8.2.min.js') ?>
        <?= file_get_contents(JS_URL.'vendor/jquery-1.11.2.min.js') ?>
        <?= file_get_contents(JS_URL.'vendor/modernizr-2.8.3-respond-1.4.2.min.js') ?>
        <?= file_get_contents(JS_URL.'owl-carousel/owl.carousel.js') ?>
        </script>

		<?= $this->Html->css('calculadora') ?>

	</head>
	<body>
		<div id="submenu"></div>
		<!-- start navigation -->
        <nav class="navbar navbar-default navbar-fixed-top templatemo-nav" role="navigation" style="z-index: 5">
	        <div class="col-xs-12 sobrenavbar text-right">
	    		<a href="https://logfitness.com.br/home"><span>Loja</span></a> <a href="https://logfitness.com.br" target="_blank"><span>Entenda a LOG</span></a> <span id="trustvox-selo-site-sincero" class="logo-trustvox ts-modal-trigger">LOG Sincera</span>  <a href="https://logfitness.com.br/academia/acesso"><span>Admin Academia</span></a> <a href="https://logfitness.com.br/professor/acesso"><span>Admin Professor</span></a> 
			</div> <!-- sobrenavbar -->
            <div class="container">
                <div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<a href="#home" class="navbar-brand"><?= $this->Html->image('logfitness.png', ['class' => 'logo_navbar'])?></a>
				</div>
                <div class="menu-navbar">
					<ul class="nav navbar-nav navbar-right text-uppercase">
						<li><a href="#home" class="menu-home">Início</a></li>
						<li><a href="#divider" class="menu-vantagens">Vantagens</a></li>
						<li><a href="#funciona" class="menu-funciona">Como Funciona</a></li>
						<li><a href="#depoimentos" class="menu-depoimentos">Depoimentos</a></li>
						<li><?= $this->Html->link('Login', WEBROOT_URL.'academia/acesso' ,['escape' => false]) ?></li>
						<li><a href="#tenhalog" class="menu-cadastrar">Cadastre-se</a></li>
					</ul>
				</div>
			</div>
		</nav>

		<!-- end navigation -->
		<!-- start home -->
		<section id="home">
			<div class="overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?= $this->Html->image('texto_topo.png', ['alt' => 'Prática Interativa & integrada', 'id' => 'txt_topo']); ?>
						</div>
					</div>
				</div>
			</div>
			<?= $this->Html->image('pcs_site.png', ['alt' => 'Homepage index', 'id' => 'computadores']); ?>
		</section>
		<!-- end home -->

		<!-- start sobre -->
		<section id="sobre">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
					<br/><p>Conheça a <strong>plataforma online</strong><br/> que está mudando a maneira de<br/> se manter uma vida <strong>mais saudável.</strong></p>
					</div>

					<div class="col-md-12">
                    <p><br/>A <strong>LOGFITNESS</strong> aliou o <strong>preço, variedade e comodidade</strong> do <span style="white-space: nowrap"> e-commerce</span><br/> com o a <strong>segurança</strong> e <strong>atendimento personalizado</strong> de uma loja física.<br/> Proporcionando uma experiência completamente <strong>NOVA</strong>.</p>
                    </div>
				</div>
			</div>
		</section>
		<!-- end sobre -->

		<!-- Inicio Vantagens -->
		<section id="divider">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_vantagens" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_vantagens">Fazer parte do nosso time é<br/>
							vantajoso para sua academia<br/>
							e para seus alunos.XXXX</p>
						</div>
					</div>
					<div class="col-md-12">
                		<div class="fundo_vantagens1"><p class="eftspacing"><strong>academia & aluno</strong><br/><br/>
                		COM A PRATICIDADE NA HORA DA ENTREGA E<br/>
						ATENDIMENTO PERSONALIZADO A RELAÇÃO ENTRE<br/>
						A ACADEMIA E ALUNO TEM UM NOVO PATAMAR.</p></div>
            			<div class="fundo_vantagens2"><p class="eftspacing"><strong>mais completa</strong><br/><br/>
            			COM A SUA PÁGINA EXCLUSIVA<br/>
            			(WWW.LOGFITNESS.COM.BR/SUAACADEMIA)<BR/>
            			SUA ACADEMIA SERÁ AINDA MAIS COMPLETA. </p></div>
					</div>
					<div class="col-md-12">
            			<div class="fundo_vantagens3"><p class="eftspacing"><strong>fonte de recursos</strong><br/><br/>
            			QUANTO MAIS ALUNOS COMPRAREM, MAIOR SERÃO<br/>
						SEUS GANHOS. AUMENTE OS SEUS LUCROS!</p></div>
            			<div class="fundo_vantagens4"><p class="eftspacing"><strong>e muito mais</strong><br/><br/>
            			AS VANTAGENS DE SER LOG NÃO<br/>
						PARAM POR AQUI. EXPLORE NOSSO MUNDO<br/>
						E DESCUBRA O PORQUE FAZER PARTE DESTE TIME.</p></div>
            		</div>
				</div>
			</div>
		</section>
		<!-- end vantagens -->

		<script type="text/javascript">
			$(document).ready(function() {
				$('.calculadora-btn').click(function() {
					var qtd_alunos = $('.qtd_alunos').val();
					var valor_mensalidade = $('.valor_mensalidade').val();
					var tem_professor = $('.tem_prof:checked').val();

					$('.erro-calc').remove();

					if(qtd_alunos >= 1 && valor_mensalidade >= 1 && tem_professor != undefined) {
						var gasto_medio = valor_mensalidade * .80;

						if(tem_professor == 'sim') {
							var qtd_alunos_compram = qtd_alunos * .65;
							var comissao = .10;
						} else {
							var qtd_alunos_compram = qtd_alunos * .25;
							var comissao = .20;
						}

						var ganho_com_log = (parseInt(qtd_alunos_compram) * gasto_medio) * comissao;

						$('.ganho_com_log').html('R$'+parseInt(ganho_com_log)+',00');
						$('.calculado').slideDown();
					} else {
						$('.btn-div').append('<p class="erro-calc" style="color: red">Preencha todos os dados!</p>');
					}
				});
			});
		</script>

		<!-- start calculadora log -->
		<section id="calculadora"  style="margin: 20px 0;">
			<div class="container">
				<div class="col-xs-12 text-center"><h1 style="color: #4e89c7; font-family: nexa_boldregular">Você não vai acreditar quando ver o quanto a sua academia pode ganhar <strong>POR MÊS</strong> com a LOG!</h1></div>
				<div class="calc-total text-center">
					<div class="valores">
						<div class="col-xs-12 text-center total-ganho" style="margin-bottom: 30px; margin-top: 15px;">
							<p class="ganho_com_log"></p>
						</div>
						<div class="calculadora">
							<div class="col-xs-12 form-single">
								<p>Qtd de alunos</p>
								<input type="number" class="qtd_alunos">
							</div>
							<div class="col-xs-12 form-single">
								<p>Valor da mensalidade</p>
								<input type="number" class="valor_mensalidade">
							</div>
							<div class="col-xs-12 form-single">
								<p>Tem professor?</p>
								<span class="tem_prof"><input type="radio" name="prof" class="tem_prof" value="sim"> Sim</span>
								<span class="tem_prof"><input type="radio" name="prof" class="tem_prof" value="nao"> Não</span>
							</div>
							<div class="col-xs-12 text-center btn-div" style="margin: 5px 0">
								<button type="button" class="calculadora-btn btn btn-success btn-calcular">Calcular</button>
							</div>
						</div>
					</div>
				</div>
				<div class="formulario_log text-left">
					<div class="col-sm-12">
						<p style="font-size: 9px">*Informações</p>
						<p style="text-align: left; font-size: 9px;">Nós calculamos o poder aquisitivo de um aluno com base na mensalidade paga, então estimamos que ele gaste 80% do valor da mensalidade em suplementos.<br>Se a academia tem professor cadastrado na LOG, a média de alunos que compram é de 65% da quantidade total de alunos da academia, pois os alunos tendem a confiar mais em seus professores que acompanham seu treino, já uma academia sem professor cadastrado na LOG, a média de alunos que compram é de 25%.<br>Calculamos então a comissão mensal de 10%(com professor) e 20%(sem professor), contando com 65% dos alunos(com professor) ou 25% dos alunos(sem professor) com um gasto médio de 80% da mensalidade.<br><br>Valores com base em academias reais e felizes com LogFitness</p>
					</div>
				</div>
			</div>
		</section>
		<!-- end calculadora log -->

		<!-- start funciona -->
		<section id="funciona">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_como_funciona" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_como_funciona">
							A LOG permite que sua academia tenha<br/>
							uma loja completa e personalizada sem<br/>
							você se preocupar com controle de estoque,<br/>
							gerenciamento financeiro ou emissão de notas.</p>
						</div>
					</div>
					<div class="col-md-3 wow fadeInUp templatemo-box" data-wow-delay="0.3s">	
                    	<span><?= $this->Html->image('icone1.png', ['alt' => 'A academia faz seu cadastro na plataforma', 'id' => 'img_funciona']); ?></span>
                    	<p align="center" id="txt_funciona"><br/>A ACADEMIA FAZ O SEU CADASTRO NA PLATAFORMA.</p>
					</div>
					<div class="col-md-3 wow fadeInUp templatemo-box" data-wow-delay="0.3s">
                		<span><?= $this->Html->image('icone2.png', ['alt' => 'Após a aprovação, a academia recebe seu login e senha da sua loja virtual', 'id' => 'img_funciona']); ?></span>
                		<p align="center" id="txt_funciona"><br/>APÓS A APROVAÇÃO, A ACADEMIA RECEBE SEU LOGIN E SENHA DA SUA LOJA VIRTUAL.</p>
					</div>
					<div class="col-md-3 wow fadeInUp templatemo-box" data-wow-delay="0.3s">
                		<span><?= $this->Html->image('icone3.png', ['alt' => 'De maneira simples você personaliza sua página', 'id' => 'img_funciona']); ?></span>
                		<p align="center" id="txt_funciona"><br/>DE MANEIRA SIMPLES VOCÊ PERSONALIZA SUA PÁGINA.</p>
					</div>
					<div class="col-md-3 wow fadeInUp templatemo-box" data-wow-delay="0.3s">
                		<span><?= $this->Html->image('icone4.png', ['alt' => 'com a loja pronta é só divulgar a novidade para os alunos', 'id' => 'img_funciona']); ?></span>
                		<p align="center" id="txt_funciona"><br/>COM A LOJA PRONTA É SÓ DIVULGAR A NOVIDADE PARA OS ALUNOS. ELES VÃO ADORAR!</p>
					</div>
				</div>
			</div>
		</section>
		<!-- end funciona -->

		<!-- start video institucional -->
		<section id="video">
			<div class="container">
				<div class="row">
					<div class="video-container">
						<iframe width="100%" height="315" src="https://www.youtube.com/embed/Nr7UNxpzO18" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</section>
		<!-- end video institucional -->

		<!-- start professor -->
		<section id="professor">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_professor_comissionado" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_professor_comissionado">Quando a venda é feita através do perfil da<br/>
							academia e o aluno seleciona um professor a<br/>
							comissão é dividida ao meio.<br/>
							Metade academia, metade professor.</p>
						</div>
					</div>
					<div class="col-md-6 ">
						<br/>
						<br/>
						<p id="txt_professor">O professor é quem conhece melhor o treino e as necessidades do aluno. Com isso ele torna-se um multiplicador do seu perfil LOG dentro da sua academia..</p><br/>
						<p id="txt_professor"><strong>TODOS</strong> ganham. Academia aumentando as vendas dentro do seu perfil dentro da LOG, o professor atendendo melhor seu aluno e o aluno tendo os melhores resultados assistido por quem ele mais confia!</p>
					</div>
					<div class="col-md-6"  id="img_professor">
						<?= $this->Html->image('professor.jpg', ['alt' => 'Professor comissionado', 'class' => 'img-responsive']); ?>
					</div>
				</div>
			</div>
		</section>
		<!-- end professor -->

		<!-- start depoimentos-->
		<section id="depoimentos">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_depoimentos" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_depoimentos">Separamos o depoimento de algumas<br/>
							pessoas que já fazem parte do nosso time.<br/>
							<strong>CONFIRA!</strong></p>
						</div>
						<div id="bg-asset"></div>
						<div id="owl-demo" class="owl-carousel owl-theme">
							<div class="item"><?= $this->Html->image('depoimento1.jpg',['alt' => 'Depoimento 1',]); ?></div>
							<div class="item"><?= $this->Html->image('depoimento4.jpg',['alt' => 'Depoimento 4',]); ?></div>
							<div class="item"><?= $this->Html->image('depoimento2.jpg',['alt' => 'Depoimento 2',]); ?></div>
							<div class="item"><?= $this->Html->image('depoimento3.jpg',['alt' => 'Depoimento 1',]); ?></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end depoimentos -->

		<!-- start tenhalog-->
		<section id="tenhalog">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_tenha_log" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_tenha_log">Preencha as informações abaixo e<br/>
							envie o formulário de solicitação ou<br/>
							tire suas dúvidas com nosso contato.</p>
						</div>
					</div>
					<div class="col-md-4 col-md-offset-2">
						<input type="button" class="botoes" id="btn_cadastrar" value="Cadastrar">
					</div>
					<div class="col-md-4">
						<input type="button" class="botoes" id="btn_duvidas" value="Tirar dúvidas">
					</div>
				</div>
			</div>
		</section>
		<!-- end tenhalog -->

		<!-- start cadastre-se-->
		<section id="cadastre-se">
			<div class="overlay">
				<div class="container" id="cadastro-academia">
						<div class="col-md-12">
							<?= $this->Form->create($academia, [
								'id'        => 'form-cadastrar-academia',
								'role'      => 'form',
								'default'   => false,
								//'type'      => 'file'
							]) ?>
							<div class="form-group row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<?= $this->Form->input('name', [
										'id'            => 'academia_name',
										'div'           => false,
										'label'         => 'Razão Social*',
										'placeholder'   => 'Razão Social',
										'class'         => 'validate[required] form-control',
									]) ?>
									<br/>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<?= $this->Form->input('valid_name', [
										'div'           => false,
									]) ?>
									<br/>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<?= $this->Form->input('shortname', [
										'div'           => false,
										'maxlength'     => 22,
										'label'         => 'Nome Curto*',
										'placeholder'   => 'Nome Fantasia / Nome de Fachada / Marca Empresarial',
										'class'         => 'validate[required,maxSize[22]] form-control',
									]) ?>
									<br/>
								</div>
							</div>
							<div class="form-group row">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <?= $this->Form->input('slug', [
                                        'value'         => 'logfitness.com.br/',
                                        'div'           => false,
                                        'disabled'      => true,
                                        'readonly'      => true,
                                        'label'         => 'URL Personalizada',
                                        'placeholder'   => 'nome-personalizado-da-sua-academia',
                                        'class'         => 'slug-academia-texto form-control text-right',
                                    ]) ?>
                                </div>

								<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
									<?= $this->Form->input('slug', [
										'div'           => false,
										'label'         => '*',
										'placeholder'   => 'sua-academia',
										'class'         => 'validate[required,custom[slug]]] form-control',
									]) ?>
									<br/>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<?= $this->Form->input('cnpj', [
										'div'           => false,
										'label'         => 'CNPJ*',
										'placeholder'   => 'CNPJ',
										'class'         => 'validate[required,custom[cnpj]] form-control cnpj',
									]) ?>
									<br/>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<?= $this->Form->input('ie', [
										'div'           => false,
										'label'         => 'Inscrição Estadual',
										'placeholder'   => 'Inscrição Estadual',
										'class'         => 'validate[optional] form-control',
									]) ?>
									<br/>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<?= $this->Form->input('contact', [
										'div'           => false,
										'label'         => 'Responsável*',
										'placeholder'   => 'Nome do Responsável',
										'class'         => 'validate[required] form-control',
									]) ?>
									<br/>
								</div>
								<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
									<?= $this->Form->input('phone', [
										'div'           => false,
										'label'         => 'Telefone 1*',
										'placeholder'   => '(99)9999-9999',
										'class'         => 'validate[required] form-control phone-mask',
									]) ?>
								</div>
								<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
									<?= $this->Form->input('mobile', [
										'div'           => false,
										'label'         => 'Telefone 2',
										'placeholder'   => '(99)99999-9999',
										'class'         => 'validate[optional] form-control phone-mask',
									]) ?>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<?= $this->Form->input('contact_partner', [
										'div'           => false,
										'label'         => 'Segundo Responsável',
										'placeholder'   => 'Nome do Segundo Responsável',
										'class'         => 'validate[optional] form-control',
									]) ?>
									<br/>
								</div>
								<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
									<?= $this->Form->input('phone_partner', [
										'div'           => false,
										'label'         => 'Telefone 1',
										'placeholder'   => '(99)9999-9999',
										'class'         => 'validate[optional] form-control phone-mask',
									]) ?>
								</div>
								<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
									<?= $this->Form->input('mobile_partner', [
										'div'           => false,
										'label'         => 'Telefone 2',
										'placeholder'   => '(99)99999-9999',
										'class'         => 'validate[optional] form-control phone-mask',
									]) ?>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
									<?= $this->Form->input('alunos', [
										'div'           => false,
										'type'          => 'number',
										'label'         => 'Quantidade de alunos*',
										'placeholder'   => 'Ex: 150',
										'class'         => 'validate[required] form-control',
									]) ?>
									<br/>
								</div>
							</div>
							<hr> <!-- barra divisor -->
							<div class="form-group row">
								<div class="col-md-12">
									<h4 class="font-bold"><i class="fa fa-plus-circle"></i> Informe quais modalidades a academia oferece</h4>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-12">
									<?php
									foreach($esportes_list as $ke => $esporte):
										echo '<div class="col-lg-4 font-bold"><label for="esporte_'.$ke.'">';
										echo $this->Form->checkbox('esportes.'.$ke, [
											'id'            => 'esporte_'.$ke,
											'hiddenField'   => 'N',
											'value'         => 'Y',
											'label'         => false,
											'class'         => 'custom-checkbox'
										]);
										echo ' '.$esporte;
										echo '</<label></div>';
									endforeach;
									?>
									<br/>
								</div>
							</div>
							<hr> <!-- barra divisor -->
							<div class="form-group row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<?= $this->Form->input('email', [
										'div'           => false,
										'label'         => 'E-mail*',
										'placeholder'   => 'E-mail',
										'class'         => 'validate[required,custom[email]] form-control',
									]) ?>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
									<?= $this->Form->input('password', [
										'div'           => false,
										'label'         => 'Senha*',
										'placeholder'   => 'Senha para acesso',
										'class'         => 'validate[required] form-control',
									]) ?>
									<br/>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
									<?= $this->Form->input('password_confirm', [
										'div'           => false,
										'label'         => 'Repetir senha*',
										'placeholder'   => 'Confirme sua senha',
										'class'         => 'validate[required] form-control',
										'type'          => 'password',
									]) ?>
									<br/>
								</div>
							</div>
							<hr> <!-- barra divisor -->
							<div class="form-group row">
								<div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
									<?= $this->Form->input('cep', [
										'div'           => false,
										'label'         => 'CEP*',
										'placeholder'   => 'CEP',
										'class'         => 'validate[required] form-control cep',
									]) ?>
								</div>
								<div class="col-xs-8 col-sm-3 col-md-3 col-lg-3">
									<?= $this->Form->input('address', [
										'div'           => false,
										'label'         => 'Endereço*',
										'placeholder'   => 'Endereço',
										'class'         => 'validate[required] form-control',
									]) ?>
								</div>
								<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
									<?= $this->Form->input('number', [
										'div'           => false,
										'label'         => 'Número*',
										'placeholder'   => 'Número',
										'class'         => 'validate[required] form-control',
									]) ?>
									<br/>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<?= $this->Form->input('complement', [
										'div'           => false,
										'label'         => 'Complemento',
										'placeholder'   => 'Ex: 2º andar',
										'class'         => 'validate[optional] form-control',
									]) ?>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<?= $this->Form->input('area', [
										'div'           => false,
										'label'         => 'Bairro*',
										'placeholder'   => 'Bairro',
										'class'         => 'validate[required] form-control',
									]) ?>
									<br/>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-3">
									<?= $this->Form->input('uf_id', [
										'id'            => 'states',
										'div'           => false,
										'label'         => 'Estado*',
										'options'       => $states,
										'empty'         => 'Selecione um estado',
										'placeholder'   => 'Estado',
										'class'         => 'validate[required] form-control',
									]) ?>
								</div>
								<div class="col-md-6 col-lg-offset-3">
									<?= $this->Form->input('city_id', [
										'id'            => 'city',
										'div'           => false,
										'label'         => 'Cidade*',
										'empty'         => 'Selecione uma cidade',
										'placeholder'   => 'Cidade',
										'class'         => 'validate[required] form-control',
									]) ?>
								</div>
								<br/>
							</div>

							<!-- <hr> < barra divisor
                                                            <div class="form-group row">
                                                                <div class="col-lg-6">
                                                                    <div class="input file"><label for="image_upload">Logotipo</label><input type="file" name="imagem" id="image_upload" placeholder="Logotipo" class="validate[custom[validateMIME[image/jpeg|image/png]]] form-control">
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-3">
                                                                    <img src="images/logfitness.png" id="image_preview" alt="Image Upload"/>
                                                                </div>
                                                            </div>
                            -->

							<hr> <!-- barra divisor -->
							<div class="row-md-12">
								<div class="col-md-3">
									<?= $this->Html->link('Leia os Termos',
										'/files'.DS.'Termos-de-Aceite.pdf',
										['target' => '_blank'])
									;?>
								</div>
								<div class="col-md-4">
									<label for="aceite-termos">&nbsp&nbsp&nbsp&nbspLi e aceito os termos de aceite
										<?= $this->Form->input('aceite_termos', [
											'type' => 'checkbox',
											'label' => false,
											'class' => 'text-center checkbox-aval validate[required]',
											'style' => 'margin: -27px 0'
										])?>
									</label>
								</div>
								<div class="form-group row">
									<div class="col-md-2">
										<?= $this->Form->button('Enviar Cadastro', [
											'id'            => 'submit-cadastrar-academia',
											'type'          => 'button',
											'div'           => false,
											'class'         => 'btn btn-default float-right',
										]) ?>
									</div>
									<div class="col-md-5" id="form-toggle-message-academia"></div>
								</div>
							</div>
							<?= $this->Form->end() ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end cadastre-se -->

		<!-- start contact -->
		<section id="duvidas">
			<div class="overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="contact-form">
                                <?= $this->Form->create(null, [
                                    'id'        => 'form-central-relacionamento',
                                    'role'      => 'form',
                                    'default'   => false,
                                    'type'      => 'file'
                                ]) ?>
                                <div class="form-group row">
									<div class="col-md-12">
                                        <?= $this->Form->input('name', [
                                            'id'            => 'contato-name',
                                            'div'           => false,
                                            'label'         => 'Nome',
                                            'placeholder'   => 'Nome Completo',
                                            'class'         => 'validate[required] form-control',
                                        ]) ?>
                                        <br/>
									</div>
                                </div>
                                <div class="form-group row">
									<div class="col-md-6">
                                        <?= $this->Form->input('phone', [
                                            'id'            => 'contato-phone',
                                            'div'           => false,
                                            'label'         => 'Telefone',
                                            'placeholder'   => 'Telefone',
                                            'class'         => 'validate[required] form-control phone-mask',
                                        ]) ?>
									</div>

									<div class="col-md-6">
                                        <?= $this->Form->input('email', [
                                            'id'            => 'contato-email',
                                            'div'           => false,
                                            'label'         => 'E-mail',
                                            'placeholder'   => 'E-mail',
                                            'class'         => 'validate[required,custom[email]] form-control',
                                        ]) ?>
                                        <br/>
									</div>
                                </div>
                                <div class="form-group row">
									<div class="col-md-6">
                                        <?= $this->Form->input('description', [
                                            'id'            => 'contato-description',
                                            'div'           => false,
                                            'label'         => 'Assunto',
                                            'placeholder'   => 'Assunto / Descrição',
                                            'class'         => 'validate[required] form-control',
                                        ]) ?>
                                        <br/>
									</div>
                                </div>
                                <div class="form-group row">
									<div class="col-md-12">
                                        <?= $this->Form->input('message', [
                                            'id'            => 'contato-name',
                                            'type'          => 'textarea',
                                            'div'           => false,
                                            'label'         => 'Mensagem',
                                            'placeholder'   => 'Mensagem',
                                            'class'         => 'validate[required] form-control',
                                        ]) ?>
                                        <br/>
									</div>
                                </div>
									<div class="col-md-8">
                                        <?= $this->Form->button('Enviar Contato', [
                                            'id'            => 'submit-cadastrar-central-relacionamento',
                                            'type'          => 'button',
                                            'div'           => false,
                                            'class'         => 'btn btn-default float-right',
                                        ]) ?>
									</div>
                                <div class="col-lg-5" id="form-toggle-message"></div>
							</div>
                            <?= $this->Form->end() ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		 <!-- end contact -->

		<!-- start quemsou -->
		<section id="quemsou">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-xs-12" id="titulo_quem_sou" >

					</div>
					<div class="col-md-6 col-xs-12">
						<p class="txt_titulos_quem_sou">Se você é um aluno e está buscando<br/>
							pelos melhores produtos do mercado<br/>
							ou é um professor querendo dar um UP<br/>
							em sua renda acesse a página que<br/>
							criamos exclusivamente pra você !</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 col-md-offset-2">
						<?= $this->Html->link('<input type="button" class="botoes" id="btn_aluno" value="Aluno">', WEBROOT_URL ,['escape' => false]) ?>
					</div>
					<div class="col-md-4">
						<?= $this->Html->link('<input type="button" class="botoes" id="btn_professor" value="Professor">', WEBROOT_URL.'professor' ,['escape' => false]) ?>
					</div>
				</div>
			</div>
		</section>
		<!-- end quemsou -->

		<!-- start social media-->

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:45px; background:#5089cf; margin-bottom: 25px;">
			<div class="row">
				<div class="col-xs-12" style="text-align: center">
					<div class="social_icon">
						<a href="https://open.spotify.com/user/logfitness"  target="_blank"><i class="fa fa-2x fa-fw fa-spotify fa-index" title="Acompanhe nossa playlist"></i></a>
					</div>
					<div class="social_icon">
						<a href="https://www.facebook.com/logfitness.com.br" class="btn-social btn-outline" target="_blank"><i class="fa fa-2x fa-fw fa-facebook fa-index" title="Curta nossa fanpage"></i></a>
					</div>
					<div class="social_icon">
						<a href="https://www.instagram.com/logfitness.com.br/" class="btn-social btn-outline" target="_blank"><i class="fa fa-2x fa-fw fa-instagram fa-index" title="Siga nosso Instagram"></i></a>
					</div>
					<div class="social_icon">
						<a href="https://twitter.com/LOGFITNESS" class="btn-social btn-outline" target="_blank"><i class="fa fa-2x fa-fw fa-twitter fa-index" title="Siga nosso Twitter"></i></a>
					</div>	
				</div>
			</div>
		</div>

		<!-- end social media-->

		<!-- Contato -->
		<div class="container info-central-box form-toggle" id="form-toggle-contato">

            <?= $this->Form->create(null, [
                'id'        => 'form-central-relacionamento',
                'role'      => 'form',
                'default'   => false,
                'type'      => 'file'
            ]) ?>

            <div class="form-group row">
                <div class="col-lg-8">
                    <?= $this->Form->input('name', [
                        'id'            => 'contato-name',
                        'div'           => false,
                        'label'         => 'Nome',
                        'placeholder'   => 'Nome Completo',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-3">
                    <?= $this->Form->input('phone', [
                        'id'            => 'contato-phone',
                        'div'           => false,
                        'label'         => 'Telefone',
                        'placeholder'   => 'Telefone',
                        'class'         => 'validate[required] form-control phone-mask',
                    ]) ?>
                </div>

                <div class="col-lg-5">
                    <?= $this->Form->input('email', [
                        'id'            => 'contato-email',
                        'div'           => false,
                        'label'         => 'E-mail',
                        'placeholder'   => 'E-mail',
                        'class'         => 'validate[required,custom[email]] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-8">
                    <?= $this->Form->input('description', [
                        'id'            => 'contato-description',
                        'div'           => false,
                        'label'         => 'Assunto',
                        'placeholder'   => 'Assunto / Descrição',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-8">
                    <?= $this->Form->input('message', [
                        'id'            => 'contato-name',
                        'type'          => 'textarea',
                        'div'           => false,
                        'label'         => 'Mensagem',
                        'placeholder'   => 'Mensagem',
                        'class'         => 'validate[required] form-control',
                    ]) ?>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-lg-2">
                    <?= $this->Form->button('Enviar Contato', [
                        'id'            => 'submit-cadastrar-central-relacionamento',
                        'type'          => 'button',
                        'div'           => false,
                        'class'         => 'btn btn-default float-right',
                    ]) ?>

                </div>

                <div class="col-lg-5" id="form-toggle-message"></div>
            </div>

            <?= $this->Form->end() ?>
        </div>

		<!-- End contato -->
 <section class="midia" id="midia">
         <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                   <p style="font-size: 80px">O que falam de nós</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-2 col-lg-offset-1 text-center alinhar-centro logos-falam">
                    <a target="_blank" href="http://exame.abril.com.br/estilo-de-vida/10-mitos-sobre-o-uso-de-suplementos-de-academia/">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="195px" height="60px" viewBox="0 0 195 60" xml:space="preserve" class="exame-logo">
                        <g id="primary">
                            <path d="M122.107,16.082c-0.143-0.081-0.667,0.081-0.771,0.609c0,0.425,0.246,0.729,0.792,0.89
                                c1.012,0.304,1.315,0.283,1.599,0.446c0.384,0.222,1.316,0.566,1.459,3.969c0.099,4.031-0.041,9.3-0.082,14.079
                                c-0.021,4.539-0.567,4.561-1.437,5.167c-1.116,0.447-1.644,0.75-1.621,1.277c0,0.384,0.284,0.771,1.437,0.771
                                c3.548-0.043,6.525-0.103,9.402-0.103c2.695,0,5.267,0.039,8.123,0.141c1.479,0.182,1.417-0.404,1.417-0.93
                                c-0.019-0.487-0.2-2.817,0.104-4.924c0-1.662-0.427-1.521-0.729-1.541c-0.406-0.059-0.852,0.202-0.852,1.054
                                c-0.02,0.812,0.182,2.917-3.425,3.263c-2.188,0.242-3.464,0.325-4.578,0.283c-1.541,0-1.824-0.02-1.845-0.952
                                c0-2.775,0.081-6.542,0.122-8.205c0.062-1.275,1.196-1.195,2.271-1.257c1.214-0.04,3.686-0.06,4.395,2.513
                                c0.325,1.339,0.588,1.316,1.014,1.257c0.467-0.081,0.668-0.345,0.607-1.337c-0.06-0.952-0.264-5.146,0.061-7.153
                                c0.08-0.971-0.04-1.215-0.466-1.335c-0.445-0.083-0.911,0.184-1.173,1.093c-0.266,0.506-0.935,2.148-3.731,2.289
                                c-2.835,0.121-3.018-0.039-2.977-1.721c-0.019-1.723,0.021-5.531,0.021-5.552c-0.04-0.304-0.04-1.519,1.294-1.419
                                c1.704-0.019,4.701,0.083,5.451,0.244c0.771,0.182,2.815,0.386,3.062,3.485c0.117,0.85,0.564,0.831,0.809,0.85
                                c0.324,0.022,0.708-0.162,0.607-0.992c-0.121-0.811-0.061-3.91,0.082-5.002c0.059-1.155-0.508-1.136-1.237-1.177
                                c-0.466,0-5.957,0.204-9.985,0.306C128.974,16.569,122.107,16.082,122.107,16.082"></path>
                            <path d="M84.568,16.204c-0.588-0.06-0.81,0.224-0.851,0.607c0.042,0.407,0.102,0.529,0.587,0.811
                                c1.479,0.689,2.877,0.81,3.161,4.741v14.284c-0.202,2.066,0.163,3.383-2.593,4.861c-0.892,0.506-1.074,0.748-1.033,1.216
                                c0.102,0.588,0.628,0.588,0.647,0.568c0.771,0.101,2.655-0.185,5.106-0.082c1.601-0.062,3.952,0.142,4.863,0.06
                                c0.546-0.162,0.668-0.282,0.668-0.749c0.042-0.324-0.141-0.689-0.709-0.952c-0.851-0.407-3.403-1.013-3.626-4.902l-0.02-11.367
                                l8.063,17.183c0.081,0.242,0.366,0.849,0.909,0.811c0.446-0.022,1.156-0.689,1.5-1.439l7.578-16.45v13.574
                                c0,0.019,0.142,2.267-1.276,2.531c-0.647,0.202-0.952,0.668-0.912,1.013c0.042,0.405,0.364,0.689,0.791,0.71
                                c0.405,0.019,2.836-0.062,5.327-0.062c2.635,0,5.327,0.101,5.733,0.021c0.628,0,0.829-0.426,0.789-0.729
                                c0.062-0.59-0.484-0.791-0.81-0.933c-1.68-0.709-3.626-1.175-3.687-6.158l0.02-14.608c0.103-1.458,0.405-2.531,2.836-3.019
                                c0.953-0.384,1.176-0.728,1.137-0.992c-0.041-0.405-0.144-0.669-0.609-0.588c-0.385-0.04-1.965,0.103-3.646,0.142
                                c-1.54-0.02-2.96-0.059-3.951-0.142c-1.461,0-1.986,1.339-2.227,1.865l-7.032,15.5l-8.164-16.532c-0.304-0.81-0.75-0.771-1.5-0.833
                                C90.93,16.163,87.566,16.507,84.568,16.204"></path>
                            <path d="M0.797,16.082c-0.141-0.081-0.667,0.081-0.769,0.609c0,0.425,0.263,0.729,0.79,0.89
                                c1.013,0.304,1.316,0.283,1.6,0.446c0.386,0.222,1.317,0.566,1.46,3.969c0.101,4.031-0.042,9.3-0.083,14.079
                                c-0.019,4.539-0.566,4.561-1.417,5.167c-1.134,0.447-1.661,0.75-1.64,1.277c0,0.384,0.285,0.771,1.438,0.771
                                c3.566-0.043,6.523-0.103,9.399-0.103c2.696,0,5.268,0.039,8.125,0.141c1.478,0.182,1.417-0.404,1.417-0.93
                                c-0.019-0.487-0.202-2.817,0.102-4.924c0-1.662-0.426-1.521-0.729-1.541c-0.405-0.059-0.851,0.202-0.851,1.054
                                c-0.021,0.812,0.184,2.917-3.403,3.263c-2.208,0.242-3.483,0.325-4.599,0.283c-1.539,0-1.822-0.02-1.843-0.952
                                c0-2.775,0.082-6.542,0.141-8.205c0.041-1.275,1.175-1.195,2.25-1.257c1.215-0.04,3.688-0.06,4.396,2.513
                                c0.324,1.339,0.587,1.316,1.012,1.257c0.466-0.081,0.669-0.345,0.607-1.337c-0.06-0.952-0.262-5.146,0.062-7.153
                                c0.079-0.971-0.042-1.215-0.467-1.335c-0.446-0.083-0.913,0.184-1.155,1.093c-0.283,0.506-0.952,2.148-3.749,2.289
                                c-2.835,0.121-3.018-0.039-2.958-1.721c-0.019-1.723,0-5.531,0-5.552c-0.04-0.304-0.019-1.519,1.298-1.419
                                c1.702-0.019,4.7,0.083,5.448,0.244c0.77,0.182,2.818,0.386,3.06,3.485c0.122,0.85,0.568,0.831,0.812,0.85
                                c0.323,0.022,0.708-0.162,0.607-0.992c-0.122-0.811-0.061-3.91,0.101-5.002c0.04-1.155-0.526-1.136-1.256-1.177
                                c-0.467,0-5.957,0.204-9.987,0.306C7.665,16.569,0.797,16.082,0.797,16.082"></path>
                            <path d="M81.955,41.811c-0.607-0.261-1.682,0.103-3.12-2.856L68.442,16.386c-0.244-0.527-0.791-0.587-1.013,0
                                l-1.196,2.371L55.841,39.644c0,0-0.688,1.642-2.024,2.107c0,0.02-1.015,0.365-0.994,0.951c0,0.446,0.425,0.649,0.953,0.609
                                c2.998-0.102,4.76-0.143,7.797,0.04c0.568,0.021,0.874-0.263,0.934-0.628c0.061-0.406-0.161-0.912-0.75-1.136
                                c-1.114-0.485-2.167-0.282-1.944-1.579c0.203-1.277,1.013-3.525,1.56-4.397c0.426-0.729,0.708-0.649,1.195-0.649h3.667v0.002h3.545
                                c0.587,0.062,0.77,0.123,1.093,0.688c0.285,0.485,1.136,1.804,1.581,3.424c0.425,1.583-0.385,2.432-1.257,2.757
                                c-0.587,0.221-1.256,0.16-1.256,0.971c0.021,0.345,0.345,0.488,0.771,0.508c1.073-0.062,3.383-0.083,5.775-0.122
                                c2.006,0.039,3.403,0.021,5.288,0.141c0.75-0.019,0.891-0.361,0.911-0.628C82.726,42.196,82.44,41.952,81.955,41.811z
                                 M68.685,32.045c-0.972,0.022-1.722,0.022-2.451,0.022v-0.001c-0.769,0-1.54-0.021-2.572-0.021
                                c-0.386-0.06-0.872,0.083-0.587-0.809l3.141-6.321l0.019,0.102l2.977,5.977C69.435,31.602,69.678,32.026,68.685,32.045z"></path>
                            <path d="M24.967,16.185c0,0-1.194-0.103-1.257,0.485c0,0.364,0.042,0.669,0.69,0.891
                                c1.882,0.912,2.855,1.923,3.281,2.613c1.417,1.56,7.211,10.149,7.211,10.129c0,0.02-7.474,8.224-7.474,8.206
                                c0,0.019-2.188,2.149-4.295,3.16c-0.669,0.364-0.75,0.709-0.669,1.032c0.062,0.426,0.446,0.649,1.215,0.628
                                c1.602,0.021,5.228-0.386,10.251,0c0.709,0,1.032-0.303,1.054-0.688c0.042-0.425-0.284-0.89-0.911-1.032
                                c-1.439-0.244-3.727-1.115-1.741-3.709c1.985-2.573,4.333-5.145,4.333-5.165c0,0.021,2.736,4.053,4.155,6.159
                                c0.87,1.316,0.384,2.552-1.074,2.857c-0.527,0.223-0.647,0.506-0.647,0.932c0,0.403,0.364,0.668,1.174,0.608
                                c1.277,0.06,5.591-0.245,10.959,0c0.872,0,1.156-0.245,1.216-0.548c0.062-0.527-0.283-0.871-0.648-1.054
                                c-0.789-0.466-1.721-0.365-3.93-3.385c-2.532-3.463-7.192-9.986-7.192-10.007c0,0.021,7.76-8.449,7.76-8.449
                                c0.283-0.404,1.62-1.68,3.525-2.065c0.667-0.263,0.81-0.769,0.728-1.054c0.041-0.323-0.364-0.567-0.83-0.546
                                c-1.541,0.161-3.283,0.303-4.863,0.343c-1.62-0.04-4.091-0.202-5.57-0.343c-0.587-0.021-0.891,0.262-0.913,0.626
                                c-0.082,0.387,0.142,0.911,0.73,0.953c1.478,0.244,3.403,0.69,1.883,2.876c-1.579,2.149-4.293,5.024-4.293,5.006
                                c0,0.019-4.052-5.917-4.052-5.936c-0.203-0.364-0.812-1.539,0.81-1.925c0.851-0.223,1.095-0.467,1.013-0.953
                                c0.06-0.525-0.426-0.628-1.054-0.647c-1.234-0.062-3.829,0.364-5.812,0.303C28.29,16.386,24.967,16.185,24.967,16.185"></path>
                            <path d="M146.588,41.036"></path>
                            <path d="M146.588,43.228"></path>
                            <path d="M148.781,43.228"></path>
                            <line x1="147.242" y1="44.031" x2="147.242" y2="41.84"></line>
                            <rect x="146.878" y="41.429" width="2.191" height="2.191"></rect>
                            <path d="M160.585,31.52c-0.714-0.5-1.567-0.75-2.553-0.75c-0.845,0-1.563,0.157-2.163,0.473
                                c-0.598,0.317-1.094,0.741-1.481,1.273c-0.388,0.534-0.674,1.143-0.858,1.826s-0.277,1.395-0.277,2.132
                                c0,0.804,0.093,1.562,0.277,2.279c0.185,0.716,0.471,1.343,0.858,1.876s0.885,0.958,1.491,1.272c0.606,0.315,1.33,0.475,2.17,0.475
                                c0.621,0,1.17-0.104,1.648-0.308c0.481-0.202,0.896-0.485,1.244-0.848c0.35-0.362,0.621-0.793,0.819-1.292
                                c0.198-0.5,0.316-1.041,0.355-1.62h1.875c-0.183,1.777-0.795,3.159-1.835,4.146c-1.039,0.987-2.462,1.48-4.264,1.48
                                c-1.093,0-2.048-0.188-2.861-0.562c-0.817-0.376-1.495-0.892-2.034-1.55c-0.541-0.657-0.944-1.435-1.213-2.33
                                c-0.271-0.896-0.405-1.854-0.405-2.881c0-1.027,0.144-1.99,0.432-2.895c0.291-0.899,0.716-1.686,1.275-2.357
                                c0.559-0.671,1.257-1.2,2.092-1.588s1.793-0.582,2.871-0.582c0.737,0,1.435,0.098,2.094,0.295c0.657,0.198,1.243,0.488,1.757,0.869
                                c0.513,0.381,0.939,0.857,1.284,1.431c0.342,0.572,0.563,1.234,0.67,1.983h-1.874C161.769,32.771,161.305,32.02,160.585,31.52"></path>
                            <path d="M165.757,33.8c0.283-0.889,0.707-1.675,1.273-2.358c0.566-0.685,1.271-1.231,2.114-1.639
                                c0.842-0.407,1.82-0.612,2.939-0.612c1.118,0,2.1,0.205,2.94,0.612c0.844,0.408,1.546,0.955,2.112,1.639
                                c0.566,0.684,0.991,1.47,1.273,2.358c0.281,0.888,0.425,1.812,0.425,2.773c0,0.961-0.144,1.885-0.425,2.772
                                c-0.282,0.889-0.707,1.675-1.273,2.359s-1.269,1.226-2.112,1.627c-0.841,0.403-1.822,0.604-2.94,0.604
                                c-1.119,0-2.098-0.201-2.939-0.604c-0.844-0.401-1.548-0.942-2.114-1.627s-0.99-1.471-1.273-2.359
                                c-0.283-0.888-0.423-1.812-0.423-2.772C165.334,35.611,165.474,34.688,165.757,33.8 M167.485,38.674
                                c0.185,0.692,0.474,1.315,0.868,1.868c0.396,0.551,0.899,0.995,1.521,1.331c0.618,0.334,1.354,0.504,2.21,0.504
                                s1.593-0.17,2.212-0.504c0.617-0.336,1.123-0.78,1.519-1.331c0.394-0.553,0.685-1.176,0.869-1.868
                                c0.184-0.689,0.276-1.391,0.276-2.101s-0.093-1.413-0.276-2.103c-0.185-0.691-0.476-1.312-0.869-1.865
                                c-0.396-0.553-0.901-0.996-1.519-1.334c-0.619-0.333-1.355-0.501-2.212-0.501s-1.592,0.168-2.21,0.501
                                c-0.621,0.338-1.125,0.781-1.521,1.334c-0.395,0.553-0.684,1.174-0.868,1.865c-0.185,0.689-0.277,1.393-0.277,2.103
                                S167.301,37.984,167.485,38.674"></path>
                            <polygon points="183.532,29.526 187.974,41.37 192.435,29.526 195,29.526 195,43.62 193.223,43.62 193.223,31.895 
                                193.186,31.895 188.782,43.62 187.185,43.62 182.782,31.895 182.744,31.895 182.744,43.62 180.966,43.62 180.966,29.526     "></polygon>
                        </g>
                        </svg>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-2 text-center alinhar-centro">
                    <div class="ts-modal-trigger logos-falam" id="trustvox-selo-site-sincero">
                      <?= $this->Html->image('selo-trustvox.png', ['alt' => 'selo trustvox', 'style' => 'width: auto; max-height: 120px;']); ?>
                    </div>
                    <script type="text/javascript">
                        var _trustvox_certificate = _trustvox_certificate || [];
                        _trustvox_certificate.push(['_certificateId', 'logfitness']);
                        (function() {
                          var tv = document.createElement('script'); tv.type = 'text/javascript'; tv.async = true;
                          tv.src = '//s3-sa-east-1.amazonaws.com/trustvox-certificate-modal-js/widget.js';
                          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tv, s);
                        })();
                    </script> 
                </div>
                <div class="col-sm-6 col-lg-2 text-center alinhar-centro logos-falam">
                    <a target="_blank" href="http://revistasuplementacao.com.br/?page=interna&slug=logfitness_chega_ao_mercado_e_espera_faturar_r_1_5_milhao_em_2017">
                        <?= $this->Html->image('logo-suplementacao.png', ['alt' => 'revista suplementacao', 'style' => 'max-width: 165px; height: auto;']); ?>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-2 text-center alinhar-centro logos-falam">
                    <a target="_blank" href="https://www.ecommercebrasil.com.br/noticias/plataforma-e-commerce-suplementos-academias/">
                        <?= $this->Html->image('ecommerce_brasil.png', ['alt' => 'Ecommerce Brasil', 'style' => 'max-width: 165px; height: auto;']); ?>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-2 text-center alinhar-centro logos-falam">
                    <a target="_blank" href="http://boaforma.abril.com.br/fitness/nutricionista-indica-os-suplementos-ideais-para-treinos-aerobicos/">
                        <?= $this->Html->image('boa_forma.png', ['alt' => 'revista suplementacao', 'style' => 'max-width: 165px; height: auto;']); ?>
                    </a>
                </div>
            </div>
        </div>
    </section>


		<!-- start footer -->
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-10">
                        Copyright &copy; <span>www.logfitness.com.br</span>. TODOS OS DIREITOS RESERVADOS.
                Todo o conteúdo do site, todas as fotos,imagens, logotipos, marcas, dizeres, som, software, conjunto imagem,
                layout, trade dress, aqui veiculados são de propriedade exclusiva da LogFitness. É vedada qualquer reprodução,
                total ou parcial, de qualquer elemento de identidade, sem expressa autorização. A violação de qualquer direito mencionado
                implicará na responsabilização cível e criminal nos termos da Lei.

                CACIQUE DIGITAL REPRESENTACOES LTDA - ME - CNPJ: 22.921.802/0001-77 Rua Antonio Munia, 141, Sala 01 - Jardim Nazareth - CEP 15054-160 - São José do Rio Preto - SP
                    </div>
                    <div class="col-md-2">
                        <?= $this->Form->button('Contato',
                        ['class' => 'btn-conheca a-contato contato-home',
                            'escape' => false]
                    ) ?>
                    </div>
			</div>
		</footer>
		  <!--   end footer -->
		<script>
		<?= file_get_contents(JS_URL.'jquery.cadastrar.js') ?>
		<?= file_get_contents(JS_URL.'jquery.js') ?>
		<?= file_get_contents(JS_URL.'bootstrap.min.jsbootstrap.min.js') ?>
		<?= file_get_contents(JS_URL.'wow.min.js') ?>
		<?= file_get_contents(JS_URL.'jquery.singlePageNav.min.js') ?>
		<?= file_get_contents(JS_URL.'custom.js') ?>
		</script>

		<script>
			$(document).ready(function(){
				$("#btn_cadastrar").click(function(){
					$("#duvidas").slideUp();
					$("#cadastre-se").slideToggle();
				});
				$("#btn_duvidas").click(function(){
					$("#cadastre-se").slideUp();
					$("#duvidas").slideToggle();
				});
				
				var medida_tela = $(window).width();
				if(medida_tela < 767) {
					//se clicar em um link do navbar -> menu fecha
					$('.nav li a').click(function () {
						$('.menu-navbar').slideUp(750);
					});
					//se clicar no icon do navbar -> menu toggle(abre e fecha)
					$('.navbar-toggle').click(function () {
						$('.menu-navbar').toggle(750);
					});
				}
				$('.nav a[href^="#"]').on('click', function(e) {
					e.preventDefault();
					var id = $(this).attr('href'),
						targetOffset = $(id).offset().top;

					$('html, body').animate({
						scrollTop: targetOffset - 50
					}, 500);
				});
            });
		</script>

		<script>
			$(document).ready(function() {

				$("#owl-demo").owlCarousel({
					autoPlay : 10000,
					navigation : false, // Show next and prev buttons
					slideSpeed : 300,
					paginationSpeed : 400,
					singleItem:true,
					stopOnHover:true

					// "singleItem:true" is a shortcut for:
					// items : 1,
					// itemsDesktop : false,
					// itemsDesktopSmall : false,
					// itemsTablet: false,
					// itemsMobile : false

				});

			});
		</script>

		<!-- Smartsupp Live Chat script -->
    <script type="text/javascript">
    var _smartsupp = _smartsupp || {};
    _smartsupp.key = '472d82fddbe4326ca5661de701a829a7553ce341';
    window.smartsupp||(function(d) {
        var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
        s=d.getElementsByTagName('script')[0];c=d.createElement('script');
        c.type='text/javascript';c.charset='utf-8';c.async=true;
        c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
    })(document);
    </script>

    <!-- trustvox -->
	<script type="text/javascript">
	    var _trustvox_certificate = _trustvox_certificate || [];
	    _trustvox_certificate.push(['_certificateId', 'logfitness']);
	    (function() {
	      var tv = document.createElement('script'); tv.type = 'text/javascript'; tv.async = true;
	      tv.src = '//s3-sa-east-1.amazonaws.com/trustvox-certificate-modal-js/widget.js';
	      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tv, s);
	    })();
	</script>

	</body>
</html>