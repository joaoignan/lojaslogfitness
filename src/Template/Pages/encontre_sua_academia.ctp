<script>
	$(document).ready(function(){
		$('#academias').addClass('active-navbar');
	});
	function openBanner() {
        document.getElementById("banner").style.height = "100%";
        $("html,body").css({"overflow":"hidden"});
    }
    function closeBanner() {
        document.getElementById("banner").style.height = "0%";
        $("html,body").css({"overflow":"auto"});
    }
    $(document).ready(function() {
	    $('#states_academias').on('change', function(){
	        $('.loading').show(1);
	        var state_id = $(this).val();
	        $.get(WEBROOT_URL + '/cidades/' + state_id + '/1',
	            function(data){
	                $('#city').html(data);
	                $('.loading').hide(1);

	                return false;
	            });
	    });
    });
    $(window).on('load', function() {
        function setCookie(cname, cvalue, exdays) {
	        var d = new Date();
	        d.setTime(d.getTime() + (exdays*24*60*60*1000));
	        var expires = "expires="+d.toUTCString();
	        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
	        var name = cname + "=";
	        var ca = document.cookie.split(';');
	        for(var i = 0; i < ca.length; i++) {
	            var c = ca[i];
	            while (c.charAt(0) == ' ') {
	                c = c.substring(1);
	            }
	            if (c.indexOf(name) == 0) {
	                return c.substring(name.length, c.length);
	            }
	        }
	        return "";
        }

        var first_access_log = getCookie("first_access");

        if(first_access_log != 0) {
        } else {
            openBanner();
            setCookie("first_access", 1, 600);
        }

        <?php if($EstadoAcademia && $CidadeAcademia) { ?>
        	$('.loading').show(1);

	        $('#states_academias').val(<?= $EstadoAcademia ?>);
        	$.get(WEBROOT_URL + '/cidades/' + <?= $EstadoAcademia ?> + '/1',
	            function(data){
	                $('#city').html(data);
                	$('#city').val(<?= $CidadeAcademia ?>);
	                setTimeout(function () {
	                	$('#city').change();
	                	$('.loading').hide(1);
	                }, 300);
	                return false;
	            });
	    <?php } else { ?>
	        navigator.geolocation.getCurrentPosition(function(posicao) {
	            var latlng = posicao.coords.latitude + "," +posicao.coords.longitude; 
	            var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlng + "&sensor=true"; 
	            $.getJSON(url, function (data) { 
	                var endereco = data.results[2].formatted_address.split(", "); 

	                cidade = endereco[0];
	                estado = endereco[1];

	                var uf = '';

	                switch (estado){
	                    case 'AC' : uf = 1; break;
	                    case 'AL' : uf = 2; break;
	                    case 'AM' : uf = 3; break;
	                    case 'AP' : uf = 4; break;
	                    case 'BA' : uf = 5; break;
	                    case 'CE' : uf = 6; break;
	                    case 'DF' : uf = 7; break;
	                    case 'ES' : uf = 8; break;
	                    case 'GO' : uf = 9; break;
	                    case 'MA' : uf = 10; break;
	                    case 'MG' : uf = 11; break;
	                    case 'MS' : uf = 12; break;
	                    case 'MT' : uf = 13; break;
	                    case 'PA' : uf = 14; break;
	                    case 'PB' : uf = 15; break;
	                    case 'PE' : uf = 16; break;
	                    case 'PI' : uf = 17; break;
	                    case 'PR' : uf = 18; break;
	                    case 'RJ' : uf = 19; break;
	                    case 'RN' : uf = 20; break;
	                    case 'RO' : uf = 21; break;
	                    case 'RR' : uf = 22; break;
	                    case 'RS' : uf = 23; break;
	                    case 'SC' : uf = 24; break;
	                    case 'SE' : uf = 25; break;
	                    case 'SP' : uf = 26; break;
	                    case 'TO' : uf = 27; break;
	                    default : uf = '';
	                }

	                $('#states_academias option[value='+uf+']').attr('selected','selected');

	                $.get(WEBROOT_URL + '/cidades/' + uf + '/1',
	                function(data){
	                    $('#city').html(data);
	                    
	                    $('#city').children().each(function() {
	                        if($(this).text() == cidade) {
	                            cidade_val = $(this).val();

	                            $('#city option[value='+cidade_val+']').attr('selected','selected');

	                            $.get(WEBROOT_URL + 'academias/busca-cidade/' + uf + '/' + cidade_val,
	                            function(data){
	                                $('#select-academia').html(data);
	                            });

	                            $('#city').change();

	                            var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + cidade + estado  + "&sensor=true"; 
	                            $.getJSON(url, function (data) { 
	                                setTimeout(function() {
	                                    var location = new google.maps.LatLng(data.results[0].geometry.location.lat, data.results[0].geometry.location.lng);
	                                    map.setCenter(location);
	                                    map.setZoom(13);
	                                }, 1500);
	                            });

	                            return false;
	                        }
	                    });
	                });
	            });
	        });
	    <?php } ?>
    });
</script>

<div id="banner" class="overlay" onclick="closeBanner()">
    <div class="overlay-content">
    <a href="javascript:void(0)" class="closebtn" onclick="closeBanner()">&times;</a>
		<?= $this->Html->image('pass_desktop.jpg',['title' => 'Banner ','id' => 'img-banner-desk']) ?>
		<?= $this->Html->image('pass_mobile.jpg',['title' => 'Banner ','id' => 'img-banner-mobile']) ?>
    </div>
</div>
<div class="main-container">
	<div class="content-left">
		<div class="filtro">
			<div class="col-xs-12 col-md-6 inner-filtro">
				<div class="col-xs-6 filtro-select">
					<?= $this->Form->input('uf_id', [
                        'value'         => SLUG ? $academia_slug->city->state_id : '',
                        'id'            => 'states_academias',
                        'div'           => false,
                        'label'         => false,
                        'options'       => $states,
                        'empty'         => 'Estado...',
                        'class'         => 'validate[required] form-control input-cadastro input-cadastro-width'
                    ]) ?>
				</div>
				<div class="col-xs-6 filtro-select" id="cidade">
					<?= $this->Form->input('city_id', [
                        'value'         => SLUG ? $academia_slug->city_id : '',
                        'id'            => 'city',
                        'div'           => false,
                        'options'       => SLUG ? $academia_slug_cities : [],
                        'label'         => false,
                        'empty'         => 'Cidade...',
                        'class'         => 'validate[required] cidade-busca-encontre form-control input-cadastro input-cadastro-width cidade-academia'
                    ]) ?>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 inner-busca">
				<div class="col-xs-6 col-md-8 filtro-select">
					<?= $this->Form->input('name', [
                        'div'           => false,
                        'label'         => false,
                        'placeholder'   => 'Academia...',
                        'class'         => 'validate[required] form-control input-cadastro input-cadastro-width input-name-busca'
                    ]) ?>
				</div>
				<div class="col-xs-6 col-md-4 text-right div-btn">
					<?= $this->Form->button('BUSCAR', [
                        'div'           => false,
                        'type'			=> 'button',
                        'class'         => 'btn  btn-buscar-name'
                    ]) ?>
				</div>
			</div>
		</div> <!-- filtro -->

		<script type="text/javascript">

			$('.btn-buscar-name').on('click', function(){
		        var city_id = $('.cidade-busca-encontre').val();
		        var name = $('.input-name-busca').val();

		        if(name == "") {
		        	$('#city').change();
		        	return false;
		        }

	        	$('.loading').show(1);

		        $.ajax({
	                type: "POST",
	                url: WEBROOT_URL + 'academia/buscar-nome/',
	                data: {
	                    city_id: city_id,
	                    name: name
	                }
	            })
	                .done(function (data) {
	                    $('#exibicao-academias-encontre').html(data);
	        			$('.loading').hide(1);

                        return false;
	                });
		    });

			$('#city').on('change', function(){
		        var city_id = $('.cidade-busca-encontre').val();
		        var city_text = $('.cidade-busca-encontre option[value='+city_id+']').text();
		        var state_id = $('#states_academias').val();

		        $('.input-name-busca').val('');

	        	$('.loading').show(1);

		        $.ajax({
	                type: "POST",
	                url: WEBROOT_URL + 'academia/buscar/',
	                data: {
	                    city_id: city_id
	                }
	            })
	                .done(function (data) {
	                    $('#exibicao-academias-encontre').html(data);
	        			$('.loading').hide(1);

	        			var uf = '';

		                switch (state_id){
		                    case 1  : uf = 'AC';  break;
		                    case 2  : uf = 'AL';  break;
		                    case 3  : uf = 'AM';  break;
		                    case 4  : uf = 'AP';  break;
		                    case 5  : uf = 'BA';  break;
		                    case 6  : uf = 'CE';  break;
		                    case 7  : uf = 'DF';  break;
		                    case 8  : uf = 'ES';  break;
		                    case 9  : uf = 'GO';  break;
		                    case 10 : uf = 'MA';  break;
		                    case 11 : uf = 'MG';  break;
		                    case 12 : uf = 'MS';  break;
		                    case 13 : uf = 'MT';  break;
		                    case 14 : uf = 'PA';  break;
		                    case 15 : uf = 'PB';  break;
		                    case 16 : uf = 'PE';  break;
		                    case 17 : uf = 'PI';  break;
		                    case 18 : uf = 'PR';  break;
		                    case 19 : uf = 'RJ';  break;
		                    case 20 : uf = 'RN';  break;
		                    case 21 : uf = 'RO';  break;
		                    case 22 : uf = 'RR';  break;
		                    case 23 : uf = 'RS';  break;
		                    case 24 : uf = 'SC';  break;
		                    case 25 : uf = 'SE';  break;
		                    case 26 : uf = 'SP';  break;
		                    case 27 : uf = 'TO';  break;
		                    default : uf = '';
		                }

                        var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + city_text + uf + "Brasil&sensor=true"; 
                        $.getJSON(url, function (data) { 
                            setTimeout(function() {
                                var location = new google.maps.LatLng(data.results[0].geometry.location.lat, data.results[0].geometry.location.lng);
                                map.setCenter(location);
                                map.setZoom(13);
                            }, 1500);
                        });

                        return false;
	                });
		    });

			$(document).ready(function() {
				var mais_iniciado = 0;
			    $('#exibicao-academias-encontre').scroll(function () {
	                if($(this).scrollTop() >= ($('.academias_exibidas_mais').height() - 1400) && mais_iniciado == 0) {
	                	mais_iniciado = 1;
	                	$.get(WEBROOT_URL + '/academia/mais/',
			            function (data) {
			            	$('.academias_carregar_mais').append(data);
			                mais_iniciado = 0;
			            });
	                }
	            });
			});
		</script>

		<div class="academias-lista" id="exibicao-academias-encontre">
			<div class="col-xs-12 academias_exibidas_mais">
				<div class="row academias_carregar_mais">
					<?php foreach($academias as $academia) { ?>
						<div class="col-sm-12 col-md-6 total-thumbnail">
					    	<div class="thumbnail">
					    		<div class="details">
					    			<?php if (!$this->request->is('mobile')) { ?>
						    			<div id="myCarousel-<?= $academia->id ?>" class="carousel slide" data-ride="carousel">
									    <!-- Wrapper for slides -->
										    <div class="carousel-inner">
										    	<?php $contador = 0; ?>
										    	<?php foreach($galeria as $imagem) { ?>
										    		<?php if($imagem[0]->academia_id == $academia->id) { ?>
												   		<div class="item item-bg <?= $contador == 0 ? 'active' : '' ?>" style="background-image:url('img/<?= $imagem[1] == 'galeria/' ? $imagem[1].'lg-'.$imagem[0]->image : '' ?><?= $imagem[1] == 'banners/' ? $imagem[1].$imagem[0]->image : '' ?>')">
									      				</div>
											      		<?php $contador++; ?>
						                        	<?php } ?>
								                <?php } ?>
								                <?php if($contador == 0) { ?>
								                	<?php 
									                	$images_padrao = [
									                		'image-padrao1.jpg',
									                		'image-padrao2.jpg',
									                		'image-padrao3.jpg',
									                		'image-padrao4.jpg'
									                	];

									                	shuffle($images_padrao); 
								                	?>
								                	<div class="item item-bg active" style="background-image: url('img/academias/<?= $images_padrao[0]?>');">
								      				</div>
								                <?php } ?>
										    </div>
									    	<!-- Left and right controls -->
										    <a class="left carousel-control" href="#myCarousel-<?= $academia->id ?>" data-slide="prev">
										      <span class="glyphicon glyphicon-chevron-left"></span>
										      <span class="sr-only">Previous</span>
										    </a>
										    <a class="right carousel-control" href="#myCarousel-<?= $academia->id ?>" data-slide="next">
										      <span class="glyphicon glyphicon-chevron-right"></span>
										      <span class="sr-only">Next</span>
										    </a>
										</div>
										<?php } ?>

									<?php
										$tamMax = 80;
								        $contador = 0;
								        $estrelas = 0;
								        $total_estrelas = 0;

								        $estrelas_txt = "";

								        foreach ($avaliacoes as $avaliacao) {
								        	if($avaliacao->academia_id == $academia->id) {
									            $estrelas += $avaliacao->rating;
									            $contador++;
								        	}
								        }

								        if($contador >= 1) {
								        	$total_estrelas = $estrelas / $contador;
								        }

								        $estrelas_txt = "";
								        for($i = 1; $i <= $total_estrelas; $i++) {
								        	$estrelas_txt = $estrelas_txt."<i class='fa fa-star dourado'></i>";
								        }

								        for($i = 1; $i <= (5 - $total_estrelas); $i++) {
								        	$estrelas_txt = $estrelas_txt."<i class='fa fa-star'></i>";
								        } 


							        	$modalidades_txt = "";

							        	$contador = 0;

							        	foreach ($modalidades as $modalidade) {
								        	if($modalidade->academia_id == $academia->id) {
								        		if($contador == 0) {
								        			$modalidades_txt = $modalidade->esporte->name;
								        		} else {
								        			$modalidades_txt = $modalidades_txt.", ".$modalidade->esporte->name;
								        		}

								        		$contador++;
								        	}
								        } 

							        	if($contador >= 1) {
							        		$modalidades_txt = $modalidades_txt;
							        	} else {
							        		$modalidades_txt = "<p>&nbsp;</p>";
							        	}

							        	$academia_true = 0;
					        			$valor_novo    = 0.0;
							        	$valor         = 90000.0;

							        	foreach ($mensalidades as $mensalidade) {
							        		if($mensalidade->academia_id == $academia->id) {
							        			$valor      = 90000.0;
							        			$valor_novo = 0.0;
							        			$academia_true = 1;

									        	foreach ($planos as $plano) {
									        		if($plano->academia_mensalidades_id == $mensalidade->id) {
										        		$valor_novo = $plano->valor_total / $plano->time_months;

										        		if($valor_novo < $valor) {
										        			$valor = $valor_novo;
										        		}
									        		}
									        	}
							        		} else if ($academia_true == 0) {
							        			$valor_novo = 0.0;
							        		}
							        	}
							        ?>
									<a href=<?=WEBROOT_URL.$academia->slug ?>>
										<div class="description">
											<div class="description-top">
												<?php if($academia->image != null) { ?>
									    			<div class="logo">
									    				<div class="logo-inner">
									    					<?= $this->Html->image('academias/'.$academia->image,['title' => 'logo academia '.$academia->shortname, 'alt' => 'Logo']) ?>
									    				</div>
									    			</div>
									    		<?php } ?>
								    			<div class="text-name">
								    				<p><strong><?= $academia->shortname ?></strong></p>
													<p><?= $estrelas_txt ?></p>
								    			</div>
							    			</div>
							    			<div class="description-bot">
							    				<p><strong>Endereço:</strong> <?= $academia->address_acad ?>, <?= $academia->number_acad ?> - <?= $academia->area_acad ?> - <?= $cidades_list[$academia->city_id_acad]['cidade'] ?>, <?= $cidades_list[$academia->city_id_acad]['estado'] ?></p>
							    			</div>
							    			<div class="ver-perfil">
							    				<p>ver loja</p>
							    			</div>
						    			</div>
					    			</a>
					    		</div>
					    	</div><!-- thumbnail -->
					  	</div>
				  	<?php } ?>
			  	</div><!-- row -->
			</div>
			<div class="text-center log-footer">
				<ul class="list-inline" itemscope itemtype="http://schema.org/Organization">
					<li>
			            <a href="http://blog.logfitness.com.br/" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-rss fa-footer"></i></a>
			        </li>
			        <li>
			            <a itemprop="sameAs" href="https://open.spotify.com/user/logfitness" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-spotify fa-footer"></i></a>
			        </li>
			        <li>
			            <a itemprop="sameAs" href="https://www.facebook.com/logfitness.com.br" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-facebook fa-footer"></i></a>
			        </li>
			        <li>
			            <a itemprop="sameAs" href="https://www.instagram.com/logfitness.com.br/" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-instagram fa-footer"></i></a>
			        </li>
			        <li>
			            <a itemprop="sameAs" href="https://twitter.com/LOGFITNESS" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-twitter fa-footer"></i></a>
			        </li>
			    </ul>
	        	<p>Copyright ©<?= date("Y") ?> <span>www.logfitness.com.br</span>. TODOS OS DIREITOS RESERVADOS.</p>
	    	</div>
    	</div>  <!-- academia-lista -->
	</div> <!-- content-left -->
	<?php if (!$this->request->is('mobile')) { ?>
	<div class="mapa">
		<div id="map"></div>
	</div>
	<?php } ?>
</div>

<style type="text/css">
	.dourado {
		color: gold;
	}
</style>

<script src="https://maps.google.com/maps/api/js?key=AIzaSyCba7qYdWhCMKwndRe8DC6TgHYq14jcRy4"></script>

<script>
    $('#states-indica').on('change', function(){
        var state_id = $(this).val();
        $.get(WEBROOT_URL + '/cidades/' + state_id,
            function(data){
                $('#city-indica').html(data);
            });
    });

	var map;
    var elevator;
    var myOptions = {
        zoomControl: true,
        scaleControl: false,
        scrollwheel: true,
        disableDoubleClickZoom: false,
        zoom: 4,
        center: new google.maps.LatLng(-15.7217175, -48.0783242),
        mapTypeId: 'terrain'
    };
    map = new google.maps.Map($('#map')[0], myOptions);

    var mapa_location = 0;
	
	<?php if (!$this->request->is('mobile')) { ?>
		<?php foreach ($academias_map as $academia) { ?>

	        <?php
	        if(!empty($academia->latitude) && !empty($academia->longitude)){
	        ?>
		        var latlng = new google.maps.LatLng(<?= $academia->latitude; ?>, <?= $academia->longitude; ?>);
		        var marker = new google.maps.Marker({
		            position: latlng,
		            map: map,
		            icon: WEBROOT_URL + 'img/map-marker.png'
		        });

		        var currWindow = false; 

		        function attachSecretMessage(marker, secretMessage) {
		          var infowindow = new google.maps.InfoWindow({
		            content: secretMessage
		          });

		          marker.addListener('click', function() {
		          	if( currWindow ) {
			           currWindow.close();
			        }

			        currWindow = infowindow;

		            infowindow.open(marker.get('map'), marker);            
		          });
		        }

		        <?php
			        $contador = 0;
			        $estrelas = 0;
			        $total_estrelas = 0;

			        foreach ($avaliacoes as $avaliacao) {
			        	if($avaliacao->academia_id == $academia->id) {
				            $estrelas += $avaliacao->rating;
				            $contador++;
			        	}
			        }

			        if($contador >= 1) {
			        	$total_estrelas = $estrelas / $contador;
			        }
			    ?>

		        var estrelas = "";
		        <?php for($i = 1; $i <= $total_estrelas; $i++) { ?>
		        	estrelas = estrelas+"<i class='fa fa-star dourado'></i>";
		        <?php } ?>

		        <?php for($i = 1; $i <= (5 - $total_estrelas); $i++) { ?>
		        	estrelas = estrelas+"<i class='fa fa-star'></i>";
		        <?php } ?>

		        var modalidades = "";

		        <?php 
		        	$contador = 0;

		        	foreach ($modalidades as $modalidade) {
			        	if($modalidade->academia_id == $academia->id) {
			        		if($contador == 0) { ?>
			        			modalidades = "<p><strong>Modalidades:</strong> <?= $modalidade->esporte->name ?>";
			        		<?php } else { ?>
			        			modalidades = modalidades+", <?= $modalidade->esporte->name ?>";
			        		<?php }

			        		$contador++;
			        	}
			        } 
		        ?>

		        <?php if($contador >= 1) { ?>
		        	var modalidades = modalidades+"</p>";
		        <?php } else { ?>
		        	var modalidades = "";
		        <?php } ?>

		        <?php
		        	$academia_true = 0;
	    			$valor_novo    = 0.0;
		        	$valor         = 90000.0;

			        foreach ($mensalidades as $mensalidade) {
		        		if($mensalidade->academia_id == $academia->id) {
		        			$valor      = 90000.0;
		        			$valor_novo = 0.0;
		        			$academia_true = 1;

				        	foreach ($planos as $plano) {
				        		if($plano->academia_mensalidades_id == $mensalidade->id) {
					        		$valor_novo = $plano->valor_total / $plano->time_months;

					        		if($valor_novo < $valor) {
					        			$valor = $valor_novo;
					        		}
				        		}
				        	}
		        		} else if ($academia_true == 0) {
		        			$valor_novo = 0.0;
		        		}
		        	}
	        	?>
		        var valor_min = "<?= $valor_novo >= 1.0 && $valor != 90000.0 ? '<p><strong>A partir de:</strong> R$ '.number_format($valor, 2, ',', '.').'</p>' : '<p>&nbsp;</p>' ?>";
		        var slug = "<?=$academia->slug ?>";
		        var name = "<?= $academia->shortname ?>";
		        var endereco = "<?= $academia->address ?>, <?= $academia->number ?> - <?= $academia->area ?>";
		        <?php if($academia->image != null) { ?>
					var imagem = '<?= $this->Html->image('academias/'.$academia->image,["title" => "logo-academia", "alt" => "Logo"]) ?>';
		        	
		        	attachSecretMessage(marker, "<a href='perfil/"+slug+"'><div class='details-js'><div class='descripton'><div class='description-top'><div class='logo'><div class='logo-inner'>"+imagem+"</div></div><div class='text-name'><p><strong>"+name+"</strong></p><p>"+estrelas+"</p></div></div><div class='content-desc'><p><strong>Endereço:</strong> "+endereco+"</p>"+modalidades+valor_min+"</div></div></div></a>");
		        <?php } else { ?>
		        	attachSecretMessage(marker, "<a href='perfil/"+slug+"'><div class='details-js'><div class='descripton'><div class='description-top'><div class='logo'><div class='logo-inner'></div></div><div class='text-name'><p><strong>"+name+"</strong></p><p>"+estrelas+"</p></div></div><div class='content-desc'><p><strong>Endereço:</strong> "+endereco+"</p>"+modalidades+valor_min+"</div></div></div></a>");
		        <?php } ?>

	        <?php } else { ?>

	        	<?php
		        	$academias_address =
		                "'"
		                . $academia->address . ','
		                . $academia->number . ','
		                . $academia->area . ','
		                . str_replace("'", '´', $academia->city->name) . ','
		                . $academia->city->uf
		                . "'";
		        ?>

	        	$.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + <?= $academias_address; ?> +'&sensor=false',
	                null,
	                function (data) {
	                    //console.log(data);
	                    var p = [];
	                    if (data.results[0].geometry.location) {
	                        p = data.results[0].geometry.location;
	                    } else {
	                        p = data.results[0]['geometry']['location'];
	                    }

	                    var latlng = new google.maps.LatLng(p.lat, p.lng);
	                    var marker = new google.maps.Marker({
	                        position: latlng,
	                        map: map,
	                        title: "<?= $academias_info; ?>",
	                        icon: WEBROOT_URL + 'img/map-marker.png'
	                        //shadow:     WEBROOT_URL + 'img/map-marker.png'
	                    });

	                    google.maps.event.addListener(marker, 'click', function () {
	                        select_academia("<?= $academia->city->uf ?>", "<?= $academia->city->name ?>", <?= $academia->id; ?>);
	                    });

	                    $.ajax({
	                        type: "POST",
	                        url: WEBROOT_URL + "academia-cordinates-add/" + <?= $academia->id; ?> +'/' + p.lat + '/' + p.lng,
	                        data: {}
	                    }).done(function (data_academia) {
	                        //console.log(data_academia);
	                    });
	                });
	        <?php } ?>
	    <?php } ?>
    <?php } ?>
</script>
<!-- Smartsupp Live Chat script -->
<script type="text/javascript">
    var _smartsupp = _smartsupp || {};
    _smartsupp.key = '472d82fddbe4326ca5661de701a829a7553ce341';
    window.smartsupp||(function(d) {
        var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
        s=d.getElementsByTagName('script')[0];c=d.createElement('script');
        c.type='text/javascript';c.charset='utf-8';c.async=true;
        c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
    })(document);
    </script>

    <script>
    jQuery(document).ready(function() {
    $(window).scroll(function () {
        set = $(document).scrollTop()+"px";
        jQuery('#float-banner').animate(
            {top:set},
            {duration:1000, queue:false}
        );
    });
    $('#my-link').on('click', function() {
      $('#float-banner').hide();
    })
  });
  </script>