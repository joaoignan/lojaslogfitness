<style>
	html,body {
		height: 100%;
		background: no-repeat;
        background: #5087C7; /* For browsers that do not support gradients */
		background: -webkit-linear-gradient(top, #5087C7, #f4d637); /* For Safari 5.1 to 6.0 */
		background: -o-linear-gradient(bottom, #5087C7, #f4d637); /* For Opera 11.1 to 12.0 */
		background: -moz-linear-gradient(bottom, #5087C7, #f4d637); /* For Firefox 3.6 to 15 */
        background: linear-gradient(to bottom, #5087C7, #f4d637); /* Standard syntax */
        overflow-x: hidden;
    }
    .calculadora{
    	height: 500px;
    }
    .imagem-calc{
    	display: flex;
    }
    .lucro{
	    height: 70px;
	    width: 100%;
	    text-align: center;
	    font-weight: 700;
    }
    .calculadora{
	    margin-top: 40px;
    }
    .calculadora p{
    	font-size: 16px;
    	font-family: nexa_boldregular;
		color: white;
		margin: 0 0 2px
    }
    .form-single{
    	margin: 3px;
    }
    .calculadora input[type="number"] {
    	font-family: nexa_boldregular;
		padding: 8px;
		border: solid 5px #c9c9c9;
		box-shadow: inset 0 0 0 1px #707070;
		transition: box-shadow 0.3s, border 0.3s;
	}
	.calculadora input[type="number"].focus {
		border: solid 5px #969696;
	}
	.formulario_log{
	    width: 35%;
	    min-height: 459px;
	    border-radius: 10px;
	    padding: 20px;
	    background: rgba(238, 238, 238, 1.0);
	    overflow-x: auto;
	}
	.formulario_log p{
		font-family: nexa_boldregular;
	}
	.formulario_log h1, h2, h3, h4, h5, h6{
		font-family: nexa_boldregular;
		color: #5087C7;
	}
	.formulario_log label{
		font-family: nexa_boldregular
	}
	.formulario{
		margin: 5px 0;
	}
	.tem_prof{
		font-family: nexa_boldregular;
		color: white;
		margin: 0 5px;
	}
	.ganho_com_log{
		font-family: nexa_boldregular;
		color: green!important;
		font-size: 30px;
		padding-top: 25px
	}
	.topo{
		font-family: nexa_boldregular;
		color: white;
		margin-bottom: 25px;
	}
	.total-ganho{
		height: 75px;
	}
	.btn-calcular{
		width: 160px
	}
	.calc-total{
		float: left;
		width: 60%;
		height: 460px;
		padding: 0 5px;
		margin-bottom: 20px;
		background: url("<?= WEBROOT_URL ?>img/calculadora.png") no-repeat center center;
	}
	.valores{
       
	}
	@media all and (max-width: 1500px){
		.valores{
	        
		}
	}
	@media all and (max-width: 1024px){
		.calc-total{
			float: left;
			width: 60%;
			padding: 0 5px;
			margin-bottom: 20px;
			background: url("<?= WEBROOT_URL ?>img/calculadora-mobile.png") no-repeat center center;
		}
	}
	@media all and (max-width: 850px){
		.calc-total{
			float: left;
			width: 100%;
			padding: 0 5px;
			margin-bottom: 20px;
			background: url("<?= WEBROOT_URL ?>img/calculadora.png") no-repeat center center;
		}
		.formulario_log{
		    width: 90%;
		    margin-left: 5%;
		    border-radius: 10px;
		    padding: 20px 0;
		    background: rgba(238, 238, 238, 1.0);
		    overflow-x: auto;
		}
	}
	@media all and (max-width: 450px){
		.calc-total{
			background: url("<?= WEBROOT_URL ?>img/calculadora-mobile.png") no-repeat center center;
		}
    	.total-ganho{
			height: 70px;
		}
		.btn-calcular{
			width: 160px
		}
	}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$('.calculadora-btn').click(function() {
			var qtd_alunos = $('.qtd_alunos').val();
			var valor_mensalidade = $('.valor_mensalidade').val();
			var tem_professor = $('.tem_prof:checked').val();

			$('.erro-calc').remove();

			if(qtd_alunos >= 1 && valor_mensalidade >= 1 && tem_professor != undefined) {
				var gasto_medio = valor_mensalidade * .80;

				if(tem_professor == 'sim') {
					var qtd_alunos_compram = qtd_alunos * .65;
					var comissao = .10;
				} else {
					var qtd_alunos_compram = qtd_alunos * .25;
					var comissao = .20;
				}

				var ganho_com_log = (parseInt(qtd_alunos_compram) * gasto_medio) * comissao;

				$('.ganho_com_log').html('R$ '+parseInt(ganho_com_log)+',00');
				$('.calculado').slideDown();
			} else {
				$('.btn-div').append('<p class="erro-calc" style="color: red">Falta alguma coisa :)</p>');
			}
		});
	});
</script>

<?= $this->Flash->render() ?>

<section>	
	<div class="content">
		<div class="col-xs-12 topo text-center">
			<h1>Você não vai acreditar quando ver o quanto a sua academia pode ganhar <strong>POR MÊS</strong> com a LOG!</h1>
		</div>
		<div class="calc-total text-center">
			<div class="valores">
				<div class="col-xs-12 text-center total-ganho" style="margin-bottom: 30px; margin-top: 15px;">
					<p class="ganho_com_log"></p>
				</div>
				<div class="calculadora">
					<div class="col-xs-12 form-single">
						<p>Qtd de alunos</p>
						<input type="number" class="qtd_alunos">
					</div>
					<div class="col-xs-12 form-single">
						<p>Valor da mensalidade</p>
						<input type="number" class="valor_mensalidade">
					</div>
					<div class="col-xs-12 form-single">
						<p>Tem professor?</p>
						<span class="tem_prof"><input type="radio" name="prof" class="tem_prof" value="sim"> Sim</span>
						<span class="tem_prof"><input type="radio" name="prof" class="tem_prof" value="nao"> Não</span>
					</div>
					<div class="col-xs-12 text-center btn-div" style="margin: 5px 0">
						<button type="button" class="calculadora-btn btn btn-success btn-calcular">Calcular</button>
					</div>
				</div>
			</div>
		</div>
			<div class="formulario_log text-left">
				<div class="col-xs-12 text-center">
					<h3>Quer saber como?</h3>
					
					<br>
				</div>
				<div class="col-xs-12">
					<?= $this->Form->create(null, ['id' => 'calculadora-form'])?>
				    <div class="col-xs-12 formulario">   
				        <?= $this->Form->input('name', [
				            'div'           => false,
				            'label'         => 'Nome',
				            'class'         => 'form-control input-md validate[required]',
				            'placeholder'   => 'Nome completo'
				        ])?>
				    </div>
				    <div class="col-xs-12 formulario">
				        <?= $this->Form->input('email', [
				            'div'           => false,
				            'label'         => 'Email',
				            'class'         => 'form-control input-md validate[required, custom[email]]',
				            'placeholder'   => 'E-mail'
				        ])?>
				    </div>
				    <div class="col-xs-12 formulario">
				        <?= $this->Form->input('phone', [
				            'div'           => false,
				            'label'         => 'Telefone',
				            'class'         => 'form-control input-md phone-mask validate[required]',
				            'placeholder'   => '(99)99999-9999'
				        ])?>
				    </div>      
				    <div class="col-xs-12 formulario text-left">
				        <p>Eu sou</p>
				        <div class="col-xs-6 text-center">
				            <label for="type-2"><input type="radio" id="type-2" name="type" value="2"> Professor</label>
				        </div>
				        <div class="col-xs-6 text-center">
				            <label for="type-3"><input type="radio" id="type-3" name="type" value="3" checked="true"> Academia</label>
				        </div>
				    </div>
				    <div class="col-xs-12 text-center">
				        <?= $this->Form->button('Saber mais', [
				            'id'        => 'calculadora-btn',
				            'type'      => 'submit',
				            'class'     => 'btn btn-lg btn-success',
				            'escape'    => false,
				            'style'		=> 'margin-top: 20px;'
				        ]) ?>
				        <?= $this->Form->end()?>
				    </div>
				</div>		
			</div>
		<div class="col-xs-12 col-md-10 col-md-offset-1 infos">
			<h3 style="color: black">Informações</h3>
			<h5 style="text-align: left; color: black;">Nós calculamos o poder aquisitivo de um aluno com base na mensalidade paga, então estimamos que ele gaste 80% do valor da mensalidade em suplementos.<br>Se a academia tem professor cadastrado na LOG, a média de alunos que compram é de 65% da quantidade total de alunos da academia, pois os alunos tendem a confiar mais em seus professores que acompanham seu treino, já uma academia sem professor cadastrado na LOG, a média de alunos que compram é de 25%.<br>Calculamos então a comissão mensal de 10%(com professor) e 20%(sem professor), contando com 65% dos alunos(com professor) ou 25% dos alunos(sem professor) com um gasto médio de 80% da mensalidade.<br><br>* Valores com base em academias reais e felizes com LogFitness</h5>
		</div>
	</div>
</section>

<script type="text/javascript">
    $('#calculadora-btn').click(function(){
        var form = $('#calculadora-form');
        var valid = form.validationEngine("validate");

        if (valid == true) {
        	$('#calculadora-btn').html('Carregando...');
        	$('#calculadora-btn').attr('disabled', 'disabled');

            var form_data = form.serialize();
            $.ajax({
                type: "POST",
                url: WEBROOT_URL + "calculadora-envio",
                data: form_data,
            })
                .done(function (data) {
                    if(data >= 1) {
                    	location.href = WEBROOT_URL + 'academia';
                    } else {
                    	$('#calculadora-btn').html('Saber mais');
        				$('#calculadora-btn').attr('disabled', 'false');
                        $('#calculadora-btn').parent().append('Falhou, tente novamente! :)');
                    }
                });
        }else{
            form.validationEngine({
                updatePromptsPosition: true,
                promptPosition: 'inline',
                scroll: false
            });
        }
    });
</script>