<div id="parallaxBar" class="parallaxBar">
    <img src="<?= IMG_URL; ?>first-img.jpg" data-speed="0.25" class="parallax-img">
    <div class="container">
        <div class="col-md-12">
            <div class="col-md-12 margin-top-10">
                <div class="col-md-3">
                    <a href="/">
                        <img src="<?= IMG_URL; ?>logfitness.png" alt="LogFitness" class="logo-topo">
                    </a>
                </div>
                <div class="col-md-3"></div>
                <?= $this->Form->create(null, ['id' => 'form-login'])?>
                <div class="col-md-6">

                    <div class="col-lg-3 padding-0">
                        <?= $this->Form->input('email', [
                            'div'           => false,
                            'label'         => false,
                            'class'         => 'input-login validate[required, custom[email]]',
                            'placeholder'   => 'E-MAIL',
                        ])?>
                        <h6 id="info-cadastro-2" class="col-md-12 white forgot-new font-bold"></h6>
                    </div>
                    <div class="col-lg-3 padding-0">
                        <?= $this->Form->input('password', [
                            'id'            => 'login-password',
                            'div'           => false,
                            'label'         => false,
                            'class'         => 'input-login validate[required]',
                            'placeholder'   => 'SENHA',
                        ])?>
                        <h6 class="col-md-12 white forgot-new">
                            <?= $this->Html->link('Esqueci a senha',
                                $SSlug.'/login/esqueci-a-senha',
                                ['class' => 'white']
                            )?>
                        </h6>
                    </div>

                    <div class="col-lg-6 padding-3">
                        <?= $this->Form->button('Entrar', [
                            'type'      => 'button',
                            'id'        => 'submit-login',
                            'class'     => 'btn btn-default btn-entrar',
                            'escape'    => false,
                            'style'     => 'margin-top: 5px; width: 39%;'
                        ]) ?>
                        <span class="text-center" style="font-size: 20px; font-weight: bold; color: white;">ou</span>
                        <?= $this->Html->link('Com o <i class="fa fa-facebook-square"></i>',
                            '/fb-login/',
                            ['class' => 'btn btn-primary btn-entrar-fb', 'escape' => false, 'style' => 'margin-top: 5px; width: 39%;']) ?>
                    </div>


                </div>
                <?= $this->Form->end()?>
            </div>

            <div class="col-md-12 col-xs-12 margin-top-190">
                <div class="col-md-7"></div>
                <div class="col-md-5 text-center">
                    <p>Com a <span>Logfitness</span> você encontra os melhores produtos para se superar e ter <span>uma vida mais saudável</span>.</p>
                    <?= $this->Form->button('Conheça',
                        [
                            'class' => 'btn-conheca margin-btn-conheca btn-conheca conheca-home margin-top-100',
                            'escape' => false
                        ]
                    ) ?>
                    <div class="btn-roll-down">
                        <?= $this->Html->image('arrow-btn.png', array('id' => 'btn-arrow'))?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container" id="bloco-2" >
    <div class="col-md-12">
        <div class="col-md-1"></div>
        <div class="col-md-6 second-img">
            <!--<img src="<?= IMG_URL ?>second-img.jpg" alt="" width="100%" height="400px">-->
        </div>
        <div class="col-md-5">
            <p class="p-second-img">A LogFitness é a primeira plataforma no mundo a aliar a comodidade, variedade e preço do e-commerce com a segurança e atendimento personalizado de uma loja física. Você agora recebe seus suplementos e produtos naturais direto no seu treino, sem frete algum!</p>
        </div>
    </div>
</div>

<div id="parallaxBar2" class="parallaxBar">
    <img src="<?= IMG_URL; ?>third-img.jpg" data-speed="1" class="parallax-img">
    <div class="container">
        <div class="col-md-12 third-img">
            <p class="text-center">Sucesso = 1% inspiração e 99% transpiração!</p>
        </div>
    </div>
</div>

<div class="container">
    <div class="col-md-12">
        <div class="col-md-5">
            <p class="p-fourth-img">Começamos em 2015 oferecendo as pessoas que praticam atividades físicas uma nova
                forma de encontrar produtos naturais e suplementos para seus treinos. Muito suor e em 1 ano já estamos
                em todos os estados do Brasil, com mais de 1100 parceiros.
            </p>
        </div>
        <div class="col-md-6 fourth-img">
            <img src="<?= IMG_URL ?>fourth-img.jpg" alt="" width="100%" height="400px">
        </div>
        <div class="col-md-1"></div>
    </div>
</div>

<div style="width: 100%">
    <div id="map_canvas"></div>
</div>

<div class="container" id="bloco-conheca">
    <div class="col-md-12">
        <div class="col-md-1"></div>
        <div class="col-md-6 fifth-img">
            <img src="<?= IMG_URL ?>fifth-img.jpg" alt="" width="100%" height="500px">
        </div>
        <div class="col-md-5">
            <p class="text-center p-fifth-img">
                Aluno, primeira vez no site? <br>
                Preencha esse cadastro. É grátis!
            </p>
            <p id="info-cadastro" class="text-center"></p>
            <?= $this->Form->create(null, ['id' => 'form-cadastro', 'default' => false, 'class' => 'form-cadastro-home'])?>
            <div class="form-group row">
                <div class="col-lg-12">
                    <?= $this->Form->input('name', [
                        'div'           => false,
                        'label'         => false,
                        'class'         => 'input-cadastro form-control validate[required] input-cadastro-width',
                        'placeholder'   => 'NOME COMPLETO',
                    ])?>
                </div>
                <div class="col-lg-12">
                    <?= $this->Form->input('email', [
                        'div'           => false,
                        'label'         => false,
                        'class'         => 'input-cadastro form-control validate[required, custom[email]] input-cadastro-width ck-c-email',
                        'placeholder'   => 'E-MAIL',
                    ])?>
                </div>
                <div class="col-lg-12">
                    <?= $this->Form->input('password', [
                        'div'           => false,
                        'label'         => false,
                        'class'         => 'input-cadastro form-control validate[required] input-cadastro-width',
                        'placeholder'   => 'SENHA',
                    ])?>
                </div>
                <div class="col-lg-12">
                    <?= $this->Form->input('telephone', [
                        'div'           => false,
                        'label'         => false,
                        'class'         => 'input-cadastro form-control validate[required] phone-mask input-cadastro-width',
                        'placeholder'   => 'TELEFONE',
                    ])?>
                </div>
                <div class="col-lg-12">
                    <?= $this->Form->input('uf_id', [
                        //'value'         => isset($cliente->city->state->id) ? $cliente->city->state->id : '',
                        'value'         => SLUG ? $academia_slug->city->state_id : '',
                        'id'            => 'states_academias',
                        'div'           => false,
                        'label'         => false,
                        'options'       => $states,
                        'empty'         => 'Estado...',
                        'class'         => 'validate[required] form-control input-cadastro input-cadastro-width'
                    ]) ?>
                </div>
                <div class="col-lg-12">
                    <?= $this->Form->input('city_id', [
                        //'value'         => isset($cliente->city->id) ? $cliente->city->id : '',
                        'value'         => SLUG ? $academia_slug->city_id : '',
                        'id'            => 'city',
                        'div'           => false,
                        'options'       => SLUG ? $academia_slug_cities : [],
                        'label'         => false,
                        'empty'         => 'Selecione uma cidade',
                        'class'         => 'validate[required] form-control input-cadastro input-cadastro-width cidade-academia'
                    ]) ?>
                </div>
                <div class="col-lg-12">
                    <?= $this->Form->input('academia_id', [
                        'id'            => 'select-academia',
                        'options'       => [],
                        //'value'         => SLUG ? $academia_slug->id : '',
                        'div'           => false,
                        'empty'         => 'Selecione uma academia...',
                        'label'         => false,
                        'class'         => 'form-control input-cadastro validate[optional] input-cadastro-width'
                    ])?>
                </div>
                <div class="col-lg-12 col-xs-12 text-center">
                    <?= $this->Form->button('INSCREVER-SE', [
                        'id'            => 'btn-submit-cadastro',
                        'type'          => 'button',
                        'class'         => 'btn font-bold btn-inscrever'
                    ])?>
                </div>
                <div class="col-lg-12 col-xs-12">
                    <p class="text-center p-input-ou">ou</p>
                </div>
                <div class="col-lg-12 col-xs-12 text-center">
                    <?= $this->Html->link('Acessar com <i class="fa fa-facebook-square"></i>',
                        '/fb-login/',
                        ['class' => 'btn btn-primary font-bold', 'escape' => false]) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end()?>
    </div>
</div>
</div>

<div id="parallaxBar3" class="parallaxBar">
    <img src="<?= IMG_URL; ?>sixth-img.jpg" data-speed="1" class="parallax-img">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <p class="text-center">Sua academia não tem Logfitness?</p>
        </div>
    </div>
</div>

<div class="container" id="indique">
    <div class="col-md-12 col-xs-12">
        <div class="col-md-6 col-xs-12">
            <p class="text-center p-indique-academia">Indique sua academia!</p>
            <?= $this->Form->create(null, [
                'id'        => 'form-indicar-academia',
                'role'      => 'form',
                'default'   => false,
                'type'      => 'file'
            ]) ?>
            <div class="form-group row">
                <div class="div-indique">
                    <div class="col-lg-12">
                        <?= $this->Form->input('name', [
                            'label'         => false,
                            'div'           => false,
                            'placeholder'   => 'Nome da Academia',
                            'class'         => 'validate[required] form-control margin-bottom-10'
                        ]) ?>
                    </div>
                    <div class="col-lg-6">
                        <?= $this->Form->input('telephone', [
                            'label'         => false,
                            'div'           => false,
                            'placeholder'   => 'Telefone',
                            'class'         => 'validate[required] form-control phone-mask margin-bottom-10'
                        ]) ?>
                    </div>
                    <div class="col-lg-6">
                        <?= $this->Form->input('responsavel', [
                            'label'         => false,
                            'div'           => false,
                            'placeholder'   => 'Responsável pela Academia',
                            'class'         => 'validate[required] form-control margin-bottom-10'
                        ]) ?>
                    </div>
                    <div class="col-lg-5">
                        <?= $this->Form->input('uf_id', [
                            'label'         => false,
                            'id'            => 'states-indica',
                            'div'           => false,
                            'options'       => $states,
                            'empty'         => 'Selecione um estado',
                            'placeholder'   => 'Estado',
                            'class'         => 'validate[required] form-control margin-bottom-30'
                        ]) ?>
                    </div>
                    <div class="col-lg-7">
                        <?= $this->Form->input('city_id', [
                            'label'         => false,
                            'id'            => 'city-indica',
                            'div'           => false,
                            'empty'         => 'Selecione uma cidade',
                            'placeholder'   => 'Cidade',
                            'class'         => 'validate[required] form-control margin-bottom-30'
                        ]) ?>
                    </div>
                    <div class="col-lg-12">
                        <?= $this->Form->button('Enviar Indicação de Academia', [
                            'id'            => 'submit-indicar-academia',
                            'type'          => 'button',
                            'div'           => false,
                            'class'         => 'btn btn-default float-right btn-width',
                        ]) ?>
                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
            <p id="message-academia-indica"></p>
        </div>
        <div class="col-md-6 col-xs-12 seventh-img">
            <img src="<?= IMG_URL ?>seventh-img.jpg" alt="" width="100%" height="500px">
        </div>
    </div>
</div>

<div id="parallaxBar4" class="parallaxBar">
    <img src="<?= IMG_URL; ?>eighth-img.jpg" data-speed="1" class="parallax-img">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <div class="col-md-7 margin-top-100"></div>
            <div class="col-md-5 margin-top-100 text-center">
                <p>Tenho um centro de atividades físicas e<br> quero ter Logfitness</p>
                <?= $this->Html->link('Saiba <span style="font-weight: bold;">mais</span>',
                    '/academia/',
                    ['class' => 'btn-conheca margin-left-50 margin-top-70', 'escape' => false]) ?>
            </div>
        </div>
    </div>
</div>

<div class="container info-central-box form-toggle" id="form-toggle-contato">

    <!--<div class="col-lg-4 form-toggle-info">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam beatae culpa cum dolorum eos error incidunt inventore, natus odio, optio porro praesentium quae quis velit. Iusto magnam sint velit.
        <br>
        <br>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam beatae culpa cum dolorum eos error incidunt inventore, natus odio, optio porro praesentium quae quis velit. Iusto magnam sint velit.
    </div>-->

    <?= $this->Form->create(null, [
        'id'        => 'form-central-relacionamento',
        'role'      => 'form',
        'default'   => false,
        'type'      => 'file'
    ]) ?>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('name', [
                'id'            => 'contato-name',
                'div'           => false,
                'label'         => 'Nome',
                'placeholder'   => 'Nome Completo',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('phone', [
                'id'            => 'contato-phone',
                'div'           => false,
                'label'         => 'Telefone',
                'placeholder'   => 'Telefone',
                'class'         => 'validate[required] form-control phone-mask',
            ]) ?>
        </div>

        <div class="col-lg-5">
            <?= $this->Form->input('email', [
                'id'            => 'contato-email',
                'div'           => false,
                'label'         => 'E-mail',
                'placeholder'   => 'E-mail',
                'class'         => 'validate[required,custom[email]] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('description', [
                'id'            => 'contato-description',
                'div'           => false,
                'label'         => 'Assunto',
                'placeholder'   => 'Assunto / Descrição',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('message', [
                'id'            => 'contato-name',
                'type'          => 'textarea',
                'div'           => false,
                'label'         => 'Mensagem',
                'placeholder'   => 'Mensagem',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">

        <div class="col-lg-2">
            <?= $this->Form->button('Enviar Contato', [
                'id'            => 'submit-cadastrar-central-relacionamento',
                'type'          => 'button',
                'div'           => false,
                'class'         => 'btn btn-default float-right',
            ]) ?>

        </div>

        <div class="col-lg-5" id="form-toggle-message"></div>
    </div>

    <?= $this->Form->end() ?>
</div>

<div class="div-footer">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <div class="col-md-9 col-xs-12 text-center div-footer-div">
                 Copyright ©<?= date("Y") ?> <span>www.logfitness.com.br</span>. TODOS OS DIREITOS RESERVADOS.
                Todo o conteúdo do site, todas as fotos, imagens, logotipos, marcas, dizeres, som, software, conjunto imagem,
                layout, trade dress, aqui veiculados são de propriedade exclusiva da LogFitness. É vedada qualquer reprodução,
                total ou parcial, de qualquer elemento de identidade, sem expressa autorização. A violação de qualquer direito mencionado
                implicará na responsabilização cível e criminal nos termos da Lei.

                CACIQUE DIGITAL REPRESENTACOES LTDA - ME - CNPJ: 22.921.802/0001-77 Rua Antonio Munia, 141, Sala 01 - Jardim Nazareth - CEP 15054-160 - São José do Rio Preto - SP   -  
                <a href="#politica-de-privacidade" class="fancybox" style="color: #555">Política de Privacidade</a>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="col-md-12 text-center">
                    <?= $this->Form->button('Contato',
                        ['class' => 'btn-conheca a-contato contato-home',
                            'escape' => false]
                    ) ?>
                </div>
                <div class="col-md-12 text-right">
                    <a href="www.m4web.com.br"><img src="<?= IMG_URL; ?>desenvolvido-agencia-m4web-preto-colorido.png" alt="M4Web" class="logo-m4"></a>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->Element('politica_de_privacidade', ['hide' => true]) ?>