<script type="text/javascript">
var _smartsupp = _smartsupp || {};
_smartsupp.key = '472d82fddbe4326ca5661de701a829a7553ce341';
window.smartsupp||(function(d) {
    var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
    s=d.getElementsByTagName('script')[0];c=d.createElement('script');
    c.type='text/javascript';c.charset='utf-8';c.async=true;
    c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
})(document);
</script>

<style>
	#logexpress-btn{
		display: none!important;
	}
	.box-correcao{
		padding-top: 110px;
	}
	.box-correcao h2{
		color: #5089cf;
	}
	.box-correcao img{
		max-height: 230px;
	}
	.footer-total{
		display: none!important;
	}
	.overlay-total-cpf {
		display: none!important;
	}
</style>

<div class="container box-correcao text-center">
	<h2>Vamos solucionar o seu problema!</h2>
	<br>
	<?= $this->Html->image('new-loading.gif');  ?>
	<br>
	<p><strong>Entre em contato com a gente através do nosso chat no canto inferior da tela para que possamos resolver o seu problema.</strong></p>
	<p><strong>Explique em poucas palavras o que está acontecendo e um de nossos monstrinhos irá te ajudar =)</strong></p>
</div>