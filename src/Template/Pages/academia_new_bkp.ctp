<!DOCTYPE html>
<html lang="pt_br">
	<head>

		<title>LOGFITNESS</title>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="">
		<meta name="description" content="">

    	<?= $this->Html->css('bootstrap.min.css')?>
    	<?= $this->Html->css('academia_new_animate.min.css')?>
    	<?= $this->Html->css('academia_new_bootstrap.min.css')?>
    	<?= $this->Html->css('font-awesome.min.css')?>
    	<?= $this->Html->css('academia_new_templatemo-style.css')?>
    	<?= $this->Html->css('media_queries.min.css')?>
    	<?= $this->Html->css('owl.carousel.css')?>
    	<?= $this->Html->css('owl.theme.css')?>

    	<script>
        <?= file_get_contents(JS_URL.'vendor/jquery-1.8.2.min.js') ?>
        <?= file_get_contents(JS_URL.'vendor/modernizr-2.8.3-respond-1.4.2.min.js') ?>
        <?= file_get_contents(JS_URL.'owl-carousel/owl.carousel.js') ?>
        </script>

	</head>
	<body>
		<div id="submenu"></div>
		<!-- start navigation -->
		<nav class="navbar navbar-default navbar-fixed-top templatemo-nav" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<a href="#home" class="navbar-brand"><img src="../../../webroot/images/logfitness.png" width="170px" /></a>
				</div>
				<div >
					<ul class="nav navbar-nav navbar-right text-uppercase">
						<li><a href="#home" class="menu-home">Home</a></li>
						<li><a href="#divider" class="menu-vantagens">Vantagens</a></li>
						<li><a href="#funciona" class="menu-funciona">Como Funciona</a></li>
						<li><a href="#depoimentos" class="menu-depoimentos">Depoimentos</a></li>
						<li><a href="http://www.logfitness.com.br/academias/acesso">Login</a></li>
						<li><a href="#cadastre-se" id="btn_cadastrar_menu" class="menu-cadastrar">Cadastre-se</a></li>
					</ul>
				</div>
			</div>
		</nav>

		<!-- end navigation -->
		<!-- start home -->
		<section id="home">
			<div class="overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<img src="../../../webroot/images/texto_topo.png" id="txt_topo" alt="home img">


						</div>
					</div>
				</div>
			</div>
			<img src="../../../webroot/images/pcs_site.png" id="computadores" alt="home img"/>
		</section>
		<!-- end home -->

		<!-- start sobre -->
		<section id="sobre">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
					<br/><p>Conheça a <strong>plataforma online</strong><br/> que está mudando a maneira de<br/> se manter uma vida <strong>mais saudável.</strong></p>
					</div>

					<div class="col-md-12">
                    <p><br/>A <strong>LOGFITNESS</strong> aliou o <strong>preço, variedade e comodidade</strong> do <span style="white-space: nowrap"> e-commerce</span><br/> com o a <strong>segurança</strong> e <strong>atendimento personalizado</strong> de uma loja física.<br/> Proporcionando uma experiência completamente <strong>NOVA</strong>.</p>
                    </div>
				</div>
			</div>
		</section>
		<!-- end sobre -->

		<!-- Inicio Vantagens -->
		<section id="divider">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_vantagens" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_vantagens">Fazer parte do nosso time é<br/>
							vantajoso para sua academia<br/>
							e para seus alunos.</p>
						</div>
					</div>
					<div class="col-md-12">
                		<div class="fundo_vantagens1"><p class="eftspacing"><strong>academia & aluno</strong><br/>
                		COM A PRATICIDADE NA HORA DA ENTREGA E<br/>
						ATENDIMENTO PERSONALIZADO A RELAÇÃO ENTRE<br/>
						A ACADEMIA E ALUNO TEM UM NOVO PATAMAR.</p></div>
            			<div class="fundo_vantagens2"><p class="eftspacing"><strong>mais completa</strong><br/>
            			A PLATAFORMA PERMITE QUE SUA ACADEMIA SEJA<br/>
						AINDA MAIS COMPLETA OFERECENDO UM NOVO<br/>
						JEITO DE SE MANTER UMA VIDA MAIS SAUDÁVEL.</p></div>
					</div>
					<div class="col-md-12">
            			<div class="fundo_vantagens3"><p class="eftspacing"><strong>fonte de recursos</strong><br/>
            			QUANTO MAIS ALUNOS COMPRAREM, MAIOR SERÃO<br/>
						SEUS GANHOS. AUMENTE OS SEUS LUCROS!</p></div>
            			<div class="fundo_vantagens4"><p class="eftspacing"><strong>e muito mais</strong><br/>
            			AS VANTAGENS DE SER LOG NÃO<br/>
						PARAM POR AQUI. EXPLORE NOSSO MUNDO<br/>
						E DESCUBRA O PORQUE FAZER PARTE DESTE TIME.</p></div>
            		</div>
				</div>
			</div>
		</section>
		<!-- end vantagens -->

		<!-- start funciona -->
		<section id="funciona">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_como_funciona" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_como_funciona">A LOG permite que sua academia tenha<br/>
							uma loja completa e personalizada sem<br/>
							você se preocupar com controle de estoque,<br/>
							gerenciamento financeiro ou emissão de notas</p>
						</div>
					</div>
					<div class="col-md-3 wow fadeInUp templatemo-box" data-wow-delay="0.3s">
                    		<span><img id="img_funciona" src="../../../webroot/images/icone1.png" /></span>
                    		<p align="center" id="txt_funciona"><br/>A ACADEMIA FAZ O SEU CADASTRO NA PLATAFORMA.</p>
					</div>
					<div class="col-md-3 wow fadeInUp templatemo-box" data-wow-delay="0.3s">
                    		<span><img id="img_funciona" src="../../../webroot/images/icone2.png" /></span>
                    		<p align="center" id="txt_funciona"><br/>APÓS A APROVAÇÃO, A ACADEMIA RECEBE SEU LOGIN E SENHA DA SUA LOJA VIRTUAL.</p>
					</div>
					<div class="col-md-3 wow fadeInUp templatemo-box" data-wow-delay="0.3s">
                    		<span><img id="img_funciona" src="../../../webroot/images/icone3.png" /></span>
                    		<p align="center" id="txt_funciona"><br/>DE MANEIRA SIMPLES VOCÊ PERSONALIZA SUA PÁGINA.</p>
					</div>
					<div class="col-md-3 wow fadeInUp templatemo-box" data-wow-delay="0.3s">
                    		<span><img id="img_funciona" src="../../../webroot/images/icone4.png" /></span>
                    		<p align="center" id="txt_funciona"><br/>COM A LOJA PRONTA É SÓ DIVULGAR A NOVIDADE PARA OS ALUNOS. ELES VÃO ADORAR!</p>
					</div>
				</div>
			</div>
		</section>
		<!-- end funciona -->

		<!-- start video institucional -->
		<section id="video">
			<div class="container">
				<div class="row">
					<div class="video-container">
						<iframe width="100%" height="315" src="https://www.youtube.com/embed/Nr7UNxpzO18" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</section>
		<!-- end video institucional -->

		<!-- start professor -->
		<section id="professor">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_professor_comissionado" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_professor_comissionado">Quando a venda é feita através do perfil da<br/>
							academia e o aluno seleciona um professor a<br/>
							comissão é dividida ao meio.<br/>
							Metade academia, metade professor.</p>
						</div>
					</div>
					<div class="col-md-6 ">
						<br/>
						<br/>
						<p id="txt_professor">O professor é quem conhece melhor o treino e as necessidades do aluno. Com isso ele torna-se um multiplicador do seu perfil LOG dentro da sua academia..</p><br/>
						<p id="txt_professor"><strong>TODOS</strong> ganham. Academia aumentando as vendas dentro do seu perfil dentro da LOG, o professor atendendo melhor seu aluno e o aluno tendo os melhores resultados assistido por quem ele mais confia!</p>
					</div>
					<div class="col-md-6"  id="img_professor">
						<img src="../../../webroot/images/professor.jpg" class="img-responsive" alt="feature img">
					</div>
				</div>
			</div>
		</section>
		<!-- end professor -->

		<!-- start depoimentos-->
		<section id="depoimentos">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_depoimentos" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_depoimentos">Separamos o depoimento de algumas<br/>
							pessoas que já fazem parte do nosso time.<br/>
							<strong>CONFIRA!</strong></p>
						</div>
						<div id="bg-asset"></div>
						<div id="owl-demo" class="owl-carousel owl-theme">
							<div class="item"><img src="../../../webroot/images/depoimento1.jpg" alt="Depoimento 1"></div>
							<div class="item"><img src="../../../webroot/images/depoimento4.jpg" alt="Depoimento 4"></div>
							<div class="item"><img src="../../../webroot/images/depoimento2.jpg" alt="Depoimento 2"></div>
							<div class="item"><img src="../../../webroot/images/depoimento3.jpg" alt="Depoimento 3"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end depoimentos -->

		<!-- start tenhalog-->
		<section id="tenhalog">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 col-xs-12" id="titulo_tenha_log" >

						</div>
						<div class="col-md-6 col-xs-12">
							<p class="txt_titulos_tenha_log">Preencha as informações abaixo e<br/>
							envie o formulário de solicitação ou<br/>
							tire suas dúvidas com nosso contato.</p>
						</div>
					</div>
					<div class="col-md-4 col-md-offset-2">
						<input type="button" class="botoes" id="btn_cadastrar" value="Cadastrar">
					</div>
					<div class="col-md-4">
						<input type="button" class="botoes" id="btn_duvidas" value="Tirar dúvidas">
					</div>
				</div>
			</div>
		</section>
		<!-- end tenhalog -->

		<!-- start cadastre-se-->
		<section id="cadastre-se">
			<div class="overlay">
				<div class="container" id="cadastro-academia">
					<div class="row-md-12">
						<div class="col-md-12">
							<?= $this->Form->create($academia, [
								'id'        => 'form-cadastrar-academia',
								'role'      => 'form',
								'default'   => false,
								//'type'      => 'file'
							]) ?>
							<div class="form-group row">
								<div class="col-md-12 col-xs-12">
									<?= $this->Form->input('name', [
										'id'            => 'academia_name',
										'div'           => false,
										'label'         => 'Razão Social*',
										'placeholder'   => 'Razão Social',
										'class'         => 'validate[required] form-control',
									]) ?>
									<br/>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-5 col-xs-12">
									<?= $this->Form->input('shortname', [
										'div'           => false,
										'maxlength'     => 22,
										'label'         => 'Nome Curto*',
										'placeholder'   => 'Nome Fantasia / Nome de Fachada / Marca Empresarial',
										'class'         => 'validate[required,maxSize[22]] form-control',
									]) ?>
									<br/>
								</div>

                                <div class="col-lg-3 col-xs-5">
                                    <?= $this->Form->input('slug', [
                                        'value'         => 'logfitness.com.br/',
                                        'div'           => false,
                                        'disabled'      => true,
                                        'readonly'      => true,
                                        'label'         => 'URL Personalizada*',
                                        'placeholder'   => 'nome-personalizado-da-sua-academia',
                                        'class'         => 'slug-academia-texto form-control text-right',
                                    ]) ?>
                                </div>

								<div class="col-md-4 col-xs-12">
									<?= $this->Form->input('slug', [
										'div'           => false,
										'label'         => 'URL Personalizada*',
										'placeholder'   => 'nome-personalizado-da-sua-academia',
										'class'         => 'validate[required,custom[slug]]] form-control',
									]) ?>
									<br/>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6 col-xs-12">
									<?= $this->Form->input('cnpj', [
										'div'           => false,
										'label'         => 'CNPJ*',
										'placeholder'   => 'CNPJ',
										'class'         => 'validate[required,custom[cnpj]] form-control cnpj',
									]) ?>
									<br/>
								</div>
								<div class="col-md-6 col-xs-12">
									<?= $this->Form->input('ie', [
										'div'           => false,
										'label'         => 'Inscrição Estadual',
										'placeholder'   => 'Inscrição Estadual',
										'class'         => 'validate[optional] form-control',
									]) ?>
									<br/>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-12 col-xs-12">
									<?= $this->Form->input('contact', [
										'div'           => false,
										'label'         => 'Responsável*',
										'placeholder'   => 'Nome do Responsável',
										'class'         => 'validate[required] form-control',
									]) ?>
									<br/>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6 col-xs-6">
									<?= $this->Form->input('phone', [
										'div'           => false,
										'label'         => 'Telefone*',
										'placeholder'   => 'Telefone',
										'class'         => 'validate[required] form-control phone-mask',
									]) ?>
								</div>
								<div class="col-md-6 col-xs-6">
									<?= $this->Form->input('mobile', [
										'div'           => false,
										'label'         => 'Celular/WhatsApp',
										'placeholder'   => 'Celular/WhatsApp',
										'class'         => 'validate[optional] form-control phone-mask',
									]) ?>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6 col-xs-12">
									<?= $this->Form->input('alunos', [
										'div'           => false,
										'type'          => 'number',
										'label'         => 'Quantidade de alunos*',
										'placeholder'   => 'Quantidade de alunos',
										'class'         => 'validate[required] form-control',
									]) ?>
									<br/>
								</div>
							</div>
							<hr> <!-- barra divisor -->
							<div class="form-group row">
								<div class="col-md-12">
									<h4 class="font-bold"><i class="fa fa-plus-circle"></i> Informe quais modalidades a academia oferece</h4>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-12">
									<?php
									foreach($esportes_list as $ke => $esporte):
										echo '<div class="col-lg-4 font-bold"><label for="esporte_'.$ke.'">';
										echo $this->Form->checkbox('esportes.'.$ke, [
											'id'            => 'esporte_'.$ke,
											'hiddenField'   => 'N',
											'value'         => 'Y',
											'label'         => false,
											'class'         => 'custom-checkbox'
										]);
										echo ' '.$esporte;
										echo '</<label></div>';
									endforeach;
									?>
									<br/>
								</div>
							</div>
							<hr> <!-- barra divisor -->
							<div class="form-group row">
								<div class="col-md-6">
									<?= $this->Form->input('email', [
										'div'           => false,
										'label'         => 'E-mail*',
										'placeholder'   => 'E-mail',
										'class'         => 'validate[required,custom[email]] form-control',
									]) ?>
								</div>
								<div class="col-md-6">
									<?= $this->Form->input('password', [
										'div'           => false,
										'label'         => 'Senha*',
										'placeholder'   => 'Senha para acesso',
										'class'         => 'validate[required] form-control',
									]) ?>
									<br/>
								</div>
							</div>
							<hr> <!-- barra divisor -->
							<div class="form-group row">
								<div class="col-md-4 col-xs-6">
									<?= $this->Form->input('cep', [
										'div'           => false,
										'label'         => 'CEP*',
										'placeholder'   => 'CEP',
										'class'         => 'validate[required] form-control cep',
									]) ?>
								</div>
								<div class="col-md-6 col-xs-12">
									<?= $this->Form->input('address', [
										'div'           => false,
										'label'         => 'Endereço*',
										'placeholder'   => 'Endereço',
										'class'         => 'validate[required] form-control',
									]) ?>
								</div>
								<div class="col-md-2 col-xs-6">
									<?= $this->Form->input('number', [
										'div'           => false,
										'label'         => 'Número*',
										'placeholder'   => 'Número',
										'class'         => 'validate[required] form-control',
									]) ?>
									<br/>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6">
									<?= $this->Form->input('complement', [
										'div'           => false,
										'label'         => 'Complemento',
										'placeholder'   => 'Complemento',
										'class'         => 'validate[optional] form-control',
									]) ?>
								</div>
								<div class="col-md-6">
									<?= $this->Form->input('area', [
										'div'           => false,
										'label'         => 'Bairro*',
										'placeholder'   => 'Bairro',
										'class'         => 'validate[required] form-control',
									]) ?>
									<br/>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-3">
									<?= $this->Form->input('uf_id', [
										'id'            => 'states',
										'div'           => false,
										'label'         => 'Estado*',
										'options'       => $states,
										'empty'         => 'Selecione um estado',
										'placeholder'   => 'Estado',
										'class'         => 'validate[required] form-control',
									]) ?>
								</div>
								<div class="col-md-6 col-lg-offset-3">
									<?= $this->Form->input('city_id', [
										'id'            => 'city',
										'div'           => false,
										'label'         => 'Cidade*',
										'empty'         => 'Selecione uma cidade',
										'placeholder'   => 'Cidade',
										'class'         => 'validate[required] form-control',
									]) ?>
								</div>
								<br/>
							</div>

							<!-- <hr> < barra divisor
                                                            <div class="form-group row">
                                                                <div class="col-lg-6">
                                                                    <div class="input file"><label for="image_upload">Logotipo</label><input type="file" name="imagem" id="image_upload" placeholder="Logotipo" class="validate[custom[validateMIME[image/jpeg|image/png]]] form-control">
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-3">
                                                                    <img src="images/logfitness.png" id="image_preview" alt="Image Upload"/>
                                                                </div>
                                                            </div>
                            -->

							<hr> <!-- barra divisor -->
							<div class="row-md-12">
								<div class="col-md-3">
									<?= $this->Html->link('Leia os Termos',
										'/files'.DS.'Termos-de-Aceite.pdf',
										['target' => '_blank'])
									;?>
								</div>
								<div class="col-md-4">
									<label for="aceite" class="">&nbsp&nbsp&nbsp&nbspLi e aceito os termos de aceite
										<?= $this->Form->input('label', [
											'type' => 'checkbox',
											'label' => false,
											'id' => 'aceite',
											'class' => 'text-center checkbox-aval validate[required]',
											'style' => 'margin: -27px 0'
										])?>
									</label>
								</div>
								<div class="form-group row">
									<div class="col-md-2">
										<?= $this->Form->button('Enviar Cadastro', [
											'id'            => 'submit-cadastrar-academia',
											'type'          => 'button',
											'div'           => false,
											'class'         => 'btn btn-default float-right',
										]) ?>
									</div>
									<div class="col-md-5" id="form-toggle-message-academia"></div>
								</div>
							</div>
							<?= $this->Form->end() ?>
						</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end cadastre-se -->

		<!-- start contact -->
		<section id="duvidas">
			<div class="overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="contact-form">
                                <?= $this->Form->create(null, [
                                    'id'        => 'form-central-relacionamento',
                                    'role'      => 'form',
                                    'default'   => false,
                                    'type'      => 'file'
                                ]) ?>
                                <div class="form-group row">
									<div class="col-md-12">
                                        <?= $this->Form->input('name', [
                                            'id'            => 'contato-name',
                                            'div'           => false,
                                            'label'         => 'Nome',
                                            'placeholder'   => 'Nome Completo',
                                            'class'         => 'validate[required] form-control',
                                        ]) ?>
                                        <br/>
									</div>
                                </div>
                                <div class="form-group row">
									<div class="col-md-6">
                                        <?= $this->Form->input('phone', [
                                            'id'            => 'contato-phone',
                                            'div'           => false,
                                            'label'         => 'Telefone',
                                            'placeholder'   => 'Telefone',
                                            'class'         => 'validate[required] form-control phone-mask',
                                        ]) ?>
									</div>

									<div class="col-md-6">
                                        <?= $this->Form->input('email', [
                                            'id'            => 'contato-email',
                                            'div'           => false,
                                            'label'         => 'E-mail',
                                            'placeholder'   => 'E-mail',
                                            'class'         => 'validate[required,custom[email]] form-control',
                                        ]) ?>
                                        <br/>
									</div>
                                </div>
                                <div class="form-group row">
									<div class="col-md-6">
                                        <?= $this->Form->input('description', [
                                            'id'            => 'contato-description',
                                            'div'           => false,
                                            'label'         => 'Assunto',
                                            'placeholder'   => 'Assunto / Descrição',
                                            'class'         => 'validate[required] form-control',
                                        ]) ?>
                                        <br/>
									</div>
                                </div>
                                <div class="form-group row">
									<div class="col-md-12">
                                        <?= $this->Form->input('message', [
                                            'id'            => 'contato-name',
                                            'type'          => 'textarea',
                                            'div'           => false,
                                            'label'         => 'Mensagem',
                                            'placeholder'   => 'Mensagem',
                                            'class'         => 'validate[required] form-control',
                                        ]) ?>
                                        <br/>
									</div>
                                </div>
									<div class="col-md-8">
                                        <?= $this->Form->button('Enviar Contato', [
                                            'id'            => 'submit-cadastrar-central-relacionamento',
                                            'type'          => 'button',
                                            'div'           => false,
                                            'class'         => 'btn btn-default float-right',
                                        ]) ?>
									</div>
                                <div class="col-lg-5" id="form-toggle-message"></div>
							</div>
                            <?= $this->Form->end() ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		 <!-- end contact -->

		<!-- start quemsou -->
		<section id="quemsou">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-xs-12" id="titulo_quem_sou" >

					</div>
					<div class="col-md-6 col-xs-12">
						<p class="txt_titulos_quem_sou">Se você é um aluno e está buscando<br/>
							pelos melhores produtos do mercado<br/>
							ou é um professor querendo dar um UP<br/>
							em sua renda acesse a página que<br/>
							criamos exclusivamente pra você !</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 col-md-offset-2">
						<a href="http://www.logfitness.com.br"><input type="button" class="botoes" id="btn_aluno" value="Aluno"></a>
					</div>
					<div class="col-md-4">
						<input type="button" class="botoes" id="btn_professor" value="Professor">
					</div>
				</div>
			</div>
		</section>
		<!-- end quemsou -->

		<!-- start footer -->
		<footer>
			<div class="container">
				<div class="row">
					<p style="font-size:12px;">Copyright ©2016 <span>www.logfitness.com.br</span>. TODOS OS DIREITOS RESERVADOS.
                Todo o conteúdo do site, todas as fotos,imagens, logotipos, marcas, dizeres, som, software, conjunto imagem,
                layout, trade dress, aqui veiculados são de propriedade exclusiva da LogFitness. É vedada qualquer reprodução,
                total ou parcial, de qualquer elemento de identidade, sem expressa autorização. A violação de qualquer direito mencionado
                implicará na responsabilização cível e criminal nos termos da Lei.

                CACIQUE DIGITAL REPRESENTACOES LTDA - ME - CNPJ: 22.921.802/0001-77 Rua Antonio Munia, 141, Sala 01 - Jardim Nazareth - CEP 15054-160 - São José do Rio Preto - SP</p>
				</div>
			</div>
		</footer>
		  <!--   end footer -->
		<script>-->
		<?= file_get_contents(JS_URL.'jquery.cadastrar.js') ?>
		<?= file_get_contents(JS_URL.'jquery.js') ?>
		<?= file_get_contents(JS_URL.'bootstrap.min.js') ?>
		<?= file_get_contents(JS_URL.'wow.min.js') ?>
		<?= file_get_contents(JS_URL.'jquery.singlePageNav.min.js') ?>
		<?= file_get_contents(JS_URL.'custom.js') ?>
		</script>

		<script>
			$(document).ready(function(){
				$("#btn_cadastrar").click(function(){
					$("#duvidas").slideUp();
					$("#cadastre-se").slideToggle();
				});
				$("#btn_duvidas").click(function(){
					$("#cadastre-se").slideUp();
					$("#duvidas").slideToggle();
				});
				$("#btn_cadastrar_menu").click(function(){
					$('body').animate({scrollTop:3650}, 1500);
					$("#btn_cadastrar").click();
				});
				var ctrl = 0;

				$('.navbar-toggle').click(function() {

					if(ctrl == 0) {
						$('nav li').show();
						$('nav ul').animate({
							height: "276px",
							opacity: 1
						}, 750);
						ctrl = 1;
					}
					else if(ctrl == 1) {
						$('nav li').hide();
						$('nav ul').animate({
							height: "0px",
							opacity:0
						}, 750);
						ctrl = 0;
					}
				});
				$('.navbar-brand').click(function(){
					$('body').animate({scrollTop:0}, 1500);
				});
				$('.menu-home').click(function(){
					$('body').animate({scrollTop:0}, 1500);
				});
				$('.menu-vantagens').click(function(){
					$('body').animate({scrollTop:730}, 1500);
				});
				$('.menu-funciona').click(function(){
					$('body').animate({scrollTop:1400}, 1500);
				});
				$('.menu-depoimentos').click(function(){
					$('body').animate({scrollTop:2950}, 1500);
				});
				$('.menu-depoimentos').click(function(){
					$('body').animate({scrollTop:2950}, 1500);
				});

			});



		</script>

		<script>
			$(document).ready(function() {

				$("#owl-demo").owlCarousel({
					autoPlay : 10000,
					navigation : false, // Show next and prev buttons
					slideSpeed : 300,
					paginationSpeed : 400,
					singleItem:true,
					stopOnHover:true

					// "singleItem:true" is a shortcut for:
					// items : 1,
					// itemsDesktop : false,
					// itemsDesktopSmall : false,
					// itemsTablet: false,
					// itemsMobile : false

				});

			});
		</script>

	</body>
</html>
