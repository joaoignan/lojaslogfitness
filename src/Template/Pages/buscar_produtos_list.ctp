<?php foreach ($produtos as $produto) { ?>
	<div class="radio">
		<label>
			<input type="radio" name="optionProduto" class="option-produto" id="id-produto-<?= $produto->id ?>" value="<?= $produto->id ?>">
			<?= $produto->produto_base->name ?> - <?= $produto->propriedade ?> - R$<?= number_format($produto->preco, 2, ',', '.') ?>.
		</label>
	</div>
<?php } ?>