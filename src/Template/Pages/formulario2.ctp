<html>
<head>
    <?= $this->Html->css('bootstrap.min.css')?>
    <?= $this->Html->css('academia_new_animate.min.css')?>
    <?= $this->Html->css('academia_new_bootstrap.min.css')?>
    <?= $this->Html->css('font-awesome.min.css')?>
    <?= $this->Html->css('academia_new_templatemo-style.css')?>
</head>
<body>
<section id="tenhalog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6 col-xs-12" id="titulo_tenha_log" >

                </div>
                <div class="col-md-6 col-xs-12">
                    <p class="txt_titulos_tenha_log">Preencha as informações abaixo e<br/>
                        envie o formulário de solicitação ou<br/>
                        tire suas dúvidas com nosso contato.</p>
                </div>
            </div>
            <div class="col-md-4 col-md-offset-2">
                <input type="button" class="botoes" id="btn_cadastrar" value="Cadastrar">
            </div>
            <div class="col-md-4">
                <input type="button" class="botoes" id="btn_duvidas" value="Tirar dúvidas">
            </div>
        </div>
    </div>
</section>

<section id="cadastre-se2">
    <div class="overlay">
        <div class="container">
            <div class="row-md-12">
                <div class="col-md-12">
                    <?= $this->Form->create($academia, [
                        'id'        => 'form-cadastrar-academia',
                        'role'      => 'form',
                        'default'   => false,
                        //'type'      => 'file'
                    ]) ?>
                    <div class="form-group row">
                        <div class="col-md-12 col-xs-12">
                            <?= $this->Form->input('name', [
                                'id'            => 'academia_name',
                                'div'           => false,
                                'label'         => 'Razão Social*',
                                'placeholder'   => 'Razão Social',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                            <br/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 col-xs-12">
                            <?= $this->Form->input('shortname', [
                                'div'           => false,
                                'maxlength'     => 22,
                                'label'         => 'Nome Curto*',
                                'placeholder'   => 'Nome Fantasia / Nome de Fachada / Marca Empresarial',
                                'class'         => 'validate[required,maxSize[22]] form-control',
                            ]) ?>
                            <br/>
                        </div>

                        <div class="col-lg-3 col-xs-5">
                            <?= $this->Form->input('slug', [
                                'value'         => 'logfitness.com.br/',
                                'div'           => false,
                                'disabled'      => true,
                                'readonly'      => true,
                                'label'         => 'URL Personalizada*',
                                'placeholder'   => 'nome-personalizado-da-sua-academia',
                                'class'         => 'slug-academia-texto form-control text-right',
                            ]) ?>
                        </div>

                        <div class="col-md-4 col-xs-12">
                            <?= $this->Form->input('slug', [
                                'div'           => false,
                                'label'         => 'URL Personalizada*',
                                'placeholder'   => 'nome-personalizado-da-sua-academia',
                                'class'         => 'validate[required,custom[slug]]] form-control',
                            ]) ?>
                            <br/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6 col-xs-12">
                            <?= $this->Form->input('cnpj', [
                                'div'           => false,
                                'label'         => 'CNPJ*',
                                'placeholder'   => 'CNPJ',
                                'class'         => 'validate[required,custom[cnpj]] form-control cnpj',
                            ]) ?>
                            <br/>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <?= $this->Form->input('ie', [
                                'div'           => false,
                                'label'         => 'Inscrição Estadual',
                                'placeholder'   => 'Inscrição Estadual',
                                'class'         => 'validate[optional] form-control',
                            ]) ?>
                            <br/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 col-xs-12">
                            <?= $this->Form->input('contact', [
                                'div'           => false,
                                'label'         => 'Responsável*',
                                'placeholder'   => 'Nome do Responsável',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                            <br/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6 col-xs-6">
                            <?= $this->Form->input('phone', [
                                'div'           => false,
                                'label'         => 'Telefone*',
                                'placeholder'   => 'Telefone',
                                'class'         => 'validate[required] form-control phone-mask',
                            ]) ?>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <?= $this->Form->input('mobile', [
                                'div'           => false,
                                'label'         => 'Celular/WhatsApp',
                                'placeholder'   => 'Celular/WhatsApp',
                                'class'         => 'validate[optional] form-control phone-mask',
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6 col-xs-12">
                            <?= $this->Form->input('alunos', [
                                'div'           => false,
                                'type'          => 'number',
                                'label'         => 'Quantidade de alunos*',
                                'placeholder'   => 'Quantidade de alunos',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                            <br/>
                        </div>
                    </div>
                    <hr> <!-- barra divisor -->
                    <div class="form-group row">
                        <div class="col-md-12">
                            <h4 class="font-bold"><i class="fa fa-plus-circle"></i> Informe quais modalidades a academia oferece</h4>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <?php
                            foreach($esportes_list as $ke => $esporte):
                                echo '<div class="col-lg-4 font-bold"><label for="esporte_'.$ke.'">';
                                echo $this->Form->checkbox('esportes.'.$ke, [
                                    'id'            => 'esporte_'.$ke,
                                    'hiddenField'   => 'N',
                                    'value'         => 'Y',
                                    'label'         => false,
                                    'class'         => 'custom-checkbox'
                                ]);
                                echo ' '.$esporte;
                                echo '</<label></div>';
                            endforeach;
                            ?>
                            <br/>
                        </div>
                    </div>
                    <hr> <!-- barra divisor -->
                    <div class="form-group row">
                        <div class="col-md-6">
                            <?= $this->Form->input('email', [
                                'div'           => false,
                                'label'         => 'E-mail*',
                                'placeholder'   => 'E-mail',
                                'class'         => 'validate[required,custom[email]] form-control',
                            ]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->input('password', [
                                'div'           => false,
                                'label'         => 'Senha*',
                                'placeholder'   => 'Senha para acesso',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                            <br/>
                        </div>
                    </div>
                    <hr> <!-- barra divisor -->
                    <div class="form-group row">
                        <div class="col-md-4 col-xs-6">
                            <?= $this->Form->input('cep', [
                                'div'           => false,
                                'label'         => 'CEP*',
                                'placeholder'   => 'CEP',
                                'class'         => 'validate[required] form-control cep',
                            ]) ?>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <?= $this->Form->input('address', [
                                'div'           => false,
                                'label'         => 'Endereço*',
                                'placeholder'   => 'Endereço',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                        </div>
                        <div class="col-md-2 col-xs-6">
                            <?= $this->Form->input('number', [
                                'div'           => false,
                                'label'         => 'Número*',
                                'placeholder'   => 'Número',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                            <br/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <?= $this->Form->input('complement', [
                                'div'           => false,
                                'label'         => 'Complemento',
                                'placeholder'   => 'Complemento',
                                'class'         => 'validate[optional] form-control',
                            ]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->input('area', [
                                'div'           => false,
                                'label'         => 'Bairro*',
                                'placeholder'   => 'Bairro',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                            <br/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <?= $this->Form->input('uf_id', [
                                'id'            => 'states',
                                'div'           => false,
                                'label'         => 'Estado*',
                                'options'       => $states,
                                'empty'         => 'Selecione um estado',
                                'placeholder'   => 'Estado',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                        </div>
                        <div class="col-md-6 col-lg-offset-3">
                            <?= $this->Form->input('city_id', [
                                'id'            => 'city',
                                'div'           => false,
                                'label'         => 'Cidade*',
                                'empty'         => 'Selecione uma cidade',
                                'placeholder'   => 'Cidade',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                        </div>
                        <br/>
                    </div>

                    <!-- <hr> < barra divisor
                                                    <div class="form-group row">
                                                        <div class="col-lg-6">
                                                            <div class="input file"><label for="image_upload">Logotipo</label><input type="file" name="imagem" id="image_upload" placeholder="Logotipo" class="validate[custom[validateMIME[image/jpeg|image/png]]] form-control">
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <img src="images/logfitness.png" id="image_preview" alt="Image Upload"/>
                                                        </div>
                                                    </div>
                    -->

                    <hr> <!-- barra divisor -->
                    <div class="row-md-12">
                        <div class="col-md-3">
                            <?= $this->Html->link('Leia os Termos',
                                '/files'.DS.'Termos-de-Aceite.pdf',
                                ['target' => '_blank'])
                            ;?>
                        </div>
                        <div class="col-md-4">
                            <label for="aceite" class="">&nbsp&nbsp&nbsp&nbspLi e aceito os termos de aceite
                                <?= $this->Form->input('label', [
                                    'type' => 'checkbox',
                                    'label' => false,
                                    'id' => 'aceite',
                                    'class' => 'text-center checkbox-aval validate[required]',
                                    'style' => 'margin: -27px 0'
                                ])?>
                            </label>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <?= $this->Form->button('Enviar Cadastro', [
                                    'id'            => 'submit-cadastrar-academia',
                                    'type'          => 'button',
                                    'div'           => false,
                                    'class'         => 'btn btn-default float-right',
                                ]) ?>
                            </div>
                            <div class="col-md-5" id="form-toggle-message-academia"></div>
                        </div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="duvidas">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="contact-form">
                        <?= $this->Form->create(null, [
                            'id'        => 'form-central-relacionamento',
                            'role'      => 'form',
                            'default'   => false,
                            'type'      => 'file'
                        ]) ?>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <?= $this->Form->input('name', [
                                    'id'            => 'contato-name',
                                    'div'           => false,
                                    'label'         => 'Nome',
                                    'placeholder'   => 'Nome Completo',
                                    'class'         => 'validate[required] form-control',
                                ]) ?>
                                <br/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <?= $this->Form->input('phone', [
                                    'id'            => 'contato-phone',
                                    'div'           => false,
                                    'label'         => 'Telefone',
                                    'placeholder'   => 'Telefone',
                                    'class'         => 'validate[required] form-control phone-mask',
                                ]) ?>
                            </div>

                            <div class="col-md-6">
                                <?= $this->Form->input('email', [
                                    'id'            => 'contato-email',
                                    'div'           => false,
                                    'label'         => 'E-mail',
                                    'placeholder'   => 'E-mail',
                                    'class'         => 'validate[required,custom[email]] form-control',
                                ]) ?>
                                <br/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <?= $this->Form->input('description', [
                                    'id'            => 'contato-description',
                                    'div'           => false,
                                    'label'         => 'Assunto',
                                    'placeholder'   => 'Assunto / Descrição',
                                    'class'         => 'validate[required] form-control',
                                ]) ?>
                                <br/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <?= $this->Form->input('message', [
                                    'id'            => 'contato-name',
                                    'type'          => 'textarea',
                                    'div'           => false,
                                    'label'         => 'Mensagem',
                                    'placeholder'   => 'Mensagem',
                                    'class'         => 'validate[required] form-control',
                                ]) ?>
                                <br/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <?= $this->Form->button('Enviar Contato', [
                                'id'            => 'submit-cadastrar-central-relacionamento',
                                'type'          => 'button',
                                'div'           => false,
                                'class'         => 'btn btn-default float-right',
                            ]) ?>
                        </div>
                        <div class="col-lg-5" id="form-toggle-message"></div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- start cadastre-se-->
<section id="cadastre-se">
    <div class="overlay">
        <div class="container" id="cadastro-professor">
            <div class="row-md-12">
                <div class="col-md-12">
                    <?= $this->Form->create(null, [
                        'id'        => 'form-cadastrar-professor',
                        'role'      => 'form',
                        'default'   => false,
                        'type'      => 'file'
                    ]) ?>

                    <div class="form-group row">
                        <div class="col-lg-8">
                            <?= $this->Form->input('name', [
                                'div'           => false,
                                'label'         => 'Nome*',
                                'placeholder'   => 'Nome Completo',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <?= $this->Form->input('cpf', [
                                'div'           => false,
                                'label'         => 'CPF*',
                                'placeholder'   => 'CPF',
                                'class'         => 'validate[required,custom[cpf]] form-control cpf',
                            ]) ?>
                        </div>

                        <div class="col-lg-2">
                            <?= $this->Form->input('rg', [
                                'div'           => false,
                                'label'         => 'RG*',
                                'placeholder'   => 'RG',
                                'class'         => 'validate[optional] form-control',
                            ]) ?>
                        </div>

                        <div class="col-lg-3">
                            <?= $this->Form->input('cref', [
                                'div'           => false,
                                'label'         => 'CREF*',
                                'placeholder'   => 'CREF',
                                'class'         => 'validate[optional] form-control',
                            ]) ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <?= $this->Form->input('phone', [
                                'div'           => false,
                                'label'         => 'Telefone*',
                                'placeholder'   => 'Telefone',
                                'class'         => 'validate[required] form-control phone-mask',
                            ]) ?>
                        </div>

                        <div class="col-lg-3">
                            <?= $this->Form->input('mobile', [
                                'div'           => false,
                                'label'         => 'Celular/WhatsApp',
                                'placeholder'   => 'Celular/WhatsApp',
                                'class'         => 'validate[optional] form-control phone-mask',
                            ]) ?>
                        </div>

                    </div>

                    <hr>

                    <hr>
                    <div class="form-group row">
                        <div class="col-lg-5">
                            <?= $this->Form->input('email', [
                                'div'           => false,
                                'label'         => 'Email*',
                                'placeholder'   => 'Email',
                                'class'         => 'validate[required,custom[email]] form-control',
                            ]) ?>
                        </div>

                        <div class="col-lg-3">
                            <?= $this->Form->input('password', [
                                'div'           => false,
                                'label'         => 'Senha*',
                                'placeholder'   => 'Senha para acesso',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                        </div>
                    </div>

                    <hr>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <?= $this->Form->input('cep', [
                                'div'           => false,
                                'label'         => 'CEP*',
                                'placeholder'   => 'CEP',
                                'class'         => 'validate[required] form-control cep',
                            ]) ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-6">
                            <?= $this->Form->input('address', [
                                'div'           => false,
                                'label'         => 'Endereço*',
                                'placeholder'   => 'Endereço',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                        </div>

                        <div class="col-lg-2">
                            <?= $this->Form->input('number', [
                                'div'           => false,
                                'label'         => 'Número*',
                                'placeholder'   => 'Número',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <?= $this->Form->input('complement', [
                                'div'           => false,
                                'label'         => 'Complemento',
                                'placeholder'   => 'Complemento',
                                'class'         => 'validate[optional] form-control',
                            ]) ?>
                        </div>
                        <div class="col-lg-5">
                            <?= $this->Form->input('area', [
                                'div'           => false,
                                'label'         => 'Bairro*',
                                'placeholder'   => 'Bairro',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3">
                            <?= $this->Form->input('uf_id', [
                                'id'            => 'states',
                                'div'           => false,
                                'label'         => 'Estado*',
                                'options'       => $states,
                                'empty'         => 'Selecione um estado',
                                'placeholder'   => 'Estado',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                        </div>

                        <div class="col-lg-5">
                            <?= $this->Form->input('city_id', [
                                'id'            => 'city',
                                'div'           => false,
                                'label'         => 'Cidade*',
                                'empty'         => 'Selecione uma cidade',
                                'placeholder'   => 'Cidade',
                                'class'         => 'validate[required] form-control',
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <?= $this->Form->button('Enviar Cadastro', [
                                //'id'            => 'submit-cadastrar-professor',
                                'type'          => 'submit',
                                'div'           => false,
                                'class'         => 'btn btn-default float-right',
                            ]) ?>

                        </div>

                        <div class="col-lg-5" id="form-toggle-message"></div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
</section>
<!-- end cadastre-se -->

<script>
    $(document).ready(function(){
        $("#btn_cadastrar").click(function(){
            $("#duvidas").slideUp();
            $("#cadastre-se").slideToggle();
        });
        $("#btn_duvidas").click(function(){
            $("#cadastre-se").slideUp();
            $("#duvidas").slideToggle();
        });
        $("#btn_cadastrar_menu").click(function(){
            $('body').animate({scrollTop:3500}, 1500);
            return false;
        });
        var medida_tela = $(window).width();
        if(medida_tela < 800) {
            //se clicar em um link do navbar -> menu fecha
            $('.nav li a').click(function () {
                $('.menu-navbar').slideUp(750);
            });
            //se clicar no icon do navbar -> menu toggle(abre e fecha)
            $('.navbar-toggle').click(function () {
                $('.menu-navbar').toggle(750);
            });
        }
        $('.navbar-brand').click(function(){
            $('body').animate({scrollTop:0}, 1500);
            return false;
        });
        $('.menu-home').click(function(){
            $('body').animate({scrollTop:0}, 1500);
            return false;
        });
        $('.menu-vantagens').click(function(){
            $('body').animate({scrollTop:730}, 1500);
            return false;
        });
        $('.menu-funciona').click(function(){
            $('body').animate({scrollTop:1400}, 1500);
            return false;
        });
        $('.menu-depoimentos').click(function(){
            $('body').animate({scrollTop:2950}, 1500);
            return false;
        });
    });
</script>

</body>
</html>