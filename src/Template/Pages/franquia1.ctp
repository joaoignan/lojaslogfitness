<!DOCTYPE html>
<html lang="en">
     <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="Logfitness - A franquia da academia do futuro!" content="">
        <meta name="Logfitness" content="">
        <title>Logfitness - A franquia da academia do futuro!</title>
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
        <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?= WEBROOT_URL?>img/favicon/manifest.json">
         
        <!-- Theme CSS -->
        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('creative.min.css') ?>
        <?= $this->Html->css('magnific-popup.css') ?>
        <?= $this->Html->css('font-awesome.min.css') ?>

        <script>
            var WEBROOT_URL  = "<?= WEBROOT_URL ?>";
        </script>
        <!-- Smart Look -->
            <script type="text/javascript">
                window.smartlook||(function(d) {
                var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
                var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
                c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
                })(document);
                smartlook('init', '65dd1b31ade411a0f1f09a9524c2adf7c3ca5eb5');
            </script>
        <!-- End Smart Look -->
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
             fbq('init', '1675974925977278'); 
            fbq('track', 'PageView');
        </script>
        <noscript>
             <img height="1" width="1" 
            src="https://www.facebook.com/tr?id=1675974925977278&ev=PageView
            &noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
     </head>
     <body id="page-top">
         <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
             <div class="container-fluid">
                 <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                          <span class="sr-only">Toggle navigation</span><i class="fa fa-bars"></i>
                     </button>
                     <a class="navbar-brand page-scroll" href="#page-top">
                     <?= $this->Html->image('logfitnessfranquias.png', ['class' => 'logo-nav','alt' => 'Logfitness'])?>
                     </a>
                 </div>
                 <!-- Collect the nav links, forms, and other content for toggling -->
                 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                         <li>
                             <a class="page-scroll" href="#neg">A franquia</a>
                         </li>
                         <li>
                             <a class="page-scroll" href="#invest">Investimento</a>
                         </li>
                         <li>
                             <a class="page-scroll" href="#contact">Seja um franqueado</a>
                         </li>
                         <!--                    <li>
                             <a class="page-scroll" href="#contact">Contact</a>
                         </li>-->
                     </ul>
                 </div>
             </div>
         </nav>
         <header>
             <div class="header-content">
                <div class="header-content-inner">
                   <div class="row">
                      <div class="col-md-6 pull-left text-left">
                         <h1 id="homeHeading">Faça do <br> fitness o <br> seu negócio!</h1>
                         <!--<a href="#neg" class="btn btn-primary btn-lg page-scroll btn-mobile">Conheça nosso modelo de franquia</a> <br/> -->
                      </div>
                   </div>
                </div>
             </div>
         </header>
         <section class="bg-primary" id="about">
             <div class="container">
                 <div class="row">
                     <div class="col-md-6 text-left">
                         <h2 class="section-heading uper">Porque escolher a Logfitness?</h2>
                         <p class="text-faded">
                         Por que escolher entre loja física ou e-commerce? Inconformados com este modelo de negócio desatualizado, desde 2015 estamos trabalhando com uma meta...
                         </p>
                         <p class="text-faded">
                         Unir o melhor do universo físico e digital numa plataforma que traz o futuro para dentro das academias!
                         </p>
                         <!--<a href="#contact" class="page-scroll btn btn-default btn-xl sr-button btn-mobile">Seja um franqueado!</a>-->
                     </div>
                     <div class="col-md-6">
                         <iframe width="100%" height="300px" src="https://www.youtube.com/embed/Nr7UNxpzO18" frameborder="0" allowfullscreen></iframe>
                     </div>
                 </div>
             </div>
         </section>
         <section id="neg">
             <div class="container">
                 <h2 class="section-heading uper">Mercado potencial</h2>
                 <div class="row" style="margin-bottom: 25px">
                     <!--<div class="col-lg-3 col-md-6 text-center">
                         <div class="service-box">
                             <i class="fa fa-5x fa-bolt text-primary sr-icons"></i>
                             <h3>Parece mágica</h3>
                             <p class="text-muted">Alta rentabilidade com micro investimento</p>
                         </div>
                     </div>
                     <div class="col-lg-3 col-md-6 text-center">
                         <div class="service-box">
                             <i class="fa fa-5x fa-line-chart text-primary sr-icons"></i>
                             <h3>Muito potencial</h3>
                             <p class="text-muted">Mercado em total ascensão pronto para ser explorado</p>
                         </div>
                     </div>
                     <div class="col-lg-3 col-md-6 text-center">
                         <div class="service-box">
                             <i class="fa fa-5x fa-joomla text-primary sr-icons"></i>
                             <h3>Home Office</h3>
                             <p class="text-muted">Sistema web integrado, sem necessidade de estrutura física</p>
                         </div>
                     </div>
                     <div class="col-lg-3 col-md-6 text-center">
                         <div class="service-box">
                             <i class="fa fa-5x fa-plane text-primary sr-icons"></i>
                             <h3>Por nossa conta</h3>
                             <p class="text-muted">Sem investimento em estoque, sem preocupação com a entrega</p>
                         </div>
                     </div>-->
                     

                     <!--<div class="col-lg-2 col-md-6 text-center">
                         <div class="service-box">
                             <?= $this->Html->image('alvo.jpg', ['class' => '','alt' => 'Linha do Tempo'])?>
                             <p><span>ALTA RENTABILIDADE COM MICRO INVESTIMENTO</span></p>
                         </div>
                     </div>
                     <div class="col-lg-2 col-md-6 text-center">
                         <div class="service-box hexagono">
                             <?= $this->Html->image('foguete.jpg', ['class' => '','alt' => 'Linha do Tempo'])?>
                             <p><span>MARCADO EM TOTAL ASCENSÃO</span></p>
                         </div>
                     </div>
                     <div class="col-lg-2 col-md-6 text-center">
                         <div class="service-box">
                             <?= $this->Html->image('correntes.jpg', ['class' => '','alt' => 'Linha do Tempo'])?>
                             <p><span>SISTEMA TODO INTEGRADO NA WEB</span></p>
                         </div>
                     </div>
                     <div class="col-lg-2 col-md-6 text-center">
                         <div class="service-box">
                             <?= $this->Html->image('computer.jpg', ['class' => '','alt' => 'Linha do Tempo'])?>
                             <p><span>NÃO É NECESSÁRIO LOCAL FÍSICO</span></p>
                         </div>
                     </div>
                     <div class="col-lg-2 col-md-6 text-center">
                         <div class="service-box">
                             <?= $this->Html->image('grafico.jpg', ['class' => '','alt' => 'Linha do Tempo'])?>
                             <p><span>SEM INVESTIMENTO EM ESTOQUE</span></p>
                         </div>
                     </div>
                     <div class="col-lg-2 col-md-6 text-center">
                         <div class="service-box">
                             <?= $this->Html->image('img.jpg', ['class' => '','alt' => 'Linha do Tempo'])?>
                             <p><span>SEM PREOCUPAÇÃO COM ENTREGA OU LOGÍSTICA</span></p>
                         </div>
                     </div> -->

                         <?= $this->Html->image('vantagens.jpg', ['class' => 'img-responsive','alt' => 'Alta rentabilidade com micro investimento. Mercado em total ascensão. Sistema todo integrado na web. Não é necessário local físico. Sem investimento em estoque. Fazer parte da maior empresa do segmento. Sem preocupação com entrega ou logística.'])?>



                 </div>
                 <div class="row">
                     <div class="col-md-6">
                         <h2 class="section-heading uper">Um mercado que só faz crescer!</h2>
                         <!--<p class="text-muted">
                         O segmento de Esporte e Lazer corresponde a 13% do faturamento do mercado de franquias no Brasil. Em números, são R$ 6,7 bilhões! Deste total, 70% vêm e franquias fitness. 
                         A participação é alta e ainda está longe de chegar a seu potencial. É um segmento relativamente novo no mercado de franquias. 
                         <br /> <small>FONTE: ABF (ASSOCIAÇÃO BRASILEIRA DE FRANCHISING) - PORTAL DO FRANCHISING</small>
                         </p>-->
                         <p class="text-muted">
                        Hoje, a indústria de suplementos alimentares é uma das maiores e mais lucrativas do mundo. Em nosso país, o segmento cresce cerca de 23% ao ano. Atualmente, no Brasil, 2,5 milhões de pessoas consomem suplementos alimentares. 
                         </p>
                         <p class="text-muted">
                         Consumir suplementos é um hábito que tem aumentado significativamente em nossa sociedade. O crescimento foi de R$150 milhões para <strong>R$ 1,3 bilhoes</strong>.
                         <br /> <small>FONTE: ASSOCIAÇÃO BRASILEIRA DE FABRICANTES DE SUPLEMENTOS (BRASNUTRI)</small>
                         </p>
                     </div>
                     <div class="col-md-5 img-responsive pull-right">
                         <?= $this->Html->image('comp.png', ['class' => 'img-responsive','alt' => 'Loja Modelo'])?>
                     </div>
                 </div>
                 <div class="row mobile-hide">
                 <div class="col-lg-12 text-left" style="padding-bottom: 25px;">
                     <h2 class="section-heading uper">E de onde veio a log?</h2>
                     <?= $this->Html->image('timeline.png', ['class' => 'img-responsive','alt' => 'Linha do Tempo'])?>
                     
                     <!--<hr class="primary">
                     </div>-->
                 </div>
             </div>
             </div>
             
         </section>
         <!---<aside class="bg-dark">
             <div class="container text-center">
                <div class="call-to-action">
                   <h2>Pronto para ter uma franquia de sucesso?</h2>
                   <a href="#contact" class="btn btn-default btn-xl sr-button btn-mobile">Faça parte do time Logfitness</a>
                </div>
             </div>
         </aside>-->
         <section id="invest">
             <div class="container">
                 <div class="row">
                     <div class="col-md-5">
                         <p class="uper yellow"><strong>Investimento inicial</strong></p>
                         <strong><span style="font-size: 1.7em" class="uper">Total R$ 12.500</span></strong>
                         <div class="pd-30"></div>
                         <p class="uper yellow"><strong>Custos Mensais</span></strong></p>
                         <table>
                             <tr>
                                 <td>Linha telefônica</td>
                                 <td>R$ 150</td>
                             </tr>
                             <tr>
                                 <td>Combustível</td>
                                 <td>R$ 500</td>
                             </tr>
                             <tr>
                                 <td>Mensalidade site</td>
                                 <td>R$ 0</td>
                             </tr>
                             <tr>
                                 <td>Royalties</td>
                                 <td>R$ 500</td>
                             </tr>
                             <tr>
                                 <td>Marketing</td>
                                 <td>R$ 200</td>
                             </tr>
                             <tr>
                                 <td><strong><span>Total</span></strong></td>
                                 <td><strong><span>R$ 1350</span></strong></td>
                             </tr>
                         </table>
                     </div>
                     <div class="col-md-7 img-responsive pull-right">
                         <h2 class="section-heading uper yellow">Valor do investimento</h2>
                         <p>No investimento inicial da franquia Logfitness estão inclusos taxa de franquia e kit inicial.</p>
                             <div class="row">
                                 <!--<div class="col-md-4 text-center">
                                     <div class="service-box">
                                         <?= $this->Html->image('refresqueira.png', ['class' => '','alt' => 'Refresqueira'])?>
                                         <h4>Refresqueira</h4>
                                     </div>
                                 </div>-->
                                 <div class="col-md-3 text-center">
                                     <div class="service-box">
                                         <?= $this->Html->image('uniforme.png', ['class' => '','alt' => 'Uniforme'])?>
                                         <h4>Uniforme</h4>
                                     </div>
                                 </div>
                                 <div class="col-md-3 text-center">
                                     <div class="service-box">
                                         <?= $this->Html->image('cartazes.png', ['class' => '','alt' => 'Cartazes'])?>
                                         
                                         <h4>R$ 500 em cartazes</h4>
                                     </div>
                                 </div>
                                 <div class="col-md-3 text-center">
                                     <div class="service-box">
                                         <?= $this->Html->image('cartoes.png', ['class' => '','alt' => 'Cartões de Visita'])?>
                                         
                                         <h4>Cartões de visita</h4>
                                     </div>
                                 </div>
                                 <!--<div class="col-md-4 text-center">
                                     <div class="service-box">
                                         <?= $this->Html->image('produtos.png', ['class' => '','alt' => 'Produtos'])?>
                                         
                                         <h4>R$ 1000 em produtos*</h4> <small>*PARA DESGUSTAÇÃO</small>
                                     </div>
                                 </div>-->
                                 <div class="col-md-3 text-center">
                                     <div class="service-box">
                                         <?= $this->Html->image('coqueteleiras.png', ['class' => '','alt' => 'Coqueteleiras'])?>
                                         
                                         <h4>Coqueteleiras</h4>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </section>
         <div class="pd-30"></div>
         <section id="contact" class="bg-dark">
             <div class="container">
                 <div class="row">
                     <div class="col-md-6 text-left">
                         <h2 class="section-heading uper">Seja um franqueado Logfitness!</h2>
                         <p>Agora que você já viu como é possível ter um negócio altamente lucrativo e com baixo investimento, ficou animado, né?</p>
                         <p>Para receber nosso material com maiores informações, você só precisa preencher este formulário. Vamos te enviar tudo por email.</p>
                         <div class="row">
                             <div class="col-md-6 text-center">
                                 <i class="fa fa-phone fa-3x sr-contact"></i>
                                 <p>+55 17 3364 3100</p>
                             </div>
                             <div class="col-md-6 text-center">
                                 <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                                 <p><a class="yellow" href="mailto:franquia@logfitness.com.br">franquia@logfitness.com.br</a></p>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-5 pull-right">
                        <?= $this->Flash->render() ?>
                        <h5 class="uper">Preencha para receber maiores informações</h5>
                        <small>* Dados obrigatórios</small>
                        <?= $this->Form->create(null,['url' => ['action' => 'franquia_envio']]) ?>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <?= $this->Form->input('name', [
                                    'div'           => false,
                                    'label'         => false,
                                    'class'         => 'form-control',
                                    'placeholder'   => 'Nome completo*',
                                    'required'      => true
                                ])?>
                            </div>
                            <div class="col-md-8 form-group">
                                <?= $this->Form->input('email', [
                                    'div'           => false,
                                    'label'         => false,
                                    'class'         => 'form-control',
                                    'placeholder'   => 'Seu melhor e-mail*',
                                    'type'          => 'email',
                                    'required'      => true
                                ])?>
                            </div>
                            <div class="col-md-4 form-group">
                                <?= $this->Form->input('phone', [
                                    'id'            => 'phone',
                                    'div'           => false,
                                    'label'         => false,
                                    'class'         => 'form-control',
                                    'placeholder'   => 'Telefone*',
                                    'type'          => 'tel',
                                    'required'      => true
                                ])?>
                             </div>
                             <div class="col-md-6 form-group">
                                <?= $this->Form->input('uf_id', [
                                    'label'         => false,
                                    'id'            => 'states-franquia',
                                    'div'           => false,
                                    'options'       => $states,
                                    'empty'         => 'Selecione um estado',
                                    'placeholder'   => 'Estado',
                                    'class'         => 'form-control',
                                    'required'      => false
                                ]) ?>
                             </div>
                             <div class="col-md-6 form-group">
                                <?= $this->Form->input('city_id', [
                                    'label'         => false,
                                    'id'            => 'city-franquia',
                                    'div'           => false,
                                    'empty'         => 'Selecione uma cidade',
                                    'placeholder'   => 'Cidade',
                                    'class'         => 'form-control',
                                    'disabled'       => true,
                                    'required'      => false
                                ]) ?>
                             </div>
                             <div class="col-md-12 form-group">
                                 <select id="cidades" class="form-control" name="grau_interesse">
                                     <option>Qual o seu grau de interesse?</option>
                                     <option value="Pouco interessado">Pouco interessado</option>
                                     <option value="Interessado">Interessado</option>
                                     <option value="Muito interessado">Muito interessado</option>
                                 </select>
                                 <p class="help-block text-danger"></p>
                             </div>
                            <div class="col-md-6 text-center pull-right">
                                <?= $this->Form->button('Quero Saber mais', [
                                    'type'      => 'submit',
                                    'class'     => 'btn btn-default btn-xl sr-button bg-yellow',
                                    'escape'    => false
                                ]) ?>
                            </div>
                            <?= $this->Form->end()?>
                            <small>Verifique também sua caixa de spam, caso não receba o email.</small>
                        </div>
                     </div>
                 </div>
             </div>
         </section>

         <footer class="footer">
             <div class="container">
                 <div class="row">
                    <div class="col-md-9">
                        <p>Copyright © www.logfitness.com.br. TODOS OS DIREITOS RESERVADOS.</p>
                    </div>
                     <div class="col-md-3 pull-right">
                         <?= $this->Html->image('logfitnessfranquiasfooter.png', ['class' => '','alt' => 'Logfitness'])?>
                     </div>
                             
                     </div>
                 </div>
             </div>
         </footer>
          <!-- jQuery -->
            <?= $this->Html->script('jquery/jquery.min.js') ?>
            <?= $this->Html->script('jquery.mask.min.js')?>
            <?= $this->Html->script('bootstrap.min.js') ?>
            <?= $this->Html->script('scrollreveal/scrollreveal.min.js') ?>
            <?= $this->Html->script('jquery.magnific-popup.min.js') ?>
            <?= $this->Html->script('creative.min.js') ?>
            <?php if($_GET['anchor'] == 1) { ?>
                <script type="text/javascript">
                    $(window).load(function() {
                        $('html, body').animate({
                            scrollTop: $('#contact').offset().top
                        }, 1000);
                    });
                </script>
            <?php } ?>
        <script>
            $('#states-franquia').on('change', function(){
                var state_id = $(this).val();
                $.get(WEBROOT_URL + '/cidades/' + state_id,
                    function(data){
                        $('#city-franquia').html(data);
                    });
            });
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script type="text/javascript">$("#phone").mask("(00) 00000-0009");</script>
        <!-- Smartsupp Live Chat script -->
        <script type="text/javascript">
            var _smartsupp = _smartsupp || {};
            _smartsupp.key = '472d82fddbe4326ca5661de701a829a7553ce341';
            window.smartsupp||(function(d) {
                var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
                s=d.getElementsByTagName('script')[0];c=d.createElement('script');
                c.type='text/javascript';c.charset='utf-8';c.async=true;
                c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
            })(document);
        </script>
            <!-- Google Analytics -->
        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                    function () {
                        (b[l].q = b[l].q || []).push(arguments)
                    });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-64522723-3', 'auto');
            ga('send', 'pageview');
        </script>
        <script>
            $('select[name="uf_id"]').on('change', function() {
                var estadoId = $(this).val();
                if(estadoId >= 1){
                  setTimeout(function() { 
                    $('#city-franquia').removeAttr('disabled');
                  }, 2000);
                }
                else{
                  console.log('não passou no if');
                }
            });
        </script>
    </body>
 </html>