<!doctype html>
	<head>
		<?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="google-site-verification" content="X0LpIu6h8FhuP9butoCMnmCB5UVGkhSzocPpM4NfFzA" />
    <title>LOGFITNESS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= WEBROOT_URL?>img/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= WEBROOT_URL?>img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta name="keywords" content="academia, academias, plataforma, logfitness, suplementos, atleta, loja virtual, frete free, frete grátis, log, startup, whey, massa, musculação, netshoes">
    <meta name="robots" content="index, follow">

    <meta name="google-site-verification" content="HKMEC3k_UJlkbbtA2qYXhh_kyo55m9jvUgFQ4jDJzUY" />
    <link rel="alternate" hreflang="pt-br" href="https://www.logfitness.com.br">

    <meta property='og:title' content='LOGFITNESS'/>
    	<?php if($produto) { ?>
        <?php $fotos = unserialize($produto->fotos); ?>
        	<meta property='og:description' content='O aluno da academia, realiza a compra do suplemento, efetua o pagamento online e recebe o produto na própria academia, com frete grátis.'/>
        	<meta property='og:image' content='https://www.logfitness.com.br/img/produtos/md-<?= $fotos[0] ?>'/>
    	<?php } else { ?>
        	<meta property='og:description' content='Conheça nossa loja de suplementos! Compre no site e retire aqui na academia.'/>
        	<meta property='og:image' content='https://www.logfitness.com.br/img/logo_face.jpg'/>
    	<?php } ?>
    	<meta property='og:url' content='<?= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>'/>
    	<meta property='og:type' content='website'/>
    	<meta property='og:site_name' content='LOGFITNESS'/>
    	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

    	<!-- CSS -->
    	<?= $this->Html->css('bootstrap') ?>
    	<?= $this->Html->css('bootstrap') ?>
    	<?= $this->Html->css('font-awesome.min') ?>

    	<!-- JS -->
    	<?= $this->Html->script('jquery/jquery.js') ?>
    	<?= $this->Html->script('bootstrap.min') ?>

		

    	<?= $this->fetch('meta') ?>
    	<?= $this->fetch('css') ?>
    	<?= $this->fetch('script') ?>
	</head>
	<body>
		
	</body>


</html>