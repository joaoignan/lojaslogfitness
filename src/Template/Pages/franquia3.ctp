<!DOCTYPE html>
<html lang="pt">

 <head>

     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta name="description" content="Conheça nossa loja de suplementos! Compre no site e retire aqui na academia.">
     <meta name="author" content="LogFitness">

     <title>Logfitness - A franquia de suplementos do futuro.</title>

     <link rel="apple-touch-icon" sizes="57x57" href="<?= WEBROOT_URL?>img/favicon/apple-icon-57x57.png">
     <link rel="apple-touch-icon" sizes="60x60" href="<?= WEBROOT_URL?>img/favicon/apple-icon-60x60.png">
     <link rel="apple-touch-icon" sizes="72x72" href="<?= WEBROOT_URL?>img/favicon/apple-icon-72x72.png">
     <link rel="apple-touch-icon" sizes="76x76" href="<?= WEBROOT_URL?>img/favicon/apple-icon-76x76.png">
     <link rel="apple-touch-icon" sizes="114x114" href="<?= WEBROOT_URL?>img/favicon/apple-icon-114x114.png">
     <link rel="apple-touch-icon" sizes="120x120" href="<?= WEBROOT_URL?>img/favicon/apple-icon-120x120.png">
     <link rel="apple-touch-icon" sizes="144x144" href="<?= WEBROOT_URL?>img/favicon/apple-icon-144x144.png">
     <link rel="apple-touch-icon" sizes="152x152" href="<?= WEBROOT_URL?>img/favicon/apple-icon-152x152.png">
     <link rel="apple-touch-icon" sizes="180x180" href="<?= WEBROOT_URL?>img/favicon/apple-icon-180x180.png">
     <link rel="icon" type="image/png" sizes="192x192"  href="<?= WEBROOT_URL?>img/favicon/android-icon-192x192.png">
     <link rel="icon" type="image/png" sizes="32x32" href="<?= WEBROOT_URL?>img/favicon/favicon-32x32.png">
     <link rel="icon" type="image/png" sizes="96x96" href="<?= WEBROOT_URL?>img/favicon/favicon-96x96.png">
     <link rel="icon" type="image/png" sizes="16x16" href="<?= WEBROOT_URL?>img/favicon/favicon-16x16.png">
     <link rel="manifest" href="<?= WEBROOT_URL?>img/favicon/manifest.json">

     <meta name="msapplication-TileColor" content="#ffffff">
     <meta name="msapplication-TileImage" content="<?= WEBROOT_URL?>img/favicon/ms-icon-144x144.png">
     <meta name="theme-color" content="#ffffff">

     <meta name="keywords" content="academia, academias, plataforma, logfitness, suplementos, atleta, loja virtual, frete free, frete grátis, log, startup, whey, massa, musculação, franquia, franquia de suplementos, baixo investimento, retorno, investimento">
     <meta name="robots" content="index, follow">

     <meta name="google-site-verification" content="HKMEC3k_UJlkbbtA2qYXhh_kyo55m9jvUgFQ4jDJzUY" />
     <link rel="alternate" hreflang="pt-br" href="https://www.logfitness.com.br">

     <meta property='og:title' content='LOGFITNESS'/>
    
     <meta property='og:description' content='Conheça nossa loja de suplementos! Compre no site e retire aqui na academia.'/>
     <meta property='og:image' content='https://www.logfitness.com.br/img/logo_face.jpg'/>

     <meta property='og:url' content='<?= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>'/>
     <meta property='og:type' content='website'/>
     <meta property='og:site_name' content='LOGFITNESS'/>

     <!-- Custom Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
     <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
     <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
     <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
     <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
     
     <?= $this->Html->script('jquery-2.2.3.min') ?>
 
     <!-- Theme CSS -->
     <?= $this->Html->css('bootstrap.min') ?>
     <?= $this->Html->css('agency.min') ?>
     <?= $this->Html->css('font-awesome.min') ?>

     <!-- Google Analytics -->
     <script>
        (function (b, o, i, l, e, r) {
            b.GoogleAnalyticsObject = l;
            b[l] || (b[l] =
                function () {
                    (b[l].q = b[l].q || []).push(arguments)
                });
            b[l].l = +new Date;
            e = o.createElement(i);
            r = o.getElementsByTagName(i)[0];
            e.src = '//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e, r)
        }(window, document, 'script', 'ga'));
        ga('create', 'UA-64522723-3', 'auto');
        ga('send', 'pageview');
     </script>
     <!-- End Google Analytics -->
</head>

 <body id="page-top" class="index">
     <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
         <div class="container">
             <!-- Brand and toggle get grouped for better mobile display -->
             <div class="navbar-header page-scroll">
                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                     <span class="sr-only"></span> Menu <i class="fa fa-bars"></i>
                 </button>
                 <a class="navbar-brand page-scroll" href="#page-top"><img src="img/logfitness.png" style="max-width: 180px;"></a>
             </div>
             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                 <ul class="nav navbar-nav navbar-right">
                     <li class="hidden">
                         <a href="#page-top"></a>
                     </li>
                     <li>
                         <a class="page-scroll" href="#atualmente">Atualmente</a>
                     </li>
                     <li>
                         <a class="page-scroll" href="#futuro">O futuro</a>
                     </li>
                     <li>
                         <a class="page-scroll" href="#franqueado">O franqueado</a>
                     </li>
                     <li>
                         <a class="page-scroll" href="#valores">Valores</a>
                     </li>
                     <li>
                         <a class="page-scroll" href="#sucesso">O sucesso</a>
                     </li>
                 </ul>
             </div>
         </div>
     </nav>

     <?= $this->Flash->render() ?>

     <script type="text/javascript">
         $(document).ready(function () {
             $('.button-abrir-form').click(function() {
                $('.form').fadeToggle();
             });
             $('.fechar-form').click(function() {
                $('.form').fadeOut();
             });
         });
     </script>

     <button class="button-abrir-form">Quero saber mais!</button>
     <div class="form col-lg-3 col-md-3 col-sm-6 col-xs-10 fundomedio">
         <?= $this->Form->create(null, ['id' => 'form']) ?>
             <fieldset>
                 <div class="col-xs-12">
                    <div class="row">
                        <h3 class="col-xs-10 texto-form"><span>Preencha rapidamente</span><br />e receba nossa apresentação.</h3>
                        <i class="fa fa-close text-right col-xs-2 fechar-form" class="desktop-hide"></i>
                    </div>
                 </div>
                 <?= $this->Form->input('nome', [
                    'div' => false,
                    'label' => 'Nome:',
                    'size' => 25,
                    'placeholder' => 'Nome',
                    'maxlength' => 255,
                    'class' => 'form-control required',
                    'required' => true
                 ]) ?>                       
                 <div id="success" style="padding-top: 10px;"></div>
                 <?= $this->Form->input('email', [
                    'div' => false,
                    'label' => 'E-mail:',
                    'size' => 25,
                    'placeholder' => 'E-mail',
                    'maxlength' => 255,
                    'class' => 'form-control required',
                    'required' => true
                 ]) ?>
                 <div id="success" style="padding-top: 10px;"></div>
                 <?= $this->Form->input('telefone', [
                    'div' => false,
                    'label' => 'Telefone:',
                    'size' => 25,
                    'placeholder' => 'Telefone',
                    'maxlength' => 15,
                    'class' => 'form-control required phone-mask',
                    'required' => true
                 ]) ?>
                 <div id="success" style="padding-top: 10px;"></div>
                 <?= $this->Form->button('Quero mais informações!', [
                    'type' => 'submit',
                    'class' => 'btn btn-loco'
                 ]) ?>
                 <div id="success" style="padding-top: 10px;"></div>
            </fieldset>
         <?= $this->Form->end() ?>
     </div>
     <header>
         <div class="container">
             <div class="intro-text">
             </div>
         </div>
         <?= $this->Html->image('franquia3/divider.jpg', ['class' => 'img-responsive','alt' => 'Academia'])?>
     </header>
     <section id="atualmente">
         <div class="container">
             <div class="row">
                 <div class="col-lg-8 col-sm-12">
                     <h2 class="section-heading">Como uma pessoa compra suplemento alimentar hoje?</h2>
                 </div>
             </div>
             <div class="col-md-8 col-sm-12">
                 <div class="row text-center">
                     <div class="col-md-4 text-center">
                         <?= $this->Html->image('franquia3/dumbbell.png', ['class' => '','alt' => 'Academia'])?>
                         <h4 class="service-heading">Academia</h4>
                         <p class="text-muted">As pessoas que compram suplemento estão na academia (público alvo).</p>
                     </div>
                     <div class="col-md-4">
                         <?= $this->Html->image('franquia3/exercise.png', ['class' => '','alt' => 'Professor'])?>
                         <h4 class="service-heading">Professor</h4>
                         <p class="text-muted">Professor indica o produto e talvez alguma loja parceira</p>
                     </div>
                     <div class="col-md-4">
                         <?= $this->Html->image('franquia3/question.png', ['class' => '','alt' => 'Nunca sabemos'])?>
                         <h4 class="service-heading">Nunca se sabe</h4>
                         <p class="text-muted">O professor nunca sabe se o aluno comprou certo e não ganha nada com isso</p>
                     </div>
                     <?= $this->Html->image('franquia3/desorganizado.png', ['class' => '','alt' => 'Tudo muito desorganizado'])?>
                 </div>
             </div>
         </div>
     </section>
     <section id="futuro" class="bg-light-gray">
         <div class="container">
             <div class="row">
                 <div class="col-lg-8 col-sm-12">
                     <h2 class="section-orcamento">Como será o futuro?</h2>
                 </div>
             </div>
             <div class="col-md-8 col-sm-12">
                 <div class="row">
                     <div class="col-md-4 text-center azul">
                         <?= $this->Html->image('franquia3/online-shop.png', ['class' => '','alt' => 'Perfil na Log'])?>
                         <h4 class="service-heading">Perfil na LOG</h4>
                         <p>Academia tem um perfil dentro do site da <span>LOGFITNESS</span>, tipo facebook. A academia usa nosso estoque de produtos e sistema.</p>
                     </div>
                     <div class="col-md-4 text-center cinza">
                         <?= $this->Html->image('franquia3/coin.png', ['class' => '','alt' => 'Comissões'])?>
                         <h4 class="service-heading">Comissões</h4>
                         <p>Academia e professor recebem comissão todo mês via <a href="http://blog.logfitness.com.br/conheca-o-logcard/" target= "_blank">LOGcard</a> ou depósito em conta pelas vendas realizadas na sua loja.</p>
                     </div>
                     <div class="col-md-4 text-center amarelo" style="height: 357px;">
                         <?= $this->Html->image('franquia3/light-bulb.png', ['class' => '','alt' => 'Inovador'])?>
                         <h4 class="service-heading">Inovador</h4>
                         <p>Um negócio organizado, transparente e inovador. Com a Log você verá que é possível lucrar muito na área fitness!</p>
                     </div>
                 </div>
                 <div class="padding"></div>
             </div>    
         </div>
     </section>
     <section id="franqueado">
         <div class="container retorno">
             <div class="row">
                 <div class="col-md-8 col-sm-12">
                     <div class="row">
                        <h2 class="section-orcamento">Qual o papel do franqueado?</h2>
                         <div class="col-md-12">
                             <ul>
                                 <li> <?= $this->Html->image('franquia3/tick.png', ['class' => '','alt' => 'Cadastre o máximo de academias em sua cidade'])?>Cadastrar o máximo de academias em sua cidade</li>
                                 <li> <?= $this->Html->image('franquia3/tick.png', ['class' => '','alt' => 'Explique como funciona'])?>Explicar como funciona nosso sistema</li>
                                 <li> <?= $this->Html->image('franquia3/tick.png', ['class' => '','alt' => 'Ajude e motive'])?>Ajudar e motivar essas academias e professores a venderem e indicarem para seus alunos</li>
                             </ul>
                             <h2 class="">E o retorno?</h2>
                             <div class="row">
                                 <div class="col-md-6 col-xs-12">
                                     <div class="boxblue">
                                         <p>Em média <strong>UMA</strong> academia vendendo para seus alunos e com assistência da franquia gera em média <strong>R$400,00</strong> de comissão para o franqueado por mês.</p>
                                     </div>
                                 </div>
                                 <div class="col-md-6 col-xs-12">
                                     <div class="boxblue">
                                         <h2>Imagine quantas academias você consegue atender na sua região?</h2>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </section>
     <section  id="valores" class="orcamento">
         <div class="container">
             <div class="row">
                 <div class="col-md-8 col-sm-12">
                     <div class="col-md-6">
                         <h2 class="section-orcamento">Quanto <br>custa?</h2>
                     </div>
                     <div class="col-md-5">
                         <p><span>Nem academia nem o franqueado precisam fazer estoque. <br>Sistema todo on-line e utilizando nosso estoque centralizado.</span></p>
                     </div>
                 </div>
                 <div class="col-md-8">
                     <div class="col-md-6">
                         <div class="precinho">
                             <h2 class="">Para o  <br>franqueado</h2>
                             <span>A partir de</span>
                             <h3>R$ 1.000,00</h3>
                         </div>
                     </div>
                     <div class="col-md-6 academia-professor-gratis">
                         <div class="precinho">
                             <h2 class="">Academia<br> e professor</h2>
                             <span>Totalmente</span>
                             <h3>GRÁTIS</h3>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </section>
     <section id="sucesso" class="bg-light-gray text-center-mobile">
         <div class="container">
             <div class="row">
                 <div class="col-md-8">
                     <div class="row">
                         <div class="col-md-6 col-xs-12">
                             <div class="col-md-6 col-xs-12" style="padding: 5px; float: right;">
                                 <?= $this->Html->image('franquia3/passo1.png', ['class' => '','alt' => 'Passos para o sucesso'])?>
                             </div>
                             <div class="col-md-6 col-xs-12 text-left text-center-mobile">
                                 <div class="timeline-panel">
                                     <div class="timeline-heading">
                                         <h4>Passo 1</h4>
                                         <h4 class="seunegocio"><span>Cadastro ao lado e contato do nosso time para explicação</span></h4>
                                     </div>
                                     <div class="timeline-body">
                                         <p>Preencha o formulário ao lado e receba o contato do nossa equipe de expansão.</p>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <div class="col-md-6 col-xs-12">
                             <div class="col-md-6 col-xs-12" style="padding: 5px">
                                 <?= $this->Html->image('franquia3/passo2.png', ['class' => '','alt' => 'Fechamento da franquia'])?>
                             </div>
                             <div class="col-md-6 col-xs-12 text-right passo2-texto text-center-mobile">
                                 <div class="timeline-panel">
                                     <div class="timeline-heading">
                                         <h4>Passo 2</h4>
                                         <h4 class="seunegocio"><span>Fechamento da Franquia</span></h4>
                                     </div>
                                     <div class="timeline-body">
                                         <p>Agora que você já conheceu nosso modelo de negócio é hora de fechar com a gente!</p>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-md-6 col-xs-12">
                             <div class="col-md-6 col-xs-12" style="padding: 5px; float: right;">
                                 <?= $this->Html->image('franquia3/3.png', ['class' => '','alt' => 'Treinamento do franqueado'])?>
                             </div>
                             <div class="col-md-6 col-xs-12 text-left text-center-mobile">
                                 <div class="timeline-panel">
                                     <div class="timeline-heading">
                                         <h4>Passo 3</h4>
                                         <h4 class="seunegocio"><span>Treinamento</span></h4>
                                     </div>
                                     <div class="timeline-body">
                                         <p>Chegou o momento de receber todo o nosso Know How e começar a vender muito em parceria com as academias da sua cidade!</p>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <div class="col-md-6 col-xs-12" style="padding: 5px">
                             <?= $this->Html->image('franquia3/sucesso.png', ['class' => 'img-responsive','alt' => 'Agora você tem um negócio de sucesso'])?>
                         </div>
                     </div>    
                 </div>
             </div>
         </div>
     </section>

     <?= $this->Html->script('bootstrap.min') ?>
     <?= $this->Html->script('agency.min') ?>
     <?= $this->Html->script('jquery.mask.min') ?>

     <script type="text/javascript">
         var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

        if($('.phone-mask').length > 0){
            $('.phone-mask').mask(SPMaskBehavior, spOptions);
        }
     </script>

    <!-- Plugin JavaScript -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" integrity="sha384-mE6eXfrb8jxl0rzJDBRanYqgBxtJ6Unn4/1F7q4xRRyIw7Vdg9jP4ycT7x1iVsgb" crossorigin="anonymous"></script>

     <!-- Smart Look -->
     <script type="text/javascript">
        window.smartlook||(function(d) {
        var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
        var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
        c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
        })(document);
        smartlook('init', '65dd1b31ade411a0f1f09a9524c2adf7c3ca5eb5');
     </script>
     <!-- End Smart Look -->

     <!-- Smartsupp -->
     <script type="text/javascript">
        var _smartsupp = _smartsupp || {};
        _smartsupp.key = '472d82fddbe4326ca5661de701a829a7553ce341';
        window.smartsupp||(function(d) {
            var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
            s=d.getElementsByTagName('script')[0];c=d.createElement('script');
            c.type='text/javascript';c.charset='utf-8';c.async=true;
            c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
        })(document);
     </script>
     <!-- End Smartsupp -->
</body>

</html>
