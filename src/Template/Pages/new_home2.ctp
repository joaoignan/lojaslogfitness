<div id="parallaxBar5" class="parallaxBar">
    <img src="<?= IMG_URL; ?>first-img2.jpg" data-speed="1" class="parallax-img">
    <div class="container">
        <div class="col-md-12">
            <div class="col-md-6 margin-top-380"></div>
            <div class="col-md-6 margin-top-380">
                <a href="/">
                    <img src="<?= IMG_URL; ?>LogFitness2.png" alt="LogFitness" class="logo-topo">
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="col-md-12">
        <div class="col-md-1 margin-top-150"></div>
        <div class="col-md-3 margin-top-150">
            <p class="p-sistema">Sistema</p>
            <p class="p-inteligente">Inteligente</p>
        </div>
        <div class="col-md-1 margin-top-150"></div>
        <div class="col-md-7 margin-top-bottom-100"">
        <iframe width="100%" height="315" src="https://www.youtube.com/embed/Nr7UNxpzO18" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="col-md-1"></div>
</div>
</div>

<div class="div-praquem">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <div class="col-md-2 col-xs-2 div-praquem-margin-top-100"></div>
            <div class="col-md-8 col-xs-8 div-praquem-margin-top-100" style="border-top: 1px solid #FFd41c;"></div>
            <div class="col-md-2 col-xs-2 div-praquem-margin-top-100"></div>
            <div class="col-md-2 col-xs-2 div-praquem-margin-top-10"></div>
            <div class="col-md-8 col-xs-8 div-praquem-margin-top-10" style="color: #FFd41c; border-bottom: 1px solid #FFd41c;">
                <p class="div-praquem-p1">Pra quem é?</p>
                <p class="div-praquem-p2">Academias / Crossfit / Pilates / Lutas / Acessorias Esportivas</p>
            </div>
            <div class="col-md-2 col-xs-2 div-praquem-margin-top-10"></div>
            <div class="col-md-12 col-xs-12 btns-hide">
                <div class="col-md-2 col-xs-2 margin-top-50"></div>
                <div class="col-md-4 col-xs-4 margin-top-50">
                    <?= $this->Form->button('Cadastre-se',

                        [
                            'id'        => 'cadastrar-academia-home',
                            'class'     => 'btn-conheca margin-left-50',
                            'escape'    => false,

                        ]
                    ) ?>
                </div>
                <div class="col-md-4 col-xs-4 margin-top-50">
                    <?= $this->Form->button('Tirar dúvidas',
                        [
                            'class'     => 'btn-conheca margin-left-50 contato-home',
                            'escape'    => false,
                        ]
                    ) ?>
                </div>
                <div class="col-md-2 col-xs-2 margin-top-50"></div>
            </div>
            <div class="col-xs-12 btns-show">
                <div class="col-xs-12 margin-top-50 text-center">
                    <?= $this->Form->button('Cadastre-se',

                        [
                            'id'        => 'cadastrar-academia-home-mobile',
                            'class'     => 'btn-conheca margin-left-50',
                            'escape'    => false,

                        ]
                    ) ?>
                </div>
                <div class="col-xs-12 margin-top-50 text-center">
                    <?= $this->Form->button('Tirar dúvidas',
                        [
                            'class'     => 'btn-conheca margin-left-50 contato-home',
                            'escape'    => false,
                        ]
                    ) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container info-central-box form-toggle" id="form-toggle">

    <?= $this->Form->create($academia, [
        'id'        => 'form-cadastrar-academia',
        'role'      => 'form',
        'default'   => false,
        //'type'      => 'file'
    ]) ?>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('name', [
                'id'            => 'academia_name',
                'div'           => false,
                'label'         => 'Razão Social*',
                'placeholder'   => 'Razão Social',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('shortname', [
                'div'           => false,
                'maxlength'     => 22,
                'label'         => 'Nome Curto*',
                'placeholder'   => 'Nome Fantasia / Nome de Fachada / Marca Empresarial / Nome popular da empresa',
                'class'         => 'validate[required,maxSize[22]] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3 col-xs-5">
            <?= $this->Form->input('slug', [
                'value'         => 'logfitness.com.br/',
                'div'           => false,
                'disabled'      => true,
                'readonly'      => true,
                'label'         => 'URL Personalizada*',
                'placeholder'   => 'nome-personalizado-da-sua-academia',
                'class'         => 'slug-academia-texto form-control text-right',
            ]) ?>
        </div>
        <div class="col-lg-5 col-xs-7">
            <?= $this->Form->input('slug', [
                'div'           => false,
                'label'         => '.',
                'placeholder'   => 'nome-personalizado-da-sua-academia',
                'class'         => 'validate[required,custom[slug]]] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-4">
            <?= $this->Form->input('cnpj', [
                'div'           => false,
                'label'         => 'CNPJ*',
                'placeholder'   => 'CNPJ',
                'class'         => 'validate[required,custom[cnpj]] form-control cnpj',
            ]) ?>
        </div>

        <div class="col-lg-4">
            <?= $this->Form->input('ie', [
                'div'           => false,
                'label'         => 'Inscrição Estadual',
                'placeholder'   => 'Inscrição Estadual',
                'class'         => 'validate[optional] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('contact', [
                'div'           => false,
                'label'         => 'Responsável*',
                'placeholder'   => 'Nome do Responsável',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-2">
            <?= $this->Form->input('phone', [
                'div'           => false,
                'label'         => 'Telefone*',
                'placeholder'   => 'Telefone',
                'class'         => 'validate[required] form-control phone-mask',
            ]) ?>
        </div>

        <div class="col-lg-2">
            <?= $this->Form->input('mobile', [
                'div'           => false,
                'label'         => 'Celular/WhatsApp',
                'placeholder'   => 'Celular/WhatsApp',
                'class'         => 'validate[optional] form-control phone-mask',
            ]) ?>
        </div>

        <div class="col-lg-3">
            <?= $this->Form->input('alunos', [
                'div'           => false,
                'type'          => 'number',
                'label'         => 'Quantidade de alunos*',
                'placeholder'   => 'Quantidade de alunos',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <hr>
    <div class="form-group row">
        <div class="col-lg-12">

            <h4 class="font-bold"><i class="fa fa-soccer-ball-o"></i> Informe quais modalidades a academia oferece</h4>
        </div>
        <div class="col-lg-12">
            <?php
            foreach($esportes_list as $ke => $esporte):

                echo '<div class="col-lg-4 font-bold"><label for="esporte_'.$ke.'">';
                echo $this->Form->checkbox('esportes.'.$ke, [
                    'id'            => 'esporte_'.$ke,
                    'hiddenField'   => 'N',
                    'value'         => 'Y',
                    'label'         => false,
                    'class'         => 'custom-checkbox'
                ]);
                echo ' '.$esporte;
                echo '</<label></div>';

            endforeach;
            ?>
        </div>
    </div>

    <hr>
    <div class="form-group row">
        <div class="col-lg-5">
            <?= $this->Form->input('email', [
                'div'           => false,
                'label'         => 'E-mail*',
                'placeholder'   => 'E-mail',
                'class'         => 'validate[required,custom[email]] form-control',
            ]) ?>
        </div>

        <div class="col-lg-3">
            <?= $this->Form->input('password', [
                'div'           => false,
                'label'         => 'Senha*',
                'placeholder'   => 'Senha para acesso',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <hr>
    <div class="form-group row">
        <div class="col-lg-2">
            <?= $this->Form->input('cep', [
                'div'           => false,
                'label'         => 'CEP*',
                'placeholder'   => 'CEP',
                'class'         => 'validate[required] form-control cep',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <?= $this->Form->input('address', [
                'div'           => false,
                'label'         => 'Endereço*',
                'placeholder'   => 'Endereço',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>

        <div class="col-lg-2">
            <?= $this->Form->input('number', [
                'div'           => false,
                'label'         => 'Número*',
                'placeholder'   => 'Número',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('complement', [
                'div'           => false,
                'label'         => 'Complemento',
                'placeholder'   => 'Complemento',
                'class'         => 'validate[optional] form-control',
            ]) ?>
        </div>
        <div class="col-lg-5">
            <?= $this->Form->input('area', [
                'div'           => false,
                'label'         => 'Bairro*',
                'placeholder'   => 'Bairro',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('uf_id', [
                'id'            => 'states',
                'div'           => false,
                'label'         => 'Estado*',
                'options'       => $states,
                'empty'         => 'Selecione um estado',
                'placeholder'   => 'Estado',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>

        <div class="col-lg-5">
            <?= $this->Form->input('city_id', [
                'id'            => 'city',
                'div'           => false,
                'label'         => 'Cidade*',
                'empty'         => 'Selecione uma cidade',
                'placeholder'   => 'Cidade',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <hr>
    <!--
    <div class="form-group row">
        <div class="col-lg-5">
            <?= $this->Form->input('imagem', [
        'id'            => 'image_upload',
        'type'          => 'file',
        'div'           => false,
        'label'         => 'Logotipo',
        'placeholder'   => 'Logotipo',
        'class'         => 'validate[custom[validateMIME[image/jpeg|image/png]]] form-control',
    ]) ?>
        </div>

        <div class="col-lg-3">
            <?= $this->Html->image('default.png', [
        'id'    => 'image_preview',
        'alt'   => 'Image Upload',
    ])?>
        </div>
    </div>
    -->

    <hr>
    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Html->link('Leia os Termos',
                '/files'.DS.'Termos-de-Aceite.pdf',
            ['target' => '_blank'])
            ;?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-3">
            <label for="aceite" class="">&nbsp&nbsp&nbsp&nbspLi e aceito os termos de aceite
                <?= $this->Form->input('label', [
                    'type' => 'checkbox',
                    'label' => false,
                    'id' => 'aceite',
                    'class' => 'text-center checkbox-aval validate[required]',
                    'style' => 'margin: -27px 0'
                ])?></label>

        </div>
    </div>
    <div class="form-group row">

        <div class="col-lg-2">
            <?= $this->Form->button('Enviar Cadastro', [
                'id'            => 'submit-cadastrar-academia',
                'type'          => 'button',
                'div'           => false,
                'class'         => 'btn btn-default float-right',
            ]) ?>
        </div>

        <div class="col-lg-5" id="form-toggle-message-academia"></div>
    </div>


    <?= $this->Form->end() ?>
</div>

<div class="container info-central-box form-toggle" id="form-toggle-contato">

    <!--<div class="col-lg-4 form-toggle-info">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam beatae culpa cum dolorum eos error incidunt inventore, natus odio, optio porro praesentium quae quis velit. Iusto magnam sint velit.
        <br>
        <br>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam beatae culpa cum dolorum eos error incidunt inventore, natus odio, optio porro praesentium quae quis velit. Iusto magnam sint velit.
    </div>-->

    <?= $this->Form->create($relacionamento_contato, [
        'id'        => 'form-central-relacionamento',
        'role'      => 'form',
        'default'   => false,
    ]) ?>

    <div class="form-group row">
        <div class="div-indiquee col-md-8">
            <?= $this->Form->input('name', [
                //'name'          => 'nome',
                'id'            => 'contato-name',
                'div'           => false,
                'label'         => 'Nome',
                'placeholder'   => 'Nome Completo',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('phone', [
                'id'            => 'contato-phone',
                'div'           => false,
                'label'         => 'Telefone',
                'placeholder'   => 'Telefone',
                'class'         => 'validate[required] form-control phone-mask',
            ]) ?>
        </div>

        <div class="col-lg-5">
            <?= $this->Form->input('email', [
                'id'            => 'contato-email',
                'div'           => false,
                'label'         => 'Email',
                'placeholder'   => 'Email',
                'class'         => 'validate[required,custom[email]] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('description', [
                'id'            => 'contato-description',
                'div'           => false,
                'label'         => 'Assunto',
                'placeholder'   => 'Assunto / Descrição',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('message', [
                'id'            => 'contato-name',
                'type'          => 'textarea',
                'div'           => false,
                'label'         => 'Mensagem',
                'placeholder'   => 'Mensagem',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">

        <div class="col-lg-2">
            <?= $this->Form->button('Enviar Contato', [
                'id'            => 'submit-cadastrar-central-relacionamento',
                'type'          => 'button',
                'div'           => false,
                'class'         => 'btn btn-default float-right',
            ]) ?>

        </div>

        <div class="col-lg-5" id="form-toggle-message"></div>
    </div>

    <?= $this->Form->end() ?>
</div>

<div id="parallaxBar6" class="parallaxBar">
    <img src="<?= IMG_URL; ?>second-img2.jpg" data-speed="1" class="parallax-img">
    <div class="container">
        <div class="col-md-12">
            <p class="text-center">Tornar as pessoas melhores através<br> do esporte, com motivação e saúde.</p>
        </div>
    </div>
</div>

<div class="div-footer">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <div class="col-md-9 col-xs-12 text-center div-footer-div">
                 Copyright ©<?= date("Y") ?> <span>www.logfitness.com.br</span>. TODOS OS DIREITOS RESERVADOS.
                Todo o conteúdo do site, todas as fotos, imagens, logotipos, marcas, dizeres, som, software, conjunto imagem,
                layout, trade dress, aqui veiculados são de propriedade exclusiva da LogFitness. É vedada qualquer reprodução,
                total ou parcial, de qualquer elemento de identidade, sem expressa autorização. A violação de qualquer direito mencionado
                implicará na responsabilização cível e criminal nos termos da Lei.

                CACIQUE DIGITAL REPRESENTACOES LTDA - ME - CNPJ: 22.921.802/0001-77 Rua Antonio Munia, 141, Sala 01 - Jardim Nazareth - CEP 15054-160 - São José do Rio Preto - SP   -   
                <a href="#politica-de-privacidade" class="fancybox" style="color: #555">Política de Privacidade</a>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="col-md-12 text-center">
                    <?= $this->Form->button('Contato',
                        ['class' => 'btn-conheca a-contato contato-home',
                            'escape' => false]
                    ) ?>
                </div>
                <div class="col-md-12 text-right">
                    <a href="www.m4web.com.br"><img src="<?= IMG_URL; ?>desenvolvido-agencia-m4web-preto-colorido.png" alt="M4Web" class="logo-m4"></a>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->Element('politica_de_privacidade', ['hide' => true]) ?>