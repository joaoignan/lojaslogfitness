<style>
    .btn-compre{
        background: #8dc73f;
    }
    .btn-compre svg{
        fill: white;
        width: 25px;
        height: auto;
    }

    .produto-acabou-btn{
        background-color: #5089cf;
    }
    .produto-acabou-btn .fa{
        margin-top: -3px;
        color: #fff;
    }
    .btn-box{
        width: 49%;
        padding:5px;
    }
    .btn-box span{
        color: #fff;
        font-size: 12px;
        margin-top: 3px;
    }
    .btn-box img{
        width: 18px;
        margin-right: 5px;
        margin-top: -5px;
    }
    .box-botoes{
        display: inline-block;
        width: 100%;
        padding-top: 5px;
        float: right;
    }
    .div-sabor{
        height: 23px;
        padding: 0;
    }
    .prdt{
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        margin-bottom: 30px;
        padding: 5px 10px;
        min-height: 380px;
    }
    .link-prdt{
        height: 72px;
    }
    #pagar-celular{
        display: none;
    }
    .promocao-index, #grade-produtos {
        padding-right: 230px;
        padding-left: 80px;
        margin-left: 0;
        margin-right: 0;
        width: 100%;
    }
    .product {
        font-size: 14px;
    }
    .produto-item .bg-white-produto {
        min-height: 314px;
        border: 2px solid rgb(234, 234, 234);
        box-shadow: 0px 1px 7px 0px rgba(200,200,200,0.75);
    }
    .bg-gray-produto {
        bottom: 0;
        position: absolute;
        min-height: 92px;
        max-width: 185px;
    }
    .description {
        width: 100%;
        height: 40px;
        overflow: hidden;
        margin: 0 0 5px 0;
        font-size: 14px;
        text-transform: uppercase;
        font-weight: 700;
    }
    @media all and (max-width: 768px) {
        .produto-item .bg-white-produto {
            min-height: 350px;
        }
    }
    @media all and (min-width: 769px) {
        .box-close-filter { 
            width: 0; 
        }
        .filtro { 
            width: 230px; 
            opacity: 1; 
        }
        .box-filtro { 
            left: 0; 
        }
    }
    .produto-grade {
        max-width: 145px;
        max-height: 150px;
        margin-top: 0;
        display: flex;
        align-items: center;
    }
    .box-imagem-produto {
        width: 100%;
        height: 150px;
        margin-top: -15px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .box-info-produto {
        width: 100%;
        height: 200px;
    }
    .box-fundo-produto {
        width: 100%;
        padding: 0;
    }
    .box-preco {
        height: 70px;
        width: 100%;
        float: left;
        display: flex;
        flex-flow: column;
        justify-content: center;
    }
    .box-mochila-add {
        height: 45px;
        width: 45px;
        float: right;
        display: flex;
        align-items: flex-end;
        margin-top: 10px;
    }
    .box-mochila-add svg {
        width: 100%;
        height: 100%;
        cursor: pointer;
        background: #8dc73f;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
        fill: white;
        margin-right: 3px;
    }
    .box-mochila-avise {
        height: 45px;
        width: 45px;
        float: right;
        margin-top: 10px;
        background-color: #5089cf;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
    }
    .box-mochila-avise > .fa{
        color: #FFF;
        font-size: 2.5em;
        margin-bottom: 2px;
        margin-top: 3px;
    }
    .categoria{
        height: 30px;
    }
    .categoria-div{
        position: absolute;
        right: 5px;
        top: 0px;
        width: 20px;
        height: 30px;
    }
    .preco {
        margin: 0;
        font-size: 16px;
    }
    .preco-desc {
        font-size: 16px;
    }
    .preco-normal {
        font-weight: 700;
        font-size: 20px;
        color: #5089cf;
    }
    .bg-white-produto a {
        min-height: 0;
    }
    .box-promocao {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: black;
        background: #f3d012;
    }
    .box-promocao:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-novidade {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: white;
        background: #5089cf;
    }
    .box-novidade:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-ultimo {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: white;
        background: red;
    }
    .box-ultimo:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-mochila-add svg:hover, .box-mochila-add svg:focus, .box-mochila-add svg:link:hover, .box-mochila-add svg:link:focus, .box-mochila-add svg:visited:hover, .box-mochila-add svg:visited:focus, .box-mochila-avise:hover, .btn-box:hover {
      -webkit-transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      -webkit-animation: diff 0.5s 1;
      animation: diff 0.5s 1;
    }

    @-webkit-keyframes diff {
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
    }
    @keyframes diff {
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
    }

    .icone-objetivo {
        position: absolute;
        left: 5px;
        bottom: 120px;
        width: 30px;
        height: 30px;
        color: #5089cf;
    }

    .box-imagem-produto svg {
        position: absolute;
        left: 5px;
        bottom: 130px;
        width: 20px;
        height: 30px;
    }

    .sabor-info {
        font-size: 12px;
        color: #333;
        text-align: center;
        overflow: hidden;
    }

    @media all and (min-width: 1650px) {
        .produto-item {
            width: 16.66666667%;
        }
    }
    @media all and (max-width: 1240px) {
        .produto-item {
            width: 33.3333333334%;
        }
    }
    @media all and (max-width: 1200px) and (min-width: 780px) {
        .form-busca {
            width: 80% !important;
        }
    }
    @media all and (max-width: 1050px) {
        .produto-item {
            width: 50%;
        }
    }
    @media all and (max-width: 850px) {
        .produto-item {
            width: 100%;
        }
    }
    @media all and (max-width: 768px) {
        #pagar-celular{
            display: block;
            margin-bottom: 7px;
        }
        #pagar-celular .btn-login-nav{
            font-size: 18px;
            padding: 6px;
        }
        .promocao-index, #grade-produtos {
            padding-left: 0;
            padding-right: 0;
        }
        .promocao-index {
            padding-top: 90px;
        }
        .produto-item {
            width: 50%;
        }
    }
    @media all and (max-width: 400px) {
        .produto-grade {
            max-width: 120px;
            max-height: 120px;
        }
        .produto-item {
            padding-right: 0px;
            padding-left: 0px;
        }
    }
    .loader {
        position: fixed;
        left: 0;
        top: 0%;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: white;
    }
    .content-loader{
        position: fixed;
        border-radius: 25px;
        left: 20%;
        top: 25%;
        width: 60%;
        height: 50%;
        z-index: 999999;
        background-color: white;
        text-align: center;
    }
    .content-loader img{
        width: 500px;
        margin-top: 45px;
    }
    .content-loader i{
        color: #f4d637;
    }
    .content-loader p{
        color: #5087c7;
        font-family: nexa_boldregular;
        font-size: 34px;
    }
    @media all and (max-width: 430px) {
        .content-loader{
            display: none;
        }
    }
    @media all and (max-width: 768px) {
        .loader {
            display: none;
        }
    }
    .dropdown a{
        color: #fff;
        font-weight: 400;
    }
    .nav .open>a, .nav .open>a:hover, .nav .open>a:focus, .nav>li>a:focus{
        background-color: rgb(66, 118, 181);
        border-color: rgb(66, 118, 181);
    }
    #dropdown-filtro ul{
        padding: 0;
    }
    #dropdown-filtro li{
        padding: 0;
    }
    .filtros-ativos{
        height: auto;
        margin-bottom: 15px;
        padding-bottom: 15px;
    }
    .filtros-ativos span{
        min-width: 100px;
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        background-color: rgba(255,255,255,1.0);
        border-radius: 5px;
        padding: 5px 10px;
        margin: 5px;
        display: inline-block;
        height: 30px;
    }
    .filtros-ativos .fa{
        color: rgb(255, 0, 0);
    }
    .filtros-ativos .fa:hover{
        color: rgb(244, 219, 27);
        cursor: pointer;
    }
    .filtros-ativos i{
        float: right;
        margin-top: 2px;
    }
    .subfiltros{
        margin-bottom: 15px;
        padding: 5px 0;
    }
    .subfiltros select{
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        background-color: rgba(255,255,255,1.0);
        border-radius: 5px;
        padding: 5px 10px;
        margin: 5px 3px;
        border: none;
        display: inline-block;
    }
    .owl-theme .owl-controls .owl-buttons div {
        color: #6f6f6f!important;
        background-color: transparent!important;
        font-size: 30px!important;
    }
    .owl-prev {
        width: 15px;
        height: 100px;
        position: absolute;
        top: 40%;
        left: 0;
        display: block!important;
        border:0px solid black;
    }

    .owl-next {
        width: 15px;
        height: 100px;
        position: absolute;
        top: 40%;
        right: -10px;
        display: block!important;
        border:0px solid black;
    }
    .logo-marcas {
        -webkit-filter: grayscale(100%);
        max-width: 80%;
        max-height: 90px;
    }
    .logo-marcas:hover {
        -webkit-filter: grayscale(0);
    }
    .faixa-combos {
        height: 474px;
        position: absolute;
        background-color: #61c513;
        left: 0;
        margin-left: -300px;
        width: 150%;
    }
    .alert {
        position: fixed;
        width: 100%;
        top: 90px;
        z-index: 1;
    }
</style>

<script>
    $(document).ready(function() {
        $('.marcas-link, .objetivo-link').click(function() {
            var container = $('.container-grade');
            var posicao_pagina = $(document).scrollTop();
            var posicao_container = (container.offset().top + 310);
            if(posicao_pagina < posicao_container) {
                if($(document).width() > 992) {
                    $('html, body').animate({
                        scrollTop: posicao_container
                    }, 700);
                }
                $('body').addClass('total-block');
            }
        });
    });
</script>

<?php if (!$this->request->is('mobile')) { ?>
    <?php if ($tipo_banner == 0) { ?>
        <?= $this->Element('banners_modelo1_home') ?>
    <?php } else if($tipo_banner == 1) {?>
        <?= $this->Element('banners_modelo2_home') ?>
    <?php } ?>
<?php } ?>

<style>
    @media all and (max-width: 500px){
        .total-produto-box-mobile{
            width: 265px;
            position: relative;
            height: 150px;
            margin: 0 auto;
            box-shadow: 0px 0px 5px #545454
        }
        .codigo-imagem-box-mobile, .informacoes-box-mobile{
            display: inline-block;
            position: relative;
            float: left;
            height: 150px;
            padding: 5px;
        }
        .codigo-imagem-box-mobile{
            width: 100px;
            font-size: 12px;
        }
        .codigo-imagem-box-mobile img{
            max-height: 100px;
            max-width: 100px;
        }
        .informacoes-box-mobile{
            width: 160px;
        }
        .informacoes-box-mobile p{
            margin-bottom: 2px;
        }
        .informacoes-box-mobile .marca, .informacoes-box-mobile .treinos{
            font-size: 10px;
        }
        .informacoes-box-mobile .parcelas{
            font-size: 12px;
        }
        .informacoes-box-mobile .marca{
            text-transform: uppercase;
        }
        .opcoes-box-mobile{
            position: absolute;
            box-shadow: inset 2px 0px 5px 0px #a7a7a7;
            top: 0;
            right: 0;
            bottom: 0;
            width: 1px;
            z-index: 3;
            background-color: rgb(245, 245, 245);
            transition: all 1s;
        }
        .aberta{
            width: 100%!important;
        }
        .span_objetivo{
            display: inline-block;
            padding: 0px 5px;
            font-size: 10px;
            margin: 0 3px 0 0;
            color: #fff;
            height: 16px;
        }
        .nome_box_mobile{
            width: 100%;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }
        .propriedade_box_mobile{
            text-transform: capitalize;
            width: 100%;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }
        .total-tarjas{
            top: 123px;
        }
        .button-abrir-opcoes{
            width: 25px;
            float: left;
            padding: 65px 8px;
        }
        .total-abrir-opcoes{
            float: left;
            width: 100%;
            display: none;
            padding: 10px;
            color: #fff;
        }
        .linha1{
            margin-bottom: 15px;
            width: 100%;
            margin: 15px 0;
        }
        .linha2{
            width: 100%;
            margin: 0 0 15px 0;
        }
        .btn-verde-box-mobile{
            background-color: rgba(72, 159, 54, 1);
            color: #fff;
        }
        .btn-verde-box-mobile:hover, .btn-verde-box-mobile:active, .btn-verde-box-mobile:focus {
            background-color: rgba(55, 119, 42, 1);
            color: #fff;
        }
        .objetivos_box_mobile{
            height: 20px;
        }
        .fa-minus-circle{
            display: none;
        }
        .active .fa-plus-circle{
            display: none!important
        }
        .active .fa-minus-circle{
            display: block!important;
        }
    }
</style>

<script>
    $(document).ready(function() {
        $('.total-produto-box-mobile').click(function (e) {
            if (!$(this).find('.opcoes-box-mobile').hasClass('active')) {
                var id = $(this).find('.opcoes-box-mobile').attr('data-id');
                $('.opcoes-box-mobile').removeClass('active');
                $('.opcoes-box-mobile').css("width", "1px");
                $(this).find('.opcoes-box-mobile').css("width", "100%");
                $(this).find('.opcoes-box-mobile').css("background-color", "rgba(56, 56, 56, 0.85);");
                $(this).find('.opcoes-box-mobile').addClass('active');
                $('.total-abrir-opcoes').fadeOut(100);
                $('.total-abrir-opcoes-'+id).fadeIn(2000);
                console.log('abriu');
            }
            else{
                $('.total-abrir-opcoes').fadeOut();
                $(this).find('.opcoes-box-mobile').css("width", "1px");
                $(this).find('.opcoes-box-mobile').css("background-color", "rgba(245, 245, 245, 1);");
                $(this).find('.opcoes-box-mobile').removeClass('active');
                console.log('fechou');
            }
        });
    });
</script>

<div class="container container-grade" data-section="home">
    <div class="col-sm-4 col-md-2 col-lg-2 filtro-div">
        <?= $this->Element('filtro'); ?>
    </div>
    <div class="col-sm-12 col-md-10 col-lg-10 produtos-div">
        <div class="produtos-grade">
            <?= $this->Element('carrousel_produtos_home', [
                'produtos' => $produtos_promocao,
                'qtd_produtos' => $qtd_produtos_promocao,
                'title' => 'Promoções',
                'link_produtos' => '/promocoes'
            ]); ?>

            <?= $this->Element('carrousel_produtos_home', [
                'produtos' => $produtos_massa,
                'qtd_produtos' => $qtd_produtos_massa,
                'title' => 'Massa Muscular',
                'link_produtos' => '/pesquisa/0/0/0/objetivo/massa-muscular'
            ]); ?>

            <?= $this->Element('carrousel_produtos_home', [
                'produtos' => $produtos_emagrecimento,
                'qtd_produtos' => $qtd_produtos_emagrecimento,
                'title' => 'Emagrecimento e Definição',
                'link_produtos' => '/pesquisa/0/0/0/objetivo/emagrecimento-e-definicao'
            ]); ?>

            <?= $this->Element('carrousel_produtos_home', [
                'produtos' => $produtos_combos,
                'qtd_produtos' => $qtd_produtos_combos,
                'title' => 'Combos Promocionais',
                'link_produtos' => '/pesquisa/combo'
            ]); ?>

            <?= $this->Element('carrousel_produtos_home', [
                'produtos' => $produtos_energia,
                'qtd_produtos' => $qtd_produtos_energia,
                'title' => 'Energia e Resistência',
                'link_produtos' => '/pesquisa/0/0/objetivo/energia-e-resistencia'
            ]); ?>

            <?= $this->Element('carrousel_produtos_home', [
                'produtos' => $produtos_saude,
                'qtd_produtos' => $qtd_produtos_saude,
                'title' => 'Saúde e Bem Estar',
                'link_produtos' => '/pesquisa/0/0/objetivo/saude-e-bem-estar'
            ]); ?>

            <div class="col-xs-12" style="margin-bottom: 15px; margin-top: 15px">
                <h3 class="col-xs-6" style="margin: 0">Marcas</h3>
            </div>
            <div class="owl-carousel-marcas">
                <?php foreach ($produtos_marcas_filtro as $marca) {
                    $marcas_rand['marcas'][] = $marca;
                } ?>
                <?php shuffle($marcas_rand['marcas']) ?>
                <?php foreach($marcas_rand['marcas'] as $marca_rand) { ?>
                    <div class="item">
                        <div class="col-xs-12 text-center">
                            <?= $this->Html->link(
                                $this->Html->image($marca_rand->image,
                                    ['class' => 'logo-marcas', 'alt' => $marca_rand->name]),
                                $SSlug.'/pesquisa/0/'.$marca_rand->slug,
                                ['escape' => false]
                            ); ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script>
    $('.abrir-email').click(function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('.enviar-whats').slideUp("slow");
        $('.enviar-email').slideToggle("slow");
    });
    $('.abrir-whats').click(function(e){
        var id = $(this).attr('data-id');
        $('.enviar-email').slideUp("slow");
        $('.enviar-whats').slideToggle("slow");
    });
</script>