<style>
	body{
		overflow-x: auto;
		width: auto;
	}
	.total-breadcrumb{
		display: none!important;
	}
</style>

<?php 
	$tem_cor = 0;
	$tem_sabor = 0;

	foreach($produtos_comparador as $produto_comparador) {
		$foto = unserialize($produto_comparador->fotos);

		$valor = $produto_comparador->preco_promo ? $produto_comparador->preco_promo * $desconto_geral : $produto_comparador->preco * $desconto_geral;

		$infos[$produto_comparador->id] = [
			'id' => $produto_comparador->id,
			'status' => $produto_comparador->status_id,
			'slug' => $produto_comparador->slug
		];

		$fotos[$produto_comparador->id] = $foto[0];
		$nomes[$produto_comparador->id] = $produto_comparador->produto_base->name;

		$produto_marcas[$produto_comparador->id] = $produto_comparador->produto_base->marca->name;

		if($produto_comparador->produto_base->propriedade->id == 1) {
			$cores[$produto_comparador->id] = $produto_comparador->propriedade;
			$sabores[$produto_comparador->id] = ' - ';

			$tem_cor = 1;
		} else if($produto_comparador->produto_base->propriedade->id == 2) {
			$sabores[$produto_comparador->id] = $produto_comparador->propriedade;
			$cores[$produto_comparador->id] = ' - ';

			$tem_sabor = 1;
		}

		$pesos[$produto_comparador->id] = strtolower($produto_comparador->tamanho.' '.$produto_comparador->unidade_medida);
		$preços[$produto_comparador->id] = number_format($valor, 2, ',','.');

		if($produto_comparador->doses) {
			$treinos[$produto_comparador->id] = $produto_comparador->doses;
			$valor_doses[$produto_comparador->id] = 'R$ '.number_format(($valor / $produto_comparador->doses), 2, ',','.');
		} else {
			$treinos[$produto_comparador->id] = ' - ';
			$valor_doses[$produto_comparador->id] = ' - ';
		}

		foreach ($produto_comparador->produto_substancias as $produto_substancia) {

			if($produto_substancia->substancia->tipo == 'tabela nutricional') {

				if(!in_array($produto_substancia->substancia->name, $todas_substancias_tabela)) {
					$todas_substancias_tabela[$produto_substancia->substancia_id] = $produto_substancia->substancia->name;
				}

			} else if($produto_substancia->substancia->tipo == 'aminograma') {

				if(!in_array($produto_substancia->substancia->name, $todas_substancias_tabela)) {
					$todas_substancias_aminograma[$produto_substancia->substancia_id] = $produto_substancia->substancia->name;
				}

			} else if($produto_substancia->substancia->tipo == 'vitaminas e minerais') {

				if(!in_array($produto_substancia->substancia->name, $todas_substancias_tabela)) {
					$todas_substancias_vitaminas[$produto_substancia->substancia_id] = $produto_substancia->substancia->name;
				}

			}

		}

		$ingredientes[$produto_comparador->id] = '<li></li>';

		$count = 0;
		foreach ($produto_comparador->produto_ingredientes as $produto_ingrediente) {
			$ingredientes_ordem[$produto_ingrediente->ordem] = $produto_ingrediente->ingrediente->name;
			$count++;
		}

		for($i = 1; $i <= $count; $i++) {
			$ingredientes[$produto_comparador->id] = $ingredientes[$produto_comparador->id].'<li>'.$ingredientes_ordem[$i].'</li>';
		}

		$ingredientes_ordem = [];
	?>

	<?php }

	if($todas_substancias_tabela[3]) {
		$valor_energetico = $todas_substancias_tabela[3];
		unset($todas_substancias_tabela[3]);
		sort($todas_substancias_tabela);
		sort($todas_substancias_aminograma);
		sort($todas_substancias_vitaminas);
		array_unshift($todas_substancias_tabela, $valor_energetico); 
	} else {
		sort($todas_substancias_tabela);
		sort($todas_substancias_aminograma);
		sort($todas_substancias_vitaminas);
	}

	foreach ($todas_substancias_tabela as $toda_substancia_tabela) {
		foreach($produtos_comparador as $produto_comparador) {
			
			$valor = ' - ';
			
			foreach ($produto_comparador->produto_substancias as $produto_substancia) {
				if($produto_substancia->substancia->name == $toda_substancia_tabela) {
					$valor = $produto_substancia->valor.' '.$produto_substancia->substancia->unidade_medida;
				}
			}

			$substancias_tabela[$toda_substancia_tabela][$produto_comparador->id] = $valor;
		}
	}

	foreach ($todas_substancias_aminograma as $toda_substancia_aminograma) {
		foreach($produtos_comparador as $produto_comparador) {
			
			$valor = ' - ';
			
			foreach ($produto_comparador->produto_substancias as $produto_substancia) {
				if($produto_substancia->substancia->name == $toda_substancia_aminograma) {
					$valor = $produto_substancia->valor.' '.$produto_substancia->substancia->unidade_medida;
				}
			}

			$substancias_aminograma[$toda_substancia_aminograma][$produto_comparador->id] = $valor;
		}
	}

	foreach ($todas_substancias_vitaminas as $toda_substancia_vitaminas) {
		foreach($produtos_comparador as $produto_comparador) {
			
			$valor = ' - ';
			
			foreach ($produto_comparador->produto_substancias as $produto_substancia) {
				if($produto_substancia->substancia->name == $toda_substancia_vitaminas) {
					$valor = $produto_substancia->valor.' '.$produto_substancia->substancia->unidade_medida;
				}
			}

			$substancias_vitaminas[$toda_substancia_vitaminas][$produto_comparador->id] = $valor;
		}
	}
?>
<script>
	$(document).ready(function(){
		$(document).scroll(function(){
			var posicaoScroll = $(document).scrollTop();
			var tamanhoTela = screen.width;
			if (tamanhoTela > 500){
				var somaScroll = 90;
			}
			else{
				var somaScroll = 60;
			}
			if (posicaoScroll > 230){
				$('.floatbar').css("top", posicaoScroll+somaScroll);
			}
			else{
				$('.floatbar').animate({'top': '-100px'}, 0);
			}
		});
	});
</script>

<div class="resize-container" >
	<div class="comparador_container text-center">
		
		<div class="">
			<div class="table-main text-center">
				<div class="floatbar">
					<?php foreach($fotos as $key_foto => $foto) { ?>
						<?php foreach ($infos as $key_info => $info) { ?>
							<?php if($key_foto == $key_info) { ?>
								<div class="item-floatbar">
									<span class="pull-right"><a class="btn-exclui-comparador-completo" data-id="<?= $info['id'] ?>"><i class="fa fa-close"></i></a></span>
									<?= $this->Html->link($this->Html->image("produtos/".$foto),
											$SSlug.'/comparador/produto/'.$info['slug'], ['escape' => false]); ?>
								</div>	
							<?php } ?>
						<?php } ?>
					<?php } ?>
					<div class="item-floatbar">
					</div>
				</div>
				<table class="ComparaTable">
					<tr class="imagens-comparador">
						<td class="foto-produto">
						</td>
						<?php foreach($fotos as $key_foto => $foto) { ?>
							<?php foreach ($infos as $key_info => $info) { ?>
								<?php if($key_foto == $key_info) { ?>
									<td>
										<div class="foto-comparador">
										<span class="pull-right"><a class="btn-exclui-comparador-completo" data-id="<?= $info['id'] ?>"><i class="fa fa-close"></i></a></span>
											<?= $this->Html->link($this->Html->image("produtos/".$foto),
											'/comparador/produto/'.$info['slug'], ['escape' => false]); ?>
										</div>
									</td>
								<?php } ?>
							<?php } ?>
						<?php } ?>
						<td class="foto-produto"></td>
					</tr>
					<tr class="nome_produto">
						<td class="titulo">Nome</td>
						
						<?php foreach($nomes as $nome) { ?>
							<td><?= $nome ?></td>
						<?php } ?>
					</tr>
					<tr class="nome_produto">
						<td class="titulo">Marca</td>
						
						<?php foreach($produto_marcas as $marca) { ?>
							<td><?= $marca ?></td>
						<?php } ?>
					</tr>
					<?php if($tem_sabor) { ?>
						<tr>
							<td class="titulo">Sabor</td>
							
							<?php foreach($sabores as $sabor) { ?>
								<td><?= $sabor ?></td>
							<?php } ?>
						</tr>
					<?php } ?>
					<?php if($tem_cor) { ?>
						<tr>
							<td class="titulo">Cor</td>
							
							<?php foreach($cores as $cor) { ?>
								<td><?= $cor ?></td>
							<?php } ?>
						</tr>
					<?php } ?>
					<tr>
						<td class="titulo">Peso</td>
						
						<?php foreach($pesos as $peso) { ?>
							<td><?= $peso ?></td>
						<?php } ?>
					</tr>
					<tr>
						<td class="titulo">Preço</td>
						
						<?php foreach($preços as $preço) { ?>
							<td>R$ <?= $preço ?></td>
						<?php } ?>
					</tr>
					<tr>
						<td class="titulo">Treinos</td>
						
						<?php foreach($treinos as $treino) { ?>
							<td><?= $treino ?></td>
						<?php } ?>
					</tr>
					<tr>
						<td class="titulo">Valor por dose</td>
						
						<?php foreach($valor_doses as $valor_dose) { ?>
							<td style="font-weight: 700;"><?= $valor_dose ?></td>
						<?php } ?>
					</tr>

					<tr>
						<td class="titulo"></td>
						<?php foreach($infos as $info) { ?>
								<td><button class="btn comprar-comparador btn-comprar-overlay">Comprar</button></td>
						<?php } ?>
					</tr>
					<tr>
						<td class="titulo"></td>
						<?php foreach($infos as $info) { ?>
							<td>
								<button class="btn indica-btn btn-indique-comparador" data-id="<?= $info['id'] ?>">Indicar</button>
								<div id="overlay-comparador-indicacao-<?= $info['id'] ?>" class="overlay overlay-indicacao">
									<div class="overlay-content text-center">
					                    <a href="javascript:void(0)" class="closebtn">&times;</a>
					                    <h4>Lembrou de alguém quando viu isso?</h4>
						                    <h4>Indique este produto!</h4>
						                    <br>
						                    <div class="row">
				                                <div class="col-xs-12 share-whastapp share-buttons">
				                                    <a href="whatsapp://send?text=Isso é a sua cara! <?= WEBROOT_URL.$SSlug.'/produto/'.$info['id'] ?>">
				                                        <button class="btn button-blue"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</button>
				                                    </a>
				                                </div>
				                                <div class="col-xs-12 share-whastapp share-buttons">
									                <button class="btn button-blue" id="share-<?= $info['id'] ?>"><i class="fa fa-comment-o" aria-hidden="true"></i> Messenger</button>
									                <script>
									                    $(document).ready(function() {
									                        $('#share-<?= $info['id'] ?>').click(function(){
									                            var link = "<?= WEBROOT_URL.$SSlug.'/comparador/produto/'.$info['id'] ?>";
									                            var app_id = '1321574044525013';
									                            window.open('fb-messenger://share?link=' + link + '&app_id=' + app_id);
									                        });
									                    });
									                </script>
									            </div>
									            <div class="tablet-hidden col-xs-12 col-md-3 share-buttons">
			                                        <button data-id="whatsweb-<?= $info['id'] ?>" class="btn button-blue abrir-whats"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp Web</button>
			                                    </div>
				                                <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
				                                    <a href="javascript: void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u=<?= WEBROOT_URL.$SSlug.'/compradador/produto/'.$info['slug'] ?>','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');">
				                                        <button class="btn button-blue fb-share-button"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</button>
				                                    </a>
				                                </div>
				                                <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
				                                    <button class="btn button-blue" id="copiar-comparador-clipboard-<?= $info['id'] ?>"><i class="fa fa-clone" aria-hidden="true"></i> Copiar link</button>
				                                </div>
				                                <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
			                                        <button  class="btn button-blue abrir-email"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail</button>
			                                    </div>
				                            </div>
				                            <div class="row">
				                            	<div id="collapse-whats-<?=$info['id'] ?>" class="tablet-hidden enviar-whats">
			                                        <div class="col-xs-4 text-center form-group">
			                                            <?= $this->Form->input('whats-web-form', [
			                                                'div'           => false,
			                                                'id'            => 'nro-whatsweb-'.$info['id'],
			                                                'label'         => false,
			                                                'class'         => 'form-control form-indicar',
			                                                'placeholder'   => "Celular*: (00)00000-0000"
			                                            ])?>
			                                        </div>
			                                        <div class="col-xs-4 text-left obs-overlay">
			                                            <p>*Para enviar pra mais de um contato deixe este campo vazio.</p>
			                                        </div>
			                                        <div class="col-xs-4 text-center form-group">
			                                            <?= $this->Form->button('Enviar', [
			                                            'type'          => 'button',
			                                            'data-id'       => $info['id'],
			                                            'class'         => 'btn button-blue',
			                                            'id'            => 'enviar-whatsweb-'.$info['id']
			                                            ])?>
			                                        </div>
			                                        <script>
			                                            $(document).ready(function() {
			                                                $('#enviar-whatsweb-<?=$info['id'] ?>').click(function(){

			                                                    var numero;
			                                                    var link = "<?= WEBROOT_URL.$SSlug.'/comparador/produto/'.$produto->slug ?>";

			                                                    if($('#nro-whatsweb-<?= $info['id'] ?>').val() != '') {
			                                                        numero = '55'+$('#nro-whatsweb-<?= $info['id'] ?>').val();
			                                                    }
			                                                    
			                                                    window.open('https://api.whatsapp.com/send?phone=' + numero + '&text=Isso é a sua cara! ' + link);
			                                                });
			                                            });
			                                        </script>
			                                    </div>
				                                <div class="col-xs-12 enviar-email" id="collapse-email-<?= $info['id'] ?>">
				                                    <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$info['id']])?>
								                    <?= $this->Form->input('name', [
								                        'div'           => false,
								                        'label'         => false,
								                        'class'         => 'form-control validate[optional] form-indicar name-comparador-'.$info['id'],
								                        'placeholder'   => 'Quem indicou?',
								                    ])?>
								                    <br>
								                    <?= $this->Form->input('email', [
								                        'div'           => false,
								                        'label'         => false,
								                        'class'         => 'form-control validate[required] form-indicar email-comparador-'.$info['id'],
								                        'placeholder'   => 'E-mail do destinatário',
								                    ])?>
								                    <?= $this->Form->input('id_produto', [
								                        'div'           => false,
								                        'class'         => 'id_produto-'.$info['id'],
								                        'value'         => $info['id'],
								                        'label'         => false,
								                        'type'          => 'hidden'
								                    ])?>
								                    <br>
								                    <?= $this->Form->button('Enviar', [
								                        'type'          => 'button',
								                        'data-id'       => $info['id'],
								                        'class'         => 'btn  button-blue send-indicacao-home-btn'
								                    ])?>
								                    <br>
								                    <br>
								                    <?= $this->Form->end()?>
				                                </div>
				                            </div>
				                            <div class="row"> 
				                                <div class="col-xs-12 clipboard">   
				                                    <?= $this->Form->input('url', [
				                                        'div'           => false,
				                                        'id'            => 'url-comparador-clipboard-'.$info['id'],
				                                        'label'         => false,
				                                        'value'         => WEBROOT_URL.$SSlug.'/produto/'.$info['slug'],
				                                        'readonly'      => true,
				                                        'class'         => 'form-control form-indicar'
				                                    ])?>
				                                </div>  
				                                <p><span class="copy-comparador-alert-<?=$info['id'] ?> hide" id="copy-alert">Copiado!</span></p>
				                                <script>
				                                    $(document).ready(function() {
				                                        // Copy to clipboard
				                                        document.querySelector("#copiar-comparador-clipboard-<?=$info['id'] ?>").onclick = () => {
				                                          // Select the content
				                                            document.querySelector("#url-comparador-clipboard-<?=$info['id'] ?>").select();
				                                            // Copy to the clipboard
				                                            document.execCommand('copy');
				                                            // Exibe span de sucesso
				                                            $("#copy-alert").removeClass("hide");
				                                            // Oculta span de sucesso
				                                            setTimeout(function() {
				                                                $("#copy-alert").addClass("hide");
				                                            }, 1500);
				                                        };
				                                    });
				                                </script>
				                            </div>
					                </div>
								</div>
							</td>
						<?php } ?>
					</tr>

					<?php if(count($todas_substancias_tabela) >= 1) { ?>
						<tr>
							<td class="titulo">
								
							</td>
							<td colspan="200" class="separador-colunas">
								<h4>Tabela Nutricional</h4>
							</td>
						</tr>
						<?php foreach($todas_substancias_tabela as $toda_substancia_tabela) { ?>
							<tr>
								<td class="titulo"><?= $toda_substancia_tabela ?></td>
								<?php foreach ($substancias_tabela[$toda_substancia_tabela] as $substancia_tabela) { ?>
									<td><?= $substancia_tabela ?></td>
								<?php } ?>
							</tr>
						<?php } ?>
					<?php } ?>

					<?php if(count($todas_substancias_aminograma) >= 1) { ?>
						<tr>
							<td class="titulo">
								
							</td>
							<td colspan="200" class="separador-colunas">
								<h4>Aminograma</h4>
							</td>
						</tr>
						<?php foreach($todas_substancias_aminograma as $toda_substancia_aminograma) { ?>
							<tr>
								<td class="titulo"><?= $toda_substancia_aminograma ?></td>
								<?php foreach ($substancias_aminograma[$toda_substancia_aminograma] as $substancia_aminograma) { ?>
									<td><?= $substancia_aminograma ?></td>
								<?php } ?>
							</tr>
						<?php } ?>
					<?php } ?>

					<?php if(count($todas_substancias_vitaminas) >= 1) { ?>
						<tr>
							<td class="titulo">
								
							</td>
							<td colspan="200" class="separador-colunas">
								<h4>Vitaminas e Minerais</h4>
							</td>
						</tr>
						<?php foreach($todas_substancias_vitaminas as $toda_substancia_vitaminas) { ?>
							<tr>
								<td class="titulo"><?= $toda_substancia_vitaminas ?></td>
								<?php foreach ($substancias_vitaminas[$toda_substancia_vitaminas] as $substancia_vitaminas) { ?>
									<td><?= $substancia_vitaminas ?></td>
								<?php } ?>
							</tr>
						<?php } ?>
					<?php } ?>
					<tr>
						<td class="titulo">
							
						</td>
						<td colspan="200" class="separador-colunas">
							<h4>Ingredientes</h4>
							<p>O <strong>primeiro ingrediente</strong> é o que tem <strong>maior quantidade</strong>. Ordem decrescente.</p>
						</td>
					</tr>
					<tr>
						<td class="titulo">
							
						</td>
						<?php foreach($ingredientes as $ingrediente) { ?>
							<td>
								<ul>
									<?= $ingrediente ?>
								</ul>
							</td>
						<?php } ?>
					</tr>

					<!-- FIXO NO FIM DA TABELA -->
					<tr>
						<td class="titulo"></td>
						<?php foreach($treinos as $treino) { ?>
							<td><button class="btn comprar-comparador btn-comprar-overlay">Comprar</button></td>
						<?php } ?>
					</tr>
					<tr>
						<td class="titulo"></td>
						<?php foreach($produtos_comparador as $produto_comparador) { ?>
							<td><button class="btn indica-btn btn-indique" data-id="<?= $produto_comparador->id ?>">Indicar</button></td>
						<?php } ?>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
    	//remover produto do comparador
		$('.btn-exclui-comparador-completo').on('click', function(e) {
	        e.preventDefault();
	        loading.show();

	        var produto_id = $(this).attr('data-id');

	        var url_atual = window.location.href;

	        url_atual = url_atual.split(produto_id)[0]+url_atual.split(produto_id)[1];

	        $.get(WEBROOT_URL + 'remover-produto-comparador/' + produto_id,
	            function (data) {
	                location.href = url_atual;
	                loading.hide();
	            });
	    });

        $('.btn-indique').click(function () {
                var id_produto = $(this).attr('data-id');
                $('#overlay-indicacao-'+id_produto).height('100%');
                $("html,body").css({"overflow":"hidden"});
        });
        function closeIndica() {
            $('.overlay-indicacao').height('0%');
            $("html,body").css({"overflow":"auto"});
        }

        $(document).on('click', '.dropbtn', function() {
            if($('.button-view-filtro').hasClass('filtro-aberto') && !$(this).hasClass('dropdown-active')) {
                $('.button-view-filtro').removeClass('filtro-aberto');
                
                $('.total-filtro').animate({'left' : '-600px'}, 300);
                $('#filtro-mobile-btn').animate({'right' : '10px'});
            }
        });

        $('.btn-indique-comparador').click(function(e){
			e.preventDefault();
			var id = $(this).attr('data-id');
			$('#overlay-comparador-indicacao-'+id).height('100%');
		});
		$('.closebtn').click(function(){
			console.log('click');
			$('.overlay').height('0%');
		});
		$('.send-indicacao-comparador-btn').click(function(e) {
            e.preventDefault();
            var botao = $(this);
        
            botao.html('Enviando...');
            botao.attr('disabled', 'disabled');

            var id = $(this).attr('data-id');

            var valid = $('.indicar_prod_home_'+id).validationEngine("validate");
            if (valid == true) {
                $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + 'produtos/overlay-indicar-produtos-home/',
                    data: {
                        name: $('.name-comparador-'+id).val(),
                        email: $('.email-comparador-'+id).val(), 
                        produto_id: $('.id_produto-'+id).val()
                    }
                })
                .done(function (data) {
                    if(data == 1) {
                        botao.html('Enviado!');
                        setTimeout(function () {
                            botao.html('Enviar');
                            botao.removeAttr('disabled');
                        }, 2200);
                    } else {
                        botao.html('Falha ao enviar... Tente novamente...');
                        setTimeout(function () {
                            botao.html('Enviar');
                            botao.removeAttr('disabled');
                        }, 2200);
                    }
                });
            }else{
                $('.indicar_prod_home_'+id).validationEngine({
                    updatePromptsPosition: true,
                    promptPosition: 'inline',
                    scroll: false
                });
            }
        });
    });
</script>
<script>
    $('.abrir-email').click(function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('.enviar-whats').slideUp("slow");
        $('.enviar-email').slideToggle("slow");
    });
    $('.abrir-whats').click(function(e){
        var id = $(this).attr('data-id');
        $('.enviar-email').slideUp("slow");
        $('.enviar-whats').slideToggle("slow");
    });
</script>