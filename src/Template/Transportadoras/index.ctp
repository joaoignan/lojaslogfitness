<div class="container informacoes text-center">
    <div class="col-lg-6 transportadoras transportadoras2">
        <?= $this->Html->image('transportadoras.png', ['alt' => 'Transportadoras'])?>
        <br class="clear">
        <div class="absolut-center-100">
            <span class="titulo">ATLETAS</span>
        </div>
    </div>
    <div class="col-lg-6 transportadoras transportadoras2">
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consequuntur delectus, ducimus illo, iste nam nemo nostrum qui quod repellendus ullam voluptatem. Accusamus architecto earum libero nesciunt ratione sed vero.
        </p>
        <div class="absolut-center-100">
            <a id="form-toggle-open">CADASTRE AQUI</a>
        </div>
    </div>
</div>

<div class="container info-central-box form-toggle" id="form-toggle">
<!--
    <div class="col-lg-4 form-toggle-info">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam beatae culpa cum dolorum eos error incidunt inventore, natus odio, optio porro praesentium quae quis velit. Iusto magnam sint velit.
        <br>
        <br>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam beatae culpa cum dolorum eos error incidunt inventore, natus odio, optio porro praesentium quae quis velit. Iusto magnam sint velit.
    </div>-->

    <?= $this->Form->create(null, [
        'id'        => 'form-cadastrar-transportadora',
        'role'      => 'form',
        'default'   => false,
        'type'      => 'file'
    ]) ?>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('name', [
                'div'           => false,
                //'label'         => true,
                'placeholder'   => 'Nome da Transportadora / Razão Social',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-4">
            <?= $this->Form->input('cnpj', [
                'div'           => false,
                'label'         => 'CNPJ',
                'placeholder'   => 'CNPJ',
                'class'         => 'validate[required] form-control cnpj',
            ]) ?>
        </div>

        <div class="col-lg-4">
            <?= $this->Form->input('ie', [
                'div'           => false,
                'label'         => 'Inscrição Estadual',
                'placeholder'   => 'Inscrição Estadual',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('phone', [
                'div'           => false,
                'label'         => 'Telefone',
                'placeholder'   => 'Telefone',
                'class'         => 'validate[required] form-control phone-mask',
            ]) ?>
        </div>

        <div class="col-lg-5">
            <?= $this->Form->input('email', [
                'div'           => false,
                'label'         => 'Email',
                'placeholder'   => 'Email',
                'class'         => 'validate[required,custom[email]] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-2">
            <?= $this->Form->input('cep', [
                'div'           => false,
                'label'         => 'CEP',
                'placeholder'   => 'CEP',
                'class'         => 'validate[required] form-control cep',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <?= $this->Form->input('address', [
                'div'           => false,
                'label'         => 'Endereço',
                'placeholder'   => 'Endereço',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>

        <div class="col-lg-2">
            <?= $this->Form->input('number', [
                'div'           => false,
                'label'         => 'Número',
                'placeholder'   => 'Número',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('complement', [
                'div'           => false,
                'label'         => 'Complemento',
                'placeholder'   => 'Complemento',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
        <div class="col-lg-5">
            <?= $this->Form->input('area', [
                'div'           => false,
                'label'         => 'Bairro',
                'placeholder'   => 'Bairro',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('uf_id', [
                'id'            => 'states',
                'div'           => false,
                'label'         => 'Estado',
                'options'       => $states,
                'empty'         => 'Selecione um estado',
                'placeholder'   => 'Estado',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>

        <div class="col-lg-5">
            <?= $this->Form->input('city_id', [
                'id'            => 'city',
                'div'           => false,
                'label'         => 'Cidade',
                'empty'         => 'Selecione uma cidade',
                'placeholder'   => 'Cidade',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-5">
            <?= $this->Form->input('imagem', [
                'id'            => 'image_upload',
                'type'          => 'file',
                'div'           => false,
                'label'         => 'Logotipo',
                'placeholder'   => 'Logotipo',
                'class'         => 'validate[required, custom[validateMIME[image/jpeg|image/png]]] form-control',
            ]) ?>
        </div>

        <div class="col-lg-3">
            <?= $this->Html->image('default.png', [
                'id'    => 'image_preview',
                'alt'   => 'Image Upload',
            ])?>
        </div>
    </div>

    <div class="form-group row">

        <div class="col-lg-2">
            <?= $this->Form->button('Enviar Cadastro', [
                'id'            => 'submit-cadastrar-transportadora',
                'type'          => 'button',
                'div'           => false,
                'class'         => 'btn btn-default float-right',
            ]) ?>

        </div>

        <div class="col-lg-5" id="form-toggle-message"></div>
    </div>

    <?= $this->Form->end() ?>
</div>


<div class="container info-central-box">
    <p class="font-12">
            <span>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut dolor ducimus esse fuga harum obcaecati reprehenderit
                similique! Ab adipisci, aliquam aliquid eos fugit illo maxime non nostrum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut dolor ducimus esse fuga harum obcaecati reprehenderit similique! Ab adipisci, aliquam aliquid eos fugit illo maxime non nostrum quaerat sed sint?
            </span>
    </p>
</div>

<!-- INFORMAÇÕES  -->
<div class="container">
    <div>
        <div class="col-md-4 informacoes-blocos seja-um-parceiro">
            <?= $this->Html->link(
                $this->Html->image('parceiro.png', ['alt' => 'Seja um parceiro']),
                '/seja-um-parceiro',
                ['escape' => false]
            )?>
            <p><span class="titulo">SEJA UM PARCEIRO </span></p>
            <p class="info"><span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ad autem commodi consequatur debitis doloremque dolorum, eaque est fuga fugiat fugit inventore laborum, laudantium quaerat quidem quod sed sunt voluptatum. </span>
            </p>

        </div>
    </div>

    <div>
        <div class="col-md-4 informacoes-blocos academias">
            <?= $this->Html->link(
                $this->Html->image('academias.png', ['alt' => 'Academias']),
                '/academias',
                ['escape' => false]
            )?>
            <p><span class="titulo">ACADEMIAS</span></p>
            <p class="info">
                <span>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ad autem commodi consequatur debitis doloremque dolorum, eaque est fuga fugiat fugit inventore laborum, laudantium quaerat quidem quod sed sunt voluptatum.
                </span>
            </p>
        </div>
    </div>
    <div>
        <div class="col-md-4 informacoes-blocos central-relacionamento">
            <?= $this->Html->link(
                $this->Html->image('central-de-relacionamento.png', ['alt' => 'Central de Relacionamento']),
                '/central-de-relacionamento',
                ['escape' => false]
            )?>
            <p><span class="titulo">CENTRAL DE RELACIONAMENTO</span></p>
            <p class="info">
               <span>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ad autem commodi consequatur debitis doloremque dolorum, eaque est fuga fugiat fugit inventore laborum, laudantium quaerat quidem quod sed sunt voluptatum.
               </span>
            </p>
        </div>
    </div>
    <!--FIM CONTAINER SEJA PARCEIRO-->
</div>