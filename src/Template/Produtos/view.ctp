<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>
<?php 
    $fotos = unserialize($produto->fotos); 
?>

<?php
foreach($fotos as $key => $foto):
    $fotos_trust[] = WEBROOT_URL.'img/produtos/md-'.$foto;
endforeach;
?>

<!-- Trustvox -->
<script type="text/javascript">
 window._trustvox =  [];
 _trustvox.push(['_storeId', '71702']);
 _trustvox.push(['_productId',  '<?= $produto->id ?>']);
 _trustvox.push(['_productName', '<?= $produto->produto_base->name ?>']);
 _trustvox.push(['_productPhotos', <?= json_encode($fotos_trust) ?>]);
</script>
<script async="true" type="text/javascript" src="//static.trustvox.com.br/assets/widget.js"></script>
<!-- End Trustvox -->

<!-- Overlay-->
<!-- <script>
function openNav() {
    document.getElementById("myNav").style.height = "100%";
}

function closeNav() {
    document.getElementById("myNav").style.height = "0%";
}
</script> -->
<!-- Overlay-->

<style>
    @-webkit-keyframes diff {
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
    }
    @keyframes diff {
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
    }
    img.comprar-hexag {
        margin-top: 0px;
        width: 25px;
        cursor: pointer;
    }
    .indique-face{
        padding: 0;
    }
    .btn-indique-view{
        float: left;
        background-color: #5089cf;
        color: white;
        font-family: nexa_boldregular;
        width: 50%;
        height: 45px;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
    }
    .btn-indique-view:hover,
    .btn-indique-view:focus,
    .btn-indique-view:visited {
        -webkit-transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
        transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
        -webkit-animation: diff 0.5s 1;
        animation: diff 0.5s 1;
        color: white;
    }
    .desc-produto,
    .combina-cont,
    .prod-fim {
        padding-top: 180px;
    }
    .dicas {
        margin: 0 13px 30px 13px;
    }
    .icone_comprar{
        max-width: 32px;
    }
    @media all and (min-width: 769px) { 
        .box-close-mochila {
            width: 0;
        }
        .mochila {
            width: 230px;
            opacity: 1;
        }
        .fundo-carrinho {
            margin-left: 0;
        }
        .cupom-desconto, 
        .total-compra, 
        .btn-fecha-mochila {
            right: 0px;
        }
        .box-close-filter { 
            width: 0; 
        }
        .filtro { 
            width: 230px; 
            opacity: 1; 
        }
        .box-filtro { 
            left: 0; 
        }
        .info-direita-mochila {
            display: block;
        }
        .produtos-mochila {
            width: 228px;
            opacity: 1;
        }
    }
    .product-view {
        font-size: 23px;
    }
    .product-price {
        font-size: 2.5em;
        color: #5089cf;
        padding-top: 0;
        margin-top: -45px;
    }
    .doses {
        font-weight: 700;
    }
    .logo-marca {
        max-width: 80%;
    }
    .preco-de {
        color: #222;
        font-size: 13px;
    }
    #sabores-link {
        border: 2px solid #5089cf;
        color: #5089cf;
        font-weight: 700;
        float: left;
    }
    #tamanhos-link {
        border: 2px solid #5089cf;
        color: #5089cf;
        font-weight: 700;
        float: left;
    }
    .texto-sabor {
        float: left;
        display: flex;
        align-items: center;
        display: -webkit-flex;
        -webkit-align-items: center;
        height: 25px;
        padding-right: 10px;
        margin-bottom: 0;
    }
    .btn-avise {
        border: 0;
        color: #fff;
        background-color: #5089cf;
        font-size: 30px;
        margin: 10px 0;
        text-transform: lowercase;
        text-align: center;
        padding: 5px 0;
        width: 100%;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
    }
    .btn-avise:hover,
    .btn-avise:focus,
    .btn-avise:visited {
        -webkit-transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
        transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
        -webkit-animation: diff 0.5s 1;
        animation: diff 0.5s 1;
    }

    .img-produto-principal {
        max-width: 100%;
        max-height: 100%;
    }
    .quadro-img-produto-principal {
        height: 380px;
    }
    .miniaturas {
        text-align: left;
    }

    /*combinations*/
    .product {
        font-size: 14px;
    }
    .produto-item .bg-white-produto {
        min-height: 290px;
        border: 2px solid rgb(234, 234, 234);
        box-shadow: 0px 1px 7px 0px rgba(200,200,200,0.75);
    }
    .bg-gray-produto {
        bottom: 0;
        position: absolute;
        min-height: 92px;
        max-width: 185px;
    }
    .description {
        height: 60px;
        width: 100%;
        overflow: hidden;
        text-overflow: ellipsis;
        max-height: 60px;
        margin: 0;
        font-size: 14px;
        text-transform: uppercase;
        font-weight: 700;
    }
    @media all and (min-width: 769px) {
        .box-close-filter { 
            width: 0; 
        }
        .filtro { 
            width: 230px; 
            opacity: 1; 
        }
        .box-filtro { 
            left: 0; 
        }
    }
    .produto-grade {
        max-width: 145px;
        max-height: 150px;
        margin-top: 0;
        display: flex;
        align-items: center;
        display: -webkit-flex;
        -webkit-align-items: center;
    }
    .box-imagem-produto {
        width: 100%;
        height: 150px;
        margin-top: 10px;
        display: flex;
        justify-content: center;
        align-items: center;
        display: -webkit-flex;
        -webkit-justify-content: center;
        -webkit-align-items: center;
    }
    .box-info-produto {
        width: 100%;
        margin-top: 10px;
        height: 120px;
    }
    .box-fundo-produto {
        width: 100%;
        height: 40px;
        margin-top: 10px;
    }
    .box-preco {
        height: 40px;
        width: 70%;
        float: left;
        display: flex;
        flex-flow: column;
        align-items: baseline;
        justify-content: center;
        display: -webkit-flex;
        -webkit-flex-flow: column;
        -webkit-align-items: baseline;
        -webkit-justify-content: center;
        padding-left: 4px;
    }
    .box-mochila-add {
        height: 100%;
        width: 30%;
        float: right;
        display: flex;
        align-items: flex-end;
        display: -webkit-flex;
        -webkit-align-items: flex-end;
    }
    .box-mochila-add svg {
        width: 100%;
        height: 100%;
        cursor: pointer;
        background: #8dc73f;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
        fill: white;
        margin-right: 3px;
    }
    .preco {
        margin: 0;
    }
    .preco-desc {
        font-size: 13px;
    }
    .preco-normal {
        font-weight: 700;
        font-size: 18px;
        color: #5089cf;
    }
    .bg-white-produto a {
        min-height: 0;
    }
    .box-promocao {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: black;
        background: #f3d012;
    }
    .box-promocao:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-novidade {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: white;
        background: #5089cf;
    }
    .box-novidade:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-mochila-add svg:hover, .box-mochila-add svg:focus, .box-mochila-add svg:link:hover, .box-mochila-add svg:link:focus, .box-mochila-add svg:visited:hover, .box-mochila-add svg:visited:focus {
      -webkit-transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      -webkit-animation: diff 0.5s 1;
      animation: diff 0.5s 1;
    }

    @-webkit-keyframes diff {
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
    }
    @keyframes diff {
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
    }

    .icone-objetivo {
        position: absolute;
        left: 5px;
        bottom: 120px;
        width: 30px;
        height: 30px;
        color: #5089cf;
    }

    .box-imagem-produto svg {
        position: absolute;
        left: 5px;
        bottom: 120px;
        width: 20px;
        height: 30px;
    }
    .atributos-produto {
        width: 100%;
    }
    .combina-com {
        margin-bottom: 0;
    }
    .atributos-produto h4 {
        cursor: auto;
        color: #2D76CA;
        font-weight: 500;
    }
    .formula {
        display: block; 
        overflow: hidden;
        justify-content: center;
    }
    .tabela {
        overflow: hidden;
        padding-top: 15px;
        margin: auto;
    }
    .vermais-formula {
        padding-left: 230px;
        padding-right: 230px;
        background: white;
        position: absolute;
        width: 100%;
        text-align: center;
        z-index: 2;
        padding-top: 10px;
    }
    .prod-fim {
        margin-top: 40px;
    }
    .sabor-info {
        font-weight: bold;
        font-size: 13px;
        color: #333;
        text-align: center;
        font-family: nexa_lightregular,"Droid Sans",serif;
    }
    .breadcrumbs {
        width: 60%; 
        height: 50px; 
        padding-left: 20px; 
        padding-top: 10px; 
        text-transform: lowercase; 
        text-decoration: underline;
        float: left;
    }
    .setas {
        text-decoration: none;
        color: #5089cf;
    }
    .codigo {
        width: 40%;
        float: right;
        padding-right: 20px; 
        padding-top: 10px; 
    }
    .codigo p {
        text-align: right;
        font-size: 12px;
    }
    .formula-text {
        padding-left: 230px; 
        padding-right: 230px;
    }
    .imagem-alinhada {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 320px;
    }   
    .total-filtro{
        position: absolute;
        top: 0;
        left: -600px;
    }
    @media all and (max-width: 768px) {
        .desc-produto, 
        .combina-cont, 
        .prod-fim, 
        .formula-text,
        .vermais-formula {
            padding-left: 15px;
            padding-right: 15px;
        }
        .desc-produto {
            padding-top: 90px;
        }
        .quadro-img-produto-principal,
        .quadro-desc-produto {
            width: 100%;
        }
        .quadro-img-produto-principal {
            white-space: normal;
        }
        .produto-item {
            width: 50%;
            padding: 0;
        }
        .produto-grade {
            max-width: 120px;
            max-height: 120px;
        }
        .produto-item .bg-white-produto {
            min-height: 310px;
        }
        .box-imagem-produto svg {
            bottom: 130px;
        }
        .produto-div,
        .vermais-formula,
        .formula-text {
            margin: 0;
        }
        .breadcrumbs {
            width: 100%;
            padding-top: 0;
            float: left;
            z-index: 2;
            position: absolute;
        }
        .box-sabor {
            display: flex;
            justify-content: center;
            display: -webkit-flex;
            -webkit-justify-content: center;
        }
        .product-price {
            margin-top: -85px;
        }
        .recomendar {
            clear: both;
        }
    }
    @media all and (max-width: 500px) {
        .desc-produto {
            padding-top: 70px;
        }
        .breadcrumbs {
            margin-top: -20px;
        }
        .product-price,
        .box-sabor,
        .product-doses,
        .logo-marca-view {
            width: 100%;
            margin-left: 0;
            margin-right: 0;
        }
        .product-price {
            margin-top: 0;
        }
        .box-sabor {
            margin-top: 0;
        }
        .btn-indique-view{
            width: 100%;
        }
    }
    @media all and (min-width: 893px) and (max-width:1023px ){
        .btn-indique-view{
            width: 100%;
        }
    }
    @media all and (max-width: 1024px){
        .preco-por{
            font-size: 23px;
        }
    }
    @media all and (max-width: 450px){
      .academia-sobre .phone {
        width: 100%;
        display: inline-block;
      }
      .editar{
        margin-top: 10px;
      }
      .almoco{
      text-align: left!important;
      }
    }
    @media all and (max-width: 992px){
        #comparar-btn{
            display: none;
        }
    }
    .button-view-filtro {
        display: flex; 
        justify-content: space-between; 
        align-items: center;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        $('.btn-indique').click(function () {
                var id_produto = $(this).attr('data-id');
                $('#overlay-indicacao-'+id_produto).height('100%');
                $("html,body").css({"overflow":"hidden"});
        });
        function closeIndica() {
            $('.overlay-indicacao').height('0%');
            $("html,body").css({"overflow":"auto"});
        }

        $(document).on('click', '.dropbtn', function() {
            if($('.button-view-filtro').hasClass('filtro-aberto') && !$(this).hasClass('dropdown-active')) {
                $('.button-view-filtro').removeClass('filtro-aberto');
                
                $('.total-filtro').animate({'left' : '-600px'}, 300);
                $('#filtro-mobile-btn').animate({'right' : '10px'});
            }
        });
    });
</script>

<div class="container desc-produto" data-id="<?= $produto->slug ?>">

    <style>
        .codigo-marca, .sugerir-compartilhar{
            float: left;
            display: inline-block;
            width: 33%;
            min-height: 25px;
            padding: 5px;
        }
        .foto-infos{
            float: right;
            display: inline-block;
            width: 66%;
            min-height: 60px;
            padding: 5px;
        }
        .foto-produto{
            width: 50%;
            float: left;
        }
        .foto-produto img{
            width: auto;
            max-height: 320px;
        }
        .dropdowns-produto{
            padding: 5px 0;
            float: left;
            width: 50%;
            display: inline-block;
        }
        .info-lateral{
            padding: 5px;
            width: 50%;
            display: inline-block;
        }
        .product-view{
            margin-top: 15px;
        }
        .mini-logo-marga{
            display: none;
        }
        .owl-prev {
        width: 15px;
        height: 100px;
        position: absolute;
        top: 40%;
        left: 0;
        display: block!important;
        border:0px solid black;
        }

        .owl-next {
            width: 15px;
            height: 100px;
            position: absolute;
            top: 40%;
            right: -10px;
            display: block!important;
            border:0px solid black;
        }

        @media all and (max-width: 992px){
            .codigo-marca, .sugerir-compartilhar{
                float: none;
                width: 100%;
            }
            .foto-infos{
                float: none;
                width: 100%;
            }
            .dropdowns-produto{
                float: right;
                width: 100%;
            }
            .foto-produto img{
                max-height: 220px;
                height: auto;
                max-width: 125px;
            }
            .product-view{
                margin: 0px;
            }
            .mini-logo-marga{
                display: block;
                width: 90px;
            }
        }
        @media all and (max-width: 500px) {
            .imagem-alinhada{
                height: 250px;
            }
            .mini-logo-marga{
                width: 50px;
            }
        }
    </style>

    <div class="row">
        <div class="codigo-marca">
            <div class=""> 
                <div class="col-xs-12">
                    <div class="">
                        <div class="col-md-7 text-left">
                            <button class="button button-blue button-view-filtro tablet-hidden" style="width: 150px;">+ Produtos <i class="fa fa-search" aria-hidden="true"></i></button>
                            <?= $this->Element('filtro'); ?>
                        </div>
                        <div class="col-md-5 text-right">
                            <h6>Código: <?= strtoupper($produto->id); ?></h6>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <h1 class="product-view text-uppercase" itemprop="name">
                        <?= $produto->produto_base->name; ?>
                    </h1>
                </div>
                <div class="col-xs-12 text-center tablet-hidden" style="margin-top: 10px">
                    <?= $this->Html->link($this->Html->image($produto->produto_base->marca->image, ['alt' => $produto->produto_base->marca->name, 'class' => 'logo-marca']),
                        $SSlug.'/pesquisa/0/'.$produto->produto_base->marca->slug, ['escape' => false]
                    ); ?>
                </div>
            </div>
        </div>

        <div class="foto-infos">
            <div class="foto-produto imagem-alinhada owl-carousel owl-carousel-produtos-view" itemscope="" itemtype="http://schema.org/Product">               
               <!-- <?= $this->Html->image($fotos[0], [
                    'id'    => 'foto-produto',
                    'val'   => $produto->slug,
                    'alt'   => $produto->produto_base->name.' '.$produto->propriedade,
                    'itemprop'  => 'image'
                ])?>  -->

                <?php foreach($fotos as $foto) { ?>

                <?= $this->Html->image($foto, [
                    'id'    => 'foto-produto',
                    'val'   => $produto->slug,
                    'alt'   => $produto->produto_base->name.' '.$produto->propriedade,
                    'itemprop'  => 'image'
                ])?>

                <?php } ?>
                
            </div>

            
            <div class="dropdowns-produto">
                <script type="text/javascript">
                    $(document).ready(function() {
                        var view_produto = 1;

                        $('.aviseme-button').click(function() {
                            var id = $(this).attr('data-id');

                            $('#aviseMe-'+id).height('100%');
                        });

                        $('#sabores-link').change(function() {
                            endereco_destino = $('.change-propriedade:selected').attr('value');

                            if(endereco_destino != null) {
                                location.href = WEBROOT_URL + endereco_destino;
                            }
                        });

                        $('#tamanhos-link').change(function() {
                            endereco_destino = $('.change-tamanho:selected').attr('value');

                            if(endereco_destino != null) {
                                location.href = WEBROOT_URL + endereco_destino;
                            }
                        });
                    });
                </script>

                <?php $produto->preco_promo ?
                    number_format($preco = $produto->preco_promo * $desconto_geral, 2, ',','.') :
                    number_format($preco = $produto->preco * $desconto_geral, 2, ',','.'); ?>

                <style type="text/css">
                    .select-estilizado {
                        display: inline-block;
                        text-decoration: none;
                        border-radius: 4px;
                        padding: 9px 20px;
                        font-size: 16px;
                        color: rgba(255,255,255,1);
                        display: flex;
                        justify-content: space-between;
                        align-items: center;
                        background-color: #5c9cd8;
                        width: 100%;
                    }

                    .select-estilizado:hover {
                        background-color: #376fa5;
                    }

                    .seta-select {
                        transform: rotate(0deg);
                    }

                    @keyframes spin {
                        from {transform: rotate(0deg)}
                        to   {transform: rotate(180deg)}
                    }

                    @keyframes spin-inverse {
                        from {transform: rotate(180deg)}
                        to   {transform: rotate(0deg)}
                    }

                    .rotate-seta-on {
                        -webkit-animation:spin .5s forwards;
                        -moz-animation:spin .5s forwards;
                        animation:spin .5s forwards;
                        transform: rotate(180deg)!important;
                    }

                    .rotate-seta-off {
                        -webkit-animation:spin-inverse .5s forwards;
                        -moz-animation:spin-inverse .5s forwards;
                        animation:spin-inverse .5s forwards;
                        transform: rotate(0deg)!important;
                    }

                    .options-div-estilizado {
                        position: absolute;
                        display: block;
                        top: 40px;
                        left: 5%;
                        width: 90%;
                        margin: 15px 0;
                        border: 2px solid #5089cf;
                        border-radius: 4px;
                        box-sizing: border-box;
                        box-shadow: 0 2px 1px rgba(0,0,0,.07);
                        background: #fff;
                        z-index: 10;
                        display: none;
                    }

                    .options-div-estilizado::before {
                        position: absolute;
                        display: block;
                        content: '';
                        bottom: 100%;
                        right: 18px;
                        width: 7px;
                        height: 7px;
                        margin-bottom: -3px;
                        border-top: 2px solid #5089cf;
                        border-left: 2px solid #5089cf;
                        background: #fff;
                        transform: rotate(45deg);
                    }

                    .option-estilizado {
                        cursor: pointer;
                        font-weight: 700;
                    }

                    .option-estilizado:hover {
                        background: #5089cf;
                        color: white;
                    }

                    .option-estilizado:hover a {
                        color: white;
                    }

                    .seta-fundo::before {
                        background: #5089cf;
                    }

                    .option-estilizado {
                        line-height: 30px;
                    }

                    .capitalize-text {
                        text-transform: capitalize;
                    }
                    .share-buttons{
                        margin-bottom: 20px;
                    }
                    .share-whastapp{
                        display: none;
                    }
                    .copy-alert{
                        color: #489f36;
                        font-size: 14px;
                        font-weight: 700;
                    }
                    @media all and (max-width: 500px){
                        .share-whastapp{
                            display: block;
                        }
                    }
                    @media all and (max-height: 500px){
                        .share-whastapp{
                            display: block;
                        }
                    }
                </style>

                <script type="text/javascript">
                    $(document).ready(function() {
                        $('.select-estilizado').click(function(e) {
                            var id = $(this).attr('data-id');
                            var seta = $(this).find('.seta-select');
                            var div_options = $('.options-div-estilizado-'+id);

                            $('.seta-select').not(seta).removeClass('rotate-seta-on').addClass('rotate-seta-off');
                            $('.options-div-estilizado').not(div_options).fadeOut();

                            if(seta.hasClass('rotate-seta-off')) {
                                seta.removeClass('rotate-seta-off');
                                seta.addClass('rotate-seta-on');
                                div_options.fadeIn();
                            } else {
                                seta.removeClass('rotate-seta-on');
                                seta.addClass('rotate-seta-off');
                                div_options.fadeOut();
                            }

                            e.stopPropagation();
                        });

                        $('.option-base').click(function() {
                            id = $(this).attr('data-id');
                            $('.select-estilizado').each(function() {
                                if($(this).attr('data-id') == id) {
                                    $(this).click();
                                }
                            });
                        });

                        $(document).click(function() {
                            $('.seta-select').removeClass('rotate-seta-on');
                            $('.seta-select').addClass('rotate-seta-off');
                            $('.options-div-estilizado').fadeOut();
                        });

                        $('.option-base').hover(function() {
                            $('.options-div-estilizado').addClass('seta-fundo');
                        }, function() {
                            $('.options-div-estilizado').removeClass('seta-fundo');
                        });
                    });
                </script>
                <?php $combo = explode('combo', strtolower($produto->produto_base->name)) ?>
                <?php if(!$combo[1]) { ?>
                    <div class="col-xs-12 col-sm-6 col-md-12" style="font-size: 15px; padding-top: 5px;">
                        <?php if ($produto->produto_base->tipo_produto_id == 1) { ?>
                            <button class="button select-estilizado" data-id="1">Tamanho: <?= strtolower($produto->tamanho.' '.$produto->unidade_medida).' - '.ucfirst(strtolower($produto->embalagem)) ?> <i class="fa fa-angle-down seta-select rotate-seta-off" aria-hidden="true"></i></button>
                            <div class="options-div-estilizado options-div-estilizado-1">
                                    <?= $this->Html->link($produto->tamanho.' '.$produto->unidade_medida.' - '.$produto->embalagem,
                                        '#',
                                        ['class' => 'text-uppercase col-xs-12 option-estilizado option-base', 'data-id' => '1']
                                    ) ?>
                                    <?php foreach($produto_tamanhos as $produto_tamanho) { ?>
                                        <?php if(!($produto_tamanho->tamanho == $produto->tamanho && $produto_tamanho->unidade_medida == $produto->unidade_medida && $produto_tamanho->embalagem == $produto->embalagem)) { ?>
                                            <?= $this->Html->link($produto_tamanho->tamanho.' '.$produto_tamanho->unidade_medida.' - '.$produto_tamanho->embalagem,
                                                $SSlug.'/produto/'.$produto_tamanho->slug,
                                                ['class' => 'text-uppercase col-xs-12 option-estilizado']
                                            ) ?>
                                        <?php } ?>
                                    <?php } ?>
                            </div>
                        <?php } else if ($produto->produto_base->tipo_produto_id == 2) { ?>
                            <button class="button select-estilizado" data-id="1">Tamanho: <?= strtolower($produto->tamanho) ?> <i class="fa fa-angle-down seta-select rotate-seta-off" aria-hidden="true"></i></button>
                            <div class="options-div-estilizado options-div-estilizado-1">
                                <?= $this->Html->link($produto->tamanho,'#',
                                    ['class' => 'text-uppercase col-xs-12 option-estilizado option-base', 'data-id' => '1']
                                ) ?>
                                <?php foreach($produto_tamanhos as $produto_tamanho) { ?>
                                    <?php if(!($produto_tamanho->tamanho == $produto->tamanho )) { ?>
                                        <?= $this->Html->link($produto_tamanho->tamanho,
                                            $SSlug.'/produto/'.$produto_tamanho->slug,
                                            ['class' => 'text-uppercase col-xs-12 option-estilizado']
                                        ) ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>

                    <?php $produto_status = $produto->estoque == 0  ?' (Indisponível)' : '' ?>
                    <div class="col-xs-12 col-sm-6 col-md-12 box-sabor" style="font-size: 15px; padding-top: 5px;">
                        <?php if ($produto->produto_base->tipo_produto_id == 1) { ?>
                            <button class="button select-estilizado capitalize-text" data-id="2"><?= $produto->produto_base->propriedade->name ?>: <?= mb_strtolower($produto->propriedade).$produto_status ?> <i class="fa fa-angle-down seta-select rotate-seta-off" aria-hidden="true"></i></button>
                            <div class="options-div-estilizado options-div-estilizado-2">
                                <?= $this->Html->link($produto->propriedade.$produto_status,
                                    '#',
                                    ['class' => 'text-uppercase col-xs-12 option-estilizado option-base', 'data-id' => '2']
                                ) ?>
                                <?php foreach($produtos as $product) { ?>
                                    <?php $status_sabor = $product->status_id == 2 ?' (Indisponível)' : '' ?>
                                    <?php if($product->tamanho == $produto->tamanho && $product->unidade_medida == $produto->unidade_medida && $product->embalagem == $produto->embalagem) { ?>
                                        <?= $this->Html->link($product->propriedade.$status_sabor,
                                            $SSlug.'/produto/'.$product->slug,
                                            ['class' => 'text-uppercase col-xs-12 option-estilizado']
                                        ) ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        <?php } else if ($produto->produto_base->tipo_produto_id == 2) { ?>
                            <button class="button select-estilizado capitalize-text" data-id="2"><?= $produto->cor.$produto_status ?> <i class="fa fa-angle-down seta-select rotate-seta-off" aria-hidden="true"></i></button>
                            <div class="options-div-estilizado options-div-estilizado-2">
                                <?= $this->Html->link($produto->cor,
                                    '#',
                                    ['class' => 'text-uppercase col-xs-12 option-estilizado option-base', 'data-id' => '2']
                                ) ?>
                                <?php foreach($produtos as $product) { ?>
                                    <?php $status_sabor = $product->estoque == 0 ?' (Indisponível)' : '' ?>
                                    <?php if($product->tamanho == $produto->tamanho && $product->cor != $produto->cor) { ?>
                                        <?= $this->Html->link($product->cor.$status_sabor,
                                            $SSlug.'/produto/'.$product->slug,
                                            ['class' => 'text-uppercase col-xs-12 option-estilizado']
                                        ) ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <?php $produto_status = $produto->status_id == 2 ?' (Indisponível)' : '' ?>
                    <div class="col-xs-12 box-sabor" style="font-size: 15px; padding-top: 5px;">
                        <button class="button select-estilizado" data-id="2"><?= $produto->produto_base->propriedade->name ?>: <?= mb_strtolower($produto->propriedade).$produto_status ?> <i class="fa fa-angle-down seta-select rotate-seta-off" aria-hidden="true"></i></button>
                        <div class="options-div-estilizado options-div-estilizado-2">
                            <?= $this->Html->link($produto->propriedade.$produto_status,
                                '#',
                                ['class' => 'text-uppercase col-xs-12 option-estilizado option-base', 'data-id' => '2']
                            ) ?>
                            <?php foreach($produtos as $product) { ?>
                                <?php $status_sabor = $product->status_id == 2 ?' (Indisponível)' : '' ?>
                                <?php if($product->tamanho == $produto->tamanho && $product->unidade_medida == $produto->unidade_medida && $product->embalagem == $produto->embalagem) { ?>
                                    <?= $this->Html->link($product->propriedade.$status_sabor,
                                        $SSlug.'/produto/'.$product->slug,
                                        ['class' => 'text-uppercase col-xs-12 option-estilizado']
                                    ) ?>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="info-lateral">
                <div class="col-xs-12">
                    <h6><s>R$ <?= number_format($produto->preco, 2, ',','.') ?></s> <span class="pull-right"><?= $this->Html->image($produto->produto_base->marca->image, ['class' => 'mini-logo-marga'])  ?></span></h6>
                </div>
                <div class="col-xs-12">
                    <h1 class="preco-por" style="margin: 0">
                        <strong style="color: #5089cf">R$ <?= number_format($preco, 2, ',','.'); ?></strong> <span style="margin: 0; text-align: center; width: 90px; font-size: 15px; color: black;">OU</span>
                    </h1>
                </div>
                <div class="col-xs-12">
                    <?php if($produto->preco_promo != null) {
                        if($produto->preco_promo < 100.00) { ?>
                            <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/2?>
                            <strong style="color: #5089cf"><p style="font-size: 15px">2x de R$<?= number_format($parcel, 2, ',', '.') ?></p></strong>
                        <?php } else if($produto->preco_promo > 100.00 && $produto->preco_promo < 200.00) { ?>
                            <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/3?>
                            <strong style="color: #5089cf"><p style="font-size: 15px">3x de R$<?= number_format($parcel, 2, ',', '.') ?></p></strong>
                        <?php } else if($produto->preco_promo > 200.00 && $produto->preco_promo < 300.00) { ?>
                            <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/5?>
                            <strong style="color: #5089cf"><p style="font-size: 15px">5x de R$<?= number_format($parcel, 2, ',', '.') ?></p></strong>
                        <?php } else if($produto->preco_promo > 300.00) { ?>
                            <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/6?>
                            <strong style="color: #5089cf"><p style="font-size: 15px">6x de R$<?= number_format($parcel, 2, ',', '.') ?></p></strong>
                        <?php }
                    } else {
                        if($produto->preco < 100.00) { ?>
                            <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/2?>
                            <strong style="color: #5089cf"><p style="font-size: 15px">2x de R$<?= number_format($parcel, 2, ',', '.') ?></p></strong>
                        <?php } else if($produto->preco > 100.00 && $produto->preco < 200.00) { ?>
                            <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/3?>
                            <strong style="color: #5089cf"><p style="font-size: 15px">3x de R$<?= number_format($parcel, 2, ',', '.') ?></p></strong>
                        <?php } else if($produto->preco > 200.00 && $produto->preco < 300.00) { ?>
                            <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/5?>
                            <strong style="color: #5089cf"><p style="font-size: 15px">5x de R$<?= number_format($parcel, 2, ',', '.') ?></p></strong>
                        <?php } else if($produto->preco > 300.00) { ?>
                            <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/6?>
                            <strong style="color: #5089cf"><p style="font-size: 15px">6x de R$<?= number_format($parcel, 2, ',', '.') ?></p></strong>
                        <?php }
                    } ?>
                </div>
                <?php if($produto->tipo_produto_id == 1) { ?>
                    <div class="col-xs-12">
                        <?php if($produto->doses) { ?>
                            <h5>Para <strong><?= $produto->doses ?></strong> Treinos</h5>
                        <?php } else { ?>
                            <h5>&nbsp;</h5>
                        <?php } ?>
                    </div>
                    <div class="col-xs-12">
                        <?php if($produto->doses) { ?>
                            <h5>Valor por dose: <strong>R$ <?= number_format(($preco / $produto->doses), 2, ',','.') ?></strong></h5>
                        <?php } else { ?>
                            <h5>&nbsp;</h5>
                        <?php } ?>
                    </div>
                <?php } else if ($produto->tipo_produto_id == 2) { ?>
                    <h5>&nbsp;</h5>
                <?php } ?>
            </div>
            <div class="comprar-similar">
                <div class="col-xs-12 col-md-6">
                    <?php if($produto->visivel == 1 && $produto->status_id == 1) { ?>
                        <button id="comprar_id" val="<?= $produto->slug ?>" slug-id="<?= $SSlug ?>" data-id="<?= $produto->id ?>" class="btn-comprar button button-green"><?= $this->Html->image('mochila_log_branca.png',['alt' => 'Mochila LOG']) ?> <span>COMPRAR</span>
                        </button>   
                    <?php } else if($produto->visivel == 1 && $produto->status_id == 2) { ?>
                        <?= $this->Html->link('<button class="button button-blue-grande">VER SIMILARES</button>',
                            $SSlug.'/similares/'.$produto->slug, ['escape' => false]
                        ); ?>
                    <?php } ?>
                </div>
            </div>   
        </div>
        <div class="sugerir-compartilhar">
            <div class="col-xs-12">
                <?php if($produto->visivel == 1 && $produto->status_id == 1) { ?>
                    <span onclick="openNav()">
                        <button class="button button-blue"><i class="fa fa-envelope-o"></i> Sugerir/Compartilhar</button>
                    </span>  
                <?php } else if($produto->visivel == 1 && $produto->status_id == 2) { ?>
                    <button class="button button-blue aviseme-button" type="button" data-id="<?= $produto->id ?>"><i class="fa fa-envelope"></i> Avise-me</button>
                <?php } ?>
            </div>
            <div class="col-xs-12 produto-comparar-btn" style="margin-top: 10px; display: block;" id="comparar-btn">
                <button class="button button-blue btn-comparar-action" data-id="<?= $produto->id ?>"><i class="fa fa-balance-scale"></i> Comparar</button>
            </div>
      </div>
    </div>

    <div class="col-md-4">
        

        <div class="row">
            

            

            
            
            
        </div>
    </div>
</div>

<div id="aviseMe-<?= $produto->id ?>" class="overlay" onclick="closeOverlay()">
  <div class="overlay-content overlay-avise-<?= $produto->id ?> text-center">
    <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
    <br />
    <br />
    <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$produto->id])?>
    <?= $this->Form->input('produto_id', [
        'div'           => false,
        'label'         => false,
        'type'          => 'hidden',
        'val'           => $produto->id,
        'id'            => 'produto_id_acabou-'.$produto->id
    ])?>
    <?= $this->Form->input('name', [
        'div'           => false,
        'label'         => false,
        'id'            => 'name_acabou-'.$produto->id,
        'class'         => 'form-control validate[optional] name_acabou',
        'placeholder'   => 'Qual o seu nome?',
    ])?>
    <br/>
    <?= $this->Form->input('email', [
        'div'           => false,
        'label'         => false,
        'id'            => 'email_acabou-'.$produto->id,
        'class'         => 'form-control validate[required, custom[email]]',
        'placeholder'   => 'Qual o seu email?',
    ])?>
    <br/>
    <?= $this->Form->button('Enviar', [
        'data-id'       => $produto->id,
        'type'          => 'button',
        'class'         => 'btn btn-success send-acabou-btn'
    ])?>
    <br />
    <br />
    <?= $this->Form->end()?>
  </div>
</div>

<div id="myNav" class="overlay">
    <div class="overlay-content text-center">
        <a href="javascript:void(0)" class="closebtn close-indicacao-btn" onclick="closeNav()">&times;</a>
        <h4>Lembrou de alguém quando viu isso?</h4>
        <h4>Indique este produto!</h4>
        <br>
        <div class="row">
            <div class="col-xs-12 share-whastapp share-buttons">
                <a href="whatsapp://send?text=Isso é a sua cara! <?= WEBROOT_URL.$SSlug.'/produto/'.$produto->slug ?>">
                    <button class="btn button-blue"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</button>
                </a>
            </div>
            <div class="col-xs-12 share-whastapp share-buttons">
                <button class="btn button-blue" id="share-<?= $produto->id ?>"><i class="fa fa-comment-o" aria-hidden="true"></i> Messenger</button>
                <script>
                    $(document).ready(function() {
                        $('#share-<?= $produto->id ?>').click(function(){
                            var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$produto->slug ?>";
                            var app_id = '1321574044525013';
                            window.open('fb-messenger://share?link=' + link + '&app_id=' + app_id);
                        });
                    });
                </script>
            </div>
            <div class="tablet-hidden col-xs-12 col-md-3 share-buttons">
                <button data-id="whatsweb-<?= $produto->id ?>" class="btn button-blue abrir-whats"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp Web</button>
            </div>
             <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                <a href="javascript: void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u=<?= WEBROOT_URL.$SSlug.'/produto/'.$produto->slug ?>','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');">
                    <button class="btn button-blue fb-share-button"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</button>
                </a>
            </div>
             <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                <button class="btn button-blue" id="copiar-clipboard-<?= $produto->id ?>">Copiar link</button>
            </div>
             <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                <button  class="btn button-blue abrir-email"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail</button>
            </div>
        </div>
        <div class="row">
            <div id="collapse-whats-<?=$produto->id ?>" class="tablet-hidden enviar-whats">
                <div class="col-xs-4 text-center form-group">
                    <?= $this->Form->input('whats-web-form', [
                        'div'           => false,
                        'id'            => 'nro-whatsweb-'.$produto->id,
                        'label'         => false,
                        'class'         => 'form-control form-indicar',
                        'placeholder'   => "Celular*: (00)00000-0000"
                    ])?>
                </div>
                <div class="col-xs-4 text-left obs-overlay">
                    <p>*Para enviar pra mais de um contato deixe este campo vazio.</p>
                </div>
                <div class="col-xs-4 text-center form-group">
                    <?= $this->Form->button('Enviar', [
                    'type'          => 'button',
                    'data-id'       => $produto->id,
                    'class'         => 'btn button-blue',
                    'id'            => 'enviar-whatsweb-'.$produto->id
                    ])?>
                </div>
                <script>
                    $(document).ready(function() {
                        $('#enviar-whatsweb-<?=$produto->id ?>').click(function(){

                            var numero;
                            var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$produto->slug ?>";

                            if($('#nro-whatsweb-<?= $produto->id ?>').val() != '') {
                                numero = '55'+$('#nro-whatsweb-<?= $produto->id ?>').val();
                            }
                            
                            window.open('https://api.whatsapp.com/send?phone=' + numero + '&text=Isso é a sua cara! ' + link);
                        });
                    });
                </script>
            </div>
            <div class="col-xs-12 enviar-email" id="collapse-email-<?=$produto->id ?>">
                <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$produto->id])?>
                <?= $this->Form->input('name', [
                    'div'           => false,
                    'label'         => false,
                    'class'         => 'form-control validate[optional] form-indicar name-'.$produto->id,
                    'placeholder'   => 'Quem indicou?',
                ])?>
                <br>
                <?= $this->Form->input('email', [
                    'div'           => false,
                    'label'         => false,
                    'class'         => 'form-control validate[required] form-indicar email-'.$produto->id,
                    'placeholder'   => 'E-mail do destinatário',
                ])?>
                <?= $this->Form->input('id_produto', [
                    'div'           => false,
                    'class'         => 'id_produto-'.$produto->id,
                    'value'         => $produto->id,
                    'label'         => false,
                    'type'          => 'hidden'
                ])?>
                <br/>
                <?= $this->Form->button('Enviar', [
                    'id'            => 'send-indicacao-btn',
                    'type'          => 'button',
                    'data-id'       => $produto->id,
                    'class'         => 'btn button-blue send-indicacao-home-btn'
                ])?>
                <br />
                <?= $this->Form->end()?>
            </div>
        </div>
        <div class="row"> 
            <div class="col-xs-12 clipboard">   
                <?= $this->Form->input('url', [
                    'div'           => false,
                    'id'            => 'url-clipboard-'.$produto->id,
                    'label'         => false,
                    'value'         => WEBROOT_URL.$SSlug.'/produto/'.$produto->slug,
                    'readonly'      => true,
                    'class'         => 'form-control form-indicar'
                ])?>
            </div>  
            <p><span class="copy-alert hide" id="copy-alert-<?=$produto->id ?>">Copiado!</span></p>
            <script>
                $(document).ready(function() {
                    // Copy to clipboard 
                    document.querySelector("#copiar-clipboard-<?=$produto->id ?>").onclick = () => {
                      // Select the content
                        document.querySelector("#url-clipboard-<?=$produto->id ?>").select();
                        // Copy to the clipboard
                        document.execCommand('copy');
                        // Exibe span de sucesso
                        $('#copy-alert-<?=$produto->id ?>').removeClass("hide");
                        // Oculta span de sucesso
                        setTimeout(function() {
                            $('#copy-alert-<?=$produto->id ?>').addClass("hide");
                        }, 1500);
                    };
                });
            </script>
        </div>
    </div>
</div>

<style type="text/css">
    @media all and (max-width: 1300px) {
        .produto-item {
            width: 33.33333333%;
        }
    }
    @media all and (max-width: 1050px) {
        .produto-item {
            width: 50%;
        }
    }
</style>

<style type="text/css">
    .container-hexagons {
        height: 150px; 
        padding-top: 40px; 
        width: 100%;
        overflow-x: auto;
        overflow-y: hidden;
        white-space: nowrap;
    }

    .hexagon-seletor-disponivel {
        width: 120px;
        height: 50px;
        font-size: 19px;
        display: inline-block;
        margin-right: 10px;
    }

    .hexagon-seletor-disponivel span {
        vertical-align: middle;
        display: table-cell;
        white-space: normal;
        height: 50px;
        width: 120px;
    }

    .hexagon-seletor-disponivel:before {
        top: -40px;
        border-left: 60px solid transparent;
        border-right: 60px solid transparent;
        border-bottom: 40px solid rgba(72, 159, 54, 1);

    }

    .hexagon-seletor-disponivel:after {
        bottom: -40px;
        border-left: 60px solid transparent;
        border-right: 60px solid transparent;
        border-top: 40px solid rgba(72, 159, 54, 1);
    }

    .hexagon-seletor-disponivel:hover:before {
        border-bottom: 40px solid rgba(55, 119, 42, 1);
    }

    .hexagon-seletor-disponivel:hover:after {
        border-top: 40px solid rgba(55, 119, 42, 1);
    }
    .container-conteudo {
        display: none;
    }
    .container-conteudo > div {
        min-height: 500px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        $('.hexagon-seletor-disponivel').click(function() {
            var data = $(this).attr('data-id');

            $('html, body').animate({
                scrollTop: $(this).offset().top - 150
            }, 700);

            $('.container-conteudo').not('#'+data).fadeOut(function() {
                setTimeout(function() {
                    $('#'+data).fadeIn();
                }, 400);
            });
        });

        $('#tabela-nutricional').fadeIn();
    });
</script>

<div class="container" style="margin-top: 20px">
    <div class="col-xs-12 text-center">
        <h3>Detalhes do Produto</h3>
    </div>
</div>

<?php 
    $tem_tabela = 0;
    $tem_aminograma = 0;
    $tem_vitaminas = 0;
?>
<?php foreach($produto->produto_substancias as $produto_substancia) {
    if($produto_substancia->substancia->tipo == 'tabela nutricional') {
        $tem_tabela = 1;
    } 

    if($produto_substancia->substancia->tipo == 'aminograma') {
        $tem_aminograma = 1;
    } 

    if($produto_substancia->substancia->tipo == 'vitaminas e minerais') {
        $tem_vitaminas = 1;
    } 
} ?>

<?php $qtd_tabelas = $tem_tabela + $tem_aminograma + $tem_vitaminas; ?>

<div class="container container-hexagons">
    <div class="hexagons-div text-center">

        <?php if($produto->produto_base->atributos) { ?>
            <a data-id="descricao" class="hexagon-disponivel hexagon-seletor-disponivel">
                <span>Descrição</span>
            </a>
        <?php } ?>

        <?php if($produto->produto_base->beneficios) { ?>
            <a data-id="beneficios" class="hexagon-disponivel hexagon-seletor-disponivel">
                <span>Benefícios</span>
            </a>
        <?php } ?>

        <?php if($produto->produto_base->dicas) { ?>
            <a data-id="dicas" class="hexagon-disponivel hexagon-seletor-disponivel">
                <span>Dicas</span>
            </a>
        <?php } ?>

        <?php if($produto->ingredientes) { ?>
            <a data-id="ingredientes" class="hexagon-disponivel hexagon-seletor-disponivel">
                <span>Ingredientes</span>
            </a>
        <?php } ?>

        <?php if($produto->como_tomar) { ?>
            <a data-id="como-tomar" class="hexagon-disponivel hexagon-seletor-disponivel">
                <span>Como tomar</span>
            </a>
        <?php } ?>
        
        <?php if($qtd_tabelas > 0) { ?>
        <a data-id="tabela-nutricional" class="hexagon-disponivel hexagon-seletor-disponivel tabela-nutri">
            <span>Tabela Nutricional</span>
        </a>
        <?php } ?>

        <a data-id="combina-com" class="hexagon-disponivel hexagon-seletor-disponivel">
            <span>Combina Com</span>
        </a>

    </div>
</div>

<style type="text/css">
    .texto-padrao * {
        font-size: medium!important;
        font-family: open_sansregular!important;
        color: black!important;
    }

    .owl-theme .owl-controls .owl-buttons div {
        color: #6f6f6f!important;
        background-color: transparent!important;
        font-size: 30px!important;
    }

    .owl-prev {
        width: 15px;
        height: 100px;
        position: absolute;
        top: 40%;
        left: 0;
        display: block!important;
        border:0px solid black;
    }

    .owl-carousel-produtos-view .owl-prev{
        width: 15px;
        height: 100px;
        position: absolute;
        top: 46%;
        left: -40px;
        display: block!important;
        border:0px solid black;
    }

    .owl-carousel-produtos-view .owl-next{
        width: 15px;
        height: 100px;
        position: absolute;
        top: 46%;
        right: 12px;
        display: block!important;
        border:0px solid black;
    }

    @media all and (max-height: 700px){
        .owl-carousel-produtos-view .owl-prev{
        width: 15px;
        height: 100px;
        position: absolute;
        top: 53%;
        left: 16px;
        display: block!important;
        border:0px solid black;
    }

    .owl-carousel-produtos-view .owl-next{
        width: 15px;
        height: 100px;
        position: absolute;
        top: 53%;
        right: 60px;
        display: block!important;
        border:0px solid black;
    }

    }

    .owl-next {
        width: 15px;
        height: 100px;
        position: absolute;
        top: 40%;
        right: -10px;
        display: block!important;
        border:0px solid black;
    }
</style>

<div class="col-xs-12 box-containers-conteudo">
    <div class="row">
        <div id="descricao" class="container container-conteudo texto-padrao">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <p><?= $produto->produto_base->atributos ?></p>
            </div>
        </div>

        <div id="beneficios" class="container container-conteudo texto-padrao">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <p><?= $produto->produto_base->beneficios ?></p>
            </div>
        </div>

        <div id="dicas" class="container container-conteudo texto-padrao">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <p><?= $produto->produto_base->dicas ?></p>
            </div>
        </div>

        <div id="ingredientes" class="container container-conteudo texto-padrao">
            <!-- <?php 
                $ingredientes_list = [];

                $ingredientes_list = str_replace('.', '--', $ingredientes_list);
                $ingredientes_list = str_replace(',', '--', $ingredientes_list);
                $ingredientes_list = str_replace(';', '--', $ingredientes_list);

                $ingredientes_list = explode('--', $ingredientes_list);
            ?>

            <?php $count_ingredientes = count($ingredientes_list); ?>
            <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                <?php for($count = 0; $count < $count_ingredientes; $count++) { ?>
                    <?php $ingredientes_list[$count] = trim($ingredientes_list[$count]); ?>
                    <?php if(!empty($ingredientes_list[$count])) { ?>
                        <p><?= $count+1 ?>º - <?= $ingredientes_list[$count] ?></p>
                    <?php } ?>
                <?php } ?>
            </div> -->
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <p><?= $produto->ingredientes ?></p>
            </div>
        </div>

        <div id="como-tomar" class="container container-conteudo texto-padrao">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <p><?= $produto->como_tomar ?></p>
            </div>
        </div>

        <div id="tabela-nutricional" class="container container-conteudo">
            <div class="formula col-xs-12">

                <?php if($qtd_tabelas <= 1) { ?>
                    <style type="text/css">
                        .tabela {
                            float: none;
                        }
                    </style>
                <?php } ?>

                <?php if($tem_tabela) { ?>
                    <div class="tabela col-md-4 col-xs-12 col-sm-8 col-sm-offset-2 col-md-offset-0 text-center">
                        <div class="col-xs-12 tabela-nutricional-linha text-center">
                            <strong style="font-size: 20px;">Tabela Nutricional</strong>
                        </div>
                        <div class="col-xs-12 tabela-nutricional-linha text-center">
                            <strong>Porção de <?= $produto->porcao ?> <?= $produto->unidade_medida ?></strong>
                        </div>
                        <?php foreach($produto->produto_substancias as $produto_substancia) { ?>
                            <?php if($produto_substancia->substancia->tipo == 'tabela nutricional') { ?>
                                <div class="col-xs-12 tabela-nutricional-linha">
                                    <div class="col-xs-8 text-center tabela-nutricional-substancias">
                                        <?= $produto_substancia->substancia->name ?>
                                    </div>
                                    <div class="col-xs-4 text-center tabela-nutricional-valores">
                                        <?= $produto_substancia->valor ?><?= $produto_substancia->substancia->unidade_medida ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                <?php } ?>

                <?php if($tem_aminograma) { ?>
                    <div class="tabela col-md-4 col-xs-12 col-sm-8 col-sm-offset-2 col-md-offset-0 text-center">
                        <div class="col-xs-12 tabela-nutricional-linha text-center">
                            <strong style="font-size: 20px;">Aminograma</strong>
                        </div>
                        <?php foreach($produto->produto_substancias as $produto_substancia) { ?>
                            <?php if($produto_substancia->substancia->tipo == 'aminograma') { ?>
                                <div class="col-xs-12 tabela-nutricional-linha">
                                    <div class="col-xs-8 text-center tabela-nutricional-substancias">
                                        <?= $produto_substancia->substancia->name ?>
                                    </div>
                                    <div class="col-xs-4 text-center tabela-nutricional-valores">
                                        <?= $produto_substancia->valor ?><?= $produto_substancia->substancia->unidade_medida ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                <?php } ?>

                <?php if($tem_vitaminas) { ?>
                    <div class="tabela col-md-4 col-xs-12 col-sm-8 col-sm-offset-2 col-md-offset-0 text-center">
                        <div class="col-xs-12 tabela-nutricional-linha text-center">
                            <strong style="font-size: 20px;">Vitaminas e Minerais</strong>
                        </div>
                        <?php foreach($produto->produto_substancias as $produto_substancia) { ?>
                            <?php if($produto_substancia->substancia->tipo == 'vitaminas e minerais') { ?>
                                <div class="col-xs-12 tabela-nutricional-linha">
                                    <div class="col-xs-8 text-center tabela-nutricional-substancias">
                                        <?= $produto_substancia->substancia->name ?>
                                    </div>
                                    <div class="col-xs-4 text-center tabela-nutricional-valores">
                                        <?= $produto_substancia->valor ?><?= $produto_substancia->substancia->unidade_medida ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>

            <style type="text/css">
                .tabela-nutricional-substancias,
                .tabela-nutricional-valores {
                    font-weight: 700;
                }

                .tabela-nutricional-linha {
                    border: 1px solid black;
                    border-top: 0;
                    padding-right: 2px;
                    padding-left: 2px;
                }

                .tabela-nutricional-linha:nth-of-type(1) {
                    border-top: 1px solid black;
                }

                .tabela-nutricional-substancias {
                    border-right: 1px solid black;
                }
            </style>
        </div>

        <div id="combina-com" class="container container-conteudo">
            <div class="owl-carousel">
                <?php $i = 1; ?>
                <?php foreach($produto_combinations as $produto_combination) { ?>
                    <div class="item">
                        <?php $fotos = unserialize($produto_combination->fotos); ?>
                        <div class="box-completa-produto" itemtype="http://schema.org/Product">
                            <div class="tarja-objetivo">
                                <?php
                                    $contador_objetivos = 0; 
                                    $novo_top = 0;
                                ?>
                                <?php foreach ($produto_combination->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                    <?php if ($contador_objetivos < 2) { ?>
                                    <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                                        <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                    </div>
                                    <?php } ?>
                                    <?php
                                        $contador_objetivos++;
                                        $novo_top = $novo_top + 19;
                                     ?>
                                <?php } ?>
                            </div>
                            <div class="total-tarjas">
                                <?php if ($produto_combination->estoque == 1) { ?>
                                    <div class="tarja tarja-ultimo">
                                        <span>Último</span>
                                    </div>
                                <?php } else if ($produto_combination->estoque >= 1 && $produto_combination->preco_promo) { ?>
                                    <div class="tarja tarja-promocao">
                                        <span>Promoção</span>
                                    </div>
                                <?php } else if($produto_combination->estoque >= 1 && $produto_combination->created > $max_date_novidade) { ?>
                                    <div class="tarja tarja-novidade">
                                        <span>Novidade</span>
                                    </div>
                                <?php } ?>               
                            </div>
                            <div class="info-produto">
                                <div class="produto-superior">
                                    <?php $fotos = unserialize($produto_combination->fotos); ?>
                                    <div class="foto-produto" id="produto-<?= $i ?>" >
                                        <?= $this->Html->image(
                                            $fotos[0],
                                            ['val' => $produto_combination->slug, 'alt' => $produto_combination->produto_base->name.' '.$produto_combination->produto_base->embalagem_conteudo]); ?>
                                    </div>
                                    <div class="detalhes-produto">
                                        <div class="informacoes-detalhes text-left">
                                            <p>Código: <?= $produto_combination->id ?></p>
                                            <p><?= strtolower($produto_combination->produto_base->marca->name); ?></p>
                                            <p><?= $produto_combination->tamanho.' '.$produto_combination->unidade_medida; ?></p>
                                            <p>para <?= $produto_combination->doses; ?> treinos</p>
                                            <p><?= strtolower($produto_combination->propriedade); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="produto-inferior">
                                    <?php if($produto_combination->preco_promo) { ?>
                                    <?php $porcentagem_nova = $produto_combination->preco * 0.089;
                                    $preco_corte = $produto_combination->preco + $porcentagem_nova; 
                                    } ?> 
                                    <?php $produto_combination->preco_promo ?
                                    number_format($preco = $produto_combination->preco_promo * $desconto_geral, 2, ',','.') :
                                    number_format($preco = $produto_combination->preco * $desconto_geral, 2, ',','.'); ?>
                                    <?php $produto_combination->preco_promo ? $porcentagem_nova = $produto_combination->preco_promo * 0.089 : $porcentagem_nova = $produto_combination->preco * 0.089 ?>
                                    <?php if($produto_combination->preco_promo != null) {
                                        if($produto_combination->preco_promo < 100.00) { ?>
                                        <?php $parcel = $produto_combination->preco_promo * $desconto_geral * 0.089; $parcel = ($produto_combination->preco_promo * $desconto_geral + $parcel)/2?>
                                        <?php $maximo_parcelas = "2x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto_combination->preco_promo > 100.00 && $produto_combination->preco_promo < 200.00) { ?>
                                        <?php $parcel = $produto_combination->preco_promo * $desconto_geral * 0.089; $parcel = ($produto_combination->preco_promo * $desconto_geral + $parcel)/3?>
                                        <?php $maximo_parcelas = "3x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto_combination->preco_promo > 200.00 && $produto_combination->preco_promo < 300.00) { ?>
                                        <?php $parcel = $produto_combination->preco_promo * $desconto_geral * 0.089; $parcel = ($produto_combination->preco_promo * $desconto_geral + $parcel)/5?>
                                        <?php $maximo_parcelas = "5x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto_combination->preco_promo > 300.00) { ?>
                                        <?php $parcel = $produto_combination->preco_promo * $desconto_geral * 0.089; $parcel = ($produto_combination->preco_promo * $desconto_geral + $parcel)/6?>
                                        <?php $maximo_parcelas = "6x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php }
                                        } else {
                                    if($produto_combination->preco < 100.00) { ?>
                                        <?php $parcel = $produto_combination->preco * $desconto_geral * 0.089; $parcel = ($produto_combination->preco * $desconto_geral + $parcel)/2?>
                                         <?php $maximo_parcelas = "2x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto_combination->preco > 100.00 && $produto_combination->preco < 200.00) { ?>
                                        <?php $parcel = $produto_combination->preco * $desconto_geral * 0.089; $parcel = ($produto_combination->preco * $desconto_geral + $parcel)/3?>
                                        <?php $maximo_parcelas = "3x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto_combination->preco > 200.00 && $produto_combination->preco < 300.00) { ?>
                                        <?php $parcel = $produto_combination->preco * $desconto_geral * 0.089; $parcel = ($produto_combination->preco * $desconto_geral + $parcel)/5?>
                                        <?php $maximo_parcelas = "5x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto_combination->preco > 300.00) { ?>
                                        <?php $parcel = $produto_combination->preco * $desconto_geral * 0.089; $parcel = ($produto_combination->preco * $desconto_geral + $parcel)/6?>
                                        <?php $maximo_parcelas = "6x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php }
                                    } ?>
                                    <?php 
                                        if($produto_combination->preco_promo) {
                                            echo $this->Html->link("
                                                <h4>".$produto_combination->produto_base->name."</h4>
                                                <p><span class='preco-corte'> R$".number_format($produto->preco, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                $SSlug.'/produto/'.$produto_combination->slug,
                                                ['escape' => false]);
                                        } else {
                                            echo $this->Html->link("
                                                <h4>".$produto_combination->produto_base->name."</h4>
                                                <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                $SSlug.'/produto/'.$produto_combination->slug,
                                                ['escape' => false]);
                                        }
                                    ?>
                                </div>
                                <div class="hover-produto <?= $produto_combination->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                                    <div class="col-xs-12">
                                        <div class="hexagono-indicar">
                                            <?php if($produto_combination->status_id == 1) { ?>
                                                <div class="hexagon-disponivel btn-indique" data-id="<?= $produto_combination->id ?>">
                                                    <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                                    <p>indique</p>
                                                </div>
                                                <script>
                                                    $('.btn-indique').click(function () {
                                                            var id_produto = $(this).attr('data-id');
                                                            $('#overlay-indicacao-'+id_produto).height('100%');
                                                            $("html,body").css({"overflow":"hidden"});
                                                    });
                                                    function closeIndica() {
                                                        $('.overlay-indicacao').height('0%');
                                                        $("html,body").css({"overflow":"auto"});
                                                    }
                                                </script>
                                            <?php } else { ?>
                                                <div class="hexagon produto-acabou-btn" data-id="<?= $produto_combination->id ?>">
                                                    <span><i class="fa fa-envelope-o"></i></span>
                                                    <p>avise-me</p>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <?php if($produto_combination->status_id == 1) { ?>
                                        <div class="col-xs-6">
                                            <?= $this->Html->link('
                                                <div class="hexagon-disponivel">
                                                    <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                    <p>detalhes</p>
                                                </div>'
                                                ,$SSlug.'/produto/'.$produto_combination->slug,
                                                ['escape' => false]);
                                            ?>
                                        </div>
                                        <div class="col-xs-6 text-center compare">
                                            <div class="hexagon-disponivel btn-comparar-action" data-id="<?= $produto_combination->id ?>">
                                                <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                                <p>comparar</p>
                                            </div>  
                                        </div>
                                    <?php }
                                    else { ?>
                                        <div class="col-xs-6">
                                            <?= $this->Html->link('
                                                <div class="hexagon">
                                                    <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                    <p>detalhes</p>
                                                </div>'
                                                ,$SSlug.'/produto/'.$produto_combination->slug,
                                                ['escape' => false]);
                                            ?>
                                        </div>
                                        <div class="col-xs-6 text-center compare">
                                            <div class="hexagon btn-comparar-action" data-id="<?= $produto_combination->id ?>">
                                                <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                                <p>comparar</p>
                                            </div>  
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="botoes-comprar-similiar">
                                <?php if($produto_combination->visivel == 1 && $produto_combination->status_id == 1) { ?>
                                    <button class="btn btn-incluir incluir-mochila" slug-id="<?= $SSlug ?>" val="<?= $produto_combination->slug ?>" data-id="<?= $produto_combination->id ?>">Comprar</button>
                                    <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $produto->id ?>">Indique</button>
                                    <button class="desktop-hidden btn btn-incluir produto-comparar-btn" data-id="<?= $produto_combination->id ?>">Comparar</button>
                                <?php } else if($produto_combination->visivel == 1 && $produto_combination->status_id == 2) { ?>
                                    <button class="btn btn-similares">Similares</button>            
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php $i++; ?>
                <?php } ?>
            </div>
        </div>
        <?php foreach($produto_combinations as $produto_combination) { ?>
            <div id="overlay-indicacao-<?= $produto_combination->id ?>" class="overlay overlay-indicacao">
                <div class="overlay-content text-center">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                    <h4>Lembrou de alguém quando viu isso?</h4>
                    <h4>Indique este produto!</h4>
                    <br>
                    <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$produto_combination->id])?>
                    <?= $this->Form->input('name', [
                        'div'           => false,
                        'label'         => false,
                        'class'         => 'form-control validate[optional] form-indicar name-'.$produto_combination->id,
                        'placeholder'   => 'Quem indicou?',
                    ])?>
                    <br>
                    <?= $this->Form->input('email', [
                        'div'           => false,
                        'label'         => false,
                        'class'         => 'form-control validate[required] form-indicar email-'.$produto_combination->id,
                        'placeholder'   => 'E-mail do destinatário',
                    ])?>
                    <?= $this->Form->input('id_produto', [
                        'div'           => false,
                        'class'         => 'id_produto-'.$produto_combination->id,
                        'value'         => $produto_combination->id,
                        'label'         => false,
                        'type'          => 'hidden'
                    ])?>
                    <br>
                    <?= $this->Form->button('Enviar', [
                        'type'          => 'button',
                        'data-id'       => $produto_combination->id,
                        'class'         => 'btn btn-success send-indicacao-home-btn'
                    ])?>
                    <br>
                    <br>
                    <?= $this->Form->end()?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<script type="text/javascript">
    $('.send-indicacao-home-btn').click(function(e) {
        e.preventDefault();
        var botao = $(this);
    
        botao.html('Enviando...');
        botao.attr('disabled', 'disabled');

        var id = $(this).attr('data-id');
        var slug = '<?= $SSlug ?>';

        var valid = $('.indicar_prod_home_'+id).validationEngine("validate");
        if (valid == true) {
            $.ajax({
                type: "POST",
                var slug = '<?= $SSlug ?>';
                url: WEBROOT_URL + slug + '/produtos/overlay-indicar-produtos-home/',
                data: {
                    name: $('.name-'+id).val(),
                    email: $('.email-'+id).val(), 
                    produto_id: $('.id_produto-'+id).val()
                }
            })
            .done(function (data) {
                if(data == 1) {
                    botao.html('Enviado!');
                    setTimeout(function () {
                        botao.html('Enviar');
                        botao.removeAttr('disabled');
                    }, 2200);
                } else {
                    botao.html('Falha ao enviar... Tente novamente...');
                    setTimeout(function () {
                        botao.html('Enviar');
                        botao.removeAttr('disabled');
                    }, 2200);
                }
            });
        }else{
            $('.indicar_prod_home_'+id).validationEngine({
                updatePromptsPosition: true,
                promptPosition: 'inline',
                scroll: false
            });
        }
    });
</script>
<script>
    $('.abrir-email').click(function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('.enviar-whats').slideUp("slow");
        $('.enviar-email').slideToggle("slow");
    });
    $('.abrir-whats').click(function(e){
        var id = $(this).attr('data-id');
        $('.enviar-email').slideUp("slow");
        $('.enviar-whats').slideToggle("slow");
    });
</script>