<?php
$myTemplates = ['inputContainer' => '{{content}}'];
$this->Form->templates($myTemplates);
?>
<?php $fotos = unserialize($produto->fotos); ?>

<?php
foreach($fotos as $key => $foto):
    $fotos_trust[] = WEBROOT_URL.'img/produtos/md-'.$foto;
endforeach;
?>

<!-- Trustvox -->
<script type="text/javascript">
 window._trustvox =  [];
 _trustvox.push(['_storeId', '71702']);
 _trustvox.push(['_productId',  '<?= $produto->id ?>']);
 _trustvox.push(['_productName', '<?= $produto->produto_base->name ?>']);
 _trustvox.push(['_productPhotos', <?= json_encode($fotos_trust) ?>]);
</script>
<script async="true" type="text/javascript" src="//static.trustvox.com.br/assets/widget.js"></script>
<!-- End Trustvox -->

<style>
    @-webkit-keyframes diff {
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
    }
    @keyframes diff {
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
    }
    .desc-produto,
    .combina-cont,
    .prod-fim {
        padding-right: 230px;
        padding-left: 230px;
        margin-left: 0;
        margin-right: 0;
        width: 100%;
    }
    .dicas {
        margin: 0 13px 30px 13px;
    }
    .icone_comprar{
        max-width: 32px;
    }
    @media all and (min-width: 769px) { 
        .box-close-mochila {
            width: 0;
        }
        .mochila {
            width: 230px;
            opacity: 1;
        }
        .fundo-carrinho {
            margin-left: 0;
        }
        .cupom-desconto, 
        .total-compra, 
        .btn-fecha-mochila {
            right: 0px;
        }
        .box-close-filter { 
            width: 0; 
        }
        .filtro { 
            width: 230px; 
            opacity: 1; 
        }
        .box-filtro { 
            left: 0; 
        }
        .info-direita-mochila {
            display: block;
        }
        .produtos-mochila {
            width: 228px;
            opacity: 1;
        }
    }
    .product-view {
        font-size: 23px;
    }
    .product-price {
        font-size: 2.5em;
        color: #5089cf;
        padding-top: 0;
        margin-top: -45px;
    }
    .doses {
        font-weight: 700;
    }
    .logo-marca {
        max-width: 80%;
    }
    .preco-de {
        color: #222;
        font-size: 13px;
    }
    #sabores-link {
        border: 2px solid #5089cf;
        color: #5089cf;
        font-weight: 700;
    }
    .texto-sabor {
        float: left;
        display: flex;
        align-items: center;
        display: -webkit-flex;
        -webkit-align-items: center;
        height: 25px;
        padding-right: 10px;
        margin-bottom: 0;
    }
    .btn-comprar {
        border: 0;
        color: #fff;
        background-color: #8dc73f;
        font-size: 30px;
        margin: 10px 0;
        text-transform: lowercase;
        text-align: center;
        padding: 5px 0;
        width: 100%;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
    }
    .btn-comprar:hover,
    .btn-comprar:focus,
    .btn-comprar:visited {
        -webkit-transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
        transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
        -webkit-animation: diff 0.5s 1;
        animation: diff 0.5s 1;
    }
    .btn-comprar svg {
        width: 40px;
        fill: white;
        position: absolute;
        left: 30%;
    }
    .btn-avise {
        border: 0;
        color: #fff;
        background-color: #5089cf;
        font-size: 30px;
        margin: 10px 0;
        text-transform: lowercase;
        text-align: center;
        padding: 5px 0;
        width: 100%;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
    }
    .btn-avise:hover,
    .btn-avise:focus,
    .btn-avise:visited {
        -webkit-transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
        transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
        -webkit-animation: diff 0.5s 1;
        animation: diff 0.5s 1;
    }
    .img-produto-principal {
        max-height: 280px;
        max-width: 320px;
    }
    .quadro-img-produto-principal {
        height: 380px;
    }
    .miniaturas {
        text-align: left;
    }

    /*combinations*/
    .product {
        font-size: 14px;
    }
    .produto-item .bg-white-produto {
        min-height: 290px;
        border: 2px solid rgb(234, 234, 234);
        box-shadow: 0px 1px 7px 0px rgba(200,200,200,0.75);
    }
    .bg-gray-produto {
        bottom: 0;
        position: absolute;
        min-height: 92px;
        max-width: 185px;
    }
    .description {
        height: 60px;
        width: 100%;
        overflow: hidden;
        text-overflow: ellipsis;
        max-height: 60px;
        margin: 0;
        font-size: 14px;
        text-transform: uppercase;
        font-weight: 700;
    }
    @media all and (min-width: 769px) {
        .box-close-filter { 
            width: 0; 
        }
        .filtro { 
            width: 230px; 
            opacity: 1; 
        }
        .box-filtro { 
            left: 0; 
        }
    }
    .produto-grade {
        max-width: 145px;
        max-height: 150px;
        margin-top: 0;
        display: flex;
        align-items: center;
        display: -webkit-flex;
        -webkit-align-items: center;
    }
    .box-imagem-produto {
        width: 100%;
        height: 150px;
        margin-top: 10px;
        display: flex;
        justify-content: center;
        align-items: center;
        display: -webkit-flex;
        -webkit-justify-content: center;
        -webkit-align-items: center;
    }
    .box-info-produto {
        width: 100%;
        margin-top: 10px;
        height: 120px;
    }
    .box-fundo-produto {
        width: 100%;
        height: 40px;
        margin-top: 10px;
    }
    .box-preco {
        height: 40px;
        width: 70%;
        float: left;
        display: flex;
        flex-flow: column;
        align-items: baseline;
        justify-content: center;
        display: -webkit-flex;
        -webkit-flex-flow: column;
        -webkit-align-items: baseline;
        -webkit-justify-content: center;
        padding-left: 4px;
    }
    .box-mochila-add {
        height: 100%;
        width: 30%;
        float: right;
        display: flex;
        align-items: flex-end;
        display: -webkit-flex;
        -webkit-align-items: flex-end;
    }
    .box-mochila-add svg {
        width: 100%;
        height: 100%;
        cursor: pointer;
        background: #8dc73f;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
        fill: white;
        margin-right: 3px;
    }
    .preco {
        margin: 0;
    }
    .preco-desc {
        font-size: 13px;
    }
    .preco-normal {
        font-weight: 700;
        font-size: 18px;
        color: #5089cf;
    }
    .bg-white-produto a {
        min-height: 0;
    }
    .box-promocao {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: black;
        background: #f3d012;
    }
    .box-promocao:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-novidade {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: white;
        background: #5089cf;
    }
    .box-novidade:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-mochila-add svg:hover, .box-mochila-add svg:focus, .box-mochila-add svg:link:hover, .box-mochila-add svg:link:focus, .box-mochila-add svg:visited:hover, .box-mochila-add svg:visited:focus {
      -webkit-transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      -webkit-animation: diff 0.5s 1;
      animation: diff 0.5s 1;
    }

    @-webkit-keyframes diff {
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
    }
    @keyframes diff {
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
    }

    .icone-objetivo {
        position: absolute;
        left: 5px;
        bottom: 120px;
        width: 30px;
        height: 30px;
        color: #5089cf;
    }

    .box-imagem-produto svg {
        position: absolute;
        left: 5px;
        bottom: 120px;
        width: 20px;
        height: 30px;
    }
    .atributos-produto {
        width: 100%;
    }
    .combina-com {
        margin-bottom: 0;
    }
    .atributos-produto h4 {
        cursor: auto;
        color: #2D76CA;
        font-weight: 500;
    }
    .formula {
        display: block; 
        padding-left: 30px; 
        padding-right: 30px; 
        max-height: 350px;
        overflow: hidden;
    }
    .tabela {
        max-height: 350px;
        overflow: hidden;
    }
    .vermais-formula {
        padding-left: 230px;
        padding-right: 230px;
        background: white;
        position: absolute;
        width: 100%;
        text-align: center;
        z-index: 2;
        padding-top: 10px;
    }
    .prod-fim {
        margin-top: 40px;
    }
    .sabor-info {
        font-weight: bold;
        font-size: 13px;
        color: #333;
        text-align: center;
        font-family: nexa_lightregular,"Droid Sans",serif;
    }
    .breadcrumbs {
        width: 60%; 
        height: 50px; 
        padding-left: 20px; 
        padding-top: 10px; 
        text-transform: lowercase; 
        text-decoration: underline;
        float: left;
    }
    .setas {
        text-decoration: none;
        color: #5089cf;
    }
    .codigo {
        width: 40%;
        float: right;
        padding-right: 20px; 
        padding-top: 10px; 
    }
    .codigo p {
        text-align: right;
        font-size: 12px;
    }
    .formula-text {
        padding-left: 230px; 
        padding-right: 230px;
    }
    @media all and (max-width: 768px) {
        .desc-produto, 
        .combina-cont, 
        .prod-fim, 
        .formula-text,
        .vermais-formula {
            padding-left: 0;
            padding-right: 0;
        }
        .desc-produto {
            padding-top: 90px;
        }
        .quadro-img-produto-principal,
        .quadro-desc-produto {
            width: 100%;
        }
        .quadro-img-produto-principal {
            white-space: normal;
        }
        .produto-item {
            width: 50%;
            padding: 0;
        }
        .produto-grade {
            max-width: 120px;
            max-height: 120px;
        }
        .produto-item .bg-white-produto {
            min-height: 310px;
        }
        .box-imagem-produto svg {
            bottom: 130px;
        }
        .produto-div,
        .vermais-formula,
        .formula-text {
            margin: 0;
        }
        .breadcrumbs {
            width: 100%;
            padding-top: 0;
            float: left;
            z-index: 2;
            position: absolute;
        }
        #sabores-link {
            float: left;
        }
        .box-sabor {
            width: 50%;
            display: flex;
            justify-content: center;
            display: -webkit-flex;
            -webkit-justify-content: center;
        }
        .product-price {
            margin-top: -85px;
        }
        .box-sabor {
            margin-top: -40px;
        }
        .recomendar {
            clear: both;
        }
        .btn-comprar svg {
            width: 40px;
            fill: white;
            position: absolute;
            left: 39%;
        }
    }
    @media all and (max-width: 500px) {
        .breadcrumbs {
            margin-top: -20px;
        }
        .product-price,
        .box-sabor,
        .product-doses,
        .logo-marca-view {
            width: 100%;
            margin-left: 0;
            margin-right: 0;
        }
        .product-price {
            margin-top: 0;
        }
        .box-sabor {
            margin-top: 0;
        }
        .btn-comprar svg {
            width: 40px;
            fill: white;
            position: absolute;
            left: 40%;
        }
    }
     @media all and (min-width: 1700px) {
        .btn-comprar svg {
            width: 40px;
            fill: white;
            position: absolute;
            left: 37%;
        }
    }
    @media all and (max-width: 1024px){
        .preco-por{
            font-size: 25px;
        }
    }

</style>

<script type="text/javascript">
    $(document).ready(function() {
        vermais = 0;

        $('#sabores-link').change(function() {
            endereco_destino = $('.change-propriedade:selected').attr('value');

            location.href = WEBROOT_URL + endereco_destino;
        });
        $('#vermais').click(function() {
            if(vermais == 0) {
                $('.tabela, .formula').css('max-height', '2000px');
                $(this).html('Ver menos');
                vermais = 1;
            } else {
                $('.tabela, .formula').css('max-height', '350px');
                $(this).html('Ver mais');
                vermais = 0;
            }
        });
    });
</script>

<!--DESCRICAO PRODUTO-->

<div class="container desc-produto" data-id="<?= $produto->slug ?>">
<div class="breadcrumbs"><a href="/academia/admin/express">Início </a><span class="fa fa-arrow-right setas"></span><a href="/academia/admin/express?m=<?= $produto->produto_base->marca->id ?>"> <?= $produto->produto_base->marca->name ?> </a><span class="fa fa-arrow-right setas"></span><a href="/academia/admin/express/<?= $produto->slug ?>"> <?= $produto->produto_base->name ?></a></div>
<div class="codigo"><p>Código: <?= strtoupper($produto->cod); ?></p></div>
    <div itemscope="" itemtype="http://schema.org/Product" class="row produto-div">
        <div>
            <div class="col-xs-6 row-bordas quadro-img-produto-principal">
                <?= $this->Html->image('produtos/md-'.$fotos[0], [
                    'id'    => 'foto-produto',
                    'val'   => $produto->slug,
                    'class' => 'img-produto-principal',
                    'alt'   => $produto->produto_base->name.' '.$produto->propriedade,
                ])?>
                <div class="col-xs-12 miniaturas">
                    <?php
                    $i = 0;
                    foreach($fotos as $key => $foto):
                        $i == 0 ? $active = 'miniatura-active' : $active = '';
                        echo '<div class="miniatura '.$active.'">';
                        echo '<span class="helper"></span>';
                        echo $this->Html->image('produtos/xs-'.$foto, [
                            'data-image'    => 'md-'.$foto,
                            'class'         => 'product-mini',
                            'alt'           => $produto->produto_base->name.' '.$produto->propriedade,
                        ]);
                        echo '</div>';
                        $i++;
                    endforeach;
                    ?>
                </div>
            </div>
        </div>
        <div>
            <div class="col-xs-6 col-sm-12 col-md-6 row-bordas quadro-desc-produto">
                <div>
                    <h1 class="product-view text-uppercase" itemprop="name">
                        <?= $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo.' '.$produto->propriedade; ?>
                    </h1>
                </div>
                <!-- Trustvox -->
                <a class="trustvox-fluid-jump trustvox-widget-rating" href="#_trustvox_widget" title="Pergunte e veja opiniões de quem já comprou">
                    <div class="trustvox-shelf-container" data-trustvox-product-code-js="<?= $produto->id ?>" data-trustvox-should-skip-filter="true" data-trustvox-display-rate-schema="true"></div><span class="rating-click-here">Clique e veja!</span>
                </a>
                <style type="text/css">
                .trustvox-widget-rating .ts-shelf-container,
                .trustvox-widget-rating .trustvox-shelf-container {display: inline-block;}
                .trustvox-widget-rating span.rating-click-here {
                  top: -3px;
                  display: inline-block;
                  position: relative;
                  color: #DAA81D;
                }
                .trustvox-widget-rating:hover span.rating-click-here {text-decoration: underline;}
                </style>

                <div class="row">
                    <div class="col-xs-6 col-xs-offset-6 logo-marca-view">
                        <a href="/professor-indicacao?m=<?= $produto->produto_base->marca->id ?>"><?= $this->Html->image('marcas/'.$produto->produto_base->marca->image, ['alt' => $produto->produto_base->marca->name, 'class' => 'logo-marca'])?></a>
                    </div>
                </div>

                <div class="row" style="padding-top: 15px">
                    <div class="col-xs-6 product-price">
                    <?php
                        $preco_de = $produto->preco;
                        $porcentagem_nova = $produto->preco * 0.089;
                    ?>
                    <span class="preco-de"><s>R$ <?= number_format($preco_de + $porcentagem_nova, 2, ',','.') ?></s></span>
                    <br>
                    <?php $produto->preco_promo ? $porcentagem_nova = $produto->preco_promo * $desconto_geral * 0.089 : $porcentagem_nova = $produto->preco * $desconto_geral * 0.089 ?>
                    <span class="preco-por">
                        R$ <?= $produto->preco_promo ?
                            number_format($preco = ($produto->preco_promo * $desconto_geral + $porcentagem_nova) * .75, 2, ',','.') :
                            number_format($preco = ($produto->preco * $desconto_geral + $porcentagem_nova) * .75, 2, ',','.'); ?>
                    </span>
                    </div>
                    <div class="col-xs-6 product-doses">
                    <span class="doses">
                        <?php if($produto->produto_base->doses > 0){
                            echo 'Doses: <strong>'.$produto->produto_base->doses.'</strong>';
                            echo '<br/>';
                            echo 'Valor por dose: <strong>R$ '. number_format(($preco / $produto->produto_base->doses), 2, ',','.').'</strong>';
                        } ?>
                    </span>
                    </div>
                </div>

                <?php if($produto->produto_base->propriedade_id && !empty($produto->propriedade)) { ?>
                <div class="row">
                    <div class="col-xs-12 box-sabor" style="font-size: 15px; padding-top: 5px;">
                        <p class="texto-sabor"><strong><?= ($produto->produto_base->propriedade_id && !empty($produto->propriedade)) ? $produto->produto_base->propriedade->name.':' : ''; ?></strong></p>

                        <select id="sabores-link">
                            <?= ($produto->produto_base->propriedade_id && !empty($produto->propriedade)) ?
                                '<option class="button-propriedade button-propriedade-active text-uppercase">'.$produto->propriedade.'</option>' : ''; ?>

                            <?php foreach($produtos as $product): ?>
                                <?= $this->Html->link(
                                    '<option class="button-propriedade text-uppercase change-propriedade" value="/produto_express/'.$product->slug.'">'.$product->propriedade.'</option>',
                                    ['controller' => 'Produtos', 'action' => 'view', $product->slug],
                                    ['escape' => false]
                                )?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <?php } ?>

                <div class="row">

                    <!--BOTAO COMPRAR-->
                    <div class="col-xs-12 comprar-geral">
                        <?php if($produto->visivel == 1 && $produto->status_id == 1) { ?>
                            <button id="comprar_express_id" val="<?= $produto->slug ?>" data-id="<?= $produto->id ?>" class="btn-comprar">
                                <div class="col-xs-12 text-center">
                                    <?= $this->Html->image('mochila_log_branca.png', ['class' => 'icone_comprar', 'alt' => 'icone mochila'])?>
                                    <span>COMPRAR</span>
                            </button>   
                            </div>
                            <?php } else if($produto->visivel == 1 && $produto->status_id == 2) { ?>
                            <span onclick="openAviseMe()" class="produto-acabou-btn" data-id="<?= $produto->id ?>"><button class="btn btn-avise"><i class="fa fa-envelope"></i> avise-me</button></span>
                            <div id="aviseMe-<?= $produto->id ?>" class="overlay-avise" onclick="closeAviseMe()">
                              <div class="overlay-avise-content overlay-avise-<?= $produto->id ?> text-center">
                                <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                <br />
                                <br />
                                <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$produto->id])?>
                                <?= $this->Form->input('produto_id', [
                                    'div'           => false,
                                    'label'         => false,
                                    'type'          => 'hidden',
                                    'val'           => $produto->id,
                                    'id'            => 'produto_id_acabou-'.$produto->id
                                ])?>
                                <?= $this->Form->input('name', [
                                    'div'           => false,
                                    'label'         => false,
                                    'id'            => 'name_acabou-'.$produto->id,
                                    'class'         => 'form-control validate[optional] name_acabou',
                                    'placeholder'   => 'Qual o seu nome?',
                                ])?>
                                <br/>
                                <?= $this->Form->input('email', [
                                    'div'           => false,
                                    'label'         => false,
                                    'id'            => 'email_acabou-'.$produto->id,
                                    'class'         => 'form-control validate[required, custom[email]]',
                                    'placeholder'   => 'Qual o seu email?',
                                ])?>
                                <br/>
                                <?= $this->Form->button('Enviar', [
                                    'data-id'       => $produto->id,
                                    'type'          => 'button',
                                    'class'         => 'btn btn-success send-acabou-btn'
                                ])?>
                                <br />
                                <br />
                                <?= $this->Form->end()?>
                              </div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="col-sm-12 text-right recomendar" >
                        <a class="col-sm-12 col-md-offset-6 col-md-6 text-right font-bold">
                            Indique para um amigo
                        </a>
                        <div
                            class="fb-like col-sm-12 col-xs-12"
                            data-href="<?= WEBROOT_URL.'produto/'.$produto->slug ?>"
                            data-layout="button"
                            data-action="recommend"
                            data-show-faces="true"
                            data-share="true">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<style type="text/css">
    @media all and (max-width: 1300px) {
        .produto-item {
            width: 33.33333333%;
        }
        #produto-4 {
            display: none;
        }
    }
    @media all and (max-width: 1050px) {
        .produto-item {
            width: 50%;
        }
        #produto-3 {
            display: none;
        }
    }
</style>

<?php if(count($produto_combinations) >= 1): ?>
    <!--COMBINA COM-->
    <div class="container combina-cont">
        <div class="col-sm-12 col-xs-12 combina-com">
            <div>
                <h2 class="h2-combina-com" style="padding-bottom: 10px">ESTE PRODUTO COMBINA COM</h2>
                <?php
                $i = 0;
                foreach($produto_combinations as $combination):
                    $fotos = unserialize($combination->fotos);
                    $i++;
                ?>
                <?php
                if($this->request->is('mobile')) {
                    if($i > 2) {
                        $hide = 'hide';
                    }
                } ?>
                <div class="<?= $hide; ?> col-xs-3 produto-item" id="produto-<?= $i ?>">
                    <div class="col-md-12 col-xs-12 text-center">
                        <div class="bg-white-produto col-xs-12">
                            <div class="box-imagem-produto">

                                    <?php $icone_ativado = 0; ?>
                                    <?php foreach($prod_objetivos as $p_objetivos): 
                                        if($p_objetivos->produto_base_id == $combination->produto_base->id && $icone_ativado == 0) {
                                        
                                            if($p_objetivos->objetivo_id == 1) { ?>
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 50.463 50.463" style="enable-background:new 0 0 50.463 50.463;" xml:space="preserve" width="20px" height="20px"><g><g><path d="M47.923,29.694c0.021-0.601-0.516-1.063-0.901-1.515c-0.676-2.733-2.016-5.864-3.961-8.971    C39.942,14.23,31.688,6.204,28.553,4.966c-0.158-0.062-0.299-0.097-0.429-0.126c-0.313-1.013-0.479-1.708-1.698-2.521    c-3.354-2.236-7.099-2.866-9.578-1.843c-2.481,1.023-3.859,6.687-1.19,8.625c2.546,1.857,7.583-1.888,9.195,0.509    c1.609,2.396,3.386,10.374,6.338,15.473c-0.746-0.102-1.514-0.156-2.307-0.156c-3.406,0-6.467,0.998-8.63,2.593    c-1.85-2.887-5.08-4.806-8.764-4.806c-3.82,0-7.141,2.064-8.95,5.13v22.619h4.879l1.042-1.849    c3.354-1.287,7.32-4.607,10.076-8.147C29.551,44.789,47.676,36.789,47.923,29.694z" fill="#5089cf"/></g></g></svg>
                                                <?php $icone_ativado = 1; ?>
                                            <?php } else if($p_objetivos->objetivo_id == 2) { ?>
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="20px" height="20px" viewBox="0 0 164.882 164.883" style="enable-background:new 0 0 164.882 164.883;" xml:space="preserve"><g><path d="M161.77,36.535h-34.86c5.286-11.953,11.972-19.461,12.045-19.543c1.157-1.272,1.065-3.249-0.207-4.402   c-1.267-1.16-3.239-1.065-4.396,0.201c-0.347,0.38-8.403,9.31-14.236,23.744H60.471c-1.72,0-3.118,1.395-3.118,3.118v39.948   c0,1.72,1.397,3.118,3.118,3.118h55.966c2.058,8.284,5.56,16.425,10.576,24.22c1.103,1.699,2.394,3.77,3.733,5.985   c-15.145,6.102-32.2,9.384-49.545,9.384c-0.006,0-0.012,0-0.018,0c-18,0-35.591-3.532-51.152-10.072   c1.178-1.941,2.338-3.763,3.31-5.297c32.15-49.922-6.933-93.712-7.338-94.147c-1.16-1.273-3.126-1.355-4.402-0.207   c-1.272,1.16-1.367,3.13-0.207,4.408c1.492,1.641,36.222,40.743,6.704,86.573c-6.043,9.377-17.284,26.84-15.396,46.722   c0.149,1.613,1.51,2.819,3.1,2.819c0.101,0,0.201,0,0.298-0.013c1.717-0.158,2.98-1.687,2.807-3.397   c-1.136-11.947,3.255-23.176,7.968-31.943c16.492,7.039,35.204,10.783,54.297,10.783c0.006,0,0.012,0,0.019,0   c18.44,0,36.586-3.525,52.649-10.113c4.561,8.67,8.708,19.625,7.6,31.268c-0.158,1.711,1.096,3.239,2.801,3.397   c0.109,0.013,0.201,0.013,0.305,0.013c1.595,0,2.953-1.206,3.093-2.82c1.906-19.881-9.353-37.344-15.393-46.728   c-4.348-6.747-7.441-13.743-9.39-20.846h38.909c1.718,0,3.118-1.397,3.118-3.118V39.638   C164.888,37.918,163.494,36.535,161.77,36.535z M158.653,76.478h-6.51V63.732c0-1.72-1.4-3.117-3.117-3.117   c-1.724,0-3.117,1.397-3.117,3.117v12.745h-8.708V63.732c0-1.72-1.395-3.117-3.117-3.117c-1.718,0-3.118,1.397-3.118,3.117v12.745   h-8.701V63.732c0-1.72-1.406-3.117-3.118-3.117c-1.723,0-3.117,1.397-3.117,3.117v12.745h-8.714V63.732   c0-1.72-1.395-3.117-3.117-3.117c-1.718,0-3.118,1.397-3.118,3.117v12.745h-8.688V63.732c0-1.72-1.404-3.117-3.118-3.117   c-1.723,0-3.118,1.397-3.118,3.117v12.745h-8.705V63.732c0-1.72-1.397-3.117-3.117-3.117c-1.717,0-3.118,1.397-3.118,3.117v12.745   h-7.639V42.765h95.07v33.713H158.653z M80.176,108.765c-3.635,0-6.585-2.953-6.585-6.582c0-3.635,2.95-6.582,6.585-6.582   s6.576,2.947,6.576,6.582C86.746,105.812,83.811,108.765,80.176,108.765z M10.564,68.022v-6.658H0V44.808h10.564v-6.658   l14.94,14.939L10.564,68.022z" fill="#5089cf"/></g></svg>
                                                <?php $icone_ativado = 1; ?>
                                            <?php } else if($p_objetivos->objetivo_id == 3) { ?>
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="20px" height="20px"><g><g><path d="M400.268,175.599c-1.399-3.004-4.412-4.932-7.731-4.932h-101.12l99.797-157.568c1.664-2.628,1.766-5.956,0.265-8.678    C389.977,1.69,387.109,0,384.003,0H247.47c-3.234,0-6.187,1.826-7.637,4.719l-128,256c-1.323,2.637-1.178,5.777,0.375,8.294    c1.562,2.517,4.301,4.053,7.262,4.053h87.748l-95.616,227.089c-1.63,3.883-0.179,8.388,3.413,10.59    c1.382,0.845,2.918,1.254,4.446,1.254c2.449,0,4.864-1.05,6.537-3.029l273.067-324.267    C401.206,182.161,401.667,178.611,400.268,175.599z" fill="#5089cf"/>
                                                    </g></g></svg>
                                                <?php $icone_ativado = 1; ?>
                                            <?php } else if($p_objetivos->objetivo_id == 4) { ?>
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 511.998 511.998" style="enable-background:new 0 0 511.998 511.998;" xml:space="preserve" width="20px" height="20px"><g><g><path d="M396.8,236.586h-93.175l-22.673-68.053c-1.775-5.325-6.929-9.523-12.424-8.747c-5.623,0.128-10.496,3.874-12.023,9.276    l-37.35,130.697l-40.252-181.146c-1.229-5.478-5.871-9.523-11.477-9.975c-5.572-0.375-10.829,2.773-12.902,7.996l-48,119.953H12.8    c-7.074,0-12.8,5.726-12.8,12.8c0,7.074,5.726,12.8,12.8,12.8h102.4c5.222,0.008,9.95-3.174,11.878-8.047l35.823-89.523    l42.197,189.952c1.271,5.726,6.272,9.847,12.126,10.027c0.128,0,0.247,0,0.375,0c5.7,0,10.726-3.772,12.297-9.276l39.851-139.426    l12.501,37.547c1.749,5.222,6.647,8.747,12.151,8.747h102.4c7.074,0,12.8-5.726,12.8-12.8    C409.6,242.311,403.874,236.586,396.8,236.586z" fill="#5089cf"/></g></g><g><g><path d="M467.012,64.374C437.018,34.38,397.705,19.387,358.4,19.387c-36.779,0-73.259,13.662-102.4,39.919    c-29.15-26.257-65.621-39.919-102.4-39.919c-39.313,0-78.618,14.993-108.612,44.988C5.214,104.157-7.595,160.187,5.385,211.011    h26.624c-13.653-43.972-3.678-93.773,31.078-128.529c24.175-24.175,56.32-37.487,90.513-37.487    c31.206,0,60.399,11.563,83.695,31.889L256,94.369l18.714-17.493c23.296-20.318,52.489-31.889,83.686-31.889    c34.193,0,66.33,13.312,90.513,37.487c49.911,49.903,49.903,131.115,0,181.018L256,456.404L87.407,287.811H51.2l204.8,204.8    l211.012-211.012C526.993,221.61,526.993,124.364,467.012,64.374z" fill="#5089cf"/></g></g></svg>
                                                <?php $icone_ativado = 1; ?>
                                            <?php }
                                        
                                        }
                                    endforeach; ?>

                                <?php if($combination->preco_promo) { ?>
                                    <div class="box-promocao">    
                                        <span>PROMOÇÃO</span>
                                    </div>
                                <?php } ?> 

                                <?= $this->Html->link(
                                    $this->Html->image(
                                        'produtos/'.$fotos[0],
                                        ['class' => 'produto-grade', 'val' => $combination->slug, 'alt' => $combination->produto_base->name.' '.$combination->produto_base->embalagem_conteudo]),
                                    $SSlug.'/produto_express/'.$combination->slug,
                                    ['escape' => false]
                                ); ?>
                            </div>

                            <div class="box-info-produto">
                                <p class="description">
                                    <span class="product"><?= $combination->produto_base->name; ?></span>
                                </p>

                                <?php if($combination->produto_base->propriedade_id && !empty($combination->propriedade)) { ?>
                                <div class="col-xs-12" style="height: 27px">
                                    <?= ($combination->produto_base->propriedade_id && !empty($combination->propriedade)) ?
                                    '<p class="sabor-info text-uppercase">'.$combination->propriedade.'</p>' : ''; ?>
                                </div>
                                <?php } else { ?>
                                    <p class="sabor-info text-uppercase">&nbsp;</p>
                                <?php } ?>

                                <div class="box-fundo-produto">
                                    <div class="box-preco">                      
                                        <p class="preco preco-desc">
                                            <?php $porcentagem_nova = $combination->preco * 0.089 ?>
                                            R$ <span style="text-decoration: line-through"><?= number_format($preco = $combination->preco + $porcentagem_nova, 2, ',','.'); ?></span>
                                        </p>

                                        <p class="preco preco-normal">
                                            <?php $combination->preco_promo ? $porcentagem_nova = $combination->preco_promo * $desconto_geral * 0.089 : $porcentagem_nova = $combination->preco * $desconto_geral * 0.089 ?>
                                            R$ <span><?= $combination->preco_promo ?
                                                number_format($preco = ($combination->preco_promo * $desconto_geral + $porcentagem_nova) * .75, 2, ',','.') :
                                                number_format($preco = ($combination->preco * $desconto_geral + $porcentagem_nova) * .75, 2, ',','.'); ?></span>
                                        </p>
                                    </div>
                                    <div class="box-mochila-add">
                                        <svg val="<?= $combination->slug ?>" data-id="<?= $combination->id ?>" class="indicar-mochila-express" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"><g><polygon points="8.669,48.706 5,62.65 6.565,64.773 10.008,54.727  "/><polygon points="93.422,64.79 95,62.65 91.331,48.706 89.991,54.727  "/><polygon points="41.311,14.947 39.661,15.937 37.462,30.863 37.071,40.502 39.622,40.182 41.47,14.947  "/><polygon points="62.928,40.51 62.538,30.863 60.338,15.937 58.689,14.947 58.529,14.947 60.378,40.188  "/><polygon points="29.01,42.523 21.586,43.458 9.53,47.966 11.045,54.785 5.5,70.967 5.5,88.654 27.14,88.654  "/><polygon points="36.48,55.079 35.119,88.654 64.879,88.654 63.519,55.079  "/><polygon points="52.475,40.195 47.463,40.199 37.03,41.513 36.716,49.244 63.282,49.244 62.969,41.516  "/><polygon points="88.954,54.785 90.47,47.966 78.413,43.458 70.989,42.524 72.859,88.654 94.5,88.654 94.5,70.967  "/><path d="M28.12,89.154h5.978l1.422-35.075h28.96L65.9,89.154h5.979l-2.368-58.421L65.62,15.752l-7.219-4.906h-7.104h-2.595h-7.105   l-7.218,4.906l-3.891,14.981L28.12,89.154z M51.578,13.947h7.388l2.256,1.354l2.312,15.433l0.79,19.511H35.675l0.792-19.511   l2.311-15.433l2.257-1.354h7.386H51.578z"/></g></svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <?php
else:
    echo '<br class="clear" />';
endif;
?>

<div class="formula-text">
    <div class="col-xs-12 atributos-produto">
        <h4 class="text-center">FÓRMULA</h4>
    </div>
    <div class="formula col-xs-12">
        <div>
            <?= $produto->produto_base->atributos; ?>
        </div>
        <p class="tabela text-center">
            <?= $produto->produto_base->dicas; ?>
        </p>
    </div>
    <div class="col-xs-12" style="text-align: center; padding-bottom: 30px">
        <button id="vermais" class="btn btn-cupom">Ver mais</button>
    </div>

    <style type="text/css">
        #_trustvox_widget {
            z-index: 0;
        }
        #_trustvox_widget .ts-widget .ts-footer {
            background: #5089cf!important;
        }
    </style>

    <div id="_trustvox_widget"></div>
</div>

<!-- <div class="container prod-fim" >
    <div>
        <div class="col-sm-6 avaliacoes stars">
            <h3>Avaliações</h3>
            <?php
            if(count($produto->produto_comentarios) <= 0){
                echo '<span><strong>Nenhuma avaliação.<br>Seja o primeiro a avaliar este produto!</strong></span>';
            }else {
                foreach ($produto->produto_comentarios as $avaliacao):
                    ?>
                    <div>
                        <span><strong><?= $avaliacao->name; ?></strong></span>
                        <?= $this->Form->input('stars', [
                            'label' => false,
                            'div' => false,
                            'class' => 'rating rating-loading',
                            'id' => 'input-7-xs',
                            'data-show-clear' => 'false',
                            'data-show-caption' => 'false',
                            'data-min' => 0,
                            'data-disabled' => 'true',
                            'data-max' => 5,
                            'data-step' => 1,
                            'data-size' => 'xs',
                            'value' => $avaliacao->stars
                        ]); ?>
                        <p><strong><?= $avaliacao->title; ?></strong></p>
                        <p><?= $avaliacao->comentario; ?></p>
                    </div>
                    <hr>
                    <?php
                endforeach;
            }
            ?>
        </div>

        <div class="col-sm-6 avaliacoes">
            <?= $this->Form->create(null, ['id' => 'aval-form', 'default' => false])?>
            <h3>Deixe sua avaliação</h3>
            <div class="margin-recomenda">
                <span style="padding: 0 15px 0 0;">
                    <strong>Recomenda este produto?</strong>
                </span>
                <br>
                <?= $this->Form->radio('recomenda',
                    [
                        [
                            'value' => 1,
                            'text' => 'Sim',
                            'class' => 'text-left col-sm-12 radio-aval validate[required]',
                            'checked'   => true
                        ],
                        ['value' => 0, 'text' => 'Não', 'class' => 'text-left col-sm-12 radio-aval validate[required]']
                    ]
                ); ?>
            </div>
            <div class="stars-aval">
                <strong> Sua Avaliação Geral</strong>
                <?= $this->Form->input('stars', [
                    'label' => false,
                    'div'   => false,
                    'class' => 'rating rating-loading',
                    'id'    => 'input-7-xs',
                    'data-show-clear'   => 'false',
                    'data-show-caption' => 'false',
                    'data-min'          => 0,
                    'data-max'          => 5,
                    'data-step'         => 1,
                    'data-size'         => 'xs',
                    'value'             => 1
                ]); ?>
            </div>
            <div>
                <div class="margin-20">
                    <span><strong>Seu Nome:</strong></span>
                    <?= $this->Form->input('produto_id', [
                        'type'  => 'hidden',
                        'value' => $produto->id
                    ]); ?>

                    <?= $this->Form->input('name', [
                        'label' => false,
                        'div'   => false,
                        'style' => 'width: 240px;',
                        'class' => 'validate[required]',
                        'placeholder' => 'Nome completo'
                    ]); ?>
                </div>
                <div class="margin-20">
                    <span><strong>E-mail:</strong></span>
                    <?= $this->Form->input('email', [
                        'type'  => 'email',
                        'label' => false,
                        'div'   => false,
                        'style' => 'width: 265px;',
                        'class' => 'validate[required]',
                        'placeholder' => 'Endereço de Email'
                    ]); ?>
                </div>
                <div class="margin-20">
                    <span><strong>Titulo da Avaliação:</strong></span>
                    <?= $this->Form->input('title', [
                        'label' => false,
                        'div'   => false,
                        'style' => 'width: 177px;',
                        'class' => 'validate[required]',
                        'placeholder' => 'Título da Avaliação'
                    ]); ?>
                </div>
                <div class="margin-20">
                    <span><strong>Escreva uma mensagem abaixo</strong></span>
                    <?= $this->Form->input('comentario', [
                        'label' => false,
                        'div'   => false,
                        'type'  => 'textarea',
                        'class' => 'caixa-text-avaliacao validate[required]',
                        'rows'  => 2,
                        'placeholder'   =>
                            'Escreva sua avaliação sobre o produto '
                            .$produto->produto_base->name.' '
                            .$produto->produto_base->embalagem_conteudo.' '
                            .$produto->propriedade,
                    ]); ?>
                </div>
                <div>
                    <span id="produto-avaliacao">
                        <?= $this->Form->button('ENVIAR', [
                            'id'    => 'aval-submit',
                            'type'  => 'button',
                            'class' => 'button-enviar',
                        ]); ?>
                    </span>
                </div>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div> -->