<script type="text/javascript">
    var search_form = $('.search-form');

    $('#comprar_id, .incluir-mochila').click(function(e) {
        e.preventDefault();
        loading.show(1);

        var url_comprar = $(this).attr('val');
        $.get(WEBROOT_URL + 'produtos/comprar/' + url_comprar,
            function (data) {
                $('.laterais.mochila').html('');
                $('.laterais.mochila').html(data);
                $('.produtos-mochila').animate({ 'width' : '228px', 'opacity' : 1 }, function() {
                    $('.info-direita-mochila').fadeIn(200);
                });
                if($('.laterais.mochila').width() < 200) {
                    $('#close-mochila').click();
                }
                loading.hide(1);
            });
    });

    $('.img-produto-principal, .img-combina-com, .produto-grade-drag').draggable({
        helper: 'clone'
    });


    $('.produtos-mochila').droppable({

        drop: function(evt, ui){

            if(ui.draggable.parent()[0] == this){
                return;
            }

            var t = $(this);
            var e = ui.draggable;
            var diff = {x: evt.pageX - ui.position.left, y: evt.pageY - ui.position.top};

            e.draggable('option','helper','');

            $.get(WEBROOT_URL + 'produtos/comprar/' + e.attr('val'),
                function (data) {
                    $('.laterais.mochila').html('');
                    $('.laterais.mochila').html(data);
                    $('.produtos-mochila').animate({ 'width' : '228px', 'opacity' : 1 }, function() {
                        $('.info-direita-mochila').fadeIn(200);
                    });
                    loading.hide(1);
                });
        }
    });

    $('#limpar-filtro').click(function() {
        loading.show(1);
        var marca_id = 0;
        var categoria_id = 0;
        var objetivo_id = 0;
        $('#filtro-ordenar').val(0);

        $('.search-query').val('');
        
        $.get(WEBROOT_URL + 'produtos/limpar-filtro/',
            function (data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);

                $('#grade-produtos').html(data);
                loading.hide(1);
            });

        $('#new-produtos-load-more').show();
    });

    $('.query_selected').click(function() {
        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0,valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';')+1);
        var ordenar_id = $('#filtro-ordenar').val();
        loading.show(1);

        $('.search-query').val('');

        if($('.marca_selected')) {
            marca_id = $('.marca_selected').attr('data-id');
        } else {
            marca_id = "0";
        }

        if($('.categoria_selected')) {
            categoria_id = $('.categoria_selected').attr('data-id');
            objetivo_id = $('.categoria_selected').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        $.ajax({
            type: "POST",
            url: WEBROOT_URL + '/produtos/busca-completa/',
            data: {query: $('.search-query').val(), objetivo: objetivo_id, categoria: categoria_id, marca: marca_id, ordenar: ordenar_id, valor_min: valor_min_id, valor_max: valor_max_id}
        })
            .done(function (data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);
                $('#grade-produtos').html(data);
                loading.hide(1);
                $('#new-produtos-load-more').show();
            });
    });

    $('.marca_selected').click(function() {
        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0,valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';')+1);
        var ordenar_id = $('#filtro-ordenar').val();
        loading.show(1);

        marca_id = "0";

        if($('.categoria_selected')) {
            categoria_id = $('.categoria_selected').attr('data-id');
            objetivo_id = $('.categoria_selected').attr('obj-id');
        } else {
            categoria_id = "0";
            objetivo_id = "0";
        }

        $.ajax({
            type: "POST",
            url: WEBROOT_URL + '/produtos/busca-completa/',
            data: {query: $('.search-query').val(), objetivo: objetivo_id, categoria: categoria_id, marca: marca_id, ordenar: ordenar_id, valor_min: valor_min_id, valor_max: valor_max_id}
        })
            .done(function (data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);
                $('#grade-produtos').html(data);
                loading.hide(1);
                $('#new-produtos-load-more').show();
            });
    });

    $('.categoria_selected').click(function() {
        var valor_id = $('#value-slider').val();
        var valor_min_id = valor_id.substring(0,valor_id.indexOf(';'));
        var valor_max_id = valor_id.substring(valor_id.indexOf(';')+1);
        var ordenar_id = $('#filtro-ordenar').val();
        loading.show(1);

        categoria_id = "0";
        objetivo_id = "0";

        if($('.marca_selected')) {
            marca_id = $('.marca_selected').attr('data-id');
        } else {
            marca_id = "0";
        }

        $.ajax({
            type: "POST",
            url: WEBROOT_URL + '/produtos/busca-completa/',
            data: {query: $('.search-query').val(), objetivo: objetivo_id, categoria: categoria_id, marca: marca_id, ordenar: ordenar_id, valor_min: valor_min_id, valor_max: valor_max_id}
        })
            .done(function (data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);
                $('#grade-produtos').html(data);
                loading.hide(1);
                $('#new-produtos-load-more').show();
            });
    });

    if($('.filtragem a').length == 0) {
        $('.voltar-link').remove();
    }

    $('#comprar_id, .incluir-mochila').click(function(e) {
        $('.produtos-mochila').animate({ 'width' : '228px', 'opacity' : 1 }, function() {
            $('.info-direita-mochila').fadeIn(200);
        });
        if($('.laterais.mochila').width() < 200) {
            $('.close-mochila').click();
        }
    });

    function get_produtos_valor(ordenar, valor_minimo, valor_maximo){
        $.get(WEBROOT_URL + 'produtos/filtro-ordenar/' + ordenar + '/' + valor_minimo + '/' + valor_maximo,
            function (data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);

                $('#grade-produtos').html(data);
                loading.hide(1);
            });
    }

    function get_produtos_ordenar(ordenar, valor_minimo, valor_maximo){
        $.get(WEBROOT_URL + 'produtos/filtro-ordenar/' + ordenar + '/' + valor_minimo + '/' + valor_maximo,
            function (data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125

                }, 700);

                $('#grade-produtos').html(data);
                loading.hide(1);
            });
    }

    function get_produtos_marca(marca, ordenar, valor_minimo, valor_maximo){
        $.get(WEBROOT_URL + '/produtos/filtro-marca/' + marca + '/' + ordenar + '/' + valor_minimo +'/' + valor_maximo,
            function (data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);

                $('#grade-produtos').html(data);
                loading.hide(1);
            });
    }

    function get_produtos_objetivo_categoria_ordenar(objetivo, categoria, ordenar, valor_minimo, valor_maximo){
        $.get(WEBROOT_URL + 'produtos/filtro-objetivo-categoria-ordenar/' + objetivo + '/' + categoria + '/' + ordenar + '/' + valor_minimo +'/' + valor_maximo,
            function (data) {
                $('html, body').animate({
                    scrollTop: $('#grade-produtos').offset().top - 125
                }, 700);

                $('#grade-produtos').html(data);
                loading.hide(1);
            });
    }
    function openNav() {
        document.getElementById("myNav").style.height = "100%";
    }

    function closeNav() {
        document.getElementById("myNav").style.height = "0%";
    }

    function openAviseMe(id) {
        document.getElementById("aviseMe-"+id).style.height = "100%";
    }

    function closeAviseMe() {
        $(".overlay-avise").height("0%");
    }
    function closeOverlay() {
        $(".overlay").height("0%");
    }

    $('.produto-acabou-btn').click(function(e) {
        e.preventDefault();
        openAviseMe($(this).attr('data-id'));
    });

    $('.overlay-avise, .overlay-avise-content').click(function(e) {
        e.preventDefault();
    });

    $('.overlay-avise-content').click(function(e) {
        e.stopPropagation();
    });

    $('#send-indicacao-btn').click(function(e) {
        e.preventDefault();
        loading.show(1);

        var valid = $('#indicar_prod').validationEngine("validate");
        if (valid == true) {
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + 'produtos/overlay-indicar-produtos/',
                    data: {
                        name: $('#name').val(),
                        email: $('#email').val(), 
                        url: $('#url').val(),
                        produto_slug: $('.container.desc-produto').attr('data-id')
                    }
                })
                    .done(function (data) {
                        $('.overlay-content').html(data);
                        loading.hide(1);
                    });
        }else{
            $('#indicar_prod').validationEngine({
                updatePromptsPosition: true,
                promptPosition: 'inline',
                scroll: false
            });
        }
    });

    $('.send-acabou-btn').click(function(e) {
        e.preventDefault();
        loading.show(1);
        var id = $(this).attr('data-id');
        var form = $('#produto_acabou-'+id);

        var valid = form.validationEngine("validate");
        if (valid == true) {
            $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + 'produtos/produto-acabou/',
                    data: {
                        name: $('#name_acabou-'+id).val(),
                        email: $('#email_acabou-'+id).val(), 
                        produto_id: $('#produto_id_acabou-'+id).val()   
                    }
                })
                    .done(function (data) {
                        if(data >= 1) {
                            $('.overlay-avise-'+data).html('Sucesso!');
                        } else {
                            $('.overlay-avise-'+data).html('Erro!');
                        }
                        loading.hide(1);
                    });
        }else{
            form.validationEngine({
                updatePromptsPosition: true,
                promptPosition: 'inline',
                scroll: false
            });
        }
    });
</script>

<style type="text/css">
    .voltar-link {
        text-align: right; 
        padding-right: 35px; 
        padding-bottom: 10px;
    }
</style>

<div class="col-xs-12 voltar-link">
    <div class="col-xs-8" style="float: left; text-align: left">
        <p class="filtragem">
            <?php if($marca_selected) { ?>
                <a data-id="<?= $marca_selected->id ?>" class="btn btn-cupom marca_selected"><?= $marca_selected->name; ?> <i class="fa fa-close"></i> </a>
            <?php } ?>
            <?php if($categoria_selected) { ?>
                <a data-id="<?= $categoria_selected->id ?>" obj-id="<?= $objetivo_selected->id ?>" class="btn btn-cupom categoria_selected"><i style="width: 31px; float: left;"><?= $objetivo_selected->image; ?></i> <?= $categoria_selected->name; ?> <i class="fa fa-close"></i> </a>
            <?php } ?>
            <?php if($query) { ?>
                <a class="btn btn-cupom query_selected"><?= $query; ?> <i class="fa fa-close"></i> </a>
            <?php } ?>
        </p>
    </div>
    <a id="limpar-filtro" class="btn btn-cupom"><span class="fa fa-close"></span> Limpar filtro</a>
</div>

<?php
$i = 1;
foreach($produtos as $produto):
    if($i > 24){$hide = 'hide';}else{$hide = '';}
    if($produto->visivel == 1 && $produto->preco >= 0.1):
        $fotos = unserialize($produto->fotos);
        $produto->produto_base->propriedade_id ? $propriedade = $produto->propriedade : $propriedade = '';
        ?>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 total-produto-box" id="produto-<?= $i ?>">
            <div class="col-md-12 col-xs-12 text-center bg-white-produto prdt" itemtype="http://schema.org/Product">
                <div class="categoria">
                         <?php $icone_ativado = 0; ?>
                            <?php foreach($prod_objetivos as $p_objetivos): 
                                if($p_objetivos->produto_base_id == $produto->produto_base->id && $icone_ativado == 0) {
                                
                                    if($p_objetivos->objetivo_id == 1) { ?>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 50.463 50.463" style="enable-background:new 0 0 50.463 50.463;" xml:space="preserve" width="20px" height="20px"><g><g><path d="M47.923,29.694c0.021-0.601-0.516-1.063-0.901-1.515c-0.676-2.733-2.016-5.864-3.961-8.971    C39.942,14.23,31.688,6.204,28.553,4.966c-0.158-0.062-0.299-0.097-0.429-0.126c-0.313-1.013-0.479-1.708-1.698-2.521    c-3.354-2.236-7.099-2.866-9.578-1.843c-2.481,1.023-3.859,6.687-1.19,8.625c2.546,1.857,7.583-1.888,9.195,0.509    c1.609,2.396,3.386,10.374,6.338,15.473c-0.746-0.102-1.514-0.156-2.307-0.156c-3.406,0-6.467,0.998-8.63,2.593    c-1.85-2.887-5.08-4.806-8.764-4.806c-3.82,0-7.141,2.064-8.95,5.13v22.619h4.879l1.042-1.849    c3.354-1.287,7.32-4.607,10.076-8.147C29.551,44.789,47.676,36.789,47.923,29.694z" fill="#5089cf"/></g></g></svg>
                                        <?php $icone_ativado = 1; ?>
                                    <?php } else if($p_objetivos->objetivo_id == 2) { ?>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="20px" height="20px" viewBox="0 0 164.882 164.883" style="enable-background:new 0 0 164.882 164.883;" xml:space="preserve"><g><path d="M161.77,36.535h-34.86c5.286-11.953,11.972-19.461,12.045-19.543c1.157-1.272,1.065-3.249-0.207-4.402   c-1.267-1.16-3.239-1.065-4.396,0.201c-0.347,0.38-8.403,9.31-14.236,23.744H60.471c-1.72,0-3.118,1.395-3.118,3.118v39.948   c0,1.72,1.397,3.118,3.118,3.118h55.966c2.058,8.284,5.56,16.425,10.576,24.22c1.103,1.699,2.394,3.77,3.733,5.985   c-15.145,6.102-32.2,9.384-49.545,9.384c-0.006,0-0.012,0-0.018,0c-18,0-35.591-3.532-51.152-10.072   c1.178-1.941,2.338-3.763,3.31-5.297c32.15-49.922-6.933-93.712-7.338-94.147c-1.16-1.273-3.126-1.355-4.402-0.207   c-1.272,1.16-1.367,3.13-0.207,4.408c1.492,1.641,36.222,40.743,6.704,86.573c-6.043,9.377-17.284,26.84-15.396,46.722   c0.149,1.613,1.51,2.819,3.1,2.819c0.101,0,0.201,0,0.298-0.013c1.717-0.158,2.98-1.687,2.807-3.397   c-1.136-11.947,3.255-23.176,7.968-31.943c16.492,7.039,35.204,10.783,54.297,10.783c0.006,0,0.012,0,0.019,0   c18.44,0,36.586-3.525,52.649-10.113c4.561,8.67,8.708,19.625,7.6,31.268c-0.158,1.711,1.096,3.239,2.801,3.397   c0.109,0.013,0.201,0.013,0.305,0.013c1.595,0,2.953-1.206,3.093-2.82c1.906-19.881-9.353-37.344-15.393-46.728   c-4.348-6.747-7.441-13.743-9.39-20.846h38.909c1.718,0,3.118-1.397,3.118-3.118V39.638   C164.888,37.918,163.494,36.535,161.77,36.535z M158.653,76.478h-6.51V63.732c0-1.72-1.4-3.117-3.117-3.117   c-1.724,0-3.117,1.397-3.117,3.117v12.745h-8.708V63.732c0-1.72-1.395-3.117-3.117-3.117c-1.718,0-3.118,1.397-3.118,3.117v12.745   h-8.701V63.732c0-1.72-1.406-3.117-3.118-3.117c-1.723,0-3.117,1.397-3.117,3.117v12.745h-8.714V63.732   c0-1.72-1.395-3.117-3.117-3.117c-1.718,0-3.118,1.397-3.118,3.117v12.745h-8.688V63.732c0-1.72-1.404-3.117-3.118-3.117   c-1.723,0-3.118,1.397-3.118,3.117v12.745h-8.705V63.732c0-1.72-1.397-3.117-3.117-3.117c-1.717,0-3.118,1.397-3.118,3.117v12.745   h-7.639V42.765h95.07v33.713H158.653z M80.176,108.765c-3.635,0-6.585-2.953-6.585-6.582c0-3.635,2.95-6.582,6.585-6.582   s6.576,2.947,6.576,6.582C86.746,105.812,83.811,108.765,80.176,108.765z M10.564,68.022v-6.658H0V44.808h10.564v-6.658   l14.94,14.939L10.564,68.022z" fill="#5089cf"/></g></svg>
                                        <?php $icone_ativado = 1; ?>
                                    <?php } else if($p_objetivos->objetivo_id == 3) { ?>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="20px" height="20px"><g><g><path d="M400.268,175.599c-1.399-3.004-4.412-4.932-7.731-4.932h-101.12l99.797-157.568c1.664-2.628,1.766-5.956,0.265-8.678    C389.977,1.69,387.109,0,384.003,0H247.47c-3.234,0-6.187,1.826-7.637,4.719l-128,256c-1.323,2.637-1.178,5.777,0.375,8.294    c1.562,2.517,4.301,4.053,7.262,4.053h87.748l-95.616,227.089c-1.63,3.883-0.179,8.388,3.413,10.59    c1.382,0.845,2.918,1.254,4.446,1.254c2.449,0,4.864-1.05,6.537-3.029l273.067-324.267    C401.206,182.161,401.667,178.611,400.268,175.599z" fill="#5089cf"/>
                                            </g></g></svg>
                                        <?php $icone_ativado = 1; ?>
                                    <?php } else if($p_objetivos->objetivo_id == 4) { ?>
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 511.998 511.998" style="enable-background:new 0 0 511.998 511.998;" xml:space="preserve" width="20px" height="20px"><g><g><path d="M396.8,236.586h-93.175l-22.673-68.053c-1.775-5.325-6.929-9.523-12.424-8.747c-5.623,0.128-10.496,3.874-12.023,9.276    l-37.35,130.697l-40.252-181.146c-1.229-5.478-5.871-9.523-11.477-9.975c-5.572-0.375-10.829,2.773-12.902,7.996l-48,119.953H12.8    c-7.074,0-12.8,5.726-12.8,12.8c0,7.074,5.726,12.8,12.8,12.8h102.4c5.222,0.008,9.95-3.174,11.878-8.047l35.823-89.523    l42.197,189.952c1.271,5.726,6.272,9.847,12.126,10.027c0.128,0,0.247,0,0.375,0c5.7,0,10.726-3.772,12.297-9.276l39.851-139.426    l12.501,37.547c1.749,5.222,6.647,8.747,12.151,8.747h102.4c7.074,0,12.8-5.726,12.8-12.8    C409.6,242.311,403.874,236.586,396.8,236.586z" fill="#5089cf"/></g></g><g><g><path d="M467.012,64.374C437.018,34.38,397.705,19.387,358.4,19.387c-36.779,0-73.259,13.662-102.4,39.919    c-29.15-26.257-65.621-39.919-102.4-39.919c-39.313,0-78.618,14.993-108.612,44.988C5.214,104.157-7.595,160.187,5.385,211.011    h26.624c-13.653-43.972-3.678-93.773,31.078-128.529c24.175-24.175,56.32-37.487,90.513-37.487    c31.206,0,60.399,11.563,83.695,31.889L256,94.369l18.714-17.493c23.296-20.318,52.489-31.889,83.686-31.889    c34.193,0,66.33,13.312,90.513,37.487c49.911,49.903,49.903,131.115,0,181.018L256,456.404L87.407,287.811H51.2l204.8,204.8    l211.012-211.012C526.993,221.61,526.993,124.364,467.012,64.374z" fill="#5089cf"/></g></g></svg>
                                        <?php $icone_ativado = 1; ?>
                                    <?php }
                                
                                }
                            endforeach; ?>
                    </div>
                <!-- <div class="bg-white-produto prdt" itemscope="" itemtype="http://schema.org/Product"> -->
                    <div class="box-imagem-produto">

                        <?php if($produto->estoque == 1) { ?>
                            <div class="box-ultimo">    
                                <span>ÚLTIMO</span>
                            </div> 
                        <?php }?>   

                        <?php if($produto->estoque == 1 && $produto->created > $max_date_novidade) { ?>
                            <div class="box-novidade" style="top: 19px;">    
                                <span>NOVIDADE</span>
                            </div>
                        <?php } else if($produto->created > $max_date_novidade) { ?>
                            <div class="box-novidade">    
                                <span>NOVIDADE</span>
                            </div>
                        <?php } ?>

                        <?php if(!($produto->estoque == 1 && $produto->created > $max_date_novidade) && $produto->preco_promo) { ?>
                            <?php if($produto->estoque == 1 && $produto->preco_promo || $produto->created > $max_date_novidade && $produto->preco_promo) { ?>
                                <div class="box-promocao" style="top: 19px;">    
                                    <span>PROMOÇÃO</span>
                                </div>
                            <?php } elseif ($produto->preco_promo) { ?>
                                <div class="box-promocao">    
                                    <span>PROMOÇÃO</span>
                                </div>
                            <?php } ?>
                        <?php } ?>

                        <?php if($produto->visivel == 1 && $produto->status_id == 1) { ?>
                            <?= $this->Html->link(
                                $this->Html->image(
                                    'produtos/sm-'.$fotos[0],
                                    ['class' => 'produto-grade produto-grade-drag', 'val' => $produto->slug, 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]),
                                $SSlug.'/produto/'.$produto->slug,
                                ['escape' => false]
                            ); ?>
                        <?php } else if($produto->visivel == 1 && $produto->status_id == 2) { ?>
                            <?= $this->Html->link(
                                $this->Html->image(
                                    'produtos/sm-'.$fotos[0],
                                    ['class' => 'produto-grade', 'val' => $produto->slug, 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]),
                                $SSlug.'/produto/'.$produto->slug,
                                ['escape' => false]
                            ); ?>
                        <?php } ?>
                    </div>
                    <div class="box-info-produto">
                        <script>
                            $('.btn-indique').click(function () {
                                    var id_produto = $(this).attr('data-id');
                                    $('#overlay-indicacao-'+id_produto).height('100%');
                                    $("html,body").css({"overflow":"hidden"});
                            });
                            function closeIndica() {
                                $('.overlay-indicacao').height('0%');
                                $("html,body").css({"overflow":"auto"});
                            }
                        </script>
                    <a href="<?= $SSlug ?>/produto/<?= $produto->slug ?>" class="link-prdt">
                        <div class="description">
                            <span class="product" itemprop="name"><?= $produto->produto_base->name; ?></span>
                        </div>

                        <?php if($produto->produto_base->propriedade_id && !empty($produto->propriedade)) { ?>
                        <div class="col-xs-12 div-sabor">
                            <?= ($produto->produto_base->propriedade_id && !empty($produto->propriedade)) ?
                            '<p class="sabor-info text-uppercase">'.$produto->propriedade.'</p>' : ''; ?>
                        </div>
                        <?php } else { ?>
                            <p class="sabor-info text-uppercase">&nbsp;</p>
                        <?php } ?>
                    </a>
                        <!-- Trustvox Estrelinhas -->
                        <!-- <div data-trustvox-product-code="<?= $produto->id ?>"></div> -->

                        <div class="col-xs-12 box-fundo-produto">
                            <div class="box-preco">
                                
                                <?php if($produto->preco_promo) { ?>
                                    <p class="preco preco-desc">
                                        <?php $porcentagem_nova = $produto->preco * 0.089 ?>
                                        R$ <span style="text-decoration: line-through"><?= number_format($preco = $produto->preco + $porcentagem_nova, 2, ',','.'); ?></span>
                                    </p>
                                <?php } ?> 

                                <p class="preco">
                                    R$ <span class="preco-normal"><?= $produto->preco_promo ?
                                        number_format($preco = $produto->preco_promo, 2, ',','.') :
                                        number_format($preco = $produto->preco, 2, ',','.'); ?></span> ou
                                </p>
                                

                                <?php $produto->preco_promo ? $porcentagem_nova = $produto->preco_promo * 0.089 : $porcentagem_nova = $produto->preco * 0.089 ?>

                                <?php if($produto->preco_promo != null) {
                                    if($produto->preco_promo < 100.00) { ?>
                                        <?php $parcel = $produto->preco_promo * 0.089; $parcel = ($produto->preco_promo + $parcel)/2?>
                                        <p>2x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                    <?php } else if($produto->preco_promo > 100.00 && $produto->preco_promo < 200.00) { ?>
                                        <?php $parcel = $produto->preco_promo * 0.089; $parcel = ($produto->preco_promo + $parcel)/3?>
                                        <p>3x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                    <?php } else if($produto->preco_promo > 200.00 && $produto->preco_promo < 300.00) { ?>
                                        <?php $parcel = $produto->preco_promo * 0.089; $parcel = ($produto->preco_promo + $parcel)/5?>
                                        <p>5x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                    <?php } else if($produto->preco_promo > 300.00) { ?>
                                        <?php $parcel = $produto->preco_promo * 0.089; $parcel = ($produto->preco_promo + $parcel)/6?>
                                        <p>6x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                    <?php }
                                } else {
                                    if($produto->preco < 100.00) { ?>
                                        <?php $parcel = $produto->preco * 0.089; $parcel = ($produto->preco + $parcel)/2?>
                                        <p>2x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                    <?php } else if($produto->preco > 100.00 && $produto->preco < 200.00) { ?>
                                        <?php $parcel = $produto->preco * 0.089; $parcel = ($produto->preco + $parcel)/3?>
                                        <p>3x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                    <?php } else if($produto->preco > 200.00 && $produto->preco < 300.00) { ?>
                                        <?php $parcel = $produto->preco * 0.089; $parcel = ($produto->preco + $parcel)/5?>
                                        <p>5x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                    <?php } else if($produto->preco > 300.00) { ?>
                                        <?php $parcel = $produto->preco * 0.089; $parcel = ($produto->preco + $parcel)/6?>
                                        <p>6x de R$<?= number_format($parcel, 2, ',', '.') ?></p>
                                    <?php }
                                } ?>
                            </div>
                            <div class="box-botoes text-right">
                                <button class="btn btn-box btn-indique" type="button" data-id="<?= $produto->id ?>">
                                    <?= $this->Html->image('icnon-indicar.png',['alt'=>'indique']) ?>
                                        <p>indique</p>
                                </button>

                            <script type="text/javascript">
                                $('.send-indicacao-home-btn').click(function(e) {
                                    e.preventDefault();
                                    var botao = $(this);
                                    
                                    botao.html('Enviando...');
                                    botao.attr('disabled', 'disabled');

                                    var id = $(this).attr('data-id');

                                    var valid = $('.indicar_prod_home_'+id).validationEngine("validate");
                                    if (valid == true) {
                                        $.ajax({
                                                type: "POST",
                                                url: WEBROOT_URL + 'produtos/overlay-indicar-produtos-home/',
                                                data: {
                                                    name: $('.name-'+id).val(),
                                                    email: $('.email-'+id).val(), 
                                                    produto_id: $('.id_produto-'+id).val()
                                                }
                                            })
                                                .done(function (data) {
                                                    if(data == 1) {
                                                        botao.html('Enviado!');
                                                        setTimeout(function () {
                                                            botao.html('Enviar');
                                                            botao.removeAttr('disabled');
                                                        }, 2200);
                                                    } else {
                                                        botao.html('Falha ao enviar... Tente novamente...');
                                                        setTimeout(function () {
                                                            botao.html('Enviar');
                                                            botao.removeAttr('disabled');
                                                        }, 2200);
                                                    }
                                                });
                                    }else{
                                        $('.indicar_prod_home_'+id).validationEngine({
                                            updatePromptsPosition: true,
                                            promptPosition: 'inline',
                                            scroll: false
                                        });
                                    }
                                });
                            </script>

                            <!-- overlay de indicação -->
                            <div id="overlay-indicacao-<?= $produto->id ?>" class="overlay overlay-indicacao">
                                <!-- Overlay content -->
                                <div class="overlay-content text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                                    <h4>Lembrou de alguém quando viu isso?</h4>
                                    <h4>Indique este produto!</h4>
                                    <br>
                                    <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$produto->id])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[optional] form-indicar name-'.$produto->id,
                                        'placeholder'   => 'Quem indicou?',
                                    ])?>
                                    <br>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[required] form-indicar email-'.$produto->id,
                                        'placeholder'   => 'E-mail do destinatário',
                                    ])?>
                                    <?= $this->Form->input('id_produto', [
                                        'div'           => false,
                                        'class'         => 'id_produto-'.$produto->id,
                                        'value'         => $produto->id,
                                        'label'         => false,
                                        'type'          => 'hidden'
                                    ])?>
                                    <br>
                                    <?= $this->Form->button('Enviar', [
                                        'type'          => 'button',
                                        'data-id'       => $produto->id,
                                        'class'         => 'btn btn-success send-indicacao-home-btn'
                                    ])?>
                                    <br>
                                    <br>
                                    <?= $this->Form->end()?>
                                </div>
                            </div>

                            <?php if($produto->visivel == 1 && $produto->status_id == 1) { ?>
                                <button class="btn btn-box btn-compre incluir-mochila" val="<?= $produto->slug ?>" data-id="<?= $produto->id ?>">
                                    <?= $this->Html->image('mochila_log_branca.png',['alt'=>'indique']) ?>
                                    <p>compre</p>
                                </button>
                            <?php } else if($produto->visivel == 1 && $produto->status_id == 2) { ?>
                                
                                    <button class="btn btn-box produto-acabou-btn" data-id="<?= $produto->id ?>" >
                                        <i class="fa fa-envelope fa-2x"></i>
                                        <p>avise</p>
                                    </button>
                                
                                <div id="aviseMe-<?= $produto->id ?>" class="overlay overlay-avise">
                                  <div class="overlay-avise-content overlay-avise-<?= $produto->id ?> text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                                    <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                    <br />
                                    <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$produto->id])?>
                                    <?= $this->Form->input('produto_id', [
                                        'div'           => false,
                                        'label'         => false,
                                        'type'          => 'hidden',
                                        'val'           => $produto->id,
                                        'id'            => 'produto_id_acabou-'.$produto->id
                                    ])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'name_acabou-'.$produto->id,
                                        'class'         => 'form-control validate[optional]',
                                        'placeholder'   => 'Qual o seu nome?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'email_acabou-'.$produto->id,
                                        'class'         => 'form-control validate[required, custom[email]]',
                                        'placeholder'   => 'Qual o seu email?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->button('Enviar', [
                                        'data-id'       => $produto->id,
                                        'type'          => 'button',
                                        'class'         => 'btn btn-success send-acabou-btn'
                                    ])?>
                                    <br />
                                    <br />
                                    <?= $this->Form->end()?>
                                  </div>
                                </div>                               
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                <!-- </div> -->
            </div>
        </div>
        <?php
        $i++;
    endif;
endforeach;
?>

<script type="text/javascript">
    $('#new-produtos-load-more').show();
</script>