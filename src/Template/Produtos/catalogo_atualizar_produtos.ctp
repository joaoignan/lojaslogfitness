<?php foreach ($produtos as $produto) { ?>
<?php $produto->preco_promo >= 1 ? $preco = $produto->preco_promo : $preco = $produto->preco ?>
<?php $foto = unserialize($produto->fotos)[0]; ?>
<?php 
	if($preco <= 100.0) {
		$em_ate = '2x';
		$preco_parcelado = $preco/2;
	} else if($preco > 100.0 && $preco <= 200.0) {
		$em_ate = '3x';
		$preco_parcelado = $preco/3;
	} else if($preco > 200.0 && $preco <= 300.0) {
		$em_ate = '5x';
		$preco_parcelado = $preco/5;
	} else if($preco > 300.0) {
		$em_ate = '6x';
		$preco_parcelado = $preco/6;
	}
?>
<div class="total-produto-catalogo-mobile" id="prod-<?= $produto->id ?>" data="<?= $produto->slug ?>">
	<div class="foto-codigo text-center">
		<p class="codigo-mobile text-left">COD <?= $produto->id ?></p>
		<?= $this->Html->image('produtos/'.$foto, ['alt' => $produto->produto_base->name]) ?>
	</div>
	<div class="infos-preco text-right">
		<span class="objetivo-catalogo" style="background-color: <?= $produto->produto_base->produto_objetivos[0]->objetivo->color ?>"><?= $produto->produto_base->produto_objetivos[0]->objetivo->texto_tarja ?></span>
		<p class="marca-catalogo"><?= $produto->produto_base->marca->name ?></p>
		<p class="nome-catalogo"><?= $produto->produto_base->name ?></p>
		<p class="duracao-catalogo"><?= $produto->tamanho ?><?= $produto->unidade_medida ?>/<strong><?= $produto->doses ?></strong> treinos</p>
		<p class="sabores"><strong><?= $produto->propriedade ?></strong></p>
		<div class="preco text-right">
			<p class="valor-produto-catalogo">R$<?= number_format($preco, 2, ',', '.') ?></p>
			<p class="complementar-valor-catalogo">em <strong><?= $em_ate ?></strong> de R$<?= number_format($preco_parcelado, 2, ',', '.') ?></p>
		</div>
	</div>
</div>
<hr>
<?php } ?>