<style>
    .btn-compre{
        background: #8dc73f;
    }
    .btn-compre svg{
        fill: white;
        width: 25px;
        height: auto;
    }

    .produto-acabou-btn{
        background-color: #5089cf;
    }
    .produto-acabou-btn .fa{
        margin-top: -3px;
        color: #fff;
    }
    .btn-box{
        width: 49%;
        padding:5px;
    }
    .btn-box span{
        color: #fff;
        font-size: 12px;
        margin-top: 3px;
    }
    .btn-box img{
        width: 18px;
        margin-right: 5px;
        margin-top: -5px;
    }
    .box-botoes{
        display: inline-block;
        width: 100%;
        padding-top: 5px;
        float: right;
    }
    .div-sabor{
        height: 23px;
        padding: 0;
    }
    .prdt{
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        margin-bottom: 30px;
        padding: 5px 10px;
        min-height: 380px;
    }
    .link-prdt{
        height: 72px;
    }
    #pagar-celular{
        display: none;
    }
    .promocao-index, #grade-produtos {
        padding-right: 230px;
        padding-left: 80px;
        margin-left: 0;
        margin-right: 0;
        width: 100%;
    }
    .product {
        font-size: 14px;
    }
    .produto-item .bg-white-produto {
        min-height: 314px;
        border: 2px solid rgb(234, 234, 234);
        box-shadow: 0px 1px 7px 0px rgba(200,200,200,0.75);
    }
    .bg-gray-produto {
        bottom: 0;
        position: absolute;
        min-height: 92px;
        max-width: 185px;
    }
    .description {
        width: 100%;
        height: 40px;
        overflow: hidden;
        margin: 0 0 5px 0;
        font-size: 14px;
        text-transform: uppercase;
        font-weight: 700;
    }
    @media all and (max-width: 768px) {
        .produto-item .bg-white-produto {
            min-height: 350px;
        }
    }
    @media all and (min-width: 769px) {
        .box-close-filter { 
            width: 0; 
        }
        .filtro { 
            width: 230px; 
            opacity: 1; 
        }
        .box-filtro { 
            left: 0; 
        }
    }
    .produto-grade {
        max-width: 145px;
        max-height: 150px;
        margin-top: 0;
        display: flex;
        align-items: center;
    }
    .box-imagem-produto {
        width: 100%;
        height: 150px;
        margin-top: -15px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .box-info-produto {
        width: 100%;
        height: 200px;
    }
    .box-fundo-produto {
        width: 100%;
        padding: 0;
    }
    .box-preco {
        height: 70px;
        width: 100%;
        float: left;
        display: flex;
        flex-flow: column;
        justify-content: center;
    }
    .box-mochila-add {
        height: 45px;
        width: 45px;
        float: right;
        display: flex;
        align-items: flex-end;
        margin-top: 10px;
    }
    .box-mochila-add svg {
        width: 100%;
        height: 100%;
        cursor: pointer;
        background: #8dc73f;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
        fill: white;
        margin-right: 3px;
    }
    .box-mochila-avise {
        height: 45px;
        width: 45px;
        float: right;
        margin-top: 10px;
        background-color: #5089cf;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
    }
    .box-mochila-avise > .fa{
        color: #FFF;
        font-size: 2.5em;
        margin-bottom: 2px;
        margin-top: 3px;
    }
    .categoria{
        height: 30px;
    }
    .categoria-div{
        position: absolute;
        right: 5px;
        top: 0px;
        width: 20px;
        height: 30px;
    }
    .preco {
        margin: 0;
        font-size: 16px;
    }
    .preco-desc {
        font-size: 16px;
    }
    .preco-normal {
        font-weight: 700;
        font-size: 20px;
        color: #5089cf;
    }
    .bg-white-produto a {
        min-height: 0;
    }
    .box-promocao {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: black;
        background: #f3d012;
    }
    .box-promocao:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-novidade {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: white;
        background: #5089cf;
    }
    .box-novidade:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-ultimo {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: white;
        background: red;
    }
    .box-ultimo:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-mochila-add svg:hover, .box-mochila-add svg:focus, .box-mochila-add svg:link:hover, .box-mochila-add svg:link:focus, .box-mochila-add svg:visited:hover, .box-mochila-add svg:visited:focus, .box-mochila-avise:hover, .btn-box:hover {
      -webkit-transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      -webkit-animation: diff 0.5s 1;
      animation: diff 0.5s 1;
    }

    @-webkit-keyframes diff {
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
    }
    @keyframes diff {
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
    }

    .icone-objetivo {
        position: absolute;
        left: 5px;
        bottom: 120px;
        width: 30px;
        height: 30px;
        color: #5089cf;
    }

    .box-imagem-produto svg {
        position: absolute;
        left: 5px;
        bottom: 130px;
        width: 20px;
        height: 30px;
    }

    .sabor-info {
        font-size: 12px;
        color: #333;
        text-align: center;
        overflow: hidden;
    }

    @media all and (min-width: 1650px) {
        .produto-item {
            width: 16.66666667%;
        }
    }
    @media all and (max-width: 1240px) {
        .produto-item {
            width: 33.3333333334%;
        }
    }
    @media all and (max-width: 1200px) and (min-width: 780px) {
        .form-busca {
            width: 80% !important;
        }
    }
    @media all and (max-width: 1050px) {
        .produto-item {
            width: 50%;
        }
    }
    @media all and (max-width: 850px) {
        .produto-item {
            width: 100%;
        }
    }
    @media all and (max-width: 768px) {
        #pagar-celular{
            display: block;
            margin-bottom: 7px;
        }
        #pagar-celular .btn-login-nav{
            font-size: 18px;
            padding: 6px;
        }
        .promocao-index, #grade-produtos {
            padding-left: 0;
            padding-right: 0;
        }
        .promocao-index {
            padding-top: 90px;
        }
        .produto-item {
            width: 50%;
        }
    }
    @media all and (max-width: 400px) {
        .produto-grade {
            max-width: 120px;
            max-height: 120px;
        }
        .produto-item {
            padding-right: 0px;
            padding-left: 0px;
        }
    }
    .loader {
        position: fixed;
        left: 0;
        top: 0%;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: white;
    }
    .content-loader{
        position: fixed;
        border-radius: 25px;
        left: 20%;
        top: 25%;
        width: 60%;
        height: 50%;
        z-index: 999999;
        background-color: white;
        text-align: center;
    }
    .content-loader img{
        width: 500px;
        margin-top: 45px;
    }
    .content-loader i{
        color: #f4d637;
    }
    .content-loader p{
        color: #5087c7;
        font-family: nexa_boldregular;
        font-size: 34px;
    }
    @media all and (max-width: 430px) {
        .content-loader{
            display: none;
        }
    }
    @media all and (max-width: 768px) {
        .loader {
            display: none;
        }
    }
    .dropdown a{
        color: #fff;
        font-weight: 400;
    }
    .nav .open>a, .nav .open>a:hover, .nav .open>a:focus, .nav>li>a:focus{
        background-color: rgb(66, 118, 181);
        border-color: rgb(66, 118, 181);
    }
    #dropdown-filtro ul{
        padding: 0;
    }
    #dropdown-filtro li{
        padding: 0;
    }
    .filtros-ativos{
        height: auto;
        margin-bottom: 15px;
        padding-bottom: 15px;
    }
    .filtros-ativos span{
        min-width: 100px;
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        background-color: rgba(255,255,255,1.0);
        border-radius: 5px;
        padding: 5px 10px;
        margin: 5px;
        display: inline-block;
        height: 30px;
    }
    .filtros-ativos .fa{
        color: rgb(255, 0, 0);
    }
    .filtros-ativos .fa:hover{
        color: rgb(244, 219, 27);
        cursor: pointer;
    }
    .filtros-ativos i{
        float: right;
        margin-top: 2px;
    }
    .subfiltros{
        padding: 0;
    }
    .subfiltros select{
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        background-color: rgba(255,255,255,1.0);
        border-radius: 5px;
        padding: 0px 10px;
        height: 25px;
        margin: 5px 3px;
        border: none;
        display: inline-block;
    }
    .owl-theme .owl-controls .owl-buttons div {
        color: #6f6f6f!important;
        background-color: transparent!important;
        font-size: 30px!important;
    }

    .owl-prev {
        width: 15px;
        height: 100px;
        position: absolute;
        top: 40%;
        left: 0;
        display: block!important;
        border:0px solid black;
    }

    .owl-next {
        width: 15px;
        height: 100px;
        position: absolute;
        top: 40%;
        right: -10px;
        display: block!important;
        border:0px solid black;
    }
    .logo-marcas {
        -webkit-filter: grayscale(100%);
        max-width: 80%;
        max-height: 90px;
    }
    .logo-marcas:hover {
        -webkit-filter: grayscale(0);
    }
    .faixa-combos {
        height: 474px;
        position: absolute;
        background-color: #61c513;
        left: 0;
        margin-left: -300px;
        width: 150%;
    }
    .container-grade {
        padding: 500px 0 25px 0;
        background-color: rgba(255, 255, 255, 1);
    }
    .dropdown-content {
        z-index: 50!important;
    }
    .btn-escolhido {
        background-color: #5c799c;
        color: white;
        font-size: 13px;
        text-transform: capitalize;
        padding: 2px 12px;
    }
    .btn-escolhido:hover {
        background-color: #4a617d;
        color: white;
    }
    .produtos-scroll {
        margin-top: 15px;
        position: relative;
        float: left;
    }
    @media (min-width: 1024px) {
        .box-completa-div:nth-of-type(1),
        .box-completa-div:nth-of-type(2),
        .box-completa-div:nth-of-type(3) {
            margin-bottom: 120px;
        }
    }
    .subfiltro-completo h4 {
        margin-top: 0; 
        color: white;
    }
    .subfiltro-completo .texto-sombra {
        text-shadow: 1px 0px 2px #1a1915, 
                    -1px 0px 2px #1a1915, 
                    0px 1px 2px #1a1915, 
                    0px -1px 2px #1a1915;
    }
    .fixed_subfiltro h4 {
        color: black;
    }

    .container-grade {
        padding: 90px 0 25px 0;
    }

    .container-inicio-similares {
        height: 320px;
        padding-top: 20px;
        background-color: rgb(255,255,255);
    }
    
    @media (max-width: 767px) {
        .container-inicio-similares {
            height: auto;
        }
        .imagem-produto-similar {
            height: 200px!important;
        }
        .texto-produto-similar {
            text-align: center;
        }
    }

    @media (min-width: 768px) {
        .imagem-produto-similar {
            justify-content: left; 
        }
    }

    .imagem-produto-similar {
        float: right;
    }

    .align-flex {
        display: flex;
        height: 100%;
        align-content: center;
        justify-content: left;
    }
    .height-100 {
        height: 100%;
    }
    .texto-produto-similar {
        margin:auto; 
        width: 100%;
    }
</style>

<div class="banner-bg" style="background-image: none; background-color: white;"></div>

<div class="container container-grade" data-section="home">
    <?php $foto = unserialize($produto_escolhido->fotos); ?>
    <div class="col-xs-12 text-center container-inicio-similares">
        <div class="row height-100">
            <div class="col-xs-12 col-sm-5 col-sm-offset-1 align-flex imagem-produto-similar">
                <?= $this->Html->image(
                    'produtos/'.$foto[0],
                    ['alt' => $produto_escolhido->produto_base->name.' '.$produto_escolhido->produto_base->embalagem_conteudo, 'class' => 'height-100']); ?>
            </div>
            <div class="col-xs-12 col-sm-6 align-flex">
                <h2 class="text-right texto-produto-similar">Não encontrou esse produto?<br>Veja <b>similares!</b></h2>
            </div>
        </div>
    </div>
    <div class="col-sm-4 col-md-2 filtro-div tablet-hidden">
        <?= $this->Element('filtro'); ?>
    </div>
    <div class="col-sm-12 col-md-10 produtos-div">
        <div class="produtos-grade">
            <div class="produtos-scroll row col-xs-12">
                
                <?= $this->Element('carrousel_produtos_similares', [
                    'produtos' => $produtos_promocao,
                    'qtd_produtos' => $qtd_produtos_promocao,
                    'title' => 'Promoções',
                    'link_produtos' => '/promocoes'
                ]); ?>

                <?php if(count($similares_produto_base) >= 1) { ?>
                    <div class="title-section">
                        <h3>Outros sabores e tamanhos</h3>
                    </div>
                    <div class="owl-carousel">
                        <?php $i = 1; ?>
                        <?php foreach($similares_produto_base as $similar_produto_base) { ?>
                            <div class="item">
                                <?php $fotos = unserialize($similar_produto_base->fotos); ?>
                                <div class="box-completa-produto" itemtype="http://schema.org/Product">
                                    <div class="tarja-objetivo">
                                        <?php
                                            $contador_objetivos = 0; 
                                            $novo_top = 0;
                                        ?>
                                        <?php foreach ($similar_produto_base->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                            <?php if ($contador_objetivos < 2) { ?>
                                            <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                                                <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                            </div>
                                            <?php } ?>
                                            <?php
                                                $contador_objetivos++;
                                                $novo_top = $novo_top + 19;
                                             ?>
                                        <?php } ?>
                                    </div>
                                    <div class="total-tarjas">
                                        <?php if ($similar_produto_base->estoque == 1) { ?>
                                            <div class="tarja tarja-ultimo">
                                                <span>Último</span>
                                            </div>
                                        <?php } else if ($similar_produto_base->estoque >= 1 && $similar_produto_base->preco_promo || $desconto_geral) { ?>
                                            <div class="tarja tarja-promocao">
                                                <span>Promoção</span>
                                            </div>
                                        <?php } else if($similar_produto_base->estoque >= 1 && $similar_produto_base->created > $max_date_novidade) { ?>
                                            <div class="tarja tarja-novidade">
                                                <span>Novidade</span>
                                            </div>
                                        <?php } ?>               
                                    </div>
                                    <div class="info-produto">
                                        <div class="produto-superior">
                                            <a href="<?= $SSlug.'/produto/'.$similar_produto_base->slug ?>">
                                                <?php $fotos = unserialize($similar_produto_base->fotos); ?>
                                                <div class="foto-produto" id="produto-<?= $i ?>" >
                                                    <?= $this->Html->image(
                                                        'produtos/md-'.$fotos[0],
                                                        ['val' => $similar_produto_base->slug, 'alt' => $similar_produto_base->produto_base->name.' '.$similar_produto_base->produto_base->embalagem_conteudo]); ?>
                                                </div>
                                                <div class="detalhes-produto">
                                                    <div class="informacoes-detalhes text-left">
                                                        <p>Código: <?= $similar_produto_base->id ?></p>
                                                        <p><?= strtolower($similar_produto_base->produto_base->marca->name); ?></p>
                                                        <p><?= $similar_produto_base->tamanho.' '.$similar_produto_base->unidade_medida; ?></p>
                                                        <p>para <?= $similar_produto_base->doses; ?> treinos</p>
                                                        <p><?= strtolower($similar_produto_base->propriedade); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="produto-inferior">
                                            <?php if($similar_produto_base->preco_promo || $desconto_geral > 0) { ?>
                                            <?php $porcentagem_nova = $similar_produto_base->preco * 0.089;
                                            $preco_corte = $similar_produto_base->preco + $porcentagem_nova; 
                                            } ?> 
                                            <?php $similar_produto_base->preco_promo ?
                                            number_format($preco = $similar_produto_base->preco_promo * $desconto_geral, 2, ',','.') :
                                            number_format($preco = $similar_produto_base->preco * $desconto_geral, 2, ',','.'); ?>
                                            <?php $similar_produto_base->preco_promo ? $porcentagem_nova = $similar_produto_base->preco_promo * 0.089 : $porcentagem_nova = $similar_produto_base->preco * 0.089 ?>
                                            <?php if($similar_produto_base->preco_promo != null) {
                                                if($similar_produto_base->preco_promo < 100.00) { ?>
                                                <?php $parcel = $similar_produto_base->preco_promo * $desconto_geral * 0.089; $parcel = ($similar_produto_base->preco_promo * $desconto_geral + $parcel)/2?>
                                                <?php $maximo_parcelas = "2x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php } else if($similar_produto_base->preco_promo > 100.00 && $similar_produto_base->preco_promo < 200.00) { ?>
                                                <?php $parcel = $similar_produto_base->preco_promo * $desconto_geral * 0.089; $parcel = ($similar_produto_base->preco_promo * $desconto_geral + $parcel)/3?>
                                                <?php $maximo_parcelas = "3x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php } else if($similar_produto_base->preco_promo > 200.00 && $similar_produto_base->preco_promo < 300.00) { ?>
                                                <?php $parcel = $similar_produto_base->preco_promo * $desconto_geral * 0.089; $parcel = ($similar_produto_base->preco_promo * $desconto_geral + $parcel)/5?>
                                                <?php $maximo_parcelas = "5x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php } else if($similar_produto_base->preco_promo > 300.00) { ?>
                                                <?php $parcel = $similar_produto_base->preco_promo * $desconto_geral * 0.089; $parcel = ($similar_produto_base->preco_promo * $desconto_geral + $parcel)/6?>
                                                <?php $maximo_parcelas = "6x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php }
                                                } else {
                                            if($similar_produto_base->preco < 100.00) { ?>
                                                <?php $parcel = $similar_produto_base->preco * $desconto_geral * 0.089; $parcel = ($similar_produto_base->preco * $desconto_geral + $parcel)/2?>
                                                 <?php $maximo_parcelas = "2x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php } else if($similar_produto_base->preco > 100.00 && $similar_produto_base->preco < 200.00) { ?>
                                                <?php $parcel = $similar_produto_base->preco * $desconto_geral * 0.089; $parcel = ($similar_produto_base->preco * $desconto_geral + $parcel)/3?>
                                                <?php $maximo_parcelas = "3x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php } else if($similar_produto_base->preco > 200.00 && $similar_produto_base->preco < 300.00) { ?>
                                                <?php $parcel = $similar_produto_base->preco * $desconto_geral * 0.089; $parcel = ($similar_produto_base->preco * $desconto_geral + $parcel)/5?>
                                                <?php $maximo_parcelas = "5x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php } else if($similar_produto_base->preco > 300.00) { ?>
                                                <?php $parcel = $similar_produto_base->preco * $desconto_geral * 0.089; $parcel = ($similar_produto_base->preco * $desconto_geral + $parcel)/6?>
                                                <?php $maximo_parcelas = "6x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php }
                                            } ?>
                                            <?php 
                                                if($similar_produto_base->preco_promo || $desconto_geral > 0) {
                                                    echo $this->Html->link("
                                                        <h4>".$similar_produto_base->produto_base->name."</h4>
                                                        <p><span class='preco-corte'> R$".number_format($preco_corte, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                        <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                        $SSlug.'/produto/'.$similar_produto_base->slug,
                                                        ['escape' => false]);
                                                } else {
                                                    echo $this->Html->link("
                                                        <h4>".$similar_produto_base->produto_base->name."</h4>
                                                        <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                        <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                        $SSlug.'/produto/'.$similar_produto_base->slug,
                                                        ['escape' => false]);
                                                }
                                            ?>
                                        </div>
                                        <div class="hover-produto <?= $similar_produto_base->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                                            <div class="col-xs-12">
                                                <div class="hexagono-indicar">
                                                    <?php if($similar_produto_base->status_id == 1) { ?>
                                                        <div class="hexagon-disponivel btn-indique" data-id="<?= $similar_produto_base->id ?>">
                                                            <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                                            <p>indique</p>
                                                        </div>
                                                        <script>
                                                            $('.btn-indique').click(function () {
                                                                    var id_produto = $(this).attr('data-id');
                                                                    $('#overlay-indicacao-'+id_produto).height('100%');
                                                                    $("html,body").css({"overflow":"hidden"});
                                                            });
                                                            function closeIndica() {
                                                                $('.overlay-indicacao').height('0%');
                                                                $("html,body").css({"overflow":"auto"});
                                                            }
                                                        </script>
                                                    <?php } else { ?>
                                                        <div class="hexagon produto-acabou-btn" data-id="<?= $similar_produto_base->id ?>">
                                                            <span><i class="fa fa-envelope-o"></i></span>
                                                            <p>avise-me</p>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <?php if($similar_produto_base->status_id == 1) { ?>
                                                <div class="col-xs-6">
                                                    <?= $this->Html->link('
                                                        <div class="hexagon-disponivel">
                                                            <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                            <p>detalhes</p>
                                                        </div>'
                                                        ,$SSlug.'/produto/'.$similar_produto_base->slug,
                                                        ['escape' => false]);
                                                    ?>
                                                </div>
                                                <div class="col-xs-6 text-center compare">
                                                    <div class="hexagon-disponivel btn-comparar-action" data-id="<?= $similar_produto_base->id ?>">
                                                        <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                                        <p>comparar</p>
                                                    </div>
                                                </div>
                                            <?php }
                                            else { ?>
                                                <div class="col-xs-6">
                                                    <?= $this->Html->link('
                                                        <div class="hexagon">
                                                            <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                            <p>detalhes</p>
                                                        </div>'
                                                        ,$SSlug.'/produto/'.$similar_produto_base->slug,
                                                        ['escape' => false]);
                                                    ?>
                                                </div>
                                                <div class="col-xs-6 text-center compare">
                                                    <div class="hexagon btn-comparar-action" data-id="<?= $similar_produto_base->id ?>">
                                                        <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                                        <p>comparar</p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $('.send-indicacao-home-btn').click(function(e) {
                                            e.preventDefault();
                                            var botao = $(this);
                                        
                                            botao.html('Enviando...');
                                            botao.attr('disabled', 'disabled');

                                            var id = $(this).attr('data-id');
                                            var slug = '<?= $SSlug ?>';

                                            var valid = $('.indicar_prod_home_'+id).validationEngine("validate");
                                            if (valid == true) {
                                                $.ajax({
                                                    type: "POST",
                                                    url: WEBROOT_URL + slug + '/produtos/overlay-indicar-produtos-home/',
                                                    data: {
                                                        name: $('.name-'+id).val(),
                                                        email: $('.email-'+id).val(), 
                                                        produto_id: $('.id_produto-'+id).val()
                                                    }
                                                })
                                                .done(function (data) {
                                                    if(data == 1) {
                                                        botao.html('Enviado!');
                                                        setTimeout(function () {
                                                            botao.html('Enviar');
                                                            botao.removeAttr('disabled');
                                                        }, 2200);
                                                    } else {
                                                        botao.html('Falha ao enviar... Tente novamente...');
                                                        setTimeout(function () {
                                                            botao.html('Enviar');
                                                            botao.removeAttr('disabled');
                                                        }, 2200);
                                                    }
                                                });
                                            }else{
                                                $('.indicar_prod_home_'+id).validationEngine({
                                                    updatePromptsPosition: true,
                                                    promptPosition: 'inline',
                                                    scroll: false
                                                });
                                            }
                                        });
                                    </script>
                                    <div class="botoes-comprar-similiar">
                                        <?php if($similar_produto_base->visivel == 1 && $similar_produto_base->status_id == 1) { ?>
                                            <?php if($Academia->prazo_medio > 0 && $Academia->prazo_medio <= 72) { ?>
                                                <?php 
                                                    if($Academia->prazo_medio <= 12) {
                                                        $prazo_medio = 12;
                                                    } else if($Academia->prazo_medio > 12 && $Academia->prazo_medio <= 24) {
                                                        $prazo_medio = 24;
                                                    } else if($Academia->prazo_medio > 24 && $Academia->prazo_medio <= 48) {
                                                        $prazo_medio = 48;
                                                    } else if($Academia->prazo_medio > 48) {
                                                        $prazo_medio = 72;
                                                    }
                                                ?>
                                                <p class="texto-prazo"><i class="fa fa-truck" aria-hidden="true"></i> Entrega em até <?= $prazo_medio ?>h</p>
                                            <?php } ?>
                                            <button class="btn btn-incluir incluir-mochila" val="<?= $similar_produto_base->slug ?>" data-id="<?= $similar_produto_base->id ?>">Comprar</button>
                                            <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $similar_produto_base->id ?>">Indique</button>
                                            <button class="desktop-hidden btn btn-incluir produto-comparar-btn btn-comparar-action" data-id="<?= $similar_produto_base->id ?>">Comparar</button> 
                                        <?php } else if($similar_produto_base->visivel == 1 && $similar_produto_base->status_id == 2) { ?>
                                            <button class="btn btn-similares">Similares</button>            
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; ?>
                        <?php } ?>
                    </div>
                    <?php foreach($similares_produto_base as $similar_produto_base) { ?>
                        <div id="aviseMe-<?= $similar_produto_base->id ?>" class="overlay overlay-avise">
                            <div class="overlay-avise-content overlay-avise-<?= $similar_produto_base->id ?> text-center">
                                <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                                <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                <br />
                                <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$similar_produto_base->id])?>
                                <?= $this->Form->input('produto_id', [
                                    'div'           => false,
                                    'label'         => false,
                                    'type'          => 'hidden',
                                    'val'           => $similar_produto_base->id,
                                    'id'            => 'produto_id_acabou-'.$similar_produto_base->id
                                ])?>
                                <?= $this->Form->input('name', [
                                    'div'           => false,
                                    'label'         => false,
                                    'id'            => 'name_acabou-'.$similar_produto_base->id,
                                    'class'         => 'form-control validate[optional]',
                                    'placeholder'   => 'Qual o seu nome?',
                                ])?>
                                <br/>
                                <?= $this->Form->input('email', [
                                    'div'           => false,
                                    'label'         => false,
                                    'id'            => 'email_acabou-'.$similar_produto_base->id,
                                    'class'         => 'form-control validate[required, custom[email]]',
                                    'placeholder'   => 'Qual o seu email?',
                                ])?>
                                <br/>
                                <?= $this->Form->button('Enviar', [
                                    'data-id'       => $similar_produto_base->id,
                                    'type'          => 'button',
                                    'class'         => 'btn btn-success send-acabou-btn'
                                ])?>
                                <br />
                                <br />
                                <?= $this->Form->end()?>
                            </div>
                        </div> 
                        <div id="overlay-indicacao-<?= $similar_produto_base->id ?>" class="overlay overlay-indicacao">
                            <div class="overlay-content text-center">
                                <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                                <h4>Lembrou de alguém quando viu isso?</h4>
                                <h4>Indique este produto!</h4>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 share-whastapp share-buttons">
                                        <a href="whatsapp://send?text=Isso é a sua cara! <?= WEBROOT_URL.$SSlug.'/produto/'.$similar_produto_base->slug ?>">
                                            <button class="btn button-blue"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</button>
                                        </a>
                                    </div>
                                    <div class="col-xs-12 share-whastapp share-buttons">
                                        <button class="btn button-blue" id="share-<?= $similar_produto_base->id ?>"><i class="fa fa-comment-o" aria-hidden="true"></i> Messenger</button>
                                        <script>
                                            $(document).ready(function() {
                                                $('#share-<?= $similar_produto_base->id ?>').click(function(){
                                                    var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$similar_produto_base->slug ?>";
                                                    var app_id = '1321574044525013';
                                                    window.open('fb-messenger://share?link=' + link + '&app_id=' + app_id);
                                                });
                                            });
                                        </script>
                                    </div>
                                    <div class="tablet-hidden col-xs-12 col-md-3 share-buttons">
                                        <button data-id="whatsweb-<?= $similar_produto_base->id ?>" class="btn button-blue abrir-whats"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp Web</button>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                        <a href="javascript: void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u=<?= WEBROOT_URL.$SSlug.'/produto/'.$similar_produto_base->slug ?>','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');">
                                            <button class="btn button-blue fb-share-button"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</button>
                                        </a>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                        <button class="btn button-blue" id="copiar-clipboard-<?= $similar_produto_base->id ?>"><i class="fa fa-clone" aria-hidden="true"></i> Copiar link</button>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                        <button  class="btn button-blue abrir-email"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="tablet-hidden enviar-whats">
                                        <div class="col-xs-4 text-center form-group">
                                            <?= $this->Form->input('whats-web-form', [
                                                'div'           => false,
                                                'id'            => 'nro-whatsweb-'.$similar_produto_base->id,
                                                'label'         => false,
                                                'class'         => 'form-control form-indicar',
                                                'placeholder'   => "Celular*: (00)00000-0000"
                                            ])?>
                                        </div>
                                        <div class="col-xs-4 text-left obs-overlay">
                                            <p>*Para enviar pra mais de um contato deixe este campo vazio.</p>
                                        </div>
                                        <div class="col-xs-4 text-center form-group">
                                            <?= $this->Form->button('Enviar', [
                                            'type'          => 'button',
                                            'data-id'       => $similar_produto_base->id,
                                            'class'         => 'btn button-blue',
                                            'id'            => 'enviar-whatsweb-'.$similar_produto_base->id
                                            ])?>
                                        </div>
                                        <script>
                                            $(document).ready(function() {
                                                $('#enviar-whatsweb-<?=$similar_produto_base->id ?>').click(function(){

                                                    var numero;
                                                    var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$similar_produto_base->slug ?>";

                                                    if($('#nro-whatsweb-<?= $similar_produto_base->id ?>').val() != '') {
                                                        numero = '55'+$('#nro-whatsweb-<?= $similar_produto_base->id ?>').val();
                                                    }
                                                    
                                                    window.open('https://api.whatsapp.com/send?phone=' + numero + '&text=Isso é a sua cara! ' + link);
                                                });
                                            });
                                        </script>
                                    </div>
                                     <div class="col-xs-12 enviar-email" id="collapse-email-<?=$similar_produto_base->id ?>">
                                        <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$similar_produto_base->id])?>
                                        <?= $this->Form->input('name', [
                                            'div'           => false,
                                            'label'         => false,
                                            'class'         => 'form-control validate[optional] form-indicar name-'.$similar_produto_base->id,
                                            'placeholder'   => 'Quem indicou?',
                                        ])?>
                                        <br>
                                        <?= $this->Form->input('email', [
                                            'div'           => false,
                                            'label'         => false,
                                            'class'         => 'form-control validate[required] form-indicar email-'.$similar_produto_base->id,
                                            'placeholder'   => 'E-mail do destinatário',
                                        ])?>
                                        <?= $this->Form->input('id_produto', [
                                            'div'           => false,
                                            'class'         => 'id_produto-'.$similar_produto_base->id,
                                            'value'         => $similar_produto_base->id,
                                            'label'         => false,
                                            'type'          => 'hidden'
                                        ])?>
                                        <br/>
                                        <?= $this->Form->button('Enviar', [
                                            'id'            => 'send-indicacao-btn',
                                            'type'          => 'button',
                                            'data-id'       => $similar_produto_base->id,
                                            'class'         => 'btn button-blue send-indicacao-home-btn'
                                        ])?>
                                        <br />
                                        <?= $this->Form->end()?>
                                    </div>
                                </div>
                                <div class="row"> 
                                    <div class="col-xs-12 clipboard">   
                                        <?= $this->Form->input('url', [
                                            'div'           => false,
                                            'id'            => 'url-clipboard-'.$similar_produto_base->id,
                                            'label'         => false,
                                            'value'         => WEBROOT_URL.$SSlug.'/produto/'.$similar_produto_base->slug,
                                            'readonly'      => true,
                                            'class'         => 'form-control form-indicar'
                                        ])?>
                                    </div>  
                                    <p><span class="copy-alert hide" id="copy-alert-<?=$similar_produto_base->id ?>">Copiado!</span></p>
                                    <script>
                                        $(document).ready(function() {
                                            // Copy to clipboard 
                                            document.querySelector("#copiar-clipboard-<?=$similar_produto_base->id ?>").onclick = () => {
                                              // Select the content
                                                document.querySelector("#url-clipboard-<?=$similar_produto_base->id ?>").select();
                                                // Copy to the clipboard
                                                document.execCommand('copy');
                                                // Exibe span de sucesso
                                                $('#copy-alert-<?=$similar_produto_base->id ?>').removeClass("hide");
                                                // Oculta span de sucesso
                                                setTimeout(function() {
                                                    $('#copy-alert-<?=$similar_produto_base->id ?>').addClass("hide");
                                                }, 1500);
                                            };
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>

                <?php $ctrl = 0; ?>
                <?php foreach($similares_subcategoria as $similar_subcategoria) { ?>
                    <?php if(count($similar_subcategoria) >= 1) { ?>
                        <div class="title-section">
                            <h3>Produtos da subcategoria <span style="color: #5089cf"><?= $similares_subcategoria_name[$ctrl] ?></span></h3>
                            <?= $this->Html->link('<span class="fa fa-plus"></span> ver todos',
                                $SSlug.'/pesquisa/0/0/subcategoria/'.$similares_subcategoria_slug[$ctrl].'/', ['escape' => false, 'style' => 'text-transform: uppercase']
                            ); ?>
                        </div>
                        <div class="owl-carousel">
                            <?php $i = 1; ?>
                            <?php foreach($similar_subcategoria as $produto_similar_subcategoria) { ?>
                                <div class="item">
                                    <?php $fotos = unserialize($produto_similar_subcategoria->fotos); ?>
                                    <div class="box-completa-produto" itemtype="http://schema.org/Product">
                                        <div class="tarja-objetivo">
                                            <?php
                                                $contador_objetivos = 0; 
                                                $novo_top = 0;
                                            ?>
                                            <?php foreach ($produto_similar_subcategoria->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                                <?php if ($contador_objetivos < 2) { ?>
                                                <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                                                    <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                    $contador_objetivos++;
                                                    $novo_top = $novo_top + 19;
                                                 ?>
                                            <?php } ?>
                                        </div>
                                        <div class="total-tarjas">
                                            <?php if ($produto_similar_subcategoria->estoque == 1) { ?>
                                                <div class="tarja tarja-ultimo">
                                                    <span>Último</span>
                                                </div>
                                            <?php } else if ($produto_similar_subcategoria->estoque >= 1 && $produto_similar_subcategoria->preco_promo || $desconto_geral) { ?>
                                                <div class="tarja tarja-promocao">
                                                    <span>Promoção</span>
                                                </div>
                                            <?php } else if($produto_similar_subcategoria->estoque >= 1 && $produto_similar_subcategoria->created > $max_date_novidade) { ?>
                                                <div class="tarja tarja-novidade">
                                                    <span>Novidade</span>
                                                </div>
                                            <?php } ?>               
                                        </div>
                                        <div class="info-produto">
                                            <div class="produto-superior">
                                                <a href="<?= $SSlug.'/produto/'.$produto_similar_subcategoria->slug ?>">
                                                    <?php $fotos = unserialize($produto_similar_subcategoria->fotos); ?>
                                                    <div class="foto-produto" id="produto-<?= $i ?>" >
                                                        <?= $this->Html->image(
                                                            'produtos/md-'.$fotos[0],
                                                            ['val' => $produto_similar_subcategoria->slug, 'alt' => $produto_similar_subcategoria->produto_base->name.' '.$produto_similar_subcategoria->produto_base->embalagem_conteudo]); ?>
                                                    </div>
                                                    <div class="detalhes-produto">
                                                        <div class="informacoes-detalhes text-left">
                                                            <p>Código: <?= $$produto_similar_subcategoria->id ?></p>
                                                            <p><?= strtolower($produto_similar_subcategoria->produto_base->marca->name); ?></p>
                                                            <p><?= $produto_similar_subcategoria->tamanho.' '.$produto_similar_subcategoria->unidade_medida; ?></p>
                                                            <p>para <?= $produto_similar_subcategoria->doses; ?> treinos</p>
                                                        <p><?= strtolower($produto_similar_subcategoria->propriedade); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="produto-inferior">
                                                <?php if($produto_similar_subcategoria->preco_promo) { ?>
                                                <?php $porcentagem_nova = $produto_similar_subcategoria->preco * 0.089;
                                                $preco_corte = $produto_similar_subcategoria->preco + $porcentagem_nova; 
                                                } ?> 
                                                <?php $produto_similar_subcategoria->preco_promo ?
                                                number_format($preco = $produto_similar_subcategoria->preco_promo, 2, ',','.') :
                                                number_format($preco = $produto_similar_subcategoria->preco, 2, ',','.'); ?>
                                                <?php $produto_similar_subcategoria->preco_promo ? $porcentagem_nova = $produto_similar_subcategoria->preco_promo * 0.089 : $porcentagem_nova = $produto_similar_subcategoria->preco * 0.089 ?>
                                                <?php if($produto_similar_subcategoria->preco_promo != null) {
                                                    if($produto_similar_subcategoria->preco_promo < 100.00) { ?>
                                                    <?php $parcel = $produto_similar_subcategoria->preco_promo * 0.089; $parcel = ($produto_similar_subcategoria->preco_promo + $parcel)/2?>
                                                    <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_subcategoria->preco_promo > 100.00 && $produto_similar_subcategoria->preco_promo < 200.00) { ?>
                                                    <?php $parcel = $produto_similar_subcategoria->preco_promo * 0.089; $parcel = ($produto_similar_subcategoria->preco_promo + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_subcategoria->preco_promo > 200.00 && $produto_similar_subcategoria->preco_promo < 300.00) { ?>
                                                    <?php $parcel = $produto_similar_subcategoria->preco_promo * 0.089; $parcel = ($produto_similar_subcategoria->preco_promo + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_subcategoria->preco_promo > 300.00) { ?>
                                                    <?php $parcel = $produto_similar_subcategoria->preco_promo * 0.089; $parcel = ($produto_similar_subcategoria->preco_promo + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                    } else {
                                                if($produto_similar_subcategoria->preco < 100.00) { ?>
                                                    <?php $parcel = $produto_similar_subcategoria->preco * 0.089; $parcel = ($produto_similar_subcategoria->preco + $parcel)/2?>
                                                     <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_subcategoria->preco > 100.00 && $produto_similar_subcategoria->preco < 200.00) { ?>
                                                    <?php $parcel = $produto_similar_subcategoria->preco * 0.089; $parcel = ($produto_similar_subcategoria->preco + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_subcategoria->preco > 200.00 && $produto_similar_subcategoria->preco < 300.00) { ?>
                                                    <?php $parcel = $produto_similar_subcategoria->preco * 0.089; $parcel = ($produto_similar_subcategoria->preco + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_subcategoria->preco > 300.00) { ?>
                                                    <?php $parcel = $produto_similar_subcategoria->preco * 0.089; $parcel = ($produto_similar_subcategoria->preco + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                } ?>
                                                <?php 
                                                    if($produto_similar_subcategoria->preco_promo) {
                                                        echo $this->Html->link("
                                                            <h4>".$produto_similar_subcategoria->produto_base->name."</h4>
                                                            <p><span class='preco-corte'> R$".number_format($preco_corte, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            $SSlug.'/produto/'.$produto_similar_subcategoria->slug,
                                                            ['escape' => false]);
                                                    } else {
                                                        echo $this->Html->link("
                                                            <h4>".$produto_similar_subcategoria->produto_base->name."</h4>
                                                            <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            $SSlug.'/produto/'.$produto_similar_subcategoria->slug,
                                                            ['escape' => false]);
                                                    }
                                                ?>
                                            </div>
                                            <div class="hover-produto <?= $produto_similar_subcategoria->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                                                <div class="col-xs-12">
                                                    <div class="hexagono-indicar">
                                                        <?php if($produto_similar_subcategoria->status_id == 1) { ?>
                                                            <div class="hexagon-disponivel btn-indique" data-id="<?= $produto_similar_subcategoria->id ?>">
                                                                <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                                                <p>indique</p>
                                                            </div>
                                                            <script>
                                                                $('.btn-indique').click(function () {
                                                                        var id_produto = $(this).attr('data-id');
                                                                        $('#overlay-indicacao-'+id_produto).height('100%');
                                                                        $("html,body").css({"overflow":"hidden"});
                                                                });
                                                                function closeIndica() {
                                                                    $('.overlay-indicacao').height('0%');
                                                                    $("html,body").css({"overflow":"auto"});
                                                                }
                                                            </script>
                                                        <?php } else { ?>
                                                            <div class="hexagon produto-acabou-btn" data-id="<?= $produto_similar_subcategoria->id ?>">
                                                                <span><i class="fa fa-envelope-o"></i></span>
                                                                <p>avise-me</p>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <?php if($produto_similar_subcategoria->status_id == 1) { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon-disponivel">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,$SSlug.'/produto/'.$produto_similar_subcategoria->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon-disponivel btn-comparar-action" data-id="<?= $produto_similar_subcategoria->id ?>">
                                                            <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                                            <p>comparar</p>
                                                        </div>
                                                    </div>
                                                <?php }
                                                else { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,$SSlug.'/produto/'.$produto_similar_subcategoria->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon btn-comparar-action" data-id="<?= $produto_similar_subcategoria->id ?>">
                                                            <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                                            <p>comparar</p>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $('.send-indicacao-home-btn').click(function(e) {
                                                e.preventDefault();
                                                var botao = $(this);
                                            
                                                botao.html('Enviando...');
                                                botao.attr('disabled', 'disabled');

                                                var id = $(this).attr('data-id');
                                                var slug = '<?= $SSlug ?>';

                                                var valid = $('.indicar_prod_home_'+id).validationEngine("validate");
                                                if (valid == true) {
                                                    $.ajax({
                                                        type: "POST",
                                                        url: WEBROOT_URL + slug + '/produtos/overlay-indicar-produtos-home/',
                                                        data: {
                                                            name: $('.name-'+id).val(),
                                                            email: $('.email-'+id).val(), 
                                                            produto_id: $('.id_produto-'+id).val()
                                                        }
                                                    })
                                                    .done(function (data) {
                                                        if(data == 1) {
                                                            botao.html('Enviado!');
                                                            setTimeout(function () {
                                                                botao.html('Enviar');
                                                                botao.removeAttr('disabled');
                                                            }, 2200);
                                                        } else {
                                                            botao.html('Falha ao enviar... Tente novamente...');
                                                            setTimeout(function () {
                                                                botao.html('Enviar');
                                                                botao.removeAttr('disabled');
                                                            }, 2200);
                                                        }
                                                    });
                                                }else{
                                                    $('.indicar_prod_home_'+id).validationEngine({
                                                        updatePromptsPosition: true,
                                                        promptPosition: 'inline',
                                                        scroll: false
                                                    });
                                                }
                                            });
                                        </script>
                                        <div class="botoes-comprar-similiar">
                                            <?php if($produto_similar_subcategoria->visivel == 1 && $produto_similar_subcategoria->status_id == 1) { ?>
                                                <?php if($Academia->prazo_medio > 0 && $Academia->prazo_medio <= 72) { ?>
                                                    <?php 
                                                        if($Academia->prazo_medio <= 12) {
                                                            $prazo_medio = 12;
                                                        } else if($Academia->prazo_medio > 12 && $Academia->prazo_medio <= 24) {
                                                            $prazo_medio = 24;
                                                        } else if($Academia->prazo_medio > 24 && $Academia->prazo_medio <= 48) {
                                                            $prazo_medio = 48;
                                                        } else if($Academia->prazo_medio > 48) {
                                                            $prazo_medio = 72;
                                                        }
                                                    ?>
                                                    <p class="texto-prazo"><i class="fa fa-truck" aria-hidden="true"></i> Entrega em até <?= $prazo_medio ?>h</p>
                                                <?php } ?>
                                                <button class="btn btn-incluir incluir-mochila" val="<?= $produto_similar_subcategoria->slug ?>" data-id="<?= $produto_similar_subcategoria->id ?>">Comprar</button>
                                                <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $produto_similar_subcategoria->id ?>">Indique</button>
                                                <button class="desktop-hidden btn btn-incluir produto-comparar-btn btn-comparar-action" data-id="<?= $produto_similar_subcategoria->id ?>">Comparar</button> 
                                            <?php } else if($produto_similar_subcategoria->visivel == 1 && $produto_similar_subcategoria->status_id == 2) { ?>
                                                <button class="btn btn-similares">Similares</button>            
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++; ?>
                            <?php } ?>
                            <div class="item">
                                <div class="box-completa-produto" id="produto-<?= $i ?>">
                                    <div class="col-md-12 col-xs-12 text-center bg-white-produto prdt">
                                        <div class="text-center" style="margin-top: 130px;">
                                            <?= $this->Html->link('<span class="fa fa-plus fa-2x"></span>',
                                                $SSlug.'/pesquisa/0/0/subcategoria/'.$similares_subcategoria_slug[$ctrl].'/', ['escape' => false, 'style' => 'font-size: 20px; position: relative; width: 100%']
                                            ); ?>
                                            <?= $this->Html->link('ver mais',
                                                $SSlug.'/pesquisa/0/0/subcategoria/'.$similares_subcategoria_slug[$ctrl].'/', ['escape' => false, 'style' => 'font-size: 25px; position: relative; width: 100%']
                                            ); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php foreach($similar_subcategoria as $produto_similar_subcategoria) { ?>
                            <div id="aviseMe-<?= $produto_similar_subcategoria->id ?>" class="overlay overlay-avise">
                                <div class="overlay-avise-content overlay-avise-<?= $produto_similar_subcategoria->id ?> text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                                    <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                    <br />
                                    <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$produto_similar_subcategoria->id])?>
                                    <?= $this->Form->input('produto_id', [
                                        'div'           => false,
                                        'label'         => false,
                                        'type'          => 'hidden',
                                        'val'           => $produto_similar_subcategoria->id,
                                        'id'            => 'produto_id_acabou-'.$produto_similar_subcategoria->id
                                    ])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'name_acabou-'.$produto_similar_subcategoria->id,
                                        'class'         => 'form-control validate[optional]',
                                        'placeholder'   => 'Qual o seu nome?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'email_acabou-'.$produto_similar_subcategoria->id,
                                        'class'         => 'form-control validate[required, custom[email]]',
                                        'placeholder'   => 'Qual o seu email?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->button('Enviar', [
                                        'data-id'       => $produto_similar_subcategoria->id,
                                        'type'          => 'button',
                                        'class'         => 'btn btn-success send-acabou-btn'
                                    ])?>
                                    <br />
                                    <br />
                                    <?= $this->Form->end()?>
                                </div>
                            </div> 
                            <div id="overlay-indicacao-<?= $produto_similar_subcategoria->id ?>" class="overlay overlay-indicacao">
                                <div class="overlay-content text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                                    <h4>Lembrou de alguém quando viu isso?</h4>
                                    <h4>Indique este produto!</h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-12 share-whastapp share-buttons">
                                            <a href="whatsapp://send?text=Isso é a sua cara! <?= WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_subcategoria->slug ?>">
                                                <button class="btn button-blue"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</button>
                                            </a>
                                        </div>
                                        <div class="col-xs-12 share-whastapp share-buttons">
                                            <button class="btn button-blue" id="share-<?= $produto_similar_subcategoria->id ?>"><i class="fa fa-comment-o" aria-hidden="true"></i> Messenger</button>
                                            <script>
                                                $(document).ready(function() {
                                                    $('#share-<?= $produto_similar_subcategoria->id ?>').click(function(){
                                                        var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_subcategoria->slug ?>";
                                                        var app_id = '1321574044525013';
                                                        window.open('fb-messenger://share?link=' + link + '&app_id=' + app_id);
                                                    });
                                                });
                                            </script>
                                        </div>
                                        <div class="tablet-hidden col-xs-12 col-md-3 share-buttons">
                                            <button data-id="whatsweb-<?= $produto_similar_subcategoria->id ?>" class="btn button-blue abrir-whats"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp Web</button>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                            <a href="javascript: void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u=<?= WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_subcategoria->slug ?>','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');">
                                                <button class="btn button-blue fb-share-button"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</button>
                                            </a>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                            <button class="btn button-blue" id="copiar-clipboard-<?= $produto_similar_subcategoria->id ?>"><i class="fa fa-clone" aria-hidden="true"></i> Copiar link</button>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                            <button  class="btn button-blue abrir-email"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="tablet-hidden enviar-whats">
                                            <div class="col-xs-4 text-center form-group">
                                                <?= $this->Form->input('whats-web-form', [
                                                    'div'           => false,
                                                    'id'            => 'nro-whatsweb-'.$produto_similar_subcategoria->id,
                                                    'label'         => false,
                                                    'class'         => 'form-control form-indicar',
                                                    'placeholder'   => "Celular*: (00)00000-0000"
                                                ])?>
                                            </div>
                                            <div class="col-xs-4 text-left obs-overlay">
                                                <p>*Para enviar pra mais de um contato deixe este campo vazio.</p>
                                            </div>
                                            <div class="col-xs-4 text-center form-group">
                                                <?= $this->Form->button('Enviar', [
                                                'type'          => 'button',
                                                'data-id'       => $produto_similar_subcategoria->id,
                                                'class'         => 'btn button-blue',
                                                'id'            => 'enviar-whatsweb-'.$produto_similar_subcategoria->id
                                                ])?>
                                            </div>
                                            <script>
                                                $(document).ready(function() {
                                                    $('#enviar-whatsweb-<?=$produto_similar_subcategoria->id ?>').click(function(){

                                                        var numero;
                                                        var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_subcategoria->slug ?>";

                                                        if($('#nro-whatsweb-<?= $produto_similar_subcategoria->id ?>').val() != '') {
                                                            numero = '55'+$('#nro-whatsweb-<?= $produto_similar_subcategoria->id ?>').val();
                                                        }
                                                        
                                                        window.open('https://api.whatsapp.com/send?phone=' + numero + '&text=Isso é a sua cara! ' + link);
                                                    });
                                                });
                                            </script>
                                        </div>
                                        <div class="col-xs-12 enviar-email" id="collapse-email-<?=$produto_similar_subcategoria->id ?>">
                                            <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$produto_similar_subcategoria->id])?>
                                            <?= $this->Form->input('name', [
                                                'div'           => false,
                                                'label'         => false,
                                                'class'         => 'form-control validate[optional] form-indicar name-'.$produto_similar_subcategoria->id,
                                                'placeholder'   => 'Quem indicou?',
                                            ])?>
                                            <br>
                                            <?= $this->Form->input('email', [
                                                'div'           => false,
                                                'label'         => false,
                                                'class'         => 'form-control validate[required] form-indicar email-'.$produto_similar_subcategoria->id,
                                                'placeholder'   => 'E-mail do destinatário',
                                            ])?>
                                            <?= $this->Form->input('id_produto', [
                                                'div'           => false,
                                                'class'         => 'id_produto-'.$produto_similar_subcategoria->id,
                                                'value'         => $produto_similar_subcategoria->id,
                                                'label'         => false,
                                                'type'          => 'hidden'
                                            ])?>
                                            <br/>
                                            <?= $this->Form->button('Enviar', [
                                                'id'            => 'send-indicacao-btn',
                                                'type'          => 'button',
                                                'data-id'       => $produto_similar_subcategoria->id,
                                                'class'         => 'btn button-blue send-indicacao-home-btn'
                                            ])?>
                                            <br />
                                            <?= $this->Form->end()?>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                        <div class="col-xs-12 clipboard">   
                                            <?= $this->Form->input('url', [
                                                'div'           => false,
                                                'id'            => 'url-clipboard-'.$produto_similar_subcategoria->id,
                                                'label'         => false,
                                                'value'         => WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_subcategoria->slug,
                                                'readonly'      => true,
                                                'class'         => 'form-control form-indicar'
                                            ])?>
                                        </div>  
                                        <p><span class="copy-alert hide" id="copy-alert-<?=$produto_similar_subcategoria->id ?>">Copiado!</span></p>
                                        <script>
                                            $(document).ready(function() {
                                                // Copy to clipboard 
                                                document.querySelector("#copiar-clipboard-<?=$produto_similar_subcategoria->id ?>").onclick = () => {
                                                  // Select the content
                                                    document.querySelector("#url-clipboard-<?=$produto_similar_subcategoria->id ?>").select();
                                                    // Copy to the clipboard
                                                    document.execCommand('copy');
                                                    // Exibe span de sucesso
                                                    $('#copy-alert-<?=$produto_similar_subcategoria->id ?>').removeClass("hide");
                                                    // Oculta span de sucesso
                                                    setTimeout(function() {
                                                        $('#copy-alert-<?=$produto_similar_subcategoria->id ?>').addClass("hide");
                                                    }, 1500);
                                                };
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php $ctrl++; ?>
                <?php } ?>

                <?php $ctrl = 0; ?>
                <?php foreach($similares_categoria as $similar_categoria) { ?>
                    <?php if(count($similar_categoria) >= 1) { ?>
                        <div class="title-section">
                            <h3>Produtos da categoria <span style="color: #5089cf"><?= $similares_categoria_name[$ctrl] ?></span></h3>
                            <?= $this->Html->link('<span class="fa fa-plus"></span> ver todos',
                                $SSlug.'/pesquisa/0/0/categoria/'.$similares_categoria_slug[$ctrl].'/', ['escape' => false, 'style' => 'text-transform: uppercase']
                            ); ?>
                        </div>
                        <div class="owl-carousel">
                            <?php $i = 1; ?>
                            <?php foreach($similar_categoria as $produto_similar_categoria) { ?>
                                <div class="item">
                                    <?php $fotos = unserialize($produto_similar_categoria->fotos); ?>
                                    <div class="box-completa-produto" itemtype="http://schema.org/Product">
                                        <div class="tarja-objetivo">
                                            <?php
                                                $contador_objetivos = 0; 
                                                $novo_top = 0;
                                            ?>
                                            <?php foreach ($produto_similar_categoria->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                                <?php if ($contador_objetivos < 2) { ?>
                                                <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                                                    <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                    $contador_objetivos++;
                                                    $novo_top = $novo_top + 19;
                                                 ?>
                                            <?php } ?>
                                        </div>
                                        <div class="total-tarjas">
                                            <?php if ($produto_similar_categoria->estoque == 1) { ?>
                                                <div class="tarja tarja-ultimo">
                                                    <span>Último</span>
                                                </div>
                                            <?php } else if ($produto_similar_categoria->estoque >= 1 && $produto_similar_categoria->preco_promo || $desconto_geral) { ?>
                                                <div class="tarja tarja-promocao">
                                                    <span>Promoção</span>
                                                </div>
                                            <?php } else if($produto_similar_categoria->estoque >= 1 && $produto_similar_categoria->created > $max_date_novidade) { ?>
                                                <div class="tarja tarja-novidade">
                                                    <span>Novidade</span>
                                                </div>
                                            <?php } ?>               
                                        </div>
                                        <div class="info-produto">
                                            <div class="produto-superior">
                                                <a href="<?= $SSlug.'/produto/'.$produto_similar_categoria->slug ?>">
                                                    <?php $fotos = unserialize($produto_similar_categoria->fotos); ?>
                                                    <div class="foto-produto" id="produto-<?= $i ?>" >
                                                        <?= $this->Html->image(
                                                            'produtos/md-'.$fotos[0],
                                                            ['val' => $produto_similar_categoria->slug, 'alt' => $produto_similar_categoria->produto_base->name.' '.$produto_similar_categoria->produto_base->embalagem_conteudo]); ?>
                                                    </div>
                                                    <div class="detalhes-produto">
                                                        <div class="informacoes-detalhes text-left">
                                                            <p>Código: <?= $produto_similar_categoria->id ?></p>
                                                            <p><?= strtolower($produto_similar_categoria->produto_base->marca->name); ?></p>
                                                            <p><?= $produto_similar_categoria->tamanho.' '.$produto_similar_categoria->unidade_medida; ?></p>
                                                            <p>para <?= $produto_similar_categoria->doses; ?> treinos</p>
                                                        <p><?= strtolower($produto_similar_categoria->propriedade); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="produto-inferior">
                                                <?php if($produto_similar_categoria->preco_promo) { ?>
                                                <?php $porcentagem_nova = $produto_similar_categoria->preco * 0.089;
                                                $preco_corte = $produto_similar_categoria->preco + $porcentagem_nova; 
                                                } ?> 
                                                <?php $produto_similar_categoria->preco_promo ?
                                                number_format($preco = $produto_similar_categoria->preco_promo, 2, ',','.') :
                                                number_format($preco = $produto_similar_categoria->preco, 2, ',','.'); ?>
                                                <?php $produto_similar_categoria->preco_promo ? $porcentagem_nova = $produto_similar_categoria->preco_promo * 0.089 : $porcentagem_nova = $produto_similar_categoria->preco * 0.089 ?>
                                                <?php if($produto_similar_categoria->preco_promo != null) {
                                                    if($produto_similar_categoria->preco_promo < 100.00) { ?>
                                                    <?php $parcel = $produto_similar_categoria->preco_promo * 0.089; $parcel = ($produto_similar_categoria->preco_promo + $parcel)/2?>
                                                    <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_categoria->preco_promo > 100.00 && $produto_similar_categoria->preco_promo < 200.00) { ?>
                                                    <?php $parcel = $produto_similar_categoria->preco_promo * 0.089; $parcel = ($produto_similar_categoria->preco_promo + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_categoria->preco_promo > 200.00 && $produto_similar_categoria->preco_promo < 300.00) { ?>
                                                    <?php $parcel = $produto_similar_categoria->preco_promo * 0.089; $parcel = ($produto_similar_categoria->preco_promo + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_categoria->preco_promo > 300.00) { ?>
                                                    <?php $parcel = $produto_similar_categoria->preco_promo * 0.089; $parcel = ($produto_similar_categoria->preco_promo + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                    } else {
                                                if($produto_similar_categoria->preco < 100.00) { ?>
                                                    <?php $parcel = $produto_similar_categoria->preco * 0.089; $parcel = ($produto_similar_categoria->preco + $parcel)/2?>
                                                     <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_categoria->preco > 100.00 && $produto_similar_categoria->preco < 200.00) { ?>
                                                    <?php $parcel = $produto_similar_categoria->preco * 0.089; $parcel = ($produto_similar_categoria->preco + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_categoria->preco > 200.00 && $produto_similar_categoria->preco < 300.00) { ?>
                                                    <?php $parcel = $produto_similar_categoria->preco * 0.089; $parcel = ($produto_similar_categoria->preco + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_categoria->preco > 300.00) { ?>
                                                    <?php $parcel = $produto_similar_categoria->preco * 0.089; $parcel = ($produto_similar_categoria->preco + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                } ?>
                                                <?php 
                                                    if($produto_similar_categoria->preco_promo) {
                                                        echo $this->Html->link("
                                                            <h4>".$produto_similar_categoria->produto_base->name."</h4>
                                                            <p><span class='preco-corte'> R$".number_format($preco_corte, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            $SSlug.'/produto/'.$produto_similar_categoria->slug,
                                                            ['escape' => false]);
                                                    } else {
                                                        echo $this->Html->link("
                                                            <h4>".$produto_similar_categoria->produto_base->name."</h4>
                                                            <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            $SSlug.'/produto/'.$produto_similar_categoria->slug,
                                                            ['escape' => false]);
                                                    }
                                                ?>
                                            </div>
                                            <div class="hover-produto <?= $produto_similar_categoria->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                                                <div class="col-xs-12">
                                                    <div class="hexagono-indicar">
                                                        <?php if($produto_similar_categoria->status_id == 1) { ?>
                                                            <div class="hexagon-disponivel btn-indique" data-id="<?= $produto_similar_categoria->id ?>">
                                                                <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                                                <p>indique</p>
                                                            </div>
                                                            <script>
                                                                $('.btn-indique').click(function () {
                                                                        var id_produto = $(this).attr('data-id');
                                                                        $('#overlay-indicacao-'+id_produto).height('100%');
                                                                        $("html,body").css({"overflow":"hidden"});
                                                                });
                                                                function closeIndica() {
                                                                    $('.overlay-indicacao').height('0%');
                                                                    $("html,body").css({"overflow":"auto"});
                                                                }
                                                            </script>
                                                        <?php } else { ?>
                                                            <div class="hexagon produto-acabou-btn" data-id="<?= $produto_similar_categoria->id ?>">
                                                                <span><i class="fa fa-envelope-o"></i></span>
                                                                <p>avise-me</p>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <?php if($produto_similar_categoria->status_id == 1) { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon-disponivel">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,$SSlug.'/produto/'.$produto_similar_categoria->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon-disponivel btn-comparar-action" data-id="<?= $produto_similar_categoria->id ?>">
                                                            <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                                            <p>comparar</p>
                                                        </div>
                                                    </div>
                                                <?php }
                                                else { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,$SSlug.'/produto/'.$produto_similar_categoria->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon btn-comparar-action" data-id="<?= $produto_similar_categoria->id ?>">
                                                            <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                                            <p>comparar</p>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $('.send-indicacao-home-btn').click(function(e) {
                                                e.preventDefault();
                                                var botao = $(this);
                                            
                                                botao.html('Enviando...');
                                                botao.attr('disabled', 'disabled');

                                                var id = $(this).attr('data-id');
                                                var slug = '<?= $SSlug ?>';

                                                var valid = $('.indicar_prod_home_'+id).validationEngine("validate");
                                                if (valid == true) {
                                                    $.ajax({
                                                        type: "POST",
                                                        url: WEBROOT_URL + slug + '/produtos/overlay-indicar-produtos-home/',
                                                        data: {
                                                            name: $('.name-'+id).val(),
                                                            email: $('.email-'+id).val(), 
                                                            produto_id: $('.id_produto-'+id).val()
                                                        }
                                                    })
                                                    .done(function (data) {
                                                        if(data == 1) {
                                                            botao.html('Enviado!');
                                                            setTimeout(function () {
                                                                botao.html('Enviar');
                                                                botao.removeAttr('disabled');
                                                            }, 2200);
                                                        } else {
                                                            botao.html('Falha ao enviar... Tente novamente...');
                                                            setTimeout(function () {
                                                                botao.html('Enviar');
                                                                botao.removeAttr('disabled');
                                                            }, 2200);
                                                        }
                                                    });
                                                }else{
                                                    $('.indicar_prod_home_'+id).validationEngine({
                                                        updatePromptsPosition: true,
                                                        promptPosition: 'inline',
                                                        scroll: false
                                                    });
                                                }
                                            });
                                        </script>
                                        <div class="botoes-comprar-similiar">
                                            <?php if($produto_similar_categoria->visivel == 1 && $produto_similar_categoria->status_id == 1) { ?>
                                                <?php if($Academia->prazo_medio > 0 && $Academia->prazo_medio <= 72) { ?>
                                                    <?php 
                                                        if($Academia->prazo_medio <= 12) {
                                                            $prazo_medio = 12;
                                                        } else if($Academia->prazo_medio > 12 && $Academia->prazo_medio <= 24) {
                                                            $prazo_medio = 24;
                                                        } else if($Academia->prazo_medio > 24 && $Academia->prazo_medio <= 48) {
                                                            $prazo_medio = 48;
                                                        } else if($Academia->prazo_medio > 48) {
                                                            $prazo_medio = 72;
                                                        }
                                                    ?>
                                                    <p class="texto-prazo"><i class="fa fa-truck" aria-hidden="true"></i> Entrega em até <?= $prazo_medio ?>h</p>
                                                <?php } ?>
                                                <button class="btn btn-incluir incluir-mochila" val="<?= $produto_similar_categoria->slug ?>" data-id="<?= $produto_similar_categoria->id ?>">Comprar</button>
                                                <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $produto_similar_categoria->id ?>">Indique</button>
                                                <button class="desktop-hidden btn btn-incluir produto-comparar-btn btn-comparar-action" data-id="<?= $produto_similar_categoria->id ?>">Comparar</button> 
                                            <?php } else if($produto_similar_categoria->visivel == 1 && $produto_similar_categoria->status_id == 2) { ?>
                                                <button class="btn btn-similares">Similares</button>            
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++; ?>
                            <?php } ?>
                            <div class="item">
                                <div class="box-completa-produto" id="produto-<?= $i ?>">
                                    <div class="col-md-12 col-xs-12 text-center bg-white-produto prdt">
                                        <div class="text-center" style="margin-top: 130px;">
                                            <?= $this->Html->link('<span class="fa fa-plus fa-2x"></span>',
                                                $SSlug.'/pesquisa/0/0/categoria/'.$similares_categoria_slug[$ctrl].'/', ['escape' => false, 'style' => 'font-size: 20px; position: relative; width: 100%']
                                            ); ?>
                                            <?= $this->Html->link('ver mais',
                                                $SSlug.'/pesquisa/0/0/categoria/'.$similares_categoria_slug[$ctrl].'/', ['escape' => false, 'style' => 'font-size: 25px; position: relative; width: 100%']
                                            ); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php foreach($similar_categoria as $produto_similar_categoria) { ?>
                            <div id="aviseMe-<?= $produto_similar_categoria->id ?>" class="overlay overlay-avise">
                                <div class="overlay-avise-content overlay-avise-<?= $produto_similar_categoria->id ?> text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                                    <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                    <br />
                                    <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$produto_similar_categoria->id])?>
                                    <?= $this->Form->input('produto_id', [
                                        'div'           => false,
                                        'label'         => false,
                                        'type'          => 'hidden',
                                        'val'           => $produto_similar_categoria->id,
                                        'id'            => 'produto_id_acabou-'.$produto_similar_categoria->id
                                    ])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'name_acabou-'.$produto_similar_categoria->id,
                                        'class'         => 'form-control validate[optional]',
                                        'placeholder'   => 'Qual o seu nome?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'email_acabou-'.$produto_similar_categoria->id,
                                        'class'         => 'form-control validate[required, custom[email]]',
                                        'placeholder'   => 'Qual o seu email?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->button('Enviar', [
                                        'data-id'       => $produto_similar_categoria->id,
                                        'type'          => 'button',
                                        'class'         => 'btn btn-success send-acabou-btn'
                                    ])?>
                                    <br />
                                    <br />
                                    <?= $this->Form->end()?>
                                </div>
                            </div> 
                            <div id="overlay-indicacao-<?= $produto_similar_categoria->id ?>" class="overlay overlay-indicacao">
                                <div class="overlay-content text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                                    <h4>Lembrou de alguém quando viu isso?</h4>
                                    <h4>Indique este produto!</h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-12 share-whastapp share-buttons">
                                            <a href="whatsapp://send?text=Isso é a sua cara! <?= WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_categoria->slug ?>">
                                                <button class="btn button-blue"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</button>
                                            </a>
                                        </div>
                                        <div class="col-xs-12 share-whastapp share-buttons">
                                            <button class="btn button-blue" id="share-<?= $produto_similar_categoria->id ?>"><i class="fa fa-comment-o" aria-hidden="true"></i> Messenger</button>
                                            <script>
                                                $(document).ready(function() {
                                                    $('#share-<?= $produto_similar_categoria->id ?>').click(function(){
                                                        var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_categoria->slug ?>";
                                                        var app_id = '1321574044525013';
                                                        window.open('fb-messenger://share?link=' + link + '&app_id=' + app_id);
                                                    });
                                                });
                                            </script>
                                        </div>
                                         <div class="tablet-hidden col-xs-12 col-md-3 share-buttons">
                                            <button data-id="whatsweb-<?= $produto_similar_categoria->id ?>" class="btn button-blue abrir-whats"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp Web</button>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                            <a href="javascript: void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u=<?= WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_categoria->slug ?>','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');">
                                                <button class="btn button-blue fb-share-button"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</button>
                                            </a>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                            <button class="btn button-blue" id="copiar-clipboard-<?= $produto_similar_categoria->id ?>"><i class="fa fa-clone" aria-hidden="true"></i> Copiar link</button>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                            <button  class="btn button-blue abrir-email"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="tablet-hidden enviar-whats">
                                            <div class="col-xs-4 text-center form-group">
                                                <?= $this->Form->input('whats-web-form', [
                                                    'div'           => false,
                                                    'id'            => 'nro-whatsweb-'.$produto_similar_categoria->id,
                                                    'label'         => false,
                                                    'class'         => 'form-control form-indicar',
                                                    'placeholder'   => "Celular*: (00)00000-0000"
                                                ])?>
                                            </div>
                                            <div class="col-xs-4 text-left obs-overlay">
                                                <p>*Para enviar pra mais de um contato deixe este campo vazio.</p>
                                            </div>
                                            <div class="col-xs-4 text-center form-group">
                                                <?= $this->Form->button('Enviar', [
                                                'type'          => 'button',
                                                'data-id'       => $produto_similar_categoria->id,
                                                'class'         => 'btn button-blue',
                                                'id'            => 'enviar-whatsweb-'.$produto_similar_categoria->id
                                                ])?>
                                            </div>
                                            <script>
                                                $(document).ready(function() {
                                                    $('#enviar-whatsweb-<?=$produto_similar_categoria->id ?>').click(function(){

                                                        var numero;
                                                        var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_categoria->slug ?>";

                                                        if($('#nro-whatsweb-<?= $produto_similar_categoria->id ?>').val() != '') {
                                                            numero = '55'+$('#nro-whatsweb-<?= $produto_similar_categoria->id ?>').val();
                                                        }
                                                        
                                                        window.open('https://api.whatsapp.com/send?phone=' + numero + '&text=Isso é a sua cara! ' + link);
                                                    });
                                                });
                                            </script>
                                        </div>
                                        <div class="col-xs-12 enviar-email" id="collapse-email-<?=$produto_similar_categoria->id ?>">
                                            <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$produto_similar_categoria->id])?>
                                            <?= $this->Form->input('name', [
                                                'div'           => false,
                                                'label'         => false,
                                                'class'         => 'form-control validate[optional] form-indicar name-'.$produto_similar_categoria->id,
                                                'placeholder'   => 'Quem indicou?',
                                            ])?>
                                            <br>
                                            <?= $this->Form->input('email', [
                                                'div'           => false,
                                                'label'         => false,
                                                'class'         => 'form-control validate[required] form-indicar email-'.$produto_similar_categoria->id,
                                                'placeholder'   => 'E-mail do destinatário',
                                            ])?>
                                            <?= $this->Form->input('id_produto', [
                                                'div'           => false,
                                                'class'         => 'id_produto-'.$produto_similar_categoria->id,
                                                'value'         => $produto_similar_categoria->id,
                                                'label'         => false,
                                                'type'          => 'hidden'
                                            ])?>
                                            <br/>
                                            <?= $this->Form->button('Enviar', [
                                                'id'            => 'send-indicacao-btn',
                                                'type'          => 'button',
                                                'data-id'       => $produto_similar_categoria->id,
                                                'class'         => 'btn button-blue send-indicacao-home-btn'
                                            ])?>
                                            <br />
                                            <?= $this->Form->end()?>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                        <div class="col-xs-12 clipboard">   
                                            <?= $this->Form->input('url', [
                                                'div'           => false,
                                                'id'            => 'url-clipboard-'.$produto_similar_categoria->id,
                                                'label'         => false,
                                                'value'         => WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_categoria->slug,
                                                'readonly'      => true,
                                                'class'         => 'form-control form-indicar'
                                            ])?>
                                        </div>  
                                        <p><span class="copy-alert hide" id="copy-alert-<?=$produto_similar_categoria->id ?>">Copiado!</span></p>
                                        <script>
                                            $(document).ready(function() {
                                                // Copy to clipboard 
                                                document.querySelector("#copiar-clipboard-<?=$produto_similar_categoria->id ?>").onclick = () => {
                                                  // Select the content
                                                    document.querySelector("#url-clipboard-<?=$produto_similar_categoria->id ?>").select();
                                                    // Copy to the clipboard
                                                    document.execCommand('copy');
                                                    // Exibe span de sucesso
                                                    $('#copy-alert-<?=$produto_similar_categoria->id ?>').removeClass("hide");
                                                    // Oculta span de sucesso
                                                    setTimeout(function() {
                                                        $('#copy-alert-<?=$produto_similar_categoria->id ?>').addClass("hide");
                                                    }, 1500);
                                                };
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php $ctrl++; ?>
                <?php } ?>

                <?php $ctrl = 0; ?>
                <?php foreach($similares_objetivo as $similar_objetivo) { ?>
                    <?php if(count($similares_objetivo) >= 1) { ?>
                        <div class="title-section">
                            <h3>Produtos do objetivo <span style="color: #5089cf"><?= $similares_objetivo_name[$ctrl] ?></span></h3>
                            <?= $this->Html->link('<span class="fa fa-plus"></span> ver todos',
                                $SSlug.'/pesquisa/0/0/objetivo/'.$similares_objetivo_slug[$ctrl].'/', ['escape' => false, 'style' => 'text-transform: uppercase']
                            ); ?>
                        </div>
                        <div class="owl-carousel">
                            <?php $i = 1; ?>
                            <?php foreach($similar_objetivo as $produto_similar_objetivo) { ?>
                                <div class="item">
                                    <?php $fotos = unserialize($produto_similar_objetivo->fotos); ?>
                                    <div class="box-completa-produto" itemtype="http://schema.org/Product">
                                        <div class="tarja-objetivo">
                                            <?php
                                                $contador_objetivos = 0; 
                                                $novo_top = 0;
                                            ?>
                                            <?php foreach ($produto_similar_objetivo->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                                <?php if ($contador_objetivos < 2) { ?>
                                                <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                                                    <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                    $contador_objetivos++;
                                                    $novo_top = $novo_top + 19;
                                                 ?>
                                            <?php } ?>
                                        </div>
                                        <div class="total-tarjas">
                                            <?php if ($produto_similar_objetivo->estoque == 1) { ?>
                                                <div class="tarja tarja-ultimo">
                                                    <span>Último</span>
                                                </div>
                                            <?php } else if ($produto_similar_objetivo->estoque >= 1 && $produto_similar_objetivo->preco_promo || $desconto_geral) { ?>
                                                <div class="tarja tarja-promocao">
                                                    <span>Promoção</span>
                                                </div>
                                            <?php } else if($produto_similar_objetivo->estoque >= 1 && $produto_similar_objetivo->created > $max_date_novidade) { ?>
                                                <div class="tarja tarja-novidade">
                                                    <span>Novidade</span>
                                                </div>
                                            <?php } ?>               
                                        </div>
                                        <div class="info-produto">
                                            <div class="produto-superior">
                                                <a href="<?= $SSlug.'/produto/'.$produto_similar_objetivo->slug ?>">
                                                    <?php $fotos = unserialize($produto_similar_objetivo->fotos); ?>
                                                    <div class="foto-produto" id="produto-<?= $i ?>" >
                                                        <?= $this->Html->image(
                                                            'produtos/md-'.$fotos[0],
                                                            ['val' => $produto_similar_objetivo->slug, 'alt' => $produto_similar_objetivo->produto_base->name.' '.$produto_similar_objetivo->produto_base->embalagem_conteudo]); ?>
                                                    </div>
                                                    <div class="detalhes-produto">
                                                        <div class="informacoes-detalhes text-left">
                                                            <p>Código: <?= $produto_similar_objetivo->id ?></p>
                                                            <p><?= strtolower($produto_similar_objetivo->produto_base->marca->name); ?></p>
                                                            <p><?= $produto_similar_objetivo->tamanho.' '.$produto_similar_objetivo->unidade_medida; ?></p>
                                                            <p>para <?= $produto_similar_objetivo->doses; ?> treinos</p>
                                                        <p><?= strtolower($produto_similar_objetivo->propriedade); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="produto-inferior">
                                                <?php if($produto_similar_objetivo->preco_promo) { ?>
                                                <?php $porcentagem_nova = $produto_similar_objetivo->preco * 0.089;
                                                $preco_corte = $produto_similar_objetivo->preco + $porcentagem_nova; 
                                                } ?> 
                                                <?php $produto_similar_objetivo->preco_promo ?
                                                number_format($preco = $produto_similar_objetivo->preco_promo, 2, ',','.') :
                                                number_format($preco = $produto_similar_objetivo->preco, 2, ',','.'); ?>
                                                <?php $produto_similar_objetivo->preco_promo ? $porcentagem_nova = $produto_similar_objetivo->preco_promo * 0.089 : $porcentagem_nova = $produto_similar_objetivo->preco * 0.089 ?>
                                                <?php if($produto_similar_objetivo->preco_promo != null) {
                                                    if($produto_similar_objetivo->preco_promo < 100.00) { ?>
                                                    <?php $parcel = $produto_similar_objetivo->preco_promo * 0.089; $parcel = ($produto_similar_objetivo->preco_promo + $parcel)/2?>
                                                    <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_objetivo->preco_promo > 100.00 && $produto_similar_objetivo->preco_promo < 200.00) { ?>
                                                    <?php $parcel = $produto_similar_objetivo->preco_promo * 0.089; $parcel = ($produto_similar_objetivo->preco_promo + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_objetivo->preco_promo > 200.00 && $produto_similar_objetivo->preco_promo < 300.00) { ?>
                                                    <?php $parcel = $produto_similar_objetivo->preco_promo * 0.089; $parcel = ($produto_similar_objetivo->preco_promo + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_objetivo->preco_promo > 300.00) { ?>
                                                    <?php $parcel = $produto_similar_objetivo->preco_promo * 0.089; $parcel = ($produto_similar_objetivo->preco_promo + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                    } else {
                                                if($produto_similar_objetivo->preco < 100.00) { ?>
                                                    <?php $parcel = $produto_similar_objetivo->preco * 0.089; $parcel = ($produto_similar_objetivo->preco + $parcel)/2?>
                                                     <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_objetivo->preco > 100.00 && $produto_similar_objetivo->preco < 200.00) { ?>
                                                    <?php $parcel = $produto_similar_objetivo->preco * 0.089; $parcel = ($produto_similar_objetivo->preco + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_objetivo->preco > 200.00 && $produto_similar_objetivo->preco < 300.00) { ?>
                                                    <?php $parcel = $produto_similar_objetivo->preco * 0.089; $parcel = ($produto_similar_objetivo->preco + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($produto_similar_objetivo->preco > 300.00) { ?>
                                                    <?php $parcel = $produto_similar_objetivo->preco * 0.089; $parcel = ($produto_similar_objetivo->preco + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                } ?>
                                                <?php 
                                                    if($produto_similar_objetivo->preco_promo) {
                                                        echo $this->Html->link("
                                                            <h4>".$produto_similar_objetivo->produto_base->name."</h4>
                                                            <p><span class='preco-corte'> R$".number_format($preco_corte, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            $SSlug.'/produto/'.$produto_similar_objetivo->slug,
                                                            ['escape' => false]);
                                                    } else {
                                                        echo $this->Html->link("
                                                            <h4>".$produto_similar_objetivo->produto_base->name."</h4>
                                                            <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            $SSlug.'/produto/'.$produto_similar_objetivo->slug,
                                                            ['escape' => false]);
                                                    }
                                                ?>
                                            </div>
                                            <div class="hover-produto <?= $produto_similar_objetivo->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                                                <div class="col-xs-12">
                                                    <div class="hexagono-indicar">
                                                        <?php if($produto_similar_objetivo->status_id == 1) { ?>
                                                            <div class="hexagon-disponivel btn-indique" data-id="<?= $produto_similar_objetivo->id ?>">
                                                                <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                                                <p>indique</p>
                                                            </div>
                                                            <script>
                                                                $('.btn-indique').click(function () {
                                                                        var id_produto = $(this).attr('data-id');
                                                                        $('#overlay-indicacao-'+id_produto).height('100%');
                                                                        $("html,body").css({"overflow":"hidden"});
                                                                });
                                                                function closeIndica() {
                                                                    $('.overlay-indicacao').height('0%');
                                                                    $("html,body").css({"overflow":"auto"});
                                                                }
                                                            </script>
                                                        <?php } else { ?>
                                                            <div class="hexagon produto-acabou-btn" data-id="<?= $produto_similar_objetivo->id ?>">
                                                                <span><i class="fa fa-envelope-o"></i></span>
                                                                <p>avise-me</p>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <?php if($produto_similar_objetivo->status_id == 1) { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon-disponivel">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,$SSlug.'/produto/'.$produto_similar_objetivo->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon-disponivel btn-comparar-action" data-id="<?= $produto_similar_objetivo->id ?>">
                                                            <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                                            <p>comparar</p>
                                                        </div>
                                                    </div>
                                                <?php }
                                                else { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,$SSlug.'/produto/'.$produto_similar_objetivo->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon btn-comparar-action" data-id="<?= $produto_similar_objetivo->id ?>">
                                                            <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                                            <p>comparar</p>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $('.send-indicacao-home-btn').click(function(e) {
                                                e.preventDefault();
                                                var botao = $(this);
                                            
                                                botao.html('Enviando...');
                                                botao.attr('disabled', 'disabled');

                                                var id = $(this).attr('data-id');
                                                var slug = '<?= $SSlug ?>';

                                                var valid = $('.indicar_prod_home_'+id).validationEngine("validate");
                                                if (valid == true) {
                                                    $.ajax({
                                                        type: "POST",
                                                        url: WEBROOT_URL + slug + '/produtos/overlay-indicar-produtos-home/',
                                                        data: {
                                                            name: $('.name-'+id).val(),
                                                            email: $('.email-'+id).val(), 
                                                            produto_id: $('.id_produto-'+id).val()
                                                        }
                                                    })
                                                    .done(function (data) {
                                                        if(data == 1) {
                                                            botao.html('Enviado!');
                                                            setTimeout(function () {
                                                                botao.html('Enviar');
                                                                botao.removeAttr('disabled');
                                                            }, 2200);
                                                        } else {
                                                            botao.html('Falha ao enviar... Tente novamente...');
                                                            setTimeout(function () {
                                                                botao.html('Enviar');
                                                                botao.removeAttr('disabled');
                                                            }, 2200);
                                                        }
                                                    });
                                                }else{
                                                    $('.indicar_prod_home_'+id).validationEngine({
                                                        updatePromptsPosition: true,
                                                        promptPosition: 'inline',
                                                        scroll: false
                                                    });
                                                }
                                            });
                                        </script>
                                        <div class="botoes-comprar-similiar">
                                            <?php if($produto_similar_objetivo->visivel == 1 && $produto_similar_objetivo->status_id == 1) { ?>
                                                <?php if($Academia->prazo_medio > 0 && $Academia->prazo_medio <= 72) { ?>
                                                    <?php 
                                                        if($Academia->prazo_medio <= 12) {
                                                            $prazo_medio = 12;
                                                        } else if($Academia->prazo_medio > 12 && $Academia->prazo_medio <= 24) {
                                                            $prazo_medio = 24;
                                                        } else if($Academia->prazo_medio > 24 && $Academia->prazo_medio <= 48) {
                                                            $prazo_medio = 48;
                                                        } else if($Academia->prazo_medio > 48) {
                                                            $prazo_medio = 72;
                                                        }
                                                    ?>
                                                    <p class="texto-prazo"><i class="fa fa-truck" aria-hidden="true"></i> Entrega em até <?= $prazo_medio ?>h</p>
                                                <?php } ?>
                                                <button class="btn btn-incluir incluir-mochila" val="<?= $produto_similar_objetivo->slug ?>" data-id="<?= $produto_similar_objetivo->id ?>">Comprar</button>
                                                <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $produto_similar_objetivo->id ?>">Indique</button>
                                                <button class="desktop-hidden btn btn-incluir produto-comparar-btn btn-comparar-action" data-id="<?= $produto_similar_objetivo->id ?>">Comparar</button> 
                                            <?php } else if($produto_similar_objetivo->visivel == 1 && $produto_similar_objetivo->status_id == 2) { ?>
                                                <button class="btn btn-similares">Similares</button>            
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++; ?>
                            <?php } ?>
                            <div class="item">
                                <div class="box-completa-produto" id="produto-<?= $i ?>">
                                    <div class="col-md-12 col-xs-12 text-center bg-white-produto prdt">
                                        <div class="text-center" style="margin-top: 130px;">
                                            <?= $this->Html->link('<span class="fa fa-plus fa-2x"></span>',
                                                $SSlug.'/pesquisa/0/0/objetivo/'.$similares_objetivo_slug[$ctrl].'/', ['escape' => false, 'style' => 'font-size: 20px; position: relative; width: 100%']
                                            ); ?>
                                            <?= $this->Html->link('ver mais',
                                                $SSlug.'/pesquisa/0/0/objetivo/'.$similares_objetivo_slug[$ctrl].'/', ['escape' => false, 'style' => 'font-size: 25px; position: relative; width: 100%']
                                            ); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php foreach($similar_objetivo as $produto_similar_objetivo) { ?>
                            <div id="aviseMe-<?= $produto_similar_objetivo->id ?>" class="overlay overlay-avise">
                                <div class="overlay-avise-content overlay-avise-<?= $produto_similar_objetivo->id ?> text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                                    <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                    <br />
                                    <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$produto_similar_objetivo->id])?>
                                    <?= $this->Form->input('produto_id', [
                                        'div'           => false,
                                        'label'         => false,
                                        'type'          => 'hidden',
                                        'val'           => $produto_similar_objetivo->id,
                                        'id'            => 'produto_id_acabou-'.$produto_similar_objetivo->id
                                    ])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'name_acabou-'.$produto_similar_objetivo->id,
                                        'class'         => 'form-control validate[optional]',
                                        'placeholder'   => 'Qual o seu nome?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'email_acabou-'.$produto_similar_objetivo->id,
                                        'class'         => 'form-control validate[required, custom[email]]',
                                        'placeholder'   => 'Qual o seu email?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->button('Enviar', [
                                        'data-id'       => $produto_similar_objetivo->id,
                                        'type'          => 'button',
                                        'class'         => 'btn btn-success send-acabou-btn'
                                    ])?>
                                    <br />
                                    <br />
                                    <?= $this->Form->end()?>
                                </div>
                            </div> 
                            <div id="overlay-indicacao-<?= $produto_similar_objetivo->id ?>" class="overlay overlay-indicacao">
                                <div class="overlay-content text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                                    <h4>Lembrou de alguém quando viu isso?</h4>
                                    <h4>Indique este produto!</h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-12 share-whastapp share-buttons">
                                            <a href="whatsapp://send?text=Isso é a sua cara! <?= WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_objetivo->slug ?>">
                                                <button class="btn button-blue"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</button>
                                            </a>
                                        </div>
                                        <div class="col-xs-12 share-whastapp share-buttons">
                                            <button class="btn button-blue" id="share-<?= $produto_similar_objetivo->id ?>"><i class="fa fa-comment-o" aria-hidden="true"></i> Messenger</button>
                                            <script>
                                                $(document).ready(function() {
                                                    $('#share-<?= $produto_similar_objetivo->id ?>').click(function(){
                                                        var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_objetivo->slug ?>";
                                                        var app_id = '1321574044525013';
                                                        window.open('fb-messenger://share?link=' + link + '&app_id=' + app_id);
                                                    });
                                                });
                                            </script>
                                        </div>
                                        <div class="tablet-hidden col-xs-12 col-md-3 share-buttons">
                                            <button data-id="whatsweb-<?= $produto_similar_objetivo->id ?>" class="btn button-blue abrir-whats"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp Web</button>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                            <a href="javascript: void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u=<?= WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_objetivo->slug ?>','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');">
                                                <button class="btn button-blue fb-share-button"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</button>
                                            </a>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                            <button class="btn button-blue" id="copiar-clipboard-<?= $produto_similar_objetivo->id ?>"><i class="fa fa-clone" aria-hidden="true"></i> Copiar link</button>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                            <button  class="btn button-blue abrir-email"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="tablet-hidden enviar-whats">
                                            <div class="col-xs-4 text-center form-group">
                                                <?= $this->Form->input('whats-web-form', [
                                                    'div'           => false,
                                                    'id'            => 'nro-whatsweb-'.$produto_similar_objetivo->id,
                                                    'label'         => false,
                                                    'class'         => 'form-control form-indicar',
                                                    'placeholder'   => "Celular*: (00)00000-0000"
                                                ])?>
                                            </div>
                                            <div class="col-xs-4 text-left obs-overlay">
                                                <p>*Para enviar pra mais de um contato deixe este campo vazio.</p>
                                            </div>
                                            <div class="col-xs-4 text-center form-group">
                                                <?= $this->Form->button('Enviar', [
                                                'type'          => 'button',
                                                'data-id'       => $produto_similar_objetivo->id,
                                                'class'         => 'btn button-blue',
                                                'id'            => 'enviar-whatsweb-'.$produto_similar_objetivo->id
                                                ])?>
                                            </div>
                                            <script>
                                                $(document).ready(function() {
                                                    $('#enviar-whatsweb-<?=$produto_similar_objetivo->id ?>').click(function(){

                                                        var numero;
                                                        var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_objetivo->slug ?>";

                                                        if($('#nro-whatsweb-<?= $produto_similar_objetivo->id ?>').val() != '') {
                                                            numero = '55'+$('#nro-whatsweb-<?= $produto_similar_objetivo->id ?>').val();
                                                        }
                                                        
                                                        window.open('https://api.whatsapp.com/send?phone=' + numero + '&text=Isso é a sua cara! ' + link);
                                                    });
                                                });
                                            </script>
                                        </div>
                                        <div class="col-xs-12 enviar-email" id="collapse-email-<?=$produto_similar_objetivo->id ?>">
                                            <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$produto_similar_objetivo->id])?>
                                            <?= $this->Form->input('name', [
                                                'div'           => false,
                                                'label'         => false,
                                                'class'         => 'form-control validate[optional] form-indicar name-'.$produto_similar_objetivo->id,
                                                'placeholder'   => 'Quem indicou?',
                                            ])?>
                                            <br>
                                            <?= $this->Form->input('email', [
                                                'div'           => false,
                                                'label'         => false,
                                                'class'         => 'form-control validate[required] form-indicar email-'.$produto_similar_objetivo->id,
                                                'placeholder'   => 'E-mail do destinatário',
                                            ])?>
                                            <?= $this->Form->input('id_produto', [
                                                'div'           => false,
                                                'class'         => 'id_produto-'.$produto_similar_objetivo->id,
                                                'value'         => $produto_similar_objetivo->id,
                                                'label'         => false,
                                                'type'          => 'hidden'
                                            ])?>
                                            <br/>
                                            <?= $this->Form->button('Enviar', [
                                                'id'            => 'send-indicacao-btn',
                                                'type'          => 'button',
                                                'data-id'       => $produto_similar_objetivo->id,
                                                'class'         => 'btn button-blue send-indicacao-home-btn'
                                            ])?>
                                            <br />
                                            <?= $this->Form->end()?>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                        <div class="col-xs-12 clipboard">   
                                            <?= $this->Form->input('url', [
                                                'div'           => false,
                                                'id'            => 'url-clipboard-'.$produto_similar_objetivo->id,
                                                'label'         => false,
                                                'value'         => WEBROOT_URL.$SSlug.'/produto/'.$produto_similar_objetivo->slug,
                                                'readonly'      => true,
                                                'class'         => 'form-control form-indicar'
                                            ])?>
                                        </div>  
                                        <p><span class="copy-alert hide" id="copy-alert-<?=$produto_similar_objetivo->id ?>">Copiado!</span></p>
                                        <script>
                                            $(document).ready(function() {
                                                // Copy to clipboard 
                                                document.querySelector("#copiar-clipboard-<?=$produto_similar_objetivo->id ?>").onclick = () => {
                                                  // Select the content
                                                    document.querySelector("#url-clipboard-<?=$produto_similar_objetivo->id ?>").select();
                                                    // Copy to the clipboard
                                                    document.execCommand('copy');
                                                    // Exibe span de sucesso
                                                    $('#copy-alert-<?=$produto_similar_objetivo->id ?>').removeClass("hide");
                                                    // Oculta span de sucesso
                                                    setTimeout(function() {
                                                        $('#copy-alert-<?=$produto_similar_objetivo->id ?>').addClass("hide");
                                                    }, 1500);
                                                };
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php $ctrl++; ?>
                <?php } ?>

                <?php if(count($similares_marca) >= 1) { ?>
                    <div class="title-section">
                        <h3>Produtos da marca <span style="color: #5089cf"><?= $similares_marca_name ?></span></h3>
                        <?= $this->Html->link('<span class="fa fa-plus"></span> ver todos',
                            $SSlug.'/pesquisa/0/'.$similares_marca_slug.'/', ['escape' => false, 'style' => 'text-transform: uppercase']
                        ); ?>
                    </div>
                    <div class="owl-carousel">
                        <?php $i = 1; ?>
                        <?php foreach($similares_marca as $similar_marca) { ?>
                            <div class="item">
                                <?php $fotos = unserialize($similar_marca->fotos); ?>
                                <div class="box-completa-produto" itemtype="http://schema.org/Product">
                                    <div class="tarja-objetivo">
                                        <?php
                                            $contador_objetivos = 0; 
                                            $novo_top = 0;
                                        ?>
                                        <?php foreach ($similar_marca->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                            <?php if ($contador_objetivos < 2) { ?>
                                            <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                                                <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                            </div>
                                            <?php } ?>
                                            <?php
                                                $contador_objetivos++;
                                                $novo_top = $novo_top + 19;
                                             ?>
                                        <?php } ?>
                                    </div>
                                    <div class="total-tarjas">
                                        <?php if ($similar_marca->estoque == 1) { ?>
                                            <div class="tarja tarja-ultimo">
                                                <span>Último</span>
                                            </div>
                                        <?php } else if ($similar_marca->estoque >= 1 && $similar_marca->preco_promo || $desconto_geral) { ?>
                                            <div class="tarja tarja-promocao">
                                                <span>Promoção</span>
                                            </div>
                                        <?php } else if($similar_marca->estoque >= 1 && $similar_marca->created > $max_date_novidade) { ?>
                                            <div class="tarja tarja-novidade">
                                                <span>Novidade</span>
                                            </div>
                                        <?php } ?>               
                                    </div>
                                    <div class="info-produto">
                                        <div class="produto-superior">
                                            <a href="<?= $SSlug.'/produto/'.$similar_marca->slug ?>">
                                                <?php $fotos = unserialize($similar_marca->fotos); ?>
                                                <div class="foto-produto" id="produto-<?= $i ?>" >
                                                    <?= $this->Html->image(
                                                        'produtos/md-'.$fotos[0],
                                                        ['val' => $similar_marca->slug, 'alt' => $similar_marca->produto_base->name.' '.$similar_marca->produto_base->embalagem_conteudo]); ?>
                                                </div>
                                                <div class="detalhes-produto">
                                                    <div class="informacoes-detalhes text-left">
                                                        <p>Código: <?= $similar_marca->id ?></p>
                                                        <p><?= strtolower($similar_marca->produto_base->marca->name); ?></p>
                                                        <p><?= $similar_marca->tamanho.' '.$similar_marca->unidade_medida; ?></p>
                                                        <p>para <?= $similar_marca->doses; ?> treinos</p>
                                                        <p><?= strtolower($similar_marca->propriedade); ?></p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="produto-inferior">
                                            <?php if($similar_marca->preco_promo) { ?>
                                            <?php $porcentagem_nova = $similar_marca->preco * 0.089;
                                            $preco_corte = $similar_marca->preco + $porcentagem_nova; 
                                            } ?> 
                                            <?php $similar_marca->preco_promo ?
                                            number_format($preco = $similar_marca->preco_promo, 2, ',','.') :
                                            number_format($preco = $similar_marca->preco, 2, ',','.'); ?>
                                            <?php $similar_marca->preco_promo ? $porcentagem_nova = $similar_marca->preco_promo * 0.089 : $porcentagem_nova = $similar_marca->preco * 0.089 ?>
                                            <?php if($similar_marca->preco_promo != null) {
                                                if($similar_marca->preco_promo < 100.00) { ?>
                                                <?php $parcel = $similar_marca->preco_promo * 0.089; $parcel = ($similar_marca->preco_promo + $parcel)/2?>
                                                <?php $maximo_parcelas = "2x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php } else if($similar_marca->preco_promo > 100.00 && $similar_marca->preco_promo < 200.00) { ?>
                                                <?php $parcel = $similar_marca->preco_promo * 0.089; $parcel = ($similar_marca->preco_promo + $parcel)/3?>
                                                <?php $maximo_parcelas = "3x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php } else if($similar_marca->preco_promo > 200.00 && $similar_marca->preco_promo < 300.00) { ?>
                                                <?php $parcel = $similar_marca->preco_promo * 0.089; $parcel = ($similar_marca->preco_promo + $parcel)/5?>
                                                <?php $maximo_parcelas = "5x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php } else if($similar_marca->preco_promo > 300.00) { ?>
                                                <?php $parcel = $similar_marca->preco_promo * 0.089; $parcel = ($similar_marca->preco_promo + $parcel)/6?>
                                                <?php $maximo_parcelas = "6x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php }
                                                } else {
                                            if($similar_marca->preco < 100.00) { ?>
                                                <?php $parcel = $similar_marca->preco * 0.089; $parcel = ($similar_marca->preco + $parcel)/2?>
                                                 <?php $maximo_parcelas = "2x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php } else if($similar_marca->preco > 100.00 && $similar_marca->preco < 200.00) { ?>
                                                <?php $parcel = $similar_marca->preco * 0.089; $parcel = ($similar_marca->preco + $parcel)/3?>
                                                <?php $maximo_parcelas = "3x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php } else if($similar_marca->preco > 200.00 && $similar_marca->preco < 300.00) { ?>
                                                <?php $parcel = $similar_marca->preco * 0.089; $parcel = ($similar_marca->preco + $parcel)/5?>
                                                <?php $maximo_parcelas = "5x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php } else if($similar_marca->preco > 300.00) { ?>
                                                <?php $parcel = $similar_marca->preco * 0.089; $parcel = ($similar_marca->preco + $parcel)/6?>
                                                <?php $maximo_parcelas = "6x";
                                                 $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                            <?php }
                                            } ?>
                                            <?php 
                                                if($similar_marca->preco_promo) {
                                                    echo $this->Html->link("
                                                        <h4>".$similar_marca->produto_base->name."</h4>
                                                        <p><span class='preco-corte'> R$".number_format($preco_corte, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                        <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                        $SSlug.'/produto/'.$similar_marca->slug,
                                                        ['escape' => false]);
                                                } else {
                                                    echo $this->Html->link("
                                                        <h4>".$similar_marca->produto_base->name."</h4>
                                                        <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                        <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                        $SSlug.'/produto/'.$similar_marca->slug,
                                                        ['escape' => false]);
                                                }
                                            ?>
                                        </div>
                                        <div class="hover-produto <?= $similar_marca->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                                            <div class="col-xs-12">
                                                <div class="hexagono-indicar">
                                                    <?php if($similar_marca->status_id == 1) { ?>
                                                        <div class="hexagon-disponivel btn-indique" data-id="<?= $similar_marca->id ?>">
                                                            <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                                            <p>indique</p>
                                                        </div>
                                                        <script>
                                                            $('.btn-indique').click(function () {
                                                                    var id_produto = $(this).attr('data-id');
                                                                    $('#overlay-indicacao-'+id_produto).height('100%');
                                                                    $("html,body").css({"overflow":"hidden"});
                                                            });
                                                            function closeIndica() {
                                                                $('.overlay-indicacao').height('0%');
                                                                $("html,body").css({"overflow":"auto"});
                                                            }
                                                        </script>
                                                    <?php } else { ?>
                                                        <div class="hexagon produto-acabou-btn" data-id="<?= $similar_marca->id ?>">
                                                            <span><i class="fa fa-envelope-o"></i></span>
                                                            <p>avise-me</p>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <?php if($similar_marca->status_id == 1) { ?>
                                                <div class="col-xs-6">
                                                    <?= $this->Html->link('
                                                        <div class="hexagon-disponivel">
                                                            <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                            <p>detalhes</p>
                                                        </div>'
                                                        ,$SSlug.'/produto/'.$similar_marca->slug,
                                                        ['escape' => false]);
                                                    ?>
                                                </div>
                                                <div class="col-xs-6 text-center compare">
                                                    <div class="hexagon-disponivel btn-comparar-action" data-id="<?= $similar_marca->id ?>">
                                                        <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                                        <p>comparar</p>
                                                    </div>
                                                </div>
                                            <?php }
                                            else { ?>
                                                <div class="col-xs-6">
                                                    <?= $this->Html->link('
                                                        <div class="hexagon">
                                                            <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                            <p>detalhes</p>
                                                        </div>'
                                                        ,$SSlug.'/produto/'.$similar_marca->slug,
                                                        ['escape' => false]);
                                                    ?>
                                                </div>
                                                <div class="col-xs-6 text-center compare">
                                                    <div class="hexagon btn-comparar-action" data-id="<?= $similar_marca->id ?>">
                                                        <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                                        <p>comparar</p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $('.send-indicacao-home-btn').click(function(e) {
                                            e.preventDefault();
                                            var botao = $(this);
                                        
                                            botao.html('Enviando...');
                                            botao.attr('disabled', 'disabled');

                                            var id = $(this).attr('data-id');
                                            var slug = '<?= $SSlug ?>';

                                            var valid = $('.indicar_prod_home_'+id).validationEngine("validate");
                                            if (valid == true) {
                                                $.ajax({
                                                    type: "POST",
                                                    url: WEBROOT_URL + slug + '/produtos/overlay-indicar-produtos-home/',
                                                    data: {
                                                        name: $('.name-'+id).val(),
                                                        email: $('.email-'+id).val(), 
                                                        produto_id: $('.id_produto-'+id).val()
                                                    }
                                                })
                                                .done(function (data) {
                                                    if(data == 1) {
                                                        botao.html('Enviado!');
                                                        setTimeout(function () {
                                                            botao.html('Enviar');
                                                            botao.removeAttr('disabled');
                                                        }, 2200);
                                                    } else {
                                                        botao.html('Falha ao enviar... Tente novamente...');
                                                        setTimeout(function () {
                                                            botao.html('Enviar');
                                                            botao.removeAttr('disabled');
                                                        }, 2200);
                                                    }
                                                });
                                            }else{
                                                $('.indicar_prod_home_'+id).validationEngine({
                                                    updatePromptsPosition: true,
                                                    promptPosition: 'inline',
                                                    scroll: false
                                                });
                                            }
                                        });
                                    </script>
                                    <div class="botoes-comprar-similiar">
                                        <?php if($similar_marca->visivel == 1 && $similar_marca->status_id == 1) { ?>
                                            <?php if($Academia->prazo_medio > 0 && $Academia->prazo_medio <= 72) { ?>
                                                <?php 
                                                    if($Academia->prazo_medio <= 12) {
                                                        $prazo_medio = 12;
                                                    } else if($Academia->prazo_medio > 12 && $Academia->prazo_medio <= 24) {
                                                        $prazo_medio = 24;
                                                    } else if($Academia->prazo_medio > 24 && $Academia->prazo_medio <= 48) {
                                                        $prazo_medio = 48;
                                                    } else if($Academia->prazo_medio > 48) {
                                                        $prazo_medio = 72;
                                                    }
                                                ?>
                                                <p class="texto-prazo"><i class="fa fa-truck" aria-hidden="true"></i> Entrega em até <?= $prazo_medio ?>h</p>
                                            <?php } ?>
                                            <button class="btn btn-incluir incluir-mochila" val="<?= $similar_marca->slug ?>" data-id="<?= $similar_marca->id ?>">Comprar</button>
                                            <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $similar_marca->id ?>">Indique</button>
                                            <button class="desktop-hidden btn btn-incluir produto-comparar-btn btn-comparar-action" data-id="<?= $similar_marca->id ?>">Comparar</button> 
                                        <?php } else if($similar_marca->visivel == 1 && $similar_marca->status_id == 2) { ?>
                                            <button class="btn btn-similares">Similares</button>            
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; ?>
                        <?php } ?>
                        <div class="item">
                            <div class="box-completa-produto" id="produto-<?= $i ?>">
                                <div class="col-md-12 col-xs-12 text-center bg-white-produto prdt">
                                    <div class="text-center" style="margin-top: 130px;">
                                        <?= $this->Html->link('<span class="fa fa-plus fa-2x"></span>',
                                            $SSlug.'/pesquisa/0/'.$similares_marca_slug.'/', ['escape' => false, 'style' => 'font-size: 20px; position: relative; width: 100%']
                                        ); ?>
                                        <?= $this->Html->link('ver mais',
                                            $SSlug.'/pesquisa/0/'.$similares_marca_slug.'/', ['escape' => false, 'style' => 'font-size: 25px; position: relative; width: 100%']
                                        ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php foreach($similares_marca as $similar_marca) { ?>
                        <div id="aviseMe-<?= $similar_marca->id ?>" class="overlay overlay-avise">
                            <div class="overlay-avise-content overlay-avise-<?= $similar_marca->id ?> text-center">
                                <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                                <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                <br />
                                <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$similar_marca->id])?>
                                <?= $this->Form->input('produto_id', [
                                    'div'           => false,
                                    'label'         => false,
                                    'type'          => 'hidden',
                                    'val'           => $similar_marca->id,
                                    'id'            => 'produto_id_acabou-'.$similar_marca->id
                                ])?>
                                <?= $this->Form->input('name', [
                                    'div'           => false,
                                    'label'         => false,
                                    'id'            => 'name_acabou-'.$similar_marca->id,
                                    'class'         => 'form-control validate[optional]',
                                    'placeholder'   => 'Qual o seu nome?',
                                ])?>
                                <br/>
                                <?= $this->Form->input('email', [
                                    'div'           => false,
                                    'label'         => false,
                                    'id'            => 'email_acabou-'.$similar_marca->id,
                                    'class'         => 'form-control validate[required, custom[email]]',
                                    'placeholder'   => 'Qual o seu email?',
                                ])?>
                                <br/>
                                <?= $this->Form->button('Enviar', [
                                    'data-id'       => $similar_marca->id,
                                    'type'          => 'button',
                                    'class'         => 'btn btn-success send-acabou-btn'
                                ])?>
                                <br />
                                <br />
                                <?= $this->Form->end()?>
                            </div>
                        </div> 
                        <div id="overlay-indicacao-<?= $similar_marca->id ?>" class="overlay overlay-indicacao">
                            <div class="overlay-content text-center">
                                <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                                <h4>Lembrou de alguém quando viu isso?</h4>
                                <h4>Indique este produto!</h4>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 share-whastapp share-buttons">
                                        <a href="whatsapp://send?text=Isso é a sua cara! <?= WEBROOT_URL.$SSlug.'/produto/'.$similar_marca->slug ?>">
                                            <button class="btn button-blue"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</button>
                                        </a>
                                    </div>
                                    <div class="col-xs-12 share-whastapp share-buttons">
                                        <button class="btn button-blue" id="share-<?= $similar_marca->id ?>"><i class="fa fa-comment-o" aria-hidden="true"></i> Messenger</button>
                                        <script>
                                            $(document).ready(function() {
                                                $('#share-<?= $similar_marca->id ?>').click(function(){
                                                    var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$similar_marca->slug ?>";
                                                    var app_id = '1321574044525013';
                                                    window.open('fb-messenger://share?link=' + link + '&app_id=' + app_id);
                                                });
                                            });
                                        </script>
                                    </div>
                                    <div class="tablet-hidden col-xs-12 col-md-3 share-buttons">
                                        <button data-id="whatsweb-<?= $similar_marca->id ?>" class="btn button-blue abrir-whats"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp Web</button>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                        <a href="javascript: void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u=<?= WEBROOT_URL.$SSlug.'/produto/'.$similar_marca->slug ?>','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');">
                                            <button class="btn button-blue fb-share-button"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</button>
                                        </a>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                        <button class="btn button-blue" id="copiar-clipboard-<?= $similar_marca->id ?>"><i class="fa fa-clone" aria-hidden="true"></i> Copiar link</button>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                        <button  class="btn button-blue abrir-email"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="tablet-hidden enviar-whats">
                                        <div class="col-xs-4 text-center form-group">
                                            <?= $this->Form->input('whats-web-form', [
                                                'div'           => false,
                                                'id'            => 'nro-whatsweb-'.$similar_marca->id,
                                                'label'         => false,
                                                'class'         => 'form-control form-indicar',
                                                'placeholder'   => "Celular*: (00)00000-0000"
                                            ])?>
                                        </div>
                                        <div class="col-xs-4 text-left obs-overlay">
                                            <p>*Para enviar pra mais de um contato deixe este campo vazio.</p>
                                        </div>
                                        <div class="col-xs-4 text-center form-group">
                                            <?= $this->Form->button('Enviar', [
                                            'type'          => 'button',
                                            'data-id'       => $similar_marca->id,
                                            'class'         => 'btn button-blue',
                                            'id'            => 'enviar-whatsweb-'.$similar_marca->id
                                            ])?>
                                        </div>
                                        <script>
                                            $(document).ready(function() {
                                                $('#enviar-whatsweb-<?=$similar_marca->id ?>').click(function(){

                                                    var numero;
                                                    var link = "<?= WEBROOT_URL.$SSlug.'/produto/'.$similar_marca->slug ?>";

                                                    if($('#nro-whatsweb-<?= $similar_marca->id ?>').val() != '') {
                                                        numero = '55'+$('#nro-whatsweb-<?= $similar_marca->id ?>').val();
                                                    }
                                                    
                                                    window.open('https://api.whatsapp.com/send?phone=' + numero + '&text=Isso é a sua cara! ' + link);
                                                });
                                            });
                                        </script>
                                    </div>
                                    <div class="col-xs-12 panel-collapse collapse" id="collapse-email-<?=$similar_marca->id ?>">
                                        <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$similar_marca->id])?>
                                        <?= $this->Form->input('name', [
                                            'div'           => false,
                                            'label'         => false,
                                            'class'         => 'form-control validate[optional] form-indicar name-'.$similar_marca->id,
                                            'placeholder'   => 'Quem indicou?',
                                        ])?>
                                        <br>
                                        <?= $this->Form->input('email', [
                                            'div'           => false,
                                            'label'         => false,
                                            'class'         => 'form-control validate[required] form-indicar email-'.$similar_marca->id,
                                            'placeholder'   => 'E-mail do destinatário',
                                        ])?>
                                        <?= $this->Form->input('id_produto', [
                                            'div'           => false,
                                            'class'         => 'id_produto-'.$similar_marca->id,
                                            'value'         => $similar_marca->id,
                                            'label'         => false,
                                            'type'          => 'hidden'
                                        ])?>
                                        <br/>
                                        <?= $this->Form->button('Enviar', [
                                            'id'            => 'send-indicacao-btn',
                                            'type'          => 'button',
                                            'data-id'       => $similar_marca->id,
                                            'class'         => 'btn button-blue send-indicacao-home-btn'
                                        ])?>
                                        <br />
                                        <?= $this->Form->end()?>
                                    </div>
                                </div>
                                <div class="row"> 
                                    <div class="col-xs-12 clipboard">   
                                        <?= $this->Form->input('url', [
                                            'div'           => false,
                                            'id'            => 'url-clipboard-'.$similar_marca->id,
                                            'label'         => false,
                                            'value'         => WEBROOT_URL.$SSlug.'/produto/'.$similar_marca->slug,
                                            'readonly'      => true,
                                            'class'         => 'form-control form-indicar'
                                        ])?>
                                    </div>  
                                    <p><span class="copy-alert hide" id="copy-alert-<?=$similar_marca->id ?>">Copiado!</span></p>
                                    <script>
                                        $(document).ready(function() {
                                            // Copy to clipboard 
                                            document.querySelector("#copiar-clipboard-<?=$similar_marca->id ?>").onclick = () => {
                                              // Select the content
                                                document.querySelector("#url-clipboard-<?=$similar_marca->id ?>").select();
                                                // Copy to the clipboard
                                                document.execCommand('copy');
                                                // Exibe span de sucesso
                                                $('#copy-alert-<?=$similar_marca->id ?>').removeClass("hide");
                                                // Oculta span de sucesso
                                                setTimeout(function() {
                                                    $('#copy-alert-<?=$similar_marca->id ?>').addClass("hide");
                                                }, 1500);
                                            };
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script>
    $('.abrir-email').click(function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('.enviar-whats').slideUp("slow");
        $('.enviar-email').slideToggle("slow");
    });
    $('.abrir-whats').click(function(e){
        var id = $(this).attr('data-id');
        $('.enviar-email').slideUp("slow");
        $('.enviar-whats').slideToggle("slow");
    });
</script>