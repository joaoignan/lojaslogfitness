<script>
    var WEBROOT_URL = "<?= WEBROOT_URL ?>";
    var SSLUG = "<?= $SSlug ?>";
    var ACADEMIA_SLUG = "<?= $academia->slug ?>";
</script>

<style>
	.overlay{
		display: block;
		background-color: rgba(0,0,0,1);
	}
</style>

<div class="container-fluid botao-exportar">
	<div class="row">
		<div class="total-logo-academia">
			<?php 
				if($academia->image != null){
					echo $this->Html->link( $this->Html->image('academias/'.$academia->image, ['alt' => $academia->shortname, 'class' => 'logo-revista']) , WEBROOT_URL.$academia->slug, ['escape' => false, 'target' => '_blank']) ;
            	} else{
            		echo $this->Html->link('<h4>'.$academia->shortname.'</h4>', WEBROOT_URL.$academia->slug, ['escape' => false, 'target' => '_blank']);
            	}
        	?>
		</div>
		<div class="total-btn-voltar-loja">
			<?= $this->Html->link('<button class="btn-voltar-loja"><i class="fa fa-arrow-left" aria-hidden="true"></i> Visitar loja</button>', WEBROOT_URL.$academia->slug, ['escape' => false, 'target' => '_blank']) ?>
		</div>
		<button class="btn-imprimir-catalogo pull-right"><i class="fa fa-print" aria-hidden="true"></i> Imprimir Catálogo</button>
	</div>
</div>

<div class="overlay loading">
    <div class="loading-box text-center" style="text-align: center">
        <?= $this->Html->image('new-loading.gif', ['class' => 'image-loading']); ?>
        <span>Catalogando...</span>
    </div>
</div>

<?php if (!$this->request->is('mobile')) { ?>
	<?= $this->Html->script('booklet/jquery.easing.1.3.js') ?>
	<?= $this->Html->script('booklet/jquery.booklet.1.1.0.min.js') ?>

	<?= $this->Html->css('booklet/jquery.booklet.1.1.0.css') ?>
	<?= $this->Html->css('booklet/style') ?>
	<?= $this->Html->css('catalogo') ?>

	<?= $this->Html->script('cufon/cufon-yui.js') ?>
	<?= $this->Html->script('cufon/ChunkFive_400.font.js') ?>
	<?= $this->Html->script('cufon/Note_this_400.font.js') ?>

	<div class="book_wrapper">
		<a id="next_page_button"></a>
		<a id="prev_page_button"></a>
		<div id="mybook" style="display:none;">
			<div class="b-load">
				<div>
					<?= $this->Html->image('catalogo2.png') ?>
				</div>
				<?php $count = 1 ?>
				<?php $quebra_pagina = 6 ?>
				<?php foreach ($produtos_desk as $produto) { ?>
					<?php $produto->preco_promo >= 1 ? $preco = $produto->preco_promo : $preco = $produto->preco ?>
					<?php $foto = unserialize($produto->fotos)[0]; ?>
					<?php 
						if($preco <= 100.0) {
							$em_ate = '2x';
							$preco_parcelado = $preco/2;
						} else if($preco > 100.0 && $preco <= 200.0) {
							$em_ate = '3x';
							$preco_parcelado = $preco/3;
						} else if($preco > 200.0 && $preco <= 300.0) {
							$em_ate = '5x';
							$preco_parcelado = $preco/5;
						} else if($preco > 300.0) {
							$em_ate = '6x';
							$preco_parcelado = $preco/6;
						}
					?>
					<?php if ($quebra_pagina % 6 == 0) { ?>
					<div>
						<div class="logo-acad-paginas">
							<?php 
								if($academia->image != null){
				                	echo $this->Html->image('academias/'.$academia->image, ['alt' => $academia->shortname, 'class' => 'logo-revista']);
				            	} else{
				               		echo '<h4>'.$academia->shortname.'</h4>';
				            	}
				        	?>
						</div>
						<div class="link-acad-paginas">
							<span>logfitness.com.br/<?= $academia->slug ?></span>
						</div>
						<div class="separacao-completa"></div>
					<?php } ?>
						<div class="box-produto-catalogo" id="prod-<?= $produto->id ?>" data="<?= $produto->slug ?>" data-id="<?= $produto->id ?>">
							<div class="text-left codigo-imagem">
								<span class="codigo-catalogo text-left">COD <?= $produto->id ?></span>
								<?= $this->Html->image('produtos/'.$foto, ['alt' => $produto->produto_base->name]) ?>
							</div>
							<div class="text-right dados-box-produto-catalogo">
								<span class="objetivo-catalogo" style="background-color: <?= $produto->produto_base->produto_objetivos[0]->objetivo->color ?>"><?= $produto->produto_base->produto_objetivos[0]->objetivo->texto_tarja ?></span>
								<p class="marca-catalogo"><?= $produto->produto_base->marca->name ?></p>
								<p class="nome-catalogo"><?= $produto->produto_base->name ?></p>
								<p class="duracao-catalogo"><?= $produto->tamanho ?><?= $produto->unidade_medida ?>/<strong><?= $produto->doses ?></strong> treinos</p>
								<p class="sabores"><strong><?= $produto->propriedade ?></strong></p>
								<div class="preco text-right">
									<p class="valor-produto-catalogo">R$<?= number_format($preco, 2, ',', '.') ?></p>
									<p class="complementar-valor-catalogo">em <strong><?= $em_ate ?></strong> R$<?= number_format($preco_parcelado, 2, ',', '.') ?></p>
								</div>
							</div>
						</div>
						<?php if ($count % 2 != 0) { ?>
						<div class="separador-vertical"></div>
						<?php } ?>
						<?php 
							$count++;
							$quebra_pagina++;
						 ?>
				 	<?php if ($quebra_pagina % 6 == 0) { ?>
					</div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</div>

	<!-- The JavaScript -->

	<script type="text/javascript">
		$(function() {
			var $mybook 		= $('#mybook');
			var $bttn_next		= $('#next_page_button');
			var $bttn_prev		= $('#prev_page_button');
			var $loading		= $('#loading');
			var $mybook_images	= $mybook.find('img');
			var cnt_images		= $mybook_images.length;
			var loaded			= 0;
			//preload all the images in the book,
			//and then call the booklet plugin

			$mybook_images.each(function(){
				var $img 	= $(this);
				var source	= $img.attr('src');
				$('<img/>').load(function(){
					++loaded;
					if(loaded == cnt_images){
						$('.loading').hide();
						$bttn_next.show();
						$bttn_prev.show();
						$mybook.show().booklet({
							name:               null,                            // name of the booklet to display in the document title bar
							width:              800,                             // container width
							height:             500,                             // container height
							speed:              600,                             // speed of the transition between pages
							direction:          'LTR',                           // direction of the overall content organization, default LTR, left to right, can be RTL for languages which read right to left
							startingPage:       0,                               // index of the first page to be displayed
							easing:             'easeInOutQuad',                 // easing method for complete transition
							easeIn:             'easeInQuad',                    // easing method for first half of transition
							easeOut:            'easeOutQuad',                   // easing method for second half of transition

							closed:             false,                           // start with the book "closed", will add empty pages to beginning and end of book
							closedFrontTitle:   null,                            // used with "closed", "menu" and "pageSelector", determines title of blank starting page
							closedFrontChapter: null,                            // used with "closed", "menu" and "chapterSelector", determines chapter name of blank starting page
							closedBackTitle:    null,                            // used with "closed", "menu" and "pageSelector", determines chapter name of blank ending page
							closedBackChapter:  null,                            // used with "closed", "menu" and "chapterSelector", determines chapter name of blank ending page
							covers:             false,                           // used with  "closed", makes first and last pages into covers, without page numbers (if enabled)

							pagePadding:        10,                              // padding for each page wrapper
							pageNumbers:        false,                            // display page numbers on each page

							hovers:             false,                            // enables preview pageturn hover animation, shows a small preview of previous or next page on hover
							overlays:           false,                            // enables navigation using a page sized overlay, when enabled links inside the content will not be clickable
							tabs:               false,                           // adds tabs along the top of the pages
							tabWidth:           60,                              // set the width of the tabs
							tabHeight:          20,                              // set the height of the tabs
							arrows:             false,                           // adds arrows overlayed over the book edges
							cursor:             'pointer',                       // cursor css setting for side bar areas

							hash:               false,                           // enables navigation using a hash string, ex: #/page/1 for page 1, will affect all booklets with 'hash' enabled
							keyboard:           true,                            // enables navigation with arrow keys (left: previous, right: next)
							next:               $bttn_next,          			// selector for element to use as click trigger for next page
							prev:               $bttn_prev,          			// selector for element to use as click trigger for previous page

							menu:               null,                            // selector for element to use as the menu area, required for 'pageSelector'
							pageSelector:       false,                           // enables navigation with a dropdown menu of pages, requires 'menu'
							chapterSelector:    false,                           // enables navigation with a dropdown menu of chapters, determined by the "rel" attribute, requires 'menu'

							shadows:            true,                            // display shadows on page animations
							shadowTopFwdWidth:  166,                             // shadow width for top forward anim
							shadowTopBackWidth: 166,                             // shadow width for top back anim
							shadowBtmWidth:     50,                              // shadow width for bottom shadow

							before:             function(){},                    // callback invoked before each page turn animation
							after:              function(){}                     // callback invoked after each page turn animation
						});
						Cufon.refresh();
					}
				}).attr('src',source);
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#mybook").bind("bookletcreate", function(event, data) {
				console.log('feio');
			});
			var slug = '<?= $academia->slug ?>';
			$(document).on('click', '.box-produto-catalogo', function(){
				window.open('https://logfitness.com.br/' + slug + '/produto/' + $(this).attr('data'));
			});
			$(document).on('click', '.logo-acad-paginas, .link-acad-paginas', function(){
				location.href = 'https://logfitness.com.br/' + slug;
			});
			$('.btn-imprimir-catalogo').click(function() {
				$.get(WEBROOT_URL + 'contar-click/catalogo/',
		            function (data) {
	                window.print();
	            });
            });
		});
	</script>
<?php } ?>

<?php if ($this->request->is('mobile')) { ?>

	<?= $this->Html->css('bootstrap.min') ?>

	<div class="overlay loading">
	    <div class="loading-box text-center">
	        <?= $this->Html->image('new-loading.gif', ['class' => 'image-loading']); ?>
	        <span>Catalogando...</span>
	    </div>
	</div>

	<div class="catalogo-mobile">
		<?php foreach ($produtos as $produto) { ?>
			<?php $produto->preco_promo >= 1 ? $preco = $produto->preco_promo : $preco = $produto->preco ?>
			<?php $foto = unserialize($produto->fotos)[0]; ?>
			<?php 
				if($preco <= 100.0) {
					$em_ate = '2x';
					$preco_parcelado = $preco/2;
				} else if($preco > 100.0 && $preco <= 200.0) {
					$em_ate = '3x';
					$preco_parcelado = $preco/3;
				} else if($preco > 200.0 && $preco <= 300.0) {
					$em_ate = '5x';
					$preco_parcelado = $preco/5;
				} else if($preco > 300.0) {
					$em_ate = '6x';
					$preco_parcelado = $preco/6;
				}
			?>
			<div class="total-produto-catalogo-mobile" id="prod-<?= $produto->id ?>" data="<?= $produto->slug ?>" data-id="<?= $produto->id ?>">
				<div class="foto-codigo text-center">
					<p class="codigo-mobile text-left">COD <?= $produto->id ?></p>
					<?= $this->Html->image('produtos/'.$foto, ['alt' => $produto->produto_base->name]) ?>
				</div>
				<div class="infos-preco text-right">
					<span class="objetivo-catalogo" style="background-color: <?= $produto->produto_base->produto_objetivos[0]->objetivo->color ?>"><?= $produto->produto_base->produto_objetivos[0]->objetivo->texto_tarja ?></span>
					<p class="marca-catalogo"><?= $produto->produto_base->marca->name ?></p>
					<p class="nome-catalogo"><?= $produto->produto_base->name ?></p>
					<p class="duracao-catalogo"><?= $produto->tamanho ?><?= $produto->unidade_medida ?>/<strong><?= $produto->doses ?></strong> treinos</p>
					<p class="sabores"><strong><?= $produto->propriedade ?></strong></p>
					<div class="preco text-right">
						<p class="valor-produto-catalogo">R$<?= number_format($preco, 2, ',', '.') ?></p>
						<p class="complementar-valor-catalogo">em <strong><?= $em_ate ?></strong> de R$<?= number_format($preco_parcelado, 2, ',', '.') ?></p>
					</div>
				</div>
			</div>
			<hr>
		<?php } ?>
	</div>

	<script>
		$(document).ready(function() {
			var slug = '<?= $academia->slug ?>';
			$(document).on('click', '.total-produto-catalogo-mobile', function(){
				window.open('https://logfitness.com.br/' + slug + '/produto/' + $(this).attr('data'));
			});
			$('.loading').hide();

	        var atualizando = 0;

			$(window).scroll(function() {
		        var posicao_pagina = $(document).scrollTop();

		        var id_produtos = [];

		        //CARREGAR MAIS PRODUTOS

		        if(posicao_pagina >= ($(document).height() - 1000) && atualizando == 0) {
		            atualizando = 1;

		            $('.total-produto-catalogo-mobile').each(function() {
		              id_produtos.push($( this ).attr('data-id'));
		            });

		            $.ajax({
		                type: "POST",
		                url: WEBROOT_URL + SSLUG + '/catalogo/atualizar-produtos/',
		                data: {
		                    produtos_ids: id_produtos
		                }
		            })
		                .done(function (data) {
		                    $('.catalogo-mobile').append(data);
		                    setTimeout(function() {
		                        atualizando = 0;
		                    }, 1200);
		                });
		        }
		    });
		});
	</script>
	<?= $this->Html->script('bootstrap.min') ?>
<?php } ?>