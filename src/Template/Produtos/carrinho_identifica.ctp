<?= $this->Html->script('jquery.mask.min.js')?>

<script type="text/javascript">
    $("#card-number").mask("0000 0000 0000 0000");
    $(".cpf-mask").mask("000.000.000-00");
    $(".cvv-mask").mask("0000");
    $(".card-number-mask").mask("0000 0000 0000 0000 000");
    $(".data-mask").mask('00/00/00', {reverse: false});

    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

    if($('.phone-mask').length > 0){
        $('.phone-mask').mask(SPMaskBehavior, spOptions);
    }
</script>

<link rel="stylesheet prefetch" href="https://fonts.googleapis.com/css?family=Inconsolata">


<style>
    input[type=number]::-webkit-outer-spin-button,
    input[type=number]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    input[type=number] {
        -moz-appearance:textfield;
    }
    .dados-entrega{
        margin-top: 10px;
    }
    .mochila-fechada {
        padding-left: 0;
        margin-left: 0;
        margin-right: 0;
        width: 100%;
    }
    #logexpress-btn {
        display: none!important;
    }
    .completar-cadastro{
        padding-left: 0;
        margin-left: 240px;
        margin-right: 240px;
    }
    .fundo-carrinho {
        background: #ffffff;
        padding: 0 5px;
        height: 70px;
        margin-left: 0px;
    }
    .btn_indicar{
        border: 2px solid;
        border-color: #5087c7;
        background-color: white;
        font-family: nexa_boldregular;
        color: #5087c7;
        padding: 3px 20px;
    }
    .btn_conclui_cadastro{
        width: 230px;
        border: none;
        background-color: #8DC73F;
        color: #FFF;
        height: 30px;
    }
    .indicar{
        margin-top: 28px;
    }
    #instagram-rodape {
        box-shadow: none;
    }
    .close-mochila-icon {
        display: none;
    }
    .titulo-produtos-informacoes{
        display: none;
    }
    .separador-produto {
        display: none;      
    }
    .btn-professor {
        border: 2px solid;
        border-color: #5087c7;
        background-color: white;
        font-family: nexa_boldregular;
        color: #5087c7;
    }
    .forma-pagamento {
        padding: 0!important;
    }
    @media all and (max-width: 1100px) {
        .forma-pagamento button {
            font-size: 1.1em;
            white-space: normal;
        }
        .completar-cadastro{
            margin-left: 70px;
            margin-right: 70px;
        }
    }
    @media all and (max-width: 768px) {
        .mochila-pagamento {
            padding-left: 0;
        }
        .titulo-pagamento {
            padding-right: 0;
        }
        .lista-produtos{
            overflow: auto;
            min-height: 130px;
            max-height: 225px;
            width: 100%;
            border: solid 1px;
            border-color: #5089cf;
            box-shadow: inset 0px 0px 6px 0px rgba(0,0,0,0.75);
        }
        .completar-cadastro{
            margin-top: 70px;
            margin-left: 30px;
            margin-right: 30px;
        }
    }
    @media all and (max-width: 550px) {
        .forma-pagamento button {
            width: 100%;
        }
        .indicar{
        margin-top: 8px;
    }

    }
    @media all and (max-width: 430px) {
        .pedidos-topo{
            display: none;
        }
        .fabricante-mobile{
            display: none;
        }
        .codigo-mobile{
            display: none;
        }
        .titulo-produtos-informacoes{
            display: block;
        }
        .separador-produto {
            display: block;
            width: 100%;
            border: 1px solid #f3d012;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        
    }
    @media all and (max-width: 400px) {
        .forma-pagamento button {
            font-size: .9em;
        }
        .imagem-produto-carrinho{
            padding-top: 10px;
        }
    }
</style>

<style>
    .box-close-mochila{
            display: none;
        }
    .mochila{
        display: none;
    }
    .mochila-pagamento h4{
        text-align: left;
        margin: 0 0 10px;
        color: white;
        font-family: nexa_boldregular;
        display: flex;
        align-items: center;
        justify-content: center;
        height: 45px;
    }
    .titulo-pagamento{
        background-color:#5089cf ;
    }
    .pedidos-topo {
        background-color: #5089cf;
        color: white;
        text-transform: uppercase;
        font-weight: 700;
    }
    .dados-total{
        float: right;
    }
    .lista-produtos{
        overflow: auto;
        min-height: 130px;
        max-height: 225px;
        width: 100%;
        border: solid 1px;
        border-color: #5089cf;
    }
    .close-mochila-icon {
        cursor: auto;
    }
    .overlay-pagamento {
        overflow: auto;
        display: none;
        position: fixed;
        top: 0!important;
        left: 0;
        width: 100%;
        height: 100%;
        padding: 50px 0;
        background: rgba(0, 0, 0, .9);
        color: #333;
        font-size: 19px;
        line-height: 30px;
        z-index: 9999;
    }

    .checkout .form {
        height: 250px;
        margin-top: 20px;
        overflow-x: hidden;
        overflow-y: scroll;
    }

    .checkout .form fieldset {
        border: none;
        padding: 0;
        padding: 10px 0;
        position: relative;
        clear: both;
    }

    .checkout .fieldset-expiration {
        float: left;
        width: 80%;
        margin-left: 10%;
    }

    .checkout .form fieldset .select {
        width: 84px;
        margin-right: 12px;
        float: left;
    }

    .checkout .form .bandeira-cartao {
        width: 80%;
        margin-left: 10%;
        float: left;
    }
    .checkout .form .numero-cartao {
        width: 80%;
        margin-left: 10%;
    }

    .checkout .form .nome-cartao {
        width: 80%;
        margin-left: 10%;
    }

    .checkout .form .fieldset-expiration {
        float: left;
        clear: none;
        width: 55%;
        margin-left: 10%;
    }

    .checkout .form .fieldset-expiration .select {
        width: 43%;
    }

    .checkout .form .fieldset-cvv {
        float: left;
        clear: none;
        width: 25%;
        margin-left: -8px;
    }

    .checkout .form .niver-cartao {
        width: 40%;
        margin-left: 10%;
        float: left;
        clear: none;
    }

    .checkout .form .fieldset-telefone {
        width: 40%;
        float: left;
        clear: none;
    }

    .checkout .form .fieldset-cpf {
        width: 80%;
        margin-left: 10%;
    }

    .checkout .form .fieldset-parcelas {
        width: 80%;
        margin-left: 10%;
    }

    .checkout .form label {
        display: block;
        text-transform: uppercase;
        font-size: 11px;
        color: hsla(0, 0%, 0%, .6);
        margin-bottom: 5px;
        font-weight: bold;
        font-family: Inconsolata;
    }

    .checkout .form input,
    .checkout .form .select {
        width: 96%;
        height: 38px;
        color: hsl(0, 0%, 20%);
        padding: 10px;
        border-radius: 5px;
        font-size: 15px;
        outline: none!important;
        border: 1px solid hsla(0, 0%, 0%, 0.3);
        box-shadow: inset 0 1px 4px hsla(0, 0%, 0%, 0.2);
    } 

    .checkout .form input .input-cart-number,
    .checkout .form .select .input-cart-number {
        width: 82px;
        display: inline-block;
        margin-right: 8px;
    }

    .checkout .form input .input-cart-number:last-child,
    .checkout .form .select .input-cart-number:last-child {
        margin-right: 0;
    }

    .checkout .form .select {
        position: relative;
    }

    .checkout .form .select:after {
        content: '';
        border-top: 8px solid #222;
        border-left: 4px solid transparent;
        border-right: 4px solid transparent;
        position: absolute;
        z-index: 2;
        top: 14px;
        right: 10px;
        pointer-events: none;
    }

    .checkout .form .select select {
        -webkit-appearance: none;
        appearance: none;
        position: absolute;
        padding: 0;
        border: none;
        width: 100%;
        outline: none!important;
        top: 6px;
        left: 6px;
        background: none;
    }

    .checkout {
      margin: 60px auto 0px;
      position: relative;
      width: 500px;
      background: white;
      border-radius: 15px;
      padding: 160px 45px 30px;
      box-shadow: 0 10px 40px hsla(0, 0, 0, .1);
    }

    .credit-card-box {
        perspective: 1000;
        width: 400px;
        height: 280px;
        position: absolute;
        top: -90px;
        left: 50%;
        transform: translateX(-50%);
        font-family: Inconsolata;
    }

    .credit-card-box.hover .flip {
        transform: rotateY(540deg);
    }

    .credit-card-box .front,
    .credit-card-box .back {
        width: 400px;
        height: 250px;
        border-radius: 15px;
        backface-visibility: hidden;
        background: #ccc;
        position: absolute;
        color: #fff;
        font-family: Inconsolata;
        top: 0;
        left: 0;
        text-shadow: 0 1px 1px hsla(0, 0, 0, 0.3);
        box-shadow: 0 1px 6px hsla(0, 0, 0, 0.3);
    }

    .card-color-change {
        position: absolute;
        z-index: -1;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border-radius: 15px;

        opacity: 0;

        background: -webkit-linear-gradient(135deg, #5087c7, #ffd41c);
        background: -moz-linear-gradient(135deg, #5087c7, #ffd41c);
        background: -o-linear-gradient(135deg, #5087c7, #ffd41c);
        background: linear-gradient(135deg, #5087c7, #ffd41c);

        -webkit-transition: opacity .7s linear;
        -moz-transition: opacity .7s linear;
        -o-transition: opacity .7s linear;
        transition: opacity .7s linear;
    }

    .credit-card-box .front::before, 
    .credit-card-box .back::before {
        content: '';
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        background: url('https://www.logfitness.com.br/webroot/images/earth.svg') no-repeat center;
        background-size: cover;
        opacity: .05;
    }

    .credit-card-box .flip {
        transition: 1s;
        transform-style: preserve-3d;
        position: relative;
    }

    .credit-card-box .logo {
        position: absolute;
        top: 9px;
        right: 20px;
        width: 60px;
    }
    
    .credit-card-box .logo svg,
    .credit-card-box .logo img {
        width: 100%;
        height: auto;
        fill: #fff;
        display: none;
    }

    .credit-card-box .front {
        z-index: 2;
        transform: rotateY(0deg);
    }

    .credit-card-box .back {
        transform: rotateY(180deg);
    }
    
    .credit-card-box .back .logo {
        top: 185px;
    }

    .credit-card-box .chip {
        position: absolute;
        width: 60px;
        height: 45px;
        top: 20px;
        left: 20px;
        background: linear-gradient(135deg, hsl(269,54%,87%) 0%,hsl(200,64%,89%) 44%,hsl(18,55%,94%) 100%);;
        border-radius: 8px;
    }

    .credit-card-box .chip::before {
        content: '';
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        border: 4px solid hsla(0, 0%, 50%, .1);
        width: 80%;
        height: 70%;
        border-radius: 5px;
    }
    
    .credit-card-box .strip {
        background: linear-gradient(135deg, hsl(0, 0%, 25%), hsl(0, 0%, 10%));
        position: absolute;
        width: 100%;
        height: 50px;
        top: 30px;
        left: 0;
    }

    .credit-card-box .number {
        position: absolute;
        margin: 0 auto;
        top: 103px;
        left: 19px;
        font-size: 38px;
    }

    .credit-card-box label {
        font-size: 10px;
        letter-spacing: 1px;
        text-shadow: none;
        text-transform: uppercase;
        font-weight: normal;
        opacity: 0.5;
        display: block;
        margin-bottom: 3px;
    }

    .credit-card-box .card-holder,
    .credit-card-box .card-expiration-date {
        position: absolute;
        margin: 0 auto;
        top: 180px;
        left: 19px;
        font-size: 22px;
        text-transform: capitalize;
    }

    .credit-card-box .card-expiration-date {
        text-align: right;
        left: auto;
        right: 20px;
    }

    .credit-card-box .cvv {
        height: 36px;
        background: #fff;
        width: 91%;
        border-radius: 5px;
        top: 110px;
        left: 0;
        right: 0;
        position: absolute;
        margin: 0 auto;
        color: #000;
        text-align: right;
        padding: 10px;
    }

    .credit-card-box .cvv label {
        margin-top: -32px;
    }

    .credit-card-box label {
        margin: -25px 0 14px;
        color: #fff;
    }
    .input-cart-number {
        width: 96%!important;
    }
    .bandeira-cartao select {
        width: 96%;
        height: 38px;
        color: hsl(0, 0%, 20%);
        padding: 10px;
        border-radius: 5px;
        font-size: 15px;
        outline: none!important;
        border: 1px solid hsla(0, 0%, 0%, 0.3);
        box-shadow: inset 0 1px 4px hsla(0, 0%, 0%, 0.2);
    }
    #cartao-pagamento {
        display: none;
    }
    .fieldset-parcelas select {
        width: 96%;
        height: 38px;
        color: hsl(0, 0%, 20%);
        border-radius: 5px;
        font-size: 15px;
        outline: none!important;
        border: 1px solid hsla(0, 0%, 0%, 0.3);
        box-shadow: inset 0 1px 4px hsla(0, 0%, 0%, 0.2);
    }
    .fechar-overlay {
        cursor:pointer;
        color: #5087c7;
        position:absolute;
        top: 15px;
        right: 20px;
    }
    .logo-moip {
        position: absolute;
        right: 8px;
    }
    #pagarCredito {
        margin-left: -10px;
        width: 200px; 
        margin-top: 10px; 
        border-color: #8DC73F; 
        background-color: #8DC73F; 
        color: white;
    }
    .cvv-number {
        margin-top: -18px;
    }
    .baixo-produtos {
        padding-left: 230px;
    }
    .erro-style {
        color: #f90000;
        font-size: 11px;
        font-style: italic;
        margin-left: 10px;
    }
    @media all and (max-width: 768px) {
        .baixo-produtos {
            padding-left: 0;
            padding-bottom: 30px;
        }
    }
    @media all and (max-width: 600px) {
        .credit-card-box {
            top: -50px;
            left: 50%;
            width: 300px;
            height: 187.5px;
        }
        .credit-card-box .front,
        .credit-card-box .back {
            width: 300px;
            height: 187.5px;
        }
        .checkout {
            width: 400px;
            padding: 160px 20px 30px 30px;
        }
        .chip {
            width: 40px!important;
            height: 30px!important;
        }
        .credit-card-box .number {
            font-size: 28px;
            top: 90px;
        }
        .credit-card-box .card-holder,
        .credit-card-box .card-expiration-date {
            top: 130px;
            font-size: 18px;
        }
        .checkout .form fieldset .select {
            margin-right: 9px;
        }
        .credit-card-box .strip {
            height: 30px;
        }
        .credit-card-box .cvv {
            top: 90px;
            height: 26px;
        }
        .cvv-number {
            margin-top: -23px;
        }
        .credit-card-box .back .logo {
            top: 145px;
        }
    }
    @media all and (max-width: 470px) {
        .credit-card-box {
            width: 240px;
            height: 150px;
        }
        .credit-card-box .front,
        .credit-card-box .back {
            width: 240px;
            height: 150px;
        }
        .checkout {
            width: 300px;
            padding: 90px 0px 30px 0px;
        }
        .chip {
            width: 40px!important;
            height: 30px!important;
            top: 15px;
        }
        .credit-card-box .number {
            font-size: 21px;
            top: 60px;
        }
        .credit-card-box .card-holder,
        .credit-card-box .card-expiration-date {
            top: 110px;
            font-size: 16px;
            line-height: 16px;
        }
        .credit-card-box .strip {
            height: 20px;
        }
        .credit-card-box .cvv {
            top: 70px;
            height: 22px;
        }
        .cvv-number {
            margin-top: -26px;
        }
        .credit-card-box .back .logo {
            top: 95px;
        }
        .credit-card-box .front .logo {
            top: 4px;
        }
        .fechar-overlay {
            right: 10px;
        }
        .checkout .form .numero-cartao,
        .checkout .form .bandeira-cartao,
        .checkout .form .nome-cartao,
        .checkout .form .fieldset-cpf,
        .checkout .form .fieldset-parcelas {
            width: 90%;
            margin-left: 6%;
        }
        .checkout .form .fieldset-cvv {
            width: 30%;
            margin-left: -6px;
        }
        .checkout .form .fieldset-expiration {
            width: 60%;
            margin-left: 6%;
        }
        .checkout .form .niver-cartao {
            width: 45%;
            margin-left: 6%;
        }
        .checkout .form .fieldset-telefone {
            width: 44%;
        }
    }

    .mochila-fechada{
        margin-top: 90px; 
    }
    .total-breadcrumb{
        display: none!important;
    }
    .mochila-fechada-collapse{
        border: 2px solid;
        border-color: #5089cf;
        cursor: pointer;
        margin-bottom: 10px;
    }
    .mochila-fechada-collapse h3{
        margin-top: 10px;
    }
    .nav-tabs > li, .nav-pills > li {
        float:none;
        display:inline-block;
        *display:inline; /* ie7 fix */
         zoom:1; /* hasLayout ie7 trigger */
    }
    .nav-tabs, .nav-pills {
        text-align:center;
    }
    .nav-tabs{
        border-bottom: 1px solid #5089cf;
    }
    .nav-tabs>li>a{
        border: none;
        background-color: #eee;
        font-size: 12px;
    }
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus{
        border: 1px solid #5089cf;
        border-bottom-color: transparent;
    }
    #boleto_card div, .bandeiras, #money_card div {
        padding-top: 25px;
    }
    .bandeiras ul{
        padding-left: 0;
    }
    .bandeiras ul li{
        display: inline-block;
        padding-right: 0px;
        padding-bottom: 5px;
        min-width: 70px; 
    }
    .bandeiras label{
        padding: 5px;
        cursor: pointer;    
    }
    .bandeiras label:hover{
        background-color: #eee;
    }
    .total-tabs{
        border: 1px solid #5089cf;
        border-top-color: transparent;
    }
    .total-pagamentos{
        padding: 0;
    }
    .bandeiras img{
        -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
        filter: grayscale(100%);
        transition: all 0.5s;
        max-width: 45px;
    }
    .bandeiras input:checked + img{
        -webkit-filter: grayscale(0%); /* Safari 6.0 - 9.0 */
        filter: grayscale(0%);
        
    }
    .anexa-card{
        width: 100px;
        display: inline-block;
    }
    #produtos_mochila_fechada{
        margin-bottom: 15px;
    }
    #collapse-mochila[aria-expanded=true] .fa-chevron-down {
       display: none;
    }
    #collapse-mochila[aria-expanded=false] .fa-chevron-up {
       display: none;
    }
    #boleto_card ul li{
        list-style: disc;
        padding-left: 5px;

    }
    .btn-outline-verde{
        background-color: #ffffff;
        color: #489f36;
        padding: 15px 10px;
        min-width: 110px;
        box-shadow: rgb(72, 159, 54) 0 0px 0px 2px inset;
        transition: all 0.5s;
    }
    .btn-outline-verde:hover{
        box-shadow: rgb(72, 159, 54) 0 0px 0px 40px inset;
        color: rgb(255,255,255);
    }
    .btn-verde{
        color: rgb(255,255,255);
        background-color: rgba(72, 159, 54, 1);
        padding: 15px 10px;
        min-width: 110px;
    }
    .btn-verde:hover{
        color: rgb(255,255,255);
        background-color: rgba(55, 119, 42, 1);
    }
    .div-card{
        padding-left: 0;
    }
    .div-card label{
        width: 100%;
    }
    .show-endereco{
        cursor: pointer;
    }
    .detalhe-endereco{
        display: none;
    }
    @media all and (min-width: 1024px){
        .nav-tabs > li, .nav-pills > li{
            width: 200px;
        }
    }
    @media all and (max-width: 500px){
        .mochila-fechada{
            margin-top: 60px;
        }
    }
</style>

<div class="mochila-fechada">
    <div class="mochila-pagamento">
        
        <div class="titulo-pagamento">
            <h4 class="mochila-pagamento">MOCHILA FECHADA</h4>
        </div>
    </div>

    <div class="container">
        <div data-toggle="collapse" id="collapse-mochila" aria-expanded=false data-target="#produtos_mochila_fechada">
            <div class="mochila-fechada-collapse col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 text-center">
                <?php foreach($PedidoCliente->pedido_itens as $item) { 
                    $qtd_itens += $item->quantidade; 
                } ?>
                <h3>Produtos da mochila (<?= $qtd_itens ?>) R$: <?= number_format($PedidoCliente->valor, 2, ',', '.') ?> <i class="fa fa-chevron-up pull-right"></i>
                <i class="fa fa-chevron-down pull-right"></i></h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div id="produtos_mochila_fechada" class="collapse">
            <div class="lista-produtos">
                <div class="col-xs-12 pedidos-topo">
                    <div class="col-xs-5 text-center">
                        <span>PRODUTO</span>
                    </div>
                    <div class="col-sm-2 text-center">
                        <span>QUANTIDADE</span>
                    </div>
                    <div class="col-sm-2 text-center">
                        <span>VALOR UNITÁRIO</span>
                    </div>
                    <div class="col-sm-2 text-right">
                        <span>VALOR TOTAL</span>
                    </div>
                </div>
                <?php $subtotal = 0; ?>
                <?php foreach ($PedidoCliente->pedido_itens as $item) { ?>
                    <?php 
                        $fotos = unserialize($item->produto->fotos);
                     ?>
                    <div class="col-xs-12">
                        <div class=" col-xs-4 col-sm-1 imagem-produto-carrinho">
                            <?= $this->Html->link(
                                $this->Html->image($fotos[0],
                                    ['class' => 'dimensao-produto-carrinho', 'alt' => $item->produto->produto_base->name . ' ' . $item->produto->propriedade]
                                ),
                                '/produto/' . $item->produto->slug,
                                ['escape' => false]
                            ) ?>
                        </div>
                        <div class="col-xs-8 col-sm-4 text-center ">
                            <?= $this->Html->link(
                                '<h1 class="font-14 h1-carrinho"><span>'
                                . $item->produto->produto_base->name . ' ' . $item->produto->produto_base->embalagem_conteudo . ' ' . $item->produto->propriedade .
                                '</span></h1>',
                                '/produto/' . $item->produto->slug,
                                ['escape' => false]
                            ) ?>
                            <p class="font-10 codigo-mobile"><span>Código: <?= strtoupper($item->produto->id); ?></span></p>
                            <p class="font-10 fabricante-mobile"><span>Fabricante: <?= $item->produto->produto_base->marca->name; ?></span></p>
                        </div>
                        <div class="col-xs-2 col-sm-2 margin-20 text-center">
                            <p class="titulo-produtos-informacoes"><b>Qtd.</b></p>
                            <p><?= $item->quantidade; ?>
                        </div>
                        <div class="col-xs-5 col-sm-2 margin-20 text-right">
                            <p class="titulo-produtos-informacoes"><b>Val. unit.</b></p>
                            <p class="produtos-informacoes">R$ <?= number_format($item->preco, 2, ',', '.'); ?></p>
                        </div>
                        <div class="col-xs-5 col-sm-2 margin-20 text-right">
                            <p class="titulo-produtos-informacoes"><b>Total</b></p>
                            <p class="produtos-informacoes" id="total-produto-<?= $item->produto->id ?>"
                           class="total-produto">R$ <?= number_format($total_produto = ($item->preco * $item->quantidade), 2, ',', '.'); ?>
                            <?php $subtotal += ($item->preco * $item->quantidade); ?>
                            </p>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 total-pagamentos">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#credit_card">
                        <i class="fa fa-credit-card fa-2x"></i>
                        <br class="clear">
                        Cartão de
                        <br class="clear">
                        crédito
                    </a>
                </li>

                <li>
                    <a data-toggle="tab" href="#boleto_card">
                        <i class="fa fa-barcode fa-2x"></i>
                        <br class="clear">
                        Boleto
                        <br class="clear">
                        &nbsp
                    </a>
                </li>

                <?php if($RetiradaLoja) { ?>
                    <li>
                        <a data-toggle="tab" href="#money_card">
                            <i class="fa fa-money fa-2x"></i>
                            <br class="clear">
                            Pagar na
                            <br class="clear">
                            loja
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 total-tabs">
            <div class="tab-content">
                <div id="credit_card" class="tab-pane fade in active">
                    <?= $this->Form->create(null, ['class' => 'credit-card-form'])?>
                    <div class="row">
                        <div class="col-xs-12 bandeiras">
                            <ul>
                                <li>
                                    <label>
                                        <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="visa"> <?= $this->Html->image('bandeiras/visa.png');  ?>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="mastercard"> <?= $this->Html->image('bandeiras/mastercard.png')  ?>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="elo"> <?= $this->Html->image('bandeiras/elo.png')  ?>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="dinners"> <?= $this->Html->image('bandeiras/dinners.png')  ?>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="credit-card-radio-bandeira" class="credit-card-bandeira" value="amex"> <?= $this->Html->image('bandeiras/american_express.jpg')  ?>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-10 col-sm-4 col-md-4 form-group">
                            <?= $this->Form->input(null, [
                                'div'           => false,
                                'label'         => 'Número do cartão',
                                'class'         => 'form-control credit-card-obrigatorio credit-card-numero card-number-mask',
                                'placeholder'   => 'Ex: 0000 0000 0000 0000',
                            ])?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-3 col-md-2 form-group">
                            <label>Validade</label>
                            <select class="form-control credit-card-obrigatorio credit-card-validade-mes" autocomplete="off">
                                <option></option>
                                <?php for($m = 1; $m <= 12; $m++) { ?>
                                    <option value="<?= str_pad($m, 2, 0, STR_PAD_LEFT) ?>"><?= str_pad($m, 2, 0, STR_PAD_LEFT) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-2 form-group">
                            <label for="">&nbsp</label>
                            <select class="form-control credit-card-obrigatorio credit-card-validade-ano" autocomplete="off">
                                <option></option>
                                <?php for($m = 0; $m < 30; $m++) { ?>
                                    <option value="<?= $m + date('Y') ?>"><?= $m + date('Y') ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-xs-10 col-sm-3 col-md-3 form-group">
                            <?= $this->Form->input(null, [
                                'div'           => false,
                                'label'         => 'Código de Segurança',
                                'class'         => 'form-control credit-card-obrigatorio cvv-mask credit-card-cvv',
                                'placeholder'   => '000',
                                'maxlength'     =>  '4',
                            ])?>
                        </div>
                        <div class="col-xs-2 form-group div-card">
                            <label for="">&nbsp</label>
                            <span style=" margin-left: 5px;"><i class="fa fa-credit-card fa-2x"></i></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 form-group">
                            <?= $this->Form->input(null, [
                                'div'           => false,
                                'label'         => 'Nome impresso no cartão',
                                'class'         => 'form-control credit-card-obrigatorio credit-card-titular',
                                'placeholder'   => 'Nome Sobrenome',
                            ])?>
                        </div>
                        <div class="col-xs-12 col-sm-6 form-group">
                            <?= $this->Form->input(null, [
                                'div'           => false,
                                'label'         => 'CPF do titular',
                                'class'         => 'form-control credit-card-obrigatorio cpf-mask credit-card-cpf',
                                'placeholder'   => '000.000.000-00',
                            ])?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 form-group">
                            <?= $this->Form->input(null, [
                                'div'           => false,
                                'label'         => 'Nascimento',
                                'class'         => 'form-control credit-card-obrigatorio data-mask credit-card-nascimento',
                                'placeholder'   => 'dd/mm/aa',
                            ])?>
                        </div>
                        <div class="col-xs-12 col-sm-6 form-group">
                            <?= $this->Form->input(null, [
                                'div'           => false,
                                'label'         => 'Telefone',
                                'class'         => 'form-control credit-card-obrigatorio phone-mask credit-card-telefone',
                                'placeholder'   => '(00) 00000-0000'
                            ])?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 form-group">
                            <label>Número de parcelas</label>
                            <select class="form-control credit-card-obrigatorio credit-card-parcelas">
                                <option class="option-vista" data-id="<?= number_format($PedidoCliente->valor, 2, '', '') ?>" value="1">1x de R$<?= number_format($PedidoCliente->valor, 2, ',', '.') ?> (À Vista)</option>
                                <?php 
                                    $valor_juros = $PedidoCliente->valor * 0.089; 
                                    $valor_juros = ($PedidoCliente->valor + $valor_juros); 
                                ?>
                                <option class="option-parcelado" data-id="<?= number_format($valor_juros, 2, '', '') ?>" value="2">2x de R$<?= number_format($valor_juros/2, 2, ',', '.').' = R$'.number_format($valor_juros, 2, ',', '.') ?></option>
                                <?php if($PedidoCliente->valor > 100.00) { ?>
                                    <option class="option-parcelado" data-id="<?= number_format($valor_juros, 2, '', '') ?>" value="3">3x de R$<?= number_format($valor_juros/3, 2, ',', '.').' = R$'.number_format($valor_juros, 2, ',', '.') ?></option>
                                <?php } if($PedidoCliente->valor > 200.00) { ?>
                                    <option class="option-parcelado" data-id="<?= number_format($valor_juros, 2, '', '') ?>" value="4">4x de R$<?= number_format($valor_juros/4, 2, ',', '.').' = R$'.number_format($valor_juros, 2, ',', '.') ?></option>
                                    <option class="option-parcelado" data-id="<?= number_format($valor_juros, 2, '', '') ?>" value="5">5x de R$<?= number_format($valor_juros/5, 2, ',', '.').' = R$'.number_format($valor_juros, 2, ',', '.') ?></option>
                                <?php } if($PedidoCliente->valor > 300.00) { ?>
                                    <option class="option-parcelado" data-id="<?= number_format($valor_juros, 2, '', '') ?>" value="6">6x de R$<?= number_format($valor_juros/6, 2, ',', '.').' = R$'.number_format($valor_juros, 2, ',', '.') ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-6 form-group">
                            <p><strong>Resumo de compra</strong></p>
                            <?php if($cupom_invalido) { ?>
                                <p style="color: red">Cupom inválido. Já utilizado por esse CPF</p>
                            <?php } else if($cupom_valido) { ?>
                                <p style="color: green">Cupom de desconto aplicado! =)</p>
                            <?php } ?>
                            <?php if ($DescontoCombo != null) { ?>
                                <p>Desconto combo: <span class="pull-right">R$ <?= number_format(($PedidoCliente->valor / 0.95) * 0.05, 2, ',', '.') ?></span></p>
                            <?php } ?>
                            <p>Total dos produtos: <span class="pull-right">R$ <?= number_format($PedidoCliente->valor, 2, ',', '.') ?></span></p>
                            <p>Frete grátis: <span class="pull-right">R$ 0,00</span></p>

                            <?php if($RetiradaLoja == 601) { ?>
                                <p class="show-endereco">Entrega: <span class="pull-right"><?= $loja_retirada->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                                <p class="detalhe-endereco"><?= $loja_retirada->address." - ".$loja_retirada->number." - ".$loja_retirada->area ?><br>
                                    <?=  $loja_retirada->city->name." - ".$loja_retirada->city->uf  ?><br>
                                    <?= $loja_retirada->phone ?>
                                </p>
                            <?php } elseif ($RetiradaLoja == 10) { ?>
                                <p class="show-endereco">Entrega: <span class="pull-right"><?= $PedidoCliente->academia->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                                <p class="detalhe-endereco"><?= $PedidoCliente->academia->address." - ".$PedidoCliente->academia->number." - ".$PedidoCliente->academia->area ?><br>
                                    <?=  $PedidoCliente->academia->city->name." - ".$PedidoCliente->academia->city->uf  ?><br>
                                    <?= $PedidoCliente->academia->phone ?>
                                </p>
                                
                            <?php } elseif ($RetiradaLoja == 20) { ?>
                                <p class="show-endereco">Entrega: <span class="pull-right"><?= $PedidoCliente->cliente->name ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                                <p class="detalhe-endereco"><?= $PedidoCliente->cliente->address." - ".$PedidoCliente->cliente->number." - ".$PedidoCliente->cliente->area ?><br>
                                    <?=  $PedidoCliente->cliente->city->name." - ".$PedidoCliente->cliente->city->uf  ?><br>
                                    <?= $PedidoCliente->cliente->mobile ?>
                                </p>
                            <?php } ?>

                            <p>Membro do time: <span class="pull-right"><?= $PedidoCliente->professore->name ?></span></p>
                        </div>
                    </div>
                    <input type="hidden" class="credit-card-pedido" value="<?= $PedidoCliente->id ?>">
                    <input type="hidden" class="credit-card-aluno" value="<?= $PedidoCliente->cliente->name ?>">
                    <input type="hidden" class="credit-card-token">
                    <div class="row"> 
                        <div class="col-xs-12 form-group text-center">
                            <button type="button" class="btn btn-verde credit-card-btn">
                                <i class="fa fa-credit-card-alt"></i> pagar
                            </button>
                            <br>
                        </div>
                    </div>
                    <?= $this->Form->end()?>
                </div>
                <div id="boleto_card" class="tab-pane fade text-left">
                    <div class="row">
                        <?= $this->Form->create(null, ['class' => 'boleto-form']) ?>
                            <div class="col-xs-12 col-md-6 text-center form-group">
                                <br>
                                <br>
                                <button type="submit" class="btn btn-verde boleto-btn">
                                    <i class="fa fa-barcode"></i>
                                    Gerar Boleto
                                </button>
                            </div>
                            <input type="hidden" class="boleto-pedido" value="<?= $PedidoCliente->id ?>">
                            <input type="hidden" class="boleto-valor" value="<?= number_format($PedidoCliente->valor, 2, '', '') ?>">
                        <?= $this->Form->end() ?>
                        <div class="col-xs-12 col-md-6 form-group">
                            <p><strong>Resumo de compra</strong></p>
                            <?php if($cupom_invalido) { ?>
                                <p style="color: red">Cupom inválido. Já utilizado por esse CPF</p>
                            <?php } else if($cupom_valido) { ?>
                                <p style="color: green">Cupom de desconto aplicado! =)</p>
                            <?php } ?>
                            <?php if ($DescontoCombo != null) { ?>
                            <p>Desconto combo: <span class="pull-right">R$ <?= number_format(($PedidoCliente->valor / 0.95) * 0.05, 2, ',', '.') ?></span></p>
                            <?php } ?>
                            <p>Total dos produtos: <span class="pull-right">R$ <?= number_format($PedidoCliente->valor, 2, ',', '.') ?></span></p>
                            <p>Frete grátis: <span class="pull-right">R$ 0,00</span></p>

                            <?php if($RetiradaLoja == 601) { ?>
                                <p class="show-endereco">Entrega: <span class="pull-right"><?= $loja_retirada->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                                <p class="detalhe-endereco"><?= $loja_retirada->address." - ".$loja_retirada->number." - ".$loja_retirada->area ?><br>
                                    <?=  $loja_retirada->city->name." - ".$loja_retirada->city->uf  ?><br>
                                    <?= $loja_retirada->phone ?>
                                </p>
                            <?php } elseif ($RetiradaLoja == 10) { ?>
                                <p class="show-endereco">Entrega: <span class="pull-right"><?= $PedidoCliente->academia->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                                <p class="detalhe-endereco"><?= $PedidoCliente->academia->address." - ".$PedidoCliente->academia->number." - ".$PedidoCliente->academia->area ?><br>
                                    <?=  $PedidoCliente->academia->city->name." - ".$PedidoCliente->academia->city->uf  ?><br>
                                    <?= $PedidoCliente->academia->phone ?>
                                </p>
                                
                            <?php } elseif ($RetiradaLoja == 20) { ?>
                                <p class="show-endereco">Entrega: <span class="pull-right"><?= $PedidoCliente->cliente->name ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                                <p class="detalhe-endereco"><?= $PedidoCliente->cliente->address." - ".$PedidoCliente->cliente->number." - ".$PedidoCliente->cliente->area ?><br>
                                    <?=  $PedidoCliente->cliente->city->name." - ".$PedidoCliente->cliente->city->uf  ?><br>
                                    <?= $PedidoCliente->cliente->mobile ?>
                                </p>
                            <?php } ?>

                            <p>Membro do time: <span class="pull-right"><?= $PedidoCliente->professore->name ?></span></p>
                        </div>
                    </div>
                    <p><strong>Data de vencimento:</strong> verifique a data de vencimento do boleto que é de 3(três) dias após ser gerado. Caso não seja pago até a data informada o pedido será automaticamente cancelado.</p>
                    <p><strong>Prazo de entrega:</strong> é contado a partir da confirmação de pagamento pelo banco. A confirmação pode levar até 2 dias úteis.</p>
                    <p><strong>Pagamento:</strong> pode ser feito pela internet e aplicativo do seu banco utilizando o código de barras ou diretamente em bancos, lotéricas e correios com a apresentação o boleto impresso.</p>
                    <h4>ATENÇÃO</h4>
                    <ul>
                        <li>Não será enviada uma cópia impressa do boleto para seu endereço.</li>
                        <li>Desabilite o recurso anti pop-up caso você use.</li>
                    </ul>
                    <br>
                    <br>
                </div>
                <?php if($RetiradaLoja == 601) { ?>
                    <div id="money_card" class="tab-pane fade text-left">
                        <div class="row">
                            <?= $this->Form->create(null, ['url' => ['controller' => 'produtos', 'action' => 'pagar_dinheiro'], 'class' => 'pagar-loja-form']) ?>
                                <div class="col-xs-12 col-md-6 text-center form-group">
                                    <h5>Clique no botão para separarmos seu pedido do estoque e você retirá-lo com a gente.</h5>
                                    <br>
                                    <button type="submit" class="btn btn-verde">
                                        <i class="fa fa-money"></i>
                                        Pagar na loja
                                    </button>
                                </div>
                                <input type="hidden" name="pedido_id" value="<?= $PedidoCliente->id ?>">
                            <?= $this->Form->end() ?>
                            <div class="col-xs-12 col-md-6 form-group">
                                <p><strong>Resumo de compra</strong></p>
                                <?php if($cupom_invalido) { ?>
                                    <p style="color: red">Cupom inválido. Já utilizado por esse CPF</p>
                                <?php } else if($cupom_valido) { ?>
                                    <p style="color: green">Cupom de desconto aplicado! =)</p>
                                <?php } ?>
                                <?php if ($DescontoCombo != null) { ?>
                                <p>Desconto combo: <span class="pull-right">R$ <?= number_format(($PedidoCliente->valor / 0.95) * 0.05, 2, ',', '.') ?></span></p>
                                <?php } ?>
                                <p>Total dos produtos: <span class="pull-right">R$ <?= number_format($PedidoCliente->valor, 2, ',', '.') ?></span></p>
                                <p>Frete grátis: <span class="pull-right">R$ 0,00</span></p>
                                <?php if($RetiradaLoja == 601) { ?>
                                    <p class="show-endereco">Entrega: <span class="pull-right"><?= $loja_retirada->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                                    <p class="detalhe-endereco"><?= $loja_retirada->address." - ".$loja_retirada->number." - ".$loja_retirada->area ?><br>
                                        <?=  $loja_retirada->city->name." - ".$loja_retirada->city->uf  ?><br>
                                        <?= $loja_retirada->phone ?>
                                    </p>
                                <?php } else { ?>
                                    <p class="show-endereco">Entrega: <span class="pull-right"><?= $PedidoCliente->academia->shortname ?> <i class="fa fa-question-circle" aria-hidden="true"></i></span></p>
                                    <p class="detalhe-endereco"><?= $PedidoCliente->academia->address." - ".$PedidoCliente->academia->number." - ".$PedidoCliente->academia->area ?><br>
                                        <?=  $PedidoCliente->academia->city->name." - ".$PedidoCliente->academia->city->uf  ?><br>
                                        <?= $PedidoCliente->academia->phone ?>
                                    </p>
                                <?php } ?>
                                <p>Membro do time: <span class="pull-right"><?= $PedidoCliente->professore->name ?></span></p>
                            </div>
                        </div>
                        <br>
                        <br>
                    </div>
                <?php } ?>
            </div>
            </div>
        </div>
        
    </div>
</div>

<script>
    $(document).ready(function() {

        $('.show-endereco').click(function(e){
            $('.detalhe-endereco').slideToggle("slow");
        });
        
        $(document).on('click', '.debit-card-bandeira', function() {
            var bandeira = $('input[name=debit-card-radio-bandeira]:checked').val();
            if(bandeira == 'Logcard') {
                $('.debit-card-titular-box').fadeOut();
                $('.debit-card-titular').val('holder holder');
            } else {
                $('.debit-card-titular-box').fadeIn();
                $('.debit-card-titular').val('');
            }
        });

        $(document).on('click', '.credit-card-bandeira', function() {
            var bandeira = $('input[name=credit-card-radio-bandeira]:checked').val();
            if(bandeira == 'Discover') {
                $('.option-vista').select();
                $('.option-parcelado').hide();
            } else {
                $('.option-parcelado').show();
            }
        });

        $(document).on('click', '.credit-card-btn', function() {
            var btn = $(this);
            var campos_obrigatorios = $('.credit-card-obrigatorio');
            var bandeira = $('input[name=credit-card-radio-bandeira]:checked').val();

            var numero_cartao = $('.credit-card-numero').val();
                numero_cartao = numero_cartao.replace(/\s/g, '');
            var validade_mes = $('.credit-card-validade-mes option:selected').val();
            var validade_ano = $('.credit-card-validade-ano option:selected').val();
            var validade = validade_mes+'/'+validade_ano;
            var codigo_cvv = $('.credit-card-cvv').val();
            var titular_cartao = $('.credit-card-titular').val();
            var cpf_cartao = $('.credit-card-cpf').val();
            var nascimento_cartao = $('.credit-card-nascimento').val();
            var telefone_cartao = $('.credit-card-telefone').val();
            var parcelas = $('.credit-card-parcelas option:selected').val();
            var valor_total = $('.credit-card-parcelas option:selected').attr('data-id');
            var pedido_id = $('.credit-card-pedido').val();
            var comprador = $('.credit-card-aluno').val();

            loading.fadeIn();
            
            var campos_nulos = 0;

            $('.alert-error').remove();

            campos_obrigatorios.each(function() {
                if($(this).val() == '' || $(this).val() == null) {
                    campos_nulos = 1;
                }
            });

            if(bandeira == '' || bandeira == null) {
                campos_nulos = 1;
            }

            if(campos_nulos == 1) {
                btn.before('<p class="alert-error" style="color: red">Preencha todos os dados!</p>');
                loading.fadeOut();
            }

            if(campos_nulos != 1) {
                if(bandeira == 'Discover' || bandeira == 'Jcb' || bandeira == 'Aura') {
                    $.ajax({
                        type: "POST",
                        url: WEBROOT_URL + 'pagar/cielo/credito',
                        data: {
                            card_numero: numero_cartao,
                            card_validade: validade,
                            card_cvv: codigo_cvv,
                            card_titular: titular_cartao,
                            card_bandeira: bandeira,
                            card_cpf: cpf_cartao,
                            card_nascimento: nascimento_cartao,
                            card_telefone: telefone_cartao,
                            parcelas: parcelas,
                            valor_total: valor_total,
                            pedido_id: pedido_id,
                            comprador: comprador
                        }
                    })
                        .done(function (data) {
                            
                            loading.fadeOut();
                            
                            if(data == 0) {
                                btn.before('<p class="alert-error" style="color: red">Pagamento não autorizado.</p><br><p class="alert-error" style="color: red">Confira os dados, tente novamente e se o erro persistir, contate a emissora do seu cartão.</p>');

                                campos_obrigatorios.each(function() {
                                    $(this).val('');
                                });
                            }

                            if(data == 1) {
                                location.href = WEBROOT_URL + 'mochila/finalizado/' + pedido_id;
                            }
                        });
                } else {
                    var cartao_valido = 1;

                    if(!Iugu.utils.validateCreditCardNumber(numero_cartao)) {
                        cartao_valido = 0;

                        btn.before('<p class="alert-error" style="color: red">Número do cartão inválido!</p>');
                        loading.fadeOut();
                    }

                    if(!Iugu.utils.validateCVV(codigo_cvv, bandeira)) {
                        cartao_valido = 0;

                        btn.before('<p class="alert-error" style="color: red">CVV inválido!</p>');
                        loading.fadeOut();
                    }

                    if(!Iugu.utils.validateExpiration(validade_mes, validade_ano)) {
                        cartao_valido = 0;
                        
                        btn.before('<p class="alert-error" style="color: red">Data de validade inválida!</p>');
                        loading.fadeOut();
                    }

                    if(Iugu.utils.getBrandByCreditCardNumber(numero_cartao) != bandeira) {
                        cartao_valido = 0;
                        btn.before('<p class="alert-error" style="color: red">Número do cartão ou bandeira inválido!</p>');
                        loading.fadeOut();
                    }

                    if(cartao_valido) {
                        var card_token = $(".credit-card-token").val();

                        Iugu.setAccountID('6F4F21663CD44586AA13CC4C648CDB51');
                        
                        //Iugu.setTestMode(true);

                        var tokenResponseHandler = function(data) {
                            if (data.errors) {
                                console.log(data.errors);
                                alert("Erro salvando cartão: " + JSON.stringify(data.errors));
                            } else {
                                $(".credit-card-token").val(data.id);
                                var card_token = $(".credit-card-token").val();
                            }

                            $.ajax({
                                type: "POST",
                                url: WEBROOT_URL + 'pagar/iugu/credito',
                                data: {
                                    card_titular: titular_cartao,
                                    card_bandeira: bandeira,
                                    card_token: card_token,
                                    card_cpf: cpf_cartao,
                                    card_nascimento: nascimento_cartao,
                                    card_telefone: telefone_cartao,
                                    parcelas: parcelas,
                                    valor_total: valor_total,
                                    pedido_id: pedido_id,
                                    comprador: comprador
                                }
                            })
                                .done(function (data) {
                                    
                                    loading.fadeOut();
                                    
                                    if(data == 0) {
                                        btn.before('<p class="alert-error" style="color: red">Pagamento não autorizado.</p><br><p class="alert-error" style="color: red">Confira os dados, tente novamente e se o erro persistir, contate a emissora do seu cartão.</p>');

                                        campos_obrigatorios.each(function() {
                                            $(this).val('');
                                        });
                                    }

                                    if(data == 1) {
                                        location.href = WEBROOT_URL + 'mochila/finalizado/' + pedido_id;
                                    }
                                });
                        }

                        titular_cartao;
                        nomes = [];

                        nomes.push( titular_cartao.split(' ')[0] );
                        nomes.push( titular_cartao.substring(nomes[0].length + 1) );

                        cc = Iugu.CreditCard(numero_cartao, validade_mes, validade_ano, nomes[0], nomes[1], codigo_cvv);
                         
                        Iugu.createPaymentToken(cc, tokenResponseHandler);
                    }
                }
            }

            return false;
        });

        $(document).on('click', '.debit-card-btn', function() {
            var btn = $(this);
            var campos_obrigatorios = $('.debit-card-obrigatorio');
            var bandeira = $('input[name=debit-card-radio-bandeira]:checked').val();

            var numero_cartao = $('.debit-card-numero').val();
            var validade_mes = $('.debit-card-validade-mes option:selected').val();
            var validade_ano = $('.debit-card-validade-ano option:selected').val();
            var validade = validade_mes+'/'+validade_ano;
            var codigo_cvv = $('.debit-card-cvv').val();
            var titular_cartao = $('.debit-card-titular').val();
            var valor_total = $('.debit-card-valor').val();
            var pedido_id = $('.debit-card-pedido').val();
            var comprador = $('.debit-card-aluno').val();

            loading.fadeIn();

            var campos_nulos = 0;

            $('.alert-error').remove();

            campos_obrigatorios.each(function() {
                if($(this).val() == '' || $(this).val() == null) {
                    campos_nulos = 1;
                }
            })

            if(bandeira == '' || bandeira == null) {
                campos_nulos = 1;
            }

            if(campos_nulos == 1) {
                btn.before('<p class="alert-error" style="color: red">Preencha todos os dados!</p>');
                loading.fadeOut();
            }

            if(campos_nulos != 1) {
                if(bandeira == 'Logcard') {
                    bandeira = 'Visa';
                }

                $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + 'pagar/cielo/debito',
                    data: {
                        card_numero: numero_cartao,
                        card_validade: validade,
                        card_cvv: codigo_cvv,
                        card_titular: titular_cartao,
                        card_bandeira: bandeira,
                        valor_total: valor_total,
                        pedido_id: pedido_id,
                        comprador: comprador
                    }
                })
                    .done(function (data) {
                        
                        loading.fadeOut();
                            
                        if(data == 0) {
                            btn.before('<p class="alert-error" style="color: red">Pagamento não autorizado.</p><br><p class="alert-error" style="color: red">Confira os dados, tente novamente e se o erro persistir, contate a emissora do seu cartão.</p>');

                            campos_obrigatorios.each(function() {
                                $(this).val('');
                            });
                        } else {
                            location.href = data;
                        }
                    });
            }

            return false;
        });

        $(document).on('click', '.boleto-btn', function() {
            var btn = $(this);

            var pedido_id = $('.boleto-pedido').val();
            var valor_total = $('.boleto-valor').val();

            loading.fadeIn();

            $('.alert-error').remove();

            $.ajax({
                type: "POST",
                url: WEBROOT_URL + 'pagar/iugu/boleto',
                data: {
                    valor_total: valor_total,
                    pedido_id: pedido_id
                }
            })
                .done(function (data) {
                    
                    loading.fadeOut();
                    
                    if(data == 0) {
                        btn.before('<p class="alert-error" style="color: red">Falha ao gerar boleto. Tente novamente!</p>');
                    }

                    if(data == 1) {
                        location.href = WEBROOT_URL + 'mochila/finalizado/' + pedido_id;
                    }
                });

            return false;
        });

        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        });
    });
</script>