
<script>
	$(document).ready(function(){
		$("#total-marcas").click(function(){
	        $(".chek-marcas").prop('checked', $(this).prop('checked'));
	    });
	    $(".chek-marcas").change(function(){ 
	    	if (!$(this).prop("checked")) {
	    		$("#total-marcas").prop("checked",false);
	    	} 
	    });
	});
</script>

<div class="container catalogo">
	<div class="row">
		<div class="col-xs-12 text-center logo-acad">
			<?php 
				if($academia->image != null){
                	echo $this->Html->link($this->Html->image('academias/'.$academia->image, ['alt' => $academia->shortname]),'/'.$academia->slug,['escape' => false]);
            	} else{
               		echo '<h4>'.$academia->shortname.'</h4>';
            	}
        	?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 text-center">
			<h2>Montar catálogo</h2>
			<p><strong>Monte catálogos personalizados com os produtos de sua(s) marca(s) favoritas ou com o foco em objetivo(s) específico(s)!</strong></p>
		</div>
	</div>
	<div class="">
		<div class="col-xs-12 catalogo-config text-left">
			<div class="row">
				<div class="col-xs-12">
					<h4 class="text-center">Marcas</h4>
						<div class="checkbox">
					    	<label>
				      			<input type="checkbox" id="total-marcas"> TODAS
					    	</label>
				    	</div>
					<?php foreach ($marcas as $marca) { ?>
						<?php if($marca->id != 49) { ?>
							<div class="checkbox check-catalogo ">
						    	<label>
					      			<input type="checkbox" class="check-marcas" value="<?= $marca->id ?>"> <?=$marca->name ?>
						    	</label>
					    	</div>
					    <?php } ?>
					<?php } ?>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-xs-12">
					<h4 class="text-center">Opções</h4>
					<div class="checkbox">
				    	<label>
			      			<input type="checkbox" class="tem_estoque" checked="true"> Somente produtos em estoque
				    	</label>
			    	</div>
			    	<div class="checkbox">
				    	<label>
			      			<input type="checkbox" class="tem_valor"> Incluir valor do produto no catálogo
				    	</label>
			    	</div>
				</div>
				<div class="col-xs-12 col-md-4 col-md-offset-4">
					<button class="btn btn-success btn-sm btn-block btn-visualizar-catalogo"><i class="fa fa-book" aria-hidden="true"></i> Visualizar catálogo</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$(document).on('click', '.btn-visualizar-catalogo', function() {
			var marcas;

			if($('.tem_estoque').is(':checked')) {
				var estoque = 1;
			} else {
				var estoque = 0;
			}
			if($('.tem_valor').is(':checked')) {
				var valor = 1;
			} else {
				var valor = 0;
			}
			var btn = $(this);

			$('.alert-error').remove();

			var ctrl = 0;
			$('.check-marcas:checked').each(function() {
				if($(this).val() > 0) {
					if(ctrl == 0) {
						marcas = $(this).val();
					} else {
						marcas = marcas+','+$(this).val();
					}
					ctrl++;
				}
			});

			if(marcas != null) {
				location.href = WEBROOT_URL + SSLUG + '/catalogo/preview?marcas='+marcas+'&estoque='+estoque+'&valor='+valor;
			} else {
				btn.before('<p class="alert-error" style="color:red; text-align: center">Selecione pelo menos 1 marca!</p>');
			}
		});
	});
</script>