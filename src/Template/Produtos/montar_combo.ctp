<script type="text/javascript">
    var query_url = "<?= $query != null ? $query : 0 ?>";
    var marca_url = "<?= $marca != null ? $marca : 0 ?>";
    var tipo_filtro_url = "<?= $tipo_filtro != null ? $tipo_filtro : 0 ?>";
    var filtro_url = "<?= $filtro_slug != null ? $filtro_slug : 0 ?>";
    var ordenacao_url = "<?= $ordenacao != null ? $ordenacao : '' ?>";
</script>
<script>
    $(document).ready(function() {
        $('.marcas-link, .objetivo-link').click(function() {
            var container = $('.container-grade');
            var posicao_pagina = $(document).scrollTop();
            var posicao_container = (container.offset().top + 310);
            if(posicao_pagina < posicao_container) {
                if($(document).width() > 992) {
                    $('html, body').animate({
                        scrollTop: posicao_container
                    }, 700);
                }
                $('body').addClass('total-block');
            }
        });
    });
</script>

<style>
    .btn-compre{
        background: #8dc73f;
    }
    .btn-compre svg{
        fill: white;
        width: 25px;
        height: auto;
    }

    .produto-acabou-btn{
        background-color: #5089cf;
    }
    .produto-acabou-btn .fa{
        margin-top: -3px;
        color: #fff;
    }
    .btn-box{
        width: 49%;
        padding:5px;
    }
    .btn-box span{
        color: #fff;
        font-size: 12px;
        margin-top: 3px;
    }
    .btn-box img{
        width: 18px;
        margin-right: 5px;
        margin-top: -5px;
    }
    .box-botoes{
        display: inline-block;
        width: 100%;
        padding-top: 5px;
        float: right;
    }
    .div-sabor{
        height: 23px;
        padding: 0;
    }
    .prdt{
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        margin-bottom: 30px;
        padding: 5px 10px;
        min-height: 380px;
    }
    .link-prdt{
        height: 72px;
    }
    #pagar-celular{
        display: none;
    }
    .promocao-index, #grade-produtos {
        padding-right: 230px;
        padding-left: 80px;
        margin-left: 0;
        margin-right: 0;
        width: 100%;
    }
    .product {
        font-size: 14px;
    }
    .produto-item .bg-white-produto {
        min-height: 314px;
        border: 2px solid rgb(234, 234, 234);
        box-shadow: 0px 1px 7px 0px rgba(200,200,200,0.75);
    }
    .bg-gray-produto {
        bottom: 0;
        position: absolute;
        min-height: 92px;
        max-width: 185px;
    }
    .description {
        width: 100%;
        height: 40px;
        overflow: hidden;
        margin: 0 0 5px 0;
        font-size: 14px;
        text-transform: uppercase;
        font-weight: 700;
    }
    @media all and (max-width: 768px) {
        .produto-item .bg-white-produto {
            min-height: 350px;
        }
    }
    @media all and (min-width: 769px) {
        .box-close-filter { 
            width: 0; 
        }
        .filtro { 
            width: 230px; 
            opacity: 1; 
        }
        .box-filtro { 
            left: 0; 
        }
    }
    .produto-grade {
        max-width: 145px;
        max-height: 150px;
        margin-top: 0;
        display: flex;
        align-items: center;
    }
    .box-imagem-produto {
        width: 100%;
        height: 150px;
        margin-top: -15px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .box-info-produto {
        width: 100%;
        height: 200px;
    }
    .box-fundo-produto {
        width: 100%;
        padding: 0;
    }
    .box-preco {
        height: 70px;
        width: 100%;
        float: left;
        display: flex;
        flex-flow: column;
        justify-content: center;
    }
    .box-mochila-add {
        height: 45px;
        width: 45px;
        float: right;
        display: flex;
        align-items: flex-end;
        margin-top: 10px;
    }
    .box-mochila-add svg {
        width: 100%;
        height: 100%;
        cursor: pointer;
        background: #8dc73f;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
        fill: white;
        margin-right: 3px;
    }
    .box-mochila-avise {
        height: 45px;
        width: 45px;
        float: right;
        margin-top: 10px;
        background-color: #5089cf;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
    }
    .box-mochila-avise > .fa{
        color: #FFF;
        font-size: 2.5em;
        margin-bottom: 2px;
        margin-top: 3px;
    }
    .categoria{
        height: 30px;
    }
    .categoria-div{
        position: absolute;
        right: 5px;
        top: 0px;
        width: 20px;
        height: 30px;
    }
    .preco {
        margin: 0;
        font-size: 16px;
    }
    .preco-desc {
        font-size: 16px;
    }
    .preco-normal {
        font-weight: 700;
        font-size: 20px;
        color: #5089cf;
    }
    .bg-white-produto a {
        min-height: 0;
    }
    .box-promocao {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: black;
        background: #f3d012;
    }
    .box-promocao:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-novidade {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: white;
        background: #5089cf;
    }
    .box-novidade:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-ultimo {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: white;
        background: red;
    }
    .box-ultimo:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-mochila-add svg:hover, .box-mochila-add svg:focus, .box-mochila-add svg:link:hover, .box-mochila-add svg:link:focus, .box-mochila-add svg:visited:hover, .box-mochila-add svg:visited:focus, .box-mochila-avise:hover, .btn-box:hover {
      -webkit-transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      -webkit-animation: diff 0.5s 1;
      animation: diff 0.5s 1;
    }

    @-webkit-keyframes diff {
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
    }
    @keyframes diff {
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
    }

    .icone-objetivo {
        position: absolute;
        left: 5px;
        bottom: 120px;
        width: 30px;
        height: 30px;
        color: #5089cf;
    }

    .box-imagem-produto svg {
        position: absolute;
        left: 5px;
        bottom: 130px;
        width: 20px;
        height: 30px;
    }

    .sabor-info {
        font-size: 12px;
        color: #333;
        text-align: center;
        overflow: hidden;
    }

    @media all and (min-width: 1650px) {
        .produto-item {
            width: 16.66666667%;
        }
    }
    @media all and (max-width: 1240px) {
        .produto-item {
            width: 33.3333333334%;
        }
    }
    @media all and (max-width: 1200px) and (min-width: 780px) {
        .form-busca {
            width: 80% !important;
        }
    }
    @media all and (max-width: 1050px) {
        .produto-item {
            width: 50%;
        }
    }
    @media all and (max-width: 850px) {
        .produto-item {
            width: 100%;
        }
    }
    @media all and (max-width: 768px) {
        #pagar-celular{
            display: block;
            margin-bottom: 7px;
        }
        #pagar-celular .btn-login-nav{
            font-size: 18px;
            padding: 6px;
        }
        .promocao-index, #grade-produtos {
            padding-left: 0;
            padding-right: 0;
        }
        .promocao-index {
            padding-top: 90px;
        }
        .produto-item {
            width: 50%;
        }
    }
    @media all and (max-width: 400px) {
        .produto-grade {
            max-width: 120px;
            max-height: 120px;
        }
        .produto-item {
            padding-right: 0px;
            padding-left: 0px;
        }
    }
    .loader {
        position: fixed;
        left: 0;
        top: 0%;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: white;
    }
    .content-loader{
        position: fixed;
        border-radius: 25px;
        left: 20%;
        top: 25%;
        width: 60%;
        height: 50%;
        z-index: 999999;
        background-color: white;
        text-align: center;
    }
    .content-loader img{
        width: 500px;
        margin-top: 45px;
    }
    .content-loader i{
        color: #f4d637;
    }
    .content-loader p{
        color: #5087c7;
        font-family: nexa_boldregular;
        font-size: 34px;
    }
    @media all and (max-width: 430px) {
        .content-loader{
            display: none;
        }
    }
    @media all and (max-width: 768px) {
        .loader {
            display: none;
        }
    }
    .dropdown a{
        color: #fff;
        font-weight: 400;
    }
    .nav .open>a, .nav .open>a:hover, .nav .open>a:focus, .nav>li>a:focus{
        background-color: rgb(66, 118, 181);
        border-color: rgb(66, 118, 181);
    }
    #dropdown-filtro ul{
        padding: 0;
    }
    #dropdown-filtro li{
        padding: 0;
    }
    .filtros-ativos{
        height: auto;
        margin-bottom: 15px;
        padding-bottom: 15px;
    }
    .filtros-ativos span{
        min-width: 100px;
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        background-color: rgba(255,255,255,1.0);
        border-radius: 5px;
        padding: 5px 10px;
        margin: 5px;
        display: inline-block;
        height: 30px;
    }
    .filtros-ativos .fa{
        color: rgb(255, 0, 0);
    }
    .filtros-ativos .fa:hover{
        color: rgb(244, 219, 27);
        cursor: pointer;
    }
    .filtros-ativos i{
        float: right;
        margin-top: 2px;
    }
    .subfiltros{
        padding: 0;
    }
    .subfiltros select{
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        background-color: rgba(255,255,255,1.0);
        border-radius: 5px;
        padding: 0px 10px;
        height: 25px;
        margin: 5px 3px;
        border: none;
        display: inline-block;
    }
    .owl-theme .owl-controls .owl-buttons div {
        color: #6f6f6f!important;
        background-color: transparent!important;
        font-size: 30px!important;
    }

    .owl-prev {
        width: 15px;
        height: 100px;
        position: absolute;
        top: 40%;
        left: 0;
        display: block!important;
        border:0px solid black;
    }

    .owl-next {
        width: 15px;
        height: 100px;
        position: absolute;
        top: 40%;
        right: -10px;
        display: block!important;
        border:0px solid black;
    }
    .logo-marcas {
        -webkit-filter: grayscale(100%);
        max-width: 80%;
        max-height: 90px;
    }
    .logo-marcas:hover {
        -webkit-filter: grayscale(0);
    }
    .faixa-combos {
        height: 474px;
        position: absolute;
        background-color: #61c513;
        left: 0;
        margin-left: -300px;
        width: 150%;
    }
    .container-grade {
        padding: 500px 0 25px 0;
        background-color: rgba(255, 255, 255, 1);
    }
    .dropdown-content {
        z-index: 50!important;
    }
    .btn-escolhido {
        color: white;
        font-size: 12px;
        text-transform: capitalize;
        padding: 2px 12px;
        margin-top: 3px;
        max-width: 100%;
    }
    .btn-escolhido p{
        margin-bottom: 2px;
        overflow-x: hidden;
        text-overflow: ellipsis;

    }
    .produtos-scroll {
        margin-top: 15px;
        position: relative;
        float: left;
        width: 100%;
    }
    .subfiltro-completo h4 {
        margin-top: 0; 
        color: white;
    }
    .subfiltro-completo .texto-sombra {
        text-shadow: 1px 0px 2px #1a1915, 
                    -1px 0px 2px #1a1915, 
                    0px 1px 2px #1a1915, 
                    0px -1px 2px #1a1915;
    }
    .fixed_subfiltro h4 {
        color: black;
    }
</style>

<?php if (!$this->request->is('mobile')) { ?>
    <div class="banner-bg" style="background-image: url('<?= WEBROOT_URL ?>/img/banners-loja/padrao-combo.jpg');"></div>
<?php } ?>

<?= $this->Element('overlay-combo'); ?>
<div class="container container-grade" data-section="home">
    <div class="col-sm-4 col-md-2 filtro-div">
        <?= $this->Element('mochila-combo'); ?>
    </div>
    <div class="col-sm-12 col-md-10 produtos-div">
        <div class="produtos-grade">
            <div class="produtos-scroll">
                <?php foreach($produtos as $produto) { ?>
                    <?php $fotos = unserialize($produto->fotos); ?>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 box-completa-div">
                        <div class="box-completa-produto box-completa-produto-busca" data-id="<?= $produto->id ?>" itemtype="http://schema.org/Product">
                            <div class="tarja-objetivo">
                                <?php
                                    $contador_objetivos = 0; 
                                    $novo_top = 0;
                                ?>
                                <?php foreach ($produto->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                    <?php if ($contador_objetivos < 2) { ?>
                                    <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                                        <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                    </div>
                                    <?php } ?>
                                    <?php
                                        $contador_objetivos++;
                                        $novo_top = $novo_top + 19;
                                     ?>
                                <?php } ?>
                            </div>
                            <div class="total-tarjas">
                                <?php if ($produto->estoque == 1) { ?>
                                    <div class="tarja tarja-ultimo">
                                        <span>Último</span>
                                    </div>
                                <?php } else if ($produto->estoque >= 1 && $produto->preco_promo || $desconto_geral) { ?>
                                    <div class="tarja tarja-promocao">
                                        <span>Promoção</span>
                                    </div>
                                <?php } else if($produto->estoque >= 1 && $produto->created > $max_date_novidade) { ?>
                                    <div class="tarja tarja-novidade">
                                        <span>Novidade</span>
                                    </div>
                                <?php } ?>               
                            </div>
                            <div class="info-produto">
                                <div class="produto-superior">
                                    <a href="<?= $SSlug.'/combo/produto/'.$number.'/'.$produto->slug ?>">
                                        <?php 
                                            $fotos = unserialize($produto->fotos); 

                                            $primeira_foto = explode('produtos/', $fotos[0]);

                                            $foto = $primeira_foto[0].'produtos/md-'.$primeira_foto[1];
                                        ?>
                                        <div class="foto-produto" id="produto-<?= $produto->id ?>" >
                                            <?= $this->Html->image(
                                                $fotos[0],
                                                ['val' => $produto->slug, 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]); ?>
                                        </div>
                                        <div class="detalhes-produto">
                                            <div class="informacoes-detalhes text-left">
                                                <p>Código: <?= $produto->id ?></p>
                                                <p><?= strtolower($produto->produto_base->marca->name); ?></p>
                                                <p><?= $produto->tamanho.' '.$produto->unidade_medida; ?></p>
                                                <p>para <?= $produto->doses; ?> treinos</p>
                                                <p><?= strtolower($produto->propriedade); ?></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="produto-inferior">
                                    <?php if($produto->preco_promo || $desconto_geral > 0) {
                                        $porcentagem_nova = $produto->preco * 0.089;
                                        $preco_corte = $produto->preco + $porcentagem_nova; 
                                    } ?> 
                                    <?php $produto->preco_promo ?
                                    number_format($preco = $produto->preco_promo * $desconto_geral, 2, ',','.') :
                                    number_format($preco = $produto->preco * $desconto_geral, 2, ',','.'); ?>
                                    <?php $produto->preco_promo ? $porcentagem_nova = $produto->preco_promo * 0.089 : $porcentagem_nova = $produto->preco * 0.089 ?>
                                    <?php if($produto->preco_promo != null) {
                                        if($produto->preco_promo < 100.00) { ?>
                                        <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/2?>
                                        <?php $maximo_parcelas = "2x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto->preco_promo > 100.00 && $produto->preco_promo < 200.00) { ?>
                                        <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/3?>
                                        <?php $maximo_parcelas = "3x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto->preco_promo > 200.00 && $produto->preco_promo < 300.00) { ?>
                                        <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/5?>
                                        <?php $maximo_parcelas = "5x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto->preco_promo > 300.00) { ?>
                                        <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/6?>
                                        <?php $maximo_parcelas = "6x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php }
                                        } else {
                                    if($produto->preco < 100.00) { ?>
                                        <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/2?>
                                         <?php $maximo_parcelas = "2x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto->preco > 100.00 && $produto->preco < 200.00) { ?>
                                        <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/3?>
                                        <?php $maximo_parcelas = "3x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto->preco > 200.00 && $produto->preco < 300.00) { ?>
                                        <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/5?>
                                        <?php $maximo_parcelas = "5x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php } else if($produto->preco > 300.00) { ?>
                                        <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/6?>
                                        <?php $maximo_parcelas = "6x";
                                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                    <?php }
                                    } ?>
                                    <?php 
                                        if($produto->preco_promo || $desconto_geral > 0) {
                                            echo $this->Html->link("
                                                <h4>".$produto->produto_base->name."</h4>
                                                <p><span class='preco-corte'> R$".number_format($preco_corte, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                $SSlug.'/combo/produto/'.$number.'/'.$produto->slug,
                                                ['escape' => false]);
                                        } else {
                                            echo $this->Html->link("
                                                <h4>".$produto->produto_base->name."</h4>
                                                <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                $SSlug.'/combo/produto/'.$number.'/'.$produto->slug,
                                                ['escape' => false]);
                                        }
                                    ?>
                                </div>
                                <div class="hover-produto <?= $produto->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                                    <div class="col-xs-12">
                                        <div class="hexagono-indicar">
                                            <?php if($produto->status_id == 1) { ?>
                                                <div class="hexagon-disponivel btn-indique" data-id="<?= $produto->id ?>">
                                                    <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                                    <p>indique</p>
                                                </div>
                                                <script>
                                                    $('.btn-indique').click(function () {
                                                            var id_produto = $(this).attr('data-id');
                                                            $('#overlay-indicacao-'+id_produto).height('100%');
                                                            $("html,body").css({"overflow":"hidden"});
                                                    });
                                                    function closeIndica() {
                                                        $('.overlay-indicacao').height('0%');
                                                        $("html,body").css({"overflow":"auto"});
                                                    }
                                                </script>
                                            <?php } else { ?>
                                                <div class="hexagon produto-acabou-btn" data-id="<?= $produto->id ?>">
                                                    <span><i class="fa fa-envelope-o"></i></span>
                                                    <p>avise-me</p>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <?php if($produto->estoque >= 1) { ?>
                                        <div class="col-xs-6">
                                            <?= $this->Html->link('
                                                <div class="hexagon-disponivel">
                                                    <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                    <p>detalhes</p>
                                                </div>'
                                                ,$SSlug.'/combo/produto/'.$number.'/'.$produto->slug,
                                                ['escape' => false]);
                                            ?>
                                        </div>
                                        <div class="col-xs-6 text-center compare">
                                            <div class="hexagon-disponivel btn-comparar-action" data-id="<?= $produto->id ?>">
                                                <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                                <p>comparar</p>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-xs-6">
                                            <?= $this->Html->link('
                                                <div class="hexagon">
                                                    <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                    <p>detalhes</p>
                                                </div>'
                                                ,$SSlug.'/combo/produto/'.$number.'/'.$produto->slug,
                                                ['escape' => false]);
                                            ?>
                                        </div>
                                        <div class="col-xs-6 text-center compare">
                                            <div class="hexagon btn-comparar-action" data-id="<?= $produto->id ?>">
                                                <span><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                                                <p>comparar</p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="botoes-comprar-similiar">
                                <?php if($produto->visivel == 1 && $produto->status_id == 1) { ?>
                                    <?php if($Academia->prazo_medio > 0 && $Academia->prazo_medio <= 72) { ?>
                                        <?php 
                                            if($Academia->prazo_medio <= 12) {
                                                $prazo_medio = 12;
                                            } else if($Academia->prazo_medio > 12 && $Academia->prazo_medio <= 24) {
                                                $prazo_medio = 24;
                                            } else if($Academia->prazo_medio > 24 && $Academia->prazo_medio <= 48) {
                                                $prazo_medio = 48;
                                            } else if($Academia->prazo_medio > 48) {
                                                $prazo_medio = 72;
                                            }
                                        ?>
                                        <p class="texto-prazo"><i class="fa fa-truck" aria-hidden="true"></i> Entrega em até <?= $prazo_medio ?>h</p>
                                    <?php } ?>
                                    <button class="btn btn-comprar-combo btn-incluir" data-id="<?= $produto->id ?>">Incluir no combo</button>
                                    <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $produto->id ?>">Indique</button>
                                    <button class="desktop-hidden btn btn-incluir produto-comparar-btn btn-comparar-action" data-id="<?= $produto->id ?>">Comparar</button>
                                <?php } else if($produto->visivel == 1 && $produto->status_id == 2) { ?>
                                    <?= $this->Html->link('
                                        <button class="btn btn-similares">
                                            <span style="font-size: 18px">Ver Similares</span>
                                            <br>
                                            <span style="font-size: 12px">Esgotado</span>
                                        </button>',
                                        $SSlug.'/similares/'.$produto->slug, 
                                        ['escape' => false]
                                    ); ?>
                                    <button class="desktop-hidden btn btn-similares produto-acabou-btn" data-id="<?= $produto->id ?>">Avise-me</button>
                                    <button class="desktop-hidden btn btn-similares produto-comparar-btn btn-comparar-action" data-id="<?= $produto->id ?>">Compare</button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php foreach($produtos as $produto) { ?>
                    <div id="aviseMe-<?= $produto->id ?>" class="overlay overlay-avise">
                        <div class="overlay-content overlay-avise-<?= $produto->id ?> text-center">
                            <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                            <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                            <br />
                            <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$produto->id])?>
                            <?= $this->Form->input('produto_id', [
                                'div'           => false,
                                'label'         => false,
                                'type'          => 'hidden',
                                'val'           => $produto->id,
                                'id'            => 'produto_id_acabou-'.$produto->id
                            ])?>
                            <?= $this->Form->input('name', [
                                'div'           => false,
                                'label'         => false,
                                'id'            => 'name_acabou-'.$produto->id,
                                'class'         => 'form-control validate[optional]',
                                'placeholder'   => 'Qual o seu nome?',
                            ])?>
                            <br/>
                            <?= $this->Form->input('email', [
                                'div'           => false,
                                'label'         => false,
                                'id'            => 'email_acabou-'.$produto->id,
                                'class'         => 'form-control validate[required, custom[email]]',
                                'placeholder'   => 'Qual o seu email?',
                            ])?>
                            <br/>
                            <?= $this->Form->button('Enviar', [
                                'data-id'       => $produto->id,
                                'type'          => 'button',
                                'class'         => 'btn btn-success send-acabou-btn'
                            ])?>
                            <br />
                            <br />
                            <?= $this->Form->end()?>
                        </div>
                    </div> 
                    <div id="overlay-indicacao-<?= $produto->id ?>" class="overlay overlay-indicacao">
                        <div class="overlay-content text-center">
                            <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                            <h4>Lembrou de alguém quando viu isso?</h4>
                            <h4>Indique este produto!</h4>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 share-whastapp share-buttons">
                                    <a href="whatsapp://send?text=Isso é a sua cara! <?= WEBROOT_URL.$SSlug.'/combo/produto/'.$number.'/'.$produto->slug ?>">
                                        <button class="btn button-blue"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</button>
                                    </a>
                                </div>
                                <div class="col-xs-12 share-whastapp share-buttons">
                                    <button class="btn button-blue" id="share-<?= $produto->id ?>"><i class="fa fa-comment-o" aria-hidden="true"></i> Messenger</button>
                                    <script>
                                        $(document).ready(function() {
                                            $('#share-<?= $produto->id ?>').click(function(){
                                                var link = "<?= WEBROOT_URL.$SSlug.'/combo/produto/'.$number.'/'.$produto->slug ?>";
                                                var app_id = '1321574044525013';
                                                window.open('fb-messenger://share?link=' + link + '&app_id=' + app_id);
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="tablet-hidden col-xs-12 col-md-3 share-buttons">
                                    <button data-id="whatsweb-<?= $produto->id ?>" class="btn button-blue abrir-whats"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp Web</button>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                    <a href="javascript: void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u=<?= WEBROOT_URL.$SSlug.'/combo/produto/'.$number.'/'.$produto->slug ?>','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');">
                                        <button class="btn button-blue fb-share-button"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</button>
                                    </a>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                    <button class="btn button-blue" id="copiar-clipboard-<?= $produto->id ?>"><i class="fa fa-clone" aria-hidden="true"></i> Copiar link</button>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                    <button  class="btn button-blue abrir-email"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail</button>
                                </div>
                            </div>
                            <div class="row">
                                <div id="collapse-whats-<?=$produto->id ?>" class="tablet-hidden enviar-whats">
                                    <div class="col-xs-4 text-center form-group">
                                        <?= $this->Form->input('whats-web-form', [
                                            'div'           => false,
                                            'id'            => 'nro-whatsweb-'.$produto->id,
                                            'label'         => false,
                                            'class'         => 'form-control form-indicar',
                                            'placeholder'   => "Celular*: (00)00000-0000"
                                        ])?>
                                    </div>
                                    <div class="col-xs-4 text-left obs-overlay">
                                        <p>*Para enviar pra mais de um contato deixe este campo vazio.</p>
                                    </div>
                                    <div class="col-xs-4 text-center form-group">
                                        <?= $this->Form->button('Enviar', [
                                        'type'          => 'button',
                                        'data-id'       => $produto->id,
                                        'class'         => 'btn button-blue',
                                        'id'            => 'enviar-whatsweb-'.$produto->id
                                        ])?>
                                    </div>
                                    <script>
                                        $(document).ready(function() {
                                            $('#enviar-whatsweb-<?=$produto->id ?>').click(function(){

                                                var numero;
                                                var link = "<?= WEBROOT_URL.$SSlug.'/combo/produto/'.$number.'/'.$produto->slug ?>";

                                                if($('#nro-whatsweb-<?= $produto->id ?>').val() != '') {
                                                    numero = '55'+$('#nro-whatsweb-<?= $produto->id ?>').val();
                                                }
                                                
                                                window.open('https://api.whatsapp.com/send?phone=' + numero + '&text=Isso é a sua cara! ' + link);
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="col-xs-12 enviar-email" id="collapse-email-<?=$produto->id ?>">
                                    <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$produto->id])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[optional] form-indicar name-'.$produto->id,
                                        'placeholder'   => 'Quem indicou?',
                                    ])?>
                                    <br>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[required] form-indicar email-'.$produto->id,
                                        'placeholder'   => 'E-mail do destinatário',
                                    ])?>
                                    <?= $this->Form->input('id_produto', [
                                        'div'           => false,
                                        'class'         => 'id_produto-'.$produto->id,
                                        'value'         => $produto->id,
                                        'label'         => false,
                                        'type'          => 'hidden'
                                    ])?>
                                    <br/>
                                    <?= $this->Form->button('Enviar', [
                                        'id'            => 'send-indicacao-btn',
                                        'type'          => 'button',
                                        'data-id'       => $produto->id,
                                        'class'         => 'btn button-blue send-indicacao-home-btn'
                                    ])?>
                                    <br />
                                    <?= $this->Form->end()?>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-xs-12 clipboard">   
                                    <?= $this->Form->input('url', [
                                        'div'           => false,
                                        'id'            => 'url-clipboard-'.$produto->id,
                                        'label'         => false,
                                        'value'         => WEBROOT_URL.$SSlug.'/combo/produto/'.$number.'/'.$produto->slug,
                                        'readonly'      => true,
                                        'class'         => 'form-control form-indicar'
                                    ])?>
                                </div>  
                                <p><span class="copy-alert hide" id="copy-alert-<?=$produto->id ?>">Copiado!</span></p>
                                <script>
                                    $(document).ready(function() {
                                        // Copy to clipboard 
                                        document.querySelector("#copiar-clipboard-<?=$produto->id ?>").onclick = () => {
                                          // Select the content
                                            document.querySelector("#url-clipboard-<?=$produto->id ?>").select();
                                            // Copy to the clipboard
                                            document.execCommand('copy');
                                            // Exibe span de sucesso
                                            $('#copy-alert-<?=$produto->id ?>').removeClass("hide");
                                            // Oculta span de sucesso
                                            setTimeout(function() {
                                                $('#copy-alert-<?=$produto->id ?>').addClass("hide");
                                            }, 1500);
                                        };
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script> 
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            autoplay:true,
            autoplayTimeout:600,
            autoplayHoverPause:true,
            pagination: false,
            margin: 10,
            navigation: true,
            navigationText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
            itemsMobile: [600, 1],
            itemsDesktopSmall: [1024, 2],
            itemsDesktop: [6000, 3]
        });
    });
</script>

<script type="text/javascript">
    $('.send-indicacao-home-btn').click(function(e) {
        e.preventDefault();
        var botao = $(this);
    
        botao.html('Enviando...');
        botao.attr('disabled', 'disabled');

        var id = $(this).attr('data-id');
        var slug = '<?= $SSlug ?>';

        var valid = $('.indicar_prod_home_'+id).validationEngine("validate");
        if (valid == true) {
            $.ajax({
                type: "POST",
                url: WEBROOT_URL + slug + '/produtos/overlay-indicar-produtos-home/',
                data: {
                    name: $('.name-'+id).val(),
                    email: $('.email-'+id).val(), 
                    produto_id: $('.id_produto-'+id).val()
                }
            })
            .done(function (data) {
                if(data == 1) {
                    botao.html('Enviado!');
                    setTimeout(function () {
                        botao.html('Enviar');
                        botao.removeAttr('disabled');
                    }, 2200);
                } else {
                    botao.html('Falha ao enviar... Tente novamente...');
                    setTimeout(function () {
                        botao.html('Enviar');
                        botao.removeAttr('disabled');
                    }, 2200);
                }
            });
        }else{
            $('.indicar_prod_home_'+id).validationEngine({
                updatePromptsPosition: true,
                promptPosition: 'inline',
                scroll: false
            });
        }
    });

    $('.abrir-email').click(function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('.enviar-whats').slideUp("slow");
        $('.enviar-email').slideToggle("slow");
    });
    $('.abrir-whats').click(function(e){
        var id = $(this).attr('data-id');
        $('.enviar-email').slideUp("slow");
        $('.enviar-whats').slideToggle("slow");
    });

    $(document).ready(function() {
        var atualizando = 0;

        //CARREGAR MAIS PRODUTOS
        $(window).scroll(function() {
            var posicao_pagina = $(document).scrollTop();
            var id_produtos = [];

            if(posicao_pagina + $(window).height() >= ($(document).height() - 800) && atualizando == 0) {
                atualizando = 1;

                $('.box-completa-produto').each(function() {
                  id_produtos.push($( this ).attr('data-id'));
                });

                $.ajax({
                    type: "POST",
                    url: WEBROOT_URL + SSLUG + '/atualizar-produtos-combo/<?= $number ?>',
                    data: {
                        produtos_ids: id_produtos
                    }
                })
                    .done(function (data) {
                        $('.produtos-scroll').append(data);
                        setTimeout(function() {
                            atualizando = 0;
                        }, 1200);
                    });
            }
        });

        $(document).on('click', '.btn-comprar-combo', function() {
            var id = $(this).attr('data-id');

            location.href = WEBROOT_URL + SSLUG + '/salvar-combo/<?= $number ?>/' + id;
        });
    });
</script>