<script type="text/javascript">
	$('.aplicar-cupom').click(function() {
    $.ajax({
        type: "POST",
        url: WEBROOT_URL + '/produtos/check-cupom/',
        data: {codigo: $('.cupom-value').val()}
    })
        .done(function (data) {
            $('.msg-cupom').remove();

            $('.area-cupom').html(data);

            var subtotal = 0;

            $('.total-produto').each(function(){
                subtotal = parseFloat(subtotal + parseFloat($(this).attr('data-total')));
            });

            var valor_cupom = $('.cupom-value').attr('data-id');
            var tipo_cupom = $('.cupom-value').attr('tipo-id');
            var desconto = 1.0 - (valor_cupom * .01);

            if(valor_cupom != 0) {
                if(tipo_cupom == 1) {
                    total = subtotal * desconto;
                } else {
                    total = subtotal - valor_cupom;
                }
            } else {
                total = subtotal;
            }

            $('#carrinho-total').html('valor total: <span>R$'+total.toFixed(2).replace('.', ',')+'</span>');
        });
	});
</script>

<?php if($usado == 1 || count($cupom_desconto) <= 0) { ?>
    <input type="text" class="cupom-value" data-id="0" placeholder=" cupom inválido!">
<?php } else { ?>
    <input type="text" class="cupom-value" data-id="<?= $cupom_desconto->valor ?>" tipo-id="<?= $cupom_desconto->tipo ?>" placeholder=" cupom válido!">
<?php } ?>
<button class="aplicar-cupom">aplicar</button>