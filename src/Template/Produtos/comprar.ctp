<style type="text/css">
    .box-close-mochila {
        width: 0;
    }
    .mochila {
        width: 230px;
        opacity: 1;
    }
    .fundo-carrinho {
        margin-left: 0;
    }
    .cupom-desconto, 
    .total-compra, 
    .btn-fecha-mochila {
        right: 0px;
    }
</style>

<?php if($alerta_inativo == 1) { ?>
    <script>
    $('.produto-verifica').append('<div class="erro-inativo" style="color: red">Um dos produtos não está disponível.</div>');
    </script>
<?php } else { ?>
    <script>
        $('.erro-inativo').remove();
    </script>
<?php } ?>

<script type="text/javascript">
    //atualiza contador de produtos da mochila
    function atualizar_contador() {
        var contador = 0;

        $('.produto-qtd').each(function() {
            contador = contador + parseInt($(this).html());
        });

        $('#contador_mochila').html(contador);
    }

    atualizar_contador();

    //atualiza valor total da mochila
    subtotal = $('#carrinho-total').attr('data-total');

    if($('.cupom-value').attr('data-id') == 1) {
        total = subtotal * 0.8;
    } else {
        total = subtotal;
    }

    total = parseFloat(total);

    $('#carrinho-total').html('valor total: <span>R$'+total.toFixed(2).replace('.', ',')+'</span>');

    //abrir mochila
    $('#abrir-mochila').click(function(){
        $('.cobertura-loja').css('display', 'block');
        setTimeout(function() {
            $('.cobertura-loja').animate({'opacity' : '1'}, 700);
            $('.lista-produtos-mochila').animate({'opacity' : '1'}, 500);
            $('.mochila-nova').animate({'width' : '300px'}, 500);
            $("body,html").addClass('total-block');
            setTimeout(function() {
                $('.lista-produtos-mochila').animate({'opacity' : '1'}, 200);
            }, 500);
        });

        if (total.toFixed(2) >= 99.0) {
            console.log(total);
            $('.proximo-passo .botoes-mochila-sub').html('pagamento');
            $('.proximo-passo button').prop('disabled', false);
            $('.proximo-passo button').removeClass('inativo');
        } else {
            $('.proximo-passo .botoes-mochila-sub').html('*valor mínimo: R$99');
            $('.proximo-passo button').prop('disabled', true);
            $('.proximo-passo button').addClass('inativo');
        }
    });

    //continuar comprando(fechar mochila)
    $('#continuar-comprando, .cobertura-loja').click(function() {
        $('.lista-produtos-mochila').animate({'opacity' : '0'}, 200);
        setTimeout(function() {
            $('.mochila-nova').animate({'width' : '0'}, 400);
            setTimeout(function() {
                $('.cobertura-loja').animate({'opacity' : '0'}, 700);
                setTimeout(function() {
                    $('.cobertura-loja').css('display', 'none');
                    $("body,html").removeClass('total-block');
                }, 500);
            }, 500);
        }, 300);
    });

    //verificar se o valor total é maior que 0 antes de finalizar a mochila
    $('.finalizar-mochila').click(function() {
        var valor_total = $('#carrinho-total').attr('data-total');
        if(valor_total < 99.0) {
            return false;
        }
    });

    /**
     * CARRINHO - EXCLUI PRODUTO
     */
    $('.lixo').click(function(e){
        e.preventDefault();
        var produto_id  = $(this).attr('data-id');
        var produto     = $('#produto-' + produto_id);
        if(produto.html() > 0){
            produto.html(parseInt(produto.html()) - parseInt(produto.html()));
            var quantidade  = (parseInt(produto.html()));

            calc_produto_carrinho(produto_id, 'subtracao');
            carrinho_edit(produto_id, quantidade);
        }
        $('#fundo-'+produto_id).remove();
        atualizar_contador();
    });

    /**
     * CARRINHO - DIMINUIR QUANTIDADE
     */
    $('.produto-menos').click(function(){
        var produto_id  = $(this).attr('data-id');
        var produto     = $('#produto-' + produto_id);
        $('.error-qtd').remove();
        if(produto.html() == 1) {
            $('#fundo-'+produto_id).remove();
        }
        if(produto.html() > 0){
            produto.html((parseInt(produto.html()) - 1));
            var quantidade  = (parseInt(produto.html()));

            calc_produto_carrinho(produto_id, 'subtracao');
            carrinho_edit(produto_id, quantidade);
        } 
        atualizar_contador();
    });

    /**
     * CARRINHO - AUMENTAR QUANTIDADE
     */
    $('.produto-mais').click(function(event){
        event.stopPropagation();
        var produto_id  = $(this).attr('data-id');
        var estoque     = $(this).attr('eid');
        var produto     = $('#produto-' + produto_id);
        var quantidade  = (parseInt(produto.html()) + 1);
        $('.error-qtd').remove();
        
        if(quantidade <= estoque) {
            produto.html(quantidade);

            calc_produto_carrinho(produto_id, 'soma');
            carrinho_edit(produto_id, quantidade);
        } else {
            $(this).parent().append('<i class="fa fa-exclamation-circle error-qtd" aria-hidden="true" title="esses são os últimos, em breve vamos colocar mais na prateleira!"></i>');

            $('.error-qtd').click(function(event) {
                event.stopPropagation();
            });

            if($('#expandir-mochila').hasClass('seta-animate-abrir')) {
                $('.error-qtd').addClass('error-qtd-expanded');
            }
        }
        atualizar_contador();
    });

    $('.error-qtd').click(function(event) {
        event.stopPropagation();
    });

    $('.aplicar-cupom').click(function() {
        $.ajax({
            type: "POST",
            url: WEBROOT_URL + '/produtos/check-cupom/',
            data: {codigo: $('.cupom-value').val()}
        })
            .done(function (data) {
                $('.area-cupom').html(data);

                var subtotal = 0;

                $('.total-produto').each(function(){
                    subtotal = parseFloat(subtotal + parseFloat($(this).attr('data-total')));
                });

                var valor_cupom = $('.cupom-value').attr('data-id');
                var desconto = 1.0 - (valor_cupom * .01);

                if(valor_cupom != 0) {
                    total = subtotal * desconto;
                } else {
                    total = subtotal;
                }

                $('#carrinho-total').html('valor total: <span>R$'+total.toFixed(2).replace('.', ',')+'</span>');

                if(total == 0){
                    $('.btn-fecha-mochila').attr('disabled', true);
                }else{
                    $('.btn-fecha-mochila').attr('disabled', false);
                }
            });
    });

    /**
     * CARRINHO - CALCULO VALOR TOTAL PRODUTO
     * @param id
     * @param operacao
     */
    function calc_produto_carrinho(id, operacao){

        var element = $('#produto-' + id);
        var preco_unitario  = element.attr('data-preco');
        var quantidade      = parseInt(element.html());

        var preco_total     = parseFloat((preco_unitario * quantidade));

        $('#total-produto-' + id)
            .html('R$ ' + preco_total.toFixed(2).replace('.', ','))
            .attr('data-total', preco_total.toFixed(2));

        var subtotal = 0;
        $('.total-produto').each(function(){
            subtotal = parseFloat(subtotal + parseFloat($(this).attr('data-total')));
        });

        var total = 0.0;
        var valor_cupom = $('.cupom-value').attr('data-id');
        var tipo_cupom = $('.cupom-value').attr('tipo-id');
        var desconto = 1.0 - (valor_cupom * .01);

        var url_atual = window.location.href;
        var express_url = url_atual.split("/");

        if(express_url[3] == 'academia' && express_url[4] == 'admin') {
            total = subtotal;
        } else {
            if(valor_cupom != 0) {
                if(tipo_cupom == 1) {
                    total = subtotal * desconto;
                } else {
                    total = subtotal - valor_cupom;
                }
            } else {
                total = subtotal;
            }
        }

        if (total.toFixed(2) >= 99.0) {
            console.log(total);
            $('.proximo-passo .botoes-mochila-sub').html('pagamento');
            $('.proximo-passo button').prop('disabled', false);
            $('.proximo-passo button').removeClass('inativo');
        } else {
            $('.proximo-passo .botoes-mochila-sub').html('*valor mínimo: R$99');
            $('.proximo-passo button').prop('disabled', true);
            $('.proximo-passo button').addClass('inativo');
        }

        $('#carrinho-total')
            .html('valor total: <span>R$'+total.toFixed(2).replace('.', ',')+'</span>')
            .attr('data-total', total.toFixed(2));

        if(express_url[3] == 'academia' && express_url[4] == 'admin') {
            if(total.toFixed(2) < 1000){
                $('.btn-fecha-mochila').prop("disabled",true);
            }else{
                $('.btn-fecha-mochila').prop("disabled",false);
            }
        }
    }

    /**
     * CARRINHO - ALTERAR ITENS
     * @param id
     * @param qtd
     */
    function carrinho_edit(id, qtd){
        $.get(WEBROOT_URL + '/mochila/alterar/' + id + '/' + qtd,
            function(data){
                console.log('Itens alterados...');
            });
    }
</script>

<div class="botoes-mochila continuar-comprando">
    <button id="continuar-comprando" class="button">
        <div class="botoes-mochila-titulo">continuar comprando</div>
        <div class="botoes-mochila-sub">colocar mais itens</div>
    </button>
</div>
<div class="lista-produtos-mochila">
    <?php foreach ($s_produtos as $s_produto) {
        $fotos = unserialize($s_produto->fotos);
        $s_produto->preco_promo ? $preco = $s_produto->preco_promo * $desconto_geral : $preco = $s_produto->preco * $desconto_geral;
        ?>

        <div id="fundo-<?= $s_produto->id ?>" class="produto-mochila">
            <div class="produto-imagem">
                <?= $this->Html->link(
                    $this->Html->image($fotos[0],
                        ['alt' => $s_produto->produto_base->name.' '.$s_produto->propriedade]
                    ),
                    $SSlug.'/produto/'.$s_produto->slug,
                    ['escape' => false]
                ) ?>
                <?= $this->Html->link('<div class="tarja-detalhes">+ detalhes</div>',
                    $SSlug.'/produto/'.$s_produto->slug,
                    ['escape' => false]
                ) ?>
            </div>
            <div class="produto-detalhes">
                <?= $this->Html->link('<p title="'.$s_produto->produto_base->name.'"><strong>'.$s_produto->produto_base->name.'</strong></p>',
                    $SSlug.'/produto/'.$s_produto->slug,
                    ['escape' => false]
                ) ?>
                
                <p class="sabor-produto"><?= $s_produto->propriedade ?></p>
                <p class="tamanho-produto"><?= $s_produto->tamanho.' '.$s_produto->unidade_medida ?></p>
                <div class="conjunto-botoes">
                    <input type="button" value='-' class="produto-menos btn-quantidade" data-id="<?= $s_produto->id ?>"/>
                    <span class="text produto-qtd quantidade-produtos" id="produto-<?= $s_produto->id ?>" data-preco="<?= number_format($preco, 2, '.', ''); ?>"><?= $session_produto[$s_produto->id]; ?></span>
                    <input type="button" value='+' class="produto-mais btn-quantidade" data-id="<?= $s_produto->id ?>" eid="<?= $s_produto->estoque ?>"/>
                    <span id="total-produto-<?= $s_produto->id ?>" class="total-quantidade total-produto" data-total="<?= number_format(($preco * $session_produto[$s_produto->id]), 2, '.', '') ?>">R$ <?= number_format($total_produto = ($preco * $session_produto[$s_produto->id]), 2, ',', '.') ?></span>
                    <?php $subtotal += $total_produto; ?>
                </div>
                <p><a class="lixo" href="#" data-id="<?= $s_produto->id ?>"><span class="btn-remover"><span class="fa fa-trash" style="color: darkgray"></span> remover</span></a></p>
            </div>
        </div>
    <?php } ?>
</div>

<div class="rodape-mochila">
    <div class="area-cupom">
        <?php if(count($cupom_desconto) > 0) { ?>
            <input type="text" class="cupom-value" data-id="<?= $cupom_desconto->valor ?>" tipo-id="<?= $cupom_desconto->tipo ?>" placeholder=" cupom válido!">
        <?php } else { ?>
            <input type="text" class="cupom-value" data-id="<?= $cupom_desconto->valor ?>" tipo-id="<?= $cupom_desconto->tipo ?>" placeholder=" insira o cupom">
        <?php } ?>
        <button class="aplicar-cupom">aplicar</button>
    </div>
    <div class="valor-total-compra text-left">
        <h4>frete: <span>R$0,00</span></h4>
    </div>
    <div class="valor-total-compra text-left">
        <?php if($cupom_desconto) { ?>
            <?php $desconto = 1.0 - ($cupom_desconto->valor * .01); $subtotal_desc = $subtotal * $desconto; ?>
            <h4 id="carrinho-total" data-total="<?= number_format($subtotal_desc, 2, '.', '')?>">valor total: <span>R$<?= number_format($subtotal_desc, 2, ',', '.')?></span></h4>
        <?php } else { ?>
            <h4 id="carrinho-total" data-total="<?= number_format($subtotal, 2, '.', '')?>">valor total: <span>R$<?= number_format($subtotal, 2, ',', '.')?></span></h4>
        <?php } ?>
    </div>
    <div class="botoes-mochila proximo-passo">
        <?= $this->Html->link(
            '<button class="button">
                <div class="botoes-mochila-titulo">próximo passo</div>
                <div class="botoes-mochila-sub">pagamento</div>
            </button>',
            $SSlug.'/mochila/fechar-o-ziper',
            ['escape' => false, 'class' => 'finalizar-mochila']
        ) ?>
    </div>
</div>

<script>
    console.log('<?= $SSlug ?>');
</script>