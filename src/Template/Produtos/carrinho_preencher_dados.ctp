<style type="text/css">
    #logexpress-btn{
        display: none!important;
    }
    .minha-conta-container {
        padding-left: 200px;
        padding-top: 70px;
        margin-left: 0;
        margin-right: 0;
        width: 100%;
    }
    .voltar-page {
        float: right;
        margin-top: 50px;
        margin-right: 15px;
        font-size: 18px;
    }
    .second-step {
        display: none;
    }
    .logo-academia-style {
        max-width: 95%;
    }
    .logo-loja{
        max-height: 80px;
        width: auto!important;
    }
    .box-completo{
        border-color: rgba(80, 137, 207, 1.0);
        padding: 15px;
        display: inline-block;
    }
    .box-logo-academia {
        align-items: center;
        justify-content: center;
        background: white;
        height: 90px;
    }
    .box-logo-academia img{
        max-width: 95%;
        max-height: 80px;
    }
    .box-dados-academia {
        align-items: center;
        justify-content: center;
        min-height: 85px;
        padding: 5px 2px;
        font-size: 18px;
    }
    .btn-professor {
        border: 2px solid;
        border-color: #5087c7;
        background-color: white;
        font-family: nexa_boldregular;
        color: #5087c7;
    }
    .btn-continuar{
        text-align: center;

    }
    .btn-continuar-entrega{
        margin-top: -76px;
    }

    .check-confirmar-academia{
        margin-top: 51px;

    }

    .select-indicacao{
        margin-top: 40px;
    }
    .form-entrega{

    }
    .local-entrega{
        margin-bottom: 32px;
        font-size: 28px;
        color: #5087C7;
        font-weight: bold;
    }
    .input-entrega {
        width: 100%;
        height: 35px;
        margin: 2px auto;
    }
    .box-entrega{
        width: 80%;
    }
    .button-cadastrar{
        margin-right: 0;
    }
    .input-cadastro{
        width: 100%;
    }

    /* novas tabs */

/*  bhoechie tab */
    div.bhoechie-tab-container{
        z-index: 10;
        position: initial;
        background-color: #ffffff;
        padding: 0 !important;
        border-radius: 4px;
        -moz-border-radius: 4px;
        border:1px solid #ddd;
        margin-top: 20px;
        margin-left: 50px;
        -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        box-shadow: 0 6px 12px rgba(0,0,0,.175);
        -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        background-clip: padding-box;
        opacity: 0.97;
        filter: alpha(opacity=97);
    }
    div.bhoechie-tab-menu{
        padding-right: 0;
        padding-left: 0;
        padding-bottom: 0;
    }
    div.bhoechie-tab-menu div.list-group{
        margin-bottom: 0;
    }
    div.bhoechie-tab-menu div.list-group>a{
        margin-bottom: 0;
    }
    div.bhoechie-tab-menu div.list-group>a .glyphicon,
    div.bhoechie-tab-menu div.list-group>a .fa {
        color: #5A55A3;
    }
    div.bhoechie-tab-menu div.list-group>a:first-child{
        border-top-right-radius: 0;
        -moz-border-top-right-radius: 0;
    }
    div.bhoechie-tab-menu div.list-group>a:last-child{
        border-bottom-right-radius: 0;
        -moz-border-bottom-right-radius: 0;
    }
    div.bhoechie-tab-menu div.list-group>a.active,
    div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
    div.bhoechie-tab-menu div.list-group>a.active .fa{
        background-color: #5A55A3;
        background-image: #5A55A3;
        color: #ffffff;
    }
    div.bhoechie-tab-menu div.list-group>a.active:after{
        content: '';
        position: absolute;
        left: 100%;
        top: 50%;
        margin-top: -13px;
        border-left: 0;
        border-bottom: 13px solid transparent;
        border-top: 13px solid transparent;
        border-left: 10px solid #3574aa;
    }

    div.bhoechie-tab-content{
        background-color: #ffffff;
    /* border: 1px solid #eeeeee; */
        padding-left: 20px;
        padding-top: 10px;
    }

    div.bhoechie-tab div.bhoechie-tab-content:not(.active){
        display: none;
    }
    /* novas tabs */

    .entrega-font{
        font-size: 28px;
        color: #5087C7;
        font-weight: bold;
    }
    .sumir-desk{
        padding-left: 0;
        padding-right: 0;
        margin-top: 20px;
        border: 1px solid #ddd;
    }
    .nav>li>a>img{
        width: 32px;
    }
    .footer-total{
        display: none;
    }

    @media all and (max-width: 1279px) {
        .minha-conta-container {
            padding-right: 70px;
            padding-left: 70px;
            margin-top: -10px;
        }
    }
    
    @media all and (max-width: 430px) {
        .minha-conta-container {
            padding-right: 0;
            padding-left: 0;
        }
        .sumir-mobile{
            display: none;
        }
        .minha-conta-container {
            padding-top: 60px;
        }
        .check-confirmar-academia{
            margin-top: 10px;
        }
    }

    @media all and (min-width: 768px) {
        .sumir-desk{
            display: none;
            padding-left: 0;
            padding-right: 0;
            margin-top: 20px;
            
        }
        .input-cadastro{
            
        }
        .input-entrega
    }

    @media all and (min-width: 1024px) {
        .sumir-desk{
            display: none;
            padding-left: 0;
            padding-right: 0;
            margin-top: 20px;
            
        }
        div.bhoechie-tab-container{
            margin-left: 10%;
        }
    }

        @media all and (min-width: 1400px) {
        .minha-conta-container{
            padding-left: 230px;
            
        }
        div.bhoechie-tab-container{
            margin-left: 10%;
        }
    }

        .sem-acad{
            padding: 53px 15px;
        }

        li.active:after{
            content: '';
            position: absolute;
            left: 45%;
            top: 108%;
            margin-top: -13px;
            border-left: 0;
            border-right: 13px solid transparent;
            border-top: 13px solid rgb(51, 122, 183);
            border-left: 10px solid transparent;
        }

        .centralizar-botoes-tab{
            position: relative;
            display: block;
            padding: 10px 8px !important;
            border: 1px solid #ddd;
            border-right: none;
        }
        .alterar-li{
            width: 33%;
            padding-left: 0 !important;
            margin-left: 0 !important;
            display: table-cell !important;
        }



</style>
<!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
<!-- <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script> -->
<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->


<div class="container minha-conta-container">
    <div class="col-xs-12 text-center">
        <?= $this->Form->create($cliente, ['default' => false, 'class' => 'form-preencher-dados'])?> 
        <div class="row">
            <br>
            <?php if ($DescontoCombo != null) { ?>
                <div class="col-xs-12">
                    <h3>Atenção: Se você fechar esta página sem confirmar seus dados será necessário montar seu combo novamente!</h3>
                </div>
            <?php } ?>
            <br>
            <div class="col-xs-12">
                <div class="first-step">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-md-offset-3">
                            <h4>Quem está comprando?</h4>
                            <div class="form-group row">
                                <!-- Dados ocultos -->
                                <div class="col-sm-12">
                                    <?= $this->Form->input('uf_id', [
                                        'value'         => $academia_slug->city->state_id,
                                        'type'          => 'hidden'
                                    ]) ?>
                                </div>
                                <div class="col-sm-12" >
                                    <?= $this->Form->input('city_id', [
                                        'value'         => $academia_slug->city_id,
                                        'type'          => 'hidden'
                                    ]) ?>
                                </div>
                                <div class="col-sm-12" >
                                    <?= $this->Form->input('academia_id', [
                                        'value'         => $academia_slug->id,
                                        'class'         => 'academia_id_hidden',
                                        'type'          => 'hidden'
                                    ])?>
                                </div>
                                <div class="col-sm-12" >
                                    <?= $this->Form->input('retirada_loja', [
                                        'value'         => 0,
                                        'class'         => 'retirada_loja_hidden',
                                        'type'          => 'hidden'
                                    ])?>
                                </div>
                                <!-- /Dados ocultos -->

                                <?php if($cliente->cpf) { ?>
                                    <div class="col-sm-12">
                                        <?= $this->Form->input('cpf', [
                                            'div'           => false,
                                            'label'         => false,
                                            'required'      => true,
                                            'readonly'      => true,
                                            'value'         => $cliente->cpf,
                                            'id'            => 'cpf-compra-rapida',
                                            'class'         => 'input-cadastro form-control cpf cpf-search',
                                            'placeholder'   => 'CPF',
                                        ])?>
                                    </div> 
                                <?php } else { ?>
                                    <div class="col-sm-12">
                                        <?= $this->Form->input('cpf', [
                                            'div'           => false,
                                            'label'         => false,
                                            'required'      => true,
                                            'id'            => 'cpf-compra-rapida',
                                            'class'         => 'input-cadastro form-control cpf cpf-search',
                                            'placeholder'   => 'CPF',
                                        ])?>
                                    </div> 
                                <?php } ?>

                                <?php if($cliente->email) { ?>
                                    <div class="col-sm-12">
                                        <?= $this->Form->input('email', [
                                            'div'           => false,
                                            'label'         => false,
                                            'required'      => true,
                                            'readonly'      => true,
                                            'type'          => 'email',
                                            'value'         => $cliente->email,
                                            'class'         => 'input-cadastro form-control validate[required, custom[email]] email-response',
                                            'placeholder'   => 'EMAIL',
                                        ])?>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-sm-12">
                                        <?= $this->Form->input('email', [
                                            'div'           => false,
                                            'label'         => false,
                                            'required'      => true,
                                            'type'          => 'email',
                                            'class'         => 'input-cadastro form-control validate[required, custom[email]] email-response',
                                            'placeholder'   => 'EMAIL',
                                        ])?>
                                    </div>
                                <?php } ?>

                                <?php if($cliente->name) { ?>
                                    <div class="col-sm-12">
                                        <?= $this->Form->input('name', [
                                            'div'           => false,
                                            'label'         => false,
                                            'required'      => true,
                                            'readonly'      => true,
                                            'value'         => $cliente->name,
                                            'class'         => 'input-cadastro form-control validate[required] name-response',
                                            'placeholder'   => 'NOME COMPLETO',
                                        ])?>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-sm-12">
                                        <?= $this->Form->input('name', [
                                            'div'           => false,
                                            'label'         => false,
                                            'required'      => true,
                                            'class'         => 'input-cadastro form-control validate[required] name-response',
                                            'placeholder'   => 'NOME COMPLETO',
                                        ])?>
                                    </div>
                                <?php } ?>

                                <?php if($cliente->telephone || $cliente->mobile) { ?>
                                    <div class="col-sm-12">
                                        <?= $this->Form->input('telephone', [
                                            'div'           => false,
                                            'label'         => false,
                                            'required'      => true,
                                            'readonly'      => true,
                                            'value'         => $cliente->telephone ? $cliente->telephone : $cliente->mobile,
                                            'class'         => 'input-cadastro form-control validate[required] phone-mask tel-response',
                                            'placeholder'   => 'TELEFONE',
                                        ])?>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-sm-12">
                                        <?= $this->Form->input('telephone', [
                                            'div'           => false,
                                            'label'         => false,
                                            'required'      => true,
                                            'class'         => 'input-cadastro form-control validate[required] phone-mask tel-response',
                                            'placeholder'   => 'TELEFONE',
                                        ])?>
                                    </div>
                                <?php } ?>

                                <div class="col-sm-12 text-center">
                                    <?= $this->Form->button('continuar', [
                                        'type'          => 'button',
                                        'class'         => 'button-cadastrar first-step-btn',
                                        'style'         => 'margin-bottom: 15px',
                                    ])?>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
                
        <div class="second-step">
            <h4 class="entrega-font">Escolha onde deseja retirar o produto</h4>
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-lg-offset-2 col-md-7 col-md-offset-1 col-sm-8 col-xs-9 bhoechie-tab-container sumir-mobile">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                        <div class="list-group">
                            
                            <?php if($academia_slug->id != 601) { ?> <!--box indicação (perfil)-->
                            <a href="#" class="list-group-item text-center">
                            <h4><img src="/img/stronghest.png" alt="">
                            <br><br>
                            Academia
                            </h4>
                            </a>
                            <?php } ?>

                            <?php if($academia_slug->id != 601) { ?>
                            <a href="#" class="list-group-item active text-center">
                            <h4><img src="/img/online-store.png" alt="">
                            <br/><br/>
                            Loja
                            </h4>
                            </a>
                            <?php } else { ?>
                            <a href="#" class="list-group-item active text-center sem-acad">
                            <h4><img src="/img/online-store.png" alt="">
                            <br/><br/>
                            Loja
                            </h4>
                            </a>
                            <?php } ?>


                            <?php if($academia_slug->id != 601) { ?>
                            <a href="#" class="list-group-item text-center teste-desk">
                            <h4><img src="/img/trucking.png" alt="">
                            <br/><br/>
                            Personalizado
                            </h4>
                            </a>
                            <?php } else { ?>
                            <a href="#" class="list-group-item text-center sem-acad teste-desk">
                            <h4><img src="/img/trucking.png" alt="">
                            <br/><br/>
                            Personalizado
                            </h4>
                            </a>
                            <?php } ?>

                        </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                            <!-- academia section -->
                            <?php if($academia_slug->id != 601) { ?> <!--box indicação (perfil)-->
                            <div class="bhoechie-tab-content">
                                <center>


                                
                                <div class="col-xs-12 col-md-12">
                                    <div class="box-completo">
                                        <div class="col-sm-12">
                                            <div class="col-xs-12 text-center box-logo-academia">
                                                <?= $this->Html->image('academias/'.$academia_slug->image, [
                                                    'class' => 'logo-academia-style',
                                                    'alt'   => 'academia '.$academia_slug->shortname,
                                                ])?>
                                            </div>
                                            <div class="col-sm-12 text-center box-dados-academia">
                                                <div class="row">
                                                    <div class="col-sm-12 text-left">
                                                        <?= $academia_slug->shortname ?>
                                                    </div>
                                                    <div class="col-sm-12 text-left">
                                                        <?= $academia_slug->address ?>, <?= $academia_slug->number ?> - <?= $academia_slug->area ?>
                                                    </div>
                                                    <div class="col-sm-12 text-left">
                                                        <?= $academia_slug->city->name ?> - <?= $academia_slug->city->uf ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="col-sm-12 check-confirmar-academia">
                                        </div>

                                        <div class="col-sm-12 text-center btn-continuar">
                                            <?= $this->Form->button('continuar', [
                                                'id'            => 'btn-submit-cadastro-main',
                                                'type'          => 'submit',
                                                'data-id'       => 10,
                                                'class'         => 'button-cadastrar btn-preencher-dados desk',
                                                'style'         => 'margin-bottom: 15px',
                                            ])?>
                                        </div>
                                    </div>
                                </div>
                                 <!--/box indicação (perfil)-->

                                </center>
                            </div>
                            <?php } ?>
                            <!-- loja section -->
                            <div class="bhoechie-tab-content active">
                                <center>
                                    

                            <?php foreach($lojas as $loja) { ?> <!--box loja de suplementos-->
                                <div class="col-xs-12 com-md-12 <?= $academia_slug->id != 601 ? 'com-acad' : 'sem-acad' ?>">
                                    <div class="box-completo">
                                    
                                            <div class="col-xs-12 text-center box-logo-academia">
                                                <?= $this->Html->image('academias/'.$loja->image, [
                                                    'class' => 'logo-academia-style',
                                                    'alt'   => 'academia '.$loja->shortname,
                                                ])?>
                                            </div>
                                    
                                        <div class="col-sm-12">

                                            <div class="col-sm-12 text-center box-dados-academia">
                                                <div class="row">
                                                    <div class="col-sm-12 text-left">
                                                        <?= $loja->shortname ?>
                                                    </div>
                                                    <div class="col-sm-12 text-left">
                                                        <?= $loja->address ?>, <?= $loja->number ?> - <?= $loja->area ?>
                                                    </div>
                                                    <div class="col-sm-12 text-left">
                                                        <?= $loja->city->name ?> - <?= $loja->city->uf ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="col-sm-12" style="margin-top: 37px">
                                        </div>



                                        <div class="col-sm-12 text-center btn-continuar">
                                            <?= $this->Form->button('continuar', [
                                                'id'            => 'btn-submit-cadastro-main',
                                                'type'          => 'submit',
                                                'data-id'       => $loja->id,
                                                'class'         => 'button-cadastrar btn-preencher-dados desk',
                                            ])?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?> <!--/box loja de suplementos-->



                                </center>
                            </div>
                
                            <!-- personalizado section -->
                            <div class="bhoechie-tab-content">
                                <center>

                                    
                            <div class="col-12 col-md-12"><!--box formulário de entrega-->
                                <div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">

                                    </div>
                                </div>
                                    <div class="col-sm-12">

                                        <div class="col-sm-12 text-center box-dados-academia">
                                        <h4 class="local-entrega">Local de entrega</h4>
                                            <div class="form-group">

                                                <div class="col-sm-12">
                                                    <?= $this->Form->input('cep', [
                                                        'div'           => false,
                                                        'label'         => false,
                                                        'required'      => true,
                                                        'id'            => 'cep',
                                                        'class'         => 'input-entrega form-control cep-response input-desk cep',
                                                        'value'         =>  $cliente->cep,
                                                        'placeholder'   => 'CEP',
                                                        'disabled'      => true,
                                                    ])?>
                                                </div> 

                                                <div class="col-sm-12">
                                                    <?= $this->Form->input('address', [
                                                        'div'           => false,
                                                        'label'         => false,
                                                        'required'      => true,
                                                        'id'            => 'address',
                                                        'class'         => 'input-entrega form-control address-response input-desk',
                                                        'value'         =>  $cliente->address,
                                                        'placeholder'   => 'RUA',
                                                        'disabled'      => true,
                                                    ])?>
                                                </div>

                                                <div class="col-sm-12">
                                                    <?= $this->Form->input('number', [
                                                        'div'           => false,
                                                        'label'         => false,
                                                        'required'      => true,
                                                        'id'            => 'number_cli',
                                                        'class'         => 'input-entrega form-control number number-response input-desk',
                                                        'value'         =>  $cliente->number,
                                                        'placeholder'   => 'NUMERO',
                                                        'disabled'      => true,
                                                    ])?>
                                                </div>

                                                <div class="col-sm-12">
                                                    <?= $this->Form->input('area', [
                                                        'div'           => false,
                                                        'label'         => false,
                                                        'required'      => true,
                                                        'id'            => 'area',
                                                        'class'         => 'input-entrega form-control area-response input-desk',
                                                        'value'         =>  $cliente->area,
                                                        'placeholder'   => 'BAIRRO',
                                                        'disabled'      => true,
                                                    ])?>
                                                </div>


                                                <div class="col-sm-12">
                                                    <?= $this->Form->input('uf_id', [
                                                        'value'         => $cliente_dados->city->state_id,
                                                        'id'            => 'states',
                                                        'options'       => $states,
                                                        'empty'         => 'Selecione um estado',
                                                        'label'         => false,
                                                        'div'           => false,
                                                        'placeholder'   => 'Estado',
                                                        'class'         => 'validate[required] form-control uf-response input-desk',
                                                        'disabled'      => true,
                                                    ]) ?>
                                                </div>



                                                <div class="col-sm-12">
                                                    <?= $this->Form->input('city_id', [
                                                        'div'           => false,
                                                        'label'         => false,
                                                        'required'      => true,
                                                        'id'            => 'city',
                                                        'options'       => $city,
                                                        'class'         => 'input-entrega form-control city-response input-desk',
                                                        'value'         =>  $cliente->city_id >= 1 ? $cliente->city_id : '',
                                                        'placeholder'   => 'CIDADE',
                                                        'disabled'      => true,
                                                    ])?>
                                                </div>

                                            </div>
                                        </div>
                                    </div> 

                                    <div class="col-sm-12" style="margin-top: 75px">
                                    </div>

                                    <div class="col-sm-12 text-center btn-continuar-entrega">
                                        <?= $this->Form->button('continuar', [
                                            'id'            => 'btn-submit-cadastro-main',
                                            'type'          => 'submit',
                                            'data-id'       => 20,
                                            'class'         => 'button-cadastrar btn-preencher-dados desk',
                                            'style'         => 'margin-bottom: 15px',
                                        ])?>
                                    </div>
                                </div>
                            </div><!--/box formulário de entrega-->
                                    


                                </center>
                            </div>

                        </div>
                </div>
                


                    <div class="container sumir-desk">
                        <ul class="nav nav-pills nav-justified">
                            
                            <?php if($academia_slug->id != 601) { ?>
                            <li class="alterar-li"><a class="centralizar-botoes-tab" data-toggle="pill" href="#home"><img src="/img/stronghest.png" alt=""><br> Academia</a></li>
                            <?php } ?>

                            <li class="active alterar-li"><a class="centralizar-botoes-tab" data-toggle="pill" href="#menu1"><img src="/img/online-store.png" alt=""><br>Loja</a></li>
                            <li class="alterar-li teste-mobile"><a class="centralizar-botoes-tab" data-toggle="pill" href="#menu2"><img src="/img/trucking.png" alt=""><br>Personalizado</a></li>
                        </ul>
                        
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade">
                                <br>
                            <?php if($academia_slug->id != 601) { ?> 
                                <div class="col-xs-12 col-md-12">
                                    <div class="box-completo">
                                        <div class="col-sm-12">
                                            <div class="col-xs-12 text-center box-logo-academia">
                                                <?= $this->Html->image('academias/'.$academia_slug->image, [
                                                    'class' => 'logo-academia-style',
                                                    'alt'   => 'academia '.$academia_slug->shortname,
                                                ])?>
                                            </div>
                                            <div class="col-sm-12 text-center box-dados-academia">
                                                <div class="row">
                                                    <div class="col-sm-12 text-left">
                                                        <?= $academia_slug->shortname ?>
                                                    </div>
                                                    <div class="col-sm-12 text-left">
                                                        <?= $academia_slug->address ?>, <?= $academia_slug->number ?> - <?= $academia_slug->area ?>
                                                    </div>
                                                    <div class="col-sm-12 text-left">
                                                        <?= $academia_slug->city->name ?> - <?= $academia_slug->city->uf ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 



                                        <div class="col-sm-12 text-center btn-continuar">
                                            <?= $this->Form->button('continuar', [
                                                'id'            => 'btn-submit-cadastro-main',
                                                'type'          => 'submit',
                                                'data-id'       => 10,
                                                'class'         => 'button-cadastrar btn-preencher-dados mobile',
                                                'style'         => 'margin-bottom: 15px',
                                            ])?>
                                        </div>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                                <?php } ?> 
                            </div>

                            <div id="menu1" class="tab-pane fade in active">
                            <?php foreach($lojas as $loja) { ?> 
                                <div class="col-xs-12 <?= $academia_slug->id != 601 ? 'col-md-12' : 'col-md-12 col-md-offset-3' ?>">
                                    <div class="box-completo">
                                    
                                            <div class="col-xs-12 text-center box-logo-academia">
                                                <?= $this->Html->image('academias/'.$loja->image, [
                                                    'class' => 'logo-academia-style',
                                                    'alt'   => 'academia '.$loja->shortname,
                                                ])?>
                                            </div>
                                    
                                        <div class="col-sm-12">

                                            <div class="col-sm-12 text-center box-dados-academia">
                                                <div class="row">
                                                    <div class="col-sm-12 text-left">
                                                        <?= $loja->shortname ?>
                                                    </div>
                                                    <div class="col-sm-12 text-left">
                                                        <?= $loja->address ?>, <?= $loja->number ?> - <?= $loja->area ?>
                                                    </div>
                                                    <div class="col-sm-12 text-left">
                                                        <?= $loja->city->name ?> - <?= $loja->city->uf ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 

                                        

                                        <div class="col-sm-12 check-confirmar-retirada" style="margin-top: 15px">
                                        </div>

                                        <div class="col-sm-12 text-center btn-continuar">
                                            <?= $this->Form->button('continuar', [
                                                'id'            => 'btn-submit-cadastro-main',
                                                'type'          => 'submit',
                                                'data-id'       => $loja->id,
                                                'class'         => 'button-cadastrar btn-preencher-dados mobile',
                                                'style'         => 'margin-bottom: 15px',
                                            ])?>
                                        </div>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                            <?php } ?> 

                            </div>
                            <div id="menu2" class="tab-pane fade">

                             <div class="col-12 col-md-12">
                                <div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">

                                    </div>
                                </div>
                                    <div class="col-sm-12">

                                        <div class="col-sm-12 text-center box-dados-academia">
                                        <h4 class="local-entrega">Local de entrega</h4>
                                            <div class="form-group">

                                                <div class="col-sm-12">
                                                    <?= $this->Form->input('cep', [
                                                        'div'           => false,
                                                        'label'         => false,
                                                        'required'      => true,
                                                        'id'            => 'cep-acad',
                                                        'class'         => 'input-entrega form-control input-mobile cep cep-response',
                                                        'value'         =>  $cliente->cep,
                                                        'disabled'      =>  true,                                                                                                                
                                                        'placeholder'   => 'CEP',
                                                    ])?>
                                                </div> 

                                                <div class="col-sm-12">
                                                    <?= $this->Form->input('address', [
                                                        'div'           => false,
                                                        'label'         => false,
                                                        'required'      => true,
                                                        'id'            => 'address-acad',
                                                        'disabled'      =>  true,
                                                        'class'         => 'input-entrega form-control input-mobile address-response',
                                                        'value'         =>  $cliente->address,
                                                        'placeholder'   => 'RUA',
                                                    ])?>
                                                </div>

                                                <div class="col-sm-12">
                                                    <?= $this->Form->input('number', [
                                                        'div'           => false,
                                                        'label'         => false,
                                                        'required'      => true,
                                                        'disabled'      =>  true,
                                                        'id'            => 'numbercli',
                                                        'class'         => 'input-entrega form-control number input-mobile number-response',
                                                        'value'         =>  $cliente->number,
                                                        'placeholder'   => 'NUMERO',
                                                    ])?>
                                                </div>

                                                <div class="col-sm-12">
                                                    <?= $this->Form->input('area', [
                                                        'div'           => false,
                                                        'label'         => false,
                                                        'required'      => true,
                                                        'disabled'      => true,
                                                        'id'            => 'area-acad',
                                                        'class'         => 'input-entrega form-control input-mobile area-response',
                                                        'value'         =>  $cliente->area,
                                                        'placeholder'   => 'BAIRRO',
                                                    ])?>
                                                </div>

                                                <div class="col-sm-12">
                                                    <?= $this->Form->input('uf_id', [
                                                        'value'         => $cliente_dados->city->state_id,
                                                        'id'            => 'states_acad',
                                                        'options'       => $states,
                                                        'empty'         => 'Selecione um estado',
                                                        'label'         => false,
                                                        'disabled'      => true,
                                                        'div'           => false,
                                                        'placeholder'   => 'Estado',
                                                        'class'         => 'validate[required] form-control input-mobile uf-response',
                                                    ]) ?>
                                                </div>


                                                <div class="col-sm-12">
                                                    <?= $this->Form->input('city_id', [
                                                        'div'           => false,
                                                        'label'         => false,
                                                        'required'      => true,
                                                        'id'            => 'city_acad',
                                                        'options'       => $city,
                                                        'disabled'      => true,
                                                        'class'         => 'input-entrega form-control input-mobile city-response',
                                                        'value'         =>  $cliente->city_id >= 1 ? $cliente->city_id : '',
                                                        'placeholder'   => 'CIDADE',
                                                    ])?>
                                                </div>

                                            </div>
                                        </div>
                                    </div> 

                                    <div class="col-sm-12" style="margin-top: 75px">
                                    </div>
                                                        
                                    <div class="col-sm-12 text-center btn-continuar-entrega">
                                        <?= $this->Form->button('continuar', [
                                            'id'            => 'btn-submit-cadastro-main',
                                            'type'          => 'submit',
                                            'data-id'       =>  20,
                                            'class'         => 'button-cadastrar btn-preencher-dados mobile',
                                            'style'         => 'margin-bottom: 15px',
                                        ])?>
                                    </div>
                                    <br>
                                    <br>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div> 

                </div>
            </div>


            <div style="display: none;">Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>

                    <div class="row">
                            
                    </div>
                </div>

                
            </div>
        </div>
        <?= $this->Form->end()?>
    </div>
</div>

<script type="text/javascript">
    function apenasNumeros(string) 
    {
        var numsStr = string.replace(/[^0-9]/g,'');
        return numsStr;
    }

    function TestaCPF(cpf) {
        var Soma;
        var Resto;
        var cpf = apenasNumeros(cpf);
        Soma = 0;
        if (cpf == "00000000000") return false;

        for (i=1; i<=9; i++) Soma = Soma + parseInt(cpf.substring(i-1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(cpf.substring(9, 10)) ) return false;

        Soma = 0;
        for (i = 1; i <= 10; i++) Soma = Soma + parseInt(cpf.substring(i-1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(cpf.substring(10, 11) ) ) return false;
        return true;
    }

    $('.first-step-btn').click(function () {
        $('.aviso-falta-dados').remove();
        if($('#cpf-compra-rapida').val() != '' && $('.email-response').val() != '' && $('.name-response').val() != '' && $('.tel-response').val() != '') {
            $('.first-step').fadeOut(400, function() {
                $('.second-step').fadeIn(400);
            });
        } else {
            $('.first-step-btn').parent().prepend('<p class="aviso-falta-dados" style="color: red">Está faltando alguma informação!</p>');
        }
    });

    $('.cpf-search').blur(function() {
      var cpf_input = $(this);
      var email_input = $('.email-response');

      /* email_input = email_input.replace(/\.$/, ""); */

      var name_input = $('.name-response');
      var tel_input = $('.tel-response');
      var prof_input = $('.prof-response');
      var cep_input = $('.cep-response');
      var address_input = $('.address-response');
      var number_input = $('.number-response');
      var area_input = $('.area-response');

      var uf_input = $('.uf-response');



      var city_input = $('.city-response');

      $('.erro-cpf-invalido').remove();
        if(TestaCPF(cpf_input.val())) {
          $.get(WEBROOT_URL + 'buscar-aluno/'+cpf_input.val(),
             function (data) {           
              if(data != 2) {
                data = JSON.parse(data);
                if(data.email != null && data.email.length >= 1) {
                  email_input.val(data.email);
                }
                if(data.name != null && data.name.length >= 1) {
                  name_input.val(data.name);
                }
                if(data.telephone != null && data.telephone.length >= 1) {
                  tel_input.val(data.telephone);
                } else if(data.mobile != null && data.mobile.length >= 1) {
                  tel_input.val(data.mobile);
                }

                if(data.cep != null && data.cep.length >= 1) {
                  cep_input.val(data.cep);
                }
                if(data.address != null && data.address.length >= 1) {
                  address_input.val(data.address);
                }
                if(data.number != null && data.number.length >= 1) {
                  number_input.val(data.number);
                }
                if(data.area != null && data.area.length >= 1) {
                  area_input.val(data.area);
                }
                // if(data.city.state_id != null && data.city.state_id.length >= 1) {
                //   uf_input.val(data.city.state_id);
                //   JSON.stringify(uf_input);

                // }
                // if(data.city.id != null && data.city.id.length >= 1) {
                //   city_input.val(data.city.id);
                // }



                $('.prof-response option').each(function () {
                    if(data.professor_id != null && data.professor_id == $(this).val()) {
                        prof_input.val(data.professor_id);
                    }
                });
              }
              console.log(data);
          });
          
        } else if (cpf_input.val().length > 0) {
          cpf_input.after('<p class="erro-cpf-invalido" style="color: red">CPF inválido!</p>');
          cpf_input.focus();             
        }
    });

    $('.email-response').blur(function () {
      var email_input = $(this);

      /* email_input = email_input.replace(/\.$/, ""); */

      var cpf_input = $('.cpf-search');
      var name_input = $('.name-response');
      var tel_input = $('.tel-response');
      var prof_input = $('.prof-response');
      var cep_input = $('.cep-response');
      var address_input = $('.address-response');
      var number_input = $('.number-response');
      var area_input = $('.area-response');
      var uf_input = $('.uf-response');
      var city_input = $('.city-response');

      $.get(WEBROOT_URL + 'buscar-aluno-email/?email='+email_input.val(),
         function (data) {
          if(data != 2) {
            data = JSON.parse(data);
            if(data.cpf != null && data.cpf.length >= 1) {
              cpf_input.val(data.cpf);
            }
            if(data.name != null && data.name.length >= 1) {
              name_input.val(data.name);
            }
            if(data.telephone != null && data.telephone.length >= 1) {
              tel_input.val(data.telephone);
            } else if(data.mobile != null && data.mobile.length >= 1) {
              tel_input.val(data.mobile);
            }

            if(data.cep != null && data.cep.length >= 1) {
                cep_input.val(data.cep);
            }
            if(data.address != null && data.address.length >= 1) {
                address_input.val(data.address);
            }
            if(data.number != null && data.number.length >= 1) {
                number_input.val(data.number);
            }
            if(data.area != null && data.area.length >= 1) {
                area_input.val(data.area);
            }
            // if(data.city.state_id != null && data.city.state_id.length >= 1) {
            //     uf_input.val(data.city.state_id);
                
                

            // }
            // if(data.city.id != null && data.city.id.length >= 1) {
            //     city_input.val(data.city.id);
            // }


            $('.prof-response option').each(function () {
                if(data.professor_id != null && data.professor_id == $(this).val()) {
                    prof_input.val(data.professor_id);
                }
            });
          }
      });

    });

    $('.btn-preencher-dados').click(function(e) {
        e.preventDefault();

        var id = $(this).attr('data-id');

        $('.retirada_loja_hidden').val(id);
        
        $('.form-preencher-dados').submit();
    });


    $(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
    });


    $('.teste-mobile').click(function() {

        $('.input-mobile').prop("disabled", false);

    });

    $('.teste-desk').click(function() {

        $('.input-desk').prop("disabled", false);

    });

    




</script>