<script type="text/javascript">
    var query_url = "<?= $query != null ? $query : 0 ?>";
    var marca_url = "<?= $marca != null ? $marca : 0 ?>";
    var tipo_filtro_url = "<?= $tipo_filtro != null ? $tipo_filtro : 0 ?>";
    var filtro_url = "<?= $filtro_slug != null ? $filtro_slug : 0 ?>";
    var ordenacao_url = "<?= $ordenacao != null ? $ordenacao : '' ?>";
</script>
<script>
    $(document).ready(function() {
        $('.marcas-link, .objetivo-link').click(function() {
            var container = $('.container-grade');
            var posicao_pagina = $(document).scrollTop();
            var posicao_container = (container.offset().top + 310);
            if(posicao_pagina < posicao_container) {
                if($(document).width() > 992) {
                    $('html, body').animate({
                        scrollTop: posicao_container
                    }, 700);
                }
                $('body').addClass('total-block');
            }
        });
    });
</script>

<style>
    .btn-compre{
        background: #8dc73f;
    }
    .btn-compre svg{
        fill: white;
        width: 25px;
        height: auto;
    }

    .produto-acabou-btn{
        background-color: #5089cf;
    }
    .produto-acabou-btn .fa{
        margin-top: -3px;
        color: #fff;
    }
    .btn-box{
        width: 49%;
        padding:5px;
    }
    .btn-box span{
        color: #fff;
        font-size: 12px;
        margin-top: 3px;
    }
    .btn-box img{
        width: 18px;
        margin-right: 5px;
        margin-top: -5px;
    }
    .box-botoes{
        display: inline-block;
        width: 100%;
        padding-top: 5px;
        float: right;
    }
    .div-sabor{
        height: 23px;
        padding: 0;
    }
    .prdt{
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        margin-bottom: 30px;
        padding: 5px 10px;
        min-height: 380px;
    }
    .link-prdt{
        height: 72px;
    }
    #pagar-celular{
        display: none;
    }
    .promocao-index, #grade-produtos {
        padding-right: 230px;
        padding-left: 80px;
        margin-left: 0;
        margin-right: 0;
        width: 100%;
    }
    .product {
        font-size: 14px;
    }
    .produto-item .bg-white-produto {
        min-height: 314px;
        border: 2px solid rgb(234, 234, 234);
        box-shadow: 0px 1px 7px 0px rgba(200,200,200,0.75);
    }
    .bg-gray-produto {
        bottom: 0;
        position: absolute;
        min-height: 92px;
        max-width: 185px;
    }
    .description {
        width: 100%;
        height: 40px;
        overflow: hidden;
        margin: 0 0 5px 0;
        font-size: 14px;
        text-transform: uppercase;
        font-weight: 700;
    }
    @media all and (max-width: 768px) {
        .produto-item .bg-white-produto {
            min-height: 350px;
        }
    }
    @media all and (min-width: 769px) {
        .box-close-filter { 
            width: 0; 
        }
        .filtro { 
            width: 230px; 
            opacity: 1; 
        }
        .box-filtro { 
            left: 0; 
        }
    }
    .produto-grade {
        max-width: 145px;
        max-height: 150px;
        margin-top: 0;
        display: flex;
        align-items: center;
    }
    .box-imagem-produto {
        width: 100%;
        height: 150px;
        margin-top: -15px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .box-info-produto {
        width: 100%;
        height: 200px;
    }
    .box-fundo-produto {
        width: 100%;
        padding: 0;
    }
    .box-preco {
        height: 70px;
        width: 100%;
        float: left;
        display: flex;
        flex-flow: column;
        justify-content: center;
    }
    .box-mochila-add {
        height: 45px;
        width: 45px;
        float: right;
        display: flex;
        align-items: flex-end;
        margin-top: 10px;
    }
    .box-mochila-add svg {
        width: 100%;
        height: 100%;
        cursor: pointer;
        background: #8dc73f;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
        fill: white;
        margin-right: 3px;
    }
    .box-mochila-avise {
        height: 45px;
        width: 45px;
        float: right;
        margin-top: 10px;
        background-color: #5089cf;
        box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
    }
    .box-mochila-avise > .fa{
        color: #FFF;
        font-size: 2.5em;
        margin-bottom: 2px;
        margin-top: 3px;
    }
    .categoria{
        height: 30px;
    }
    .categoria-div{
        position: absolute;
        right: 5px;
        top: 0px;
        width: 20px;
        height: 30px;
    }
    .preco {
        margin: 0;
        font-size: 16px;
    }
    .preco-desc {
        font-size: 16px;
    }
    .preco-normal {
        font-weight: 700;
        font-size: 20px;
        color: #5089cf;
    }
    .bg-white-produto a {
        min-height: 0;
    }
    .box-promocao {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: black;
        background: #f3d012;
    }
    .box-promocao:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-novidade {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: white;
        background: #5089cf;
    }
    .box-novidade:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-ultimo {
        position: absolute;
        top: 0px;
        letter-spacing: 2px;
        left: -6px;
        width: 100px;
        font-weight: 700;
        font-size: 13px;
        color: white;
        background: red;
    }
    .box-ultimo:before {
        content: "";
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 6px 6px 0;
        border-color: transparent #404041 transparent transparent;
        position: absolute;
        left: 0px;
        bottom: -6px;
    }

    .box-mochila-add svg:hover, .box-mochila-add svg:focus, .box-mochila-add svg:link:hover, .box-mochila-add svg:link:focus, .box-mochila-add svg:visited:hover, .box-mochila-add svg:visited:focus, .box-mochila-avise:hover, .btn-box:hover {
      -webkit-transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      transition-timing-function: cubic-bezier(0.62, 4, 0.32, 0.83);
      -webkit-animation: diff 0.5s 1;
      animation: diff 0.5s 1;
    }

    @-webkit-keyframes diff {
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);}
    }
    @keyframes diff {
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
      from,to {-webkit-transform: scale(1, 1);transform: scale(1, 1);}
      25% {-webkit-transform: scale(0.9, 1.1);transform: scale(0.9, 1.1);}
      50% {-webkit-transform: scale(1.1, 0.9);transform: scale(1.1, 0.9);}
      75% {-webkit-transform: scale(0.95, 1.05);transform: scale(0.95, 1.05);}
    }

    .icone-objetivo {
        position: absolute;
        left: 5px;
        bottom: 120px;
        width: 30px;
        height: 30px;
        color: #5089cf;
    }

    .box-imagem-produto svg {
        position: absolute;
        left: 5px;
        bottom: 130px;
        width: 20px;
        height: 30px;
    }

    .sabor-info {
        font-size: 12px;
        color: #333;
        text-align: center;
        overflow: hidden;
    }

    @media all and (min-width: 1650px) {
        .produto-item {
            width: 16.66666667%;
        }
    }
    @media all and (max-width: 1240px) {
        .produto-item {
            width: 33.3333333334%;
        }
    }
    @media all and (max-width: 1200px) and (min-width: 780px) {
        .form-busca {
            width: 80% !important;
        }
    }
    @media all and (max-width: 1050px) {
        .produto-item {
            width: 50%;
        }
    }
    @media all and (max-width: 850px) {
        .produto-item {
            width: 100%;
        }
    }
    @media all and (max-width: 768px) {
        #pagar-celular{
            display: block;
            margin-bottom: 7px;
        }
        #pagar-celular .btn-login-nav{
            font-size: 18px;
            padding: 6px;
        }
        .promocao-index, #grade-produtos {
            padding-left: 0;
            padding-right: 0;
        }
        .promocao-index {
            padding-top: 90px;
        }
        .produto-item {
            width: 50%;
        }
    }
    @media all and (max-width: 400px) {
        .produto-grade {
            max-width: 120px;
            max-height: 120px;
        }
        .produto-item {
            padding-right: 0px;
            padding-left: 0px;
        }
    }
    .loader {
        position: fixed;
        left: 0;
        top: 0%;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: white;
    }
    .content-loader{
        position: fixed;
        border-radius: 25px;
        left: 20%;
        top: 25%;
        width: 60%;
        height: 50%;
        z-index: 999999;
        background-color: white;
        text-align: center;
    }
    .content-loader img{
        width: 500px;
        margin-top: 45px;
    }
    .content-loader i{
        color: #f4d637;
    }
    .content-loader p{
        color: #5087c7;
        font-family: nexa_boldregular;
        font-size: 34px;
    }
    @media all and (max-width: 430px) {
        .content-loader{
            display: none;
        }
    }
    @media all and (max-width: 768px) {
        .loader {
            display: none;
        }
    }
    .dropdown a{
        color: #fff;
        font-weight: 400;
    }
    .nav .open>a, .nav .open>a:hover, .nav .open>a:focus, .nav>li>a:focus{
        background-color: rgb(66, 118, 181);
        border-color: rgb(66, 118, 181);
    }
    #dropdown-filtro ul{
        padding: 0;
    }
    #dropdown-filtro li{
        padding: 0;
    }
    .filtros-ativos{
        height: auto;
        margin-bottom: 15px;
        padding-bottom: 15px;
    }
    .filtros-ativos span{
        min-width: 100px;
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        background-color: rgba(255,255,255,1.0);
        border-radius: 5px;
        padding: 5px 10px;
        margin: 5px;
        display: inline-block;
        height: 30px;
    }
    .filtros-ativos .fa{
        color: rgb(255, 0, 0);
    }
    .filtros-ativos .fa:hover{
        color: rgb(244, 219, 27);
        cursor: pointer;
    }
    .filtros-ativos i{
        float: right;
        margin-top: 2px;
    }
    .subfiltros{
        padding: 0;
    }
    .subfiltros select{
        box-shadow: 0px 0px 1px 1px rgba(130, 130, 130, 0.4);
        background-color: rgba(255,255,255,1.0);
        border-radius: 5px;
        padding: 0px 10px;
        height: 25px;
        margin: 5px 3px;
        border: none;
        display: inline-block;
    }
    .owl-theme .owl-controls .owl-buttons div {
        color: #6f6f6f!important;
        background-color: transparent!important;
        font-size: 30px!important;
    }

    .owl-prev {
        width: 15px;
        height: 100px;
        position: absolute;
        top: 40%;
        left: 0;
        display: block!important;
        border:0px solid black;
    }

    .owl-next {
        width: 15px;
        height: 100px;
        position: absolute;
        top: 40%;
        right: -10px;
        display: block!important;
        border:0px solid black;
    }
    .logo-marcas {
        -webkit-filter: grayscale(100%);
        max-width: 80%;
        max-height: 90px;
    }
    .logo-marcas:hover {
        -webkit-filter: grayscale(0);
    }
    .faixa-combos {
        height: 474px;
        position: absolute;
        background-color: #61c513;
        left: 0;
        margin-left: -300px;
        width: 150%;
    }
    .container-grade {
        padding: 500px 0 25px 0;
        background-color: rgba(255, 255, 255, 1);
    }
    .dropdown-content {
        z-index: 50!important;
    }
    .btn-escolhido {
        color: white;
        font-size: 12px;
        text-transform: capitalize;
        padding: 2px 12px;
        margin-top: 3px;
        max-width: 100%;
    }
    .btn-escolhido p{
        margin-bottom: 2px;
        overflow-x: hidden;
        text-overflow: ellipsis;

    }
    .produtos-scroll {
        margin-top: 15px;
        position: relative;
        float: left;
        width: 100%;
    }
    @media (min-width: 1025px) {
        .box-completa-div:nth-of-type(1),
        .box-completa-div:nth-of-type(2),
        .box-completa-div:nth-of-type(3) {
            margin-bottom: 120px;
        }
    }
    .subfiltro-completo h4 {
        margin-top: 0; 
        color: white;
    }
    .subfiltro-completo .texto-sombra {
        text-shadow: 1px 0px 2px #1a1915, 
                    -1px 0px 2px #1a1915, 
                    0px 1px 2px #1a1915, 
                    0px -1px 2px #1a1915;
    }
    .fixed_subfiltro h4 {
        color: black;
    }
    img.comprar-hexag {
        margin-top: 0px;
        width: 25px;
        cursor: pointer;
    }
</style>

<?php if (!$this->request->is('mobile')) { ?>
<?php 
    if($query != null || $marca != null || $tipo_filtro != null || $filtro != null) {
        $filtragem = 1;
    } else {
        echo '<script>setCookie("image_bg", "padrao-comparador.jpg", 365);</script>';
        $filtragem = 0;
    }
?>

<?php if($marca != null && $filtro == null) { ?>
    <script type="text/javascript">
        $(document).ready(function() {
            setCookie("image_bg", "marcas/"+marca_url+".jpg", 365);
        });
    </script>
<?php } ?>

<?php if($marca == null && $filtro != null) { ?>
    <script type="text/javascript">
        $(document).ready(function() {
            setCookie("image_bg", tipo_filtro_url+"s/"+filtro_url+".jpg", 365);
        });
    </script>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function() {
        var image_bg = getCookie("image_bg");

        if(image_bg == '') {
            image_bg = 'padrao-comparador.jpg';
        } 

        $('.banner-bg').css('background-image', 'url(<?= WEBROOT_URL ?>/img/banners-loja/'+image_bg+')');
    });
</script>

<div class="banner-bg"></div>
<?php } ?>
<div class="container container-grade" data-section="home">
    <div class="col-sm-4 col-md-2 filtro-div">
        <?= $this->Element('filtro_comparador'); ?>
    </div>
    <div class="col-sm-12 col-md-10 produtos-div">
        <div class="produtos-grade">
            
            <?php $marca_name = str_replace('-', ' ', $marca); ?>
            <?php $filtro_name = str_replace('-', ' ', $filtro); ?>
            <?php $objetivo_da_categoria ? $objetivo_da_categoria = strtolower($objetivo_da_categoria).' <i class="fa fa-caret-right"></i> ' : '' ?>
            <?php $categoria_da_subcategoria ? $categoria_da_subcategoria = strtolower($categoria_da_subcategoria).' <i class="fa fa-caret-right"></i> ' : '' ?>

            <div class="col-xs-12 subfiltro-completo">
                <?php if($count_produtos >= 1) { ?>
                    <h4 class="texto-sombra">Encontramos <span style="font-size: 25px"><?= $count_produtos ?></span> produtos para sua busca:</h4>
                <?php } else { ?>
                    <h4 class="texto-sombra">Não encontramos produtos para sua busca, veja similares:</h4>
                <?php } ?>
                <div class="subfiltros tablet-hidden">
                    <div class="col-xs-12">
                        <h3>
                            <?php if($query == null && $marca == null && $filtro == null) { ?>
                            <button class="btn btn-escolhido btn-danger">Todos</button>
                        <?php } ?>
                        <?= $query != null ? '<button class="btn btn-escolhido btn-danger query_selected"><p><i class="fa fa-close"></i> '.$query.'</p></button>' : '' ?>
                        <?= $marca != null ? '<button class="btn btn-escolhido btn-danger marca_selected"><p><i class="fa fa-close"></i> '.$marca_name.'</p></button>' : '' ?>
                        <?= $filtro != null ? '<button class="btn btn-escolhido btn-danger filtro_selected"><p><i class="fa fa-close"></i> '.$objetivo_da_categoria.$categoria_da_subcategoria.$filtro_name.'</p></button>' : '' ?>

                            <div class="dropdowns">
                                <?= $this->Form->input('ordenar_por', [
                                    'options' => [
                                        'randomico' => 'Aleatório',
                                        'promocoes' => 'Promoções',
                                        'maior-valor' => 'Maior valor',
                                        'menor-valor' => 'Menor valor'
                                    ],
                                    'value'   => $ordenacao,
                                    'class'   => 'form-control ordenar-select',
                                    'label'   => false,
                                    'style'   => 'margin-top: -10px;'
                                ]) ?>
                            </div>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="produtos-scroll">
                <?php if($count_produtos >= 1) { ?>
                    <?php foreach($produtos as $produto) { ?>
                        <?php $fotos = unserialize($produto->fotos); ?>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 box-completa-div">
                            <div class="box-completa-produto box-completa-produto-busca" data-id="<?= $produto->id ?>" itemtype="http://schema.org/Product">
                                <div class="tarja-objetivo">
                                    <?php
                                        $contador_objetivos = 0; 
                                        $novo_top = 0;
                                    ?>
                                    <?php foreach ($produto->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                        <?php if ($contador_objetivos < 2) { ?>
                                        <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                                            <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                        </div>
                                        <?php } ?>
                                        <?php
                                            $contador_objetivos++;
                                            $novo_top = $novo_top + 19;
                                         ?>
                                    <?php } ?>
                                </div>
                                <div class="total-tarjas">
                                    <?php if ($produto->estoque == 1) { ?>
                                        <div class="tarja tarja-ultimo">
                                            <span>Último</span>
                                        </div>
                                    <?php } else if ($produto->estoque >= 1 && $produto->preco_promo) { ?>
                                        <div class="tarja tarja-promocao">
                                            <span>Promoção</span>
                                        </div>
                                    <?php } else if($produto->estoque >= 1 && $produto->created > $max_date_novidade) { ?>
                                        <div class="tarja tarja-novidade">
                                            <span>Novidade</span>
                                        </div>
                                    <?php } ?>               
                                </div>
                                <div class="info-produto">
                                    <div class="produto-superior">
                                        <a href="<?= '/comparador/produto/'.$produto->slug ?>">
                                            <?php $fotos = unserialize($produto->fotos); ?>
                                            <div class="foto-produto" id="produto-<?= $produto->id ?>" >
                                                <?= $this->Html->image(
                                                    'produtos/md-'.$fotos[0],
                                                    ['val' => $produto->slug, 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]); ?>
                                            </div>
                                            <div class="detalhes-produto">
                                                <div class="informacoes-detalhes text-left">
                                                    <p>Código: <?= $produto->id ?></p>
                                                    <p><?= strtolower($produto->produto_base->marca->name); ?></p>
                                                    <p><?= $produto->tamanho.' '.$produto->unidade_medida; ?></p>
                                                    <p>para <?= $produto->doses; ?> treinos</p>
                                                    <p><?= strtolower($produto->propriedade); ?></p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="produto-inferior">
                                        <?php if($produto->preco_promo || $desconto_geral > 0) {
                                            $porcentagem_nova = $produto->preco * 0.089;
                                            $preco_corte = $produto->preco + $porcentagem_nova; 
                                        } ?> 
                                        <?php $produto->preco_promo ?
                                        number_format($preco = $produto->preco_promo * $desconto_geral, 2, ',','.') :
                                        number_format($preco = $produto->preco * $desconto_geral, 2, ',','.'); ?>
                                        <?php $produto->preco_promo ? $porcentagem_nova = $produto->preco_promo * 0.089 : $porcentagem_nova = $produto->preco * 0.089 ?>
                                        <?php if($produto->preco_promo != null) {
                                            if($produto->preco_promo < 100.00) { ?>
                                            <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/2?>
                                            <?php $maximo_parcelas = "2x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php } else if($produto->preco_promo > 100.00 && $produto->preco_promo < 200.00) { ?>
                                            <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/3?>
                                            <?php $maximo_parcelas = "3x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php } else if($produto->preco_promo > 200.00 && $produto->preco_promo < 300.00) { ?>
                                            <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/5?>
                                            <?php $maximo_parcelas = "5x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php } else if($produto->preco_promo > 300.00) { ?>
                                            <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/6?>
                                            <?php $maximo_parcelas = "6x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php }
                                            } else {
                                        if($produto->preco < 100.00) { ?>
                                            <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/2?>
                                             <?php $maximo_parcelas = "2x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php } else if($produto->preco > 100.00 && $produto->preco < 200.00) { ?>
                                            <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/3?>
                                            <?php $maximo_parcelas = "3x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php } else if($produto->preco > 200.00 && $produto->preco < 300.00) { ?>
                                            <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/5?>
                                            <?php $maximo_parcelas = "5x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php } else if($produto->preco > 300.00) { ?>
                                            <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/6?>
                                            <?php $maximo_parcelas = "6x";
                                             $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                        <?php }
                                        } ?>
                                        <?php 
                                            if($produto->preco_promo || $desconto_geral > 0) {
                                                echo $this->Html->link("
                                                    <h4>".$produto->produto_base->name."</h4>
                                                    <p><span class='preco-corte'> R$".number_format($preco_corte, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                    <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                    '/comparador/produto/'.$produto->slug,
                                                    ['escape' => false]);
                                            } else {
                                                echo $this->Html->link("
                                                    <h4>".$produto->produto_base->name."</h4>
                                                    <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                    <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                    '/comparador/produto/'.$produto->slug,
                                                    ['escape' => false]);
                                            }
                                        ?>
                                    </div>
                                    <div class="hover-produto <?= $produto->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                                        <div class="col-xs-12">
                                            <div class="hexagono-indicar">
                                                <?php if($produto->status_id == 1) { ?>
                                                    <div class="hexagon-disponivel btn-indique" data-id="<?= $produto->id ?>">
                                                        <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                                        <p>indique</p>
                                                    </div>
                                                    <script>
                                                        $('.btn-indique').click(function () {
                                                                var id_produto = $(this).attr('data-id');
                                                                $('#overlay-indicacao-'+id_produto).height('100%');
                                                                $("html,body").css({"overflow":"hidden"});
                                                        });
                                                        function closeIndica() {
                                                            $('.overlay-indicacao').height('0%');
                                                            $("html,body").css({"overflow":"auto"});
                                                        }
                                                    </script>
                                                <?php } else { ?>
                                                    <div class="hexagon produto-acabou-btn" data-id="<?= $produto->id ?>">
                                                        <span><i class="fa fa-envelope-o"></i></span>
                                                        <p>avise-me</p>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <?php if($produto->status_id == 1) { ?>
                                            <div class="col-xs-6">
                                                <?= $this->Html->link('
                                                    <div class="hexagon-disponivel">
                                                        <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                        <p>detalhes</p>
                                                    </div>'
                                                    ,'/comparador/produto/'.$produto->slug,
                                                    ['escape' => false]);
                                                ?>
                                            </div>
                                            <div class="col-xs-6 text-center compare">
                                                <div class="hexagon-disponivel btn-comprar-overlay">
                                                    <span><?= $this->Html->image('mochila_log_branca.png',['alt' => 'Mochila LOG', 'class' => 'comprar-hexag']) ?></span>
                                                    <p>comprar</p>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-xs-6">
                                                <?= $this->Html->link('
                                                    <div class="hexagon">
                                                        <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                        <p>detalhes</p>
                                                    </div>'
                                                    ,'/comparador/produto/'.$produto->slug,
                                                    ['escape' => false]);
                                                ?>
                                            </div>
                                            <div class="col-xs-6 text-center compare">
                                                <?= $this->Html->link('
                                                    <div class="hexagon">
                                                        <span>ver</span>
                                                        <p>similares</p>
                                                    </div>',
                                                    '/comparador/similares/'.$produto->slug, 
                                                    ['escape' => false]
                                                ); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="botoes-comprar-similiar">
                                    <?php if($produto->visivel == 1 && $produto->status_id == 1) { ?>
                                        <button class="btn btn-comparar btn-comparar-action" data-id="<?= $produto->id ?>"><i class="fa fa-balance-scale"></i> Comparar</button>
                                        <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $produto->id ?>">Indique</button>
                                        <button class="desktop-hidden btn btn-incluir btn-comprar-overlay">Comprar</button>
                                    <?php } else if($produto->visivel == 1 && $produto->status_id == 2) { ?>
                                        <button class="btn btn-comparar btn-comparar-action" data-id="<?= $produto->id ?>"><i class="fa fa-balance-scale"></i> Comparar</button>
                                        <button class="desktop-hidden btn btn-similares produto-acabou-btn" data-id="<?= $produto->id ?>">Avise-me</button>
                                        <button class="desktop-hidden btn btn-similares produto-comparar-btn">Compare</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php foreach($produtos as $produto) { ?>
                        <div id="aviseMe-<?= $produto->id ?>" class="overlay overlay-avise">
                            <div class="overlay-content overlay-avise-<?= $produto->id ?> text-center">
                                <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                                <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                <br />
                                <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$produto->id])?>
                                <?= $this->Form->input('produto_id', [
                                    'div'           => false,
                                    'label'         => false,
                                    'type'          => 'hidden',
                                    'val'           => $produto->id,
                                    'id'            => 'produto_id_acabou-'.$produto->id
                                ])?>
                                <?= $this->Form->input('name', [
                                    'div'           => false,
                                    'label'         => false,
                                    'id'            => 'name_acabou-'.$produto->id,
                                    'class'         => 'form-control validate[optional]',
                                    'placeholder'   => 'Qual o seu nome?',
                                ])?>
                                <br/>
                                <?= $this->Form->input('email', [
                                    'div'           => false,
                                    'label'         => false,
                                    'id'            => 'email_acabou-'.$produto->id,
                                    'class'         => 'form-control validate[required, custom[email]]',
                                    'placeholder'   => 'Qual o seu email?',
                                ])?>
                                <br/>
                                <?= $this->Form->button('Enviar', [
                                    'data-id'       => $produto->id,
                                    'type'          => 'button',
                                    'class'         => 'btn btn-success send-acabou-btn'
                                ])?>
                                <br />
                                <br />
                                <?= $this->Form->end()?>
                            </div>
                        </div> 
                        <div id="overlay-indicacao-<?= $produto->id ?>" class="overlay overlay-indicacao">
                            <div class="overlay-content text-center">
                                <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                                <h4>Lembrou de alguém quando viu isso?</h4>
                                <h4>Indique este produto!</h4>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 share-whastapp share-buttons">
                                        <a href="whatsapp://send?text=Isso é a sua cara! <?= WEBROOT_URL.$SSlug.'/comparador/produto/'.$produto->slug ?>">
                                            <button class="btn button-blue"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</button>
                                        </a>
                                    </div>
                                    <div class="col-xs-12 share-whastapp share-buttons">
                                        <button class="btn button-blue" id="share-<?= $produto->id ?>"><i class="fa fa-comment-o" aria-hidden="true"></i> Messenger</button>
                                        <script>
                                            $(document).ready(function() {
                                                $('#share-<?= $produto->id ?>').click(function(){
                                                    var link = "<?= WEBROOT_URL.$SSlug.'/comparador/produto/'.$produto->slug ?>";
                                                    var app_id = '1321574044525013';
                                                    window.open('fb-messenger://share?link=' + link + '&app_id=' + app_id);
                                                });
                                            });
                                        </script>
                                    </div>
                                    <div class="tablet-hidden col-xs-12 col-md-3 share-buttons">
                                        <button data-id="whatsweb-<?= $produto->id ?>" class="btn button-blue abrir-whats"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp Web</button>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                        <a href="javascript: void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u=<?= WEBROOT_URL.$SSlug.'/comparador/produto/'.$produto->slug ?>','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');">
                                            <button class="btn button-blue fb-share-button"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</button>
                                        </a>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                        <button class="btn button-blue" id="copiar-clipboard-<?= $produto->id ?>"><i class="fa fa-clone" aria-hidden="true"></i> Copiar link</button>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                                        <button  class="btn button-blue abrir-email"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="collapse-whats-<?=$produto->id ?>" class="tablet-hidden enviar-whats">
                                        <div class="col-xs-4 text-center form-group">
                                            <?= $this->Form->input('whats-web-form', [
                                                'div'           => false,
                                                'id'            => 'nro-whatsweb-'.$produto->id,
                                                'label'         => false,
                                                'class'         => 'form-control form-indicar',
                                                'placeholder'   => "Celular*: (00)00000-0000"
                                            ])?>
                                        </div>
                                        <div class="col-xs-4 text-left obs-overlay">
                                            <p>*Para enviar pra mais de um contato deixe este campo vazio.</p>
                                        </div>
                                        <div class="col-xs-4 text-center form-group">
                                            <?= $this->Form->button('Enviar', [
                                            'type'          => 'button',
                                            'data-id'       => $produto->id,
                                            'class'         => 'btn button-blue',
                                            'id'            => 'enviar-whatsweb-'.$produto->id
                                            ])?>
                                        </div>
                                        <script>
                                            $(document).ready(function() {
                                                $('#enviar-whatsweb-<?=$produto->id ?>').click(function(){

                                                    var numero;
                                                    var link = "<?= WEBROOT_URL.$SSlug.'/comparador/produto/'.$produto->slug ?>";

                                                    if($('#nro-whatsweb-<?= $produto->id ?>').val() != '') {
                                                        numero = '55'+$('#nro-whatsweb-<?= $produto->id ?>').val();
                                                    }
                                                    
                                                    window.open('https://api.whatsapp.com/send?phone=' + numero + '&text=Isso é a sua cara! ' + link);
                                                });
                                            });
                                        </script>
                                    </div>
                                    <div class="col-xs-12 enviar-email" id="collapse-email-<?=$produto->id ?>">
                                       <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$produto->id])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[optional] form-indicar name-'.$produto->id,
                                        'placeholder'   => 'Quem indicou?',
                                    ])?>
                                    <br>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[required] form-indicar email-'.$produto->id,
                                        'placeholder'   => 'E-mail do destinatário',
                                    ])?>
                                    <?= $this->Form->input('id_produto', [
                                        'div'           => false,
                                        'class'         => 'id_produto-'.$produto->id,
                                        'value'         => $produto->id,
                                        'label'         => false,
                                        'type'          => 'hidden'
                                    ])?>
                                    <br>
                                    <?= $this->Form->button('Enviar', [
                                        'type'          => 'button',
                                        'data-id'       => $produto->id,
                                        'class'         => 'btn button-blue send-indicacao-home-btn'
                                    ])?>
                                    <br>
                                    <br>
                                    <?= $this->Form->end()?>
                                    </div>
                                </div>
                                <div class="row"> 
                                    <div class="col-xs-12 clipboard">   
                                        <?= $this->Form->input('url', [
                                            'div'           => false,
                                            'id'            => 'url-clipboard-'.$produto->id,
                                            'label'         => false,
                                            'value'         => WEBROOT_URL.$SSlug.'/comparador/produto/'.$produto->slug,
                                            'readonly'      => true,
                                            'class'         => 'form-control form-indicar'
                                        ])?>
                                    </div>  
                                    <p><span class="copy-alert hide" id="copy-alert-<?=$produto->id ?>">Copiado!</span></p>
                                    <script>
                                        $(document).ready(function() {
                                            // Copy to clipboard 
                                            document.querySelector("#copiar-clipboard-<?=$produto->id ?>").onclick = () => {
                                              // Select the content
                                                document.querySelector("#url-clipboard-<?=$produto->id ?>").select();
                                                // Copy to the clipboard
                                                document.execCommand('copy');
                                                // Exibe span de sucesso
                                                $('#copy-alert-<?=$produto->id ?>').removeClass("hide");
                                                // Oculta span de sucesso
                                                setTimeout(function() {
                                                    $('#copy-alert-<?=$produto->id ?>').addClass("hide");
                                                }, 1500);
                                            };
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <?php if(count($similares_query) >= 1) { ?>
                        <div class="title-section">
                            <h3>Produtos com a pesquisa <span style="color: #5089cf"><?= $similares_query_name ?></span></h3>
                            <?= $this->Html->link('<span class="fa fa-plus"></span> ver todos',
                                $SSlug.'/pesquisa/'.$similares_query_name.'/', ['escape' => false, 'style' => 'text-transform: uppercase']
                            ); ?>
                        </div>
                        <div class="owl-carousel">
                            <?php $i = 1; ?>
                            <?php foreach($similares_query as $similar_query) { ?>
                                <div class="item">
                                    <?php $fotos = unserialize($similar_query->fotos); ?>
                                    <div class="box-completa-produto" itemtype="http://schema.org/Product">
                                        <div class="tarja-objetivo">
                                            <?php
                                                $contador_objetivos = 0; 
                                                $novo_top = 0;
                                            ?>
                                            <?php foreach ($similares_query->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                                <?php if ($contador_objetivos < 2) { ?>
                                                <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                                                    <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                    $contador_objetivos++;
                                                    $novo_top = $novo_top + 19;
                                                 ?>
                                            <?php } ?>
                                        </div>
                                        <div class="total-tarjas">
                                            <?php if ($similares_query->estoque == 1) { ?>
                                                <div class="tarja tarja-ultimo">
                                                    <span>Último</span>
                                                </div>
                                            <?php } else if ($similares_query->estoque >= 1 && $similares_query->preco_promo) { ?>
                                                <div class="tarja tarja-promocao">
                                                    <span>Promoção</span>
                                                </div>
                                            <?php } else if($similares_query->estoque >= 1 && $similares_query->created > $max_date_novidade) { ?>
                                                <div class="tarja tarja-novidade">
                                                    <span>Novidade</span>
                                                </div>
                                            <?php } ?>               
                                        </div>
                                        <div class="info-produto">
                                            <div class="produto-superior">
                                                <a href="<?= '/comparador/produto/'.$similar_query->slug ?>">
                                                    <?php $fotos = unserialize($similar_query->fotos); ?>
                                                    <div class="foto-produto" id="produto-<?= $i ?>" >
                                                        <?= $this->Html->image(
                                                            'produtos/md-'.$fotos[0],
                                                            ['val' => $similar_query->slug, 'alt' => $similar_query->produto_base->name.' '.$similar_query->produto_base->embalagem_conteudo]); ?>
                                                    </div>
                                                    <div class="detalhes-produto">
                                                        <div class="informacoes-detalhes text-left">
                                                            <p>Código: <?= $similar_query->id ?></p>
                                                            <p><?= strtolower($similar_query->produto_base->marca->name); ?></p>
                                                            <p><?= $similar_query->tamanho.' '.$similar_query->unidade_medida; ?></p>
                                                            <p>para <?= $similar_query->doses; ?> treinos</p>
                                                            <p><?= strtolower($similar_query->propriedade); ?></p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="produto-inferior">
                                                <?php if($similar_query->preco_promo) { ?>
                                                <?php $porcentagem_nova = $similar_query->preco * 0.089;
                                                $preco_corte = $similar_query->preco + $porcentagem_nova; 
                                                } ?> 
                                                <?php $similar_query->preco_promo ?
                                                number_format($preco = $similar_query->preco_promo, 2, ',','.') :
                                                number_format($preco = $similar_query->preco, 2, ',','.'); ?>
                                                <?php $similar_query->preco_promo ? $porcentagem_nova = $similar_query->preco_promo * 0.089 : $porcentagem_nova = $similar_query->preco * 0.089 ?>
                                                <?php if($similar_query->preco_promo != null) {
                                                    if($similar_query->preco_promo < 100.00) { ?>
                                                    <?php $parcel = $similar_query->preco_promo * 0.089; $parcel = ($similar_query->preco_promo + $parcel)/2?>
                                                    <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_query->preco_promo > 100.00 && $similar_query->preco_promo < 200.00) { ?>
                                                    <?php $parcel = $similar_query->preco_promo * 0.089; $parcel = ($similar_query->preco_promo + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_query->preco_promo > 200.00 && $similar_query->preco_promo < 300.00) { ?>
                                                    <?php $parcel = $similar_query->preco_promo * 0.089; $parcel = ($similar_query->preco_promo + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_query->preco_promo > 300.00) { ?>
                                                    <?php $parcel = $similar_query->preco_promo * 0.089; $parcel = ($similar_query->preco_promo + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                    } else {
                                                if($similar_query->preco < 100.00) { ?>
                                                    <?php $parcel = $similar_query->preco * 0.089; $parcel = ($similar_query->preco + $parcel)/2?>
                                                     <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_query->preco > 100.00 && $similar_query->preco < 200.00) { ?>
                                                    <?php $parcel = $similar_query->preco * 0.089; $parcel = ($similar_query->preco + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_query->preco > 200.00 && $similar_query->preco < 300.00) { ?>
                                                    <?php $parcel = $similar_query->preco * 0.089; $parcel = ($similar_query->preco + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_query->preco > 300.00) { ?>
                                                    <?php $parcel = $similar_query->preco * 0.089; $parcel = ($similar_query->preco + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                } ?>
                                                <?php 
                                                    if($similar_query->preco_promo) {
                                                        echo $this->Html->link("
                                                            <h4>".$similar_query->produto_base->name."</h4>
                                                            <p><span class='preco-corte'> R$".number_format($preco_corte, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            '/comparador/produto/'.$similar_query->slug,
                                                            ['escape' => false]);
                                                    } else {
                                                        echo $this->Html->link("
                                                            <h4>".$similar_query->produto_base->name."</h4>
                                                            <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            '/comparador/produto/'.$similar_query->slug,
                                                            ['escape' => false]);
                                                    }
                                                ?>
                                            </div>
                                            <div class="hover-produto <?= $similar_query->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                                                <div class="col-xs-12">
                                                    <div class="hexagono-indicar">
                                                        <?php if($similar_query->status_id == 1) { ?>
                                                            <div class="hexagon-disponivel btn-indique" data-id="<?= $similar_query->id ?>">
                                                                <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                                                <p>indique</p>
                                                            </div>
                                                            <script>
                                                                $('.btn-indique').click(function () {
                                                                        var id_produto = $(this).attr('data-id');
                                                                        $('#overlay-indicacao-'+id_produto).height('100%');
                                                                        $("html,body").css({"overflow":"hidden"});
                                                                });
                                                                function closeIndica() {
                                                                    $('.overlay-indicacao').height('0%');
                                                                    $("html,body").css({"overflow":"auto"});
                                                                }
                                                            </script>
                                                        <?php } else { ?>
                                                            <div class="hexagon produto-acabou-btn" data-id="<?= $similar_query->id ?>">
                                                                <span><i class="fa fa-envelope-o"></i></span>
                                                                <p>avise-me</p>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <?php if($similar_query->status_id == 1) { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon-disponivel">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,'/comparador/produto/'.$similar_query->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon-disponivel btn-comprar-overlay">
                                                            <span><?= $this->Html->image('mochila_log_branca.png',['alt' => 'Mochila LOG', 'class' => 'comprar-hexag']) ?></span>
                                                            <p>comprar</p>
                                                        </div>
                                                    </div>
                                                <?php }
                                                else { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,'/comparador/produto/'.$similar_query->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon">
                                                                <span>ver</span>
                                                                <p>similares</p>
                                                            </div>',
                                                            '/comparador/similares/'.$similar_query->slug, 
                                                            ['escape' => false]
                                                        ); ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="botoes-comprar-similiar">
                                            <?php if($similar_query->visivel == 1 && $similar_query->status_id == 1) { ?>
                                                <button class="btn btn-comparar btn-comparar-action" data-id="<?= $similar_query->id ?>"><i class="fa fa-balance-scale"></i> Comparar</button>
                                                <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $similar_query->id ?>">Indique</button>
                                                <button class="desktop-hidden btn btn-incluir btn-comprar-overlay">Comprar</button> 
                                            <?php } else if($similar_query->visivel == 1 && $similar_query->status_id == 2) { ?>
                                                <button class="btn btn-comparar btn-comparar-action" data-id="<?= $similar_query->id ?>"><i class="fa fa-balance-scale"></i> Comparar</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++; ?>
                            <?php } ?>
                            <div class="item">
                                <?= $this->Html->link('
                                    <div class="box-completa-produto" id="produto-<?= $i ?>">
                                        <div class="col-md-12 col-xs-12 text-center bg-white-produto prdt">
                                            <div class="text-center" style="margin-top: 155px;">
                                                <span class="fa fa-plus fa-2x"></span><br>
                                                <span>ver mais</span>
                                            </div>
                                        </div>
                                    </div>',$SSlug.'/pesquisa/'.$similares_query_name.'/', ['escape' => false]); 
                                ?>
                            </div>
                        </div>
                        <?php foreach($similares_query as $similar_query) { ?>
                            <div id="aviseMe-<?= $similar_query->id ?>" class="overlay overlay-avise">
                                <div class="overlay-content overlay-avise-<?= $similar_query->id ?> text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                                    <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                    <br />
                                    <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$similar_query->id])?>
                                    <?= $this->Form->input('produto_id', [
                                        'div'           => false,
                                        'label'         => false,
                                        'type'          => 'hidden',
                                        'val'           => $similar_query->id,
                                        'id'            => 'produto_id_acabou-'.$similar_query->id
                                    ])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'name_acabou-'.$similar_query->id,
                                        'class'         => 'form-control validate[optional]',
                                        'placeholder'   => 'Qual o seu nome?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'email_acabou-'.$similar_query->id,
                                        'class'         => 'form-control validate[required, custom[email]]',
                                        'placeholder'   => 'Qual o seu email?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->button('Enviar', [
                                        'data-id'       => $similar_query->id,
                                        'type'          => 'button',
                                        'class'         => 'btn btn-success send-acabou-btn'
                                    ])?>
                                    <br />
                                    <br />
                                    <?= $this->Form->end()?>
                                </div>
                            </div> 
                            <div id="overlay-indicacao-<?= $similar_query->id ?>" class="overlay overlay-indicacao">
                                <div class="overlay-content text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                                    <h4>Lembrou de alguém quando viu isso?</h4>
                                    <h4>Indique este produto!</h4>
                                    <br>
                                    <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$similar_query->id])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[optional] form-indicar name-'.$similar_query->id,
                                        'placeholder'   => 'Quem indicou?',
                                    ])?>
                                    <br>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[required] form-indicar email-'.$similar_query->id,
                                        'placeholder'   => 'E-mail do destinatário',
                                    ])?>
                                    <?= $this->Form->input('id_produto', [
                                        'div'           => false,
                                        'class'         => 'id_produto-'.$similar_query->id,
                                        'value'         => $similar_query->id,
                                        'label'         => false,
                                        'type'          => 'hidden'
                                    ])?>
                                    <br>
                                    <?= $this->Form->button('Enviar', [
                                        'type'          => 'button',
                                        'data-id'       => $similar_query->id,
                                        'class'         => 'btn btn-success send-indicacao-home-btn'
                                    ])?>
                                    <br>
                                    <br>
                                    <?= $this->Form->end()?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>

                    <?php if(count($similares_subcategoria) >= 1) { ?>
                        <div class="title-section">
                            <h3>Produtos da subcategoria <span style="color: #5089cf"><?= $similares_subcategoria_name ?></span></h3>
                            <?= $this->Html->link('<span class="fa fa-plus"></span> ver todos',
                                $SSlug.'/pesquisa/0/0/subcategoria/'.$similares_subcategoria_slug.'/', ['escape' => false, 'style' => 'text-transform: uppercase']
                            ); ?>
                        </div>
                        <div class="owl-carousel">
                            <?php $i = 1; ?>
                            <?php foreach($similares_subcategoria as $similar_subcategoria) { ?>
                                <div class="item">
                                    <?php $fotos = unserialize($similar_subcategoria->fotos); ?>
                                        <div class="tarja-objetivo">
                                            <?php
                                                $contador_objetivos = 0; 
                                                $novo_top = 0;
                                            ?>
                                            <?php foreach ($similar_subcategoria->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                                <?php if ($contador_objetivos < 2) { ?>
                                                <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                                                    <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                    $contador_objetivos++;
                                                    $novo_top = $novo_top + 19;
                                                 ?>
                                            <?php } ?>
                                        </div>
                                        <div class="total-tarjas">
                                            <?php if ($similar_subcategoria->estoque == 1) { ?>
                                                <div class="tarja tarja-ultimo">
                                                    <span>Último</span>
                                                </div>
                                            <?php } else if ($similar_subcategoria->estoque >= 1 && $similar_subcategoria->preco_promo) { ?>
                                                <div class="tarja tarja-promocao">
                                                    <span>Promoção</span>
                                                </div>
                                            <?php } else if($similar_subcategoria->estoque >= 1 && $similar_subcategoria->created > $max_date_novidade) { ?>
                                                <div class="tarja tarja-novidade">
                                                    <span>Novidade</span>
                                                </div>
                                            <?php } ?>               
                                        </div>
                                        <div class="info-produto">
                                            <div class="produto-superior">
                                                <a href="<?= '/comparador/produto/'.$similar_subcategoria->slug ?>">
                                                    <?php $fotos = unserialize($similar_subcategoria->fotos); ?>
                                                    <div class="foto-produto" id="produto-<?= $i ?>" >
                                                        <?= $this->Html->image(
                                                            'produtos/md-'.$fotos[0],
                                                            ['val' => $similar_subcategoria->slug, 'alt' => $similar_subcategoria->produto_base->name.' '.$similar_subcategoria->produto_base->embalagem_conteudo]); ?>
                                                    </div>
                                                    <div class="detalhes-produto">
                                                        <div class="informacoes-detalhes text-left">
                                                            <p>Código: <?= $similar_subcategoria->id ?></p>
                                                            <p><?= strtolower($similar_subcategoria->produto_base->marca->name); ?></p>
                                                            <p><?= $similar_subcategoria->tamanho.' '.$similar_subcategoria->unidade_medida; ?></p>
                                                            <p>para <?= $similar_subcategoria->doses; ?> treinos</p>
                                                            <p><?= strtolower($similar_subcategoria->propriedade); ?></p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="produto-inferior">
                                                <?php if($similar_subcategoria->preco_promo) { ?>
                                                <?php $porcentagem_nova = $similar_subcategoria->preco * 0.089;
                                                $preco_corte = $similar_subcategoria->preco + $porcentagem_nova; 
                                                } ?> 
                                                <?php $similar_subcategoria->preco_promo ?
                                                number_format($preco = $similar_subcategoria->preco_promo, 2, ',','.') :
                                                number_format($preco = $similar_subcategoria->preco, 2, ',','.'); ?>
                                                <?php $similar_subcategoria->preco_promo ? $porcentagem_nova = $similar_subcategoria->preco_promo * 0.089 : $porcentagem_nova = $similar_subcategoria->preco * 0.089 ?>
                                                <?php if($similar_subcategoria->preco_promo != null) {
                                                    if($similar_subcategoria->preco_promo < 100.00) { ?>
                                                    <?php $parcel = $similar_subcategoria->preco_promo * 0.089; $parcel = ($similar_subcategoria->preco_promo + $parcel)/2?>
                                                    <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_subcategoria->preco_promo > 100.00 && $similar_subcategoria->preco_promo < 200.00) { ?>
                                                    <?php $parcel = $similar_subcategoria->preco_promo * 0.089; $parcel = ($similar_subcategoria->preco_promo + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_subcategoria->preco_promo > 200.00 && $similar_subcategoria->preco_promo < 300.00) { ?>
                                                    <?php $parcel = $similar_subcategoria->preco_promo * 0.089; $parcel = ($similar_subcategoria->preco_promo + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_subcategoria->preco_promo > 300.00) { ?>
                                                    <?php $parcel = $similar_subcategoria->preco_promo * 0.089; $parcel = ($similar_subcategoria->preco_promo + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                    } else {
                                                if($similar_subcategoria->preco < 100.00) { ?>
                                                    <?php $parcel = $similar_subcategoria->preco * 0.089; $parcel = ($similar_subcategoria->preco + $parcel)/2?>
                                                     <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_subcategoria->preco > 100.00 && $similar_subcategoria->preco < 200.00) { ?>
                                                    <?php $parcel = $similar_subcategoria->preco * 0.089; $parcel = ($similar_subcategoria->preco + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_subcategoria->preco > 200.00 && $similar_subcategoria->preco < 300.00) { ?>
                                                    <?php $parcel = $similar_subcategoria->preco * 0.089; $parcel = ($similar_subcategoria->preco + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_subcategoria->preco > 300.00) { ?>
                                                    <?php $parcel = $similar_subcategoria->preco * 0.089; $parcel = ($similar_subcategoria->preco + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                } ?>
                                                <?php 
                                                    if($similar_subcategoria->preco_promo) {
                                                        echo $this->Html->link("
                                                            <h4>".$similar_subcategoria->produto_base->name."</h4>
                                                            <p><span class='preco-corte'> R$".number_format($preco_corte, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            '/comparador/produto/'.$similar_subcategoria->slug,
                                                            ['escape' => false]);
                                                    } else {
                                                        echo $this->Html->link("
                                                            <h4>".$similar_subcategoria->produto_base->name."</h4>
                                                            <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            '/comparador/produto/'.$similar_subcategoria->slug,
                                                            ['escape' => false]);
                                                    }
                                                ?>
                                            </div>
                                            <div class="hover-produto <?= $similar_subcategoria->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                                                <div class="col-xs-12">
                                                    <div class="hexagono-indicar">
                                                        <?php if($similar_subcategoria->status_id == 1) { ?>
                                                            <div class="hexagon-disponivel btn-indique" data-id="<?= $similar_subcategoria->id ?>">
                                                                <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                                                <p>indique</p>
                                                            </div>
                                                            <script>
                                                                $('.btn-indique').click(function () {
                                                                        var id_produto = $(this).attr('data-id');
                                                                        $('#overlay-indicacao-'+id_produto).height('100%');
                                                                        $("html,body").css({"overflow":"hidden"});
                                                                });
                                                                function closeIndica() {
                                                                    $('.overlay-indicacao').height('0%');
                                                                    $("html,body").css({"overflow":"auto"});
                                                                }
                                                            </script>
                                                        <?php } else { ?>
                                                            <div class="hexagon produto-acabou-btn" data-id="<?= $similar_subcategoria->id ?>">
                                                                <span><i class="fa fa-envelope-o"></i></span>
                                                                <p>avise-me</p>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <?php if($similar_subcategoria->status_id == 1) { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon-disponivel">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,'/comparador/produto/'.$similar_subcategoria->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon-disponivel btn-comprar-overlay">
                                                            <span><?= $this->Html->image('mochila_log_branca.png',['alt' => 'Mochila LOG', 'class' => 'comprar-hexag']) ?></span>
                                                            <p>comprar</p>
                                                        </div>
                                                    </div>
                                                <?php }
                                                else { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,'/comparador/produto/'.$similar_subcategoria->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon">
                                                            <span>ver</span>
                                                            <p>similares</p>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="botoes-comprar-similiar">
                                            <?php if($similar_subcategoria->visivel == 1 && $similar_subcategoria->status_id == 1) { ?>
                                                <button class="btn btn-comparar btn-comparar-action" data-id="<?= $similar_subcategoria->id ?>"><i class="fa fa-balance-scale"></i> Comparar</button>
                                                <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $similar_subcategoria->id ?>">Indique</button>
                                                <button class="desktop-hidden btn btn-incluir btn-comprar-overlay">Comprar</button>  
                                            <?php } else if($similar_subcategoria->visivel == 1 && $similar_subcategoria->status_id == 2) { ?>
                                                <button class="btn btn-comparar btn-comparar-action" data-id="<?= $similar_subcategoria->id ?>"><i class="fa fa-balance-scale"></i> Comparar</button>    
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++; ?>
                            <?php } ?>
                            <div class="item">
                                <?= $this->Html->link('
                                    <div class="box-completa-produto" id="produto-<?= $i ?>">
                                        <div class="col-md-12 col-xs-12 text-center bg-white-produto prdt">
                                            <div class="text-center" style="margin-top: 155px;">
                                                <span class="fa fa-plus fa-2x"></span><br>
                                                <span>ver mais</span>
                                            </div>
                                        </div>
                                    </div>',$SSlug.'/pesquisa/0/0/subcategoria/'.$similares_subcategoria_slug.'/', ['escape' => false]); 
                                ?>
                            </div>
                        </div>
                        <?php foreach($similares_subcategoria as $similar_subcategoria) { ?>
                            <div id="aviseMe-<?= $similar_subcategoria->id ?>" class="overlay overlay-avise">
                                <div class="overlay-content overlay-avise-<?= $similar_subcategoria->id ?> text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                                    <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                    <br />
                                    <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$similar_subcategoria->id])?>
                                    <?= $this->Form->input('produto_id', [
                                        'div'           => false,
                                        'label'         => false,
                                        'type'          => 'hidden',
                                        'val'           => $similar_subcategoria->id,
                                        'id'            => 'produto_id_acabou-'.$similar_subcategoria->id
                                    ])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'name_acabou-'.$similar_subcategoria->id,
                                        'class'         => 'form-control validate[optional]',
                                        'placeholder'   => 'Qual o seu nome?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'email_acabou-'.$similar_subcategoria->id,
                                        'class'         => 'form-control validate[required, custom[email]]',
                                        'placeholder'   => 'Qual o seu email?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->button('Enviar', [
                                        'data-id'       => $similar_subcategoria->id,
                                        'type'          => 'button',
                                        'class'         => 'btn btn-success send-acabou-btn'
                                    ])?>
                                    <br />
                                    <br />
                                    <?= $this->Form->end()?>
                                </div>
                            </div> 
                            <div id="overlay-indicacao-<?= $similar_subcategoria->id ?>" class="overlay overlay-indicacao">
                                <div class="overlay-content text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                                    <h4>Lembrou de alguém quando viu isso?</h4>
                                    <h4>Indique este produto!</h4>
                                    <br>
                                    <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$similar_subcategoria->id])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[optional] form-indicar name-'.$similar_subcategoria->id,
                                        'placeholder'   => 'Quem indicou?',
                                    ])?>
                                    <br>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[required] form-indicar email-'.$similar_subcategoria->id,
                                        'placeholder'   => 'E-mail do destinatário',
                                    ])?>
                                    <?= $this->Form->input('id_produto', [
                                        'div'           => false,
                                        'class'         => 'id_produto-'.$similar_subcategoria->id,
                                        'value'         => $similar_subcategoria->id,
                                        'label'         => false,
                                        'type'          => 'hidden'
                                    ])?>
                                    <br>
                                    <?= $this->Form->button('Enviar', [
                                        'type'          => 'button',
                                        'data-id'       => $similar_subcategoria->id,
                                        'class'         => 'btn btn-success send-indicacao-home-btn'
                                    ])?>
                                    <br>
                                    <br>
                                    <?= $this->Form->end()?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>

                    <?php if(count($similares_categoria) >= 1) { ?>
                        <div class="title-section">
                            <h3>Produtos da categoria <span style="color: #5089cf"><?= $similares_categoria_name ?></span></h3>
                            <?= $this->Html->link('<span class="fa fa-plus"></span> ver todos',
                                $SSlug.'/pesquisa/0/0/categoria/'.$similares_categoria_slug.'/', ['escape' => false, 'style' => 'text-transform: uppercase']
                            ); ?>
                        </div>
                        <div class="owl-carousel">
                            <?php $i = 1; ?>
                            <?php foreach($similares_categoria as $similar_categoria) { ?>
                                <div class="item">
                                    <?php $fotos = unserialize($similar_categoria->fotos); ?>
                                    <div class="box-completa-produto" itemtype="http://schema.org/Product">
                                        <div class="tarja-objetivo">
                                            <?php
                                                $contador_objetivos = 0; 
                                                $novo_top = 0;
                                            ?>
                                            <?php foreach ($similar_categoria->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                                <?php if ($contador_objetivos < 2) { ?>
                                                <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                                                    <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                    $contador_objetivos++;
                                                    $novo_top = $novo_top + 19;
                                                 ?>
                                            <?php } ?>
                                        </div>
                                        <div class="total-tarjas">
                                            <?php if ($similar_categoria->estoque == 1) { ?>
                                                <div class="tarja tarja-ultimo">
                                                    <span>Último</span>
                                                </div>
                                            <?php } else if ($similar_categoria->estoque >= 1 && $similar_categoria->preco_promo) { ?>
                                                <div class="tarja tarja-promocao">
                                                    <span>Promoção</span>
                                                </div>
                                            <?php } else if($similar_categoria->estoque >= 1 && $similar_categoria->created > $max_date_novidade) { ?>
                                                <div class="tarja tarja-novidade">
                                                    <span>Novidade</span>
                                                </div>
                                            <?php } ?>               
                                        </div>
                                        <div class="info-produto">
                                            <div class="produto-superior">
                                                <a href="<?= '/comparador/produto/'.$similar_categoria->slug ?>">
                                                    <?php $fotos = unserialize($similar_categoria->fotos); ?>
                                                    <div class="foto-produto" id="produto-<?= $i ?>" >
                                                        <?= $this->Html->image(
                                                            'produtos/md-'.$fotos[0],
                                                            ['val' => $similar_categoria->slug, 'alt' => $similar_categoria->produto_base->name.' '.$similar_categoria->produto_base->embalagem_conteudo]); ?>
                                                    </div>
                                                    <div class="detalhes-produto">
                                                        <div class="icones-objetivos text-center">
                                                            <?php $contador_objetivos = 0; ?>
                                                            <?php if ($contador_objetivos <= 1) { ?>
                                                                <?php foreach ($similar_categoria->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                                                    <div class="col-xs-6" title="<?= $produto_objetivo->objetivo->name ?>">
                                                                        <div class="row">
                                                                            <?= $produto_objetivo->objetivo->image ?>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                                <?php $contador_objetivos++; ?>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="informacoes-detalhes text-left">
                                                            <p>Código: <?= $similar_categoria->id ?></p>
                                                            <p><?= strtolower($similar_categoria->produto_base->marca->name); ?></p>
                                                            <p><?= $similar_categoria->tamanho.' '.$similar_categoria->unidade_medida; ?></p>
                                                            <p>para <?= $similar_categoria->doses; ?> treinos</p>
                                                            <p><?= strtolower($similar_categoria->propriedade); ?></p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="produto-inferior">
                                                <?php if($similar_categoria->preco_promo) { ?>
                                                <?php $porcentagem_nova = $similar_categoria->preco * 0.089;
                                                $preco_corte = $similar_categoria->preco + $porcentagem_nova; 
                                                } ?> 
                                                <?php $similar_categoria->preco_promo ?
                                                number_format($preco = $similar_categoria->preco_promo, 2, ',','.') :
                                                number_format($preco = $similar_categoria->preco, 2, ',','.'); ?>
                                                <?php $similar_categoria->preco_promo ? $porcentagem_nova = $similar_categoria->preco_promo * 0.089 : $porcentagem_nova = $similar_categoria->preco * 0.089 ?>
                                                <?php if($similar_categoria->preco_promo != null) {
                                                    if($similar_categoria->preco_promo < 100.00) { ?>
                                                    <?php $parcel = $similar_categoria->preco_promo * 0.089; $parcel = ($similar_categoria->preco_promo + $parcel)/2?>
                                                    <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_categoria->preco_promo > 100.00 && $similar_categoria->preco_promo < 200.00) { ?>
                                                    <?php $parcel = $similar_categoria->preco_promo * 0.089; $parcel = ($similar_categoria->preco_promo + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_categoria->preco_promo > 200.00 && $similar_categoria->preco_promo < 300.00) { ?>
                                                    <?php $parcel = $similar_categoria->preco_promo * 0.089; $parcel = ($similar_categoria->preco_promo + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_categoria->preco_promo > 300.00) { ?>
                                                    <?php $parcel = $similar_categoria->preco_promo * 0.089; $parcel = ($similar_categoria->preco_promo + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                    } else {
                                                if($similar_categoria->preco < 100.00) { ?>
                                                    <?php $parcel = $similar_categoria->preco * 0.089; $parcel = ($similar_categoria->preco + $parcel)/2?>
                                                     <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_categoria->preco > 100.00 && $similar_categoria->preco < 200.00) { ?>
                                                    <?php $parcel = $similar_categoria->preco * 0.089; $parcel = ($similar_categoria->preco + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_categoria->preco > 200.00 && $similar_categoria->preco < 300.00) { ?>
                                                    <?php $parcel = $similar_categoria->preco * 0.089; $parcel = ($similar_categoria->preco + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_categoria->preco > 300.00) { ?>
                                                    <?php $parcel = $similar_categoria->preco * 0.089; $parcel = ($similar_categoria->preco + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                } ?>
                                                <?php 
                                                    if($similar_categoria->preco_promo) {
                                                        echo $this->Html->link("
                                                            <h4>".$similar_categoria->produto_base->name."</h4>
                                                            <p><span class='preco-corte'> R$".number_format($preco_corte, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            '/comparador/produto/'.$similar_categoria->slug,
                                                            ['escape' => false]);
                                                    } else {
                                                        echo $this->Html->link("
                                                            <h4>".$similar_categoria->produto_base->name."</h4>
                                                            <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            '/comparador/produto/'.$similar_categoria->slug,
                                                            ['escape' => false]);
                                                    }
                                                ?>
                                            </div>
                                            <div class="hover-produto <?= $similar_categoria->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                                                <div class="col-xs-12">
                                                    <div class="hexagono-indicar">
                                                        <?php if($similar_categoria->status_id == 1) { ?>
                                                            <div class="hexagon-disponivel btn-indique" data-id="<?= $similar_categoria->id ?>">
                                                                <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                                                <p>indique</p>
                                                            </div>
                                                            <script>
                                                                $('.btn-indique').click(function () {
                                                                        var id_produto = $(this).attr('data-id');
                                                                        $('#overlay-indicacao-'+id_produto).height('100%');
                                                                        $("html,body").css({"overflow":"hidden"});
                                                                });
                                                                function closeIndica() {
                                                                    $('.overlay-indicacao').height('0%');
                                                                    $("html,body").css({"overflow":"auto"});
                                                                }
                                                            </script>
                                                        <?php } else { ?>
                                                            <div class="hexagon produto-acabou-btn" data-id="<?= $similar_categoria->id ?>">
                                                                <span><i class="fa fa-envelope-o"></i></span>
                                                                <p>avise-me</p>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <?php if($similar_categoria->status_id == 1) { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon-disponivel">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,'/comparador/produto/'.$similar_categoria->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon-disponivel btn-comprar-overlay">
                                                            <span><?= $this->Html->image('mochila_log_branca.png',['alt' => 'Mochila LOG', 'class' => 'comprar-hexag']) ?></span>
                                                            <p>comprar</p>
                                                        </div>
                                                    </div>
                                                <?php }
                                                else { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,'/comparador/produto/'.$similar_categoria->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon">
                                                            <span>ver</span>
                                                            <p>similares</p>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="botoes-comprar-similiar">
                                            <?php if($similar_categoria->visivel == 1 && $similar_categoria->status_id == 1) { ?>
                                                <button class="btn btn-comparar btn-comparar-action" data-id="<?= $similar_categoria->id ?>"><i class="fa fa-balance-scale"></i> Comparar</button>
                                                <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $similar_categoria->id ?>">Indique</button>
                                                <button class="desktop-hidden btn btn-incluir btn-comprar-overlay">Comprar</button> 
                                            <?php } else if($similar_categoria->visivel == 1 && $similar_categoria->status_id == 2) { ?>
                                                <button class="btn btn-comparar btn-comparar-action" data-id="<?= $similar_categoria->id ?>"><i class="fa fa-balance-scale"></i> Comparar</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++; ?>
                            <?php } ?>
                            <div class="item">
                                <?= $this->Html->link('
                                    <div class="box-completa-produto" id="produto-<?= $i ?>">
                                        <div class="col-md-12 col-xs-12 text-center bg-white-produto prdt">
                                            <div class="text-center" style="margin-top: 155px;">
                                                <span class="fa fa-plus fa-2x"></span><br>
                                                <span>ver mais</span>
                                            </div>
                                        </div>
                                    </div>',$SSlug.'/pesquisa/0/0/categoria/'.$similares_categoria_slug.'/', ['escape' => false]); 
                                ?>
                            </div>
                        </div>
                        <?php foreach($similares_categoria as $similar_categoria) { ?>
                            <div id="aviseMe-<?= $similar_categoria->id ?>" class="overlay overlay-avise">
                                <div class="overlay-content overlay-avise-<?= $similar_categoria->id ?> text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                                    <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                    <br />
                                    <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$similar_categoria->id])?>
                                    <?= $this->Form->input('produto_id', [
                                        'div'           => false,
                                        'label'         => false,
                                        'type'          => 'hidden',
                                        'val'           => $similar_categoria->id,
                                        'id'            => 'produto_id_acabou-'.$similar_categoria->id
                                    ])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'name_acabou-'.$similar_categoria->id,
                                        'class'         => 'form-control validate[optional]',
                                        'placeholder'   => 'Qual o seu nome?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'email_acabou-'.$similar_categoria->id,
                                        'class'         => 'form-control validate[required, custom[email]]',
                                        'placeholder'   => 'Qual o seu email?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->button('Enviar', [
                                        'data-id'       => $similar_categoria->id,
                                        'type'          => 'button',
                                        'class'         => 'btn btn-success send-acabou-btn'
                                    ])?>
                                    <br />
                                    <br />
                                    <?= $this->Form->end()?>
                                </div>
                            </div> 
                            <div id="overlay-indicacao-<?= $similar_categoria->id ?>" class="overlay overlay-indicacao">
                                <div class="overlay-content text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                                    <h4>Lembrou de alguém quando viu isso?</h4>
                                    <h4>Indique este produto!</h4>
                                    <br>
                                    <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$similar_categoria->id])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[optional] form-indicar name-'.$similar_categoria->id,
                                        'placeholder'   => 'Quem indicou?',
                                    ])?>
                                    <br>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[required] form-indicar email-'.$similar_categoria->id,
                                        'placeholder'   => 'E-mail do destinatário',
                                    ])?>
                                    <?= $this->Form->input('id_produto', [
                                        'div'           => false,
                                        'class'         => 'id_produto-'.$similar_categoria->id,
                                        'value'         => $similar_categoria->id,
                                        'label'         => false,
                                        'type'          => 'hidden'
                                    ])?>
                                    <br>
                                    <?= $this->Form->button('Enviar', [
                                        'type'          => 'button',
                                        'data-id'       => $similar_categoria->id,
                                        'class'         => 'btn btn-success send-indicacao-home-btn'
                                    ])?>
                                    <br>
                                    <br>
                                    <?= $this->Form->end()?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>

                    <?php if(count($similares_objetivo) >= 1) { ?>
                        <div class="title-section">
                            <h3>Produtos do objetivo <span style="color: #5089cf"><?= $similares_objetivo_name ?></span></h3>
                            <?= $this->Html->link('<span class="fa fa-plus"></span> ver todos',
                                $SSlug.'/pesquisa/0/0/objetivo/'.$similares_objetivo_slug.'/', ['escape' => false, 'style' => 'text-transform: uppercase']
                            ); ?>
                        </div>
                        <div class="owl-carousel">
                            <?php $i = 1; ?>
                            <?php foreach($similares_objetivo as $similar_objetivo) { ?>
                                <div class="item">
                                    <?php $fotos = unserialize($similar_objetivo->fotos); ?>
                                    <div class="box-completa-produto" itemtype="http://schema.org/Product">
                                        <div class="tarja-objetivo">
                                            <?php
                                                $contador_objetivos = 0; 
                                                $novo_top = 0;
                                            ?>
                                            <?php foreach ($similar_objetivo->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                                <?php if ($contador_objetivos < 2) { ?>
                                                <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                                                    <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                    $contador_objetivos++;
                                                    $novo_top = $novo_top + 19;
                                                 ?>
                                            <?php } ?>
                                        </div>
                                        <div class="total-tarjas">
                                            <?php if ($similar_objetivo->estoque == 1) { ?>
                                                <div class="tarja tarja-ultimo">
                                                    <span>Último</span>
                                                </div>
                                            <?php } else if ($similar_objetivo->estoque >= 1 && $similar_objetivo->preco_promo) { ?>
                                                <div class="tarja tarja-promocao">
                                                    <span>Promoção</span>
                                                </div>
                                            <?php } else if($similar_objetivo->estoque >= 1 && $similar_objetivo->created > $max_date_novidade) { ?>
                                                <div class="tarja tarja-novidade">
                                                    <span>Novidade</span>
                                                </div>
                                            <?php } ?>               
                                        </div>
                                        <div class="info-produto">
                                            <div class="produto-superior">
                                                <a href="<?= '/comparador/produto/'.$similar_objetivo->slug ?>">
                                                    <?php $fotos = unserialize($similar_objetivo->fotos); ?>
                                                    <div class="foto-produto" id="produto-<?= $i ?>" >
                                                        <?= $this->Html->image(
                                                            'produtos/md-'.$fotos[0],
                                                            ['val' => $similar_objetivo->slug, 'alt' => $similar_objetivo->produto_base->name.' '.$similar_objetivo->produto_base->embalagem_conteudo]); ?>
                                                    </div>
                                                    <div class="detalhes-produto">
                                                        <div class="informacoes-detalhes text-left">
                                                            <p>Código: <?= $similar_objetivo->id ?></p>
                                                            <p><?= strtolower($similar_objetivo->produto_base->marca->name); ?></p>
                                                            <p><?= $similar_objetivo->tamanho.' '.$similar_objetivo->unidade_medida; ?></p>
                                                            <p>para <?= $similar_objetivo->doses; ?> treinos</p>
                                                            <p><?= strtolower($similar_objetivo->propriedade); ?></p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="produto-inferior">
                                                <?php if($similar_objetivo->preco_promo) { ?>
                                                <?php $porcentagem_nova = $similar_objetivo->preco * 0.089;
                                                $preco_corte = $similar_objetivo->preco + $porcentagem_nova; 
                                                } ?> 
                                                <?php $similar_objetivo->preco_promo ?
                                                number_format($preco = $similar_objetivo->preco_promo, 2, ',','.') :
                                                number_format($preco = $similar_objetivo->preco, 2, ',','.'); ?>
                                                <?php $similar_objetivo->preco_promo ? $porcentagem_nova = $similar_objetivo->preco_promo * 0.089 : $porcentagem_nova = $similar_objetivo->preco * 0.089 ?>
                                                <?php if($similar_objetivo->preco_promo != null) {
                                                    if($similar_objetivo->preco_promo < 100.00) { ?>
                                                    <?php $parcel = $similar_objetivo->preco_promo * 0.089; $parcel = ($similar_objetivo->preco_promo + $parcel)/2?>
                                                    <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_objetivo->preco_promo > 100.00 && $similar_objetivo->preco_promo < 200.00) { ?>
                                                    <?php $parcel = $similar_objetivo->preco_promo * 0.089; $parcel = ($similar_objetivo->preco_promo + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_objetivo->preco_promo > 200.00 && $similar_objetivo->preco_promo < 300.00) { ?>
                                                    <?php $parcel = $similar_objetivo->preco_promo * 0.089; $parcel = ($similar_objetivo->preco_promo + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_objetivo->preco_promo > 300.00) { ?>
                                                    <?php $parcel = $similar_objetivo->preco_promo * 0.089; $parcel = ($similar_objetivo->preco_promo + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                    } else {
                                                if($similar_objetivo->preco < 100.00) { ?>
                                                    <?php $parcel = $similar_objetivo->preco * 0.089; $parcel = ($similar_objetivo->preco + $parcel)/2?>
                                                     <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_objetivo->preco > 100.00 && $similar_objetivo->preco < 200.00) { ?>
                                                    <?php $parcel = $similar_objetivo->preco * 0.089; $parcel = ($similar_objetivo->preco + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_objetivo->preco > 200.00 && $similar_objetivo->preco < 300.00) { ?>
                                                    <?php $parcel = $similar_objetivo->preco * 0.089; $parcel = ($similar_objetivo->preco + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_objetivo->preco > 300.00) { ?>
                                                    <?php $parcel = $similar_objetivo->preco * 0.089; $parcel = ($similar_objetivo->preco + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                } ?>
                                                <?php 
                                                    if($similar_objetivo->preco_promo) {
                                                        echo $this->Html->link("
                                                            <h4>".$similar_objetivo->produto_base->name."</h4>
                                                            <p><span class='preco-corte'> R$".number_format($preco_corte, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            '/comparador/produto/'.$similar_objetivo->slug,
                                                            ['escape' => false]);
                                                    } else {
                                                        echo $this->Html->link("
                                                            <h4>".$similar_objetivo->produto_base->name."</h4>
                                                            <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            '/comparador/produto/'.$similar_objetivo->slug,
                                                            ['escape' => false]);
                                                    }
                                                ?>
                                            </div>
                                            <div class="hover-produto <?= $similar_objetivo->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                                                <div class="col-xs-12">
                                                    <div class="hexagono-indicar">
                                                        <?php if($similar_objetivo->status_id == 1) { ?>
                                                            <div class="hexagon-disponivel btn-indique" data-id="<?= $similar_objetivo->id ?>">
                                                                <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                                                <p>indique</p>
                                                            </div>
                                                            <script>
                                                                $('.btn-indique').click(function () {
                                                                        var id_produto = $(this).attr('data-id');
                                                                        $('#overlay-indicacao-'+id_produto).height('100%');
                                                                        $("html,body").css({"overflow":"hidden"});
                                                                });
                                                                function closeIndica() {
                                                                    $('.overlay-indicacao').height('0%');
                                                                    $("html,body").css({"overflow":"auto"});
                                                                }
                                                            </script>
                                                        <?php } else { ?>
                                                            <div class="hexagon produto-acabou-btn" data-id="<?= $similar_objetivo->id ?>">
                                                                <span><i class="fa fa-envelope-o"></i></span>
                                                                <p>avise-me</p>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <?php if($similar_objetivo->status_id == 1) { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon-disponivel">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,'/comparador/produto/'.$similar_objetivo->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon-disponivel btn-comprar-overlay">
                                                            <span><?= $this->Html->image('mochila_log_branca.png',['alt' => 'Mochila LOG', 'class' => 'comprar-hexag']) ?></span>
                                                            <p>comprar</p>
                                                        </div>
                                                    </div>
                                                <?php }
                                                else { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,'/comparador/produto/'.$similar_objetivo->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon">
                                                            <span>ver</span>
                                                            <p>similares</p>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="botoes-comprar-similiar">
                                            <?php if($similar_objetivo->visivel == 1 && $similar_objetivo->status_id == 1) { ?>
                                                <button class="btn btn-comparar btn-comparar-action" data-id="<?= $similar_objetivo->id ?>"><i class="fa fa-balance-scale"></i> Comparar</button>
                                                <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $similar_objetivo->id ?>">Indique</button>
                                                <button class="desktop-hidden btn btn-incluir btn-comprar-overlay">Comprar</button> 
                                            <?php } else if($similar_objetivo->visivel == 1 && $similar_objetivo->status_id == 2) { ?>
                                                <button class="btn btn-comparar btn-comparar-action" data-id="<?= $similar_objetivo->id ?>"><i class="fa fa-balance-scale"></i> Comparar</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++; ?>
                            <?php } ?>
                            <div class="item">
                                <?= $this->Html->link('
                                    <div class="box-completa-produto" id="produto-<?= $i ?>">
                                        <div class="col-md-12 col-xs-12 text-center bg-white-produto prdt">
                                            <div class="text-center" style="margin-top: 155px;">
                                                <span class="fa fa-plus fa-2x"></span><br>
                                                <span>ver mais</span>
                                            </div>
                                        </div>
                                    </div>',$SSlug.'/pesquisa/0/0/objetivo/'.$similares_objetivo_slug.'/', ['escape' => false]); 
                                ?>
                            </div>
                        </div>
                        <?php foreach($similares_objetivo as $similar_objetivo) { ?>
                            <div id="aviseMe-<?= $similar_objetivo->id ?>" class="overlay overlay-avise">
                                <div class="overlay-content overlay-avise-<?= $similar_objetivo->id ?> text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                                    <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                    <br />
                                    <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$similar_objetivo->id])?>
                                    <?= $this->Form->input('produto_id', [
                                        'div'           => false,
                                        'label'         => false,
                                        'type'          => 'hidden',
                                        'val'           => $similar_objetivo->id,
                                        'id'            => 'produto_id_acabou-'.$similar_objetivo->id
                                    ])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'name_acabou-'.$similar_objetivo->id,
                                        'class'         => 'form-control validate[optional]',
                                        'placeholder'   => 'Qual o seu nome?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'email_acabou-'.$similar_objetivo->id,
                                        'class'         => 'form-control validate[required, custom[email]]',
                                        'placeholder'   => 'Qual o seu email?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->button('Enviar', [
                                        'data-id'       => $similar_objetivo->id,
                                        'type'          => 'button',
                                        'class'         => 'btn btn-success send-acabou-btn'
                                    ])?>
                                    <br />
                                    <br />
                                    <?= $this->Form->end()?>
                                </div>
                            </div> 
                            <div id="overlay-indicacao-<?= $similar_objetivo->id ?>" class="overlay overlay-indicacao">
                                <div class="overlay-content text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                                    <h4>Lembrou de alguém quando viu isso?</h4>
                                    <h4>Indique este produto!</h4>
                                    <br>
                                    <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$similar_objetivo->id])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[optional] form-indicar name-'.$similar_objetivo->id,
                                        'placeholder'   => 'Quem indicou?',
                                    ])?>
                                    <br>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[required] form-indicar email-'.$similar_objetivo->id,
                                        'placeholder'   => 'E-mail do destinatário',
                                    ])?>
                                    <?= $this->Form->input('id_produto', [
                                        'div'           => false,
                                        'class'         => 'id_produto-'.$similar_objetivo->id,
                                        'value'         => $similar_objetivo->id,
                                        'label'         => false,
                                        'type'          => 'hidden'
                                    ])?>
                                    <br>
                                    <?= $this->Form->button('Enviar', [
                                        'type'          => 'button',
                                        'data-id'       => $similar_objetivo->id,
                                        'class'         => 'btn btn-success send-indicacao-home-btn'
                                    ])?>
                                    <br>
                                    <br>
                                    <?= $this->Form->end()?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>

                    <?php if(count($similares_marca) >= 1) { ?>
                        <div class="title-section">
                            <h3>Produtos da marca <span style="color: #5089cf"><?= $similares_marca_name ?></span></h3>
                            <?= $this->Html->link('<span class="fa fa-plus"></span> ver todos',
                                $SSlug.'/pesquisa/0/'.$similares_marca_slug.'/', ['escape' => false, 'style' => 'text-transform: uppercase']
                            ); ?>
                        </div>
                        <div class="owl-carousel">
                            <?php $i = 1; ?>
                            <?php foreach($similares_marca as $similar_marca) { ?>
                                <div class="item">
                                    <?php $fotos = unserialize($similar_marca->fotos); ?>
                                    <div class="box-completa-produto" itemtype="http://schema.org/Product">
                                        <div class="tarja-objetivo">
                                            <?php
                                                $contador_objetivos = 0; 
                                                $novo_top = 0;
                                            ?>
                                            <?php foreach ($similar_marca->produto_base->produto_objetivos as $produto_objetivo) { ?>
                                                <?php if ($contador_objetivos < 2) { ?>
                                                <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                                                    <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                                                </div>
                                                <?php } ?>
                                                <?php
                                                    $contador_objetivos++;
                                                    $novo_top = $novo_top + 19;
                                                 ?>
                                            <?php } ?>
                                        </div>
                                        <div class="total-tarjas">
                                            <?php if ($similar_marca->estoque == 1) { ?>
                                                <div class="tarja tarja-ultimo">
                                                    <span>Último</span>
                                                </div>
                                            <?php } else if ($similar_marca->estoque >= 1 && $similar_marca->preco_promo) { ?>
                                                <div class="tarja tarja-promocao">
                                                    <span>Promoção</span>
                                                </div>
                                            <?php } else if($similar_marca->estoque >= 1 && $similar_marca->created > $max_date_novidade) { ?>
                                                <div class="tarja tarja-novidade">
                                                    <span>Novidade</span>
                                                </div>
                                            <?php } ?>               
                                        </div>
                                        <div class="info-produto">
                                            <div class="produto-superior">
                                                <a href="<?= '/comparador/produto/'.$similar_marca->slug ?>">
                                                    <?php $fotos = unserialize($similar_marca->fotos); ?>
                                                    <div class="foto-produto" id="produto-<?= $i ?>" >
                                                        <?= $this->Html->image(
                                                            'produtos/md-'.$fotos[0],
                                                            ['val' => $similar_marca->slug, 'alt' => $similar_marca->produto_base->name.' '.$similar_marca->produto_base->embalagem_conteudo]); ?>
                                                    </div>
                                                    <div class="detalhes-produto">
                                                        <div class="informacoes-detalhes text-left">
                                                            <p>Código: <?= $similar_marca->id ?></p>
                                                            <p><?= strtolower($similar_marca->produto_base->marca->name); ?></p>
                                                            <p><?= $similar_marca->tamanho.' '.$similar_marca->unidade_medida; ?></p>
                                                            <p>para <?= $similar_marca->doses; ?> treinos</p>
                                                            <p><?= strtolower($similar_marca->propriedade); ?></p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="produto-inferior">
                                                <?php if($similar_marca->preco_promo) { ?>
                                                <?php $porcentagem_nova = $similar_marca->preco * 0.089;
                                                $preco_corte = $similar_marca->preco + $porcentagem_nova; 
                                                } ?> 
                                                <?php $similar_marca->preco_promo ?
                                                number_format($preco = $similar_marca->preco_promo, 2, ',','.') :
                                                number_format($preco = $similar_marca->preco, 2, ',','.'); ?>
                                                <?php $similar_marca->preco_promo ? $porcentagem_nova = $similar_marca->preco_promo * 0.089 : $porcentagem_nova = $similar_marca->preco * 0.089 ?>
                                                <?php if($similar_marca->preco_promo != null) {
                                                    if($similar_marca->preco_promo < 100.00) { ?>
                                                    <?php $parcel = $similar_marca->preco_promo * 0.089; $parcel = ($similar_marca->preco_promo + $parcel)/2?>
                                                    <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_marca->preco_promo > 100.00 && $similar_marca->preco_promo < 200.00) { ?>
                                                    <?php $parcel = $similar_marca->preco_promo * 0.089; $parcel = ($similar_marca->preco_promo + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_marca->preco_promo > 200.00 && $similar_marca->preco_promo < 300.00) { ?>
                                                    <?php $parcel = $similar_marca->preco_promo * 0.089; $parcel = ($similar_marca->preco_promo + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_marca->preco_promo > 300.00) { ?>
                                                    <?php $parcel = $similar_marca->preco_promo * 0.089; $parcel = ($similar_marca->preco_promo + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                    } else {
                                                if($similar_marca->preco < 100.00) { ?>
                                                    <?php $parcel = $similar_marca->preco * 0.089; $parcel = ($similar_marca->preco + $parcel)/2?>
                                                     <?php $maximo_parcelas = "2x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_marca->preco > 100.00 && $similar_marca->preco < 200.00) { ?>
                                                    <?php $parcel = $similar_marca->preco * 0.089; $parcel = ($similar_marca->preco + $parcel)/3?>
                                                    <?php $maximo_parcelas = "3x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_marca->preco > 200.00 && $similar_marca->preco < 300.00) { ?>
                                                    <?php $parcel = $similar_marca->preco * 0.089; $parcel = ($similar_marca->preco + $parcel)/5?>
                                                    <?php $maximo_parcelas = "5x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php } else if($similar_marca->preco > 300.00) { ?>
                                                    <?php $parcel = $similar_marca->preco * 0.089; $parcel = ($similar_marca->preco + $parcel)/6?>
                                                    <?php $maximo_parcelas = "6x";
                                                     $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                                                <?php }
                                                } ?>
                                                <?php 
                                                    if($similar_marca->preco_promo) {
                                                        echo $this->Html->link("
                                                            <h4>".$similar_marca->produto_base->name."</h4>
                                                            <p><span class='preco-corte'> R$".number_format($preco_corte, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            '/comparador/produto/'.$similar_marca->slug,
                                                            ['escape' => false]);
                                                    } else {
                                                        echo $this->Html->link("
                                                            <h4>".$similar_marca->produto_base->name."</h4>
                                                            <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                                            <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                                            '/comparador/produto/'.$similar_marca->slug,
                                                            ['escape' => false]);
                                                    }
                                                ?>
                                            </div>
                                            <div class="hover-produto <?= $similar_marca->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                                                <div class="col-xs-12">
                                                    <div class="hexagono-indicar">
                                                        <?php if($similar_marca->status_id == 1) { ?>
                                                            <div class="hexagon-disponivel btn-indique" data-id="<?= $similar_marca->id ?>">
                                                                <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                                                <p>indique</p>
                                                            </div>
                                                            <script>
                                                                $('.btn-indique').click(function () {
                                                                        var id_produto = $(this).attr('data-id');
                                                                        $('#overlay-indicacao-'+id_produto).height('100%');
                                                                        $("html,body").css({"overflow":"hidden"});
                                                                });
                                                                function closeIndica() {
                                                                    $('.overlay-indicacao').height('0%');
                                                                    $("html,body").css({"overflow":"auto"});
                                                                }
                                                            </script>
                                                        <?php } else { ?>
                                                            <div class="hexagon produto-acabou-btn" data-id="<?= $similar_marca->id ?>">
                                                                <span><i class="fa fa-envelope-o"></i></span>
                                                                <p>avise-me</p>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <?php if($similar_marca->status_id == 1) { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon-disponivel">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,'/comparador/produto/'.$similar_marca->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon-disponivel btn-comprar-overlay">
                                                            <span><?= $this->Html->image('mochila_log_branca.png',['alt' => 'Mochila LOG', 'class' => 'comprar-hexag']) ?></span>
                                                            <p>comprar</p>
                                                        </div>
                                                    </div>
                                                <?php }
                                                else { ?>
                                                    <div class="col-xs-6">
                                                        <?= $this->Html->link('
                                                            <div class="hexagon">
                                                                <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                                                <p>detalhes</p>
                                                            </div>'
                                                            ,'/comparador/produto/'.$similar_marca->slug,
                                                            ['escape' => false]);
                                                        ?>
                                                    </div>
                                                    <div class="col-xs-6 text-center compare">
                                                        <div class="hexagon">
                                                            <span>ver</span>
                                                            <p>similares</p>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="botoes-comprar-similiar">
                                            <?php if($similar_marca->visivel == 1 && $similar_marca->status_id == 1) { ?>
                                                <button class="btn btn-comparar btn-comparar-action" data-id="<?= $produto->id ?>"></i> Comparar</button>
                                                <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $similar_marca->id ?>">Indique</button>
                                                <button class="desktop-hidden btn btn-incluir btn-comprar-overlay">Comprar</button> 
                                            <?php } else if($similar_marca->visivel == 1 && $similar_marca->status_id == 2) { ?>
                                                <button class="btn btn-comparar btn-comparar-action" data-id="<?= $produto->id ?>"><i class="fa fa-balance-scale"></i> Comparar</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++; ?>
                            <?php } ?>
                            <div class="item">
                                <?= $this->Html->link('
                                    <div class="box-completa-produto" id="produto-<?= $i ?>">
                                        <div class="col-md-12 col-xs-12 text-center bg-white-produto prdt">
                                            <div class="text-center" style="margin-top: 155px;">
                                                <span class="fa fa-plus fa-2x"></span><br>
                                                <span>ver mais</span>
                                            </div>
                                        </div>
                                    </div>',$SSlug.'pesquisa/0/'.$similares_marca_slug.'/', ['escape' => false]); 
                                ?>
                            </div>
                        </div>
                        <?php foreach($similares_marca as $similar_marca) { ?>
                            <div id="aviseMe-<?= $similar_marca->id ?>" class="overlay overlay-avise">
                                <div class="overlay-content overlay-avise-<?= $similar_marca->id ?> text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
                                    <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
                                    <br />
                                    <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$similar_marca->id])?>
                                    <?= $this->Form->input('produto_id', [
                                        'div'           => false,
                                        'label'         => false,
                                        'type'          => 'hidden',
                                        'val'           => $similar_marca->id,
                                        'id'            => 'produto_id_acabou-'.$similar_marca->id
                                    ])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'name_acabou-'.$similar_marca->id,
                                        'class'         => 'form-control validate[optional]',
                                        'placeholder'   => 'Qual o seu nome?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'id'            => 'email_acabou-'.$similar_marca->id,
                                        'class'         => 'form-control validate[required, custom[email]]',
                                        'placeholder'   => 'Qual o seu email?',
                                    ])?>
                                    <br/>
                                    <?= $this->Form->button('Enviar', [
                                        'data-id'       => $similar_marca->id,
                                        'type'          => 'button',
                                        'class'         => 'btn btn-success send-acabou-btn'
                                    ])?>
                                    <br />
                                    <br />
                                    <?= $this->Form->end()?>
                                </div>
                            </div> 
                            <div id="overlay-indicacao-<?= $similar_marca->id ?>" class="overlay overlay-indicacao">
                                <div class="overlay-content text-center">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
                                    <h4>Lembrou de alguém quando viu isso?</h4>
                                    <h4>Indique este produto!</h4>
                                    <br>
                                    <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$similar_marca->id])?>
                                    <?= $this->Form->input('name', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[optional] form-indicar name-'.$similar_marca->id,
                                        'placeholder'   => 'Quem indicou?',
                                    ])?>
                                    <br>
                                    <?= $this->Form->input('email', [
                                        'div'           => false,
                                        'label'         => false,
                                        'class'         => 'form-control validate[required] form-indicar email-'.$similar_marca->id,
                                        'placeholder'   => 'E-mail do destinatário',
                                    ])?>
                                    <?= $this->Form->input('id_produto', [
                                        'div'           => false,
                                        'class'         => 'id_produto-'.$similar_marca->id,
                                        'value'         => $similar_marca->id,
                                        'label'         => false,
                                        'type'          => 'hidden'
                                    ])?>
                                    <br>
                                    <?= $this->Form->button('Enviar', [
                                        'type'          => 'button',
                                        'data-id'       => $similar_marca->id,
                                        'class'         => 'btn btn-success send-indicacao-home-btn'
                                    ])?>
                                    <br>
                                    <br>
                                    <?= $this->Form->end()?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script> 
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            autoplay:true,
            autoplayTimeout:600,
            autoplayHoverPause:true,
            pagination: false,
            margin: 10,
            navigation: true,
            navigationText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
            itemsMobile: [600, 1],
            itemsDesktopSmall: [1024, 2],
            itemsDesktop: [6000, 3]
        });
    });
</script>

<script type="text/javascript">
    $('.send-indicacao-home-btn').click(function(e) {
        e.preventDefault();
        var botao = $(this);
    
        botao.html('Enviando...');
        botao.attr('disabled', 'disabled');

        var id = $(this).attr('data-id');
        var slug = '<?= $SSlug ?>';

        var valid = $('.indicar_prod_home_'+id).validationEngine("validate");
        if (valid == true) {
            $.ajax({
                type: "POST",
                url: WEBROOT_URL + slug + '/produtos/overlay-indicar-produtos-home/',
                data: {
                    name: $('.name-'+id).val(),
                    email: $('.email-'+id).val(), 
                    produto_id: $('.id_produto-'+id).val()
                }
            })
            .done(function (data) {
                if(data == 1) {
                    botao.html('Enviado!');
                    setTimeout(function () {
                        botao.html('Enviar');
                        botao.removeAttr('disabled');
                    }, 2200);
                } else {
                    botao.html('Falha ao enviar... Tente novamente...');
                    setTimeout(function () {
                        botao.html('Enviar');
                        botao.removeAttr('disabled');
                    }, 2200);
                }
            });
        }else{
            $('.indicar_prod_home_'+id).validationEngine({
                updatePromptsPosition: true,
                promptPosition: 'inline',
                scroll: false
            });
        }

        $('.abrir-email').click(function(e){
            var id = $(this).attr('data-id');
            $('.enviar-email').toggle("slow");
        });
    });
</script>
<script>
    $('.abrir-email').click(function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('.enviar-whats').slideUp("slow");
        $('.enviar-email').slideToggle("slow");
    });
    $('.abrir-whats').click(function(e){
        var id = $(this).attr('data-id');
        $('.enviar-email').slideUp("slow");
        $('.enviar-whats').slideToggle("slow");
    });
</script>