<?php $fotos = unserialize($produto->fotos); ?>
<!-- CONTENT -->
<div class="col-lg-12 col-xs-12 text-center promo">
    <h2 class="titulo">
        <?= $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo.' '.$produto->propriedade; ?>
    </h2>
</div>
<div class="container promo promo-mobile text-center">
    <span>+++</span>
    <br>
    <span>VENDIDOS</span>
    <div class="col-lg-12 col-xs-12">
        <div class="button"><a href="" class="text-center">PRESENTEIE <br>UM AMIGO</a></div>
    </div>
    <span class="helper"></span>
    <?= $this->Html->image('produtos/md-'.$fotos[0], [
        'id'    => 'foto-produto',
        'class' => 'img-produto-principal',
        'alt'   => $produto->produto_base->name.' '.$produto->propriedade,
    ])?>
    <span class="col-lg-12 col-xs-12">
            <?= $produto->preco_promo ? 'De <span class="de">R$ '.number_format($produto->preco, 2, ',','.').'</span>' : ''; ?>
        </span>
    <br class="clear">
    <span class="col-lg-12 col-xs-12">por </span>
        <span class="col-lg-12 col-xs-12 promo-price">
            R$ <?= $produto->preco_promo ?
                number_format($preco = $produto->preco_promo, 2, ',','.') :
                number_format($preco = $produto->preco, 2, ',','.'); ?>
        </span>
    <br class="clear">
    <?= $this->Html->link(
        'COMPRAR',
        ['controller' => 'Produtos', 'action' => 'comprar', $produto->slug],
        ['escape' => false, 'class' => 'promo-purchase']
    )?>
</div>
<div class="container promo promo-desk">
    <div class="mais-vendidos bg-white text-center bloco">
        <span>+++</span>
        <br>
        <span>VENDIDOS</span>
        <div class="col-lg-12 col-xs-12">
            <div class="button"><a href="" class="text-center">PRESENTEIE <br>UM AMIGO</a></div>
        </div>
    </div>
    <div class="promo-produto bg-white bloco-central text-center">
        <span class="helper"></span>
        <?= $this->Html->image('produtos/md-'.$fotos[0], [
            'id'    => 'foto-produto',
            'class' => 'img-produto-principal',
            'alt'   => $produto->produto_base->name.' '.$produto->propriedade,
        ])?>
    </div>
    <div class="promo-price-bloco bg-white bloco">
        <span class="col-lg-12 col-xs-12">
            <?= $produto->preco_promo ? 'De <span class="de">R$ '.number_format($produto->preco, 2, ',','.').'</span>' : ''; ?>
        </span>
        <br class="clear">
        <span class="col-lg-12 col-xs-12">por </span>
        <span class="col-lg-12 col-xs-12 promo-price">
            R$ <?= $produto->preco_promo ?
                number_format($preco = $produto->preco_promo, 2, ',','.') :
                number_format($preco = $produto->preco, 2, ',','.'); ?>
        </span>
        <br class="clear">
        <?= $this->Html->link(
            'COMPRAR',
            ['controller' => 'Produtos', 'action' => 'comprar', $produto->slug],
            ['escape' => false, 'class' => 'promo-purchase']
        )?>
    </div>
</div>
<div class="container text-center promo">
    <div class="col-lg-55 tempo bloco-central">
        <span class="col-lg-12 col-xs-12 time" id="countdown">6h 36m 11s</span>
        <span class="col-lg-12 col-xs-12">TEMPO RESTANTE</span>
    </div>
</div>
<div class="container info-central-produto">
    <div class="row bg-white">
        <div class="col-lg-6 col-xs-12">
            <span class="title">DESCRIÇÃO</span>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis delectus libero nisi provident quidem quis sed voluptates? Alias assumenda beatae consequuntur, culpa molestiae molestias nam provident saepe vitae voluptatum?
            </p>
        </div>
        <div class="col-lg-6 col-xs-12">
            <span class="title">DICAS</span>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis delectus libero nisi provident quidem quis sed voluptates? Alias assumenda beatae consequuntur, culpa molestiae molestias nam provident saepe vitae voluptatum?
            </p>
        </div>
    </div>
</div>

<!--COMBINA COM-->
<div class="container">
    <div class="col-md-12 col-xs-12 combina-com">
        <div class="">
            <h2 class="h2-combina-com">ESTE PRODUTO COMBINA COM</h2>

            <?php
            foreach($produto_combinations as $combination):
                $fotos = unserialize($combination->fotos);
                ?>
                <div class="col-md-3 produto">
                    <div class="combina-img">
                        <span class="helper"></span>
                        <?= $this->Html->image('produtos/'.$fotos[0],
                            [
                                'alt'   => $combination->produto_base->name.' '
                                    .$combination->produto_base->embalagem_conteudo.' '.$combination->propriedade,
                                'title' => $combination->produto_base->name.' '
                                    .$combination->produto_base->embalagem_conteudo.' '.$combination->propriedade,
                                'class' => 'img-combina-com'
                            ]
                        ); ?>
                    </div>
                    <div>
                        <button class="combina-carrinho">
                            <span class="col-lg-9">
                                <?=
                                $this->Html->link(
                                    'COLOCAR NA<br>MOCHILA',
                                    ['controller' => 'Produtos', 'action' => 'comprar', $combination->slug],
                                    ['escape' => false, 'class' => '']
                                )
                                ; ?>

                            </span>
                            <span class="col-lg-3 img">
                                <?=
                                $this->Html->link(
                                    $this->Html->image('mochila-compras-log-fitness-branca.png',
                                        [
                                            'alt'   => 'Colocar na mochila '.$combination->produto_base->name.' '
                                                .$combination->produto_base->embalagem_conteudo.' '.$combination->propriedade,
                                            'title' => 'Colocar na mochila '.$combination->produto_base->name.' '
                                                .$combination->produto_base->embalagem_conteudo.' '.$combination->propriedade
                                        ]
                                    ),
                                    ['controller' => 'Produtos', 'action' => 'comprar', $combination->slug],
                                    ['escape' => false, 'class' => '']
                                )
                                ; ?>
                            </span>
                        </button>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>