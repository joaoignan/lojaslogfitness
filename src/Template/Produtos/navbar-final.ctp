<style>
    @media all and (min-width: 769px) {
      .sub-menu {
          color: white;
      }
      .btn-login-nav {
          padding: 1px 6px;
          margin-top: 6px;
          line-height: 1.3em !important;
          border-color: #8dc73f;
          background-color: #8dc73f;
          color: white;
      }
      .icones {
          margin-top: 40px;
          float: left;
      }
      .notify {
          right: -216px;
          top: 50px;
      }
      .profile {
          display: none;
          position: absolute;
          width: 200px;
          min-height: 120px;
          background: #FFF;
          z-index: 9;
          right: 50px;
          top: 38px;
          color: #333;
          padding: 2px 10px;
          border-radius: 0 0 5px 5px;
          border: 1px solid rgba(80,135,199,0.59);
          box-shadow: -1px 1px 8px #AAA;
      }
      .profile span {
          display: block;
          font-size: 20px;
          line-height: 30px;
          font-weight: 700;
          position: relative;
      }
      .sub-icones i:hover {
          color: #23527c;
      }
      #bem-vindo {
          color: #5089cf;
          font-size: 2em!important;
          margin-top: -2px;
      }
      .menu-principal {
          width: 100%;
          box-shadow: 1px 1px 5px 0px rgba(0,0,0,0.75);
      }
      .icones-deslogado {
          float: left;
          right: 40px;
          margin-top: 15px;
      }
      .icones-logado {
          float: left;
          margin-right: 20px;
          margin-top: 32px;
      }
      .navbar-header {
          text-align: center;
      }
      .logo-topo {
          margin-top: 7px;
      }
      .logo-topo-logfitness {
          margin-top: 15px;
      }
      .menu-principal {
          height: 110px;
      }
      .button-busca{
          background-color: #5089CF;
          border: 1px solid #5089CF;
          padding: 1px 15px 3px 15px;
          margin: 0;
          right: 0;
          position: absolute;
          float: right;
          color: #fff;
      }
      .logo-logfitness img{
          width: 85%;
      }
      .logo-margin {
          margin-left: 70px!important;
      }
      nav {
          position: fixed;
          z-index: 200;
          width: 100%;
      }
      .reparo-nav {
          height: 110px;
          width: 100%;
          background-color: white;
      }
      .sub-menu {
          color: #5089cf;
      }
    }
    
    @media all and (max-width: 1200px) {
        .form-busca {
            width: 80%!important;
        }
    }
    @media all and (max-width: 950px) {
        .icones-logado {
            width: 23%;
        }
        .navbar-header {
            width: 180px;
        }
    }

    @media all and (max-width: 768px) {
        .navbar-mobile {
          width: 100%;
          height: 50px;
          background-color: white;
          z-index: 3100;
          position: fixed;
          top: 0;
          box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
        }

        .menu-mobile {
          display: none; 
          width: 100%;
          height: 100%;
          background-color: white;
          z-index: 4000;
          position: fixed;
          text-align: left;
          top: 50px;
          box-shadow: 0px 3px 5px 0px rgba(50,50,50,0.75);
          padding-top: 10px;
          padding-bottom: 50px;
          overflow-y: auto;
        }

        .menu-mobile p,
        .menu-mobile span {
          color: #5089cf;
          font-weight: 700;
          font-size: 18px;
          padding-left: 25px;
        }

        .menu-mobile i,
        .menu-mobile img {
          width: 20px;
        }

        .search-navbar-mobile {
          display: none; 
          width: 100%;
          height: 40px;
          background-color: white;
          z-index: 3000;
          position: fixed;
          top: 50px;
          box-shadow: 0px 0px 5px 1px rgba(50,50,50,0.75);
        }
        
        .c-hamburger {
          display: block;
          position: relative;
          overflow: hidden;
          margin: 0;
          margin-left: 7px;
          padding: 0;
          width: 50px;
          height: 50px;
          font-size: 0;
          text-indent: -9999px;
          appearance: none;
          box-shadow: none;
          border-radius: none;
          border: none;
          cursor: pointer;
          transition: background 0.3s;
          z-index: 400;
        }

        .c-hamburger:focus {
          outline: none;
        }

        .c-hamburger span {
          display: block;
          position: absolute;
          top: 21px;
          left: 8px;
          right: 8px;
          height: 6px;
          background: #5089cf;
        }

        .c-hamburger span::before,
        .c-hamburger span::after {
          position: absolute;
          display: block;
          left: 0;
          width: 100%;
          height: 6px;
          background-color: #5089cf;
          content: "";
        }

        .c-hamburger span::before {
          top: -12px;
        }

        .c-hamburger span::after {
          bottom: -12px;
        }

        .c-hamburger--htx {
          background-color: transparent;
        }

        .c-hamburger--htx span {
          transition: background 0s 0.3s;
        }

        .c-hamburger--htx span::before,
        .c-hamburger--htx span::after {
          transition-duration: 0.3s, 0.3s;
          transition-delay: 0.3s, 0s;
        }

        .c-hamburger--htx span::before {
          transition-property: top, transform;
        }

        .c-hamburger--htx span::after {
          transition-property: bottom, transform;
        }

        .c-hamburger--htx.is-active span {
          background: none;
        }

        .c-hamburger--htx.is-active span::before {
          top: 0;
          transform: rotate(45deg);
        }

        .c-hamburger--htx.is-active span::after {
          bottom: 0;
          transform: rotate(-45deg);
        }

        .c-hamburger--htx.is-active span::before,
        .c-hamburger--htx.is-active span::after {
          transition-delay: 0s, 0.3s;
        }

        .logo-topo {
            max-height: 40px;
            width: auto;
        }
        .box-search-mobile {
            float: right;
            height: 50px;
            width: 60px;
            line-height: 60px;
            position: absolute;
            text-align: center;
            top: 0;
            right: 0;
            z-index: 400;
        }
        .box-logo-mobile {
            width: 100%;
            height: 50px;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .academia-logo {
            font-size: 1.2em;
            line-height: 50px;
        }
        .button-busca {
            right: 15px;
            height: 34px;
        }
        .form-search {
            border-radius: 0;
            position: absolute;
            width: 91%!important;
        }
        .navbar-mobile .col-xs-3,
        .navbar-mobile .col-xs-6 {
            padding: 0;
        }
        .btn-login-nav {
            padding: 1px 6px;
            margin-top: 6px;
            line-height: 1.3em !important;
            border-color: #8dc73f;
            background-color: #8dc73f;
            color: white;
            width: 130px;
        }
        .separador {
            width: 100%;
            border: 1px solid #5087c7;
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .notify {
            width: 100%;
            left: 0;
            top: 0px;
            position: relative;
            border: none;
            min-height: inherit;
        }
    }

    #navbar_pc_config,
    #navbar_mobile_config {
      display: none
    }
</style>

<script>
    $(document).ready(function() {
        function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
        }
        
        var notif = getCookie("notif");
        var notif_pos = <?= count($notifications) ?>;

        if (notif_pos != notif) {
            $('.fa-globe').addClass('notify-badge badge-position');
            $('#notify-button').click(function() {
                $('.fa-globe').removeClass('notify-badge badge-position');
                setCookie("notif", notif_pos, 600);
            });
        } else {
            $('#notify-button').click(function() {
                $('.fa-globe').removeClass('notify-badge badge-position');
                setCookie("notif", notif_pos, 600);
            });
        }

        $('#logado').click(function() {
            $('.profile').fadeToggle();
        });
        $('#notify-button').click(function() {
            $('.notify').fadeToggle();
        });

        var ctrl_hamburger = 0;
        var search_aberto = 0;
        var medida_tela = $(window).width();

        if(medida_tela <= 768) {
          $('.c-hamburger').click(function() {
            if(ctrl_hamburger == 0) {
                $(this).addClass('is-active');
                $('.menu-mobile').slideDown();
                $('body').css('overflow', 'hidden');

                if($('.filtro').width() > 1) {
                  $('.close-filter').click();
                }

                if($('.mochila').width() > 1) {
                  $('.close-mochila').click();
                }

                if(search_aberto == 1) {
                  $('#botao-search').click();
                }

                ctrl_hamburger = 1;
            } else {
                $(this).removeClass('is-active');
                $('.menu-mobile').slideUp(600); 
                $('body').css('overflow', 'auto');
                ctrl_hamburger = 0;
            }

            if(search_aberto == 1 || ctrl_hamburger == 1) {
              $('.navbar-mobile').css('box-shadow', 'none');
            } else {
              $('.navbar-mobile').css('box-shadow', '0px 0px 5px 1px rgba(50,50,50,0.75)');
            }
          });

          $('#botao-search').click(function(e) {
            e.preventDefault();
            if(search_aberto == 0) {
              $('.search-navbar-mobile').slideDown();

              if($('.filtro').width() > 1) {
                $('.close-filter').click();
              }

              if(ctrl_hamburger == 1) {
                $('.c-hamburger').click();
              }

              if($('.mochila').width() > 1) {
                $('.close-mochila').click();
              }

              search_aberto = 1;
            } else {
              $('.search-navbar-mobile').slideUp(600);
              search_aberto = 0;
            }

            if(search_aberto == 1 || ctrl_hamburger == 1) {
              $('.navbar-mobile').css('box-shadow', 'none');
            } else {
              $('.navbar-mobile').css('box-shadow', '0px 0px 5px 1px rgba(50,50,50,0.75)');
            }
          });

          $('.close-filter, .close-mochila').click(function() {
          if(search_aberto == 1) {
            $('#botao-search').click();
          }
          if(ctrl_hamburger == 1) {
            $('.c-hamburger').click();
          }
          });

          $('.close-mochila').click(function() {
          if($('.filtro').width() > 1) {
            $('.close-filter').click();
          }
          });

          $('.close-filter').click(function() {
          if($('.mochila').width() > 1) {
            $('.close-mochila').click();
          }
          });

          $('#notificacoes-mobile').click(function() {
            $('.notify').slideToggle();
          });
        }
    });
</script>

<div id="navbar_pc_config">
<nav class="navbar-default" id="menu-principal">
    <div class="container menu-principal" >
        <div id="logo-esquerdo" class="navbar-header logo-margin">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="helper"></span>

            <?php
            if(isset($ACADEMIA)){
                if($ACADEMIA->image != null &&  $ACADEMIA->image != '' && $ACADEMIA->image != 'default.png'){
                    echo $this->Html->link(
                        $this->Html->image('academias/'.$ACADEMIA->image, ['alt' => 'LogFitness', 'class' => 'logo-topo']),
                        $SSlug.'/home',
                        ['escape' => false, 'class' => 'inline-block']
                    );
                    ?>
                    <script>
                        $('#logo-esquerdo').removeClass('logo-margin');
                    </script>
                    <?php
                }else{
                    echo $this->Html->link(
                        '<span class="academia-logo">'
                        .str_replace(array('academia', 'Academia'), '', $ACADEMIA->shortname)
                        .'</span>',
                        $SSlug.'/home',
                        ['escape' => false, 'class' => 'inline-block link-logo']
                    );
                }
            }else{
                echo $this->Html->link(
                    $this->Html->image('logfitness.png', ['alt' => 'LogFitness', 'class' => 'logo-topo']),
                    $SSlug.'/home',
                    ['escape' => false, 'class' => 'inline-block']
                );
            }
            ?>
        </div>
        <div class="navbar-header logo-logfitness" style="float: right;">
            <span class="helper"></span>
            <?= isset($ACADEMIA) ?
                $this->Html->link(
                    $this->Html->image('logfitness.png', ['alt' => 'LogFitness', 'class' => 'logo-topo-logfitness']),
                    $SSlug.'/home',
                    ['escape' => false, 'class' => 'inline-block']
                ) : '';?>
        </div>
        <div class="col-lg-4 col-sm-3 left" style="margin-top: 37px">
            <?= $this->Form->create(null, [
                'id'        => 'search-form',
                'class'     => 'navbar-form',
                'default'   => false,
                'onsubmit'  => 'return false;'
            ])?>

            <?php if(isset($CLIENTE)){ 
              $client_name = explode(' ', $CLIENTE->name); ?>
            <input class="form-busca form-control" placeholder="<?= array_shift($client_name) ?>, posso te ajudar?" type="text" id="search-query" />
            <?php } else { ?>
            <input class="form-busca form-control" placeholder="Busca" type="text" id="search-query" />
            <?php } ?>

            <button class="button-busca"
                    type="button"
                    id="search-button"
                    style="height: 34px;">
                <i class="fa fa-search"></i>
            </button>
            <?= $this->Form->end()?>
        </div>

        <div class="icones-deslogado col-sm-5 text-right">
                <?php
                if(isset($CLIENTE)){
                    $client_name = explode(' ', $CLIENTE->name);
                    echo '<span id="bem-vindo" class="usuario text-uppercase fa fa-user"></span>'.$this->Html->link('<span>&nbsp'.array_shift($client_name).'</span>',
                        $SSlug.'#',
                        ['escape' => false, 'id' => 'logado','class' => 'sub-menu usuario text-uppercase', 'title' => '']
                    );

                    $notific="";
                    /*if(count($notifications) > 0){
                        $notific = "notify-badge badge-position";
                    }*/

                    echo $this->Html->link('<i class="fa fa-globe '.$notific.'"></i>',
                        '#',
                        [
                            'escape'    => false,
                            'id'        => 'notify-button',
                            'class'     => 'sub-menu sub-icones',
                            'title'     => 'Notificações'
                        ]
                    );

                }else { ?>

                <script type="text/javascript">
                    $(document).ready(function() {
                        $('.icones-deslogado').removeClass('col-sm-5').addClass('col-sm-4 col-sm-offset-1');
                    });
                </script>

                <?php
                    echo $this->Html->link('<span>entrar</span>',
                        $SSlug.'/login', [
                            'escape' => false,
                            'class' => 'usuario btn-login-nav btn-cupom',
                            'title' => 'Fazer login'
                        ]
                    );
                }
                ?>
                <div class="profile text-left">
                    <?= $this->Html->link('<span>Início</span>',
                        $SSlug.'/home', ['escape' => false]
                    );?>
                    <?= $this->Html->link('<span>Minha Conta</span>',
                        $SSlug.'/minha-conta', ['escape' => false]
                    );?>
                    <?= $this->Html->link('<span>Meus Pedidos</span>',
                        $SSlug.'/minha-conta/meus_pedidos', ['escape' => false]
                    );?>
                    <?= $this->Html->link('<span>Indicações</span>',
                        $SSlug.'/minha-conta/indicacao-de-produtos', ['escape' => false]
                    );?>
                    <?= $this->Html->link('<span>Minha Academia</span>',
                        $SSlug.'/minha-academia', ['escape' => false]
                    );?>
                    <?= $this->Html->link('<span>Meus Objetivos</span>',
                        $SSlug.'/minha-conta/meus-dados', ['escape' => false]
                    );?>
                    <?= $this->Html->link('<span>Sair</span>',
                        $SSlug.'/logout', ['escape' => false]
                    );?>
                </div>
                <div class="notify text-left">
                    <span><i class="fa fa-globe"></i>&nbsp;&nbsp;Notificações</span>
                    <?php
                    if(count($notifications) <= 0 ){
                        ?>
                        <a>
                            <h5 class="font-bold"><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;Nenhuma...</h5>
                            <h6>Você não tem notificações no momento</h6>
                        </a>
                        <?php
                    }else{
                        echo '<div class="notifications">';
                        foreach($notifications as $notification):
                            ?>
                            <a <?= !empty($notification->link) ? 'href="'.$notification->link.'" target="_blank"' : ''; ?>>
                                <h5 class="font-bold"><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<?= $notification->name; ?></h5>
                                <h6><?= $notification->description; ?></h6>
                            </a>
                            <?php
                        endforeach;
                        echo '</div>';
                    }
                    ?>
                </div>
            </div>
            <?php if(isset($ACADEMIA)){ ?>
                <script>
                    $('.icones-deslogado').addClass('icones-logado').addClass('col-sm-3');
                    $('.icones-logado').removeClass('icones-deslogado').removeClass('col-sm-5');
                </script>
            <?php } ?>
    </div>
</nav>

<div class="reparo-nav"></div>
</div>


<div id="navbar_mobile_config">
<div class="navbar-mobile">
    <div class="col-xs-3">
      <button class="c-hamburger c-hamburger--htx">
        <span>toggle menu</span>
      </button>
    </div>
    <div class="col-xs-6">
      <?php
          if(isset($ACADEMIA)){
              if($ACADEMIA->image != null &&  $ACADEMIA->image != '' && $ACADEMIA->image != 'default.png'){
                  echo $this->Html->link(
                      $this->Html->image('academias/'.$ACADEMIA->image, ['alt' => 'LogFitness', 'class' => 'logo-topo']),
                      $SSlug.'/home',
                      ['escape' => false, 'class' => 'inline-block box-logo-mobile']
                  );
              }else{
                  echo $this->Html->link(
                      '<span class="academia-logo">Academia '
                      .str_replace(array('academia', 'Academia'), '', $ACADEMIA->shortname)
                      .'</span>',
                      $SSlug.'/home',
                      ['escape' => false, 'class' => 'inline-block link-logo box-logo-mobile']
                  );
              }
          }else{
              echo $this->Html->link(
                  $this->Html->image('logfitness.png', ['alt' => 'LogFitness', 'class' => 'logo-topo']),
                  $SSlug.'/home',
                  ['escape' => false, 'class' => 'inline-block box-logo-mobile']
              );
          }
      ?>
    </div>
    <div class="box-search-mobile col-xs-3">
        <?= $this->Html->link('<i class="fa fa-2x fa-search"></i> <span class="hidden-xs"></span>',
            '#',
            [
                'escape'    => false,
                'title'     => 'Busca',
                'id'        => 'botao-search'
            ]
        );?>
    </div>
</div>

<div class="menu-mobile" style="text-align: left">
  <?php
  if(isset($CLIENTE)){
      $client_name = explode(' ', $CLIENTE->name);?>
      
      <p style="font-size: 22px; text-align: center; padding-left: 0">Olá <?= array_shift($client_name) ?></p>
      <p id="notificacoes-mobile"><i class="fa fa-globe"></i>&nbsp;&nbsp;Notificações</p>

      <div class="notify text-left">
          <?php
          if(count($notifications) <= 0 ){
              ?>
              <a>
                  <h5 class="font-bold"><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;Nenhuma...</h5>
                  <h6>Você não tem notificações no momento</h6>
              </a>
              <?php
          }else{
              echo '<div class="notifications">';
              foreach($notifications as $notification):
                  ?>
                  <a <?= !empty($notification->link) ? 'href="'.$notification->link.'" target="_blank"' : ''; ?>>
                      <h5 class="font-bold"><i class="fa fa-chevron-circle-right"></i>&nbsp;&nbsp;<?= $notification->name; ?></h5>
                      <h6><?= $notification->description; ?></h6>
                  </a>
                  <?php
              endforeach;
              echo '</div>';
          }
          ?>
      </div>

      <hr class="separador" style="width: 90%">
      
      <?php
      echo $this->Html->link('<p><i class="fa fa-home"></i>&nbsp;&nbsp;Início</p>',
          $SSlug.'/home', ['escape' => false]
      );
      echo $this->Html->link('<p><i class="fa fa-users"></i>&nbsp;&nbsp;Minha Conta</p>',
          $SSlug.'/minha-conta', ['escape' => false]
      );
      echo $this->Html->link('<p><img src="http://teste.logfitness.com.br/webroot/img/mochila_log.png" style="margin-top: -5px;">&nbsp;&nbsp;Meus Pedidos</p>',
          $SSlug.'/minha-conta/meus_pedidos', ['escape' => false]
      );
      echo $this->Html->link('<p><i class="fa fa-comment-o"></i>&nbsp;&nbsp;Indicações</p>',
          $SSlug.'/minha-conta/indicacao-de-produtos', ['escape' => false]
      );
      echo $this->Html->link('<p><i class="fa fa-building-o"></i>&nbsp;&nbsp;Minha Academia</p>',
          $SSlug.'/minha-academia', ['escape' => false]
      );
      echo $this->Html->link('<p><i class="fa fa-line-chart"></i>&nbsp;&nbsp;Meus Objetivos</p>',
          $SSlug.'/minha-conta/meus-dados', ['escape' => false]
      );
      echo $this->Html->link('<p><i class="fa fa-sign-out"></i>&nbsp;&nbsp;Sair</p>',
          $SSlug.'/logout', ['escape' => false]
      );

  }else { ?>

      <style type="text/css">
        .menu-mobile {
          height: auto;
        }
      </style>
      
      <div style="width: 100%; text-align: center">
          <?= $this->Html->link('<p style="margin: 0; padding-left: 0;">entrar</p>',
              $SSlug.'/login', [
                  'escape' => false,
                  'class' => 'usuario btn-login-nav btn-cupom',
                  'title' => 'Fazer login'
              ]
          ); ?>
      </div>
  <?php } ?>
</div>

<div class="search-navbar-mobile">
    <div>
        <?= $this->Form->create(null, [
            'id'        => 'search-form',
            'class'     => 'navbar-form',
            'default'   => false,
            'onsubmit'  => 'return false;'
        ])?>
        <?php if(isset($CLIENTE)){ 
          $client_name = explode(' ', $CLIENTE->name); ?>
        <input class="form-busca form-control" placeholder="<?= array_shift($client_name) ?>, posso te ajudar?" type="text" id="search-query" />
        <?php } else { ?>
        <input class="form-busca form-control" placeholder="Busca" type="text" id="search-query" />
        <?php } ?>
        <button class="button-busca"
                type="button"
                id="search-button">
            <i class="fa fa-search"></i>
        </button>
        <?= $this->Form->end()?>
    </div>
</div>

<div class="fundo-branco">
    <span class="fa fa-sliders fa-2x close-filter-icon close-filter"></span>

    <svg class="close-mochila-icon close-mochila" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"><g><polygon points="8.669,48.706 5,62.65 6.565,64.773 10.008,54.727  "/><polygon points="93.422,64.79 95,62.65 91.331,48.706 89.991,54.727  "/><polygon points="41.311,14.947 39.661,15.937 37.462,30.863 37.071,40.502 39.622,40.182 41.47,14.947  "/><polygon points="62.928,40.51 62.538,30.863 60.338,15.937 58.689,14.947 58.529,14.947 60.378,40.188  "/><polygon points="29.01,42.523 21.586,43.458 9.53,47.966 11.045,54.785 5.5,70.967 5.5,88.654 27.14,88.654  "/><polygon points="36.48,55.079 35.119,88.654 64.879,88.654 63.519,55.079  "/><polygon points="52.475,40.195 47.463,40.199 37.03,41.513 36.716,49.244 63.282,49.244 62.969,41.516  "/><polygon points="88.954,54.785 90.47,47.966 78.413,43.458 70.989,42.524 72.859,88.654 94.5,88.654 94.5,70.967  "/><path d="M28.12,89.154h5.978l1.422-35.075h28.96L65.9,89.154h5.979l-2.368-58.421L65.62,15.752l-7.219-4.906h-7.104h-2.595h-7.105   l-7.218,4.906l-3.891,14.981L28.12,89.154z M51.578,13.947h7.388l2.256,1.354l2.312,15.433l0.79,19.511H35.675l0.792-19.511   l2.311-15.433l2.257-1.354h7.386H51.578z"/></g></svg>
</div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    var medida_tela = $(window).width();

    if(medida_tela <= 768) {
        $('#navbar_pc_config').remove();
        $('#navbar_mobile_config').show();
    } else {
        $('#navbar_pc_config').show();
        $('#navbar_mobile_config').remove();
    }
  });
</script>