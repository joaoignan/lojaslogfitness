<style>
	.logo-revista{
		max-height: 60px;
    	width: auto;
	}
	.botao-exportar{
		padding-top: 25px;
	}
</style>
<div class="container-fluid botao-exportar">
	<div class="row">
		<div class="col-xs-12 text-right">
			<button class="btn btn-primary"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Exportar em PDF</button>
		</div>
	</div>
</div>

<div class="flipbook-viewport">
	<div class="container text-center">
		<div class="flipbook">
			<div>
				Logfitness
			</div>
			<?php $count = 1 ?>
			<?php $quebra_pagina = 6 ?>
			<?php foreach ($produtos as $produto) { ?>
			<?php $foto = unserialize($produto->fotos)[0]; ?>
			<?php if ($quebra_pagina % 6 == 0) { ?>
			<div>
				<?php 
					if($academia->image != null){
	                	echo $this->Html->image('academias/'.$academia->image, ['alt' => $academia->shortname, 'class' => 'logo-revista']);
	            	} else{
	               		echo '<h4>'.$academia->shortname.'</h4>';
	            	}
	        	?>
				<div class="separacao-completa"></div>
			<?php } ?>
				<div class="box-produto-catalogo">
					<div class="text-left codigo-imagem">
						<span class="codigo-catalogo text-left">COD <?= $produto->id ?></span>
						<?= $this->Html->image('produtos/'.$foto, ['alt' => $produto->produto_base->name]) ?>
					</div>
					<div class="text-right dados-box-produto-catalogo">
						<span class="objetivo-catalogo" style="background-color: <?= $produto->produto_base->produto_objetivos[0]->objetivo->color ?>"><?= $produto->produto_base->produto_objetivos[0]->objetivo->texto_tarja ?></span>
						<p class="marca-catalogo"><?= $produto->produto_base->marca->name ?></p>
						<p class="nome-catalogo"><?= $produto->produto_base->name ?></p>
						<p class="duracao-catalogo"><?= $produto->tamanho ?><?= $produto->unidade_medida ?>/<strong><?= $produto->doses ?></strong> treinos</p>
						<p class="sabores"><strong><?= $produto->propriedade ?></strong></p>
						<?php if($tem_valor) { ?>
							<?php $produto->preco_promo >= 1 ? $preco = $produto->preco_promo : $preco = $produto->preco ?>
							<div class="preco text-right">
								<p class="valor-produto-catalogo">R$<?= number_format($preco, 2, ',', '.') ?></p>
								<p class="complementar-valor-catalogo">em até <strong>12x</strong> de</p>
								<p class="valor-produto-catalogo">R$22,49</p>
							</div>
						<?php } ?>
					</div>
				</div>
				<?php if ($count % 2 != 0) { ?>
				<div class="separador-vertical"></div>
				<?php } ?>
				<?php 
					$count++;
					$quebra_pagina++;
				 ?>
		 	<?php if ($quebra_pagina % 6 == 0) { ?>
			</div>
			<?php } ?>
			<?php } ?>
		</div>
	</div>
</div>

<script type="text/javascript">

function loadApp() {

	// Create the flipbook

	$('.flipbook').turn({
			// Width

			width:922,
			
			// Height

			height:600,

			// Elevation

			elevation: 50,
			
			// Enable gradients

			gradients: true,
			
			// Auto center this flipbook

			autoCenter: true

	});
}

// Load the HTML4 version if there's not CSS transform

yepnope({
	test : Modernizr.csstransforms,
	yep: ['../../js/turnjs/turn.js'],
	nope: ['../../js/turnjs/turn.html4.min.js'],
	both: ['../../css/turnjs/basic.css'],
	complete: loadApp
});

</script>

	