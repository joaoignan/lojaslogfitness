<?php foreach($produtos as $produto) { ?>
    <?php $fotos = unserialize($produto->fotos); ?>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 box-completa-div">
        <div class="box-completa-produto box-completa-produto-busca" data-id="<?= $produto->id ?>" itemtype="http://schema.org/Product">
            <div class="tarja-objetivo">
                <?php
                    $contador_objetivos = 0; 
                    $novo_top = 0;
                ?>
                <?php foreach ($produto->produto_base->produto_objetivos as $produto_objetivo) { ?>
                    <?php if ($contador_objetivos < 2) { ?>
                    <div class="tarja objetivo-tarja" style="top: <?= $novo_top ?>px; background-color: <?= $produto_objetivo->objetivo->color ?>">
                        <span><?= $produto_objetivo->objetivo->texto_tarja ?></span>
                    </div>
                    <?php } ?>
                    <?php
                        $contador_objetivos++;
                        $novo_top = $novo_top + 19;
                     ?>
                <?php } ?>
            </div>
            <div class="total-tarjas">
                <?php if ($produto->estoque == 1) { ?>
                    <div class="tarja tarja-ultimo">
                        <span>Último</span>
                    </div>
                <?php } else if ($produto->estoque >= 1 && $produto->preco_promo) { ?>
                    <div class="tarja tarja-promocao">
                        <span>Promoção</span>
                    </div>
                <?php } else if($produto->estoque >= 1 && $produto->created > $max_date_novidade) { ?>
                    <div class="tarja tarja-novidade">
                        <span>Novidade</span>
                    </div>
                <?php } ?>               
            </div>
            <div class="info-produto">
                <div class="produto-superior">
                    <a href="<?= '/comparador/produto/'.$produto->slug ?>">
                        <?php $fotos = unserialize($produto->fotos); ?>
                        <div class="foto-produto" id="produto-<?= $produto->id ?>" >
                            <?= $this->Html->image(
                                'produtos/md-'.$fotos[0],
                                ['val' => $produto->slug, 'alt' => $produto->produto_base->name.' '.$produto->produto_base->embalagem_conteudo]); ?>
                        </div>
                        <div class="detalhes-produto">
                            <div class="informacoes-detalhes text-left">
                                <p>Código: <?= $produto->id ?></p>
                                <p><?= strtolower($produto->produto_base->marca->name); ?></p>
                                <p><?= $produto->tamanho.' '.$produto->unidade_medida; ?></p>
                                <p>para <?= $produto->doses; ?> treinos</p>
                                <p><?= strtolower($produto->propriedade); ?></p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="produto-inferior">
                    <?php if($produto->preco_promo || $desconto_geral > 0) {
                        $porcentagem_nova = $produto->preco * 0.089;
                        $preco_corte = $produto->preco + $porcentagem_nova; 
                    } ?> 
                    <?php $produto->preco_promo ?
                    number_format($preco = $produto->preco_promo * $desconto_geral, 2, ',','.') :
                    number_format($preco = $produto->preco * $desconto_geral, 2, ',','.'); ?>
                    <?php $produto->preco_promo ? $porcentagem_nova = $produto->preco_promo * 0.089 : $porcentagem_nova = $produto->preco * 0.089 ?>
                    <?php if($produto->preco_promo != null) {
                        if($produto->preco_promo < 100.00) { ?>
                        <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/2?>
                        <?php $maximo_parcelas = "2x";
                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                    <?php } else if($produto->preco_promo > 100.00 && $produto->preco_promo < 200.00) { ?>
                        <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/3?>
                        <?php $maximo_parcelas = "3x";
                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                    <?php } else if($produto->preco_promo > 200.00 && $produto->preco_promo < 300.00) { ?>
                        <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/5?>
                        <?php $maximo_parcelas = "5x";
                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                    <?php } else if($produto->preco_promo > 300.00) { ?>
                        <?php $parcel = $produto->preco_promo * $desconto_geral * 0.089; $parcel = ($produto->preco_promo * $desconto_geral + $parcel)/6?>
                        <?php $maximo_parcelas = "6x";
                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                    <?php }
                        } else {
                    if($produto->preco < 100.00) { ?>
                        <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/2?>
                         <?php $maximo_parcelas = "2x";
                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                    <?php } else if($produto->preco > 100.00 && $produto->preco < 200.00) { ?>
                        <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/3?>
                        <?php $maximo_parcelas = "3x";
                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                    <?php } else if($produto->preco > 200.00 && $produto->preco < 300.00) { ?>
                        <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/5?>
                        <?php $maximo_parcelas = "5x";
                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                    <?php } else if($produto->preco > 300.00) { ?>
                        <?php $parcel = $produto->preco * $desconto_geral * 0.089; $parcel = ($produto->preco * $desconto_geral + $parcel)/6?>
                        <?php $maximo_parcelas = "6x";
                         $valor_parcela = number_format($parcel, 2, ',', '.') ?>
                    <?php }
                    } ?>
                    <?php 
                        if($produto->preco_promo || $desconto_geral > 0) {
                            echo $this->Html->link("
                                <h4>".$produto->produto_base->name."</h4>
                                <p><span class='preco-corte'> R$".number_format($preco_corte, 2, ',','.')."</span> por <span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                '/comparador/produto/'.$produto->slug,
                                ['escape' => false]);
                        } else {
                            echo $this->Html->link("
                                <h4>".$produto->produto_base->name."</h4>
                                <p><span class='preco-principal'> R$".number_format($preco, 2, ',','.')."</span></p>
                                <p>ou até ".$maximo_parcelas." de R$".$valor_parcela."</p>",
                                '/comparador/produto/'.$produto->slug,
                                ['escape' => false]);
                        }
                    ?>
                </div>
                <div class="hover-produto <?= $produto->status_id == 1 ? 'hover-produto-disponivel' : 'hover-produto-indisponivel' ?> text-center">
                    <div class="col-xs-12">
                        <div class="hexagono-indicar">
                            <?php if($produto->status_id == 1) { ?>
                                <div class="hexagon-disponivel btn-indique" data-id="<?= $produto->id ?>">
                                    <span><?= $this->Html->image('icnon-indicar.png'); ?></span>
                                    <p>indique</p>
                                </div>
                                <script>
                                    $('.btn-indique').click(function () {
                                            var id_produto = $(this).attr('data-id');
                                            $('#overlay-indicacao-'+id_produto).height('100%');
                                            $("html,body").css({"overflow":"hidden"});
                                    });
                                    function closeIndica() {
                                        $('.overlay-indicacao').height('0%');
                                        $("html,body").css({"overflow":"auto"});
                                    }
                                </script>
                            <?php } else { ?>
                                <div class="hexagon produto-acabou-btn" data-id="<?= $produto->id ?>">
                                    <span><i class="fa fa-envelope-o"></i></span>
                                    <p>avise-me</p>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <?php if($produto->status_id == 1) { ?>
                        <div class="col-xs-6">
                            <?= $this->Html->link('
                                <div class="hexagon-disponivel">
                                    <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                    <p>detalhes</p>
                                </div>'
                                ,'/comparador/produto/'.$produto->slug,
                                ['escape' => false]);
                            ?>
                        </div>
                        <div class="col-xs-6 text-center compare">
                            <div class="hexagon-disponivel btn-comprar-overlay">
                                <span><?= $this->Html->image('mochila_log_branca.png',['alt' => 'Mochila LOG', 'class' => 'comprar-hexag']) ?></span>
                                <p>comprar</p>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="col-xs-6">
                            <?= $this->Html->link('
                                <div class="hexagon">
                                    <span><i class="fa fa-search" aria-hidden="true"></i></i></span>
                                    <p>detalhes</p>
                                </div>'
                                ,'/comparador/produto/'.$produto->slug,
                                ['escape' => false]);
                            ?>
                        </div>
                        <div class="col-xs-6 text-center compare">
                            <?= $this->Html->link('
                                <div class="hexagon">
                                    <span>ver</span>
                                    <p>similares</p>
                                </div>',
                                '/comparador/similares/'.$produto->slug, 
                                ['escape' => false]
                            ); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="botoes-comprar-similiar">
                <?php if($produto->visivel == 1 && $produto->status_id == 1) { ?>
                    <button class="btn btn-comparar btn-comparar-action" data-id="<?= $produto->id ?>"><i class="fa fa-balance-scale"></i> Comparar</button>
                    <button class="desktop-hidden btn btn-incluir btn-indique" data-id="<?= $produto->id ?>">Indique</button>
                    <button class="desktop-hidden btn btn-incluir btn-comprar-overlay">Comprar</button>
                <?php } else if($produto->visivel == 1 && $produto->status_id == 2) { ?>
                    <button class="btn btn-comparar btn-comparar-action" data-id="<?= $produto->id ?>"><i class="fa fa-balance-scale"></i> Comparar</button>
                    <button class="desktop-hidden btn btn-similares produto-acabou-btn" data-id="<?= $produto->id ?>">Avise-me</button>
                    <button class="desktop-hidden btn btn-incluir btn-comprar-overlay">Comprar</button>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<?php foreach($produtos as $produto) { ?>
    <div id="aviseMe-<?= $produto->id ?>" class="overlay overlay-avise">
        <div class="overlay-content overlay-avise-<?= $produto->id ?> text-center">
            <a href="javascript:void(0)" class="closebtn" onclick="closeAviseMe()">&times;</a>
            <h4>Já estamos colocando mais na prateleira. Como o preço pode variar deixe seu contato que avisamos quando estiver disponível =)</h4>
            <br />
            <?= $this->Form->create(null, ['id' => 'produto_acabou-'.$produto->id])?>
            <?= $this->Form->input('produto_id', [
                'div'           => false,
                'label'         => false,
                'type'          => 'hidden',
                'val'           => $produto->id,
                'id'            => 'produto_id_acabou-'.$produto->id
            ])?>
            <?= $this->Form->input('name', [
                'div'           => false,
                'label'         => false,
                'id'            => 'name_acabou-'.$produto->id,
                'class'         => 'form-control validate[optional]',
                'placeholder'   => 'Qual o seu nome?',
            ])?>
            <br/>
            <?= $this->Form->input('email', [
                'div'           => false,
                'label'         => false,
                'id'            => 'email_acabou-'.$produto->id,
                'class'         => 'form-control validate[required, custom[email]]',
                'placeholder'   => 'Qual o seu email?',
            ])?>
            <br/>
            <?= $this->Form->button('Enviar', [
                'data-id'       => $produto->id,
                'type'          => 'button',
                'class'         => 'btn btn-success send-acabou-btn'
            ])?>
            <br />
            <br />
            <?= $this->Form->end()?>
        </div>
    </div> 
    <div id="overlay-indicacao-<?= $produto->id ?>" class="overlay overlay-indicacao">
        <div class="overlay-content text-center">
            <a href="javascript:void(0)" class="closebtn" onclick="closeIndica()">&times;</a>
            <h4>Lembrou de alguém quando viu isso?</h4>
            <h4>Indique este produto!</h4>
            <br>
            <div class="row">
                <div class="col-xs-12 share-whastapp share-buttons">
                    <a href="whatsapp://send?text=Isso é a sua cara! <?= WEBROOT_URL.$SSlug.'/comparador/produto/'.$produto->slug ?>">
                        <button class="btn button-blue"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</button>
                    </a>
                </div>
                <div class="col-xs-12 share-whastapp share-buttons">
                    <button class="btn button-blue" id="share-<?= $produto->id ?>"><i class="fa fa-comment-o" aria-hidden="true"></i> Messenger</button>
                    <script>
                        $(document).ready(function() {
                            $('#share-<?= $produto->id ?>').click(function(){
                                var link = "<?= WEBROOT_URL.$SSlug.'/comparador/produto/'.$produto->slug ?>";
                                var app_id = '1321574044525013';
                                window.open('fb-messenger://share?link=' + link + '&app_id=' + app_id);
                            });
                        });
                    </script>
                </div>
                <div class="tablet-hidden col-xs-12 col-md-3 share-buttons">
                    <button data-id="whatsweb-<?= $produto->id ?>" class="btn button-blue abrir-whats"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp Web</button>
                </div>
               <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                    <a href="javascript: void(0);" onclick="window.open('http://www.facebook.com/sharer.php?u=<?= WEBROOT_URL.$SSlug.'/comparador/produto/'.$produto->slug ?>','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');">
                        <button class="btn button-blue fb-share-button"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</button>
                    </a>
                </div>
               <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                    <button class="btn button-blue" id="copiar-clipboard-<?= $produto->id ?>"><i class="fa fa-clone" aria-hidden="true"></i> Copiar link</button>
                </div>
               <div class="col-xs-12 col-sm-4 col-md-3 share-buttons">
                    <button  class="btn button-blue abrir-email"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail</button>
                </div>
            </div>
            <div class="row">
                <div id="collapse-whats-<?=$produto->id ?>" class="tablet-hidden enviar-whats">
                    <div class="col-xs-4 text-center form-group">
                        <?= $this->Form->input('whats-web-form', [
                            'div'           => false,
                            'id'            => 'nro-whatsweb-'.$produto->id,
                            'label'         => false,
                            'class'         => 'form-control form-indicar',
                            'placeholder'   => "Celular*: (00)00000-0000"
                        ])?>
                    </div>
                    <div class="col-xs-4 text-left obs-overlay">
                        <p>*Para enviar pra mais de um contato deixe este campo vazio.</p>
                    </div>
                    <div class="col-xs-4 text-center form-group">
                        <?= $this->Form->button('Enviar', [
                        'type'          => 'button',
                        'data-id'       => $produto->id,
                        'class'         => 'btn button-blue',
                        'id'            => 'enviar-whatsweb-'.$produto->id
                        ])?>
                    </div>
                    <script>
                        $(document).ready(function() {
                            $('#enviar-whatsweb-<?=$produto->id ?>').click(function(){

                                var numero;
                                var link = "<?= WEBROOT_URL.$SSlug.'/comparador/produto/'.$produto->slug ?>";

                                if($('#nro-whatsweb-<?= $produto->id ?>').val() != '') {
                                    numero = '55'+$('#nro-whatsweb-<?= $produto->id ?>').val();
                                }
                                
                                window.open('https://api.whatsapp.com/send?phone=' + numero + '&text=Isso é a sua cara! ' + link);
                            });
                        });
                    </script>
                </div>
                <div class="col-xs-12 enviar-email" id="collapse-email-<?=$produto->id ?>">
                    <?= $this->Form->create(null, ['class' => 'indicar_prod_home_'.$produto->id])?>
                    <?= $this->Form->input('name', [
                        'div'           => false,
                        'label'         => false,
                        'class'         => 'form-control validate[optional] form-indicar name-'.$produto->id,
                        'placeholder'   => 'Quem indicou?',
                    ])?>
                    <br>
                    <?= $this->Form->input('email', [
                        'div'           => false,
                        'label'         => false,
                        'class'         => 'form-control validate[required] form-indicar email-'.$produto->id,
                        'placeholder'   => 'E-mail do destinatário',
                    ])?>
                    <?= $this->Form->input('id_produto', [
                        'div'           => false,
                        'class'         => 'id_produto-'.$produto->id,
                        'value'         => $produto->id,
                        'label'         => false,
                        'type'          => 'hidden'
                    ])?>
                    <br>
                    <?= $this->Form->button('Enviar', [
                        'type'          => 'button',
                        'data-id'       => $produto->id,
                        'class'         => 'btn button-blue send-indicacao-home-btn'
                    ])?>
                    <br>
                    <br>
                    <?= $this->Form->end()?>
                </div>
            </div>
            <div class="row"> 
                <div class="col-xs-12 clipboard">   
                    <?= $this->Form->input('url', [
                        'div'           => false,
                        'id'            => 'url-clipboard-'.$produto->id,
                        'label'         => false,
                        'value'         => WEBROOT_URL.$SSlug.'/comparador/produto/'.$produto->slug,
                        'readonly'      => true,
                        'class'         => 'form-control form-indicar'
                    ])?>
                </div>  
                <p><span class="copy-alert hide" id="copy-alert-<?=$produto->id ?>">Copiado!</span></p>
                <script>
                    $(document).ready(function() {
                        // Copy to clipboard 
                        document.querySelector("#copiar-clipboard-<?=$produto->id ?>").onclick = () => {
                          // Select the content
                            document.querySelector("#url-clipboard-<?=$produto->id ?>").select();
                            // Copy to the clipboard
                            document.execCommand('copy');
                            // Exibe span de sucesso
                            $('#copy-alert-<?=$produto->id ?>').removeClass("hide");
                            // Oculta span de sucesso
                            setTimeout(function() {
                                $('#copy-alert-<?=$produto->id ?>').addClass("hide");
                            }, 1500);
                        };
                    });
                </script>
            </div>
        </div>
    </div>
<?php } ?>

<script>
    $(document).ready(function() {
        // abrir overlay-comprar
        $('.btn-comprar-overlay').click(function() {
            $('.overlay-comprar').animate({'top' : '0'}, 500);
        });
        
        // fechar overlay-comprar
        $('.overlay-comprar').click(function() {
            $('.overlay-comprar').animate({'top' : '2000px'}, 500);
        });
    });
</script>

<script type="text/javascript">
    $('.send-indicacao-home-btn').click(function(e) {
        e.preventDefault();
        var botao = $(this);
    
        botao.html('Enviando...');
        botao.attr('disabled', 'disabled');

        var id = $(this).attr('data-id');
        var slug = '<?= $SSlug ?>';

        var valid = $('.indicar_prod_home_'+id).validationEngine("validate");
        if (valid == true) {
            $.ajax({
                type: "POST",
                url: WEBROOT_URL + slug + '/produtos/overlay-indicar-produtos-home/',
                data: {
                    name: $('.name-'+id).val(),
                    email: $('.email-'+id).val(), 
                    produto_id: $('.id_produto-'+id).val()
                }
            })
            .done(function (data) {
                if(data == 1) {
                    botao.html('Enviado!');
                    setTimeout(function () {
                        botao.html('Enviar');
                        botao.removeAttr('disabled');
                    }, 2200);
                } else {
                    botao.html('Falha ao enviar... Tente novamente...');
                    setTimeout(function () {
                        botao.html('Enviar');
                        botao.removeAttr('disabled');
                    }, 2200);
                }
            });
        }else{
            $('.indicar_prod_home_'+id).validationEngine({
                updatePromptsPosition: true,
                promptPosition: 'inline',
                scroll: false
            });
        }
    });
</script>
<script>
    $('.abrir-email').click(function(e){
            e.preventDefault();
            var id = $(this).attr('data-id');
            $('.enviar-whats').slideUp("slow");
            $('.enviar-email').slideToggle("slow");
        });
        $('.abrir-whats').click(function(e){
            var id = $(this).attr('data-id');
            $('.enviar-email').slideUp("slow");
            $('.enviar-whats').slideToggle("slow");
        });
</script>