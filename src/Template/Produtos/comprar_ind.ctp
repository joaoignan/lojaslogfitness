<style type="text/css">
    .box-close-mochila {
        width: 0;
    }
    .mochila {
        width: 230px;
        opacity: 1;
    }
    .fundo-carrinho {
        margin-left: 0;
    }
    .cupom-desconto, 
    .total-compra, 
    .btn-fecha-mochila {
        right: 0px;
    }
</style>

<?php if($alerta_inativo == 1) { ?>
    <script>
    $('.produto-verifica').append('<div class="erro-inativo" style="color: red">Um dos produtos não está disponível.</div>');
    </script>
<?php } else { ?>
    <script>
        $('.erro-inativo').remove();
    </script>
<?php } ?>

<script type="text/javascript">
    function atualizar_contador() {
        var contador = 0;

        $('.produto-qtd').each(function() {
            contador = contador + parseInt($(this).val());
        });

        $('.contador-itens').html(contador);
    }

    atualizar_contador();

    subtotal = $('#carrinho-total').attr('data-total');

    if($('.cupom-value').attr('data-id') == 1) {
        total = subtotal * 0.8;
    } else {
        total = subtotal;
    }

    console.log($('.cupom-value').attr('data-id'));

    total = parseFloat(total);

    $('#carrinho-total').html('TOTAL: R$ ' + total.toFixed(2).replace('.', ','));


    var valor_total;
    $('.mochila, .lixo, .produto-mais, .produto-menos').click(function(e) {
        e.preventDefault();
        valor_total = $('#carrinho-total').html();
        if(valor_total == 'TOTAL: R$ 0,00') {
            $('.btn-fecha-mochila').attr('disabled', 'disabled');
        } else {
            $('.btn-fecha-mochila').removeAttr('disabled');
        }
    });

    $('.lixo').click(function(e){
        e.preventDefault();
        var produto_id  = $(this).attr('data-id');
        var produto     = $('#produto-' + produto_id);
        if(produto.val() > 0){
            produto.val(parseInt(produto.val()) - parseInt(produto.val()));
            var quantidade  = (parseInt(produto.val()));

            calc_produto_carrinho(produto_id, 'subtracao');
            carrinho_edit(produto_id, quantidade);
        }
        $('#fundo-'+produto_id).hide();
        atualizar_contador();
    });

    $('.produto-menos').click(function(){
        var produto_id  = $(this).attr('data-id');
        var produto     = $('#produto-' + produto_id);
        if(produto.val() == 1) {
            $('#fundo-'+produto_id).hide();
        }
        if(produto.val() > 0){
            produto.val((parseInt(produto.val()) - 1));
            var quantidade  = (parseInt(produto.val()));

            calc_produto_carrinho(produto_id, 'subtracao');
            carrinho_edit(produto_id, quantidade);
        } 
        atualizar_contador();
    });

    $('.produto-mais').click(function(event){
        event.stopPropagation();
        var produto_id  = $(this).attr('data-id');
        var estoque     = $(this).attr('eid');
        var produto     = $('#produto-' + produto_id);
        var quantidade  = (parseInt(produto.val()) + 1);
        $('.error-qtd').remove();
        
        if(quantidade <= estoque) {
            produto.val(quantidade);

            calc_produto_carrinho(produto_id, 'soma');
            carrinho_edit(produto_id, quantidade);
        } else {
            $(this).parent().append('<i class="fa fa-exclamation-circle error-qtd" aria-hidden="true" title="esses são os últimos, em breve vamos colocar mais na prateleira!"></i>');

            $('.error-qtd').click(function(event) {
                event.stopPropagation();
            });

            if($('#expandir-mochila').hasClass('seta-animate-abrir')) {
                $('.error-qtd').addClass('error-qtd-expanded');
            }
        }
        atualizar_contador();
    });

    $('.error-qtd').click(function(event) {
        event.stopPropagation();
    });

    $('.btn-fecha-mochila').click(function() {
        var url_atual = window.location.href;
        var academia_url = url_atual.split("/");
        if(academia_url[3] != 'home') {
            location.href = academia_url[3]+'/mochila/fechar-o-ziper';
        } else {
            location.href = '/mochila/fechar-o-ziper';
        }
    });

    $('.detalhes-btn').click(function(e) {
        e.preventDefault();
        $('#expandir-mochila').click();
    });

    $('.img-produto-principal, .img-combina-com, .produto-grade').draggable({
        helper: 'clone'
    });

    $('.produtos-mochila').droppable({

        drop: function(evt, ui){

            if(ui.draggable.parent()[0] == this){
                return;
            }

            var t = $(this);
            var e = ui.draggable;
            var diff = {x: evt.pageX - ui.position.left, y: evt.pageY - ui.position.top};
            e.draggable('option','helper','');

            $.get(WEBROOT_URL + 'produtos/comprar/' + e.attr('val'),
                function (data) {
                    $('.laterais.mochila').html('');
                    $('.laterais.mochila').html(data);
                    $('.produtos-mochila').animate({ 'width' : '228px', 'opacity' : 1 }, function() {
                        $('.info-direita-mochila').fadeIn(200);
                    });
                    loading.hide(1);
                });
        }
    });

    $('.aplicar-cupom').click(function() {
        $.ajax({
            type: "POST",
            url: WEBROOT_URL + '/produtos/check-cupom/',
            data: {codigo: $('.cupom-value').val()}
        })
            .done(function (data) {
                $('.msg-cupom').remove();

                $('.cupom-desconto').html(data);

                var subtotal = 0;

                $('.total-produto').each(function(){
                    subtotal = parseFloat(subtotal + parseFloat($(this).attr('data-total')));
                });

                var valor_cupom = $('.cupom-value').attr('data-id');
                var desconto = 1.0 - (valor_cupom * .01);

                if(valor_cupom != 0) {
                    total = subtotal * desconto;
                } else {
                    total = subtotal;
                }

                $('#carrinho-total').html('TOTAL: R$ ' + total.toFixed(2).replace('.', ','));

                if(total == 0){
                    $('.btn-fecha-mochila').attr('disabled', true);
                }else{
                    $('.btn-fecha-mochila').attr('disabled', false);
                }
            });
    });

    /**
     * CARRINHO - CALCULO VALOR TOTAL PRODUTO
     * @param id
     * @param operacao
     */
    function calc_produto_carrinho(id, operacao){

        var element = $('#produto-' + id);
        var preco_unitario  = element.attr('data-preco');
        var quantidade      = element.val();

        var preco_total     = parseFloat((preco_unitario * quantidade));

        $('#total-produto-' + id)
            .html('R$ ' + preco_total.toFixed(2).replace('.', ','))
            .attr('data-total', preco_total.toFixed(2));

        var subtotal = 0;
        $('.total-produto').each(function(){
            subtotal = parseFloat(subtotal + parseFloat($(this).attr('data-total')));
        });

        if($('.cupom-value').attr('data-id') == 1) {
            total = subtotal * 0.8;
        } else {
            total = subtotal;
        }

        $('#carrinho-total').html('TOTAL: R$ ' + total.toFixed(2).replace('.', ','));

        if(total == 0){
            $('.btn-fecha-mochila').attr('disabled', true);
        }else{
            $('.btn-fecha-mochila').attr('disabled', false);
        }
    }

    /**
     * CARRINHO - ALTERAR ITENS
     * @param id
     * @param qtd
     */
    function carrinho_edit(id, qtd){
        $.get(WEBROOT_URL + '/mochila/alterar/' + id + '/' + qtd,
            function(data){
                console.log('Itens alterados...');
            });
    }
    
    var medida_mochila_aberta = $(document).width() - 230;
    var medida_metade = medida_mochila_aberta/2;
    var correcao = medida_metade - 247 + 50;
    var ctrl_abrir = 0;

    $('#expandir-mochila').click(function(){
        if(ctrl_abrir == 0) {
            $('.produtos-mochila').animate({ 'width' : medida_mochila_aberta - 2 }, 2000);
            $('.mochila').animate({ 'width' : medida_mochila_aberta }, 2000, function() {
                $('.info-prod, .valor-produto').fadeIn(400);
            });
            $('.mochila-titulo').animate({ 'paddingLeft' : correcao }, 2000);
            $('#expandir-mochila').removeClass('seta-animate-fechar').addClass('seta-animate-abrir');
            $('.info-direita-mochila').animate({paddingRight : '30px', width : '300px'}, 2000);
            $('.close-mochila').css('pointer-events', 'none');
            ctrl_abrir = 1;
        } else {
            $('.mochila').animate({ 'width' : '230px' }, 2000);
            $('.produtos-mochila').animate({ 'width' : '228px' }, 2000);
            $('.info-prod, .valor-produto').fadeOut(400);
            $('.mochila-titulo').animate({ 'paddingLeft' : '30px' }, 2000);
            $('#expandir-mochila').removeClass('seta-animate-abrir').addClass('seta-animate-fechar');
            $('.info-direita-mochila').animate({paddingRight : 0, width : '130px'}, 2000);
            $('.close-mochila').css('pointer-events', 'auto');
            ctrl_abrir = 0;
        }
    });

    $('.fechar-mochila-x').click(function() {
        $('.close-mochila').click();
    });
</script>

    <i class="fa fa-close fechar-mochila-x"></i>
    <div class="fundo-titulo">
        <div class="col-sm-12">
            <span id="expandir-mochila" class="fa fa-arrow-left fa-2x expandir" style="color: white;"></span>
            <div class="titulos mochila-titulo"> mochila</div>
        </div>
    </div>
    <div class="produtos-mochila">
        <?php
            foreach ($s_produtos as $s_produto):
                $fotos = unserialize($s_produto->fotos);
                ?>

                <div id="fundo-<?= $s_produto->id ?>" class="col-md-12 fundo-carrinho">
                    <div class="imagem-produto-carrinho">
                        <?= $this->Html->link(
                            $this->Html->image('produtos/' . $fotos[0],
                                ['class' => 'dimensao-produto-carrinho', 'alt' => $s_produto->produto_base->name . ' ' . $s_produto->propriedade]
                            ),
                            '/produto/' . $s_produto->slug,
                            ['escape' => false]
                        ) ?>
                    </div>
                    <div class="col-md-4 text-center info-prod">
                        <?= $this->Html->link(
                            '<h1 class="font-14 h1-carrinho"><span>'
                            . $s_produto->produto_base->name . ' ' . $s_produto->produto_base->embalagem_conteudo . ' ' . $s_produto->propriedade .
                            '</span></h1>',
                            '/produto/' . $s_produto->slug,
                            ['escape' => false]
                        ) ?>
                        <p class="font-10"><span><?= strtoupper($s_produto->cod); ?></span></p>
                    </div>
                    <div class="text-right valor-produto">
                        <?= $s_produto->preco_promo ?
                            '<p><span><s>R$ ' . number_format($s_produto->preco, 2, ',', '.') . '</s></span></p>' :
                            ''; ?>
                        <p>
                        <span><b>R$ <?= $s_produto->preco_promo ?
                                    number_format($preco = $s_produto->preco_promo, 2, ',', '.') :
                                    number_format($preco = $s_produto->preco, 2, ',', '.'); ?></b>
                        </span>
                        </p>
                    </div>
                    <div class="info-direita-mochila">
                        <div class="text-right">
                            <b id="total-produto-<?= $s_produto->id ?>"
                               class="total-produto"
                               data-total="<?= number_format(($preco * $session_produto[$s_produto->id]), 2, '.', '') ?>">
                                R$ <?= number_format($total_produto = ($preco * $session_produto[$s_produto->id]), 2, ',', '.') ?>
                                <?php $subtotal += $total_produto; ?>
                            </b>
                        </div>
                        <div class="text-center qtd-produtos">
                            <input type="button" value='-' class="produto-menos" data-id="<?= $s_produto->id ?>"/>
                            <input class="text produto-qtd"
                                   id="produto-<?= $s_produto->id ?>"
                                   size="1"
                                   type="text"
                                   value="<?= $session_produto[$s_produto->id]; ?>"
                                   data-preco="<?= $s_produto->preco_promo ?
                                       number_format($s_produto->preco_promo, 2, '.', '') :
                                       number_format($s_produto->preco, 2, '.', ''); ?>"
                                   maxlength="2"
                                   readonly="true"/>
                            <input type="button" value='+' class="produto-mais" data-id="<?= $s_produto->id ?>" eid="<?= $s_produto->estoque ?>"/>
                        </div>
                    </div>
                    <div class="info-direita-mochila">
                        <div class="lixo-div">
                            <a class="lixo" href="#" data-id="<?= $s_produto->id ?>"><span class="fa fa-trash" style="color: darkgray"></span></a>
                        </div>

                        <div class="detalhes">
                            <a href="#" class="detalhes-btn"><span style="font-size: 14px; font-weight: 700">+</span> detalhes</a>
                        </div>
                    </div>
                </div>
                <?php
            endforeach;
        ?>
    </div>

    <div class="limpar-mochila col-sm-12">
        <button class="btn-limpar-mochila"><span class="fa fa-trash"></span> limpar mochila</button>    </div>
    <div class="cupom-desconto col-sm-12">
        <div class="col-xs-7">
            <input type="text" name="cupon-desconto" class="cupom-value" placeholder=" cupom desconto" data-id="<?= $cupom_desconto->valor ?>">
        </div>
        <div class="col-xs-4">
            <button class="btn-cupom aplicar-cupom">aplicar</button>
        </div>
        <?php if(count($cupom_desconto) > 0) { ?>
            <div class="msg-cupom" style="color: green; text-align: center;">cupom válido!</div>
        <?php } ?>
    </div>
    <div class="total-compra col-sm-12">
        <p class="frete-txt"><b>*FRETE GRÁTIS</b></p>
        <?php if($cupom_desconto) { ?>
            <?php $desconto = 1.0 - ($cupom_desconto->valor * .01); $subtotal_desc = $subtotal * $desconto; ?>
            <p class="total-txt"><b id="carrinho-total" data-total="<?= number_format($subtotal_desc, 2, '.', '')?>">TOTAL: R$ <?= number_format($subtotal_desc, 2, ',', '.')?></b></p>
        <?php } else { ?>
            <p class="total-txt"><b id="carrinho-total" data-total="<?= number_format($subtotal, 2, '.', '')?>">TOTAL: R$ <?= number_format($subtotal, 2, ',', '.')?></b></p>
        <?php } ?>
    </div>
    <div class="fechar-mochila">
    <?= $subtotal > 0 ? 
        $this->Html->link(
        '<button class="btn-fecha-mochila">fechar mochila</button>',
        $SSlug.'/mochila/fechar-o-ziper',
        ['escape' => false, 'class' => 'btn-fecha-mochila']
    ) : $this->Html->link(
        '<button class="btn-fecha-mochila" disabled>fechar mochila</button>',
        $SSlug.'/mochila/fechar-o-ziper',
        ['escape' => false, 'class' => 'btn-fecha-mochila']
    ) ?>
    </div>