<style type="text/css">
    .footer-total{
        display: none;
    }
    .minha-conta-container {
        padding-right: 230px;
        padding-left: 230px;
        padding-top: 160px;
        margin-left: 0;
        margin-right: 0;
        width: 100%;
    }
    .title-page {
        float: left;
        color: #5089cf;
    }
    .voltar-page {
        float: right;
        margin-top: 50px;
        margin-right: 15px;
        font-size: 18px;
    }
    #logexpress-btn {
        display: none!important;
    }
    .button-tente-novamente {
        background: #489f36;
        padding: 5px 10px;
        margin-bottom: 5px;
    }
    .button-tente-novamente:hover {
        background: #327325;
        color: rgba(255,255,255,.8);
    }
    @media all and (max-width: 1279px) {
        .minha-conta-container {
            padding-right: 70px;
            padding-left: 70px;
            margin-top: 50px;
        }
    }
    @media all and (max-width: 430px) {
        .minha-conta-container {
            padding-right: 50px;
            padding-left: 50px;
        }
    }
</style>

<script type="text/javascript">
    ga('require', 'ecommerce');

    ga('ecommerce:clear');

    ga('ecommerce:addTransaction', {
      'id': '<?= $pedido->id ?>',
      'affiliation': 'LogFitness',
      'revenue': '<?= $pedido->valor ?>',
      'shipping': '0'
    }); 
</script>

<?php foreach ($pedido->pedido_itens as $item) { ?>
<script type="text/javascript">
    ga('ecommerce:addItem', {
      'id': '<?= $pedido->id ?>',
      'name': '<?= $item->produto->produto_base->name ?>',
      'sku': '<?= $item->produto->cod ?>',
      'price': '<?= $item->preco ?>',
      'quantity': '<?= $item->quantidade; ?>'
    });
</script>
<?php } ?>

<script type="text/javascript">
    ga('ecommerce:send');
</script>

<div class="container minha-conta-container">
    <h2 class="font-bold">Pedido #<?= $pedido->id ?></h2>

    <h4 class="font-bold">Status do pedido: <?= $pedido->pedido_status->name ?><?= $pedido->pedido_status_id == 7 ? ' - Falha no pagamento' : '' ?></h4>
    <?php if($pedido->pedido_status_id == 7) { ?>
        <?= $this->Html->link('Tente novamente', '/mochila/fechar-o-ziper/retormar-pedido/'.$pedido->id, ['class' => 'button button-tente-novamente'])?>
    <?php } ?>
    <p>Data do pedido: <?= $pedido->created ?></p>


    <p>Você pode acompanhar seus pedidos e/ou obter mais detalhes acessando <strong><?= $this->Html->link('acompanhar pedido', '/'.$AcademiaSession->slug.'/rastreio-rapido', ['target' => '_blank'])?></strong></p>

    <?php
    if($pedido->pedido_status_id == 2){
        echo '<br><br>';
        echo 'Aguardando confirmação - Se você passou seu cartão é só aguardar a liberação da operadora ;) ou se for boleto esperamos o pagamento e a liberação do banco.';
    } else if($pedido->pedido_status_id == 3) {
        echo '<br><br>';
        echo 'Pago - Já foi aprovado pela operadora de cartão :) deixa que todo resto é com a gente!';
    } else if($pedido->pedido_status_id == 7) {
        echo '<br><br>';
        echo 'Cancelado - Ocorreu algum erro na transação com seu cartão :(';
    }
    ?>
</div>