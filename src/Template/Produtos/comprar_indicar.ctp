<style type="text/css">
    .box-close-mochila {
        width: 0;
    }
    .mochila {
        width: 230px;
        opacity: 1;
    }
    .fundo-carrinho {
        margin-left: 0;
    }
    .cupom-desconto, 
    .total-compra, 
    .btn-fecha-mochila {
        right: 0px;
    }
</style>

<script type="text/javascript">
    var valor_total;
    $('.mochila, .lixo, .produto-mais, .produto-menos').click(function(e) {
        e.preventDefault();
        valor_total = $('#carrinho-total').html();
        if(valor_total == 'TOTAL: R$ 0,00') {
            $('.btn-fecha-mochila').attr('disabled', 'disabled');
        } else {
            $('.btn-fecha-mochila').removeAttr('disabled');
        }
    });

    $('.lixo').click(function(e){
        e.preventDefault();
        var produto_id  = $(this).attr('data-id');
        var produto     = $('#produto-' + produto_id);
        if(produto.val() > 0){
            produto.val(parseInt(produto.val()) - parseInt(produto.val()));
            var quantidade  = (parseInt(produto.val()));

            calc_produto_carrinho(produto_id, 'subtracao');
            carrinho_edit(produto_id, quantidade);
        }
        $('#fundo-'+produto_id).hide();
    });

    $('.produto-menos').click(function(){
        var produto_id  = $(this).attr('data-id');
        var produto     = $('#produto-' + produto_id);
        if(produto.val() > 0){
            produto.val((parseInt(produto.val()) - 1));
            var quantidade  = (parseInt(produto.val()));

            calc_produto_carrinho(produto_id, 'subtracao');
            carrinho_edit(produto_id, quantidade);
        }
    });

    $('.produto-mais').click(function(){
        var produto_id  = $(this).attr('data-id');
        var produto     = $('#produto-' + produto_id);
        var quantidade  = (parseInt(produto.val()) + 1);
        produto.val(quantidade);

        calc_produto_carrinho(produto_id, 'soma');
        carrinho_edit(produto_id, quantidade);

    });

    $('.btn-fecha-mochila').click(function() {
        location.href =  WEBROOT_URL + 'professores/admin/aluno/indicar-produtos/';
    });

    $('.detalhes-btn').click(function(e) {
        e.preventDefault();
        $('#expandir-mochila').click();
    });

    $('.img-produto-principal, .img-combina-com, .produto-grade').draggable({
        helper: 'clone'
    });

    $('.produtos-mochila').droppable({

        drop: function(evt, ui){

            if(ui.draggable.parent()[0] == this){
                return;
            }

            var t = $(this);
            var e = ui.draggable;
            var diff = {x: evt.pageX - ui.position.left, y: evt.pageY - ui.position.top};
            e.draggable('option','helper','');

            $.get(WEBROOT_URL + 'produtos/comprar/' + e.attr('val'),
                function (data) {
                    $('.laterais.mochila').html('');
                    $('.laterais.mochila').html(data);
                    $('.produtos-mochila').animate({ 'width' : '228px', 'opacity' : 1 }, function() {
                        $('.info-direita-mochila').fadeIn(200);
                    });
                    loading.hide(1);
                });
        }
    });

    /**
     * CARRINHO - CALCULO VALOR TOTAL PRODUTO
     * @param id
     * @param operacao
     */
    function calc_produto_carrinho(id, operacao){

        var element = $('#produto-' + id);
        var preco_unitario  = element.attr('data-preco');
        var quantidade      = element.val();

        var preco_total     = parseFloat((preco_unitario * quantidade));
        //var subtotal        = parseFloat($('#carrinho-subtotal').attr('data-subtotal'));
        //var total           = parseFloat($('#carrinho-total').attr('data-total'));

        $('#total-produto-' + id)
            .html('R$ ' + preco_total.toFixed(2).replace('.', ','))
            .attr('data-total', preco_total.toFixed(2));

        var subtotal = 0;
        $('.total-produto').each(function(){
            subtotal = parseFloat(subtotal + parseFloat($(this).attr('data-total')));
        });
        $('#carrinho-subtotal').html('SUBTOTAL: R$ ' + subtotal.toFixed(2).replace('.', ','));
        $('#carrinho-total').html('TOTAL: R$ ' + subtotal.toFixed(2).replace('.', ','));

        if(subtotal == 0){
            $('.btn-fecha-mochila').attr('disabled', true);
        }else{
            $('.btn-fecha-mochila').attr('disabled', false);
        }
    }

    /**
     * CARRINHO - ALTERAR ITENS
     * @param id
     * @param qtd
     */
    function carrinho_edit(id, qtd){
        $.get(WEBROOT_URL + '/mochila/alterar/' + id + '/' + qtd,
            function(data){
                console.log('Itens alterados...');
            });
    }
    
    var medida_mochila_aberta = $(document).width() - 230;
    var medida_metade = medida_mochila_aberta/2;
    var correcao = medida_metade - 247 + 50;
    var ctrl_abrir = 0;

    $('#expandir-mochila').click(function(){
        if(ctrl_abrir == 0) {
            $('.produtos-mochila').animate({ 'width' : medida_mochila_aberta - 2 }, 2000);
            $('.mochila').animate({ 'width' : medida_mochila_aberta }, 2000, function() {
                $('.info-prod, .valor-produto').fadeIn(400);
            });
            $('.mochila-titulo').animate({ 'paddingLeft' : correcao }, 2000);
            $('#expandir-mochila').removeClass('seta-animate-fechar').addClass('seta-animate-abrir');
            $('.info-direita-mochila').animate({paddingRight : '30px', width : '300px'}, 2000);
            $('.close-mochila').css('pointer-events', 'none');
            ctrl_abrir = 1;
        } else {
            $('.mochila').animate({ 'width' : '230px' }, 2000);
            $('.produtos-mochila').animate({ 'width' : '228px' }, 2000);
            $('.info-prod, .valor-produto').fadeOut(400);
            $('.mochila-titulo').animate({ 'paddingLeft' : '30px' }, 2000);
            $('#expandir-mochila').removeClass('seta-animate-abrir').addClass('seta-animate-fechar');
            $('.info-direita-mochila').animate({paddingRight : 0, width : '130px'}, 2000);
            $('.close-mochila').css('pointer-events', 'auto');
            ctrl_abrir = 0;
        }
    });

    $('.fechar-mochila-x').click(function() {
        $('.close-mochila').click();
    });
</script>

<div class="laterais mochila">
    <i class="fa fa-close fechar-mochila-x"></i>
    <div class="fundo-titulo">
        <div class="col-sm-12">
            <span id="expandir-mochila" class="fa fa-arrow-left fa-2x expandir" style="color: white;"></span>
            <div class="titulos mochila-titulo"> mochila</div>
        </div>
    </div>
    <div class="produtos-mochila">
        <?php
            foreach ($s_produtos as $s_produto):
                $fotos = unserialize($s_produto->fotos);
                ?>

                <div id="fundo-<?= $s_produto->id ?>" class="col-md-12 fundo-carrinho">
                    <div class="imagem-produto-carrinho">
                        <?= $this->Html->link(
                            $this->Html->image('produtos/' . $fotos[0],
                                ['class' => 'dimensao-produto-carrinho', 'alt' => $s_produto->produto_base->name . ' ' . $s_produto->propriedade]
                            ),
                            '/produto_indicar/' . $s_produto->slug,
                            ['escape' => false]
                        ) ?>
                    </div>
                    <div class="col-md-4 text-center info-prod">
                        <?= $this->Html->link(
                            '<h1 class="font-14 h1-carrinho"><span>'
                            . $s_produto->produto_base->name . ' ' . $s_produto->produto_base->embalagem_conteudo . ' ' . $s_produto->propriedade .
                            '</span></h1>',
                            '/produto_indicar/' . $s_produto->slug,
                            ['escape' => false]
                        ) ?>
                        <p class="font-10"><span><?= strtoupper($s_produto->cod); ?></span></p>
                    </div>
                    <div class="text-right valor-produto">
                        <?= $s_produto->preco_promo ?
                            '<p><span><s>R$ ' . number_format($s_produto->preco, 2, ',', '.') . '</s></span></p>' :
                            ''; ?>
                        <p>
                        <span><b>R$ <?= $s_produto->preco_promo ?
                                    number_format($preco = $s_produto->preco_promo, 2, ',', '.') :
                                    number_format($preco = $s_produto->preco, 2, ',', '.'); ?></b>
                        </span>
                        </p>
                    </div>
                    <div class="info-direita-mochila">
                        <div class="text-right">
                            <b id="total-produto-<?= $s_produto->id ?>"
                               class="total-produto"
                               data-total="<?= number_format(($preco * $session_produto[$s_produto->id]), 2, '.', '') ?>">
                                R$ <?= number_format($total_produto = ($preco * $session_produto[$s_produto->id]), 2, ',', '.') ?>
                                <?php $subtotal += $total_produto; ?>
                            </b>
                        </div>
                        <div class="text-center qtd-produtos">
                            <input type="button" value='-' class="produto-menos" data-id="<?= $s_produto->id ?>"/>
                            <input class="text produto-qtd"
                                   id="produto-<?= $s_produto->id ?>"
                                   size="1"
                                   type="text"
                                   value="<?= $session_produto[$s_produto->id]; ?>"
                                   data-preco="<?= $s_produto->preco_promo ?
                                       number_format($s_produto->preco_promo, 2, '.', '') :
                                       number_format($s_produto->preco, 2, '.', ''); ?>"
                                   maxlength="2"/>
                            <input type="button" value='+' class="produto-mais" data-id="<?= $s_produto->id ?>"/>
                        </div>
                    </div>
                    <div class="info-direita-mochila">
                        <div class="lixo-div">
                            <a class="lixo" href="#" data-id="<?= $s_produto->id ?>"><span class="fa fa-trash" style="color: darkgray"></span></a>
                        </div>

                        <div class="detalhes">
                            <a href="/produto/<?= $s_produto->slug ?>" class="detalhes-btn"><span style="font-size: 14px; font-weight: 700">+</span> detalhes</a>
                        </div>
                    </div>
                </div>
                <?php
            endforeach;
        ?>
    </div>

    <div class="limpar-mochila col-sm-12">
        <button class="btn-limpar-mochila"><span class="fa fa-trash"></span> limpar mochila</button>    </div>
    <div class="cupom-desconto col-sm-12">
        <div class="col-xs-7">
            <input type="text" name="cupon-desconto" placeholder=" cupom desconto">
        </div>
        <div class="col-xs-4">
            <button class="btn-cupom" type="">aplicar</button>
        </div>
    </div>
    <div class="total-compra col-sm-12">
        <p class="frete-txt"><b>*FRETE GRÁTIS</b></p>
        <p class="total-txt"><b id="carrinho-total" data-total="<?= number_format($subtotal, 2, '.', '')?>">TOTAL: R$ <?= number_format($subtotal, 2, ',', '.')?></b></p>
    </div>
    <div class="fechar-mochila">
    <?= $subtotal > 0 ? 
        $this->Html->link(
        '<button class="btn-fecha-mochila">indicar mochila</button>',
        '/professores/admin/aluno/indicar-produtos/',
        ['escape' => false, 'class' => 'btn-fecha-mochila']
    ) : $this->Html->link(
        '<button class="btn-fecha-mochila" disabled>indicar mochila</button>',
        '/professores/admin/aluno/indicar-produtos/',
        ['escape' => false, 'class' => 'btn-fecha-mochila']
    ) ?>
    </div>
</div>