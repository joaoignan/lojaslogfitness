<!--MOCHILA-->
<div class="container">
    <div class="col-md-12 col-xs-12">
        <h2 class="h2-carrinho">
            <?= $this->Html->image('mochila-compras-log-fitness-branca.png')?>
            MOCHILA
        </h2>
    </div>
    <?php
    $subtotal = 0;
    if(empty($produtos)){
        ?>
        <div class="col-md-12 col-xs-12 fundo-carrinho">
            <div class="col-md-6">
                <h4 class="font-bold">Mochila vazia</h4>
                <p>
                    Para colocar itens na mochila, busque pelo produto em nosso site e clique no botão "Comprar"
                    na página do mesmo.
                </p>
                <br class="clear">
                <?= $this->Html->link(
                    '<i class="fa fa-arrow-circle-left"></i> Voltar para a loja',
                    $SSlug.'/home',
                    ['class' => 'btn btn-primary font-bold', 'escape' => false])
                ?>
            </div>
            <div class="col-md-6" style="border-left: 1px solid #EEE;">
                <h4 class="font-bold">Por que minha mochila está vazia?</h4>
                <p>
                    1. Os itens selecionados permaneceram em sua mochila por mais tempo do que o permitido pelo sistema.
                    Este período pode variar de acordo com cada item selecionado.
                    <br>
                    2. Você retirou os itens da mochila.
                    <br>
                    3. Você utilizou as opções Avançar/Voltar da barra de ferramentas do seu navegador.
                </p>
            </div>

        </div>
        <?php
    }else{
        ?>

        <div class="col-md-12 col-sm-12 tabela-coluna-carrinho">
            <div class="col-md-5 text-center table-title">
                <span>PRODUTO</span>
            </div>

            <div class="col-md-2 text-center">
                <span>QUANTIDADE</span>
            </div>

            <div class="col-md-2 col-xs-2 text-right">
                <span>VALOR UNITÁRIO</span>
            </div>

            <div class="col-md-3 col-xs-2 text-right">
                <span>VALOR TOTAL</span>
            </div>
        </div>

        <?php
        foreach ($produtos as $produto):
            $fotos = unserialize($produto->fotos);
            ?>

            <div class="col-md-12 fundo-carrinho">
                <div class="imagem-produto-carrinho col-md-1">
                    <?= $this->Html->link(
                        $this->Html->image('produtos/' . $fotos[0],
                            ['class' => 'dimensao-produto-carrinho', 'alt' => $produto->produto_base->name . ' ' . $produto->propriedade]
                        ),
                        '/produto/' . $produto->slug,
                        ['escape' => false]
                    ) ?>
                </div>
                <div class="col-md-4 text-center">
                    <?= $this->Html->link(
                        '<h1 class="font-14 h1-carrinho"><span>'
                        . $produto->produto_base->name . ' ' . $produto->produto_base->embalagem_conteudo . ' ' . $produto->propriedade .
                        '</span></h1>',
                        '/produto/' . $produto->slug,
                        ['escape' => false]
                    ) ?>
                    <p class="font-10"><span><?= strtoupper($produto->cod); ?></span></p>
                    <p class="font-10"><span>Fabricante: <?= $produto->produto_base->marca->name; ?></span></p>
                </div>
                <div class="col-md-2 margin-20 text-center">
                    <input type="button" value='-' class="produto-menos" data-id="<?= $produto->id ?>"/>
                    <input class="text produto-qtd"
                           id="produto-<?= $produto->id ?>"
                           size="1"
                           type="text"
                           value="<?= $session_produto[$produto->id]; ?>"
                           data-preco="<?= $produto->preco_promo ?
                               number_format($produto->preco_promo, 2, '.', '') :
                               number_format($produto->preco, 2, '.', ''); ?>"
                           maxlength="2"/>
                    <input type="button" value='+' class="produto-mais" data-id="<?= $produto->id ?>"/>
                </div>
                <div class="col-md-2 margin-20 text-right">
                    <?= $produto->preco_promo ?
                        '<p><span>De <s>R$ ' . number_format($produto->preco, 2, ',', '.') . '</s></span></p>' :
                        ''; ?>
                    <p>
                    <span>Por <b>R$ <?= $produto->preco_promo ?
                                number_format($preco = $produto->preco_promo, 2, ',', '.') :
                                number_format($preco = $produto->preco, 2, ',', '.'); ?></b>
                    </span>
                    </p>
                </div>
                <div class="col-md-3 margin-20 text-right">
                    <b id="total-produto-<?= $produto->id ?>"
                       class="total-produto"
                       data-total="<?= number_format(($preco * $session_produto[$produto->id]), 2, '.', '') ?>">
                        R$ <?= number_format($total_produto = ($preco * $session_produto[$produto->id]), 2, ',', '.') ?>
                        <?php $subtotal += $total_produto; ?>
                    </b>
                </div>
            </div>
            <?php
        endforeach;
        ?>
        <!--
            <div class="col-md-12 subtotal-carrinho">
                <p id="carrinho-subtotal" data-subtotal="<?= number_format($subtotal, 2, '.', '')?>">
                    SUBTOTAL: R$ <?= number_format($subtotal, 2, ',', '.')?>
                </p>
            </div>
        -->
        <div class="col-md-12 total-carrinho">
            <p>
                <b id="carrinho-total" data-total="<?= number_format($subtotal, 2, '.', '')?>">
                    TOTAL: R$ <?= number_format($subtotal, 2, ',', '.')?>
                </b>
            </p>
        </div>

        <?php if(!isset($session_cliente->academia_id)){ ?>
            <div class="col-md-12 text-center" style="margin-top: 25px;">
                <strong id="info-cadastro">* É necessário informar qual academia você frequenta para finalizar a compra!</strong>
                <p id="info-cadastro">Acesse minha conta, opção meus dados e indique sua academia.</p>
            </div>
        <?php } ?>

        <div class="col-xs-12">
            <?= $this->Html->link(
                '<button class="button-finalizar col-md-4 col-xs-12">FINALIZAR</button>',
                '/mochila/fechar-o-ziper',
                ['escape' => false, 'class' => 'col-xs-12']
            )?>
        </div>
        <?php
    }
    ?>
</div>
