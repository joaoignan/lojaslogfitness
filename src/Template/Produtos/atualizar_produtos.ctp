<?= $this->Element('lista_produtos', [
    'produtos' => $produtos,
]); ?>

<script>
    $('.abrir-email').click(function(e){
            e.preventDefault();
            var id = $(this).attr('data-id');
            $('.enviar-whats').slideUp("slow");
            $('.enviar-email').slideToggle("slow");
        });
        $('.abrir-whats').click(function(e){
            var id = $(this).attr('data-id');
            $('.enviar-email').slideUp("slow");
            $('.enviar-whats').slideToggle("slow");
        });
</script>