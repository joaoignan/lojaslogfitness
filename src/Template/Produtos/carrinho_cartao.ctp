<link rel="stylesheet prefetch" href="https://fonts.googleapis.com/css?family=Inconsolata">

<style>
    .mochila-fechada {
        padding-right: 230px;
        padding-left: 230px;
        margin-left: 0;
        margin-right: 0;
        width: 100%;
    }
    .fundo-carrinho {
    background: #ffffff;
    padding: 0 5px;
    height: 70px;
    margin-left: 0px;
    }

    @media all and (max-width: 1279px) {
        .mochila-fechada {
            padding-right: 70px;
            padding-left: 70px;
        }
    }
</style>

<script>
    $('body').addClass('smartnot');
</script>

<div class="container mochila-fechada">

<!--MOCHILA-->
        <div class="col-md-12">
            <h2 class="h2-carrinho">
                <?= $this->Html->image('mochila-compras-log-fitness-branca.png')?>
                MOCHILA FECHADA
            </h2>
        </div>

        <h4 class="text-center col-xs-12 font-bold">Escolha uma forma de pagamento</h4>

        <div class="col-md-12 bg-white">
            <div class="col-md-12 ">
                <div class="col-md-6 forma-pagamento">
                    <button class="btn col-md-6 text-center" id="pag_boleto">
                        <i class="fa fa-barcode"></i>
                        <br class="clear">
                        Boleto
                    </button>
                </div>

                <div class="col-md-6 forma-pagamento">
                    <button class="btn col-md-6" id="pag_cc">
                        <i class="fa fa-credit-card"></i>
                        <br class="clear">
                        Cartão de Crédito
                    </button>
                </div>

            </div>



            <!-- Boleto -->
            <div class="col-md-12 bloco-pagamento" id="bloco_boleto">
                <h4 class="font-bold">Clique no botão abaixo para gerar seu boleto para impressão.</h4>
                <h5 class="font-bold">
                    Obs.: Permita que popups deste site sejam sempre abertas para que o boleto seja
                    visualizado corretamente.
                </h5>

                <button id="pagarBoleto" class="btn">
                    <i class="fa fa-barcode"></i>
                    Gerar Boleto
                </button>

                <a data-href="#"
                   data-cid="<?= $PedidoCliente->cliente_id ?>"
                   data-pid="<?= $PedidoCliente->id ?>"
                   data-k="<?= $token ?>"
                   id="boleto_url"
                   class="hide"
                   target="_blank">
                    <button id="imprimirBoleto" class="btn">
                        <i class="fa fa-print"></i>
                        Boleto Gerado. Clique Para Imprimir
                    </button>
                </a>
            </div>


            <style type="text/css">
                .checkout .form fieldset {
                    border: none;
                    padding: 0;
                    padding: 10px 0;
                    position: relative;
                    clear: both;
                }

                .checkout .form fieldset .fieldset-expiration {
                    float: left;
                    width: 60%;
                }

                .checkout .form fieldset .select {
                    width: 84px;
                    margin-right: 12px;
                    float: left;
                }

                .checkout .form .fieldset-ccv {
                    clear: none;
                    float: right;
                    width: 86px;
                }

                .checkout .form label {
                    display: block;
                    text-transform: uppercase;
                    font-size: 11px;
                    color: hsla(0, 0, 0, .6);
                    margin-bottom: 5px;
                    font-weight: bold;
                    font-family: Inconsolata;
                }

                .checkout .form input,
                .checkout .form .select {
                    width: 100%;
                    height: 38px;
                    color: hsl(0, 0, 20);
                    padding: 10px;
                    border-radius: 5px;
                    font-size: 15px;
                    outline: none!important;
                    border: 1px solid hsla(0, 0, 0, 0.3);
                    box-shadow: inset 0 1px 4px hsla(0, 0, 0, 0.2);
                } 

                .checkout .form input .input-cart-number,
                .checkout .form .select .input-cart-number {
                    width: 82px;
                    display: inline-block;
                    margin-right: 8px;
                }

                .checkout .form input .input-cart-number:last-child,
                .checkout .form .select .input-cart-number:last-child {
                    margin-right: 0;
                }

                .checkout .form .select {
                    position: relative;
                }

                .checkout .form .select:after {
                    content: '';
                    border-top: 8px solid #222;
                    border-left: 4px solid transparent;
                    border-right: 4px solid transparent;
                    position: absolute;
                    z-index: 2;
                    top: 14px;
                    right: 10px;
                    pointer-events: none;
                }

                .checkout .form .select select {
                    appearance: none;
                    position: absolute;
                    padding: 0;
                    border: none;
                    width: 100%;
                    outline: none!important;
                    top: 6px;
                    left: 6px;
                    background: none;
                }


                  }

                .checkout .form button {
                    width: 100%;
                    outline: none!important;
                    background: linear-gradient(180deg, #49a09b, #3d8291);
                    text-transform: uppercase;
                    font-weight: bold;
                    border: none;
                    box-shadow: none;
                    text-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
                    margin-top: 90px;
                }

                .checkout .form button .fa {
                    margin-right: 6px;
                }

                .checkout {
                  margin: 150px auto 30px;
                  position: relative;
                  width: 460px;
                  background: white;
                  border-radius: 15px;
                  padding: 160px 45px 30px;
                  box-shadow: 0 10px 40px hsla(0, 0, 0, .1);
                }

                .credit-card-box {
                    perspective: 1000;
                    width: 400px;
                    height: 280px;
                    position: absolute;
                    top: -112px;
                    left: 50%;
                    transform: translateX(-50%);
                    font-family: Inconsolata;
                }

                .credit-card-box:hover .flip,
                .credit-card-box.hover .flip {
                    transform: rotateY(180deg);
                }

                .credit-card-box .front,
                .credit-card-box .back {
                    width: 400px;
                    height: 250px;
                    border-radius: 15px;
                    backface-visibility: hidden;
                    background: linear-gradient(135deg, #bd6772, #53223f);
                    position: absolute;
                    color: #fff;
                    font-family: Inconsolata;
                    top: 0;
                    left: 0;
                    text-shadow: 0 1px 1px hsla(0, 0, 0, 0.3);
                    box-shadow: 0 1px 6px hsla(0, 0, 0, 0.3);
                }

                .credit-card-box:before {
                    content: '';
                    position: absolute;
                    width: 100%;
                    height: 100%;
                    top: 0;
                    left: 0;
                    background: url('http://cdn.flaticon.com/svg/44/44386.svg') no-repeat center;
                    background-size: cover;
                    opacity: .05;
                }

                .credit-card-box .flip {
                    transition: 0.6s;
                    transform-style: preserve-3d;
                    position: relative;
                }

                .credit-card-box .logo {
                    position: absolute;
                    top: 9px;
                    right: 20px;
                    width: 60px;
                }
                
                .credit-card-box .logo svg {
                    width: 100%;
                    height: auto;
                    fill: #fff;
                }

                .credit-card-box .front {
                    z-index: 2;
                    transform: rotateY(0deg);
                }

                .credit-card-box .back {
                    transform: rotateY(180deg);
                }
                
                .credit-card-box .back .logo {
                    top: 185px;
                }

                .credit-card-box .chip {
                    position: absolute;
                    width: 60px;
                    height: 45px;
                    top: 20px;
                    left: 20px;
                    background: linear-gradient(135deg, hsl(269,54%,87%) 0%,hsl(200,64%,89%) 44%,hsl(18,55%,94%) 100%);;
                    border-radius: 8px;
                }

                .credit-card-box .chip:before {
                    content: '';
                    position: absolute;
                    top: 0;
                    bottom: 0;
                    left: 0;
                    right: 0;
                    margin: auto;
                    border: 4px solid hsla(0, 0, 50, .1);
                    width: 80%;
                    height: 70%;
                    border-radius: 5px;
                }
                
                .credit-card-box .strip {
                    background: linear-gradient(135deg, hsl(0, 0, 25%), hsl(0, 0, 10%));
                    position: absolute;
                    width: 100%;
                    height: 50px;
                    top: 30px;
                    left: 0;
                }

                .credit-card-box .number {
                    position: absolute;
                    margin: 0 auto;
                    top: 103px;
                    left: 19px;
                    font-size: 38px;
                }

                .credit-card-box label {
                    font-size: 10px;
                    letter-spacing: 1px;
                    text-shadow: none;
                    text-transform: uppercase;
                    font-weight: normal;
                    opacity: 0.5;
                    display: block;
                    margin-bottom: 3px;
                }

                .credit-card-box .card-holder,
                .credit-card-box .card-expiration-date {
                    position: absolute;
                    margin: 0 auto;
                    top: 180px;
                    left: 19px;
                    font-size: 22px;
                    text-transform: capitalize;
                }

                .credit-card-box .card-expiration-date {
                    text-align: right;
                    left: auto;
                    right: 20px;
                }

                .credit-card-box .ccv {
                    height: 36px;
                    background: #fff;
                    width: 91%;
                    border-radius: 5px;
                    top: 110px;
                    left: 0;
                    right: 0;
                    position: absolute;
                    margin: 0 auto;
                    color: #000;
                    text-align: right;
                    padding: 10px;
                }

                .credit-card-box label {
                    margin: -25px 0 14px;
                    color: #fff;
                }
            </style>

            <script type="text/javascript">
                $(document).ready(function() {
                    $('.input-cart-number').on('keyup change', function(){
                      $t = $(this);
                      
                      if ($t.val().length > 3) {
                        $t.next().focus();
                      }
                      
                      var card_number = '';
                      $('.input-cart-number').each(function(){
                        card_number += $(this).val() + ' ';
                        if ($(this).val().length == 4) {
                          $(this).next().focus();
                        }
                      })
                      
                      $('.credit-card-box .number').html(card_number);
                    });

                    $('#card-holder').on('keyup change', function(){
                      $t = $(this);
                      $('.credit-card-box .card-holder div').html($t.val());
                    });

                    $('#card-holder').on('keyup change', function(){
                      $t = $(this);
                      $('.credit-card-box .card-holder div').html($t.val());
                    });

                    $('#card-expiration-month, #card-expiration-year').change(function(){
                      m = $('#card-expiration-month option').index($('#card-expiration-month option:selected'));
                      m = (m < 10) ? '0' + m : m;
                      y = $('#card-expiration-year').val().substr(2,2);
                      $('.card-expiration-date div').html(m + '/' + y);
                    })

                    $('#card-ccv').on('focus', function(){
                      $('.credit-card-box').addClass('hover');
                    }).on('blur', function(){
                      $('.credit-card-box').removeClass('hover');
                    }).on('keyup change', function(){
                      $('.ccv div').html($(this).val());
                    });


                    /*--------------------
                    CodePen Tile Preview
                    --------------------*/
                    setTimeout(function(){
                      $('#card-ccv').focus().delay(1000).queue(function(){
                        $(this).blur().dequeue();
                      });
                    }, 500);
                });
            </script>

            <div class="col-md-12 bloco-pagamento">
                <div class="checkout">
                  <div class="credit-card-box">
                    <div class="flip">
                      <div class="front">
                        <div class="chip"></div>
                        <div class="logo">
                          <svg version="1.1" id="visa" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                               width="47.834px" height="47.834px" viewBox="0 0 47.834 47.834" style="enable-background:new 0 0 47.834 47.834;">
                            <g>
                              <g>
                                <path d="M44.688,16.814h-3.004c-0.933,0-1.627,0.254-2.037,1.184l-5.773,13.074h4.083c0,0,0.666-1.758,0.817-2.143
                                         c0.447,0,4.414,0.006,4.979,0.006c0.116,0.498,0.474,2.137,0.474,2.137h3.607L44.688,16.814z M39.893,26.01
                                         c0.32-0.819,1.549-3.987,1.549-3.987c-0.021,0.039,0.317-0.825,0.518-1.362l0.262,1.23c0,0,0.745,3.406,0.901,4.119H39.893z
                                         M34.146,26.404c-0.028,2.963-2.684,4.875-6.771,4.875c-1.743-0.018-3.422-0.361-4.332-0.76l0.547-3.193l0.501,0.228
                                         c1.277,0.532,2.104,0.747,3.661,0.747c1.117,0,2.313-0.438,2.325-1.393c0.007-0.625-0.501-1.07-2.016-1.77
                                         c-1.476-0.683-3.43-1.827-3.405-3.876c0.021-2.773,2.729-4.708,6.571-4.708c1.506,0,2.713,0.31,3.483,0.599l-0.526,3.092
                                         l-0.351-0.165c-0.716-0.288-1.638-0.566-2.91-0.546c-1.522,0-2.228,0.634-2.228,1.227c-0.008,0.668,0.824,1.108,2.184,1.77
                                         C33.126,23.546,34.163,24.783,34.146,26.404z M0,16.962l0.05-0.286h6.028c0.813,0.031,1.468,0.29,1.694,1.159l1.311,6.304
                                         C7.795,20.842,4.691,18.099,0,16.962z M17.581,16.812l-6.123,14.239l-4.114,0.007L3.862,19.161
                                         c2.503,1.602,4.635,4.144,5.386,5.914l0.406,1.469l3.808-9.729L17.581,16.812L17.581,16.812z M19.153,16.8h3.89L20.61,31.066
                                         h-3.888L19.153,16.8z"/>
                              </g>
                            </g>
                          </svg>
                        </div>
                        <div class="number"></div>
                        <div class="card-holder">
                          <label>Card holder</label>
                          <div></div>
                        </div>
                        <div class="card-expiration-date">
                          <label>Expires</label>
                          <div></div>
                        </div>
                      </div>
                      <div class="back">
                        <div class="strip"></div>
                        <div class="logo">
                          <svg version="1.1" id="visa" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                               width="47.834px" height="47.834px" viewBox="0 0 47.834 47.834" style="enable-background:new 0 0 47.834 47.834;">
                            <g>
                              <g>
                                <path d="M44.688,16.814h-3.004c-0.933,0-1.627,0.254-2.037,1.184l-5.773,13.074h4.083c0,0,0.666-1.758,0.817-2.143
                                         c0.447,0,4.414,0.006,4.979,0.006c0.116,0.498,0.474,2.137,0.474,2.137h3.607L44.688,16.814z M39.893,26.01
                                         c0.32-0.819,1.549-3.987,1.549-3.987c-0.021,0.039,0.317-0.825,0.518-1.362l0.262,1.23c0,0,0.745,3.406,0.901,4.119H39.893z
                                         M34.146,26.404c-0.028,2.963-2.684,4.875-6.771,4.875c-1.743-0.018-3.422-0.361-4.332-0.76l0.547-3.193l0.501,0.228
                                         c1.277,0.532,2.104,0.747,3.661,0.747c1.117,0,2.313-0.438,2.325-1.393c0.007-0.625-0.501-1.07-2.016-1.77
                                         c-1.476-0.683-3.43-1.827-3.405-3.876c0.021-2.773,2.729-4.708,6.571-4.708c1.506,0,2.713,0.31,3.483,0.599l-0.526,3.092
                                         l-0.351-0.165c-0.716-0.288-1.638-0.566-2.91-0.546c-1.522,0-2.228,0.634-2.228,1.227c-0.008,0.668,0.824,1.108,2.184,1.77
                                         C33.126,23.546,34.163,24.783,34.146,26.404z M0,16.962l0.05-0.286h6.028c0.813,0.031,1.468,0.29,1.694,1.159l1.311,6.304
                                         C7.795,20.842,4.691,18.099,0,16.962z M17.581,16.812l-6.123,14.239l-4.114,0.007L3.862,19.161
                                         c2.503,1.602,4.635,4.144,5.386,5.914l0.406,1.469l3.808-9.729L17.581,16.812L17.581,16.812z M19.153,16.8h3.89L20.61,31.066
                                         h-3.888L19.153,16.8z"/>
                              </g>
                            </g>
                          </svg>

                        </div>
                        <div class="ccv">
                          <label>CCV</label>
                          <div></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <form class="form" autocomplete="off" novalidate>
                    <fieldset>
                      <label for="card-number">Card Number</label>
                      <input type="num" id="card-number" class="input-cart-number" maxlength="4" />
                      <input type="num" id="card-number-1" class="input-cart-number" maxlength="4" />
                      <input type="num" id="card-number-2" class="input-cart-number" maxlength="4" />
                      <input type="num" id="card-number-3" class="input-cart-number" maxlength="4" />
                    </fieldset>
                    <fieldset>
                      <label for="card-holder">Card holder</label>
                      <input type="text" id="card-holder" />
                    </fieldset>
                    <fieldset class="fieldset-expiration">
                      <label for="card-expiration-month">Expiration date</label>
                      <div class="select">
                        <select id="card-expiration-month">
                          <option></option>
                          <option>01</option>
                          <option>02</option>
                          <option>03</option>
                          <option>04</option>
                          <option>05</option>
                          <option>06</option>
                          <option>07</option>
                          <option>08</option>
                          <option>09</option>
                          <option>10</option>
                          <option>11</option>
                          <option>12</option>
                        </select>
                      </div>
                      <div class="select">
                        <select id="card-expiration-year">
                          <option></option>
                          <option>2016</option>
                          <option>2017</option>
                          <option>2018</option>
                          <option>2019</option>
                          <option>2020</option>
                          <option>2021</option>
                          <option>2022</option>
                          <option>2023</option>
                          <option>2024</option>
                          <option>2025</option>
                        </select>
                      </div>
                    </fieldset>
                    <fieldset class="fieldset-ccv">
                      <label for="card-ccv">CCV</label>
                      <input type="text" id="card-ccv" maxlength="3" />
                    </fieldset>
                    <button class="btn"><i class="fa fa-lock"></i> submit</button>
                  </form>
                </div>
            </div>


            <!-- Cartão de crédito -->
            <div class="col-md-12 bloco-pagamento" id="bloco_cc" style="display: none">
                <form id="cc" class="col-md-offset-4">
                    <div class="row">
                        <div class="col-md-3 form-group">
                            <label for="Instituicao">Bandeira do Cartão</label>
                            <select name="Instituicao"
                                    id="Instituicao"
                                    class="form-control col-md-6 validate[required]">
                                <option value="">Selecione...</option>
                                <option value="AmericanExpress">American Express</option>
                                <option value="Diners">Diners</option>
                                <option value="Mastercard">MasterCard</option>
                                <option value="Hipercard">Hipercard</option>
                                <option value="Hiper">Hiper</option>
                                <option value="Elo">Elo</option>
                                <option value="Visa">Visa</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="Numero">Número do Cartão*</label>
                            <input type="number"
                                   maxlength="16"
                                   name="Numero"
                                   class="form-control validate[required]"
                                   placeholder="Número do Cartão">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2 form-group">
                            <label for="ExpiracaoMes">Validade (Mês)</label>
                            <select name="ExpiracaoMes"
                                    id=""
                                    class="form-control col-md-6 validate[required]">
                                <option value="">Selecione...</option>
                                <?php for($m = 1; $m <= 12; $m++): ?>
                                    <option value="<?= str_pad($m, 2, 0, STR_PAD_LEFT) ?>"><?= str_pad($m, 2, 0, STR_PAD_LEFT) ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>

                        <div class="col-md-2 form-group">
                            <label for="ExpiracaoAno">Validade (Ano)</label>
                            <select name="ExpiracaoAno"
                                    id=""
                                    class="form-control col-md-6 validate[required]">
                                <option value="">Selecione...</option>
                                <?php for($m = 0; $m < 30; $m++): ?>
                                    <option value="<?= $m + date('y') ?>"><?= $m + date('Y') ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>

                        <div class="col-md-2 form-group">
                            <label for="CodigoSeguranca">Código de Segurança*</label>
                            <input type="number"
                                   maxlength="3"
                                   name="CodigoSeguranca"
                                   class="form-control validate[required]"
                                   placeholder="Código de Seguranca">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="Portador_Nome">Nome gravado no Cartão*</label>
                            <input type="text"
                                   name="Portador_Nome"
                                   class="form-control validate[required]"
                                   placeholder="Nome gravado no cartão">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="Portador_DataNascimento">Data de nascimento*</label>
                            <input type="text"
                                   name="Portador_DataNascimento"
                                   class="form-control br_date validate[required, custom[brDate]]"
                                   placeholder="Data de Nascimento">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 form-group">
                            <label for="Portador_Telefone">Telefone*</label>
                            <input type="text"
                                   name="Portador_Telefone"
                                   class="form-control phone-mask validate[required]"
                                   placeholder="Telefone">
                        </div>

                        <div class="col-md-3 form-group">
                            <label for="Portador_Identidade">CPF*</label>
                            <input type="text"
                                   name="Portador_Identidade"
                                   class="form-control cpf  validate[required, custom[cpf]]"
                                   placeholder="CPF">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="Parcelas">Quantidade de Parcelas*</label>
                            <select id="Parcelas" name="Parcelas" class="form-control validate[required]">
                                <option value="1">1x de <?= number_format($PedidoCliente->valor, 2, ',', '.') ?> (À Vista)</option>
                                <?php
                                /*if($parcelas > 1):
                                    for($i = 2; $i <= $parcelas; $i++):
                                        echo '<option value="'.$i.'">'.$i.'x </option>';
                                    endfor;
                                endif;*/
                                ?>
                            </select>
                        </div>
                    </div>

                </form>

                <button id="pagarCredito"
                        data-cid="<?= $PedidoCliente->cliente_id ?>"
                        data-pid="<?= $PedidoCliente->id ?>"
                        data-k="<?= $token ?>"
                        class="btn">
                    <i class="fa fa-credit-card-alt"></i>
                    Pagar
                </button>

                <h5 class="font-bold" id="cc_message"></h5>
            </div>
        </div>

        <h5 class="text-right col-xs-12"><?= $this->Html->image('poweredbymoip25px.png')?></h5>

        <div class="col-md-12 col-xs-12 tabela-coluna-carrinho">
            <div class="col-md-5 col-xs-4 text-center">
                <span>PRODUTO</span>
            </div>
            <div class="col-md-2 col-xs-3 text-center">
                <span>QUANTIDADE</span>
            </div>
            <div class="col-md-2 col-xs-2 text-center">
                <span>VALOR UNITÁRIO</span>
            </div>
            <div class="col-md-3 col-xs-3 text-right">
                <span>VALOR TOTAL</span>
            </div>
        </div>
        <?php
        $subtotal = 0;
        foreach ($PedidoCliente->pedido_itens as $item):
            $fotos = unserialize($item->produto->fotos);
            ?>
            <div class="col-md-12 col-xs-12 fundo-carrinho">
                <div class="imagem-produto-carrinho col-md-1 col-xs-1">
                    <?= $this->Html->link(
                        $this->Html->image('produtos/' . $fotos[0],
                            ['class' => 'dimensao-produto-carrinho', 'alt' => $item->produto->produto_base->name . ' ' . $item->produto->propriedade]
                        ),
                        '/produto/' . $item->produto->slug,
                        ['escape' => false]
                    ) ?>
                </div>
                <div class="col-md-4 col-xs-4 text-center">
                    <?= $this->Html->link(
                        '<h1 class="font-14 h1-carrinho"><span>'
                        . $item->produto->produto_base->name . ' ' . $item->produto->produto_base->embalagem_conteudo . ' ' . $item->produto->propriedade .
                        '</span></h1>',
                        '/produto/' . $item->produto->slug,
                        ['escape' => false]
                    ) ?>
                    <p class="font-10"><span><?= strtoupper($item->produto->cod); ?></span></p>
                    <p class="font-10"><span>Fabricante: <?= $item->produto->produto_base->marca->name; ?></span></p>
                </div>
                <div class="col-md-2 col-xs-2 margin-20 text-center">
                    <?= $item->quantidade; ?>
                </div>
                <div class="col-md-2 col-xs-2 margin-20 text-right">
                    <p>
                <span>
                    <b>R$ <?= number_format($item->preco, 2, ',', '.'); ?></b>
                </span>
                    </p>
                </div>
                <div class="col-md-3 col-xs-3 margin-20 text-right">
                    <p>
            <span>
                <b id="total-produto-<?= $item->produto->id ?>"
                   class="total-produto">
                    R$ <?= number_format($total_produto = ($item->preco * $item->quantidade), 2, ',', '.'); ?>
                    <?php $subtotal += $total_produto; ?>
                </b>
            </span>
                    </p>
                </div>
            </div>
            <?php
        endforeach;
        ?>
        <div class="col-md-12 subtotal-carrinho font-bold">
            <p>
                <span id="carrinho-subtotal" data-subtotal="<?= number_format($subtotal, 2, '.', '')?>">
                    FRETE GRÁTIS*: R$ 0,00
                </span>
            </p>
            <!--
            <p>
                <span id="carrinho-subtotal" data-subtotal="<?= number_format($subtotal, 2, '.', '')?>">
                    SUBTOTAL: R$ <?= number_format($subtotal, 2, ',', '.')?>
                </span>
            </p>
            -->
        </div>

        <div class="col-md-12 total-carrinho">
            <p>
                <span>
                    <b id="carrinho-total" data-total="<?= number_format($subtotal, 2, '.', '')?>">
                        TOTAL: R$ <?= number_format($subtotal, 2, ',', '.')?>
                    </b>
                </span>
            </p>
        </div>

        <div class="col-md-12 bg-white">
            <h4 class="font-bold">FRETE GRÁTIS*</h4>
            <h4 class="font-bold">Entrega na Academia: <?= $cliente->academia->shortname?></h4>
            <h5 class="font-bold">
                Endereço: <?=
                $cliente->academia->address.', '
                .$cliente->academia->number.' '
                .$cliente->academia->complement.' - '
                .$cliente->academia->area.' - '
                .$cliente->academia->city->name.'-'
                .$cliente->academia->city->uf
                ?>
            </h5>
            <h5 class="font-bold">
                Contato:
                <?= $cliente->academia->phone ?>
                <?= $cliente->academia->mobile ?  ' / '.$cliente->academia->mobile : ''?>
                <?= $cliente->academia->email ?  ' / '.$cliente->academia->email : ''?>
            </h5>
            <div class="row">
                <h5 class="col-md-2 font-bold">Professor da academia: </h5>
                <div class="col-md-3 form-group">
                    <?= $this->Form->input('professor_id',[
                        'id'    => 'professor_carrinho',
                        'type'  => 'select',
                        'div'       => false,
                        'label'     => false,
                        'empty'     => 'Selecione um professor...',
                        'options'   => $professores_list,
                        'value'     => $PedidoCliente->professor_id,
                        'data-pid'  => $PedidoCliente->id,
                        'class'     => 'form-control'
                    ])?>
                </div>
            </div>
            <h5>
                * Você deve retirar seu pedido na academia informada.
            </h5>
        </div>

        <div>
            <?php /*= $this->Html->link('Realizar Pagamento',
                SSLUG.'/mochila/checkout/'. $PedidoCliente->id.'/'.$PedidoCliente->cliente_id.'/'.str_replace(array(',', '.'), '', $subtotal))
                */
            ?>
        </div>
        <?php
    }
    ?>
</div>

<div id="MoipWidget"
     data-token="<?= isset($token) ? $token : '' ?>"
     callback-method-success="pagamento_sucesso"
     callback-method-error="pagamento_falha">

</div>
<script type='text/javascript' src='<?= MOIP_WIDGET ?>' charset=ISO-8859-1"></script>

<script type="text/javascript">
    /*parcelas = function() {

        var settings = {
            cofre: "",
            instituicao: "Visa",
            callback: "retornoCalculoParcelamento"
        };

        MoipUtil.calcularParcela(settings);
    };*/
</script>

<script type="text/javascript">
    /*retornoCalculoParcelamento = function(data) {
        alert(JSON.stringify(data));
    };*/
</script>