
<script>
	$(document).ready(function(){
		$("#total-marcas").click(function(){
	        $(".chek-marcas").prop('checked', $(this).prop('checked'));
	    });
	    $(".chek-marcas").change(function(){ 
	    	if (!$(this).prop("checked")) {
	    		$("#total-marcas").prop("checked",false);
	    	} 
	    });
	    $("#checkSubcategoriasMae").click(function(){
	        $(".checkSubcategoriasFilha").prop('checked', $(this).prop('checked'));
	    });
	    $(".checkSubcategoriasFilha").change(function(){ 
	    	if (!$(this).prop("checked")) {
	    		$("#checkSubcategoriasMae").prop("checked",false);
	    	} 
	    });
	});
</script>

<div class="container catalogo">
	<div class="row">
		<div class="col-xs-12 text-center logo-acad">
			<?php 
				if($academia->image != null){
                	echo $this->Html->link($this->Html->image('academias/'.$academia->image, ['alt' => $academia->shortname]),'/'.$academia->slug,['escape' => false]);
            	} else{
               		echo '<h4>'.$academia->shortname.'</h4>';
            	}
        	?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 text-center">
			<h2>Montar catálogo</h2>
			<p><strong>Monte catálogos personalizados com os produtos de sua(s) marca(s) favoritas ou com o foco em objetivo(s) específico(s)!</strong></p>
		</div>
	</div>
	<div class="">
		<div class="col-xs-12 catalogo-config text-left">
			<div class="row">
				<div class="col-xs-12">
					<h4 class="text-center">Marcas</h4>
						<div class="checkbox">
					    	<label>
				      			<input type="checkbox" id="total-marcas"> TODAS
					    	</label>
				    	</div>
					<?php foreach ($marcas as $marca) { ?>
						<div class="checkbox check-catalogo ">
					    	<label>
				      			<input type="checkbox" class="chek-marcas" value="<?= $marca->slug ?>"> <?=$marca->name ?>
					    	</label>
				    	</div>
					<?php } ?>
				</div>
				<div class="col-xs-12 col-md-4">
					<h4 class="text-center">Categorias</h4>
					<div class="collapse-categorias text-left">
						<?php foreach ($objetivos as $objetivo) { ?>
							<div data-toggle="collapse" data-id="<?= $objetivo->id ?>" id="<?= $objetivo->name ?>" class="colapses-objetivos" aria-expanded=false data-target="#objetivos-tipo-<?= $objetivo->id ?>">
								<div><?= $objetivo->name ?> <i class="fa fa-angle-right pull-right" aria-hidden="true"></i><i class="fa fa-angle-down pull-right" aria-hidden="true"></i></div>
							</div>
							<div class="clearfix"></div>
							<div id="objetivos-tipo-<?= $objetivo->id ?>" class="collapse">
										<div class="checkbox checkCategoria">
									    	<label>
								      			<input type="checkbox" id="checkTodas-<?= $objetivo->id ?>"> TODAS
									    	</label>
								    	</div>
								<?php foreach ($produtos_categorias_filtro as $categoria) { ?>
                        			<?php if($categoria->objetivo_id == $objetivo->id) { ?>
                        			<?php $tem_subcategoria = 0; ?>
                    				<?php foreach($subcategorias as $subcategoria) { ?>
		                                <?php if($subcategoria->categoria_id == $categoria->id) {
		                                    $tem_subcategoria = 1;
		                                } ?>
		                            <?php } ?>
		                            	<?php if($tem_subcategoria) { ?>
											<div class="checkbox checkCategoria">
										    	<label>
									      			<input type="checkbox" id="categoria-<?= $categoria->id ?>" class="checkCats-<?= $objetivo->id ?>" data-id="<?= $categoria->id ?>"> <?= $categoria->name ?> +
										    	</label>
									    	</div>
		                            	<?php } else { ?>
											<div class="checkbox checkCategoria">
										    	<label>
									      			<input type="checkbox" id="categoria-<?= $categoria->id ?>" class="checkCats-<?= $objetivo->id ?>"> <?= $categoria->name ?>
										    	</label>
									    	</div>
		                            	<?php } ?>
								    <?php } ?>
							    	<script>
							    		$(document).ready(function(){
							    			$("#checkTodas-<?= $objetivo->id ?>").click(function(){
							    				$is_checked = $(this).prop('checked');
							    				$(".checkCats-<?= $objetivo->id ?>").each(function(){
							    					$(this).prop('checked', $is_checked);

							    					if ($(this).prop("checked")) { 
											        	$(".subCat-"+$(this).attr('data-id')).css({'display':'block'});
											        	$(".checkSub-"+$(this).attr('data-id')).prop('checked', $(this).prop('checked'));
											    	} else {
											        	$(".subCat-"+$(this).attr('data-id')).css({'display':'none'});
											        	$(".checkSub-"+$(this).attr('data-id')).prop("checked",false);
											    	}
							    				});
										    });
										    $(".checkCats-<?= $objetivo->id ?>").change(function(){ 
										    	if (!$(this).prop("checked")) {
										    		$("#checkTodas-<?= $objetivo->id ?>").prop("checked",false);
										    	} 
										    });
										    $("#categoria-<?= $categoria->id ?>").change(function(){
										    	if ($(this).prop("checked")) { 
										        	$(".subCat-<?= $categoria->id ?>").css({'display':'block'});
										        	$(".checkSub-<?= $categoria->id ?>").prop('checked', $(this).prop('checked'));
										    	} else {
										        	$(".subCat-<?= $categoria->id ?>").css({'display':'none'});
										        	$(".checkSub-<?= $categoria->id ?>").prop("checked",false);
										    	}
										    });
							    		});
							    	</script>
                    			<?php } ?>
							</div>
							<div class="clearfix"></div>
						<?php } ?>
					</div>
				</div>
				<div class="col-xs-12 col-md-4">
					<h4 class="text-center">Sub categorias</h4>
						<div class="checkbox subCategoria">
					    	<label>
				      			<input type="checkbox" id="checkSubcategoriasMae"> TODAS
					    	</label>
				    	</div>
					<?php foreach($produtos_subcategorias_filtro as $subcategoria) { ?>
						<div class="checkbox subCategoria subCat-<?= $subcategoria->categoria_id ?>">
					    	<label>
				      			<input type="checkbox" class="checkSubcategoriasFilha checkSub-<?= $subcategoria->categoria_id ?>"> <?= $subcategoria->name  ?>
					    	</label>
				    	</div>
			    	<?php } ?>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-xs-12">
					<h4 class="text-center">Opções</h4>
					<div class="checkbox">
				    	<label>
			      			<input type="checkbox" checked="true"> Somente produtos em estoque
				    	</label>
			    	</div>
			    	<div class="checkbox">
				    	<label>
			      			<input type="checkbox"> Incluir valor do produto no catálogo
				    	</label>
			    	</div>
				</div>
				<div class="col-xs-12 col-md-4 col-md-offset-4">
					<button class="btn btn-success btn-sm btn-block"><i class="fa fa-book" aria-hidden="true"></i> Visualizar catálogo</button>
				</div>
			</div>
		</div>
	</div>
</div>