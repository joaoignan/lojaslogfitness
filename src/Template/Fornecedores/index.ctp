<div class="container informacoes text-center">
    <div class="col-lg-6 seja-um-parceiro">
        <?php /*= $this->Html->image('parceiro.png', ['alt' => 'Seja um parceiro'])*/ ?>
        <div class="col-md-12">
            <div id="map_canvas" class="map_canvas_parceiro"></div>
        </div>
        <br class="clear">
        <div class="absolut-center-100">
            <span class="titulo">SEJA UM PARCEIRO</span>
        </div>
    </div>
    <div class="col-lg-6 seja-um-parceiro">
        <p>
            Imagine sua marca com todos os produtos em um operador logístico, linkado a uma plataforma que tem mais 500 pontos de retirada espalhados em todos os estados do Brasil.
        </p>
        <div class="absolut-center-100">
            <a id="form-toggle-open">CADASTRE AQUI</a>
        </div>
    </div>
</div>

<div class="container info-central-box form-toggle" id="form-toggle">

    <!--<div class="col-lg-4 form-toggle-info">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam beatae culpa cum dolorum eos error incidunt inventore, natus odio, optio porro praesentium quae quis velit. Iusto magnam sint velit.
        <br>
        <br>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam beatae culpa cum dolorum eos error incidunt inventore, natus odio, optio porro praesentium quae quis velit. Iusto magnam sint velit.
    </div>-->

    <?= $this->Form->create(null, [
        'id'        => 'form-cadastrar-parceiro',
        'role'      => 'form',
        'default'   => false,
    ]) ?>

    <div class="form-group row">
        <div class="col-lg-8">
            <?= $this->Form->input('name', [
                'div'           => false,
                'label'         => 'Nome',
                'placeholder'   => 'Nome / Razão Social',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-4">
            <?= $this->Form->input('cnpj', [
                'div'           => false,
                'label'         => 'CNPJ',
                'placeholder'   => 'CNPJ',
                'class'         => 'validate[required, custom[cnpj]] form-control cnpj',
            ]) ?>
        </div>

        <div class="col-lg-4">
            <?= $this->Form->input('ie', [
                'div'           => false,
                'label'         => 'Inscrição Estadual',
                'placeholder'   => 'Inscrição Estadual',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('phone', [
                'div'           => false,
                'label'         => 'Telefone',
                'placeholder'   => 'Telefone',
                'class'         => 'validate[required] form-control phone-mask',
            ]) ?>
        </div>

        <div class="col-lg-5">
            <?= $this->Form->input('email', [
                'div'           => false,
                'label'         => 'Email',
                'placeholder'   => 'Email',
                'class'         => 'validate[required,custom[email]] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-2">
            <?= $this->Form->input('cep', [
                'div'           => false,
                'label'         => 'CEP',
                'placeholder'   => 'CEP',
                'class'         => 'validate[required] form-control cep',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6">
            <?= $this->Form->input('address', [
                'div'           => false,
                'label'         => 'Endereço',
                'placeholder'   => 'Endereço',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>

        <div class="col-lg-2">
            <?= $this->Form->input('number', [
                'div'           => false,
                'label'         => 'Número',
                'placeholder'   => 'Número',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('complement', [
                'div'           => false,
                'label'         => 'Complemento',
                'placeholder'   => 'Complemento',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
        <div class="col-lg-5">
            <?= $this->Form->input('area', [
                'div'           => false,
                'label'         => 'Bairro',
                'placeholder'   => 'Bairro',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <?= $this->Form->input('uf_id', [
                'id'            => 'states',
                'div'           => false,
                'label'         => 'Estado',
                'options'       => $states,
                'empty'         => 'Selecione um estado',
                'placeholder'   => 'Estado',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>

        <div class="col-lg-5">
            <?= $this->Form->input('city_id', [
                'id'            => 'city',
                'div'           => false,
                'label'         => 'Cidade',
                'empty'         => 'Selecione uma cidade',
                'placeholder'   => 'Cidade',
                'class'         => 'validate[required] form-control',
            ]) ?>
        </div>
    </div>

    <!--
    <div class="form-group row">
        <div class="col-lg-5">
            <?= $this->Form->input('imagem', [
                'id'            => 'image_upload',
                'type'          => 'file',
                'div'           => false,
                'label'         => 'Logotipo',
                'placeholder'   => 'Logotipo',
                'class'         => 'validate[required, custom[validateMIME[image/jpeg|image/png]]] form-control',
            ]) ?>
        </div>

        <div class="col-lg-3">
            <?= $this->Html->image('default.png', [
                'id'    => 'image_preview',
                'alt'   => 'Image Upload',
            ])?>
        </div>
    </div>
    -->

    <div class="form-group row">

        <div class="col-lg-2">
            <?= $this->Form->button('Enviar Cadastro', [
                'id'            => 'submit-cadastrar-parceiro',
                'type'          => 'button',
                'div'           => false,
                'class'         => 'btn btn-default float-right',
            ]) ?>

        </div>

        <div class="col-lg-5" id="form-toggle-message"></div>
    </div>

    <?= $this->Form->end() ?>
</div>


<div class="container info-central-box">
    <p class="font-12">
            <span>
                Isso mesmo... É a sua marca direto no público alvo, totalmente direcionado com um sistema inteligente
                que indica os melhores produtos de acordo com a necessidade do atleta. Os atletas podem fazer composição
                com outras marcas e ir tudo em uma embalagem só, diminuindo muito o frete. Cliente paga menos, a sua marca vende mais!
            </span>
    </p>
</div>

<!-- INFORMAÇÕES  -->
<div class="container">

    <?= $this->Element('bloco_academias')?>

    <?= $this->Element('bloco_atletas')?>

    <?= $this->Element('bloco_central_relacionamento')?>

</div>