<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Pedido Entity.
 *
 * @property int $id
 * @property int $cliente_id
 * @property \App\Model\Entity\Cliente $cliente
 * @property \Cake\I18n\Time $entrega
 * @property string $peso
 * @property string $volume
 * @property float $frete
 * @property int $academia_id
 * @property \App\Model\Entity\Academia $academia
 * @property int $transportadora_id
 * @property \App\Model\Entity\Transportadora $transportadora
 * @property int $pedido_status_id
 * @property \App\Model\Entity\PedidoStatus $pedido_status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $finalized
 * @property \App\Model\Entity\PedidoIten[] $pedido_itens
 * @property \App\Model\Entity\PedidoPagamento[] $pedido_pagamentos
 */
class Pedido extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
