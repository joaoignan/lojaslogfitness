<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProfessorComisso Entity.
 *
 * @property int $id
 * @property int $ano
 * @property int $mes
 * @property int $professor_id
 * @property \App\Model\Entity\Professore $professore
 * @property float $meta
 * @property float $vendas
 * @property float $comissao
 * @property int $aceita
 * @property int $paga
 * @property string $obs
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Pedido[] $pedidos
 */
class ProfessorComisso extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
