<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Log Entity.
 */
class Log extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'controller' => true,
        'action' => true,
        'registro' => true,
        'detalhes' => true,
        'user_id' => true,
        'user' => true,
    ];
}
