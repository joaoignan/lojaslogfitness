<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClientesSugesto Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $telephone
 * @property int $academia_id
 * @property int $professor_id
 * @property int $city_id
 * @property int $aceitou
 * @property \Cake\I18n\Time $created
 */
class ClienteSugesto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
