<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Produto Entity.
 *
 * @property int $id
 * @property int $produto_base_id
 * @property \App\Model\Entity\ProdutoBase $produto_base
 * @property float $preco
 * @property float $preco_promo
 * @property \App\Model\Entity\Propriedade $propriedade
 * @property string $fotos
 * @property string $__avaliacao
 * @property int $estoque
 * @property int $estoque_min
 * @property int $visivel
 * @property int $status_id
 * @property \App\Model\Entity\Status $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\PedidoIten[] $pedido_itens
 * @property \App\Model\Entity\ProdutoComentario[] $produto_comentarios
 */
class Produto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];
}
