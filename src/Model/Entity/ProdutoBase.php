<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProdutoBase Entity.
 *
 * @property int $id
 * @property string $name
 * @property int $marca_id
 * @property \App\Model\Entity\Marca $marca
 * @property string $atributos
 * @property string $dicas
 * @property string $embalagem_conteudo
 * @property int $doses
 * @property int $peso
 * @property int $largura
 * @property int $altura
 * @property int $profundidade
 * @property int $propriedade_id
 * @property \App\Model\Entity\Propriedade $propriedade
 * @property string $objetivos_ids
 * @property string $categorias_ids
 * @property int $status_id
 * @property \App\Model\Entity\Status $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\ProdutoObjetivo[] $produto_objetivos
 * @property \App\Model\Entity\Produto[] $produtos
 */
class ProdutoBase extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];
}
