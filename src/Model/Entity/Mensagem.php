<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Mensagem Entity.
 *
 * @property int $id
 * @property string $remetente
 * @property string $destinatario
 * @property string $msg
 * @property date $agendado
 * @property datetime $created
 * @property datetime $modified
 */
class Mensagem extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
