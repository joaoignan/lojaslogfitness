<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Fornecedore Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $cnpj
 * @property string $ie
 * @property string $phone
 * @property string $email
 * @property string $password
 * @property int $marca_id
 * @property \App\Model\Entity\Marca $marca
 * @property string $cep
 * @property string $address
 * @property int $number
 * @property string $complement
 * @property string $area
 * @property int $city_id
 * @property \App\Model\Entity\City $city
 * @property string $image
 * @property int $status_id
 * @property \App\Model\Entity\Status $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Fornecedore extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
