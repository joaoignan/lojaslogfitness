<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * Academia Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $shortname
 * @property string $slug
 * @property string $cnpj
 * @property string $ie
 * @property string $contact
 * @property string $phone
 * @property string $mobile
 * @property string $email
 * @property string $password
 * @property string $address
 * @property string $number
 * @property string $complement
 * @property string $area
 * @property int $city_id
 * @property \App\Model\Entity\City $city
 * @property string $cep
 * @property string $latitude
 * @property string $longitude
 * @property string $map
 * @property string $image
 * @property int $alunos
 * @property int $user_id
 * @property int $status_id
 * @property \App\Model\Entity\Status $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Cliente[] $clientes
 */
class AcademiaConta extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * Fields that are excluded from JSON an array versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'senha'
    ];

    protected function _setPassword($senha){
        if(!empty($senha)) {
            return (new DefaultPasswordHasher)->hash($senha);
        }else {
            //return false;
            return '';
        }
    }
}
