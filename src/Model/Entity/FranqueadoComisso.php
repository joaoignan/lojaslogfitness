<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FranqueadoComisso Entity.
 *
 * @property int $id
 * @property int $academia_id
 * @property \App\Model\Entity\Academia $academia
 * @property float $meta
 * @property float $vendas
 * @property float $comissao
 * @property int $aceita
 * @property int $paga
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class FranqueadoComisso extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
