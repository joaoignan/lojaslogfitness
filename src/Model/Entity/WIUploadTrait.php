<?php
namespace App\Model\Entity;

/**
 * Class WIUploadTrait
 * @package App\Model\Entity
 */
trait WIUploadTrait {

    /**
     * UPLOAD
     * @param $file
     * @param $name
     * @param null $subdir
     * @param null $mask
     * @param null $width
     * @param null $height
     * @return bool|string
     */
    public function wiUpload($file, $name, $subdir = null, $mask = null, $width = null, $height = null){
        if(isset($file) AND isset($name)){
            if($subdir == null){
                $dir = WWW_ROOT.'img'.DS;
            }else{
                $dir = WWW_ROOT.'img'.DS.$subdir;
            }

            //Nome do arquivo
            $filename   = $file['name'];
            $parts      = explode('.', $filename);
            $extension  = end($parts);
            if($mask == 'mask'){
                $filename   = strtolower(md5($name).'.'.$extension);
            }else{
                $filename   = strtolower($name.'.'.$extension);
            }
            include_once (WIDE_IMAGE);
            $img = \WideImage::load($file['tmp_name']);
            if($width != null && $height != null) {
                $img = $img->resize($width, $height, 'inside', 'any');
            }else{
                $img = $img->resize(500, 500, 'inside', 'any');
            }
            //$img = $img->crop('50%-100','50%-50',200,100);

            if(strpos($subdir, 'channels') !== false){
                //$white = $img->allocateColor(255, 255, 255);
                $img->resizeCanvas($width, $height, 'center', 'center')
                    ->saveToFile($dir.DS.$name.'.png', 6);
                $bd_filename = $name.'.png';
            }else{
                if(strtolower($extension) == 'jpg' || strtolower($extension) == 'jpeg'){
                    $img->saveToFile($dir.DS.$filename, 100);
                }elseif(strtolower($extension) == 'png'){
                    $img->saveToFile($dir.DS.$filename, 2);
                }
                $bd_filename = IMG_URL.$subdir.DS.$filename;
            }

            $img->destroy();
            return $bd_filename;
        }else{
            return false;
        }
    }


    /**
     * UPLOAD
     * @param $file
     * @param $name
     * @param null $subdir
     * @param null $mask
     * @param null $width
     * @param null $height
     * @return bool|string
     */
    public function wiUpload2($file, $name, $subdir = null, $mask = null, $width = null, $height = null){
        $servidor = 'ftp.logfitness.com.br';
        $caminho_absoluto = '/api.logfitness.com.br/webroot/img/produtos/';
        $con_id = ftp_connect($servidor) or die( 'Não conectou em: '.$servidor );
        ftp_login( $con_id, 'apilogfitness', 'zBv7u$34' );
        if(isset($file) AND isset($name)){
            if($subdir == null){
                $dir = WWW_ROOT.'img'.DS;
            }else{
                $dir = WWW_ROOT.'img'.DS.$subdir;
            }
            //Nome do arquivo
            $filename   = $file['name'];
            $parts      = explode('.', $filename);
            $extension  = end($parts);
            if($mask == 'mask'){
                $filename   = strtolower(md5($name).'.'.$extension);
            }else{
                $filename   = strtolower($name.'.'.$extension);
            }
            include_once (WIDE_IMAGE);
            $img = \WideImage::load($file['tmp_name']);
            if($width != null && $height != null) {
                $img = $img->resize($width, $height, 'inside', 'any');
            }else{
                $img = $img->resize(500, 500, 'inside', 'any');
            }
            //$img = $img->crop('50%-100','50%-50',200,100);

            if(strpos($subdir, 'channels') !== false){
                //$white = $img->allocateColor(255, 255, 255);
                $img->resizeCanvas($width, $height, 'center', 'center')
                    ->saveToFile($dir.DS.$name.'.png', 6);
                $bd_filename = $name.'.png';
            }else{
                if(strtolower($extension) == 'jpg' || strtolower($extension) == 'jpeg'){
                    ftp_login( $con_id, 'apilogfitness', 'zBv7u$34' );    
                    ftp_put($con_id, $caminho_absoluto.$filename, $file['tmp_name'], FTP_BINARY);
                }elseif(strtolower($extension) == 'png'){
                    ftp_login( $con_id, 'apilogfitness', 'zBv7u$34' );    
                    ftp_put($con_id, $caminho_absoluto.$filename, $file['tmp_name'], FTP_BINARY);
                }
                $bd_filename = IMG_URL_API.$subdir.DS.$filename;
            }

            $img->destroy();
            return $bd_filename;
        }else{
            return false;
        }
    }

    /**
     * UPLOAD BANNER
     * @param $file
     * @param $name
     * @param null $subdir
     * @param null $mask
     * @param null $width
     * @param null $height
     * @return bool|string
     */
    public function wiUploadBanner($file, $name, $subdir = null, $mask = null, $width = null, $height = null){
        if(isset($file) AND isset($name)){
            if($subdir == null){
                $dir = WWW_ROOT.'img'.DS;
            }else{
                $dir = WWW_ROOT.'img'.DS.$subdir;
            }

            //Nome do arquivo
            $filename   = $file['name'];
            $parts      = explode('.', $filename);
            $extension  = end($parts);
            if($mask == 'mask'){
                $filename   = strtolower(md5($name).'.'.$extension);
            }else{
                $filename   = strtolower($name.'.'.$extension);
            }

            include_once (WIDE_IMAGE);
            $img = \WideImage::load($file['tmp_name']);
            if($width != null && $height != null) {
                $img = $img->resize($width, $height, 'outside');
                $img = $img->crop('center', 'center', $width, $height);
            }else{
                $img = $img->resize(500, 500, 'outside');
                $img = $img->crop('center', 'center', 500, 500);
            }

            if(strtolower($extension) == 'jpg' || strtolower($extension) == 'jpeg'){
                $img->saveToFile($dir.DS.$filename, 100);
            }elseif(strtolower($extension) == 'png'){
                $img->saveToFile($dir.DS.$filename, 2);
            }
            $bd_filename = $filename;

            $img->destroy();
            return $bd_filename;
        }else{
            return false;
        }
    }

    /**
     * SANITIZE STRING
     * @param $str
     * @return mixed
     */
    public function sanitizeString($str) {
        $str = preg_replace('/[áàãâä]/ui', 'a', $str);
        $str = preg_replace('/[éèêë]/ui', 'e', $str);
        $str = preg_replace('/[íìîï]/ui', 'i', $str);
        $str = preg_replace('/[óòõôö]/ui', 'o', $str);
        $str = preg_replace('/[úùûü]/ui', 'u', $str);
        $str = preg_replace('/[ç]/ui', 'c', $str);
        // $str = preg_replace('/[,(),;:|!"#$%&/=?~^><ªº-]/', '_', $str);
        $str = preg_replace('/[^a-z0-9]/i', '_', $str);
        $str = preg_replace('/_+/', '-', $str); // ideia do Bacco :)
        return $str;
    }
}