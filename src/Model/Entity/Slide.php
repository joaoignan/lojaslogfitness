<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Slide Entity.
 */
class Slide extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     * Note that '*' is set to true, which allows all unspecified fields to be
     * mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'image' => true,
        'background' => true,
        'link' => true,
        'ordem' => true,
        'status_id' => true,
        'status' => true,
    ];
}
