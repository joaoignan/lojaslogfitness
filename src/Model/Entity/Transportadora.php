<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Transportadora Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $complement
 * @property string $area
 * @property int $number
 * @property int $city_id
 * @property \App\Model\Entity\City $city
 * @property string $cep
 * @property int $status_id
 * @property \App\Model\Entity\Status $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Pedido[] $pedidos
 */
class Transportadora extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
