<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FranqueadoComissoesPedido Entity.
 *
 * @property int $id
 * @property int $franqueado_comissao_id
 * @property \App\Model\Entity\FranqueadoComisso $franqueado_comisso
 * @property int $pedido_id
 * @property \App\Model\Entity\Pedido $pedido
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class FranqueadoComissoesPedido extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
