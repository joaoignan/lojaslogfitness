<?php
namespace App\Model\Table;

use App\Model\Entity\ProfessorComisso;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProfessorComissoes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Professores
 * @property \Cake\ORM\Association\BelongsToMany $Pedidos
 */
class ProfessorComissoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('professor_comissoes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Professores', [
            'foreignKey' => 'professor_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Pedidos', [
            'foreignKey' => 'professor_comisso_id',
            'targetForeignKey' => 'pedido_id',
            'joinTable' => 'professor_comissoes_pedidos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('ano')
            ->requirePresence('ano', 'create')
            ->notEmpty('ano');

        $validator
            ->integer('mes')
            ->requirePresence('mes', 'create')
            ->notEmpty('mes');

        $validator
            ->decimal('meta')
            ->requirePresence('meta', 'create')
            ->notEmpty('meta');

        $validator
            ->decimal('vendas')
            ->requirePresence('vendas', 'create')
            ->notEmpty('vendas');

        $validator
            ->decimal('comissao')
            ->requirePresence('comissao', 'create')
            ->notEmpty('comissao');

        $validator
            ->integer('aceita')
            ->requirePresence('aceita', 'create')
            ->notEmpty('aceita');

        $validator
            ->integer('paga')
            ->requirePresence('paga', 'create')
            ->notEmpty('paga');

        $validator
            ->allowEmpty('obs');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['professor_id'], 'Professores'));
        return $rules;
    }
}
