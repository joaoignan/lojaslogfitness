<?php
namespace App\Model\Table;

use App\Model\Entity\AcademiasSugesto;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AcademiasSugestoes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cities
 */
class ClienteSugestoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cliente_sugestoes');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Academias', [
            'foreignKey' => 'academia_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Professores', [
            'foreignKey' => 'professor_id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->requirePresence('telephone', 'create')
            ->allowEmpty('telephone');

        $validator
            ->allowEmpty('academia_id');

        $validator
            ->allowEmpty('professor_id');

        $validator
            ->notEmpty('city_id');

        $validator
            ->notEmpty('aceitou');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['academia_id'], 'Academias'));
        $rules->add($rules->existsIn(['professor_id'], 'Professores'));
        $rules->add($rules->existsIn(['city_id'], 'Cities'));
        return $rules;
    }
}
