<?php
namespace App\Model\Table;

use App\Model\Entity\AcademiaMensalidade;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AcademiaMensalidades Model
 *
 */
class AcademiaMensalidadesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('academia_mensalidades');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Academias', [
            'foreignKey' => 'academia_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('MensalidadesCategorias', [
            'foreignKey' => 'mensalidades_categorias_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('AcademiaMensalidadesPlanos', [
            'foreignKey' => 'academia_mensalidades_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['mensalidades_categorias_id'], 'MensalidadesCategorias'));
        return $rules;
    }
}
