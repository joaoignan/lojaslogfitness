<?php
namespace App\Model\Table;

use App\Model\Entity\Pedido;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pedidos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Clientes
 * @property \Cake\ORM\Association\BelongsTo $Academias
 * @property \Cake\ORM\Association\BelongsTo $Transportadoras
 * @property \Cake\ORM\Association\BelongsTo $PedidoStatus
 * @property \Cake\ORM\Association\HasMany $PedidoItens
 * @property \Cake\ORM\Association\HasMany $PedidoPagamentos
 */
class PedidosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pedidos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Academias', [
            'foreignKey' => 'academia_id'
        ]);
        $this->belongsTo('Professores', [
            'foreignKey' => 'professor_id'
        ]);
        $this->belongsTo('Transportadoras', [
            'foreignKey' => 'transportadora_id'
        ]);
        $this->belongsTo('PedidoStatus', [
            'foreignKey' => 'pedido_status_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PlanosClientes', [
            'foreignKey' => 'plano_cliente_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CupomDesconto', [
            'foreignKey' => 'cupom_desconto_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('PedidoItens', [
            'foreignKey' => 'pedido_id'
        ]);
        $this->hasMany('PedidoPagamentos', [
            'foreignKey' => 'pedido_id'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('data_pagamento')
            ->allowEmpty('data_pagamento');

        $validator
            ->dateTime('data_transporte')
            ->allowEmpty('data_transporte');

        $validator
            ->dateTime('entrega')
            ->allowEmpty('entrega');

        $validator
            ->allowEmpty('peso');

        $validator
            ->allowEmpty('volume');

        $validator
            ->decimal('frete')
            ->allowEmpty('frete');

        $validator
            ->dateTime('finalized')
            ->allowEmpty('finalized');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        $rules->add($rules->existsIn(['academia_id'], 'Academias'));
        $rules->add($rules->existsIn(['transportadora_id'], 'Transportadoras'));
        $rules->add($rules->existsIn(['pedido_status_id'], 'PedidoStatus'));
        $rules->add($rules->existsIn(['cupom_desconto_id'], 'CupomDesconto'));
        return $rules;
    }
}
