<?php
namespace App\Model\Table;

use App\Model\Entity\WIUploadTrait;
use App\Model\Entity\Banner;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Banners Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Status
 * @property \Cake\ORM\Association\HasMany $AssinePages
 */
class AcademiaBannersTable extends Table
{

    use WIUploadTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('academia_banners');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->requirePresence('image', 'create')
            ->notEmpty('image');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['status_id'], 'Status'));
        return $rules;
    }

    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options){

        if(!empty($data['imagem'])) {
            $image = $data['imagem'];
            if (!empty($image)) {
                if (!empty($image['name']) && $image['size'] > 0) {
                    $name = str_replace(' ', '-', strtolower(trim($data['title']))).'-'.date('Y-m-d-H-i-s');
                    $name = $this->sanitizeString($name);

                    $extension  = explode('.', $image['name']);
                    $extension  = strtolower(end($extension));

                    if($extension == 'png') {
                        $img_r = @imagecreatefrompng($image['tmp_name']);
                        if (!$img_r) {
                            $img_r  = imagecreate(150, 30);
                            $bgc = imagecolorallocate($img_r, 255, 255, 255);
                            $tc  = imagecolorallocate($img_r, 0, 0, 0);
                            imagefilledrectangle($img_r, 0, 0, 150, 30, $bgc);

                            imagestring($img_r, 1, 5, 5, "Error loading $img_r", $tc);
                        }
                    } else {
                        $img_r = @imagecreatefromjpeg($image['tmp_name']);
                        if (!$img_r) {
                            $img_r  = imagecreate(150, 30);
                            $bgc = imagecolorallocate($img_r, 255, 255, 255);
                            $tc  = imagecolorallocate($img_r, 0, 0, 0);
                            imagefilledrectangle($img_r, 0, 0, 150, 30, $bgc);

                            imagestring($img_r, 1, 5, 5, "Error loading $img_r", $tc);
                        }
                    }

                    $dst_r = ImageCreateTrueColor( $data['cropwidth'], $data['cropheight'] );

                    $background = imagecolorallocate($dst_r , 0, 0, 0);
                    imagecolortransparent($dst_r, $background);
                    imagealphablending($dst_r, false);
                    imagesavealpha($dst_r, true);

                    imagecopyresampled($dst_r,$img_r,0,0,$data['cropx'],$data['cropy'],
                    $data['cropwidth'],$data['cropheight'],$data['cropw'],$data['croph']);

                    $dir = WWW_ROOT.'img'.DS.'banners';

                    if($extension == 'png') {
                        imagepng($dst_r, $dir.DS.$name.'.'.$extension, 9);
                    } else {
                        imagejpeg($dst_r, $dir.DS.$name.'.'.$extension, 100);
                    }

                    $data['image'] = $name.'.'.$extension;

                    //$arquivo = $this->wiUpload($dst_r, $name, 'academias', null, 160, 95);
                    //$data['image'] = $arquivo;
                }
            }
        }
    }
}
