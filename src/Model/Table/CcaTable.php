<?php
namespace App\Model\Table;

use App\Model\Entity\Cca;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Network\Session;

/**
 * Cca Model
 *
 * @property \Cake\ORM\Association\BelongsTo $CcaActions
 * @property \Cake\ORM\Association\BelongsTo $Groups
 */
class CcaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cca');
        $this->displayField('cca_action_id');
        $this->primaryKey(['cca_action_id', 'group_id']);

        $this->belongsTo('CcaActions', [
            'foreignKey' => 'cca_action_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cca_action_id'], 'CcaActions'));
        $rules->add($rules->existsIn(['group_id'], 'Groups'));
        return $rules;
    }

    /**
     * Returns true if user group is and controller/action in cca table
     * @param null $prefix
     * @param $controller
     * @param $action
     * @param $group
     * @return bool
     */
    public function cca($prefix = null, $controller, $action, $Group_id, $session_cache = false){
        if(
            ($controller == 'Pages' && ($action == 'index' || $action == 'display' || $action == 'home'))  ||
            ($controller == 'Users' && ($action == 'login' || $action == 'logout'))
        ){
            return true;
        }else{
            if($Group_id == 1){
                return true;
            }else {
                //$session = Session::create();
                $session = new Session;
                $cca_actions = TableRegistry::get('CcaActions');

                if (!$session->read('Admin.CcaActions') || $session_cache == false) {
                    //SALVAR DADOS NA SESSAO
                    $cca_all_actions = $cca_actions
                        ->find('all')
                        ->hydrate(false)
                        ->all()
                        ->toArray();
                    $session->write('Admin.CcaActions', $cca_all_actions);
                }

                //PROCURAR PELA AÇÃO
                foreach ($session->read('Admin.CcaActions') as $cca_action) {
                    if (
                        $cca_action['controller'] == $controller &&
                        $cca_action['action'] == $action &&
                        $cca_action['prefix'] == $prefix
                    ) {
                        $cca_action_verify = $cca_action['id'];
                        break;
                    }
                }

                //VERIFICAR SE USUARIO TEM PERMISSAO PARA EXECUTAR A ACAO
                if (isset($cca_action_verify)) {
                    if(!$session->read('Admin.Cca') || $session_cache == false){
                        $cca = TableRegistry::get('Cca');
                        $cca_all = $cca
                            ->find('all')
                            ->hydrate(false)
                            ->all();
                        $session->write('Admin.Cca', $cca_all);
                    }

                    foreach ($session->read('Admin.Cca') as $cca_verify) {
                        if ($cca_verify['group_id'] == $Group_id &&
                            $cca_verify['cca_action_id'] == $cca_action_verify
                        ) {
                            $permission = true;
                            break;
                        }
                    }
                }

                //SE USUARIO TEM PREVILEGIO RETORNA VERDADEIRO
                if(isset($permission) && $permission == true){
                    return true;
                }

            }

            /*$cca_action = $cca_actions->findByPrefixAndControllerAndAction($prefix, $controller, $action)->first();
            if($cca_action){
                if($Group_id != 1){
                    return true;
                }else{
                    $cca = TableRegistry::get('Cca');
                    $cca_all = $cca->find('all')->hydrate(false)->all();
                    $session->write('Admin.Cca', $cca_all);
                    $cca = $cca->findByGroupIdAndCcaActionId($Group_id, $cca_action['id'])->first();
                    if($cca){
                        return true;
                    }
                }
            }*/
        }
        return false;
    }
}
