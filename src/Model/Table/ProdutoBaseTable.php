<?php
namespace App\Model\Table;

use App\Model\Entity\ProdutoBase;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProdutoBase Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Marcas
 * @property \Cake\ORM\Association\BelongsTo $Propriedades
 * @property \Cake\ORM\Association\BelongsTo $Status
 * @property \Cake\ORM\Association\HasMany $ProdutoObjetivos
 * @property \Cake\ORM\Association\HasMany $Produtos
 */
class ProdutoBaseTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('produto_base');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Marcas', [
            'foreignKey' => 'marca_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Fornecedores', [
            'foreignKey' => 'fornecedor_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Propriedades', [
            'foreignKey' => 'propriedade_id'
        ]);
        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ProdutoObjetivos', [
            'foreignKey' => 'produto_base_id'
        ]);
        $this->hasMany('ProdutoCategorias', [
            'foreignKey' => 'produto_base_id'
        ]);
        $this->hasMany('ProdutoSubcategorias', [
            'foreignKey' => 'produto_base_id'
        ]);
        $this->hasMany('ProdutoCombinations', [
            'foreignKey' => 'produto_base1_id'
        ]);
        $this->hasMany('Produtos', [
            'foreignKey' => 'produto_base_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('atributos');

        $validator
            ->allowEmpty('dicas');

        $validator
            ->requirePresence('embalagem_conteudo', 'create')
            ->notEmpty('embalagem_conteudo');

        $validator
            ->integer('doses')
            ->allowEmpty('doses');

        $validator
            ->integer('peso')
            ->allowEmpty('peso');

        $validator
            ->integer('largura')
            ->allowEmpty('largura');

        $validator
            ->integer('altura')
            ->allowEmpty('altura');

        $validator
            ->integer('profundidade')
            ->allowEmpty('profundidade');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['marca_id'], 'Marcas'));
        $rules->add($rules->existsIn(['fornecedor_id'], 'Fornecedores'));
        $rules->add($rules->existsIn(['propriedade_id'], 'Propriedades'));
        $rules->add($rules->existsIn(['status_id'], 'Status'));
        return $rules;
    }
}
