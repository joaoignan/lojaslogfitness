<?php
namespace App\Model\Table;

use App\Model\Entity\WIUploadTrait;
use App\Model\Entity\Marca;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Marcas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Status
 * @property \Cake\ORM\Association\HasMany $ProdutoBase
 */
class MarcasTable extends Table
{

    use WIUploadTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('marcas');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ProdutoBase', [
            'foreignKey' => 'marca_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('id', 'create')
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('slug', 'create')
            ->notEmpty('slug');

        $validator
            ->allowEmpty('image');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['status_id'], 'Status'));
        return $rules;
    }

    public function newUploadImage($image, $name){

        if($image['error'] == 0) {
            $name = $this->sanitizeString($name);
            //Cria imagem para o produto, e devolve o caminho
            $arquivo = $this->wiUpload($image, $name, 'marcas', null, 206, 107);
        }

        return $arquivo;
    }

    public function newUploadImageBannerFundo($image, $name, $cropx, $cropy, $cropw, $croph){

        if($image['error'] == 0) {
            $name = $this->sanitizeString($name);
            
            $extension  = explode('.', $image['name']);
            $extension  = strtolower(end($extension));

            if($extension == 'png') {
                $img_r = @imagecreatefrompng($image['tmp_name']);
                if (!$img_r) {
                    $img_r  = imagecreate(150, 30);
                    $bgc = imagecolorallocate($img_r, 255, 255, 255);
                    $tc  = imagecolorallocate($img_r, 0, 0, 0);
                    imagefilledrectangle($img_r, 0, 0, 150, 30, $bgc);

                    imagestring($img_r, 1, 5, 5, "Error loading $img_r", $tc);
                }
            } else {
                $img_r = @imagecreatefromjpeg($image['tmp_name']);
                if (!$img_r) {
                    $img_r  = imagecreate(150, 30);
                    $bgc = imagecolorallocate($img_r, 255, 255, 255);
                    $tc  = imagecolorallocate($img_r, 0, 0, 0);
                    imagefilledrectangle($img_r, 0, 0, 150, 30, $bgc);

                    imagestring($img_r, 1, 5, 5, "Error loading $img_r", $tc);
                }
            }

            $dst_r = ImageCreateTrueColor( 1920, 850 );

            $background = imagecolorallocate($dst_r , 255, 0, 0);
            imagefill($dst_r, 0, 0, $background);

            imagecopyresampled($dst_r,$img_r,0,0,$cropx,$cropy,
            1920,850,$cropw,$croph);

            $dir = WWW_ROOT.'img'.DS.'banners-loja'.DS.'marcas';

            if($extension == 'png') {
                imagepng($dst_r, $dir.DS.$name.'.'.$extension, 6);
            } else {
                imagejpeg($dst_r, $dir.DS.$name.'.'.$extension, 100);
            }

            $arquivo = IMG_URL.'banners-loja'.DS.'marcas'.DS.$name.'.'.$extension;
        }

        return $arquivo;
    }

    public function newUploadImageBannerFundoCurto($image, $name, $cropx, $cropy, $cropw, $croph){

        if($image['error'] == 0) {
            $name = $this->sanitizeString($name);
            
            $extension  = explode('.', $image['name']);
            $extension  = strtolower(end($extension));

            if($extension == 'png') {
                $img_r = @imagecreatefrompng($image['tmp_name']);
                if (!$img_r) {
                    $img_r  = imagecreate(150, 30);
                    $bgc = imagecolorallocate($img_r, 255, 255, 255);
                    $tc  = imagecolorallocate($img_r, 0, 0, 0);
                    imagefilledrectangle($img_r, 0, 0, 150, 30, $bgc);

                    imagestring($img_r, 1, 5, 5, "Error loading $img_r", $tc);
                }
            } else {
                $img_r = @imagecreatefromjpeg($image['tmp_name']);
                if (!$img_r) {
                    $img_r  = imagecreate(150, 30);
                    $bgc = imagecolorallocate($img_r, 255, 255, 255);
                    $tc  = imagecolorallocate($img_r, 0, 0, 0);
                    imagefilledrectangle($img_r, 0, 0, 150, 30, $bgc);

                    imagestring($img_r, 1, 5, 5, "Error loading $img_r", $tc);
                }
            }

            $dst_r = ImageCreateTrueColor( 1920, 350 );

            $background = imagecolorallocate($dst_r , 255, 0, 0);
            imagefill($dst_r, 0, 0, $background);

            imagecopyresampled($dst_r,$img_r,0,0,$cropx,$cropy,
            1920,350,$cropw,$croph);

            $dir = WWW_ROOT.'img'.DS.'banners-loja'.DS.'marcas';

            if($extension == 'png') {
                imagepng($dst_r, $dir.DS.$name.'.'.$extension, 6);
            } else {
                imagejpeg($dst_r, $dir.DS.$name.'.'.$extension, 100);
            }

            $arquivo = IMG_URL.'banners-loja'.DS.'marcas'.DS.$name.'.'.$extension;
        }

        return $arquivo;
    }

    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options){
        if(!empty($data['banner_fundo'])) {
            $valorTamanhoBanner = $data['valor-tipo-banner'];
            // exit($valorTamanhoBanner);
            $image = $data['banner_fundo'];
            if (!empty($image)) {
                if (!empty($image['name']) && $image['size'] > 0) {
                    if(isset($data['name'])){
                        $img_name = str_replace(' ', '-', strtolower(trim($data['name']))).'-'.date('YmdHis');
                    }else{
                        $img_name = str_replace(' ', '-', strtolower(trim($data['img_name']))).'-'.date('YmdHis');
                    }
                    $name = $img_name;
                    $name = $this->sanitizeString($name);

                    $extension  = explode('.', $image['name']);
                    $extension  = strtolower(end($extension));

                    if($extension == 'png') {
                        $img_r = @imagecreatefrompng($image['tmp_name']);
                        if (!$img_r) {
                            $img_r  = imagecreate(150, 30);
                            $bgc = imagecolorallocate($img_r, 255, 255, 255);
                            $tc  = imagecolorallocate($img_r, 0, 0, 0);
                            imagefilledrectangle($img_r, 0, 0, 150, 30, $bgc);

                            imagestring($img_r, 1, 5, 5, "Error loading $img_r", $tc);
                        }
                    } else {
                        $img_r = @imagecreatefromjpeg($image['tmp_name']);
                        if (!$img_r) {
                            $img_r  = imagecreate(150, 30);
                            $bgc = imagecolorallocate($img_r, 255, 255, 255);
                            $tc  = imagecolorallocate($img_r, 0, 0, 0);
                            imagefilledrectangle($img_r, 0, 0, 150, 30, $bgc);

                            imagestring($img_r, 1, 5, 5, "Error loading $img_r", $tc);
                        }
                    }

                    if ($valorTamanhoBanner == 0) {

                        $dst_r = ImageCreateTrueColor( 1920, 850 );

                        $background = imagecolorallocate($dst_r , 255, 0, 0);
                        imagefill($dst_r, 0, 0, $background);

                        imagecopyresampled($dst_r,$img_r,0,0,$data['banner_fundo_cropx'],$data['banner_fundo_cropy'],
                        1920,850,$data['banner_fundo_cropw'],$data['banner_fundo_croph']);

                    } else if ($valorTamanhoBanner == 1) {

                        $dst_r = ImageCreateTrueColor( 1920, 350 );

                        $background = imagecolorallocate($dst_r , 255, 0, 0);
                        imagefill($dst_r, 0, 0, $background);

                        imagecopyresampled($dst_r,$img_r,0,0,$data['banner_fundo_cropx'],$data['banner_fundo_cropy'],
                        1920,350,$data['banner_fundo_cropw'],$data['banner_fundo_croph']);

                    }

                    $dir = WWW_ROOT.'img'.DS.'banners-loja'.DS.'marcas';

                    if($extension == 'png') {
                        imagepng($dst_r, $dir.DS.$name.'.'.$extension, 6);
                    } else {
                        imagejpeg($dst_r, $dir.DS.$name.'.'.$extension, 100);
                    }

                    $data['banner_fundo'] = IMG_URL.'banners-loja'.DS.'marcas'.DS.$name.'.'.$extension;
                }
            }
        }

        if(!empty($data['imagem'])) {
            $image = $data['imagem'];
            if (!empty($image)) {
                if (!empty($image['name'])) {
                    $name = str_replace(' ', '-', strtolower(trim($data['slug']))).date('dmyhis');
                    $name = $this->sanitizeString($name);
                    $arquivo = $this->wiUpload($image, $name, 'marcas', null, 206, 107);
                    $data['image'] = $arquivo;
                }
            }
        }
    }
}
