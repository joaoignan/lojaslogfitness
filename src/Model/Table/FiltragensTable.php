<?php
namespace App\Model\Table;

use App\Model\Entity\Filtragem;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Filtragens Model
 */
class FiltragensTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('filtragens');
        $this->displayField('palavra_chave');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('palavra_chave', 'create')
            ->notEmpty('palavra_chave');

        $validator
            ->requirePresence('qtd', 'create')
            ->notEmpty('qtd');

        return $validator;
    }
}
