<?php
namespace App\Model\Table;

use App\Model\Entity\AcademiaEsporte;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AcademiaEsportes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Academias
 * @property \Cake\ORM\Association\BelongsTo $Esportes
 */
class AcademiaEsportesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('academia_esportes');
        $this->displayField('esporte_id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Academias', [
            'foreignKey' => 'academia_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Esportes', [
            'foreignKey' => 'esporte_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['academia_id'], 'Academias'));
        $rules->add($rules->existsIn(['esporte_id'], 'Esportes'));
        return $rules;
    }
}
