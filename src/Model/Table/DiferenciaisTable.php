<?php
namespace App\Model\Table;

use App\Model\Entity\Diferencial;
use App\Model\Entity\WIUploadTrait;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Categorias Model
 *
 * @property \Cake\ORM\Association\HasMany $AcademiaDiferenciais
 */
class DiferenciaisTable extends Table
{
    use WIUploadTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('diferenciais');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('AcademiaDiferenciais', [
            'foreignKey' => 'diferencial_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('image');

        $validator
            ->allowEmpty('status');

        return $validator;
    }

    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options){
        if(!empty($data['imagem'])) {
            $image = $data['imagem'];
            if (!empty($image)) {
                if (!empty($image['name']) && $image['size'] > 0) {
                    if(isset($data['name'])){
                        $img_name = str_replace(' ', '-', strtolower(trim($data['name']))).'-'.date('YmdHis');
                    }else{
                        $img_name = str_replace(' ', '-', strtolower(trim($data['img_name']))).'-'.date('YmdHis');
                    }
                    $name = $img_name;
                    $name = $this->sanitizeString($name);
                    $arquivo = $this->wiUpload($image, $name, 'diferenciais', null, 160, 95);
                    $data['image'] = $arquivo;
                }
            }
        }
    }
}
