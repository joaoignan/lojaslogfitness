<?php
namespace App\Model\Table;

use App\Model\Entity\ProdutoCombination;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProdutoCombinations Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ProdutoBase
 */
class ProdutoCombinationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('produto_combinations');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ProdutoBase', [
            'foreignKey' => 'produto_base1_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ProdutoBase', [
            'foreignKey' => 'produto_base2_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['produto_base1_id'], 'ProdutoBase'));
        $rules->add($rules->existsIn(['produto_base2_id'], 'ProdutoBase'));
        return $rules;
    }
}
