<?php
namespace App\Model\Table;

use App\Model\Entity\Atleta;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Atletas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cities
 * @property \Cake\ORM\Association\BelongsTo $States
 */
class AtletasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('atletas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Cities', [
            'foreignKey' => 'cities_id'
        ]);
        $this->belongsTo('States', [
            'foreignKey' => 'state_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nome');

        $validator
            ->allowEmpty('telephone');

        $validator
            ->requirePresence('whatsapp', 'create')
            ->notEmpty('whatsapp');

        $validator
            ->allowEmpty('academia');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cities_id'], 'Cities'));
        $rules->add($rules->existsIn(['state_id'], 'States'));
        return $rules;
    }
}
