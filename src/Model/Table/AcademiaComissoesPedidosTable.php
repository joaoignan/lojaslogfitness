<?php
namespace App\Model\Table;

use App\Model\Entity\AcademiaComissoesPedido;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AcademiaComissoesPedidos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $AcademiaComissoes
 * @property \Cake\ORM\Association\BelongsTo $Pedidos
 */
class AcademiaComissoesPedidosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('academia_comissoes_pedidos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('AcademiaComissoes', [
            'foreignKey' => 'academia_comissao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Pedidos', [
            'foreignKey' => 'pedido_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['academia_comissao_id'], 'AcademiaComissoes'));
        $rules->add($rules->existsIn(['pedido_id'], 'Pedidos'));
        return $rules;
    }
}
