<?php
namespace App\Model\Table;

use App\Model\Entity\WIUploadTrait;
use App\Model\Entity\Produto;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Produtos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ProdutoBase
 * @property \Cake\ORM\Association\BelongsTo $Status
 * @property \Cake\ORM\Association\HasMany $PedidoItens
 * @property \Cake\ORM\Association\HasMany $ProdutoComentarios
 */
class ProdutosTable extends Table
{

    use WIUploadTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('produtos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ProdutoBase', [
            'foreignKey' => 'produto_base_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Sabores', [
            'foreignKey' => 'sabor_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasMany('PedidoItens', [
            'foreignKey' => 'produto_id'
        ]);
        $this->hasMany('ProdutoComentarios', [
            'foreignKey' => 'produto_id'
        ]);
        $this->hasMany('AcademiaMensalidadesPlanos', [
            'foreignKey' => 'produto_id'
        ]);
        $this->hasMany('ProdutoSubstancias', [
            'foreignKey' => 'produto_id'
        ]);
        $this->hasMany('ProdutoIngredientes', [
            'foreignKey' => 'produto_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('cod_interno');

        $validator
            ->decimal('preco')
            ->allowEmpty('preco');

        $validator
            ->decimal('preco_promo')
            ->allowEmpty('preco_promo');

        $validator
            ->allowEmpty('propriedade');

        $validator
            ->requirePresence('fotos', 'create')
            ->notEmpty('fotos');

        $validator
            ->integer('estoque')
            ->allowEmpty('estoque');

        $validator
            ->integer('estoque_min')
            ->allowEmpty('estoque_min');

        $validator
            ->integer('visivel')
            ->requirePresence('visivel', 'create')
            ->notEmpty('visivel');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['produto_base_id'], 'ProdutoBase'));
        $rules->add($rules->existsIn(['status_id'], 'Status'));
        return $rules;
    }

    /**
     * @param \Cake\Event\Event $event
     * @param \ArrayObject $data
     * @param \ArrayObject $options
     *
     */
    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options){

        if(isset($data['foto'])) {
            $fotos = [];
            foreach ($data['foto'] as $key => $foto) {
                if (!empty($foto)) {
                    $image = $foto;
                    if (!empty($image)) {
                        if (!empty($image['name'])) {
                            $name = str_replace(' ', '-', strtolower(trim($data['slug']))) . '-' . $key;
                            $name = $this->sanitizeString($name);
                            $arquivo = $this->wiUpload($image, $name, 'produtos', null, 800, 800);
                            $arquivo1 = $this->wiUpload($image, 'md-' . $name, 'produtos', null, 400, 400);
                            $arquivo2 = $this->wiUpload($image, 'sm-' . $name, 'produtos', null, 170, 170);
                            $arquivo3 = $this->wiUpload($image, 'xs-' . $name, 'produtos', null, 60, 60);
                            $fotos[] = $arquivo;
                        } else {
                            if (!empty($data['photo'][$key])) {
                                $fotos[] = $data['photo'][$key];
                            }
                        }
                    } else {
                        if (!empty($data['photo'][$key])) {
                            $fotos[] = $data['photo'][$key];
                        }
                    }
                } else {
                    if (!empty($data['photo'][$key])) {
                        $fotos[] = $data['photo'][$key];
                    }
                }
            }
            if(count($fotos) >= 1) {
                $data['fotos'] = serialize($fotos);
            } else {
                $data['fotos'] = serialize('default.jpg');
            }
        }
    }

    public function newUploadImage($images, $name){

        $images_list = [];
        foreach ($images as $key => $image) {
            if($image['error'] == 0) {
                $name = $this->sanitizeString($name);
                //Cria imagem para o produto, e devolve o caminho
                $arquivo = $this->wiUpload($image, $name.$key, 'produtos', null, 800, 800);

                //Cria as demais imagens
                $this->wiUpload($image, 'md-' . $name.$key, 'produtos', null, 400, 400);
                $this->wiUpload($image, 'sm-' . $name.$key, 'produtos', null, 170, 170);
                $this->wiUpload($image, 'xs-' . $name.$key, 'produtos', null, 60, 60);

                $images_list[] = $arquivo;
            }
        }

        return $images_list;
    }

    public function newUploadImage2($images, $name){

        $images_list = [];
        foreach ($images as $key => $image) {
            if($image['error'] == 0) {
                $name = $this->sanitizeString($name);
                //Cria imagem para o produto, e devolve o caminho
                $arquivo = $this->wiUpload2($image, $name.$key, 'produtos', null, 800, 800);

                //Cria as demais imagens
                $this->wiUpload2($image, 'md-' . $name.$key, 'produtos', null, 400, 400);
                $this->wiUpload2($image, 'sm-' . $name.$key, 'produtos', null, 170, 170);
                $this->wiUpload2($image, 'xs-' . $name.$key, 'produtos', null, 60, 60);

                $images_list[] = $arquivo;
            }
        }

        return $images_list;
    }

    public function uploadImage($image, $name){

        $name = $this->sanitizeString($name);

        //Cria imagem para o produto, e devolve o caminho
        $arquivo  = $this->wiUpload($image, $name, 'produtos', null, 800, 800);

        //Cria as demais imagens
        $this->wiUpload($image, 'md-' . $name, 'produtos', null, 400, 400);
        $this->wiUpload($image, 'sm-' . $name, 'produtos', null, 170, 170);
        $this->wiUpload($image, 'xs-' . $name, 'produtos', null, 60, 60);

        return $arquivo;
    }
}
