<?php
namespace App\Model\Table;

use App\Model\Entity\Academia;
use App\Model\Entity\WIUploadTrait;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Academias Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cities
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Status
 * @property \Cake\ORM\Association\HasMany $AcademiaBanners
 * @property \Cake\ORM\Association\HasMany $AcademiaComissoes
 * @property \Cake\ORM\Association\HasMany $AcademiaEsportes
 * @property \Cake\ORM\Association\HasMany $Clientes
 * @property \Cake\ORM\Association\HasMany $Pedidos
 */
class AcademiasTable extends Table
{

    use WIUploadTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config){

        parent::initialize($config);

        $this->table('academias');
        $this->displayField('shortname');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);


        $this->belongsTo('Professores', [
            'foreignKey' => 'professor_id',
//            'joinType' => 'INNER'
        ]);


        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('AcademiaBanners', [
            'foreignKey' => 'academia_id'
        ]);
        $this->hasMany('AcademiaComissoes', [
            'foreignKey' => 'academia_id'
        ]);
        $this->hasMany('AcademiaEsportes', [
            'foreignKey' => 'academia_id'
        ]);
        $this->hasMany('AcademiaDiferenciais', [
            'foreignKey' => 'academia_id'
        ]);
        $this->hasMany('Clientes', [
            'foreignKey' => 'academia_id'
        ]);
        $this->hasMany('Pedidos', [
            'foreignKey' => 'academia_id'
        ]);
        $this->hasMany('ProfessorAcademias', [
            'foreignKey' => 'academia_id'
        ]);
        $this->hasMany('Avaliacoes', [
            'foreignKey' => 'academia_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('shortname', 'create')
            ->notEmpty('shortname');

        $validator
            ->requirePresence('slug', 'create')
            ->notEmpty('slug')
            ->add('slug', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('cnpj', 'create')
            ->allowEmpty('cnpj')
            ->add('cnpj', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('cpf', 'create')
            ->allowEmpty('cpf')
            ->add('cpf', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('ie');

        $validator
            ->allowEmpty('contact');

        $validator
            ->requirePresence('phone', 'create')
            ->notEmpty('phone');

        $validator
            ->allowEmpty('mobile');

        $validator
            ->allowEmpty('contact_partner');

        $validator
            ->allowEmpty('phone_partner');

        $validator
            ->allowEmpty('mobile_partner');    

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            //->requirePresence('password', 'create')
            ->allowEmpty('password');

        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->allowEmpty('number');

        $validator
            ->allowEmpty('complement');

        $validator
            ->requirePresence('area', 'create')
            ->notEmpty('area');

        $validator
            ->requirePresence('cep', 'create')
            ->notEmpty('cep');

        $validator
            ->allowEmpty('latitude');

        $validator
            ->allowEmpty('longitude');

        $validator
            ->allowEmpty('map');

        $validator
            ->allowEmpty('image');

        $validator
            ->allowEmpty('password');

        $validator
            ->integer('alunos')
            ->allowEmpty('alunos');

        $validator
            ->allowEmpty('aceite_termos');

        $validator
            ->allowEmpty('sobre');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['cnpj']));
        $rules->add($rules->isUnique(['slug']));
        $rules->add($rules->existsIn(['city_id'], 'Cities'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['status_id'], 'Status'));
        return $rules;
    }


    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options){

        if(!empty($data['imagem'])) {
            $image = $data['imagem'];
            if (!empty($image)) {
                if (!empty($image['name']) && $image['size'] > 0) {
                    if(isset($data['name'])){
                        $img_name = str_replace(' ', '-', strtolower(trim($data['name']))).'-'.date('YmdHis');
                    }else{
                        $img_name = str_replace(' ', '-', strtolower(trim($data['img_name']))).'-'.date('YmdHis');
                    }
                    $name = $img_name;
                    $name = $this->sanitizeString($name);

                    $extension  = explode('.', $image['name']);
                    $extension  = strtolower(end($extension));

                    if($extension == 'png') {
                        $img_r = @imagecreatefrompng($image['tmp_name']);
                        if (!$img_r) {
                            $img_r  = imagecreate(150, 30);
                            $bgc = imagecolorallocate($img_r, 255, 255, 255);
                            $tc  = imagecolorallocate($img_r, 0, 0, 0);
                            imagefilledrectangle($img_r, 0, 0, 150, 30, $bgc);

                            imagestring($img_r, 1, 5, 5, "Error loading $img_r", $tc);
                        }
                    } else {
                        $img_r = @imagecreatefromjpeg($image['tmp_name']);
                        if (!$img_r) {
                            $img_r  = imagecreate(150, 30);
                            $bgc = imagecolorallocate($img_r, 255, 255, 255);
                            $tc  = imagecolorallocate($img_r, 0, 0, 0);
                            imagefilledrectangle($img_r, 0, 0, 150, 30, $bgc);

                            imagestring($img_r, 1, 5, 5, "Error loading $img_r", $tc);
                        }
                    }

                    $dst_r = ImageCreateTrueColor( $data['cropwidth'], $data['cropheight'] );

                    $background = imagecolorallocate($dst_r , 255, 0, 0);
                    // imagecolortransparent($dst_r, $background);
                    // imagealphablending($dst_r, false);
                    // imagesavealpha($dst_r, true);
                    imagefill($dst_r, 0, 0, $background);

                    imagecopyresampled($dst_r,$img_r,0,0,$data['cropx'],$data['cropy'],
                    $data['cropwidth'],$data['cropheight'],$data['cropw'],$data['croph']);

                    $dir = WWW_ROOT.'img'.DS.'academias';

                    if($extension == 'png') {
                        imagepng($dst_r, $dir.DS.$name.'.'.$extension, 6);
                    } else {
                        imagejpeg($dst_r, $dir.DS.$name.'.'.$extension, 100);
                    }

                    $data['image'] = $name.'.'.$extension;

                    //$arquivo = $this->wiUpload($dst_r, $name, 'academias', null, 160, 95);
                    //$data['image'] = $arquivo;
                }
            }
        }

        if(!empty($data['imagem_comprovante'])) {
            $image = $data['imagem_comprovante'];
            if (!empty($image)) {
                if (!empty($image['name']) && $image['size'] > 0) {
                    $name = str_replace(' ', '-', strtolower(trim($data['img_name']))).'-'.date('YmdHis');
                    $name = $this->sanitizeString($name);
                    $arquivo  = $this->wiUpload($image, $name, 'comprovantes', null, 250, 250);
                    $arquivo1 = $this->wiUpload($image, 'lg-'.$name, 'comprovantes', null, 800, 800);
                    $data['image_comprovante'] = $arquivo;
                }
            }
        }
    }
}
