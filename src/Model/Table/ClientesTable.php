<?php
namespace App\Model\Table;

use App\Model\Entity\Cliente;
use App\Model\Entity\WIUploadTrait;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Clientes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cities
 * @property \Cake\ORM\Association\BelongsTo $Academias
 * @property \Cake\ORM\Association\BelongsTo $Status
 * @property \Cake\ORM\Association\HasMany $Pedidos
 */
class ClientesTable extends Table
{

    use WIUploadTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('clientes');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Academias', [
            'foreignKey' => 'academia_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Professor', [
            'foreignKey' => 'professor_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasMany('Pedidos', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->hasMany('Avaliacoes', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->hasMany('PlanosClientes', [
            'foreignKey' => 'cliente_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('genre');

        $validator
            ->allowEmpty('cpf');

        $validator
            ->allowEmpty('rg');

        /*$validator
            ->date('birth')
            ->allowEmpty('birth');
        */

        $validator
            ->allowEmpty('address');

        $validator
            ->allowEmpty('complement');

        $validator
            ->allowEmpty('area');

        $validator
            ->allowEmpty('cep');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->allowEmpty('image');

        $validator
            ->allowEmpty('email_notificacao');

        $validator
            ->allowEmpty('password');

        $validator
            ->allowEmpty('telephone');

        $validator
            ->allowEmpty('mobile');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['city_id'], 'Cities'));
        $rules->add($rules->existsIn(['academia_id'], 'Academias'));
        $rules->add($rules->existsIn(['status_id'], 'Status'));
        return $rules;
    }

    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options){

        if(!empty($data['imagem'])) {
            $image = $data['imagem'];
            if (!empty($image)) {
                if (!empty($image['name']) && $image['size'] > 0) {
                    if(isset($data['name'])){
                        $img_name = str_replace(' ', '-', strtolower(trim($data['name']))).'-'.date('YmdHis');
                    }else{
                        $img_name = str_replace(' ', '-', strtolower(trim($data['img_name']))).'-'.date('YmdHis');
                    }
                    $name = $img_name;
                    $name = $this->sanitizeString($name);
                    $arquivo = $this->wiUpload($image, $name, 'clientes', null, 160, 95);
                    $data['image'] = $arquivo;
                }else{
                    //$data['image'] = 'default.png';
                }
            }else{
                //$data['image'] = 'default.png';
            }
        }else{
            //$data['image'] = 'default.png';
        }
    }
}
