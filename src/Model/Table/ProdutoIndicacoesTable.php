<?php
namespace App\Model\Table;

use App\Model\Entity\ProdutoObjetivo;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProdutoObjetivos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ProdutoBase
 * @property \Cake\ORM\Association\BelongsTo $Objetivos
 */
class ProdutoIndicacoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('produto_indicacoes');
        $this->displayField('url');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('email');

        $validator
            ->notEmpty('url');

        $validator
            ->notEmpty('aceitou');

        return $validator;
    }
}
