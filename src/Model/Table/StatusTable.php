<?php
namespace App\Model\Table;

use App\Model\Entity\Status;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Status Model
 *
 * @property \Cake\ORM\Association\HasMany $Academias
 * @property \Cake\ORM\Association\HasMany $Banners
 * @property \Cake\ORM\Association\HasMany $Clientes
 * @property \Cake\ORM\Association\HasMany $Embalagens
 * @property \Cake\ORM\Association\HasMany $Groups
 * @property \Cake\ORM\Association\HasMany $Marcas
 * @property \Cake\ORM\Association\HasMany $Objetivos
 * @property \Cake\ORM\Association\HasMany $PagamentoFormas
 * @property \Cake\ORM\Association\HasMany $ProdutoBase
 * @property \Cake\ORM\Association\HasMany $ProdutoComentarios
 * @property \Cake\ORM\Association\HasMany $Produtos
 * @property \Cake\ORM\Association\HasMany $Transportadoras
 * @property \Cake\ORM\Association\HasMany $Users
 */
class StatusTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('status');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasMany('Academias', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('Banners', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('Clientes', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('Embalagens', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('Groups', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('Marcas', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('Objetivos', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('PagamentoFormas', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('ProdutoBase', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('ProdutoComentarios', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('Produtos', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('Transportadoras', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'status_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}
